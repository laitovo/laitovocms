Bizzone
=======

Сервис управления интернет магазинами и другими e-commerce инструментами, построенный на базе [Yii 2](http://www.yiiframework.com/)

Руководство разработчика [docs/guide/README.md](docs/guide/README.md).


СТРУКТУРА
---------

```
common
    assets/              общие ресурсы
    config/              общие конфиги
    mail/                шаблоны писем
    models/              базовые модели
    rbac/                правила rbac
    widgets/             общие виджеты
console
    config/              конфиги для консольного приложения
    controllers/         команды консоли
    migrations/          миграции
    models/              модели
    runtime/             файлы сгенерированные во время выполнения консольных команд
backend
    assets/              ресурсы бекенда
    config/              конфиги бекенда
    controllers/         контроллеры бекенда
    models/              модели бекенда
    modules/             модули бекенда
    runtime/             файлы сгенерированные во время работы в бекенде
    themes/              шаблоны бекенда
    views/               представления бекенда
    web/                 корневая директория для бекенда
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  зависимые пакеты
environments/            окружения
docs/                    документация
upload/                  загруженные файлы
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```
