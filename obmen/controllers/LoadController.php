<?php
namespace obmen\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\web\NotFoundHttpException;

/**
 * Load controller
 */
class LoadController extends Controller
{


    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex($file)
    {
        $response = Yii::$app->response;

        $file=Yii::$app->basePath.'/files/'.$file;

        if (file_exists($file) && is_file($file)){
            return $response->sendFile($file);
        } else {
            throw new NotFoundHttpException;
        }
    }
}
