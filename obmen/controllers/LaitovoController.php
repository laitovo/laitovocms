<?php
namespace obmen\controllers;

use common\models\logistics\StorageState;
use Yii;
use yii\db\Query;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use obmen\models\laitovo\Cars;
use obmen\models\laitovo\ErpNaryad;
use obmen\models\laitovo\ErpOrder;
use yii\base\Module;

/**
 * Laitovo controller
 */
class LaitovoController extends Controller
{
    private $_shipmentManager;

    public function __construct($id, Module $module, array $config = [])
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');

        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionCars($last=null)
    {
        $query = Cars::find();

        if ($last) $query->andWhere(['>','updated_at',time()-$last]);

        return new ActiveDataProvider([
            'pagination' => [
                'pageSize' => 100000,
            ],
            'query' => $query,
        ]);
    }

    public function actionCar($article)
    {
        $query = Cars::find()->where(['article'=>$article]);

        return $query->one();
    }

    public function actionNaryad($last=null)
    {
        $query = ErpOrder::find()->where(['or',['status'=>ErpOrder::STATUS_READY],['status'=>ErpOrder::STATUS_CANCEL]]);

        if ($last) $query->andWhere(['>','updated_at',time()-$last]);

        return new ActiveDataProvider([
            'pagination' => [
                'pageSize' => 100000,
            ],
            'query' => $query,
        ]);
    }

    /**
     * Обмен остатками на складе с 1С
     * @param null $last
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionStorage($last=0)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
          SELECT 
            MAX(a.id) as id,
            b.article, 
            COUNT(b.article) as quantity 
          FROM logistics_storage_state as a
          LEFT OUTER JOIN logistics_naryad as b ON a.upn_id = b.id
          where a.id > :last_id
          GROUP BY b.article", [':last_id' => $last]);

        $data = $command->queryAll();

        $result = [];

        foreach ($data as $row) {
            $article = $row['article'];
            $items = explode('-',$article);
            $main = true;
            if (count($items) == 4 && $items[0] == 'OT') { $main = false; }
            $window = $items['0'];
            $car    = $main ? $items['2'] : $items['1'];
            $type   = $main ? $items['3'] : $items['2'];
            $cloth  = $main ? $items['4'] : $items['3'];

            $row['barcode'] = $window
                . '' . substr(10000 + $car, 1)
                . '' . substr(100 + intval($type), 1)
                . '' . $cloth;
            $result[] = $row;
        }

        return $result;
    }

    /**
     * Обмен отгрузками с 1С (для создания реализации в 1С)
     * @param null $data
     * @return array
     */
    public function actionShipments($data = null)
    {
        $result = [];
        foreach ($this->_shipmentManager->findShipmentsWasHandledInLast($data) as $shipment) {
            $handledAt = $this->_shipmentManager->getHandledAt($shipment);
            $deliveryPrice = $this->_shipmentManager->getDeliveryPrice($shipment);
            $row         = [];
            $row['id']   = $this->_shipmentManager->getId($shipment);
            $row['date'] = $handledAt ? date('d.m.Y H:i:s', $handledAt) : null;
            if ($deliveryPrice) {
                $row['delivery'] = $deliveryPrice;
            }
            $row['naryads'] = [];
            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
            foreach ($orderEntries as $orderEntry) {
                $row['naryads'][] = [
                    'barcode' => $orderEntry->order_barcode,
                    'article' => $orderEntry->barcode,
                ];
            }
            $result[] = $row;
        }

        return $result;
    }

    /**
     * Обмен отгрузками с 1С (для проведения реализаций в 1С)
     * @return array
     */
    public function actionShipmentsForAccounting()
    {
        $result = [];
        foreach ($this->_shipmentManager->findShipmentsCanBeAccounted() as $shipment) {
            $shipmentDate = $this->_shipmentManager->getShipmentDate($shipment);
            $row['id']   = $this->_shipmentManager->getId($shipment);
            $row['date'] = $shipmentDate ? date('d.m.Y H:i:s', $shipmentDate) : null;
            $result[] = $row;
        }

        return $result;
    }

    /**
     * Проведение отгрузки в 1С (приём от 1С уведомления о проведенной реализации)
     * @return string
     */
    public function actionAccountShipment($id)
    {
        $shipment = $this->_shipmentManager->findById($id);
        if (!$shipment || !$this->_shipmentManager->account($shipment)) {
            return 'error';
        }

        return 'ok';
    }
}
