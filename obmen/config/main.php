<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'obmen',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'obmen\controllers',
    'bootstrap' => ['log'],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'order' => 'order/index',
                'client' => 'team/index',
                'product' => 'product/index',
                'category' => 'product/category',
                'load' => 'load/index',
                'laitovo-cars' => 'laitovo/cars',
                'laitovo-cars/<article:\d+>' => 'laitovo/car',
                'laitovo-naryad' => 'laitovo/naryad',
                'laitovo-storage' => 'laitovo/storage',
                'laitovo-ship' => 'laitovo/shipments',
                'laitovo-ship-account' => 'laitovo/shipments-for-accounting',
                'laitovo-ship-account-ok' => 'laitovo/account-shipment',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => false,
            'loginUrl' => null,

        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
