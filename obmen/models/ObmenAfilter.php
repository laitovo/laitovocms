<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace obmen\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use obmen\models\team\Team;
use obmen\models\team\Organization;
use obmen\models\order\Product;
use obmen\models\order\Price;
use obmen\models\order\Category;
use obmen\models\order\Order;
use obmen\models\order\Item;

/**
 * Обмен с автофильтром
 *
 * @author basyan <basyan@yandex.ru>
 */
class ObmenAfilter extends Model
{

    private $data=null;
    private $filename;
    private $team=null;

    private function formatdate($date)
    {
        $date=explode('.', $date);
        $date[2]='20'.$date[2];
        return implode('.', $date);
    }

    public function __construct($data,$filename='',$config = [])
    {
        $this->data=$data;
        $this->filename=$filename;
        if (($this->team=\common\models\team\Team::findOne(2))===null)
            throw new NotFoundHttpException();

        parent::__construct($config);
    }

    /**
     * Загрузка контрагентов
     * @return type
     */
    public function importclients()
    {
        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_Контрагенты']['ЭлСправочника_Контрагенты'] as $key => $value) {
            if(trim((string)$value['Синхронизировать'])==1){

                $load['Team']=[
                    'name'=>trim((string)$value['Наименование']),
                    'discount'=>trim((string)$value['Скидка']),
                    'status'=>1,
                    'updated_at'=>(int)strtotime($this->formatdate(trim((string)$value['ДатаИзменения'])).' '.trim((string)$value['ВремяИзменения']))?:null,
                    'json'=>Json::encode([
                        'file'=>$this->filename,
                        'web'=>trim((string)$value['АдресСайта']),
                        'email'=>trim((string)$value['Мыло']),
                        'phone'=>trim((string)$value['Телефоны']),
                        'adres'=>trim((string)$value['ФактическийАдрес']),
                        'postadres'=>trim((string)$value['ПочтовыйАдрес']),
                        'kodpoluch'=>trim((string)$value['КодПолучателя']),
                        'kontaktperson'=>trim((string)$value['КонтактноеЛицо']),
                        'dopinfo'=>trim((string)$value['ДополнительныеСв']),
                        'payform'=>trim((string)$value['ФормаОплаты']),
                        'otsrochday'=>trim((string)$value['ДнейОтсрочки']),
                    ]),
                ];

                $team=Team::find()->select('clients.*')->joinWith('clients clients')->where(['team_id'=>$this->team->id, 'clients.name'=>$load['Team']['name']])->one();
                
                if (!$team){
                    $team=new Team;
                } else {
                    $load['Team']['json']=Json::encode( ArrayHelper::merge(Json::decode($team->json ? $team->json : '[]'),Json::decode($load['Team']['json'])) );
                }

                $team->load($load);

                if ($team->save()){

                    $this->team->unlink('clients', $team, true);
                    $this->team->link('clients', $team);

                    $load['Organization']=[
                        'team_id'=>$team->id,
                        'name'=>trim((string)$value['ПолнНаименование']),
                        'json'=>Json::encode([
                            'inn'=>trim((string)$value['ИНН']),
                            'uradres'=>trim((string)$value['ЮридическийАдрес']),
                            'okpo'=>trim((string)$value['ОКПО']),
                        ]),
                    ];

                    $organization=Organization::find()->where(['team_id'=>$load['Organization']['team_id'], 'name'=>$load['Organization']['name']])->one();
                    
                    if (!$organization){
                        $organization=new Organization;
                    } else {
                        $load['Organization']['json']=Json::encode( ArrayHelper::merge(Json::decode($organization->json ? $organization->json : '[]'),Json::decode($load['Organization']['json'])) );
                    }

                    $organization->load($load);
                    $organization->save();
                } else {
                    // print_r($team->getErrors());
                }

            }
        }
    }

    /**
     * Загрузка номенклатуры
     * @return type
     */
    public function importproducts()
    {
        $cat=Category::findOne(2838);//Типы товаров
        foreach ($this->data['ОбъектыМДТипа_Перечисление']['Перечисление_ТипыТоваров']['ЗначениеПеречисления_ТипыТоваров'] as $key => $value) {

            $load['Category']=[
                'team_id'=>$this->team->id,
                'parent_id'=>$cat->id,
                'name'=>trim((string)$value['Наименование']),
            ];

            $category=Category::find()->where(['team_id'=>$load['Category']['team_id'],'parent_id'=>$load['Category']['parent_id'], 'name'=>$load['Category']['name']])->one();
            if (!$category){
                $category=new Category;
            }
            $category->load($load);

            if ($category->save()){
                $typetovar[trim((string)$value['ВнутреннийИдентификатор'])]=$category;
            }
        }

        $cat=Category::findOne(2839);//Типы номенклатуры
        foreach ($this->data['ОбъектыМДТипа_Перечисление']['Перечисление_ТипыНоменклатуры']['ЗначениеПеречисления_ТипыНоменклатуры'] as $key => $value) {

            $load['Category']=[
                'team_id'=>$this->team->id,
                'parent_id'=>$cat->id,
                'name'=>trim((string)$value['Наименование']),
            ];

            $category=Category::find()->where(['team_id'=>$load['Category']['team_id'],'parent_id'=>$load['Category']['parent_id'], 'name'=>$load['Category']['name']])->one();
            if (!$category){
                $category=new Category;
            }
            $category->load($load);

            if ($category->save()){
                $typenomenkl[trim((string)$value['ВнутреннийИдентификатор'])]=$category;
            }
        }

        $cat=Category::findOne(2847);//Типы фильтра
        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_ТипФильтра']['ЭлСправочника_ТипФильтра'] as $key => $value) {

            $load['Category']=[
                'team_id'=>$this->team->id,
                'parent_id'=>$cat->id,
                'name'=>trim((string)$value['Наименование']),
            ];

            $category=Category::find()->where(['team_id'=>$load['Category']['team_id'],'parent_id'=>$load['Category']['parent_id'], 'name'=>$load['Category']['name']])->one();
            if (!$category){
                $category=new Category;
            }
            $category->load($load);

            if ($category->save()){
                $typefilter[trim((string)$value['ВнутреннийИдентификатор'])]=$category;
            }
        }

        $cat=Category::findOne(2856);//Категории
        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_Номенклатура']['ГрСправочника_Номенклатура'] as $key => $value) {

            $load['Category']=[
                'team_id'=>$this->team->id,
                'parent_id'=>$cat->id,
                'name'=>trim((string)$value['Наименование']),
                'json'=>Json::encode([
                    'id'=>trim((string)$value['ВнутреннийИдентификатор']),
                ]),
            ];

            $category=Category::find()->where(['and',['team_id'=>$load['Category']['team_id']], ['name'=>$load['Category']['name']], ['like','json',trim($load['Category']['json'],'{}')] ])->one();
            if (!$category){
                $category=new Category;
            } else {
                $load['Category']['json']=Json::encode( ArrayHelper::merge(Json::decode($category->json ? $category->json : '[]'),Json::decode($load['Category']['json'])) );
            }
            $category->load($load);

            if ($category->save()){
                $categor1[trim((string)$value['ВнутреннийИдентификатор'])]=$category;
            }
        }
        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_Номенклатура']['ГрСправочника_Номенклатура'] as $key => $value) {

            $load['Category']=[
                'team_id'=>$this->team->id,
                'name'=>trim((string)$value['Наименование']),
                'json'=>Json::encode([
                    'id'=>trim((string)$value['ВнутреннийИдентификатор']),
                ]),
            ];

            $category=Category::find()->where(['and',['team_id'=>$load['Category']['team_id']], ['name'=>$load['Category']['name']], ['like','json',trim($load['Category']['json'],'{}')] ])->one();
            if ($category){
                if (trim((string)$value['Родитель']) && isset($categor1[trim((string)$value['Родитель'])]) ){
                    $category->parent_id=$categor1[trim((string)$value['Родитель'])]->id;
                    $category->save();
                }
            }
        }

        //ндс
        $sprnds=isset($this->data['ОбъектыМДТипа_Справочник']['Справочник_СтавкиНДС']['ЭлСправочника_СтавкиНДС']) ? $this->data['ОбъектыМДТипа_Справочник']['Справочник_СтавкиНДС']['ЭлСправочника_СтавкиНДС'] : $this->data['ОбъектыМДТипа_Справочник']['Справочник_СтавкиНДС'][0];
        foreach ($sprnds as $key => $value) {
            $stavkands[trim((string)$value['ВнутреннийИдентификатор'])]=trim((string)$value['Ставка']);
        }

        //единицы измерений
        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_ЕдиницыИзмерений']['ЭлСправочника_ЕдиницыИзмерений'] as $key => $value) {
            $izmeren[trim((string)$value['ВнутреннийИдентификатор'])]=trim((string)$value['Наименование']);
        }

        //номенклатура
        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_Номенклатура']['ЭлСправочника_Номенклатура'] as $key => $value) {
            if(trim((string)$value['Синхронизировать'])==1){

                $_price=null;
                $_nds=null;

                foreach ($value as $key1 => $value1) {
                    if ($key1=='РеквизитСправочника_Номенклатура_Цена')
                        $_price=trim((string)$value1['ЗначениеРеквизита']);
                    if ($key1=='РеквизитСправочника_Номенклатура_СтавкаНДС')
                        $_nds=$stavkands[trim((string)$value1['ЗначениеРеквизита'])];
                }

                $load['Product']=[
                    'team_id'=>$this->team->id,
                    'name'=>trim((string)$value['Наименование']),
                    'unit'=>$izmeren[trim((string)$value['ЕдиницаИзмерения'])],
                    'updated_at'=>(int)strtotime($this->formatdate(trim((string)$value['ДатаИзменения'])).' '.trim((string)$value['ВремяИзменения']))?:null,
                    'status'=>1,
                    'json'=>Json::encode([
                        'fullname'=>trim((string)$value['ПолнНаименование']),
                        'nameexport'=>trim((string)$value['НаименованиеЭкспорт']),
                        'tnvd'=>trim((string)$value['КодТНВД']),
                        'vkorobke'=>trim((string)$value['ВКоробке']),
                        'obemkor'=>trim((string)$value['Объем']),
                        'diam_narug_d'=>trim((string)$value['ДиметрНаружный']),
                        'diam_vnutr_d1'=>trim((string)$value['ДиаметрВнутренний']),
                        'diam_vnutr_d2'=>trim((string)$value['ДиаметрВнутренний2']),
                        'diam_d3'=>trim((string)$value['Диаметр']),
                        'dlina_l'=>trim((string)$value['Длина']),
                        'shirina_b'=>trim((string)$value['Ширина']),
                        'rezba_g'=>trim((string)$value['Резьба']),
                        'rezba_f'=>trim((string)$value['Резьба2']),
                        'visota_h'=>trim((string)$value['Высота']),
                        'mass'=>trim((string)$value['Вес']),
                        'eb'=>trim((string)$value['ЕстьКомплект']),
                        'diam_narug_d_eb'=>trim((string)$value['ДиметрНаружныйЭБ']),
                        'diam_vnutr_d1_eb'=>trim((string)$value['ДиаметрВнутреннийЭБ']),
                        'diam_vnutr_d2_eb'=>trim((string)$value['ДиаметрВнутренний2ЭБ']),
                        'diam_d3_eb'=>trim((string)$value['ДиаметрЭБ']),
                        'visota_h_eb'=>trim((string)$value['ВысотаН']),
                        'davlenie_klapana'=>trim((string)$value['ДавлениеКлапана']),
                        'klapan'=>trim((string)$value['ЕстьКлапан']),
                    ]),
                ];

                $product=Product::find()->where(['team_id'=>$load['Product']['team_id'], 'name'=>$load['Product']['name']])->one();

                if (!$product){
                    $product=new Product;
                } else {
                    $load['Product']['json']=Json::encode( ArrayHelper::merge(Json::decode($product->json ? $product->json : '[]'),Json::decode($load['Product']['json'])) );
                }
                
                $product->load($load);

                if ($product->save()){

                    if (trim((string)$value['ТипНоменклатуры'])){
                        $product->unlink('categories', $typenomenkl[trim((string)$value['ТипНоменклатуры'])], true);
                        $product->link('categories', $typenomenkl[trim((string)$value['ТипНоменклатуры'])]);
                    }
                    if (trim((string)$value['ТипТовара'])){
                        $product->unlink('categories', $typetovar[trim((string)$value['ТипТовара'])], true);
                        $product->link('categories', $typetovar[trim((string)$value['ТипТовара'])]);
                    }
                    if (trim((string)$value['ТипФильтра'])){
                        $product->unlink('categories', $typefilter[trim((string)$value['ТипФильтра'])], true);
                        $product->link('categories', $typefilter[trim((string)$value['ТипФильтра'])]);
                    }

                    if (trim((string)$value['Родитель'])){
                        $product->unlink('categories', $categor1[trim((string)$value['Родитель'])], true);
                        $product->link('categories', $categor1[trim((string)$value['Родитель'])]);
                    }
                    if ($_price!==null){

                        $load['Price']=[
                            'region_id'=>1,
                            'product_id'=>$product->id,
                            'price'=>$_price,
                            'nds'=>$_nds,
                            'currency'=>"RUB",
                            'nds_in'=>0,
                            'maxdiscount'=>isset($value['БезСкидки']) ? (float)$value['БезСкидки']:null,
                        ];


                        $price=Price::find()->where(['region_id'=>$load['Price']['region_id'], 'product_id'=>$load['Price']['product_id']])->one();

                        if (!$price){
                            $price=new Price;
                        }
                        
                        $price->load($load);
                        $price->save();
                    }

                } else {
                    // print_r($product->getErrors());
                }
            }
        }
    }

    /**
     * Загрузка заказов
     * @return type
     */
    public function importorders()
    {
        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_Контрагенты']['ЭлСправочника_Контрагенты'] as $key => $value) {

            $load['Team']=[
                'name'=>trim((string)$value['Наименование']),
            ];

            $team=Team::find()->select('clients.*')->joinWith('clients clients')->where(['team_id'=>$this->team->id, 'clients.name'=>$load['Team']['name']])->one();
            
            if (!$team){
                $team=new Team;
                $team->status=0;
            }
            
            $team->load($load);

            if ($team->save()){
                $this->team->unlink('clients', $team, true);
                $this->team->link('clients', $team);
            }
            $clients[trim((string)$value['ВнутреннийИдентификатор'])]=$team;
            $organizations[trim((string)$value['ВнутреннийИдентификатор'])]=trim((string)$value['ПолнНаименование']);
        }

        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_Номенклатура']['ЭлСправочника_Номенклатура'] as $key => $value) {

            $load['Product']=[
                'team_id'=>$this->team->id,
                'name'=>trim((string)$value['Наименование']),
            ];

            $product=Product::find()->where(['team_id'=>$load['Product']['team_id'], 'name'=>$load['Product']['name']])->one();
            
            if (!$product){
                $product=new Product;
                $product->status=0;
            }
            
            $product->load($load);

            // $product->save();
            $products[trim((string)$value['ВнутреннийИдентификатор'])]=$product;
        }

        //ндс
        foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_ВариантыРасчетаНалогов']['ЭлСправочника_ВариантыРасчетаНалогов'] as $key => $value) {
            $reschetnds[trim((string)$value['ВнутреннийИдентификатор'])]=trim((string)$value['Наименование']);
        }

        //форма отгрузки
        if (isset($this->data['ОбъектыМДТипа_Справочник']['Справочник_ФормаОтгрузки'])) {
            foreach ($this->data['ОбъектыМДТипа_Справочник']['Справочник_ФормаОтгрузки']['ЭлСправочника_ФормаОтгрузки'] as $key => $value) {
                $formgruz[trim((string)$value['ВнутреннийИдентификатор'])]=trim((string)$value['Наименование']);
            }
        }

        $maxid=[];
        $loaddate=strtotime(date('01.01.Y'));
        foreach ($this->data['ОбъектыМДТипа_Документ']['Документ_Счет'] as $key => $value) {
            if(!trim((string)$value['СчетАФ'])){

                $org=1;
                if (isset($value['СчетАФ']))
                    $org=2;
                
                $typends=$reschetnds[trim((string)$value['ВариантРасчетаНалогов'])];

                $sum=0;
                $ndssum=0;
                $ndstotal=0;
                foreach ($value as $key1 => $value1) {
                    if ($key1=='СтрокаДокумента_Счет'){
                        $sum+=($typends=='НДС в сумме') ? ((float)$value1['Сумма']-(float)$value1['НДС']) : (float)$value1['Сумма'];
                        $ndssum+=(float)$value1['НДС'];
                    }
                }
                if ($sum)
                    $ndstotal=round($ndssum/$sum*100);

                $load['Order']=[
                    'team_id'=>$this->team->id,
                    'organization_id'=>$org,
                    'client_team_id'=>trim((string)$value['Контрагент']) ? $clients[trim((string)$value['Контрагент'])]->id : null,
                    'client_organization_id'=>(trim((string)$value['Контрагент']) && ($organization=$clients[trim((string)$value['Контрагент'])]->getOrganizations()->andWhere(['name'=>$organizations[trim((string)$value['Контрагент'])]])->one())!==null) ? $organization->id : null,
                    'number'=>trim((int)$value['НомерДок']),
                    'created_at'=>(int)strtotime($this->formatdate(trim((string)$value['ДатаДок'])).' '.trim((string)$value['ВремяДок'])),
                    'discount'=>trim((float)$value['Скидка']),
                    'updated_at'=>(int)strtotime($this->formatdate(trim((string)$value['ДатаИзменения'])).' '.trim((string)$value['ВремяИзменения']))?:null,
                    'json'=>Json::encode([
                        'coment'=>trim((string)$value['Комментарий']),
                        'formotgruz'=>trim((string)$value['ФормаОтгрузки']) ? $formgruz[trim((string)$value['ФормаОтгрузки'])] : '',
                        'shipdate'=>strtotime($this->formatdate(trim((string)$value['ДатаОтгрузки']))) ? $this->formatdate(trim((string)$value['ДатаОтгрузки'])) : '',
                        'noload'=>trim((string)$value['НеВыгружать']),
                        'noinreport'=>trim((string)$value['НеВключатьВОтчет']),
                        'currency'=>"RUB",
                        'nds_in'=>($typends=='НДС в сумме') ? "1" : "0",
                        'nds'=>$ndstotal,
                    ]),
                ];

                $order=Order::find()->where(['team_id'=>$load['Order']['team_id'], 'organization_id'=>$load['Order']['organization_id'], 'number'=>$load['Order']['number'], 'created_at'=>$load['Order']['created_at']])->one();

                if (!$order){
                    $order=new Order;
                } else {
                    $load['Order']['json']=Json::encode( ArrayHelper::merge(Json::decode($order->json ? $order->json : '[]'),Json::decode($load['Order']['json'])) );
                }

                $order->load($load);

                if ($order->save()){
                    $loaditem=[];

                    foreach ($value as $key1 => $value1) {
                        if ($key1=='СтрокаДокумента_Счет'){
                            $nds=0;
                            if (trim((float)$value1['Сумма'])){
                                $nds=round((float)$value1['НДС']/( ($typends=='НДС в сумме') ? ((float)$value1['Сумма']-(float)$value1['НДС']) : (float)$value1['Сумма'] )*100);
                            }

                            $load['Item']=[
                                'order_id'=>$order->id,
                                'product_id'=>trim((string)$value1['Товар']) ? $products[trim((string)$value1['Товар'])]->id : null,
                                'name'=>trim((string)$value1['Товар']) ? $products[trim((string)$value1['Товар'])]->name : null,
                                'retail'=>(trim((string)$value1['Товар']) && $products[trim((string)$value1['Товар'])]->prices) ? $products[trim((string)$value1['Товар'])]->prices[0]->price(false,'RUB') : null,
                                'price'=>trim((float)$value1['Цена']),
                                'quantity'=>trim((float)$value1['Количество']),
                                'nds'=>$nds,
                                'nds_in'=>($typends=='НДС в сумме') ? 1 : 0,
                                'lock'=>(boolean)trim((string)$value1['ФиксЦена']),
                                'currency'=>"RUB",
                            ];

                            $item=Item::find()->where(['order_id'=>$load['Item']['order_id'], 'name'=>$load['Item']['name']])->one();

                            if (!$item){
                                $item=new Item;
                            }

                            $item->load($load);
                            $item->save();
                            $loaditem[$item->id]=1;
                        }
                    }
                    
                    foreach ($order->items as $row) {
                        if (!isset($loaditem[$row->id]))
                            $row->delete();
                    }

                } else {
                    // print_r($order->getErrors());
                }
                if ($load['Order']['created_at']>=$loaddate)
                    $maxid[trim((int)$value['НомерДок'])]=trim((int)$value['НомерДок']);
            }
        }

        // $teamjson=Json::decode($this->team->json ? $this->team->json : '{}');
        // $teamjson['nextidorder']=$this->team->getOrders()->max('number')+1;
        // $this->team->json=Json::encode($teamjson);
        // $this->team->save();

        $org1=$this->team->getOrganizations()->andWhere(['id'=>$org])->one();
        if ($org1){
            $maxnumber=$org1->getOrders()->andWhere(['>=','created_at',$loaddate])->max('number');
            $maxid[$maxnumber]=$maxnumber;
            $teamjson=Json::decode($org1->json ? $org1->json : '{}');
            $teamjson['nextidorder']=max($maxid)+5;
            $org1->json=Json::encode($teamjson);
            $org1->save();
        }

    }

}
