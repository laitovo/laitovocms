<?php

namespace obmen\models\order;

use Yii;
use yii\helpers\Json;
use obmen\models\team\Team;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Product extends \common\models\order\Product
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['team_id'], 'required'],
            [['team_id','updated_at'], 'number'],
            [['name','unit'], 'string', 'max'=>255],
            [['json'], 'string'],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['status'], 'boolean'],
        ];
    }

    public function beforeValidate()
    {
        if ($this['updated_at']>$this->getOldAttribute('updated_at') || $this->isNewRecord){
            return true;
        } else {
            return false;
        }
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            return Json::decode($this->json);
        };

        $fields['created_at']=function () {
            return $this->created_at ? Yii::$app->formatter->asDatetime($this->created_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        $fields['updated_at']=function () {
            return $this->updated_at ? Yii::$app->formatter->asDatetime($this->updated_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        return $fields;
    }

    public function extraFields()
    {
        return ['prices','categories'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('{{%product_category}}', ['product_id' => 'id']);
    }

}
