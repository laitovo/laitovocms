<?php

namespace obmen\models\order;

use Yii;
use yii\helpers\Json;
use common\models\Region;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Price extends \common\models\order\Price
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['product_id','region_id'], 'required'],
            [['product_id','region_id','price','nds','maxdiscount'], 'number'],
            [['currency'], 'string', 'max'=>255],
            [['nds_in'], 'boolean'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            return Json::decode($this->json);
        };

        $fields['created_at']=function () {
            return $this->created_at ? Yii::$app->formatter->asDatetime($this->created_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        $fields['updated_at']=function () {
            return $this->updated_at ? Yii::$app->formatter->asDatetime($this->updated_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        return $fields;
    }

}
