<?php

namespace obmen\models\order;

use Yii;
use yii\helpers\Json;
use obmen\models\team\Team;
use obmen\models\team\Organization;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Order extends \common\models\order\Order
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['team_id'], 'required'],
            [['team_id','organization_id','client_team_id','client_organization_id','number','created_at','updated_at'], 'number'],
            [['json'], 'string'],
            [['discount'], 'number','min'=>0,'max'=>100],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['organization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['organization_id' => 'id']],
            [['client_team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['client_team_id' => 'id']],
            [['client_organization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['client_organization_id' => 'id']],
        ];
    }

    public function beforeValidate()
    {
        if ($this['updated_at']>$this->getOldAttribute('updated_at') || $this->isNewRecord){
            return true;
        } else {
            return false;
        }
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            return Json::decode($this->json);
        };

        $fields['created_at']=function () {
            return $this->created_at ? Yii::$app->formatter->asDatetime($this->created_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        $fields['updated_at']=function () {
            return $this->updated_at ? Yii::$app->formatter->asDatetime($this->updated_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        return $fields;
    }

    public function extraFields()
    {
        return ['items','region','clientTeam','clientOrganization'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'client_team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'client_organization_id']);
    }

}
