<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace obmen\models;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\StorageState;
use common\models\laitovo\ErpOrdersUserReport;
use common\models\logistics\ArticleStatistics;
use core\models\article\Article;
use obmen\models\laitovo\CarsAnalog;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use obmen\models\laitovo\Cars;
use obmen\models\laitovo\ErpOrder;
use yii\httpclient\Client;

/**
 * Обмен с сайтом лайтово
 *
 * @author basyan <basyan@yandex.ru>
 */
class ObmenLaitovoSite extends Model
{

    private $db;

    public function __construct($config = [])
    {
        $this->db = new \yii\db\Connection([
            'dsn' => 'mysql:host=' . Yii::$app->params['obmen_laitovo_site']['host'] . ';port=' . Yii::$app->params['obmen_laitovo_site']['port'] . ';dbname=' . Yii::$app->params['obmen_laitovo_site']['dbname'] . '',
            'username' => Yii::$app->params['obmen_laitovo_site']['username'],
            'password' => Yii::$app->params['obmen_laitovo_site']['password'],
            'charset' => 'utf8',
        ]);

        parent::__construct($config);
    }

    public function loadOrders()
    {
        $command = $this->db->createCommand("select big2.*, sum(total) as payed from (select * from `ru_bill` where status=1) as bill1 right outer join (select big1.*, del.name2 from `ru_delivery_method` del right outer join (select ord.*, sum(price * `count`) as totalprice from `ru_orders_in` as inn inner join (select o2.*, IF(o2.pid = '1', l2.name, l2.name) as region2 from ru_location as l2 right outer join  (select o1.*,l.pid, IF(l.pid = '1', '', l.name)  as region1 from ru_location as l right outer join (select oo1.*,uu.name2 as otvetstven from (select o.id, o.type, o.text, o.text2, o.textproduction, u.email, u.inn, o.username as name, o.userphone as phone, o.region, o.delivery,o.`responsible`, o.costpayment, o.index, o.street, o.house, o.flat, o.address, o.kzt, o.kzt_d, o.factor, o.`createdate`, o.shipdate, o.payment_opt from `ru_orders` as o inner join `ru_user` as u on u.id=o.client where o.`shipper`='100001' and o.status='1' and o.biz=1 and o.condition='gotovo_k_proizvodstvu' and o.remote_storage_id is null) as oo1 inner join `user` as uu on oo1.`responsible`=uu.id) as o1 on o1.region=l.id) as o2 on IF(o2.pid = '1', o2.region, o2.pid)=l2.id) as ord on inn.pid=ord.id group by id) big1 on big1.delivery=del.id) as big2 on big2.id=bill1.order group by id;");
        $rows = $command->queryAll();

        $saved = [];

        foreach ($rows as $key => $order) {

            if (!ErpOrder::findOne($order['id'])) {

                $erporder = new ErpOrder();
                $erporder->id = $order['id'];
                $json['category'] = $order['type'] ? 'Опт' : 'Физик';
                $json['username'] = $order['name'];
                $json['userphone'] = $order['phone'];
                $json['useremail'] = $order['email'];
                $json['userinn'] = $order['inn'];
                $json['comment'] = $order['text'];
                $json['innercomment'] = $order['textproduction'];
                $json['delivery'] = $order['name2'];
                $json['manager'] = $order['otvetstven'];
                $json['address'] = ($order['region2'] . ', ' . $order['region1'] . ', ' . $order['address']);
                $json['address'] = implode(', ', [$order['region2'], $order['region1'], $order['address']]);

                if (in_array($order['name2'], ['Германия', 'Казахстан', 'Беларусь', 'Киргизия', 'Казахстан СДЭК', 'Индия', 'Гонконг','Сербия'])) {
                    $json['country'] = explode(' ', $order['name2'])[0];
                    $json['category'] = 'Экспорт';
                }

                $command = $this->db->createCommand("select o.price, o.`count`, c.`barcode`, c.`article`, c.`newarticle`, c.name, o.setid, o.settext, o.without_labels, o.half_stake, CONCAT_WS(' ', o.info,o.text) as itemtext from `ru_orders_in` as o inner join `ru_catalog` as c on c.id=o.goodid where o.pid=" . $order['id']);
                $items = $command->queryAll();

                $orderitems = [];
                foreach ($items as $key => $item) {

                    $window = mb_substr($item['barcode'], 0, 2, 'utf-8');
                    $window2 = mb_strtolower($window);
                    $window2 = str_replace('fw', 'ps', $window2);
                    $window2 = str_replace('fv', 'pf', $window2);
                    $window2 = str_replace('fd', 'pb', $window2);
                    $window2 = str_replace('rd', 'zb', $window2);
                    $window2 = str_replace('rv', 'zf', $window2);
                    $window2 = str_replace('bw', 'zs', $window2);
                    $cararticle = (int)mb_substr($item['barcode'], 2, 4, 'utf-8');
                    $typewindow = (int)mb_substr($item['barcode'], 6, 2, 'utf-8');
                    $color = (int)mb_substr($item['barcode'], 8, 1, 'utf-8');

                    $car = Cars::find()->where(['article' => $cararticle])->one();

                    if ($car) {
                        $auto = $car->name;
                        $lekalo = $car->json('nomerlekala');
                        $comment = $car->json('info');
                        $natyag = '';

                        if ($color == 5) {//chiko
                            switch ($typewindow) {
                                case '2':
                                    $type = 'smoke';
                                    break;
                                case '3':
                                    $type = 'mirror';
                                    break;
                                case '6':
                                    $type = 'short';
                                    break;

                                default:
                                    $type = 'standart';
                                    break;
                            }
                            $natyag = @$car->json('_' . mb_strtolower($window))['chiko'][$type]['natyag'];
                        }
                        $visota = @$car->json('visota' . mb_strtolower($window2));
                        $magnit = @$car->json('_' . mb_strtolower($window))['magnit'];
                        $hlyastik = @$car->json('_' . mb_strtolower($window))['hlyastik'];

                    } else {
                        $auto = '';
                        $lekalo = '';
                        $natyag = '';
                        $visota = '';
                        $magnit = '';
                        $hlyastik = '';
                        $comment = '';
                    }

                    $orderitems[] = [
                        'car' => $auto,
                        'lekalo' => $lekalo,
                        'natyag' => $natyag,
                        'visota' => $visota,
                        'magnit' => $magnit,
                        'hlyastik' => $hlyastik,

                        'name' => $item['name'],
                        'comment' => $comment,
                        'article' => $item['newarticle'],
                        'barcode' => $item['barcode'],
                        'quantity' => $item['count'],
                        // 'price'=>$item['price'],
                        'settext' => $item['settext'],
                        'itemtext' => $item['itemtext'],

                        'halfStake' => $item['half_stake'],
                        'withoutLabels' => $item['without_labels'],
                    ];
                }
                $json['items'] = $this->toOneitem($orderitems);
                $erporder->json = Json::encode($json);

                if ($erporder->save()) {
                    $saved[] = $erporder->id;
                }
            }
        }

        echo 'Загружено: ' . count($saved);
    }

    protected function toOneitem($items)
    {
        $newitems = [];
        foreach ($items as $key => $value) {
            $quantity = $value['quantity'];
            if ($quantity > 0) {
                while ($quantity >= 1) {
                    $value['quantity'] = 1;
                    $newitems[] = $value;
                    $quantity--;
                }
            }
        }
        return $newitems;
    }

    protected function distributeInSets($items)
    {
        $newItemList = [];

        $sets = [];
        foreach ($items as $key => $value) {
            $setIndex = $value['setIndex'];
            $quantity = $value['quantity'];
            if ($setIndex) {
                $sets[$setIndex]['elements'][] = $key;
                $sets[$setIndex]['quantity'] = $quantity;
            }else{
                $newItemList[] = $value;
            }
        }

        $setIndex = 1;
        if ($sets) {
            foreach ($sets as $set) {
                $quantity = $set['quantity'];
                while ($quantity >= 1) {
                    foreach ($set['elements'] as $element) {
                        $newItem = $items[$element];
                        $newItem['quantity'] = 1;
                        $newItem['setIndex'] = $setIndex;
                        $newItemList[] = $newItem;
                    }
                    $quantity--;
                    $setIndex++;
                }
            }
        }

        return $newItemList;
    }

    public function loadOrdersForLogist()
    {
        $source_id = Yii::$app->params['laitovo_source_id'];
        $command = $this->db->createCommand("select big2.*, sum(total) as payed, SUM(IF(bill1.uses is not null, total, 0)) as bonus from (select * from `ru_bill` where status=1) as bill1 right outer join (select big1.*, del.name2 from `ru_delivery_method` del right outer join (select ord.*, sum(price * `count`) as totalprice from `ru_orders_in` as inn inner join (select o2.*, IF(o2.pid = '1', l2.name, l2.name) as region2 from ru_location as l2 right outer join  (select o1.*,l.pid, IF(l.pid = '1', '', l.name)  as region1 from ru_location as l right outer join (select oo1.*,uu.name2 as otvetstven from (select o.id, o.type, o.text, o.text2, o.textproduction, u.email, u.inn, u.ipn as `clientIpn`, u.id as `laitovoUserId`, usel.ipn as `sellerIpn`,uship.ipn as `shipperIpn`, o.username as name, o.userphone as phone, o.region, o.delivery,o.`responsible`, o.costpayment, o.index, o.street, o.house, o.flat, o.address, o.kzt, o.kzt_d, o.factor, o.`createdate`, o.shipdate, o.payment_opt, o.payment, o.needTTH, o.export_username, o.export_type, o.remote_storage_from_id, o.ozon_id from `ru_orders` as o inner join `ru_user` as u on u.id=o.client inner join `ru_user` as usel on usel.id=o.seller inner join `ru_user` as uship on uship.id=o.shipper where o.`shipper`='100001' and o.status='1' and o.biz=1 and o.condition='gotovo_k_proizvodstvu' and o.remote_storage_id is null) as oo1 inner join `user` as uu on oo1.`responsible`=uu.id) as o1 on o1.region=l.id) as o2 on IF(o2.pid = '1', o2.region, o2.pid)=l2.id) as ord on inn.pid=ord.id group by id) big1 on big1.delivery=del.id) as big2 on big2.id=bill1.order group by id;");
        $rows = $command->queryAll();

        $saved = [];

        foreach ($rows as $key => $order) {

            if (!Order::findConsole(1)->where(['source_innumber' => $order['id'], 'source_id' => $source_id])->one()) {

                $erporder = new Order();
                $erporder->team_id = 1;
                $erporder->source_innumber = $order['id'];
                $erporder->source_id       = $source_id;
                $erporder->category        = $order['type'] ? 'Опт' : 'Физик';
                $erporder->username        = $order['name'];
                $erporder->userphone       = $order['phone'];
                $erporder->useremail       = $order['email'];
                $erporder->userinn         = $order['inn'];
                $erporder->comment         = $order['text'];
                $erporder->innercomment    = $order['textproduction'];
                $erporder->delivery        = $order['name2'];
                $erporder->manager         = $order['otvetstven'];
                $erporder->address         = mb_substr(implode(', ', [$order['region2'], $order['region1'], $order['address']]), 0, 254);
                $erporder->delivery_price  = $order['costpayment']??0;
                $erporder->is_need_ttn     = $order['needTTH']??false;
                $erporder->isCOD           = isset($order['payment']) && ($order['payment'] == 2 || $order['payment'] == 4) ? true : false; //Эта строчка отвечает - оплата наложенным платедом или нет
                $erporder->shipment_date   = !empty($order['shipdate']) ? strtotime($order['shipdate']) : null;
                //Сумма уплаченная за заказ с учетом бонусов
                $erporder->paid            = $order['payed'];
                //Сумма уплаченная бонусами
                $erporder->bonus           = $order['bonus'];
                //Для реализации
                $erporder->sender_id       = $order['shipperIpn'];
                $erporder->receiver_id     = $order['clientIpn'];
                $erporder->payer_id        = $order['clientIpn'];
                //Идентификатор удаленного склада
                $erporder->remoteStorage   = $order['remote_storage_from_id'] ?: 0;
                $erporder->laitovo_user_id = $order['laitovoUserId'] ?: 0;
                $erporder->ozon_posting_number = $order['ozon_id'];


                if (in_array($order['name2'], ['Германия', 'Индия', 'Гонконг', 'Сербия'])) {
                    $erporder->country   = explode(' ', $order['name2'])[0];
                    $erporder->category  = 'Экспорт';
                    $erporder->export_username = empty($order['export_username']) ? ($order['name'] == 'Hamburg' ? 'Склад' : null) : $order['export_username'];
                    $erporder->export_type     = empty($erporder->export_username) ? null : ($erporder->export_username == 'Склад' ? null : ($order['export_type'] ? 'Опт' : 'Физик'));
                }

                $command = $this->db->createCommand("select o.price, o.`count`, c.`barcode`, c.`article`, c.`newarticle`, c.name, o.setid, o.settext, o.without_labels, o.half_stake, CONCAT_WS(' ',o.info,o.text) as itemtext, o.index_group as setIndex from `ru_orders_in` as o inner join `ru_catalog` as c on c.id=o.goodid where o.pid=" . $order['id']);
                $items = $command->queryAll();

                $orderitems = [];
                foreach ($items as $key => $item) {

                    $window = mb_substr($item['barcode'], 0, 2, 'utf-8');
                    $window2 = mb_strtolower($window);
                    $window2 = str_replace('fw', 'ps', $window2);
                    $window2 = str_replace('fv', 'pf', $window2);
                    $window2 = str_replace('fd', 'pb', $window2);
                    $window2 = str_replace('rd', 'zb', $window2);
                    $window2 = str_replace('rv', 'zf', $window2);
                    $window2 = str_replace('bw', 'zs', $window2);
                    $cararticle = (int)mb_substr($item['barcode'], 2, 4, 'utf-8');
                    $typewindow = (int)mb_substr($item['barcode'], 6, 2, 'utf-8');
                    $color = (int)mb_substr($item['barcode'], 8, 1, 'utf-8');

                    $car = Cars::find()->where(['article' => $cararticle])->one();

                    if ($car) {
                        $auto = $car->name;
                        $lekalo = $car->json('nomerlekala');
                        $comment = $car->json('info');
                        $natyag = '';

                        if ($color == 5) {//chiko
                            switch ($typewindow) {
                                case '2':
                                    $type = 'smoke';
                                    break;
                                case '3':
                                    $type = 'mirror';
                                    break;
                                case '6':
                                    $type = 'short';
                                    break;

                                default:
                                    $type = 'standart';
                                    break;
                            }
                            $natyag = @$car->json('_' . mb_strtolower($window))['chiko'][$type]['natyag'];
                        }
                        $visota = @$car->json('visota' . mb_strtolower($window2));
                        $magnit = @$car->json('_' . mb_strtolower($window))['magnit'];
                        $hlyastik = @$car->json('_' . mb_strtolower($window))['hlyastik'];

                    } else {
                        $auto = '';
                        $lekalo = '';
                        $natyag = '';
                        $visota = '';
                        $magnit = '';
                        $hlyastik = '';
                        $comment = '';
                    }

                    $orderitems[] = [
                        'car' => $auto,
                        'lekalo' => $lekalo,
                        'natyag' => $natyag,
                        'visota' => $visota,
                        'magnit' => $magnit,
                        'hlyastik' => $hlyastik,

                        'name' => $item['name'],
                        'comment' => $comment,
                        'article' => $item['newarticle'],
                        'barcode' => $item['barcode'],
                        'quantity' => $item['count'],
                        'price'=>$item['price'],
                        'settext' => $item['settext'],
                        'itemtext' => $item['itemtext'],
                        'setIndex' => $item['setIndex'],

                        'halfStake' => $item['half_stake'],
                        'withoutLabels' => $item['without_labels'],
                    ];
                }
                $orderitems = $this->distributeInSets($orderitems);
                $orderitems = $this->toOneitem($orderitems);

                if ($this->saveOrderWithItems($erporder,$orderitems)) {
                    $saved[] = $erporder->id;
                }
            }
        }

        echo 'Загружено: ' . count($saved);
    }

    protected function saveOrderWithItems($erporder, $items)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $erporder->save();

            $position = 0;
            foreach ($items as $item) {
                $position++;
                $element = new OrderEntry();
                $element->order_barcode='B'.$erporder->source_innumber.'D'.$position;
                $element->order_id = @$erporder->id;
                $element->article = @$item['article'];
                $element->car = @$item['car'];
                $element->lekalo = @$item['lekalo'];
                $element->natyag = @$item['natyag'];
                $element->visota = @$item['visota'];
                $element->magnit = @$item['magnit'];
                $element->hlyastik = @$item['hlyastik'];
                $element->name = @$item['name'];
                $element->comment = @$item['comment'];
                $element->barcode = @$item['barcode'];
                $element->quantity = @$item['quantity'];
                $element->settext = @$item['settext'];
                $element->setIndex = @$item['setIndex'];
                $element->itemtext = @$item['itemtext'];
                $element->halfStake = @$item['halfStake'];
                $element->withoutLabels = @$item['withoutLabels'];
                $element->price = @$item['price'];
                $element->is_custom = !empty($element->itemtext) || !empty($erporder->innercomment) || $erporder->category  == 'Экспорт';
                $element->save();
            }
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollback();
            return false;
        }
    }

    public function postCars()
    {
        $car = Cars::find()->where('article>0 and checked=1 and (sync_at is null or sync_at<updated_at)')->orderBy('article DESC')->one();

        if ($car) {
            $car->sync_at = time();
            $car->save();

            //Шторка негабарит
            $gab = ( isset($car->json('_bw')['dlina']) && (int)$car->json('_bw')['dlina'] > 1220 ) ? 1 : 0;
            $gab = ( isset($car->json('_bw')['visota']) && (int)$car->json('_bw')['visota'] > 730 ) ? 1 : $gab;
            if ($gab) {
                $gab = ( isset($car->json('_bw')['gabarit']) && (int)$car->json('_bw')['gabarit'] == 1 ) ? 0 : $gab;
            }

            $gab_height = isset($car->json('_bw')['visota']) ? (int)$car->json('_bw')['visota'] : 0;
            $gab_width = isset($car->json('_bw')['dlina']) ? (int)$car->json('_bw')['dlina'] : 0;

            $client = new Client();
            $response = $client->createRequest()
                ->addHeaders(['Accept' => 'text/html'])
                ->setMethod('get')
                ->setUrl('https://laitovo.ru/api/order/car')
                ->setData([
                    'article' => $car->article,
                    'mark' => $car->mark,
                    'name' => $car->model,
                    'year1' => (int)$car->firstyear,
                    'year2' => (int)$car->lastyear,
                    'category' => strtr($car->category, ['A' => 1, 'B' => 2, 'C' => 3, 'D' => 4]),
                    'generation' => (string)$car->generation,
                    'carcass' => (string)$car->carcass,
                    'doors' => (string)$car->doors,
                    'ps' => (int)$car->json('fw'),
                    'pf' => (int)$car->json('fv'),
                    'pb' => (int)$car->json('fd'),
                    'zb' => (int)$car->json('rd'),
                    'zf' => (int)$car->json('rv'),
                    'zs' => (int)$car->json('bw'),
                    'modification' => (string)$car->modification,
                    'modification_en' => (string)$car->json('modification_en'),
                    'mark_en' => (string)$car->json('mark_en'),
                    'name_en' => (string)$car->json('model_en'),
                    'pk' => (int)$car->json('pk'),
                    'gab' => $gab,
                    'gab_height' => $gab_height,
                    'gab_width' => $gab_width,
                    'onTape' => ( (isset($car->json('_fd')['use_tape']) && (int)$car->json('_fd')['use_tape']) || (isset($car->json('_rd')['use_tape']) && (int)$car->json('_rd')['use_tape']) ) ? 1 : 0,
                    'easyCrep' => preg_match('/([cCсС])/', $car->json('typekrep')) === 1 ? 0 : 1,
                ])
                ->send();

            if ($response->isOk) {
                echo $car->id;
                print_r($response->data);

                $response = $client->createRequest()
                    ->addHeaders(['Accept' => 'text/html'])
                    ->setMethod('get')
                    ->setUrl('https://laitovo.eu/de_DE/api/order/carsync/'.$car->article)->send();

                print_r($response->data);

                $response = $client->createRequest()
                    ->addHeaders(['Accept' => 'text/html'])
                    ->setMethod('get')
                    ->setUrl('https://laitovo.eu/kz_KZ/api/order/carsync/'.$car->article)->send();

                print_r($response->data);

            }
        }
    }

    public function postOneCar($article)
    {
        $car = Cars::find()->where('article = ' . $article . ' and checked=1')->one();

        if ($car) {
            $car->sync_at = time();
            $car->save();

            //Шторка негабарит
            $gab = ( isset($car->json('_bw')['dlina']) && (int)$car->json('_bw')['dlina'] > 1220 ) ? 1 : 0;
            $gab = ( isset($car->json('_bw')['visota']) && (int)$car->json('_bw')['visota'] > 730 ) ? 1 : $gab;
            if ($gab) {
                $gab = ( isset($car->json('_bw')['gabarit']) && (int)$car->json('_bw')['gabarit'] == 1 ) ? 0 : $gab;
            }
            $gab_height = isset($car->json('_bw')['visota']) ? (int)$car->json('_bw')['visota'] : 0;
            $gab_width = isset($car->json('_bw')['dlina']) ? (int)$car->json('_bw')['dlina'] : 0;

            $client = new Client();
            $response = $client->createRequest()
                ->addHeaders(['Accept' => 'text/html'])
                ->setMethod('get')
                ->setUrl('https://laitovo.ru/api/order/car')
                ->setData([
                    'article' => $car->article,
                    'mark' => $car->mark,
                    'name' => $car->model,
                    'year1' => (int)$car->firstyear,
                    'year2' => (int)$car->lastyear,
                    'category' => strtr($car->category, ['A' => 1, 'B' => 2, 'C' => 3]),
                    'generation' => (string)$car->generation,
                    'carcass' => (string)$car->carcass,
                    'doors' => (string)$car->doors,
                    'ps' => (int)$car->json('fw'),
                    'pf' => (int)$car->json('fv'),
                    'pb' => (int)$car->json('fd'),
                    'zb' => (int)$car->json('rd'),
                    'zf' => (int)$car->json('rv'),
                    'zs' => (int)$car->json('bw'),
                    'modification' => (string)$car->modification,
                    'modification_en' => (string)$car->json('modification_en'),
                    'mark_en' => (string)$car->json('mark_en'),
                    'name_en' => (string)$car->json('model_en'),
                    'pk' => (int)$car->json('pk'),
                    'gab' => $gab,
                    'gab_height' => $gab_height,
                    'gab_width' => $gab_width,
                    'onTape' => ( (isset($car->json('_fd')['use_tape']) && (int)$car->json('_fd')['use_tape']) || (isset($car->json('_rd')['use_tape']) && (int)$car->json('_rd')['use_tape']) ) ? 1 : 0,
                    'easyCrep' => preg_match('/([cCсС])/', $car->json('typekrep')) === 1 ? 0 : 1,
                ])
                ->send();

            if ($response->isOk) {
                echo $car->id;
            }
        }
    }


    public function loadOrdersClientRu()
    {
        $order_ru = ErpOrdersUserReport::find()->where(['site'=>ErpOrdersUserReport::LAITOVO_RU])->orderBy('id DESC')->one();

        $order_id_ru = $order_ru && $order_ru->order_id ? $order_ru->order_id : 0;

        $command_ru = $this->db->createCommand("
                SELECT u.id AS client_id, u.name as client_name, u.inn as client_inn, u.role as role, u.region_name as region_name, 
                o.id AS order_id, UNIX_TIMESTAMP(o.createdate) as created_at, oi.sum_order as sum_order,
                oi.pricenonds as pricenonds
                FROM ru_orders AS o
                LEFT JOIN 
                    (SELECT us.id AS id, us.name AS name , us.inn AS inn, us.role AS role, l.name as region_name 
                        FROM ru_user AS us
                        LEFT JOIN ru_location AS l ON l.id = us.region
                    ) AS u ON u.id = o.client
                LEFT JOIN
                    (SELECT oin.pid, TRUNCATE(SUM(oin.priceclient*count),0) AS sum_order, TRUNCATE(SUM(c.pricenonds),0) as pricenonds 
                         FROM ru_orders_in as oin
                         LEFT JOIN ru_catalog AS c ON c.id = oin.goodid
                         GROUP BY oin.pid
                     ) AS oi ON oi.pid = o.id
                WHERE
                    o.condition <> 'canceled' 
                AND 
                    o.condition <> ''
                AND
                    o.id>:order_id_ru 
                ORDER BY o.id ASC
                LIMIT 10000
            ")
            ->bindValue(':order_id_ru', $order_id_ru);
        $rows_ru = $command_ru->queryAll();

        foreach ($rows_ru as $row){
            $this->saveOrdersUserReport($row, ErpOrdersUserReport::LAITOVO_RU);
        }
    }


    public function loadOrdersClientDe()
    {
        $order_de = ErpOrdersUserReport::find()->where(['site'=>ErpOrdersUserReport::LAITOVO_DE])->orderBy('id DESC')->one();

        $order_id_de = $order_de && $order_de->order_id ? $order_de->order_id : 0;

        $command_de = $this->db->createCommand("
                SELECT u.id AS client_id, u.name as client_name, u.inn as client_inn, u.role as role, u.region_name as region_name, 
                o.id AS order_id, UNIX_TIMESTAMP(o.createdate) as created_at, oi.sum_order as sum_order,
                oi.pricenonds as pricenonds
                FROM de_orders AS o
                LEFT JOIN 
                    (SELECT us.id AS id, us.name AS name , us.inn AS inn, us.role AS role, l.name as region_name 
                        FROM de_user AS us
                        LEFT JOIN ru_location AS l ON l.id = us.region
                    ) AS u ON u.id = o.client
                LEFT JOIN
                    (SELECT oin.pid, TRUNCATE(SUM(oin.priceclient*count),0) AS sum_order, TRUNCATE(SUM(c.pricenonds),0) as pricenonds 
                         FROM de_orders_in as oin
                         LEFT JOIN de_catalog AS c ON c.id = oin.goodid
                         GROUP BY oin.pid
                     ) AS oi ON oi.pid = o.id
                WHERE
                    o.condition <> 'canceled' 
                AND 
                    o.condition <> ''
                AND
                    o.id>:order_id_de 
                ORDER BY o.id ASC
                LIMIT 2000
            ")
            ->bindValue(':order_id_de', $order_id_de);
        $rows_de = $command_de->queryAll();

        foreach ($rows_de as $row){
            $this->saveOrdersUserReport($row, ErpOrdersUserReport::LAITOVO_DE);
        }
    }


    public function saveOrdersUserReport($row,$site)
    {
        $orders_user = new ErpOrdersUserReport();

        $orders_user->client_id = isset($row['client_id']) ? $row['client_id'] : null;
        $orders_user->client_name = isset($row['client_name']) ? $row['client_name'] : null;
        $orders_user->client_inn = isset($row['client_inn']) ? $row['client_inn'] : null;
        $orders_user->order_id = isset($row['order_id']) ? $row['order_id'] : null;
        $orders_user->role = isset($row['role']) ? $row['role'] : null;
        $orders_user->region_name = isset($row['region_name']) ? $row['region_name'] : null;
        $orders_user->sum_order = isset($row['sum_order']) ? $row['sum_order'] : null;
        $orders_user->created_at = isset($row['created_at']) ? $row['created_at'] : null;
        $orders_user->site = $site;
        $orders_user->save();
    }

    public function loadLogistStatist()
    {
        $orderIds = ArticleStatistics::find()->where('orderId is not null')->select(['orderId'])->column();
        $orderIds = array_merge([1],$orderIds);
        $command_ru = $this->db->createCommand("
            SELECT 
                UNIX_TIMESTAMP(ru_orders.createdate) as created_at,
                ru_catalog.newarticle as article,
                ru_orders.id as orderId
            FROM ru_orders
            LEFT JOIN ru_orders_in ON ru_orders.id = ru_orders_in.pid
            LEFT JOIN ru_user    ON ru_user.id = ru_orders.client
            LEFT JOIN ru_catalog ON ru_orders_in.goodid = ru_catalog.id

            WHERE
                ru_orders.condition = 'zakryit'
            AND 
                ru_orders.id not in (" . implode(',', $orderIds) . ")  
            AND 
                UNIX_TIMESTAMP(ru_orders.createdate) < " . time() . "
            AND
                UNIX_TIMESTAMP(ru_orders.createdate) > " . strtotime("2016-03-15 00:00:00") ."
            AND
                ru_user.use_statistics = 0
            ORDER BY created_at ASC
        ");

        $rows_ru = $command_ru->queryAll();

        $rowCount = 0;
        $allCount = count($rows_ru);
        foreach ($rows_ru as $row) {
            $rowCount++;
            echo "Всего артикулов: {$allCount}"  . PHP_EOL;
            echo "Текущая позиция: {$rowCount}"  . PHP_EOL;
            echo "Осталось для обработки: " . ($allCount - $rowCount)  . PHP_EOL;
            echo "__________________________" . PHP_EOL;
            $statistics             = new ArticleStatistics();
            $statistics->article    = isset($row['article']) ? $row['article'] : null; 
            $statistics->created_at = isset($row['created_at']) ? $row['created_at'] : null; 
            $statistics->type       = ArticleStatistics::TYPE_LAITOVO; 
            $statistics->orderId    = isset($row['orderId']) ? $row['orderId'] : null;
            $statistics->save();
        }
    }

    public function updateLeftovers()
    {
        $deleteDate = date('Y-m-d H:i:s', time());

        $scriptSt = microtime(true);

        $sql = "
            SELECT b.article as article, count(a.id) as num  FROM logistics_storage_state as a
            LEFT JOIN  bizzone.logistics_naryad as b ON a.upn_id = b.id
            LEFT JOIN  bizzone.logistics_storage as c ON a.storage_id = c.id
            where b.reserved_id is null
            GROUP BY b.article;
        ";
        $rows = Yii::$app->db->createCommand($sql)->queryAll();

        if (count($rows)) {
            $table = "`num_by_code`";
            $code  = "`code`";
            $num  = "`num`";
            $date  = "`last`";
            foreach ($rows as $row) {
                $startd = microtime(true);
                echo "Обрабатываю артикул : " . $row['article'] . PHP_EOL;
                $articleObj = new Article($row['article']);
                $positions = $articleObj->articleWithAnalogs;
                echo "Позиции с аналогами : ". implode(',',$positions) . PHP_EOL;
                $sql2 = "
                    SELECT count(a.id) as num  FROM logistics_storage_state as a
                    LEFT JOIN  bizzone.logistics_naryad as b ON a.upn_id = b.id
                    LEFT JOIN  bizzone.logistics_storage as c ON a.storage_id = c.id
                    where b.reserved_id is null and c.type <> 3 and c.type <> 4 and b.article in (\"" . implode('","',$positions) . "\")
                    GROUP BY b.article;
                ";
                $count = Yii::$app->db->createCommand($sql2)->queryScalar();
                $count = $count ?: 0;
                echo "Всего на складе". $count . PHP_EOL;
                foreach ($positions as $pos) {
                    $value1 = $pos;
                    $value2 = $count;
                    $value3 = date('Y-m-d H:i:s', time());
                    $sql = "INSERT INTO ". $table . "
                    (". $code .",". $num .",". $date .")
                    VALUES
                    ('". $value1 ."',". $value2 .", '". $value3 ."')
                    ON DUPLICATE KEY UPDATE
                    `num` = ". $value2 . ", `last` = '". $value3 . "'";

                    $this->db->createCommand($sql)->execute();
                    $endd = microtime(true);
                    $continies = $endd - $startd;
                    echo "Обновлены остатки для : ". $pos . " за время " . $continies . PHP_EOL;
                }
            }
        }else{
            echo "Нулевые остатки" . PHP_EOL;
        }

        $this->db
            ->createCommand()
            ->delete('num_by_code',['<','last',$deleteDate])
            ->execute();

        $scriptEnd = microtime(true);
        $scriptContinieu = ($scriptEnd - $scriptSt) / (60*60) ;
        echo "Все остатки выгружены. Время : " . $scriptContinieu . PHP_EOL;
    }


    private function _getListWithAnalogsArticles($article)
    {
        $articles = [];
        //3. Разбиваем артикул на составляющие с цеот
        $value = explode('-', $article);

        //Артикул, для которого мы ищмем аналоги, по умолчанию попадает в массив возращаемых значений
        $articles[] = $article;

        //Если артикул начинаеться с "OT" то мы не ищем по нему аналоги и возвращается массив со значем только текущего артикула
        //В противном случае мы приступаем к поиску аналогов.
        if ($value[0] != 'OT') {

            $valueSearch = $value[0];
            $valueSearch = str_replace('FW', 'ПШ', $valueSearch);
            $valueSearch = str_replace('FV', 'ПФ', $valueSearch);
            $valueSearch = str_replace('FD', 'ПБ', $valueSearch);
            $valueSearch = str_replace('RD', 'ЗБ', $valueSearch);
            $valueSearch = str_replace('RV', 'ЗФ', $valueSearch);
            $valueSearch = str_replace('BW', 'ЗШ', $valueSearch);

            //Ищем автомобиль
            $car =  Cars::find()->where(['article' => @$value[2]])->one();
            //Находим аналоги по оконному проему

            $analogsIds = $this->_searchAnalogs($car->id,$valueSearch);
            if ( ($deleteKey = array_search($car->id,$analogsIds)) !== false ) {
                unset($analogsIds[$deleteKey]);
            }

            $analogs = Cars::find()->where(['in','id',$analogsIds])->all();

            //если аналоги существуют
            if ($analogs) {

                //обходим список аналогов
                foreach ($analogs as $analog) {

                    //заменяем данные из текущего артикула на необходимые данные аналоги, а именно
                    //заменяем значение артикула автомобиля, зашитое в артикуле
                    $value[2] = $analog->article;
                    //заменяем значение первой буквы марки автомобиля, защитое в артикуле, по правилам генерации артикула с Laitovo.ru
                    //Использую вспомогательную функцию getTranslit() для перевода русских марок в нужное написание
                    $value[1] = $analog->getTranslit(mb_substr($analog->mark,0,1));
                    //Получаем новый артикул путем соединения всех данных воедино пи помощи функция склеивания осколков
                    $newarticle = implode('-',$value);
                    //Добавляем новый артикул в массив результирующих артикулов
                    $articles[] = $newarticle;
                }
            }
        }

        return $articles;
    }

    private function _searchAnalogs ($car_id,$type,$ids = []) {
        $ids2 = [];
        $ids2[] = $car_id;
        $analogs = CarsAnalog::find()->where(['car_id' => $car_id])->all();
        foreach ($analogs as $analog) {
            $json=Json::decode($analog->json ? $analog->json : '{}');
            $types = isset($json['elements']) ? $json['elements'] : [];
            if (!in_array($analog->analog_id, $ids2) && in_array($type,$types)) {
                $ids2[] = $analog->analog_id;
            }
        }

        //Находим расхождения в массивах
        $diff = array_diff($ids2, $ids);

        if (count($diff)) {
            foreach ($diff as $key => $value) {
                $values = $this->_searchAnalogs($value,$type,array_merge($diff, $ids));
                if ($values) {
                    foreach ($values as $value) {
                        if (!in_array($value, $diff)) {
                            $diff[] = $value;
                        }
                    }
                }

            }
        }

        //возвращаем либо массив либо ложь
        return empty($diff) ? false : $diff;
    }

    /**
     * @throws \yii\db\Exception
     */
    public function updateBonusOrders()
    {
        $orders = Order::find()->all();
        $count = count($orders);
        echo 'Всего: ' . $count . PHP_EOL;

        foreach ($orders as $order) {
            echo 'Позиция: ' . $count-- . ' Заказ: ' . $order->source_innumber .  PHP_EOL;
            $command = $this->db->createCommand("select sum(total) from `ru_bill` where status=1 and `order` = :orderId and uses > 0 and uses is not null group by `order`")
            ->bindValue(':orderId',$order->source_innumber);
            $total = $command->queryScalar();
            $total = (double)$total;
            echo 'Бонусы : ' . $total;
            if ($total) {
                $order->bonus = $total;
                if ($order->save())
                 echo 'Запись успешно обновлена:' . PHP_EOL;
            }
        }

        echo 'Все ок!';
    }
}
