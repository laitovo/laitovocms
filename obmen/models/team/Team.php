<?php

namespace obmen\models\team;

use Yii;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Team extends \common\models\team\Team
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max'=>255],
            [['json'], 'string'],
            [['discount'], 'number','min'=>0,'max'=>100],
            [['updated_at'], 'number'],
            [['status'], 'boolean'],
        ];
    }

    public function beforeValidate()
    {
        if ($this['updated_at']>$this->getOldAttribute('updated_at') || $this->isNewRecord){
            return true;
        } else {
            return false;
        }
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            $j = Json::decode($this->json);
            unset($j['Invite']);
            unset($j['fields']);
            return $j;
        };

        $fields['created_at']=function () {
            return $this->created_at ? Yii::$app->formatter->asDatetime($this->created_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        $fields['updated_at']=function () {
            return $this->updated_at ? Yii::$app->formatter->asDatetime($this->updated_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        return $fields;
    }

    public function extraFields()
    {
        return ['organizations'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizations()
    {
        return $this->hasMany(Organization::className(), ['team_id' => 'id']);
    }

}
