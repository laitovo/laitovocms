<?php

namespace obmen\models\laitovo;

use Yii;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class CarsAnalog extends \common\models\laitovo\CarsAnalog
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['car_id', 'analog_id'], 'required'],
            [['car_id', 'analog_id'], 'integer'],
            [['json'], 'string'],
            [['analog_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['analog_id' => 'id']],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['car_id', 'analog_id'], 'unique', 'targetAttribute' => ['car_id', 'analog_id']],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            return Json::decode($this->json);
        };

        $fields['car_article']=function () {
            return $this->car->article;
        };

        $fields['analog_article']=function () {
            return $this->analog->article;
        };

        return $fields;
    }
}
