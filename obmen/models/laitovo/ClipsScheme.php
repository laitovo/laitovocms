<?php

namespace obmen\models\laitovo;

use Yii;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class ClipsScheme extends \common\models\laitovo\ClipsScheme
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['brand', 'type', 'type_clips', 'window'], 'string', 'max' => 255],
            [['car_id', 'brand', 'type', 'type_clips', 'window'], 'unique', 'targetAttribute' => ['car_id', 'brand', 'type', 'type_clips', 'window']],

        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            return Json::decode($this->json);
        };

        $fields['created_at']=function () {
            return $this->created_at ? Yii::$app->formatter->asDatetime($this->created_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        $fields['updated_at']=function () {
            return $this->updated_at ? Yii::$app->formatter->asDatetime($this->updated_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        return $fields;
    }
}
