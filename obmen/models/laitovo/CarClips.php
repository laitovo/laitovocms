<?php

namespace obmen\models\laitovo;

use Yii;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class CarClips extends \common\models\laitovo\CarClips
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'string', 'max' => 255],
            [['car_id', 'name', 'type'], 'unique', 'targetAttribute' => ['car_id', 'name', 'type']],

        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            return Json::decode($this->json);
        };

        $fields['created_at']=function () {
            return $this->created_at ? Yii::$app->formatter->asDatetime($this->created_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        $fields['updated_at']=function () {
            return $this->updated_at ? Yii::$app->formatter->asDatetime($this->updated_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        return $fields;
    }
}
