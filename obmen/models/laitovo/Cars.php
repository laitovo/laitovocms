<?php

namespace obmen\models\laitovo;

use Yii;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Cars
 * @package obmen\models\laitovo
 *
 * @property $article integer
 * @property $mark string
 */

class Cars extends \common\models\laitovo\Cars
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['mounting', 'json'], 'string'],
            [['name', 'mark', 'model', 'carcass', 'modification', 'country', 'category'], 'string', 'max' => 255],
            [['article', 'generation','doors','firstyear','lastyear'], 'number'],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            return Json::decode($this->json);
        };

        $fields['created_at']=function () {
            return $this->created_at ? Yii::$app->formatter->asDatetime($this->created_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        $fields['updated_at']=function () {
            return $this->updated_at ? Yii::$app->formatter->asDatetime($this->updated_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        return $fields;
    }

    public function extraFields()
    {
        return ['clips','schemes','analogs'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchemes()
    {
        return $this->hasMany(ClipsScheme::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnalogs()
    {
        return $this->hasMany(CarsAnalog::className(), ['car_id' => 'id']);
    }

    public function getLekalo() {
        return $this->json('nomerlekala') ? (int)$this->json('nomerlekala') : null;
    }

    public function getFW() {
        return $this->json('fw');
    }

    public function getFV() {
        return $this->json('fv');
    }

    public function getFD() {
        return $this->json('fd');
    }

    public function getRD() {
        return $this->json('rd');
    }

    public function getRV() {
        return $this->json('rv');
    }

    public function getBW() {
        return $this->json('bw');
    }

}
