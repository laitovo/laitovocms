<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace obmen\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use obmen\models\laitovo\Cars;
use obmen\models\laitovo\CarClips;
use obmen\models\laitovo\ClipsScheme;
use obmen\models\laitovo\CarsAnalog;

/**
 * Обмен с лайтово
 *
 * @author basyan <basyan@yandex.ru>
 */
class ObmenLaitovo extends Model
{

    private $data=null;
    private $team=null;

    public function __construct($data,$config = [])
    {
        $this->data=$data;
        if (($this->team=\common\models\team\Team::findOne(1))===null)
            throw new NotFoundHttpException();

        parent::__construct($config);
    }

    /**
     * Загрузка авто
     * @return type
     */
    public function importcars()
    {
        function boo($value='')
        {
            if ($value=='false')
                $value=0;
            return (boolean)$value;
        }

        function tostr($value='')
        {
            if ($value=='0')
                $value='';
            if ($value=='ДА')
                $value='Да';
            if ($value=='НЕТ')
                $value='Нет';

            if ((boolean)$value)
                return (string)$value;
            else
                return '';
        }

        function clip($value='')
        {
            if ($value=='00000000-0000-0000-0000-000000000000')
                $value='';
            return $value;
        }

        $kuzov=[];
        if (isset($this->data['V8ExchData']['CatalogObject.Автошторки_Кузовы']['Ref'])){
            $value=$this->data['V8ExchData']['CatalogObject.Автошторки_Кузовы'];
            $kuzov[(string)$value['Ref']]=(string)$value['Description'];
        } else {
            foreach ($this->data['V8ExchData']['CatalogObject.Автошторки_Кузовы'] as $key => $value) {
                $kuzov[(string)$value->Ref]=(string)$value->Description;
            }
        }

        $categor=[];
        if (isset($this->data['V8ExchData']['CatalogObject.Автошторки_КлассыПродукции']['Ref'])){
            $value=$this->data['V8ExchData']['CatalogObject.Автошторки_КлассыПродукции'];
            $categor[(string)$value['Ref']]=(string)$value['Description'];
        } else {
            foreach ($this->data['V8ExchData']['CatalogObject.Автошторки_КлассыПродукции'] as $key => $value) {
                $categor[(string)$value->Ref]=(string)$value->Description;
            }
        }

        $obshiv=[];
        if (isset($this->data['V8ExchData']['CatalogObject.ТипОбшивки']['Ref'])){
            $value=$this->data['V8ExchData']['CatalogObject.ТипОбшивки'];
            $obshiv[(string)$value['Ref']]=(string)$value['Description'];
        } else {
            foreach ($this->data['V8ExchData']['CatalogObject.ТипОбшивки'] as $key => $value) {
                $obshiv[(string)$value->Ref]=(string)$value->Description;
            }
        }

        $kreplen=[];
        if (isset($this->data['V8ExchData']['CatalogObject.Автошторки_Крепления']['Ref'])){
            $value=$this->data['V8ExchData']['CatalogObject.Автошторки_Крепления'];
            $kreplen[(string)$value['Owner']][(string)$value['Ref']]['name']=(string)$value['Description'];
            $kr=(string)$value['ТипКрепления'];
            $kr=str_replace('А', 'A', $kr);
            $kr=str_replace('В', 'B', $kr);
            $kr=str_replace('С', 'C', $kr);
            $kr=str_replace('М', 'M', $kr);
            $kr=str_replace('Р', 'P', $kr);
            $kreplen[(string)$value['Owner']][(string)$value['Ref']]['type']=$kr;
        } else {
            foreach ($this->data['V8ExchData']['CatalogObject.Автошторки_Крепления'] as $key => $value) {
                $kreplen[(string)$value->Owner][(string)$value->Ref]['name']=(string)$value->Description;
                $kr=(string)$value->ТипКрепления;
                $kr=str_replace('А', 'A', $kr);
                $kr=str_replace('В', 'B', $kr);
                $kr=str_replace('С', 'C', $kr);
                $kr=str_replace('М', 'M', $kr);
                $kr=str_replace('Р', 'P', $kr);
                $kreplen[(string)$value->Owner][(string)$value->Ref]['type']=$kr;
            }
        }

        $sheme=[];
        if (isset($this->data['V8ExchData']['CatalogObject.АвтоШторки_СхемыКрепления']['Ref'])){
            $value=$this->data['V8ExchData']['CatalogObject.АвтоШторки_СхемыКрепления'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['name']=(string)$value['Description'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['brand']=str_replace('Chico','Chiko',(string)$value['МаркаПродукции']);
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['type']=(string)$value['ВидПродукции'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['type_clips']=(string)$value['ТипЗажима'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k1']=(string)$value['К1'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k2']=(string)$value['К2'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k3']=(string)$value['К3'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k4']=(string)$value['К4'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k5']=(string)$value['К5'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k6']=(string)$value['К6'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k7']=(string)$value['К7'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k8']=(string)$value['К8'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k9']=(string)$value['К9'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k10']=(string)$value['К10'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k11']=(string)$value['К11'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k12']=(string)$value['К12'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k13']=(string)$value['К13'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k14']=(string)$value['К14'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k15']=(string)$value['К15'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k16']=(string)$value['К16'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k17']=(string)$value['К17'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k18']=(string)$value['К18'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k19']=(string)$value['К19'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k20']=(string)$value['К20'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k21']=(string)$value['К21'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k22']=(string)$value['К22'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k23']=(string)$value['К23'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k24']=(string)$value['К24'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k25']=(string)$value['К25'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k26']=(string)$value['К26'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k27']=(string)$value['К27'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k28']=(string)$value['К28'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k29']=(string)$value['К29'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k30']=(string)$value['К30'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k31']=(string)$value['К31'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k32']=(string)$value['К32'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k33']=(string)$value['К33'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k34']=(string)$value['К34'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k35']=(string)$value['К35'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k36']=(string)$value['К36'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k37']=(string)$value['К37'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k38']=(string)$value['К38'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k39']=(string)$value['К39'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k40']=(string)$value['К40'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k41']=(string)$value['К41'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k42']=(string)$value['К42'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k43']=(string)$value['К43'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k44']=(string)$value['К44'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k45']=(string)$value['К45'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k46']=(string)$value['К46'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k47']=(string)$value['К47'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k48']=(string)$value['К48'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k49']=(string)$value['К49'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k50']=(string)$value['К50'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k51']=(string)$value['К51'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k52']=(string)$value['К52'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k53']=(string)$value['К53'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k54']=(string)$value['К54'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k55']=(string)$value['К55'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k56']=(string)$value['К56'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k57']=(string)$value['К57'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k58']=(string)$value['К58'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k59']=(string)$value['К59'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k60']=(string)$value['К60'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k61']=(string)$value['К61'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k62']=(string)$value['К62'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k63']=(string)$value['К63'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k64']=(string)$value['К64'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k65']=(string)$value['К65'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k66']=(string)$value['К66'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k67']=(string)$value['К67'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k68']=(string)$value['К68'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k69']=(string)$value['К69'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k70']=(string)$value['К70'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k71']=(string)$value['К71'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k72']=(string)$value['К72'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k73']=(string)$value['К73'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k74']=(string)$value['К74'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k75']=(string)$value['К75'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k76']=(string)$value['К76'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k77']=(string)$value['К77'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k78']=(string)$value['К78'];
            $sheme[(string)$value['Owner']][(string)$value['Ref']]['k79']=(string)$value['К79'];
        } else {
            foreach ($this->data['V8ExchData']['CatalogObject.АвтоШторки_СхемыКрепления'] as $key => $value) {
                $sheme[(string)$value->Owner][(string)$value->Ref]['name']=(string)$value->Description;
                $sheme[(string)$value->Owner][(string)$value->Ref]['brand']=str_replace('Chico', 'Chiko', (string)$value->МаркаПродукции);
                $sheme[(string)$value->Owner][(string)$value->Ref]['type']=(string)$value->ВидПродукции;
                $sheme[(string)$value->Owner][(string)$value->Ref]['type_clips']=(string)$value->ТипЗажима;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k1']=(string)$value->К1;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k2']=(string)$value->К2;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k3']=(string)$value->К3;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k4']=(string)$value->К4;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k5']=(string)$value->К5;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k6']=(string)$value->К6;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k7']=(string)$value->К7;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k8']=(string)$value->К8;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k9']=(string)$value->К9;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k10']=(string)$value->К10;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k11']=(string)$value->К11;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k12']=(string)$value->К12;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k13']=(string)$value->К13;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k14']=(string)$value->К14;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k15']=(string)$value->К15;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k16']=(string)$value->К16;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k17']=(string)$value->К17;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k18']=(string)$value->К18;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k19']=(string)$value->К19;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k20']=(string)$value->К20;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k21']=(string)$value->К21;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k22']=(string)$value->К22;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k23']=(string)$value->К23;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k24']=(string)$value->К24;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k25']=(string)$value->К25;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k26']=(string)$value->К26;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k27']=(string)$value->К27;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k28']=(string)$value->К28;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k29']=(string)$value->К29;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k30']=(string)$value->К30;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k31']=(string)$value->К31;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k32']=(string)$value->К32;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k33']=(string)$value->К33;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k34']=(string)$value->К34;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k35']=(string)$value->К35;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k36']=(string)$value->К36;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k37']=(string)$value->К37;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k38']=(string)$value->К38;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k39']=(string)$value->К39;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k40']=(string)$value->К40;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k41']=(string)$value->К41;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k42']=(string)$value->К42;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k43']=(string)$value->К43;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k44']=(string)$value->К44;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k45']=(string)$value->К45;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k46']=(string)$value->К46;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k47']=(string)$value->К47;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k48']=(string)$value->К48;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k49']=(string)$value->К49;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k50']=(string)$value->К50;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k51']=(string)$value->К51;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k52']=(string)$value->К52;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k53']=(string)$value->К53;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k54']=(string)$value->К54;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k55']=(string)$value->К55;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k56']=(string)$value->К56;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k57']=(string)$value->К57;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k58']=(string)$value->К58;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k59']=(string)$value->К59;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k60']=(string)$value->К60;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k61']=(string)$value->К61;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k62']=(string)$value->К62;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k63']=(string)$value->К63;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k64']=(string)$value->К64;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k65']=(string)$value->К65;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k66']=(string)$value->К66;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k67']=(string)$value->К67;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k68']=(string)$value->К68;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k69']=(string)$value->К69;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k70']=(string)$value->К70;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k71']=(string)$value->К71;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k72']=(string)$value->К72;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k73']=(string)$value->К73;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k74']=(string)$value->К74;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k75']=(string)$value->К75;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k76']=(string)$value->К76;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k77']=(string)$value->К77;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k78']=(string)$value->К78;
                $sheme[(string)$value->Owner][(string)$value->Ref]['k79']=(string)$value->К79;
            }
        }

        foreach ($this->data['V8ExchData'] as $key => $value) {
            var_dump($key);
        }

        if (isset($this->data['V8ExchData']['CatalogObject.АвтоШторки_Автомобили']['Ref'])){
            $value=$this->data['V8ExchData']['CatalogObject.АвтоШторки_Автомобили'];

            if (!boo($value['НеСинхронизировать'])){

                $load['Cars']=[
                    'Ref'=>tostr($value['Ref']),
                    'article'=>(int)tostr($value['Артикул']),
                    'name'=>tostr($value['Description']),
                    'mark'=>tostr($value['Марка']),
                    'model'=>tostr($value['Модель']),
                    'generation'=>tostr($value['Поколение'])?:'',
                    'carcass'=>@$kuzov[tostr($value['Кузов'])],
                    'doors'=>tostr($value['КоличествоДверей']),
                    'firstyear'=>tostr($value['ГодНачалаВыпуска']),
                    'lastyear'=>tostr($value['ГодОкончанияВыпуска'])!='9999' ? tostr($value['ГодОкончанияВыпуска']) : '',
                    'modification'=>tostr($value['Модификация']),
                    'country'=>tostr($value['СтранаПроизводитель']),
                    'category'=>@$categor[tostr($value['Категория'])],
                    'json'=>[
                        'modelde'=>tostr($value['Модель2']),
                        'info'=>tostr($value['ОсобУст']),
                        'visotapb'=>tostr($value['ВысотаПБ']),
                        'dlinapb'=>tostr($value['ДлинаПБ']),
                        'dlinapb1'=>tostr($value['ДлинаПБ1']),
                        'visotazb'=>tostr($value['ВысотаЗБ']),
                        'dlinazb'=>tostr($value['ДлинаЗБ']),
                        'dlinazb1'=>tostr($value['ДлинаЗБ1']),
                        'visotazf'=>tostr($value['ВысотаЗФ']),
                        'dlinazf'=>tostr($value['ДлинаЗФ']),
                        'dlinazf1'=>tostr($value['ДлинаЗФ1']),
                        'visotazs'=>tostr($value['ВысотаЗШ']),
                        'dlinazs'=>tostr($value['ДлинаЗШ']),
                        'dlinazs1'=>tostr($value['ДлинаЗШ1']),
                        'nomerlekala'=>tostr($value['НомерЛекала']),
                        'typekrep'=>tostr($value['ТипКрепления']),
                        'fw'=>(int)boo($value['ПШ']),
                        'fv'=>(int)boo($value['ПФ']),
                        'fd'=>(int)boo($value['ПБ']),
                        'rd'=>(int)boo($value['ЗБ']),
                        'rv'=>(int)boo($value['ЗФ']),
                        'bw'=>(int)boo($value['ЗШ']),
                        'pk'=>(int)boo($value['ПК']),
                        '_fw'=>[
                            'status'=>(int)boo($value['ПШ_В']),
                            'date'=>strtotime(tostr($value['ПШ_Дата']))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value['ПШ_Дата'])),'dd.MM.yyyy') : '',
                            'scheme'=>[
                                tostr($value['Р1']),
                                tostr($value['Р2']),
                                tostr($value['Р3']),
                                tostr($value['Р4']),
                                tostr($value['Р5']),
                                tostr($value['Р6']),
                                tostr($value['Р7']),
                                tostr($value['Р8']),
                                tostr($value['Р9']),
                            ],
                        ],
                        '_fv'=>[
                            'date'=>strtotime(tostr($value['ПФ_Дата']))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value['ПФ_Дата'])),'dd.MM.yyyy') : '',
                            'dlina'=>tostr($value['ПФ_Д']),
                            'visota'=>tostr($value['ПФ_Ш']),
                            'magnit'=>tostr($value['ПФ_ЕМД']),
                            'laitovo'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ПФ_ЛС']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['ПФ_ЧЛБ']),
                                ],
                            ],
                            'chiko'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ПФ_ЧЛС']),
                                    'natyag'=>tostr($value['ПФ_натЧС']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['ПФ_ЧБ']),
                                ],
                            ],
                        ],
                        '_fd'=>[
                            'date'=>strtotime(tostr($value['ПБ_Дата']))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value['ПБ_Дата'])),'dd.MM.yyyy') : '',
                            'dlina'=>tostr($value['ПБ_Д']),
                            'visota'=>tostr($value['ПБ_Ш']),
                            'magnit'=>tostr($value['ПБ_ЕМД']),
                            'obshivka'=>@$obshiv[tostr($value['ПБ_Обш'])],
                            'hlyastik'=>tostr($value['ПБ_ХЛ']),
                            'laitovo'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ПБ_ЛС']),
                                ],
                                'short'=>[
                                    'status'=>(int)boo($value['ПБ_ЛУ']),
                                ],
                                'smoke'=>[
                                    'status'=>(int)boo($value['ПБ_ЛВК']),
                                ],
                                'mirror'=>[
                                    'status'=>(int)boo($value['ПБ_ЛВБЗ']),
                                    'maxdlina'=>tostr($value['ПБ_ЛВЗД']),
                                    'maxvisota'=>tostr($value['ПБ_ЛВЗВ']),
                                ],
                                'dontlooks'=>[
                                    'status'=>(int)boo($value['ПБ_ЧДЛС']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['ПБ_ЧДЛБ']),
                                ],
                            ],
                            'chiko'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ПБ_ЧС']),
                                    'natyag'=>tostr($value['ПБ_НатЧС']),
                                ],
                                'short'=>[
                                    'status'=>(int)boo($value['ПБ_ЧУ']),
                                    'natyag'=>tostr($value['ПБ_НатЧУ']),
                                ],
                                'smoke'=>[
                                    'status'=>(int)boo($value['ПБ_ЧВК']),
                                    'natyag'=>tostr($value['ПБ_НатЧВК']),
                                ],
                                'mirror'=>[
                                    'status'=>(int)boo($value['ПБ_ЧВБЗ']),
                                    'natyag'=>tostr($value['ПБ_НатЧВЗ']),
                                ],
                                'dontlooks'=>[
                                    'status'=>(int)boo($value['ПБ_ЧДЧС']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['ПБ_ЧДЧБ']),
                                ],
                            ],
                            'chikomagnet'=>[
                                'status'=>(int)boo($value['ПБ_ЧМС']),
                                'magnitov'=>tostr($value['ПБ_ЧМКМ']),
                            ],
                        ],
                        '_rd'=>[
                            'date'=>strtotime(tostr($value['ЗБ_Дата']))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value['ЗБ_Дата'])),'dd.MM.yyyy') : '',
                            'dlina'=>tostr($value['ЗБ_Д']),
                            'visota'=>tostr($value['ЗБ_Ш']),
                            'magnit'=>tostr($value['ЗБ_ЕМД']),
                            'obshivka'=>@$obshiv[tostr($value['ЗБ_Обш'])],
                            'chastei'=>tostr($value['ЧЭ']),
                            'hlyastik'=>tostr($value['ЗБ_ХЛ']),
                            'laitovo'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ЗБ_ЛС']),
                                ],
                                'dontlooks'=>[
                                    'status'=>(int)boo($value['ЗБ_ЧЛС']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['ЗБ_ЛБ']),
                                ],
                            ],
                            'chiko'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ЗБ_ЧС']),
                                    'natyag'=>tostr($value['ЗБ_НатЧС']),
                                ],
                                'dontlooks'=>[
                                    'status'=>(int)boo($value['ЗБ_ЧСД']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['Зб_ЧЛБ']),
                                ],
                            ],
                            'chikomagnet'=>[
                                'status'=>(int)boo($value['ЗБ_ЧМС']),
                                'magnitov'=>tostr($value['ЗБ_ЧМКМ']),
                            ],
                        ],
                        '_rv'=>[
                            'date'=>strtotime(tostr($value['ЗФ_Дата']))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value['ЗФ_Дата'])),'dd.MM.yyyy') : '',
                            'dlina'=>tostr($value['ЗФ_Д']),
                            'visota'=>tostr($value['ЗФ_Ш']),
                            'magnit'=>tostr($value['ЗФ_ЕМД']),
                            'obshivka'=>@$obshiv[tostr($value['ЗФ_Обш'])],
                            'openwindow'=>tostr($value['ЗФ_СтеклоОткрывается']),
                            'openwindowtrue'=>tostr($value['ЗФ_МожноОткрытьФорточку']),
                            'hlyastik'=>tostr($value['ЗФ_ХЛ']),
                            'laitovo'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ЗФ_ЛС']) ?: (int)boo($value['ЗФ_ЛТ']),
                                    'forma'=>(int)boo($value['ЗФ_ЛС']) ? 'Квадратная' : ( (int)boo($value['ЗФ_ЛТ']) ? 'Треугольная' : '' ),
                                ],
                                'dontlooks'=>[
                                    'status'=>(int)boo($value['ЗФ_ЧЛС']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['ЗФ_ЧЛБ']),
                                ],
                            ],
                            'chiko'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ЗФ_ЧС']) ?: (int)boo($value['ЗФ_ЧТ']),
                                    'forma'=>(int)boo($value['ЗФ_ЧС']) ? 'Квадратная' : ( (int)boo($value['ЗФ_ЧТ']) ? 'Треугольная' : '' ),
                                    'natyag'=>(int)boo($value['ЗФ_ЧС']) ? tostr($value['ЗФ_НатЧС']) : tostr($value['ЗФ_НатЧСТ']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['ЗФ_ЧЧЛБ']),
                                ],
                            ],
                        ],
                        '_bw'=>[
                            'date'=>strtotime(tostr($value['ЗШ_Дата']))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value['ЗШ_Дата'])),'dd.MM.yyyy') : '',
                            'dlina'=>tostr($value['ЗШ_Д']),
                            'visota'=>tostr($value['ЗШ_Ш']),
                            'magnit'=>tostr($value['ЗШ_ЕМД']),
                            'obshivka'=>@$obshiv[tostr($value['ЗШ_Обш'])],
                            'openwindow'=>tostr($value['ЗШ_СтеклоОткрывается']),
                            'openwindowtrue'=>tostr($value['ЗШ_МожноОткрытьФорточку']),
                            'chastei'=>tostr($value['ЗШ_ЧастЭкр']),
                            'chiko'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ЗШ_ЧЧС']),
                                    'natyag'=>tostr($value['ЗШ_натЧС']),
                                ],
                                'dontlookb'=>[
                                    'status'=>(int)boo($value['ЗШ_ЧЧЛБ']),
                                ],
                            ],
                            'laitovo'=>[
                                'standart'=>[
                                    'status'=>(int)boo($value['ЗШ_ЛС']),
                                ],
                                'dontlooks'=>[
                                    'status'=>(int)boo($value['ЗШ_ЧЛС']),
                                ],
                            ],
                        ],
                    ],
                ];


                $car=Cars::find()->where(['article'=>$load['Cars']['article']])->one();
                
                if (!$car){
                    $car=new Cars;
                    $load['Cars']['json']=Json::encode( $load['Cars']['json'] );
                } else {
                    $load['Cars']['json']=Json::encode( ArrayHelper::merge(Json::decode($car->json ? $car->json : '[]'), $load['Cars']['json']) );
                }

                $car->load($load);
                $car->name=$car->fullname;

                $car->save();

            }


        } else {

            $carses=[];
            foreach ($this->data['V8ExchData']['CatalogObject.АвтоШторки_Автомобили'] as $key => $value) {
                if (!boo($value->НеСинхронизировать)){

                    $load['Cars']=[
                        'Ref'=>tostr($value->Ref),
                        'article'=>(int)tostr($value->Артикул),
                        'name'=>tostr($value->Description),
                        'mark'=>tostr($value->Марка),
                        'model'=>tostr($value->Модель),
                        'generation'=>tostr($value->Поколение)?:'',
                        'carcass'=>@$kuzov[tostr($value->Кузов)],
                        'doors'=>tostr($value->КоличествоДверей),
                        'firstyear'=>tostr($value->ГодНачалаВыпуска),
                        'lastyear'=>tostr($value->ГодОкончанияВыпуска)!='9999' ? tostr($value->ГодОкончанияВыпуска) : '',
                        'modification'=>tostr($value->Модификация),
                        'country'=>tostr($value->СтранаПроизводитель),
                        'category'=>@$categor[tostr($value->Категория)],
                        'json'=>[
                            'modelde'=>tostr($value->Модель2),
                            'mark_en'=>tostr($value->E_Mark),
                            'model_en'=>tostr($value->E_Model),
                            'modelde_en'=>tostr($value->E_Model2),
                            'modification_en'=>tostr($value->E_Modification),
                            'country_en'=>tostr($value->E_Country),
                            'info'=>tostr($value->ОсобУст),
                            'visotapb'=>tostr($value->ВысотаПБ),
                            'dlinapb'=>tostr($value->ДлинаПБ),
                            'dlinapb1'=>tostr($value->ДлинаПБ1),
                            'visotazb'=>tostr($value->ВысотаЗБ),
                            'dlinazb'=>tostr($value->ДлинаЗБ),
                            'dlinazb1'=>tostr($value->ДлинаЗБ1),
                            'visotazf'=>tostr($value->ВысотаЗФ),
                            'dlinazf'=>tostr($value->ДлинаЗФ),
                            'dlinazf1'=>tostr($value->ДлинаЗФ1),
                            'visotazs'=>tostr($value->ВысотаЗШ),
                            'dlinazs'=>tostr($value->ДлинаЗШ),
                            'dlinazs1'=>tostr($value->ДлинаЗШ1),
                            'nomerlekala'=>tostr($value->НомерЛекала),
                            'typekrep'=>tostr($value->ТипКрепления),
                            'fw'=>(int)boo($value->ПШ),
                            'fv'=>(int)boo($value->ПФ),
                            'fd'=>(int)boo($value->ПБ),
                            'rd'=>(int)boo($value->ЗБ),
                            'rv'=>(int)boo($value->ЗФ),
                            'bw'=>(int)boo($value->ЗШ),
                            'pk'=>(int)boo($value->ПК),
                            '_fw'=>[
                                'status'=>(int)boo($value->ПШ_В),
                                'date'=>strtotime(tostr($value->ПШ_Дата))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value->ПШ_Дата)),'dd.MM.yyyy') : '',
                                'scheme'=>[
                                    tostr($value->Р1),
                                    tostr($value->Р2),
                                    tostr($value->Р3),
                                    tostr($value->Р4),
                                    tostr($value->Р5),
                                    tostr($value->Р6),
                                    tostr($value->Р7),
                                    tostr($value->Р8),
                                    tostr($value->Р9),
                                ],
                            ],
                            '_fv'=>[
                                'date'=>strtotime(tostr($value->ПФ_Дата))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value->ПФ_Дата)),'dd.MM.yyyy') : '',
                                'dlina'=>tostr($value->ПФ_Д),
                                'visota'=>tostr($value->ПФ_Ш),
                                'magnit'=>tostr($value->ПФ_ЕМД),
                                'laitovo'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ПФ_ЛС),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->ПФ_ЧЛБ),
                                    ],
                                ],
                                'chiko'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ПФ_ЧЛС),
                                        'natyag'=>tostr($value->ПФ_натЧС),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->ПФ_ЧБ),
                                    ],
                                ],
                            ],
                            '_fd'=>[
                                'date'=>strtotime(tostr($value->ПБ_Дата))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value->ПБ_Дата)),'dd.MM.yyyy') : '',
                                'dlina'=>tostr($value->ПБ_Д),
                                'visota'=>tostr($value->ПБ_Ш),
                                'magnit'=>tostr($value->ПБ_ЕМД),
                                'obshivka'=>@$obshiv[tostr($value->ПБ_Обш)],
                                'hlyastik'=>tostr($value->ПБ_ХЛ),
                                'laitovo'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ПБ_ЛС),
                                    ],
                                    'short'=>[
                                        'status'=>(int)boo($value->ПБ_ЛУ),
                                    ],
                                    'smoke'=>[
                                        'status'=>(int)boo($value->ПБ_ЛВК),
                                    ],
                                    'mirror'=>[
                                        'status'=>(int)boo($value->ПБ_ЛВБЗ),
                                        'maxdlina'=>tostr($value->ПБ_ЛВЗД),
                                        'maxvisota'=>tostr($value->ПБ_ЛВЗВ),
                                    ],
                                    'dontlooks'=>[
                                        'status'=>(int)boo($value->ПБ_ЧДЛС),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->ПБ_ЧДЛБ),
                                    ],
                                ],
                                'chiko'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ПБ_ЧС),
                                        'natyag'=>tostr($value->ПБ_НатЧС),
                                    ],
                                    'short'=>[
                                        'status'=>(int)boo($value->ПБ_ЧУ),
                                        'natyag'=>tostr($value->ПБ_НатЧУ),
                                    ],
                                    'smoke'=>[
                                        'status'=>(int)boo($value->ПБ_ЧВК),
                                        'natyag'=>tostr($value->ПБ_НатЧВК),
                                    ],
                                    'mirror'=>[
                                        'status'=>(int)boo($value->ПБ_ЧВБЗ),
                                        'natyag'=>tostr($value->ПБ_НатЧВЗ),
                                    ],
                                    'dontlooks'=>[
                                        'status'=>(int)boo($value->ПБ_ЧДЧС),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->ПБ_ЧДЧБ),
                                    ],
                                ],
                                'chikomagnet'=>[
                                    'status'=>(int)boo($value->ПБ_ЧМС),
                                    'magnitov'=>tostr($value->ПБ_ЧМКМ),
                                ],
                            ],
                            '_rd'=>[
                                'date'=>strtotime(tostr($value->ЗБ_Дата))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value->ЗБ_Дата)),'dd.MM.yyyy') : '',
                                'dlina'=>tostr($value->ЗБ_Д),
                                'visota'=>tostr($value->ЗБ_Ш),
                                'magnit'=>tostr($value->ЗБ_ЕМД),
                                'obshivka'=>@$obshiv[tostr($value->ЗБ_Обш)],
                                'hlyastik'=>tostr($value->ЗБ_ХЛ),
                                'chastei'=>tostr($value->ЧЭ),
                                'laitovo'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ЗБ_ЛС),
                                    ],
                                    'dontlooks'=>[
                                        'status'=>(int)boo($value->ЗБ_ЧЛС),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->ЗБ_ЛБ),
                                    ],
                                ],
                                'chiko'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ЗБ_ЧС),
                                        'natyag'=>tostr($value->ЗБ_НатЧС),
                                    ],
                                    'dontlooks'=>[
                                        'status'=>(int)boo($value->ЗБ_ЧСД),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->Зб_ЧЛБ),
                                    ],
                                ],
                                'chikomagnet'=>[
                                    'status'=>(int)boo($value->ЗБ_ЧМС),
                                    'magnitov'=>tostr($value->ЗБ_ЧМКМ),
                                ],
                            ],
                            '_rv'=>[
                                'date'=>strtotime(tostr($value->ЗФ_Дата))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value->ЗФ_Дата)),'dd.MM.yyyy') : '',
                                'dlina'=>tostr($value->ЗФ_Д),
                                'visota'=>tostr($value->ЗФ_Ш),
                                'magnit'=>tostr($value->ЗФ_ЕМД),
                                'obshivka'=>@$obshiv[tostr($value->ЗФ_Обш)],
                                'openwindow'=>tostr($value->ЗФ_СтеклоОткрывается),
                                'openwindowtrue'=>tostr($value->ЗФ_МожноОткрытьФорточку),
                                'hlyastik'=>tostr($value->ЗФ_ХЛ),
                                'laitovo'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ЗФ_ЛС) ?: (int)boo($value->ЗФ_ЛТ),
                                        'forma'=>(int)boo($value->ЗФ_ЛС) ? 'Квадратная' : ( (int)boo($value->ЗФ_ЛТ) ? 'Треугольная' : '' ),
                                    ],
                                    'dontlooks'=>[
                                        'status'=>(int)boo($value->ЗФ_ЧЛС),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->ЗФ_ЧЛБ),
                                    ],
                                ],
                                'chiko'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ЗФ_ЧС) ?: (int)boo($value->ЗФ_ЧТ),
                                        'forma'=>(int)boo($value->ЗФ_ЧС) ? 'Квадратная' : ( (int)boo($value->ЗФ_ЧТ) ? 'Треугольная' : '' ),
                                        'natyag'=>(int)boo($value->ЗФ_ЧС) ? tostr($value->ЗФ_НатЧС) : tostr($value->ЗФ_НатЧСТ),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->ЗФ_ЧЧЛБ),
                                    ],
                                ],
                            ],
                            '_bw'=>[
                                'date'=>strtotime(tostr($value->ЗШ_Дата))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value->ЗШ_Дата)),'dd.MM.yyyy') : '',
                                'dlina'=>tostr($value->ЗШ_Д),
                                'visota'=>tostr($value->ЗШ_Ш),
                                'magnit'=>tostr($value->ЗШ_ЕМД),
                                'obshivka'=>@$obshiv[tostr($value->ЗШ_Обш)],
                                'openwindow'=>tostr($value->ЗШ_СтеклоОткрывается),
                                'openwindowtrue'=>tostr($value->ЗШ_МожноОткрытьФорточку),
                                'chastei'=>tostr($value->ЗШ_ЧастЭкр),
                                'chiko'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ЗШ_ЧЧС),
                                        'natyag'=>tostr($value->ЗШ_натЧС),
                                    ],
                                    'dontlookb'=>[
                                        'status'=>(int)boo($value->ЗШ_ЧЧЛБ),
                                    ],
                                ],
                                'laitovo'=>[
                                    'standart'=>[
                                        'status'=>(int)boo($value->ЗШ_ЛС),
                                    ],
                                    'dontlooks'=>[
                                        'status'=>(int)boo($value->ЗШ_ЧЛС),
                                    ],
                                ],
                            ],
                        ],
                    ];

                    $firstdate='';
                    if (isset($value->ЖурналВнесенныхИзменений->Row[0])){
                        $firstdate=strtotime(tostr($value->ЖурналВнесенныхИзменений->Row[0]->Дата))>0 ? Yii::$app->formatter->asDate(strtotime(tostr($value->ЖурналВнесенныхИзменений->Row[0]->Дата)),'dd.MM.yyyy') : '';
                    }

                    if ($load['Cars']['json']['fw']){
                        $load['Cars']['json']['fw1']=1;
                        if (!$load['Cars']['json']['_fw']['date'] && $firstdate)
                            $load['Cars']['json']['_fw']['date']=$firstdate;
                    }
                    if ($load['Cars']['json']['fv']){
                        $load['Cars']['json']['fv1']=1;
                        if (!$load['Cars']['json']['_fv']['date'] && $firstdate)
                            $load['Cars']['json']['_fv']['date']=$firstdate;
                    }
                    if ($load['Cars']['json']['fd']){
                        $load['Cars']['json']['fd1']=1;
                        if (!$load['Cars']['json']['_fd']['date'] && $firstdate)
                            $load['Cars']['json']['_fd']['date']=$firstdate;
                    }
                    if ($load['Cars']['json']['rd']){
                        $load['Cars']['json']['rd1']=1;
                        if (!$load['Cars']['json']['_rd']['date'] && $firstdate)
                            $load['Cars']['json']['_rd']['date']=$firstdate;
                    }
                    if ($load['Cars']['json']['rv']){
                        $load['Cars']['json']['rv1']=1;
                        if (!$load['Cars']['json']['_rv']['date'] && $firstdate)
                            $load['Cars']['json']['_rv']['date']=$firstdate;
                    }
                    if ($load['Cars']['json']['bw']){
                        $load['Cars']['json']['bw1']=1;
                        if (!$load['Cars']['json']['_bw']['date'] && $firstdate)
                            $load['Cars']['json']['_bw']['date']=$firstdate;
                    }

                    $car=Cars::find()->where(['article'=>$load['Cars']['article']])->one();
                    
                    if (!$car){
                        $car=new Cars;
                        $load['Cars']['json']=Json::encode( $load['Cars']['json'] );
                    } else {
                        $load['Cars']['json']=Json::encode( ArrayHelper::merge(Json::decode($car->json ? $car->json : '[]'), $load['Cars']['json']) );
                    }

                    $car->load($load);
                    $car->name=$car->fullname;

                    if ($car->save()){
                        $carses[$load['Cars']['Ref']]=$car->id;
                        $clipses=[];
                        if (isset($kreplen[$load['Cars']['Ref']])){
                            foreach ($kreplen[$load['Cars']['Ref']] as $key => $value) {
                                $clips=CarClips::find()->where(['car_id'=>$car->id,'name'=>$value['name'],'type'=>$value['type']])->one();
                                if (!$clips)
                                    $clips=new CarClips;
                                $clips->car_id=$car->id;
                                $clips->name=$value['name'];
                                $clips->type=$value['type'];
                                if ($clips->save()){
                                    $clipses[$key]=$clips->id;
                                }
                            }
                        }
                        foreach ($car->clips as $row) {
                            if (!in_array($row->id, $clipses)){
                                $row->delete();
                            }
                        }

                        $schemes=[];
                        if (isset($sheme[$load['Cars']['Ref']])){
                            foreach ($sheme[$load['Cars']['Ref']] as $key => $value) {
                                //пф
                                if ($value['type']=='Стандарт' && (
                                    clip($value['k27'])
                                    || clip($value['k28'])
                                    || clip($value['k29'])
                                    || clip($value['k30'])
                                    || clip($value['k31'])
                                    || clip($value['k32'])
                                    || clip($value['k33'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k27'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k28'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k29'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k30'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k31'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k32'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k33'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ПФ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ПФ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }

                                //пб
                                if ($value['type']=='Стандарт' && (
                                    clip($value['k10'])
                                    || clip($value['k11'])
                                    || clip($value['k12'])
                                    || clip($value['k13'])
                                    || clip($value['k14'])
                                    || clip($value['k15'])
                                    || clip($value['k16'])
                                    || clip($value['k47'])
                                    || clip($value['k46'])
                                    || clip($value['k45'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k10'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k11'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k12'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k13'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k14'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k15'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k16'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k47'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k46'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k45'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ПБ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ПБ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }
                                if ($value['type']=='Укороченный' && (
                                    clip($value['k10'])
                                    || clip($value['k11'])
                                    || clip($value['k12'])
                                    || clip($value['k13'])
                                    || clip($value['k14'])
                                    || clip($value['k46'])
                                    || clip($value['k45'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k10'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k11'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k12'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k13'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k14'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k46'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k45'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ПБ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ПБ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }
                                if ($value['type']=='ВырезДляКурящих' && (
                                    clip($value['k10'])
                                    || clip($value['k11'])
                                    || clip($value['k12'])
                                    || clip($value['k13'])
                                    || clip($value['k14'])
                                    || clip($value['k16'])
                                    || clip($value['k47'])
                                    || clip($value['k46'])
                                    || clip($value['k45'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k10'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k11'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k12'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k13'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k14'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k16'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k47'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k46'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k45'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ПБ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ПБ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }
                                if ($value['type']=='ВырезДляЗеркала' && (
                                    clip($value['k10'])
                                    || clip($value['k11'])
                                    || clip($value['k12'])
                                    || clip($value['k13'])
                                    || clip($value['k14'])
                                    || clip($value['k15'])
                                    || clip($value['k46'])
                                    || clip($value['k45'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k10'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k11'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k12'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k13'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k14'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k15'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k46'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k45'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ПБ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ПБ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }


                                //зб
                                if ($value['type']=='Стандарт' && (
                                    clip($value['k1'])
                                    || clip($value['k2'])
                                    || clip($value['k3'])
                                    || clip($value['k4'])
                                    || clip($value['k5'])
                                    || clip($value['k6'])
                                    || clip($value['k7'])
                                    || clip($value['k8'])
                                    || clip($value['k9'])
                                    || clip($value['k50'])
                                    || clip($value['k49'])
                                    || clip($value['k48'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k1'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k2'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k3'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k4'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k5'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k6'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k7'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k8'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k9'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k50'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k49'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k48'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ЗБ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ЗБ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }

                                //зф
                                if ($value['type']=='Стандарт' && (
                                    clip($value['k17'])
                                    || clip($value['k18'])
                                    || clip($value['k78'])
                                    || clip($value['k19'])
                                    || clip($value['k20'])
                                    || clip($value['k21'])
                                    || clip($value['k79'])
                                    || clip($value['k22'])
                                    || clip($value['k23'])
                                    || clip($value['k24'])
                                    || clip($value['k25'])
                                    || clip($value['k26'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k17'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k18'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k78'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k19'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k20'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k21'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k79'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k22'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k23'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k24'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k25'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k26'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ЗФ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ЗФ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }
                                if ($value['type']=='Треугольная' && (
                                    clip($value['k17'])
                                    || clip($value['k18'])
                                    || clip($value['k78'])
                                    || clip($value['k79'])
                                    || clip($value['k22'])
                                    || clip($value['k23'])
                                    || clip($value['k26'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k17'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k18'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k78'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k79'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k22'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k23'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k26'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ЗФ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ЗФ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }

                                //зш
                                if ($value['type']=='Стандарт' && (
                                    clip($value['k34'])
                                    || clip($value['k35'])
                                    || clip($value['k36'])
                                    || clip($value['k37'])
                                    || clip($value['k75'])
                                    || clip($value['k38'])
                                    || clip($value['k39'])
                                    || clip($value['k40'])
                                    || clip($value['k41'])
                                    || clip($value['k42'])
                                    || clip($value['k76'])
                                    || clip($value['k43'])
                                    || clip($value['k77'])
                                    || clip($value['k44'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k34'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k35'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k36'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k37'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k75'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k38'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k39'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k40'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k41'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k42'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k76'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k43'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k77'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k44'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ЗШ','brand'=>$value['brand'],'type'=>$value['type'],'type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'];
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ЗШ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }
                                if ($value['type']=='Стандарт' && (
                                    clip($value['k51'])
                                    || clip($value['k52'])
                                    || clip($value['k53'])
                                    || clip($value['k54'])
                                    || clip($value['k55'])
                                    || clip($value['k56'])
                                    || clip($value['k57'])
                                    || clip($value['k58'])
                                    || clip($value['k59'])
                                    || clip($value['k60'])
                                    || clip($value['k61'])
                                    || clip($value['k62'])
                                    || clip($value['k63'])
                                    || clip($value['k64'])
                                    || clip($value['k65'])
                                    || clip($value['k66'])
                                    || clip($value['k67'])
                                    || clip($value['k68'])
                                    || clip($value['k69'])
                                    || clip($value['k70'])
                                    || clip($value['k71'])
                                    || clip($value['k72'])
                                    || clip($value['k73'])
                                    || clip($value['k74'])
                                    )){
                                    $pos_scheme=[];
                                    $pos_scheme[]=(string)@$clipses[clip($value['k51'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k52'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k53'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k54'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k55'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k56'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k57'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k58'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k59'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k60'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k61'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k62'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k63'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k64'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k65'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k66'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k67'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k68'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k69'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k70'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k71'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k72'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k73'])]?:'';
                                    $pos_scheme[]=(string)@$clipses[clip($value['k74'])]?:'';
                                    $scheme=ClipsScheme::find()->where(['car_id'=>$car->id,'window'=>'ЗШ','brand'=>$value['brand'],'type'=>$value['type'].' 2 части','type_clips'=>$value['type_clips']])->one();
                                    if (!$scheme)
                                        $scheme=new ClipsScheme;
                                    $scheme->car_id=$car->id;
                                    $scheme->brand=$value['brand'];
                                    $scheme->type=$value['type'].' 2 части';
                                    $scheme->type_clips=$value['type_clips'];
                                    $scheme->window='ЗШ';
                                    $scheme->json=Json::encode(['scheme'=>$pos_scheme]);

                                    if ($scheme->save()){
                                        $schemes[$scheme->id]=$scheme->id;
                                    }
                                }
                            }
                        }
                        foreach ($car->schemes as $row) {
                            if (!in_array($row->id, $schemes)){
                                $row->delete();
                            }
                        }
                    }

                }
            }

            $elements=[];
            foreach ($this->data['V8ExchData']['InformationRegisterRecordSet.Автошторки_Аналоги'] as $key => $value) {
                if (isset($carses[tostr($value->Records->Record->Автомобиль)]) && isset($carses[tostr($value->Records->Record->Аналог)])){
                    $car_id=$carses[tostr($value->Records->Record->Автомобиль)];
                    $analog_id=$carses[tostr($value->Records->Record->Аналог)];
                    
                    $elements[$car_id.'-'.$analog_id][]=tostr($value->Records->Record->Элемент);

                    $analog=CarsAnalog::find()->where(['car_id'=>$car_id,'analog_id'=>$analog_id])->one();
                    if (!$analog)
                        $analog=new CarsAnalog;
                    $analog->car_id=$car_id;
                    $analog->analog_id=$analog_id;
                    $analog->json=Json::encode(['elements'=>$elements[$car_id.'-'.$analog_id]]);
                    if ($analog->save()){
                    }
                }
            }

        }

        echo 'finish';
    }


}
