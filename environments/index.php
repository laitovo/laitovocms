<?php
/**
 * The manifest of files that are local to specific environment.
 * This file returns a list of environments that the application
 * may be installed under. The returned data must be in the following
 * format:
 *
 * ```php
 * return [
 *     'environment name' => [
 *         'path' => 'directory storing the local files',
 *         'skipFiles'  => [
 *             // list of files that should only copied once and skipped if they already exist
 *         ],
 *         'setWritable' => [
 *             // list of directories that should be set writable
 *         ],
 *         'setExecutable' => [
 *             // list of files that should be set executable
 *         ],
 *         'setCookieValidationKey' => [
 *             // list of config files that need to be inserted with automatically generated cookie validation keys
 *         ],
 *         'createSymlink' => [
 *             // list of symlinks to be created. Keys are symlinks, and values are the targets.
 *         ],
 *     ],
 * ];
 * ```
 */
return [
    'Development' => [
        'path' => 'dev',
        'setWritable' => [
            'obmen/runtime',
            'obmen/files',
            'obmen/web/assets',
            'backend/runtime',
            'backend/web/assets',
            'api/runtime',
            'api/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
        ],
        'setExecutable' => [
            'yii',
            'tests/codeception/bin/yii',
        ],
        'setCookieValidationKey' => [
            '.env/COOKIE_KEY_BACKEND',
            '.env/COOKIE_KEY_API',
            '.env/COOKIE_KEY_OBMEN',
            '.env/COOKIE_KEY_FRONTEND',
        ],
    ],
    'Production' => [
        'path' => 'prod',
        'setWritable' => [
            'obmen/runtime',
            'obmen/files',
            'obmen/web/assets',
            'backend/runtime',
            'backend/web/assets',
            'api/runtime',
            'api/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            '.env/COOKIE_KEY_BACKEND',
            '.env/COOKIE_KEY_API',
            '.env/COOKIE_KEY_OBMEN',
            '.env/COOKIE_KEY_FRONTEND',
        ],
    ],
];
