<?php
return [
    'supportEmail' => 'support@biz-zone.biz',
    'mailer.transport' =>[
    	'supportEmail'=>[
	        'class' => 'Swift_SmtpTransport',
	        'host' => 'smtp.yandex.ru',
	        'username' => 'support@biz-zone.biz',
	        'password' => '',
	        'port' => '465',
	        'encryption' => 'ssl',
	    ],
    ],
    'remoteStorage' => [
        'host'  => 'http://localhost',
        'token' => 'token'
    ],
];
