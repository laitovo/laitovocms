<?php
Yii::$container->setSingletons([
    #LocationRepository
    'backend\modules\logistics\modules\implementationMonitor\interfaces\ILocationRepository' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\LocationRepository'
        ],
    #LocationService
    'backend\modules\logistics\modules\implementationMonitor\interfaces\ILocationService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\LocationService'
        ],
    #OrderElementsService
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\OrderElementService'
        ],
    #OrderElementsRepository
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\OrderElementsRepository'
        ],
    #OrderService
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\OrderService'
        ],
    #OrdersRepository
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\OrdersRepository'
        ],
    #UpnRepository
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnRepository' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\UpnRepository'
        ],
    #UpnService
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\UpnService'
        ],
    #IBarcodeHelper
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IBarcodeHelper' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\BarcodeHelper'
        ],

]);

Yii::$container->setDefinitions([
    #ITransaction
    'backend\modules\logistics\modules\implementationMonitor\interfaces\ITransaction' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\Transaction'
        ],
]);

Yii::$container->setSingleton(
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IProcessService',
    'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\ProcessService',
    [
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\ITransaction'),

    ]
);

Yii::$container->setSingleton(
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IPackUpnService',
    'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\PackUpnService',
    [
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IBarcodeHelper'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnService'),

    ]
);

Yii::$container->setSingleton(
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IMonitorData',
    'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\MonitorData',
    [
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementService'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnService'),
    ]
);

Yii::$container->setSingleton(
    'backend\modules\logistics\modules\implementationMonitor\interfaces\IReadyReestr',
    'backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces\ReadyReestr',
    [
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService'),
    ]
);
