<?php

Yii::$container->setSingletons([
    #OrderService
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrderService'
        ],
    #OrdersRepository
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrdersRepository' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrdersRepository'
        ],
    #OrderItemService
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrderItemService'
        ],
    #OrderItemsRepository
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemsRepository' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrderItemsRepository'
        ],
    #OrderDocumentService
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrderDocumentService'
        ],
    #OrderDocumentsRepository
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentsRepository' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrderDocumentsRepository'
        ],
    #OrderLogistService
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderLogistService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrderLogistService'
        ],
]);

#OrdersProvider
Yii::$container->setSingleton(
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrdersProvider',
    'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrdersProvider',
    [
        \yii\di\Instance::of('backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrdersRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderService'),
    ]
);

#OrderItemsProvider
Yii::$container->setSingleton(
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemsProvider',
    'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrderItemsProvider',
    [
        \yii\di\Instance::of('backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemsRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemService'),
    ]
);

#OrderDocumentsProvider
Yii::$container->setSingleton(
    'backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentsProvider',
    'backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations\OrderDocumentsProvider',
    [
        \yii\di\Instance::of('backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentsRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentService'),
    ]
);