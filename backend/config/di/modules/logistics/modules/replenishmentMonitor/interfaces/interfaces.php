<?php

Yii::$container->setSingletons([
    #ArticlesRepository
    'backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticlesRepository' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations\ArticlesRepository'
        ],
    #ArticleService
    'backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticleService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations\ArticleService'
        ],
    #ConfigService
    'backend\modules\logistics\modules\replenishmentMonitor\interfaces\IConfigService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations\ConfigService'
        ],
    #CommandService
    'backend\modules\logistics\modules\replenishmentMonitor\interfaces\ICommandService' =>
        [
            'class' => 'backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations\CommandService'
        ],
]);

#ArticlesDtoProvider
Yii::$container->setSingleton(
    'backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticlesDtoProvider',
    'backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations\ArticlesDtoProvider',
    [
        \yii\di\Instance::of('backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticlesRepository'),
        \yii\di\Instance::of('backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticleService'),
        \yii\di\Instance::of('backend\modules\logistics\modules\replenishmentMonitor\interfaces\IConfigService'),
    ]
);
