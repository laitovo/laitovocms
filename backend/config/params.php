<?php
return [
	'erp_dispatcher'=>7,
	'erp_okleika'=>3,
	'erp_shveika'=>4,
	'erp_label'=>5,
	'erp_clipsi'=>1,
	'erp_izgib'=>2,
	'erp_otk'=>6,
    //необходимо проставить ID вида работы для клипс 1, 2 и 3
    'jobClips1' => 1,
    'jobClips2' => 2,
    'jobClips3' => 3,
    'jobClipsMagnets' => 192,
    'materialClips1' => 0,
    'materialClips2' => 0,
    'materialClips3' => 0,

    //вид работы велькро
    'velkro'=>20,
    //вид работы велькро
    'stochka'=> [211,212,213,214,215,216,217,218],

    //идентификатор источника заявок в модуль логистики для сайта Laitovo.ru
    'laitovo_source_id' => 3,
    //артикулы, которые грузяься со склада
    'storageArticlesRules' => [
        '#^(OT)-(673|674|675)-\d+-\d$#',
        '#^(OT)-(1211)-\d+-\d$#',
        '#^(OT)-(1510)-\d+-\d$#',
        '#^(OT)-(1209|1213)-\d+-\d$#',
        '#^(OT)-(1214)-(47)-\d$#',
        '#^(OT)-(1189)-(47)-\d$#',
        '#^(OT)-(1332|1481)-\d+-\d$#',
        '#^(OT)-(1345)-\d+-\d$#',
        '#^(OT)-(1190)-(47)-\d$#',
        '#^(OT)-(1215)-(47)+-(3)$#',
        '#^(OT)-(1394)-\d+-\d$#',
        '#^(OT)-(1220)-\d+-\d$#',
        '#^(OT)-(1221)-\d+-\d$#',
        '#^(OT)-(1222)-\d+-\d$#',
        '#^(OT)-(1223)-\d+-\d$#',
        '#^(OT)-(1224)-\d+-\d$#',
    ],
        

    //правило определения нарядов для стажера
    'trainee_pattern' => '/((FD|RD|RV|FV)-\w-[0-9]+-1-\d+)/',
    'trainee_pattern_sewing' => '/((FD|RD|RV|FV)-\w-[0-9]+-1-5)/',
    'trainee_pattern_sewing_2' => '/((FD|RD|RV|FV)-\w-[0-9]+-1-(5|2|4|8))/',
    // api clients
    'client_url' => 'https://clients.biz-zone.biz/api/v1/'
];
