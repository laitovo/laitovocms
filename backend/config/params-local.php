<?php
return [
    'obmen_laitovo_site' => [
        'host'     => $_ENV['DB_LAITOVO_HOST'],
        'port'     => $_ENV['DB_LAITOVO_PORT'],
        'dbname'   => $_ENV['DB_LAITOVO_NAME'],
        'username' => $_ENV['DB_LAITOVO_USER'],
        'password' => $_ENV['DB_LAITOVO_PASSWD'],
        'charset'  => $_ENV['DB_LAITOVO_CHARSET'],
    ],
    'obmen_clients' => [
        'host'     => $_ENV['DB_CLIENTS_HOST'],
        'port'     => $_ENV['DB_CLIENTS_PORT'],
        'dbname'   => $_ENV['DB_CLIENTS_NAME'],
        'username' => $_ENV['DB_CLIENTS_USER'],
        'password' => $_ENV['DB_CLIENTS_PASSWD'],
        'charset'  => $_ENV['DB_CLIENTS_CHARSET'],
    ],
    'obmen_storage' => [
        'host'     => $_ENV['DB_STORAGE_HOST'],
        'port'     => $_ENV['DB_STORAGE_PORT'],
        'dbname'   => $_ENV['DB_STORAGE_NAME'],
        'username' => $_ENV['DB_STORAGE_USER'],
        'password' => $_ENV['DB_STORAGE_PASSWD'],
        'charset'  => $_ENV['DB_STORAGE_CHARSET'],
    ],
    'ozon_integration' => [
        '266368' => [
            'Client-Id' => $_ENV['OZON_FILTERS_CLIENT_ID'],
            'Api-Key' => $_ENV['OZON_FILTERS_API_KEY']
        ],
        '167094' => [
            'Client-Id' => $_ENV['OZON_LAITOVO_CLIENT_ID'],
            'Api-Key' => $_ENV['OZON_LAITOVO_API_KEY']
        ]
    ]
];

