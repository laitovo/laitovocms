<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$cookieDomain = '.' . $_SERVER['SERVER_NAME'];

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log','returnModule\config\SetUpReturn'],
    'modules' => [
        'uploading-test' => [
            'class' => 'backend\modules\uploadingTest\Module',
        ],
        'admin' => [
            'class' => 'backend\modules\admin\Module',
        ],
        'order' => [
            'class' => 'backend\modules\order\Module',
        ],
        'user' => [
            'class' => 'backend\modules\user\Module',
        ],
        'website' => [
            'class' => 'backend\modules\website\Module',
        ],
        'laitovo' => [
            'class' => 'backend\modules\laitovo\Module',
        ],
        'wage' => [
            'class' => 'backend\modules\wage\Module',
        ],
        'logistics' => [
            'class' => 'backend\modules\logistics\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'leader-module' => [
            'class' => 'backend\modules\leaderModule\LeaderModule',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => true,
                'domain' => $cookieDomain
            ],
        ],
        'session' => [
            'cookieParams' => [
                'domain' => $cookieDomain,
                'httpOnly' => true,
            ],
            'name' => 'PHPSESSID',
        ],
//        'user' => [
//            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//        ],
        'team' => [
            'class' => 'backend\models\Team',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@backend/views' => '@backend/themes/remark',
                    '@backend/modules' => '@backend/themes/remark/modules',
                    '@backend/widgets' => '@backend/themes/remark/widgets',
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'depends' => [
                        'yii\jui\JuiAsset',
                        'yii\bootstrap\BootstrapAsset',
                    ],
                ],
            ],
        ],    
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'fileManager' => [
            'class' => 'backend\components\fileManager\FileManager',
        ]
    ],
    'params' => $params,
];
