<?php

namespace backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations;

use backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticlesRepository;
use core\models\article\Article;
use yii\db\Query;

class ArticlesRepository implements IArticlesRepository
{
    public function getArticles()
    {
        $result = [];

        $query = new Query();
        $query
            ->select('*')
            ->from('tmp_statistics ')
            ->where('needAbsolute <> 0 AND needRelative <> 0')
            ->andWhere('prefix <> "OT"')
            //Отключаю артикулы на магнитах
            ->andWhere(['not like', 'article', '%-%-%-67-%', false])
            ->andWhere(['not like', 'article', '%-%-%-72-%', false])
            ->andWhere(['not like', 'article', '%-%-%-71-%', false])
            ->andWhere(['not like', 'article', '%-%-%-70-%', false])

            ->orderBy('needRelative DESC, needAbsolute DESC');
        $rows = $query->all();

        foreach ($rows as $row) {
            $result[] = (object)$row;
        }

        return $result;
    }

    public function getArticlesFilteredByWindows($windows = [],$brands = [],$carName = null)
    {
        $queryStr = '"'. implode('","',$windows) . '"';
        $result = [];

        $query = new Query();
        $query
            ->select('*')
            ->from('tmp_statistics ')
            ->where('needAbsolute <> 0 AND needRelative <> 0')
            ->andWhere('prefix <> "OT"')
//            ->andWhere(['not like', 'article', '%-%-%-45-%', false])
//            ->andWhere(['not like', 'article', '%-%-%-48-%', false])
//            ->andWhere(['not like', 'article', '%-%-%-55-%', false])
//            ->andWhere(['not like', 'article', '%-%-%-67-%', false])
//            ->andWhere(['not like', 'article', '%-%-%-72-%', false])
//            ->andWhere(['not like', 'article', '%-%-%-71-%', false])
//            ->andWhere(['not like', 'article', '%-%-%-70-%', false]);
            ->andWhere(['like', 'article', '%-%-%-1-%', false]);

        if (!empty($windows))
        $query
            ->andWhere('prefix in ('.$queryStr.')');

        if (in_array('Laitovo',$brands) and count($brands) == 1) {
            $query
                ->andWhere(['or',
                    ['like', 'article', '%-%-%-%-2', false],
                    ['like', 'article', '%-%-%-%-3', false],
                    ['like', 'article', '%-%-%-%-4', false],
                    ['like', 'article', '%-%-%-%-8', false],
                ]);
        }
        if (in_array('Chiko',$brands) and count($brands) == 1) {
            $query
                ->andWhere(['like', 'article', '%-%-%-%-5', false]);
        }

        if ($carName)
            $query
                ->andWhere(['like', 'analogs', $carName]);

        $query
            ->orderBy('needRelative DESC, needAbsolute DESC');
        $rows = $query->all();

        foreach ($rows as $row) {
            $article = new Article($row['article']);
            $row['statistics'] = $article->saleSixMonth;
            $row['balancePlan'] = $article->balancePlan;
            $result[] = (object)$row;
        }

        return $result;
    }
}