<?php

namespace backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations;

use backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticleService;

class ArticleService implements IArticleService
{
    public function getId($article)
    {
        return 1; //test
    }

    public function getArticle($article)
    {
        return $article->article; //test
    }

    public function getCar($article)
    {
        return $article->car; //test
    }

    public function getDemandInPercent($article)
    {
        return $article->needRelative; //test
    }

    public function getDemandInPieces($article)
    {
        return $article->needAbsolute; //test
    }

    public function getBalanceOnStorage($article)
    {
        return $article->fact; //test
    }
    public function getBalanceInMake($article)
    {
        return $article->prod; //test
    }
    public function getTitle($article)
    {
        return $article->title; //test
    }
    public function getAnalogs($article)
    {
        return $article->analogs; //test
    }
    public function getUpdateDate($article)
    {
        return $article->updated_at; //test
    }


}