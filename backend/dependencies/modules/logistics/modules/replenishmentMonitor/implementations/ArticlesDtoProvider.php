<?php

namespace backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations;

use backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticlesDtoProvider;

class ArticlesDtoProvider extends \backend\modules\logistics\modules\replenishmentMonitor\models\ArticlesDtoProvider implements IArticlesDtoProvider {}