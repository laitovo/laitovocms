<?php

namespace backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryadFactory;
use backend\modules\laitovo\models\MainProductInfo;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\modules\replenishmentMonitor\interfaces\ICommandService;
use core\models\article\Article;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class CommandService implements ICommandService
{
    public function replenish($article, $count)
    {
        $connection = \Yii::$app->db;

        $transaction = $connection->beginTransaction();
        try {
            for ($i = 1; $i <= $count; $i++) {
                $naryad = new Naryad();
                $naryad->article = $article;
                $naryad->team_id = \Yii::$app->team->id;
                $naryad->comment = "Пополнение склада";
                $naryad->source_id = 1;
                $naryad->save();

                $title = MainProductInfo::getTitleNew($article);
                $factory = new ErpNaryadFactory();
                $workOrderBarcode = BarcodeHelper::upnToWorkOrderBarcode($naryad->barcode);
                $factory->create($article,$title,$naryad->id,$workOrderBarcode,$naryad->comment,'','',7);
            }

            $articleObj = new Article($article);
            $bool = (new Query())
                ->select('article')
                ->from('tmp_statistics')
                ->where('article = :article')
                ->addParams([':article' => $articleObj->id])
                ->exists();

            //Получим фактические остатки.

            $query = new Query();
            $query->select('article')
                ->from('laitovo_erp_naryad')
                ->where('logist_id is not null')
                ->andWhere(['distributed' => 0])
                ->andWhere(['IN','article',$articleObj->articleWithAnalogs]);

            $prod = $query->count() ?: 0;

            $fact = $prod + $articleObj->balanceFact;
            $abs = ($articleObj->balancePlan - $fact) > 0 ? $articleObj->balancePlan - $fact : 0;
            $rel = !$articleObj->balancePlan ? 0 : ( ($articleObj->balancePlan - $fact) < 0 ? 0 : ( ($articleObj->balancePlan - $fact) / $articleObj->balancePlan )*100 );


            if ($bool) {
                $connection->createCommand()->update('tmp_statistics', [
                    'prefix'       => $articleObj->windowEn,
                    'car'          => $articleObj->carName,
                    'needAbsolute' => $abs,
                    'needRelative' => $rel,
                    'plan'         => $articleObj->balancePlan,
                    'fact'         => $articleObj->balanceFact,
                    'prod'         => $prod,
                    'updated_at'   => time(),
                ],'article = :article',[':article' => $articleObj->id])->execute();
            } else {
                $connection->createCommand()->insert('tmp_statistics', [
                    'article'      => $articleObj->id,
                    'prefix'       => $articleObj->windowEn,
                    'car'          => $articleObj->carName,
                    'needAbsolute' => $abs,
                    'needRelative' => $rel,
                    'plan'         => $articleObj->balancePlan,
                    'fact'         => $articleObj->balanceFact,
                    'prod'         => $prod,
                    'updated_at'   => time(),
                ])->execute();
            }

            $transaction->commit();
            return [
                'replenished_count'   => $count,
                'replenished_article' => $article,
            ];//test

        }catch (\Exception $e) {
            $transaction->rollBack();
        }

        return [];
    }

    public function replenishAndPause($article, $count)
    {
        $connection = \Yii::$app->db;

        $transaction = $connection->beginTransaction();
        $workOrders = [];
        try {
            for ($i = 1; $i <= $count; $i++) {
                $naryad = new Naryad();
                $naryad->article = $article;
                $naryad->team_id = \Yii::$app->team->id;
                $naryad->comment = "Пополнение склада";
                $naryad->source_id = 1;
                $naryad->save();

                $title = MainProductInfo::getTitleNew($article);
                $factory = new ErpNaryadFactory();
                $workOrderBarcode = BarcodeHelper::upnToWorkOrderBarcode($naryad->barcode);
                $workOrders[] = $factory->createAndPause($article,$title,$naryad->id,$workOrderBarcode,$naryad->comment,'','',7);
            }

            $listWorkOrders = ArrayHelper::map($workOrders,'logist_id','logist_id');

            $articleObj = new Article($article);
            $bool = (new Query())
                ->select('article')
                ->from('tmp_statistics')
                ->where('article = :article')
                ->addParams([':article' => $articleObj->id])
                ->exists();

            //Получим фактические остатки.

            $query = new Query();
            $query->select('article')
                ->from('laitovo_erp_naryad')
                ->where('logist_id is not null')
                ->andWhere(['distributed' => 0])
                ->andWhere(['IN','article',$articleObj->articleWithAnalogs]);

            $prod = $query->count() ?: 0;

            $fact = $prod + $articleObj->balanceFact;
            $abs = ($articleObj->balancePlan - $fact) > 0 ? $articleObj->balancePlan - $fact : 0;
            $rel = !$articleObj->balancePlan ? 0 : ( ($articleObj->balancePlan - $fact) < 0 ? 0 : ( ($articleObj->balancePlan - $fact) / $articleObj->balancePlan )*100 );


            if ($bool) {
                $connection->createCommand()->update('tmp_statistics', [
                    'prefix'       => $articleObj->windowEn,
                    'car'          => $articleObj->carName,
                    'needAbsolute' => $abs,
                    'needRelative' => $rel,
                    'plan'         => $articleObj->balancePlan,
                    'fact'         => $articleObj->balanceFact,
                    'prod'         => $prod,
                    'updated_at'   => time(),
                ],'article = :article',[':article' => $articleObj->id])->execute();
            } else {
                $connection->createCommand()->insert('tmp_statistics', [
                    'article'      => $articleObj->id,
                    'prefix'       => $articleObj->windowEn,
                    'car'          => $articleObj->carName,
                    'needAbsolute' => $abs,
                    'needRelative' => $rel,
                    'plan'         => $articleObj->balancePlan,
                    'fact'         => $articleObj->balanceFact,
                    'prod'         => $prod,
                    'updated_at'   => time(),
                ])->execute();
            }

            $transaction->commit();
            return [
                'replenished_count'   => $count,
                'replenished_article' => $article,
                'replenished_article_work_orders' => implode(' , ',$listWorkOrders)
            ];//test

        }catch (\Exception $e) {
            $transaction->rollBack();
        }

        return [];
    }

    public function refresh($article)
    {
        $connection = \Yii::$app->db;

        $transaction = $connection->beginTransaction();
        try {

            $articleObj = new Article($article);
            $bool = (new Query())
                ->select('article')
                ->from('tmp_statistics')
                ->where('article = :article')
                ->addParams([':article' => $articleObj->id])
                ->exists();

            //Получим фактические остатки.

            $query = new Query();
            $query->select('article')
                ->from('laitovo_erp_naryad')
                ->where('logist_id is not null')
                ->andWhere(['distributed' => 0])
                ->andWhere(['IN','article',$articleObj->articleWithAnalogs]);

            $prod = $query->count() ?: 0;

            $fact = $prod + $articleObj->balanceFact;
            $relInMonth = floor($articleObj->saleSixMonth / 6);
            $plan = $relInMonth > $articleObj->balancePlan ? $relInMonth : $articleObj->balancePlan;
            $abs = ($plan - $fact) > 0 ? $plan - $fact : 0;
            $rel = !$plan ? 0 : (($plan - $fact) < 0 ? 0 : (($plan - $fact) / $plan) * 100);
            $updateDate = time();

            if ($bool) {
                $connection->createCommand()->update('tmp_statistics', [
                    'prefix'       => $articleObj->windowEn,
                    'car'          => $articleObj->carName,
                    'needAbsolute' => $abs,
                    'needRelative' => $rel,
                    'plan'         => $articleObj->balancePlan,
                    'fact'         => $articleObj->balanceFact,
                    'prod'         => $prod,
                    'updated_at'   => $updateDate,
                ],'article = :article',[':article' => $articleObj->id])->execute();
            } else {
                $connection->createCommand()->insert('tmp_statistics', [
                    'article'      => $articleObj->id,
                    'prefix'       => $articleObj->windowEn,
                    'car'          => $articleObj->carName,
                    'needAbsolute' => $abs,
                    'needRelative' => $rel,
                    'plan'         => $articleObj->balancePlan,
                    'fact'         => $articleObj->balanceFact,
                    'prod'         => $prod,
                    'updated_at'   => $updateDate,
                ])->execute();
            }

            $transaction->commit();
            return [
                'article'      => $articleObj->id,
                'needAbsolute' => $abs,
                'needRelative' => $rel .'%',
                'fact'         => $articleObj->balanceFact,
                'prod'         => $prod,
                'update'       => \Yii::$app->formatter->asDatetime($updateDate),
            ];//test

        }catch (\Exception $e) {
            $transaction->rollBack();
        }

        return [];
    }



}