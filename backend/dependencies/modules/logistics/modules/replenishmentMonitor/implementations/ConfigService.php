<?php

namespace backend\dependencies\modules\logistics\modules\replenishmentMonitor\implementations;

use backend\modules\logistics\modules\replenishmentMonitor\interfaces\IConfigService;

class ConfigService implements IConfigService
{
    public function getDefaultReplenishedCount()
    {
        return 5;
    }
}