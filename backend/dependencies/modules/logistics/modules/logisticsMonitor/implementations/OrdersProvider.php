<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrdersProvider;

/**
 * Реализация DTO-провайдера заказов для модуля "Монитор логистики".
 *
 * Class OrdersProvider
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrdersProvider extends \backend\modules\logistics\modules\logisticsMonitor\models\OrdersProvider implements IOrdersProvider {}