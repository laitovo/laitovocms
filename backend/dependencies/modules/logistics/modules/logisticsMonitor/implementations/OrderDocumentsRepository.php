<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\models\orderDocument\OrderDocumentRepository;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentsRepository;

/**
 * Реализация репозитория документов заказа для модуля "Монитор логистики".
 *
 * Class OrderDocumentsRepository
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrderDocumentsRepository extends OrderDocumentRepository implements IOrderDocumentsRepository {}