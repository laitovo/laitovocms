<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemsProvider;

/**
 * Реализация DTO-провайдера элементов заказа для модуля "Монитор логистики".
 *
 * Class OrderItemsProvider
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrderItemsProvider extends \backend\modules\logistics\modules\logisticsMonitor\models\OrderItemsProvider implements IOrderItemsProvider {}