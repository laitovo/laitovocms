<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemsRepository;

/**
 * Реализация репозитория заказов для модуля "Монитор логистики".
 *
 * Class OrderItemsRepository
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrderItemsRepository extends OrderEntry implements IOrderItemsRepository
{
    /**
     * Функция возвращает элементы заказа по переданному идентификатору заказа
     *
     * @param int $orderId Идентификатор заказа
     * @return array Массив объектов "Элемент заказа"
     */
    public function getItemsByOrderId($orderId)
    {
        return self::find()->where(['order_id' => $orderId])->all();
    }
}