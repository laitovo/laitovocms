<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrdersRepository;

/**
 * Реализация репозитория заказов для модуля "Монитор логистики".
 *
 * Class OrdersRepository
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrdersRepository extends Order implements IOrdersRepository
{
    /**
     * Функция возвращает массив заказов.
     * Выводим заказы только предварительно обработанные.
     *
     * @return array Массив объектов "Заказ"
     */
    public function getOrders()
    {
        return self::find()->where(['processed' => true])->all();
    }

    /**
     * Функция возвращает заказ по переданному идентификатору
     *
     * @param $id int Идентификатор заказа
     * @return mixed Объект "Заказ"
     */
    public function getOrderById($id)
    {
        return self::findOne($id);
    }
}