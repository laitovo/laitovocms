<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\order\OrderService as OrderPropertyService;
use backend\modules\logistics\models\order\OrderPriceService;
use backend\modules\logistics\models\order\OrderStatusService;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderService;

/**
 * Реализация сервиса, предоставляющего модулю "Монитор логистики" доступ к свойствам заказа.
 *
 * Class OrderService
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrderService implements IOrderService
{
    /**
     * @var OrderPropertyService $_orderService Сервис для работы со свойствами заказа
     */
    private $_orderService;

    /**
     * @var OrderPriceService $_orderPriceService Сервис для работы с ценами заказа
     */
    private $_orderPriceService;

    /**
     * @var OrderStatusService $_orderStatusService Сервис для работы со статусами заказа
     */
    private $_orderStatusService;

    /**
     * В конструкторе инициализируем необходимые сервисы приложения
     *
     * OrderService constructor.
     */
    public function __construct()
    {
        $this->_orderService = new OrderPropertyService();
        $this->_orderPriceService = new OrderPriceService();
        $this->_orderStatusService = new OrderStatusService();
    }

    /**
     * Идентификатор заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return int
     */
    public function getId($order)
    {
        return $this->_orderService->getId($order);
    }

    /**
     * Внутренний номер источника заказа (Идентификатор заявки внутри другого сайта)
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getNumber($order)
    {
        return $this->_orderService->getSourceInternalNumber($order);
    }

    /**
     * Логистический статус заказа (Обработан, Не обработан, Ожидается взвешивание, ...).
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getLogisticsStatus($order)
    {
        return $this->_orderService->getLogisticsStatusTitle($order);
    }

    /**
     * Приоритет логистического статуса заказа.
     * Необходим для сортировки списка заказов.
     *
     * @param mixed $order Объект "Заказ"
     * @return int
     */
    public function getLogisticsStatusPriority($order)
    {
        $logisticsStatus = $this->_orderService->getLogisticsStatus($order);

        return $this->_orderStatusService->getLogisticsStatusPriority($logisticsStatus);
    }

    /**
     * Проверка, закончил ли логист работу с заказом
     *
     * @param mixed $order Объект "Заказ"
     * @return bool
     */
    public function isHandled($order)
    {
        return $order->logistics_status == Order::LOGISTICS_STATUS_HANDLED;
    }

    /**
     * Наименование транспортной компании
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getTransportCompanyName($order)
    {
        return $this->_orderService->getTransportCompanyName($order);
    }

    /**
     * Весовой статус (Со взвешиванием / Без взвешивания)
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getWeightStatus($order)
    {
        return $this->_orderService->getWeightStatusTitle($order);
    }

    /**
     * Назначено ли взвешивание данному заказу
     *
     * @param mixed $order Объект "Заказ"
     * @return bool
     */
    public function hasWeighing($order)
    {
        return $this->_orderService->hasWeighing($order);
    }

    /**
     * ФИО заказчика
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getClientName($order)
    {
        return $this->_orderService->getClientName($order);
    }

    /**
     * Телефон заказчика
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getClientPhone($order)
    {
        return $this->_orderService->getClientPhone($order);
    }

    /**
     * Адрес заказчика
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getClientAddress($order)
    {
        return $this->_orderService->getClientAddress($order);
    }

    /**
     * Сумма заказа.
     * Равняется сумме цен элементов данного заказа.
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getItemsPrice($order)
    {
        return $this->_orderPriceService->getItemsPrice($order);
    }

    /**
     * Статус оплаты товара (Не оплачено, Оплачено, Наложка)
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getPaymentStatus($order)
    {
        return $this->_orderService->getPaymentStatusTitle($order);
    }

    /**
     * Тип доставки (Платная, Бесплатная)
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getDeliveryType($order)
    {
        return $this->_orderService->getDeliveryTypeTitle($order);
    }

    /**
     * Стоимость доставки
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getDeliveryPrice($order)
    {
        return $this->_orderService->getDeliveryPrice($order);
    }

    /**
     * Статус оплаты доставки (Не оплачено, Оплачено, Наложка)
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getDeliveryStatus($order)
    {
        return $this->_orderService->getDeliveryStatusTitle($order);
    }

    /**
     * Общая сумма, полученная за весь заказ наложенным платежом
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getCodTotalPrice($order)
    {
        return $this->_orderPriceService->getCodTotalPrice($order);
    }

    /**
     * Общая сумма, полученная за товар наложенным платежом
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getCodItemsPrice($order)
    {
        return $this->_orderPriceService->getCodItemsPrice($order);
    }

    /**
     * Общая сумма, полученная за доставку наложенным платежом
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getCodDeliveryPrice($order)
    {
        return $this->_orderPriceService->getCodDeliveryPrice($order);
    }

    /**
     * Комментарий к заказу
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getComment($order)
    {
        return $this->_orderService->getComment($order);
    }

    /**
     * Дата отгрузки заказа (timestamp)
     *
     * @param mixed $order Объект "Заказ"
     * @return int|null
     */
    public function getShipmentDate($order)
    {
        return $this->_orderService->getShipmentDate($order);
    }

    /**
     * Трэк-код заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getTrackCode($order)
    {
        return $this->_orderService->getTrackCode($order);
    }

    /**
     * Вес заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getWeight($order)
    {
        return $this->_orderService->getWeight($order);
    }
}