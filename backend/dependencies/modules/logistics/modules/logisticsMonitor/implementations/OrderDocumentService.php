<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentService;

/**
 * Реализация сервиса, предоставляющего модулю "Монитор логистики" доступ к свойствам документа заказа.
 *
 * Class OrderDocumentService
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrderDocumentService extends \backend\modules\logistics\models\orderDocument\OrderDocumentService implements IOrderDocumentService {}