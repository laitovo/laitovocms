<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\models\orderEntry\OrderEntryService;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemService;

/**
 * Реализация сервиса, предоставляющего модулю "Монитор логистики" доступ к свойствам элемента заказа.
 *
 * Class OrderItemService
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrderItemService implements IOrderItemService
{
    /**
     * @var OrderEntryService $_orderService Сервис для работы со свойствами элементов заказа
     */
    private $_orderEntryService;

    /**
     * В конструкторе инициализируем необходимые сервисы приложения
     *
     * OrderService constructor.
     */
    public function __construct()
    {
        $this->_orderEntryService = new OrderEntryService();
    }

    /**
     * Артикул
     *
     * @param mixed $orderItem Объект "Элемент заказа"
     * @return string|null
     */
    public function getArticle($orderItem)
    {
        return $this->_orderEntryService->getArticle($orderItem);
    }

    /**
     * Наименование
     *
     * @param mixed $orderItem Объект "Элемент заказа"
     * @return string|null
     */
    public function getName($orderItem)
    {
        return $this->_orderEntryService->getName($orderItem);
    }


    /**
     * Габариты
     *
     * @param mixed $orderItem Объект "Элемент заказа"
     * @return string|null
     */
    public function getSize($orderItem)
    {
        return '50X50X50'; //test
    }

    /**
     * Цена
     *
     * @param mixed $orderItem Объект "Элемент заказа"
     * @return double|null
     */
    public function getPrice($orderItem)
    {
        return $this->_orderEntryService->getPrice($orderItem);
    }
}