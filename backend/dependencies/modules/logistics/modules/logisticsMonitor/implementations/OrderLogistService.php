<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\components\fileManager\FileManager;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\order\OrderRepository;
use backend\modules\logistics\models\order\OrderService;
use backend\modules\logistics\models\order\OrderStatusService;
use backend\modules\logistics\models\orderDocument\OrderDocument;
use backend\modules\logistics\models\orderDocument\OrderDocumentFactory;
use backend\modules\logistics\models\orderDocument\OrderDocumentService;
use backend\modules\logistics\models\orderDocument\OrderDocumentRepository;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderLogistService;
use Exception;
use Yii;

/**
 * Реализация сервиса, предоставляющего модулю "Монитор логистики" функционал для обработки заказов логистом.
 *
 * Class OrderLogistService
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrderLogistService implements IOrderLogistService
{
    /**
     * @var OrderRepository Сервис для поиска, сохранения и удаления заказов
     */
    private $_orderRepository;
    /**
     * @var OrderService Сервис для работы со свойствами заказа
     */
    private $_orderService;
    /**
     * @var OrderStatusService Сервис для работы со статусами заказа
     */
    private $_orderStatusService;
    /**
     * @var FileManager Сервис для создания документов
     */
    private $_orderDocumentFactory;
    /**
     * @var OrderDocumentService Сервис для работы со свойствами документа
     */
    private $_orderDocumentService;
    /**
     * @var OrderDocumentRepository Сервис для поиска, сохранения и удаления документов
     */
    private $_orderDocumentRepository;
    /**
     * @var mixed Сервис для работы с файлами
     */
    private $_fileManager;

    /**
     * В конструкторе инициализируем необходимые сервисы приложения
     *
     * OrderLogistService constructor.
     */
    public function __construct()
    {
        $this->_orderRepository = new OrderRepository();
        $this->_orderService = new OrderService();
        $this->_orderStatusService = new OrderStatusService();
        $this->_orderDocumentFactory = new OrderDocumentFactory();
        $this->_orderDocumentService = new OrderDocumentService();
        $this->_orderDocumentRepository = new OrderDocumentRepository();
        $this->_fileManager = Yii::$app->fileManager;
    }

    /**
     * Пометка что логист завершил работу над заказом
     *
     * @param int $orderId Идентификатор заказа
     * @return bool Результат операции (успешно или нет)
     */
    public function handle($orderId)
    {
        $order = $this->_findOrder($orderId);
        $result = $this->_orderStatusService->handle($order);

        return $result;
    }

    /**
     * Пометка (или отмена пометки) для склада (кладовщика) что заказ должен быть взвешен
     *
     * @param int $orderId Идентификатор заказа
     * @return array|bool
     */
    public function updateWeightStatus($orderId)
    {
        $order = $this->_findOrder($orderId);
        $result = $this->_orderStatusService->updateWeightStatus($order);
        if (!$result) {
            return false;
        }
        $has_weighing = $this->_orderService->hasWeighing($order);
        $weight_status = $this->_orderService->getWeightStatusTitle($order);

        return [
            'has_weighing' => $has_weighing,
            'weight_status' => $weight_status,
        ];
    }

    /**
     * Сохранение даты отгрузки заказа
     *
     * @param int $orderId Идентификатор заказа
     * @param null|string $shipment_date Дата отгрузки
     * @return bool Результат операции (успешно или нет)
     */
    public function setShipmentDate($orderId, $shipment_date)
    {
        $order = $this->_findOrder($orderId);
        if ($shipment_date) {
            $shipment_date = strtotime($shipment_date);
        }
        $result = $this->_orderService->setShipmentDate($order, $shipment_date);

        return $result;
    }

    /**
     * Сохранение трэк-кода заказа
     *
     * @param int $orderId Идентификатор заказа
     * @param null|string $track_code Трэк-код
     * @return bool Результат операции (успешно или нет)
     */
    public function setTrackCode($orderId, $track_code)
    {
        $order = $this->_findOrder($orderId);
        $result = $this->_orderService->setTrackCode($order, $track_code);

        return $result;
    }

    /**
     * Сохранение веса заказа.
     * Может повлечь за собой смену логистического статуса.
     *
     * @param int $orderId Идентификатор заказа
     * @param null|string $weight Вес
     * @return bool Результат операции (успешно или нет)
     */
    public function setWeight($orderId, $weight)
    {
        $order = $this->_findOrder($orderId);
        if (!$this->_orderService->setWeight($order, $weight)) {
            return false;
        }
        if ($weight) {
            $result = $this->_orderStatusService->applyStatusWeighed($order);
        } else {
            $result = $this->_orderStatusService->applyStatusWeightForWeighing($order);
        }

        return $result;
    }

    /**
     * Загрузка нового документа
     *
     * @param int $orderId Идентификатор заказа
     * @param string $fieldName Поле формы, из которого берём файл
     * @return array|bool
     */
    public function uploadDocument($orderId, $fieldName)
    {
        $order = $this->_findOrder($orderId);
        $orderDocument = $this->_orderDocumentFactory->create();
        $this->_orderDocumentService->setOrderId($orderDocument, $this->_orderService->getId($order));
        if (!$this->_orderDocumentRepository->save($orderDocument, $fieldName)) {
            return false;
        }

        return [
            'id' => $this->_orderDocumentService->getId($orderDocument),
        ];
    }

    /**
     * Удаление документа
     *
     * @param int $orderDocumentId Идентификатор документа
     * @return bool Результат операции (успешно или нет)
     */
    public function deleteDocument($orderDocumentId)
    {
        $orderDocument = $this->_findOrderDocument($orderDocumentId);

        return $this->_orderDocumentRepository->delete($orderDocument);
    }

    /**
     * Функция возвращает документ по переданному идентификатору
     *
     * @param int $orderId Идентификатор заказа
     * @return Order Объект "Заказ"
     * @throws Exception
     */
    private function _findOrder($orderId)
    {
        if (!($order = $this->_orderRepository->getOrderById($orderId))) {
            throw new Exception('Заказ не найден');
        }

        return $order;
    }

    /**
     * Функция возвращает документ по переданному идентификатору
     *
     * @param int $orderDocumentId Идентификатор документа
     * @return OrderDocument Объект "Документ заказа"
     * @throws Exception
     */
    private function _findOrderDocument($orderDocumentId)
    {
        if (!($orderDocument = $this->_orderDocumentRepository->getOrderDocumentById($orderDocumentId))) {
            throw new Exception('Документ не найден');
        }

        return $orderDocument;
    }
}