<?php

namespace backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentsProvider;

/**
 * Реализация DTO-провайдера документов заказа для модуля "Монитор логистики".
 *
 * Class OrderDocumentsProvider
 * @package backend\dependencies\modules\logistics\modules\logisticsMonitor\implementations
 */
class OrderDocumentsProvider extends \backend\modules\logistics\modules\logisticsMonitor\models\OrderDocumentsProvider implements IOrderDocumentsProvider {}