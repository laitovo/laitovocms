<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\logistics\modules\implementationMonitor\interfaces\IPackUpnService;
use backend\modules\logistics\modules\implementationMonitor\models\PackUpn;

class PackUpnService extends PackUpn implements IPackUpnService
{
    public function packForShipment($shipmentId, $barcode)
    {
        return parent::packForShipment($shipmentId, $barcode);
    }
}