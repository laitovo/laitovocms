<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService;
use common\models\laitovo\ErpOrder;

class OrderService implements IOrderService
{
    public function getNumber($order)
    {
        return $order->id;
    }

    public function getSourceNumber($order)
    {
        return $order->source_innumber;
    }

    public function getFactShipmentDate($order)
    {
        return time();
    }

    public function getPlanShipmentDate($order)
    {
        return time();
    }

    public function getPackageDate($order)
    {
        return time();
    }

    public function checkAsProcessed($order)
    {
        $erpOrder = ErpOrder::find()->where(['id' => $order->source_innumber])->one();
        $orderStatus = ErpNaryad::find()->where(['order_id' => $erpOrder->id])->exists() ? ErpOrder::STATUS_IN_WORK : ErpOrder::STATUS_READY;
        $order->processed = true;
        $erpOrder->status = $orderStatus;
        $erpOrder->is_new_scheme = true;
        return ($erpOrder->save() && $order->save());
    }

    public function getCommand($order)
    {
        return $order->ready;
    }

    public function shipOrder($order)
    {
        if ($order) {
            $order->shipped = true;
            $order->save();
        }

        return $order->shipped ? true : false;
    }

    public function getRegistryId($order)
    {
        return $order->reestrId;
    }

    public function getManager($order)
    {
        return $order->manager;
    }

    public function getDelivery($order)
    {
        return $order->delivery;
    }

    public function getAddress($order)
    {
        return $order->address;
    }

    public function getClientName($order)
    {
        return $order->username;
    }

    public function getExportClientName($order)
    {
        return $order->export_username;
    }

    public function getClientPhone($order)
    {
        return $order->userphone;
    }

    public function getClientEmail($order)
    {
        return $order->useremail;
    }

    public function getClientCategory($order)
    {
        return $order->category;
    }

    public function getExportClientCategory($order)
    {
        return $order->export_type;
    }

    public function getCreatedAt($order)
    {
        return $order->created_at;
    }

    public function getInnerComment($order)
    {
        return $order->innercomment;
    }

    public function isNeedTtn($order)
    {
        return $order->is_need_ttn;
    }

    public function isCOD($order)
    {
        return $order->isCOD;
    }

    public function isOrderPacked($order) {
        return $order->packed ? true : false;
    }

    public function isHandled($order)
    {
        return $order->logistics_status == Order::LOGISTICS_STATUS_HANDLED;
    }

    public function isOnWeighing($order)
    {
        return $order->logistics_status == Order::LOGISTICS_STATUS_WAIT_FOR_WEIGHING;
    }
}