<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\logistics\modules\implementationMonitor\interfaces\ITransaction;

class Transaction implements ITransaction
{
    private $_transaction;

    public function begin()
    {
        $this->_transaction = \Yii::$app->db->beginTransaction();
    }

    public function commit()
    {
        $this->_transaction->commit();
        $this->_transaction = null;

    }

    public function rollBack()
    {
        $this->_transaction->rollBack();
        $this->_transaction = null;
    }
}
