<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\logistics\modules\implementationMonitor\interfaces\IMonitorData;

class MonitorData extends \backend\modules\logistics\modules\implementationMonitor\models\MonitorData implements IMonitorData
{
    public function getData()
    {
        return parent::getData();
    }

    public function getDataForOne($id)
    {
        return parent::getDataForOne($id);
    }


}