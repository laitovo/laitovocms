<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpOrder;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository;
use yii\helpers\ArrayHelper;

class OrdersRepository extends Order implements IOrdersRepository
{
    public function getIncomingOrders()
    {
        $team =  \Yii::$app->team->id;
        return self::find()->where(['team_id' => $team])->andWhere(['processed' => true])->andWhere(['ready' => false])->all();
    }

    public function getPackageOrders()
    {
        $team =  \Yii::$app->team->id;
        return self::find()->where(['team_id' => $team])->andWhere(['ready' => true])->all();
    }

    public function getPackageReestrOrders()
    {
        $team =  \Yii::$app->team->id;
        return self::find()->where(['team_id' => $team])->andWhere(['ready' => true])->andWhere(['reestrId' => null])->all();
    }


    public function getReadyToShipmentOrders()
    {
        return self::find()->all();
    }

    public function getLastOrder()
    {
        $team =  \Yii::$app->team->id;
        $count = Order::find()->where(['team_id' => $team])->andWhere(['and',['processed' => true],['shipped' => null]])->count();

        if (2 <= $count) return null;

        $orders = ErpOrder::find()->select('id')->where(['status'=> null])->asArray()->all();
        //callback for array_map
        $func = function($value) {
            return $value['id'];
        };
        $orders = array_map($func,$orders);

        $logistOrder = Order::find()->where(['in','source_innumber',$orders])->orderBy(['id' => SORT_ASC])->one();

        return $logistOrder;
    }

    public function getOrderById($id)
    {
        return self::findOne($id);
    }

    public function getOrders()
    {
        //Нам надо получить заявки, которые еще не упакованы
        $team =  \Yii::$app->team->id;
        return self::find()->where(['team_id' => $team])
            ->where(['or',
            ['and',['processed' => true],['ready' => false],['or',['packed' => null],['packed' => false]]],
            ['and',['processed' => true],['ready' => true],['or',['packed' => null],['packed' => false]]]
        ])->all();
    }

    public function getOrdersJournal($searchParams = null)
    {
        //Нам надо получить заявки, которые еще не упакованы
        $team =  \Yii::$app->team->id;
        $search = $searchParams['search'] ?? null;
        $onlyActual = $searchParams['onlyActual'] ?? null;
//        $query = self::find()->where(['team_id' => $team])
//            ->where(['and',['processed' => true],['shipped' => null]]);
        $query = self::find()->where(['team_id' => $team]);
        if (!empty($search))
            $query->andWhere(['like','source_innumber','%'.$search.'%', false]);
        if ($onlyActual && $search == null) {
            //Найдем  все отгруженные заказы
            $shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
            $shipments = $shipmentManager->findWhereColumn('id',['or',['logisticsStatus' => 'shipped'],['logisticsStatus' => 'packed'],['logisticsStatus' => 'handled']]);
            $maxOrderEntryId = OrderEntry::find()->max('id');
            $orders = OrderEntry::find()
                ->select('order_id')
                ->where(['>','id', $maxOrderEntryId - 10000])
                ->andWhere('order_id is not NULL')
                ->andWhere(['or','shipment_id not IN (' .implode(',',$shipments) .')','shipment_id is NULL'] )
                ->groupBy('order_id')->column();
            $query->andWhere('id IN (' .implode(',',$orders) .')');
            $query->andWhere(['and',['processed' => true],['shipped' => null]]);
            $query->orderBy(['created_at' => SORT_DESC])
            ->limit(50);
        } else {
            $query->andWhere(['processed' => true]);
            $query->orderBy(['created_at' => SORT_DESC])->limit(50);
        }

        return $query->all();
    }


    public function getOrdersJournalDangerous()
    {
        $team =  \Yii::$app->team->id;

        $query = self::find()->where(['team_id' => $team])->select('source_innumber');
            //Найдем  все отгруженные заказы
        $shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $shipments = $shipmentManager->findWhereColumn('id',['logisticsStatus' => 'not_handled']);
        $errorOrderEntries = [];
        $maxOrderEntryId = OrderEntry::find()->max('id');
        $orderEntries = OrderEntry::find()
            ->select('id')
            ->where(['>','id', $maxOrderEntryId - 3000])
            ->andWhere(['is_deleted' => NULL])
            ->andWhere(['or',['shipment_id' => $shipments],['shipment_id' => NULL]])
            ->column();
        $logisticsNaryads = Naryad::find()
            ->where(['reserved_id' => $orderEntries])
            ->select('id')
            ->indexBy('reserved_id')
            ->column();
        //Сначала находим ошибки, по которым не привязалось ни одного логистического наряда
        $flipLogisticsNaryads = array_flip($logisticsNaryads);
        $errorOrderEntries = ArrayHelper::merge($errorOrderEntries, array_diff($orderEntries, $flipLogisticsNaryads));
        $erpNaryads = ErpNaryad::find()->where(['logist_id' => $logisticsNaryads])->andWhere(['distributed' => false])->select('logist_id')->column();
        $storageState = StorageState::find()->where(['upn_id' => $logisticsNaryads])->select('upn_id')->column();
        $errorLogisticsNaryads = array_diff($logisticsNaryads, $erpNaryads, $storageState);
        $flipErrorLogisticsNaryads = array_flip($errorLogisticsNaryads);
        $errorOrderEntries = ArrayHelper::merge($errorOrderEntries, $flipErrorLogisticsNaryads);
        $orders = OrderEntry::find()
            ->select('order_id')
            ->where(['id' => $errorOrderEntries])
            ->distinct()
            ->column();
        if (empty($orders))
            return [];
        $query->andWhere('id IN (' .implode(',',$orders) .')');
        $query->andWhere(['and',['processed' => true],['shipped' => null]]);
        $query->orderBy(['created_at' => SORT_DESC]);

        return $query->column();
    }


    public function getOrdersStorage()
    {
        //Нам надо получить заявки, которые еще не упакованы
        $team =  \Yii::$app->team->id;
        return self::find()->where(['team_id' => $team])
            ->where(['or',
                ['and',['processed' => true],['ready' => true],['or',['packed' => null],['packed' => false]]]
            ])->all();
    }

    public function getOrdersShipmentCount()
    {
        $team =  \Yii::$app->team->id;
        return self::find()->where(['team_id' => $team])
            ->where(['or',
                ['and',['processed' => true],['ready' => true],['packed' => 1],['or',['shipped' => null],['shipped' => false]]]
            ])->count();
    }

    public function getOrdersByReestrId($id)
    {
        $team =  \Yii::$app->team->id;
        return self::find()->where(['team_id' => $team])->andWhere(['reestrId' => $id])->all();
    }

    public function getOrderWithReestr()
    {
        $team =  \Yii::$app->team->id;
        return self::find()->where(['team_id' => $team])->andWhere(['and',['is not','reestrId', null],['!=','reestrId', '']])->all();
    }
}