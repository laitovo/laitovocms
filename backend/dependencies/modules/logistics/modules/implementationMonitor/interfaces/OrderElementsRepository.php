<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository;

class OrderElementsRepository extends OrderEntry implements IOrderElementsRepository
{
    public function getElementsForOrder($order)
    {
        $id = $order->id;
        return self::find()->where(['order_id' => $id])->all();
    }

    public function getElementsForOrderNotDeleted($order)
    {
        $id = $order->id;
        return self::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['order_id' => $id]
        ])->all();
    }
}