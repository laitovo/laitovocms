<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnService;
use Yii;

class UpnService implements IUpnService
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager    = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function getNumber($upn)
    {
        if ($upn instanceof Naryad)
            return $upn->id;
        else
            return null;
    }

    public function getParent($element)
    {
        if ($element instanceof Naryad)
            return $element->pid;
        else
            return null;
    }

    public function isReadyToPackage($upn)
    {
        //Проверка на переданное значение
        if ($upn !== null && !($upn instanceof Naryad))
            throw new Exception('Переданное значение должно быть элементов логистической заявки');

        if ($upn === null) return null;

        //Если текущий upn находится на складе - тогда пишим склад
        $state = StorageState::find()->where(['upn_id' => $upn->id])->one();

        return ( $state && ($storage=$state->storage) != null && ($storage->type != Storage::TYPE_SUMP));
    }

    public function getId($upn)
    {
        if ($upn instanceof Naryad)
            return $upn->id;
        else
            return null;
    }

    /**
     * @param object $upn
     * @return bool
     * @throws \yii\db\Exception
     */
    public function pack($upn)
    {
        if ($upn instanceof Naryad) {
            if ($upn->isParent) {
                $transaction = \Yii::$app->db->beginTransaction();
                /**
                 * @var $child \common\models\logistics\Naryad
                 */
                foreach ($upn->children as $child)
                {
                    if ($child->packed) {
                        $transaction->rollBack();
                        return false;
                    }

                    if (!($state = $child->storageState)) {
                        $transaction->rollBack();
                        return false;
                    }

                    $child->packed = true;
                    $state->literal = ":::" . $state->literal;
                    if (!$child->save() || !$state->save()) {
                        $transaction->rollBack();
                        return false;
                    };
                }
                $transaction->commit();
                return true;
            }else{
                $transaction = \Yii::$app->db->beginTransaction();

                if ($upn->packed) {
                    $transaction->rollBack();
                    return false;
                }

                if (!($state = $upn->storageState)) {
                    $transaction->rollBack();
                    return false;
                }

                $upn->packed = true;
                $state->literal = ":::" . $state->literal;
                if (!$upn->save() || !$state->save()) {
                    $transaction->rollBack();
                    return false;
                };

                $transaction->commit();
                return true;
            }
        }
        else
            return false;
    }

    public function getLocation($upn)
    {
        //Проверка на переданное значение
        if ($upn !== null && !($upn instanceof Naryad))
            throw new Exception('Переданное значение должно быть элементов логистической заявки');

        if ($upn === null) return null;

        if ( ($orderEntry = $upn->orderEntry) && ($shipmentId = $orderEntry->shipment_id) && ($shipment = $this->_shipmentManager->findById($shipmentId)) ) {
            if ($this->_shipmentManager->isHandled($shipment)) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipmentId]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipmentId .' ]. Ожидается упаковка : ' . date('d.m.Y H:i:s', $this->_shipmentManager->getHandledAt($shipment)) . '</a>';
            } elseif($this->_shipmentManager->isPacked($shipment)) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipmentId]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipmentId .' ]. Упакован, ожидается отгрузка : ' . date('d.m.Y H:i:s', $this->_shipmentManager->getPackedAt($shipment)) . '</a>';
            } elseif($this->_shipmentManager->isShipped($shipment)) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipmentId]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipmentId .' ]. Отгружен : ' . date('d.m.Y H:i:s', $this->_shipmentManager->getShippedAt($shipment)) . '</a>';
            }
        }

        $state = StorageState::find()->where(['upn_id' => $upn->id])->one();
        $naryad = ErpNaryad::find()->where(['logist_id' => $upn->id])->one();

        //Если текущий upn находится на складе - тогда пишим склад
        if ($state) {
            if ($state->storage) {
                $distributed_date = empty($naryad) ? 0 : $naryad->distributed_date;
                $reserved_at_date = empty($upn) ? 0 : $upn->reserved_at;
                $reservedAt = max($distributed_date, $reserved_at_date);
                $reservedAt = $reservedAt ? date('d.m.Y H:i:s', $reservedAt) : null;
                $literal = $state->literal;
                $str =  ($literal && $reservedAt) ? ($literal . ' : ' . $reservedAt) : $literal;
                if ($state->storage->type == Storage::TYPE_SUMP) {
                    return $str ." <br> Отстойник. Ожидает проверки ОТК";
                }
                return $str;
            } else {
                return 'Склад';
            }
        }
        //Если теккущий upn находиться на производстве - пишем локацию
        if ($naryad)
        {
            if ($naryad->status == ErpNaryad::STATUS_READY) return 'Диспетчер-Склад : ' . date('d.m.Y H:i:s', $naryad->updated_at);
            return @$naryad->location && !$naryad->locationstart ? @$naryad->location->name : 'Диспетчер : '.@$naryad->locationstart->name;
        }

        return 'Ожидется обработка';
    }

    public function getLocationStorage($upn)
    {
        //Проверка на переданное значение
        if ($upn !== null && !($upn instanceof Naryad))
            throw new Exception('Переданное значение должно быть элементов логистической заявки');

        if ($upn === null) return null;

        //Если текущий upn находится на складе - тогда пишим склад
        $state = StorageState::find()->where(['upn_id' => $upn->id])->one();
        if ($state) return $state->storage ? ($state->literal) : 'Склад';
        //Если теккущий upn находиться на производстве - пишем локацию
        $naryad = ErpNaryad::find()->where(['logist_id' => $upn->id])->one();
        if ($naryad)
        {
            if ($naryad->status == ErpNaryad::STATUS_READY) return 'Диспетчер-Склад';
            return @$naryad->location && !$naryad->locationstart ? @$naryad->location->name : 'Диспетчер : '.@$naryad->locationstart->name;
        }

        return 'Ожидется обработка';
    }

    public function isPacked($upn)
    {
        if ($upn instanceof Naryad)
        {
            if ($upn->packed) return true;
        }
        else
            return false;
    }

    public function shipUpn($upn)
    {
        if ($upn && $upn->storageState) {
            $upn->storageState->delete();
        }
    }
}