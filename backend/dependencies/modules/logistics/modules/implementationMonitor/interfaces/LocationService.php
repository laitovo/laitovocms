<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\laitovo\models\ErpLocation;
use backend\modules\logistics\modules\implementationMonitor\interfaces\ILocationService;

class LocationService implements ILocationService
{
    public function getTitle($location)
    {
        if ($location instanceof ErpLocation)
            return $location->name;
        else
            return null;
    }
}