<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;


use backend\modules\logistics\modules\implementationMonitor\interfaces\IReadyReestr;

class ReadyReestr extends \backend\modules\logistics\modules\implementationMonitor\models\ReadyReestr implements IReadyReestr
{
    public function getData()
    {
        return parent::getData();
    }

    public function getPrintData($ids = [])
    {
        return parent::getPrintData($ids);
    }
}
