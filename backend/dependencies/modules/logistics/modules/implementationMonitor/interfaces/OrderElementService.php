<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\helpers\ArticleHelper;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementService;
use common\models\logistics\StorageState;
use core\logic\CheckStoragePosition;

class OrderElementService implements IOrderElementService
{
    public function getArticle($element)
    {
        return $element->article;
    }

    public function getTitle($element)
    {
        return $element->name;
    }

    public function getItemText($element)
    {
        return $element->itemtext;
    }

    public function getReservedUpn($element)
    {
        //Проверка на переданное значение
        if ($element !== null && !($element instanceof OrderEntry))
            throw new Exception('Переданное значение должно быть элементов логистической заявки');

        if ($element === null) return null;

        $upn = Naryad::find()->where(['reserved_id' => $element->id])->one();

        return $upn ? $upn : null;
    }

    /**
     * @param $element
     * @return bool|mixed
     * @throws \Exception
     */
    public function isStoragePosition($element)
    {

        if (!($element instanceof OrderEntry)) return false;

        return CheckStoragePosition::isStoragePosition($element);

    }

    public function isCustom($element)
    {
        return $element->is_custom;
    }

    /**
     * Принимает в качестве значения объект элемента заказа и возвращает наименоване для данного элемента
     * @param $element
     * @return mixed
     */
    public function getInvoiceTitle($element) {
        return ArticleHelper::getInvoiceTitle($element->article);
    }

    public function getCar($element)
    {
        return $element->car ? $element->car : '';
    }

    public function getSourceBarcode($element)
    {
        return $element->order_barcode;
    }

    public function getOrderRow($element)
    {
        $str = $element->order_barcode;
        $str = str_ireplace('B','',$str);
        $str = str_ireplace('D','/',$str);
        return $str;
    }

    public function getOrderId($element)
    {
        return $element->order_id;
    }

    public function getDestroyer($element)
    {
        return \common\models\User::findOne($element->deleted_by);
    }
}