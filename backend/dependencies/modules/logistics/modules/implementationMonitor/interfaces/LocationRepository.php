<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\laitovo\models\ErpLocation;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\modules\implementationMonitor\interfaces\ILocationRepository;

class LocationRepository extends ErpLocation implements ILocationRepository
{
    public function getLocationWhereUpn($upn)
    {
        //Проверка на переданное значение
        if ($upn !== null && !($upn instanceof Naryad))
            throw new Exception('Переданное значение должно быть элементов логистической заявки');

        if ($upn === null) return null;

        //Если текущий upn находится на складе - тогда пишим склад
        $state = StorageState::find()->where(['upn_id' => $upn->id])->one();
        if ($state) return $state->storage ? $state->storage->title : 'Склад';
        //Если теккущий upn находиться на производстве - пишем локацию

        return null;
    }
}