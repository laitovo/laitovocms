<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnRepository;
use yii\base\Exception;

class UpnRepository extends StorageState implements IUpnRepository
{
    public function getUpnForElement($element)
    {
        //Проверка на переданное значение
        if ($element !== null && !($element instanceof OrderEntry))
            throw new Exception('Переданное значение должно быть элементов логистической заявки');

        if ($element === null) return null;

        $upn = Naryad::find()->where(['reserved_id' => $element->id])->one();

        return $upn ? $upn : null;
    }

    public function getUpnByBarcode($barcode)
    {
        return Naryad::find()->where(['barcode' => $barcode])->one();
    }
}