<?php
namespace backend\dependencies\modules\logistics\modules\implementationMonitor\interfaces;

use backend\modules\logistics\modules\implementationMonitor\interfaces\IProcessService;

class ProcessService extends \backend\modules\logistics\modules\implementationMonitor\models\ProcessService implements IProcessService
{
    public function handleOrder()
    {
        return parent::handleOrder();
    }

    public function handleOrderById($id)
    {
        return parent::handleOrderById($id);
    }
}
