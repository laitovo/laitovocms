<?php

namespace backend\controllers;

use Yii;
use backend\models\SettingsForm;
use backend\models\EditPasswordForm;
use backend\models\TeamForm;
use backend\models\TeamFields;
use backend\models\InviteForm;
use common\models\team\Module;
use common\models\user\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * User controller
 */
class SettingsController extends Controller
{
    private $fields=[
        'order'=>[
            '{{%order}}'=>'Заказ',
            '{{%team}}'=>'Контрагент',
            '{{%organization}}'=>'Организация',
            '{{%category}}'=>'Категории',
            '{{%product}}'=>'Товары',
        ],
        'laitovo'=>[
            '{{%laitovo_cars}}'=>'Автомобили',
            '{{%laitovo_erp_order}}'=>'Заказы',
            '1{{%laitovo_config}}'=>'Настройки: автомобили',
            '2{{%laitovo_config}}'=>'Настройки: производство',
        ],
    ];

    public $defaultAction = 'user';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-team' => ['post'],
                    'unlink-team' => ['post'],
                ],
            ],
        ];
    }

    public function actionUser()
    {
        $model=new SettingsForm();

        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));

            return $this->refresh();
        }

        return $this->render('user', [
            'model' => $model,
        ]);
    }

    public function actionPassword()
    {
        $model = new EditPasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Пароль изменен.'));
            return $this->refresh();
        } else {
            return $this->render('password', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateTeam()
    {
        $model = new TeamForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->team->link('users', $model->getUser());
            Yii::$app->session->setFlash('success', Yii::t('app', 'Добро пожаловать в команду "{team}".',['team'=>$model->team->name]));
            Yii::$app->team->setTeam($model->team->id);
            return $this->redirect(['team', 'id' => $model->team->id]);
        } else {
            return $this->render('create-team', [
                'model' => $model,
            ]);
        }
    }

    public function actionTeam($id)
    {
        $model = new TeamForm($id);
        $invite = new InviteForm($id);
        Yii::$app->team->setTeam($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
            return $this->refresh();
        } elseif ($invite->load(Yii::$app->request->post()) && $invite->sendInvite()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Приглашение отправлено.'));
            return $this->refresh();
        } else {

            $modules=Module::find()->with('modules')->where(['parent'=>null])->all();

            $query=User::find()->joinWith('teams')->where(['team.id'=>$id]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);

            $validator = new \yii\validators\StringValidator();

            if ($validator->validate(Yii::$app->request->get('search'), $error)) {

                $query->andFilterWhere(['or', 
                    ['like', 'user.name', Yii::$app->request->get('search')],
                    ['like', 'user.username', Yii::$app->request->get('search')],
                    ['like', 'user.phone', Yii::$app->request->get('search')],
                    ['like', 'user.email', Yii::$app->request->get('search')],
                ]);
            }

            return $this->render('team', [
                'model' => $model,
                'invite' => $invite,
                'modules' => $modules,
                'dataProvider' => $dataProvider,
                'fields' => $this->fields,
            ]);
        }
    }

    public function actionDeleteTeam($id)
    {
        $model = new TeamForm($id);
        $model->delete();
        return $this->goHome();
    }

    public function actionUnlinkTeam($id,$user)
    {
        $model = new TeamForm($id);
        $model->unlink($user);
        return $this->redirect(['team', 'id' => $model->team->id]);
    }

    public function actionFields($team,$module,$table)
    {
        $model = new TeamFields($team, $module, $table, $this->fields);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
            return $this->redirect(['team','id'=>$team]);
        } else {

            return $this->render('fields', [
                'model' => $model,
                'field' => $this->fields[$module][$table],
            ]);

        }

    }

    public function actionCloseSidebarMenu()
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        if (Yii::$app->session->get('close-sidebar-menu')){
            Yii::$app->session->set('close-sidebar-menu',false);
        } else {
            Yii::$app->session->set('close-sidebar-menu',true);
        }

    }
}
