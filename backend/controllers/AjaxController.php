<?php

namespace backend\controllers;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpLocationWorkplace;
use backend\modules\laitovo\models\ErpManning;
use backend\modules\laitovo\models\ErpPosition;
use backend\modules\logistics\models\Naryad;
use common\models\laitovo\ErpUserPosition;
use common\models\laitovo\Weekend;
use core\logic\DistributionProductionLiterals;
use core\models\article\Article;
use core\models\shipment\Shipment;
use core\services\SProductionLiteral;
use Yii;
use common\models\Region;
use common\models\order\Price;
use common\models\team\Team;
use common\models\User;
use common\models\team\Organization;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use backend\modules\laitovo\models\ErpUser;
use common\models\laitovo\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\LaitovoReworkAct;
use common\models\laitovo\MaterialLocation;
use common\models\laitovo\MaterialParameters;
use common\models\laitovo\ErpProductType;
use common\models\laitovo\Cars;
use backend\modules\laitovo\models\ErpJobType;
use common\models\laitovo\Config;
use common\models\laitovo\MaterialProductionScheme;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;
use backend\modules\laitovo\models\ErpLocationWorkplacePlan;
use backend\modules\laitovo\models\MainProductInfo;
use backend\modules\logistics\models\AutomaticOrderMonitor;

class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
        return parent::beforeAction($action);
    }

    public function actionOrganizations($team)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $team = Team::find()->where(['id' => $team])->one();
        if ($team) {
            return ArrayHelper::map($team->getOrganizations()->asArray()->all(), 'id', 'name');
        }

        return [];
    }

    /**
     * @param $shipmentId
     * @param $value
     * @return mixed
     */
    public function actionShipmentPrintDocs($shipmentId,$value)
    {
        $response['status'] = 'error';
        $response['message'] = 'Что то пошло не так, обратитесь к IT!';
        $response['value'] = $value;

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $shipment = Shipment::findOne($shipmentId);
        if (!$shipment)
            return $response;

        $shipment->documentsArePrinted = $value;
        if ($shipment->save()) {
            $response['status'] = 'success';
            $response['message'] = $value == 1 ? 'Документы успешно распечатаны' : 'Отметка о распечатанных документах снята';
        }

        return $response;
    }

    public function actionTeam($team)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $team = Team::find()->where(['id' => $team])->one();
        if ($team) {
            $json = Json::decode($team->json ? $team->json : '{}');

            if (isset($json['country']) && ($region = Region::find()->where(['id' => $json['country']])->one()) !== null) {
                $regions = ArrayHelper::map($region->getRegions()->asArray()->all(), 'id', 'name');
            }

            if (isset($json['region']) && ($region = Region::find()->where(['id' => $json['region']])->one()) !== null) {
                $citys = ArrayHelper::map($region->getRegions()->asArray()->all(), 'id', 'name');
            }

            return [
                'discount' => $team->discount,
                'country' => isset($json['country']) ? $json['country'] : null,
                'region' => isset($json['region']) ? $json['region'] : null,
                'city' => isset($json['city']) ? $json['city'] : null,
                'regions' => isset($regions) ? $regions : null,
                'citys' => isset($citys) ? $citys : null,
            ];
        }

        return [];
    }

    public function actionRegions($region)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $region = (int)$region;

        $region = Region::find()->where(['id' => $region])->one();
        if ($region) {
            return ArrayHelper::map($region->getRegions()->asArray()->all(), 'id', 'name');
        }

        return [];
    }

    public function actionPrice(
        $product,
        $currency = null,
        $country,
        $region,
        $city,
        $team = null,
        $user = null,
        $discount = 0
    ) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $product = (int)$product;
        $country = (int)$country;
        $region = (int)$region;
        $city = (int)$city;
        $team = (int)$team;
        $user = (int)$user;

        $team = Team::find()->where(['id' => $team])->one();
        $user = User::find()->where(['id' => $user])->one();
        $price = Price::find()->where(['region_id' => $city, 'product_id' => $product])->one();
        if (!$price) {
            $price = Price::find()->where(['region_id' => $region, 'product_id' => $product])->one();
        }
        if (!$price) {
            $price = Price::find()->where(['region_id' => $country, 'product_id' => $product])->one();
        }
        if ($price) {
            $_price = $price->price(false, $currency) - $price->price(false, $currency) * $discount / 100;

            // if ($team && $team->discount){
            //     $_price=$_price-$_price*$team->discount/100;
            // } elseif ($user && $user->discount){
            //     $_price=$_price-$_price*$user->discount/100;
            // }
            if (is_numeric($price->maxdiscount)) {
                $minprice = $price->price(false, $currency) - $price->price(false,
                        $currency) * $price->maxdiscount / 100;
                $_price = $_price < $minprice ? $minprice : $_price;
            }

            return [
                'price' => round($_price, 2),
                'currency' => $currency,
                'original_price' => $price->price,
                'original_currency' => $price->currency,
            ];
        }

        return [
            'price' => 0,
        ];
    }

    public function actionReworkUser($location, $naryad_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $location = (int)$location;
        $naryad_id = (int)$naryad_id;

        $location_name = ErpLocation::findOne($location)->name;

        $bad_naryad = ErpNaryad::findOne($naryad_id);

        foreach ($bad_naryad->json('log') as $item) {
            if ($item['location_from'] == $location_name) {
                $user_name = isset($item['user']) ? $item['user'] : '';
            }
        }

        $made_user = ErpUser::find()->where(['name' => $user_name])->one();

        return [
            'made_user_name' => $user_name,
            'made_user_id' => $made_user->id,

        ];
    }

    /*
     *Метод получает из erp-rework-act/view json с данными  
     * и перезаписывает модель наряда и дозаписывает модель акта переделки
     */

    public function actionReloadNaryad($naryad_id, $location_start, $id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $id = (int)$id;
        $naryad_id = (int)$naryad_id;
        $location_start = (int)$location_start;
        $reworkAct = LaitovoReworkAct::findOne($id);
        $reworkAct->location_start = $location_start;
        //$reworkAct = LaitovoReworkAct::findOne($id);
        $naryad = ErpNaryad::findOne($naryad_id);
        $naryad->location_id = Yii::$app->params['erp_dispatcher'];
        $naryad->status = ErpNaryad::STATUS_IN_WORK;
        $naryad->sort = 1;
        $naryad->reworkScenario = true;
        $literalLocation = $naryad->start;
        $naryad->start = $location_start;
        $naryad->rework = 1;
        if ($reworkAct->save() && $naryad->save()) {
            SProductionLiteral::unRegisterLiteral($naryad->id,$literalLocation);
            (new DistributionProductionLiterals())->distribute($naryad->barcode);
            return true;
        }
        return false;
    }

    public function actionReloadUser($status)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $status = (int)$status;
        if ($status == 1) {
            return ErpUser::userInWork();
        }
        return ErpUser::dispatcherInWork();
    }

    /*
     * Функция перезаписывает связи участков и материалов
     *
     */
    public function actionSaveLocationMaterial()
    {
        if (Yii::$app->request->post('location') && Yii::$app->request->post('id')) {
            $id = Yii::$app->request->post('id');
            MaterialLocation::deleteAll(['material_parameters_id' => $id]);
            $location = Yii::$app->request->post('location');
            foreach ($location as $items) {
                $location_material = new MaterialLocation();
                $location_material->material_parameters_id = $id;
                $location_material->laitovo_erp_location_id = $items;
                $location_material->save();
            }
            $message = MaterialParameters::locationName($id);
            return $message;
            //return print_r(Yii::$app->request->post('location'));
        }
        $id = Yii::$app->request->post('id');
        MaterialLocation::deleteAll(['material_parameters_id' => $id]);
        return true;
    }

    /*
     * метод для сохранения нового вида продукта из формы Производственного цикла
     *
     *
     */
    public function actionSaveNewProductType()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($request = Yii::$app->request->post()) {
            if ($request['title'] != null && $request['rule'] != null) {// return  print_r($request['title'], true);
                $model = new ErpProductType();
                $model->title = $request['title'];
                $model->rule = $request['rule'];
                if ($model->save()) {
                    if ($request['production_id'] != null) {
                        return [
                            'id' => $model->id,
                            'title' => $model->title,
                            'value' => ArrayHelper::map(ErpProductType::find()->where([
                                'or',
                                ['production_scheme_id' => null],
                                ['production_scheme_id' => $request['production_id']]
                            ])->all(), 'id', 'title')
                        ];
                    }
                    return [
                        'id' => $model->id,
                        'title' => $model->title,
                        'value' => ArrayHelper::map(ErpProductType::find()->all(), 'id', 'title')
                    ];
                }
            }
            return [
                'message' => "Заполните все поля"
            ];
        }

        return false;
    }

    /**
     * @param $location
     * @return array
     */
    public function actionJobType($location)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $location = (int)$location;
        if ($location) {
            $jobType = ErpJobType::find()->where([
                'and',
                ['location_id' => $location],
                [
                    'or',
                    ['status' => ErpJobType::ACTIVE],
                    ['status' => null]
                ]
            ])->all();
        } else {
            $jobType = ErpJobType::find()->where([
                'or',
                ['status' => ErpJobType::ACTIVE],
                ['status' => null]
            ])->all();
        }
        return ArrayHelper::map($jobType, 'id', 'title');
    }

    /**
     * @param $count
     * @param $jobType
     * @return float|int
     */
    public function actionJobCount($count, $jobType)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $count = (double)$count;
        $jobType = (int)$jobType;
        $typeJob = ErpJobType::findOne($jobType);
        if ($typeJob) {
            $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
            if ($typeJob->rate != 0 && $count != 0 && $config->json('averageRate') != 0 && $config->json('hourrate') != 0) {
                //                       средняя дневная норма                         цена нормированного коэффициента
                $jobPrice = $config->json('averageRate') / $typeJob->rate * $count * $config->json('hourrate');
                return round($jobPrice, 2);
            }
            return 0;
        }
        return 0;
    }

    /**
     * @param $id
     * @param $product_id
     * @return bool|int|string
     */
    public function actionProductActive($id, $product_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        $product_id = (int)$product_id;
        $product = ErpProductType::findOne($product_id);
        if ($product->production_scheme_id != 0) {
            $product->production_scheme_id = null;
            if ($product->save()) {
                return 2;
            }
        }
        $product->production_scheme_id = $id;
        if ($product->save()) {
            return $product->title;
        }
        return false;
    }

    public function actionProductStatus($id, $product_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        $product_id = (int)$product_id;
        $product = ErpProductType::find()->where(['id' => $product_id, 'production_scheme_id' => $id])->one();
        if (isset($product)) {
            return true;
        }
        return false;
    }

    /*
     * ставим чекбоксы у материалов в карточке продукта
     */
    public function actionRegisterMaterial($location_id, $production_scheme_id, $material_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $location_id = (int)$location_id;
        $production_scheme_id = (int)$production_scheme_id;
        $material_id = (int)$material_id;

        $materialType = MaterialProductionScheme::find()->where([
            'production_scheme_id' => $production_scheme_id,
            'location_id' => $location_id,
            'material_parameter_id' => $material_id
        ])->one();
        if (!$materialType) {
            $materialScheme = new MaterialProductionScheme();
            $materialScheme->production_scheme_id = $production_scheme_id;
            $materialScheme->location_id = $location_id;
            $materialScheme->material_parameter_id = $material_id;
            if ($materialScheme->save()) {
                return true;
            }
        }
    }

    /**
     * Вывод предварительной информации для регистрации сотрудника на рабочем месте
     */
    public function actionRenderWorkplace($user_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = (int)$user_id;
        $workplace = ErpLocationWorkplaceRegister::find()->where([
            'and',
            ['user_id' => $user_id],
            [
                'or',
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_ACTIVE],
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_INACTIVE]
            ]
        ])->orderBy('register_at DESC')->one();
        if (isset($workplace) && $workplace->action_type == ErpLocationWorkplaceRegister::TYPE_ACTIVE) {
            return [
                'status' => 1,
                'message' => 'Закончить рабочий день на рабочем месте: ',
                'workplaceName' => $workplace->workplace->location_number
            ];
        } else {
            $workplacePlan = ErpLocationWorkplacePlan::find()->where(['user_id' => $user_id])->orderBy('created_at DESC')->one();
            if (isset($workplacePlan)) {
                return [
                    'status' => 2,
                    'message' => 'Начать рабочий день на рабочем месте: ',
                    'workplaceName' => $workplacePlan->workplace->location_number
                ];
            } else {
                return ['message' => 'Вам не запланировано рабочее место'];
            }
        }
    }

    /**
     * Регистрация сотрудника на рабочем месте
     */
    public function actionRegisterWorkplace($user_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = (int)$user_id;
        $workplaceRegister = ErpLocationWorkplaceRegister::find()->where([
            'and',
            ['user_id' => $user_id],
            [
                'or',
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_ACTIVE],
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_INACTIVE]
            ]
        ])->orderBy('register_at DESC')->one();
        if ($workplaceRegister && $workplaceRegister->action_type == ErpLocationWorkplaceRegister::TYPE_ACTIVE && $workplaceRegister->workplace_id) {
            $register = new ErpLocationWorkplaceRegister();
            $register->user_id = $user_id;
            $register->workplace_id = $workplaceRegister->workplace_id;
            $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
            $register->register_at = time();
            $register->save();

            $register = new ErpLocationWorkplaceRegister();
            $register->user_id = $user_id;
            $register->workplace_id = $workplaceRegister->workplace_id;
            $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
            $register->register_at = time();
            $register->save();

            $register = new ErpLocationWorkplaceRegister();
            $register->user_id = $user_id;
            $register->workplace_id = $workplaceRegister->workplace_id;
            $register->action_type = ErpLocationWorkplaceRegister::TYPE_OUT;
            $register->register_at = time();
            $register->save();

            //снимаем человека с рабочего места
            $workplaceOut = ErpLocationWorkplace::findOne($workplaceRegister->workplace_id);
            $workplaceOut->user_id = null;
            $workplaceOut->activity = 0;
            $workplaceOut->save();

            return ['flag' => 2, 'date' => date("Y-m-d H:i:s")];
        } else {
            $workplacePlan = ErpLocationWorkplacePlan::find()->where(['user_id' => $user_id])->orderBy('created_at DESC')->one();
            if ($workplacePlan && $workplacePlan->workplace_id) {
                $register = new ErpLocationWorkplaceRegister();
                $register->user_id = $user_id;
                $register->workplace_id = $workplacePlan->workplace_id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_IN;
                $register->register_at = time();
                $register->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->user_id = $user_id;
                $register->workplace_id = $workplacePlan->workplace_id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_TO;
                $register->register_at = time();
                $register->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->user_id = $user_id;
                $register->workplace_id = $workplacePlan->workplace_id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_ACTIVE;
                $register->register_at = time();
                $register->save();
                //ставим человека на рабочее место
                $workplaceIn = ErpLocationWorkplace::findOne($workplacePlan->workplace_id);
                $workplaceIn->user_id = $workplacePlan->user_id;
                $workplaceIn->activity = 1;
                if ($workplaceIn->save()) {
                    return ['flag' => 1, 'date' => date("Y-m-d H:i:s")];
                }
            } else {
                return ['flag' => 3, 'message' => 'Вам не запланировано рабочее место'];
            }
        }
    }

    public function actionSaveWeekend($weekend)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $weekend = (string)$weekend;
        $wekIn = Weekend::find()->where(['weekend' => $weekend])->one();
        if ($weekend && $wekIn) {
            $wekIn->delete();
        }
        if ($weekend && !$wekIn) {
            $wek = new Weekend();
            $wek->weekend = $weekend;
            $wek->save();
        }
    }

    public function actionPositionList($division_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $division_id = (int)$division_id;
        $user_position = ArrayHelper::map(ErpUserPosition::find()->all(), 'position_id', 'position_id');
        if ($user_position) {
            $positions = ErpPosition::find()->where([
                'and',
                ['division_id' => $division_id],
                ['not in', 'id', $user_position]
            ])->all();
            if (!count($positions)) {
                return [];
            }
        } else {
            $positions = ErpPosition::find()->where(['division_id' => $division_id])->all();
        }
        return ArrayHelper::map($positions, 'id', 'title');
    }

    public function actionDeletePositionList($division_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $division_id = (int)$division_id;
        $user_position = ArrayHelper::map(ErpUserPosition::find()->all(), 'position_id', 'position_id');
        if ($user_position) {
            $positions = ErpPosition::find()->where([
                'and',
                ['division_id' => $division_id],
                ['in', 'id', $user_position]
            ])->all();
            if (!count($positions)) {
                return [];
            }
        } else {
            $positions = ErpPosition::find()->where(['division_id' => $division_id])->all();
        }
        return ArrayHelper::map($positions, 'id', 'title');
    }

    public function actionUserPosition($position_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $position_id = (int)$position_id;

        $user_position = ErpUserPosition::find()->where(['position_id' => $position_id])->one();
        if ($user_position && $user_position->user_id) {
            return $user_position->user_id;
        }
        return false;
    }

    public function actionProductTypes($type=null,$cloth=null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $type = (string)$type;
        $cloth = (string)$cloth;

        return MainProductInfo::getGoodsNameListByWindowTypeAndCloth($type,$cloth);
    }

    public function actionCarName($article)
    {
        $response['status'] = 'error';
        $response['message'] = 'Не найден такой автомобиль';

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $article = (int)$article;

        $car = Cars::find()->where(['article' => $article])->one();
        if ($car) {
            $response['status'] = 'success';
            $response['message'] = $car->name;
        }

        return $response;
    }

    public function actionCreateUpn()
    {
        var_dump(Yii::$app->request->post());
        die();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }

    public function actionCheckNeed($goodid,$cararticle)
    {
        if (!$cararticle || !$goodid) return null;
        // var_dump($goodid);
        // var_dump($cararticle);
        // die();

        $response['status'] = 'error';
        $response['message'] = 'В данном артикуле нет необходимости';

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $article = MainProductInfo::getGoodArticle($goodid,$cararticle);
        if (!$article) return null;
        $title = MainProductInfo::getGoodTitle($goodid);

//        $model = new Article($article);
//        if ($model->saleRate >= 1){
            $response['status'] = 'success';
            $response['message'] = $title;
//        }

        return $response;
    }


    public function actionAutomaticArticleSearch($article)
    {
        $response['status'] = 'error';
        $response['message'] = 'Не найден такой автомобиль';

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        try{
            $elements = explode('-',$article);
            if (!in_array(count($elements),[4,5])) throw new \Exception();
            $search = new Article($article);

            $response['status'] = 'success';
            $response['message'] = [
                'auto' => $search->carName,
                'group' => $search->groupName,
                'article' => $search->id,
                'rpp' => $search->salePrevious,
                'rp' => $search->saleLast,
                'pnp' => $search->increase,
                'pr' => $search->salePlan,
                'ksp' => $search->makeRate,
                'kk' => $search->fixRate,
                'chr' => $search->saleRate,
                'op' => $search->balancePlan,
                'stat' => $search->saleSixMonth,
                'of' => $search->balanceFact,
                'inp' => $search->balanceProdFree,  //в производстве
                'names' => implode('<br>',$search->carNameWithAnalogs),
            ];
        }catch (\Exception $e){
            $response['status'] = 'error';
            $response['message'] = ['Не удалось по данному артикулу определить состояние склада. Скорей всего он введен неверно!'];
        }

        return $response;
    }

    public function actionOrderByUpn()
    {
        $response =
        [
            'order_number' => null,
            'upn' => null,
            'article' => null,
        ];

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $barcode = Yii::$app->request->post('barcode');
        if (empty($barcode)) return $response;
        $barcode = BarcodeHelper::toLatin($barcode);
        $first = mb_substr($barcode,0,1);
        if (strtoupper($first) === 'L' && mb_strlen($barcode) == 9) {
            $upn = Naryad::findOne(['barcode' => $barcode]);
        }else{
            $upn = Naryad::findOne(['id' => $barcode]);
        }
        if ($upn->isParent)
            $upn = $upn->children[0];

        if (!$upn) return $response;

        $response['order_number'] = intval(@$upn->orderEntry->order->source_innumber);
        $response['upn'] = $upn->id;
        $response['article'] = $upn->article;

        return $response;
    }

    public function actionTimestamp($date)
    {
//        date_default_timezone_set("Europe/Moscow");
//        $formatter = Yii::$app->formatter;
//        $formatter->timeZone = "Europe/Moscow";
//        $formatter->datetimeFormat = 'php:d.m.Y H:i:s';
//        Yii::$app->formatter->timeZone = "Europe/Moscow";
//        $response['date'] = $formatter->asTimestamp($date);
        $response['status'] = 'success';
        $response['date'] = strtotime($date);


        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $response;
    }
}

