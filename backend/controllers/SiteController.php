<?php
namespace backend\controllers;

use backend\modules\laitovo\models\CarsForm;
use backend\modules\logistics\models\Order;
use common\models\laitovo\ClipsScheme;
use core\helpers\StringHelper;
use core\logic\DatePeriodManager;
use core\models\article\Article;
use core\services\SProductionLiteral;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use backend\models\LoginForm;
use backend\models\SignupForm;
use backend\models\ResetPasswordForm;
use backend\models\PasswordResetRequestForm;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use common\models\User;
use common\models\team\Team;
use yii\helpers\Json;
use backend\modules\laitovo\models\ErpNaryad;
use common\models\laitovo\Cars;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['error', 'captcha'],
                'rules' => [
                    [
                        'actions' => ['login', 'signup', 'request-password-reset', 'reset-password', 'invite'],
                        'roles' => ['?'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @param $continue string
     * @return mixed
     */
    public function actionLogin($continue = null)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
            return $continue ? $this->redirect($continue) : $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
                'continue' => $continue
            ]);
        }
    }

    /**
     * Signs user up.
     *
     * @param $continue string
     * @return mixed
     */
    public function actionSignup($continue = null)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user, 3600 * 24 * 3000)) {
//                    return $this->goHome();
                    return $continue ? $this->redirect($continue) : $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'continue' => $continue
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @param $continue string
     * @return mixed
     */
    public function actionRequestPasswordReset($continue = null)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail($continue)) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Проверьте вашу электронную почту для получения дальнейших инструкций.'));

//                return $this->goHome();
                return $this->redirect(['login', 'continue' => $continue]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Восстановление пароля невозможно.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
            'continue' => $continue
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token, $continue = null)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Пароль изменен.'));

//            return $this->goHome();
            return $this->redirect(['login', 'continue' => $continue]);
        }

        return $this->render('resetPassword', [
            'model' => $model,
            'continue' => $continue
        ]);
    }

    public function actionInvite($team, $email, $invite)
    {
        $team=(int)$team;
        
        if (empty($team) || empty($email) || empty($invite) || !is_int($team) || !is_string($email) || !is_string($invite) || ($_team=Team::findOne($team))===null )
            throw new BadRequestHttpException(Yii::t('app', 'Неверная ссылка.'));

        $json=Json::decode($_team->json ? $_team->json : '{}');
        if (isset($json['Invite'][$email]) && $json['Invite'][$email]==$invite) {
            $user=User::findByEmail($email);
            if ($user) {
                if (!Yii::$app->user->isGuest && Yii::$app->user->id!=$user->id)
                    Yii::$app->user->logout();
                $_team->unlink('users', $user, true);
                $_team->link('users', $user);
                unset($json['Invite'][$email]);
                $_team->json=Json::encode($json);
                $_team->save();
                Yii::$app->team->setTeam($_team->id);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Добро пожаловать в команду "{team}".',['team'=>$_team->name]));
                return $this->goHome();
            } else {
                Yii::$app->user->logout();
                Yii::$app->session['invite_team_id']=$_team->id;
                Yii::$app->session['invite_email']=$email;
                return $this->redirect(['signup']);
            }
        } else {
            throw new BadRequestHttpException(Yii::t('app', 'Неверная ссылка.'));
        }
    }

    public function actionSetTeam($id)
    {
        Yii::$app->team->setTeam($id);
        return $this->redirect(['settings/team','id'=>Yii::$app->team->id]);
    }

    //Тестовый экшн.
    public function actionLekalo()
    {
        $articles = [];
        $carArticle = [];
        $use = [];
        $all = [];
        $notUse = [];

        $query = ErpNaryad::find();
        $query->groupBy(['article']);
        $query->select(['article']);
        $naryads = $query->all();
        
        foreach ($naryads as $naryad) {
            $articles[] =  $naryad->article;
        }
        
        foreach ($articles as $article) {
            $value = explode('-',$article);
            if (isset($value[0]) && $value[0] != 'OT') {
                if (isset($value[2])) {
                    $carArticle[] = $value[2];
                }
            }
        }

        $cars = Cars::find()->where(['in','article',$carArticle])->all();
        foreach ($cars as $car) {
            $lekalo = preg_replace('~\D+~','',$car->json('nomerlekala'));
            if (!in_array($lekalo,$use)) {
                $use[] = $lekalo;
            }
        }

        $cars = Cars::find()->all();
        foreach ($cars as $car) {
            $lekalo = preg_replace('~\D+~','',$car->json('nomerlekala'));
            if (!in_array($lekalo,$all)) {
                $all[] = $lekalo;
            }
        }

        foreach ($all as $key => $value) {
            if (!in_array($value,$use)) {
                $notUse[] = $value;
            }
        }

        var_dump($notUse);
        var_dump($use);
        die();
    }

    //Отчет - автомобили только на машгнитах
    public function actionCarsWithMagnetsOnly($offset=0,$count=30)
    {
        $cars = Cars::find()->limit($count)->offset($offset)->all();
        foreach ($cars as $car) {
            $form = new CarsForm($car->id);
            if (!$form->fd_laitovo_standart_scheme && $form->fd_laitovo_standart_schemem && $form->fd_magnit == 'Да') {
                echo "<a style='margin:10px 0px' target='_blank' href='". Yii::$app->urlManager->createUrl(['/laitovo/cars/view','id' => $car->id]) . "'>" . $car->fullName . "</a><br>";
            }
        }
        die();
    }

    //Отчет - автомобили только на машгнитах
    public function actionCarLogisticLength()
    {
        $cars = Cars::find()->where(['mastered' => true])->all();
        $result = [];
        $count = 1;
        foreach ($cars as $car) {
//            $form = new CarsForm($car->id);
            $car1['index'] = $count++;
            $car1['name'] = $car->fullName;
            $car1['article'] = $car->article;
//            $car1['FV'] = $car->json('_bw')['dlina']$form->fv_dlina . ' х ' . $form->fv_visota;
//            $car1['FD'] = $form->fd_dlina . ' х ' . $form->fd_visota;
//            $car1['RD'] = $form->rd_dlina . ' х ' . $form->rd_visota;
//            $car1['RV'] = $form->rv_dlina . ' х ' . $form->rv_visota;
//            $car1['BW'] = $form->bw_dlina . ' х ' . $form->bw_visota;
            $car1['lekalo'] = $car->lekalo;
            $car1['FV'] = @$car->json('_fv')['dlina'] . ' х ' . @$car->json('_fv')['visota'];
            $car1['FD'] = @$car->json('_fd')['dlina'] . ' х ' . @$car->json('_fd')['visota'];
            $car1['RD'] = @$car->json('_rd')['dlina'] . ' х ' . @$car->json('_rd')['visota'];
            $car1['RV'] = @$car->json('_rv')['dlina'] . ' х ' . @$car->json('_rv')['visota'];
            $car1['BW'] = @$car->json('_bw')['dlina'] . ' х ' . @$car->json('_bw')['visota'];
            $car1['link'] = "<a style='margin:10px 0px' target='_blank' href='". Yii::$app->urlManager->createUrl(['/laitovo/cars/view','id' => $car->id]) . "'>>></a><br>";
            $result[] = $car1;
        }

        usort($result, function ($a, $b) {
            switch (true) {
                case intval($a['lekalo']) > intval($b['lekalo']):
                    return 1;
                default:
                    return 0;
            }
        });

        echo "<table cellpadding='5' border='1px' style='font-size: 12px;border-collapse: collapse'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Лекало</th>";
        echo "<th>Артикул</th>";
        echo "<th>Наименование автомобиля</th>";
        echo "<th>ПФ</th>";
        echo "<th>ПБ</th>";
        echo "<th>ЗБ</th>";
        echo "<th>ЗФ</th>";
        echo "<th>ЗШ</th>";
        echo "<th>Ссылка</th>";
        echo "</tr>";
        echo "<thead>";
        echo "<tbody>";
        foreach ($result as $item) {
            echo "<tr>";
            echo "<td>{$item['lekalo']}</td>";
            echo "<td>{$item['article']}</td>";
            echo "<td>{$item['name']}</td>";
            echo "<td>{$item['FV']}</td>";
            echo "<td>{$item['FD']}</td>";
            echo "<td>{$item['RD']}</td>";
            echo "<td>{$item['RV']}</td>";
            echo "<td>{$item['BW']}</td>";
            echo "<td>{$item['link']}</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";

        die();
    }

    public function actionTapeCars()
    {
        //Получить все схемы, где  fd и rd содержат тип креплений c
        $carsArray = [
            2,
            16,
            21,
            23,
            26,
            70,
            74,
            119,
            163,
            168,
            180,
            248,
            268,
            289,
            292,
            293,
            295,
            297,
            307,
            308,
            312,
            314,
            315,
            385,
            387,
            414,
            437,
            439,
            441,
            446,
            551,
            552,
            574,
            575,
            598,
            669,
            683,
            709,
            771,
            985,
            1004,
            1045,
            1113,
            1181,
            1249,
            1258,
            1263,
            1269,
            1274,
            1286,
            1289,
            1306,
            1402,
            1404,
            1411,
            1542,
            1552,
            1636
        ];

        $cars = (new Query())
            ->select('id')
            ->from('laitovo_cars')
            ->where('article in (' . implode(',',$carsArray) .')')
            ->column();

        $clips = (new Query())
            ->select('id')
            ->from('laitovo_cars_clips')
            ->where('name = 3 and (type = "C" OR type = "CC")')
            ->andWhere(['in','car_id',$cars])
            ->column();

        $schemes = ClipsScheme::find()->where(['and',
            ['in','car_id',$cars],
            ['brand' => 'Laitovo'],
            ['type' => 'Стандарт'],
            ['type_clips' => 'Простые'],
            ['or',
                ['window' => 'ПБ'],
                ['window' => 'ЗБ'],
            ],
        ])->all();

        $findCars = [];
        foreach ($schemes as $scheme) {
            $array = $scheme->json('scheme');
            foreach ($array as $clip) {
                if (in_array($clip,$clips)) {
                    $findCars[] = $scheme->car_id;
                    continue;
                }
            }
        }

        $carsRes = Cars::find()->where(['in','id',$findCars])->all();

        foreach ($carsRes as $carR) {
            $form = new CarsForm($carR->id);
            if ((bool)$form->fd_use_tape === false && (bool)$form->rd_use_tape === false) {
                echo $carR->article . '<br>';
            }
        }

//        var_dump($schemes);
        die;
    }

    public function actionTapeList()
    {
        $count = 0;
        $tapeCars = [];
        $cars = Cars::find()->orderBy('name')->all();
        foreach ($cars as $car) {
            if (@$car->json('_fd')['use_tape'] || @$car->json('_rd')['use_tape'])
                $tapeCars[] = $car;
        }
        echo '<table style="border: 1px solid black">';
            echo '<thead>';
            echo '<tr>
                <th>Артикул</th>
                <th>Наименование</th>
                <th>ПБ</th>
                <th>ЗБ</th>
            </tr>';
            echo '</thead>';
            echo '<tbody>';
            foreach ($tapeCars as $tapeCar) {
                echo "<tr>
                    <td style=\"border: 1px solid black\">{$tapeCar->article}</td>
                    <td style=\"border: 1px solid black\">{$tapeCar->fullName}</td>
                    <td style=\"border: 1px solid black\">" . (@$tapeCar->json('_fd')['use_tape'] ? '+' : '-' ). "</td>
                    <td style=\"border: 1px solid black\">" . (@$tapeCar->json('_rd')['use_tape'] ? '+' : '-' ). "</td>
                </tr>";
            }
            echo '</tbody>';
        echo '</table>';
    }

    public function actionTapeListReload($token)
    {
        if ($token != 'f,hfrflf,hf') {
            echo 'У вас нет доступа';
            die;
        }
        $cars = Cars::find()->orderBy('name')->all();
        foreach ($cars as $car) {
            if (@$car->json('_fd')['use_tape'] || @$car->json('_rd')['use_tape']) {
                $car->updated_at = time();
                if ($car->save())
                    echo "Сохранил автомобиль => {$car->article} <br>";
            }
        }

    }


    public function actionClipsList($order)
    {
        $order = Order::find()->where(['source_innumber' => $order])->one();
        if (!$order)
            throw new \DomainException('Не могу найти заказ с таким номером!');

        $elements = $order->orderEntries;
        if (!$elements)
            throw new \DomainException('Нет позиций заказа!');

        $articles = [];

        foreach ($elements as $element) {
            if (isset($articles[$element->article]))
                $articles[$element->article]++;
            else
                $articles[$element->article] = 1;
        }

        $cars = [];
        foreach ($articles as $article => $count) {
            $obj = new Article($article);
            if ($obj->car) {
                $types = $obj->getClipsWithTypesAndCount();
                $cars[$obj->carArticle]['articles'][$article]['count'] = $count;
                $cars[$obj->carArticle]['articles'][$article]['types'] = $types;
                if (!isset($cars[$obj->carArticle]['title']))
                    $cars[$obj->carArticle]['title'] = $obj->carEnName;
            }
        }

        echo "<table style='border-spacing: 0px'>";

        $countRow = 1;
        echo "<tr>
            <th>№ п/п</th>
            <th>Автомобиль</th>
            <th>Артикул</th>
            <th>Клипс в артикуле</th>
            <th>Всего артикулов</th>
            <th>Всего клипс</th>
</tr>";
        foreach ($cars as $car) {
//            echo $car['title'];
            foreach ($car['articles'] as $articleTitle => $article) {
                echo "<tr><td style='border: 1px solid black;padding: 5px 5px'>";
                echo $countRow;
                echo "</td>";

                echo "<td style='border: 1px solid black; padding: 5px 5px'>";
                echo $car['title'];
                echo "</td>";


                echo "<td style='border: 1px solid black; padding: 5px 5px'>";
                echo $articleTitle;
                echo "</td>";


                echo "<td style='border: 1px solid black; padding: 5px 5px'>";
                foreach ($article['types'] as $name => $countClips) {
                    echo $name . " - " . $countClips .  " шт.  <br />";
                }
                echo "</td>";

                echo "<td style='border: 1px solid black; padding: 5px 5px'>";
                echo $article['count'];
                echo "</td>";

                echo "<td style='border: 1px solid black; padding: 5px 5px'>";
                echo $article['count'] * array_sum($article['types']);
                echo "</td></tr>";
                $countRow++;
            }
        }

        echo "</table>";

//
//        foreach ($types as $name => $type) {
//            echo $name.$type;
//        }
        echo 'Все ок!';

    }

    public function actionTestLiterals()
    {
        (new SProductionLiteral())->test();
    }

}
