<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>

<h2>Изображение</h2>
<?= Html::img($model->getImg(), ['alt' => '[отсутствует]']) ?>
<?= $form->field($model, 'img_file')->label('')->fileInput(); ?>

<h2>Документы</h2>
<?php foreach($model->getDocs() as $fileUrl): ?>
    <a href="<?= $fileUrl ?>">Скачать</a> <br>
<?php endforeach; ?>
<?= $form->field($model, 'doc_files[]')->label('')->fileInput(['multiple' => true]); ?>

<?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end(); ?>
