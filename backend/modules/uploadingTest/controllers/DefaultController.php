<?php

namespace backend\modules\uploadingTest\controllers;

use backend\modules\uploadingTest\models\TestModel;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $model = new TestModel();

        if (Yii::$app->request->isPost) {
            $model->saveImg();
            $model->saveDocs();
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
