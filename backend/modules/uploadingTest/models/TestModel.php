<?php

namespace backend\modules\uploadingTest\models;

use common\models\LogActiveRecord;
use Yii;

class TestModel extends LogActiveRecord
{
    public $img_file;
    public $doc_files;
    private $_fileManager;
    private $_dbImg;
    private $_dbDocs;

    public function __construct(array $config = [])
    {
        $this->_fileManager = Yii::$app->fileManager;
        $this->_dbImg = Yii::getAlias('@backend') . '/modules/uploadingTest/db_img.txt';
        $this->_dbDocs = Yii::getAlias('@backend') . '/modules/uploadingTest/db_docs.txt';

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['img_file'], 'file', 'extensions' => 'png, jpg'],
            [['doc_files'], 'file', 'maxFiles' => 3],
        ];
    }

    public function saveImg()
    {
        //загрузка файла на сервер
        if (!($fileKey = $this->_fileManager->uploadTeamFile('TestModel[img_file]', 'vasya/img'))) {
            return false;
        }
        //сохранение ключа к файлу в бд
        if (!file_put_contents($this->_dbImg, $fileKey)) {
            return false;
        }

        return true;
    }

    public function saveDocs()
    {
        //загрузка файлов на сервер
        if (!($keysOfFiles = $this->_fileManager->uploadTeamFiles('TestModel[doc_files]', 'vasya/docs'))) {
            return false;
        }
        //сохранение ключей к файлам в бд
        if (!file_put_contents($this->_dbDocs, implode(',', $keysOfFiles))) {
            return false;
        }

        return true;
    }

    public function getImg()
    {
        $fileKey = file_get_contents($this->_dbImg);
        $fileUrl = $this->_fileManager->getFileUrlForView($fileKey);
        
        return $fileUrl;
    }

    public function getDocs()
    {
        $keysOfFiles = array_filter(explode(',', file_get_contents($this->_dbDocs)));
        $urlsOfFiles = [];
        foreach ($keysOfFiles as $fileKey) {
            $fileUrl = $this->_fileManager->getFileUrlForDownload($fileKey);
            $urlsOfFiles[] = $fileUrl;
        }
        
        return $urlsOfFiles;
    }
}