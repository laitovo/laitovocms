<?php

namespace backend\modules\wage\controllers;

use backend\modules\laitovo\models\ErpJobScheme;
use core\helpers\UpdateSessionTrait;
use Yii;
use backend\modules\laitovo\models\ErpJobType;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;

/**
 * ErpJobTypesController implements the CRUD actions for ErpJobType model.
 */
class ErpJobTypesController extends Controller
{
    use UpdateSessionTrait;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpJobType models.
     * @return mixed
     */
    public function actionIndex()
    {
        /**
         * @var $hideInactiveJob boolean [ Скрывать неактивные работы ]
         */
        $hideInactiveJob = $this->_updateSession('wage_erp_job_type_inactive_filter','update_wage_erp_job_type_inactive_filter', true);

        $query=ErpJobType::find()->orderBy('status ASC');
        if ($hideInactiveJob)
            $query->andWhere(['or',['status' => 1],['status' => null]]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['title', 'location_id', 'status'],
            ],
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['id'=>Yii::$app->request->get('search')],
                ['like','title',Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'hideInactiveJob' => $hideInactiveJob
        ]);
    }

    /**
     * Displays a single ErpJobType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);

        $dataProviderTypeRate = new ActiveDataProvider([
            'query' => $model->getErpJobTypeRates()
        ]);
        $dataProviderScheme = new ActiveDataProvider([
            'query' => $model->getErpJobSchemes()
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProviderTypeRate' => $dataProviderTypeRate,
            'dataProviderScheme' => $dataProviderScheme,
        ]);
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);

        $content = $this->renderPartial('print',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}', 
            'options' => ['title' => $model->name],
        ]);

        return $pdf->render(); 
    }

    /**
     * Creates a new ErpJobType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpJobType();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpJobType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpJobType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);

        $mayBeDeactivated = !ErpJobScheme::find()->where(['type_id' => $id])->andWhere(['is not','production_scheme_id', null])->exists();
        $model->status = $model->status == 1 ? 2 : 1;

        if (!$mayBeDeactivated && $model->status === 2) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Данный вид работ участвует в
             производственном процессе. Его нельзя деактивировать!'));
            return $this->redirect(['view', 'id' => $model->id]);
        }
        if ($model->save()) {
            $model->status == 1
                ? Yii::$app->session->setFlash('success', Yii::t('app', 'Вид работ успешно активирован!'))
                : Yii::$app->session->setFlash('success', Yii::t('app', 'Вид работ успешно деактивирован!'));

        } else {
            $model->status == 1
                ? Yii::$app->session->setFlash('success', Yii::t('app', 'Не удалось активировать вид работ!'))
                : Yii::$app->session->setFlash('success', Yii::t('app', 'Вид работ успешно ДеАктивировать!'));
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Deletes an existing ErpJobType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpJobType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpJobType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpJobType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
