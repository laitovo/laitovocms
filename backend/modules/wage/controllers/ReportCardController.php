<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/11/17
 * Time: 9:00 AM
 */

namespace backend\modules\wage\controllers;

use backend\modules\laitovo\models\ArchiveReportCard;
use backend\modules\laitovo\models\ErpArchiveReportCard;
use backend\modules\laitovo\models\ErpDocumentCause;
use backend\modules\laitovo\models\ErpReportCardForm;
use backend\modules\laitovo\models\ErpReportCardTableClose;
use backend\modules\laitovo\models\ErpReportCartTable;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ReportCard;
use common\models\laitovo\ErpReportCard;
use common\models\laitovo\Weekend;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use Yii;

class ReportCardController extends Controller
{
    public function actionIndex($date = null)
    {
        $model = new ReportCard();
        if ($date != null) {
            $model->setAttributes(['dateFrom' => $date]);
        }
        if (Yii::$app->request->get()) {
            $model->load(Yii::$app->request->get());
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionWeekend()
    {
        $model = new Weekend();
        $report = new ReportCard();

        return $this->render('weekend', [
            'model' => $model,
            'report' => $report->resultArray(87),
            'result' => $model->resultArray(1501534800),
            'dataProvider' => $model->getArrayDataProvider(1501534800)
        ]);
    }

    public function actionReportCardForm($date, $user_id)
    {
        $report_form = new ErpReportCardForm($date, $user_id);
        $document = new ErpDocumentCause();
        if (Yii::$app->request->post()) {
            $report_form->fields = Yii::$app->request->post('fields');
            if ($report_form->saveCard()) {
                return $this->redirect(['index', 'date' => $date]);
            }
        }
        return $this->render('report-card-form', [
            'model' => $report_form,
            'dataProvider' => $report_form->getArrayDataProvoder(),
            'document_provider' => $document->document_list($date, $user_id)
        ]);
    }

    public function actionPerfectCardForm($date, $user_id)
    {
        $report_form = new ErpReportCardForm($date, $user_id);
        $document = new ErpDocumentCause();

        if (Yii::$app->request->post()) {
            $report_form->fields = Yii::$app->request->post('fields');
            if ($report_form->saveCard()) {
                return $this->redirect(['index', 'date' => $date]);
            }
        }

        return $this->render('report-card-form', [
            'model' => $report_form,
            'dataProvider' => $report_form->getPerfectArrayDataProvoder(),
            'document_provider' => $document->document_list($date, $user_id)
        ]);
    }

    public function actionCreateDocumentCause($date = null, $user_id)
    {
        $model = new ErpDocumentCause();
        $model->user_id = $user_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($date) {
                return $this->redirect(['report-card-form', 'date' => $date, 'user_id' => $user_id]);
            } else {
                return $this->redirect(['document-cause-index', 'user_id' => $user_id]);
            }
        }
        return $this->render('document-cause/create',
            ['model' => $model, 'document_provider' => $model->document_list($date, $user_id), 'date' => $date]);
    }

    public function actionPerfectCardDocumentForm($date, $user_id)
    {
        $report_form = new ErpReportCardForm($date, $user_id);
        $document = new ErpDocumentCause();

        if (Yii::$app->request->post()) {
            $report_form->fields = Yii::$app->request->post('fields');
            if ($report_form->saveCard()) {
                return $this->redirect(['index', 'date' => $date]);
            }
        }

        return $this->render('report-card-form', [
            'model' => $report_form,
            'dataProvider' => $report_form->getPerfectArrayDocumentDataProvoder(),
            'document_provider' => $document->document_list($date, $user_id)
        ]);
    }

    /**
     * Deletes an existing MaterialForm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteDocument($user_id, $date = null, $document_id)
    {
        $document = ErpDocumentCause::findOne($document_id);
        $document->delete();
        if ($date) {
            return $this->redirect(['report-card-form', 'date' => $date, 'user_id' => $user_id]);
        }
        return $this->redirect(['document-cause-index', 'user_id' => $user_id]);
    }

    public function actionIndexArchive($year = null, $month = null)
    {
        $model = new ArchiveReportCard();
        if (!$year) {
            return $this->render('year-month', ['model' => $model]);
        }
        if ($year && !$month) {
            return $this->render('year-month', ['model' => $model, 'year' => $year]);
        }
        if ($year && $month) {
            return $this->render('year-month', ['model' => $model, 'year' => $year, 'month' => $month]);
        }
    }

    public function actionDocumentCauseIndex($user_id = null)
    {
        if (!$user_id) {
            $user_list = ErpUser::userInWork();
            return $this->render('document-cause/document-cause-list', ['user_list' => $user_list]);
        }
        if ($user_id) {
            $user = ErpUser::findOne($user_id);
            $document_list = ErpDocumentCause::find()->where(['user_id' => $user_id])->orderBy('id DESC');
            $dataProvider = new ActiveDataProvider([
                'query' => $document_list
            ]);
            return $this->render('document-cause/document-cause-list',
                ['dataProvider' => $dataProvider, 'user' => $user]);
        }
    }

    /**
     * @return string
     */
    public function actionReportCardTable()
    {
        $model = new ErpReportCartTable();
        if(Yii::$app->request->post()){
           $model->dateFrom = Yii::$app->request->post('ErpReportCartTable')['dateFrom'];
           $model->dateTo = Yii::$app->request->post('ErpReportCartTable')['dateTo'];
        }
        return $this->render('report-card-table',['model'=>$model]);
    }

    /**
     * @return string
     */
    public function actionReportCardClose()
    {
        $model = new ErpReportCardTableClose();
        if (Yii::$app->request->get()) {
            $model->load(Yii::$app->request->get());
        }

        $dataProviderSupport = new ArrayDataProvider([
            'allModels' => $model->resultArraySupport(),
            'sort' => [
                'attributes' => ['username', 'email'],
            ],
            'pagination' => [ 'pageSize' => 100 ],
            
        ]);

        $dataProviderProduction = new ArrayDataProvider([
            'allModels' => $model->resultArrayProduction(),
            'sort' => [
                'attributes' => ['username', 'email'],
            ],
            'pagination' => [ 'pageSize' => 100 ],
        ]);

        $dataProviderOther = new ArrayDataProvider([
            'allModels' => $model->resultArrayOther(),
            'sort' => [
                'attributes' => ['username', 'email'],
            ],
            'pagination' => [ 'pageSize' => 100 ],
        ]);

        return $this->render('report-card-close', [
            'model'=>$model,
            'dataProviderSupport' => $dataProviderSupport,
            'dataProviderProduction' => $dataProviderProduction,
            'dataProviderOther' => $dataProviderOther,

            ]);
    }

}