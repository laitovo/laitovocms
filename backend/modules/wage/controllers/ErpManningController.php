<?php

namespace backend\modules\wage\controllers;

use backend\modules\admin\models\User;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpPosition;
use backend\modules\laitovo\models\ErpUser;
use common\models\laitovo\ErpDivision;
use common\models\laitovo\ErpUserPosition;
use Yii;
use backend\modules\laitovo\models\ErpManning;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\debug\models\timeline\DataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ErpManningController implements the CRUD actions for ErpManning model.
 */
class ErpManningController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['delete','test'],
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            #Ограничиваю доступ к отчету для пользователей
                            $users = [2,37,21,45,47,121,122,123,124];
                            if ($action->id == 'index' && !in_array(Yii::$app->user->getId(),$users))
                                return false;
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all ErpManning models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ErpPosition::find()->joinWith('division as d')->orderBy('d.article ASC'),
            'pagination' => [
                'pageSize' => ErpPosition::find()->count(),
            ],
        ]);

        if(Yii::$app->request->post() && isset(Yii::$app->request->post()['delete_manning']) && isset(Yii::$app->request->post()['id'])){
            $this->actionDelete(Yii::$app->request->post()['id']);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpManning model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpManning model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpManning();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpManning model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }



    public function actionUpdateModal($id)
    {
        $model = ErpPosition::findOne($id);
        if ($model->load(Yii::$app->request->post()))
             if($model->save()){
                 return $this->renderPartial('update-modal', ['model' => $model, 'flag'=>true]);
             }
        return $this->renderPartial('update-modal', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing ErpManning model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        if($model->user_id){
            $user = ErpUser::findOne($model->user_id);
            $user->status=ErpUser::STATUS_DISMISSED;
            $user->save();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpManning model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpManning the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpManning::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function actionCreatePositionModal()
    {
        $model = new ErpPosition();
        if($model->load(Yii::$app->request->post())) {
                $old_position = ErpPosition::find()->where(['and',['division_id'=>$model->division_id], ['title'=>null]])->one();
                if($old_position){
                    $old_position->title = $model->title;
                    $old_position->salary_one = $model->salary_one ? $model->salary_one : 0;
                    $old_position->salary_two = $model->salary_two ?  $model->salary_two : 0;
                    $old_position->car_compensation = $model->car_compensation ? $model->car_compensation : 0;
                    $old_position->surcharge = $model->surcharge ? $model->surcharge : 0 ;
                    $old_position->qualification = $model->qualification ? $model->qualification : 0;
                    $old_position->premium = $model->premium ? $model->premium : 0;
                    $old_position->kpi_one = $model->kpi_one ? $model->kpi_one : 0;
                    $old_position->kpi_two = $model->kpi_two ? $model->kpi_two : 0;
                    $old_position->kpi_three = $model->kpi_three ? $model->kpi_three : 0;
                    if($old_position->save())
                        return $this->renderPartial('create-position',['model'=>$model, 'flag'=>true]);
                }else{
                    if($model->save())
                        return $this->renderPartial('create-position',['model'=>$model, 'flag'=>true]);
                }
            }
        return $this->renderPartial('create-position',['model'=>$model]);
    }


    public function actionCreatePosition()
    {
        $model = new ErpPosition();
        if($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['index-position']);

        return $this->render('create-position',['model'=>$model]);
    }


    public function actionUpdatePosition($id)
    {
        $model = ErpPosition::findOne($id);
        if($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['index-position']);

        return $this->render('create-position',['model'=>$model]);
    }


    public function actionPositionIndex()
    {
        $query  = ErpPosition::find();

        $dataProvider = new ActiveDataProvider([
            'query'=>$query
        ]);

        return $this->render('position-index',[
            'dataProvider'=> $dataProvider
        ]);
    }


    public function actionCreateUserModal()
    {
        $model = new ErpUser();
        if($model->load(Yii::$app->request->post()))
            $model->save();
        return $this->renderPartial('create-user-modal',['model'=>$model]);
    }



    public function actionCreateDivisionModal()
    {
        $model = new ErpDivision();
        if($model->load(Yii::$app->request->post()))
            if($model->save()){
                $position = new ErpPosition;
                $position->division_id = $model->id;
                $position->save();
                return $this->renderPartial('create-division-modal',['model'=>$model, 'flag'=>true]);
            }
        return $this->renderPartial('create-division-modal',['model'=>$model]);
    }



    public function actionDeleteDivisionModal()
    {
        $model = new ErpDivision();
        if(Yii::$app->request->post('ErpDivision') && isset(Yii::$app->request->post('ErpDivision')['id'])){
            $division = ErpDivision::findOne(Yii::$app->request->post('ErpDivision')['id']);
            if($division!== null)
                $division->delete();
        }

        return $this->renderPartial('delete-division-modal',['model'=>$model]);
    }




    public function actionDeletePositionModal()
    {
        $model = new ErpPosition;
        if(Yii::$app->request->post('ErpPosition') && isset(Yii::$app->request->post('ErpPosition')['id'])){
            $position = ErpPosition::findOne(Yii::$app->request->post('ErpPosition')['id']);
            if($position!== null)
                $position_count = ErpPosition::find()->where(['division_id'=>Yii::$app->request->post('ErpPosition')['division_id']])->count();
                if($position_count==1){
                    $division = ErpDivision::findOne(Yii::$app->request->post('ErpPosition')['division_id']);
                    $division->delete();
                }
                $position->delete();
        }

        return $this->renderPartial('delete-position-modal',['model'=>$model]);
    }



    public function actionUserPositionModal()
    {
        $user_position = new ErpUserPosition();
        if($user_position->load(Yii::$app->request->post())){
            $user_position->save();
        }
        return $this->renderPartial('user-position-modal',['user_position'=>$user_position]);
    }

    public function actionDeleteUserPositionModal()
    {
        $user_position = new ErpUserPosition();
        if(Yii::$app->request->post() && isset(Yii::$app->request->post('ErpUserPosition')['user_id'])){
            $position = ErpUserPosition::find()->where(['user_id'=>Yii::$app->request->post('ErpUserPosition')['user_id']])->one();
            if($position)
                $position->delete();
        }
        return $this->renderPartial('delete-user-position-modal',['user_position'=>$user_position]);
    }



    public function actionCreateUser(){
        $model = new ErpUser();
        if($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect('index');
        return $this->render('create-user',['model'=>$model]);
    }



    public function actionDismissedUser(){
        $model = new ErpUser();
        if($model->load(Yii::$app->request->post()) && $model->dismissed()){
                    return$this->redirect('index');
            }
        return $this->render('dismissed-user',['model'=>$model]);
    }


    public function actionUpdateDivisionModal($division_id)
    {
        $model = ErpDivision::findOne($division_id);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->renderPartial('create-division-modal',['model'=>$model, 'flag'=>true]);
        }
        return $this->renderPartial('create-division-modal',['model'=>$model]);
    }


}
