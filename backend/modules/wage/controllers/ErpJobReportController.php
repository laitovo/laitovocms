<?php

namespace backend\modules\wage\controllers;

use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\PieceWorkPayment;
use Yii;
use backend\modules\laitovo\models\ErpUsersReport;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\laitovo\Config;
use yii\data\ArrayDataProvider;
use backend\modules\laitovo\models\LaitovoReworkAct;


/**
 * ErpJobReportController implements the CRUD actions for ErpUsersReport model.
 */
class ErpJobReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpUsersReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $normal = Config::findOne(2)->json('hourrate')?:0 ;

        $query=ErpUsersReport::find();

        $query->groupBy(['user_id']);
        $query->select(['user_id', 'job_count' => 'SUM(job_count * job_rate * ' . $normal . ')']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['id'=>Yii::$app->request->get('search')],
            ]);
        }


        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPieceWorkPayment(){

        $model =  new PieceWorkPayment();

        if(Yii::$app->request->get()){
            $model->load(Yii::$app->request->get());
        }
        $dataProviderProduction = $model->generalReport(ErpUser::TYPE_PRODUCTION);
        $dataProviderSupport = $model->generalReport(ErpUser::TYPE_SUPPORT);
        $rework_count = LaitovoReworkAct::find()->where(['and',
            ['>', 'created_at', $model->dateFrom1],
            ['<', 'created_at', $model->dateTo1],
        ])->count();



        return $this->render('piece-work-payment', [
            'dataProviderProduction' => $dataProviderProduction,
            'dataProviderSupport' => $dataProviderSupport,
            'model'=>$model,
            'rework_count'=>$rework_count
        ]);
    }


    public function actionPieceWorkPaymentDayList($user_id, $dateFrom=null, $dateTo=null){

        $model =  new PieceWorkPayment();
        $user = ErpUser::findOne($user_id);
        $model->dateFrom1=$dateFrom;
        $model->dateTo1=$dateTo;

        if(Yii::$app->request->get())
        {
            $model->load(Yii::$app->request->get());
        }


        $dataProvider = $model->dayListReport($user_id);


        return $this->render('piece-work-payment-day-list', [
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'user'=>$user,
        ]);
    }

    public function actionReportDay($user_id, $date){

        $model =  new PieceWorkPayment();
        $user = ErpUser::findOne($user_id);

        if(Yii::$app->request->get())
            $model->load(Yii::$app->request->get());

        $dataProviderNaryad = $model->dayReportNaryad($user_id, $date, Yii::$app->request->queryParams)['dataProvider'];
        $dataProviderRework = $model->dayReportRework($user_id, $date, Yii::$app->request->queryParams)['dataProvider'];
        $dataProviderFine = $model->dayReportFine($user_id, $date)['dataProvider'];
        $dataProviderPremium = $model->dayReportPremium($user_id, $date)['dataProvider'];
        $sum_naryad = $model->dayReportNaryad($user_id, $date, Yii::$app->request->queryParams)['sum_naryad'];
        $sum_deduction = $model->dayReportRework($user_id, $date, Yii::$app->request->queryParams)['sum_deduction'];
        $sum_fine = $model->dayReportFine($user_id, $date)['sum_fine'];
        $sum_premium = $model->dayReportPremium($user_id, $date)['sum_premium'];
        $sum_total =  $sum_naryad  - $sum_deduction  - $sum_fine + $sum_premium;

        return $this->render('report-day', [
            'dataProviderNaryad' => $dataProviderNaryad,
            'dataProviderRework'=>$dataProviderRework,
            'dataProviderFine'=>$dataProviderFine,
            'dataProviderPremium'=>$dataProviderPremium,
            'model'=>$model,
            'user'=>$user,
            'sum_naryad'=>$sum_naryad,
            'sum_deduction'=>$sum_deduction,
            'sum_fine'=>$sum_fine,
            'sum_premium'=>$sum_premium,
            'sum_total'=>$sum_total,
        ]);
    }

    /**
     * @return string
     * Выводим заработки для использования сотрудников
     */

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function actionUserList()
    {
        $query = ErpUser::find()->where(['type'=>ErpUser::TYPE_PRODUCTION, 'status'=>ErpUser::STATUS_WORK]);

        $dataProvider = new ActiveDataProvider([
            'query'=>$query,
        ]);

        return $this->render('user-list', [
            'dataProvider'=>$dataProvider
        ]);
    }


    public function actionPaymentDayListDispatcher($user_id)
    {
        $model =  new PieceWorkPayment();
        $user = ErpUser::findOne($user_id);

        if(Yii::$app->request->get())
        {
            $model->load(Yii::$app->request->get());
        }

        $dataProvider = $model->dayListReport($user_id);

        return $this->render('payment-day-list-dispatcher', [
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'user'=>$user,
        ]);
    }


    public function actionReportDayDispatcher($user_id, $date)
    {
        $model =  new PieceWorkPayment();
        $user = ErpUser::findOne($user_id);

        if(Yii::$app->request->get())
            $model->load(Yii::$app->request->get());

        $dataProviderNaryad = $model->dayReportNaryad($user_id, $date, Yii::$app->request->queryParams)['dataProvider'];
        $dataProviderRework = $model->dayReportRework($user_id, $date, Yii::$app->request->queryParams)['dataProvider'];
        $dataProviderFine = $model->dayReportFine($user_id, $date)['dataProvider'];
        $sum_naryad = $model->dayReportNaryad($user_id, $date, Yii::$app->request->queryParams)['sum_naryad'];
        $sum_rework = $model->dayReportRework($user_id, $date, Yii::$app->request->queryParams)['sum_deduction'];
        $sum_fine = $model->dayReportFine($user_id, $date)['sum_fine'];
        $sum_deduction = $sum_fine+$sum_rework;
        $sum_total = $sum_naryad  - $sum_deduction ;

        return $this->render('report-day-dispatcher', [
            'dataProviderNaryad' => $dataProviderNaryad,
            'dataProviderRework'=>$dataProviderRework,
            'dataProviderFine'=>$dataProviderFine,
            'model'=>$model,
            'user'=>$user,
            'sum_naryad'=>$sum_naryad,
            'sum_deduction'=>$sum_deduction,
            'sum_total'=>$sum_total
        ]);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
}
