<?php

namespace backend\modules\wage\controllers;

use Yii;
use backend\modules\laitovo\models\LaitovoReworkAct;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\laitovo\models\ErpTerminal;
use backend\modules\laitovo\models\ErpNaryad;
use common\models\laitovo\ErpLocation;

/**
 * LaitovoReworkActController implements the CRUD actions for LaitovoReworkAct model.
 */
class ErpReworkActController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionTerminal()
    {
        $model = new ErpTerminal();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        return $this->render('terminal', [
            'model' => $model,

        ]);

    }

    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = new ErpTerminal();
        $barcode = $model->toLatin($barcode);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $naryad = ErpNaryad::find()->where(['barcode' => $barcode])->one();

        if (!$naryad)
        {
            $barcode = 'B'.trim(str_replace("/", "D", $barcode));
            $naryad = ErpNaryad::find()->where(['barcode' => $barcode])->one();
        }

        if ($naryad && ($naryad->status != ErpNaryad::STATUS_READY && $naryad->status != ErpNaryad::STATUS_FROM_SKLAD && $naryad->status != ErpNaryad::STATUS_CANCEL)){
            //поиска наряда  
            $response['status'] = 'success'; 
            $response['message'] = $naryad->name;
            $response['manual'] = $naryad->id;

        }
        elseif($naryad && ($naryad->status == ErpNaryad::STATUS_READY || $naryad->status == ErpNaryad::STATUS_FROM_SKLAD || $naryad->status == ErpNaryad::STATUS_CANCEL )){
            $response['status'] = 'error';
            $response['message'] = 'Для этого наряда нельзя составить акт'; 
        }else {
            $response['status'] = 'error';
            $response['message'] = 'Наряд не найден';
        }

        return $response;
    }

    /**
     * Lists all LaitovoReworkAct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LaitovoReworkAct;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'=>$searchModel
        ]);
    }

    /**
     * Displays a single LaitovoReworkAct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LaitovoReworkAct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($naryad_id)
    {
        $model = new LaitovoReworkAct();
        $model->naryad_id = $naryad_id;
        $count_act_naryad = LaitovoReworkAct::find()->where(['naryad_id'=>$naryad_id])->count()+1;
        $model->rework_number = "". $count_act_naryad ."-". $model->naryad->number ."";
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'naryad_id'=>$naryad_id,
                
            ]);
        }
    }
    public function actionNaryadAct($naryad_id)
    {
     $searchModel = new LaitovoReworkAct;
        $dataProvider = $searchModel->searchNaryadAct($naryad_id, Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'=>$searchModel
        ]);   
    }


    public function actionPrintCoveringLetter($id)
    {
        
        return $this->renderPartial('covering-letter', [
            'model' => $this->findModel($id),
           
            
        ]);         
    }
    public function actionPrintRework($id)
    {
        
        return $this->renderPartial('rework-act', [
            'model' => $this->findModel($id),
           
            
        ]);         
    }
    
    

    /**
     * Updates an existing LaitovoReworkAct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LaitovoReworkAct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LaitovoReworkAct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LaitovoReworkAct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LaitovoReworkAct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
