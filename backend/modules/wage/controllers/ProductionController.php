<?php

namespace backend\modules\wage\controllers;

use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\StorageState;
use common\models\logistics\ArticleStatistics as ArticleStatistics;
use Yii;
use common\models\laitovo\ProductionScheme;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\laitovo\models\ErpProductType;
use backend\modules\laitovo\models\ErpScheme;
use common\models\laitovo\MaterialProductionScheme;
use common\models\laitovo\JobProductionScheme;
use backend\modules\laitovo\models\ErpJobScheme;
use backend\modules\laitovo\models\ErpLocation;
use common\models\laitovo\Config;

/**
 * 
 * ProductionController implements the CRUD actions for ProductionScheme model.
 */
class ProductionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductionScheme models.
     * @return mixed
     */
    public function actionIndex()
    {
        //добавил join для сортировки
        $query = ProductionScheme::find()->joinWith('product',
            'product.id = prodactionScheme.id');
       
        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['like', 'production_scheme.title', Yii::$app->request->get('search')]);
        }
        //убрал начальную сортировку по алфавиту из query
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //добавил сортировку по категории
        $sort = $dataProvider->getSort();
        $sort->defaultOrder = ['title'=>SORT_ASC];
        $sort->attributes = array_merge($sort->attributes, [
            'product.category' => [
                'asc' => ['laitovo_erp_product_type.category' => SORT_ASC],
                'desc' => ['laitovo_erp_product_type.category' => SORT_DESC],
            ]
        ]);
        $dataProvider->setSort($sort);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductionScheme model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $totalMaterial = $model->materialCount();
        $totalJobClips = $model->totalJobCount();
        $materialProduction = new ActiveDataProvider([
            'query' => ErpLocation::find()->where(['in','id', ErpScheme::findOne($model->scheme_id)->locations])->orderBy('sort'),
        ]);
        $jobProduction = new ActiveDataProvider([
            'query' => ErpLocation::find()->where(['in','id', ErpScheme::findOne($model->scheme_id)->locations])->orderBy('sort'),
        ]);
        
        return $this->render('view', [
            'model' => $model,
            'materialProduction' => $materialProduction,
            'jobProduction' => $jobProduction,
            'totalMaterial'=>$totalMaterial,
            'totalJobClips'=>$totalJobClips,
        ]);
    }

    /**
     * Creates a new ProductionScheme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductionScheme();
        $model2 = new ErpProductType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'model2' => $model2,
            ]);
        }
    }
    public function actionCreateProductTypeAjax()
    {
        $model = new ErpProductType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'message'=>'Продукт успешно добавлен',
                'id'=>$model->id,
            ];
        } else {
            return $this->renderPartial('create-product-type', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProductionScheme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model2 = new ErpProductType();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'model2' => $model2,
            ]);
        }
    }

    /**
     * Deletes an existing ProductionScheme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionStatistics()
    {
        $products = ErpProductType::find()->all();
        $total = 0;

        foreach ($products as $product) {
            $scheme = ProductionScheme::find()->where(['product_id' => $product->id])->one();

            if (!$scheme) continue;

            $totalMaterial = $scheme->materialCount();
            $totalJobCount = $scheme->totalJobCount();
            $sum = $totalMaterial + $totalJobCount;

            $rule = $product->rule;
            $rule = trim($rule, '#');
            $rule = str_replace('\w', '[[:alnum:]]', $rule);
            $rule = str_replace('\d', '[[:digit:]]', $rule);

            $query = StorageState::find()->alias('t')->joinWith('upn')->where(['REGEXP', 'article', $rule]);
            $query->select(['upn_id']);
            $rows = $query->all();
            $ids = ArrayHelper::map($rows, 'upn_id', 'upn_id');

            $query = Naryad::find();
            $query->where(['in','id',$ids]);
            $query->andWhere(['reserved_id' => null]);
            $query->select('count(id) as count');
            $column = $query->column();

            $result = round($sum * $column[0],2);
            $total += $result;

            echo $product->title . ' : ' . $result . ' руб. <br />' ;
        }
        echo "<hr />";

        echo "Всего: $total руб. <br />";
    }

    /**
     * Finds the ProductionScheme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductionScheme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductionScheme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
