<?php

$this->render('@backend/modules/wage/views/menu');

$this->title = Yii::t('app','Изменить премию');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['/laitovo/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Журнал премий'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'users' => $users,
]) ?>
