<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->render('@backend/modules/wage/views/menu');

$this->title = Yii::t('app','Просмотр премии');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['/laitovo/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Журнал премий'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="form-group">
    <?= Html::a('<i class="icon wb-edit"></i> ' . Yii::t('app','Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('<i class="icon wb-trash"></i> ' . Yii::t('app','Удалить'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger deleteconfirm',
        'data' => [
            'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
            'method' => 'post',
        ],
    ]) ?>
    <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
</div>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'user.name',
            'label' => 'Сотрудник',
        ],
        [
            'attribute' => 'act.rework_number',
            'label' => 'Документ основание',
        ],
        [
            'attribute' => 'amount',
            'label' => 'Размер премии',
        ],
        [
            'label' => 'Начислено',
            'value' => function($data) {
                return $data->is_paid ? 'Да' : 'Нет';
            },
        ],
        [
            'attribute' => 'paid_at',
            'format'=> ['datetime', 'php:d.m.Y H:i:s'],
            'label' => 'Дата начисления',
        ],
        [
            'attribute' => 'updated_at',
            'format'=> ['datetime', 'php:d.m.Y H:i:s'],
            'label' => 'Дата изменения',
        ],
        [
            'attribute' => 'updater.name',
            'label' => 'Последний редактор',
        ],
    ],
]) ?>
