<?php

namespace backend\modules\wage\modules\premium\models;

use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\LaitovoReworkAct;
use common\models\LogActiveRecord;
use common\models\user\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Класс Премия
 *
 * Class Premium
 * @package backend\modules\laitovo\wage\premium\models
 */
class Premium extends LogActiveRecord
{
    /**
     * Наименование таблицы в базе данных
     *
     * @return string
     */
    public static function tableName()
    {
        return 'laitovo_erp_premium';
    }

    /**
     * Функция, вызываемая перед сохранением модели
     * Автоматически заполняем/обнуляем поле "Дата назначения" при смене поля "Назначена или нет"
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->is_paid) {
            $this->paid_at = time();
        } else {
            $this->paid_at = null;
        }

        return parent::beforeSave($insert);
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'amount'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id'], 'message' => 'Сотрудник не найден'],
            [['amount', 'is_paid'], 'integer', 'message' => 'Это поле должно быть целым числом'],
        ];
    }

    /**
     * Поведения (примеси)
     * Автоматически заполняем поля created_at, author_id, updated_at, updater_id
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id', 'updater_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * Сотрудник, которому предназначается премия
     *
     * @return ErpUser|null
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }

    /**
     * Акт штрафа, являющийся основанием премии
     * Это для премий, которые создаются автоматически при создании акта штрафа
     *
     * @return LaitovoReworkAct|null
     */
    public function getAct()
    {
        return $this->hasOne(LaitovoReworkAct::className(), ['id' => 'rework_act_id']);
    }

    /**
     * Создатель премии
     *
     * @return User|null
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * Последний редактор премии
     *
     * @return User|null
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }
}