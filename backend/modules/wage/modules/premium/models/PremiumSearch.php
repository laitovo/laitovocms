<?php

namespace backend\modules\wage\modules\premium\models;

use yii\data\ActiveDataProvider;

/**
 * Класс для фильтрации списка премий
 *
 * Class PremiumSearch
 * @package backend\modules\wage\modules\premium\models
 */
class PremiumSearch extends Premium
{
    public $user_name;
    public $act_number;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_name', 'act_number', 'is_paid', 'paid_at'], 'safe'],
        ];
    }

    /**
     * Функция возвращает отфильтрованный список премий
     *
     * @param array $params Параметры фильтрации
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Premium::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if ($this->load($params)) {
            if ($this->is_paid != 'all') {
                $query->andFilterWhere(['laitovo_erp_premium.is_paid' => $this->is_paid]);
            }
            if ($this->paid_at) {
                $time = strtotime($this->paid_at);
                $query->andFilterWhere(['between', 'paid_at', $time, $time + 60*60*24-1]);
            }
            $query->joinWith('user as user');
            $query->joinWith('act as act');
            $query->andFilterWhere(['like', 'user.name', $this->user_name]);
            $query->andFilterWhere(['like', 'act.rework_number', $this->act_number]);
        }

        return $dataProvider;
    }
}