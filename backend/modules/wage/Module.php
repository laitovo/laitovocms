<?php

namespace backend\modules\wage;

/**
 * wage module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\wage\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        \Yii::$app->layout='2columns';
        parent::init();

        // custom initialization code goes here
        $this->modules = [
            'premium' =>
                ['class' => 'backend\modules\wage\modules\premium\Module'],
//            'custom-label' =>
//                ['class' => 'backend\modules\laitovo\modules\customLabel\Module'],
//            'properties' =>
//                ['class' => 'backend\modules\laitovo\modules\properties\Module'],
//            'instruction-type' =>
//                ['class' => 'backend\modules\laitovo\modules\instructionType\Module'],
//            'instruction' =>
//                ['class' => 'backend\modules\laitovo\modules\instruction\Module'],
//            'universal-instruction' =>
//                ['class' => 'backend\modules\laitovo\modules\universalInstruction\Module'],
        ];
    }
}
