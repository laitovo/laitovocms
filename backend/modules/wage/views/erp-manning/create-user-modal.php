<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpPosition;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpManning */


$this->params['breadcrumbs'][] = ['label' => 'Штатное расписание', 'url' => ['index']];

?>
<div class="erp-user-create">
    <h1>Добавить нового сотрудника</h1>

    <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'location_id')->dropDownList(ErpLocation::locationList()) ?>

        <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>

        <?= $form->field($model, 'type')->dropDownList($model->types) ?>

        <?= $form->field($model, 'factor')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Создать', ['class' =>'btn btn-outline btn-round btn-success',
                'onclick'=>'$("#modalView.in").modal("hide");']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>