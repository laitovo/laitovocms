<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\laitovo\ErpDivision;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpPosition;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpManning */


$this->params['breadcrumbs'][] = ['label' => 'Штатное расписание', 'url' => ['index']];

if(isset($flag)) {
    ?>
    <script>
        $("#modalView.in").modal("hide");
    </script>
    <?
}

?>
<div class="erp-manning-update">

    <h1>Изменить штатное расписание для <?= $model->title ? $model->title : ''?></h1>

    <?php $form = ActiveForm::begin(['enableAjaxValidation'=>true, 'options' => ['data-pjax'=>'']]); ?>

    <?= $form->field($model, 'division_id')->dropDownList(ErpDivision::division_list()) ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'article')->textInput(['maxlength'=>4]) ?>

    <?= $form->field($model, 'salary_one')->textInput() ?>

    <?= $form->field($model, 'salary_two')->textInput() ?>

    <?= $form->field($model, 'car_compensation')->textInput() ?>

    <?= $form->field($model, 'surcharge')->textInput() ?>

    <?= $form->field($model, 'qualification')->textInput() ?>

    <?= $form->field($model, 'premium')->textInput() ?>

    <?= $form->field($model, 'kpi_one')->textInput() ?>

    <?= $form->field($model, 'kpi_two')->textInput() ?>

    <?= $form->field($model, 'kpi_three')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Изменить', ['class' =>'btn btn-outline btn-round btn-primary',
//            'onclick'=>'$("#modalView.in").modal("hide");'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>