<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpPosition;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpManning */

if(isset($flag)){?>
    <script>
        $("#modalView.in").modal("hide");
    </script>
<?
}

?>
<div class="erp-user-create">
   <? if($model->isNewRecord):?>
    <h1>Добавить новый отдел</h1>
   <?else:?>
       <h1>Редактировать отдел</h1>
   <? endif ?>
    <?php $form = ActiveForm::begin(['enableAjaxValidation'=>true, 'options' => ['data-pjax'=>'']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'article')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать': 'Изменить', ['class' =>'btn btn-outline btn-round btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>