<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpManning */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Штатное расписание', 'url' => ['index']];
$this->render('../menu');

?>
<div class="erp-manning-create">

    <h1>Добавить в штатное расписание</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
