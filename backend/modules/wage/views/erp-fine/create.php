<?php

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\ErpFine */

$this->params['breadcrumbs'][] = ['label' => 'Журнал штрафов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>
<div class="erp-fine-create">

    <h1>Добавить штраф</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
