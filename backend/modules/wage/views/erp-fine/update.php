<?php

use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\MaterialForm */

$this->params['breadcrumbs'][] = ['label' => 'Журнал штрафов', 'url' => ['index']];
$this->render('../menu');
?>
<div class="material-form-create">

    <h1>Изменить штраф № <?=$model->id?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>