<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('../menu');
?>
<div class="material-production-scheme-index">

    <h1>Журнал штрафов</h1>

    <p>
        <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить штраф')]) ?>

    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'show'=>['id', 'user_id', 'description', 'amount', 'created_at'],
        'columns' => [
            [
                'attribute'=>'id',
                'value'=>function($data){
                    return 'Штраф №'.$data->id;
                }
            ],
            [
                'attribute'=>'user_id',
                'value'=>function($data){
                    return $data->user_id ? $data->user->getName() : '';
                }
            ],
            'description',
            [
               'attribute'=>'amount',
                'value'=>function($data){
                    return $data->amount ? Yii::$app->formatter->asDecimal($data->amount): 0;
                }
            ],
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{update}{delete}'
            ],
        ],
    ]); ?>
</div>