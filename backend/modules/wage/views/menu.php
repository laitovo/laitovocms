<?php
use common\models\laitovo\Cars;
use common\models\laitovo\CarsAct;

/* @var $this yii\web\View */

$this->params['menuItems'][0]['label'] = '<span class="lead">Основное меню</span>'; 

if (in_array(Yii::$app->user->getId(),[21,121,122,123,124])) {

    $this->params['menuItems'][0]['items'][0]['label'] = 'Зарплата';

    $this->params['menuItems'][0]['items'][0]['items'][0]['label'] = Yii::t('app', 'Табель учета рабочего времени');
    $this->params['menuItems'][0]['items'][0]['items'][0]['url'] = ['/wage/report-card/index'];

    $this->params['menuItems'][0]['items'][0]['items'][1]['label'] = Yii::t('app', 'Архив табелей');
    $this->params['menuItems'][0]['items'][0]['items'][1]['url'] = ['/wage/report-card/index-archive'];

    $this->params['menuItems'][0]['items'][0]['items'][2]['label'] = Yii::t('app', 'Отчет по закрытым табелям');
    $this->params['menuItems'][0]['items'][0]['items'][2]['url'] = ['/wage/report-card/report-card-close'];

    $this->params['menuItems'][0]['items'][0]['items'][3]['label'] = Yii::t('app', 'Оплата сдельщиков');
    $this->params['menuItems'][0]['items'][0]['items'][3]['url'] = ['/wage/erp-job-report/piece-work-payment'];

    $this->params['menuItems'][0]['items'][0]['items'][4]['label'] = Yii::t('app', 'Cводный отчет заработной платы');
    $this->params['menuItems'][0]['items'][0]['items'][4]['url'] = ['/wage/report-card/report-card-table'];

    $this->params['menuItems'][0]['items'][1]['label'] = 'Работники';

    $this->params['menuItems'][0]['items'][1]['items'][0]['label'] = Yii::t('app',     'Штатное расписание');
    $this->params['menuItems'][0]['items'][1]['items'][0]['url'] = ['/wage/erp-manning/index'];

    $this->params['menuItems'][0]['items'][1]['items'][1]['label'] = Yii::t('app', 'Документы');
    $this->params['menuItems'][0]['items'][1]['items'][1]['url'] = ['/wage/report-card/document-cause-index'];

    $this->params['menuItems'][0]['items'][1]['items'][2]['label'] = Yii::t('app', 'Журнал штрафов');
    $this->params['menuItems'][0]['items'][1]['items'][2]['url'] = ['/wage/erp-fine/index'];

    $this->params['menuItems'][0]['items'][1]['items'][3]['label'] = Yii::t('app', 'Журнал премий');
    $this->params['menuItems'][0]['items'][1]['items'][3]['url'] = ['/wage/premium/default/index'];

    $this->params['menuItems'][0]['items'][2]['label'] = 'Производство';

    $this->params['menuItems'][0]['items'][2]['items'][0]['label'] = Yii::t('app', 'Журнал видов работ');
    $this->params['menuItems'][0]['items'][2]['items'][0]['url'] = ['/wage/erp-job-types/index'];

    $this->params['menuItems'][0]['items'][2]['items'][1]['label'] = Yii::t('app', 'Карточки продуктов');
    $this->params['menuItems'][0]['items'][2]['items'][1]['url'] = ['/wage/production/index'];

    $this->params['menuItems'][0]['items'][2]['items'][2]['label'] = Yii::t('app', 'Наряды на доп. работы');
    $this->params['menuItems'][0]['items'][2]['items'][2]['url'] = ['/wage/erp-additional-work/index'];

    $this->params['menuItems'][0]['items'][2]['items'][3]['label'] = Yii::t('app', 'Акты переделок');
    $this->params['menuItems'][0]['items'][2]['items'][3]['url'] = ['/wage/erp-rework-act/index'];

    $this->params['menuItems'][0]['items'][2]['items'][4]['label'] = Yii::t('app', 'Рапорта выполненных работ');
    $this->params['menuItems'][0]['items'][2]['items'][4]['url'] = ['/wage/erp-job-report/user-list'];

} else {
    $this->params['menuItems'][0]['items'][0]['label'] = Yii::t('app', 'Оплата сдельщиков');
    $this->params['menuItems'][0]['items'][0]['url'] = ['/wage/erp-job-report/piece-work-payment'];

    $this->params['menuItems'][0]['items'][1]['label'] = Yii::t('app', 'Наряды на доп. работы');
    $this->params['menuItems'][0]['items'][1]['url'] = ['/wage/erp-additional-work/index'];
}

//if (!in_array(Yii::$app->user->getId(), [40,41,43,44,120,62])):
//
//#Меню отчетов
//$this->params['menuItems'][0]['items'][0]['label'] = 'Отчеты';
//
//$this->params['menuItems'][0]['items'][0]['items'][0]['label'] = Yii::t('app', 'Отчет о ткани');
//$this->params['menuItems'][0]['items'][0]['items'][0]['url'] = ['/laitovo/erp-leader-report/textile-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][1]['label'] = Yii::t('app', 'Отчет о заказах');
//$this->params['menuItems'][0]['items'][0]['items'][1]['url'] = ['/laitovo/erp-leader-report/order-time-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][2]['label'] = Yii::t('app', 'Отчет по продуктам');
//$this->params['menuItems'][0]['items'][0]['items'][2]['url'] = ['/laitovo/erp-leader-report/product-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][3]['label'] = Yii::t('app', 'Отчет по участкам');
//$this->params['menuItems'][0]['items'][0]['items'][3]['url'] = ['/laitovo/erp-leader-report/location-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][4]['label'] = Yii::t('app', 'Отчет о просрочке');
//$this->params['menuItems'][0]['items'][0]['items'][4]['url'] = ['/laitovo/erp-leader-report/timeout-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][5]['label'] = Yii::t('app', 'Отчет по расходу материалов');
//$this->params['menuItems'][0]['items'][0]['items'][5]['url'] = ['/laitovo/material/report-material-cost'];
//
//if (in_array(Yii::$app->user->getId(), [21,2,11,27,37,30, 36])):
//
//$this->params['menuItems'][0]['items'][0]['items'][6]['label'] = Yii::t('app', 'Отчет по дилерам');
//$this->params['menuItems'][0]['items'][0]['items'][6]['url'] = ['/laitovo/erp-orders-user-report/index'];
//
//endif;
//
//$this->params['menuItems'][0]['items'][0]['items'][7]['label'] = Yii::t('app', 'Отчет по остаткам на складе');
//$this->params['menuItems'][0]['items'][0]['items'][7]['url'] = ['/laitovo/erp-leader-report/balance'];
//
//#Меню освоения автомобилей
//$this->params['menuItems'][0]['items'][1]['label'] = 'Техническая служба';
//
//$this->params['menuItems'][0]['items'][1]['items'][0]['label'] =
//	 Yii::t('app', 'Автомобили').(Cars::receiveNotCheckedCarsCount() || Cars::receiveNotCheckedReworkCarsCount() ?
//	 	'<span class="badge">'.Cars::receiveNotCheckedCarsCount().' / '.Cars::receiveNotCheckedReworkCarsCount().'</span>' : '');
//
//$this->params['menuItems'][0]['items'][1]['items'][0]['url'] = ['/laitovo/cars/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][1]['label'] =
//	Yii::t('app', 'Акты')
//    .('<span class="badge">'.CarsAct::receiveMasteringActForCheckCount().' / '.CarsAct::receiveMasteringActForReworkCount().'</span>')
//    .(CarsAct::receiveChangesActForCheckCount() || CarsAct::receiveChangesActForReworkCount() ?
//        '<span class="badge">'.CarsAct::receiveChangesActForCheckCount().' / '.CarsAct::receiveChangesActForReworkCount().'</span>' : '');
//
//$this->params['menuItems'][0]['items'][1]['items'][1]['url'] = ['/laitovo/carsact/index'];
//
//
//
//$this->params['menuItems'][0]['items'][1]['items'][3]['label'] = Yii::t('app', 'Отчет по а/м');
//$this->params['menuItems'][0]['items'][1]['items'][3]['url'] = ['/laitovo/cars/report'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['label'] = Yii::t('app', 'Продукция');
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][0]['label'] = Yii::t('app', 'Маршрутизация ');
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][0]['url'] = ['/laitovo/erp-scheme/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][1]['label'] = Yii::t('app', 'Виды продуктов');
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][1]['url'] = ['/laitovo/erp-product-type/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][2]['label'] = Yii::t('app', 'Журнал карточек продуктов');
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][2]['url'] = ['/laitovo/production/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['label'] = Yii::t('app', 'Справочники');
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][0]['label'] = Yii::t('app', 'Виды оконных проемов');
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][0]['url'] = ['/laitovo/properties/window-opening-type/crud/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][1]['label'] = Yii::t('app', 'Бренды');
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][1]['url'] = ['/laitovo/properties/brand/crud/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][2]['label'] = Yii::t('app', 'Виды исполнений');
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][2]['url'] = ['/laitovo/properties/execution-type/crud/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][3]['label'] = Yii::t('app', 'Виды тканей');
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][3]['url'] = ['/laitovo/properties/cloth-type/crud/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][4]['label'] = Yii::t('app', 'Виды инструкций');
//$this->params['menuItems'][0]['items'][1]['items'][4]['items'][3]['items'][4]['url'] = ['/laitovo/instruction-type/crud/index'];
//
//#Меню производсвенных процессов
//$this->params['menuItems'][0]['items'][2]['label'] = 'Производственный цех';
//
//$this->params['menuItems'][0]['items'][2]['items'][0]['label'] = Yii::t('app', 'Монитор производства (главная)');
//$this->params['menuItems'][0]['items'][2]['items'][0]['url'] = ['/laitovo/erp/index'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['label'] = Yii::t('app', 'Диспетчер ');
//
//// $this->params['menuItems'][0]['items'][2]['items'][1]['items'][0]['label'] = Yii::t('app', 'Ручная выдача нарядов');
//// $this->params['menuItems'][0]['items'][2]['items'][1]['items'][0]['url'] = ['erp/main-terminal'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][1]['label'] = Yii::t('app', 'Выдача нарядов на работы');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][1]['url'] = ['/laitovo/erp-additional-work/index'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][2]['label'] = Yii::t('app', 'Журнал актов');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][2]['url'] = ['/laitovo/erp-rework-act/index'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][3]['label'] = Yii::t('app', 'Журнал заказов');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][3]['url'] = ['/laitovo/erp-order/index'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][4]['label'] = Yii::t('app', 'Журнал нарядов');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][4]['url'] = ['/laitovo/erp-naryad/index'];
//
//if(Yii::$app->user->getId()==2 || Yii::$app->user->getId()==37 || Yii::$app->user->getId()==21):
//
//    $this->params['menuItems'][0]['items'][2]['items'][1]['items'][5]['label'] = Yii::t('app', 'Журнал штрафов');
//    $this->params['menuItems'][0]['items'][2]['items'][1]['items'][5]['url'] = ['/laitovo/erp-fine/index'];
//
//endif;
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][6]['label'] = Yii::t('app', 'Рапорта выполненных работ');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][6]['url'] = ['/laitovo/erp-job-report/user-list'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][7]['label'] = Yii::t('app', 'Журнал лекал');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][7]['url'] = ['/laitovo/pattern-register/index'];
//
////$this->params['menuItems'][0]['items'][2]['items'][1]['items'][8]['label'] = Yii::t('app', 'Ручная выдача');
////$this->params['menuItems'][0]['items'][2]['items'][1]['items'][8]['url'] = ['/laitovo/erp/main-terminal'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][9]['label'] = Yii::t('app', 'Ручная выдача на работника');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][9]['url'] = ['/laitovo/erp/main-user-terminal'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][10]['label'] = Yii::t('app', 'Ручная выдача по лекалу');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][10]['url'] = ['/laitovo/erp/main-lekalo-terminal'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][11]['label'] = Yii::t('app', 'Ручная выдача по литере');
//$this->params['menuItems'][0]['items'][2]['items'][1]['items'][11]['url'] = ['/laitovo/erp/main-literal-terminal'];
//
//
//
//$this->params['menuItems'][0]['items'][2]['items'][2]['label'] = 'Терминалы нарядов';
//
//$this->params['menuItems'][0]['items'][2]['items'][2]['items'][0]['label'] = Yii::t('app', 'Терминал - выдача');
//$this->params['menuItems'][0]['items'][2]['items'][2]['items'][0]['url'] = ['/laitovo/erp/terminal-vidacha'];
//
//$this->params['menuItems'][0]['items'][2]['items'][2]['items'][1]['label'] = Yii::t('app', 'Терминал - сдача');
//$this->params['menuItems'][0]['items'][2]['items'][2]['items'][1]['url'] = ['/laitovo/erp/terminal-sdacha'];
//
//
//
//$this->params['menuItems'][0]['items'][2]['items'][8]['label'] = Yii::t('app', 'Маршрутизация ');
//$this->params['menuItems'][0]['items'][2]['items'][8]['url'] = ['/laitovo/erp-scheme/index'];
//
//$this->params['menuItems'][0]['items'][2]['items'][10]['label'] = Yii::t('app', 'Печать инструкций');
//$this->params['menuItems'][0]['items'][2]['items'][10]['url'] = ['/laitovo/print/terminal'];
//
//
//#Меню регистрации сотрудников
//$this->params['menuItems'][0]['items'][3]['label'] = 'Трудовые ресурсы';
//
//$this->params['menuItems'][0]['items'][3]['items'][0]['label'] = Yii::t('app', 'Участки');
//$this->params['menuItems'][0]['items'][3]['items'][0]['url'] = ['/laitovo/erp-location/index'];
//
//$this->params['menuItems'][0]['items'][3]['items'][1]['label'] = Yii::t('app', 'Рабочие места');
//$this->params['menuItems'][0]['items'][3]['items'][1]['url'] = ['/laitovo/erp-location-workplace/index'];
//
//$this->params['menuItems'][0]['items'][3]['items'][2]['label'] = Yii::t('app', 'Сотрудники');
////$this->params['menuItems'][0]['items'][3]['items'][2]['url'] = ['erp-user/index'];
//    $this->params['menuItems'][0]['items'][3]['items'][2]['items'][0]['label'] = Yii::t('app', 'Журнал сотрудников');
//    $this->params['menuItems'][0]['items'][3]['items'][2]['items'][0]['url'] = ['/laitovo/erp-user/index'];
//
//if(in_array(Yii::$app->user->getId(),[2,37,21,45,47])):
//    $this->params['menuItems'][0]['items'][3]['items'][2]['items'][1]['label'] = Yii::t('app', 'Штатное расписание');
//    $this->params['menuItems'][0]['items'][3]['items'][2]['items'][1]['url'] = ['/laitovo/erp-manning/index'];
//
//    $this->params['menuItems'][0]['items'][3]['items'][2]['items'][2]['label'] = Yii::t('app', 'Журнал должностей');
//    $this->params['menuItems'][0]['items'][3]['items'][2]['items'][2]['url'] = ['/laitovo/erp-manning/position-index'];
//endif;
//
//$this->params['menuItems'][0]['items'][3]['items'][3]['label'] = Yii::t('app', 'Ручная регистрация');
//$this->params['menuItems'][0]['items'][3]['items'][3]['url'] = ['/laitovo/erp-location-workplace-register/manual-registration'];
//
//$this->params['menuItems'][0]['items'][3]['items'][4]['label'] = Yii::t('app', 'Монитор рабочих мест');
//$this->params['menuItems'][0]['items'][3]['items'][4]['url'] = ['/laitovo/erp-location-workplace/monitor'];
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['label'] = 'Учет рабочего времени';
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][0]['label'] = Yii::t('app', 'Журнал регистрации сотрудников');
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][0]['url'] = ['/laitovo/erp-location-workplace-register/index'];
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][1]['label'] = Yii::t('app', 'Терминал');
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][1]['url'] = ['/laitovo/erp/terminal-register'];
//
//if(in_array(Yii::$app->user->getId(),[2,37,21,45,47,14])):
//
//    //табель учета рабочего времени
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][4]['label'] = Yii::t('app', 'Табель учета рабочего времени');
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][4]['url'] = ['/laitovo/report-card/index'];
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][5]['label'] = Yii::t('app', 'Архив табелей');
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][5]['url'] = ['/laitovo/report-card/index-archive'];
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][6]['label'] = Yii::t('app', 'Календарь выходных');
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][6]['url'] = ['/laitovo/report-card/weekend'];
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][7]['label'] = Yii::t('app', 'Документы');
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][7]['url'] = ['/laitovo/report-card/document-cause-index'];
//
//endif;
//
//if(Yii::$app->user->getId()==2 || Yii::$app->user->getId()==37 || Yii::$app->user->getId()==21 || Yii::$app->user->getId()==45 || Yii::$app->user->getId() == 47):
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][8]['label'] = Yii::t('app', 'Cводный отчет заработной платы');
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][8]['url'] = ['/laitovo/report-card/report-card-table'];
//
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][9]['label'] = Yii::t('app', 'Отчет по закрытым табелям');
//$this->params['menuItems'][0]['items'][3]['items'][5]['items'][9]['url'] = ['/laitovo/report-card/report-card-close'];
//
//endif;
//
//
////$this->params['menuItems'][0]['items'][3]['items'][5]['label'] = Yii::t('app', 'Непроизв. раб. места');
////$this->params['menuItems'][0]['items'][3]['items'][5]['url'] = ['erp-location-workplace/monitor-plan'];
//
//$this->params['menuItems'][0]['items'][4]['label'] = 'Снабжение и Материалы';
//
//$this->params['menuItems'][0]['items'][4]['items'][1]['label'] = Yii::t('app', 'Журнал изменений карточек материалов');
//$this->params['menuItems'][0]['items'][4]['items'][1]['url'] = ['/laitovo/material/material-list'];
//
//$this->params['menuItems'][0]['items'][4]['items'][3]['label'] = Yii::t('app', 'Журнал карточек материалов');
//$this->params['menuItems'][0]['items'][4]['items'][3]['url'] = ['/laitovo/material/materials'];
//
//$this->params['menuItems'][0]['items'][4]['items'][5]['label'] = Yii::t('app', 'Отчет по расходу материалов');
//$this->params['menuItems'][0]['items'][4]['items'][5]['url'] = ['/laitovo/material/report-material-cost'];
//
//#Меню заработной платы
//$this->params['menuItems'][0]['items'][5]['label'] = 'Экономика';
//
//$this->params['menuItems'][0]['items'][5]['items'][0]['label'] = 'Заработная плата';
//
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][0]['label'] = Yii::t('app', 'Журнал видов работ');
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][0]['url'] = ['/laitovo/erp-job-types/index'];
//
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][1]['label'] = Yii::t('app', 'Отчет по работникам');
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][1]['url'] = ['/laitovo/erp-user-report/index'];
//
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][2]['label'] = Yii::t('app', 'Сводный отчет');
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][2]['url'] = ['/laitovo/erp-job-report/index'];
//
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][2]['label'] = Yii::t('app', 'Оплата сдельщиков');
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][2]['url'] = ['/laitovo/erp-job-report/piece-work-payment'];
//
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][3]['label'] = Yii::t('app', 'Журнал премий');
//$this->params['menuItems'][0]['items'][5]['items'][0]['items'][3]['url'] = ['/laitovo/premium/default/index'];
//// $this->params['menuItems'][0]['items'][3]['items'][2]['label'] = Yii::t('app', 'КПД работников по участкам');
//// $this->params['menuItems'][0]['items'][3]['items'][2]['url'] = ['erp-job-location-rate/index'];
//
//// $this->params['menuItems'][0]['items'][3]['items'][3]['label'] = Yii::t('app', 'КПД работников по типам работ');
//// $this->params['menuItems'][0]['items'][3]['items'][3]['url'] = ['erp-job-type-rate/index'];
//
//
//
//$this->params['menuItems'][0]['items'][5]['items'][1]['label'] = 'Производительность';
//
//$this->params['menuItems'][0]['items'][5]['items'][1]['items'][0]['label'] = Yii::t('app', 'Группы продуктов');
//$this->params['menuItems'][0]['items'][5]['items'][1]['items'][0]['url'] = ['/laitovo/erp-product-group/index'];
//#Меню продукции
//
//$this->params['menuItems'][0]['items'][6]['label'] = 'Логистика';
//
//$this->params['menuItems'][0]['items'][6]['items'][0]['label'] = Yii::t('app', 'Наряды ОТК');
//$this->params['menuItems'][0]['items'][6]['items'][0]['url'] = ['/laitovo/otk/index'];
//
//$this->params['menuItems'][0]['items'][6]['items'][1]['label'] = Yii::t('app', 'Этикетки англия');
//$this->params['menuItems'][0]['items'][6]['items'][1]['url'] = ['/laitovo/custom-label/base/index'];
//
//if (in_array(Yii::$app->user->getId(), [2,3,21,48,53,120,62])):
//
//$this->params['menuItems'][0]['items'][7]['label'] = 'Настройки';
//
//$this->params['menuItems'][0]['items'][7]['items'][0]['label'] = Yii::t('app', 'Автомобили');
//$this->params['menuItems'][0]['items'][7]['items'][0]['url'] = ['/laitovo/settings/update', 'id' => 1];
//
//$this->params['menuItems'][0]['items'][7]['items'][1]['label'] = Yii::t('app', 'Производство');
//$this->params['menuItems'][0]['items'][7]['items'][1]['url'] = ['/laitovo/settings/update', 'id' => 2];
//
//endif;
//
//endif;
//
//if (Yii::$app->user->getId() == 41 ) :
//
//$this->params['menuItems'][0]['items'][0]['label'] = Yii::t('app', 'Терминал - выдача');
//$this->params['menuItems'][0]['items'][0]['url'] = ['/laitovo/erp/terminal-vidacha'];
//
//$this->params['menuItems'][0]['items'][1]['label'] = Yii::t('app', 'Печать инструкций');
//$this->params['menuItems'][0]['items'][1]['url'] = ['/laitovo/print/terminal'];
//
//endif;
//
//if (Yii::$app->user->getId() == 40 ) :
//
//$this->params['menuItems'][0]['items'][0]['label'] = Yii::t('app', 'Терминал - сдача');
//$this->params['menuItems'][0]['items'][0]['url'] = ['/laitovo/erp/terminal-sdacha'];
//
//$this->params['menuItems'][0]['items'][1]['label'] = Yii::t('app', 'Печать инструкций');
//$this->params['menuItems'][0]['items'][1]['url'] = ['/laitovo/print/terminal'];
//
//$this->params['menuItems'][0]['items'][2]['label'] = Yii::t('app', 'Наряды ОТК');
//$this->params['menuItems'][0]['items'][2]['url'] = ['/laitovo/otk/index'];
//
//$this->params['menuItems'][0]['items'][3]['label'] = Yii::t('app', 'Автомобили');
//$this->params['menuItems'][0]['items'][3]['url'] = ['/laitovo/cars/index'];
//
//
//endif;
//
//######################################################################################
//#МЕНЮ ДЛЯ ДИСПЕТЧЕРОВ
//
//if (Yii::$app->user->getId() == 43 || Yii::$app->user->getId() == 44) :
//
//#Меню отчетов
//$this->params['menuItems'][0]['items'][0]['label'] = 'Отчеты';
//
//$this->params['menuItems'][0]['items'][0]['items'][0]['label'] = Yii::t('app', 'Отчет о ткани');
//$this->params['menuItems'][0]['items'][0]['items'][0]['url'] = ['/laitovo/erp-leader-report/textile-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][1]['label'] = Yii::t('app', 'Отчет о заказах');
//$this->params['menuItems'][0]['items'][0]['items'][1]['url'] = ['/laitovo/erp-leader-report/order-time-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][2]['label'] = Yii::t('app', 'Отчет по продуктам');
//$this->params['menuItems'][0]['items'][0]['items'][2]['url'] = ['/laitovo/erp-leader-report/product-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][3]['label'] = Yii::t('app', 'Отчет по участкам');
//$this->params['menuItems'][0]['items'][0]['items'][3]['url'] = ['/laitovo/erp-leader-report/location-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][4]['label'] = Yii::t('app', 'Отчет о просрочке');
//$this->params['menuItems'][0]['items'][0]['items'][4]['url'] = ['/laitovo/erp-leader-report/timeout-report'];
//
//$this->params['menuItems'][0]['items'][0]['items'][5]['label'] = Yii::t('app', 'Отчет по расходу материалов');
//$this->params['menuItems'][0]['items'][0]['items'][5]['url'] = ['/laitovo/material/report-material-cost'];
//
//$this->params['menuItems'][0]['items'][1]['label'] = Yii::t('app', 'Диспетчер ');
//
//$this->params['menuItems'][0]['items'][1]['items'][0]['label'] = Yii::t('app', 'Главная');
//$this->params['menuItems'][0]['items'][1]['items'][0]['url'] = ['/laitovo/erp/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][1]['label'] = Yii::t('app', 'Монитор рабочих мест');
//$this->params['menuItems'][0]['items'][1]['items'][1]['url'] = ['/laitovo/erp-location-workplace/monitor'];
//
//// $this->params['menuItems'][0]['items'][1]['items'][2]['label'] = Yii::t('app', 'Ручная выдача нарядов');
//// $this->params['menuItems'][0]['items'][1]['items'][2]['url'] = ['erp/main-terminal'];
//
//$this->params['menuItems'][0]['items'][1]['items'][3]['label'] = Yii::t('app', 'Выдача нарядов на работы');
//$this->params['menuItems'][0]['items'][1]['items'][3]['url'] = ['/laitovo/erp-additional-work/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][4]['label'] = Yii::t('app', 'Оформить акт');
//$this->params['menuItems'][0]['items'][1]['items'][4]['url'] = ['/laitovo/erp-rework-act/terminal'];
//
//$this->params['menuItems'][0]['items'][1]['items'][5]['label'] = Yii::t('app', 'Журнал актов');
//$this->params['menuItems'][0]['items'][1]['items'][5]['url'] = ['/laitovo/erp-rework-act/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][6]['label'] = Yii::t('app', 'Журнал заказов');
//$this->params['menuItems'][0]['items'][1]['items'][6]['url'] = ['/laitovo/erp-order/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][7]['label'] = Yii::t('app', 'Журнал нарядов');
//$this->params['menuItems'][0]['items'][1]['items'][7]['url'] = ['/laitovo/erp-naryad/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][8]['label'] = Yii::t('app', 'Список сотрудников ');
//$this->params['menuItems'][0]['items'][1]['items'][8]['url'] = ['/laitovo/erp-user/index'];
//
//$this->params['menuItems'][0]['items'][1]['items'][9]['label'] = Yii::t('app', 'Рапорта выполненных работ');
//$this->params['menuItems'][0]['items'][1]['items'][9]['url'] = ['/laitovo/erp-job-report/user-list'];
//
//$this->params['menuItems'][0]['items'][1]['items'][10]['label'] = Yii::t('app', 'Журнал лекал');
//$this->params['menuItems'][0]['items'][1]['items'][10]['url'] = ['/laitovo/pattern-register/index'];
//
////$this->params['menuItems'][0]['items'][1]['items'][11]['label'] = Yii::t('app', 'Ручная выдача');
////$this->params['menuItems'][0]['items'][1]['items'][11]['url'] = ['/laitovo/erp/main-terminal'];
//
//$this->params['menuItems'][0]['items'][1]['items'][12]['label'] = Yii::t('app', 'Ручная выдача на работника');
//$this->params['menuItems'][0]['items'][1]['items'][12]['url'] = ['/laitovo/erp/main-user-terminal'];
//
//$this->params['menuItems'][0]['items'][1]['items'][13]['label'] = Yii::t('app', 'Ручная выдача по лекалу на изгиб');
//$this->params['menuItems'][0]['items'][1]['items'][13]['url'] = ['/laitovo/erp/main-lekalo-terminal'];
//
//$this->params['menuItems'][0]['items'][2]['label'] = 'Терминалы нарядов';
//
//$this->params['menuItems'][0]['items'][2]['items'][0]['label'] = Yii::t('app', 'Терминал - выдача');
//$this->params['menuItems'][0]['items'][2]['items'][0]['url'] = ['/laitovo/erp/terminal-vidacha'];
//
//$this->params['menuItems'][0]['items'][2]['items'][1]['label'] = Yii::t('app', 'Терминал - сдача');
//$this->params['menuItems'][0]['items'][2]['items'][1]['url'] = ['/laitovo/erp/terminal-sdacha'];
//
//$this->params['menuItems'][0]['items'][3]['label'] = Yii::t('app', 'Печать инструкций');
//$this->params['menuItems'][0]['items'][3]['url'] = ['/laitovo/print/terminal'];
//
//$this->params['menuItems'][0]['items'][4]['label'] = Yii::t('app', 'Автомобили');
//$this->params['menuItems'][0]['items'][4]['url'] = ['/laitovo/cars/index'];
//
//$this->params['menuItems'][0]['items'][5]['label'] = Yii::t('app', 'Наряды ОТК');
//$this->params['menuItems'][0]['items'][5]['url'] = ['/laitovo/otk/index'];
//
//endif;
//
//######################################################################################
//#МЕНЮ ДЛЯ СКЛАДСКИХ РАБОТНИКОВ (ГАЛОФЕЕВА ЕЛЕНА)
//
//if (Yii::$app->user->getId() == 120 || Yii::$app->user->getId() == 62) :
//
//$this->params['menuItems'][0]['items'][0]['label'] = Yii::t('app', 'Главная');
//$this->params['menuItems'][0]['items'][0]['url'] = ['/laitovo/erp/index'];
//
//$this->params['menuItems'][0]['items'][1]['label'] = Yii::t('app', 'Журнал заказов');
//$this->params['menuItems'][0]['items'][1]['url'] = ['/laitovo/erp-order/index'];
//
//$this->params['menuItems'][0]['items'][2]['label'] = Yii::t('app', 'Журнал нарядов');
//$this->params['menuItems'][0]['items'][2]['url'] = ['/laitovo/erp-naryad/index'];
//
//$this->params['menuItems'][0]['items'][3]['label'] = Yii::t('app', 'Этикетки АНГЛИЯ');
//$this->params['menuItems'][0]['items'][3]['url'] = ['/laitovo/custom-label/base/index'];
//
//$this->params['menuItems'][0]['items'][4]['label'] = Yii::t('app', 'Автомобили');
//$this->params['menuItems'][0]['items'][4]['url'] = ['/laitovo/cars/index'];
//
//endif;

