<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];

//$this->params['searchmodel'] = false;
$this->render('../menu');
?>

<h1>Оплата сдельщиков</h1>

<?php $form = ActiveForm::begin(['method'=>'get']); ?>

    <div class="col-xs-4">

        <?= $form->field($model, 'dateFrom1')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'dd.MM.yyyy',
        ]) ?>


    </div>
    <div class="col-xs-4">

        <?= $form->field($model, 'dateTo1')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'dd.MM.yyyy',
        ]) ?>

    </div>
<div class="clearfix">

</div>
<div>
    <div class="col-xs-4 ">

        <?= $form->field($model, 'dismissed')->checkbox([])?>

    </div>

    <div class="col-xs-4 ">

        <?= $form->field($model, 'nullPosition')->checkbox([])?>

    </div>

    <div class="col-xs-4 ">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => '  btn  btn-outline btn-round btn-primary']) ?>
    </div>

</div>



<?php ActiveForm::end(); ?>

<div class="clearfix"></div>

<h1 style="color: red; font-size: x-large">Количество переделок: <?= $rework_count ? $rework_count : ''?></h1>

<br>
<br>
<br>

<?php $contentProduction= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $dataProviderProduction,
            'showPageSummary'=>true,
            'toolbar'=> [
                '{export}',
            ],
            'panel'=>[
                'heading'=>'Оплата сделки за '.$model->date,
            ],
            'rowOptions'=> function($data){
                if(!$data['name']){
                    return ['class' => 'warning','style'=>['font-weight'=>'900']];
                }
            },
            'columns' => [
                [
                    'attribute' => 'name',
                    'label'=>'ФИО',
                    'format' => 'raw',
                    'value' => function ($data) use($model) {
                        return $data['name'] ? Html::a(Html::encode($data['name']), ['piece-work-payment-day-list', 'user_id' => $data['id'], 'dateFrom'=>date('d.m.Y',$model->dateFrom1), 'dateTo'=>date('d.m.Y',$model->dateTo1)]) : '';
                    },
                    'pageSummary'=>'Оплата сделки за '.$model->date,
                    'pageSummaryOptions'=>['class'=>'text-right text-danger'],
                ],
                [
                    'attribute' => 'naryad_sum',
                    'format' => 'raw',
                    'label'=>'Сумма по нарядам',
                    'value'=>function($data){
//                        die(print_r($data));
                        return number_format($data['naryad_sum'],2,',',' ');
                    },
                ],
                [
                    'attribute' => 'naryad_add_sum',
                    'format' => 'raw',
                    'label'=>'Сумма по доп нарядам',
                    'value'=>function($data){
//                        die(print_r($data));
                        return '<span style="color:red"> ( ' .  number_format($data['add_naryad_sum'],2,',',' ') . ' ) </span> ' ;
                    },
                ],
                [
                    'attribute' => 'deduction',
                    'label'=>'Сумма к удержанию',
                    'value'=>function($data){
                        return number_format($data['deduction'],2,',',' ');
                    },
                ],
                [
                    'attribute' => 'fine',
                    'label'=>'Сумма штрафов',
                    'value'=>function($data){
                        return number_format($data['fine'],2,',',' ');
                    },
                ],
                [
                    'attribute' => 'premium',
                    'label'=>'Сумма премий',
                    'value'=>function($data){
                        return number_format($data['premium'],2,',',' ');
                    },
                ],
                [
                    'attribute' => 'total_sum',
                    'label'=>'Итого',
                    'value'=>function($data){
                        return number_format($data['total_sum'],2,',',' ');
                    },
                ],
            ],
        ]);

        $contentSupport= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $dataProviderSupport,
            'toolbar'=> [
                '{export}',
            ],

            'panel'=>[
                'heading'=>'Оплата сделки за '.$model->date,
            ],
            'rowOptions'=> function($data){
                if(!$data['name']){
                    return ['class' => 'warning', 'style'=>['font-weight'=>'900']];
                }
            },
            'columns' => [

                [
                    'attribute' => 'name',
                    'label'=>'ФИО',
                    'format' => 'html',
                    'value' => function ($data) use($model) {
                        return $data['name'] ? Html::a(Html::encode($data['name']), ['piece-work-payment-day-list', 'user_id' => $data['id'], 'dateFrom'=>date('d.m.Y',$model->dateFrom1), 'dateTo'=>date('d.m.Y',$model->dateTo1)]) : '';
                    },
                    'pageSummary'=>'Оплата сделки за '.$model->date,
                    'pageSummaryOptions'=>['class'=>'text-right text-danger'],
                ],
                [
                    'attribute' => 'naryad_sum',
                    'format' => 'raw',
                    'label'=>'Сумма по нарядам',
                    'value'=>function($data){
                        return number_format($data['naryad_sum'],2,',',' ') . ' <span style="color:red"> ( ' .  number_format($data['add_naryad_sum'],2,',',' ') . ' ) </span> ' ;

                    }
                ],
                [
                    'attribute' => 'deduction',
                    'label'=>'Сумма к удержанию',
                    'value'=>function($data){
                        return number_format($data['deduction'],2,',',' ');
                    }
                ],
                [
                    'attribute' => 'fine',
                    'label'=>'Сумма штрафов',
                    'value'=>function($data){
                        return number_format($data['fine'],2,',',' ');
                    },
                ],
                [
                    'attribute' => 'premium',
                    'label'=>'Сумма премий',
                    'value'=>function($data){
                        return number_format($data['premium'],2,',',' ');
                    },
                ],
                [
                    'attribute' => 'total_sum',
                    'label'=>'Итого',
                    'value'=>function($data){
                        return number_format($data['total_sum'],2,',',' ');
                    }
                ],
            ],
        ]);

    $items[0]['label'] = 'Сдельщики';
    $items[0]['content'] = $contentProduction;

    $items[1]['label'] = 'Окладники';
    $items[1]['content'] = $contentSupport;

?>

<?= Tabs::widget([
    'items' => $items,
    'encodeLabels' => false,
]); ?>
