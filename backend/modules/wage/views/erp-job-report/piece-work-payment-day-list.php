<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\bootstrap\ActiveForm;



/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Оплата сдельщиков'), 'url' => ['piece-work-payment']];


//$this->params['searchmodel'] = false;
$this->render('../menu');
?>

    <h1><?= @$user->getName()?></h1>

<?php $form = ActiveForm::begin(['method'=>'get']); ?>

    <div class="col-xs-4">

        <?= $form->field($model, 'dateFrom1')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'dd.MM.yyyy',
        ]) ?>

    </div>
    <div class="col-xs-4">

        <?= $form->field($model, 'dateTo1')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'dd.MM.yyyy',
        ]) ?>

    </div>

    <div class="col-xs-4 ">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => '  btn  btn-outline btn-round btn-primary']) ?>
    </div>


<?php ActiveForm::end(); ?>


    <br>
    <br><br>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'rowOptions'=> function($data){
        if(!$data['created_at']){
            return ['class' => 'warning'];
        }
    },
    'show' => ['created_at', 'naryad_sum', 'deduction', 'fine', 'premium', 'total_sum'],
    'columns' => [

        [
            'attribute' => 'created_at',
            'label'=>'Дата',
            'format' => 'html',
            'value' => function ($data)use($user) {
                return $data['created_at'] ? Html::a($data['created_at'],['report-day', 'user_id'=>$user->id, 'date'=>$data['created_at']]) : '';
            },
        ],
        [
            'attribute' => 'naryad_sum',
            'label'=>'Сумма по нарядам',
            'format' => 'html',
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data['naryad_sum']) . ' <span style="color:red"> ( ' .  Yii::$app->formatter->asDecimal($data['add_naryad_sum']) . ' ) </span> ' ;

            },
        ],
        [
            'attribute' => 'deduction',
            'label'=>'Сумма к удержанию',
            'format' => 'html',
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data['deduction']);
            },
        ],
        [
            'attribute' => 'fine',
            'label'=>'Сумма штрафов',
            'format' => 'html',
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data['fine']);
            },
        ],
        [
            'attribute' => 'premium',
            'label'=>'Сумма премий',
            'format' => 'html',
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data['premium']);
            },
        ],
        [
            'attribute' => 'total_sum',
            'label'=>'Итого',
            'format' => 'html',
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data['total_sum']) ;
            },
        ],
    ],
]); ?>