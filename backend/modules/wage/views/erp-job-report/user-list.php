<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];

//$this->params['searchmodel'] = false;
$this->render('../menu');

?>

<h1>Список сотрудников</h1>


<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name'],
    'columns' => [

        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->getName()), ['payment-day-list-dispatcher', 'user_id' => $data->id]) : '';
            },
        ],

    ],
]); ?>
