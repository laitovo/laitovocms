<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use common\models\laitovo\ErpDocumentCause;



/* @var $this yii\web\View */
/* @var $model common\models\laitovo\ErpDocumentCause */

$this->render('../../menu');
?>
<div class="material-form-create">

    <h1>Добавить документ для <span style="color: #0b58a2"><?= @$model->user->name ?></span></h1>

    <? $form = ActiveForm::begin(); ?>

    <?= $form->field($model,'title')->textInput() ?>

    <?= $form->field($model,'type')->dropDownList(ErpDocumentCause::$types) ?>

    <?= $form->field($model,'date_start')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model,'date_finish')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <? ActiveForm::end() ?>
</div>
<?php if(isset($date)): ?>
    <h1>Документы за учетный период</h1>
    <div>
        <?=
            GridView::widget([
                'dataProvider' => $document_provider,
                'layout'=>"{items}",
                'columns' =>[
                    'type',
                    'title',
                    'date_start',
                    'date_finish'
                ],
            ])
        ?>
    </div>
<?php endif; ?>
