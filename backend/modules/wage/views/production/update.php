<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\ProductionScheme */


$this->params['breadcrumbs'][] = ['label' => 'Журнал карточек продуктов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];

$this->render('../menu');
?>
<div class="production-scheme-update">

    <h1><?= Html::encode('Изменить '. $model->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model2' => $model2,
    ]) ?>

</div>
