<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpProductType */
?>
<div class="erp-product-type-create">

    <h1><?= Html::encode('Добавить новый вид продукта') ?></h1>

    <div class="erp-product-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rule')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::button('Добавить', ['class' =>  'btn btn-success',
             'onclick' => '
                     $.post("' . Url::to(['create-product-type-ajax']) . '",{"title": "'. $model->title.'", "rule": "'.$model->rule.'"},function(data){ 
                              if(data){
                              
                                    
                                    notie.alert(1,data,4);
                                    
                                };
                         });
             ',         
            ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>








