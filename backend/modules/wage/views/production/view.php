<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\models\laitovo\MaterialProductionScheme;
use backend\modules\laitovo\models\ErpJobScheme;
use yii\helpers\Url;
use common\models\laitovo\Config;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\ProductionScheme */


$this->params['breadcrumbs'][] = ['label' => ' Журнал карточек продуктов', 'url' => ['index']];
$this->render('../menu');
if($model->product_id)
    {
        Yii::$app->view->registerJs('

                    $(document).ready(function(){
                             $.get("' . Url::to(['/ajax/product-status']) . '",{"id": "'.$model->id.'","product_id": "'. $model->product_id.'" },function(data){
                                        if(data)
                                        {
                                           $("#product_button").attr("data-original-title", "Выключить");
                                           $("#product_button").prop("class", "btn btn-icon btn-sm  btn-round btn-success");
                                           $("#pruduct-button-status").text("Включен для "+"'. htmlspecialchars($model->product->title) .'");
                                        }
                                });
                    });
               ', \yii\web\View::POS_END);
    }
?>
<div class="production-scheme-view">

    <h1><?= Html::encode($model->title) ?></h1>
    
    <div style="padding: 10px">
        <?php if($model->product_id): ?>
            <b><?= Html::button('<i class="glyphicon glyphicon-ok"></i>', [
                'id'=>'product_button',
                'data-toggle' => "tooltip",
                'data-original-title' => Yii::t('app', 'Включить'),
                'class' => 'btn btn-icon btn-sm  btn-round btn-danger',
                'onclick'=>'
                     $.get("' . Url::to(['/ajax/product-active']) . '",{"id": "'.$model->id.'","product_id": "'. $model->product_id.'" },function(data){
                                if(data && data!=2)
                                {
                                     $("#product_button").attr("data-original-title", "Выключить");
                                     $("#product_button").prop("class", "btn btn-icon btn-sm  btn-round btn-success");
                                     $("#pruduct-button-status").text("Включен для "+"'. htmlspecialchars($model->product->title) .'");
                                     notie.alert(1,"Включен для: "+data,4);
                                }else if(data==2)
                                {
                                     $("#product_button").attr("data-original-title", "Включить");
                                     $("#product_button").prop("class", "btn btn-icon btn-sm  btn-round btn-danger");
                                     $("#pruduct-button-status").text("Выключен для "+"'. htmlspecialchars($model->product->title) .'");
                                     notie.alert(3,"Выключен для: "+"'. htmlspecialchars($model->product->title) .'");
                                }else{ 
                                   notie.alert(3,"Ошибка",4);
                                   }
                        });
                '
                ])?>
        <?php endif ?>
            <span id="pruduct-button-status">Включить для продукта <?= isset($model->product_id) ? htmlspecialchars($model->product->title) : ''?> </span>
  
       </b>
       
    </div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'product.category',
            'scheme.name',
            [
               'attribute'=>'product_id',
               'label'=>'Название продукта', 
               'value'=> function($data){
                   return $data->product_id ? @$data->product->title : '';
               } 
            ],
            [
                'attribute'=>'article',
                'label'=>'Артикул продукта',
                'value'=>function($data){
                   return $data->product_id ? ($data->product->article ? $data->product->article : ($data->product->rule ? $data->product->articleRule : '')) : '';
               }
            ],
            'author.name',
            'created_at:datetime',
        ],
    ]) ?>
    
    <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Перейти к журналу карточек продуктов'), ['index', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>

   <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
    'class' => 'deleteconfirm pull-right btn btn-outline btn-round btn-danger',
    'style' => 'margin-right:5px',
    'data' => [
        'confirm' => Yii::t('yii', 'Вы действительно хотите удалить?'),
        'method' => 'post',
    ]])?>
</div>

            <div style = "font-size:30px; color:red">
                <h>Себестоимость продукта <?= $totalJobClips && $totalMaterial ? round($totalJobClips,2)+round($totalMaterial,2): ''?><h>
            </div>
 
             <div  style = "font-size:20px; color:green">
                 <h> Стоимость материалов <?= $totalMaterial ? round($totalMaterial,2): ''?><h>
            </div>
            <div>
                 <h2>Виды материалов</h2>
                 <?= GridView::widget([
                    'dataProvider' => $materialProduction,
                     'tableOptions' => [
                        'class' => 'table table-striped table-bordered',
                        
                    ],
                    'columns' => [
                       'name',
                     
                        [
                           
                           'label' =>'Материалы',
                           'format' =>'html',
                           'value' =>  function($data) use ($model){
                                $job_list = MaterialProductionScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $return = '';
                                if ($job_list) {
                                    foreach ($job_list as $row) {
                                        $return .= Html::tag('div',

                                            Html::tag('span',
                                                ($row->materialParameter->parameter || $row->materialParameter->material->name ?  @$row->materialParameter->material->name.' '.@$row->materialParameter->parameter : '')
                                            ), ['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                        }
                                }
                             return $return ? $return :'';                           
                           }
                        ],     
                        [
                           'attribute'=>'count1',
                           'label' =>'Количество',
                           'format' =>'html',
                           'value' =>  function($data) use ($model){
                                $job_list = MaterialProductionScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $return = '';
                                if ($job_list) {
                                    foreach ($job_list as $row) {
                                        $return .= Html::tag('div',

                                            Html::tag('span',
                                                ($row->material_count  ?  @$row->material_count : '')
                                            ),['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                        }
                                }
                            return $return ? $return :'';                            
                           }
                        ],     
                        
                        [
                           'attribute'=>'settlement',
                           'label' =>'Расч. ед.',
                           'format' =>'html',
                           'value' =>  function($data) use ($model){
                                $job_list = MaterialProductionScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $return = '';
                                if ($job_list) {
                                    foreach ($job_list as $row) {
                                        $return .= Html::tag('div',

                                                 Html::tag('span',
                                                ($row->materialParameter->lastSettlement ? $row->materialParameter->lastSettlement->unitSettlement->name : '')
                                            ),['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                    }
                                }
                              return $return ? $return :'';                              
                           }
                        ], 
                        [
                           'attribute'=>'price_one',
                           'label' =>'Цена расч. ед.',
                           'format' =>'html',
                           'value' =>  function($data) use ($model){
                                $job_list = MaterialProductionScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $return = '';
                                if ($job_list) {
                                    foreach ($job_list as $row) {
                                        if($row->materialParameter->lastSettlement->coefficient_settlement && $row->materialParameter->lastAccounting->accounting_unit_price )    
                                        {
                                            $return .= Html::tag('div',
                                           
                                                 Html::tag('span',
                                                ($row->materialParameter->lastSettlement->coefficient_settlement*$row->materialParameter->lastAccounting->accounting_unit_price)
                                            ),['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                        }    
                                    }
                                }
                             return $return ? $return: '';                           
                           }
                        ],             
                        [
                           'attribute'=>'price',
                           'label' =>'Стоимость',
                           'format' =>'html',
                           'value' =>  function($data) use ($model){
                                $job_list = MaterialProductionScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $return = '';
                                if ($job_list) {
                                    foreach ($job_list as $row) {
                                        $unitPrice = $row->materialParameter->lastAccounting->accounting_unit_price*$row->materialParameter->lastSettlement->coefficient_settlement;
                                        if($unitPrice!=0 && $row->material_count !=0) 
                                            {
                                                $return .= Html::tag('div',

                                                         Html::tag('span',
                                                        (round($row->material_count*$unitPrice,2))
                                                    ),['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                            }
                                    }
                                }
                              return $return ? $return :'';                              
                           }
                        ],     
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($url,$data) use ($model) {
                 
                                    return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size: 20px; color: orange;"></span>', 
                                    ['/laitovo/material/register-material-in-location','production_scheme_id'=>$model->id, 'location_id'=>$data->id]);
                                },
                               
                            ],
                        ],
                        
                    ],
                ]); ?>
            </div>
           
         
            
            <div style = "font-size:20px; color:green">
                <h>Стоимость работ <?= $totalJobClips ? round($totalJobClips,2): ''?><h>
            </div>
           <div>
               <h2>Виды работ</h2>
             <?= GridView::widget([
                'dataProvider' => $jobProduction,
                 'tableOptions' => [
                    'class' => 'table table-striped table-bordered',
                   
                ],
                'columns' => [
                    
                        'name',
                        [
                          // 'attribute' => 'job_type', 
                           'label' =>'Вид работ',
                           'format' =>'html',
                          'value' =>  function($data) use ($model){
                                $job_type_list = ErpJobScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $return = '';
                                if ($job_type_list) {
                                    foreach ($job_type_list as $row) {
                                        $return .= Html::tag('div',

                                            Html::tag('span',
                                                ($row->type->title ?  @$row->type->title : '')
                                            ),['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                        }
                                }
                             return $return;                           
                           }
                        ],
                        [
                          'attribute' => 'type', 
                          'label' =>'Количество',
                          'format' =>'html',
                          'value' =>  function($data) use ($model){
                                $job_type_list = ErpJobScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $return = '';
                                if ($job_type_list) {
                                    foreach ($job_type_list as $row) {
                                        $return .= Html::tag('div',

                                            Html::tag('span',
                                                ($row->job_count ?  @$row->job_count : '')
                                            ),['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                        }
                                }
                             return $return;                           
                           }
                        ],
                        [
                          'attribute' => 'type_unit', 
                          'label' =>'Ед. изм.',
                          'format' =>'html',
                          'value' =>  function($data) use ($model){
                                $job_type_list = ErpJobScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $return = '';
                                if ($job_type_list) {
                                    foreach ($job_type_list as $row) {
                                        $return .= Html::tag('div',

                                            Html::tag('span',
                                                ($row->type ?  @$row->type->unit : '')
                                            ),['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                        }
                                }
                             return $return;                           
                           }
                        ],
                        [
                           'attribute' => 'count2', 
                           'label' =>'Стоимость',
                           'format' =>'html',
                           'value' =>  function($data) use ($model){
                                $job_type_list = ErpJobScheme::find()->where(['production_scheme_id'=>$model->id, 'location_id'=>$data->id])->all();
                                $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
                                if($config->json('averageRate') !=0 && $config->json('hourrate') !=0 )
                                {
                                    $return = '';
                                    if ($job_type_list) {
                                        foreach ($job_type_list as $row) {
                                            if($row->type->rate !=0 && $row->job_count!=0)
                                            {
                                                $return .= Html::tag('div',

                                                Html::tag('span',
                                                    ($row->type->rate ? round($config->json('averageRate')/ $row->type->rate * @$row->job_count * $config->json('hourrate'),2) : '')
                                                ),['style'=>[
                                                    'padding'=>'5px',
                                                ]]);
                                            }

                                        }
                                    }
                                }
                             return $return;                           
                           }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($url,$data) use ($model) {
                 
                                    return Html::a('<span class="glyphicon glyphicon-pencil"  style="font-size: 20px; color: orange;"></span>', 
                                    ['/laitovo/erp-job-scheme/register-job-type-in-location','production_scheme_id'=>$model->id, 'location_id'=>$data->id]);
                                },
                               
                            ],
                        ],
                ],
            ]); ?>

            </div>
           
        
        

