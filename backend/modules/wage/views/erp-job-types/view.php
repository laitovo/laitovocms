<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpJobType;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

/* @var $this yii\web\View */
/* @var $model ErpJobType */
/* @var $dataProviderTypeRate yii\data\ActiveDataProvider */
/* @var $dataProviderScheme yii\data\ActiveDataProvider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Виды работ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'title',
        'unit',
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => $model->location ? Html::a(Html::encode($model->location->name),
                ['erp-location/view', 'id' => $model->location->id]) : null,
        ],

        'rate',
        'jobRate',
        'created_at:datetime',
        [
            'label' => 'Создал',
            'attribute' => 'author.name',
        ],
        'updated_at:datetime',
        [
            'label' => 'Внес последние изменения',
            'attribute' => 'updater.name',
        ],
        [
            'attribute' => 'status',
            'value'=> function ($model) {
                return $model->getStatus();
            }
        ],
    ],
]) ?>

<?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id],
    ['class' => 'btn btn-outline btn-round btn-primary']) ?>

<?php if (in_array(Yii::$app->user->getId(), [21,2])) :?>

<?= Html::a(Yii::t('app',  $model->status == 1 ? 'Деактивировать' : 'Активировать'), ['change-status', 'id' => $model->id],
    ['class' => 'btn btn-outline btn-round btn-primary']) ?>

<?php endif; ?>


<?= $model->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
    'data-toggle' => "tooltip",
    'data-original-title' => Yii::t('yii', 'Delete'),
    'data' => [
        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
        'method' => 'post',
    ],
]) ?>
