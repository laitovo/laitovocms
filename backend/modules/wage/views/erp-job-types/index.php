<?php

use backend\modules\laitovo\models\ErpJobScheme;
use backend\modules\laitovo\models\ErpUsersReport;
use backend\widgets\ActiveForm;
use yii\helpers\Html;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpJobType;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $hideInactiveJob boolean */

$this->title = Yii::t('app', 'Виды работ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<div class="row">
    <span class="bg-success" style="display: inline-block;padding: 5px;">Использутся</span>
    <span class="bg-info" style="display: inline-block;padding: 5px;">Использовались последний год</span>
    <span class="bg-warning" style="display: inline-block;padding: 5px;">Не используются</span>
    <span class="bg-danger" style="display: inline-block;padding: 5px;">Выключены</span>
</div>
<?php ActiveForm::begin([
    'id'      => 'search-form',
    'action'  => [''],
    'method'  => 'get',
]); ?>

<?= Html::tag('div',
    Html::checkbox("wage_erp_job_type_inactive_filter", $hideInactiveJob, ['id' => 'checkbox-auto-refresh'])
    .Html::tag('label','Скрывать неактивные работы', ['for' => 'checkbox-auto-refresh']),
    [
        'class' => 'checkbox-custom checkbox-primary text-left',
        'onchange' => '$("#search-form").submit();'
    ]
) ?>
<?= Html::hiddenInput('update_wage_erp_job_type_inactive_filter', true) ?>

<?php ActiveForm::end(); ?>

<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['title', 'location_id', 'rate', 'unitPrice'],
    'rowOptions' => function ($data) {
        // Если за последний год есть запись с данным типом работы
        if (ErpJobScheme::find()->where(['type_id' => $data->id])->andWhere(['is not','production_scheme_id', null])->exists()) {
            return ['class' => 'success'];
        }

        // Если за последний год есть запись с данным типом работы
        if (ErpUsersReport::find()->where(['job_type_id' => $data->id])->andWhere(['>','created_at', time()-31536000])->exists()) {
            return ['class' => 'info'];
        }

        if( $data->status == ErpJobType::INACTIVE ){
            return ['class' => 'danger'];
        }

        return ['class' => 'warning'];
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($data) {
                return $data->title ? Html::a(Html::encode($data->title), ['view', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view', 'id' => $data->location->id]) : null;
            },
        ],
        'unit',
        'rate',
        'jobRate',
        [
            'attribute'=>'unitPrice',
            'label'=>'Цена за единицу',
            'value'=>function($data){
                    return $data->unitPrice ? Yii::$app->formatter->asDecimal($data->unitPrice) : '';
            }
        ],
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
            'attribute' => 'status',
            'value' => function ($data) {
                return $data->getStatus();
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
