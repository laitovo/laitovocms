<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpLocation;
use yii\helpers\Url;
use kartik\select2\Select2;

if(isset($model->naryad->user->name)){
Yii::$app->view->registerJs('
            $(window).load(function(){
                    $("#select2-laitovoreworkact-found_user_id-container").text("'. $model->naryad->user->name .'");
                    $("#laitovoreworkact-found_user_id").val("'. $model->naryad->user_id .'");
            });
            $("#compiled_user_checkbox").change(function(){
                    if($("#compiled_user_checkbox").prop("checked"))
                    {
                        $.get("' . Url::to(['/ajax/reload-user']) . '",{"status":1},function(data){
                                $("#laitovoreworkact-compiled_user").html("");
                                $("#laitovoreworkact-compiled_user").html("<option></option>");
                                $.each(data, function(key, value) {
                                    $("#laitovoreworkact-compiled_user").append($("<option></option>").attr("value",key).text(value));
                                });
                            });
                     }else{
                        $.get("' . Url::to(['/ajax/reload-user']) . '",{"status":2},function(data){
                                $("#laitovoreworkact-compiled_user").html("");                            
                                $("#laitovoreworkact-compiled_user").html("<option></option>");
                                $.each(data, function(key, value) {
                                    $("#laitovoreworkact-compiled_user").append($("<option></option>").attr("value",key).text(value));
                                });
                            });
                    }
                });
                
                $("#real_quility_checkbox").change(function(){
                        if($("#real_quility_checkbox").prop("checked"))
                        {
                            $("#real_quilty_2").attr({"type":"text", "name":"LaitovoReworkAct[real_quilty]"});
                            $(".control-label.label-real_quilty_2").attr("hidden",false);
                            $(".form-group.field-laitovoreworkact-real_quilty.required").attr({"hidden":true, "name":false});
                           
                        }else{
                        $("#real_quilty_2").attr({"type":"hidden", "name":false});
                         $(".control-label.label-real_quilty_2").attr("hidden",true);
                         $(".form-group.field-laitovoreworkact-real_quilty.required").attr({"hidden":false, "name":"LaitovoReworkAct[real_quilty]"});
                        }        


                });

        ', \yii\web\View::POS_END);
}

?>

<div class="laitovo-rework-act-form">
    
    

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'naryad_id')->hiddenInput()->label(false) ?>
          
    <?= $form->field($model, 'found_user_id')->widget(Select2::classname(), [
        'data' => ErpUser::userInWork(),
        'theme' => Select2::THEME_BOOTSTRAP,
        'language' => 'ru',
        'options' => [
            'placeholder' => 'Введите имя ...',
            
         ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
   
    <input type="checkbox" id=compiled_user_checkbox>Выбрать другого пользователя*
    <?= $form->field($model, 'compiled_user')->dropDownList(ArrayHelper::merge(['' => ''], ErpUser::dispatcherInWork())) ?>

  <?= $form->field($model, 'location_defect')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->where(['!=','id',Yii::$app->params['erp_dispatcher']])->all(), 'id', 'name')),
            ['class' => 'form-control', 'onchange' => '$.get("' . Url::to(['/ajax/rework-user']) . '",{"location":$(this).val(),"naryad_id":$("#laitovoreworkact-naryad_id").val()},function(data){
                    $("#select2-laitovoreworkact-made_user_id-container").text(data.made_user_name); 
                    $("#laitovoreworkact-made_user_id").val(data.made_user_id);
                    $("#select2-laitovoreworkact-real_quilty-container").text(data.made_user_name); 
                    $("#laitovoreworkact-real_quilty").val(data.made_user_name);
                  
            });']
        )
             ?>
  

    <?= $form->field($model, 'made_user_id')->widget(Select2::classname(), [
        'data' => ErpUser::userInWork(),
        'language' => 'ru',
        //'initValueText' => common\models\laitovo\ErpUser::findOne($made_user_id)->name,
        'options' => [
            'placeholder' => 'Введите имя ...',
            'onchange' => '  
                $("#select2-laitovoreworkact-real_quilty-container").text($("option:selected", this).text());
                $("#laitovoreworkact-real_quilty").val($("option:selected", this).text());
        ',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?> 
    <?= $form->field($model, 'deacription_problem')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cause_problem')->textInput(['maxlength' => true]) ?>
<!--    <input type="checkbox" id=real_quility_checkbox>Выбрать другое*-->
<!--    --><?//= $form->field($model, 'real_quilty')->widget(Select2::classname(), [
//        'data' => ErpUser::userNameInWork(),
//        'language' => 'ru',
//        //'initValueText' => common\models\laitovo\ErpUser::findOne($made_user_id)->name,
//        'options' => [
//            'placeholder' => 'Введите имя ...',
//        ],
//        'pluginOptions' => [
//            'allowClear' => true
//        ],
//        ]);?>
<!--    <div>-->
<!--    --><?//= Html::label('Реальный виновный *', 'real_quilty2', ['class' => 'control-label label-real_quilty_2', 'hidden'=>true]) ?>
<!--    </div>-->
<!--    --><?//= Html::input('hidden', false, $model->real_quilty, ['class' => 'form-control', 'id'=>'real_quilty_2'])?>
   
    
    <?= $form->field($model, 'amount_damage')->textInput(['maxlength' => 11]) ?>

    <?= $form->field($model, 'deduction')->textInput(['maxlength' => 11]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить акт' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
