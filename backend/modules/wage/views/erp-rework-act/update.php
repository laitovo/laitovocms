<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\LaitovoReworkAct */


$this->params['breadcrumbs'][] = ['label' => 'Постмотреть акты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить данные акта';
$this->render('../menu');
?>
<div class="laitovo-rework-act-update">
<div style="font-size:30px">Изменить данные акта № <?php echo $model->name ?></div>
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
