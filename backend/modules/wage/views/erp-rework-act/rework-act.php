

<style type="text/css">
  
   b { 
    text-align: left; 
    font-family: Verdana, Arial, Helvetica, sans-serif; 
    color: #336;
   }
   a { 
    text-align: left; 
    font-family: Verdana; 
    color: #336;
   }
  </style>
<table cellspacing="2" cellpadding="1" >
    <tr>
        <td colspan="2" style="text-align: center">
    <b>
        <?= $model->name; ?>
    </b>
        <td>
    </tr>
    <tr>
        <td>
            <b>
                Дата создания акта:
            </b>
        </td>
        <td>
            <a>
                <?= date("m-d-Y",$model->created_at)  ?>
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
                Номер наряда:
            </b>
        </td>
        <td>
            <a>
                <?= $model->naryad->name  ?>
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              ФИО составившего акт:
            </b>
        </td>
        <td>
            <a>
                <? if (isset($model->compiledUser->name)):?>
                <?= $model->compiledUser->name  ?>
                <? else: ?>
                не задано
                <? endif ?>
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              ФИО обнаружевшего брак:
            </b>
        </td>
        <td>
            <a>
                <? if (isset($model->foundUser->name)):?>
                <?= $model->foundUser->name  ?>
                <? else: ?>
                не задано
                <? endif ?>
                
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              ФИО допустившего брак
            </b>
        </td>
        <td>
            <a>
               
                <? if (isset($model->madeUser->name)):?>
                <?= $model->madeUser->name  ?>  
                <? else: ?>
                не задано
                <? endif ?>
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              Описание дефекта
            </b>
        </td>
        <td>
            <a>
                <? if (isset($model->deacription_problem)):?>
                <?= $model->deacription_problem  ?>
                <? else: ?>
                не задано
                <? endif ?>
               
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              Причина дефекта
            </b>
        </td>
        <td>
            <a>
                <? if (isset($model->cause_problem)):?>
                <?= $model->cause_problem  ?>
                <? else: ?>
                не задано
                <? endif ?>
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              Участок где допустили несоответствие
            </b>
        </td>
        <td>
            <a>
                <? if (isset($model->locationDefect->name)):?>
                <?= $model->locationDefect->name  ?>
                <? else: ?>
                не задано
                <? endif ?>
               
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
                
              Участок на который отправили наряд
            </b>
        </td>
        <td>
            <a>
                <? if (isset($model->locationStart->name)):?>
                    <?=$model->locationStart->name  ?>
                <? else: ?>
                не задано
                <? endif ?>
               
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              Реальный виновный
            </b>
        </td>
        <td style="font-size: 20px">
            <ins>
                <strong>
                <? if (isset($model->real_quilty)):?>
                <?= $model->real_quilty  ?>
                <? else: ?>
                не задано
                <? endif ?>
               </strong>
            </ins>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              Размер ущерба
            </b>
        </td>
        <td>
            <a>
                <? if (isset($model->amount_damage )):?>
                <?= $model->amount_damage  ?>
                <? else: ?>
                не задано
                <? endif ?>
               
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <b>
              Размер удержания с работника
            </b>
        </td>
        <td>
            <a>
                <? if (isset($model->deduction)):?>
                <?= $model->deduction  ?>
                <? else: ?>
                не задано
                <? endif ?>
               
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a>
              Работник ознакомлен______________
            </a>
        </td>
    </tr> 
    <tr>
        <td>
            <a>
                Подпись руководителя_____________
            </a>
        </td>
        <td>
            <a>
               Дата: <?=date('d-m-Y'); ?>
            </a>
        </td>
    </tr>
    
</table>
