<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

$js = <<<JS
    function parseitemsjson (callback) {
        let articles =[];
        $('.ids.item').each(function( index, value ) {
            itemjson = $(this).find("input").serializeArray();
            console.log(itemjson);
            $.each(itemjson, function( index1, value1 ) {
                articles.push(value1.value);
            });
        });
        $('#ids-data').val(JSON.stringify(articles));
    }

    let sendForm = function(href) {
        let  form = $('#my-form');
        form.attr('action',href);
        parseitemsjson();
        form.submit();
    }
JS;

$this->registerJs($js,\yii\web\View::POS_HEAD);

?>
<div class="erp-additional-work-index">

    <h1>Журнал нарядов на дополнительные работы</h1>

    <p>
         <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Создать наряд на доп. работы')]) ?>
    </p>

    <h3>Внимание!!! Подтвержденные наряды попадут в зарплату следующим числом : <span style="color: red"><?= Yii::$app->formatter->asDate(time())?></span></h3>

    <?php $form = ActiveForm::begin(['id' => 'my-form']); ?>

        <?= Html::hiddenInput('ids',null,['id' => 'ids-data']) ?>

    <?php ActiveForm::end(); ?>

    <?= !in_array(Yii::$app->user->getId(),[2,21,121]) ? '' :
    Html::button('Подтвердить отмеченные наряды',
        [
            'title' => Yii::t('app', 'Потверждение отмеченных нарядов и начисление заработной платы'),
            'class' => 'btn btn-primary',
            'onclick' => "sendForm('confirm-many')",
            'data' => [
                'confirm' => 'Вы уверены, что хотите подтвердить выбранные наряды и начислить заработную плату ?',
                'method' => 'post',
            ],
            'data-toggle' => "tooltip",
            'data-original-title' => Yii::t('app', 'Исключить из текущего заказа на автопополнение')
        ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
         'show'=>['checkbox','id', 'user_id', 'job_type', 'job_count' ,'location_id','status'],
        'rowOptions' => function ($data) {
            if ($data->confirmed) {
                return ['class' => 'success'];
            }elseif ($data->status) {
                return ['class' => 'info'];
            } else return ['class' => 'danger'];
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'checkbox',
                'label' => 'Отметить',
                'format' => 'raw',
                'value' => function ($data) {
                    if (!in_array(Yii::$app->user->getId(),[2,21,121])) return '';
                    return $data->confirmed ? '<span class="glyphicon glyphicon-ok text-default"></span>' : Html::tag('div',
                        Html::checkbox("ids[]",
                            $checked = false,
                            ['value' => $data->id])
                        .Html::tag('label',false),
                        ['class' => 'checkbox-custom checkbox-primary text-left ids item']
                    );
                },
            ],
            [
                'attribute'=> 'id',
                'format' =>'html',
                'value' =>function($data){
                   return $data->id ? Html::a(Html::encode($data->id), ['view', 'id' => $data->id]): '';
               }      
            ],
            [
                'attribute'=> 'user_id',
                'value' => function($data){
                    return $data->user_id ? $data->user->name : '';
                }
            ],
            [
                'attribute'=> 'location_id',
                'value' => function($data){
                    return $data->location_id ? $data->location->name : '';
                }
            ],
            [
                'attribute'=> 'job_type',
                'value' => function($data){
                    return $data->job_type ? $data->jobType->title : '';
                }
            ],
            [
                'attribute'=> 'job_count',
                'value' => function($data){
                    return $data->job_count  ? $data->job_count : '';
                }
            ],
            [
                'attribute'=> 'job_price',
                'value' => function($data){
                    return $data->job_price  ? $data->job_price : '';
                }
            ],
            [
                'attribute' => 'comment',
                'value'=> function($data) {
                    return $data->comment ? $data->comment: '';
                }
            ],
            [
                'attribute'=> 'status',
                'value' => function($data){
                   
                    return $data->status   ?'Готов'  : 'В работе';
                }
            ],
            [
                'attribute'=> 'created_at',
                'value' => function($data){
                   
                    return $data->created_at   ? Yii::$app->formatter->asDatetime($data->created_at) : '';
                }
            ],
            [
                'attribute'=> 'finish_at',
                'value' => function($data){
                   
                    return $data->finish_at   ? Yii::$app->formatter->asDatetime($data->finish_at) : '';
                }
            ],
            [
                'attribute'=> 'updated_at',
                'value' => function($data){
                   
                    return $data->updated_at   ? Yii::$app->formatter->asDatetime($data->updated_at) : '';
                }
            ],
            [
                'attribute'=> 'author_id',
                'value' => function($data){
                   
                    return $data->author_id   ? $data->author->name : '';
                }
            ],
            
            [
                'attribute'=> 'updater_id',
                'value' => function($data){
                    return $data->updater_id   ? @$data->updater->name : '';
                }
            ],

        ],
    ]); ?>
</div>
