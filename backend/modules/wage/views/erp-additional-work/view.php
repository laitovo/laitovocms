<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpAdditionalWork */


$this->params['breadcrumbs'][] = ['label' => 'Журнал дополнительных нарядов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-additional-work-view">

    <h1>Дополнительный наряд № <?= Html::encode($model->id) ?></h1>

   
    <p> <?php if($model->status == null) :?>
            <?= Html::a('Закрыть наряд', ['finish', 'id' => $model->id], ['class' => 'btn  btn-round btn-lg btn-warning']) ?>
        <?php endif; ?> </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            [
              'attribute' => 'user_id',
              'value'=>$model->user_id? $model->user->name: '',
            ],
            [
              'attribute' => 'location_id',
              'value'=>$model->location_id? $model->location->name : '',
            ],
            [
              'attribute' => 'job_type',
              'value'=>$model->job_type? $model->jobType->title : '',
            ],
            [
              'attribute' => 'job_count',
              'value'=>$model->job_count? $model->job_count : '', 
            ],
            [
              'attribute' => 'job_price',
              'value'=>$model->job_price? $model->job_price : '', 
            ],
            [
              'attribute' => 'status',
              'value'=>$model->status ? 'Готов' : 'В работе',
            ],
            [
              'attribute' => 'confirmed',
              'format' => 'raw',
              'value'=>$model->confirmed ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>',
            ],
            [
              'attribute' => 'author_id',
              'value'=>$model->author_id ? $model->author->name  : '',  
            ],
            [
              'attribute' => 'created_at',
              'value'=>$model->created_at ? Yii::$app->formatter->asDatetime($model->created_at)  : '',  
            ],
            [
              'attribute' => 'finish_at',
              'value'=>$model->finish_at ? Yii::$app->formatter->asDatetime($model->finish_at)  : '',  
            ],
            [
              'attribute' => 'comment',
                'value'=> $model->comment ? $model->comment: '',
            ],
        ],
    ]) ?>
    
     <p>

        <?= $model->confirmed || !in_array(Yii::$app->user->getId(),[2,21])? '' : Html::a('Потвердить', ['confirm', 'id' => $model->id], ['class' => 'btn btn-round btn-primary']) ?>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary',]) ?>
        <?= Html::a('Распечатать наряд', ['print-additional-naryad', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary', 'target'=>'blank']) ?>
        <?= Html::a('Перейти к журналу', ['index', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary',]) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id],[
            'class' => 'deleteconfirm pull-right btn btn-outline btn-round btn-danger',
            'style'=>'margin-right:5px',
            'data' => [
                'confirm' => Yii::t('yii', 'Отменить этот акт?'),
                'method' => 'post',
             ]]) ?>
    </p>

</div>
