<style>
    div{
        padding-top:15px;
        padding-bottom: 15px;
        text-align: center;
        border: 1px solid;
    }

</style>

<div id="rework" style="position: absolute">
    <div  style=" background:black; color: white; font-size: 40px;">
        Наряд на доп. работы № <?= @$model->id ?>
    </div>
    <div style="background:grey; color: white;font-size: 20px;">
        Исполнитель: <?= @$model->user_id ? $model->user->name : '' ?>
    </div>
    <div>
        <a>Участок: <?= @$model->location_id ? $model->location->name: '' ?></a>
    </div>
    <div>
        <a>Вид работы: <?= @$model->job_type ? $model->jobType->title: '' ?></a>
    </div>
    <div>
        Количество работ: <?= @$model->job_count ? $model->job_count : '' ?>
    </div>

    <div>
        Стоимость работы: <?= @$model->job_price ? $model->job_price : '' ?>
    </div>

    <div>
        Статус: <?= @$model->status ? 'Готов' : 'В работе' ?>
    </div>

    <? if($model->created_at): ?>
    <div>
        Дата создания наряда: <?= @$model->created_at ? Yii::$app->formatter->asDatetime($model->created_at) : '' ?>
    </div>
    <? endif ?>
    <? if($model->finish_at): ?>
    <div>
        Дата выполнения наряда наряда: <?= @$model->finish_at ? Yii::$app->formatter->asDatetime($model->finish_at) : '' ?>
    </div>
    <? endif ?>
    <? if($model->author_id): ?>
    <div>
        ФИО выдавшего наряд: <?= @$model->author_id ? $model->author->name : '' ?>
    </div>
    <? endif ?>

</div>