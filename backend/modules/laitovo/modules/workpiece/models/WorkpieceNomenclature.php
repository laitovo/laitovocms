<?php

namespace backend\modules\laitovo\modules\workpiece\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%laitovo_erp_workpiece_nomenclature}}".
 *
 * @property int $id ID
 * @property string $title Наименование
 *
 * @property WorkpieceDocument[] $workpieceDocuments
 */
class WorkpieceNomenclature extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%laitovo_erp_workpiece_nomenclature}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Наименование'),
        ];
    }

    /**
     * Gets query for [[LaitovoErpWorkpieceDocuments]].
     *
     * @return ActiveQuery
     */
    public function getWorkpieceDocuments(): ActiveQuery
    {
        return $this->hasMany(WorkpieceDocument::class, ['nomenclature_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return WorkpieceNomenclatureQuery the active query used by this AR class.
     */
    public static function find(): WorkpieceNomenclatureQuery
    {
        return new WorkpieceNomenclatureQuery(get_called_class());
    }

    /**
     * @return bool
     */
    public function beforeDelete(): bool
    {
        //Нельзя удалить номенклатуру, если она выбрана в каком нибудь документе.
        if ($this->getWorkpieceDocuments()->exists())
            return false;
        return parent::beforeDelete();
    }
}
