<?php

namespace backend\modules\laitovo\modules\workpiece\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the ActiveQuery class for [[WorkpieceDocument]].
 *
 * @see WorkpieceDocument
 */
class WorkpieceDocumentQuery extends ActiveQuery
{

    /**
     * {@inheritdoc}
     * @return WorkpieceDocument[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return array|ActiveRecord|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
