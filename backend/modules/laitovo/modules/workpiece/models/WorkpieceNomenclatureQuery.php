<?php

namespace backend\modules\laitovo\modules\workpiece\models;

/**
 * This is the ActiveQuery class for [[WorkpieceNomenclature]].
 *
 * @see WorkpieceNomenclature
 */
class WorkpieceNomenclatureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return WorkpieceNomenclature[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return WorkpieceNomenclature|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
