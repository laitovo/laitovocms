<?php

namespace backend\modules\laitovo\modules\workpiece\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * WorkpieceDocumentSearch represents the model behind the search form of `backend\modules\laitovo\modules\workpiece\models\WorkpieceDocument`.
 */
class WorkpieceDocumentSearch extends WorkpieceDocument
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'nomenclature_id', 'user_id', 'document_type', 'count', 'date'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = WorkpieceDocument::find();

        if (isset($_GET['sort'])) {
            if (preg_match('/-?nomenclature\./', $_GET['sort']) === 1) { //Подключаем таблицу номенклатуры для сортировки
                $query->joinWith('nomenclature as nomenclature');
            } elseif (preg_match('/-?user\./', $_GET['sort']) === 1) { //Подключаем таблицу пользователей для сортировки
                $query->joinWith('user as user');
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nomenclature_id' => $this->nomenclature_id,
            'user_id' => $this->user_id,
            'document_type' => $this->document_type,
            'count' => $this->count,
            'date' => $this->date,
        ]);

        return $dataProvider;
    }
}
