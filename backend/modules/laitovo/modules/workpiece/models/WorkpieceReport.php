<?php
namespace backend\modules\laitovo\modules\workpiece\models;

use DateTime;
use Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Класс для генерации отчета по заготовкам
 */
class WorkpieceReport extends Model
{
    /**
     * @var int timestamp
     */
    private $startDate;

    /**
     * @var int timestamp
     */
    private $endDate;

    /**
     * @var int timestamp Вспомогательное поле для поиска моделей
     */
    private $endDateEndOfDay;

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate ?: strtotime('today');
    }

    /**
     * @return mixed
     */
    public function setStartDate($value)
    {
        return $this->startDate = strtotime($value);
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate ?: strtotime('today');
    }

    /**
     * @return mixed
     */
    public function setEndDate($value)
    {
        return $this->endDate = strtotime($value);
    }

    public function getEndDateEndOfDay()
    {
        $date = $this->getEndDate();
        return strtotime('+1 day -1 second', $date);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return ArrayHelper::merge([
            'startDate' => Yii::t('app', 'Начало периода'),
            'endDate' => Yii::t('app', 'Конец периода'),
        ], parent::attributeLabels());
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return ArrayHelper::merge([
            ['startDate', 'safe'],
            ['endDate', 'safe'],
        ], parent::rules());
    }

    /**
     * @return array
     * @throws Exception
     */
    public function generate(): array
    {
        $data = [];
        $nomenclature = WorkpieceNomenclature::find()->select(['id','title'])->all();
        foreach ($nomenclature as $row) {
            $data[$row->id] = [
                'id' => $row->id,
                'title' => $row->title,
                'count_at_start_date' => WorkpieceDocument::find()
                    ->select('SUM(count * document_type) as amount')
                    ->where(['nomenclature_id' => $row->id])
                    ->andWhere(['<','date',$this->getStartDate()])
                    ->groupBy(['nomenclature_id'])->scalar() ?: 0,
                'count_income' => WorkpieceDocument::find()
                    ->select('SUM(count * document_type) as amount')
                    ->where(['nomenclature_id' => $row->id])
                    ->andWhere(['document_type' => WorkpieceDocument::TYPE_IN])
                    ->andWhere(['>=','date',$this->getStartDate()])
                    ->andWhere(['<=','date',$this->getEndDateEndOfDay()])
                    ->groupBy(['nomenclature_id'])->scalar() ?: 0,
                'count_output' => WorkpieceDocument::find()
                    ->select('SUM(count * document_type) as amount')
                    ->where(['nomenclature_id' => $row->id])
                    ->andWhere(['document_type' => WorkpieceDocument::TYPE_OUT])
                    ->andWhere(['>=','date',$this->getStartDate()])
                    ->andWhere(['<=','date',$this->getEndDateEndOfDay()])
                    ->groupBy(['nomenclature_id'])->scalar() ?: 0,
                'count_at_end_date' => ($remains = WorkpieceDocument::find()
                    ->select('SUM(count * document_type) as amount')
                    ->where(['nomenclature_id' => $row->id])
                    ->andWhere(['<=','date',$this->getEndDateEndOfDay()])
                    ->groupBy(['nomenclature_id'])->scalar() ?: 0),
                'average_rate' => ($rate = $this->getAverageRate($row->id)),
                'projected_balance' => ($monthCount = $this->getProjectedBalance($remains, $rate)),
                'projected_balance_date' => $this->getProjectedBalanceDate($monthCount),
            ];
        }
        return $data;
    }

    /**
     * Cреднемесячный расход (Расход за период с 1 марта 2024 по текущий день разделить на кол-во дней начиная с 1 марта 2024 и умножить на 30)
     * Когда период станет больше 8 мес, считаем просто за последние 8 мес
     *
     * @param int $id
     * @return float
     * @throws Exception
     */
    private function getAverageRate(int $id): float
    {
        $firstDate = new DateTime('2024-03-01'); //Дата начало подсчета статистики

        $end = new DateTime(date('Y-m-d',$this->getEndDateEndOfDay()));
        $end->modify('+1 day');

        $begin = clone $end;
        $begin->modify('-8 month');

        $interval1 = $end->diff($begin);
        $interval2 = $end->diff($firstDate);
        $countDays = min($interval1->days, $interval2->days);
        $k = min($countDays, 30);

        $consumption = WorkpieceDocument::find()
            ->select('SUM(count) as amount')
            ->where(['nomenclature_id' => $id])
            ->andWhere(['document_type' => WorkpieceDocument::TYPE_OUT])
            ->andWhere(['>=','date',$begin->getTimestamp()])
            ->andWhere(['<=','date',$end->getTimestamp()])
            ->groupBy(['nomenclature_id'])->scalar() ?: 0;

        return  $countDays ? round(($consumption / $countDays) * $k, 2) : 0;
    }

    /**
     * Прогноз на сколько месяцев остаток: Остаток заготовок разделить на среднемесячный расход
     *
     * @param int $remains
     * @param float $rate
     * @return float|null
     */
    private function getProjectedBalance(int $remains, float $rate)
    {
        return $rate ? round($remains / $rate) : INF;
    }

    /**
     * Прогнозируемая дата
     *
     * @param $monthCount
     * @return string|null
     * @throws Exception
     */
    private function getProjectedBalanceDate($monthCount)
    {
        $date = new DateTime(date('Y-m-d',$this->getEndDateEndOfDay()));
        if ($monthCount === INF) return INF;
        return $monthCount ? $date->modify("+$monthCount month")->format('Y-m-d') : $date->format('Y-m-d');
    }

}