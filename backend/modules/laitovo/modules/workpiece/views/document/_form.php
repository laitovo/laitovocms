<?php

use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\modules\workpiece\models\WorkpieceNomenclature;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\modules\workpiece\models\WorkpieceDocument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="workpiece-document-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomenclature_id')->dropDownList(ArrayHelper::merge([''=>''], ArrayHelper::map(WorkpieceNomenclature::find()->all(), 'id', 'title')))->label(Yii::t('app', 'Номенклатура')) ?>

    <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::merge([''=>''], ArrayHelper::map(ErpUser::find()->where(['status' => 'Работает'])->all(), 'id', 'name')))->label(Yii::t('app', 'Сотрудник')) ?>

    <?= $form->field($model, 'document_type')->dropDownList(ArrayHelper::merge([''=>''], $model->getTypes()))->label(Yii::t('app', 'Тип движения')) ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'date')->widget(DatePicker::class, [
        'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
