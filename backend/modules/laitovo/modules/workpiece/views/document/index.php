<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\modules\workpiece\models\WorkpieceDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Документы движения заготовок');
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>
<div class="workpiece-document-index">

    <p>
        <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить документ')]) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'show' => ['id', 'nomenclature.title', 'user.name', 'document_type', 'count', 'date'],
        'tableOptions' => ['class' => 'table table-hover'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Номер документа',
                'attribute' => 'id'
            ],
            [
                'label' => 'Номенклатура',
                'attribute' => 'nomenclature.title'
            ],
            [
                'label' => 'Сотрудник',
                'attribute' => 'user.name'
            ],
            [
                'attribute' => 'document_type',
                'value' => function($model) {
                    return $model->getTypes()[$model->document_type];
                }
            ],
            'count',
            'date:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
