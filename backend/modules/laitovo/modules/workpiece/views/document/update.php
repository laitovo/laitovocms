<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\modules\workpiece\models\WorkpieceDocument */

$this->title = Yii::t('app', 'Изменить документ движения заготовок № {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Workpiece Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->render('@backendViews/laitovo/views/menu');

?>
<div class="workpiece-document-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
