<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\modules\workpiece\models\WorkpieceDocument */

$this->title = 'Документ движения №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Документы движения заготовок'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$this->render('@backendViews/laitovo/views/menu');

?>
<div class="workpiece-document-view">

    <p>
        <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Номер документа',
                'attribute' => 'id'
            ],
            [
                'label' => 'Номенклатура',
                'attribute' => 'nomenclature.title'
            ],
            [
                'label' => 'Сотрудник',
                'attribute' => 'user.name'
            ],
            [
                    'attribute' => 'document_type',
                    'value' => function($model) {
                        return $model->getTypes()[$model->document_type];
                    }
            ],
            'count',
            'date:date',
        ],
    ]) ?>

</div>
