<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\modules\workpiece\models\WorkpieceDocument */

$this->title = Yii::t('app', 'Добавить документ движения заготовок');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Документы движения заготовок'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>
<div class="workpiece-document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
