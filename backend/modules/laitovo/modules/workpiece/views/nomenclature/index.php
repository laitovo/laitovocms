<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\modules\workpiece\models\WorkpieceNomenclatureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Номенклатура заготовок');
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>
<div class="workpiece-nomenclature-index">

    <p>
        <?= Html::a(Yii::t('app', 'Добавить номенклатуру'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'show' => ['id', 'title'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
