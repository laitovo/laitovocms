<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\modules\workpiece\models\WorkpieceNomenclature */

$this->title = Yii::t('app', 'Добавить номенклатуру');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Номенклатура заготовок'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>
<div class="workpiece-nomenclature-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
