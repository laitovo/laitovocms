<?php

use backend\modules\laitovo\modules\workpiece\models\WorkpieceReport;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use backend\widgets\GridView;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\modules\workpiece\models\WorkpieceDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model WorkpieceReport */

$this->title = Yii::t('app', 'Отчет движения заготовок');
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>
<div class="workpiece-report-index">

    <?php $form = ActiveForm::begin(['method' => 'get']); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'startDate')->widget(DatePicker::class, [
                'language'   => 'ru',
                'options' => [
                    'class' => 'form-control',
                ],
                'dateFormat' => 'dd.MM.yyyy',
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'endDate')->widget(DatePicker::class, [
                'language'   => 'ru',
                'options' => [
                    'class' => 'form-control',
                ],
                'dateFormat' => 'dd.MM.yyyy',
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'show' => ['title', 'count_at_start_date', 'count_income', 'count', 'count_at_end_date', 'average_rate', 'projected_balance', 'projected_balance_date'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Номенклатура',
                'attribute' => 'title'
            ],
            [
                'label' => 'Остаток на начало периода',
                'attribute' => 'count_at_start_date'
            ],
            [
                'label' => 'Приход',
                'attribute' => 'count_income'
            ],
            [
                'label' => 'Расход',
                'attribute' => 'count_output'
            ],
            [
                'label' => 'Остаток на конец периода',
                'attribute' => 'count_at_end_date'
            ],
            [
                'label' => 'Средне-месячный расход, шт/мес',
                'attribute' => 'average_rate'
            ],
            [
                'label' => 'Прогноз остатка, мес',
                'attribute' => 'projected_balance',
                'format' => 'raw',
                'value' => function($data) {
                    return $data['projected_balance'] === INF ? '<span class="glyphicon glyphicon-option-horizontal"></span>' : $data['projected_balance'];
                }
            ],
            [
                'label' => 'Прогноз остатка, дата',
                'attribute' => 'projected_balance_date',
                'format' => 'raw',
                'value' => function($data) {
                    return $data['projected_balance_date'] === INF? '<span class="glyphicon glyphicon-option-horizontal"></span>' : $data['projected_balance_date'];
                }
            ],
        ],
    ]); ?>

</div>
