<?php

namespace backend\modules\laitovo\modules\workpiece\controllers;

use backend\modules\laitovo\modules\workpiece\models\WorkpieceDocument;
use backend\modules\laitovo\modules\workpiece\models\WorkpieceDocumentSearch;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * WorkpieceDocumentController implements the CRUD actions for WorkpieceDocument model.
 */
class DocumentController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all WorkpieceDocument models.
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new WorkpieceDocumentSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $sort = $dataProvider->getSort();
        $sort->defaultOrder = ['id'=> SORT_DESC];
        $sort->attributes = ArrayHelper::merge($sort->attributes,
            [
                'user.name' => [
                    'asc' => ['user.name' => SORT_ASC],
                    'desc' => ['user.name' => SORT_DESC],
                ],
                'nomenclature.title' => [
                    'asc' => ['nomenclature.title' => SORT_ASC],
                    'desc' => ['nomenclature.title' => SORT_DESC],
                ],
        ]);
        $dataProvider->setSort($sort);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WorkpieceDocument model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WorkpieceDocument model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new WorkpieceDocument();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing WorkpieceDocument model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return Response|string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WorkpieceDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return Response
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(int $id): Response
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WorkpieceDocument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return WorkpieceDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): WorkpieceDocument
    {
        if (($model = WorkpieceDocument::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
