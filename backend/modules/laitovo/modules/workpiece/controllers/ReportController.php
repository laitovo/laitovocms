<?php

namespace backend\modules\laitovo\modules\workpiece\controllers;

use backend\modules\laitovo\modules\workpiece\models\WorkpieceReport;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

/**
 * Report controller for the `workpiece` module
 */
class ReportController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex(): string
    {
        $model = new WorkpieceReport();
        $model->load(\Yii::$app->request->get());

        $dataProvider = new ArrayDataProvider([
            'allModels' => $model->generate(),
            'sort' => [
                'attributes' => [
                    'title',
                    'count_at_start_date',
                    'count_income',
                    'count_output',
                    'count_at_end_date',
                    'average_rate',
                    'projected_balance',
                    'projected_balance_date'
                ],
                'defaultOrder' => ['title' => SORT_ASC]
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }
}
