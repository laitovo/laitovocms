<?php

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>
<table class="invoice_items" width="300" cellpadding="2" cellspacing="2"
       style="border: none;font-size: 15px;text-align: center;">
    <tbody>
    <tr>
        <td style="padding: 3px;border:1px solid" colspan="2">
            <b>
                Protective screens for car windows<br>
                <?= $title ?>
            </b>

        </td>
    </tr>
    <tr>
        <td style="border-left: none;border-right: none; font-size: 17px;border-bottom: none">
            <br>
            <b><?= $article ?></b>
            <br>
            <br>
        </td>
    </tr>
    <tr>
        <td style="border-collapse: collapse;padding: 0;border-bottom: none;border: none;" colspan="2">
            <div style="text-align: center;"><div style="display: inline-block;"><?= $generator->getBarcode($barcode, $generator::TYPE_CODE_128, 2,
                        50) ?></div></div>
        </td>
    </tr>
    </tbody>
</table>
