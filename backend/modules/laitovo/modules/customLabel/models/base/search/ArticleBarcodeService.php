<?php
namespace backend\modules\laitovo\modules\customLabel\models\base\search;

class ArticleBarcodeService
{
    /**
     * Проверить - является ли строка штрихкодмо артикула и если чего вернуть артикул
     * @param $barcode
     * @return array
     */
    public function getItems($barcode) {
        $array = [];
        $checked = $this->_isArticle($barcode);
        if ($checked === false) {
            return $array;
        }

        $array['window'] = mb_substr($barcode,0,2);
        $array['car']    = ltrim(mb_substr($barcode,2,4),'0');
        $array['brand']  = mb_substr($barcode,8,1) == 5 ? 'Chiko' : 'Laitovo';

        return $array;
    }

    /**
     * Проверка - является ли штрихкод - штрихкодом артикула
     * @param $barcode
     * @return bool|null
     */
    private function _isArticle($barcode)
    {
        if (!is_string($barcode)) {
            return false;
        }

        $length = mb_strlen($barcode);
        if ($length !== 9) {
            return false;
        }

        $prefix = mb_substr($barcode,0,2);
        if (mb_strtolower($prefix) !== 'rd') {
            return false;
        }

        return true;
    }
}