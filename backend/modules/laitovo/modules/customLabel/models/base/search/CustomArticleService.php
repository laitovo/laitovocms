<?php
namespace backend\modules\laitovo\modules\customLabel\models\base\search;

class CustomArticleService
{
    public function getRow($article,$brand)
    {
        $mask = $brand === 'Chiko' ? 'NP-%-'.$article : 'LA-%-'.$article;

        $row = (new \yii\db\Query())
            ->select(['article', 'title','barcode'])
            ->from('custom_article')
            ->where('article like :mask', array(':mask'=>$mask))
            ->one();


        return $row ? (object)$row : $row;
    }

    public function getArticle($row)
    {
        return $row->article;
    }

    public function getTitle($row)
    {
        return $row->title;
    }

    public function getBarcode($row)
    {
        return $row->barcode;
    }

}