<?php
namespace backend\modules\laitovo\modules\customLabel\models\base\search;

use backend\helpers\ArticleHelper;
use backend\modules\logistics\models\Naryad;

class ErpNaryadService
{
    public function getNaryadByBarcode($barcode)
    {
        return Naryad::find()->where(['barcode' => $barcode,'isParent' => true])->one();
    }

    public function getCarArticle(Naryad $naryad)
    {
        return ArticleHelper::getCarArticle($naryad->article);
    }

    public function getWindow(Naryad $naryad)
    {
        return ArticleHelper::getWindow($naryad->article);
    }

    public function getBrand(Naryad $naryad)
    {
        return ArticleHelper::getBrand($naryad->article);
    }
}