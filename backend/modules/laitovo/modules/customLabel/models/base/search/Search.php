<?php
namespace backend\modules\laitovo\modules\customLabel\models\base\search;

use backend\helpers\BarcodeHelper;

/**
 * Class Search
 * @package backend\modules\laitovo\modules\customLabel\models\base\search
 */
class Search
{
    private $_service;
    private $_customService;
    private $_articleService;

    public function __construct ()
    {
        $this->_service = new ErpNaryadService();
        $this->_customService = new CustomArticleService();
        $this->_articleService = new ArticleBarcodeService();
    }

    public function search($barcode)
    {
        //Подготовили ответ
        $response = $this->_defaultResponse();

        //Сама логика
        $barcode = BarcodeHelper::toLatin($barcode);

        $items = $this->_articleService->getItems($barcode);

        if ( !empty($items) ) {
            $article = $items['car'];
            $brand   = $items['brand'];
            $window  = $items['window'];
            if (mb_strtolower($window) !== 'rd') {
                $response = $this->_setResponseMessage($response,'Вы должны пикать только наряды с ЗБ (ЗАДНИЕ БОКОВЫЕ)!!!');
                return $response;
            }
        } else {
            $parentUpn = $this->_service->getNaryadByBarcode($barcode);

            if (!$parentUpn) {
                $response = $this->_setResponseMessage($response,'Наряд по данному штрихоку не найден!!!');
                return $response;
            }

            $article = null;
            $brand = null;
            foreach ($parentUpn->children as $child) {
                $window = $this->_service->getWindow($child);
                $article = $this->_service->getCarArticle($child);
                $brand = $this->_service->getBrand($child);
                if ($window == 'ЗБ') break;
            }
            if ($window !== 'ЗБ' || !$article || !$brand) {
                $response = $this->_setResponseMessage($response,'Комплект должен содержать наряды с ЗБ (ЗАДНИЕ БОКОВЫЕ)!!!');
                return $response;
            }

        }
        $row = $this->_customService->getRow($article,$brand);

        if (!$row) {
            $response = $this->_setResponseMessage($response,'Для данного наряда не находится соответствующая Английская этикетка!!!');
            return $response;
        }

        $infoArticle = $this->_customService->getArticle($row);
        $infoTitle = $this->_customService->getTitle($row);
        $infoBarcode = $this->_customService->getBarcode($row);

        $data = [
            'article' => $infoArticle,
            'barcode' => $infoBarcode,
            'title'   => $infoTitle,
        ];

        $response = $this->_setResponseStatus($response,'success');
        $response = $this->_setResponseMessage($response,'Этикетка успешно распечатана!!!');
        $response = $this->_setResponseData($response,$data);

        return $response;
    }

    private function _defaultResponse()
    {
        $response = [];
        $response['status']  = 'error';
        $response['message'] = 'По данному наряду невозможно получить этикету для АНГЛИЙСКОГО комплекта';
        $response['data'] = null;

        return $response;
    }

    private function _setResponseMessage($response,$message)
    {
        $response['message'] = $message;
        return $response;
    }

    private function _setResponseStatus($response,$status)
    {
        $response['status'] = $status;
        return $response;
    }

    private function _setResponseData($response,$data)
    {
        $response['data'] = $data;
        return $response;
    }

}
