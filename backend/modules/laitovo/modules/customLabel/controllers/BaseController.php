<?php

namespace backend\modules\laitovo\modules\customLabel\controllers;

use backend\modules\laitovo\modules\customLabel\models\base\search\Search;
use yii\base\Module;
use yii\web\Controller;
use yii\web\Response;

/**
 * Контроллер для монитора этикеток
 *
 * Class BaseController
 * @package backend\modules\laitovo\modules\customLabel\controllers
 */
class BaseController extends Controller
{
    private $_search;

    public function __construct($id, Module $module, array $config = [])
    {
        $this->_search = new Search();
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        return $this->render('index/index');
    }

    public function actionSearch($barcode)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = $this->_search->search($barcode);

        return $response;
    }

    public function actionPrint($article,$title,$barcode)
    {
        return $this->renderPartial('print/print',[
            'article' => $article,
            'title'   => $title,
            'barcode' => $barcode
        ]);
    }
}
