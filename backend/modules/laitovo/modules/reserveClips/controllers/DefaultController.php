<?php

namespace backend\modules\laitovo\modules\reserveClips\controllers;

use yii\web\Controller;

/**
 * Default controller for the `reserveClips` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
