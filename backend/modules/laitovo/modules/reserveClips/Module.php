<?php

namespace backend\modules\laitovo\modules\reserveClips;

/**
 * reserveClips module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\laitovo\modules\reserveClips\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
