<?php

namespace backend\modules\laitovo\modules\premium\controllers;

use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\modules\premium\models\Premium;
use backend\modules\laitovo\modules\premium\models\PremiumSearch;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Контроллер журнала премий
 *
 * Class DefaultController
 * @package backend\modules\laitovo\modules\premium\controllers
 */
class DefaultController extends Controller
{
    /**
     * Список премий
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PremiumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Просмотр премии
     *
     * @param int $id Идентификатор премии
     * @return string
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Создание премии
     *
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Premium();
        $users = ErpUser::find()->where(['status' => 'Работает'])->orderBy('name')->select(['name', 'id'])->indexBy('id')->column();

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'users' => $users,
            ]);
        }
    }

    /**
     * Редактирование премии
     *
     * @param int $id Идентификатор премии
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $users = ErpUser::find()->where(['status' => 'Работает'])->orderBy('name')->select(['name', 'id'])->indexBy('id')->column();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'users' => $users,
            ]);
        }
    }

    /**
     * Удаление премии
     *
     * @param int $id Идентификатор премии
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductionScheme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Premium the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Premium::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
