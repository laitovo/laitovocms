<?php

namespace backend\modules\laitovo\modules\properties\modules\brand\models\crud;

use Yii;

class Create
{
    private $_service;

    public function __construct()
    {
        $this->_service = Yii::$container->get('core\entities\propBrand\BrandManager');
    }

    public function createModel()
    {
        return $this->_service->create();
    }

    public function save($model, $params)
    {
        if (!$this->_service->fillByForm($model, $params) || !$this->_service->save($model)) {
            return false;
        }

        return $this->_service->getId($model);
    }
}


