<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var yii\web\View $this */
/* @var mixed $model */
/* @var bool $isNew */

?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name'); ?>
<?= $form->field($model, 'instructionAlias'); ?>
<?= $form->field($model, 'description')->textarea(['rows' => '6']) ?>

<div class="form-group">
    <?php if ($isNew) :?>
        <?= Html::submitButton('<i class="icon wb-plus"></i> ' . Yii::t('app', 'Добавить'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
    <?php else: ?>
        <?= Html::submitButton('<i class="icon wb-check"></i> ' . Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="icon wb-close"></i> ' . Yii::t('app','Отмена'), ['view','id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('<i class="icon wb-trash"></i> ' . Yii::t('app','Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger deleteconfirm',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    <?php endif;?>
</div>

<?php ActiveForm::end(); ?>
