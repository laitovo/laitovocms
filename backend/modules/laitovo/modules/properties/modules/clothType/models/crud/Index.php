<?php

namespace backend\modules\laitovo\modules\properties\modules\clothType\models\crud;

use yii\data\ArrayDataProvider;
use Yii;

class Index
{
    private $_service;

    public function __construct()
    {
        $this->_service = Yii::$container->get('core\entities\propClothType\ClothTypeManager');
    }

    public function getProvider()
    {
        return new ArrayDataProvider([
            'key'       => $this->_service->getPrimaryKey(),
            'allModels' => $this->_service->findAll(),
            'sort'      => [
                'attributes' => $this->_service->getAttributes(),
            ],
        ]);
    }

    public function getAttributes()
    {
        return $this->_service->getAttributes();
    }
}


