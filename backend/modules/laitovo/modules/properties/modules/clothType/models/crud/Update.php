<?php

namespace backend\modules\laitovo\modules\properties\modules\clothType\models\crud;

use Yii;

class Update
{
    private $_service;

    public function __construct()
    {
        $this->_service = Yii::$container->get('core\entities\propClothType\ClothTypeManager');
    }

    public function findModel($id)
    {
        return $this->_service->findById($id);
    }

    public function save($model, $params)
    {
        if (!$this->_service->fillByForm($model, $params) || !$this->_service->save($model)) {
            return false;
        }

        return $this->_service->getId($model);
    }
}


