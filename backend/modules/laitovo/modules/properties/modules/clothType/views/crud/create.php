<?php

/* @var yii\web\View $this */
/* @var mixed $model */

$this->render('@backendViews/laitovo/views/menu');

$this->title = Yii::t('app', 'Добавить вид тканей');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['/laitovo/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Виды тканей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'isNew' => true,
]) ?>
    