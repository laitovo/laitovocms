<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var yii\web\View $this */
/* @var mixed $model */
/* @var array $attributes */

$this->title = Yii::t('app', 'Просмотр вида тканей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['/laitovo/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Виды тканей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>

<div class="form-group">
    <?= Html::a('<i class="icon wb-edit"></i> ' . Yii::t('app','Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('<i class="icon wb-trash"></i> ' . Yii::t('app','Удалить'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger deleteconfirm',
        'data' => [
            'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
            'method' => 'post',
        ],
    ]) ?>
    <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
</div>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => $attributes
]) ?>
