<?php

namespace backend\modules\laitovo\modules\properties\modules\windowOpeningType\models\crud;

use Yii;

class View
{
    private $_service;

    public function __construct()
    {
        $this->_service = Yii::$container->get('core\entities\propWindowOpeningType\WindowOpeningTypeManager');
    }

    public function findModel($id)
    {
        return $this->_service->findById($id);
    }

    public function getAttributes()
    {
        return $this->_service->getAttributes();
    }
}


