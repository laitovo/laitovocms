<?php

use backend\widgets\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $provider yii\data\ArrayDataProvider */
/* @var $attributes array */

$this->title = Yii::t('app', 'Виды оконных проемов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['/laitovo/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>

<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'summary' => false,
    'dataProvider' => $provider,
    'show' => $attributes,
    'columns' => array_merge(
        [['class' => 'yii\grid\SerialColumn']],
        $attributes,
        [[
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ]]
    )
]) ?>
