<?php

namespace backend\modules\laitovo\modules\properties\modules\executionType\models\crud;

use Yii;

class Delete
{
    private $_service;

    public function __construct()
    {
        $this->_service = Yii::$container->get('core\entities\propExecutionType\ExecutionTypeManager');
    }

    public function findModel($id)
    {
        return $this->_service->findById($id);
    }

    public function deleteModel($model)
    {
        return $this->_service->delete($model);
    }
}


