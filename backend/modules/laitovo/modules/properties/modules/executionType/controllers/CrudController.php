<?php

namespace backend\modules\laitovo\modules\properties\modules\executionType\controllers;

use backend\modules\laitovo\modules\properties\modules\executionType\models\crud\Create;
use backend\modules\laitovo\modules\properties\modules\executionType\models\crud\Delete;
use backend\modules\laitovo\modules\properties\modules\executionType\models\crud\Index;
use backend\modules\laitovo\modules\properties\modules\executionType\models\crud\Update;
use backend\modules\laitovo\modules\properties\modules\executionType\models\crud\View;
use yii\web\Controller;
use yii\base\Module;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

class CrudController extends Controller
{
    private $_index;
    private $_view;
    private $_create;
    private $_update;
    private $_delete;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_index = new Index();
        $this->_view = new View();
        $this->_create = new Create();
        $this->_update = new Update();
        $this->_delete = new Delete();

        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            #Ограничиваю доступ к отчету для пользователей
                            $users = [21,48];
                            if (!in_array(Yii::$app->user->getId(),$users))
                                return false;
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'provider'   => $this->_index->getProvider(),
            'attributes' => $this->_index->getAttributes(),
        ]);
    }

    public function actionView($id)
    {
        if (!($model = $this->_view->findModel($id))) {
            throw new NotFoundHttpException('Элемент не найден');
        }

        return $this->render('view', [
            'model'      => $model,
            'attributes' => $this->_view->getAttributes(),
        ]);
    }

    public function actionCreate()
    {
        $model = $this->_create->createModel();
        if ($id = $this->_create->save($model, Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        if (!($model = $this->_update->findModel($id))) {
            throw new NotFoundHttpException('Элемент не найден');
        }
        if ($id = $this->_update->save($model, Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        if (!($model = $this->_delete->findModel($id))) {
            throw new NotFoundHttpException('Элемент не найден');
        }
        $this->_delete->deleteModel($model);

        return $this->redirect(['index']);
    }
}
