<?php

namespace backend\modules\laitovo\modules\properties;

class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        $this->modules = [
            'window-opening-type' =>
                ['class' => 'backend\modules\laitovo\modules\properties\modules\windowOpeningType\Module'],
            'brand' =>
                ['class' => 'backend\modules\laitovo\modules\properties\modules\brand\Module'],
            'execution-type' =>
                ['class' => 'backend\modules\laitovo\modules\properties\modules\executionType\Module'],
            'cloth-type' =>
                ['class' => 'backend\modules\laitovo\modules\properties\modules\clothType\Module'],
        ];
    }
}
