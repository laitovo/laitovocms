<?php
namespace backend\modules\laitovo\modules\universalInstruction\models;

use backend\helpers\ArticleBuilder;
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsSchemeNew;
use common\models\laitovo\Cars;
use yii\base\Model;
use yii\web\NotFoundHttpException;

/**
 * Класс для передачи всех данных по артикулу в сами инструкции для их формирования.
 *
 * Class InstructionDTO
 * @package backend\modules\laitovo\modules\universalInstruction\models
 *
 * @property $article string
 */
class InstructionDTO extends Model
{
    /**
     * @var $_article string [ Артикул продукта ]
     */
    private $_article;

    /**
     * @var $_articleDTO ArticleDTO [ ОБъект ДТО артикула со всей информцией по артикулу ]
     */
    private $_articleInfo;

    /**
     * @var $schemeClipsSrc string|null [ Путь до схемы c расстановкой клипс ]
     */
    public $schemeClipsSrc;

    /**
     * @var $schemeMagnetsSrc string|null [ Путь до схемы с расстановкой магнитов ]
     */
    public $schemeMagnetsSrc;

    /**
     * @var $schemeMagnetsSrc string|null [ Путь до схемы бех расстановок (чистой схемы) ]
     */
    public $schemeSrc;

    /**
     * @var $articleList array [ Массив артикулов с аналогами, штрихкодами и наименованиями автомобилей ]
     */
    public $articleList;

    /**
     * InstructionDTO constructor.
     * @param $article string [ Артикул продукта ]
     * @param array $config
     */
    public function __construct($article,array $config = [])
    {
        parent::__construct($config);

        $this->_article = $article;
        $this->_articleInfo = new ArticleDTO($this->_article);

        $this->schemeClipsSrc   = $this->generateClipsScheme();
        $this->schemeMagnetsSrc = $this->generateMagnetsScheme();
        $this->schemeSrc        = $this->generateClearScheme();

        $this->articleList      = $this->buildCarsList($this->_article);
    }

    /**
     * @return ArticleDTO
     */
    public function getArticleInfo()
    {
        return $this->_articleInfo;
    }

    /**
     * Отдает схему на клипсах
     * @return CarsSchemeNew
     */
    private function generateClipsScheme()
    {
        return $this->getScheme('Простые');
    }

    /**
     * Отдает схему на магнитах
     * @return CarsSchemeNew
     */
    private function generateMagnetsScheme()
    {
        return $this->getScheme('Магнитные');
    }

    /**
     * Отдает пустую схему
     * @return CarsSchemeNew
     */
    private function generateClearScheme()
    {
        return $this->getScheme();
    }

    /**
     * Получаем объект схемы.
     *
     * @param null $typeClips
     * @return CarsSchemeNew
     */
    private function getScheme($typeClips = null)
    {
        $articleInfo = $this->_articleInfo;

        $carModel = null;
        if ($this->_articleInfo->carArticle && ($car = Cars::find()->where(['article' => $this->_articleInfo->carArticle])->one()) != null) {
            try{
                $carModel = new CarsForm($car->id);
            }catch(NotFoundHttpException $exception) {}
        }

        /**
         * Еси указано использовать крепления на скотче, тогда ище схему на скотче
         */
        if ($typeClips == 'Простые' && $articleInfo->isTape) {
            $typeClips = 'Скотч';
        }

        $scheme = new CarsSchemeNew([
            'model'      => $carModel,
            'brand'      => $articleInfo->brand,
            'type'       => $articleInfo->type,
            'type_clips' => $typeClips,
            'window'     => $articleInfo->window,
        ]);

        return $scheme->file();
    }

    /**
     * Формирование списка артикулов и аналогов по артикулу.
     *
     * @param $article
     * @return array
     */
    private function buildCarsList($article)
    {
        return ArticleBuilder::buildList($article);
    }



}