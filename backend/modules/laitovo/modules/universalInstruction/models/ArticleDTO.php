<?php
namespace backend\modules\laitovo\modules\universalInstruction\models;

use yii\base\Model;
use backend\helpers\ArticleHelper;

/**
 * Класс содержит в себе всю информацию по артикулу
 *
 * Class ArticleDTO
 * @package backend\modules\laitovo\modules\universalInstruction\models
 *
 */
class ArticleDTO
{
    /**
     * @var $_article string [ Артикул продукции ]
     */
    private $_article;

    /**
     * @var $carArticle string|null [ Артикул автомобиля ]
     */
    public $carArticle;

    /**
     * @var $brand string [ Бренд продукции ]
     */
    public $brand;

    /**
     * @var $type string [ Вид исполнения продукции ]
     */
    public $type;

    /**
     * @var $window string [ Оконный проем, для кототорой создана продукция ]
     */
    public $window;

    /**
     * @var $windowEn string [ Оконный проем, для кототорой создана продукция (Латиница) ]
     */
    public $windowEn;

    /**
     * @var $enTypeTitle string [ Наименование вида исполнения на английском языке ]
     */
    public $enTypeTitle;

    /**
     * @var $ruTypeTitle string [ Наименование вида исполнения на русском языке ]
     */
    public $ruTypeTitle;

    /**
     * @var $enWindowTitle string [ Наименование оконного проема на английском языке ]
     */
    public $enWindowTitle;

    /**
     * @var $ruWindowTitle string [ Наименование оконного проема на русском языке ]
     */
    public $ruWindowTitle;

    /**
     * @var $countWindows integer [ Количество защитных экранов в артикуле ]
     */
    public $countWindows;

    /**
     * @var $countWindows boolean [ Нужен ли крючек при установке ]
     */
    public $needHook;

    /**
     * @var $clipsWithCount array [ Список клипс и их количество ]
     */
    public $clipsWithCount;

    /**
     * @var $magnetsWithCount array [ Список магнитов и их количество ]
     */
    public $magnetsWithCount;

    /**
     * @var $clipsWithTypes array [ Список клипс и их типы ]
     */
    public $clipsWithTypes;

    /**
     * @var $bwSklad boolean [ Является ли задняя шторка складной ]
     */
    public $bwSklad;

    /**
     * @var $hasVelkro boolean [ Имеет ли артикул клипсу "Велькро"]
     */
    public $hasVelkro;

    /**
     * @var $isMagnet boolean [ Является ли номенклатура Magnet]
     */
    public $isMagnet;

    /**
     * @var $isOnMagnets boolean [ Является ли номенклатура - номенклатурой на магнитах]
     */
    public $isOnMagnets;

    /**
     * @var $isOnlyMagnets boolean [ Устанавливаются только магнитные держатели ]
     */
    public $isOnlyMagnets;

    /**
     * @var $isTape boolean [ Является ли схема - схемой расстовки креплений на скотче]
     */
    public $isTape;

    /**
     * ArticleDTO constructor.
     * @param $article string [ Артикул продукции ]
     */
    public function __construct($article)
    {
        $this->_article = $article;

        $this->carArticle       = ArticleHelper::getCarArticle($this->_article);
        $this->brand            = ArticleHelper::getBrand($this->_article);
        $this->type             = ArticleHelper::getType($article);
        $this->window           = ArticleHelper::getWindow($article);
        $this->windowEn         = ArticleHelper::getWindowEn($article);

        $this->countWindows     = ArticleHelper::getCountWindow($article);
        $this->needHook         = ArticleHelper::needHook($article);
        $this->clipsWithCount   = ArticleHelper::getClipsOnly($article);
        $this->magnetsWithCount = ArticleHelper::getMagnets($article);
        $this->isOnlyMagnets    = ( empty($this->clipsWithCount ) && !empty($this->magnetsWithCount) );
        $this->clipsWithTypes   = ArticleHelper::getClipsWithTypes($article);
        $this->bwSklad          = ArticleHelper::isBwSklad($article);
        $this->isMagnet         = ArticleHelper::isMagnet($article);
        $this->isOnMagnets      = ArticleHelper::isOnMagnets($article);
        $this->isTape           = ArticleHelper::isTape($article);
        $this->hasVelkro        = isset($this->clipsWithTypes['V']);

        $this->enTypeTitle      = ArticleHelper::getTypeTitleEn($article);
        $this->ruTypeTitle      = ArticleHelper::getTypeTitleRu($article);
        $this->enWindowTitle    = ArticleHelper::getWindowTitleEn($article);
        $this->ruWindowTitle    = ArticleHelper::getWindowTitleRu($article);
    }

    /**
     * getter Article
     * @return string
     */
    public function getArticle()
    {
        return $this->_article;
    }
}