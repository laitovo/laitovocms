<?php
namespace backend\modules\laitovo\modules\universalInstruction\controllers;

use backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO;
use Yii;
use yii\helpers\ArrayHelper;
use \yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller
{
    /**
     * Действия для печати инструкции
     *
     * @param $article string [ Артикул продукта, для которого необходимо распечатать инструкции ]
     * @param $lang string|null [ Артикул продукта, для которого необходимо распечатать инструкции ]
     * @return string [ Инструкцию в формате HTML ]
     */
    public function actionPrint($article, $lang = null)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $dto = new InstructionDTO($article);
        return $this->renderPartial('index',[ 'dto' => $dto , 'lang' => $lang]);
    }

    /**
     * Действия для печати инструкции
     *
     * @param $article string [ Артикул продукта, для которого необходимо распечатать инструкции ]
     * @param $lang string|null [ Артикул продукта, для которого необходимо распечатать инструкции ]
     * @return string [ Инструкцию в формате HTML ]
     */
    public function actionPrintWalser($article)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $dto = new InstructionDTO($article);
        return $this->renderPartial('_text/walser/walser',[ 'dto' => $dto]);
    }

    /**
     * @param string $articles http://laitovo.loc/download-instruction/?articles=RD-F-112-1-2_RV-F-112-1-2_BW-F-112-1-5
     * артиулы передаются через _
     * @return mixed|string
     */
    public function actionPrintMany($articles, $lang = null, $clearCache = false)
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;

            ini_set("max_execution_time", "0");
            ini_set("memory_limit","-1");
            $key = $articles . ($lang ?: '_default');
            $articles = explode('_', $articles);
            if (\Yii::$app->cache->exists($key) && !$clearCache)
                return ['html' => \Yii::$app->cache->get( $key)];
            $html = '';
            $i = 1;
            $count = count($articles);
            foreach ($articles as $article) {
                $dto = new InstructionDTO(mb_strtoupper($article));
                $html .=  $this->renderPartial('index',[ 'dto' => $dto  , 'lang' => $lang]);
                if ($i < $count) {
                    $html .= "<p style='page-break-before: always;'>&nbsp;</p>";
                }
            }
            \Yii::$app->cache->set($key, $html,604800);
            return ['html' => $html];
        } catch (\Exception $e) {
            throw new \DomainException($e->getMessage());
        }

    }
}
