<?php
namespace backend\modules\laitovo\modules\universalInstruction\controllers;

use backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO;
use backend\modules\laitovo\modules\universalInstruction\models\OrderSearch;
use backend\modules\logistics\models\Order;
use \yii\web\Controller;
use Yii;

class RomaniaController extends Controller
{
    /**
     * Действия для печати инструкции
     *
     * @param $article string [ Артикул продукта, для которого необходимо распечатать инструкции ]
     * @return string [ Инструкцию в формате HTML ]
     */
    public function actionPrint($article)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $dto = new InstructionDTO($article);

//        echo '<pre>';
//        var_dump($dto);
//        die;
//        echo '</pre>';

        return $this->renderPartial('index',[ 'dto' => $dto ]);
    }


    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintForOrder($orderId)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","512M");

        $articles = Order::find()->joinWith('orderEntries')->select('logistics_order_entry.article')->where(['source_innumber' => $orderId])->column();

        $content = '';
        $content .= '
              <style>
                   @media print {
                    .more {
                     page-break-after: always;
                    } 
                   } 
              </style>';
        $count = 0;
        $dtos = [];
        foreach ($articles as $article) {
            if (!isset($dtos[$article])) {
                $dto = new InstructionDTO($article);
                $dtos[$article] = $dto;
            }
            $dto = $dtos[$article];
            $content .= $this->renderPartial('index',[ 'dto' => $dto ]) . '<div class="more"></div>';
        }

        return $content;
    }
}
