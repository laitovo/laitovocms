<?php
/* @var $this yii\web\View */
/* @var $dto \backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO */
?>
<!--Вся инструкция будет ввиде таблицы-->
<table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial" >
    <!--Блок 1-ый. Вводная верхня надпись для инструкции-->
    <tr>
        <td colspan="4" style="background-color:; text-align: left">
            <?= $this->render('_header', ['info' => $dto->getArticleInfo()]);?>
        </td>
    </tr>
    <!--Блок 2-ой. Артикулы и автомобили-->
    <tr>
        <td colspan="4" style="text-align: left">
            <hr size="4" color="black">
            <?= $this->render('_cars',   ['list' => $dto->articleList]);?>
            <hr size="4" color="black">
        </td>
    </tr>
    <!--Блок 3-ой. Блок схем креплений-->
<!--    <tr>-->
<!--        <td colspan="2" style="text-align: left">-->
<!--            --><?//= $this->render('clips',['file' => $scheme->file()]);?>
<!--        </td>-->
<!--        <td colspan="2" style="text-align: left">-->
<!--            --><?//= $this->render('magnets',['article' => $model->article]);?>
<!--        </td>-->
<!--    </tr>-->
    <tr>
        <td colspan="4" style="text-align: left">
            <?= $this->render('_schemes',['dto' => $dto]);?>
        </td>
    </tr>
    <!--Блок 4-ый. Блок изображений установки-->
    <tr>
        <td colspan="4" style="text-align: left">
            <?= $this->render('_pictures',['info' => $dto->getArticleInfo()]);?>
        </td>
    </tr>
    <!--Блок 5-ый. Блок текст установки-->
    <tr>
        <td colspan="4" style="text-align: left">
            <?= $this->render('_text/_main',[
                'dto' => $dto,
                'article' => $dto->getArticleInfo()->getArticle(),
            ]);?>
        </td>
    </tr>
</table>