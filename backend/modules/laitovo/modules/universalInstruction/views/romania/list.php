<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'source_innumber',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{today_action}',
            'buttons' => [
                'today_action' => function ($url, $data){
                    return Html::a('<span class="glyphicon glyphicon-print"></span>', ['print-for-order','orderId' => $data->source_innumber ],
                        [
                            'title' => Yii::t('app', 'Добавить в группу'),
                            'target' => '_blank',
                        ]);
                }
            ],
        ],
    ],
]); ?>