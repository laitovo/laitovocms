<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 9:37 AM
 */
use Picqer\Barcode\BarcodeGeneratorHTML as BarcodeGenerator;
use backend\helpers\ArticleBuilder;

/**
 * @var $list array
 */

$generator = new BarcodeGenerator();

?>
<div style="text-align: center">
    Produs compatibil:
</div>
<hr>
<?php foreach ($list as $row):?>
    <div style="width: 50%;float: left;">
        <div style="width: 40%;float: left">
            <div style="padding-left:5%">
                <?= $generator->getBarcode($row->barcode, $generator::TYPE_CODE_128, 1)?>
                <div style="text-align: left;font-size: 12px"><?=$row->article?></div>
            </div>
        </div>
        <div style="width: 60%;float: left">
            <span style="font-size: 14px;font-weight: bold"><?=$row->carTitle?> Code: <?= \backend\helpers\ArticleHelper::getCarArticle($row->article)?></span>
        </div>
        <div style="display: table; clear:both"></div>
    </div>
<?php endforeach;?>
<div style="display: table; clear:both"></div>


