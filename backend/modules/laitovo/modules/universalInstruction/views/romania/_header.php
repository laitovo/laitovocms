<?php

use \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO;
use \backend\helpers\ArticleHelper;

/**
 * @var $info ArticleDTO
 */
?>

<div style="font-size:9.9px;text-align: center">
    INSTRUCTION FOR INSTALLATION AND SERVICING OF THE SET OF PROTECTIVE SCREENS FOR CAR WINDOWS <b>·</b>
    ANLEITUNG FÜR MONTAGE UND BETRIEB VOM SONNENSHUTZ-SET FÜR AUTOFENSTER<b>·</b>
    INSTRUCTIUNI PENTRU INSTALAREA SETULUI DE PERDELUTE
</div>
<div style="font-size:18px;text-align: center">
    <?= $info->enTypeTitle?> / <?= $info->enWindowTitle?>
</div>