<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:43 AM
 */
use backend\modules\laitovo\models\CarsPictures;

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

$pictureManager = new CarsPictures($info->getArticle());
$pictures = $pictureManager->getImages();

?>
<div style="margin: 20px 0px">
<!--    --><?php //foreach ($pictures as $picture) {?>
<!--        <div style="text-align: center">-->
<!--            <img style="height: 180px;" src="--><?//=$picture->src?><!--" /><br>-->
<!--            <p>Pic.--><?//=$picture->num?><!-- / Abb. --><?//=$picture->num?><!-- / Рис. --><?//=$picture->num?><!--</p>-->
<!--        </div>-->
<!--    --><?php //} ?>
    <?php foreach ($pictures as $key => $picture) {?>
        <div style="text-align: center">
            <img style="height: 180px;" src="<?=$picture->src?>" /><br>
            <p>Pic.<?=$picture->num?> / Abb. <?=$picture->num?> / Fig. <?=$picture->num?></p>
        </div>
    <?php } ?>
</div>