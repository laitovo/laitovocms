<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

?>
<h4>КОМПЛЕКТАЦИЯ</h4>
<!--Комплектация-->
<div >
    <div style="width: 50%;float: left">
        <ul>
            <li>
                Защитный экран ............ <?= $info->countWindows ?>
            </li>
            <li>
                Инструкция  .................... 1
            </li>
            <?if ($info->needHook):?>
                <li>
                    Установочный крючок ... 1
                </li>
            <?endif;?>
        </ul>
    </div>
    <div style="width: 50%;float: left">
        <ul>
            <?if (!$info->isMagnet && !$info->isOnMagnets) :?>
                <? if ($info->isOnlyMagnets) :?>
                    <li>
                        Комплект магнитных держателей (10) ... 1
                    </li>
                <?elseif ($info->isTape):?>
                    <li>
                        Комплект зажимов на скотче (8) ... 1
                    </li>
                <?else :?>
                    <? foreach ($info->clipsWithCount as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Комплект магнитных держателей (10) ... 1
                            </li>
                        <? else : ?>
                            <li>
                                Зажим № <?= $key ?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endif; ?>
        </ul>
    </div>
    <div style="display: table; clear:both"></div>
</div>