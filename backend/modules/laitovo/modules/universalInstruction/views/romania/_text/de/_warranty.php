<h4>GARANTIEVERPFLICHTUNGEN.</h4>
<p>
    Bei der gehörigen Erfüllung der Betriebsvorschriften vom Kunden, der Verkäufer setzt eine Garantiefrist für Sonnenschutztschirme für 12 Monate ab Zeit des Empfangs der Ware.
    Garantieverpflichtungen erstrecken sich über die Ganzheit der Nahen (Fugen), die Ganzheit des Metallrahmens und der Befestigungen, die Ganzheit des Gewebes und der Gewebebeschichtung.
</p>

