<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

$clips = $info->clipsWithTypes;
if (isset($clips['V'])) { unset($clips['V']);}
?>

<h4>ENTFERNEN DES SCHUTZSCHIRMS.</h4>
<ul style="list-style-type: decimal;padding: 0px 20px">
    <?if ($info->isMagnet || $info->isOnlyMagnets || $info->isOnMagnets):?>
        <li>
            Fassen Sie die Schleife oben auf dem Schutzschirm.
        </li>
        <li>
            Ziehen Sie den Schutzschirmrand leicht zur Mitte.
        </li>
    <? else:?>
        <?if (!empty($clips)) :?>
            <!--Если есть клипсы-->
            <li>
                Fassen Sie die Schleife oben auf dem Schutzschirm.
            </li>
            <li>
                Ziehen Sie den Schutzschirmrand leicht zur Mitte.
            </li>
            <li>
                Entfernen Sie den Schutzschirm aus der Halteklammer.
            </li>
            <li>
                Entfernen Sie den Schutzschirm aus der anderen Halteklammern.
            </li>
        <?else :?>
            <!--Если нет клипс-->
            <li>
                Fassen Sie die Schleife oben auf dem Schutzschirm.
            </li>
            <li>
                Ziehen Sie den Schutzschirmrand leicht zur Mitte.
            </li>
            <li>
                Nehmen Sie den oberen Teil vom Schutzschirm raus.
            </li>
            <li>
                Nehmen Sie den Schutzschirm komplett raus.
            </li>
        <?endif;?>
    <?endif;?>
</ul>

