<h4>PFLEGE VON SCHUTZSCHIRMEN</h4>
<p>
    Wenn die Gewebeoberﬂäche verschmutzt ist, empﬁehlt es sich, sie mit einem feuchten Tuch
    oder Schwamm zu reinigen. Bei starker Verschmutzung können schonende Reinigungsmittel
    oder Mittel zur Pﬂege von Gewebeoberﬂächen verwendet werden
</p>