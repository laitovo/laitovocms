<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 * @var $pictures array
 * @var $picNumbers array
 */

$newClips = $info->clipsWithTypes;
?>
<h4>INSTALLATION</h4>
<p>
    Stellen Sie vor der Installation sicher, dass Sie ein Kit erworben haben, das Ihrer
    Automarke und der in der Anleitung angegebenen Fensteröﬀnung entspricht, und
    versuchen Sie zuerst, einen Schutzschirm gegen das Fenster des Autos anzubringen.
    Überprüfen Sie die Konformität der in der Anleitung angegebenen Ausrüstung mit der
    tatsächlichen Verfügbarkeit.
</p>
<? if ($info->isMagnet || $info->isOnMagnets):?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <li>
            Installieren Sie den Schutzschirm in der Fensteröffnung, die in der Anleitung angegeben ist.2
        </li>
        <li>
            Wenn die Fensteröffnung der Tür Ihres Autos eine nichtmetallische Verkleidung hat, verwenden Sie Metallhalter (mitgeliefert), um den Schutzschirm zu befestigen.
        </li>
        <?if ($info->needHook):?>
            <li>
                Wenn im Lieferumfang Ihres Bausatzes ein Installationshaken enthalten ist, verwenden
                Sie ihn zum Biegen der Fensterverkleidung
            </li>
        <?endif;?>
        <li>
            Finden Sie die Stellen der zwischengenähten Magneten auf dem Schutzschirmrahmen. Ohne den Schutzfilm
            vom Klebeband zu entfernen, verteilen Sie zusätzliche Metallverschlüsse gleichmäßig auf dem
            Schutzschirmrahmen und befestigen Sie sie mit Magneten, die in den Schutzschirmrahmen eingenäht sind.
        </li>
        <li>
            Bringen Sie den Schutzschirm an die Fensteröffnung an, um die Montagestellen der Befestigungen an der
            Türverkleidung zu bestimmen (auf den Metallteilen der Fensteröffnungsverkleidung).
        </li>
        <li>
            Installieren Sie die Befestigungen nacheinander und fixieren Sie sie mit Klebeband am Gehäuse.
        </li>
        <li>
            Installieren Sie den Schutzschirm in der Fensteröffnung, stellen Sie sicher,
            dass die Magnete den Schutzschirm sicher an den Metallhaltern befestigt haben.
        </li>
    </ul>
<? elseif ($info->isTape) :?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Öffnen Sie die Tür und lassen Sie die Scheibe runter.
            </li>
        <?endif;?>
        <?php foreach ($newClips as $name => $type) {?>
            <!--            <li>-->
            <!--                Возьмите пакетик с зажимами держателями №--><?//=$key;?>
            <!--                и установите их в места, обозначенные на схеме (рис.1) под №--><?//=$key;?><!--,-->
            <!--                так как показано на рис.--><?//=isset($pictures[$value])?$pictures[$value]->num:'';?><!--.-->
            <!--            </li>-->
            <li>
                Nehmen Sie den Beutel mit den Halteklammern und installieren  Sie diese an die Stellen,
                die im Diagramm (Abb. 1) angegeben sind, so wie in Abb. <?=isset($picNumbers[$type])?implode(', ',$picNumbers[$type]):'';?>) gezeigt ist.
                Vorläufig wärmen Sie die Klammern mit der warmen Luft aus der Luftleitung im Auto.
            </li>
        <?php } ?>
        <li>
            Entfetten Sie die Montagestellen der Halteklammern an der Türverkleidung.
        </li>
        <li>
            Entfernen Sie den Schutzfilm von den Klammern, die sich auf dem oberen Bogen der Türöffnung befinden,
            ohne die Klammern vom Schutzschirm  zu entfernen. (Restliche Klammer müssen mit dem Schutzfilm auf dem Schutzschirm bleiben).
            Montieren Sie die Sonnenschutzblende so in die Fensteröffnung, dass keine Spalte entstehen, und drücken Sie die Klammern  ohne Schutzfolie an das Gehäuse.
        </li>
        <li>
            Wiederholen Sie den Vorgang in der gleichen Reihenfolge mit den Klammern an der vertikalen Säule der Türöffnung.
        </li>
        <li>
            Wiederholen Sie den Vorgang in der gleichen Reihenfolge mit den verbleibenden Klammern (sofern diese für das Modell Ihres Autos vorhanden sind).
        </li>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Lassen Sie die Scheibe hoch.
            </li>
        <?endif;?>
        <li>
            Es wird nicht empfohlen, die Sonnenschutzschirme innerhalb von 24 Stunden zu entfernen, damit das Band sich besser befestigt.
        </li>
    </ul>
<? else:?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <?php if ($info->bwSklad):?>
            <li>
                Wenn Sie einen Klappschutzschirm gekauft haben, müssen Sie ihn vor der
                Installation zusammenbauen. Fassen Sie an der Stelle der Biegung des
                Schutzschirms das Verbindungsrohr und stecken Sie es in die gegenüberliegende
                Kante des Rahmens ein, wie in Abb.<?=isset($pictures['bwSklad'])?$pictures['bwSklad']->num:'';?> gezeigt ist.
            </li>
        <?php endif;?>
        <?php if ($info->hasVelkro):?>
            <li>
                Nehmen Sie den Schutzschirm an der Stelle, wo das Klettband ist, und kleben
                Sie es an die Fensteröffnung an die angegebene Stelle (Abb. 1) "V"-förmig, wie in Abb.
                <?=isset($pictures['V'])?$pictures['V']->num:'';?> gezeigt ist.
            </li>
            <?php
            $velkro = true;
            unset($newClips['V']);
            ?>
        <?php endif;?>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Installieren Sie den Schutzschirm in die Fensteröﬀnung, die in der
                Anleitung angegeben ist.
            </li>
        <?endif;?>
        <?if ($info->needHook):?>
            <li>
                Wenn im Lieferumfang Ihres Bausatzes ein Installationshaken enthalten ist, verwenden
                Sie ihn zum Biegen der Fensterverkleidung
            </li>
        <?endif;?>
        <?if (empty($newClips) || isset($velkro) || in_array($info->windowEn,['BW']) ):?>
            <li>
                Installieren Sie den Schutzschirm in der Fensteröffnung, die in der Anleitung angegeben ist.<?= (!isset($velkro)) && !empty($pictures) && empty($newClips) ? ' Abb.' . @$pictures[0]->num : '' ?>
            </li>
        <?endif;?>
        <?php foreach ($newClips as $name => $type) {?>
<!--            <li>-->
<!--                Nehmen Sie den Beutel mit den Halteklammern Nr. --><?//=$key;?><!-- . Stellen, die im-->
<!--                Diagramm (Abb. 1) unter Nr. --><?//=$key;?><!-- angegeben sind, wie in Abb. --><?//=isset($pictures[$value])?$pictures[$value]->num:'';?><!-- zur zusätzlichen-->
<!--                ﬁxierung des bildschirms-->
<!--            </li>-->
            <li>
                Nehmen Sie den Beutel mit den Halteklammern Nr. <?=$name;?> . Stellen, die im
                Diagramm (Abb. 1) unter Nr. <?=$name;?> angegeben sind, wie in Abb. <?=isset($picNumbers[$type])?implode(', ',$picNumbers[$type]):'';?> zur zusätzlichen
                ﬁxierung des bildschirms
            </li>
        <?php } ?>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Machen Sie das Fenster zu
            </li>
        <?endif;?>
        <?php if (!isset($velkro) && !empty($newClips) && !in_array($info->windowEn,['BW'])):?>
            <li>
                Setzen Sie den Schirmrahmen in die Halterklammern ein. Es ist besser, dies
                nacheinander zu tun: zuerst die Seitenkante des Bildschirms, dann die Oberseite.
                Die untere Kante des Schutzschirms sollte auf dem Gehäuse liegen, ohne in den
                Cliphaltern zu befestigen.
            </li>
        <?php endif;?>
        <?php if ($info->isOnlyMagnets):?>
            <li>
                Installieren Sie die Magnethalter gemäß den beiliegenden Anweisungen.
            </li>
        <?php endif;?>
    </ul>
<?endif;?>
