<h4>SUN SHADES CARE</h4>
<p>
    When the material of the screen is dirty,
    it is recommended to wipe it with a wet wipe or a damp sponge.
    In cases of severe dirt, it is possible to use a gentle detergent.
</p>