<h4>WARNINGS</h4>
<p>
<ul style="padding: 0px 20px">
    <li>
        It is not recommended to use the front side protective screens on-the-ride.
    </li>
    <li>
        It is not recommended to flick cigarette ash or throw litter out of the windows with the screens installed.
    </li>
    <li>
        In cold weather and when the vehicle interior is not warmed up, the material of the protective
        screens may sag slightly. This issue resolves itself once the interior is warmed up.
    </li>
    <li>
        It is not recommended to make a physical impact on textile cover of protective screens
        (jamming, transfixing etc.) as it may cause its damage.
    </li>
</ul>
</p>