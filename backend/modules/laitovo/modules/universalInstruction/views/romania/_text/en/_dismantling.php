<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

$clips = $info->clipsWithTypes;
if (isset($clips['V'])) { unset($clips['V']);}
?>

<h4>REMOVING OF THE SUN SHADE</h4>
<ul style="list-style-type: decimal;padding: 0px 20px">
    <?if ($info->isMagnet || $info->isOnlyMagnets || $info->isOnMagnets):?>
        <li>
            Grasp the loop at the top of the sun shade.
        </li>
        <li>
            Slightly pull the edge of the shade towards the center.
        </li>
    <? else:?>
        <?if (!empty($clips)) :?>
            <!--Если есть клипсы-->
            <li>
                Grasp the loop at the top of the sun shade.
            </li>
            <li>
                Slightly pull the edge of the shade towards the center so that it can be removed from the clip-holder.
            </li>
            <li>
                Remove the sun shade from the area of the clip holder.
            </li>
            <li>
                Remove the sun shade from the rest of the clip holders.
            </li>
        <?else :?>
            <!--Если нет клипс-->
            <li>
                Grasp the loop at the top of the sun shade.
            </li>
            <li>
                Slightly pull the edge of the shade towards the center.
            </li>
            <li>
                Remove the top of the shade.
            </li>
            <li>
                Remove the sun shade completely.
            </li>
        <?endif;?>
    <?endif;?>
</ul>

