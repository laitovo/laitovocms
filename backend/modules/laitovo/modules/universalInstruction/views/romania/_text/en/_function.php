<h4>FUNCTION AND SERVICING</h4>
<p>
    These protective sunshades are designed to protect the passengers inside from UV rays,
    whilst also acting as a bug shield and giving the interior fresh air and ventilation too.
    The sun shades also offer an alternative to tinted windows, giving passengers more privacy.
</p>