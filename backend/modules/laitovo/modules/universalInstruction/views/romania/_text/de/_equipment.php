<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

?>
<h4>PACKUNGSINHALT</h4>
<!--Комплектация-->
<div >
    <div style="width: 50%;float: left">
        <ul>
            <li>
                Schutzschirme ............ <?= $info->countWindows ?>
            </li>
            <li>
                Anleitung  ..................... 1
            </li>
            <?if ($info->needHook):?>
                <li>
                    Installationshaken ....... 1
                </li>
            <?endif;?>
        </ul>
    </div>
    <div style="width: 50%;float: left">
        <ul>
            <?if (!$info->isMagnet && !$info->isOnMagnets) :?>
                <? if ($info->isOnlyMagnets) :?>
                    <li>
                        Magnethalter (10) ... 1
                    </li>
                <?elseif ($info->isTape):?>
                    <li>
                        Clips auf Klebeband (8) ... 1
                    </li>
                <?else:?>
                    <? foreach ($info->clipsWithCount as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Magnethalter (10) ... 1
                            </li>
                        <? else : ?>
                            <li>
                                Halteklammer № <?= $key ?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endif; ?>
        </ul>
    </div>
    <div style="display: table; clear:both"></div>
</div>