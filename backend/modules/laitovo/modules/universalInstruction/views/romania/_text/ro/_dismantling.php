<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

$clips = $info->clipsWithTypes;
if (isset($clips['V'])) { unset($clips['V']);}
?>

<h4>DEMONTAREA PERDELUTELOR:</h4>
<ul style="list-style-type: decimal;padding: 0px 20px">
    <?if ($info->isMagnet || $info->isOnlyMagnets || $info->isOnMagnets):?>
        <li>
            Apucati partea de sus a perdelutei.
        </li>
        <li>
            Trageti usor marginea perdelutei spre centru.
        </li>
    <? else:?>
        <?if (!empty($clips)) :?>
            <!--Если есть клипсы-->
            <li>
                Apucati partea de sus a perdelutei.
            </li>
            <li>
                Trageti usor marginea perdelutei spre centru.
            </li>
            <li>
                Scoateti partea de sus a perdelutei.
            </li>
            <li>
                Scoateti complet perdeluta.
            </li>
        <?else :?>
            <!--Если нет клипс-->
            <li>
                Apucati partea de sus a perdelutei.
            </li>
            <li>
                Trageti usor marginea perdelutei spre centru.
            </li>
            <li>
                Scoateti partea de sus a perdelutei.
            </li>
            <li>
                Scoateti complet perdeluta.
            </li>
        <?endif;?>
    <?endif;?>
</ul>

