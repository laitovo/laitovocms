<?php
use backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO;

/* @var $dto InstructionDTO */
?>
<!--<p>-->
<!--    You may change the content of this page by modifying-->
<!--    the file <code>--><?//= __FILE__; ?><!--</code>.-->
<!--<img src="--><?//= $dto->schemeClipsSrc?><!--" alt="">-->
<!--<img src="--><?//= $dto->schemeMagnetsSrc?><!--" alt="">-->
<!--<img src="--><?//= $dto->schemeSrc?><!--" alt="">-->
<!--</p>-->

<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:02 AM
 */

$window = \backend\helpers\ArticleHelper::getWindowEn($dto->getArticleInfo()->getArticle());
switch ($window)  {
    case 'FD':
        $flag = true;
        break;
    case 'RD':
        $flag = true;
        break;
    case 'RV':
        $flag = false;
        break;
    case 'FV':
        $flag = false;
        break;
    case 'BW':
        $flag = false;
        break;
    case 'FW':
        $flag = false;
        break;
    default:
        $flag = false;
        break;
}
?>
<? if ($dto->getArticleInfo()->isMagnet || $dto->getArticleInfo()->isOnMagnets):?>
    <div style="width:100%;float: left">
        <div style="font-size: 12px;font-weight: bold;text-align: center">
            SCHEME OF INSTALLATION OF CLIPS-HOLDERS <br>
            BEFESTIGUNGPLAN FÜR HALTEKLAMMERN <br>
            SCHEMA DE INSTALARE A CLIPSURILOR
        </div>
        <div style="text-align: center">
            <img src="<?= $dto->schemeSrc?>" width="320px">
        </div>
        <div style="text-align: center">
            <p> Pic. 1 / Abb. 1 / Fig. 1</p>
        </div>
    </div>
<? else : ?>
    <div style="width:<?=$flag? 50 : 100;?>%;float: left">
        <div style="font-size: 12px;font-weight: bold;text-align: center">
            SCHEME OF INSTALLATION OF CLIPS-HOLDERS <br>
            BEFESTIGUNGPLAN FÜR HALTEKLAMMERN <br>
            SCHEMA DE INSTALARE A CLIPSURILOR
        </div>
        <div style="text-align: center">
            <img src="<?= $dto->schemeClipsSrc?>" width="320px">
        </div>
        <div style="text-align: center">
            <p> Pic. 1 / Abb. 1 / Fig. 1</p>
        </div>
    </div>
    <?if ($flag):?>
    <div style="width:50%;float: left">
        <div style="font-size: 12px;font-weight: bold;text-align: center">
            SCHEME OF INSTALLATION OF MAGNETIC-HOLDERS<br>
            BEFESTIGUNGPLAN FÜR MAGNETHALTERN<br>
            SCHEMA DE INSTALARE A CLIPSURILOR OR MAGNETICE
        </div>
        <div style="text-align: center">
            <img src="<?= $dto->schemeMagnetsSrc?>" width="320px">
        </div>
        <div style="text-align: center">
            <p> Pic. 2 / Abb. 2 / Fig. 2</p>
        </div>
    </div>
    <div style="display: block;clear: both;"></div>
    <?endif;?>
<?endif;?>
