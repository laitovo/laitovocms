<?php
/* @var $this yii\web\View */
/* @var $dto InstructionDTO */
/* @var $lang string|null*/

use backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO;

$info = $dto->getArticleInfo();

switch ($lang) {
    case  'cs':
        $render = "_text/_main_cs";
        break;
    case  'eu':
        $render = "_text/_main_eu";
        break;
    default:
        $render = "_text/$lang/_block";
}

echo $this->render($render ,['dto' => $dto]);
