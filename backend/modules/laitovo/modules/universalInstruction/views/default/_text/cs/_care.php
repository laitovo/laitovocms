<h4 style="margin:5px 0; margin-top: 10px;">PÉČE O SLUNEČNÍ STÍNY</h4>
<p style="margin:5px 0">
    Pokud je materiál slunečních clon znečištěný, doporučujeme jej otřít vlhkým hadříkem.
    V případě silného znečištění je možné použít šetrný mycí prostředek.
</p>