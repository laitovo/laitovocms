<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

?>

<h4 style="text-align: center;">OBSAH BALENÍ</h4>
<!--Комплектация-->
<div>
    <div style="width: 100%;float: left">
        <ul style="padding-left: 25px">
            <li>
                Sluneční clony ..... <?= $info->countWindows ?>
            </li>
            <li>
                Návod k použití ............... 1
            </li>
            <?if ($info->needHook):?>
                <li>
                    Háček pro odchlípnutí okenního čalounění / těsnění ...... 1
                </li>
            <?endif;?>
        </ul>
        <ul style="padding-left: 25px">
            <?if (!$info->isMagnet && !$info->isOnMagnets) :?>
                <? if ($info->isOnlyMagnets) :?>
                    <li>
                        Magnetické úchytky (10) ... 1
                    </li>
                <?elseif ($info->isTape):?>
                    <li>
                        Montážní prvky s lepicí páskou (8) ... 1
                    </li>
                <?else:?>
                    <?php $types = $info->clipsWithTypes; ?>
                    <? foreach ($info->clipsWithCount as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Magnetické úchytky (10) ... 1
                            </li>
                        <? else : ?>
                            <li>
                                Montáční háčky No. <?= $key . '/' . ($types[$key]??'')?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endif; ?>
        </ul>
    </div>
    <div style="display: table; clear:both"></div>
</div>