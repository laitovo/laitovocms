<h4 style="margin:5px 0">ZWECKBESTIMMUNG UND BETRIEB</h4>
<p  style="margin:5px 0">
    Der Sonnenschutz für die Autofenster dient dem Schutz des KFZ-Innenraums und den darin befindlichen Fahrgästen vor Sonne,
    Insekten und Glassplittern. Auch Ihre Privatsphäre im Auto wird geschützt.
</p>