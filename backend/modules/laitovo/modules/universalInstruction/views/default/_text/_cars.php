<?php

use Picqer\Barcode\BarcodeGeneratorHTML as BarcodeGenerator;

/**
 * @var $list array
 */

$generator = new BarcodeGenerator();

?>
<?php foreach ($list as $row):?>
    <div style="width: 50%;float: left;">
        <div style="width: 40%;float: left">
            <div style="padding-left:5%">
                <?= $generator->getBarcode($row->barcode, $generator::TYPE_CODE_128, 1)?>
                <div style="text-align: left;font-size: 12px"><?=$row->article?></div>
            </div>
        </div>
        <div style="width: 60%;float: left">
            <span style="font-size: 14px;font-weight: bold"><?=$row->carTitle?></span>
        </div>
        <div style="display: table; clear:both"></div>
    </div>
<?php endforeach;?>
<div style="display: table; clear:both"></div>


