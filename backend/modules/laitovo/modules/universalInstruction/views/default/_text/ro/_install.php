<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 * @var $pictures array
 * @var $picNumbers array
 */

$newClips = $info->clipsWithTypes;
?>

<h4>INSTRUCTIUNI DE MONTAJ:</h4>
<p>
    Inainte de instalare, asigurati-va ca ati cumparat setul corect pentru vehiculul dvs.
    specific. La inceput, asezati fiecare perdeluta pe fereastra pentru a verifica forma generala
    si asigurati-va ca toate clipsurile sunt incluse conform instructiunilor specifice incluse in setul dvs.
</p>
<? if ($info->isMagnet || $info->isOnMagnets):?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <li>
            Instalati perdeluta in orificiul indicat în instructiuni.2.
        </li>
        <li>
            În cazul în care deschiderea geamului usii nu are acoperire metalica,
            utilizati suporturile metalice (furnizate în set) pentru instalarea ecranului de protectie.
        </li>
        <?if ($info->needHook):?>
            <li>
                Daca exista inclus un carlig de montare cu kit-ul,
                utilizati-l pentru al indoi pe bandoul de pe marginea geamului.
            </li>
        <?endif;?>
        <li>
            Gasiti locația magnetilor cusuti pe marginea perdeluteti. Distribuiti uniform metal suplimentar
            de fixare pe marginea perdelutei, fara a scoate filmul de protectie de pe banda adezivă,
            fixandu-le cu magneti cusuti pe perdea.
        </li>
        <li>
            Pozitionati perdeluta la deschiderea ferestrei pentru a determina locatiile de montare
            a elementelor de fixare de pe capacul usii (pe partile metalice ale acoperirii deschiderii ferestrei)
        </li>
        <li>
            Instalati elementele de fixare unul cate unul, fixandu-le cu banda adeziva.
        </li>
        <li>
            Instalati pedeluta pe marginea ferestrei, asigurati-va ca magnetii sunt fixati in suporturile metalice.
        </li>
    </ul>
<? elseif ($info->isTape) :?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Deschideti usa si coborati geamul usii.
            </li>
        <?endif;?>
        <?php foreach ($newClips as $name => $type) {?>
            <!--            <li>-->
            <!--                Возьмите пакетик с зажимами держателями №--><?//=$key;?>
            <!--                и установите их в места, обозначенные на схеме (рис.1) под №--><?//=$key;?><!--,-->
            <!--                так как показано на рис.--><?//=isset($pictures[$value])?$pictures[$value]->num:'';?><!--.-->
            <!--            </li>-->
            <li>
                Luati un pachet cu suporturi de prindere si atasati-le pe ecran în
                locurile indicate în diagrama (Fig.1) Asa cum se arata în diagrama (Fig.<?=isset($picNumbers[$type])?implode(', ',$picNumbers[$type]):'';?>) ,
                preincalzitile cu aer cald de la masina.
            </li>
        <?php } ?>
        <li>
            Degresati zona unde se lipesc suportii.
        </li>
        <li>
            Scoateți folia de protectie din clemele de pe arcul superior al deschiderii usii,
            fara a scoate clemele de pe ecran. (alte cleme trebuie să ramana pe ecran cu un film de protectie).
            Montați ecranul în orificiul ferestrei astfel încât să nu existe vreun spatiu si apasati clemele fara film de protectie pe acoperire.
        </li>
        <li>
            Repetati în aceeasi ordine pentru clemele situate pe stalpul vertical al portierei.
        </li>
        <li>
            Repetati în aceeasi ordine pentru celelalte cleme (daca acestea sunt furnizate pe modelul masinii).
        </li>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Închideți fereastra.
            </li>
        <?endif;?>
        <li>
            Pentru ca banda să fie fixata mai bine, nu se recomandă scoaterea perdelutelor 24 de ore.
        </li>
    </ul>
<? else:?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <?php if ($info->bwSklad):?>
            <li>
                Daca ati achiziționat o perdeluta pliabila, mai intai trebuie sa o asamblati. Localizati locul curburii ramei perdelei unde este tubul de conectare,
                si introduceti marginea opusă a cadrului în ea, asa cum se arata în Fig.<?=isset($pictures['bwSklad'])?$pictures['bwSklad']->num:'';?>.
            </li>
        <?php endif;?>
        <?php if ($info->hasVelkro):?>
            <li>
                Luati protectia de fixare a benzii Velcro si lipiti-l la deschiderea ferestrei in locul indicat in diagrama (fig.1) cu litera "V" asa cum se arata in Fig.<?=isset($pictures['V'])?$pictures['V']->num:'';?>
            </li>
            <?php
            $velkro = true;
            unset($newClips['V']);
            ?>
        <?php endif;?>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Deschideti usa si lasati in jos geamul.
            </li>
        <?endif;?>
        <?if ($info->needHook):?>
            <li>
                Daca exista inclus un carlig de montare cu kit-ul,
                utilizati-l pentru al indoi pe bandoul de pe marginea geamului.
            </li>
        <?endif;?>
        <?if (empty($newClips) || isset($velkro) || in_array($info->windowEn,['BW']) ):?>
            <li>
                Instalati perdeluta in orificiul indicat în instructiuni.<?= (!isset($velkro)) && !empty($pictures) && empty($newClips) ? ' Pic.' . @$pictures[0]->num : '' ?>
            </li>
        <?endif;?>
        <?php foreach ($newClips as $name => $type) {?>
<!--            <li>-->
<!--                Take the pack with clips №--><?//=$key;?><!-- , and set them as indicated-->
<!--                in the diagram (pic.1) under №--><?//=$key;?><!-- as shown in the pic. --><?//=isset($pictures[$value])?$pictures[$value]->num:'';?><!--.-->
<!--            </li>-->
            <li>
                Luati pachetul cu clipsuri  nr <?=$name;?> și fixati-le asa cum este indicat în diagrama (pic.1) sub nr. <?=$name;?> asa cum se arata in fig. <?=isset($picNumbers[$type])?implode(', ',$picNumbers[$type]):'';?>.
            </li>
        <?php } ?>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Inchideti fereastra.
            </li>
        <?endif;?>
        <?php if (!isset($velkro) && !empty($newClips) && !in_array($info->windowEn,['BW'])):?>
            <li>
                Introduceti cadrul ramei solare in suporturile pentru cleme.
                Este mai bine să faceti acest pas cu pas: in primul rand, marginea laterala a perdelutei, apoi partea de sus.
                Marginea inferioara a perdelutei trebuie să se afle pe carcasa fara a fi fixata în suporturile pentru cleme.
            </li>
        <?php endif;?>
        <?php if ($info->isOnlyMagnets):?>
            <li>
                Instalati suporturile magnetice conform instructiunilor anexate.
            </li>
        <?php endif;?>
    </ul>
<?endif;?>
