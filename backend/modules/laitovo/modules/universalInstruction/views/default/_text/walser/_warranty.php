<h4 style="margin:5px 0; margin-top: 10px;">GARANTIEVERPFLICHTUNG.</h4>
<p style="margin:5px 0;">
    Bei sachgemäßer Verwendung des Sonnenschutzes, gewährt der Verkäufer eine Garantie von 12 Monaten,
    auf die Nähte, den Metallrahmen, die Befestigungen und das Gewebe, ab dem Zeitpunkt des Empfangs der Ware.
</p>

