<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

?>
<h4 style="text-align: center;">PACKUNGSINHALT</h4>
<!--Комплектация-->
<div >
    <div style="width: 100%;float: left">
        <ul style="padding-left: 15px">
            <li>
                Schutzschirme ......... <?= $info->countWindows ?>
            </li>
            <li>
                Anleitung  .................. 1
            </li>
            <?if ($info->needHook):?>
                <li>
                    Installationshaken .... 1
                </li>
            <?endif;?>
        </ul>
        <ul style="padding-left: 15px">
            <?if (!$info->isMagnet && !$info->isOnMagnets) :?>
                <? if ($info->isOnlyMagnets) :?>
                    <li>
                        Magnethalter (10) ... 1
                    </li>
                <?elseif ($info->isTape):?>
                    <li>
                        Clips auf Klebeband (8) ... 1
                    </li>
                <?else:?>
                    <?php $types = $info->clipsWithTypes; ?>
                    <? foreach ($info->clipsWithCount as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Magnethalter (10) ... 1
                            </li>
                        <? else : ?>
                            <li>
                                Halteklammer № <?= $key . '/' . ($types[$key]??'')?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endif; ?>
        </ul>
    </div>
    <div style="display: table; clear:both"></div>
</div>