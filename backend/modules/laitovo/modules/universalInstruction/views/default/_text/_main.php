<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:50 AM
 */
use backend\modules\laitovo\models\CarsPictures;

/**
 * @var $dto \backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO
 */


$newClips = \backend\helpers\ArticleHelper::getClipsWithTypes($article);
$window = \backend\helpers\ArticleHelper::getWindowEn($article);
$bwSklad = \backend\helpers\ArticleHelper::isBwSklad($article);

echo $this->render('en/_block',['info' => $dto->getArticleInfo()]);
echo '<br>';
echo $this->render('de/_block',['info' => $dto->getArticleInfo()]);
echo '<br>';
//echo $this->render('ro/_block',['info' => $dto->getArticleInfo()]);
//echo '<br>';
echo $this->render('ru/_block',['info' => $dto->getArticleInfo()]);