<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

$clips = $info->clipsWithTypes;
if (isset($clips['V'])) { unset($clips['V']);}
?>

<h4 style="margin:5px 0; margin-top: 10px;">DEMONTÁŽ</h4>
<ul style="list-style-type: decimal;padding: 0 20px;  margin:5px 0">
    <?if ($info->isMagnet || $info->isOnlyMagnets || $info->isOnMagnets):?>
        <li>
            Uchopte sluneční clonu za textilní proužek v horní části sluneční clony.
        </li>
        <li>
            Lehce zatáhněte směrem ke středu clony.
        </li>
    <? else:?>
        <?if (!empty($clips)) :?>
            <!--Если есть клипсы-->
            <li>
                Uchopte sluneční clonu za textilní proužek v horní části sluneční clony.
            </li>
            <li>
                Lehce zatáhněte směrem ke středu clony, aby bylo možné clonu vyjmout z háčku.
            </li>
            <li>
                Vyjměte clonu z háčku.
            </li>
            <li>
                Vyjměte sluneční clonu z ostatních háčků.
            </li>
        <?else :?>
            <!--Если нет клипс-->
            <li>
                Uchopte sluneční clonu za textilní proužek v horní části sluneční clony.
            </li>
            <li>
                Lehce zatáhněte směrem ke středu clony.
            </li>
            <li>
                Vyjměte vrchní část clony.
            </li>
            <li>
                Vyjměte clonu z okna.
            </li>
        <?endif;?>
    <?endif;?>
</ul>

