<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 * @var $pictures array
 * @var $picNumbers array
 */

$newClips = $info->clipsWithTypes;
?>
<h4 style="margin:5px 0">MONTAGE</h4>
<p style="margin:5px 0">
    Stellen Sie vor der Montage sicher, dass Sie das Set erworben haben,
    das der Marke und dem Modell Ihres Fahrzeugs entspricht.
    Um die Passform des Sonnenschutzes zu überprüfen, halten Sie ihn gegen die entsprechende Fahrzeugscheibe.
    Überprüfen Sie außerdem, ob Sie alle Bestandteile des Sets, wie in der Anleitung angegeben, erhalten haben.
</p>
<ul style="list-style-type: decimal;padding: 0px 20px">
  <li>
      Befestigen Sie den Sonnenschutz in der Fahrzeugscheibe, die auf der jeweiligen Montageanleitung angegeben ist. Öffnen Sie dafür als erstes die entsprechende Fahrzeugscheibe.
  </li>
  <li>
      Nehmen Sie den Beutel mit den Halteklammern Nr. 1. Sie sehen in Abbildung 1 an welchen Positionen Sie die Halteklammern am Fahrzeug befestigen müssen.
  </li>
  <li>
      Schließen Sie das Fenster Ihres Fahrzeugs.
  </li>
  <li>
      Anschließend befestigen Sie den Sonnenschutz in den Halteklammern. Dafür setzen Sie den Sonnenschutz zuerst in die Halteklammern ein welche seitlich am Fahrzeug befestigt sind. Erst danach setzten Sie den Sonnenschutz an der oberen Halteklammern ein und anschließend schieben Sie die untere Kante des Sonnenschutzes zur Fahrzeugscheibe.
  </li>
</ul>

