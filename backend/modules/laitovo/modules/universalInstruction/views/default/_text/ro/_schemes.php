<?php

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO;

/* @var $dto InstructionDTO */
/* @var $lang string */
?>
<!--<p>-->
<!--    You may change the content of this page by modifying-->
<!--    the file <code>--><?//= __FILE__; ?><!--</code>.-->
<!--<img src="--><?//= $dto->schemeClipsSrc?><!--" alt="">-->
<!--<img src="--><?//= $dto->schemeMagnetsSrc?><!--" alt="">-->
<!--<img src="--><?//= $dto->schemeSrc?><!--" alt="">-->
<!--</p>-->

<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:02 AM
 */

$info = $dto->getArticleInfo();
$pictureManager  = new \backend\modules\laitovo\models\CarsPictures($info->getArticle());
$pictures        = $pictureManager->getImages();
$picNumbers      = $pictureManager->getPicNumbers();

$window = ArticleHelper::getWindowEn($dto->getArticleInfo()->getArticle());
switch ($window)  {
    case 'FD':
        $flag = true;
        break;
    case 'RD':
        $flag = true;
        break;
    case 'RV':
        $flag = false;
        break;
    case 'FV':
        $flag = false;
        break;
    case 'BW':
        $flag = false;
        break;
    case 'FW':
        $flag = false;
        break;
    default:
        $flag = false;
        break;
}
?>
<?php if ($lang == 'ru'): ?>
<? if ($dto->getArticleInfo()->isMagnet):?>
    <div style="width:100%;float: left">
        <div style="font-size: 12px;font-weight: bold;text-align: center">
            СХЕМА УСТАНОВКИ ЗАЖИМОВ-ДЕРЖАТЕЛЕЙ
        </div>
        <div style="text-align: center">
            <img src="<?= Yii::$app->urlManager->createAbsoluteUrl($dto->schemeSrc)?>" width="320px">
        </div>
        <div style="text-align: center">
            <p> Рис. 1</p>
        </div>
    </div>
<? elseif ($dto->getArticleInfo()->isOnMagnets) : ?>

<? else : ?>
    <div style="width:100%">
        <div style="text-align: center;float: left; margin-right: 30px; width: 320px;">
            <div style="font-size: 12px;font-weight: bold;text-align: center">
                <h4>СХЕМА УСТАНОВКИ ЗАЖИМОВ-ДЕРЖАТЕЛЕЙ</h4>
            </div>
            <div>
                <img src="<?= Yii::$app->urlManager->createAbsoluteUrl($dto->schemeClipsSrc)?>" width="320px" >
            </div>
            <p> Рис. 1</p>
        </div>
        <div style="font-size: 14px;">
            <?= $this->render('_text/ru/_install',[
                'info'       => $info,
                'pictures'   => $pictures,
                'picNumbers' => $picNumbers,
            ])?>
        </div>
    </div>
<?endif;?>
<?php else: ?>
    <? if ($dto->getArticleInfo()->isMagnet):?>
        <div style="width:100%;float: left">
            <div style="font-size: 12px;font-weight: bold;text-align: center">
                SCHEME OF INSTALLATION OF CLIPS-HOLDERS <br>
                BEFESTIGUNGPLAN FÜR HALTEKLAMMERN
            </div>
            <div style="text-align: center">
                <img src="<?= Yii::$app->urlManager->createAbsoluteUrl($dto->schemeSrc)?>" width="320px">
            </div>
            <div style="text-align: center">
                <p> Pic. 1 / Abb. 1</p>
            </div>

        </div>
    <? elseif ($dto->getArticleInfo()->isOnMagnets) : ?>

    <? else : ?>
        <div style="width:100%">
            <div style="text-align: center;float: left; margin-right: 30px; width: 320px;">
                <div style="font-size: 12px;font-weight: bold;text-align: center">
                    SCHEME OF INSTALLATION OF CLIPS-HOLDERS
                </div>
                <div>
                    <img src="<?= Yii::$app->urlManager->createAbsoluteUrl($dto->schemeClipsSrc)?>" width="320px" >
                </div>
                <p> Pic. 1</p>
            </div>
            <div style="font-size: 14px;">
                <?= $this->render('_text/en/_install',[
                    'info'       => $info,
                    'pictures'   => $pictures,
                    'picNumbers' => $picNumbers,
                ])?>
            </div>
        </div>
    <?endif;?>
<?php endif; ?>
