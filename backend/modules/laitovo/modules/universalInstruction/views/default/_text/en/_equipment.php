<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

?>

<h4 style="text-align: center;">PACKAGE CONTENTS</h4>
<!--Комплектация-->
<div>
    <div style="width: 100%;float: left">
        <ul style="padding-left: 25px">
            <li>
                Protective screens ... <?= $info->countWindows ?>
            </li>
            <li>
                Instruction  ................ 1
            </li>
            <?if ($info->needHook):?>
                <li>
                    Installation hook ...... 1
                </li>
            <?endif;?>
        </ul>
        <ul style="padding-left: 25px">
            <?if (!$info->isMagnet && !$info->isOnMagnets) :?>
                <? if ($info->isOnlyMagnets) :?>
                    <li>
                        Magnetic holders (10) ... 1
                    </li>
                <?elseif ($info->isTape):?>
                    <li>
                        Tape clips (8) ... 1
                    </li>
                <?else:?>
                    <?php $types = $info->clipsWithTypes; ?>
                    <? foreach ($info->clipsWithCount as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Magnetic holders (10) ... 1
                            </li>
                        <? else : ?>
                            <li>
                                Clip-holder № <?= $key . '/' . ($types[$key]??'')?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endif; ?>
        </ul>
    </div>
    <div style="display: table; clear:both"></div>
</div>