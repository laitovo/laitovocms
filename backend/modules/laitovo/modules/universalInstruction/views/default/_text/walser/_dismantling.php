<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

$clips = $info->clipsWithTypes;
if (isset($clips['V'])) { unset($clips['V']);}
?>

<h4 style="margin:5px 0; margin-top: 10px;">ENTFERNEN DES SONNENSCHUTZES.</h4>
<ul style="list-style-type: decimal;padding: 0 20px;  margin:5px 0">
    <li>
        Verwenden Sie dafür die oben auf dem Sonnenschutz befestigte Schlaufe.
    </li>
    <li>
        Ziehen Sie den Schutzschirm mit Hilfe dieser Schlaufe leicht Richtung Mitte.
    </li>
    <li>
        Hängen Sie den Sonnenschutz zuerst aus den oberen Halteklammern aus.
    </li>
    <li>
        Danach hängen Sie ihn aus den restlichen Halteklammern aus.
    </li>
</ul>

