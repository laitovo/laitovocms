<?php

use \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO;
use \backend\helpers\ArticleHelper;

/**
 * @var $info ArticleDTO
 * @var $lang ArticleDTO
 */
?>

<?php if ($lang == 'ru') : ?>
<div style="font-size:9.9px;text-align: center">
    ИНСТРУКЦИЯ ПО УСТАНОВКЕ И ЭКСПЛУАТАЦИИ ЗАЩИТНЫХ ЭКРАНОВ ДЛЯ АВТОМОБИЛЬНЫХ ОКОН LAITOVO (CHIKO)
</div>
<div style="font-size:18px;text-align: center">
    <?= $info->ruTypeTitle?> / <?= $info->ruWindowTitle?>
</div>
<div style="font-size:12px;text-align: left; border:1px solid black; padding: 5px 20px">
    <?= ArticleHelper::getInvoiceRusTitle($info->getArticle()) ?>
</div>
<div style="font-size:11px;text-align: center;margin-top: 3px">
    Made by request of LAITOVO Manufactory T. & S. GMBH Ferdinandstrasse 25-27 D-20095 Hamburg Germany,<i>Tel.</i> +49(0)4095063310 <i>E-mail.</i> info@laitovo.de
</div>

<?php else : ?>

<div style="font-size:9.9px;text-align: center">
    INSTRUCTION FOR INSTALLATION AND SERVICING OF THE SET OF PROTECTIVE SCREENS FOR CAR WINDOWS LAITOVO (CHIKO) <b>·</b>
    ANLEITUNG FÜR MONTAGE UND BETRIEB VOM SONNENSHUTZ-SET FÜR AUTOFENSTER LAITOVO (CHIKO)<b>·</b>
</div>
<div style="font-size:18px;text-align: center">
    <?= $info->enTypeTitle?> / <?= $info->enWindowTitle?>
</div>
<div style="font-size:12px;text-align: left; border:1px solid black; padding: 5px 20px">
    <?= ArticleHelper::getInvoiceTitle($info->getArticle())?>
</div>
<div style="font-size:11px;text-align: center;margin-top: 3px">
    Made by request of LAITOVO Manufactory T. & S. GMBH Ferdinandstrasse 25-27 D-20095 Hamburg Germany,<i>Tel.</i> +49(0)4095063310 <i>E-mail.</i> info@laitovo.de
</div>
<?php endif;?>
