<h4 style="margin:5px 0; margin-top: 10px;">UPOZORNĚNÍ</h4>
<ul style="padding: 0 20px; margin:5px 0">
    <li>
        Nepoužívejte clony na předních oknech během jízdy.
    </li>
    <li>
        Neklepejte cigaretový popel z okna, pokud jsou clony nasazené.
    </li>
    <li>
        V chladném počasí se může materiál ochranných clon mírně prohýbat. Toto se samo vyřeší po zahřátí interiéru.
    </li>
    <li>
        Nedoporučujeme do slunečních clon fyzicky zasahovat jinak, než je uvedeno v manuálu. Mohlo by dojít k jejich poškození.
    </li>
</ul>
