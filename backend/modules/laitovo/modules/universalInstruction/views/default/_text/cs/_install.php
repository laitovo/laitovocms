<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 * @var $pictures array
 * @var $picNumbers array
 */

$newClips = $info->clipsWithTypes;
?>

<h4 style="margin:5px 0">INSTALACE</h4>
<p style="margin:5px 0">
    Před instalací se ujistěte, že zakoupená sada odpovídá Vašemu vozu.
    Nejprve umístěte každou sluneční clonu proti oknu, abyste zkontrolovali celkový tvar.
    Ujistěte se, že jsou v balení zahrnuty všechny háčky a jejich popisky odpovídají náčrtu.
</p>
<? if ($info->isMagnet):?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <li>
            Nainstalujte clonu do okna dle návodu.
        </li>
        <li>
            Pokud není okenní rám železný, použijte běžné háčky, které jsou dodané v balení.
        </li>
        <?if ($info->needHook):?>
            <li>
                Pokud je součástí sady montážní hák, použijte jej k odchlípnutí okenního čalounění/těsnění.
            </li>
        <?endif;?>
        <li>
            Umistětě úchytky na clonu do míst, kde jsou všité magnety.
        </li>
        <li>
            Přiložte clonu na okno, aby bylo patrné, kam je třeba úchytky umístit.
        </li>
        <li>
            Nalepte úchytky jednu po druhé na pozice, které jste vyměřili v předchozím kroku.
        </li>
        <li>
            Vložte sluneční clonu do úchytek.
        </li>
    </ul>
<? elseif ($info->isOnMagnets) :?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <li>
            Vyjměte háčky ze sáčku.
        </li>
        <li>
            Zahřejte je na pokojovou teplotu (nedoporučujeme lepit v nízkých teplotách).
        </li>
        <li>
            Připevněte je na plochou na magnety, které jsou na sluneční cloně.
        </li>
        <li>
            Přiložte clonu na okno, aby bylo patrné, kam je třeba úchytky umístit.
        </li>
        <li>
            Odmastěte rám okna v místech, které jste vyměřili v předchozím kroku.
        </li>
        <li>
            Sejměte ochrannou vrstu na úchytkách (úchytky ponechte na sluneční cloně).
        </li>
        <li>
            Vložte sluneční clonu včetně úchytek rámu okna (obr. 1).
        </li>
        <li>
            Pečlivě přimáčkněte clonu do rámu okna, aby se přilepily jednotlivé úchytky.
        </li>
        <li>
            Ujistěte se, že clona pevně drží.
        </li>
    </ul>
<? elseif ($info->isTape) :?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Otevřete dveře i okno.
            </li>
        <?endif;?>
        <?php foreach ($newClips as $name => $type) {?>
            <!--            <li>-->
            <!--                Возьмите пакетик с зажимами держателями №--><?//=$key;?>
            <!--                и установите их в места, обозначенные на схеме (рис.1) под №--><?//=$key;?><!--,-->
            <!--                так как показано на рис.--><?//=isset($pictures[$value])?$pictures[$value]->num:'';?><!--.-->
            <!--            </li>-->
            <li>
                Vyjměte háčky ze sáčku a umístěte je na sluneční clonu do pozic uvedených v nákresu (obr. 1)
            </li>
        <?php } ?>
        <li>
            Odmastěte rám okna v místech, které jste vyměřili v předchozím kroku.
        </li>
        <li>
            Sejměte ochrannou vrstu na úchytkách na vrchní straně clony (úchytky ponechte na sluneční cloně).
            Nainstalujte clonu do rámu okna tak, aby jím bylo vyplněné bez mezer.
            Mírně zatlačte na vrchní úchytky bez ochranné vrstvy.
        </li>
        <li>
            Opakujte postup pro ostatní úchytky.
        </li>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Zavřete okno.
            </li>
        <?endif;?>
        <li>
            Po instalaci doporučujeme clony alespoň 24 hodin nesundávat.
        </li>
    </ul>
<? else:?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <?php if ($info->bwSklad):?>
            <li>
                Jestliže je součástí balení skládací clona, je třeba ji před nasazením na okno rozložit.
                Přeloženou clonu narovnejte a zasuňte do sebe protilehlé konce. (obr. <?=isset($pictures['bwSklad'])?$pictures['bwSklad']->num:'';?>).
            </li>
        <?php endif;?>
        <?php if ($info->hasVelkro):?>
            <li>
                V případě, že je clona opatřena suchým zipem, nalepte pásku se suchým zipem na horní část okna
                (obr. <?=isset($pictures['V'])?$pictures['V']->num:'';?>).
            </li>
            <?php
            $velkro = true;
            unset($newClips['V']);
            ?>
        <?php endif;?>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Otevřete dveře i okno.
            </li>
        <?endif;?>
        <?if ($info->needHook):?>
            <li>
                Pokud je součástí sady montážní hák, použijte jej k odchlípnutí okenního čalounění/těsnění.
            </li>
        <?endif;?>
        <?if (empty($newClips) || isset($velkro) || in_array($info->windowEn,['BW']) ):?>
            <li>
                Nainstalujte clonu do okna dle návodu (<?= (!isset($velkro)) && !empty($pictures) && empty($newClips) ? ' obr.' . @$pictures[0]->num : '' ?>)
            </li>
        <?endif;?>
        <?php foreach ($newClips as $name => $type) {?>
<!--            <li>-->
<!--                Take the pack with clips №--><?//=$key;?><!-- , and set them as indicated-->
<!--                in the diagram (pic.1) under №--><?//=$key;?><!-- as shown in the pic. --><?//=isset($pictures[$value])?$pictures[$value]->num:'';?><!--.-->
<!--            </li>-->
            <li>
                Nainstalujte háčky  №<?=$name;?> , do rámu okna, jak je znázorněno na obr. <?=isset($picNumbers[$type])?implode(', ',$picNumbers[$type]):'';?>.
            </li>
        <?php } ?>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Zavřete okno.
            </li>
        <?endif;?>
        <?php if (!isset($velkro) && !empty($newClips) && !in_array($info->windowEn,['BW'])):?>
            <li>
                Vložte sluneční clonu nejprve do bočních háčků a následně do háčků na horní straně okna.
                Spodní strana sluneční clony se pomocí háčků neuchycuje.
            </li>
        <?php endif;?>
        <?php if ($info->isOnlyMagnets):?>
            <li>
                Instalujte magnetické úchytky podle schématu.
            </li>
        <?php endif;?>
    </ul>
<?endif;?>
