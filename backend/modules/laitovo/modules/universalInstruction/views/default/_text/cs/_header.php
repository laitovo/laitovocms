<?php

use \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO;
use \backend\helpers\ArticleHelper;

/**
 * @var $info ArticleDTO
 * @var $lang ArticleDTO
 */
?>

<div style="width: 5%; float: left">
    <div style="display: inline-block;color: white;background-color: black;font-weight: bold;padding: 4px">CS</div>
</div>
<div style="width: 95%; float: left">
    <div style="font-size:9.9px;text-align: center">
        NÁVOD NA INSTLACI A ÚDRŽBU SLUNEČNÍCH CLON LAITOVO (CHIKO)
    </div>
    <div style="font-size:18px;text-align: center">
        <?= $info->enTypeTitle?> / <?= $info->enWindowTitle?>
    </div>
</div>
<div style="clear: both; content: '';"></div>
<div style="font-size:12px;text-align: left; border:1px solid black; padding: 5px 20px">
    <?= ArticleHelper::getInvoiceTitle($info->getArticle())?>
</div>
<div style="font-size:11px;text-align: center;margin-top: 3px">
    Made by request of LAITOVO Manufactory T. & S. GMBH Ferdinandstrasse 25-27 D-20095 Hamburg Germany,<i>Tel.</i> +49(0)4095063310 <i>E-mail.</i> info@laitovo.de
</div>
