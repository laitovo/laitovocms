<?php

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\models\CarsPictures;
use backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO;

/* @var $dto InstructionDTO */
/* @var $lang string */
?>

<?php

$info = $dto->getArticleInfo();
$pictureManager  = new CarsPictures($info->getArticle());
$pictures        = $pictureManager->getImages();
$picNumbers      = $pictureManager->getPicNumbers();

$window = ArticleHelper::getWindowEn($info->getArticle());
?>

<?php if ($info->isMagnet || $info->isOnMagnets):?>
    <div style="width:100%">
        <div style="font-size: 14px;">
            <?= $this->render('_install',[
                'info'       => $info,
                'pictures'   => $pictures,
                'picNumbers' => $picNumbers,
            ])?>
        </div>
    </div>
<?php else : ?>
    <div style="width:100%">
        <div style="text-align: center;float: left; margin-right: 30px; width: 320px;">
            <div style="font-size: 12px;font-weight: bold;text-align: center">
                <h4 style="margin:5px 0;">СХЕМА УСТАНОВКИ ЗАЖИМОВ-ДЕРЖАТЕЛЕЙ</h4>
            </div>
            <div>
                <img src="<?= Yii::$app->urlManager->createAbsoluteUrl($dto->schemeClipsSrc)?>" width="320px" >
            </div>
            <p>Рис. 1</p>
        </div>
        <div style="font-size: 14px;">
            <?= $this->render('_install',[
                'info'       => $info,
                'pictures'   => $pictures,
                'picNumbers' => $picNumbers,
            ])?>
        </div>
    </div>
<?php endif;?>

