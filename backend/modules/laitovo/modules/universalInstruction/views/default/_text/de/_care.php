<h4 style="margin:5px 0; margin-top: 10px;">PFLEGE VON SCHUTZSCHIRMEN</h4>
<p  style="margin:5px 0">
    Wenn die Gewebeoberﬂäche verschmutzt ist, empﬁehlt es sich, sie mit einem feuchten Tuch
    oder Schwamm zu reinigen. Bei starker Verschmutzung können schonende Reinigungsmittel
    oder Mittel zur Pﬂege von Gewebeoberﬂächen verwendet werden
</p>