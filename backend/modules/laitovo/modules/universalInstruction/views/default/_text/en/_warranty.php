<h4 style="margin:5px 0; margin-top: 10px;">WARRANTY</h4>
<p style="margin:5px 0;">
    Upon proper observance by the buyer of the rules of exploitation, the seller establishes a warranty period on the shades of 12 months from the date of receipt by the buyer of the goods.
    The warranty period extends to the integrity of the seams, the integrity of the metal frame and clip-holders, the integrity of the fabric and its cover.
</p>

