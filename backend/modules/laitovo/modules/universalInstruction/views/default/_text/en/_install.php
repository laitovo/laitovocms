<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 * @var $pictures array
 * @var $picNumbers array
 */

$newClips = $info->clipsWithTypes;
?>

<h4 style="margin:5px 0">INSTALLATION MANUAL</h4>
<p style="margin:5px 0">
    Before installation, ensure that the correct set has been purchased for your specific vehicle.
    At first, place each shade against the window to check the overall shape and make sure all clips
    are included as stated in the specific instructions included with your set.
</p>
<? if ($info->isMagnet):?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <li>
            Install the screen into the window aperture indicated in the instructions in thrust.2.
        </li>
        <li>
            If the window aperture of the door of your car has non-metallic covering,
            use metal holders (supplied in the set) for protective screen installing.
        </li>
        <?if ($info->needHook):?>
            <li>
                If there is included an installation hook with your kit,
                use it to bend the casing of the window trim.
            </li>
        <?endif;?>
        <li>
            Find the location of the sewn magnets on the protective screen. Evenly distribute additional metal
            fasteners on the screen, without removing the protective film from the adhesive tape,
            fixing them with magnets sewn into the screen.
        </li>
        <li>
            Attach a protective screen to the window aperture to determine the mounting locations
            of the fasteners on the door covering (on the metal parts of the window aperture covering).
        </li>
        <li>
            Install the fasteners one by one, fixing them on the covering with adhesive tape.
        </li>
        <li>
            Install the screen in the window aperture, make sure that the magnets securely fix it on the metal holders.
        </li>
    </ul>
<? elseif ($info->isOnMagnets) :?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <li>
            Find the metal clip holders in the kit.
        </li>
        <li>
            Warm them up with warm air in the car.
        </li>
        <li>
            Fasten them with a flat plate to the magnets that are sticked to the frame screen (see Pic. 2)
        </li>
        <li>
            Attach the screen to the window as shown in Pic. 1 and determine the installation locations of the clip holders on the sheathing of the car.
        </li>
        <li>
            Degrease with an alcohol napkin the place where the tape will be fixed
        </li>
        <li>
            Peel off the protective film from the metal holders one by one
        </li>
        <li>
            Without removing the holders, install the screen in the window as shown in Pic. 1
        </li>
        <li>
            Press the holders so that the tape stickedto the sheathing
        </li>
        <li>
            Make sure the screen is securely installed
        </li>
    </ul>
<? elseif ($info->isTape) :?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Open the door and lower the door glass.
            </li>
        <?endif;?>
        <?php foreach ($newClips as $name => $type) {?>
            <!--            <li>-->
            <!--                Возьмите пакетик с зажимами держателями №--><?//=$key;?>
            <!--                и установите их в места, обозначенные на схеме (рис.1) под №--><?//=$key;?><!--,-->
            <!--                так как показано на рис.--><?//=isset($pictures[$value])?$pictures[$value]->num:'';?><!--.-->
            <!--            </li>-->
            <li>
                Take a pack with clip-holders and attach them to the screen in the places indicated
                in the diagram (Fig. No.1 ) as shown in the diagram (Fig. No.<?=isset($picNumbers[$type])?implode(', ',$picNumbers[$type]):'';?>) ,
                preheating them with warm air from the air duct in the car.
            </li>
        <?php } ?>
        <li>
            Degrease the covering at the mounting points.
        </li>
        <li>
            Remove the protective film from the clips located on the upper arc of the door aperture,
            without removing the clips from the screen. (other clips must remain on the screen with a protective film).
            Install the screen in the window aperture so that there are no gap exist and press the clips without protective film to the covering.
        </li>
        <li>
            Repeat in the same sequence for the clips located on the vertical pillar of the doorway.
        </li>
        <li>
            Repeat in the same sequence for the remaining clips (if they are supplied on the model of your car)
        </li>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Close the window.
            </li>
        <?endif;?>
        <li>
            In order for the tape to be fixed better, it is not recommended to remove the screens within 24 hours.
        </li>
    </ul>
<? else:?>
    <ul style="list-style-type: decimal;padding: 0px 20px">
        <?php if ($info->bwSklad):?>
            <li>
                If you`ve purchased a folding screen, firstly you need to assemble it before starting
                the installation. Feel in the place of the bend of the screen frame the connecting tube,
                and insert the opposite edge of the frame into it, as shown in
                Fig.<?=isset($pictures['bwSklad'])?$pictures['bwSklad']->num:'';?>.
            </li>
        <?php endif;?>
        <?php if ($info->hasVelkro):?>
            <li>
                Take the screen at the place of fastening the Velcro tape, and glue it to the window
                aperture in the place indicated in the diagram (fig.1) with the letter “V”
                as shown in Fig.<?=isset($pictures['V'])?$pictures['V']->num:'';?>
            </li>
            <?php
            $velkro = true;
            unset($newClips['V']);
            ?>
        <?php endif;?>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Open the door and open the window.
            </li>
        <?endif;?>
        <?if ($info->needHook):?>
            <li>
                If there is included an installation hook with your kit,
                use it to bend the casing of the window trim.
            </li>
        <?endif;?>
        <?if (empty($newClips) || isset($velkro) || in_array($info->windowEn,['BW']) ):?>
            <li>
                Install the screen into the window aperture indicated in the instructions in thrust.<?= (!isset($velkro)) && !empty($pictures) && empty($newClips) ? ' Pic.' . @$pictures[0]->num : '' ?>
            </li>
        <?endif;?>
        <?php foreach ($newClips as $name => $type) {?>
<!--            <li>-->
<!--                Take the pack with clips №--><?//=$key;?><!-- , and set them as indicated-->
<!--                in the diagram (pic.1) under №--><?//=$key;?><!-- as shown in the pic. --><?//=isset($pictures[$value])?$pictures[$value]->num:'';?><!--.-->
<!--            </li>-->
            <li>
                Take the pack with clips №<?=$name;?> , and set them as indicated
                in the diagram (pic.1) under №<?=$name;?> as shown in the pic. <?=isset($picNumbers[$type])?implode(', ',$picNumbers[$type]):'';?>.
            </li>
        <?php } ?>
        <?php if (in_array($info->windowEn,['FD','RD']) && !empty($newClips)):?>
            <li>
                Close the window.
            </li>
        <?endif;?>
        <?php if (!isset($velkro) && !empty($newClips) && !in_array($info->windowEn,['BW'])):?>
            <li>
                Insert the frame of the sun shade into the clip-holders.
                It is better to do this step by step: first, the side edge of the sun shade, then the top.
                The lower edge of the sun shade must lie on the casing without fixing in the clips-holders.
            </li>
        <?php endif;?>
        <?php if ($info->isOnlyMagnets):?>
            <li>
                Install the magnetic holders according to the enclosed instructions.
            </li>
        <?php endif;?>
    </ul>
<?endif;?>
