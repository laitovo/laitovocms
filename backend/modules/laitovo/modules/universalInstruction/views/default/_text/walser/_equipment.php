<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

?>
<h4 style="text-align: center;">PACKUNGSINHALT</h4>
<!--Комплектация-->
<div >
    <div style="width: 100%;float: left; font-size: 12px;">
        <ul style="padding-left: 30%">
            <li>
                Sonnenschutz ......... <?= $info->countWindows ?>
            </li>
            <li>
                Anleitung  .................. 1
            </li>
            <?if ($info->needHook):?>
                <li>
                    Installationshaken .... 1
                </li>
            <?endif;?>
        </ul>
        <ul style="padding-left: 30%">
            <?if (!$info->isMagnet && !$info->isOnMagnets) :?>
                <? if ($info->isOnlyMagnets) :?>
                    <li>
                        Montageanleitung (10) ... 1
                    </li>
                <?elseif ($info->isTape):?>
                    <li>
                        Clips auf Klebeband (8) ... 1
                    </li>
                <?else:?>
                    <? foreach ($info->clipsWithCount as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Montageanleitung (10) ... 1
                            </li>
                        <? else : ?>
                            <li>
                                Halteklammer № <?= $key ?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endif; ?>
        </ul>
    </div>
    <div style="display: table; clear:both"></div>
</div>