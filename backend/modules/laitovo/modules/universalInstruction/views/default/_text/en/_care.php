<h4 style="margin:5px 0; margin-top: 10px;">SUN SHADES CARE</h4>
<p style="margin:5px 0">
    When the material of the screen is dirty,
    it is recommended to wipe it with a wet wipe or a damp sponge.
    In cases of severe dirt, it is possible to use a gentle detergent.
</p>