<h4 style="margin:5px 0; margin-top: 10px;">BETRIEBSBESONDERHEITEN</h4>
<ul style="padding: 0 20px; margin:5px 0">
    <li>
        Beim Autofahren ist der Betrieb der Seitenschutzschirme vorn nicht empfehlenswert.
    </li>
    <li>
        Es ist nicht empfehlenswert, Zigarettenasche abzuschütteln, Müll zum Autofenster herauszuwerfen,
        an dem Schutzschirme befestigt sind, sofern sie keine Spezialausschnitte haben. Denn dadurch
        kann das Gewebe beschädigt werden.
    </li>
    <li>
        Winters (bei kaltem Wetter) kann es passieren, dass im unbeheizten Innenraum das
        Schutzschirmmaterial durchhängt (Materialbesonderheit); dieser Defekt verschwindet
        von selbst, sobald das Auto beheizt wird.
    </li>
    <li>
        Es ist nicht empfehlenswert, das Schutzschirmgewebe einer physischen Einwirkung
        (der Einklemmung, dem Durchstechen) zu unterziehen. Denn dadurch kann es beschädigt werden.
    </li>
</ul>
