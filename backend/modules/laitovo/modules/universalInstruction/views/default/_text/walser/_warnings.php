<h4 style="margin:5px 0; margin-top: 10px;">BEI VERWENDUNG ZU BEACHTEN</h4>
<ul style="padding: 0 20px; margin:5px 0">
    <li>
        Bitte achten Sie darauf, den Sonnenschutz nicht mit spitzen Gegenständen durchzustechen oder diesen einzuklemmen,
        da das Gewebe dadurch beschädigt werden kann.
    </li>
    <li>
        Im Winter bei kaltem Wetter kann es passieren, dass das Material des Sonnenschutzes durchhängt, wenn der KFZ-Innenraum nicht beheizt ist.
        Sobald der KFZ-Innenraum warm ist, befindet sich das Material wieder im Originalzustand
    </li>
</ul>
