<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

$pictureManager  = new \backend\modules\laitovo\models\CarsPictures($info->getArticle());
$pictures        = $pictureManager->getImages();
$picNumbers      = $pictureManager->getPicNumbers();

?>
<hr>
<div style="padding: 5px 20px">
    <div style="text-align: center">
        <div style="display: inline-block;color: white;background-color: black;font-weight: bold;padding: 1px">RO</div>
    </div>
    <div>
        <div style="width: 100%">
            <section style="font-size: 12px">
                <div style="width: 50%;float: left">
                    <?= $this->render('_equipment',['info' => $info])?>
                </div>
                <div style="width: 50%;float: left">
                    <?= $this->render('_function')?>
                </div>
                <div style="display: table; clear:both"></div>
            </section>
            <section style="font-size: 12px">

                <?= $this->render('_warnings')?>
                <?= $this->render('_care')?>
                <?= $this->render('_install',[
                    'info'       => $info,
                    'pictures'   => $pictures,
                    'picNumbers' => $picNumbers,
                ])?>

                <div>
                    <div style="float:left;width: 70%">
                        <?= $this->render('_dismantling',['info' => $info])?>
                        <?= $this->render('_warranty')?>
                    </div>
                    <div style="float:left;width: 30%;text-align: center">
                        <?= $this->render('_contacts')?>
                        <?= $this->render('_qr_code')?>
                    </div>
                    <div style="display: table; clear:both"></div>
                </div>
            </section>
        </div>
        <div style="width: 50%"></div>
    </div>
</div>