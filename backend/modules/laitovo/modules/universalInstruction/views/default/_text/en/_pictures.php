<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:43 AM
 */
use backend\modules\laitovo\models\CarsPictures;
use backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO;

/**
 * @var $info ArticleDTO
 */

$pictureManager = new CarsPictures($info->getArticle());
$pictures = $pictureManager->getImages();

if ($info->windowEn == 'BW') {
    /** Translates of sides */
    $sideTranslates = [
        'top' => 'top',
        'left' => 'left',
        'right' => 'right',
        'bottom' => 'bottom',
    ];
} else {
    $sideTranslates = [
        'top' => 'top',
        'left' => 'back',
        'right' => 'front',
        'bottom' => 'bottom',
    ];
}
?>

<div style="margin: 20px 0">
    <?php $flag = false; ?>
    <?php foreach ($pictures as $key => $picture) {?>
        <tr>
            <?php if (!$flag):?>
                <td colspan="1" rowspan="<?= count($pictures)?>" style="padding: 10px; vertical-align: center">
                    <section style="font-size: 12px">
                        <div style="width: 100%">
                            <?= $this->render('_equipment',['info' => $info])?>
                        </div>
                        <div style="width: 100%;">
                            <?= $this->render('_qr_code')?>
                        </div>
                    </section>
                </td>
                <?php $flag = true;?>
            <?php endif;?>
            <td colspan="2" style="text-align: left">
                <div style="text-align: center; padding-bottom: 10px">
                    <img style="height: 140px;" src="<?= Yii::$app->urlManager->createAbsoluteUrl($picture->src)?>" /> <br>
                </div>
            </td>
            <td colspan="1" style="text-align: left; padding: 10px"">
            <? if ($picture->name): ?>
            <div style="text-align: center;">
                <span style="display: inline-block; border: 2px solid black; padding: 10px; font-size: 16px;">
                    <?= $picture->side ? $sideTranslates[$picture->side] : '' ?><br>
                    <?= "№ $picture->name" ?>
                </span>
            </div>
            <?php endif;?>
            <div style="text-align: center;">
            <span style="display: inline-block; padding: 10px; font-size: 16px;">
                Pic.<?=$picture->num?>
            </span>
            </div>
            </td>
        </tr>
    <?php } ?>
</div>





