<?php
/* @var $this yii\web\View */
/* @var $dto InstructionDTO */
/* @var $lang string|null*/
/* @var $info ArticleDTO */

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO;
use backend\modules\laitovo\models\CarsPictures;
use backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO;

$info = $dto->getArticleInfo();
$pictureManager = new CarsPictures($info->getArticle());
$pictures = $pictureManager->getImages();
$picNumbers = $pictureManager->getPicNumbers();

?>
<!--Вся инструкция будет ввиде таблицы-->
<div style="background-image: url('/img/instructions/walser.jpg');background-repeat: no-repeat;background-size:820px; padding-top: 130px; background-position-y: -20px; background-position-x: -20px;">
    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <!--Блок 1-ый. Вводная верхня надпись для инструкции-->
        <tr>
            <td colspan="4" style="background-color:; text-align: left">
                <div style="width: 50%; float: left">&nbsp;
                </div>
                <div style="width: 50%; float: left">
                    <div style="font-size:9.9px;text-align: center">
                        <div style="width: 20%; float: left">&nbsp;
                        </div>
                        <div style="width: 80%; float: left">
                            ANLEITUNG FÜR MONTAGE UND BETRIEB VOM SONNENSHUTZ-SET FÜR AUTOFENSTER LAITOVO (CHIKO)
                        </div>
                        <div style="clear: both; content: '';"></div>
                    </div>
                    <div style="font-size:18px;text-align: center">
                        <?= $info->enTypeTitle?> / <?= $info->enWindowTitle?>
                    </div>
                    <div style="font-size:12px;text-align: left; border:1px solid black; padding: 5px 20px">
                        <?= ArticleHelper::getInvoiceTitle($info->getArticle())?>
                    </div>
                </div>
                <div style="clear: both; content: '';"></div>
            </td>
        </tr>
        <!--Блок 2-ой. Артикулы и автомобили-->
        <tr>
            <td colspan="4" style="text-align: left">
                <hr size="4" color="black">
                <?= $this->render('../_cars', ['list' => $dto->articleList]); ?>
                <hr size="4" color="black">
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: left">
                <?= $this->render("_schemes", ['dto' => $dto]); ?>
            </td>
        </tr>
        <!--Блок 4-ый. Блок изображений установки-->
        <!--Блок 5-ый. Блок текст установки-->
        <tr>
            <td colspan="4" style="text-align: left">
                <hr>
                <div style="padding: 5px 20px">
                    <div>
                        <div style="width: 100%">
                            <section style="font-size: 14px">

                                <?= $this->render('_function') ?>
                                <?= $this->render('_warnings') ?>
                                <?= $this->render('_care') ?>

                                <div>
                                    <div style="float:left;width: 70%">
                                        <?= $this->render('_dismantling', ['info' => $info]) ?>
                                        <?= $this->render('_warranty') ?>
                                    </div>
                                    <div style="float:left;width: 30%;text-align: center">
                                        <?= $this->render('_contacts') ?>
                                    </div>
                                    <div style="display: table; clear:both"></div>
                                </div>
                            </section>
                        </div>
                        <div style="width: 50%"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px; padding-left: 40px; vertical-align: center">
                <?= $this->render('_equipment',['info' => $info])?>
            </td>
            <td style="padding: 10px; vertical-align: center">
                <?= $this->render('_qr_code')?>
            </td>
        </tr>
    </table>
</div>