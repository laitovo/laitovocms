<?php

/**
 * @var $info ArticleDTO
 */

use backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO;

?>
<h4 style="text-align: center;">КОМПЛЕКТАЦИЯ</h4>
<!--Комплектация-->
<div>
    <div style="width: 100%;float: left">
        <ul style="padding-left: 25px">
            <li>
                Защитный экран ............ <?= $info->countWindows ?>
            </li>
            <li>
                Инструкция  .................... 1
            </li>
            <?if ($info->needHook):?>
                <li>
                    Установочный крючок ... 1
                </li>
            <?endif;?>
        </ul>
        <ul  style="padding-left: 25px">
            <?if (!$info->isMagnet && !$info->isOnMagnets) :?>
                <? if ($info->isOnlyMagnets) :?>
                    <li>
                        Комплект магнитных держателей (10) ... 1
                    </li>
                <?elseif ($info->isTape):?>
                    <li>
                        Комплект зажимов на скотче (8) ... 1
                    </li>
                <?else :?>
                    <?php $types = $info->clipsWithTypes; ?>
                    <? foreach ($info->clipsWithCount as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Комплект магнитных держателей (10) ... 1
                            </li>
                        <? else : ?>
                            <li>
                                Зажим № <?= $key . '/' . ($types[$key]??'')?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endif; ?>
        </ul>
    </div>
    <div style="display: table; clear:both"></div>
</div>