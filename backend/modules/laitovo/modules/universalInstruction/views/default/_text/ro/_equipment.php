<?php

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

?>

<h4>CONTINUTUL PACHETULUI:</h4>
<!--Комплектация-->
<div >
    <div style="width: 50%;float: left">
        <ul>
            <li>
                Perdelute de geam ...... <?= $info->countWindows ?>
            </li>
            <li>
                Instructiuni  ................... 1
            </li>
            <?if ($info->needHook):?>
                <li>
                    Carlige de instalare ......... 1
                </li>
            <?endif;?>
        </ul>
    </div>
    <div style="width: 50%;float: left">
        <ul>
            <?if (!$info->isMagnet && !$info->isOnMagnets) :?>
                <? if ($info->isOnlyMagnets) :?>
                    <li>
                        Cleme magnetice (10) ... 1
                    </li>
                <?elseif ($info->isTape):?>
                    <li>
                        Cleme cu adeziv (8) ... 1
                    </li>
                <?else:?>
                    <?php $types = $info->clipsWithTypes; ?>
                    <? foreach ($info->clipsWithCount as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Cleme magnetice (10) ... 1
                            </li>
                        <? else : ?>
                            <li>
                                Clipusuri Nr. <?= $key . '/' . ($types[$key]??'')?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endif; ?>
        </ul>
    </div>
    <div style="display: table; clear:both"></div>
</div>