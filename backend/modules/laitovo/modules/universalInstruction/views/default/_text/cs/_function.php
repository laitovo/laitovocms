<h4 style="margin:5px 0">FUNKCE A SERVIS</h4>
<p  style="margin:5px 0">
    Ochranné sluneční clony jsou navrženy tak, aby chránily cestující před slunečním zářením.
    Zároveň fungují jako ochrana proti hmyzu. Sluneční clony také nabízejí alternativu k
    tónovaným oknům nebo fóliím a poskytují cestujícím více soukromí.
</p>