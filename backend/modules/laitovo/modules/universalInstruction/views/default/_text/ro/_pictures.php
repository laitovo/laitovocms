<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:43 AM
 */
use backend\modules\laitovo\models\CarsPictures;

/**
 * @var $info \backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO
 */

$pictureManager = new CarsPictures($info->getArticle());
$pictures = $pictureManager->getImages();

$sideTranslates = [];
$sideTranslates['ru'] = [
    'top' => 'Сверху',
    'left' => 'Справа',
    'right' => 'Слева',
    'bottom' => 'Снизу',
];
$sideTranslates['en'] = [
    'top' => 'top',
    'left' => 'left',
    'right' => 'right',
    'bottom' => 'bottom',
];

?>

<? if ($lang == 'ru'):?>

<div style="margin: 20px 0px">
    <?php $flag = false;?>
    <?php foreach ($pictures as $key => $picture) {?>
    <tr>
        <?if (!$flag):?>
            <td colspan="1" rowspan="<?= count($pictures)?>" style="padding: 10px; vertical-align: center">
                <section style="font-size: 12px">
                    <div style="width: 100%">
                        <?= $this->render('_text/ru/_equipment',['info' => $info])?>
                    </div>
                    <div style="width: 100%;">
                        <?= $this->render('_text/ru/_qr_code')?>
                    </div>
                </section>
            </td>
            <?php $flag = true;?>
        <?endif;?>
        <td colspan="2" style="text-align: left">
            <div style="text-align: center; padding-bottom: 10px">
                <img style="height: 140px;" src="<?= Yii::$app->urlManager->createAbsoluteUrl($picture->src)?>" /> <br>
            </div>
        </td>
        <td colspan="1" style="text-align: left; padding: 10px"">
            <div style="text-align: center;">
                    <span style="display: inline-block; border: 2px solid black; padding: 10px; font-size: 16px;">
                        <?= $picture->side ? $sideTranslates['ru'][$picture->side] : '' ?><br>
                        <?= $picture->name ? "№ $picture->name" : '' ?>
                    </span>
            </div>
            <div style="text-align: center;">
                <span style="display: inline-block; padding: 10px; font-size: 16px;">
                    Рис.<?=$picture->num?>
                </span>
            </div>
        </td>

    </tr>
    <?php } ?>
</div>

<?php else: ?>

<div style="margin: 20px 0px">
    <?php $flag = false;?>
    <?php foreach ($pictures as $key => $picture) {?>
        <tr>
            <?if (!$flag):?>
                <td colspan="1" rowspan="<?= count($pictures)?>" style="padding: 10px; vertical-align: center">
                    <section style="font-size: 12px">
                        <div style="width: 100%">
                            <?= $this->render('_text/en/_equipment',['info' => $info])?>
                        </div>
                        <div style="width: 100%;">
                            <?= $this->render('_text/en/_qr_code')?>
                        </div>
                    </section>
                </td>
                <?php $flag = true;?>
            <?endif;?>
            <td colspan="2" style="text-align: left">
                <div style="text-align: center;  padding-bottom: 10px">
                    <img style="height: 140px;" src="<?= Yii::$app->urlManager->createAbsoluteUrl($picture->src)?>" /> <br>
                </div>
            </td>
            <td colspan="1" style="text-align: left; padding: 10px">
                <div style="text-align: center;">
                        <span style="display: inline-block; border: 2px solid black; padding: 10px; font-size: 16px;">
                            <?= $picture->side ? $sideTranslates['en'][$picture->side]: '' ?><br>
                            <?= $picture->name ? "№ $picture->name" : '' ?>
                        </span>
                </div>
                <div style="text-align: center;">
                    <span style="display: inline-block; padding: 10px; font-size: 16px;">
                        Pic.<?=$picture->num?>
                    </span>
                </div>
            </td>

        </tr>
    <?php } ?>
</div>

<?php endif; ?>