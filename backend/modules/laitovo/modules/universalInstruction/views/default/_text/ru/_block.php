<?php


/* @var $this yii\web\View */
/* @var $dto InstructionDTO */
/* @var $info ArticleDTO */

use backend\modules\laitovo\models\CarsPictures;
use backend\modules\laitovo\modules\universalInstruction\models\ArticleDTO;
use backend\modules\laitovo\modules\universalInstruction\models\InstructionDTO;

$info = $dto->getArticleInfo();
$pictureManager  = new CarsPictures($info->getArticle());
$pictures        = $pictureManager->getImages();
$picNumbers      = $pictureManager->getPicNumbers();

?>
<!--Вся инструкция будет ввиде таблицы-->
<div>
    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial" >
        <!--Блок 1-ый. Вводная верхня надпись для инструкции-->
        <tr>
            <td colspan="4" style="background-color:; text-align: left">
                <?= $this->render("_header", ['info' => $info]);?>
            </td>
        </tr>
        <!--Блок 2-ой. Артикулы и автомобили-->
        <tr>
            <td colspan="4" style="text-align: left">
                <hr size="4" color="black">
                <?= $this->render('../_cars',   ['list' => $dto->articleList]);?>
                <hr size="4" color="black">
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: left">
                <?= $this->render("_schemes",['dto' => $dto]);?>
            </td>
        </tr>
        <!--Блок 4-ый. Блок изображений установки-->
        <?= $this->render("_pictures",['info' => $info]);?>
        <!--Блок 5-ый. Блок текст установки-->
        <tr>
            <td colspan="4" style="text-align: left">
                <hr>
                <div style="padding: 5px 20px">
                    <div>
                        <div style="width: 100%">
                            <section style="font-size: 14px">

                                <?= $this->render('_function')?>
                                <?= $this->render('_warnings')?>
                                <?= $this->render('_care')?>

                                <div>
                                    <div style="float:left;width: 70%">
                                        <?= $this->render('_dismantling',['info' => $info])?>
                                        <?= $this->render('_warranty')?>
                                    </div>
                                    <div style="float:left;width: 30%;text-align: center">
                                        <?= $this->render('_contacts')?>
                                    </div>
                                    <div style="display: table; clear:both"></div>
                                </div>
                                <div style="border:1px solid black; padding: 5px">
                                    <?= $this->render('_disclaimer')?>
                                </div>
                            </section>
                        </div>
                        <div style="width: 50%"></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
