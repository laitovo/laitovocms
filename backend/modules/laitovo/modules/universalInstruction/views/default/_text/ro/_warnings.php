<h4>ATENTIONARI:</h4>
<p>
<ul style="padding: 0px 20px">
    <li>
        Nu este recomandat sa folositi perdelutele frontale in timpul mersului.
    </li>
    <li>
        Nu este recomandat sa scuturati scrumul de tigara sau sa aruncati diverse obiecte pe geam cu perdelutele instalate.
    </li>
    <li>
        In conditii de temperatură scazuta si atunci cand interiorul vehiculului nu este incalzit, materialul ecranelor
        de protectie poate cadea usor. Aceasta problema se rezolva odata ce interiorul este incalzit.
    </li>
    <li>
        Nu se recomanda sa se produca un impact fizic asupra materialului textil al perdelutelor, deoarece ar putea provoca deteriorarea acestora.
    </li>
</ul>
</p>