<h4 style="margin:5px 0; margin-top: 10px;">REINIGEN DES SONNENSCHUTZES</h4>
<p  style="margin:5px 0">
    Wenn die Gewebeoberfläche verschmutzt ist, empfehlen wir Ihnen diese mit einem feuchten Tuch oder Schwamm zu reinigen.
    Bei starken Verschmutzungen können schonende Reinigungsmittel oder Mittel zu Pflege von Gewebeoberflächen verwendet werden.
</p>