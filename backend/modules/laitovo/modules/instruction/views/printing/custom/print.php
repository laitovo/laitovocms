<style>
    #output p {
        margin-top: 0;
        margin-bottom: 0;
        font-family: Calibri, sans-serif;
        font-size: 10px;
        line-height: 0.8em;
    }
</style>
<div id="output">
    <div class="block1" style="width:820px;"><?=$blocks[1] ?? null?></div>
    <div class="block2">
        <?php if ($imageUrl): ?>
            <img src="<?=$imageUrl?>" style="width:820px;">
        <?php endif; ?>
    </div>
    <table width="820">
        <tr>
            <td valign="top" width="50%">
                <div class="ru">
                    <div class="block3"><?=$blocks[3]['ru'] ?? null?></div>
                    <div class="block4"><?=$blocks[4]['ru'] ?? null?></div>
                    <div class="block5"><?=$blocks[5]['ru'] ?? null?></div>
                    <div class="block6"><?=$blocks[6]['ru'] ?? null?></div>
                </div>
            </td>
            <td valign="top">
                <div class="de">
                    <div class="block3"><?=$blocks[3]['de'] ?? null?></div>
                    <div class="block4"><?=$blocks[4]['de'] ?? null?></div>
                    <div class="block5"><?=$blocks[5]['de'] ?? null?></div>
                    <div class="block6"><?=$blocks[6]['de'] ?? null?></div>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top" width="50%">
                <div class="en">
                    <div class="block3"><?=$blocks[3]['en'] ?? null?></div>
                    <div class="block4"><?=$blocks[4]['en'] ?? null?></div>
                    <div class="block5"><?=$blocks[5]['en'] ?? null?></div>
                    <div class="block6"><?=$blocks[6]['en'] ?? null?></div>
                </div>
            </td>
            <td valign="top">
                <div class="cn">
                    <div class="block3"><?=$blocks[3]['cn'] ?? null?></div>
                    <div class="block4"><?=$blocks[4]['cn'] ?? null?></div>
                    <div class="block5"><?=$blocks[5]['cn'] ?? null?></div>
                    <div class="block6"><?=$blocks[6]['cn'] ?? null?></div>
                </div>
            </td>
        </tr>
    </table>
</div>
