<?php
//use backend\modules\laitovo\models\CarsForm;
//use backend\modules\laitovo\models\CarsScheme;
//use common\models\laitovo\Cars;
use backend\widgets\viewbuilder\ManualAnalogs;
use backend\helpers\ArticleHelper;
use backend\helpers\SchemeHelper;

$car = ArticleHelper::getCar($article)->one();
?>
<!--    <p class="page-break---><?//= $model->id ?><!--"></p>-->


<?// if (@$model->json('items')[0]['car']): ?>
<? if ($car): ?>

    <table width="820" cellspacing="5" cellpadding="5">
        <tr>
            <td colspan="2" style=" text-align: center;font-size: 17px">
               
                <b>ИНСТРУКЦИЯ ПО УСТАНОВКЕ И ЭКСПЛУАТАЦИИ </b><br>
                <b> <b>КОМПЛЕКТА ЗАЩИТНЫХ ЭКРАНОВ ДЛЯ АВТОМОБИЛЬНЫХ ОКОН LAITOVO MAGNET  </b>
               
            </td>
           
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px">
<!--                --><?//= ManualAnalogs::widget([
//                    'car_id'=> @$model->car->id,
//                    'window_type' => $model->window
//                ]) ?>
                <?= ManualAnalogs::widget([
                    'car_id'=> @$car->id,
                    'window_type' => ArticleHelper::getWindow($article)
                ]) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left">
                <a style="font-size: 15px;">Защитные экраны для автомобильных окон предназначены для защиты салона
                    транспортного средства, в том числе находящихся в нем пассажиров от солнца, насекомых, осколков
                    стекла и защиты частной жизни в транспортном средстве.
                </a>
            </td>
           
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: left; ">
                    <b style="">Особенности эксплуатации</b>
                </li>
                <li style=" padding-left: 20px ">
                    запрещается стряхивать пепел от сигарет, выбрасывать мусор в окно с установленными защитными
                    экранами, это может привести к повреждению тканевого покрытия.
                </li>
                <li style=" padding-left: 20px ">
                    запрещается оказывать физическое воздействие (ущемление, прокалывание и т. д.) тканевого покрытия,
                    это может привести к его повреждению.
                </li>
                <li style=" padding-left: 20px ">
                    не рекомендуется эксплуатация передних боковых защитных экранов во время движения автомобиля.
                </li>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Уход</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a>
                    При загрязнении поверхности экрана рекомендуется промыть под струей воды, (изделия из ткани №1.5(medium) и ткани №2(темная)
                    возможно промывать струей высокого давления), просушить. При незначительном загрязнении тканевого покрытия рекомендуется протереть
                    влажной салфеткой или губкой, возможно применение щадящих моющих средств или средств по уходу за тканевыми поверхностями.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Комплектность</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: left; ">
                    <a>В комплект поставки входит:</a>
                </li>
                <li style=" list-style-type: none;">
                    Защитные экраны, шт ...................................................2
                </li>
                <li style="list-style-type: none;">
                    Инструкция по эксплуатации, экз .................................1
                </li>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Хранение</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a>
                    Комплект защитных экранов Laitovo Magnet вне эксплуатации не требует особых условий хранения и осуществляется в любом
                    удобном месте. Рекомендуется хранить в специальной фирменной сумке. Не следует хранить вблизи открытого пламени, острых,
                    режущих предметов и в помещении с повышенной влажностью.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px;border: 2px solid;
             border-radius: 15px;padding-left: 5px;
            background-color:yellow ">
                <b style="">ВНИМАНИЕ!              
                   Защитные экраны Laitovo Magnet используются только на автомобилях с металлической обшивкой оконного проема двери.
                   </b>
            </td>
        </tr>
       
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px;">
                <b style="">Установка</b>
            </td>
        </tr>
    </table>

    <table width="820">
        <tr>
            <td style=" ">
                <img width="250px" src="/img/manual-chico-magnet/12.jpg">
            </td>
            <td style=" text-align: left; padding-left: 30px ">
                <a style="font-size: 18px">
                    <b>1</b>. Откройте дверь.
                </a>
            </td>

        </tr>
        <tr>
            <td width="15%" style=" ">
                <img width="250px" src="/img/manual-chico-magnet/7.jpg">
            </td>
            <td  style=" text-align: left; padding-left: 30px">
                <a style="font-size: 18px">
                    <b>2</b>. Установите экран в оконный проем, прикрепив его к металлической
                    рамке двери. Убедитесь, что защитный экран  надежно зафиксирован в оконном проеме.
                </a>
            </td>
        </tr>
        <tr>
        </tr>
            <td colspan="2" style=" text-align: left; font-size: 15px;">
                <b style="">Гарантийные обязательства</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px">
                <a>
                    При надлежащем соблюдении покупателем правил эксплуатации, продавец устанавливает срок гарантии на
                    изделие
                    – 1 год с даты получения покупателем товара.
                </a>
            </td>
        </tr>
    </table>

    <table width="820" >
        <tr>
            <td width="20%" style=" text-align: left; font-size: 14px;">
                <b style="">Контакты:
                    +7 499 703 06-85
                    E-mail: info@laitovo.ru<br>
                www.laitovo.ru </b>
            </td>
            
             <td width="" style="text-align: center">
                <img width="200" src="/img/manual/laitovo-magnet/logo-laitovo-magnet.png">
            </td>
            
            <td style="text-align: center">
                <img width="50 px" src="/img/manual/round.jpg"><br>
                <a style="font-size: 13px"> ОТК </a>
            </td>
            <td width="20%" style=" text-align: left; font-size: 15px">
                
                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    _________________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                    <?=date('d-m-Y'); ?>
                </li>
            </td>
        </tr>
    </table>


<? endif ?>
