<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:49 AM
 */
?>
<hr>
<div style="padding: 5px 20px">
    <div style="text-align: center">
        <div style="display: inline-block;color: white;background-color: black;font-weight: bold;padding: 1px">EN</div>
    </div>
    <div>
        <div style="width: 100%">
            <section style="font-size: 12px">
                <div style="width: 50%;float: left">
                    <h4>PACKAGE CONTENTS</h4>
                    <!--Комплектация-->
                    <div >
                        <div style="width: 50%;float: left">
                            <ul>
                                <li>
                                    Protective screens ...... <?= $equipment['countWindows']??5 ?>
                                </li>
                                <li>
                                    Instruction  ................... 1
                                </li>
                                <?if ($needHook):?>
                                <li>
                                    Installation hook ......... 1
                                </li>
                                <?endif;?>
                            </ul>
                        </div>
                        <div style="width: 50%;float: left">
                            <ul>
                                <? if (isset($equipment['clips']) && count( ($clips = $equipment['clips']) )): foreach ($clips as $key => $value) : ?>
                                    <? if ($key == 'M') : ?>
                                        <li>
                                            Magnetic holders ... 10
                                        </li>
                                    <? else : ?>
                                        <li>
                                            Clip-holder № <?= $key ?>..... <?= $value ?>
                                        </li>
                                    <? endif; ?>
                                <? endforeach; ?>
                                <? endif; ?>
                            </ul>
                        </div>
                        <div style="display: table; clear:both"></div>
                    </div>
                </div>
                <div style="width: 50%;float: left">
                    <h4>FUNCTION AND SERVICING</h4>
                    <p>
                        Protective screens for car windows are designed for car saloon protection,
                        also protection of the passengers inside from the sun, insects, glass fragments
                        and protection of a private life in a vehicle
                    </p>
                </div>
                <div style="display: table; clear:both"></div>
            </section>
            <section style="font-size: 12px">

                <h4>WARNINGS</h4>
                <p>
                <ul style="padding: 0px 20px">
                    <li>
                        It is not recommended to use the front side protective screens on-the-ride.
                    </li>
                    <li>
                        It is not recommended to flick cigarette ash, dump the trash out of the windows
                        with protective screens nstalled, if they are not equipped with special cutaways.
                        It may cause textile cover damage.
                    </li>
                    <li>
                        In winter (during cold weather), in a not warmed-up saloon, the material of protective
                        screens may hang down (textile peculiarity). This defect vanishes after you warmup the vehicle.
                    </li>
                    <li>
                        It is not recommended to make a physical impact on textile cover of protective screens
                        (jamming, transfixing etc.) as it may cause its damage.
                    </li>
                </ul>
                </p>
                <h4>SUN SHADES CARE</h4>
                <p>
                    When the protective surface of the sun shade is dirty, it is recommended to wipe it with the wet wipes or
                    sponge. In case of strong dirtiness, it is possible to use gentle detergents or means for the care of fabric
                    surfaces.
                </p>
                <h4>INSTALLATION MANUAL</h4>
                <p>
                    Before the installation, make sure that you have purchased a kit that will be suitable for
                    your car model and for the window specified in the instruction, first try on a sun shade to
                    the windows of the car. Check the conformity of the kit specified in the instructions with
                    the actual presence.
                </p>
                <ul style="list-style-type: decimal;padding: 0px 20px">
                    <?php if (isset($newClips['V'])):?>
                        <li>
                            Take the screen at the place of fastening the Velcro tape, and glue it to the window
                            aperture in the place indicated in the diagram (fig.1) with the letter “V”
                            as shown in Fig.<?=isset($pictures['V'])?$pictures['V']->num:'';?>
                        </li>
                        <?php $velkro = true;unset($newClips['V']);?>
                    <?php endif;?>
                    <?php if (in_array($window,['FD','RD'])):?>
                        <li>
                            Open the door and open the window.
                        </li>
                    <?endif;?>
                    <?if ($needHook):?>
                    <li>
                        If there is included an installation hook with your kit,
                        use it to bend the casing of the window trim.
                    </li>
                    <?endif;?>
                    <?if (empty($newClips) || isset($velkro) || in_array($window,['BW']) ):?>
                        <li>
                            Install the screen into the window aperture indicated in the instructions in thrust.<?= (!isset($velkro)) && !empty($pictures) && empty($newClips) ? ' Pic.' . @$pictures[0]->num : '' ?>
                        </li>
                    <?endif;?>
                    <?php foreach ($newClips as $key => $value) {?>
                        <li>
                            Take the pack with clips №<?=$key;?> , and set them as indicated
                            in the diagram (pic.1) under №<?=$key;?> as shown in the pic. <?=isset($pictures[$value])?$pictures[$value]->num:'';?>.
                        </li>
                    <?php } ?>
                    <?php if (in_array($window,['FD','RD'])):?>
                        <li>
                            Close the window.
                        </li>
                    <?endif;?>
                    <?php if (!isset($velkro) && !empty($newClips) && !in_array($window,['BW'])):?>
                    <li>
                        Insert the frame of the sun shade into the clip-holders.
                        It is better to do this step by step: first, the side edge of the sun shade, then the top.
                        The lower edge of the sun shade must lie on the casing without fixing in the clips-holders.
                    </li>
                    <?php endif;?>
                </ul>
                <div>
                    <div style="float:left;width: 70%">
                        <h4>REMOVING OF THE SUN SHADE</h4>
                        <ul style="list-style-type: decimal;padding: 0px 20px">
                            <li>
                                Grasp the loop at the top of the sun shade.
                            </li>
                            <li>
                                Slightly pull the edge of the shade towards the center so that it can be removed from the clip-holder.
                            </li>
                            <li>
                                Remove the sun shade from the area of the clip holder.
                            </li>
                            <li>
                                Remove the sun shade from the rest of the clip holders.
                            </li>
                        </ul>
                        <h4>CONTACTS</h4>
                        <p>
                            Should you have any questions<br>
                            feel free to contact us under:<br>
                            Tel.:+44 20 3695 1976 (UK)<br>
                            <span style="text-decoration: underline">E-mail: info@laitovo.eu</span>
                        </p>
                    </div>
                    <p style="float:right">
                        <img style="width: 200px" src="/img/instructions/qren.png" alt="">
                    </p>
                    <div style="display: table; clear:both"></div>
                </div>
            </section>
        </div>
        <div style="width: 50%"></div>
    </div>
</div>
