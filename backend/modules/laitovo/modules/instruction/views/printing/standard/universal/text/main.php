<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:50 AM
 */
use backend\modules\laitovo\models\CarsPictures;

/**
 * @var $article string
 * @var $equipment string
 */

$pictureManager = new CarsPictures($article);
$pictures = $pictureManager->getImages();
$newClips = \backend\helpers\ArticleHelper::getClipsWithTypes($article);
$needHook = \backend\helpers\ArticleHelper::needHook($article);
$window = \backend\helpers\ArticleHelper::getWindowEn($article);

echo $this->render('en',['equipment' => $equipment,'pictures' => $pictures,'newClips' => $newClips,'needHook' => $needHook,'window' => $window]);
echo '<br>';
echo $this->render('de',['equipment' => $equipment,'pictures' => $pictures,'newClips' => $newClips,'needHook' => $needHook,'window' => $window]);
echo '<br>';
echo $this->render('ru',['equipment' => $equipment,'pictures' => $pictures,'newClips' => $newClips,'needHook' => $needHook,'window' => $window]);