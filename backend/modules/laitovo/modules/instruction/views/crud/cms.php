<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Конструктор инструкций');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['/laitovo/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>

<style>
    b, strong {
        font-weight: bold;
    }

    textarea {
        height: 100px !important;
    }

    .content-block {
        margin-bottom: 40px;
    }

    .country-block {
        margin-top: 10px;
    }

    .selection-block {
        margin-bottom: 10px;
    }

    .selection-block .button-select-variant, .selection-block select {
        float:left;
        margin-right:5px;
    }

    .selection-block select {
        width:200px;
    }

    .selection-block .button-select-ok, .selection-block .button-select-cancel {
        cursor: pointer;
    }

    .selection-block.closed select, .selection-block.closed .button-select-ok, .selection-block.closed .button-select-cancel {
        display: none;
    }

    /* TEXT EDITOR */

    .redactor-box .redactor-editor {
        border: 1px solid rgb(228, 234, 236);
        border-radius: 4px;
        transition: border-color 0.2s linear;
    }

    .redactor-box .redactor-editor:focus {
        border: 1px solid rgb(103, 186, 179);
    }

    .redactor-box  .redactor-editor b, .redactor-box .redactor-editor strong {
        font-weight: bold;
    }

    .redactor-box  .redactor-editor p {
        margin: 0;
        font-family: Calibri, sans-serif;
    }
</style>

<div style="margin-bottom:40px;">
    <b class="text-info" style="font-size:2em;">Автомобиль: </b><?= Html::a(Yii::t('app', $carName), Url::to(['/laitovo/cars/view', 'id' => $carId]))?><br>
    <b class="text-info" style="font-size:2em;">Вид инструкции: </b><?=$instructionTypeInfo?>
</div>

<?= Html::beginForm(Url::to(['save', 'carId' => $carId, 'instructionTypeId' => $instructionTypeId]),'POST', ['enctype' => 'multipart/form-data']);?>
<div id="content">
    <div class="content-block block1">
        <h3>Блок 1 (Шапка)</h3>
        <?= \vova07\imperavi\Widget::widget([
            'name' => 'blocks[1]',
            'value' => $blocks[1] ?? '<p style="text-align: center;">INSTRUCTION FOR INSTALLATION AND SERVICING OF THE SET OF PROTECTIVE SCREENS FOR CAR WINDOWS</p> 
                <p style="text-align: center;">LAITOVO FOR ' . $windowAlias . '</p>
                <p style="text-align: center;">' . $carName . '</p>
                <p style="text-align: center;">' . $analogsInfo . '</p>',
            'settings' => [
                'minHeight' => 200,
                'lang' => 'ru',
                'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                'plugins' => [
                    'fontcolor',
                ],
            ],
        ]); ?>
    </div>
    <div class="content-block block2">
        <h3>Блок 2 (Картинка)</h3>
        <?php if ($isNew): ?>
            <img id="image" class="hide" src="" style="height:100px;">
        <?php else: ?>
            <img id="image" src="<?=$imageUrl?>" style="height:100px;">
        <?php endif; ?>
        <input type="file" id="file" name="image"/>
    </div>
    <div class="content-block block3">
        <h3>Блок 3 (Комплектация)</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="country-block ru">
                    <h4>RU</h4>
                    <?php
                        $html = '<p><strong><span style="border:1px solid;margin-right:5px;position:relative;top:-3px;">RU</span>КОМПЛЕКТАЦИЯ:</strong></p>';
                        $html .= '<p>Защитные экраны, шт - ' . $windowsCount . '</p>';
                        foreach ($clips as $key => $value) {
                            if ($key == 'M') {
                                $html .= '<p>Комплект магнитных держателей, шт - 10</p>';
                            } else {
                                $html .= '<p>Зажим № ' . $key . ', шт - ' . $value . '</p>';
                            }
                        }
                        $html .= '<p>Инструкция по эксплуатации, экз - 1</p>';
                        echo \vova07\imperavi\Widget::widget([
                            'name' => 'blocks[3][ru]',
                            'value' => $blocks[3]['ru'] ?? $html,
                            'settings' => [
                                'minHeight' => 200,
                                'lang' => 'ru',
                                'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                                'plugins' => [
                                    'fontcolor',
                                ],
                            ],
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="country-block de">
                    <h4>DE</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[3][de]',
                        'value' => $blocks[3]['de'] ?? '<p><strong><span style="border:1px solid;margin-right:5px;position:relative;top:-3px;">DE</span></strong>&nbsp;</p>',
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="country-block en">
                    <h4>EN</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[3][en]',
                        'value' => $blocks[3]['en'] ?? '<p><strong><span style="border:1px solid;margin-right:5px;position:relative;top:-3px;">EN</span></strong>&nbsp;</p>',
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="country-block cn">
                    <h4>中文</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[3][cn]',
                        'value' => $blocks[3]['cn'] ?? '<p><strong><span style="border:1px solid;margin-right:5px;position:relative;top:-3px;">中文</span></strong>&nbsp;</p>',
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content-block block4">
        <h3>Блок 4 (Эксплуатация)</h3>
        <div class="selection-block closed clearfix">
            <?= Html::button(Yii::t('app',  'Выбрать вариант из списка'), ['class' => 'btn btn-sm btn-primary button-select-variant']) ?>
            <select class="form-control">
                <option
                    data-ru='<p><strong>НАЗНАЧЕНИЕ И ЭКСПЛУАТАЦИЯ</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span>Защитные экраны для автомобильных окон предназначены для защиты
                        салона транспортного средства, в том числе находящихся в нем пассажиров от солнца, насекомых,
                        осколков стекла и защиты частной жизни в транспортном средстве.</p>
                        <p><strong>УХОД ЗА ЗАЩИТНЫМИ ЭКРАНАМИ</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span>При загрязнении тканевой поверхности защитного экрана из других
                        материалов рекомендуется протереть влажной салфеткой или губкой. При сильных загрязнениях
                        возможно применение щадящих моющих средств или средств по уходу за тканевыми поверхностями.</p>
                        <p><strong>ПРЕДУПРЕЖДЕНИЯ</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span>Не рекомендуется эксплуатация передних боковых защитных экранов
                        во время движения автомобиля, стряхивать пепел от сигарет, выбрасывать мусор в окно с
                        установленными защитными экранами, оказывать физическое воздействие (ущемление, прокалывание
                        и т. д.) тканевого покрытия защитного экран, это может привести к его повреждению. Зимой (в
                        холодную погоду), в не прогретом салоне материал защитных экранов может провисать (особенность
                        материала). Этот эффект исчезает как только вы прогреете автомобиль.</p>'
                    data-en='<p style="text-align:justify;"><span style="margin-right:20px;"></span>Protective screens for car windows are designed for car
                        saloon protection, also protection of the passengers inside from the sun, insects, glass
                        fragments and protection of a private life in a vehicle.</p>
                        <p><strong>WARNINGS</strong></p>
	                    <p style="text-align:justify;"><span style="margin-right:20px;"></span>It is not recommended to use the front side protective screens
                        on-the-ride; to flick cigarette ash, dump the trash out of the windows with protective screens
                        installed, if they are not equipped with special cutaways, to make a physical impact on textile
                        cover of protective screens (jamming, transfixing etc.) it may cause textile cover damage. In
                        winter (during cold weather), in a not warmed-up saloon, the material of protective screens may
                        hang down (textile peculiarity). This effect vanishes after you warmup the vehicle.</p>'
                    data-de='<p><strong>ZWECKBESTIMMUNG UND BETRIEB</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span>Schutzschirme für Autofenster dienen dem Schutz des
                        Kfz-Innenraums, einschließlich der darin befindlichen Fahrgäste, vor Sonne, Insekten,
                        Glassplittern; auch Privatleben im Auto wird geschützt.</p>
                        <p><strong>BETRIEBSBESONDERHEITEN</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span>Beim Autofahren ist der Betrieb der Seitenschutzschirme vorn
                        nicht empfehlenswert. Es ist nicht empfehlenswert, Zigarettenasche abzuschütteln; Müll zum
                        Autofenster herauszuwerfen, an dem Schutzschirme befestigt sind, sofern sie keine
                        Spezialausschnitte haben; das Schutzschirmgewebe einer physischen Einwirkung (der Einklemmung,
                        dem Durchstechen) zu unterziehen, denn dadurch kann das Gewebe beschädigt werden. Winters
                        (bei kaltem Wetter) kann es passieren, dass im unbeheizten Innenraum das Schutzschirmmaterial
                        durchhängt (Materialbesonderheit); dieser Effekt verschwindet von selbst, sobald das Auto
                        beheizt wird.</p>'
                    data-cn='<p><strong>功能和服务</strong></p>
                        <p style="text-align:justify;">车窗防护网是专门为汽车轿车防护而设计的，还可以保护乘客免受阳光照射，昆虫，
                        玻璃碎片和车辆私人生活的保护。</p>
                        <p><strong>警告</strong></p>
                        <p style="text-align:justify;">建议不要在车上使用前侧防护网。将香烟灰甩出，将装有防护网的窗户上的垃圾倒掉，
                        如果没有配备特殊的切口，对保护网的纺织品表面造成物理冲击（堵塞，穿透等），可能会导致纺织品罩损坏。在冬季（寒冷的天气），
                        在一个没有温暖的轿车里，防护网的材料可能会下垂（纺织品的特殊性）。这个效果在你暖车后消失。</p>'
                >Вариант 1</option>
            </select>
            <i class="fa fa-check button-select-ok"></i>
            <i class="fa fa-close button-select-cancel"></i>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="country-block ru">
                    <h4>RU</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[4][ru]',
                        'value' => $blocks[4]['ru'] ?? null,
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="country-block de">
                    <h4>DE</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[4][de]',
                        'value' => $blocks[4]['de'] ?? null,
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="country-block en">
                    <h4>EN</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[4][en]',
                        'value' => $blocks[4]['en'] ?? null,
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="country-block cn">
                    <h4>中文</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[4][cn]',
                        'value' => $blocks[4]['cn'] ?? null,
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content-block block5">
        <h3>Блок 5 (Установка)</h3>
        <div class="selection-block closed clearfix">
            <?= Html::button(Yii::t('app',  'Выбрать вариант из списка'), ['class' => 'btn btn-sm btn-primary button-select-variant']) ?>
            <select class="form-control">
                <option
                    data-ru='<p><strong>УСТАНОВКА ЗАЖИМОВ-ДЕРЖАТЕЛЕЙ</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span>Перед установкой убедитесь, что Вы приобрели комплект для
                        Вашего автомобиля - примерьте защитный экран к окнам автомобиля. Если экраны повторяют контур
                        оконного проема, приступайте к установке.</p>
                        <p style="text-align:justify;"><strong>1.</strong> Ознакомьтесь со схемой расстановки зажимов-держателей.</p>
                        <p style="text-align:justify;"><strong>2.</strong> Опустите стекло (открыть окно)</p>
                        <p style="text-align:justify;"><strong>3.</strong> Зажим-держатель № 1. Отогните резиновый уплотнитель и
                        зацепите зажим-держатель за металлический бортик  со стороны улицы (Рис 2.), затем защелкните
                        вторую часть зажима за бортик со стороны салона (Рис 3).</p>
                        <p style="text-align:justify;"><strong>4.</strong> Зажим-держатель № 2 и №3.  Зацепите длинную часть
                        зажима-держателя за бортик со стороны салона (Рис.5) затем отогнув резиновый уплотнитель
                        защелкните вторую часть зажима за бортик  со стороны улицы(Рис.6).</p>
                        <p style="text-align:justify;"><strong>5.</strong> Закрыть окно.</p>
                        <p style="text-align:justify;"><strong>6.</strong> Установите экран.</p>
                        <p><strong>УСТАНОВКА ЭКРАНА</strong></p>
                        <p style="text-align:justify;"><strong>1.</strong> Установите часть экрана в зажимы-держатели № 2 и №3
                        установленные по стойке.</p>
                        <p style="text-align:justify;"><strong>2.</strong> Полностью установите низ экрана на обшивку.</p>
                        <p style="text-align:justify;"><strong>3.</strong> Потянуть вниз хлястик и вставить экран в зажим №1 на дуге.</p>
                        <p style="text-align:justify;"><strong>4.</strong> При необходимости поправьте экран.</p>
                        <p style="text-align:justify;"><strong>5.</strong> Демонтаж экрана производится в обратном порядке.</p>
                        <p style="text-align:justify;">Подробную видео инструкци по установке на Ваш автомобиль Вы можете найти на сайте laitovo.ru
                        в разделе  О лайтово / Инструкция по установке  или на нашем канале в YouTube “Установка Laitovo”</p>'
                    data-en='<p><strong>CLIP-HOLDERS INSTALLATION</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span>Before installing clip-holders and protective screens make sure
                        that you have received a set for your car. If the screen is similar to the contour of the
                        window opening - put down the glass and install clipholders according to installation diagram
                        (Pic.1) and instructions for the corresponding type of fixing <strong>(A,B,C)</strong>.</p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>TYPE OF FIXING <em>A</em>:</strong> <strong>1.</strong> Unbend a rubber weatherstrip located in
                        the door window frame in the position of a clip-holder installation; <strong>2.</strong> Fasten the clip-holder;
                        <strong>3.</strong> Return a rubber weatherstrip into the original position; <strong>4.</strong> Repeat steps 1-3 in the rest of
                        positions* of clip-holders installing, according to the clip-holders installation diagram.</p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>TYPE OF FIXING <em>B</em>:</strong> <strong>1.</strong> Take out a rubber weatherstrip located in
                        the window frame in the  position of the clip holder installation;  <strong>2.</strong> Hook the clip-holder to
                        the window-frame; <strong>3.</strong> Install a rubber weatherstrip into the original position; <strong>4.</strong> Repeat steps
                        1-3 in the rest of positions* of clip-holders installing, according to the clip-holders
                        installation diagram.</p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>TYPE OF FIXING <em>C</em>:</strong> <strong>1.</strong> Hook the edge of plastic cover with help
                        of special hook and click the clip-holder; <strong>2.</strong> Put a clip-holder into the bent edge of plastic
                        cover; <strong>3.</strong> Put the bend of a clip-holder to the other edge of plastic cover and click the
                        clip-holder use special hook if needed; <strong>4.</strong> Repeat steps 1-3 in the rest of positions* of
                        clip-holders installing, according to the clip-holders installation diagram.</p>
                        <p><strong>PROTECTIVE SCREEN INSTALLATION INTO CLIP-HOLDERS</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>1.</strong> Uplift the glass. <strong>2.</strong> Insert the frame of the protective screen
                        into the clip-holders, one at the time: first the side edge of the screen, then the upper edge.
                        The lower edge of the protective screen must be located on the cover. (Pic.2)</p>
                        <p><strong>PROTECTIVE SCREEN DISMANTLING</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>1.</strong> Grasp the loop in the upper part of a screen. <strong>2.</strong> Slightly
                        pull the edge of the screen toward the center so that it can be taken out of the clip-holder.
                        <strong>3.</strong> Take off the protective screen out of the holder-zone of a clip-holder. <strong>4.</strong> Take off the
                        protective screen out the rest of clip-holders.</p>'
                    data-de='<p><strong>ANBRINGUNG VON KLAMMERHALTERN</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span>Vor Anbringung der Klammerhalter und der Schutzschirme
                        überzeugen Sie sich davon, dass Sie Schutzschirme für Ihr KfZ erworben haben und der
                        Schutzschirm der Fensteröffnung entspricht. Wenn die Kurven der Fensteröffnung stimmen,
                        versetzen Sie das Türfenster in die untere Lage und installieren die Klemmhaltern laut
                        dem Befestigungsschema (Abb.1) und der Anleutung für jeden Befestigungstyp <strong>(A,B,C)</strong>.</p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>BEFESTIGUNGTYP <em>A</em>:</strong>  <strong>1.</strong> Biegen Sie den Gummiverdichter im
                        Türfensterrahmen am  Anbringungsort des Klammerhalters; <strong>2.</strong> Hinter dem Bördelklammerbereich
                        befestigen Sie den Klammerhalter; <strong>3.</strong> Den Gummiverdichter versetzen Sie in die ursprüngliche
                        Lage; <strong>4.</strong> Gehen Sie Punkte 1 bis 3 an allen anderen Anbringungsstellen der Klammerhalter
                        entsprechend dem Befestigungsplan durch.</p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>BEFESTIGUNGTYP <em>B</em>:</strong>  <strong>1.</strong> Nehmen Sie den Gummiverdichter im
                        Türfensterrahmen am ersten Anbringungsort  des Klammerhalters heraus; <strong>2.</strong> Haken Sie den
                        Klammerhalter mit dem Klemmbereich am Fensterrahmenbördel ein; <strong>3.</strong> Den Gummiverdichter versetzen
                        Sie in die ursprüngliche Lage; <strong>4.</strong> Gehen Sie Punkte 1 bis 3 an allen anderen Anbringungsstellen
                        entsprechend dem Befestigungsplan durch.</p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>BEFESTIGUNGTYP <em>C</em>:</strong>  <strong>1.</strong> Am Anbringungsort (von der Seite des
                        Kfz-Innenraums her) haken Sie mit einem Spezialhaken den Plastikbeschlagrand ein; <strong>2.</strong> An den
                        abgebogenen Plastikbeschlagrand führen Sie den Klammerhalter ein; <strong>3.</strong> Führen Sie die
                        Klammerhalterbiegung an den Plastikbeschlagrand heran und schnappen Sie den Klammerhalter ein;
                        <strong>4.</strong> Gehen Sie Punkte 1 bis 3 an allen anderen Anbringungsstellen*  des Klammerhalters
                        entsprechend dem Befestigungsplan durch.</p>
                        <p><strong>SCHUTZCHIRMANBRINGUNG AN KLAMMERHALTERN</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>1.</strong> Die Glasscheibe wird hochgehoben. <strong>2.</strong> Setzen Sie der
                        Schutzschirmrahmen in die Klammerhalter ein. Dies sollte man am besten der Reihe nach
                        ausführen. Zuerst die untere Schutzschirmseitenkante, danach die obere Schutzschirmkante.
                        Die untere Schutzschirmkante soll auf dem Beschlag ohne Fixierung in den Klammerhaltern liegen.
                        (Abb. 2)</p>
                        <p><strong>SCHUTZSCHIRMDEMONTAGE</strong></p>
                        <p style="text-align:justify;"><span style="margin-right:20px;"></span><strong>1.</strong> Schleife im oberen Schutzschirmteil ergreifen. <strong>2.</strong> Den
                        Schutzschirmrand ein klein wenig in Richtung Mitte so ziehen, dass man ihn aus dem
                        Klammerhalter herausziehen kann.  <strong>3.</strong> Den Schutzschirm aus dem Klammerhalterbereich
                        herausziehen.  <strong>4.</strong> Den Schutzschirm aus anderen Klammerhaltern herausziehen.</p>'
                    data-cn='<p><strong>夹持器安装</strong></p>
                        <p style="text-align:justify;">在安装夹子和保护屏之前，确保你已经收到了一套你的车。 如果屏幕与窗口的轮廓相似，
                        请按照安装图（图1）和相应固定类型<strong>（A，B，C）</strong>的说明放下玻璃并安装夹持器。</p>
                        <p style="text-align:justify;"><strong>固定类型  A</strong>  <strong>1.</strong> 在夹持器安装位置的门窗框上松开橡胶密封条; <strong>2.</strong> 固定夹子固定器;
                        <strong>3.</strong> 将橡胶密封条放回到原来的位置; <strong>4.</strong> 根据夹子安装图，在夹子固定器的其余位置重复步骤 1-3。</p>
                        <p style="text-align:justify;"><strong>固定类型  B</strong> <strong>1.</strong> 在夹架安装位置取出位于窗框内的橡胶密封条;
                        <strong>2.</strong> 将夹子固定在窗框上; <strong>3.</strong> 将橡胶密封条安装到原来的位置; <strong>4.</strong> 根据夹子安装图，在夹子固定器的其余位置重复步骤1-3。</p>
                        <p style="text-align:justify;"><strong>固定类型 C</strong> <strong>1.</strong> 用特殊的挂钩钩住塑料盖的边缘，并单击卡夹;
                        <strong>2.</strong> 将夹子固定在塑料盖的弯曲边缘; <strong>3.</strong> 将夹子固定器的弯曲部分放到塑料盖子的另一边，然后单击夹子固定器，如果需要，
                        请使用专用挂钩。<strong>4.</strong> 重复步骤1-3，根据夹子安装图，安装夹具。</p>
                        <p><strong>保护性屏幕安装到夹持器中</strong></p>
                        <p style="text-align:justify;"><strong>1.</strong> 升起玻璃。 <strong>2.</strong> 将保护屏幕的框架插入夹子固定器中，一次：首先是屏幕的侧边缘，
                        然后是上边缘。 保护屏的下边缘必须位于盖子上。（图2）</p>
                        <p><strong>保护屏幕解散</strong></p>
                        <p style="text-align:justify;"><strong>1.</strong> 抓住屏幕上部的循环。 <strong>2.</strong> 轻轻地将屏幕边缘向中心拉，以便将其从夹子夹中取出。
                        <strong>3.</strong> 从保持架的保持区域取下保护屏。 <strong>4.</strong> 从保持架的其余部分取下保护罩。</p>'
                >Вариант 1</option>
            </select>
            <i class="fa fa-check button-select-ok"></i>
            <i class="fa fa-close button-select-cancel"></i>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="country-block ru">
                    <h4>RU</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[5][ru]',
                        'value' => $blocks[5]['ru'] ?? null,
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="country-block de">
                    <h4>DE</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[5][de]',
                        'value' => $blocks[5]['de'] ?? null,
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="country-block en">
                    <h4>EN</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[5][en]',
                        'value' => $blocks[5]['en'] ?? null,
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="country-block cn">
                    <h4>中文</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[5][cn]',
                        'value' => $blocks[5]['cn'] ?? null,
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content-block block6">
        <h3>Блок 6 (Контактная информация)</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="country-block ru">
                    <h4>RU</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[6][ru]',
                        'value' => $blocks[6]['ru'] ?? ' 
                            <p style="text-align: right;"><strong>КОНТАКТЫ:</strong></p> 
                            <p style="text-align: right;"><strong>8 800 555 01 20</strong> (звонок бесплатный) E-mail: 
                            <strong>info@laitovo.ru</strong></p>
                            <p style="text-align: right;">Изготовлено по заказу <strong>Laitovo Manufactory Trading & Service 
                            GmbH</strong></p>',
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="country-block de">
                    <h4>DE</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[6][de]',
                        'value' => $blocks[6]['de'] ?? '<p style="text-align: right;"><strong>KONTAKT</strong></p>
                            <p style="text-align: right;">Bei allen Fragen melden Sie sich 
                            bitte per E-mail: <strong>info@laitovo.de</strong></p>
                            <p style="text-align: right;">oder telefonisch unter 
                            <strong>+49 (0) 40-95063310  +49 (0) 1639459907</strong></p>',
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="country-block en">
                    <h4>EN</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => 'blocks[6][en]',
                        'value' => $blocks[6]['en'] ?? '<p style="text-align: right;"><strong>CONTACTS</strong></p>
                            <p style="text-align: right;">Should you have any questions 
                            feel free to contact us  under:</p>
                            <p style="text-align: right;">Tel: <strong>+44 20 3695 1976 (UK)</strong>
                            Email: <strong>info@laitovo.eu</strong></p>',
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="country-block cn">
                    <h4>中文</h4>
                    <?= \vova07\imperavi\Widget::widget([
                        'name' => $blocks[6]['cn'] ?? 'blocks[6][cn]',
                        'value' => '<p style="text-align: right;"><strong>联系</strong></p>
                            <p style="text-align: right;">如有任何问题，请随时与我们联系：
                            电话：<strong>+44 20 3695 1976</strong> 电子邮箱：<strong>info@laitovo.cn</strong></p>',
                        'settings' => [
                            'minHeight' => 200,
                            'lang' => 'ru',
                            'buttons' => ['html', 'bold', 'italic', 'underline', 'deleted', 'outdent', 'indent', 'alignment', 'horizontalrule'],
                            'plugins' => [
                                'fontcolor',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="text-center">
    <?= Html::button(Yii::t('app', 'Просмотр / Печать'), ['class' => 'btn btn-info print-button']) ?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
</div>
<?= Html::endForm();?>

<div id="output-template" class="hide">
    <div class="block1"></div>
    <div class="block2"></div>
    <table width="820">
        <tr>
            <td valign="top" width="50%">
                <div class="ru">
                    <div class="block3"></div>
                    <div class="block4"></div>
                    <div class="block5"></div>
                    <div class="block6"></div>
                </div>
            </td>
            <td valign="top">
                <div class="de">
                    <div class="block3"></div>
                    <div class="block4"></div>
                    <div class="block5"></div>
                    <div class="block6"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top" width="50%">
                <div class="en">
                    <div class="block3"></div>
                    <div class="block4"></div>
                    <div class="block5"></div>
                    <div class="block6"></div>
                </div>
            </td>
            <td valign="top">
                <div class="cn">
                    <div class="block3"></div>
                    <div class="block4"></div>
                    <div class="block5"></div>
                    <div class="block6"></div>
                </div>
            </td>
        </tr>
    </table>
</div>
<div id="output" class="hide"></div>

<?php Yii::$app->view->registerJs('
    $(document).ready(function() {
        document.getElementById("file").addEventListener("change", handleUploadedImage, false);
        if ($("#image").attr("src")) {
            document.querySelector("#output-template > .block2").innerHTML = "<img src=\'" + $("#image").attr("src") + "\'  style=\'width:100%\' />";
        }
    });
    
    $(".selection-block .button-select-variant").click(function() {
        $(this).closest(".selection-block").removeClass("closed");
    });
    
    $(".selection-block .button-select-cancel").click(function() {
        $(this).closest(".selection-block").addClass("closed");
    });
    
    $(".selection-block .button-select-ok").click(function() {
        var ru = $(this).closest(".selection-block").find("select option:selected").data("ru");
        var en = $(this).closest(".selection-block").find("select option:selected").data("en");
        var de = $(this).closest(".selection-block").find("select option:selected").data("de");
        var cn = $(this).closest(".selection-block").find("select option:selected").data("cn");
        
        $(this).closest(".content-block").find(".country-block.ru textarea").redactor("code.set", ru);
        $(this).closest(".content-block").find(".country-block.en textarea").redactor("code.set", en);
        $(this).closest(".content-block").find(".country-block.de textarea").redactor("code.set", de);
        $(this).closest(".content-block").find(".country-block.cn textarea").redactor("code.set", cn);

        $(this).closest(".selection-block").addClass("closed");
    });
    
    $(".print-button").click(function() {
        $("#output").html($("#output-template").html());

        $("#output .block1").html($("#content .block1 textarea").val());
                
        $("#output .ru .block3").html($("#content .block3 .ru textarea").val());
        $("#output .de .block3").html($("#content .block3 .de textarea").val());
        $("#output .en .block3").html($("#content .block3 .en textarea").val());
        $("#output .cn .block3").html($("#content .block3 .cn textarea").val());

        $("#output .ru .block4").html($("#content .block4 .ru textarea").val());
        $("#output .de .block4").html($("#content .block4 .de textarea").val());
        $("#output .en .block4").html($("#content .block4 .en textarea").val());
        $("#output .cn .block4").html($("#content .block4 .cn textarea").val());

        $("#output .ru .block5").html($("#content .block5 .ru textarea").val());
        $("#output .de .block5").html($("#content .block5 .de textarea").val());
        $("#output .en .block5").html($("#content .block5 .en textarea").val());
        $("#output .cn .block5").html($("#content .block5 .cn textarea").val());
        
        $("#output .ru .block6").html($("#content .block6 .ru textarea").val());
        $("#output .de .block6").html($("#content .block6 .de textarea").val());
        $("#output .en .block6").html($("#content .block6 .en textarea").val());
        $("#output .cn .block6").html($("#content .block6 .cn textarea").val());
        
        prepareOutputForPrint();
        printHtml($("#output").html());
    });
    
    function prepareOutputForPrint() {
        $("#output p").css({
            "margin-top": "0",
            "margin-bottom": "0",
            "font-family": "Calibri, sans-serif",
            "font-size": "10px",
            "line-height": "0.8em"
        });
    }
   
    function handleUploadedImage(e) {
        var files = e.target.files;
        f = files[0];
        if (!f || !f.type.match(\'image.*\')) {
            document.querySelector("#output-template > .block2").innerHTML = "";
            $("#image").attr("src", "").addClass("hide");
            $("#file").val("");
            return;
        }
        var fr = new FileReader();
        fr.onload = (function(theFile) {
            return function(e) {
                document.querySelector("#output-template > .block2").innerHTML = "<img src=\'" + e.target.result + "\'  style=\'width:100%\' />";
                $("#image").attr("src", e.target.result).removeClass("hide");
            };
        })(f);
        fr.readAsDataURL(f);
    }
    
    //Функция выводит на печать html-контент
    function printHtml(html) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        $("#print-content").html(\'<iframe id="iframe-print-content" src="" style="display:none;"></iframe>\');
        var iframe = document.getElementById("iframe-print-content");
        var frameDoc = iframe.document;
        if (iframe.contentWindow) {
            frameDoc = iframe.contentWindow.document;
        }
        frameDoc.open();
        frameDoc.writeln(html);
        frameDoc.close();
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>
