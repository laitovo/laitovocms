<?php

namespace backend\modules\laitovo\modules\instruction\controllers;

use yii\web\Controller;
use Yii;
use yii\web\Response;
use yii\base\Module;

class AjaxController extends Controller
{
    private $_customCarInstructionManager;

    public function __construct($id, Module $module, array $config = [])
    {
        $this->_customCarInstructionManager = Yii::$container->get('core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager');

        parent::__construct($id, $module, $config);
    }

    public function actionSaveIsUsed($id, $isUsed)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $instruction = $this->_customCarInstructionManager->findById($id);
        if (!$instruction) {
            return [
                'status' => false,
                'message' => 'Индивидуальная инструкция не найдена'
            ];
        }
        $this->_customCarInstructionManager->setIsUsed($instruction, (bool)$isUsed);
        if (!$this->_customCarInstructionManager->save($instruction)) {
            return [
                'status' => false,
                'message' => 'Не удалось сохранить индивидуальную инструкцию' . (bool)$isUsed
            ];
        }

        return [
            'status' => true,
            'message' => 'Успешно выполнено',
            'isUsed' => $this->_customCarInstructionManager->getIsUsed($instruction)
        ];
    }
}
