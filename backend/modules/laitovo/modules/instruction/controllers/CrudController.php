<?php

namespace backend\modules\laitovo\modules\instruction\controllers;

use backend\modules\laitovo\modules\instruction\models\crud\Cms;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\base\Module;

class CrudController extends Controller
{
    private $_cms;

    public function __construct($id, Module $module, array $config = [])
    {
        $this->_cms = new Cms();

        parent::__construct($id, $module, $config);
    }

    public function actionCms($carId, $instructionTypeId)
    {
        if (!($car = $this->_cms->findCar($carId))) {
            throw new NotFoundHttpException('Автомобиль не найден');
        }
        if (!($instructionType = $this->_cms->findInstructionType($instructionTypeId))) {
            throw new NotFoundHttpException('Вид инструкций не найден');
        }

        return $this->render('cms', $this->_cms->getData($car, $instructionType));
    }

    public function actionSave($carId, $instructionTypeId)
    {
        if (!($car = $this->_cms->findCar($carId))) {
            throw new NotFoundHttpException('Автомобиль не найден');
        }
        if (!($instructionType = $this->_cms->findInstructionType($instructionTypeId))) {
            throw new NotFoundHttpException('Вид инструкций не найден');
        }
        $result = $this->_cms->createOrUpdate($car, $instructionType);

        if (!$result['status']) {
            Yii::$app->session->setFlash('error', $result['message']);
            return $this->redirect(['cms', 'carId' => $carId, 'instructionTypeId' => $instructionTypeId]);
        } else {
            Yii::$app->session->setFlash('success', $result['message']);
            return $this->redirect(['/laitovo/cars/view', 'id' => $carId]);
        }
    }
}
