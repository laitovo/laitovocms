<?php

namespace backend\modules\laitovo\modules\instruction\controllers;

use backend\modules\laitovo\modules\instruction\models\printing\PrintAny;
use backend\modules\laitovo\modules\instruction\models\printing\PrintCustom;
use backend\modules\laitovo\modules\instruction\models\printing\PrintStandard;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\Module;

class PrintingController extends Controller
{
    private $_printAny;
    private $_printCustom;
    private $_printStandard;

    public function __construct($id, Module $module, array $config = [])
    {
        $this->_printAny      = new PrintAny();
        $this->_printCustom   = new PrintCustom();
        $this->_printStandard = new PrintStandard();

        parent::__construct($id, $module, $config);
    }

    public function actionPrintAny($carId, $instructionTypeId)
    {
        if ($this->_printAny->isCustom($carId, $instructionTypeId)) {
            return $this->redirect(['print-custom', 'instructionId' => $this->_printAny->getInstructionId($carId, $instructionTypeId)]);
        } else {
            return $this->redirect(['print-standard', 'carId' => $carId, 'instructionTypeId' => $instructionTypeId]);
        }
    }

    public function actionPrintAnyEu($carId, $instructionTypeId)
    {
        if ($this->_printAny->isCustom($carId, $instructionTypeId)) {
            return $this->redirect(['print-custom', 'instructionId' => $this->_printAny->getInstructionId($carId, $instructionTypeId)]);
        } else {
            return $this->redirect(['print-standard-eu', 'carId' => $carId, 'instructionTypeId' => $instructionTypeId]);
        }
    }

    public function actionPrintCustom($instructionId)
    {
        if (!($instruction = $this->_printCustom->findInstruction($instructionId))) {
            throw new NotFoundHttpException('Индивидуальная инструкция не найдена');
        }

        return $this->renderPartial('custom/print', [
            'blocks'   => $this->_printCustom->getBlocks($instruction),
            'imageUrl' => $this->_printCustom->getImageUrl($instruction),
        ]);
    }

    public function actionPrintStandard($carId, $instructionTypeId)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        if (!($car = $this->_printStandard->findCar($carId))) {
            throw new NotFoundHttpException('Автомобиль не найден');
        }
        if (!($instructionType = $this->_printStandard->findInstructionType($instructionTypeId))) {
            throw new NotFoundHttpException('Вид инструкций не найден');
        }

        return $this->redirect(['/laitovo/universal-instruction/default/print','article' =>$this->_printStandard->getArticle($car, $instructionType),'lang' => 'ru']);
//        return $this->renderPartial($this->_printStandard->getViewNew(),[
//            'article' => $this->_printStandard->getArticle($car, $instructionType),
//        ]);
    }

    public function actionPrintStandardEu($carId, $instructionTypeId)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        if (!($car = $this->_printStandard->findCar($carId))) {
            throw new NotFoundHttpException('Автомобиль не найден');
        }
        if (!($instructionType = $this->_printStandard->findInstructionType($instructionTypeId))) {
            throw new NotFoundHttpException('Вид инструкций не найден');
        }

        return $this->redirect(['/laitovo/universal-instruction/default/print','article' =>$this->_printStandard->getArticle($car, $instructionType),'lang' => 'eu']);
//        return $this->renderPartial($this->_printStandard->getViewNew(),[
//            'article' => $this->_printStandard->getArticle($car, $instructionType),
//        ]);
    }
}
