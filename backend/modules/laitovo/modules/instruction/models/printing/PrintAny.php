<?php

namespace backend\modules\laitovo\modules\instruction\models\printing;

use Yii;

class PrintAny
{
    private $_customCarInstructionManager;

    public function __construct()
    {
        $this->_customCarInstructionManager = Yii::$container->get('core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager');
    }

    public function isCustom($carId, $instructionTypeId)
    {
        if (!($instruction = $this->_findInstruction($carId, $instructionTypeId))) {
            return false;
        }
        if (!$this->_isUsed($instruction)) {
            return false;
        }

        return true;
    }

    public function getInstructionId($carId, $instructionTypeId)
    {
        if (!($instruction = $this->_findInstruction($carId, $instructionTypeId))) {
            return null;
        }

        return $this->_getId($instruction);
    }

    private function _findInstruction($carId, $instructionTypeId)
    {
        return $this->_customCarInstructionManager->findBy(['carId' => $carId, 'instructionTypeId' => $instructionTypeId])[0] ?? null;
    }

    private function _isUsed($instruction)
    {
        return $this->_customCarInstructionManager->getIsUsed($instruction);
    }

    private function _getId($instruction)
    {
        return $this->_customCarInstructionManager->getId($instruction);
    }
}


