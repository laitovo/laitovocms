<?php

namespace backend\modules\laitovo\modules\instruction\models\printing;

use Yii;
use common\models\laitovo\Cars;
use backend\helpers\ArticleHelper;

class PrintStandard
{
    private $_instructionTypeManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
    }

    public function findCar($carId)
    {
        return Cars::findOne($carId);
    }

    public function findInstructionType($instructionTypeId)
    {
        return $this->_instructionTypeManager->findById($instructionTypeId);
    }

    public function getArticle($car, $instructionType)
    {
        $articleMask = $this->_instructionTypeManager->getArticleMask($instructionType);
        $article = preg_replace('/%-%/', $this->_getArticleMark($car) . '-' . $car->article, $articleMask);

        return $article;
    }

    public function getView($car, $instructionType)
    {
        $article = $this->getArticle($car, $instructionType);
        if (ArticleHelper::getChikoInMagnet($article)) {
            return 'standard/print-manual-chiko-in-magnet';
        }elseif (ArticleHelper::laitovoMagnet($article)) {
            return 'standard/print-manual-laitovo-magnet';
        }elseif (ArticleHelper::chikoMagnet($article)) {
            return 'standard/print-manual-chiko-magnet';
        }elseif (ArticleHelper::laitovoInMagnet($article)) {
            return 'standard/print-manual-laitovo-in-magnet';
        }elseif (ArticleHelper::mosquitoNet($article)) {
            return 'standard/print-manual-mosquito-net';
        }elseif (ArticleHelper::vizorMagnet($article) && ArticleHelper::getBrand($article)=='Chiko') {
            return 'standard/print-manual-chiko-visor-magnet';
        }elseif (ArticleHelper::vizorInMagnet($article) && ArticleHelper::getBrand($article)=='Chiko') {
            return 'standard/print-manual-chiko-vizor-in-magnet';
        }elseif (ArticleHelper::vizorMagnet($article) && ArticleHelper::getBrand($article)=='Laitovo') {
            return 'standard/print-manual-laitovo-vizor-magnet';
        }elseif (ArticleHelper::vizorInMagnet($article) && ArticleHelper::getBrand($article)=='Laitovo') {
            return 'standard/print-manual-laitovo-vizor-in-magnet';
        }

        return 'standard/print-manual-1';
    }

    public function getViewEu($car, $instructionType)
    {
        $article = $this->getArticle($car, $instructionType);
        if (ArticleHelper::getChikoInMagnet($article)) {
            return 'standard/print-manual-eu-chiko-in-magnet';
        }elseif (ArticleHelper::laitovoInMagnet($article)) {
            return 'standard/print-manual-eu-laitovo-in-magnet';
        }elseif (ArticleHelper::getBrand($article) == 'Chiko') {
            return 'standard/print-manual-eu-chiko';
        }

        return 'standard/print-manual-eu';
    }

    public function getViewNew()
    {
        return 'standard/universal/print-manual-universal';
    }

    private function _getArticleMark($car)
    {
        return Cars::getTranslit(mb_substr($car->mark, 0, 1));
    }
}


