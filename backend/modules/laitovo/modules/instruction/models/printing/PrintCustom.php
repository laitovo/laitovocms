<?php

namespace backend\modules\laitovo\modules\instruction\models\printing;

use Yii;

class PrintCustom
{
    private $_customCarInstructionManager;

    public function __construct()
    {
        $this->_customCarInstructionManager = Yii::$container->get('core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager');
    }

    public function findInstruction($instructionId)
    {
        return $this->_customCarInstructionManager->findById($instructionId);
    }

    public function getBlocks($instruction)
    {
        return json_decode($this->_customCarInstructionManager->getContentBlocks($instruction), true);
    }

    public function getImageUrl($instruction)
    {
        $blocks = $this->getBlocks($instruction);

        return Yii::$app->fileManager->getFileUrlForView($blocks[2] ?? null);
    }
}


