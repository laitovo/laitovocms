<?php

namespace backend\modules\laitovo\modules\instruction\models\crud;

use backend\helpers\ArticleHelper;
use Yii;
use common\models\laitovo\Cars;
use yii\helpers\ArrayHelper;
use \yii\web\UploadedFile;
use common\models\laitovo\CarsAnalog;

class Cms
{
    private $_customCarInstructionManager;
    private $_instructionTypeManager;
    private $_windowOpeningTypeManager;
    private $_brandManager;
    private $_executionTypeManager;

    public function __construct()
    {
        $this->_customCarInstructionManager = Yii::$container->get('core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager');
        $this->_instructionTypeManager      = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_windowOpeningTypeManager    = Yii::$container->get('core\entities\propWindowOpeningType\WindowOpeningTypeManager');
        $this->_brandManager                = Yii::$container->get('core\entities\propBrand\BrandManager');
        $this->_executionTypeManager        = Yii::$container->get('core\entities\propExecutionType\ExecutionTypeManager');
    }

    public function findCar($carId)
    {
        return Cars::findOne($carId);
    }

    public function findInstructionType($instructionTypeId)
    {
        return $this->_instructionTypeManager->findById($instructionTypeId);
    }

    public function getData($car, $instructionType)
    {
        $carId = $car->id;
        $instructionTypeId = $this->_instructionTypeManager->getId($instructionType);
        $instruction = $this->_customCarInstructionManager->findBy(['carId' => $carId, 'instructionTypeId' => $instructionTypeId])[0] ?? null;
        $isNew = $instruction == null;
        $blocks = $instruction ? json_decode($this->_customCarInstructionManager->getContentBlocks($instruction), true) : null;
        $imageUrl = $instruction ? Yii::$app->fileManager->getFileUrlForView($blocks[2]) : null;
        $window = $this->_instructionTypeManager->getRelatedWindowOpeningType($instructionType);
        $windowName = $this->_windowOpeningTypeManager->getName($window);
        $windowAlias = $this->_windowOpeningTypeManager->getInstructionAlias($window);
        $article = $this->_getArticle($car, $instructionType);
        $windowsCount = ArticleHelper::getCountWindow($article);
        $clips = ArticleHelper::getClips($article);
        $analogsInfo = $this->_getAnalogsInfo($carId, $windowName);
        $instructionTypeInfo = $this->_getInstructionTypeInfo($instructionType);
        $carName = $car->fullEnName;

        return [
            'isNew' => $isNew,
            'carId' => $carId,
            'instructionTypeId' => $instructionTypeId,
            'carName' => $carName,
            'instructionTypeInfo' => $instructionTypeInfo,
            'windowAlias' => $windowAlias,
            'blocks' => $blocks,
            'imageUrl' => $imageUrl,
            'windowsCount' => $windowsCount,
            'clips' => $clips,
            'analogsInfo' => $analogsInfo
        ];
    }

    public function createOrUpdate($car, $instructionType)
    {
        $carId = $car->id;
        $instructionTypeId = $this->_instructionTypeManager->getId($instructionType);
        $instruction = $this->_customCarInstructionManager->findBy(['carId' => $carId, 'instructionTypeId' => $instructionTypeId])[0] ?? null;
        if (!$instruction) {
            $instruction = $this->_customCarInstructionManager->create();
            $this->_customCarInstructionManager->setCarId($instruction, $carId);
            $this->_customCarInstructionManager->setInstructionTypeId($instruction, $instructionTypeId);
            $this->_customCarInstructionManager->setCarArticle($instruction, $car->article);
            $this->_customCarInstructionManager->setIsUsed($instruction, false);
        }
        $oldImageFileKey = json_decode($this->_customCarInstructionManager->getContentBlocks($instruction), true)[2] ?? null;
        if (!UploadedFile::getInstanceByName('image')) {
            $imageFileKey = $oldImageFileKey;
        } else {
            $imageFileKey = Yii::$app->fileManager->uploadTeamFile('image', 'instruction/images');
            if ($imageFileKey) {
                Yii::$app->fileManager->deleteFile($oldImageFileKey);
            }
        }
        if (!$imageFileKey) {
            return [
                'status' => false,
                'message' => Yii::t('app', 'В инструкции должна быть картинка')
            ];
        }
        $blocks = Yii::$app->request->post('blocks');
        $blocks[2] = $imageFileKey;
        $this->_customCarInstructionManager->setContentBlocks($instruction, json_encode($blocks));
        if (!$this->_customCarInstructionManager->save($instruction)) {
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось сохранить инструкцию')
            ];
        }

        return [
            'status' => true,
            'message' => Yii::t('app', 'Инструкция успешно сохранена')
        ];
    }

    private function _getAnalogsInfo($carId, $windowName)
    {
        $cars = CarsAnalog::find()
            ->where(['and',
                ['car_id' => $carId],
                ['like','json','%'.$windowName.'%',false]
            ])->all();
        $array = ArrayHelper::map($cars,'analog_id','analog_id');
        $cars = Cars::find()->where(['in','id',$array])->all();
        $carNames = [];
        foreach ($cars as $car) {
            $carNames[] = $car->fullEnName;
        }
        if (empty($carNames)) {
            return null;
        }

        return '(' . implode(', ', $carNames) . ')';
    }

    private function _getInstructionTypeInfo($instructionType)
    {
        return implode (' ', [
            $this->_windowOpeningTypeManager->getName($this->_instructionTypeManager->getRelatedWindowOpeningType($instructionType)),
            $this->_brandManager->getName($this->_instructionTypeManager->getRelatedBrand($instructionType)),
            $this->_executionTypeManager->getName($this->_instructionTypeManager->getRelatedExecutionType($instructionType)),
            $this->_instructionTypeManager->getFixationTypeLabel($instructionType)
        ]);
    }

    private function _getArticle($car, $instructionType)
    {
        $articleMask = $this->_instructionTypeManager->getArticleMask($instructionType);
        $article = preg_replace('/%-%/', $this->_getArticleMark($car) . '-' . $car->article, $articleMask);

        return $article;
    }

    private function _getArticleMark($car)
    {
        return Cars::getTranslit(mb_substr($car->mark, 0, 1));
    }
}


