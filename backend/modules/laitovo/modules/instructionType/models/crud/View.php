<?php

namespace backend\modules\laitovo\modules\instructionType\models\crud;

use Yii;

class View
{
    private $_instructionTypeManager;
    private $_dtoProvider;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_dtoProvider = new DtoProvider();
    }

    public function findModel($id)
    {
        $model = $this->_instructionTypeManager->findById($id);

        return $this->_prepareModel($model);
    }

    public function getAttributesWithLabels()
    {
        return $this->_dtoProvider->getAttributesWithLabels();
    }

    private function _prepareModel($model)
    {
        return $this->_dtoProvider->prepareModel($model);
    }
}


