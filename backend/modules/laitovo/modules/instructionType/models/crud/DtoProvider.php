<?php

namespace backend\modules\laitovo\modules\instructionType\models\crud;

use Yii;

class DtoProvider
{
    private $_instructionTypeManager;
    private $_windowOpeningTypeManager;
    private $_brandManager;
    private $_executionTypeManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_windowOpeningTypeManager = Yii::$container->get('core\entities\propWindowOpeningType\WindowOpeningTypeManager');
        $this->_brandManager = Yii::$container->get('core\entities\propBrand\BrandManager');
        $this->_executionTypeManager = Yii::$container->get('core\entities\propExecutionType\ExecutionTypeManager');
    }

    public function getAttributes()
    {
        return [
            'id',
            'windowOpeningType',
            'brand',
            'executionType',
            'fixationType',
            'articleMask',
            'articlePattern',
            'carsFormStatus',
        ];
    }

    public function getAttributesWithLabels()
    {
        return [
            ['attribute' => 'id', 'label' => 'ID'],
            ['attribute' => 'windowOpeningType', 'label' => 'Вид оконного проёма'],
            ['attribute' => 'brand', 'label' => 'Бренд'],
            ['attribute' => 'executionType', 'label' => 'Вид исполнения'],
            ['attribute' => 'fixationType', 'label' => 'Тип крепления'],
            ['attribute' => 'articleMask', 'label' => 'Маска артикула'],
            ['attribute' => 'articlePattern', 'label' => 'Паттерн артикула'],
            ['attribute' => 'carsFormStatus', 'label' => 'Зависимый статус'],
        ];
    }

    public function prepareModel($model)
    {
        if (!($preparedModel = $this->prepareModelAsArray($model))) {
            return null;
        }

        return (object)$preparedModel;
    }

    public function prepareModelAsArray($model)
    {
        if (!$model) {
            return null;
        }
        $windowOpeningType = $this->_instructionTypeManager->getRelatedWindowOpeningType($model);
        $brand = $this->_instructionTypeManager->getRelatedBrand($model);
        $executionType = $this->_instructionTypeManager->getRelatedExecutionType($model);
        $preparedModel['id'] = $this->_instructionTypeManager->getId($model);
        $preparedModel['windowOpeningType'] = $windowOpeningType ? $this->_windowOpeningTypeManager->getName($windowOpeningType) : null;
        $preparedModel['brand'] = $brand ? $this->_brandManager->getName($brand) : null;
        $preparedModel['executionType'] = $executionType ? $this->_executionTypeManager->getName($executionType) : null;
        $preparedModel['fixationType'] = $this->_instructionTypeManager->getFixationTypeLabel($model);
        $preparedModel['articleMask'] = $this->_instructionTypeManager->getArticleMask($model);
        $preparedModel['articlePattern'] = $this->_instructionTypeManager->getArticlePattern($model);
        $preparedModel['carsFormStatus'] = $this->_instructionTypeManager->getCarsFormStatus($model);

        return $preparedModel;
    }
}


