<?php

namespace backend\modules\laitovo\modules\instructionType\models\crud;

use yii\data\ArrayDataProvider;
use Yii;

class Index
{
    private $_instructionTypeManager;
    private $_dtoProvider;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_dtoProvider = new DtoProvider();
    }

    public function getProvider()
    {
        return new ArrayDataProvider([
            'key'       => $this->_instructionTypeManager->getPrimaryKey(),
            'allModels' => $this->_prepareModels($this->_instructionTypeManager->findAll()),
            'sort'      => [
                'attributes' => $this->getAttributes(),
            ],
        ]);
    }

    public function getAttributes()
    {
        return $this->_dtoProvider->getAttributes();
    }

    public function getAttributesWithLabels()
    {
        return $this->_dtoProvider->getAttributesWithLabels();
    }

    private function _prepareModels($models)
    {
        $rows = [];
        foreach ($models as $model) {
            $rows[] = $this->_prepareModel($model);
        }

        return $rows;
    }

    private function _prepareModel($model)
    {
        return $this->_dtoProvider->prepareModelAsArray($model);
    }
}


