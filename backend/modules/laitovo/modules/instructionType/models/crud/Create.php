<?php

namespace backend\modules\laitovo\modules\instructionType\models\crud;

use Yii;

class Create
{
    private $_instructionTypeManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
    }

    public function createModel()
    {
        return $this->_instructionTypeManager->create();
    }

    public function save($model, $params)
    {
        if (!$this->_instructionTypeManager->fillByForm($model, $params) || !$this->_instructionTypeManager->save($model)) {
            return false;
        }

        return $this->_instructionTypeManager->getId($model);
    }
}


