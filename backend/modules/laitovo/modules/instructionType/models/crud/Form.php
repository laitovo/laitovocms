<?php

namespace backend\modules\laitovo\modules\instructionType\models\crud;

use Yii;

class Form
{
    private $_instructionTypeManager;
    private $_windowOpeningTypeService;
    private $_brandService;
    private $_executionTypeService;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_windowOpeningTypeService = Yii::$container->get('core\entities\propWindowOpeningType\WindowOpeningTypeManager');
        $this->_brandService = Yii::$container->get('core\entities\propBrand\BrandManager');
        $this->_executionTypeService = Yii::$container->get('core\entities\propExecutionType\ExecutionTypeManager');
    }

    public function getData()
    {
        return (object)[
            'windowOpeningTypes' => $this->_windowOpeningTypeService->getNameListIndexById(),
            'brands'             => $this->_brandService->getNameListIndexById(),
            'executionTypes'     => $this->_executionTypeService->getNameListIndexById(),
            'fixationTypes'      => $this->_instructionTypeManager->getFixationTypeVariantsWithLabels(),
        ];
    }
}


