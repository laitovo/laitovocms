<?php

namespace backend\modules\laitovo\modules\instructionType\models\crud;

use Yii;

class Update
{
    private $_instructionTypeManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
    }

    public function findModel($id)
    {
        return $this->_instructionTypeManager->findById($id);
    }

    public function save($model, $params)
    {
        if (!$this->_instructionTypeManager->fillByForm($model, $params) || !$this->_instructionTypeManager->save($model)) {
            return false;
        }

        return $this->_instructionTypeManager->getId($model);
    }
}


