<?php

namespace backend\modules\laitovo\modules\instructionType\models\crud;

use Yii;

class Delete
{
    private $_instructionTypeManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
    }

    public function findModel($id)
    {
        return $this->_instructionTypeManager->findById($id);
    }

    public function deleteModel($model)
    {
        return $this->_instructionTypeManager->delete($model);
    }
}


