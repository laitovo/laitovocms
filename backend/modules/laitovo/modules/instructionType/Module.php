<?php

namespace backend\modules\laitovo\modules\instructionType;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\laitovo\modules\instructionType\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        \Yii::$app->layout='2columns';
        parent::init();

        // custom initialization code goes here
        $this->modules = [

        ];
    }
}
