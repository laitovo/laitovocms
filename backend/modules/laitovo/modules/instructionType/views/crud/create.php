<?php

/* @var yii\web\View $this */
/* @var mixed $model */
/* @var object $formData */

$this->render('@backendViews/laitovo/views/menu');

$this->title = Yii::t('app', 'Добавить вид инструкций');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['/laitovo/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Виды инструкций'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'formData' => $formData,
    'isNew' => true,
]) ?>
    