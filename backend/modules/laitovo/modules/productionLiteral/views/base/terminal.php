<?php

use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;

use yii\helpers\Url;

$this->render('@backendViews/laitovo/views/menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    $("#erp-terminal-form").on("ajaxComplete", function (event, messages) {
        document.location.reload();
    });

    setInterval(function(){ $(".reloadterminal").click();}, 60*1000);

    var barcode=$(\'#erpterminal-barcode\');

    function searchterminal(e)
    {
        $.get("' . Url::to(['search']) . '",{
            barcode: barcode.val()
        },function(data){

            if (data.status=="success"){
                notie.alert(1,data.message,15);
            } else if(data.status=="warning"){
                notie.alert(2,data.message,15);
            } else {
                notie.alert(3,data.message,15);
            }

        },"json");

        e.preventDefault();
    }
    
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erpterminal-barcode" && e.which == 13){
                searchterminal(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchterminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });

    ', \yii\web\View::POS_END);


?>
<h3>Терминал переноса производственных литер</h3>
<br>
<div class="row">
    <div class="col-xs-4">
        <input type="text" id="erpterminal-barcode" placeholder="Поиск..." class="form-control">
    </div>
</div>




