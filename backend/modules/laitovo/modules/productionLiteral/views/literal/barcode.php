<?php
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

/**
 * @var $literal string
 */
?>
<h1 style="font-weight: bold; font-size: 12em; text-align: center; margin-bottom: 0"><?=$literal?></h1>

<div style="padding-left: 48%"><?= $generator->getBarcode($literal, $generator::TYPE_CODE_128, 1) ?></div>
