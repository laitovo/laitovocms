<?php

namespace backend\modules\laitovo\modules\productionLiteral\controllers;

use core\services\SProductionLiteral;
use yii\web\Controller;

/**
 * Class BaseController
 * @package backend\modules\laitovo\modules\productionLiteral\controllers
 */
class LiteralController extends Controller
{
    /**
     * @return string
     */
    public function actionBarcodePrint(): string
    {
        $content = '';
        $service = new SProductionLiteral();
        $literals = $service->generateLiteralList();
        $count = 0;
        foreach ($literals as $literal) {
           $count++;
           $content .= $this->renderPartial('barcode', ['literal' => $literal]);
           if ($count % 2 == 0) {
               $content .= '<div style = "page-break-before:always;"></div>';
           }
        }
        return $content;
    }
}
