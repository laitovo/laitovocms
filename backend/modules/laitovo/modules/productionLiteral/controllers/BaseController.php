<?php

namespace backend\modules\laitovo\modules\productionLiteral\controllers;

use backend\modules\laitovo\models\ErpNaryad;
use core\logic\DistributionProductionLiterals;
use yii\web\Controller;
use yii\web\Response;
use Yii;

/**
 * Class BaseController
 * @package backend\modules\laitovo\modules\productionLiteral\controllers
 */
class BaseController extends Controller
{
    /**
     * @return string
     */
    public function actionTerminal(): string
    {
        return $this->render('terminal');
    }

    /**
     * @param $barcode
     * @return array
     */
    public function actionSearch($barcode): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /**
         * @var $workOrder ErpNaryad
         */
        return (new DistributionProductionLiterals())->distribute($barcode);
    }
}
