<?php

namespace backend\modules\laitovo\models;

use backend\helpers\ArticleHelper;
use core\logic\UpnGroup;
use core\models\article\Article;
use core\services\SProductionLiteral;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;
use common\models\laitovo\Config;

/**
 * Class ErpTerminal
 * @package backend\modules\laitovo\models
 *
 * @property ErpUser $user [ работник ]
 * @property ErpLocation $location [ участок ]
 */

class ErpTerminal extends Model
{
    ############################################# Properties ###########################################################
    #########

    /**
     * @var integer|null $location_id [ уникальный идентификатор рабочего участка ( ErpLocation->id либо null ) ]
     */
    public $location_id;

    /**
     * @var integer|null $user_id [ уникальный идентификатор работника ( ErpUser->id либо null ) ]
     */
    public $user_id;

    /**
     * @var array $workOrders [ список идентификаторов нарядов, с которыми осуществляются действия ( ErpNaryad->id[] либл [] ) ]
     */
    public $workOrders = [];

    /**
     * @var integer|null $cloth Переменная, которая хранит в себе значение ткани для выдачи.
     */
    public $cloth;

    /**
     * @var integer|null $window Переменная, которая хранит в себе значение ткани для выдачи.
     */
    public $window;

    /**
     * @var array $clothValues Массив допустимых значений ткани на оклейке
     */
    public $clothValues = [];

    /**
     * @var array $windowValues Массив допустимых значений оконных проемов на оклейке
     */
    public $windowValues = [
        'FV' => 'Передняя форточка',
        'FD' => 'Переднее боковое окно',
        'RD' => 'Заднее боковое окно',
        'RV' => 'Задняя форточка',
        'BW' => 'Задняя шторка',
    ];

    public $windowValuesExisting = [];

    /**
     * @var array $equipment Массив id upn-ов, которые находятся в одной комплектации
     */
    public $equipment = [];

    /**
     * @var array $labelPrint Запоминаем наряд для распечатки этикетки
     */
    public $labelPrint;

    /**
     * @var array $doubleWorkOrderClickId Запоминаем наряд для двойного пропикивания
     */
    public $doubleWorkOrderClickId;


    /**
     * @var array $doubleAddGroupClickId Запоминаем Реестр на дополнительную продукцию для двойного пропикивания
     */
    public $doubleAddGroupClickId;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'location_id'=> Yii::t('app', 'Участок'),
            'user_id'    => Yii::t('app', 'Сотрудник'),
            'workOrders' => Yii::t('app', 'Наряды'),
        ];
    }

    #########
    ############################################# Properties ###########################################################


    ############################################# Relations ############################################################
    #########

    /**
     * @return ErpLocation|static
     */
    public function getLocation()
    {
        return $this->location_id ? ErpLocation::findOne($this->location_id) : new ErpLocation;
    }

    /**
     * @return ErpUser|static
     */
    public function getUser()
    {
        return $this->user_id ? ErpUser::findOne($this->user_id) : new ErpUser;
    }

    #########
    ############################################# Relations ############################################################


    ############################################# Validation ###########################################################
    #########

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id'], 'required', 'message'=>'{attribute} не указан'],
            [['location_id'], 'integer'],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => 'id'],
            [['user_id'], 'required', 'message'=>'{attribute} не указан'],
            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => 'id'],
            [['user_id'], 'checkWorkerLocation'],
        ];
    }

    /**
     * Проверка на соответсвие участка пользователя и участка, указанного в терминале выдачи
     *
     * @param $attribute
     */
    public function checkWorkerLocation($attribute)
    {
        if ($this->user->location_id != $this->location_id)
            $this->addError($attribute, Yii::t('app', '{attribute} не зарегистрирован на участке',['attribute'=>$this->getAttributeLabel($attribute)]));
    }

    #########
    ############################################# Validation ###########################################################


    ############################################# Functions ############################################################
    #########

    /**
     * ErpTerminal constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->location_id             = Yii::$app->request->cookies->getValue('Erp_Terminal_Location');
        $this->user_id                 = Yii::$app->request->cookies->getValue('Erp_Terminal_User');
        $this->cloth                   = Yii::$app->request->cookies->getValue('Erp_Terminal_Cloth');
        $this->window                  = Yii::$app->request->cookies->getValue('Erp_Terminal_Window');
        $this->workOrders              = Yii::$app->request->cookies->getValue('Erp_Terminal_WorkOrders')?:[];
        $this->equipment               = Yii::$app->request->cookies->getValue('Erp_Terminal_Equipment')?:[];
        $this->labelPrint              = Yii::$app->request->cookies->getValue('Erp_Terminal_LabelPr');
        $this->doubleWorkOrderClickId  = Yii::$app->request->cookies->getValue('Erp_Terminal_DoubleWorkOrder');
        $this->doubleAddGroupClickId   = Yii::$app->request->cookies->getValue('Erp_Terminal_DoubleAddGroup');

        parent::__construct($config);
    }

    /**
     * Функция для сохранения в cookies основных значений
     */
    public function saveProperties()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_Location',
            'value' => $this->location_id,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_User',
            'value' => $this->user_id,
            'expire' => time() + 30,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_WorkOrders',
            'value' => $this->workOrders,
            'expire' => time() + 30,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_Equipment',
            'value' => $this->equipment,
            'expire' => time() + 30,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_LabelPr',
            'value' => $this->labelPrint,
            'expire' => time() + 10,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_Cloth',
            'value' => $this->cloth,
            'expire' => time() + 30,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_Window',
            'value' => $this->window,
            'expire' => time() + 30,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_DoubleWorkOrder',
            'value' => $this->doubleWorkOrderClickId,
            'expire' => time() + 30,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_Terminal_DoubleAddGroup',
            'value' => $this->doubleAddGroupClickId,
            'expire' => time() + 30,
        ]));

    }

    /**
     * Функция для преобразования штрихкода из кириллицы в латиницу
     *
     * @param $str
     * @return string
     */
    public function toLatin($str) 
    {
        $tr = array(
            "Й"=>"q","Ц"=>"w","У"=>"e","К"=>"r","Е"=>"t","Н"=>"y","Г"=>"u","Ш"=>"i","Щ"=>"o","З"=>"p","Ф"=>"a","Ы"=>"s","В"=>"d","А"=>"f","П"=>"g","Р"=>"h","О"=>"j","Л"=>"k","Д"=>"l","Я"=>"z","Ч"=>"x","С"=>"c","М"=>"v","И"=>"b","Т"=>"n","Ь"=>"m",
            "й"=>"q","ц"=>"w","у"=>"e","к"=>"r","е"=>"t","н"=>"y","г"=>"u","ш"=>"i","щ"=>"o","з"=>"p","ф"=>"a","ы"=>"s","в"=>"d","а"=>"f","п"=>"g","р"=>"h","о"=>"j","л"=>"k","д"=>"l","я"=>"z","ч"=>"x","с"=>"c","м"=>"v","и"=>"b","т"=>"n","ь"=>"m"
        );
        return strtr($str,$tr);
    }


    ///%%% Основной алгоритм выдачи нарядов на участки %%%\\\

    /**
     * Задача данной функции - дать упорядоченный список идентификаторов нарядов на выдачу
     * @return array
     */
    public function algorithm()
    {
        $ids = [];

        if (!$this->validate()) return $ids;

        /**
         * Костыль на запрет выдачи нарядов через терминал с 7 00 утра до 8 20 . Введен Денисом Леонидовичем
         * 28.02.2020 По просьбе в Viber от Дениса Леонидовича изменил время на с 7 00 до 8 00
         */
//        $time = time();
//        if ($time > mktime(7,0,0) && $time < mktime(8,0,0)) return $ids;


        switch ($this->location_id) {
            case Yii::$app->params['erp_izgib']:
                $ids = $this->idsForSectionOfBending();
                return $ids;
            case Yii::$app->params['erp_okleika']:
                $ids = $this->idsForSectionOfPastingByProductionLiterals();
                return $ids;
            case Yii::$app->params['erp_shveika']:
                $ids = $this->idsForSectionOfSewingByProductionLiterals();
                return $ids;
            case Yii::$app->params['erp_clipsi']:
                $ids = $this->idsForSectionOfClipMakingByProductionalLiteral();
                return $ids;
            case Yii::$app->params['erp_otk']:
                $ids = $this->idsForSectionOfTCD();
                return $ids;
            case 18:
                $ids = $this->idsForSectionOfSewing2();
                return $ids;
            case 19:
                $ids = $this->idsForSectionOfCut();
                return $ids;
            case 22:
                $ids = $this->idsForSectionOfCut();
                return $ids;
            default:
                return $ids;
        }
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на ИЗГИБ человеку
     * @return array
     */
    public function idsForSectionOfBending()
    {
        $ids = [];
        //Проверка на наличие несданных нарядов
        if (!$this->checkWorkOrdersNeedEnd($this->user_id,$this->location_id)) return $ids;
        //Инициализируем списко лекал, на которые мы не можем выдавать наряды
        $otherTemplates = $this->getOtherTemplates($this->user_id);
//        $otherTemplates = array_merge($otherTemplates,$this->getTemplatesWaitTCD());
        //Проверка на наличие переделок (самых срочных заказов)

        //Если пользователь - стажер
        if ($this->user && $this->user->trainee) {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[7]);
            $workOrders = $this->filterByTemplates($workOrders,$otherTemplates);
            $workOrders = $this->filterByTrainee($workOrders);
            if(!empty($workOrders)) {
                //Группируем по продукту
//                $workOrders = $this->groupByProduct($workOrders);
                //Группируем по лекалу
                $workOrders = $this->groupByCar($workOrders,true);
                //Обрезать по необходимому количеству
                $workOrders = $this->sliceNaryadsByLimit($workOrders);
                $ids = ArrayHelper::map($workOrders,'id','id');

                return $ids;
            }
        }

        if (
            ($top = $this->getListOfWorkOrdersForLocation($this->location_id,[1],true))
        &&  (count(($top = $this->filterByTemplates([$top],$otherTemplates))))
        &&  (count(($top = $this->filterByTrainee($top))))
        ){
            $top = array_shift (  $top );
            return [$top->id];
        } elseif (
            ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[2,3]))
        &&  (count(($workOrders = $this->filterByTemplates($workOrders,$otherTemplates))))
        &&  (count(($workOrders = $this->filterByTrainee($workOrders))))
        ) {
            //Тело не нужно
        } elseif (
            ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[4,5]))
        &&  (count(($workOrders = $this->filterByTemplates($workOrders,$otherTemplates))))
        &&  (count(($workOrders = $this->filterByTrainee($workOrders))))
        ) {
            //Тело не нужно
        } elseif ( ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[6]))
        &&  (count(($workOrders = $this->filterByTemplates($workOrders,$otherTemplates))))
        &&  (count(($workOrders = $this->filterByTrainee($workOrders))))
        ) {
            //Тело не нужно
        } elseif ( ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[7]))
        &&  (count(($workOrders = $this->filterByTemplates($workOrders,$otherTemplates))))
        &&  (count(($workOrders = $this->filterByTrainee($workOrders))))
        ) {
            //Тело не нужно
        } else {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id);
            $workOrders = $this->filterByTemplates($workOrders,$otherTemplates);
            $workOrders = $this->filterByTrainee($workOrders);
        }
        //Группируем по продукту
//        $workOrders = $this->groupByProduct($workOrders);
        //Группируем по лекалу
        $workOrders = $this->groupByCar($workOrders,true);
        //Обрезать по необходимому количеству
        $workOrders = $this->sliceNaryadsByLimit($workOrders);
        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на ОКЛЕЙКУ человеку
     * @return array
     */
    public function idsForSectionOfPasting()
    {
        $ids = [];
        $naryads = $this->checkWorkOrdersOnUser($this->user_id,$this->location_id);
        $clothCanDo = $this->getCountWorkplacesForCloth($this->user,$naryads);
        if (!$this->checkWorkOrdersNeedEndForPasting($this->user_id,$this->location_id)) return $ids;
        //hekstodo Нужен некий ограничитель на количество выдавемых нарядов
        if (
            ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[1,2]))
            &&  (count(($workOrders = $this->filterByClothAndWorkplace($workOrders,$clothCanDo)))) ) {
            //Тело не нужно
        } elseif (
            ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[3,4,5,6,7]))
            &&  (count(($workOrders = $this->filterByClothAndWorkplace($workOrders,$clothCanDo)))) ) {
            //Тело не нужно
        } else {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id);
            $workOrders = $this->filterByClothAndWorkplace($workOrders,$clothCanDo);
        }

        $workOrders = $this->sliceNaryadsByLimit($workOrders);
        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на ОКЛЕЙКУ человеку
     * @return array
     */
    public function idsForSectionOfPastingByProductionLiterals()
    {
        $ids = [];
        $naryads = $this->checkWorkOrdersOnUser($this->user_id,$this->location_id);
        $clothCanDo = $this->getCountWorkplacesForCloth($this->user,$naryads);
        if (!$this->checkWorkOrdersNeedEndForPasting($this->user_id,$this->location_id)) return $ids;
        //hekstodo Нужен некий ограничитель на количество выдавемых нарядов

        $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id);
        $workOrders = $this->filterByClothAndWorkplace($workOrders,$clothCanDo);
        $workOrders = $this->groupByProdGroup($workOrders);

        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на ШВЕЙКУ человеку
     * @return array
     */
    public function idsForSectionOfSewingByProductionLiterals()
    {
        $ids = [];
        //Проверка на наличие несданных нарядов
        if (!$this->checkWorkOrdersNeedEnd($this->user_id,$this->location_id)) return $ids;

        //Если пользователь - стажер, то не вижу смысла выдавать ему литерой.
        //Ему выдаем в первую очередь пониженку, по лимиту, с любого места
        if ($this->user && $this->user->trainee) {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[7]);
            $workOrders = $this->filterByTrainee($workOrders);
            $workOrders = $this->filterByDontLook($workOrders);
            $workOrders = $this->filterByStochka($this->user, $workOrders);
            $workOrders = $this->groupByProduct($workOrders, true);
            if(!empty($workOrders)) {
                $workOrders = $this->sliceNaryadsByLimit($workOrders);
                $workOrders = $this->groupByProdGroup($workOrders);
                $ids = ArrayHelper::map($workOrders,'id','id');
                return $ids;
            }
        }

        //Проверка на наличие переделок (самых срочных заказов)
        if (($workOrderNeedTemplate = $this->checkWorkOrdersNeedTemplates())) {
            return [$workOrderNeedTemplate->id];
        //Если есть переделки
        }  else {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id);
            $workOrders = $this->filterByTrainee($workOrders);
            $workOrders = $this->filterByDontLook($workOrders);
            $workOrders = $this->filterByStochka($this->user, $workOrders);
            $workOrders = $this->groupByProductionLiteral($workOrders);
        }

        $workOrders = $this->groupByProdGroup($workOrders);
        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на ШВЕЙКУ человеку
     * @return array
     */
    public function idsForSectionOfSewing()
    {
        $ids = [];
        //Проверка на наличие несданных нарядов
        if (!$this->checkWorkOrdersNeedEnd($this->user_id,$this->location_id)) return $ids;

        //Если пользователь - стажер
        if ($this->user && $this->user->trainee) {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[7]);
            $workOrders = $this->filterByTrainee($workOrders);
            $workOrders = $this->filterByDontLook($workOrders);
            $workOrders = $this->groupByProduct($workOrders, true);
            $workOrders = $this->groupByLiteral($workOrders,true);
            if(!empty($workOrders)) {
                $workOrders = $this->sliceNaryadsByLimit($workOrders);
                $ids = ArrayHelper::map($workOrders,'id','id');

                return $ids;
            }
        }

        //Проверка на наличие переделок (самых срочных заказов)
        if ( ($workOrderNeedTemplate = $this->checkWorkOrdersNeedTemplates())) {
//            $workOrderNeedTemplate = array_shift (  $workOrderNeedTemplate );
            return [$workOrderNeedTemplate->id];
        } elseif ( ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[1,2]))
        &&  (count(($workOrders = $this->filterByTrainee($workOrders)))) &&  (count(($workOrders = $this->filterByDontLook($workOrders))))
        ) {
//            $workOrders = $this->groupByProduct($workOrders);
            $workOrders = $this->groupByLiteral($workOrders);
        }  else {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[3,4,5,6,7]);
            $workOrders = $this->filterByTrainee($workOrders);
            $workOrders = $this->filterByDontLook($workOrders);
//            $workOrders = $this->groupByProduct($workOrders, true);
            $workOrders = $this->groupByLiteral($workOrders);
        }

        $workOrders = $this->sliceNaryadsByLimit($workOrders);
        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на участок швейки комбинезонов
     * @return array
     */
    public function idsForSectionOfSewing2()
    {
        return [];

//        $ids = [];
//        //Проверка на наличие несданных нарядов
//        if (!$this->checkWorkOrdersNeedEnd($this->user_id,$this->location_id)) return $ids;
//        //Проверка на наличие переделок (самых срочных заказов)
//
//        $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id);
//
//        $workOrders = $this->sliceNaryadsByLimit($workOrders);
//        $ids = ArrayHelper::map($workOrders,'id','id');
//
//        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на участок расройки для автофильтров
     * @return array
     */
    public function idsForSectionOfCut()
    {
        $ids = [];
        //Проверка на наличие несданных нарядов
        if (!$this->checkWorkOrdersNeedEnd($this->user_id,$this->location_id)) return $ids;
        //Проверка на наличие переделок (самых срочных заказов)

        $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id);
//        if (count($workOrders) < 4) return $ids; выдвать не меньше 4х. Убрали 16.09.2020

        $workOrders = $this->sliceNaryadsByLimit($workOrders);
        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на КЛИПСЫ человеку
     * @return array
     */
    public function idsForSectionOfClipMaking()
    {
        $ids = [];
        //Проверка на наличие несданных нарядов
        if (!$this->checkWorkOrdersNeedEnd($this->user_id,$this->location_id)) return $ids;
        //Проверка на наличие переделок (самых срочных заказов)

        //Если пользователь - стажер
        if ($this->user && $this->user->trainee) {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[7]);
            if(!empty($workOrders)) {
                //Обрезать по необходимому количеству
                $workOrders = $this->sliceNaryadsByTimeForClips($workOrders);
                $ids = ArrayHelper::map($workOrders,'id','id');

                return $ids;
            }
        }

        if (
            ($top = $this->getListOfWorkOrdersForLocation($this->location_id,[1],true))) {
            return [$top->id];
        } elseif ( ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[2,3,4,5,6,7]))) {
            //Тело не нужно
        } else {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id);
        }
        //Группируем по продукту
        $workOrders = $this->groupByCar($workOrders);
        $workOrders = $this->groupByTraineeClips($workOrders,true);

        //Обрезать по необходимому количеству
        $workOrders = $this->sliceNaryadsByTimeForClips($workOrders);
        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на КЛИПСЫ человеку
     * @return array
     */
    public function idsForSectionOfClipMakingByProductionalLiteral()
    {
        $ids = [];
        //Проверка на наличие несданных нарядов
        if (!$this->checkWorkOrdersNeedEnd($this->user_id,$this->location_id)) return $ids;
        //Проверка на наличие переделок (самых срочных заказов)

        //Если пользователь - стажер
        if ($this->user && $this->user->trainee) {
            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[7]);
            $workOrders = $this->filterByMagnetsOnClips($this->user, $workOrders);
            if(!empty($workOrders)) {
                //Обрезать по необходимому количеству
                $workOrders = $this->groupByTraineeClips($workOrders,true);
                $workOrders = $this->sliceNaryadsByTimeForClips($workOrders);
                $workOrders = $this->groupByProdGroup($workOrders);
                $ids = ArrayHelper::map($workOrders,'id','id');

                return $ids;
            }

            $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[1,2,3,4,5,6]);
            $workOrders = $this->filterByMagnetsOnClips($this->user, $workOrders);
            if(!empty($workOrders)) {
                //Обрезать по необходимому количеству
                $workOrders = $this->groupByTraineeClips($workOrders,true);
                $workOrders = $this->sliceNaryadsByTimeForClips($workOrders);
                $workOrders = $this->groupByProdGroup($workOrders);
                $ids = ArrayHelper::map($workOrders,'id','id');

                return $ids;
            }
        }

        $workOrders = $this->getListOfWorkOrdersForLocation($this->location_id);
        $workOrders = $this->filterByMagnetsOnClips($this->user, $workOrders);
        $workOrders = $this->filterByTrainee($workOrders);
        $workOrders = $this->groupByProductionLiteral($workOrders);
        $workOrders = $this->filterByMagnetsOnClips($this->user, $workOrders);
        $workOrders = $this->groupByProdGroup($workOrders);

        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Функция отвечает за логику формирования списка нарядов для выдачи на ОТК человеку
     * @return array
     */
    public function idsForSectionOfTCD()
    {
        $ids = [];
        //Проверка на наличие несданных нарядов
        if (!$this->checkWorkOrdersNeedEnd($this->user_id,$this->location_id)) return $ids;
        //Инициализируем списко лекал, на которые мы не можем выдавать наряды
        $otherTemplates = $this->getOtherTemplates($this->user_id);
        if (
            ($top = $this->getListOfWorkOrdersForTCD($this->location_id,[1],true))
            &&  (count(($top = $this->filterByTemplates([$top],$otherTemplates)))) ) {
            $top = array_shift (  $top );
            return [$top->id];
//        } elseif (
//            ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[2,3]))
//            &&  (count(($workOrders = $this->filterByOrganazier($this->user, $workOrders))))
//            &&  (count(($workOrders = $this->filterByTemplates($workOrders,$otherTemplates))))
//            &&  (count(($workOrders = $this->filterByProduction($workOrders))))
//            &&  (count(($workOrders = $this->groupByTemplate($workOrders))))
//            &&  (count(($workOrders = $this->filterByEquipmentNew($workOrders))))
////            &&  (count(($workOrders = $this->filterByHamburg($workOrders))))
//        ) {
//            //Тело не нужно
//        } elseif (
//            ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[4,5]))
//            &&  (count(($workOrders = $this->filterByOrganazier($this->user, $workOrders))))
//            &&  (count(($workOrders = $this->filterByTemplates($workOrders,$otherTemplates))))
//            &&  (count(($workOrders = $this->filterByProduction($workOrders))))
//            &&  (count(($workOrders = $this->groupByTemplate($workOrders))))
//            &&  (count(($workOrders = $this->filterByEquipmentNew($workOrders))))
////            &&  (count(($workOrders = $this->filterByHamburg($workOrders))))
//        ) {
//            //Тело не нужно
//        } elseif (
//            ($workOrders = $this->getListOfWorkOrdersForLocation($this->location_id,[6,7]))
//            &&  (count(($workOrders = $this->filterByOrganazier($this->user, $workOrders))))
//            &&  (count(($workOrders = $this->filterByTemplates($workOrders,$otherTemplates))))
//            &&  (count(($workOrders = $this->filterByProduction($workOrders))))
//            &&  (count(($workOrders = $this->groupByTemplate($workOrders))))
//            &&  (count(($workOrders = $this->filterByEquipmentNew($workOrders))))
////            &&  (count(($workOrders = $this->filterByHamburg($workOrders))))
//        ) {
            //Тело не нужно
        } else {
            $workOrders = $this->getListOfWorkOrdersForTCD($this->location_id);
            $workOrders = $this->filterByOrganazier($this->user, $workOrders);
            $workOrders = $this->filterByTemplates($workOrders,$otherTemplates);
//            $workOrders = $this->filterByProduction($workOrders);
            $workOrders = $this->groupByTemplate($workOrders);
            $workOrders = $this->filterByEquipmentNew($workOrders);
//            $workOrders = $this->filterByHamburg($workOrders);
        }

        $workOrders = $this->groupByProdGroup($workOrders);
        //Сгруппировали наряды по лекалу
//        $workOrders = $this->groupByTemplate($workOrders);
        //Обрезали наряды по лимиту
//        $workOrders = $this->sliceNaryadsByLimit($workOrders);
        $ids = ArrayHelper::map($workOrders,'id','id');

        return $ids;
    }

    /**
     * Проверка на наличие нарядов на работнике, которые он должен сдать, прежде чем получать новые
     * @param $user_id
     * @param $location_id
     * @return bool
     */
    public function checkWorkOrdersNeedEnd($user_id, $location_id)
    {
        return (ErpNaryad::find()->where(['and',
            ['status'=>ErpNaryad::STATUS_IN_WORK],
            ['location_id'=>$location_id],
            ['user_id'=>$user_id],
//            ['<','updated_at', $time],
        ])
        ->count()) ? false : true;
    }

    /**
     * Проверка на наличие нарядов на работнике, которые он должен сдать, прежде чем получать новые
     * @param $user_id
     * @param $location_id
     * @return bool
     */
    public function checkWorkOrdersNeedEndForPasting($user_id, $location_id)
    {
        $config = Config::findOne(2);
        $limit = (int)$config->json('timeoutOnTable') ?: 60;

        return (ErpNaryad::find()->where(['and',
            ['status'=>ErpNaryad::STATUS_IN_WORK],
            ['location_id'=>$location_id],
            ['user_id'=>$user_id],
            ['<','updated_at', time()-60*$limit] /*Кроме нарядов на паузе*/
        ])
        ->count()) ? false : true;
    }

    /**
     * Проверка на наличие нарядов на работнике, которые он должен сдать, прежде чем получать новые
     * @param $user_id
     * @param $location_id
     * @return bool
     */
    public function checkWorkOrdersOnUser($user_id, $location_id)
    {
        $result = [];
        $workOrders =  ErpNaryad::find()->where(['and',
            ['status'=>ErpNaryad::STATUS_IN_WORK],
            ['location_id'=>$location_id],
            ['user_id'=>$user_id],
//            ['<','updated_at', $time],
        ])
        ->all();

        foreach ($workOrders as $workOrder) {
            @$result[$workOrder->getArticleTkan()]++;
        }

        return $result;

    }

    /**
     * Если есть переделка для выдачи на локацию, тогда выдать ее человеку
     *
     * @param $location_id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function checkForUrgentWorkOrders($location_id)
    {
        return ErpNaryad::find()->where(['and',
            ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
            ['start' => $location_id],
            ['sort' => 1],
        ])->orderBy('sort,id')
        ->one();
    }

    /**
     * Фукнция для получения массива нарядов, которые должны идти на участок согласно приоритетам и дате создания
     * @param $location_id
     * @param array $sort
     * @param boolean $one
     * @return array|ErpNaryad[]|ErpNaryad
     */
    public function getListOfWorkOrdersForLocation($location_id, $sort = [], $one = false)
    {
        $config = Config::findOne(2);
        $query = ErpNaryad::find()->where(['and',
            ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
            ['start'=>$location_id],
            ['actNumber'=> null],
        ]);
        if (count($sort)) {
            $query->andWhere(['in','sort',$sort]);
        }
        //TODO Костыль, убрать после внедрения новой схемы
        if (!$config->json('vidachaNewScheme') && $location_id == Yii::$app->params['erp_otk']) {
            $query->andWhere('is_new_scheme is null');
        }

        /*
         * Костыль, чтобы не выдавались наряды просто так на участки, для которых необходимо сделать лекало.
         */
        if ($location_id == Yii::$app->params['erp_shveika']) {
            $query->andWhere(['isNeedTemplate' => false]);
            $query->andWhere(['not like', 'article', '%OT-2079%', false]);
            $query->andWhere(['not like', 'article', '%OT-1189%', false]);
        }
        $query->orderBy('sort,id');
        return $one ? $query->one() : $query->all();
    }

    public function getListOfWorkOrdersForTCD($location_id, $sort = [], $one = false)
    {
        $query = ErpNaryad::find()->where(['and',
            ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
            ['start'=>$location_id],
            ['actNumber'=> null],
            ['isCompletion'=> false],
        ]);

        if (count($sort)) {
            $query->andWhere(['in','sort',$sort]);
        }

        $query->orderBy('sort,id');
        return $one ? $query->one() : $query->all();
    }

    /**
     * Фукнция для получения массива нарядов, которые должны идти на участок согласно приоритетам и дате создания
     * @param $location_id
     * @return array|ErpNaryad[]
     */
    public function getListOfWorkOrdersForSectionOfSewing($location_id)
    {
        //Сначала выбираем срочные наряды
        $step1 = ErpNaryad::find()->where(['and',
            ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
            ['start'=>$location_id],
            ['in','sort',[2,3]],
        ])
        ->orderBy('sort,id')
        ->all();

        //Если они есть -> то отадем их
        if ($step1) return $step1;

        //Потом выбираем наряды с обычным приоритетом или низким приоритетом
        $step2 = ErpNaryad::find()->where(['and',
            ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
            ['start'=>$location_id],
            ['in','sort',[4,5]],
        ])
        ->orderBy('sort,id')
        ->all();

        if ($step2) return $step2;

        //Если не под один приоритет не подходит, тогда выбираем наряды все
        $step3 = ErpNaryad::find()->where(['and',
            ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
            ['start'=>$location_id],
        ])
        ->orderBy('sort,id')
        ->all();

        return $step3;
    }

    /**
     * Функция для получения списка лекал, находящихся в пользовании у других работников на текущний момент
     * @param $user_id
     * @return array
     */
    public function getOtherTemplates($user_id)
    {
        $templates = [];
        $templateWorkOrders = ErpNaryad::find()->where(['and',
            ['status'=>ErpNaryad::STATUS_IN_WORK],
            ['or',
                ['location_id'=>Yii::$app->params['erp_izgib']],
                ['location_id'=>Yii::$app->params['erp_otk']]
            ],
            ['!=','user_id',$user_id],
        ])->all();

        foreach ($templateWorkOrders as $templateWorkOrder) {
            $lekalo = $templateWorkOrder->lekalo ? (int)$templateWorkOrder->lekalo : 0;
            if (!in_array($lekalo,$templates)) {
                $templates[] = $lekalo;
            }
        }

        return $templates;
    }

    /**
     * Функция для получения списка лекал, находящихся в пользовании у других работников на текущний момент
     * @return array
     */
    public function getTemplatesWaitTCD()
    {
        $templates = [];

        $otkLocation = ErpLocation::find()->where(['id' => Yii::$app->params['erp_otk']])->one();
        $workplaces = $otkLocation->recieveActiveWorkplaces();
        if (!$workplaces || count($workplaces) < 2)
            return [];


        $templateWorkOrders = ErpNaryad::find()
            ->select(['id','logist_id','order_id','json'])
            ->where(['and',
                ['status'=>ErpNaryad::STATUS_IN_WORK],
                ['start'=>Yii::$app->params['erp_otk']]
            ])->all();

        $resWorkOrders = [];
        $exc = [];

        foreach ($templateWorkOrders as $mainKey => $workOrder) {

            if (in_array($workOrder->id,$exc)) continue;
            //Если нету группы то пофиг
            $upn = $workOrder->upn;
            if (!$upn || !$upn->group) {
                $resWorkOrders[]= $workOrder;
                continue;
            }
            //Если в группе только 1 позиция то пофиг
            $group = UpnGroup::getAnotherFromGroup($upn);
            if (empty($group)) {
                $resWorkOrders[]= $workOrder;
                continue;
            }
            //Если в группе больше 1 ой позиции, тогда смотрим, а имеем ли мы прав

            $ids = ArrayHelper::map($group,'id','id');
            $groupWorkOrders = ErpNaryad::find()
                ->where(['and',
                    ['order_id' => $workOrder->order_id],
                    ['in','logist_id',$ids],
                    ['start' => Yii::$app->params['erp_otk']],
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['location_id' => Yii::$app->params['erp_dispatcher']],
                ])
                ->all();

            if (count($ids) !== count($groupWorkOrders)) {
                $searchExist = ErpNaryad::find()
                    ->where(['and',
                        ['order_id' => $workOrder->order_id],
                        ['in','logist_id',$ids],
                        ['and',
                            ['or',
                                ['and',['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                                ['status' => null],
                            ],
                        ],
                        ['or',['!=', 'location_id', Yii::$app->params['erp_otk']],['location_id' => null]],
                        ['or',['!=', 'start', Yii::$app->params['erp_otk']],['start' => null]],
                    ])
                    ->count();
                if (!$searchExist) {
                    $resWorkOrders[] = $workOrder;
                    foreach ($groupWorkOrders as $row) {
                        $resWorkOrders[] = $row;
                        if (!in_array($row->id,$exc)) $exc[] = $row->id;
                    }
                }
            }else{
                $resWorkOrders[] = $workOrder;
                foreach ($groupWorkOrders as $row) {
                    $resWorkOrders[] = $row;
                    if (!in_array($row->id,$exc)) $exc[] = $row->id;
                }
            }
        }

        $stopTemplate= [];

        foreach ($resWorkOrders as $templateWorkOrder) {
            $lekalo = $templateWorkOrder->lekalo ? (int)$templateWorkOrder->lekalo : 0;
            if (isset($templates[$lekalo])) {
                $templates[$lekalo]++;
                if ($templates[$lekalo] > 10)
                    if (!in_array($lekalo, $stopTemplate))
                        $stopTemplate[] = $lekalo;
            } else {
                $templates[$lekalo] = 1;
            }
        }

        return $stopTemplate;
    }

    /**
     * Функция для фильтрации списка нарядов по лекалам, отрасывая наряды на лекала, которые уже используются
     *
     * @param array|ErpNaryad[] $workOrders
     * @param array $templates
     * @return mixed
     */
    public function filterByTemplates($workOrders, $templates = [])
    {
        foreach ($workOrders as $key => $workOrder) {
            if (in_array(( (int)$workOrder->lekalo ), $templates)) {
                unset($workOrders[$key]);
            }
        }

        return $workOrders;
    }

    public function filterByOrganazier($user, $workOrders)
    {
        //Получим массив вида ткань - количество столов
        $workplaces = $user->recieveWorkplacesByLocation($this->location_id);

        if (empty($workplaces))
            return [];

        $workplace = array_shift($workplaces);

        switch ($workplace->property) {
            case 'Органайзеры' :
                return [];
            case 'Автоодеяло' :
                return [];
            default:
                foreach ($workOrders as $key => $workOrder) {
                    if (preg_match('/OT-(1189|1190|2061)/', $workOrder->article) === 1)
                        unset($workOrders[$key]);
                }
                return $workOrders;
        }
    }

    /**
     * Функция для фильтрации списка нарядов по лекалам, отрасывая наряды на лекала, которые уже используются
     * и группируя наряды по лекалам в конце для более удобной выдачи
     *
     * @param $workOrders
     * @param bool $slice
     * @return array
     */
    public function groupByTemplate($workOrders,$slice = false)
    {
        $array1 = []; //Для последовательности лекал
        $array2 = []; //Для учета работы по лекалу

        foreach ($workOrders as $key => $workOrder) {
            $lekalo = $workOrder->lekalo ? (int)$workOrder->lekalo: 0;

            if ( !in_array($lekalo,$array1) ) {
                $array1[] = $lekalo;
            }
            $array2[$lekalo][] = $workOrder;
        }

        $workOrders = [];
        foreach ($array1 as $value) {
            foreach ($array2[$value] as $workOrder) {
                $workOrders[] = $workOrder;
            }
            if ($slice) break;
        }

        return $workOrders;
    }

    /**
     * Функция для фильтрации списка нарядов по лекалам, отрасывая наряды на лекала, которые уже используются
     * и группируя наряды по лекалам в конце для более удобной выдачи
     *
     * @param $workOrders
     * @return array
     */
    public function groupByProdGroup($workOrders) : array
    {
        $result = [];
        if (empty($workOrders)) return $result;

        $workOrder = array_shift($workOrders);
        if ($workOrder->prodGroup && $this->location_id) {
            return ErpNaryad::find()
                ->where(['prodGroup' => $workOrder->prodGroup])
                ->andWhere(['start' => $this->location_id])
                ->andWhere(['status' => ErpNaryad::STATUS_IN_WORK])
                ->all();
        } else {
            $result[] = $workOrder;
            foreach ($workOrders as $workOrder) {
                if (!$workOrder->prodGroup) {
                    $result[] = $workOrder;
                }
            }
            return $result;
        }
    }

    /**
     * Функция для получения списка тканей, с которыми может работать пользователь
     *
     * @param ErpUser $user
     * @return mixed
     */
    public function getCountWorkplacesForCloth(ErpUser $user,$naryads)
    {
        //Получим массив вида ткань - количество столов
        $workplaces = $user->recieveWorkplacesByLocation($this->location_id);
        $propertiesWorkplace = [];
        $result = [];

        if ($workplaces)
            foreach ($workplaces as $workplace) {
                @$propertiesWorkplace[$workplace->property]++;
            }

        //Получим лимит нарядов на 1 ткань стола
        $config = Config::findOne(2);
        $limit = (int)$config->json('limitOnTable') ?: 6;

        //Получим количество нарядов на ткань, которые могут быть у человека на руках
        foreach ($propertiesWorkplace as $key => $value) {
            $minus = $naryads[$key] ?? 0;
            $result[$key] = $value * $limit - $minus;
        }


        return $result;
    }

    /**
     * Функция для получения списка тканей, с которыми может работать пользователь
     *
     * @param ErpUser $user
     * @return mixed
     */
    public function filterByStochka(ErpUser $user,$naryads)
    {
        //Получим массив вида ткань - количество столов
        $workplaces = $user->recieveWorkplacesByLocation($this->location_id);

        if (empty($workplaces))
            return [];

        $workplace = array_shift($workplaces);

        $stochka = null;
        switch ($workplace->property) {
            case 'Сточка резинки' :
                $stochka = false;
                break;
            case 'Пошив без сточки' :
                $stochka = true;
                break;
            case 'Пошив' :
                $stochka = null;
                break;
            default:
                return [];
        }

        if ($stochka === null)
            return $naryads;

        foreach ($naryads as $key => $naryad) {
            if ($naryad->stochka != $stochka) {
                unset($naryads[$key]);
            }
        }

        return $naryads;
    }

    //На клипсах отделяем магниты от клипс
    public function filterByMagnetsOnClips(ErpUser $user,$naryads)
    {
        //Получим массив вида ткань - количество столов
        $workplaces = $user->recieveWorkplacesByLocation($this->location_id);

        if (empty($workplaces))
            return [];

        $workplace = array_shift($workplaces);

        switch ($workplace->property) {
            case 'Магниты' :
                foreach ($naryads as $key => $naryad) {
                    if (preg_match('/^\w+-\w-\d+-(67|70|71|72)-\d+$/', $naryad->article) !== 1) {
                        unset($naryads[$key]);
                    }
                }
                return $naryads;
            case 'Клипсы' :
                foreach ($naryads as $key => $naryad) {
                    if (preg_match('/^\w+-\w-\d+-(67|70|71|72)-\d+$/', $naryad->article) === 1) {
                        unset($naryads[$key]);
                    }
                }
                return $naryads;
            default:
                return $naryads;
        }
    }

    /**
     * Функция для группировки клипс по автомобилю
     *
     * @param array|ErpNaryad[] $workOrders
     * @return array
     */
    public function groupByCar($workOrders, $slice = false)
    {
        $array1 = []; //Для последовательности лекал
        $array2 = []; //Для учета работы по лекалу

        foreach ($workOrders as $key => $workOrder) {
            $car = $workOrder->carArticle ? (int)$workOrder->carArticle : 0;

            if ( !in_array($car,$array1) ) {
                $array1[] = $car;
            }
            $array2[$car][] = $workOrder;
        }

        $workOrders = [];
        foreach ($array1 as $value) {
            foreach ($array2[$value] as $workOrder) {
                $workOrders[] = $workOrder;
            }
            if ($slice) break;
        }

        return $workOrders;
    }

    /**
     * Функция для фильтрации списка нарядов по лекалам, отрасывая наряды на лекала, которые уже используются
     *
     * @param array|ErpNaryad[] $workOrders
     * @param array $properties
     * @return mixed
     */
    public function filterByCloth($workOrders, $properties)
    {
        //Если ни одно из свойств не установлено, тогда возвращаем пустой массив
        if (empty($properties)) return [];

        foreach ($workOrders as $key => $workOrder) {
            if (!in_array($workOrder->articleTkan, $properties)) {
                unset($workOrders[$key]);
            }
        }

        return $workOrders;
    }

    /**
     * Функция для фильтрации списка нарядов по лекалам, отрасывая наряды на лекала, которые уже используются
     *
     * @param array|ErpNaryad[] $workOrders
     * @param array $properties
     * @return mixed
     */
    public function filterByClothAndWorkplace($workOrders, $properties)
    {
        //Если ни одно из свойств не установлено, тогда возвращаем пустой массив
        if (empty($properties)) return [];

        $array1 = [];   //Последовательность групп
        $array2 = [];   //Наряды по группам
        $array3 = [];   //Наряды по группам
        //Сначала давай переберем
        $clothList = array_keys($properties);
        foreach ($workOrders as $key => $workOrder) {
            $articleObj = new Article($workOrder->article);
            $cloth = $articleObj->articleCloth?:0;
            $window = $articleObj->windowEn ?: 0;
            if ($cloth == 5 && ($window == 'BW' || $articleObj->isNoName())) {
                $cloth = '5C';
            } else if ($cloth == 5 && $window != 'BW') {
                $cloth = '5T';
            }
            if (!in_array($cloth, $clothList) && !in_array('Все ткани', $clothList)) {
                continue;
            }
            if ( !in_array($cloth,$array1) ) {
                $array1[] = $cloth;
            }
            if (!isset($array2[$cloth]['windows'])) {
                $array2[$cloth]['windows'] = [];
            }
            if (!in_array($this->windowValues[$window], $array2[$cloth]['windows'])) {
                $array2[$cloth]['windows'][$window] = $this->windowValues[$window];
            }

            $array2[$cloth]['orders'][] = $workOrder;
            @$array2[$cloth]['count']++;
            $array3[$cloth.$window]['orders'][] = $workOrder;
        }

        $this->clothValues = $array1;
        if (!empty($this->cloth) && isset($array2[$this->cloth]['windows'])) {
            $this->windowValuesExisting = $array2[$this->cloth]['windows'];
        }

            // Если установлена ткань и оконный проем, тогда только
        if (!empty($this->cloth) && !empty($this->window) && isset($array3[$this->cloth.$this->window]['orders'])) {
            return array_slice($array3[$this->cloth.$this->window]['orders'],0,2);
        } elseif (!empty($this->window)) {
            return [];
        }

        // Если установлена ткань и оконный проем, тогда только
        if (!empty($this->cloth) && isset($array2[$this->cloth]['orders'])) {
            return $this->groupByProductionLiteral($array2[$this->cloth]['orders'], true);
        }

        $maxCount = 0;
        $maxKey = null;
        foreach ($array2 as $key => $value) {
            if ($value['count'] > $maxCount) {
                $maxCount = $value['count'];
                $maxKey = $key;
            }
        }

        return $this->groupByProductionLiteral($array2[$maxKey]['orders'] ?? [], true);
    }

    /**
     * Алгоритм для фильтрации нарядов по комплектации для ОТК
     *
     * @param ErpNaryad[] $workOrders
     * @return mixed
     */
    public function filterByEquipment($workOrders)
    {
        foreach ($workOrders as $mainKey => $workOrder) {


            if ($workOrder->window && $workOrder->window != 'OT') {
                $equipment = $workOrder->equipment;
                $count = 0;
                foreach ($equipment as $key => $value) {
                    $searchWorkOrder = ErpNaryad::find()
                        ->where(['and',
                            ['order_id' => $workOrder->order_id],
                            ['like', 'article', $key . '-%-' . $workOrder->carArticle . '-%-%', false],
                            ['start' => Yii::$app->params['erp_otk']],
                            ['status' => ErpNaryad::STATUS_IN_WORK],
                            ['location_id' => Yii::$app->params['erp_dispatcher']],
                        ])
                        ->one();

                    if ($searchWorkOrder)
                        $count++;
                }

                if (count($equipment) !== $count) {
                    foreach ($equipment as $key => $value) {
                        $searchExist = ErpNaryad::find()
                            ->where(['and',
                                ['order_id' => $workOrder->order_id],
                                ['like', 'article', $key . '-%-' . $workOrder->carArticle . '-%-%', false],
                                ['or', ['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                                ['!=', 'location_id', Yii::$app->params['erp_otk']],
                                ['!=', 'start', Yii::$app->params['erp_otk']],
                            ])
                            ->one();
                        if ($searchExist) {
                            unset($workOrders[$mainKey]);
                        }
                    }
                }
            }
        }

        return $workOrders;
    }


    /**
     * Алгоритм для фильтрации нарядов по комплектации для ОТК
     *
     * @param ErpNaryad[] $workOrders
     * @return mixed
     */
    public function filterByEquipmentNew($workOrders)
    {
        $resWorkOrders = [];
        $exc = [];

        $firstLekalo = null;
        $flag = true;

        foreach ($workOrders as $mainKey => $workOrder) {
            $lekalo = $workOrder->lekalo;
            $lekalo = $lekalo ? (int)$lekalo : 0;
            if ($firstLekalo === null)
                $firstLekalo = $lekalo;
            if ($flag && $firstLekalo == $lekalo) {
                $maxCount = 10;
            } else {
                $maxCount = 6;
                $flag = false;
            }

            if (count($resWorkOrders) >= $maxCount) break;
            if (in_array($workOrder->id,$exc)) continue;
            //Если нету группы то пофиг
            $upn = $workOrder->upn;
            if (!$upn || !$upn->group) {
                $resWorkOrders[]= $workOrder;
                continue;
            }
            //Если в группе только 1 позиция то пофиг
            $upnGroup = $upn->group;
            $group = UpnGroup::getAnotherFromGroup($upn);
            if (empty($group)) {
                $resWorkOrders[]= $workOrder;
                continue;
            }
            //Если в группе больше 1 ой позиции, тогда смотрим, а имеем ли мы прав

            $ids = ArrayHelper::map($group,'id','id');
            $groupWorkOrders = ErpNaryad::find()
                ->where(['and',
                    ['order_id' => $workOrder->order_id],
                    ['in','logist_id',$ids],
                    ['start' => Yii::$app->params['erp_otk']],
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['location_id' => Yii::$app->params['erp_dispatcher']],
                ])
                ->all();

            if (count($ids) !== count($groupWorkOrders)) {
                $searchExist = ErpNaryad::find()
                    ->where(['and',
                        ['order_id' => $workOrder->order_id],
                        ['in','logist_id',$ids],
                        ['and',
                            ['or',
                                ['and',['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                                ['status' => null],
                            ],
                        ],
                        ['or',['!=', 'location_id', Yii::$app->params['erp_otk']],['location_id' => null]],
                        ['or',['!=', 'start', Yii::$app->params['erp_otk']],['start' => null]],
                    ])
                    ->count();
                if (!$searchExist) {
                    $resWorkOrders[] = $workOrder;
                    foreach ($groupWorkOrders as $row) {
                        $resWorkOrders[] = $row;
                        if (!in_array($row->id,$exc)) $exc[] = $row->id;
                    }
                }
            }else{
                $resWorkOrders[] = $workOrder;
                foreach ($groupWorkOrders as $row) {
                    $resWorkOrders[] = $row;
                    if (!in_array($row->id,$exc)) $exc[] = $row->id;
                }
            }
        }

        return $resWorkOrders;
    }

    /**
     * Алгоритм для фильтрации нарядов по тому, кто ранее принимал участие в производстве нарядов для ОТК
     *
     * @param ErpNaryad[] $workOrders
     * @return mixed
     */
    public function filterByProduction($workOrders)
    {
//        foreach ($workOrders as $key => $workOrder) {
//            $userIds = ErpNaryadLog::getUsersIds($workOrder->id);
////            var_dump($workOrder->id,$userIds);die();
//            if (in_array($this->user_id,$userIds)) {
//                unset($workOrders[$key]);
//            }
//        }

        return $workOrders;
    }

    /**
     * @param $workOrders
     * @return mixed
     */
    public function filterByHamburg($workOrders)
    {
        foreach ($workOrders as $key => $workOrder) {
            if (($order = $workOrder->order) && $order->json('username') == 'Hamburg') {
                unset($workOrders[$key]);
            }
        }

        return $workOrders;
    }

    /**
     * Алгоритм для фильтрации нарядов для стажеров.
     * Если у пользователя стоит статус стажер, то он может получать наряды только определенные на участки.
     *
     * @param ErpNaryad[] $workOrders
     * @return mixed
     */
    public function filterByTrainee($workOrders)
    {
        if (!@$this->user->trainee) return $workOrders;

        $result = [];

        if ($this->location_id == Yii::$app->params['erp_shveika']) {
            $patternTrainee = $this->user->trainee == 2 ? Yii::$app->params['trainee_pattern_sewing_2'] : Yii::$app->params['trainee_pattern_sewing'];
        } else{
            $patternTrainee = Yii::$app->params['trainee_pattern'];
        }
        foreach ($workOrders as $key => $workOrder) {
            if (preg_match($patternTrainee, $workOrder->article) === 1) {
                $result[] = $workOrder;
            }
        }

        return $result;
    }

    /**
     * Алгоритм фильтрации Донтулков на швейке
     *
     * @param  ErpNaryad[] $workOrders
     * @return array
     */
    public function filterByDontLook($workOrders)
    {
        $can =  @$this->user->canCreateDontlook;

        $result = [];

        foreach ($workOrders as $key => $workOrder) {
            if (!ArticleHelper::isDontLook($workOrder->article) || $can) {
                $result[] = $workOrder;
            }
        }

        return $result;
    }


    /**
     * [ Функция возвращает наряд, на который надо изготовить Лекало, если пользователь может изготавливать лекало ]
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public function checkWorkOrdersNeedTemplates()
    {
        if (!@$this->user->canCreateTemplate) return null;

        $query = ErpNaryad::find()->where(['and',
            ['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK]],
            ['start' => Yii::$app->params['erp_shveika']],
            ['isNeedTemplate' => true],
        ]);
        $query->orderBy('sort,id');
        return $query->one();
    }


    /**
     * Алгоритм выдачи нарядов на участки клипс для стажеров.
     *
     * @param $workOrders
     * @param bool $slice
     * @return array
     */
    public function groupByTraineeClips($workOrders,$slice = false)
    {
        if (!@$this->user->trainee) return $workOrders;

        $priorityArr = [];
        $vars =  [];
        foreach ($workOrders as $key => $workOrder) {

            $clipsArr = $workOrder->getTypeClips();
            //тут запиываем сортировку
            if ( (isset($clipsArr['A']) || isset($clipsArr['AA'])) && !(isset($clipsArr['B']) && !isset($clipsArr['C']) && !isset($clipsArr['CC'])) ) {
                $priority = 1;
                $priorityArr[$priority][] = $workOrder;
                if (!in_array($priority,$vars)) $vars[] = $priority;
                continue;
            }
            if ( isset($clipsArr['B']) && !isset($clipsArr['A']) && !isset($clipsArr['AA']) && !isset($clipsArr['C']) && !isset($clipsArr['CC']) ) {
                $priority = 2;
                $priorityArr[$priority][] = $workOrder;
                if (!in_array($priority,$vars)) $vars[] = $priority;
                continue;
            }
            if ( (isset($clipsArr['A']) || isset($clipsArr['AA'])) && isset($clipsArr['B']) && !isset($clipsArr['C']) && !isset($clipsArr['CC']) ) {
                $priority = 3;
                $priorityArr[$priority][] = $workOrder;
                if (!in_array($priority,$vars)) $vars[] = $priority;
                continue;
            }
            if ( isset($clipsArr['C']) || isset($clipsArr['CC']) ) {
                $priority = 4;
                $priorityArr[$priority][] = $workOrder;
                if (!in_array($priority,$vars)) $vars[] = $priority;
                continue;
            }
        }

        sort($vars);
        $workOrders = [];

        foreach ($vars as $value) {
            foreach ($priorityArr[$value] as $workOrder) {
                $workOrders[] = $workOrder;
            }
            if ($slice) break;
        }

        return $workOrders;
    }

    /**
     * Функция осуществляет группировку по продукту для нарядов
     *
     * @param array|ErpNaryad[]  $workOrders
     * @param boolean $slice
     * @return array|ErpNaryad[] $workOrders
     */
    public function groupByProduct($workOrders, $slice = false)
    {
        $array1 = [];   //Последовательность групп
        $array2 = [];   //Наряды по группам
        $group  = null;

        foreach ($workOrders as $key => $workOrder) {
            $product = $workOrder->searchProduct();
            if ($product) {
                $group = $product->recieveGroupByLocation($workOrder->start);
            }

            $group = $group ?: 0;

            if ( !in_array($group,$array1) ) {
                $array1[] = $group;
            }

            $array2[$group][] = $workOrder;
        }

        $workOrders = [];
        foreach ($array1 as $value) {
            foreach ($array2[$value] as $workOrder) {
                $workOrders[] = $workOrder;
            }
            if ($slice) break;
        }

        return $workOrders;
    }

    /**
     * Функция осуществляет группировку по литере нарядов для нарядов
     *
     * @param array|ErpNaryad[] $workOrders
     * @param boolean $slice
     * @return array|ErpNaryad[] $workOrders
     */
    public function groupByLiteral($workOrders, $slice = false)
    {
        $array1 = [];   //Последовательность групп
        $array2 = [];   //Наряды по группам

        foreach ($workOrders as $key => $workOrder) {
            $literal = $workOrder->literal ?: 0;

            if ( !isset($array1[$workOrder->sort]) || !in_array($literal,$array1[$workOrder->sort]) ) {
                $array1[$workOrder->sort][] = $literal;
            }

            $array2[$workOrder->sort][$literal][] = $workOrder;
        }

        ksort($array2);

        $workOrders = [];
        foreach ($array2 as $sort => $literalArray) {
            foreach ($array1[$sort] as $value) {
                foreach ($literalArray[$value] as $workOrder) {
                    $workOrders[] = $workOrder;
                }
                if ($slice) break;
            }
        }

        return $workOrders;
    }

    /**
     * Функция осуществляет группировку по литере нарядов для нарядов
     *
     * @param array|ErpNaryad[] $workOrders
     * @param bool $cross
     * @return array|ErpNaryad[]
     */
    public function groupByProductionLiteral($workOrders, $cross = false): array
    {
        if (empty($workOrders))
            return [];

        if ($cross)
            $inIds = ArrayHelper::map($workOrders, 'id','id');

        //Получаем наряд, если массив нарядов не пустой.
        $workOrder = array_shift($workOrders);
        //Далее, находим для наряда на текущий участок литеру.
        $ids = SProductionLiteral::searchWorkOrdersFromLiteral($workOrder->id, $workOrder->start);

        if ($cross)
            $ids = array_intersect($inIds, $ids);

            //Далее, отдаем все наряды из литеры на выдачу
        return ErpNaryad::find()->where(['id' => $ids])->all();
    }

    /**
     * Обрезать количество
     *
     * @param $workOrders
     * @return array
     */
    private function sliceNaryadsByLimit($workOrders)
    {
        $config=Config::findOne(2);
        $flag = 0;
        $fullTime = 0;
        $maxTime = $config->json('maxLimitTimeNaryads');
        foreach ($workOrders as $key => $workOrder) {
            //получаем наряда
            $product = $workOrder->searchProduct();
            $group = $product ? $product->recieveGroupByLocation($workOrder->start) : null;
            $row = ErpProductPerformance::find()
                ->where(['group_id' => $group])
                ->andWhere(['user_id' => $this->user_id])
                ->one();

            $configTime=$config->json('timeOnNaryad');
            $time = $row ? ($row->average_time ? $row->average_time : ($configTime ? $configTime : 1440)) : ($configTime ? $configTime : 1440);
            $fullTime += $time;
            $flag = $key;
            if ($fullTime > ($maxTime ? $maxTime : 7200))
            {
                break;
            }
        }

        $type = $config->json('typeVidacha');
        $limit = $config->json('limitvidacha');
        if ($this->location_id == Yii::$app->params['erp_okleika']) {
            $limit = $config->json('limitvidachaOkleika') ?:4;
            if (($current = current($workOrders)) && $current->isAnotherPlaced == true)
                    $limit = count($workOrders);
        }
        if ($this->location_id == Yii::$app->params['erp_izgib']) $limit = 12;
        if ($this->location_id == Yii::$app->params['erp_shveika']) $limit = 10;
        if (@$this->user->trainee) $limit = $config->json('limitvidachatrainee');

        if ($this->location_id == 18) $limit = 1;
        if ($this->location_id == 19) $limit = 60;
        if ($this->location_id == 22) $limit = 60;
        if ($this->location_id == 22) $limit = 60;


        if ($type && $type == 'По лимиту' && $limit) {
            $flag = $limit;
        }
        $workOrders = array_slice($workOrders, 0, ($flag ? $flag : 1), true);

        return $workOrders;
    }

    /**
     * Обрезать количество
     *
     * @param $workOrders
     * @return array
     */
    private function sliceNaryadsByTimeForClips($workOrders)
    {
        if (empty($workOrders)) return [];

        $timeLimit = 40;
        if (@$this->user->trainee) $timeLimit = 10;

        $time = 0;
        $count = 1;
        foreach ($workOrders as $workOrder) {
            $value = explode('-', $workOrder->article);
            if ($value[0] != 'OT') {
                if (ArticleHelper::isOnMagnets($workOrder->article)) {
                    $time += 8;
                } else {
                    foreach ($workOrder->getTypeClips() as $key => $value) {
                        switch ($key) {
                            case 'A':
                                $time += 0.4 * $value;
                                break;
                            case 'AA':
                                $time += 0.4 * $value;
                                break;
                            case 'B':
                                $time += 0.7 * $value;
                                break;
                            case 'C':
                                $time += 1.4 * $value;
                                break;
                            case 'CC':
                                $time += 1.4 * $value;
                                break;
                            default;
                                $time += 0.9 * $value;
                                break;
                        }
                    }
                }
            } else {
                $time += 6;
            }
            if ($time > $timeLimit) break;
            $count++;
        }

        return array_slice($workOrders, 0, $count, true);
    }

    /**
     * @return array|int|\yii\db\ActiveRecord[]
     * Получаем наряды на выдачу
     */
    public function  getNaryadsNaVidachu()
    {
        if ($this->user_id) {

            $idsYes =  $this->algorithm();

            $naryads = ErpNaryad::find()->where(['and',
                ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
                ['start'=>$this->location_id],
                ['in','id',$idsYes],
            ])->orderBy('sort,id')->all();

            return $naryads ? $naryads : 0;
        }
        
        return 0;
    }

    /**
     * Получаем наряды на выдачу по одному
     * @return array|int|null|\yii\db\ActiveRecord
     */
    public function  getNaryadsNaVidachuByOne()
    {
        if ($this->user_id) {

            $idsYes =  $this->algorithm();

            $naryads = ErpNaryad::find()->where(['and',
                ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
                ['start'=>$this->location_id],
                ['in','id',$idsYes],
            ])->orderBy('sort,id')->one();

            return $naryads ? $naryads : 0;
        }

        return 0;
    }


    #########
    ############################################# Functions ############################################################
}
