<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/28/17
 * Time: 2:23 PM
 */

namespace backend\modules\laitovo\models;


use common\models\laitovo\ErpReportCard;
use backend\modules\admin\models\User;
use common\models\laitovo\ErpUserPosition;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Weekend;

class ErpReportCardForm extends ErpReportCard
{
    public $date;
    public $user;

    public $start_work = '2017-08-01';

    public $fields = [];

    public function __construct($date, $user=null)
    {
        $this->date = $date;
        $this->user = $user;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['json'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function getDateName(){
        return $this->getDateAction()->getMonth().'-'.$this->getDateAction()->getEar();
    }

    public function getUserName(){
        return ErpUser::findOne($this->user)->name;
    }

    public function getDateAction(){
        return new DateAction($this->date, $this->user);
    }


    public function getDateArray(){
        $dateAction = $this->getDateAction();
        if($dateAction->getDateArray())
            return $dateAction->getDateArray();
    }


    public function getResultArray(){
            $dateAction = $this->getDateAction();
            return $dateAction->resultArray();
    }

    public function getDocumentDateArray(){
        $dateAction = $this->getDateAction();
        return $dateAction->getDocumentDateArray();
    }


    public function getArrayDataProvoder(){
        return new ArrayDataProvider([
            'allModels' => $this->getResultArray(),
        ]);
    }



     public function getPerfectArrayDataProvoder(){
            return new ArrayDataProvider([
                'allModels' =>  $this->getPerfectResult()
            ]);
        }


    public function getPerfectArrayDocumentDataProvoder(){
        return new ArrayDataProvider([
                'allModels' =>  $this->perfectResultDocumentArray()
            ]);
        }

    /**
     * Делаем массив с идеальными результатами
     */
    public function getPerfectResult()
    {
        $weekend = Weekend::getWeekendList();
        $perfect_array = [
            'final_difference'=>[
                'label'=>'раб. час',
            ],
            'time_in'=>[
                'label'=>'приход',
            ],
            'time_out'=>[
                'label'=>'уход',
            ],
            'in_production'=>[
                'label'=>'сделка',
            ],
        ];
        foreach($this->getDateArray() as $date)
        {
            if(!array_key_exists($date,$weekend)){
                $perfect_array['final_difference'][$date] = '8.00';
                $perfect_array['time_in'][$date] = '8.30';
                $perfect_array['time_out'][$date] = '17.30';
                $perfect_array['in_production'][$date] = '0';
            }else{
                $perfect_array['final_difference'][$date] = '0';
                $perfect_array['time_in'][$date] = '0';
                $perfect_array['time_out'][$date] = '0';
                $perfect_array['in_production'][$date] = '0';
            }
        }
        $perfect_array['final_difference']['fact_sum'] = $this->getMonthRate()['month_rate'];
        $perfect_array['final_difference']['month_rate'] = $this->getMonthRate()['month_rate'];
        return $perfect_array;
    }


    public function perfectResultDocumentArray(){
        $weekend = Weekend::getWeekendList();
        $document_date_arr = $this->getDocumentDateArray();
        $perfect_array = [
            'final_difference'=>[
                'label'=>'раб. час',
            ],
            'time_in'=>[
                'label'=>'приход',
            ],
            'time_out'=>[
                'label'=>'уход',
            ],
            'in_production'=>[
                'label'=>'сделка',
            ],
        ];
        foreach($this->getDateArray() as $date)
        {
            if(!array_key_exists($date,$weekend)){
                $perfect_array['final_difference'][$date] = '8.00';
                $perfect_array['time_in'][$date] = '8.30';
                $perfect_array['time_out'][$date] = '17.30';
                $perfect_array['in_production'][$date] = '0';
            }else{
                $perfect_array['final_difference'][$date] = '0';
                $perfect_array['time_in'][$date] = '0';
                $perfect_array['time_out'][$date] = '0';
                $perfect_array['in_production'][$date] = '0';
            }
        }
        if(count($document_date_arr)){
            $sum = 0;
            foreach ($perfect_array as $key=>$value)
            {
                foreach ($document_date_arr as $type=>$dates){
                    foreach ($dates as $date){
                        if(array_key_exists($date,$value)) {
                            $perfect_array[$key][$date] = $type;
                        }
                    }
                }
            }
            foreach ($perfect_array['final_difference'] as $value){
                if($value=='8.00' || $value==ErpDocumentCause::BISINESS_TRIP){
                    $sum +=8;
                }
            }
            $perfect_array['final_difference']['fact_sum'] = $sum;

        }else{
            $perfect_array['final_difference']['fact_sum'] = $this->getMonthRate()['month_rate'];
        }
        $perfect_array['final_difference']['month_rate'] = $this->getMonthRate()['month_rate'];
        return $perfect_array;
    }



    public function saveCard()
    {
        $model=ErpReportCard::find()->where(['user_id'=>$this->user,'month_year'=>$this->getFlag()])->one();
        if(!$model){
            $model = new ErpReportCard();
        }
        $result_array = array_merge($this->getFactSum(), $this->getMonthRate(),$this->fields);
        $model->user_id = $this->user;
        $model->month_year = date('Y-m-d',strtotime($this->date));
        $model->json = json_encode($result_array);
        if($model->save())  return true;
    }


    //
    public function getFlag()
    {
        $value = explode('.',$this->date);
        return $value[1].'.'.$value[2];
    }

    //пересчитываем сумму часов согласно введенным в форме данным
    public function getFactSum()
    {
        $sum_sec = 0;

        foreach ($this->fields as $field)
        {
            if(isset($field['final_difference']) && $field['final_difference']!= ErpDocumentCause::BISINESS_TRIP)
            {
                $sum_sec += DateAction::inSecond($field['final_difference']);
            }elseif (isset($field['final_difference']) && $field['final_difference']== ErpDocumentCause::BISINESS_TRIP){
                $sum_sec += 8*60*60;
            }
        }
        return ['fact_sum'=>DateAction::hour_minute($sum_sec)];
    }


    public function getMonthRate(){
        return ['month_rate'=>$this->getDateAction()->monthRate()];
    }
}