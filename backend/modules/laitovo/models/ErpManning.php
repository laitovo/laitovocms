<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/21/17
 * Time: 3:08 PM
 */

namespace backend\modules\laitovo\models;


use common\models\laitovo\ErpDivision;

class ErpManning extends \common\models\laitovo\ErpManning
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id','created_at'], 'integer'],
            [['salary_one', 'salary_two', 'car_compensation', 'surcharge', 'qualification', 'premium', 'kpi_one', 'kpi_two', 'kpi_three'], 'number'],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpPosition::className(), 'targetAttribute' => ['position_id' => 'id']],
            [['division_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpDivision::className(), 'targetAttribute' => ['division_id' => 'id']],
        ];
    }
}