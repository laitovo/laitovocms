<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpNaryad;
use common\models\laitovo\Config;

/**
 * Терминал для перемещения нарядов
 */
class ErpTerminalOld extends Model
{

    public $location_id;
    public $user_id;
    public $naryads=[];
    public $findnaryads=[];
    //Переменная для сообщения, где в данный момент находиться наряд, в случае,
    //если наряд не проходит валидацию
    public $whereNaryad;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id','user_id'], 'required', 'message'=>'{attribute} не указан'],
            [['location_id','user_id'], 'integer'],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => 'id'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => 'id'],
            [['findnaryads'], 'each','rule' => ['integer']],
            [['findnaryads'], 'each','rule' => [
                'exist', 
                'skipOnError' => true, 
                'targetClass' => ErpNaryad::className(),
                // 'filter' => ['location_id' => null],
                'filter' => ['location_id' => $this->location_id],
                'targetAttribute' => 'id'
            ]],
            [['user_id'], 'validateLocation'],
        ];
    }

    public function validateLocation($attribute, $params)
    {
        if ($this->user->location_id!=$this->location_id)
            $this->addError($attribute, Yii::t('app', '{attribute} не зарегистрирован на участке',['attribute'=>$this->getAttributeLabel($attribute)]));
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'location_id' => Yii::t('app', 'Участок'),
            'user_id' => Yii::t('app', 'Сотрудник'),
            'findnaryads' => Yii::t('app', 'Наряд'),
        ];
    }

    public function __construct($config = [])
    {

        $this->location_id=Yii::$app->request->cookies->getValue('ERP_TERMINAL_LOCATION');
        $this->user_id=Yii::$app->request->cookies->getValue('ERP_TERMINAL_USER');
        $this->naryads=Yii::$app->request->cookies->getValue('ERP_TERMINAL_NARYADS')?:[];

        parent::__construct($config);
    }

    public function saveProperties()
    {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ERP_TERMINAL_LOCATION',
            'value' => $this->location_id,
        ]));
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ERP_TERMINAL_USER',
            'value' => $this->user_id,
            'expire' => time() + 15,

        ]));
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ERP_TERMINAL_NARYADS',
            'value' => $this->naryads,
            'expire' => time() + 20,

        ]));
    }

    public function getLocation()
    {
        return $this->location_id ? ErpLocation::findOne($this->location_id) : new ErpLocation;
    }

    public function getUser()
    {
        return $this->user_id ? ErpUser::findOne($this->user_id) : new ErpUser;
    }

    public function toLatin($str) 
    {
        $tr = array(
            "Й"=>"q","Ц"=>"w","У"=>"e","К"=>"r","Е"=>"t","Н"=>"y","Г"=>"u","Ш"=>"i","Щ"=>"o","З"=>"p","Ф"=>"a","Ы"=>"s","В"=>"d","А"=>"f","П"=>"g","Р"=>"h","О"=>"j","Л"=>"k","Д"=>"l","Я"=>"z","Ч"=>"x","С"=>"c","М"=>"v","И"=>"b","Т"=>"n","Ь"=>"m",
            "й"=>"q","ц"=>"w","у"=>"e","к"=>"r","е"=>"t","н"=>"y","г"=>"u","ш"=>"i","щ"=>"o","з"=>"p","ф"=>"a","ы"=>"s","в"=>"d","а"=>"f","п"=>"g","р"=>"h","о"=>"j","л"=>"k","д"=>"l","я"=>"z","ч"=>"x","с"=>"c","м"=>"v","и"=>"b","т"=>"n","ь"=>"m"
        );
        return strtr($str,$tr);
    }


    public function algorithm() {

        $intCountNaryadsOnUser = 0;
        $intCountNaryadsOnUserNeedEnd =0;
        //Получаем наряды на пользователе
        if ($this->location_id && $this->location_id !== Yii::$app->params['erp_okleika'])
        {
            $intCountNaryadsOnUser = ErpNaryad::find()->where(['and',
                ['status'=>ErpNaryad::STATUS_IN_WORK],
                ['location_id'=>$this->location_id],
                ['user_id'=>$this->user_id],
            ])
            ->count();

            //Получаем наряды на пользователе которые он давно получил и должен сдать
            $intCountNaryadsOnUserNeedEnd = ErpNaryad::find()->where(['and',
                ['status'=>ErpNaryad::STATUS_IN_WORK],
                ['location_id'=>$this->location_id],
                ['user_id'=>$this->user_id],
                ['<','updated_at', time() - 60*15],
            ])
            ->count();
        }

        //Инициализируем массив id которые не могут быть выбраны
        $idsNot = [];

        //Инициализируем массив id которые должны быть выбраны
        $idsYes = [];

        if ($intCountNaryadsOnUserNeedEnd)
            return $idsYes;

        //временно по старому
        $config=Config::findOne(2);
        $limitvidacha=$config->json('limitvidacha');
        // if ($this->location_id == Yii::$app->params['erp_okleika']) $limitvidacha = $limitvidacha*2; //если оклейка
        if ($intCountNaryadsOnUser > $limitvidacha) $intCountNaryadsOnUser = $limitvidacha;
        $limitvidacha= $limitvidacha - $intCountNaryadsOnUser;

        //Получим наряды на участке не на этом пользователе.
        $naryadsOnLocationOnOtherUsersLekalo = ErpNaryad::find()->where(['and',
            ['status'=>ErpNaryad::STATUS_IN_WORK],
            ['or',
                ['location_id'=>Yii::$app->params['erp_izgib']],
                ['location_id'=>Yii::$app->params['erp_otk']]
            ],
            ['!=','user_id',$this->user_id],
        ])->all();

        $naryadForCheckSort = null;

        while (count($idsYes) < $limitvidacha) {

            //Найдем первый наряд, который должен быть выбран
            $naryad =  ErpNaryad::find()->where(['and',
                ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
                ['start'=>$this->location_id],
                ['not in','id',$idsNot],
                ['not in','id',$idsYes],
            ])->orderBy('sort,id')
                ->one();

            if (!$naryad)
                break;

            //если это переделка, то тогда их выдавать по одному и впервую очередь
            if ($naryad->sort == 1){
                if ($this->location_id == Yii::$app->params['erp_okleika'])
                {
                    //получим текущего пользователя
                    $user = $this->getUser();
                    //для пользователя получим рабочие места
                    if ($user)
                        $workplaces = $user->recieveWorkplacesByLocation($this->location_id);
                    //для каждого из рабочих мест получим свойство
                    $propertiesWorkplace = [];
                    $properties = [];

                    if ($user && $workplaces)
                        foreach ($workplaces as $workplace) {
                            $propertiesWorkplace[] = $workplace->property;
                        }

                    if (count($propertiesWorkplace))
                        foreach ($propertiesWorkplace as $stringProperty) {
                            $list = explode(',', $stringProperty);
                            foreach ($list as $key => $value) {
                                $properties[] = trim($value);
                            }
                        }

                    //имея массив свойст отбираем наряды
                    if (count($properties) && in_array($naryad->articleTkan, $properties))
                    {
                        $idsYes[] = $naryad->id;
                        break;
                    }

                }else{
                    $idsYes[] = $naryad->id;
                    break;
                }
            }

            if ($naryadForCheckSort !== null && $naryadForCheckSort !== $naryad->sort) {
                $idsNot[] = $naryad->id;
                continue;
            }

            if ($this->location_id == Yii::$app->params['erp_izgib'])
            {
                if ($naryadsOnLocationOnOtherUsersLekalo)
                {
                    foreach ($naryadsOnLocationOnOtherUsersLekalo as $naryadOnLocation)
                    {
                        if ((int)$naryad->lekalo == (int)$naryadOnLocation->lekalo)
                        {
                            $idsNot[] = $naryad->id; //добавляем исключение в наряды
                            break;
                        }
                    }
                }
            }

//            $endTime = microtime(true);
//            $resTime = $endTime - $startTime;
//            var_dump($resTime);

            //логика подбора нарядов на оклейку
            if ($this->location_id == Yii::$app->params['erp_okleika'])
            {
                //получим текущего пользователя
                $user = $this->getUser();
                //для пользователя получим рабочие места
                if ($user)
                    $workplaces = $user->recieveWorkplacesByLocation($this->location_id);
                //для каждого из рабочих мест получим свойство
                $propertiesWorkplace = [];
                $properties = [];

                if ($user && $workplaces)
                    foreach ($workplaces as $workplace) {
                        $propertiesWorkplace[] = $workplace->property;
                    }

                if (count($propertiesWorkplace))
                    foreach ($propertiesWorkplace as $stringProperty) {
                        $list = explode(',', $stringProperty);
                        foreach ($list as $key => $value) {
                            $properties[] = trim($value);
                        }
                    }

                //имея массив свойст отбираем наряды
                if (count($properties) && !in_array($naryad->articleTkan, $properties))
                    $idsNot[] = $naryad->id;

            }

            elseif ($this->location_id == Yii::$app->params['erp_otk'])
            {

                //делаем вещи только для нарядов, которые стандартные рамки
                if ($naryad->window && $naryad->window != 'OT')
                {
                    // если стандартные рамки, тогда производим просмотр комплектации для наряда
                    $equipment = $naryad->equipment;

                    $count = 0;
                    //теперь можем получить наряды из комплектации, которые ждут на отк
                    foreach ($equipment as $key => $value) {
                        $searchNaryad = ErpNaryad::find()
                            ->where(['and',
                                ['order_id' => $naryad->order_id],
                                ['like', 'article', $key.'-%-'.$naryad->carArticle.'-%-%',false],
                                ['start' => Yii::$app->params['erp_otk']],
                                ['status' => ErpNaryad::STATUS_IN_WORK],
                                ['location_id' => Yii::$app->params['erp_dispatcher']],
                            ])
                            ->one();

                        if ($searchNaryad)
                            $count++;
                    }

                    //Далее проверяем, собрана ли комплектация
                    if (count($equipment) !== $count)
                    {
                        //Если комплектация не собрана, дополнительно проводим проверку
                        foreach ($equipment as $key => $value) {
                            $searchExist = ErpNaryad::find()
                                ->where(['and',
                                    ['order_id' => $naryad->order_id],
                                    ['like', 'article', $key.'-%-'.$naryad->carArticle.'-%-%',false],
                                    ['or',['!=','status', ErpNaryad::STATUS_READY],['!=','status',ErpNaryad::STATUS_CANCEL]],
                                    ['!=','location_id',Yii::$app->params['erp_otk']],
                                    ['!=','start',Yii::$app->params['erp_otk']],
                                ])
                                ->one();

                            if ($searchExist)
                                $idsNot[] = $naryad->id;
                        }
                    }

                    //После проверки по комплектации, проверяем по лекалу

                    if ($naryadsOnLocationOnOtherUsersLekalo && !in_array($naryad->id, $idsNot))
                    {
                        foreach ($naryadsOnLocationOnOtherUsersLekalo as $naryadOnLocation)
                        {
                            if ((int)$naryad->lekalo == (int)$naryadOnLocation->lekalo)
                            {
                                $idsNot[] = $naryad->id; //добавляем исключение в наряды
                                break;
                            }
                        }
                    }
                }

            }

            if (!in_array($naryad->id, $idsNot) && !in_array($naryad->id, $idsYes)) {
                $idsYes[] = $naryad->id;
                if ($naryadForCheckSort === null && count($idsYes)) {
                    $naryadForCheckSort = @ErpNaryad::findOne($idsYes[0])->sort;
                }
            }
        }

        return $idsYes;


    }
    
    public function  getNaryadsNaVidachu()
    {
        if ($this->user_id) {

            $idsYes =  $this->algorithm();

            $naryads = ErpNaryad::find()->where(['and',
                ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
                ['start'=>$this->location_id],
                ['in','id',$idsYes],
            ])->orderBy('sort,id')->all();

            return $naryads ? $naryads : 0;
        }
        
        return 0;
    }

    public function  getNaryadsNaVidachuByOne()
    {
        if ($this->user_id) {

            $idsYes =  $this->algorithm();

            $naryads = ErpNaryad::find()->where(['and',
                ['or',['status'=>null],['status'=>''],['status'=>ErpNaryad::STATUS_IN_WORK]],
                ['start'=>$this->location_id],
                ['in','id',$idsYes],
            ])->orderBy('sort,id')->one();

            return $naryads ? $naryads : 0;
        }

        return 0;
    }


}
