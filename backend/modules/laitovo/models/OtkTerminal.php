<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use backend\modules\laitovo\models\ErpUser;

/**
 * Терминал для оприходывания нарядов
 */
class OtkTerminal extends Model
{
    public $user_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'Сотрудник'),
        ];
    }

    public function __construct($config = [])
    {
        $this->user_id=Yii::$app->request->cookies->getValue('OTK_TERMINAL_USER');

        parent::__construct($config);
    }

    public function saveProperties()
    {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'OTK_TERMINAL_USER',
            'value' => $this->user_id,
            // 'expire' => time() + 60*2,
        ]));
    }

    public function deleteProperties()
    {
        $cookies = Yii::$app->response->cookies;
        $cookies->remove('OTK_TERMINAL_USER');
        unset($cookies['OTK_TERMINAL_USER']);
    }

    public function toLatin($str) 
    {
        $tr = array(
            "Й"=>"q","Ц"=>"w","У"=>"e","К"=>"r","Е"=>"t","Н"=>"y","Г"=>"u","Ш"=>"i","Щ"=>"o","З"=>"p","Ф"=>"a","Ы"=>"s","В"=>"d","А"=>"f","П"=>"g","Р"=>"h","О"=>"j","Л"=>"k","Д"=>"l","Я"=>"z","Ч"=>"x","С"=>"c","М"=>"v","И"=>"b","Т"=>"n","Ь"=>"m",
            "й"=>"q","ц"=>"w","у"=>"e","к"=>"r","е"=>"t","н"=>"y","г"=>"u","ш"=>"i","щ"=>"o","з"=>"p","ф"=>"a","ы"=>"s","в"=>"d","а"=>"f","п"=>"g","р"=>"h","о"=>"j","л"=>"k","д"=>"l","я"=>"z","ч"=>"x","с"=>"c","м"=>"v","и"=>"b","т"=>"n","ь"=>"m"
        );
        return strtr($str,$tr);
    }


    public function getUser()
    {
        return $this->user_id ? ErpUser::findOne($this->user_id) : new ErpUser;
    }
}
