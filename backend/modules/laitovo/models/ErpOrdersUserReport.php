<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 26.09.17
 * Time: 10:07
 */

namespace backend\modules\laitovo\models;

use yii\base\Model;
use backend\modules\laitovo\models\DateAction;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class ErpOrdersUserReport extends Model
{
    public $month_to;
    public $year_to;
    public $month_from;
    public $year_from;
    private $country;
    public $compare;
    public $last_year;
    public $client_name;
    private $dealer;

    const DEALER = 'dealer';
    const DISTRIBUTOR = 'distributor';
    const LAITOVO_RU = 'laitovo.ru';
    const LAITOVO_DE = 'laitovo.de';

    static $countrys = [
        1 => 'Россия',
        2 => 'Европа'
    ];
    static $years = [
        0 => '',
        2025 => '2025',
        2024 => '2024',
        2023 => '2023',
        2022 => '2022',
        2021 => '2021',
        2020 => '2020',
        2019 => '2019',
        2018 => '2018',
        2017 => '2017',
        2016 => '2016',
        2015 => '2015',
    ];
    static $months = [
        0 => '',
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь',
    ];

    public function getDealer()
    {
        return !isset($this->dealer) ? true : $this->dealer;
    }

    public function setDealer($value)
    {
        $this->dealer = $value;
    }

    public function getCountry()
    {
        return !isset($this->country) ? 1 : $this->country;
    }

    public function setCountry($value)
    {
        $this->country = $value;
    }

    public function getDateFrom()
    {
        if ($this->month_from && $this->year_from) {
            $date = $this->year_from . '-' . $this->month_from . '-01';
        } elseif ($this->month_from && !$this->year_from) {
            $date = date('Y') . '-' . $this->month_from . '-01';
        } elseif ($this->year_from && !$this->month_from) {
            $date = $this->year_from . '-01-01';
        } else {
            $date = date('Y') . '-01-01';
        }
        return $date ? $date : false;
    }

    public function getDateTo()
    {
        if ($this->month_to && $this->year_to) {
            $first_day = strtotime($this->year_to . '-' . $this->month_to . '-01');
            $date = $this->year_to . '-' . $this->month_to . '-' . date('t', $first_day);
        } elseif ($this->month_to && !$this->year_to) {
            $first_day = strtotime(date('Y') . '-' . $this->month_to . '-01');
            $date = date('Y') . '-' . $this->month_to . '-' . date('t', $first_day);
        } elseif ($this->year_to && !$this->month_to) {
            $first_day = strtotime($this->year_to . '-12-01');
            $date = $this->year_to . '-12-' . date('t', $first_day);
        } else {
            $date = date('Y') . '-12-' . date('t', time());
        }
        return $date ? $date : false;
    }

    public function getSite()
    {
        if ($this->getCountry() == 1){
            return self::LAITOVO_RU;
        } elseif ($this->getCountry() == 2) {
            return self::LAITOVO_DE;
        }
    }

    /**
     * @return array
     */
    public function getDateList()
    {
        return DateAction::datePeriodListMonthArray($this->getDateFrom(), $this->getDateTo());
    }

    public function rules()
    {
        return [
            [['month_to', 'year_to', 'month_from', 'year_from','client_name', 'country'], 'safe'],
            [['dealer', 'compare', 'last_year'], 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'year_to' => 'Конец периода',
            'year_from' => 'Начало периода',
            'dealer' => 'Отображать только диллеров',
            'country' => 'Континент',
            'compare' => 'Сравнить спрошлым годом',
            'last_year' => 'Сортироватать по прошлому году',
            'client_name' => 'Название диллера',
        ];
    }

    public function getActiveQuery($date_from, $date_to, $dateArray)
    {
        $reports = (new \yii\db\Query())
            ->select(['g.client_id','client_name', 'region_name', 'role'])
            ->from('laitovo_orders_user_report AS g')
            ->where(['site' => $this->getSite()])
            ->groupBy('g.client_id,  client_name, region_name, role' );

        if ($this->client_name) {
            $reports->andFilterWhere(['like', 'client_name', '%'.$this->client_name.'%',false]);
        }

        if (!$this->last_year){
            $reports->orderBy('total DESC');
        }

        if ($this->last_year){
            $reports->orderBy('last_total DESC');
        }


        if ($this->getDealer()) {
            $reports->andWhere(['or',
                ['role' =>self::DEALER],
                ['role' =>self::DISTRIBUTOR]
            ]);
        }

        if(!$this->compare){
            $reports->addSelect(['total' => 'SUM(sum_order)']);
            $reports->andWhere(['and',
                ['>','created_at', strtotime($date_from)],
                ['<','created_at', (strtotime($date_to)+86340)],
            ]);
        }

        if($this->compare){

            $subQuery = (new \yii\db\Query())
                ->select(['client_id', 'total' =>'SUM(sum_order)'])
                ->from('laitovo_orders_user_report')
                ->where(['and',
                    ['>', 'created_at', strtotime($date_from)],
                    ['<','created_at', (strtotime($date_to)+86340)],
                    ['site' => $this->getSite()]
                ])
                ->groupBy('client_id');
            $reports->leftJoin(['t'=>$subQuery], 't.client_id = g.client_id');
            $reports->addSelect('total');
            $reports->addGroupBy('total');

            $subQuery = (new \yii\db\Query())
                ->select(['client_id', 'last_total' =>'SUM(sum_order)'])
                ->from('laitovo_orders_user_report')
                ->where(['and',
                    ['>', 'created_at', strtotime($this->lastDateFrom())],
                    ['<','created_at', (strtotime($this->lastDateTo())+86340)],
                    ['site' => $this->getSite()]
                ])
                ->groupBy(' client_id');
            $reports->leftJoin(['lt'=>$subQuery], 'lt.client_id = g.client_id');
            $reports->addSelect('last_total');
            $reports->addGroupBy('last_total');


            foreach ($this->getLastEarDateArray() as $lastDateFrom){
                $random = $this->random_string();
                $dateFromTime = strtotime($lastDateFrom);
                $dateToTime = strtotime(date('Y-m-t',$dateFromTime));
                $subQuery = (new \yii\db\Query())
                    ->select(['client_id', $lastDateFrom =>'SUM(sum_order)'])
                    ->from('laitovo_orders_user_report')
                    ->where(['and',
                        ['>', 'created_at', $dateFromTime],
                        ['<','created_at', ($dateToTime+86340)],
                        ['site' => $this->getSite()]
                    ])
                    ->groupBy('client_id');
                $reports->leftJoin([$random=>$subQuery], $random.'.client_id = g.client_id');
                $reports->addSelect($lastDateFrom);
                $reports->addGroupBy($lastDateFrom);
            }
        }

        foreach ($dateArray as $dateFrom){
            $random = $this->random_string();
            $dateFromTime = strtotime($dateFrom);
            $dateToTime = strtotime(date('Y-m-t',$dateFromTime));
            $subQuery = (new \yii\db\Query())
                ->select(['client_id', $dateFrom =>'SUM(sum_order)'])
                ->from('laitovo_orders_user_report')
                ->where(['and',
                        ['>', 'created_at', $dateFromTime],
                        ['<','created_at', ($dateToTime+86340)],
                        ['site' => $this->getSite()]
                ])
                ->groupBy('client_id');
            $reports->leftJoin([$random=>$subQuery], $random.'.client_id = g.client_id');
            $reports->addSelect($dateFrom);
            $reports->addGroupBy($dateFrom);
        }
        return $reports;
    }


    public function getArrayDataProvider()
    {
        $array = $this->getActiveQuery($this->getDateFrom(), $this->getDateTo(), $this->getDateList());
        return new ActiveDataProvider([
           'query' => $array,
           'sort' => [
               'attributes' => ArrayHelper::merge(['client_name', 'total','region_name'], $this->getDateList())
           ],
           'pagination' => [
               'pageSize' => 50,
          ],
        ]);
    }

    public function random_string($max = 20){
        $rtn = '';
        $chars = explode(" ", "a b c d e f g h i j k l m n o p q r s t u v w x y z");
        for($i = 0; $i < $max; $i++){
            $rnd = array_rand($chars);
            $rtn .= $chars[$rnd];
        }
        return substr(str_shuffle(strtolower($rtn)), 0, $max);
    }

    static function date_lable($date)
    {
        $value = explode('-', $date);
        if(isset($value[0]) && isset($value[1]) && isset(self::$months[(int)$value[1]])){
            return self::$months[(int)$value[1]] . ' ' . $value[0] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ;
        }
    }

    /**Прошлая дата начала
     * @return false|string
     */
    public function lastDateFrom()
    {
        $value = explode('-', $this->getDateFrom());
        $last_year = mktime(0,0,0,$value[1],$value[2],$value[0]-1);
        return date('Y-m-d', $last_year);
    }

    /**прошлая дата конца
     * @return false|string
     */
    public function lastDateTo()
    {
        $value = explode('-', $this->getDateTo());
        $last_year = mktime(0,0,0,$value[1],$value[2],$value[0]-1);
        return date('Y-m-d', $last_year);
    }


    static function lastDate($date)
    {
        $value = explode('-', $date);
        $last_year = mktime(0,0,0,$value[1],$value[2],$value[0]-1);
        return date('Y-m-d', $last_year);
    }

    /**
     * Список месяцов за прошлый год
     * @return array
     */
    public function getLastEarDateArray()
    {
        return DateAction::datePeriodListMonthArray($this->lastDateFrom(), $this->lastDateTo());
    }



}