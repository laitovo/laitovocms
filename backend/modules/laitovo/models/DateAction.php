<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/28/17
 * Time: 3:17 PM
 */

namespace backend\modules\laitovo\models;


use yii\base\Model;
use DatePeriod;
use DateInterval;
use DateTime;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Weekend;

class DateAction extends Model
{
    public $date;
    public $user;


    public function __construct($date, $user=null)
    {
        $this->date = $date;
        $this->user = $user;
    }


    public function getDateFrom()
    {
        if($this->date)
        {
            if(count(explode('-', $this->date))>1){
                $time = explode('-', $this->date);
            }elseif (count(explode('.', $this->date))>1){
                $time = explode('.', $this->date);
            }
            if($time[1])
                return date('Y-m-d',mktime(0, 0, 0, $time[1], 1, $time[2]));
            else
                return date('Y-m-d',mktime(0, 0, 0, date('m'), 1));
        }
        return date('Y-m-d',mktime(0, 0, 0, date('m'), 1));
    }



    public function getDateTo()
    {
        return  $this->date ? date('Y-m-t',strtotime($this->getDateFrom($this->date))) :  date('Y-m-t');
    }

    //получаем массив дат по дням за выбраный промежуток времени
    public function getDateArray()
    {
        $begin = new DateTime($this->getDateFrom());
        $end = new DateTime($this->getDateTo());
        $end = $end->modify( '+1 day' );

        $period = new DatePeriod($begin, new DateInterval('P1D'), $end);
        $arrayOfDates = array_map(
            function($item){
                return $item->format('Y-m-d');
            },
            iterator_to_array($period)
        );
        return $arrayOfDates;
    }



    static function datePeriodListArray($dateFrom,$dateTo){
        $begin = new DateTime($dateFrom);
        $end = new DateTime($dateTo);
        $end = $end->modify( '+1 day' );

        $period = new DatePeriod($begin, new DateInterval('P1D'), $end);
        $arrayOfDates = array_map(
            function($item){
                return $item->format('Y-m-d');
            },
            iterator_to_array($period)
        );
        return $arrayOfDates;
    }

    static function datePeriodListMonthArray($dateFrom,$dateTo){
        $begin = new DateTime($dateFrom);
        $end = new DateTime($dateTo);
        $end = $end->modify( '+1 day' );

        $period = new DatePeriod($begin, new DateInterval('P1M'), $end);
        $arrayOfDates = array_map(
            function($item){
                return $item->format('Y-m-d');
            },
            iterator_to_array($period)
        );
        return $arrayOfDates;
    }



    public function getTitle()
    {

        return 'Табель за ' . $this->getMonth() . ' ' . $this->getEar();
    }


    public function getProductionWorkplace()
    {
        $workplace = ErpLocationWorkplace::find()->joinWith('location l')->where(['l.type'=>ErpLocation::TYPE_PRODUCTION])->all();
        if($workplace)
            return ArrayHelper::map($workplace, 'id', 'id');
    }


    public function resultArray()
    {
        $date_list_document = $this->getDocumentDateArray();

        $resultArr = [
            'final_difference'=>[
                'label'=>'раб. час',
            ],
            'time_in'=>[
                'label'=>'приход',
            ],
            'time_out'=>[
                'label'=>'уход',
            ],
            'in_production'=>[
                'label'=>'сделка',
            ],
        ];
        $sum_fact = 0;

        if($this->getDateArray() && count($this->getDateArray())>0)
        {
            foreach ($this->getDateArray() as $date)
            {
                $key_date = 0;//флаг обозначающий что это число занято документом
                if(count($date_list_document)){
                    foreach ($date_list_document as $key=>$value){
                        if(in_array($date,$value)){
                            $key_date=$key;
                            break;
                        }
                    }
                }
                if($key_date){
                    $resultArr['time_in'][$date]=$key_date;
                    $resultArr['time_out'][$date]=$key_date;
                    $resultArr['in_production'][$date]=$key_date;
                    $resultArr['final_difference'][$date]=$key_date;
                    if((string)$key_date===ErpDocumentCause::BISINESS_TRIP){
                        $sum_fact+=8*3600;
                    }
                }else{
                    $reportIn = ErpLocationWorkplaceRegister::find()->where(['and',
                        ['user_id'=>$this->user],
                        ['action_type'=>ErpLocationWorkplaceRegister::TYPE_IN],
                        ['>','register_at', strtotime($date)],
                        ['<','register_at', strtotime($date)+(60*60*20)]
                    ])->orderBy('id DESC')
                        ->one();

                    if(isset($reportIn) && $reportIn->register_at){
                        $resultArr['time_in'][$date]= date('H.i', $reportIn->register_at);
                    }else{
                        $resultArr['time_in'][$date] = 0;
                    }

                    $reportOut = ErpLocationWorkplaceRegister::find()->where(['and',
                        ['user_id'=>$this->user],
                        ['action_type'=>ErpLocationWorkplaceRegister::TYPE_OUT],
                        ['>','register_at', strtotime($date)],
                        ['<','register_at', strtotime($date)+(60*60*20)]
                    ])->orderBy('id DESC')
                        ->one();

                    if(isset($reportOut) && $reportOut->register_at)
                        $resultArr['time_out'][$date] =  date('H.i', $reportOut->register_at);
                    else $resultArr['time_out'][$date] = 0;

                    if($this->getProductionWorkplace() && $this->getActiveInProduction($date, $this->user))
                    {
                        $resultArr['in_production'][$date] = date("H.i", mktime(0, 0, $this->getActiveInProduction($date, $this->user) ));
                    }else{
                        $resultArr['in_production'] [$date]=0;
                    }
                    if(isset($reportOut) && isset($reportIn) && $reportOut->register_at && $reportIn->register_at)
                    {
                        $final_difference = ($reportOut->register_at - $reportIn->register_at - 3600)-$this->getActiveInProduction($date, $this->user);

                        $resultArr['final_difference'][$date] =  $final_difference >0 ? $this->getHourMinute($final_difference) : 0;

                    }else{
                        $resultArr['final_difference'][$date] = 0;
                    }
                    if(isset($reportOut) && isset($reportIn) && $reportOut->register_at && $reportIn->register_at){
                        $sum_fact +=  ($reportOut->register_at - $reportIn->register_at - 3600) - $this->getActiveInProduction($date, $this->user) >0 ? ($reportOut->register_at - $reportIn->register_at - 3600) - $this->getActiveInProduction($date, $this->user) :0;
                    }
                }
            }

            $resultArr['final_difference']['fact_sum'] = $sum_fact>0 ? $this->getHourMinute($sum_fact) : 0;
            $resultArr['time_in']['fact_sum'] = '';
            $resultArr['time_out']['fact_sum'] = '';
            $resultArr['in_production']['fact_sum'] = '';

            $resultArr['final_difference']['month_rate'] = $this->monthRate();
            $resultArr['time_in']['month_rate'] = '';
            $resultArr['time_out']['month_rate'] = '';
            $resultArr['in_production']['month_rate'] = '';

        }
        return $resultArr;
    }

    /**
     * @param $date
     * время активное на производстве
     */
    public function getActiveInProduction($date, $user)
    {
        $time = 0;
        $maxDate = 0;//наибольшая метка времени

        //ищем все производственные рабочие места локации на которые работник встал в течение дня
        $productionActives = ErpLocationWorkplaceRegister::find()
            ->where(['and',
                ['user_id'=>$user],
                ['in', 'workplace_id', $this->getProductionWorkplace()],
                ['action_type'=>ErpLocationWorkplaceRegister::TYPE_ACTIVE],
                ['>','register_at', strtotime($date)],
                ['<','register_at', strtotime($date)+(60*60*15)]
            ])
            ->orderBy('id ASC')
            ->all();

        if($productionActives)
        {
            foreach ($productionActives as $productionActive)
            {

                //ищем ближайшую по дате запись о том что сотрудник покнул рабочее место или стал неактивным на рабочем месте

                if(isset($productionActive) && $productionActive->workplace_id)
                {
                    $productionInactives = ErpLocationWorkplaceRegister::find()
                        ->where(['and',
                            ['user_id'=>$user],
                            [ 'workplace_id'=>$productionActive->workplace_id],
                            ['action_type'=>ErpLocationWorkplaceRegister::TYPE_INACTIVE],
                            ['>','register_at', $maxDate==0 ? strtotime($date) : $maxDate],
                            ['<','register_at', strtotime($date)+(60*60*20)]
                        ])
                        ->orderBy('id asc')
                        ->one();

                    if(isset($productionInactives))
                    {
                        if($productionActive->register_at && $productionInactives->register_at && $productionActive->register_at>$maxDate)
                        {
                            $maxDate = $productionInactives->register_at;
                            $time+= $maxDate-$productionActive->register_at;
                        }
                        if($productionActive->register_at && $productionInactives->register_at && $productionActive->register_at<$maxDate && $productionInactives->register_at>$maxDate)
                        {
                            $time+= $productionInactives->register_at - $maxDate;
                            $maxDate = $productionInactives->register_at;
                        }
                    }
                }
            }
        }
        return $time;
    }


    static function littleLabel($date)
    {
        $value = explode('-',$date);
        if(isset($value[2]) && $value[2])
            return $value[2];
    }


    public function getHourMinute($time)
    {
        $hour = floor($time/3600);
        $sec = $time - ($hour*3600);
        $min = floor($sec/60);
        return $hour.'.'.$min;
    }


    public function monthRate()
    {
        $result = 0;
        $date_array = $this->getDateArray();
        $date_weekend = ArrayHelper::map(Weekend::find()->all(),'id','weekend');
        foreach ($date_array as $date)
        {
            if(!in_array($date,$date_weekend))
                $result +=8;
        }
        return $result;
    }

    public function getMonth()
    {
        $m='';
        if(count(explode('-', $this->date))>1){
            $value = explode('-', $this->date);
        }elseif (count(explode('.', $this->date))>1){
            $value = explode('.', $this->date);
        }
        if($value[1]){
            switch ($value[1]) {
                case 1:
                    $m = 'январь';
                    break;
                case 2:
                    $m = 'февраль';
                    break;
                case 3:
                    $m = 'март';
                    break;
                case 4:
                    $m = 'апрель';
                    break;
                case 5:
                    $m = 'май';
                    break;
                case 6:
                    $m = 'июнь';
                    break;
                case 7:
                    $m = 'июль';
                    break;
                case 8:
                    $m = 'август';
                    break;
                case 9:
                    $m = 'сентябрь';
                    break;
                case 10:
                    $m = 'октябрь';
                    break;
                case 11:
                    $m = 'ноябрь';
                    break;
                case 12:
                    $m = 'декабрь';
                    break;
            }
        }
        return $m;
    }

    public function getEar(){
        if(count(explode('-', $this->date))>1){
            $value = explode('-', $this->date);
        }elseif (count(explode('.', $this->date))>1){
            $value = explode('.', $this->date);
        }
        if(isset($value[0]))
            return $value[0];
    }



    static function hour_minute($time)
    {
        $hour = floor($time/3600);
        $sec = $time - ($hour*3600);
        $min = floor($sec/60);
        return $hour.'.'.$min;
    }



    static function inSecond($date)
    {
        $sec = 0;

        if(preg_match('/-/',$date)) {
            $value = explode('-', $date);
        } elseif (preg_match('/\./',$date)){
            $value = explode('.', $date);
        }
        if(isset($value[0])){
            $sec += $value[0]*3600;
        }
        if(isset($value[1])){
            $sec += $value[1]*60;
        }
        return $sec;
    }

    public function getDocumentDateArray(){
        $resultArr=[];
        //список документов которые подходят по дате
        $document_list = ErpDocumentCause::find()->where(['and',
                ['user_id'=>$this->user],
                ['or',
                    ['and',
                        ['>=','date_start',$this->date],
                        ['<=','date_start',date('Y-m-t',strtotime($this->date))],
                    ],
                    ['and',
                        ['>=','date_finish',$this->date],
                        ['<=','date_finish', date('Y-m-t',strtotime($this->date))],
                    ]
                ]
            ]
        )->all();

        if(count($document_list)){
            foreach ($document_list as $document){
                if(isset( $resultArr[$document->type])){
                    $resultArr[$document->type]=ArrayHelper::merge($resultArr[$document->type],self::datePeriodListArray($document->date_start,$document->date_finish));
                }else{
                    $resultArr[$document->type]=self::datePeriodListArray($document->date_start,$document->date_finish);
                }
            }
        }
        return $resultArr;
    }
}