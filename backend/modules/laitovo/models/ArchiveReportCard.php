<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 12.09.17
 * Time: 7:57
 */

namespace backend\modules\laitovo\models;


use common\models\laitovo\ErpDivision;
use common\models\laitovo\ErpReportCard;
use common\models\laitovo\ErpUserPosition;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class ArchiveReportCard extends Model
{
    public $year = [
        '2017',
        '2018',
        '2019',
        '2020',
        '2021',
        '2022',
        '2023',
        '2024'
    ];

    public $month = [
        1=>'Январь',
        2=>'Февраль',
        3=>'Март',
        4=>'Апрель',
        5=>'Май',
        6=>'Июнь',
        7=>'Июль',
        8=>'Август',
        9=>'Сентябрь',
        10=>'Октябрь',
        11=>'Ноябрь',
        12=>'Декабрь',
    ];


    public function getDate($year, $month)
    {
        return '01' . '.' . $month . '.' . $year;
    }


    public function getArrayDivision($division,$year,$month)
    {
        $erp_report_card = new ErpReportCard();
        $arhive_array = $erp_report_card->getArchiveReportCard($this->getDate($year,$month),$division);
        return $arhive_array;
    }


    public function getArrayProvider($division,$year,$month)
    {
        $array = $this->getArrayDivision($division,$year,$month);
        return new ArrayDataProvider([
            'allModels'=>$array
        ]);
    }


    public function getDivisions()
    {
        return ArrayHelper::map(ErpDivision::find()->all(), 'id','id');
    }


    public function getDateArray($year, $month)
    {
        $date_action = new DateAction($this->getDate($year,$month),null);
        return $date_action->getDateArray();
    }


    public function littleLabel($day)
    {
        return DateAction :: littleLabel($day);
    }
}