<?php

namespace backend\modules\laitovo\models;

use backend\modules\laitovo\modules\premium\models\Premium;
use common\models\laitovo\Config;
use Yii;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;


/**
 * This is the model class for table "laitovo_rework_act".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $naryad_id
 * @property integer $found_user_id
 * @property integer $made_user_id
 * @property string $deacription_problem
 * @property string $cause_problem
 * @property integer $location_defect
 * @property string $real_quilty
 * @property integer $amount_damage
 * @property integer $deduction
 *
 * @property ErpNaryad $naryad
 * @property LaitovoErpUser $foundUser
 * @property LaitovoErpLocation $locationDefect
 * @property LaitovoErpUser $madeUser
 */
class LaitovoReworkAct extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_rework_act';
    }

      // public $location_start;
       
      public $window;
      public $windowType;
      public $tkan;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount_damage'],'integer'],
            [['deduction'],'integer'],
            [['deduction'],'validateDeduction'],
            [['created_at', 'found_user_id', 'made_user_id', 'location_defect', 'location_start', 'compiled_user'], 'integer'],
            [['found_user_id', 'made_user_id', 'location_defect', 'amount_damage', 'deacription_problem', 'cause_problem','compiled_user' ], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['deacription_problem', 'cause_problem', 'real_quilty','rework_number'], 'string', 'max' => 255],
            [['found_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['found_user_id' => 'id']],
            [['location_defect'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_defect' => 'id']],
            [['location_start'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_start' => 'id']],
            [['made_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['made_user_id' => 'id']],
            [['compiled_user'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['compiled_user' => 'id']],
            [['naryad_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpNaryad::className(), 'targetAttribute' => ['naryad_id' => 'id']],
            [[  'window', 'windowType', 'tkan'], 'safe'],
        ];
    }

     public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                   
                ],
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => Yii::t('app', 'Дата создания'),
            'found_user_id' => Yii::t('app', 'ФИО обнаружевшего брак'),
            'made_user_id' => Yii::t('app', 'ФИО допустившего брак'),
            'deacription_problem' => Yii::t('app', 'Описание дефекта'),
            'cause_problem' => Yii::t('app', 'Причина дефекта'),
            'location_defect' => Yii::t('app', 'Участок, где был допущен дефект'),
            'real_quilty' => Yii::t('app', 'Реальный виновный'),
            'amount_damage' => Yii::t('app', 'Размер ущерба'),
            'deduction' => Yii::t('app', 'Размер удержания с работника'),
            'naryad_id' => Yii::t('app', 'Номер наряда'),
            'naryad.name' => Yii::t('app', 'Номер наряда'),
            'locationDefect.name' => Yii::t('app', 'Участок где допустили несоответствие'),
            'location_start' => Yii::t('app', 'Участок на который отправили наряд'),
            'foundUser.name' => Yii::t('app', 'ФИО обнаружевшего брак'),
            'madeUser.name' => Yii::t('app', 'Виновный'),
            'rework_number' => Yii::t('app', '№ Акта'),
            'compiled_user' => Yii::t('app', 'ФИО составившего акт'),
            'compiledUser.name' => Yii::t('app', 'ФИО составившего акт'),
            
            
        ];
    }

    /**
     * @param $attribute
     */
    public function validateDeduction($attribute)
    {
        $naryad = $this->naryad;
        if ( (!$naryad || !$naryad->isFromSump) && $this->deduction < 100) {
            $this->addError($attribute,'Минимальное значение штрафа - 100 рублей');
        }
    }
//     public function afterSave($insert, $changedAttributes)
//     {
//         $naryad =$this->naryad;
//         $naryad->location_id = Yii::$app->params['erp_dispatcher'];
//         $naryad->status = ErpNaryad::STATUS_IN_WORK;
//         $naryad->sort = 1;
//         if($this->location_defect != Yii::$app->params['erp_dispatcher'])
//         {
//             $naryad->start = $this->location_defect;
//         }
//         $naryad->rework = 1;
//         $naryad->save();         
//     }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updatePremium();
    }

    /**
     * Функция создает премию под текущий акт (или обновляет, если премия уже есть)
     * Премия создается при условии, что наряд текущего акта был на участке "ОТК"
     * Размер премии высчитывается из размера удержания с работника (процент задается в настройках)
     */
    protected function updatePremium()
    {
        $config = Config::findOne(2);
        $percent = $config->json('percent') ? $config->json('percent') : 0;
        if ($this->naryad->location_id != Yii::$app->params['erp_otk']) {
            return;
        }
        $premium = Premium::find()->where(['rework_act_id' => $this->id])->one();
        if (!$premium) {
            $premium = new Premium();
        }
        $premium->rework_act_id = $this->id;
        $premium->user_id = $this->found_user_id;
        $premium->amount = (int)($this->deduction * $percent / 100);
        $premium->is_paid = 1;
        $premium->save();
    }
     
     public function search($params)
     {
         $query = LaitovoReworkAct::find()->joinWith('naryad');
         $dataProvider = new ActiveDataProvider([
              'query' => $query,
          ]);
         
         if($this->load($params))
         {
            if ($this->location_defect)$query->andFilterWhere(['location_defect'=> $this->location_defect]); 
            if ($this->compiled_user)$query->andFilterWhere(['compiled_user'=> $this->compiled_user]); 
            if ($this->location_start)$query->andFilterWhere(['location_start'=> $this->location_start]); 
            if ($this->made_user_id)$query->andFilterWhere(['made_user_id'=> $this->made_user_id]); 
            if ($this->window) $query->andFilterWhere(['like', 'article',$this->window.'-%-%-%-%',false]);
            if ($this->rework_number) $query->andFilterWhere(['like', 'rework_number','%'.$this->rework_number.'%',false]);
            if ($this->tkan)$query->andFilterWhere(['like', 'article','%-%-%-%-'.$this->tkan, false]);
            if ($this->windowType)$query->andFilterWhere(['like', 'article','%-%-%-'.$this->windowType.'-%', false]);
            return $dataProvider;
         }
         
         return $dataProvider;
     }
     public function searchNaryadAct($naryad_id,$params)
     {
         $query = LaitovoReworkAct::find()->where(['naryad_id'=>$naryad_id]);
         $dataProvider = new ActiveDataProvider([
              'query' => $query,
          ]);
         
         if($this->load($params))
         {
            if ($this->location_defect)$query->andFilterWhere(['location_defect'=> $this->location_defect]); 
            if ($this->made_user_id)$query->andFilterWhere(['made_user_id'=> $this->made_user_id]); 
            if ($this->window) $query->andFilterWhere(['like', 'article',$this->window.'-%-%-%-%',false]);
            if ($this->tkan)$query->andFilterWhere(['like', 'article','%-%-%-%-'.$this->tkan, false]);
            if ($this->windowType)$query->andFilterWhere(['like', 'article','%-%-%-'.$this->windowType.'-%', false]);
            return $dataProvider;
         }
         
         return $dataProvider;
     }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoundUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'found_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationDefect()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_defect']);
    }
    public function getLocationStart()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_start']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMadeUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'made_user_id']);
    }
    public function getCompiledUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'compiled_user']);
    }
    
     public function getNaryad()
    {
        return $this->hasOne(ErpNaryad::className(), ['id' => 'naryad_id']);
    }
    public function getName()
    {
        return Yii::t('app', 'Акт №{n}',['n'=>$this->rework_number]);     
    }
}
