<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use backend\modules\laitovo\models\ErpNaryad;
use yii\data\ActiveDataProvider;

/**
* 
*/
class ErpDangerTimeoutReport extends ErpNaryad
{
	public $fullname;
	public $ordername;
	public $locationName;
	public $userName;

    public function rules()
    {
        return [
            [['id','user_id','updated_at', 'author_id', 'updater_id'], 'integer'],
            [['barcode','fullname','article','ordername','locationName','userName','status'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


	public function search($params)
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_WORK],
                    ['status' => self::STATUS_IN_PAUSE],
                ],
                ['IS NOT','location_id',NULL],
                ['IS NOT','user_id',NULL],
                ['<','sort',7],
                ['or',['logist_id' => NULL],['is_new_scheme' => 1]],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }

    	//надо найти наряды, которые находяться в статусе на паузе или в работе и находятся на пользователе
        $query = ErpNaryad::find()
        ->where(['and',
            ['in','laitovo_erp_naryad.id',$ids],
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
	        'attributes' => [
	            'ordername' => [
	                'asc' => ['laitovo_erp_naryad.order_id' => SORT_ASC],
	                'desc' => ['laitovo_erp_naryad.order_id' => SORT_DESC],
	            ],
	            'created_at' => [
	                'asc' => ['laitovo_erp_naryad.created_at' => SORT_ASC],
	                'desc' => ['laitovo_erp_naryad.created_at' => SORT_DESC],
	            ],
	            'sort' => [
	                'asc' => ['laitovo_erp_naryad.sort' => SORT_ASC],
	                'desc' => ['laitovo_erp_naryad.sort' => SORT_DESC],
	            ],
	            'status' => [
	                'asc' => ['laitovo_erp_naryad.status' => SORT_ASC],
	                'desc' => ['laitovo_erp_naryad.status' => SORT_DESC],
	            ],
	            'fullname' => [
	                'asc' => ['laitovo_erp_naryad.barcode' => SORT_ASC],
	                'desc' => ['laitovo_erp_naryad.barcode' => SORT_DESC],
	            ],
	            'article' => [
	                'asc' => ['laitovo_erp_naryad.article' => SORT_ASC],
	                'desc' => ['laitovo_erp_naryad.article' => SORT_DESC],
	            ],
	            'locationName' => [
	                'asc' => ['laitovo_erp_location.name' => SORT_ASC],
	                'desc' => ['laitovo_erp_location.name' => SORT_DESC],
	                'label' => 'Участок'
	            ],
	            'userName' => [
	                'asc' => ['laitovo_erp_user.name' => SORT_ASC],
	                'desc' => ['laitovo_erp_user.name' => SORT_DESC],
	                'label' => 'Работник'
	            ],
	        ]
	    ]);

        $this->load($params);

		$query->andFilterWhere(
		   ['like','laitovo_erp_naryad.barcode',str_replace('/', 'D', $this->fullname)]
		);

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author_id' => $this->author_id,
            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['like', 'article', $this->article]);
        $query->andFilterWhere(['like', 'order_id', $this->ordername]);
        $query->andFilterWhere(['like', 'laitovo_erp_naryad.status', $this->status]);

        $query->joinWith(['location' => function ($q) {
        	$q->where('laitovo_erp_location.name LIKE "%' . $this->locationName . '%"');
    	}]);

        $query->joinWith(['user' => function ($q) {
        	$q->where('laitovo_erp_user.name LIKE "%' . $this->userName . '%"');
    	}]);

        return $dataProvider;
    }

    public function searchLite($params)
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_WORK],
                    ['status' => self::STATUS_IN_PAUSE],
                ],
                ['IS NOT','location_id',NULL],
                ['IS NOT','user_id',NULL],
                ['<','sort',7],
                ['or',['logist_id' => NULL],['is_new_scheme' => 1]],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }
            
        //надо найти наряды, которые находяться в статусе на паузе или в работе и находятся на пользователе
        $query = ErpNaryad::find()
        ->where(['and',
            ['in','laitovo_erp_naryad.id',$ids],
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query->groupBy('user_id'),
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'userName' => [
                    'asc' => ['laitovo_erp_user.name' => SORT_ASC],
                    'desc' => ['laitovo_erp_user.name' => SORT_DESC],
                    'label' => 'Работник'
                ],
            ]
        ]);

        $this->load($params);

        $query->joinWith(['user' => function ($q) {
            $q->where('laitovo_erp_user.name LIKE "%' . $this->userName . '%"');
        }]);

        return $dataProvider;
    }



    public function search2($params)
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_PAUSE],
                ],
                ['IS NOT','location_id',NULL],
                ['IS NOT','user_id',NULL],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }

        //надо найти наряды, которые находяться в статусе на паузе или в работе и находятся на пользователе
        $query = ErpNaryad::find()
        ->where(['and',
            ['in','laitovo_erp_naryad.id',$ids],
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'ordername' => [
                    'asc' => ['laitovo_erp_naryad.order_id' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.order_id' => SORT_DESC],
                ],
                'created_at' => [
                    'asc' => ['laitovo_erp_naryad.created_at' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.created_at' => SORT_DESC],
                ],
                'sort' => [
                    'asc' => ['laitovo_erp_naryad.sort' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.sort' => SORT_DESC],
                ],
                'status' => [
                    'asc' => ['laitovo_erp_naryad.status' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.status' => SORT_DESC],
                ],
                'fullname' => [
                    'asc' => ['laitovo_erp_naryad.barcode' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.barcode' => SORT_DESC],
                ],
                'article' => [
                    'asc' => ['laitovo_erp_naryad.article' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.article' => SORT_DESC],
                ],
                'locationName' => [
                    'asc' => ['laitovo_erp_location.name' => SORT_ASC],
                    'desc' => ['laitovo_erp_location.name' => SORT_DESC],
                    'label' => 'Участок'
                ],
                'userName' => [
                    'asc' => ['laitovo_erp_user.name' => SORT_ASC],
                    'desc' => ['laitovo_erp_user.name' => SORT_DESC],
                    'label' => 'Работник'
                ],
            ]
        ]);

        $this->load($params);

        $query->andFilterWhere(
           ['like','laitovo_erp_naryad.barcode',str_replace('/', 'D', $this->fullname)]
        );

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author_id' => $this->author_id,
            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['like', 'article', $this->article]);
        $query->andFilterWhere(['like', 'order_id', $this->ordername]);
        $query->andFilterWhere(['like', 'laitovo_erp_naryad.status', $this->status]);

        $query->joinWith(['location' => function ($q) {
            $q->where('laitovo_erp_location.name LIKE "%' . $this->locationName . '%"');
        }]);

        $query->joinWith(['user' => function ($q) {
            $q->where('laitovo_erp_user.name LIKE "%' . $this->userName . '%"');
        }]);

        return $dataProvider;
    }

    public function searchLite2($params)
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_PAUSE],
                ],
                ['IS NOT','location_id',NULL],
                ['IS NOT','user_id',NULL],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }
            
        //надо найти наряды, которые находяться в статусе на паузе или в работе и находятся на пользователе
        $query = ErpNaryad::find()
        ->where(['and',
            ['in','laitovo_erp_naryad.id',$ids],
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query->groupBy('user_id'),
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'userName' => [
                    'asc' => ['laitovo_erp_user.name' => SORT_ASC],
                    'desc' => ['laitovo_erp_user.name' => SORT_DESC],
                    'label' => 'Работник'
                ],
            ]
        ]);

        $this->load($params);

        $query->joinWith(['user' => function ($q) {
            $q->where('laitovo_erp_user.name LIKE "%' . $this->userName . '%"');
        }]);

        return $dataProvider;
    }


public function search3($params)
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_WORK],
                ],
                ['IS NOT','location_id',NULL],
                ['!=','location_id',Yii::$app->params['erp_dispatcher']],
                ['IS NOT','user_id',NULL],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }

        //надо найти наряды, которые находяться в статусе на паузе или в работе и находятся на пользователе
        $query = ErpNaryad::find()
        ->where(['and',
            ['in','laitovo_erp_naryad.id',$ids],
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'ordername' => [
                    'asc' => ['laitovo_erp_naryad.order_id' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.order_id' => SORT_DESC],
                ],
                'created_at' => [
                    'asc' => ['laitovo_erp_naryad.created_at' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.created_at' => SORT_DESC],
                ],
                'sort' => [
                    'asc' => ['laitovo_erp_naryad.sort' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.sort' => SORT_DESC],
                ],
                'status' => [
                    'asc' => ['laitovo_erp_naryad.status' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.status' => SORT_DESC],
                ],
                'fullname' => [
                    'asc' => ['laitovo_erp_naryad.barcode' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.barcode' => SORT_DESC],
                ],
                'article' => [
                    'asc' => ['laitovo_erp_naryad.article' => SORT_ASC],
                    'desc' => ['laitovo_erp_naryad.article' => SORT_DESC],
                ],
                'locationName' => [
                    'asc' => ['laitovo_erp_location.name' => SORT_ASC],
                    'desc' => ['laitovo_erp_location.name' => SORT_DESC],
                    'label' => 'Участок'
                ],
                'userName' => [
                    'asc' => ['laitovo_erp_user.name' => SORT_ASC],
                    'desc' => ['laitovo_erp_user.name' => SORT_DESC],
                    'label' => 'Работник'
                ],
            ]
        ]);

        $this->load($params);

        $query->andFilterWhere(
           ['like','laitovo_erp_naryad.barcode',str_replace('/', 'D', $this->fullname)]
        );

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author_id' => $this->author_id,
            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['like', 'article', $this->article]);
        $query->andFilterWhere(['like', 'order_id', $this->ordername]);
        $query->andFilterWhere(['like', 'laitovo_erp_naryad.status', $this->status]);

        $query->joinWith(['location' => function ($q) {
            $q->where('laitovo_erp_location.name LIKE "%' . $this->locationName . '%"');
        }]);

        $query->joinWith(['user' => function ($q) {
            $q->where('laitovo_erp_user.name LIKE "%' . $this->userName . '%"');
        }]);

        return $dataProvider;
    }

    public function searchLite3($params)
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_WORK],
                    ['status' => self::STATUS_IN_PAUSE],
                ],
                ['IS NOT','location_id',NULL],
                ['IS NOT','user_id',NULL],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }
            
        //надо найти наряды, которые находяться в статусе на паузе или в работе и находятся на пользователе
        $query = ErpNaryad::find()
        ->where(['and',
            ['in','laitovo_erp_naryad.id',$ids],
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query->groupBy('user_id'),
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'userName' => [
                    'asc' => ['laitovo_erp_user.name' => SORT_ASC],
                    'desc' => ['laitovo_erp_user.name' => SORT_DESC],
                    'label' => 'Работник'
                ],
            ]
        ]);

        $this->load($params);

        $query->joinWith(['user' => function ($q) {
            $q->where('laitovo_erp_user.name LIKE "%' . $this->userName . '%"');
        }]);

        return $dataProvider;
    }
}