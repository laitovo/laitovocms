<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use common\models\user\User;
use common\models\laitovo\Config;
use common\models\laitovo\JobProductionScheme;
use common\models\laitovo\ProductionScheme;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%laitovo_erp_job_type}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $location_id
 * @property double $rate
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 * @property integer $status
 *
 * @property ErpJobScheme[] $laitovoErpJobSchemes
 * @property ErpLocation $location
 * @property ErpJobTypeRate[] $laitovoErpJobTypeRates
 * @property ErpUsersReport[] $laitovoErpUsersReports
 */
class ErpJobType extends \common\models\LogActiveRecord
{
    private $jobRate;

    const ACTIVE = 1;
    const INACTIVE = 2;

    static $statuses = [
        self::ACTIVE => 'Активный',
        self::INACTIVE => 'Неактивный',
    ];

    public function getStatus()
    {
        return $this->status ? self::$statuses[$this->status] : '';
    }



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_job_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['location_id', 'created_at', 'updated_at', 'author_id', 'updater_id', 'production_scheme_id', 'status'], 'integer'],
            [['rate'], 'number'],
            [['title'], 'string', 'max' => 255],
            [['unit'], 'string', 'max' => 255],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['production_scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionScheme::className(), 'targetAttribute' => ['production_scheme_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Наименование'),
            'location_id' => Yii::t('app', 'Участок'),
            'rate' => Yii::t('app', 'Дневная норма'),
            'unit' => Yii::t('app', 'Единица измерения'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'jobRate' => Yii::t('app', 'Ур. Коэффициент'),
            'production_scheme_id' => Yii::t('app', 'Производственная схема'),
            'status' => Yii::t('app', 'Статус'),
        ];
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpJobSchemes()
    {
        return $this->hasMany(ErpJobScheme::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }
    public function getProductionScheme()
    {
        return $this->hasOne(ProductionScheme::className(), ['id' => 'production_scheme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpJobTypeRates()
    {
        return $this->hasMany(ErpJobTypeRate::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpUsersReports()
    {
        return $this->hasMany(ErpUsersReport::className(), ['job_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobRate()
    {
        $config=Config::findOne(2);
        $averageRate=$config->json('averageRate');
        if ($averageRate)
        {
            return number_format($averageRate / ($this->rate ? $this->rate : $averageRate), 2);
        }
        else
        {
            return 0;
        }
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
    public function getJobProductionScheme()
    {
        return $this->hasMany(JobProductionScheme::className(), ['job_type_id' => 'id']);
    }


    public function getUnitPrice()
    {
        $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
        if($config && $config->json('averageRate') && $config->json('hourrate')!=0 && $this->rate) {
            //     средняя дневная норма                         цена нормированного коэффициента
            $price = $config->json('averageRate') / $this->rate * $config->json('hourrate');
            return round($price,2);
        }

        return 0;

    }

    static function activeList()
    {
        $job_type = self::find()->where(['or',
                ['status' => self::ACTIVE],
                ['status' => null],
            ])->all();
        return ArrayHelper::map($job_type, 'id', 'title');
    }

    public function beforeDelete()
    {
        return false;
    }

}