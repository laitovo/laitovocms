<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use common\models\laitovo\Cars;


/**
 * ErpLocationWorkplacePlanSearch represents the model behind the search form about `backend\modules\laitovo\models\ErpLocationWorkplacePlan`.
 */
class CarsFormSearch extends Model
{
    /**
     * @inheritdoc
     */
    public $filter;
    public $reload;
    public $notclipsed;


    public function rules()
    {
        return [
            [['filter','reload','notclipsed'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'filter' => Yii::t('app', 'Фильтрация по среднему количеству нарядов за 2 последних месяца'),
            'reload' => Yii::t('app', 'Только проверенные'),
            'notclipsed' => Yii::t('app', 'Только без клипс'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cars::find();

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        // ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
                // ['id'=>Yii::$app->request->get('search')],
                ['article'=>Yii::$app->request->get('search')],
            ]);
        }

        //получив фильтр нам необходимо пеербрать каждую машину и получить для нее количество нарядов
        $cars = $query->all();

        $this->load($params);

        $result = [];
        if ($cars)
            foreach ($cars as $car){
                $add = true;
                if ($this->reload && !$car->json('reload'))
                    $add = false;
                if (isset($this->filter) && $car->naryads <= $this->filter)
                    $add = false;
                if ($add && $this->notclipsed){
                    if (($car->json('_fd')['laitovo']['standart']['status']  && !$car->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ПБ'])->one()) 
                    || ($car->json('_fd')['laitovo']['short']['status']     && !$car->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Укороченный','type_clips'=>'Простые','window'=>'ПБ'])->one())
                    || ($car->json('_fd')['laitovo']['smoke']['status']     && !$car->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'ВырезДляКурящих','type_clips'=>'Простые','window'=>'ПБ'])->one())
                    || ($car->json('_fd')['laitovo']['mirror']['status']    && !$car->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'ВырезДляЗеркала','type_clips'=>'Простые','window'=>'ПБ'])->one())
                    || ($car->json('_fd')['chiko']['standart']['status']    && !$car->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ПБ'])->one())
                    || ($car->json('_fd')['chiko']['short']['status']       && !$car->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Укороченный','type_clips'=>'Простые','window'=>'ПБ'])->one())
                    || ($car->json('_fd')['chiko']['smoke']['status']       && !$car->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'ВырезДляКурящих','type_clips'=>'Простые','window'=>'ПБ'])->one())
                    || ($car->json('_fd')['chiko']['mirror']['status']      && !$car->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'ВырезДляЗеркала','type_clips'=>'Простые','window'=>'ПБ'])->one()))
                     $result[] = $car;
                 }elseif($add){
                    $result[] = $car;
                 }
            } 

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
            'sort' => [
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        // $query->andWhere(['and',
        //     // ['>','time_from',Yii::$app->formatter->asTimestamp($this->getCurDateMin())],
        //     // ['<','time_from',Yii::$app->formatter->asTimestamp($this->getCurDateMax())],
        //     // ['>','time_to',Yii::$app->formatter->asTimestamp($this->getCurDateMin())],
        //     ['<','time_to',Yii::$app->formatter->asTimestamp($this->getCurDateMax())],
        // ]);

        // // grid filtering conditions
        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'workplace_id' => $this->workplace_id,
        //     'user_id' => $this->user_id,
        //     'time_to' => $this->time_to,
        //     'created_at' => $this->created_at,
        //     'author_id' => $this->author_id,
        // ]);

        return $dataProvider;
    }
}
