<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\laitovo\models;
use backend\modules\admin\models\User;
use common\models\laitovo\ErpUser;
use backend\modules\laitovo\models\ErpJobType;
use backend\modules\laitovo\models\ErpLocation;


/**
 * Description of ErpAdditionalWork
 *
 * @author User
 */
class  ErpAdditionalWork extends \common\models\laitovo\ErpAdditionalWork
{
     public function rules()
    {
        return [
            [['user_id', 'location_id', 'job_type', 'status', 'created_at', 'finish_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            ['confirmed', 'boolean'],
            [['comment'], 'string', 'max' => 255],
            [['job_price','job_count'], 'number'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['job_type'], 'exist', 'skipOnError' => true, 'targetClass' => ErpJobType::className(), 'targetAttribute' => ['job_type' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }
}
