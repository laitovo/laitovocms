<?php

namespace backend\modules\laitovo\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "laitovo_erp_location_workplace_property".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $title
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property User $author
 * @property User $updater
 * @property LaitovoErpLocationWorkplaceType $type
 */
class ErpLocationWorkplaceProperty extends \common\models\laitovo\ErpLocationWorkplaceProperty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocationWorkplaceType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }
}
