<?php

namespace backend\modules\laitovo\models;

use Yii;

/**
 * This is the model class for table "laitovo_erp_product_performance".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $group_id
 * @property integer $average_time
 * @property integer $count_naryads
 *
 * @property LaitovoErpProductGroup $group
 * @property LaitovoErpUser $user
 */
class ErpProductPerformance extends \common\models\laitovo\ErpProductPerformance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id', 'average_time', 'count_naryads','full_time','day_count'], 'integer'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpProductGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }
}
