<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\laitovo\Config;
use yii\web\NotFoundHttpException;

/**
 * Config
 */
class SettingsForm extends Model
{

    public $name;

    public $fields=[];

    public $config;
    private $_json=[];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['fields'], 'each','rule' => ['string']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
        ];
    }

    public function __construct($config_id=null, $config = [])
    {
        if ($config_id===null){

            $this->config=new Config;

            foreach ($this->config->getFields($config_id) as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=null;
            }

        } elseif ( ($this->config=Config::find()->where(['id'=>$config_id])->one())!==null ) {

            $this->_json=Json::decode($this->config->json ? $this->config->json : '{}');

            $this->name=$this->config->name;

            foreach ($this->config->getFields($config_id) as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=isset($this->_json[$value['fields']['id']['value']]) ? $this->_json[$value['fields']['id']['value']] : '';
            }

        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $json = $this->_json;

            $config = $this->config;

            $config->name=$this->name;

            foreach ($config->getFields($config->id) as $key => $value) {
                $json[$value['fields']['id']['value']]=isset($this->fields[$value['fields']['id']['value']]) ? $this->fields[$value['fields']['id']['value']] : '';
            }
            $config->json=Json::encode($json);

            return $config->save();
        } else {
            return false;
        }
    }
}
