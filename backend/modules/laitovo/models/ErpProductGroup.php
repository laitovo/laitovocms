<?php

namespace backend\modules\laitovo\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "laitovo_erp_product_group".
 *
 * @property integer $id
 * @property string $title
 * @property integer $location_id
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property LaitovoErpLocation $location
 * @property User $updater
 */
class ErpProductGroup extends \common\models\laitovo\ErpProductGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id', 'created_at', 'author_id', 'updated_at', 'updater_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsCount()
    {
        return count($this->products);
    }

}
