<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 18.10.17
 * Time: 15:59
 */

namespace backend\modules\laitovo\models;

use backend\modules\laitovo\modules\premium\models\Premium;
use common\models\laitovo\ErpFine;
use yii\base\Model;
use common\models\laitovo\ErpReportCard;
use yii\helpers\ArrayHelper;
use common\models\laitovo\ErpUser;

class ErpReportCardTableClose extends Model
{
    public $date_from;
    public $date_to;

    public function rules()
    {
        return [
            ['date_from', 'safe'],
            ['date_to', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_from' => 'Начало периода',
            'date_to' => 'Конец периода',
        ];
    }

    /**
     * @return mixed
     */
    public function getDatePeriod()
    {
        if ($this->date_from) {
            return $date = preg_replace('/(^\d{4})-(\d{2})-(\d{2})$/','${1}-${2}-01', $this->date_from);
        }
    }

    /**
     * @return false|int
     */
    public function timeDateFrom()
    {
        if ($this->date_from) {
           return strtotime($this->date_from);
        }
    }

    /**
     * @return int
     */
    public function timeDateTo()
    {
        if ($this->date_to) {
           return strtotime($this->date_to)+(3600*23+59*60+59);
        }
    }

    /**
     * @return array
     */
    public function resultArraySupport()
    {
        $resultArr = [];
        $report_card = $this->getArchiveReportCard();
        $i = 0;

        $userWithNaryads = $this->getUserWithNaryad();
        foreach ($report_card as $report) {
            if($report->user->type == ErpUser::TYPE_SUPPORT) {
                $resultArr[$i]['username'] = @$report->user->name;
                $resultArr[$i]['salary_one'] = @$report->position->salary_one;
                $resultArr[$i]['salary_two'] = @$report->position->salary_two;
                $json = $this->jsonDecode($report->json);
                $resultArr[$i]['sum_hour_in_support'] = round($json->fact_sum,2);
                $resultArr[$i]['sum_day_in_support'] = round($json->fact_sum/8,2);
                $resultArr[$i]['sum_price_in_support_1'] = round(($resultArr[$i]['sum_hour_in_support']/$json->month_rate)*$resultArr[$i]['salary_one'],2);
                $resultArr[$i]['sum_price_in_support_2'] = round(($resultArr[$i]['sum_hour_in_support']/$json->month_rate)*$resultArr[$i]['salary_two'],2);
                $resultArr[$i]['sum_price_in_production'] = round(ErpUsersReport::getPriceNaryadUser($report->user_id, $this->timeDateFrom(), $this->timeDateTo()),2);
                $resultArr[$i]['fine'] = round($this->getFine($report->user_id),2);
                $resultArr[$i]['premium'] = round($this->getPremium($report->user_id),2);
                $resultArr[$i]['total_sum'] =
                    $resultArr[$i]['sum_price_in_support_1']
                    + $resultArr[$i]['sum_price_in_production']
                    + $resultArr[$i]['sum_price_in_support_2']
                    - $resultArr[$i]['fine']
                    + $resultArr[$i]['premium'];
                $i++;
            }
        }
        return $resultArr;
    }


    public function resultArrayProduction()
    {
        $resultArr = [];
        $report_card = $this->getArchiveReportCard();
        $i = 0;

        foreach ($report_card as $report) {
            if($report->user->type == ErpUser::TYPE_PRODUCTION ) {
                $resultArr[$i]['username'] = $report->user->name;
                $resultArr[$i]['salary_one'] = @$report->position->salary_one;
                $resultArr[$i]['salary_two'] = @$report->position->salary_two;
                $json = $this->jsonDecode($report->json);
                $resultArr[$i]['sum_hour_in_support'] = $json->fact_sum;
                $resultArr[$i]['sum_day_in_support'] = round($json->fact_sum/8,2);
                $resultArr[$i]['sum_price_in_1'] =  round(($json->fact_sum/$json->month_rate)*@$report->position->salary_one,2);
                $resultArr[$i]['sum_price_in_production'] = round(ErpUsersReport::getPriceNaryadUser($report->user_id, $this->timeDateFrom(), $this->timeDateTo())-$this->getFine($report->user_id)+$this->getPremium($report->user_id),2);
                $resultArr[$i]['sum_price_in_2'] = $resultArr[$i]['sum_price_in_production']-$resultArr[$i]['sum_price_in_1'];
                $resultArr[$i]['total_sum'] = $resultArr[$i]['sum_price_in_production'];
                $i++;
            }
        }
        return $resultArr;
    }

    public function resultArrayOther()
    {
        $resultArr = [];
        $report_card = $this->getArchiveReportCard();
        $i = 0;

        $userWithNaryads = $this->getUserWithNaryad();
        foreach ($report_card as $report) {
            if($report->user->type == ErpUser::TYPE_SUPPORT && array_key_exists(@$report->user->id, $userWithNaryads) ) {
                $resultArr[$i]['username'] = @$report->user->name;
                $resultArr[$i]['salary_one'] = @$report->position->salary_one;
                $resultArr[$i]['salary_two'] = @$report->position->salary_two;
                $json = $this->jsonDecode($report->json);
                $resultArr[$i]['sum_hour_in_support'] = round($json->fact_sum,2);
                $resultArr[$i]['sum_day_in_support'] = round($json->fact_sum/8,2);
                $resultArr[$i]['sum_price_in_support_1'] = round(($resultArr[$i]['sum_hour_in_support']/$json->month_rate)*($resultArr[$i]['salary_one']),2);
                $resultArr[$i]['sum_price_in_production'] = round(ErpUsersReport::getPriceNaryadUser($report->user_id, $this->timeDateFrom(), $this->timeDateTo()),2);
                $resultArr[$i]['fine_2'] = round($this->getFine($report->user_id),2);
                $resultArr[$i]['premium'] = round($this->getPremium($report->user_id),2);
                $i++;
            }
        }
        return $resultArr;
    }

    public function getArchiveReportCard()
    {
       return $report_card = ErpReportCard::find()->joinWith(['user'])->where(['month_year' =>$this->getDatePeriod()])->all();
    }

    public function jsonDecode($json)
    {
        return json_decode($json);
    }

    public function getUserWithNaryad()
    {
        $user_naryad = ErpUsersReport::find()->where([
            'and',
            ['>=', 'created_at', $this->timeDateFrom()],
            ['<=', 'created_at', $this->timeDateTo()],
        ])->select('user_id')->groupBy('user_id')->all();
        return ArrayHelper::map($user_naryad, 'user_id', 'user_id');
    }

    public function getFine($user_id)
    {
        if ($user_id) {
            $fine = ErpFine::find()->where(['and',
                ['>=', 'created_at', $this->timeDateFrom()],
                ['<=', 'created_at', $this->timeDateTo()],
                ['user_id'=>$user_id]
            ])->sum('amount');
            if ($fine) {
                return $fine;
            }
        }
        return 0;
    }

    public function getPremium($user_id)
    {
        if ($user_id) {
            $premium = Premium::find()->where(['and',
                ['>=', 'created_at', $this->timeDateFrom()],
                ['<=', 'created_at', $this->timeDateTo()],
                ['user_id'=>$user_id],
                ['is_paid'=>true],
            ])->sum('amount');
            if ($premium) {
                return $premium;
            }
        }
        return 0;
    }
}