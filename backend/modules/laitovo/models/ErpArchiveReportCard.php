<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 9/6/17
 * Time: 12:58 PM
 */

namespace backend\modules\laitovo\models;


use common\models\laitovo\ErpReportCard;
use yii\data\ActiveDataProvider;

class ErpArchiveReportCard extends ErpReportCard
{
    public $date;

    public function __construct($date){
        $this->date = $date;
    }

    public function rules()
    {
        return[
            ['date','safe']
        ];
    }

    public function getArchiveData()
    {
        $card_report = self::find()->joinWith('position p')->where(['month_year'=>$this->date])->orderBy('p.division_id ASC');
        $dataProvider = new ActiveDataProvider([
            'query'=>$card_report
        ]);
        return $dataProvider;
    }
}