<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\laitovo\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\laitovo\Material;
use common\models\laitovo\MaterialParameters;
use common\models\laitovo\UnitAccounting;
use common\models\laitovo\UnitSettlement;
use backend\modules\laitovo\models\ErpProductType;
use common\models\laitovo\ProductionScheme;
use common\models\laitovo\MaterialProductionScheme;
use backend\modules\laitovo\models\ErpNaryad;
 
use yii\base\Model;

/**
 * Description of Materials
 *
 * @author User
 */
class Materials extends MaterialParameters
{/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_parameters';
    }

    protected $accounting_unit;
    protected $accounting_unit_price;
    protected $unit_settlement;
    protected $coefficient_settlement;


    /**
     * @inheritdoc
     */
    public function rules()
    {
         
    
        return [
            [['created_at', 'material_id', 'accounting_unit', 'unit_settlement'], 'integer'],
            [[ 'accounting_unit_price', 'coefficient_settlement'], 'required'],
            [['parameter'], 'string', 'max' => 255],     
            [['material_id'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['material_id' => 'id']],
            [['locationId'], 'safe'],
           // [[ 'accounting_unit_price', 'accounting_unit', 'coefficient_settlement', 'unit_settlement', 'name'], 'required'],
            [['parameter' ], 'string'],
            [['accounting_unit_price', 'coefficient_settlement'], 'double'],

        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'material_id' => 'Материал',
            'parameter' => 'Параметр материала',
            'accounting_unit' => 'Учетная единица',
            'accounting_unit_price' => 'Цена учетной единицы',
            'unit_settlement' => 'Расчетная единица',
            'coefficient_settlement' => 'Коэффициент для расчета(взвешивание)',
            'price_settlement' => 'Стоимость расчетной единицы',
            'dateFrom' => 'Начало периода *(00:00:00)',
            'dateTo' => 'Конец периода * (23:59:59)',
            'product_id' => 'Вид продукта',
        ];
    }

    public function getMaterialName()
    {
        return parent::getMaterialName();
    }

    public function getAccounting_unit()
    {
        if($this->getIsNewRecord())return $this->accounting_unit;
        $unit = UnitAccounting::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
        return $unit->accounting_unit;
    }
    public function getAccountingUnitName()
    {
        if($this->getIsNewRecord())return $this->accounting_unit;
        $unit = UnitAccounting::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
        return $unit->accountingUnit->name;
    }

    public function getAccounting_unit_price()
    {
        if($this->getIsNewRecord()) return $this->accounting_unit_price;
        $unit = UnitAccounting::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
        return $unit->accounting_unit_price;
    }

    public function getUnit_settlement()
    {
        if($this->getIsNewRecord())return $this->unit_settlement;
        $unit = UnitSettlement::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
        return $unit->unit_settlement;
    }
    
    public function getCoefficient_settlement()
    {
        if($this->getIsNewRecord()) return $this->coefficient_settlement;
        $unit = UnitSettlement::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
        return $unit->coefficient_settlement;
    }
    
    public function afterSave($insert, $changedAttributes) {
        $accounting = new UnitAccounting();
        $accounting->material_parameter_id = $this->id;
        $accounting->accounting_unit = $this->accounting_unit;
        $accounting->accounting_unit_price = $this->accounting_unit_price;
        $accounting->save();

        $settlement = new UnitSettlement();
        $settlement->material_parameter_id = $this->id;
        $settlement->unit_settlement = $this->unit_settlement;
        $settlement->coefficient_settlement = $this->coefficient_settlement;
        $settlement->save();
        
        parent::afterSave($insert, $changedAttributes);
    }


    /*
     * доводим правило до кондиции что бы искать REGEXP ом в MYSQL
     */

    public function getRegular($rule)
    {
        $value = str_replace('\d+', '([0-9]+)', $rule);
        $value = str_replace('#', '', $value);
        $value = str_replace('\d', '([0-9])', $value);
        $value = str_replace('\w', '([a-z])', $value);

        return $value;
    }

}
