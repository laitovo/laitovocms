<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use common\models\laitovo\Config;
use backend\modules\laitovo\models\ErpNaryad;

/**
 * Терминал для выдачи нарядов
 */
class ErpMainTerminalOld extends Model
{

    public $location_id;
    public $user_id;
    public $naryads;
    public $print=[];
    public $printReestr=[];
    public $group=0;

    public $structure = []; //перекладная функция для реструктуризация последовательности выдачи нарядов

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['user_id'], 'required', 'message'=>'{attribute} не указан'],
            [['naryads'], 'required', 'message'=>'{attribute} не выбраны'],
            [['user_id','location_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => 'id'],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => 'id'],
            [['naryads'], 'each','rule' => ['integer']],
            [['naryads'], 'each','rule' => [
                'exist', 
                'skipOnError' => true, 
                'targetClass' => ErpNaryad::className(),
                'filter' => ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK],['start' => $this->location_id]],
                'targetAttribute' => 'id'
            ]],
            [['group'], 'boolean'],
            [['user_id'], 'validateUsernaryads'],
        ];
    }

    public function validateUsernaryads($attribute, $params)
    {
        if ($this->user_id && $this->location_id) {
            if ((count($naryads=ErpNaryad::find()
                    ->where(['and',
                        ['location_id'=>$this->location_id], /*Все наряды текущей локации*/
                        ['status' => ErpNaryad::STATUS_IN_WORK], /*Кроме нарядов на паузе*/
                        ['user_id' => $this->user_id] /*Кроме нарядов на паузе*/
                    ])->all()) != null && $this->location_id !== Yii::$app->params['erp_okleika']))
            {
                    // Среди этих нарядов проверяем наряды, которые были обновлены давно, то есть более часа назад(определенного времени)
                    // foreach ($naryads as $naryad)
                    // {
                    //     if ($naryad->updated_at < time()-60*15)
                    //     {
                            $this->addError($attribute, Yii::t('app', 'У вас есть не сданные наряды',['attribute'=>$this->getAttributeLabel($attribute)]));
                        //     break;
                        // }
                    // }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'Сотрудник'),
            'naryads' => Yii::t('app', 'Наряды'),
            'group' => Yii::t('app', 'Группировать'),
        ];
    }

    public function __construct($config = [])
    {

        $this->naryads=Yii::$app->request->cookies->getValue('ERP_MAINTERMINAL_NARYADS')?:[];

        parent::__construct($config);
    }


    public function saveProperties()
    {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ERP_MAINTERMINAL_NARYADS',
            'value' => $this->naryads,
            'expire' => time() + 10,

        ]));
    }

    //метод который проверяет могут ли стартануть наряды и дает их подсветить
    public function startNaryads()
    {
        if ($this->group){
            $this->groupNaryads();
        }
        if ($this->validate()){
//            foreach ($this->naryads as $key => $value) {
//                if (($naryad=ErpNaryad::findOne($value))!=null && !$naryad->location_id)
//                    $this->print[]=$value;
//                $naryad->nextPlace($this->user->id);
//
//            }
            return true;
        }
        return false;
    }


        //метод который проверяет могут ли стартануть наряды и дает их подсветить
    public function handOutWorkOrders()
    {
        if ($this->group){
            $this->groupNaryads();
        }
        if ($this->validate()){
           foreach ($this->naryads as $key => $value) {
               if (($naryad=ErpNaryad::findOne($value))!=null && !$naryad->location_id)
                   $this->pri[]=$value;
               $naryad->nextPlace($this->user->id);
           }
            return true;
        }
        return false;
    }

    //метод, которые печатает, перемещая наряды на человека
    public function startNaryadsWithPrint()
    {
        if ($this->group){
            $this->groupNaryads();
        }
        if ($this->validate()){
            $literal = false;
            foreach ($this->naryads as $key => $value) {
                if (($naryad=ErpNaryad::findOne($value))!=null && !$naryad->location_id)
                {
                    if (!$literal) $literal = $naryad->recieveLiteral();
                    $naryad->literal = $literal ? $literal : 1;
                    $naryad->literal_date = time();
                    $this->print[]=$value;
                    $naryad->nextPlace($this->user->id);
                }elseif (($naryad=ErpNaryad::findOne($value))!=null && $naryad->location_id)
                {
                    $this->printReestr[]=$value;
                    $naryad->nextPlace($this->user->id);
                }
            }
            return true;
        }
        return false;
    }


    public function startNaryadsWithPrintReestr()
    {
        if ($this->group){
            $this->groupNaryads();
        }
        if ($this->validate()){
           foreach ($this->naryads as $key => $value) {
               if (($naryad=ErpNaryad::findOne($value))!=null && (!$naryad->location_id || $naryad->location_id == Yii::$app->params['erp_dispatcher']))
                   $this->print[]=$value;
               $naryad->nextPlace($this->user->id);

           }
            return true;
        }
        return false;
    }

    public function groupNaryads($save=false)
    {
        if ($this->validate()){
            
            //условие для переделки
            if ($this->naryads && count($this->naryads) == 1)
                foreach ($this->naryads as $key => $value) {
                    $naryad=ErpNaryad::findOne($value);
                    if ($naryad && $naryad->sort == 1)
                        return false;
                }
            //окончание условия для переделки

            $limitvidacha = count($this->naryads);
            switch ($this->location_id) {
                case Yii::$app->params['erp_clipsi']:
                //если это клипсы, тогда группируем по автомобилю 
                    $naryadsOnUser = $this->getNaryadsOnTerminalUser();

                    foreach ($this->naryads as $key => $value) {
                        $naryad=ErpNaryad::findOne($value);
                        if (!in_array($naryad->id, $this->structure)) {
                            $this->structure[] = $naryad->id;
                        } 
                        $group=ErpNaryad::find()->where(['and',
                            ['like', 'article', '%-%-'.$naryad->carArticle.'-%-%',false], 
                            ['sort' => $naryad->sort],
                            // ['scheme_id'=>$naryad->scheme_id],
                            ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                            ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                            ['start' => $this->location_id],
                        ])->orderBy('id')
                        ->all();
                        $this->rebuildGroupList($group,1);
                    }
                    if ($naryadsOnUser)
                        foreach ($naryadsOnUser as $naryad) {
                            $group=ErpNaryad::find()->where(['and',
                                ['like', 'article', '%-%-'.$naryad->carArticle.'-%-%',false], 
                                // ['scheme_id'=>$naryad->scheme_id],
                                ['sort' => $naryad->sort],
                                ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                ['start' => $this->location_id],
                            ])->orderBy('id')
                            ->all();
                            $this->rebuildGroupList($group,1);
                        } 

                    //Добавим здесь проверку по времени выполнени каждого наряда
                    //Сначала нам надо обойти структуру и для каждого наряда структуры получить время
                    $this->sliceNaryadsByLimit();
                    return true;


                /*Если участок "ИЗГИБ" - тогда алгоритм группировки:*/
                case Yii::$app->params['erp_izgib']:
                    //перед всеми группировками нам необходимо получить наряды, которые сейчас на пользователе
                    //их мы используем позже на случай, если выдаа будет осщуествляться по одному наряду
                    $naryadsOnUser = $this->getNaryadsOnTerminalUser();

                    //сначала идет группировка по продукту
                    //Получаем группы продуктов

                    //Здесь осуществляем всю обработку нарядов
                    foreach ($this->naryads as $key => $value) {
                        //Для начала ищем наряд, соответсвующий даному ID
                        $naryad=ErpNaryad::findOne($value);
                        //Далее достаем схему наряда
                        // //находим продукт используя метод наряда
                        $product = $naryad->searchProduct();
                        //находим группу по методу продука через локацию
                        if ($product)
                            $groupsOfProducts = $product->recieveGroupByLocation($naryad->start);
                        //Пытаемся достать запись о производительности для сотрудника и группы
                        $ids=[];
                        if (isset($groupsOfProducts) && $groupsOfProducts) 
                        {

                            //Опишем алгоритм ищем все наряды которые должны выйти на участок
                            $naryads=ErpNaryad::find()->where(['and',
                                ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                ['sort' => $naryad->sort],
                                ['start' => $this->location_id],
                            ])->orderBy('id')
                            ->all();

                            foreach ($naryads as $naryad) {
                                $product = $naryad->searchProduct();
                                if (is_object($product))
                                {
                                    $groupsOfProducts = $product->recieveGroupByLocation($naryad->start);
                                    if ($groupsOfProducts == $groupsOfProducts)
                                    {
                                        $ids[$naryad->id] = $naryad->id;
                                    }
                                }
                            }
                        }
                        //Добавляем разумееться наряд в нашу структуру
                        if (!in_array($naryad->id, $this->structure)) {
                            $this->structure[] = $naryad->id;
                        } 
                        //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту)
                        $group=ErpNaryad::find()->where(['in','id',$ids])->orderBy('id')
                        ->all();

                        $this->rebuildGroupList($group,1);
                    }

                    //теперь тоже самое проделываю для нарядов на человеке если на нем они есть, чтобы группировка работала и при получении
                    //по одному наряду
                    if ($naryadsOnUser)
                        foreach ($naryadsOnUser as $naryad) {
                            $product = $naryad->searchProduct();
                            //находим группу по методу продука через локацию
                            if ($product)
                                $groupsOfProducts = $product->recieveGroupByLocation($naryad->start);
                            //Пытаемся достать запись о производительности для сотрудника и группы
                            $ids=[];
                            if (isset($groupsOfProducts) && $groupsOfProducts)
                            {
                                //Опишем алгоритм ищем все наряды которые должны выйти на участок
                                $naryads=ErpNaryad::find()->where(['and',
                                    ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                    ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                    ['sort' => $naryad->sort],
                                    ['start' => $this->location_id],
                                ])->orderBy('id')
                                ->all();

                                foreach ($naryads as $naryad) {
                                    $product = $naryad->searchProduct();
                                    if ($product)
                                        $groupsOfProducts = $product->recieveGroupByLocation($naryad->start);
                                    if (isset($groupsOfProducts) && $groupsOfProducts == $groupsOfProducts)
                                    {
                                        $ids[$naryad->id] = $naryad->id;
                                    }
                                }
                            }
                            //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту)
                            $group=ErpNaryad::find()->where(['in','id',$ids])->orderBy('id')
                            ->all();

                            $this->rebuildGroupList($group,0);

                        }  

                    //После того как сформировали структуру проверяя по продуктам, запищем результат
                    $this->naryads = $this->structure;

                    //Теперь делаем следующую группировку - это группировка по лекалу
                    //Для начала обнулим структуру, а затем соберем ее заново
                    $this->structure = [];

                    //Обходим уже сгруппированные по продукту наряды
                    foreach ($this->naryads as $key => $value) {
                        //Для начала ищем наряд, соответсвующий даному ID
                        $naryad=ErpNaryad::findOne($value);
                        //Добовляем наряд в пересматриваемую струкутуру
                        if (!in_array($naryad->id, $this->structure)) {
                            $this->structure[] = $naryad->id;
                        } 
                        //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту)
                        $group=ErpNaryad::find()->where(['and',
                            ['like', 'json', '%"lekalo":"'.(int)$naryad->lekalo.'"%',false],
                            ['scheme_id'=>$naryad->scheme_id], 
                            ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                            ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                            ['sort' => $naryad->sort],
                            ['start' => $this->location_id],
                        ])->orderBy('id')
                        ->all();
                        $this->rebuildGroupList($group,1);
                    }

                    //теперь тоже самое проделываю для нарядов на человеке если на нем они есть, чтобы группировка работала и при получении
                    //по одному наряду
                    if ($naryadsOnUser)
                        foreach ($naryadsOnUser as $naryad) {
                            $group=ErpNaryad::find()->where(['and',
                                ['like', 'json', '%"lekalo":"'.(int)$naryad->lekalo.'"%',false],
                                ['scheme_id'=>$naryad->scheme_id], 
                                ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                ['sort' => $naryad->sort],
                                ['start' => $this->location_id],
                            ])->orderBy('id')
                            ->all();
                            $this->rebuildGroupList($group,0);

                        }    
                    
                    $some = [];
                    $lekals = $this->checkLekalo();
                    foreach ($this->structure as $value) {
                        $notLekal=ErpNaryad::findOne($value);
                        if ($notLekal && !in_array((int)$notLekal->lekalo, $lekals)){
                            $some[] =  $value;
                        }
                    }
                    $this->structure = $some;
                    //Добавим здесь проверку по времени выполнени каждого наряда
                    //Сначала нам надо обойти структуру и для каждого наряда структуры получить время
                    $this->sliceNaryadsByLimit();

                    return true;

                /*Если участок "ОКЛЕЙКА" - тогда алгоритм группировки:*/
                case Yii::$app->params['erp_okleika']:
                    //Добавим здесь проверку по времени выполнени каждого наряда
                    //Сначала нам надо обойти структуру и для каждого наряда структуры получить время
                    foreach ($this->naryads as $key => $value) {
                        $naryad=ErpNaryad::findOne($value);
                        if (!in_array($naryad->id, $this->structure)) {
                            $this->structure[] = $naryad->id;
                        } 
                    }

                    $this->sliceNaryadsByLimit();
                    return true;
                
                /*Если участок "ШВЕЙКА" - тогда алгоритм группировки:*/
                case Yii::$app->params['erp_shveika']:
                    //перед всеми группировками нам необходимо получить наряды, которые сейчас на пользователе
                    //их мы используем позже на случай, если выдаа будет осщуествляться по одному наряду
                    $naryadsOnUser = $this->getNaryadsOnTerminalUser();

                    //сначала идет группировка по продукту
                    //Получаем группы продуктов
                    $groupsOfProducts = $this->getGroupsOfProducts(Yii::$app->params['erp_shveika']);
                    //Дальнейшая работа по группировке по видам продукта идет только в случае, если группы для данной локации указазны
                    if (isset($groupsOfProducts) && count($groupsOfProducts))
                    {
                        //Здесь осуществляем всю обработку нарядов
                        foreach ($this->naryads as $key => $value) {
                            $searchProduct = 1;
                            //Для начала ищем наряд, соответсвующий даному ID
                            $naryad=ErpNaryad::findOne($value);
                            //Далее достаем схему наряда
                            $product = $naryad->scheme_id;
                            //Смотрим какой группе продуктов принадлежит данный продукт на данном участке
                            foreach ($groupsOfProducts as $keypr => $valuepr) {
                                //перебирая возможные группы продукта на данном этапе ищу совпадения до первого
                                //и получаю ключ массива к группе, который подставим в дальнейшем в запросе
                                if (in_array($product, $valuepr,true))
                                {
                                  $searchProduct = $keypr;  
                                  break;
                                } 
                                
                            }
                            //Добавляем разумееться наряд в нашу структуру
                            if (!in_array($naryad->id, $this->structure)) {
                                $this->structure[] = $naryad->id;
                            } 
                            //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту по литере)
                            $group=ErpNaryad::find()->where(['and',
                                ['in','scheme_id',$groupsOfProducts[$searchProduct]],
                                ['literal' => $naryad->literal], //вставка для группировке по литере
                                ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                ['sort' => $naryad->sort],
                                ['start' => $this->location_id],
                            ])->orderBy('id')
                            ->all();
                            $this->rebuildGroupList($group,1);

                            //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту)
                            $group=ErpNaryad::find()->where(['and',
                                ['in','scheme_id',$groupsOfProducts[$searchProduct]],
                                ['not in','id',$this->structure],//нарядов, которых нет
                                ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                ['sort' => $naryad->sort],
                                ['start' => $this->location_id],
                            ])->orderBy('id')
                            ->all();
                            $this->rebuildGroupList($group,1);
                        }

                        //теперь тоже самое проделываю для нарядов на человеке если на нем они есть, чтобы группировка работала и при получении
                        //по одному наряду
                        if ($naryadsOnUser)
                            foreach ($naryadsOnUser as $naryad) {
                                //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту по литере)
                                $searchProduct = 1;
                                $product = $naryad->scheme_id;
                                //Смотрим какой группе продуктов принадлежит данный продукт на данном участке
                                foreach ($groupsOfProducts as $keypr => $valuepr) {
                                    //перебирая возможные группы продукта на данном этапе ищу совпадения до первого
                                    //и получаю ключ массива к группе, который подставим в дальнейшем в запросе
                                    if (in_array($product, $valuepr,true)) 
                                    {
                                        $searchProduct = $keypr;
                                        break;
                                    }
                                }
                                $group=ErpNaryad::find()->where(['and',
                                    ['in','scheme_id',$groupsOfProducts[$searchProduct]],
                                    ['literal' => $naryad->literal], //вставка для группировке по литере
                                    ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                    ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                    ['sort' => $naryad->sort],
                                    ['start' => $this->location_id],
                                ])->orderBy('id')
                                ->all();
                                $this->rebuildGroupList($group,1);

                                //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту)
                                $group=ErpNaryad::find()->where(['and',
                                    ['in','scheme_id',$groupsOfProducts[$searchProduct]],
                                    ['not in','id',$this->structure],//
                                    ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                    ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                    ['sort' => $naryad->sort],
                                    ['start' => $this->location_id],
                                ])->orderBy('id')
                                ->all();
                                $this->rebuildGroupList($group,1);
                            }    
                    }

                    //Добавим здесь проверку по времени выполнени каждого наряда
                    //Сначала нам надо обойти структуру и для каждого наряда структуры получить время
                    $this->sliceNaryadsByLimit();

                    return true;

                /*Если участок "ЛЕЙБЛ" - тогда алгоритм группировки:*/
                case Yii::$app->params['erp_label']:
                    //Добавим здесь проверку по времени выполнени каждого наряда
                    //Сначала нам надо обойти структуру и для каждого наряда структуры получить время
                    foreach ($this->naryads as $key => $value) {
                        $naryad=ErpNaryad::findOne($value);
                        if (!in_array($naryad->id, $this->structure)) {
                            $this->structure[] = $naryad->id;
                        } 
                    }

                    $this->sliceNaryadsByLimit();
                    return true;

                /*Если участок "ОТК" - тогда алгоритм группировки:*/
                case Yii::$app->params['erp_otk']:
                    //их мы используем позже на случай, если выдаа будет осщуествляться по одному наряду
                    $naryadsOnUser = $this->getNaryadsOnTerminalUser();

                     //Группируем наряды по комплектации
                    foreach ($this->naryads as $key => $value) {
                        //Для начала ищем наряд, соответсвующий даному ID
                        $naryad=ErpNaryad::findOne($value);
                        //Добовляем наряд в пересматриваемую струкутуру
                        if (!in_array($naryad->id, $this->structure)) {
                            $this->structure[] = $naryad->id;
                        } 
                        //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту)
                        $group = ErpNaryad::find()
                        ->where(['and',
                            ['order_id' => $naryad->order_id],
                            ['like', 'article', '%-%-'.$naryad->carArticle.'-%-%',false],
                            ['sort' => $naryad->sort],
                            ['not like', 'article', 'OT-%-%-%-%',false],
                            ['or',
                                ['and',
                                    ['or',['status' => null],['status' => '']],
                                    ['location_id' => null],
                                    ['start' => Yii::$app->params['erp_otk']],
                                ],
                                ['and',
                                    ['status' => ErpNaryad::STATUS_IN_WORK],
                                    ['start' => Yii::$app->params['erp_otk']],
                                    ['location_id' => Yii::$app->params['erp_dispatcher']],
                                ],
                            ],
                        ])
                        ->orderBy('id')
                        ->all();
                        $this->rebuildGroupList($group,1);
                    }

                    //теперь тоже самое проделываю для нарядов на человеке если на нем они есть, чтобы группировка работала и при получении
                    //по одному наряду
                    if ($naryadsOnUser)
                        foreach ($naryadsOnUser as $naryad) {
                            $group = ErpNaryad::find()
                            ->where(['and',
                                ['order_id' => $naryad->order_id],
                                ['like', 'article', '%-%-'.$naryad->carArticle.'-%-%',false],
                                ['sort' => $naryad->sort],
                                ['not like', 'article', 'OT-%-%-%-%',false],
                                ['or',
                                    ['and',
                                        ['or',['status' => null],['status' => '']],
                                        ['location_id' => null],
                                        ['start' => Yii::$app->params['erp_otk']],
                                    ],
                                    ['and',
                                        ['status' => ErpNaryad::STATUS_IN_WORK],
                                        ['start' => Yii::$app->params['erp_otk']],
                                        ['location_id' => Yii::$app->params['erp_dispatcher']],
                                    ],
                                ],
                            ])
                            ->orderBy('id')
                            ->all();

                            $this->rebuildGroupList($group,0);

                        }    

                    //После того как сформировали структуру проверяя по продуктам, запищем результат
                    $this->naryads = array_slice($this->structure, 0, $limitvidacha, true);
                    //Теперь делаем следующую группировку - это группировка по лекалу
                    //Для начала обнулим структуру, а затем соберем ее заново
                    $this->structure = [];

                    //Обходим уже сгруппированные по заказам наряды
                    foreach ($this->naryads as $key => $value) {
                        //Для начала ищем наряд, соответсвующий даному ID
                        $naryad=ErpNaryad::findOne($value);
                        //Добовляем наряд в пересматриваемую струкутуру
                        if (!in_array($naryad->id, $this->structure)) {
                            $this->structure[] = $naryad->id;
                        } 
                        //Ищем наряды, которые могут быть подтянуты (сгруппированы по данному признаку, а именно по продукту)
                        $group=ErpNaryad::find()->where(['and',
                            ['like', 'json', '%"lekalo":"'.(int)$naryad->lekalo.'"%',false],
                            ['scheme_id'=>$naryad->scheme_id], 
                            ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                            ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                            ['sort' => $naryad->sort],
                            ['start' => $this->location_id],
                        ])->orderBy('id')
                        ->all();
                        $this->rebuildGroupList($group,1);
                    }

                    //теперь тоже самое проделываю для нарядов на человеке если на нем они есть, чтобы группировка работала и при получении
                    //по одному наряду
                    if ($naryadsOnUser)
                        foreach ($naryadsOnUser as $naryad) {
                            $group=ErpNaryad::find()->where(['and',
                                ['like', 'json', '%"lekalo":"'.(int)$naryad->lekalo.'"%',false],
                                ['scheme_id'=>$naryad->scheme_id], 
                                ['or',['location_id'=>null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],
                                ['sort' => $naryad->sort],
                                ['start' => $this->location_id],
                            ])->orderBy('id')
                            ->all();
                            $this->rebuildGroupList($group,0);

                        }  

                    $this->naryads = $this->structure;
                    $this->structure = [];

                    //Добавим здесь проверку по времени выполнени каждого наряда
                    //Сначала нам надо обойти структуру и для каждого наряда структуры получить время
                    foreach ($this->naryads as $key => $value) {
                        $naryad=ErpNaryad::findOne($value);
                        if (!in_array($naryad->id, $this->structure)) {
                            $this->structure[] = $naryad->id;
                        } 
                    }
                    
                    $this->sliceNaryadsByLimit();
                    return true;

                default:
                    return true;
            }
            return true;
        }
        return false;
    }

    public function getUser()
    {
        return $this->user_id ? ErpUser::findOne($this->user_id) : new ErpUser;
    }
    
    /**
     * Функция для получения нарядов на пользователе, который сейчас работает с терминалом
     * 
     * Если на пользоватле нет нарядов - функция возваращает null
     * Если не указан пользователь или локация - функция возвращает null
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version 1.0
     * @global $this->location_id
     * @global $this->user_id
     * @return array of objects ErpNaryad       
     */
    private function getNaryadsOnTerminalUser()
    {
        if (!$this->location_id || !$this->user_id){
            return null;
        }
        
        $naryadsOnUser = ErpNaryad::find()->where(['and',
            ['status'=>ErpNaryad::STATUS_IN_WORK],
            ['location_id'=>$this->location_id],
            ['user_id'=>$this->user_id],
            ['>','updated_at', time() - 60*15],
            ['<','updated_at', time()],
        ])
        ->all();
        
        return $naryadsOnUser;
    }
    
    /**
     * Процедура, реализующая переформирование структуру нарядов терминала
     * 
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version 1.0
     * @global $this->location_id
     * @param array $group Массив нарядов, по основанию которых производить группировку
     * @param int $group Массив нарядов, по основанию которых производить группировку
     */
    private function rebuildGroupList($group,$n)
    {
        if ($group && count($group)>$n){
            foreach ($group as $row) {
                if (!in_array($row->id, $this->naryads)) {
                    $this->naryads[]=$row->id;
                }
                if (!in_array($row->id, $this->structure)) {
                    $this->structure[]=$row->id;
                }
            }
        }
    }
    
    /**
     * Процедура, которая возвращает урезает количество нарядов на выдачу согласно заданного лимита
     * 
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version 1.0
     */
    private function sliceNaryadsByLimit()
    {
        $config=Config::findOne(2);
        $flag = 0;
        $fullTime = 0;
        $maxTime = $config->json('maxLimitTimeNaryads');
        foreach ($this->structure as $key => $value) {
            //получаем наряда
            $naryad=ErpNaryad::findOne($value);
            $product = $naryad->searchProduct();
            $group = $product ? $product->recieveGroupByLocation($naryad->start) : null;
            $row = ErpProductPerformance::find()
                ->where(['group_id' => $group])
                ->andWhere(['user_id' => $this->user_id])   
                ->one();

            $configTime=$config->json('timeOnNaryad');
            $time = $row ? ($row->average_time ? $row->average_time : ($configTime ? $configTime : 1440)) : ($configTime ? $configTime : 1440);
            $fullTime += $time;
            $flag = $key;
            if ($fullTime > ($maxTime ? $maxTime : 7200))
            {
                break;
            }
        }

        $type = $config->json('typeVidacha');
        $limit= $config->json('limitvidacha');
        if ($type && $type == 'По лимиту' && $limit) {
            $flag = $limit;
        }
        $this->naryads = array_slice($this->structure, 0, ($flag ? $flag : 1), true);
    }

    
    /**
     * Функция, позволяющая получить лекала, на которые нельзя группировать продукцию
     * 
     * @param type $location_id
     * @return type
     */
    public function checkLekalo()
    {
        $result = [];
        //Получим наряды на участке не на этом пользователе.
        $naryadsOnLocationOnOtherUsersLekalo = ErpNaryad::find()->where(['and',
            ['status'=>ErpNaryad::STATUS_IN_WORK],
            ['or',
                ['location_id'=>Yii::$app->params['erp_izgib']],
                ['location_id'=>Yii::$app->params['erp_otk']]
            ],
            ['!=','user_id',$this->user_id],
        ])->all();
        
        if ($naryadsOnLocationOnOtherUsersLekalo) {
            foreach ($naryadsOnLocationOnOtherUsersLekalo as $naryad) {
                $result[] = (int)$naryad->lekalo;
            }
        }
        
        return $result;
    }


    public function getGroupsOfProducts($location_id)
    {
        //Здесь должны быть описаны все группы продуктов по участкам
        //Создаем массив групп, в которые может входить продукт
        //Структруа массива будет следующая МассивГрупп[Локация(участок)][группа продуктов][массив продуктов]
        $groupsOfProducts = [];
        /*
            Группа 1 для изгиба:
            - Основной экран (ПБ, ЗБ, ЗФ)
            - Основной экран (ЗШ, ПФ)
            - Chiko magnet
            - Chiko/Laitovo на магнитах
        */
        $groupsOfProducts[Yii::$app->params['erp_izgib']][] = [1,2,3,18];
        /*
            Группа 1 для изгиба:
            - Доп. Сдвижной
        */
        $groupsOfProducts[Yii::$app->params['erp_izgib']][] = [4];

        /*
            Группа 1 для швейки:
            - Chiko magnet
            - Chiko/Laitovo на магнитах
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [18,3];
        /*
            Группа 2 для швейки:
            - Основной экран (ПБ, ЗБ, ЗФ)
            - Основной экран (ЗШ, ПФ)
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [1,2];
        /*
            Группа 3 для швейки:
            - Доп. Сдвижной
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [4];
        /*
            Группа 4 для швейки:
            - Бескаркасный (ПБ, ЗБ, ЗФ)
            - Бескаркасный (ПШ)
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [5,6];
        /*
            Группа 5 для швейки:
            - Перегородка для органайзера
            - Дно для органайзера
            - Органайзер для багажника
            - Сумка для экранов
            - Чехол на сидение для животных
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [10,11,12,13,14];
        /*
            Группа 6 для швейки:
            - Чехол для сноуборда
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [9];
        /*
            Группа 7 для швейки:
            - Чехол для спинки переднего сидения
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [8];
        /*
            Группа 8 для швейки:
            - Чехлы для колес
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [7];
        /*
            Группа 8 для швейки:
            - Доп комплект застежки-липцчки
        */
        $groupsOfProducts[Yii::$app->params['erp_shveika']][] = [15];

        return isset($groupsOfProducts[$location_id]) ? $groupsOfProducts[$location_id] : [];
    }


}
