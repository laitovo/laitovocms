<?php

namespace backend\modules\laitovo\models;

use Yii;
use Yii\base\Model;
use backend\modules\laitovo\models\ErpNaryad;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;

/**
* 
*/
class ErpTextileReport extends Model
{
	#Properties
	private $dateFrom;
	private $dateTo;
	private $countNaryads = [];

	public $location;

	public function rules()
	{
        return [
            [['dateFrom', 'dateTo'], 'date'],
            [['location'], 'integer'],


        ];
	}

	public function setDateFrom($value)
	{
		$this->dateFrom = Yii::$app->formatter->asTimestamp($value);
	}

	public function getDateFrom()
	{
		return $this->dateFrom;
	}

	public function setDateTo($value)
	{
		$this->dateTo = Yii::$app->formatter->asTimestamp($value);
	}

	public function getDateTo()
	{
		return $this->dateTo;
	}

	 public function attributeLabels()
    {
        return [
            'dateFrom' => Yii::t('app', 'Начало периода *(00:00:00)'),
            'dateTo' => Yii::t('app', 'Конец периода * (23:59:59)'),
            'location' => Yii::t('app', 'Участок'),
        ];
    }

	public function extractData()
	{
		#В делаем выборку нарядов в заданом временном интервале
		$Naryads = ErpNaryad::find()
		   	     ->andWhere(['>','updated_at',$this->dateFrom])
			   	 ->andWhere(['<','updated_at',time()])
			   	 ->orderBy('id')
			   	 ->all();

	   	$arTextile = [];

		#Перебираем данную выборку нарядов с целью получения артикула и подсчетка по каждому артикулу количества
		foreach ($Naryads as $naryad) {
			if ($this->location) {
	            $arJsonValue = Json::decode($naryad->json);
                if ($arJsonValue['log'])
                    for ($i=0;$i<count($arJsonValue['log']);$i++)
                    {
                        if (($arJsonValue['log'][$i]['location_from'] == ErpLocation::findOne($this->location)->name) &&
                            ($arJsonValue['log'][$i]['date'] > $this->dateFrom) &&
                            ($arJsonValue['log'][$i]['date'] < ($this->dateTo + (60*60*24-1))))
                        {
                            $value=explode('-', $naryad['article']);

                            @$value[4] = str_replace(2, '№2 "Двушка"', @$value[4]);
                            @$value[4] = str_replace(3, '№3 "Бифлекс"', @$value[4]);
                            @$value[4] = str_replace(5, '№5 "Chiko" Темная + Светлая', @$value[4]);
                            @$value[4] = str_replace(1, '№1 "Москитная сетка"', @$value[4]);
                            @$value[4] = str_replace(4, '№1,5 "Полторашка"', @$value[4]);
                            @$value[4] = str_replace(8, '№ 1,25 "Китайская ткань"', @$value[4]);

                            if(strtoupper(@$value[0]) == 'BW' && @$value[4] == '№5 "Chiko" Темная + Светлая') {
                                $index = '№5 "Chiko" ЗШ';
                                $arTextile[$index]['type'] = $index;
                                isset($arTextile[$index]['count']) ? $arTextile[$index]['count']++ : $arTextile[$index]['count'] = 1;
                            }elseif(strtoupper(@$value[3]) == 55 && @$value[4] == '№5 "Chiko" Темная + Светлая') {
                                $index = '№5 "Chiko" SUNSCREENS БЕЗ ЗШ';
                                $arTextile[$index]['type'] = $index;
                                isset($arTextile[$index]['count']) ? $arTextile[$index]['count']++ : $arTextile[$index]['count'] = 1;
                            }elseif (@$value[4] == '№5 "Chiko" Темная + Светлая') {
                                $index = '№5 "Chiko" Темная';
                                $arTextile[$index]['type'] = $index;
                                isset($arTextile[$index]['count']) ? $arTextile[$index]['count']++ : $arTextile[$index]['count'] = 1;
                            }

                            $arTextile[$value[4]]['type'] = $value[4];
                            isset($arTextile[$value[4]]['count']) ? $arTextile[$value[4]]['count']++ : $arTextile[$value[4]]['count'] = 1;
                            break;
                        }
                    }
			}
			else
			{
				$value=explode('-', $naryad['article']);



                @$value[4] = str_replace(2, '№2 "Двушка"', @$value[4]);
                @$value[4] = str_replace(3, '№3 "Бифлекс"', @$value[4]);
                @$value[4] = str_replace(5, '№5 "Chiko" Темная + Светлая', @$value[4]);
                @$value[4] = str_replace(1, '№1 "Москитная сетка"', @$value[4]);
                @$value[4] = str_replace(4, '№1,5 "Полторашка"', @$value[4]);
                @$value[4] = str_replace(8, '№1,25 "Китайская ткань"', @$value[4]);

                if(strtoupper(@$value[0]) == 'BW' && @$value[4] == '№5 "Chiko" Темная + Светлая') {
                    $index = '№5 "Chiko" ЗШ';
                    $arTextile[$index]['type'] = $index;
                    isset($arTextile[$index]['count']) ? $arTextile[$index]['count']++ : $arTextile[$index]['count'] = 1;
                }elseif(strtoupper(@$value[3]) == 55 && @$value[4] == '№5 "Chiko" Темная + Светлая') {
                    $index = '№5 "Chiko" SUNSCREENS БЕЗ ЗШ';
                    $arTextile[$index]['type'] = $index;
                    isset($arTextile[$index]['count']) ? $arTextile[$index]['count']++ : $arTextile[$index]['count'] = 1;
                }elseif (@$value[4] == '№5 "Chiko" Темная + Светлая') {
                    $index = '№5 "Chiko" Темная';
                    $arTextile[$index]['type'] = $index;
                    isset($arTextile[$index]['count']) ? $arTextile[$index]['count']++ : $arTextile[$index]['count'] = 1;
                }

                $arTextile[$value[4]]['type'] = $value[4];
                isset($arTextile[$value[4]]['count']) ? $arTextile[$value[4]]['count']++ : $arTextile[$value[4]]['count'] = 1;
			}
		}

		if (count($arTextile))
			ksort($arTextile);
		$this->countNaryads = $arTextile;

		$provider = new ArrayDataProvider([
		    'allModels' => $arTextile,
		    'sort' => [
		        'attributes' => ['type', 'count'],
		    ],
		    'pagination' => [
		        'pageSize' => 10,
		    ],
		]);

		return $provider;
	}

	public function calcTotal()
	{
		$arNaryads = isset($this->countNaryads) ? $this->countNaryads : NULL;
		if (isset($arNaryads)){
			$count = 0;
			foreach ($arNaryads as $key => $value) {
				$count += isset($value['count']) ? $value['count'] : 0;				
			}
		}

		return $count;
	}


}