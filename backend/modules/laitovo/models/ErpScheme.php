<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "laitovo_erp_scheme".
 *
 * @property array $keytoid [ Массив всех точке вида : точка - идентфикатор участка производства ]
 * @property array $idtokey [ Массив всех точке вида : идентфикатор участка производства - точка ]
 * @property array $fromto [ Массив всех точке вида : идентфикатор участка производства откуда - идентфикатор участка производства куда ]
 */
class ErpScheme extends \common\models\laitovo\ErpScheme
{
	public $map='{}';
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['map','rule'], 'string'],
            [['map'], 'default','value' => '{}'],
            ['rule', 'validateRule'],
           // ['map', 'validateScheme'],
        ];
    }

    public function validateRule($attribute, $params)
    {
        try {
            preg_match($this->rule, '');
        } catch (\Exception $e) {
            $this->addError($attribute, Yii::t('app', '{attribute} должно быть регулярным выражением',['attribute'=>$this->getAttributeLabel($attribute)]));
        }
    }

    public function validateScheme($attribute, $params)
    {

        // if (!in_array($this->$attribute, ['USA', 'Web'])) {
        //     $this->addError($attribute, 'Страна должна быть либо "USA" или "Web".');
        // }
        $map=Json::decode($this->map);
        // print_r($map['nodeDataArray']);
        print_r($map['linkDataArray']);
        foreach ($map['nodeDataArray'] as $key => $value) {
            print_r($value);
            // print_r($value['category'] || $value['location_id']);
            // print_r($value['key']);
            // echo "<br><br>";
        }
        die();

    }

    public function afterFind()
    {
        $this->map=Json::encode( $this->json('scheme') );
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->json=Json::encode(['scheme'=>Json::decode($this->map)]);
            return true;
        } else {
            return false;
        }
    }

    public function getKeytoid()
    {
        $arr=[];
        foreach ($this->json('scheme')['nodeDataArray'] as $key => $value) {
            $arr[$value['key']]=isset($value['category'])?$value['category']:$value['location_id'];
        }
        return $arr;
    }

    public function getIdtokey()
    {
        $arr=[];
        foreach ($this->json('scheme')['nodeDataArray'] as $key => $value) {
            if (isset($value['category'])){
                $arr[$value['category']]=$value['key'];
            } else {
                $arr[$value['location_id']]=$value['key'];
            }
        }
        return $arr;
    }

    public function getFromto()
    {
        $arr=[];
        foreach ($this->json('scheme')['linkDataArray'] as $key => $value) {
            $arr[$value['from']]=$value['to'];
        }
        return $arr;
    }

    public function getStartFrom()
    {
        //Ищем массив всех записаных узлов
        $nodes = $this->json('scheme')['nodeDataArray'] ?? [];
        if (empty($nodes)) return null;

        $keyFrom = null;

        //Находим среди узлов - старт схемы.
        foreach ($nodes as $node) {
            if ( (isset($node['category']) && $node['category'] == 'Start') ) {
                $keyFrom = $node['key'];
                break;
            }
        }

        if (!$keyFrom) return null;

        //Ищем все движения
        $links = $this->json('scheme')['linkDataArray'] ?? [];
        if (empty($links)) return null;

        $keyTo = null;

        //Ищем движение от стартовой локации
        foreach ($links as $link) {
            if ( ( isset($link['from']) && isset($link['to']) && $link['from'] == $keyFrom ) ) {
                $keyTo = $link['to'];
                break;
            }
        }

        foreach ($nodes as $node) {
            if ( isset($node['key']) && $node['key'] == $keyTo &&  isset($node['location_id']) ) {
                return $node['location_id'];
            }
        }

        return null;
    }

    public function getLocations()
    {
        $arr=[];
        foreach ($this->json('scheme')['nodeDataArray'] as $key => $value) {
            if (isset($value['location_id']))
                $arr[$value['key']]= $value['location_id'];
        }
        return $arr;
    }
}