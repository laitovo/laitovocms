<?php

namespace backend\modules\laitovo\models;

use backend\helpers\BarcodeHelper;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\MainProductInfo;
use backend\modules\logistics\models\Naryad;
use common\models\laitovo\Cars;

/**
 * Терминал для оприходывания нарядов
 */
class OtkCreate extends Model
{
    //Нам необходимо получить артикул продукта и его наименование
    //
    public $user_id;
    public $window;        //оконный проем
    public $carArticle;    //артикул автомобиля
    public $goodid;          //вид продукта
    public $tkan;          //ткань

    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['window'], 'string'],
            [['window'], 'required'],
            [['carArticle'], 'integer'],
            [['carArticle'], 'required'],
            [['goodid'], 'integer'],
            [['goodid'], 'required'],
            [['tkan'], 'integer'],
            [['tkan'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'window'     => Yii::t('app', 'Оконный проем'),
            'carArticle' => Yii::t('app', 'Артикул'),
            'goodid'       => Yii::t('app', 'Вид продукта'),
            'tkan'       => Yii::t('app', 'Ткань'),
        ];
    }

    public function beforeValidate()
    {
        if ($this->carArticle && Cars::find()->where(['article' => $this->carArticle ])->one() == null) {
            $this->addError('carArticle', Yii::t('app','Автомобиля с таким артикулом не существует' ));
            return false; 
        }
        return true;
    }

    public function getProductArticle($value='')
    {
        if (!$this->validate()) return false;

    }

    public function getProductName($value='')
    {
        if (!$this->validate()) return false;

    }

    public function getProductBarcode($value='')
    {
        if (!$this->validate()) return false;

    }

    public function getProductTypes()
    {
        return [

        ];
    }

    public function getUser()
    {
        return $this->user_id ? ErpUser::findOne($this->user_id) : new ErpUser;
    }

    public function save()
    {
        if (!$this->validate()) return null;

        $article = MainProductInfo::getGoodArticle($this->goodid,$this->carArticle);
        if (!$article) return null;
        $title = MainProductInfo::getGoodTitle($this->goodid);

        $naryad = new Naryad();
        $naryad->article = $article;
        $naryad->source_id = 1;
        $naryad->team_id = Yii::$app->team->getId();
        $naryad->title = $title;
        $naryad->save();
        if (!$naryad) return null;


        $barcode = $naryad->getArticleBarcode($article);

        $window=mb_substr($barcode, 0,2,'utf-8');
        $window2=mb_strtolower($window);
        $window2=str_replace('fw', 'ps', $window2);
        $window2=str_replace('fv', 'pf', $window2);
        $window2=str_replace('fd', 'pb', $window2);
        $window2=str_replace('rd', 'zb', $window2);
        $window2=str_replace('rv', 'zf', $window2);
        $window2=str_replace('bw', 'zs', $window2);

        $cararticle=(int) mb_substr($barcode, 2,4,'utf-8');
        $typewindow=(int) mb_substr($barcode, 6,2,'utf-8');
        $color=(int) mb_substr($barcode, 8,1,'utf-8');

        $car=Cars::find()->where(['article'=>$naryad->getArticleCar($naryad->article)])->one();

        $response['name']= $naryad->title ? $naryad->title : '';
        $response['article']=$naryad->article;
        $response['barcode']=$barcode;
        $response['car']='';
        $response['lekalo']='';
        $response['natyag']='';
        $response['visota']='';
        $response['magnit']='';
        $response['hlyastik']='';
        $response['comment']='';
        if ($car) {
            $response['car']=$car->name;
            $response['lekalo']=$car->json('nomerlekala');
            if ($color==5){//chiko
                switch ($typewindow) {
                    case '2':
                        $type='smoke';
                        break;
                    case '3':
                        $type='mirror';
                        break;
                    case '6':
                        $type='short';
                        break;
                    
                    default:
                        $type='standart';
                        break;
                }
                $response['natyag']=@$car->json('_'.mb_strtolower($window))['chiko'][$type]['natyag'];
            }
            $response['visota']=@$car->json('visota'.mb_strtolower($window2));
            $response['magnit']=@$car->json('_'.mb_strtolower($window))['magnit'];
            $response['hlyastik']=@$car->json('_'.mb_strtolower($window))['hlyastik'];
            $response['comment']=$car->json('info') . $naryad->comment;
            $response['settext']='';
            $response['itemtext']='';
            $response['quantity']= 1;
        }

        $erpnaryad=new ErpNaryad;
        $erpnaryad->barcode= BarcodeHelper::upnToWorkOrderBarcode($naryad->barcode);
        $erpnaryad->logist_id= $naryad->id;
        $erpnaryad->article=$naryad->article;
        $erpnaryad->items=Json::encode([$response]);
        $erpnaryad->save();

        if (!$erpnaryad->save()) return null;
        $erpnaryad->user_id = $this->user_id;
        $erpnaryad->start = null;
        $erpnaryad->location_id = Yii::$app->params['erp_otk'];
        $erpnaryad->status = ErpNaryad::STATUS_IN_WORK;
        $erpnaryad->literal = $erpnaryad->recieveLiteral();
        $erpnaryad->literal_date = time();
        $erpnaryad->save();

        return $erpnaryad ? $erpnaryad->id : null;
    }
}
