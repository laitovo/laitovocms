<?php
/**
 * Created by PhpStorm.
 * User: bizzon2
 * Date: 17.04.2017
 * Time: 19:50
 */

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;

class ErpOrderTimeReport extends Model
{
    #Properties
    public $dateFrom;
    public $dateTo;

    #Getters and Setters

    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'date','format'=>'dd.mm.YY'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => Yii::t('app', 'Начало периода *(00:00:00)'),
            'dateTo' => Yii::t('app', 'Конец периода * (23:59:59)'),
        ];
    }

    public function generateReport()
    {
        $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom);
        $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo) + (60 * 60 * 24 - 1);

        $ordersReady =
            ErpOrder::find()
                ->where(['and',
                    ['status' => ErpOrder::STATUS_READY],
                    ['>', 'updated_at', $dateFrom],
                    ['<', 'updated_at', $dateTo]
                ])
                ->all();

        $objResult = [];

        $objResult['arResult'] = []; //массив
        /*Устанавлимваем свойства структуры массива*/
        $objResult['arResult'][0]['title'] = 'менее суток';
        $objResult['arResult'][0]['index'] = 0;
        $objResult['arResult'][1]['title'] = '1 день';
        $objResult['arResult'][1]['index'] = 1;
        $objResult['arResult'][2]['title'] = '2 дня';
        $objResult['arResult'][2]['index'] = 2;
        $objResult['arResult'][3]['title'] = '3 дня';
        $objResult['arResult'][3]['index'] = 3;
        $objResult['arResult'][4]['title'] = '4 дня';
        $objResult['arResult'][4]['index'] = 4;
        $objResult['arResult'][5]['title'] = '5 дней';
        $objResult['arResult'][5]['index'] = 5;
        $objResult['arResult'][6]['title'] = '6 дней';
        $objResult['arResult'][6]['index'] = 6;
        $objResult['arResult'][7]['title'] = '7 дней';
        $objResult['arResult'][7]['index'] = 7;
        $objResult['arResult'][8]['title'] = 'более 7 дней';
        $objResult['arResult'][8]['index'] = 8;
        /*Инициалзиция счетчиков*/
        for ($i = 0; $i < count($objResult['arResult']); $i++) {
            $objResult['arResult'][$i]['count'] = 0;
            $objResult['arResult'][$i]['countCur'] = 0;
        }

        $objResult['intLongest'] = 0; //самый долгий заказ (в днях) - по умолчанию 0 дней

        $objResult['intMedium'] = 0; //средний покозатель скорости выполенеия заказа - по умолчанию 0 дней

        $objResult['intMediumInHours'] = 0; //средний покозатель скорости выполенеия заказа в часах - по умолчанию 0 часов

        $countReadyOrders = 0; //инициализируем счетчик, который понадобиться нам в цикле

        foreach ($ordersReady as $order) {
            //Заполняем свойство массива (счетчики)
            switch ($intTime = round(($order->updated_at - $order->created_at) / (60 * 60 * 24))) {
                case ($intTime == 0):
                    $objResult['arResult'][0]['count'] = $objResult['arResult'][0]['count'] ? ($objResult['arResult'][0]['count'] + 1) : 1;
                    break;
                case ($intTime == 1):
                    $objResult['arResult'][1]['count'] = $objResult['arResult'][1]['count'] ? ($objResult['arResult'][1]['count'] + 1) : 1;
                    break;
                case ($intTime == 2):
                    $objResult['arResult'][2]['count'] = $objResult['arResult'][2]['count'] ? ($objResult['arResult'][2]['count'] + 1) : 1;
                    break;
                case ($intTime == 3):
                    $objResult['arResult'][3]['count'] = $objResult['arResult'][3]['count'] ? ($objResult['arResult'][3]['count'] + 1) : 1;
                    break;
                case ($intTime == 4):
                    $objResult['arResult'][4]['count'] = $objResult['arResult'][4]['count'] ? ($objResult['arResult'][4]['count'] + 1) : 1;
                    break;
                case ($intTime == 5):
                    $objResult['arResult'][5]['count'] = $objResult['arResult'][5]['count'] ? ($objResult['arResult'][5]['count'] + 1) : 1;
                    break;
                case ($intTime == 6):
                    $objResult['arResult'][6]['count'] = $objResult['arResult'][6]['count'] ? ($objResult['arResult'][6]['count'] + 1) : 1;
                    break;
                case ($intTime == 7):
                    $objResult['arResult'][7]['count'] = $objResult['arResult'][7]['count'] ? ($objResult['arResult'][7]['count'] + 1) : 1;
                    break;
                default:
                    $objResult['arResult'][8]['count'] = $objResult['arResult'][8]['count'] ? ($objResult['arResult'][8]['count'] + 1) : 1;
                    break;
            }

            //Получаем самое большое значение
            if ($objResult['intLongest'] < $intTime) {
                $objResult['intLongest'] = $intTime;
                $objResult['intLongestID'] = $order->id;
            }

            //Получаем данные для вычисления среднего значения
            //Для начала инициализируем счетчик
            $countReadyOrders++;
            $objResult['intMedium'] = $objResult['intMedium'] + $intTime;
            $objResult['intMediumInHours'] = $objResult['intMediumInHours'] + (($order->updated_at - $order->created_at) / (60 * 60));
        }

        //А после цикла вычисляем среднее значение
        $objResult['intMedium'] = $objResult['intMedium'] / ($countReadyOrders ? $countReadyOrders : 1);
        $objResult['intMediumInHours'] = round(($objResult['intMediumInHours'] / ($countReadyOrders ? $countReadyOrders : 1)), 0);

        $ordersInWork =
            ErpOrder::find()
                ->where(['and',
                    ['!=', 'status', ErpOrder::STATUS_READY],
                    ['!=', 'status', ErpOrder::STATUS_CANCEL],
                ])
                ->all();

        foreach ($ordersInWork as $order){
            //Заполняем свойство массива (счетчики)
            switch ($intTimeCur =  round((time() - $order->created_at)/(60*60*24))) {
                case ($intTimeCur == 0):
                    $objResult['arResult'][0]['countCur'] = $objResult['arResult'][0]['countCur'] ?  ($objResult['arResult'][0]['countCur'] + 1)  : 1;
                    break;
                case ($intTimeCur == 1):
                    $objResult['arResult'][1]['countCur'] = $objResult['arResult'][1]['countCur'] ?  ($objResult['arResult'][1]['countCur'] + 1)  : 1;
                    break;
                case ($intTimeCur == 2):
                    $objResult['arResult'][2]['countCur'] = $objResult['arResult'][2]['countCur'] ?  ($objResult['arResult'][2]['countCur'] + 1)  : 1;
                    break;
                case ($intTimeCur == 3):
                    $objResult['arResult'][3]['countCur'] = $objResult['arResult'][3]['countCur'] ?  ($objResult['arResult'][3]['countCur'] + 1)  : 1;
                    break;
                case ($intTimeCur == 4):
                    $objResult['arResult'][4]['countCur'] = $objResult['arResult'][4]['countCur'] ?  ($objResult['arResult'][4]['countCur'] + 1)  : 1;
                    break;
                case ($intTimeCur == 5):
                    $objResult['arResult'][5]['countCur'] = $objResult['arResult'][5]['countCur'] ?  ($objResult['arResult'][5]['countCur'] + 1)  : 1;
                    break;
                case ($intTimeCur == 6):
                    $objResult['arResult'][6]['countCur'] = $objResult['arResult'][6]['countCur'] ?  ($objResult['arResult'][6]['countCur'] + 1)  : 1;
                    break;
                case ($intTimeCur == 7):
                    $objResult['arResult'][7]['countCur'] = $objResult['arResult'][7]['countCur'] ?  ($objResult['arResult'][7]['countCur'] + 1)  : 1;
                    break;
                default:
                    $objResult['arResult'][8]['countCur'] = $objResult['arResult'][8]['countCur'] ?  ($objResult['arResult'][8]['countCur'] + 1)  : 1;
                    break;
            }
        }

        //После комплектации объекта, возвращаем его
        return $objResult;
    }

    public function receiveNaryads($dateFrom,$dateTo,$index)
    {
        $dateFrom = Yii::$app->formatter->asTimestamp($dateFrom);
        $dateTo = Yii::$app->formatter->asTimestamp($dateTo) + (60 * 60 * 24 - 1);

        $naryads =
            ErpOrder::find()
                ->where(['and',
                    ['status' => ErpOrder::STATUS_READY],
                    ['>', 'updated_at', $dateFrom],
                    ['<', 'updated_at', $dateTo]
                ])
                ->all();

        $objResult = [];

        $objResult['arResult'] = []; //массив
        /*Устанавлимваем свойства структуры массива*/
        $objResult['arResult'][0]['title'] = 'менее суток';
        $objResult['arResult'][0]['index'] = 0;
        $objResult['arResult'][1]['title'] = '1 день';
        $objResult['arResult'][1]['index'] = 1;
        $objResult['arResult'][2]['title'] = '2 дня';
        $objResult['arResult'][2]['index'] = 2;
        $objResult['arResult'][3]['title'] = '3 дня';
        $objResult['arResult'][3]['index'] = 3;
        $objResult['arResult'][4]['title'] = '4 дня';
        $objResult['arResult'][4]['index'] = 4;
        $objResult['arResult'][5]['title'] = '5 дней';
        $objResult['arResult'][5]['index'] = 5;
        $objResult['arResult'][6]['title'] = '6 дней';
        $objResult['arResult'][6]['index'] = 6;
        $objResult['arResult'][7]['title'] = '7 дней';
        $objResult['arResult'][7]['index'] = 7;
        $objResult['arResult'][8]['title'] = 'более 7 дней';
        $objResult['arResult'][8]['index'] = 8;
        /*Инициалзиция счетчиков*/
        for ($i = 0; $i < count($objResult['arResult']); $i++)
        {
            $objResult['arResult'][$i]['count'] = 0;
        }

        $objResult['intLongest'] = 0; //самый долгий заказ (в днях) - по умолчанию 0 дней

        $objResult['intMedium'] = 0; //средний покозатель скорости выполенеия заказа - по умолчанию 0 дней

        $countNaryads = 0; //инициализируем счетчик, который понадобиться нам в цикле
        foreach ($naryads as $naryad)
        {
            //Заполняем свойство массива (счетчики)
            switch ($intTime =  round(($naryad->updated_at - $naryad->created_at)/(60*60*24))) {
                case ($intTime == 0):
                    $objResult['arResult'][0]['count'] = $objResult['arResult'][0]['count'] ?  ($objResult['arResult'][0]['count']+ 1)  : 1;
                    $objResult['arResult'][0]['naryads'][] = $naryad;
                    break;
                case ($intTime == 1):
                    $objResult['arResult'][1]['count'] = $objResult['arResult'][1]['count'] ?  ($objResult['arResult'][1]['count']+ 1)  : 1;
                    $objResult['arResult'][1]['naryads'][] = $naryad;
                    break;
                case ($intTime == 2):
                    $objResult['arResult'][2]['count'] = $objResult['arResult'][2]['count'] ?  ($objResult['arResult'][2]['count']+ 1)  : 1;
                    $objResult['arResult'][2]['naryads'][] = $naryad;
                    break;
                case ($intTime == 3):
                    $objResult['arResult'][3]['count'] = $objResult['arResult'][3]['count'] ?  ($objResult['arResult'][3]['count']+ 1)  : 1;
                    $objResult['arResult'][3]['naryads'][] = $naryad;
                    break;
                case ($intTime == 4):
                    $objResult['arResult'][4]['count'] = $objResult['arResult'][4]['count'] ?  ($objResult['arResult'][4]['count']+ 1)  : 1;
                    $objResult['arResult'][4]['naryads'][] = $naryad;
                    break;
                case ($intTime == 5):
                    $objResult['arResult'][5]['count'] = $objResult['arResult'][5]['count'] ?  ($objResult['arResult'][5]['count']+ 1)  : 1;
                    $objResult['arResult'][5]['naryads'][] = $naryad;
                    break;
                case ($intTime == 6):
                    $objResult['arResult'][6]['count'] = $objResult['arResult'][6]['count'] ?  ($objResult['arResult'][6]['count']+ 1)  : 1;
                    $objResult['arResult'][6]['naryads'][] = $naryad;
                    break;
                case ($intTime == 7):
                    $objResult['arResult'][7]['count'] = $objResult['arResult'][7]['count'] ?  ($objResult['arResult'][7]['count']+ 1)  : 1;
                    $objResult['arResult'][7]['naryads'][] = $naryad;
                    break;
                default:
                    $objResult['arResult'][8]['count'] = $objResult['arResult'][8]['count'] ?  ($objResult['arResult'][8]['count']+ 1)  : 1;
                    $objResult['arResult'][8]['naryads'][] = $naryad;
                    break;
            }

            //Получаем самое большое значение
            if ($objResult['intLongest'] < $intTime) $objResult['intLongest'] = $intTime;

            //Получаем данные для вычисления среднего значения
            //Для начала инициализируем счетчик
            $countNaryads++;
            $objResult['intMedium'] = $objResult['intMedium'] + $intTime;
        }

        //А после цикла вычисляем среднее значение
        $objResult['intMedium'] = $objResult['intMedium'] /($countNaryads ? $countNaryads : 1);

        //После комплектации объекта, возвращаем его
        return  $objResult['arResult'][$index]['naryads'];
    }

    public function receiveNaryadsCur($dateFrom,$dateTo,$index)
    {
        $dateFrom = Yii::$app->formatter->asTimestamp($dateFrom);
        $dateTo = Yii::$app->formatter->asTimestamp($dateTo) + (60 * 60 * 24 - 1);

        $naryads =
            ErpOrder::find()
                ->where(['and',
                    ['!=','status',ErpOrder::STATUS_CANCEL],
                    ['!=','status',ErpOrder::STATUS_READY],
                ])
                ->all();

        $objResult = [];

        $objResult['arResult'] = []; //массив
        /*Устанавлимваем свойства структуры массива*/
        $objResult['arResult'][0]['title'] = 'менее суток';
        $objResult['arResult'][0]['index'] = 0;
        $objResult['arResult'][1]['title'] = '1 день';
        $objResult['arResult'][1]['index'] = 1;
        $objResult['arResult'][2]['title'] = '2 дня';
        $objResult['arResult'][2]['index'] = 2;
        $objResult['arResult'][3]['title'] = '3 дня';
        $objResult['arResult'][3]['index'] = 3;
        $objResult['arResult'][4]['title'] = '4 дня';
        $objResult['arResult'][4]['index'] = 4;
        $objResult['arResult'][5]['title'] = '5 дней';
        $objResult['arResult'][5]['index'] = 5;
        $objResult['arResult'][6]['title'] = '6 дней';
        $objResult['arResult'][6]['index'] = 6;
        $objResult['arResult'][7]['title'] = '7 дней';
        $objResult['arResult'][7]['index'] = 7;
        $objResult['arResult'][8]['title'] = 'более 7 дней';
        $objResult['arResult'][8]['index'] = 8;
        /*Инициалзиция счетчиков*/
        for ($i = 0; $i < count($objResult['arResult']); $i++)
        {
            $objResult['arResult'][$i]['count'] = 0;
        }

        $objResult['intLongest'] = 0; //самый долгий заказ (в днях) - по умолчанию 0 дней

        $objResult['intMedium'] = 0; //средний покозатель скорости выполенеия заказа - по умолчанию 0 дней

        $countNaryads = 0; //инициализируем счетчик, который понадобиться нам в цикле
        foreach ($naryads as $naryad)
        {
            //Заполняем свойство массива (счетчики)
            switch ($intTime =  round((time() - $naryad->created_at)/(60*60*24))) {
                case ($intTime == 0):
                    $objResult['arResult'][0]['count'] = $objResult['arResult'][0]['count'] ?  ($objResult['arResult'][0]['count']+ 1)  : 1;
                    $objResult['arResult'][0]['naryads'][] = $naryad;
                    break;
                case ($intTime == 1):
                    $objResult['arResult'][1]['count'] = $objResult['arResult'][1]['count'] ?  ($objResult['arResult'][1]['count']+ 1)  : 1;
                    $objResult['arResult'][1]['naryads'][] = $naryad;
                    break;
                case ($intTime == 2):
                    $objResult['arResult'][2]['count'] = $objResult['arResult'][2]['count'] ?  ($objResult['arResult'][2]['count']+ 1)  : 1;
                    $objResult['arResult'][2]['naryads'][] = $naryad;
                    break;
                case ($intTime == 3):
                    $objResult['arResult'][3]['count'] = $objResult['arResult'][3]['count'] ?  ($objResult['arResult'][3]['count']+ 1)  : 1;
                    $objResult['arResult'][3]['naryads'][] = $naryad;
                    break;
                case ($intTime == 4):
                    $objResult['arResult'][4]['count'] = $objResult['arResult'][4]['count'] ?  ($objResult['arResult'][4]['count']+ 1)  : 1;
                    $objResult['arResult'][4]['naryads'][] = $naryad;
                    break;
                case ($intTime == 5):
                    $objResult['arResult'][5]['count'] = $objResult['arResult'][5]['count'] ?  ($objResult['arResult'][5]['count']+ 1)  : 1;
                    $objResult['arResult'][5]['naryads'][] = $naryad;
                    break;
                case ($intTime == 6):
                    $objResult['arResult'][6]['count'] = $objResult['arResult'][6]['count'] ?  ($objResult['arResult'][6]['count']+ 1)  : 1;
                    $objResult['arResult'][6]['naryads'][] = $naryad;
                    break;
                case ($intTime == 7):
                    $objResult['arResult'][7]['count'] = $objResult['arResult'][7]['count'] ?  ($objResult['arResult'][7]['count']+ 1)  : 1;
                    $objResult['arResult'][7]['naryads'][] = $naryad;
                    break;
                default:
                    $objResult['arResult'][8]['count'] = $objResult['arResult'][8]['count'] ?  ($objResult['arResult'][8]['count']+ 1)  : 1;
                    $objResult['arResult'][8]['naryads'][] = $naryad;
                    break;
            }

            //Получаем самое большое значение
            if ($objResult['intLongest'] < $intTime) $objResult['intLongest'] = $intTime;

            //Получаем данные для вычисления среднего значения
            //Для начала инициализируем счетчик
            $countNaryads++;
            $objResult['intMedium'] = $objResult['intMedium'] + $intTime;
        }

        //А после цикла вычисляем среднее значение
        $objResult['intMedium'] = $objResult['intMedium'] /($countNaryads ? $countNaryads : 1);

        //После комплектации объекта, возвращаем его
        return  $objResult['arResult'][$index]['naryads'];
    }
    

}