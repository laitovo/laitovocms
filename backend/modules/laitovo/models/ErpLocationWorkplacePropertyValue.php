<?php

namespace backend\modules\laitovo\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "laitovo_erp_location_workplace_property_value".
 *
 * @property integer $id
 * @property integer $workplace_id
 * @property integer $property_id
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property User $author
 * @property LaitovoErpLocationWorkplaceProperty $property
 * @property User $updater
 * @property LaitovoErpLocationWorkplace $workplace
 */
class ErpLocationWorkplacePropertyValue extends \common\models\laitovo\ErpLocationWorkplacePropertyValue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workplace_id','created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['property_id','value'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
            [['workplace_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocationWorkplace::className(), 'targetAttribute' => ['workplace_id' => 'id']],
        ];
    }
}
