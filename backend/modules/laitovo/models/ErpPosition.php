<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/21/17
 * Time: 3:09 PM
 */

namespace backend\modules\laitovo\models;


use yii\helpers\ArrayHelper;
use common\models\laitovo\ErpDivision;


class ErpPosition extends \common\models\laitovo\ErpPosition
{
    public $division_article;


    public function getDivisionArticle(){
        return $this->getDivision()->article;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['salary_one', 'salary_two', 'car_compensation', 'surcharge', 'qualification', 'premium', 'kpi_one', 'kpi_two', 'kpi_three'], 'number'],
            [['salary_one', 'salary_two', 'car_compensation', 'surcharge', 'qualification', 'premium', 'kpi_one', 'kpi_two', 'kpi_three'], 'default','value'=>0],
            [['title'],'string'],
            [['article'],'integer'],
            ['article', 'validateUniqueInDivision'],
            [['division_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpDivision::className(), 'targetAttribute' => ['division_id' => 'id']],
        ];
    }

    public function validateUniqueInDivision($attribute, $params)
    {
        $array = ArrayHelper::map(self::find()->where(['and',['division_id'=>$this->division_id],['!=', 'article', $this->getOldAttribute('article')]])->all(),'article','article');
        if (array_key_exists($this->$attribute, $array)){
            $this->addError($attribute, 'Этот артикул уже занят');
        }
    }

//    public function beforeSave($insert)
//    {
//        if($this->division_article){
//            $division = ErpDivision::find()->where(['article'=>$this->division_article])->one();
//            if(!$division){
//                $position_division = $this->getDivision();
//                $position_division->article = $this->division_article;
//                $position_division->save();
//            }
//            if()
//        }
//        return parent::beforeSave($insert);
//    }

    static function positionList()
    {
        return ArrayHelper::merge([''=>''], ArrayHelper::map(self::find()->all(),'id', 'title'));
    }
}