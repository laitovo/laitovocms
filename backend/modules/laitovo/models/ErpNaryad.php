<?php

namespace backend\modules\laitovo\models;

use backend\helpers\ArticleHelper;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\StorageState;
use common\models\laitovo\CarClips;
use common\models\laitovo\ClipsScheme;
use backend\modules\logistics\models\Naryad as LogisticsNaryad;
use common\models\laitovo\ErpNaryadLog;
use core\logic\StochkaService;
use core\logic\UpnGroup;
use core\models\semiFinishedArticles\SemiFinishedArticles;
use core\services\SProductionLiteral;
use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Config;
use common\models\laitovo\Cars;
use common\models\laitovo\MaterialProductionScheme;
use common\models\laitovo\ErpMaterialReport;
use common\models\logistics\ArticleStatistics;

/**
 * This is the model class for table "laitovo_erp_naryad".
 *
 * Class ErpNaryad
 * @package backend\modules\laitovo\models
 * @property ErpScheme               scheme                           [ схема движения наряда по произвосдвту, объект ]
 * @property string|null             $lekalo
 * @property Config                  $config                          [ Объект настроек производста ]
 * @property ErpOrder                $order                           [ Заказ ]
 * @property SemiFinishedArticles    $semiFinishedArticle             [ Полуфабрикат ]
 * @property boolean                 $isAnotherPlaced                 [ На другой вешалке изгиба ]
 * @property string                  $productionLiteral               [ Производственная литера, на которой следует искать наряд ]
 * @property string                  $currentProductionLiteral        [ Производственная литера, на которой следует искать наряд ]
 */
class ErpNaryad extends \common\models\laitovo\ErpNaryad
{

    const MOVE_ACT_BARCODE_PREFIX = 'MA';

    const PROD_GROUP_BARCODE_PREFIX = 'PG';

    const ADD_GROUP_BARCODE_PREFIX = 'AG';

    /**
     * @var string $items  [ Свойство для хранения описания шторок, которые изготавливаются по данному наряду.
     * В текущией реализации массив $items имеет только 1 элемент, с индексом 0 ]
     */
    public $items = '[]';

    /**
     * @var integer|null $location_from [ Переменная, которая хранит в себе идентификатор участка, с которого движется наряд ]
     */
    public $location_from;

    /**
     * @var boolean $stochka_before [ Переменная, которая хранит в себе значение сточки ДО]
     */
    public $stochka_before;

    /**
     * @var integer|null $updated_at_before [ Служебная переменная для хранения предыдущей отментки времени изменения наряда ]
     */
    public $updated_at_before;

    /**
     * @var integer|null $start_before [ Служебная переменная для хранения предыдущего участка, с которого должен был стартовать наряд ]
     */
    public $start_before;

    /**
     * @var bool $reworkScenario [ Это свойство устанавливаеться в true - если заказ изменяеться при сохранении акта ]
     */
    public $reworkScenario = false;

    /**
     * @var integer $count [ Левая переменная, которая нужна для какого то непонятно подсчета нарядов по материалам ]
     */
    public $count;

    /**
     * @var integer $queue [ Слудебная переменная - очередь. Предназначение тольком не известно, но за нее что то завязано ]
     */
    private $queue;

    /**
     * @var null|array Массив значений для логирования нарядов в таблицу ErpNaryadLog
     */
    private $_log;

    /**
     * @var boolean $_oldstatus [ Служебная переменная для храенения значение статуса до изменения наряда ]
     */
    private $_oldstatus;

    /**
     * @var boolean $_oldNeedTemplate [ Служебная переменная для храенения значение статуса необходимости создания лекала до изменения наряда ]
     */
    private $_oldNeedTemplate;

    /**
     * @var boolean $config [ Служебная переменная для храенения всех настроек производства ]
     */
    private $_config;

    /**
     * @var ErpProductType $_product [ Служебная переменная для храенения объекта продукта, который изготовливается в наряде ]
     */
    private $_product;
    /**
     * @var object $_jobState [ Служебная переменная для храенения информации об оплате за наряд ]
     */
    private $_jobState;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'scheme_id', 'location_id', 'start', 'literal_date', 'literal', 'rework','ready_date','actNumber'], 'integer'],
            [['status'], 'string', 'max' => 255],
            [['sort'], 'integer', 'min' => 1, 'max' => 7],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpOrder::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['logist_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsNaryad::className(), 'targetAttribute' => ['logist_id' => 'id']],
            [['scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpScheme::className(), 'targetAttribute' => ['scheme_id' => 'id']],
            [['start'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['start' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['items'], 'string'],
            [['items'], 'default', 'value' => '[]'],
            [['status', 'location_id'], 'validateStatus'],
            ['isNeedTemplate', 'boolean'],
            ['isFromSump', 'boolean'],
            ['isAdd', 'boolean'],
            ['isAnotherPlaced', 'boolean'],
            ['withoutLabels', 'boolean'],
            ['halfStake', 'boolean'],
            ['stochka', 'boolean'],
            [['sumpLiteral'], 'string', 'max' => 255],
            [['oldSort'], 'safe'],
        ];
    }

    /**
     * Функция для проверки правильности установки статуса наряда, в зависимости от нахождении его на различных участках.
     *
     * @param $attribute
     * @param $params
     */
    public function validateStatus($attribute, $params)
    {

        if ($this->location_id && $this->status != self::STATUS_READY_TO_WORK && $this->status != self::STATUS_IN_WORK && $this->status != self::STATUS_IN_PAUSE)
            $this->addError($attribute, Yii::t('app', 'Наряд на участке', ['attribute' => $this->getAttributeLabel($attribute)]));

        if (!$this->location_id && !$this->start && ($this->status == self::STATUS_READY_TO_WORK || $this->status == self::STATUS_IN_WORK))
            $this->addError('location_id', Yii::t('app', '{attribute} не указан', ['attribute' => $this->getAttributeLabel('location_id')]));

    }

    /**
     * Получение объекта схемы, по которой движется наряд на производстве
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScheme()
    {
        return $this->hasOne(ErpScheme::className(), ['id' => 'scheme_id']);
    }

    /**
     * Получение объекта схемы, по которой движется наряд на производстве
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpn()
    {
        /**
         * @return \yii\db\ActiveQuery
         */
        return $this->hasOne(LogisticsNaryad::className(), ['id' => 'logist_id']);
    }


    /**
     * Получение объекта схемы, по которой движется наряд на производстве
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemiFinishedArticle()
    {
        /**
         * @return \yii\db\ActiveQuery
         */
        return $this->hasOne(SemiFinishedArticles::className(), ['upnId' => 'logist_id']);
    }

    /**
     * Функция для поиска и установки схемы, по котороый движется наряд на производстве. Они берется из карточки продукта.
     *
     * @return bool
     */
    public function searchScheme()
    {
        /**
         * @var $product ErpProductType
         */
        foreach (ErpProductType::find()->all() as $product) {
            if ($product->rule && preg_match($product->rule, $this->article)) {
                $productionScheme = $product->productionScheme;
                if ($productionScheme){
                    $this->scheme_id = $productionScheme->scheme_id;
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * [ Поиск продукта, по артикулу наряда ]
     *
     * @return null|\yii\db\ActiveRecord
     */
    public function searchProduct()
    {
        if ($this->_product) return $this->_product;
        /**
         * @var $product ErpProductType
         */
        $products = ErpProductType::find()->all();
        foreach ($products as $product) {
            if ($product->rule && preg_match($product->rule, $this->article)) {
                $this->_product = $product;
                return $this->_product;
            }
        }
        return null;
    }

    /**
     * [ Функция для доступа к конфигу производства ]
     *
     * @param $key
     * @return null|string|integer
     */
    public function cnf($key)
    {
        if (!$this->_config) {
            $this->_config = Config::findOne(2);
        }

        if (!$this->_config) return null;

        return $this->_config->json($key);
    }

    /**
     * В afterFind() мы делаем предзагрузку необходимых нам служебных свойств.
     *
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->items             = Json::encode($this->json('items'));
        $this->location_from     = $this->location_id;
        $this->stochka_before    = $this->stochka;
        $this->updated_at_before = $this->updated_at;
        $this->start_before      = $this->start;
        $this->_oldstatus        = $this->status;
        $this->_oldNeedTemplate  = $this->isNeedTemplate;
    }

    /**
     * [ В этой функции происходит:
     *  - Устанавливаем дату готовности автоматически, как только заказ стал в статус "Готов"
     *  - Устанавливаем автоматически дату распределения с производства на склад
     *  - Устанавилваем схему наряду, для его дальнешего движения по производству
     *  - Устанавливаем участок, на основании полученной схемы, с которого должен начать свое движение наряд.
     *  - Логирование в наряд о его движении на производстве
     * ]
     *
     * @inheritdoc
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        /*
         * Устанавливаем дату готовности автоматически, как только заказ стал в статус "Готов"
         */
        if ($this->_oldstatus != $this->status && $this->status == self::STATUS_READY) {
            $this->ready_date = time();
        }

        /*
         * Устанавливаем автоматически дату распределения с производства на склад
         */
        if ($this->distributed && !$this->distributed_date) {
            $this->distributed_date = time();
        }

        if (parent::beforeSave($insert)) {

            if ($insert) {
                /*
                 * Устанавилваем схему наряду, для его дальнешего движения по производству
                 */
                $this->searchScheme();
                /*
                 * Устанавливаем флаг о том, что данному наряду необходимо изготовить лекало.
                 */
                $this->isNeedTemplate = ArticleHelper::isNeedTemplate($this->article) && !ArticleHelper::isTemplateExists($this->article);
            }

            /*
             * Устанавливаем участок, на основании полученной схемы, с которого должен начать свое движение наряд.
             */
            $start = $this->scheme ? $this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey['Start']]] : '';


            if ($start != $this->cnf('reestrstartlocation'))
                $this->reestr = null;

            if ($this->locationfrom->id == $this->cnf('reestrendlocation'))
                $this->reestr = null;

            if ($insert) {
                $this->start = $this->start ?: $start;
                /*
                 * Устанавливаем участок швейка как начальный для того, чтобы наряд выдался на построение лекала, если его необхоимо строить.
                 */
                if ($this->isNeedTemplate) $this->start = Yii::$app->params['erp_shveika'];

                if ($this->start == Yii::$app->params['erp_otk']) {
                    $this->sort = 4;
                }
            }

            /*
             * Логирование в наряд о его движении на производстве
             */
            $this->json = Json::encode([
                'items' => Json::decode($this->items),
                'log' => $this->isNewRecord || $this->location_id == $this->location_from ? $this->json('log') : ArrayHelper::merge($this->json('log') ?: [] , [[
                    'user' => $this->user ? $this->user->name : Yii::$app->user->identity->name,
                    'date' => time(),
                    'location_from' => $this->locationfrom->name,
                    'location_to' => $this->location ? $this->location->name : '',
                ]]),
            ]);

            $this->_log = $this->isNewRecord || $this->location_id == $this->location_from ? null : [
                'date' => time(),
                'workerId' => $this->user ? $this->user->id : null,
                'workerName' => $this->user ? $this->user->name : Yii::$app->user->identity->name,
                'locationFromId' => $this->locationfrom->id,
                'locationFromName' => $this->locationfrom->name,
                'locationToId' => $this->location ? $this->location->id : null,
                'locationToName' => $this->location ? $this->location->name: '',
            ];

            return true;
        } else {
            return false;
        }
    }

    /**
     * [ В этой функции происходит:
     *  - Логирование нарядов в отдельную таблицу логов нарядов.
     *  - Изменение статуса заказ, в зависимости от состояния текущего наряда.
     *  - Добавление в статистику логистического модуля при завершении работы над нарядом.
     *  - Начисление заработной платы и списание материалов в производство.
     *  - Устанавливаем активность работнику, который работал с текущим нарядом.
     * ]
     *
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            /**
             * @var $upnRec Naryad
             */
            if ($upnRec = $this->upn) {
                $upnRec->withTape = ArticleHelper::isTape($this->article);
                $upnRec->save();
            }
        }
        /*
         * Логирование нарядов в отдельную таблицу логов нарядов.
         */
        if (is_array($this->_log)) {
            $this->_log['workOrderId'] = $this->id;
            ErpNaryadLog::log($this->_log);
            $this->_log = null;
        }

        /*
         *  Изменение статуса заказ, в зависимости от состояния текущего наряда
         */
        if ($this->order) {
            if ($this->order->getNaryads()->count() == $this->order->getNaryads()->andWhere(['status' => self::STATUS_CANCEL])->count()) {
                $this->order->status = ErpOrder::STATUS_CANCEL;
            } elseif ($this->order->getNaryads()->count() == $this->order->getNaryads()->andWhere(['or', ['status' => self::STATUS_READY], ['status' => self::STATUS_CANCEL], ['status' => self::STATUS_FROM_SKLAD]])->count()) {
                $this->order->status = ErpOrder::STATUS_READY;
            } elseif ($this->order->getNaryads()->andWhere(['or', ['status' => self::STATUS_READY_TO_WORK], ['status' => self::STATUS_IN_WORK]])->count() > 0) {
                $this->order->status = ErpOrder::STATUS_IN_WORK;
            }
            $this->order->save();
        }

        /*
         *  Добавление в статистику логистического модуля при завершении работы над нарядом
         */
//        if ($this->_oldstatus &&
//            ( (!in_array($this->_oldstatus, [self::STATUS_READY,self::STATUS_FROM_SKLAD])
//             && $this->status == self::STATUS_READY)
//           ||
//             (!in_array($this->_oldstatus, [self::STATUS_READY,self::STATUS_FROM_SKLAD])
//             && $this->status == self::STATUS_FROM_SKLAD) )
//           ) {
//            $statistics = new ArticleStatistics();
//            $statistics->article = $this->article;
//            $statistics->created_at = $this->created_at;
//            $statistics->type = ArticleStatistics::TYPE_BIZZONE;
//            $statistics->save();
//        }



        /**
         * Начисление заработной платы и списание материалов в производство
         * [ Если это сценарий оформления переделки и участок отк - тогда зп начисляеться, в противном случае нет ]
         */
        if (($this->location_from != NULL) && $this->location_id != $this->location_from && ( !$this->reworkScenario || ($this->reworkScenario && $this->location_from == Yii::$app->params['erp_otk']) ))
        {
            #Флаг - Начислять
            $charge = true;
            //кусок для насчитывания доп комплекта клипс
            $value = explode('-', $this->article);
            if($this->article == 'OT-564-0-0' && $this->location_from == Yii::$app->params['erp_clipsi'])
            {
                $this->erpUsersReportAddClips();
            }

            if ($this->_oldNeedTemplate && !$this->isNeedTemplate && $this->location_from == Yii::$app->params['erp_shveika'] && ArticleHelper::isNeedTemplate($this->article) && ArticleHelper::isTemplateExists($this->article))
            {
                $this->erpUsersReportAddTemplate($this->article);
                $charge = false;
            }
            //ищем вид продукта
            /**
             * @var ErpProductType|null $currentType
             */
            $currentType = $this->searchProduct();
             
            if($currentType && $currentType->production_scheme_id && $charge) {
                $erpJobSchemes = ErpJobScheme::find()->where(['production_scheme_id' => $currentType->production_scheme_id, 'location_id' => $this->location_from])->all();
                $materialSchemes = MaterialProductionScheme::find()->where(['production_scheme_id' => $currentType->production_scheme_id, 'location_id' => $this->location_from])->all();

                $averageRate = $this->cnf('averageRate') ?: 0;
                $hourRate    = $this->cnf('hourrate')    ?: 0;

                $jobClips1       = Yii::$app->params['jobClips1'] ?: null;
                $jobClips2       = Yii::$app->params['jobClips2'] ?: null;
                $jobClips3       = Yii::$app->params['jobClips3'] ?: null;
                $jobClipsMagnets = Yii::$app->params['jobClipsMagnets'] ?: null;

                /*
                 * Начисление заработной платы
                 */
                if (($erpJobSchemes || $this->location_from == Yii::$app->params['erp_clipsi']) && $averageRate != 0 && $hourRate != 0) {
                    /*
                     * Запись работы за наряд сотруднику на участке клипс
                     */
                    if ($this->location_from == Yii::$app->params['erp_clipsi']) {
                        $value = explode('-', $this->article);

                        /**
                         * Здесь будут вставлены костыли для того, чтобы на заказы полипласта платить за доп комлпект клипс
                         */
                        //Для гранты универсал для ООО ПОЛИПЛАСТ + 2 клипсы на ЗШ
                        if ($value[0] == 'BW' && $value[2] == 1806 && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
                            if (($jobType = ErpJobType::findOne($jobClips1)) && $jobType->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $jobClips1, 2, $jobType->rate);
                            }
                        }
                        //Для Skoda Kodiaq 1G Кроссовер 5D (2016 - н.в.) без штатных шторок ООО ПОЛИПЛАСТ + 2 клипсы на ЗБ
                        if ($value[0] == 'RD' && $value[2] == 1548 && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
                            if (($jobType = ErpJobType::findOne($jobClips1)) && $jobType->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $jobClips1, 2, $jobType->rate);
                            }
                        }
                        //Для 	Skoda Octavia 4G Лифтбэк 5D (2019 - н.в.) NX без штатных шторок ООО ПОЛИПЛАСТ
                        //+ 2 клипсы на ЗБ
                        if ($value[0] == 'RD' && $value[2] == 1993 && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
                            if (($jobType = ErpJobType::findOne($jobClips1)) && $jobType->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $jobClips1, 2, $jobType->rate);
                            }
                        }
                        // + 1 клипса на ЗШ
                        if ($value[0] == 'BW' && $value[2] == 1993 && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
                            if (($jobType = ErpJobType::findOne($jobClips1)) && $jobType->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $jobClips1, 1, $jobType->rate);
                            }
                        }
                        //Для Renault Kaptur (Russia) 1G Кроссовер 5D (2016 - 2019) ООО ПОЛИПЛАСТ
                        //+ 2 клипсы на ЗБ
                        if ($value[0] == 'RD' && $value[2] == 1489 && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
                            if (($jobType = ErpJobType::findOne($jobClips1)) && $jobType->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $jobClips1, 2, $jobType->rate);
                            }
                        }
                        // + 1 клипса на ЗШ
                        if ($value[0] == 'BW' && $value[2] == 1489 && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
                            if (($jobType = ErpJobType::findOne($jobClips1)) && $jobType->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $jobClips1, 1, $jobType->rate);
                            }
                        }
                        //Для Renault Duster (2017 - н.в) ООО ПОЛИПЛАСТ
                        //+ 2 клипсы на ЗБ
                        if ($value[0] == 'RD' && $value[2] == 2172 && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
                            if (($jobType = ErpJobType::findOne($jobClips1)) && $jobType->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $jobClips1, 2, $jobType->rate);
                            }
                        }
                        // + 1 клипса на ЗШ
                        if ($value[0] == 'BW' && $value[2] == 2172 && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
                            if (($jobType = ErpJobType::findOne($jobClips1)) && $jobType->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $jobClips1, 1, $jobType->rate);
                            }
                        }
                        /**
                         * Костыли для полипласта здесь заканчиваються
                         */

                        if ($value[0] != 'OT') {
                            if (ArticleHelper::isOnMagnets($this->article)) {
                                if (($jobType = ErpJobType::findOne($jobClipsMagnets)) && $jobType->rate) {
                                    $this->erpUsersReport($averageRate, $hourRate, $jobClipsMagnets, 1, $jobType->rate);
                                }
                            } else {
                                foreach ($this->getTypeClips() as $key => $value) {
                                    switch ($key) {
                                        case 'A':
                                            $jobClips = $jobClips1;
                                            break;
                                        case 'AA':
                                            $jobClips = $jobClips1;
                                            break;
                                        case 'B':
                                            $jobClips = $jobClips2;
                                            break;
                                        case 'C':
                                            $jobClips = $jobClips3;
                                            break;
                                        case 'CC':
                                            $jobClips = $jobClips3;
                                            break;
                                        default;
                                            $jobClips = null;
                                            break;
                                    }

                                    if ($jobClips && $value && ($jobType = ErpJobType::findOne($jobClips)) && $jobType->rate) {
                                        $this->erpUsersReport($averageRate, $hourRate, $jobClips, $value, $jobType->rate);
                                    }
                                }
                            }
                        }
                    /*
                     * Запись работы за наряд сотруднику на участке швейка
                     */
                    } elseif ($this->location_from == Yii::$app->params['erp_shveika']) {
                        //Если была сделана сточка наряда, тогда платим только за сточку
                        if ($this->stochka && !$this->stochka_before) {
                            foreach ($erpJobSchemes as $erpJobScheme) {
                                if (in_array($erpJobScheme->type_id, Yii::$app->params['stochka'])) {
                                    if ($erpJobScheme->type_id && $erpJobScheme->job_count && $erpJobScheme->type->rate) {
                                        $this->erpUsersReport($averageRate, $hourRate, $erpJobScheme->type_id, $erpJobScheme->job_count, $erpJobScheme->type->rate);
                                    }
                                    break;
                                }
                            }

                        } else {
                            //В противном случае платим как положено на швейке.
                            foreach ($erpJobSchemes as $erpJobScheme) {
                                if ($erpJobScheme->type_id == Yii::$app->params['velkro']) continue;
                                if (in_array($erpJobScheme->type_id, Yii::$app->params['stochka']) && $this->stochka) continue;
                                if ($erpJobScheme->type_id && $erpJobScheme->job_count && $erpJobScheme->type->rate) {
                                    $this->erpUsersReport($averageRate, $hourRate, $erpJobScheme->type_id, $erpJobScheme->job_count, $erpJobScheme->type->rate);
                                }
                            }
                            //Если велькро - работа за велькро
                            if ($this->velkroFlag()) {
                                $velkro = ErpJobType::findOne(Yii::$app->params['velkro']);
                                if ($velkro && $velkro->rate)
                                    $this->erpUsersReport($averageRate, $hourRate, $velkro->id, 1, $velkro->rate);
                            }
                        }
                    /*
                    * Запись работы за наряд сотруднику на всех остальных участках
                    */
                    }else {
                        foreach ($erpJobSchemes as $erpJobScheme) {
                            if ($erpJobScheme->type_id && $erpJobScheme->job_count && $erpJobScheme->type->rate) {
                                $this->erpUsersReport($averageRate, $hourRate, $erpJobScheme->type_id, $erpJobScheme->job_count, $erpJobScheme->type->rate);
                            }
                        }
                    }
                }

                /*
                 * Указаниие израсходованных материалов
                 */
                if($materialSchemes)
                {
                    foreach ($materialSchemes as $materialScheme) {
                        $reportMaterial = new ErpMaterialReport();
                        $reportMaterial->location_id = $this->location_from;
                        $reportMaterial->user_id =   $this->user_id;
                        $reportMaterial->naryad_id = $this->id;
                        if ($materialScheme->material_parameter_id) {
                            $reportMaterial->material_parameter_id = $materialScheme->material_parameter_id;
                        }
                        if ($materialScheme->material_count !== 0) {
                            $reportMaterial->material_count = $materialScheme->material_count;
                        } else {
                            $reportMaterial->material_count = 0;
                        }
                        if ($reportMaterial->save()) true;
                    }
                }
            }
        }

        /*
         * Устанавливаем активность работнику, который работал с текущим нарядом
         */
        if ($this->location_id !== $this->location_from && $this->user_id)
        {
            //если произошло движение наряда, тогда 
            $workplaces = ErpLocationWorkplace::find()->where(['user_id' => $this->user_id])->all();
            if ($workplaces)
            {
                foreach ($workplaces as $workplace) {
                    $workplace->last_activity = time();
                    $workplace->activity = 1;
                    $workplace->save();
                }
            }
        }

        /**
         * Ускоряем наряды из комплектации если наряд встал на диспетчер ОТК
         */
//        if ($this->location_id == Yii::$app->params['erp_dispatcher'] && $this->start == Yii::$app->params['erp_otk'] && $this->upn && $this->upn->group) {
//            UpnGroup::boostSpeedForGroup($this->upn);
//        }

        if ($this->location_id == Yii::$app->params['erp_dispatcher'] && $this->start && $this->upn && $this->upn->group) {
            UpnGroup::boostSpeedForGroupNew($this->upn, $this->start);
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * [ В этой функции происходит:
     *  - Смена статуса заказа, после удаления текущего наряда
     * ]
     *
     * @inheritdoc
     */
    public function afterDelete()
    {
        if ($this->order) {
            if ($this->order->getNaryads()->count() == $this->order->getNaryads()->andWhere(['status' => self::STATUS_CANCEL])->count()) {
                $this->order->status = ErpOrder::STATUS_CANCEL;
            } elseif ($this->order->getNaryads()->count() == $this->order->getNaryads()->andWhere(['or', ['status' => self::STATUS_READY], ['status' => self::STATUS_CANCEL], ['status' => self::STATUS_FROM_SKLAD]])->count()) {
                $this->order->status = ErpOrder::STATUS_READY;
            } elseif ($this->order->getNaryads()->andWhere(['or', ['status' => self::STATUS_READY_TO_WORK], ['status' => self::STATUS_IN_WORK]])->count() > 0) {
                $this->order->status = ErpOrder::STATUS_IN_WORK;
            }
            $this->order->save();
        }

        return parent::afterDelete();
    }

    /**
     * Отмена наряда.
     *
     * @return bool
     */
    public function cancel()
    {
        $this->location_id = null;
        $this->status = self::STATUS_CANCEL;
        return $this->save();
    }

    /**
     * Постановка наряда на паузу
     *
     * @param bool $byOrder Флаг, означающий что на паузу ставится весь заказ
     * @return bool
     */
    public function pause($byOrder = false)
    {
        if (!$this->canBePaused()) {
            return false;
        }
        $this->status = self::STATUS_IN_PAUSE;
        if ($byOrder) {
            $this->is_paused_by_order = true;
        }

        return $this->save();
    }

    /**
     * Снятие наряда с паузы
     *
     * @return bool
     */
    public function unpause()
    {
        if ($this->location_id) {
            $this->status = self::STATUS_IN_WORK;
        } else {
            $this->status = null;
        }
        $this->is_paused_by_order = null;

        return $this->save();
    }

    /**
     * Проверка, что наряд стоит на паузе
     *
     * @return bool
     */
    public function isPaused() {
        return $this->status == self::STATUS_IN_PAUSE;
    }

    /**
     * Проверка, что наряд стоит на паузе, причём пауза ставилась на весь заказ
     *
     * @return bool
     */
    public function isPausedByOrder() {
        return $this->status == self::STATUS_IN_PAUSE && $this->is_paused_by_order;
    }

    /**
     * Проверка, может ли наряд быть поставлен на паузу
     *
     * @return bool
     */
    public function canBePaused() {
        return !$this->status || $this->status == self::STATUS_IN_WORK;
    }

    /**
     * Движение наряда на следующий участок
     *
     * @param null $user
     * @return bool
     * @throws \yii\db\Exception
     */
    public function nextPlace($user = null)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try{
            /**
             * Для нарядов из отстойника
             */
            if ($this->isFromSump && ($upn = $this->upn) && ($state = $upn->storageState))
            {
                $this->sumpLiteral = $state->literal;
                /**
                 * @var $state StorageState
                 */
                $state->delete();
            }

            $this->user_id = $user;

            $this->location_id = $this->start;
            $literalLocation = $this->start;
            // $this->location_id = ($place=$this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey[$this->location_id?:'Start']]])=='End' ? null : $place;
            $this->status = $this->location_id ? self::STATUS_IN_WORK : self::STATUS_READY;
            $this->start = null;
            if ($this->save()) $transaction->commit();

            /**
             * Убираем запись о том, что наряд числится на такой то производственной литере
             */
            if ($literalLocation != Yii::$app->params['erp_clipsi'])
                SProductionLiteral::unRegisterLiteral($this->id,$literalLocation);
            return true;
        }catch (\Exception $exception) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * Движение наряда на следующий участок
     *
     * @param null $user
     * @return bool
     * @throws \yii\db\Exception
     */
    public function nextPlaceAdd($user = null)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try{
            /**
             * Для нарядов из отстойника
             */
            if ($this->isFromSump && ($upn = $this->upn) && ($state = $upn->storageState))
            {
                $this->sumpLiteral = $state->literal;
                /**
                 * @var $state StorageState
                 */
                $state->delete();
            }

            $this->user_id = $user;
            $this->isAdd = true;

            $this->location_id = $this->start;
            $literalLocation = $this->start;
            // $this->location_id = ($place=$this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey[$this->location_id?:'Start']]])=='End' ? null : $place;
            $this->status = $this->location_id ? self::STATUS_IN_WORK : self::STATUS_READY;
            $this->start = null;
            if ($this->save()) $transaction->commit();
            /**
             * Убираем запись о том, что наряд числится на такой то производственной литере
             */
            SProductionLiteral::unRegisterLiteral($this->id, $literalLocation);
            return true;
        }catch (\Exception $exception) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * Сдача наряда на лекало.
     *
     * @param null $user
     * @return bool
     */
    public function endPlaceTemplate($user = null)
    {
        if (!ArticleHelper::isTemplateExists($this->article)) return false;

        if (($this->location_id == '' || $this->location_id == null) && ($this->start == '' || $this->start == NULL)) return false;

        if ($this->updated_at > time() - 4) return false;

        $this->user_id = $user;

        /*
         * Костыль. Если наряд сдают со швейки, который нуждался в лекале, считаем, что его лекало сделали.
         */
        if ($this->isNeedTemplate && $this->location_id == Yii::$app->params['erp_shveika']) {
            $this->isNeedTemplate = false;
            $this->start = $this->scheme ? $this->scheme->getStartFrom() : null;
            $this->status = self::STATUS_IN_WORK;
        }

        $this->location_id = $this->start ? Yii::$app->params['erp_dispatcher'] : null;

        return $this->save();
    }


    /**
     * Движение наряда с участка.
     *
     * @param null $user
     * @return bool
     */
    public function endPlace($user = null, $skipDelay = false)
    {
        if ($this->status == ErpNaryad::STATUS_IN_PAUSE)
            return false;

        $group = null;
        /*Функционал при сдаче наряда для производительности*/
        /*Фактически добавляем наряд в счетчик дневного количества согласно групп*/
        $product = $this->searchProduct();
        if ($product)
            $group = $product->recieveGroupByLocation($this->location_id);
        $row = ErpProductPerformance::find() 
            ->where(['group_id' => $group]) 
            ->andWhere(['user_id' => $user])    
            ->one();    
        if ($row) 
        { 
            $count = $row->day_count ? $row->day_count : 0; 
            $row->day_count = $count + 1; 
            $row->save(); 
        }  
        else 
        { 
            $row = new ErpProductPerformance;  
            $row->user_id = $user;  
            $row->group_id = $group;  
            $row->day_count = 1;  
            $row->save();  
        } 
        /////////////////////////////////////

        if (($this->location_id == '' || $this->location_id == null) && ($this->start == '' || $this->start == NULL)) return false;

        if (!$skipDelay)
            if ($this->updated_at > time() - 4) return false;

        $this->user_id = $user;

        $this->isAdd = false;

        $next = ($place = $this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey[$this->location_id ?: 'Start']]]) == 'End' ? null : $place;

        if ($next == Yii::$app->params['erp_clipsi'] && !ArticleHelper::isOnMagnets($this->article) &&  (!count(($clips = $this->getClips())) || (count($clips) == 1 && isset($clips['V'])) || ArticleHelper::isTape($this->article)) )
        {   
            $this->start = ($place = $this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey[Yii::$app->params['erp_clipsi'] ?: 'Start']]]) == 'End' ? null : $place;
        }else{
            $this->start = ($place = $this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey[$this->location_id ?: 'Start']]]) == 'End' ? null : $place;
        }

        //Если сточка, тогда при сдаче наряда устанавливаем наряд в сточку и следующим участком ставим опять же швейку
        if ($user)
            StochkaService::checkAndSetValue($this, ErpUser::findOne($user));

        $this->status = $this->start ? self::STATUS_IN_WORK : self::STATUS_READY;
        $this->location_id = $this->start ? Yii::$app->params['erp_dispatcher'] : null;

        return $this->save();
    }

    /**
     * Движение наряда с участка.
     *
     * @param null $user
     * @return bool
     */
    public function endPlaceMoveAct($user = null)
    {
        if ($this->status == ErpNaryad::STATUS_IN_PAUSE)
            return false;

        $group = null;
        /*Функционал при сдаче наряда для производительности*/
        /*Фактически добавляем наряд в счетчик дневного количества согласно групп*/
        $product = $this->searchProduct();
        if ($product)
            $group = $product->recieveGroupByLocation($this->location_id);
        $row = ErpProductPerformance::find()
            ->where(['group_id' => $group])
            ->andWhere(['user_id' => $user])
            ->one();
        if ($row)
        {
            $count = $row->day_count ? $row->day_count : 0;
            $row->day_count = $count + 1;
            $row->save();
        }
        else
        {
            $row = new ErpProductPerformance;
            $row->user_id = $user;
            $row->group_id = $group;
            $row->day_count = 1;
            $row->save();
        }
        /////////////////////////////////////

        if (($this->location_id == '' || $this->location_id == null) && ($this->start == '' || $this->start == NULL)) return false;

        if ($this->updated_at > time() - 4) return false;

        $this->user_id = $user;

        $this->isAdd = false;

        $next = ($place = $this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey[$this->location_id ?: 'Start']]]) == 'End' ? null : $place;

//        if ($next == Yii::$app->params['erp_clipsi'] && (!count($this->getClips()) || ArticleHelper::isTape($this->article)))
//        {
//            $this->start = ($place = $this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey[Yii::$app->params['erp_clipsi'] ?: 'Start']]]) == 'End' ? null : $place;
//        }else{
        $this->start = ($place = $this->scheme->keytoid[$this->scheme->fromto[$this->scheme->idtokey[$this->location_id ?: 'Start']]]) == 'End' ? null : $place;
//        }

        $this->status = $this->start ? self::STATUS_IN_WORK : self::STATUS_READY;
        $this->location_id = $this->start ? Yii::$app->params['erp_dispatcher'] : null;

        return $this->save();
    }

    /**
     * @return ErpLocation|static
     */
    public function getLocationfrom()
    {
        return $this->location_from ? ErpLocation::findOne($this->location_from) : new ErpLocation;
    }

    /**
     * Служебная функция получаения оконного проема наряда
     *
     * @return mixed|null
     */
    public function getWindow()
    {
        return ArticleHelper::getWindow($this->article);
    }

    /**
     * Служебная функция получения наименование оконного проема
     *
     * @return mixed|string
     */
    public function getWindowName()
    {
        $value = mb_substr($this->article, 0, 2, 'utf-8');
        $value = str_replace('FW', 'ШТОРКА НА ЛОБОВОЕ СТЕКЛО', $value);
        $value = str_replace('FV', 'ПЕРЕДНИЕ ФОРТОЧКИ', $value);
        $value = str_replace('FD', 'ПЕРЕДНИЕ БОКОВЫЕ', $value);
        $value = str_replace('RD', 'ЗАДНИЕ БОКОВЫЕ', $value);
        $value = str_replace('RV', 'ЗАДНИЕ ФОРТОЧКИ', $value);
        $value = str_replace('BW', 'ШТОРКА НА ЗАДНЕЕ СТЕКЛО', $value);

        return $value;
    }


    /**
     * @return string
     */
    public function getTkanType()
    {
        $value = explode('-', $this->article);

        if (@$value[3] == 49)
            return 'Москитка';
        /*
                if (@$value[3] == 55)
                    return 'Санскрин';*/

        if ((@$value[3] == 45 || @$value[3] == 48) && @$value[4] == 5)
            return 'ChikoMagnet';
        //автошторки chiko на магнитах
        if ((@$value[3] == 67) && (@$value[4] == 5))
            return 'Chiko на магнитах';

        if (@$value[4] == 5)
            return 'Chiko';

        return '';
    }

    public function getWindowType()
    {
        $value = explode('-', $this->article);

        if ($value[0] == 'OT')
            return 'Доп.номенклатура';

        if (@$value[3] == 49)
            return 'Стандарт';

        //автошторки чико no name
        if ((@$value[3] == 55) && (@$value[4] == 5))
            return 'NO NAME, без Лейблов';

        //автошторки чико на магнитах
        if ((@$value[3] == 67) && (@$value[4] == 5))
            return 'Chiko на магнитах, Стандарт';

        //удадлить после перехода
        if ((@$value[3] == 67) && (@$value[4] == 2))
            return 'Laitovo на магнитах, Светлая';
        ///////////////////////////////////////////

        if ((@$value[3] == 67) && (@$value[4] == 4))
            return 'Laitovo на магнитах, Medium';

        //Удалить после перехода
        if ((@$value[3] == 70) && (@$value[4] == 2))
            return 'Laitovo на магнитах, Темная';
        ///////////////////////////////////////////

        if ((@$value[3] == 72) && (@$value[4] == 5))
            return 'Chiko на магнитах, Укороченный';

        if ((@$value[3] == 72) && (@$value[4] == 2))
            return 'Укороченный, Laitovo на магнитах (Темная)';

        if ((@$value[3] == 72) && (@$value[4] == 4))
            return 'Укороченный, Laitovo на магнитах (Medium)';

        if ((@$value[3] == 72) && (@$value[4] == 8))
            return 'Укороченный, Laitovo на магнитах (Светлая)';

        if ((@$value[3] == 70) && (@$value[4] == 1))
            return 'Laitovo на магнитах, Светлая';

        if ((@$value[3] == 70) && (@$value[4] == 8))
            return 'Laitovo на магнитах, Светлая';

        if ((@$value[3] == 71) && (@$value[4] == 2))
            return 'Laitovo на магнитах, Темная';

        if ((@$value[3] == 71) && (@$value[4] == 4))
            return 'Laitovo на магнитах, Медиум';

        if (@$value[3] == 45 && @$value[4] == 4)
            return 'LaitovoMagnet (Medium), Стандарт';

        if (@$value[3] == 45 && @$value[4] == 8)
            return 'LaitovoMagnet (Светлая), Стандарт';

        if (@$value[3] == 45 && @$value[4] == 2)
            return 'LaitovoMagnet (Темная), Стандарт';

        if (@$value[3] == 45 && @$value[4] == 5)
            return 'ChikoMagnet, Стандарт';

        if (@$value[3] == 48 && @$value[4] == 4)
            return 'Укороченный, LaitovoMagnet (Medium)';

        if (@$value[3] == 48 && @$value[4] == 8)
            return 'Укороченный, LaitovoMagnet (Светлая)';

        if (@$value[3] == 48 && @$value[4] == 2)
            return 'Укороченный, LaitovoMagnet (Темная)';

        if (@$value[3] == 48 && @$value[4] == 5)
            return 'ChikoMagnet, Укороченный';

        if (@$value[3] == 68)
            return 'Козырек Magnet';
        
        if (@$value[3] == 69)
            return 'Козырек на магнитах';

        if (@$value[3] == 7) return 'Складной';
        if (@$value[3] == 1) return 'Стандарт';
        if (@$value[3] == 2) return 'Вырез курил';
        if (@$value[3] == 3) return 'Вырез зерк';
        if (@$value[3] == 4) return 'Сдвижной';
        if (@$value[3] == 5) return 'Бескаркасный';
        if (@$value[3] == 6) return 'Укороченный';

        return @$value[3];
    }

    public function getTkan()
    {
        $value = explode('-', $this->article);

        //светлая
        if ((@$value[3] == 67) && (@$value[4] == 2))
            return '№ 1';

        //medium
        if ((@$value[3] == 70) && (@$value[4] == 2))
            return '№ 1,25';

        //светлая
        if ((@$value[3] == 70) && (@$value[4] == 1))
            return '№ 1,25';

        //темная
        if ((@$value[3] == 71) && (@$value[4] == 2))
            return '№ 2';

        @$value[4] = str_replace(4, '1,5', @$value[4]);
        @$value[4] = str_replace(8, '1,25', @$value[4]);

        if ($value[4] == 5 && ($value[0] == 'BW' || $value[3] == 55)) {
            $value[4] = '5C';
        } elseif ($value[4] == 5) {
            $value[4] = '5T';
        }

        return '№' . @$value[4];
    }

    public function getArticleTkan()
    {
        $value = explode('-', $this->article);
        return isset($value[4]) ? $value[4] : null ;
    }

    public function getChikoInMagnet()
    {
        return ArticleHelper::getChikoInMagnet($this->article);
    }
    
    public function mosquitoNet()
    {
        return ArticleHelper::mosquitoNet($this->article);
    }

    public function laitovoInMagnet()
    {
        return ArticleHelper::laitovoInMagnet($this->article);
    }

    public function getCarArticle()
    {
        return ArticleHelper::getCarArticle($this->article);
    }

    public function vizorMagnet()
    {

        return ArticleHelper::vizorMagnet($this->article);
    }

    public function vizorInMagnet()
    {
        return ArticleHelper::vizorInMagnet($this->article);
    }
    
    public function chikoMagnet()
    {
        return ArticleHelper::chikoMagnet($this->article);
    }

    public function laitovoMagnet()
    {
        return ArticleHelper::laitovoMagnet($this->article);
    }

    public function onMagnets()
    {
        return ArticleHelper::isOnMagnets($this->article);
    }

    public function getDopArticle()
    {
        $value = explode('-', $this->article);
        if ($value[0] == 'OT')
            return @$value[1];
        return null;
    }

    public function getBrand()
    {
        return ArticleHelper::getBrand($this->article);
    }

    public function getType()
    {
        return ArticleHelper::getType($this->article);
    }

    public function getType_clips()
    {

        return ArticleHelper::getType_clips($this->article);
    }

    public function getCar()
    {
        return ArticleHelper::getCar($this->article);
    }

    public function getClips()
    {
        return ArticleHelper::getClips($this->article);
    }

    public function getCountWindow()
    {
        return ArticleHelper::getCountWindow($this->article);
    }

    ### Статичекие методы

    static function windowStatic($article)
    {
        $value = mb_substr($article, 0, 2, 'utf-8');
        $value = str_replace('FW', 'ПШ', $value);
        $value = str_replace('FV', 'ПФ', $value);
        $value = str_replace('FD', 'ПБ', $value);
        $value = str_replace('RD', 'ЗБ', $value);
        $value = str_replace('RV', 'ЗФ', $value);
        $value = str_replace('BW', 'ЗШ', $value);
        $value = str_replace('OT', '', $value);

        return $value;
    }

    static function windowTypeStatic($article)
    {
        $value = explode('-', $article);

        if ($value[0] == 'OT') {
            $naryad = ErpNaryad::find()->where(['article' => $article])->one();
            $items = $naryad->json('items');
            $name = $items[0]['name'];
            return $name;
        } else {
            if (@$value[3] == 49)
                return 'Москитная сетка';

            //автошторки чико no name
            if ((@$value[3] == 55) && (@$value[4] == 5))
                return 'NO NAME, без Лейблов';

            if (@$value[3] == 45 && @$value[4] == 4)
                return 'LaitovoMagnet (Medium), Стандарт';

            if (@$value[3] == 45 && @$value[4] == 8)
                return 'LaitovoMagnet (Светлая), Стандарт';

            if (@$value[3] == 45 && @$value[4] == 2)
                return 'LaitovoMagnet (Темная), Стандарт';

            if (@$value[3] == 45 && @$value[4] == 5)
                return 'ChikoMagnet, Стандарт';

            if (@$value[3] == 48 && @$value[4] == 4)
                return 'LaitovoMagnet (Medium), Укороченный';

            if (@$value[3] == 48 && @$value[4] == 8)
                return 'LaitovoMagnet (Светлая), Укороченный';

            if (@$value[3] == 48 && @$value[4] == 2)
                return 'LaitovoMagnet (Темная), Укороченный';

            if (@$value[3] == 48 && @$value[4] == 5)
                return 'ChikoMagnet, Укороченный';

            //автошторки чико на магнитах
            if ((@$value[3] == 67) && (@$value[4] == 5) || @$value[3] == 55)
                return 'Chiko на магнитах, Стандарт';
            if ((@$value[3] == 67) && (@$value[4] == 2))
                return 'Laitovo на магнитах, Светлая';

            if ((@$value[3] == 70) && (@$value[4] == 2))
                return 'Laitovo на магнитах, Medium';


            if ((@$value[3] == 72) && (@$value[4] == 5))
                return 'Chiko на магнитах, Укороченный';

            if (@$value[3] == 68)
                return 'Козырек Magnet';
        
            if (@$value[3] == 69)
                return 'Козырек на магнитах';

            if (@$value[3] == 7) return 'Складной';

            @$value[3] = str_replace(1, 'Стандарт', @$value[3]);
            @$value[3] = str_replace(2, 'Вырез курил.', @$value[3]);
            @$value[3] = str_replace(3, 'Вырез зерк.', @$value[3]);
            @$value[3] = str_replace(4, 'Сдвижной', @$value[3]);
            @$value[3] = str_replace(5, 'Бескаркасный', @$value[3]);
            @$value[3] = str_replace(6, 'Укороченный', @$value[3]);


            return @$value[3];
        }

    }

    static function tkanStatic($article)
    {
        $value = explode('-', $article);
        if ($value[0] != 'OT') {
            @$value[4] = str_replace(4, '1,5', @$value[4]);
            @$value[4] = str_replace(8, '1,25', @$value[4]);

            return '№' . @$value[4];
        }
        return '';
    }

    static function filterTypeStatic($group = null)
    {
        $groups =
            [
                1 => 1,  //Основная продукция
                2 => 2,  //Дополнительная продукция
                //3 => 3,  //Прочая продукция
            ];

        $result = [];
        $products = [];

        $products[$groups[1]][1] = 'Стандарт';
        $products[$groups[1]][45] = 'ChikoMagnet, Стандарт';
        $products[$groups[1]][48] = 'ChikoMagnet, Укороченный';
        $products[$groups[1]][67] = 'Chiko на магнитах, Стандарт/Laitovo на магнитах, Светлая';
        $products[$groups[1]][72] = 'Chiko на магнитах, Укороченный';
        $products[$groups[1]][70] = 'Laitovo на магнитах, Medium';
        $products[$groups[1]][71] = 'Laitovo на магнитах, Темная';
        $products[$groups[1]][68] = 'Козырек Magnet';
        $products[$groups[1]][69] = 'Козырек на магнитах';
        $products[$groups[1]][2] = 'Вырез курил.';
        $products[$groups[1]][6] = 'Укороченный';
        $products[$groups[1]][7] = 'Складной';
        $products[$groups[1]][3] = 'Вырез. зерк';
        $products[$groups[1]][49] = 'Москитная сетка';

        $products[$groups[2]][4] = 'Сдвижной';
        $products[$groups[2]][5] = 'Бескаркасный';


        if ($group && in_array($group, $groups))
        {
            return $products[$group];
        }
        elseif (!$group || ($group && !in_array($group, $groups)))
        {

            foreach ($groups as $singleGroup)
            {
                foreach ($products[$singleGroup] as $key => $name)
                {
                    $result[$key] = $name;
                }
            }
            return $result ? $result : [];
        }
    }

    static function getDangerUserTimeoutNaryads()
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_WORK],
                    ['status' => self::STATUS_IN_PAUSE],
                ],
                ['IS NOT','location_id',NULL],
                ['IS NOT','user_id',NULL],
                ['<','sort',7],
                ['or',['logist_id' => NULL],['is_new_scheme' => 1]],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }

        //В конце делаем результирующую выборку и получаем наряды
        $result = self::find()
            ->where(['in','id',$ids])
            ->count();

        return $result;
    }

    static function getDangerUserTimeoutNaryadsPause()
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_PAUSE],
                ],
                ['IS NOT','location_id',NULL],
                ['IS NOT','user_id',NULL],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }

        //В конце делаем результирующую выборку и получаем наряды
        $result = self::find()
            ->where(['in','id',$ids])
            ->count();

        return $result;
    }

    static function getDangerUserTimeoutNaryadsUser()
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = self::find()
            ->where(['and',
                ['or',
                    ['status' => self::STATUS_IN_WORK],
                ],
                ['IS NOT','location_id',NULL],
                ['!=','location_id',Yii::$app->params['erp_dispatcher']],
                ['IS NOT','user_id',NULL],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }

        //В конце делаем результирующую выборку и получаем наряды
        $result = self::find()
            ->where(['in','id',$ids])
            ->count();

        return $result;
    }

    static function prioritets(){
        return [
            7=>'Низкий',
            6=>'Обычный',
            5=>'Комплектация',
            4=>'Срочный',
            3=>'Большой заказ',
            2=>'TopSpeed',
            1=>'Срочный-Переделка',
        ];
    }

    static function prioritetName($sort)
    {

        if($sort==1)return 'Срочный-Переделка';
        if($sort==2)return 'TopSpeed';
        if($sort==3)return 'Большой заказ';
        if($sort==4)return 'Срочный';
        if($sort==5)return 'Комплектация';
        if($sort==6)return 'Обычный';
        if($sort==7)return 'Низкий';
        return 'Нет приоритета';
    }


    public function getLekalo()
    {
        $arResult = Json::decode($this->json);
        return $arResult['items'][0]['lekalo'] ? $arResult['items'][0]['lekalo'] : '';
    }

    public function getTemplate()
    {
        $arResult = Json::decode($this->json);
        return $arResult['items'][0]['lekalo'] ? $arResult['items'][0]['lekalo'] : '';
    }

    public function getValueLiteral()
    {
        $literals = [];

        $literals[1] = 'А';
        $literals[2] = 'Б';
        $literals[3] = 'В';
        $literals[4] = 'Г';
        $literals[5] = 'Д';
        $literals[6] = 'Е';
        $literals[7] = 'Ж';
        $literals[8] = 'З';
        $literals[9] = 'И';
        $literals[10] = 'К';
        $literals[100] = 'Ф';
        $literals[101] = 'О';

        return isset($literals[$this->literal]) ? $literals[$this->literal] : NULL;
    }

    public function getProductionLiteral()
    {
        $prodLiteral = SProductionLiteral::workOrderInfo($this->id,$this->location_id);
        if (!$prodLiteral)
            $prodLiteral = SProductionLiteral::workOrderLog($this->id,$this->location_id);
        $simpleLiteral = '';
        if ($this->prodGroup) $simpleLiteral = 'Группа ' . $this->prodGroup;
        return $prodLiteral ?: $simpleLiteral;
    }

    public function getCurrentProductionLiteral()
    {
        return SProductionLiteral::workOrderInfo($this->id,$this->start);
    }

    /**
     * Генерация литеры для производства
     *
     * @return int|mixed|null
     */
    public function recieveLiteral()
    {
        if (preg_match('/OT-2079/', $this->article) === 1) {
            return 100;
        }

        if (preg_match('/OT-1189/', $this->article) === 1) {
            return 101;
        }

        $naryad = self::find()
        ->where(['status' => self::STATUS_IN_WORK])
        ->andWhere(['not',['literal_date' => null]])
        ->andWhere(['not',['literal' => null]])
        ->orderBy('literal_date desc')//от большего к меньшего (от последней даты к первой)
        ->one();

        if (!$naryad) return 8;

        return ($naryad->literal < 10) ? ($naryad->literal + 1) : 1;
    }

    public function generateActNumber()
    {
        $max =  self::find()->max('actNumber');
        return $max ? $max + 1: 1;
    }

    public function generateprodGroupNumber()
    {
        $max =  self::find()->max('prodGroup');
        return $max ? $max + 1: 1;
    }

    public function generateAddGroupNumber()
    {
        $max =  self::find()->max('addGroup');
        return $max ? $max + 1: 1;
    }
    
    public function getEquipment()
    {
        //массив возможных комплектаций
        $equipment = [];
        $equipment['FW'] = 'ПШ';
        $equipment['FV'] = 'ПФ';
        $equipment['FD'] = 'ПБ';
        $equipment['RD'] = 'ЗБ';
        $equipment['RV'] = 'ЗФ';
        $equipment['BW'] = 'ЗШ';
        //массив возвращаемых значений
        $resultSettext = [];

        $settext = isset($this->json('items')[0]['settext']) ? ($this->json('items')[0]['settext']) : '';
        $settext = explode(";", $settext);

        //Здесь в случае чего необходимо будет обойти массив и заного разбить по пробелам

        //После того как разбили на подстроки, делаем проверку, являються ли эти подстроки реальным показателем вида окна
        if (count($settext))
            foreach ($settext as $value) {
                if (in_array($value, $equipment))
                    $resultSettext[array_search($value,$equipment)] = $value;
            }

        //Удаляем из массива не нужное значение самого наряда
        if($key = array_search($this->window, $resultSettext))
            unset($resultSettext[$key]);

        return $resultSettext;
    }  


    /*
     * Метод возвращает цену наряда исходя из участка на который он был выдал
     * 
     */
    public function jobPrice()
    {
        $averageRate = $this->cnf('averageRate') ?: 0;
        $hourRate    = $this->cnf('hourrate')    ?: 0;

        $k = $this->halfStake ? 0.5 : 1;

        if ($averageRate !=0 && $hourRate !=0) {
            if ($this->location_id == Yii::$app->params['erp_clipsi']) {
                $value = explode('-', $this->article);
                // для доп нарядов
                if ($this->article == 'OT-564-0-0') {
                    return ($this->priceAddClips() ?: 0) * $k;
                } elseif(ArticleHelper::isOnMagnets($this->article)) {
                   return ($this->getGlueMagnetJobPrice() * $k);
                } else {
                   return ($this->priceClips($averageRate, $hourRate) ? $this->priceClips($averageRate, $hourRate) : 0) * $k;
                }
            } elseif($this->location_id == Yii::$app->params['erp_shveika'] && $this->isNeedTemplate) {
                return ($this->priceAddTemplate($this->article) ?: 0) * $k;
            } elseif($this->location_id == Yii::$app->params['erp_shveika'] && $this->velkroFlag()) {
                return ($this->shveikaWithVelkro($averageRate, $hourRate)) * $k;
            }
            /*ищем вид продукта*/
            $product_types = ErpProductType::find()->all();
            if (isset($product_types))
            {
                foreach ($product_types as $product_type) {
                    //если установлено правило и оно подходит под номенклатуру
                    if ($product_type->rule && preg_match($product_type->rule, $this->article)) {
                        $currentType = $product_type;
                        break;
                    }
                }
                //если нашли продукт который соответствует правилу, вытаскиваем записи из карточки продукта
                if (isset($currentType) && $currentType->production_scheme_id) {

                    $query = (new \yii\db\Query())
                        ->select('SUM("' . $averageRate . '"/jt.rate*js.job_count*"' . $hourRate . '") as jobPrice')
                        ->from('laitovo_erp_job_scheme as js')
                        ->leftJoin('laitovo_erp_job_type as jt', 'jt.id=js.type_id')
                        ->where([
                            'js.production_scheme_id' => $currentType->production_scheme_id,
                            'js.location_id' => $this->location_id
                        ])
                        ->andWhere(['!=', 'jt.id', Yii::$app->params['velkro']])
                        ->groupBy('js.location_id')
                        ->one();
                    if(isset($query['jobPrice'])) {
                        /**
                         * Блок изменения цены в зависимости от флажка - другая цена
                         */
//                        if ($this->user && $this->user->otherPricing && $this->location_id == Yii::$app->params['erp_shveika']) {
//                            if ($this->article == 'OT-1189-47-3') {
//                                return round($query['jobPrice'] / 1.66666666667, 2);
//                            }
//                        }

                        if ($this->getDoubleElement()) {
                            return round($query['jobPrice'] * $this->getDoubleElement() * $k, 2);
                        } else {
                            return round($query['jobPrice'] * $k, 2);
                        }
                    }
                }
            }
        }
    }

    
    public function windowBW()
    {
        return ArticleHelper::windowBW($this->article);
    }
    public function isNoName()
    {
        return ArticleHelper::isNoName($this->article);
    }

    /**
     * Возвращает цену, если дополнительные комлпекты клипс
     * @return mixed
     */
    public function priceAddClips()
    {
        $job = $this->jobPriceAddClips();
        return $job->price;
    }

    /**
     * Расчет работы для дополнительного комлпекта клипс
     * @return object
     */
    public function jobPriceAddClips()
    {
        if ($this->_jobState) return $this->_jobState;

        $averageRate = $this->cnf('averageRate') ?: 0;
        $hourRate    = $this->cnf('hourrate')    ?: 0;

        $subQuery = (new \yii\db\Query())
            ->select('SUM(job_price) as jobPrice, naryad_id as workOrder')
            ->from('laitovo_erp_users_report')
            ->where(['and',
                ['location_id' => Yii::$app->params['erp_clipsi']],
                ['is not','job_price', null],
                ['is not','naryad_id', null],
                ['!=','job_price',0],
                ['!=','job_price',0],
            ])
            ->orderBy(['id' => SORT_DESC])
            ->groupBy('naryad_id')
            ->limit(200);

        $query = (new \yii\db\Query())
            ->select('SUM(jobPrice)/COUNT(workOrder) as jobPrice')
            ->from(['jobs' => $subQuery])
            ->one();

        $jobPrice = isset($query['jobPrice'])   ? round($query['jobPrice'], 2)  : 0;
        $jobCount = 1;
        $jobRate  = $jobPrice == 0? 0 :round($averageRate / $jobPrice * $jobCount * $hourRate,0);
        $jobRate  = round($jobRate / 1.8,3); //Увеличиваем ценник за ДОП наряд 26.01.2024 Евдокимов А.А. по указанию Антонова Д.Л.
        #Пересчет для нормализации
        $jobPrice = round($averageRate / $jobRate * $jobCount * $hourRate,2);

        $result = [
            'rate' => $jobRate,
            'count' => $jobCount,
            'price' => $jobPrice,
        ];

        $this->_jobState = (object)$result;

        return $this->_jobState;
    }

    /**
     * Запись о работе с дополнительным комлпектом клипс.
     * @return bool
     */
    public function erpUsersReportAddClips()
    {
        $job = $this->jobPriceAddClips();

        $model              = new ErpUsersReport();
        $model->location_id = $this->location_from;
        $model->user_id     = $this->user_id;
        $model->naryad_id   = $this->id;
        $model->job_rate    = $job->rate;
        $model->job_count   = $job->count;
        $model->job_price   = $job->price;

        if ($this->actNumber) {
            $model->job_rate = $model->job_rate * 1.1;
        }elseif ($this->prodGroup && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
            $model->job_rate = $model->job_rate * 1.1;
        }

        return $model->save();
    }

    /**
     * Запись о работе по созданию лекала.
     *
     * @param $article
     * @return bool
     */
    public function erpUsersReportAddTemplate($article)
    {
        if (!$article) return false;

        $job = $this->jobAddTemplate($article);

        $model              = new ErpUsersReport();
        $model->location_id = $this->location_from;
        $model->user_id     = $this->user_id;
        $model->naryad_id   = $this->id;

        $model->job_rate    = $job->rate;
        $model->job_count   = $job->count;
        $model->job_price   = $job->price;

        if ($this->actNumber) {
            $model->job_rate = $model->job_rate * 1.1;
        }elseif ($this->prodGroup && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
            $model->job_rate = $model->job_rate * 1.1;
        }

        return $model->save();
    }

    /**
     * Получить цену за изготовление лекала
     *
     * @param $article
     * @return int
     */
    public function priceAddTemplate($article)
    {
        $job = $this->jobAddTemplate($article);

        return $job ? $job->price : 0;
    }

    /**
     * Получить работу за изготовлеие лекала для конретного артикула
     *
     * @param $article
     * @return null|object
     */
    public function jobAddTemplate($article)
    {
        $averageRate = $this->cnf('averageRate') ?: 0;
        $hourRate    = $this->cnf('hourrate')    ?: 0;

        $jobs = [];
        $window = ArticleHelper::getWindow($article);

        $value = explode('-', $article);
        if (!(isset($value[0]) && isset($value[3]) && $value[0] != 'OT' && ($value[3] == 4 ||  $value[3] == 5))) return null;

        $jobs['ПФ4'] = 144;
        $jobs['ПБ4'] = 144;
        $jobs['ЗБ4'] = 144;
        $jobs['ЗФ4'] = 144;
        $jobs['ЗШ4'] = 145;

        $jobs['ПФ5'] = 146;
        $jobs['ПБ5'] = 146;
        $jobs['ЗБ5'] = 146;
        $jobs['ЗФ5'] = 146;
        $jobs['ПШ5'] = 147;

        $key = $window . $value[3];

        if (!isset($jobs[$key]) || !($jobType = ErpJobType::findOne($jobs[$key]))) return null;

        $result = [
            'rate' => $jobType->rate,
            'count' =>  1,
            'price' =>  round($averageRate / $jobType->rate * 1 * $hourRate,2)
        ];

        return (object)$result;
    }


    // метод считает стоимость работы на клипсы для заданного наряда
    public function priceClips($averageRate, $hourrate)
    {
        if (isset($this->typeClips) && $averageRate!=0 && $hourrate!=0)
        {
            $total_sum = 0;
            foreach ($this->getTypeClips() as $key => $value) {
                if ($key == 'M') {
                    continue;
                } else {
                    if ($key == 'A') {
                        if (Yii::$app->params['jobClips1']) {
                            $jobType = ErpJobType::findOne(Yii::$app->params['jobClips1']);
                        }
                    } elseif ($key == 'AA') {
                        if (Yii::$app->params['jobClips1']) {
                            $jobType = ErpJobType::findOne(Yii::$app->params['jobClips1']);
                        }
                    } elseif ($key == 'B') {
                        if (Yii::$app->params['jobClips2']) {
                            $jobType = ErpJobType::findOne(Yii::$app->params['jobClips2']);
                        }
                    } elseif ($key == 'C') {
                        if (Yii::$app->params['jobClips3']) {
                            $jobType = ErpJobType::findOne(Yii::$app->params['jobClips3']);
                        }
                    } elseif ($key == 'CC') {
                        if (Yii::$app->params['jobClips3']) {
                            $jobType = ErpJobType::findOne(Yii::$app->params['jobClips3']);
                        }
                    } elseif ($key == 'V') {
                        continue;
                    }
                    if (isset($jobType->rate) && $value) {
                        //                       средняя дневная норма                         цена нормированного коэффициента
                        $total_sum += $averageRate / $jobType->rate * $value * $hourrate;
                    }
                }
            }
            return round($total_sum, 2);
        }
    }

    // метод считает стоимость работы на клипсы для заданного наряда
    public function getGlueMagnetJobPrice()
    {
        $averageRate = $this->cnf('averageRate') ?: 0;
        $hourRate    = $this->cnf('hourrate')    ?: 0;
        if (!ArticleHelper::isOnMagnets($this->article)) return 0;
        $jobClipsMagnets = Yii::$app->params['jobClipsMagnets'] ?: null;
        if (($jobType = ErpJobType::findOne($jobClipsMagnets)) && $jobType->rate) {
            return $averageRate / $jobType->rate * 1 * $hourRate;
        } else {
            return 0;
        }
    }

    ///вытаскиваем массив клипс по наряду тип клипсы(А,B,C) => количество
    public function getTypeClips()
    {
        $newClips = [];
        $newArray = [];
        if ($this->getCar() && $this->getCar()->one()) {
            $car_id = $this->getCar()->one()->id;
            $clipsShema = ClipsScheme::find()->where([
                'window' => $this->getWindow(),
                'car_id' => $car_id,
                'brand'=>$this->getBrand(),
                'type' => $this->getType(),
                'type_clips'=>$this->getType_clips(),
            ])->one();
        }

        if (isset($clipsShema)) {
            $clips = Json::decode($clipsShema->json);
            foreach ($clips['scheme'] as $item => $value) {
                if ($value) $newClips[] = $value;
            }

            if (isset($newClips) && count($newClips)) {
                foreach ($newClips as $key => $item) {
                    if (isset($newArray[CarClips::findOne($item)->type])) {
                        $newArray[CarClips::findOne($item)->type]++;
                    } else {
                        $newArray[CarClips::findOne($item)->type] = 1;
                    }

                }

                foreach ($newArray as $key => $value) {
                    switch ($this->getWindow()) {
                        case 'ПШ':
                            $newArray[$key] = ($value * 1);
                            break;
                        case 'ПФ':
                            $newArray[$key] = ($value * 2);
                            break;
                        case 'ПБ':
                            $newArray[$key] = ($value * 2);
                            break;
                        case 'ЗБ':
                            $newArray[$key] = ($value * 2);
                            break;
                        case 'ЗФ':
                            $newArray[$key] = ($value * 2);
                            break;
                        case 'ЗШ':
                            $newArray[$key] = ($value * 1);
                            break;
                        default:
                            $newArray[$key] = ($value * 1);
                    }
                }
            }
        }
        return $newArray;
    }


    ///вытаскиваем массив клипс по наряду тип клипсы(А,B,C) => количество
    public function getTypeClipsStr()
    {
        if (!$this->getCar()) return '';

        $newClips = [];
        $newArray = [];
        $clipsShema = ClipsScheme::find()->where([
            'window' => $this->getWindow(),
            'car_id' => $this->getCar()->one()->id,
            'brand'=>$this->getBrand(),
            'type' => $this->getType(),
            'type_clips'=>$this->getType_clips(),
        ])->one();
        if (isset($clipsShema)) {
            $clips = Json::decode($clipsShema->json);
            foreach ($clips['scheme'] as $item => $value) {
                if ($value) $newClips[] = $value;
            }

            if (isset($newClips) && count($newClips)) {
                foreach ($newClips as $key => $item) {
                    if (isset($newArray[@CarClips::findOne($item)->type])) {
                        $newArray[@CarClips::findOne($item)->type]++;
                    } else {
                        $newArray[@CarClips::findOne($item)->type] = 1;
                    }

                }

                foreach ($newArray as $key => $value) {
                    switch ($this->getWindow()) {
                        case 'ПШ':
                            $newArray[$key] = ($value * 1);
                            break;
                        case 'ПФ':
                            $newArray[$key] = ($value * 2);
                            break;
                        case 'ПБ':
                            $newArray[$key] = ($value * 2);
                            break;
                        case 'ЗБ':
                            $newArray[$key] = ($value * 2);
                            break;
                        case 'ЗФ':
                            $newArray[$key] = ($value * 2);
                            break;
                        case 'ЗШ':
                            $newArray[$key] = ($value * 1);
                            break;
                        default:
                            $newArray[$key] = ($value * 1);
                    }
                }
            }
        }
        if (!count($newArray)) return '';

        $str = '';
        foreach ($newArray as $type => $countClips) {
            $str .= $type . '-' . $countClips . ' ';
        }

        return ' <span style="color:red">Типы клипс :</span> [ ' . $str . ' ]';
    }



    public function erpUsersReport($averageRate, $hourrate, $job_type_id, $job_count, $job_rate)
    {
        if ($this->location_from == Yii::$app->params['erp_shveika'] && $this->withoutLabels && $job_type_id == 44)
            return true;
        $model = new ErpUsersReport();
        $model->location_id = $this->location_from;
        $model->user_id = $this->user_id;
        $model->naryad_id = $this->id;
        $model->job_type_id = $job_type_id;
        $model->job_count = $job_count;
        if(($double = $this->getDoubleElement())){
            $model->job_rate = $job_rate / $double;
        }else{
            $model->job_rate = $job_rate;
        }

        if ($this->actNumber) {
            $model->job_rate = $model->job_rate * 1.1;
        }elseif ($this->prodGroup && $this->order && $this->order->json('username') == 'ПК Полипласт ООО') {
            $model->job_rate = $model->job_rate * 1.1;
        }

        if ($this->halfStake) {
            $model->job_rate = $model->job_rate * 0.5;
        }

        /**
         * Блок изменения цены в зависимости от флажка - другая цена
         */
//        if ($this->user && $this->user->otherPricing && $this->location_from == Yii::$app->params['erp_shveika']) {
//            if ($this->article == 'OT-1189-47-3') {
//                $model->job_rate = $model->job_rate * 1.66666666667;
//            }
//        }

        $model->job_price = round($averageRate / $model->job_rate * $job_count * $hourrate,2);
        if($model->save())
            return true;
    }

    //выводим в реестр для клипс детализацию по наряду с суммой типа клипс
    public function getDetailClisp()
    {
        if (ArticleHelper::isOnMagnets($this->article))
            return 'Приклеить магниты - ' . $this->getGlueMagnetJobPrice();
        $value = explode('-',$this->article);
        if($value[0] && $value[0]!='OT')
        {
            $newClips = [];
            $newArray = [];
            $clipsShema = ClipsScheme::find()->where([
                'window' => $this->getWindow(),
                'car_id' => $this->getCar()->one()->id,
                'brand'=>$this->getBrand(),
                'type' => $this->getType(),
                'type_clips'=>$this->getType_clips(),
            ])->one();
            if (isset($clipsShema)) {
                $clips = Json::decode($clipsShema->json);
                foreach ($clips['scheme'] as $item => $value) {
                    if ($value) $newClips[] = $value;
                }

                if (isset($newClips) && count($newClips)) {
                    foreach ($newClips as $key => $item) {
                        if (isset($newArray[CarClips::findOne($item)->id])) {
                            $newArray[CarClips::findOne($item)->id]['value']++;
                        } else {
                            $newArray[CarClips::findOne($item)->id]['value'] = 1;
                            $newArray[CarClips::findOne($item)->id]['name'] = CarClips::findOne($item)->name;
                            $newArray[CarClips::findOne($item)->id]['type'] = CarClips::findOne($item)->type;
                        }
                    }
                    foreach ($newArray as $key => $value) {
                        switch ($this->getWindow()) {
                            case 'ПШ':
                                $newArray[$key]['value'] = ($value['value'] * 1);
                                break;
                            case 'ПФ':
                                $newArray[$key]['value'] = ($value['value'] * 2);
                                break;
                            case 'ПБ':
                                $newArray[$key]['value'] = ($value['value'] * 2);
                                break;
                            case 'ЗБ':
                                $newArray[$key]['value'] = ($value['value'] * 2);
                                break;
                            case 'ЗФ':
                                $newArray[$key]['value'] = ($value['value'] * 2);
                                break;
                            case 'ЗШ':
                                $newArray[$key]['value'] = ($value['value'] * 1);
                                break;
                            default:
                                $newArray[$key]['value'] = ($value['value'] * 1);
                        }
                    }
                }
                $result = '';
                foreach ($newArray as $array)
                {
                    $result .= '№' .$array['name']. '/' .$array['type']. ' - '.$array['value'].' - '. $this->priceOneClipsType($array['type'], $array['value']). ' || ';
                }
                return $result ;
            }

        }else{
            return 'нет автомобиля в артикуле';
        }

    }


    //считаем цену каждого типа клипс в наряде
    public function priceOneClipsType($type, $value)
    {
        $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
        if ($this->cnf('averageRate') != 0 && $this->cnf('hourrate') != 0) {
            if ($type && $type == 'A') {
                $jobType = ErpJobType::findOne(Yii::$app->params['jobClips1']);
            }
            if ($type && $type == 'AA') {
                $jobType = ErpJobType::findOne(Yii::$app->params['jobClips1']);
            }
            if ($type && $type == 'B') {
                $jobType = ErpJobType::findOne(Yii::$app->params['jobClips2']);
            }
            if ($type && $type == 'C') {
                $jobType = ErpJobType::findOne(Yii::$app->params['jobClips3']);
            }
            if ($type && $type == 'CC') {
                $jobType = ErpJobType::findOne(Yii::$app->params['jobClips3']);
            }

            if (isset($jobType->rate) && $value) {
                //                       средняя дневная норма                         цена нормированного коэффициента
                return round($this->cnf('averageRate') / $jobType->rate * $value * $this->cnf('hourrate'),2);
            }

        }
    }

    //даем множитель если находим что для этого наряда на сдвоенный элемент
    public function getDoubleElement()
    {
         $value = explode('-', $this->article) ;
         if(@$value[0] && @$value[2])
         {
             $conformity = $this->conformity($value[0]);
             if (!$conformity) return null;

             $car = Cars::find()->where(['article'=>$value[2]])->one();
             if (!$car) return null;

             $json = Json::decode($car->json);
             if(isset($json[$conformity]) && $json[$conformity])
             {
                 return $this->multiplier($value[0]);
             }

         }
        return null;
    }

//определяем тип оконного проема и возвращаем значение для поиска сдвоенного элемента
    public function conformity($window)
    {
        if($window == 'FV') return 'dlinapf1';
        if($window == 'FD') return 'dlinapb1';
        if($window == 'RD') return 'dlinazb1';
        if($window == 'RV') return 'dlinazf1';
        if($window == 'BW') return 'dlinazs1';
        return false;
    }

    public function multiplier($window)
    {
        if($window == 'FV') return 1.5;
        if($window == 'FD') return 1.5;
        if($window == 'RD') return 1.5;
        if($window == 'RV') return 1.5;
        if($window == 'BW') return 1.5;
        return 1;
    }

//определяем есть ли в наряде клипсы с велькро что бы засчитать на швейке доп работу по нашивке велькро
    public function velkroFlag()
    {
        $value = explode('-',$this->article);
        if($value[0] && $value[0]!='OT') {
            $newClips = [];
            $clipsShema = ClipsScheme::find()->where([
                'window' => $this->getWindow(),
                'car_id' => $this->getCar()->one()->id,
                'brand' => $this->getBrand(),
                'type' => $this->getType(),
                'type_clips' => $this->getType_clips(),
            ])->one();
            if (isset($clipsShema)) {
                $clips = Json::decode($clipsShema->json);
                foreach ($clips['scheme'] as $item => $value) {
                    if ($value) $newClips[] = $value;
                }

                if (isset($newClips) && count($newClips)) {
                    foreach ($newClips as $key => $item) {
                        if (CarClips::findOne($item) && CarClips::findOne($item)->name == 'V')
                            return true;
                    }
                }
            }
        }
        return false;
    }

    //расчет стоимости заработка в нарядах с велькро
    public function shveikaWithVelkro($averageRate, $hourrate)
    {
        $velcroPrice = 0;
        $jobPrice = 0;
        /*ищем вид продукта*/
        $product_types = ErpProductType::find()->all();
        if (isset($product_types))
        {
            foreach ($product_types as $product_type) {
                //если установлено правило и оно подходит под номенклатуру
                if ($product_type->rule && preg_match($product_type->rule, $this->article)) {
                    $currentType = $product_type;
                    break;
                }
            }

            //если нашли продукт который соответствует правилу, вытаскиваем записи из карточки продукта
            //все кроме работ по пришиванию велькро т к в карточке продукта он записывается неправильно
            if (isset($currentType) && $currentType->production_scheme_id) {

                $query = (new \yii\db\Query())
                    ->select('SUM("'.$averageRate.'"/jt.rate*js.job_count*"'.$hourrate.'") as jobPrice')
                    ->from('laitovo_erp_job_scheme as js')
                    ->leftJoin('laitovo_erp_job_type as jt', 'jt.id=js.type_id')
                    ->where([
                        'js.production_scheme_id' => $currentType->production_scheme_id,
                        'js.location_id' => $this->location_id,
                    ])
                    ->andWhere(['!=', 'jt.id', Yii::$app->params['velkro']])
                    ->groupBy('js.location_id')
                    ->one();


            //отдельно достаем вид работ для велькро и плюсуем его к общему результату
                $velkro = ErpJobType::findOne(Yii::$app->params['velkro']);

                if($velkro && $velkro->rate)
                    $velcroPrice = $averageRate/ $velkro->rate*$hourrate;

                if(isset($query['jobPrice'])) {
                    if ($this->getDoubleElement()) {
                       $jobPrice = $query['jobPrice'] * $this->getDoubleElement();
                    } else {
                       $jobPrice =  $query['jobPrice'];
                    }
                }
                return round($velcroPrice+$jobPrice,2);
            }
        }
    }

    //Запрос на наряды без схем
    public static function queryNoScheme()
    {
        return self::find()
        ->where(['or',
            ['scheme_id' => null],
            ['scheme_id' => '']
        ]);
    }

    public function getUpdateUrl()
    {
        return Yii::$app->urlManager->createUrl(['laitovo/erp-naryad/update','id' => $this->id]);
    }

    public function getDontLook()
    {
        $value = explode('-', $this->article);
        if (isset($value[3]) && $value[3] == 5 ) {
            return PatternRegister::BK;
        }
        if (isset($value[3]) && $value[3] == 4) {
            return PatternRegister::SD;
        }
    }


    public function getShLekalo()
    {
        $cars = [];
        $car_id  = $this->getCar()->one()->id;
        $cars[] = $car_id;
        $analogs = $this->getCar()->one()->analogs;
        foreach ($analogs as $analog) {
            $elements = $analog->json('elements');
            if (in_array($this->getWindow(),$elements)) {
                $cars[] = $analog->analog_id;
            }
        }
        if($car_id && $this->getWindow() && $this->getDontLook()) {
            $pattern_register = PatternRegister::find()->where(['and',
                ['window_type' => $this->getWindow()],
                ['product_type' => $this->getDontLook()],
                ['in','car_id',$cars]
            ])->one();
            return $pattern_register ? $pattern_register->pattern_article : '';
        }
    }

    /**
     * [isExport Функция, проверяющая, являеться ли наряд экспортным]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-26
     * @anotherdate 2017-10-26T11:48:10+0300
     * @return      boolean                  [Если экспорт - true]
     */
    public function isExport()
    {
        if ($this->order && $this->order->json('category')=='Экспорт' && $this->order->json('country')!='Казахстан' && $this->order->json('country')!='Киргизия' && $this->order->json('country')!='Беларусь') {
            return true;
        }

        return false;
    }

    public function isHindu()
    {
        if ($this->order && $this->order->json('username') == 'Hot Tracks') {
            return true;
        }
        return false;
    }

    /**
     *проверяем был ли исполнитель с предыдущего участка стажером
     * @return boolean
     */
    public function lastExecutorTrainee()
    {
        if (empty($this->json('log')[count($this->json('log'))-2])) {
            return false;
        }
        $lastLog = $this->json('log')[count($this->json('log'))-2];

        if (empty($lastLog['user'])) {
            return false;
        }

        $lastUserName = $lastLog['user'];
        $user = ErpUser::findOne(['name' => $lastUserName]);
        if (!$user || ($user && !$user->trainee)) {
            return false;
        }
        return true;
    }

    /**
     * Общее количество нарядов
     *
     * @return int
     */
    public static function countAll()
    {
        return self::find()->count();
    }

    /**
     * Количество нарядов на производстве
     *
     * @return int
     */
    public static function countOnProduction()
    {
        return self::find()->where(['or',
            ['in', 'status', [self::STATUS_IN_WORK, self::STATUS_IN_PAUSE]],
            ['status' => null]
        ])->count();
    }

    /**
     * Количество нарядов на паузе
     *
     * @return int
     */
    public static function countOnPause()
    {
        return self::find()->where(['status' => ErpNaryad::STATUS_IN_PAUSE])->count();
    }

    /**
     * Проверка, что наряд находится на производстве
     *
     * @return bool
     */
    public function isOnProduction()
    {
        return !$this->status || $this->status == self::STATUS_IN_WORK || $this->status == self::STATUS_IN_PAUSE;
    }

    /**
     * Проверка, может ли наряд менять срочность
     *
     * @return bool
     */
    public function canUpdatePriority() {
        return $this->isOnProduction();
    }

    /**
     * Изменение срочности наряда
     *
     * @param int $sort
     * @return array
     */
    public function updatePriority($sort)
    {
        if (!$this->canUpdatePriority()) {
            return [
                'status'  => false,
                'message' => Yii::t('app', 'Данный наряд не может менять срочность')
            ];
        }
        if ($this->sort == 5) {
            if($sort < 5) {
                $this->sort = $sort;
            }
        } else {
            $this->sort = $sort;
        }

        $this->autoSpeed = false;

        $this->oldSort = $sort;

        if (!$this->save()) {
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось сохранить наряд')
            ];
        }

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    /**
     * Функция сравнивает две группы нарядов по срочности
     *
     * @param ErpNaryad[] $naryads1
     * @param ErpNaryad[] $naryads2
     * @return int -1,0,1 равносильно >,<,=
     */
    public static function compareByPriority(array $naryads1, array $naryads2)
    {
        $collection1 = [];
        foreach ($naryads1 as $item) {
            if ($item->order_id) {
                $collection1[] = $item;
            }
        }

        $collection2 = [];
        foreach ($naryads2 as $item) {
            if ($item->order_id) {
                $collection2[] = $item;
            }
        }

        $count1 = count($collection1);
        $count2 = count($collection2);
        switch (true) {
            case $count1 && !$count2:
                return -1;
            case !$count1 && $count2:
                return 1;
            case !$count1 && !$count2:
                return 0;
            default:
                break;
        }
        $sorts1 = [];
        $dates1 = [];
        foreach ($collection1 as $naryad) {
            $sorts1[] = $naryad->sort;
            $dates1[] = $naryad->created_at;
        }
        $sortAverage1   = round(array_sum($sorts1)/$count1, 2);
        $sortDeviation1 = round(self::_getStandardDeviation($sorts1), 2);
        $dateAverage1   = round(array_sum($dates1)/$count1, 2);
        $dateDeviation1 = round(self::_getStandardDeviation($dates1), 2);
        $sorts2 = [];
        $dates2 = [];
        foreach ($collection2 as $naryad) {
            $sorts2[] = $naryad->sort;
            $dates2[] = $naryad->created_at;
        }
        $sortAverage2   = round(array_sum($sorts2)/$count2, 2);
        $sortDeviation2 = round(self::_getStandardDeviation($sorts2), 2);
        $dateAverage2   = round(array_sum($dates2)/$count2, 2);
        $dateDeviation2 = round(self::_getStandardDeviation($dates2), 2);
        switch (true) {
            case $sortAverage1 < $sortAverage2:
                return -1;
            case $sortAverage1 > $sortAverage2:
                return 1;
            case ($sortAverage1 == $sortAverage2) && ($sortDeviation1 > $sortDeviation2):
                return -1;
            case ($sortAverage1 == $sortAverage2) && ($sortDeviation1 < $sortDeviation2):
                return 1;
            case ($sortAverage1 == $sortAverage2) && ($sortDeviation1 == $sortDeviation2) && ($dateAverage1 < $dateAverage2):
                return -1;
            case ($sortAverage1 == $sortAverage2) && ($sortDeviation1 == $sortDeviation2) && ($dateAverage1 > $dateAverage2):
                return 1;
            case ($sortAverage1 == $sortAverage2) && ($sortDeviation1 == $sortDeviation2) && ($dateAverage1 == $dateAverage2) && ($dateDeviation1 > $dateDeviation2):
                return -1;
            case ($sortAverage1 == $sortAverage2) && ($sortDeviation1 == $sortDeviation2) && ($dateAverage1 == $dateAverage2) && ($dateDeviation1 < $dateDeviation2):
                return 1;
            default:
                return 0;
        }
    }

    /**
     * Функция вычисляет стандартное отклонение массива
     *
     * @param $arr
     * @return float
     */
    private static function _getStandardDeviation($arr)
    {
        $n = count($arr);
        if (!$n) {
            return 0.0;
        }
        $mean = array_sum($arr) / $n;
        $carry = 0.0;
        foreach ($arr as $val) {
            $d = ((double) $val) - $mean;
            $carry += $d * $d;
        };

        return sqrt($carry / $n);
    }


    /**
     * @param $barcode
     * @return mixed|null
     */
    public static function findAct($barcode)
    {
        if (mb_stripos($barcode,self::MOVE_ACT_BARCODE_PREFIX) === false) return null;
        $actNumber = str_ireplace(self::MOVE_ACT_BARCODE_PREFIX,'',$barcode);
        if (self::find()->where(['actNumber'=>$actNumber])->exists())
            return $actNumber;

        return null;
    }


    /**
     * @param $barcode
     * @return mixed|null
     */
    public static function findProdGroup($barcode)
    {
        if (mb_stripos($barcode,self::PROD_GROUP_BARCODE_PREFIX) === false) return null;
        $actNumber = str_ireplace(self::PROD_GROUP_BARCODE_PREFIX,'',$barcode);
        if (self::find()->where(['prodGroup'=>$actNumber])->exists())
            return $actNumber;

        return null;
    }


    /**
     * @param $barcode
     * @return mixed|null
     */
    public static function findAddGroup($barcode)
    {
        if (mb_stripos($barcode,self::ADD_GROUP_BARCODE_PREFIX) === false) return null;
        $addGroupNumber = str_ireplace(self::ADD_GROUP_BARCODE_PREFIX,'',$barcode);
        if (self::find()->where(['addGroup'=> $addGroupNumber])->exists())
            return $addGroupNumber;

        return null;
    }

    /**
     * @param $actNumber
     * @return string
     */
    public static function generateActBarcode($actNumber)
    {
        return self::MOVE_ACT_BARCODE_PREFIX . $actNumber;
    }

    public function getReworkActs()
    {
        return $this->hasMany(LaitovoReworkAct::className(), ['naryad_id' => 'id']);
    }
}
