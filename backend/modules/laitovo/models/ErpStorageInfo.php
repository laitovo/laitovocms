<?php

namespace backend\modules\laitovo\models;

use backend\modules\logistics\models\StorageState;
use core\models\article\Article;
use core\models\realizationLog\RealizationLog;
use Yii;
use Yii\base\Model;


/**
*  Модель для отображения экрана логистики
*/
class ErpStorageInfo extends Model
{
	public function rules()
	{
        return [

       ];
	}

	public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * [getCountUpnToPosting Функция для получения количества UPN на диспетчер-склад]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-31
     * @anotherdate 2017-10-31T11:08:53+0300
     * @return      [integer]                   [Количество нарядов, которые ожидают отгрузки на склад]
     */
    public static function getCountUpnToPosting()
    {
    	return ErpNaryad::find()->where(['and',
    		['and',['is not','logist_id', null],['!=','logist_id','']], //только наряды с логистики
    		['status' => ErpNaryad::STATUS_READY], //только готовые наряды
    		['distributed' => false], //только готовые наряды
    	])->count();
    }

    /**
     * [getCountUpnToPosting Функция для получения количества UPN на диспетчер-склад]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-31
     * @anotherdate 2017-10-31T11:08:53+0300
     * @return      [integer]                   [Количество нарядов, которые ожидают отгрузки на склад]
     */
    public static function getCountUpnToPostingAF()
    {
    	return ErpNaryad::find()->where(['and',
            ['>','ready_date', time()-60*30-3600*24*6*30],
    		['and',['is not','logist_id', null],['!=','logist_id','']], //только наряды с логистики
    		['status' => ErpNaryad::STATUS_READY], //только готовые наряды
    		['distributed' => false], //только готовые наряды
            ['like','article','OT-2079%', false], //только готовые наряды
    	])->count();
    }

    /**
     * [getCountUpnInProduction Функция для получения количества UPN в процессе производства ]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-31
     * @anotherdate 2017-10-31T11:33:19+0300
     * @return      [integer]                   [Количество UPN которые еще производятся]
     */
    public static function getCountUpnInProduction()
    {
    	return ErpNaryad::find()->where(['and',
    		['and',['is not','logist_id', null],['!=','logist_id','']], //только наряды с логистики
    		['or',
    			['status' => ErpNaryad::STATUS_IN_PAUSE],
    			['status' => ErpNaryad::STATUS_IN_WORK],
			] //только готовые наряды
    	])->count();
    }

    /**
     * [getCountUpnOverdue Функция, отображающая количество посроченных нарядов]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-31
     * @anotherdate 2017-10-31T11:49:29+0300
     * @return      [integer]                   [Количество просроченных нарядлов]
     */
    public static function getCountUpnOverdue()
    {
		return ErpNaryad::find()
            ->where(['<','ready_date', time()-60*30])
            ->andWhere(['>','ready_date', time()-60*30-3600*24*365])
            ->andWhere(['distributed_date' => null])
            ->andWhere(['status' => ErpNaryad::STATUS_READY])
            ->andWhere(['!=','logist_id',''])
            ->andWhere(['is not','logist_id', null])
            ->count();
//
//
//        return ErpNaryad::find()->where(
//            ['and',
//    		['and',['is not','logist_id', null],['!=','logist_id','']], //только наряды с логистики
//    		['status' => ErpNaryad::STATUS_READY], //только готовые наряды
//    		['and',['distributed_date' => null],['<','ready_date', time()-60*30]], //которые были готовы 6 часов назад
//    	])->count();
    }

    /**
     * [getUpnOverdueByReadyDate Фукнция для получения просроченного наряда]
     * This is a cool function
     * @return ErpNaryad[] [ErpNaryad] [Возвращает непосредственно наряда со свойствами]
     * @version     [1.0]
     * @date        2017-10-31
     * @anotherdate 2017-10-31T11:57:24+0300
     * @author Алексей Евдокимов <heksweb@gmail.com>
     */
    public static function getUpnOverdueByReadyDate(): array
    {
		return ErpNaryad::find()
            ->where(['<','ready_date', time()-60*60*6])
            ->andWhere(['>','ready_date', time()-60*30-3600*24*365])
            ->andWhere(['distributed_date' => null])
            ->andWhere(['status' => ErpNaryad::STATUS_READY])
            ->andWhere(['!=','logist_id',''])
            ->andWhere(['is not','logist_id', null])
            ->orderBy('ready_date')
            ->limit(5)
            ->all();
    }

    /**
     * [getSumCountUpn Функция для получения общего количества UPN на производстве]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-31
     * @anotherdate 2017-10-31T12:00:49+0300
     * @return      [integer]                   [Количество UPN на производстве]
     */
    public static function getSumCountUpn()
    {
		return self::getCountUpnInProduction() + self::getCountUpnToPosting();  	
    }

    /**
     * [getUpnOnDispathcerProvider Возвращает датапровайдер нарядов на диспетчере склад]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-11-01
     * @anotherdate 2017-11-01T07:48:12+0300
     * @return      [ArrayDataProvider]                   [Список нарядов]
     */
    public function getUpnOnDispathcerQuery()
    {
        return ErpNaryad::find()->where(['and',
            ['and',['is not','logist_id', null],['!=','logist_id','']], //только наряды с логистики
            ['status' => ErpNaryad::STATUS_READY], //только готовые наряды
            ['distributed' => false], //только готовые наряды
        ]);

    }

    public static function getCountAutoFilterOnStorage()
    {
        return StorageState::find()->joinWith('upn')
            ->where(['and',
                ['like','article','OT-2079%', false], //только готовые наряды
            ])->count();
    }

    public static function getCountAutoFilterComingToStorage($dateFrom,$dateTo)
    {
        return ErpNaryad::find()->where(['and',
            ['and',['is not','logist_id', null],['!=','logist_id','']], //только наряды с логистики
            ['status' => ErpNaryad::STATUS_READY], //только готовые наряды
            ['like','article','OT-2079%', false], //только готовые наряды
            ['>=','distributed_date',$dateFrom], //только готовые наряды
            ['<=','distributed_date',$dateTo + (60 * 60 * 24 - 1)], //только готовые наряды
        ])->count();
    }

    public static function getCountAutoFilterRealizated()
    {
        return RealizationLog::find()->joinWith('orderEntry')
            ->where(['and',
            ['like','article','OT-2079%', false], //только готовые наряды
        ])->count();
    }
}