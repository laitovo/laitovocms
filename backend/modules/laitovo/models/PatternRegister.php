<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 09.10.17
 * Time: 10:06
 */

namespace backend\modules\laitovo\models;

use common\models\laitovo\Cars;

class PatternRegister extends \common\models\laitovo\PatternRegister
{
    public $litera;
    public $car;

    protected $status;

    public static $litera_items = [
        'А','Б','В','Г','Д','Е','Ж','З','И','К'
    ];

    public function getStatus()
    {
        return  !isset($this->status) ? true : $this->status;
    }

    public function setStatus($value)
    {
        $this->status = $value;
    }

    public function rules(){
        return [
            ['car', 'safe'],
            ['status', 'boolean'],
            ['litera', 'safe'],
            ['product_type', 'string'],
            ['window_type', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'status' => 'не показывать неосвоенные',
            'car' => 'Поиск',
            'litera' => 'Литера',
        ];
    }

    public function search()
    {
        if ($this->getStatus() != null) {
            $query = Cars::find()->joinWith('patternRegister p')->where(['is not', 'p.pattern_article', null])->groupBy('p.car_id');
        }

        if($this->getStatus() == 0 || $this->car) {
            $query = Cars::find()->joinWith('patternRegister')->groupBy('id');
        }

        if($this->car) {
            $query->andWhere(['or',
                    ['like', 'name', '%' . $this->car . '%', false],
                    ['article' => $this->car],
                ]);
        }

        if (isset($_GET['sort']) && preg_match('/FW/',$_GET['sort']) === 1) {
            $query->andWhere(
                ['p.window_type' => 'ПШ']
            );
        }
        if (isset($_GET['sort']) && preg_match('/FV/',$_GET['sort']) === 1) {
            $query->andWhere(
                ['p.window_type' => 'ПФ']
            );
        }
        if (isset($_GET['sort']) && preg_match('/FD/',$_GET['sort']) === 1) {
            $query->andWhere(
                ['p.window_type' => 'ПБ']
            );
        }
        if (isset($_GET['sort']) && preg_match('/RD/',$_GET['sort']) === 1) {
            $query->andWhere(
                ['p.window_type' => 'ЗБ']
            );
        }
        if (isset($_GET['sort']) && preg_match('/RV/',$_GET['sort']) === 1) {
            $query->andWhere(
                ['p.window_type' => 'ЗФ']
            );
        }
        if (isset($_GET['sort']) && preg_match('/BW/',$_GET['sort']) === 1) {
            $query->andWhere(
                ['p.window_type' => 'ЗШ']
            );
        }
        return $query;
    }

    public function beforeSave($insert)
    {   $car = $this->getCar()->one();
        $this->pattern_article = $car->article . '-' . $this->product_type . '-' . $this->window_type . '-' . self::$litera_items[$this->litera];
        return parent::beforeSave($insert);
    }

    public function getLiteralCount()
    {
        $result = [];
        foreach (self::$litera_items as $key => $item) {
            $count = PatternRegister::find()->where(['like', 'pattern_article', '%'.$item, false])->count('car_id');
            $result[$item] = $count ;
        }
        return $result;
    }
}