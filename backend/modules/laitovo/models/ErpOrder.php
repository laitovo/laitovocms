<?php

namespace backend\modules\laitovo\models;

use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use Yii;
use common\models\order\Order;
use common\models\laitovo\Cars;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%laitovo_erp_order}}".
 */
class ErpOrder extends \common\models\laitovo\ErpOrder
{

    public $items='[]';
    public $inputfields=[];
    private $_json=[];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['status'], 'string', 'max' => 255],
            [['sort'], 'integer', 'min'=>1, 'max' => 7],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id'], 'filter' => ['team_id' => 1]],
            [['order_id'], 'unique'],
            [['items'], 'string'],
            [['items'], 'default','value' => '[]'],
            [['inputfields'], 'each','rule' => ['string']],
            [['status'], 'validateStatus'],
        ];
    }

    public function validateStatus($attribute, $params)
    {

        if ($this->status==self::STATUS_READY && $this->getNaryads()->count()!=$this->getNaryads()->andWhere(['or',['status'=>ErpNaryad::STATUS_READY],['status'=>ErpNaryad::STATUS_CANCEL],['status'=>ErpNaryad::STATUS_FROM_SKLAD]])->count())
            $this->addError($attribute, Yii::t('app', 'Наряды не закрыты',['attribute'=>$this->getAttributeLabel($attribute)]));
    }

    public function __construct($config = [])
    {
        foreach ($this->fields as $key => $value) {
            $this->inputfields[$value['fields']['id']['value']]=null;
        }

        parent::__construct($config);
    }

    public function getNaryads()
    {
        return $this->hasMany(ErpNaryad::className(), ['order_id' => 'id']);
    }

    public function afterFind()
    {
        $this->items=Json::encode( $this->json('items') );

        $this->_json=Json::decode($this->json ? : '{}');

        foreach ($this->fields as $key => $value) {
            $this->inputfields[$value['fields']['id']['value']]=isset($this->_json[$value['fields']['id']['value']]) ? $this->_json[$value['fields']['id']['value']] : '';
        }

    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert) && $this->toOneitem()) {

            $json = $this->_json;

            foreach ($this->fields as $key => $value) {
                $json[$value['fields']['id']['value']]=isset($this->inputfields[$value['fields']['id']['value']]) ? $this->inputfields[$value['fields']['id']['value']] : '';
            }

            $json['items']=Json::decode($this->items);

            $this->json=Json::encode($json);

            return true;
        } else {
            return false;
        }
    }

    public function toOneitem()
    {
        $items=Json::decode($this->items);
        $newitems=[];
        foreach ($items as $key => $value) {
            $quantity=$value['quantity'];
            if ($quantity>0){
                while ($quantity>=1) {
                    $value['quantity']=1;
                    $newitems[]=$value;
                    $quantity--;
                }
            }
        }
        $this->items=Json::encode($newitems);
        return true;
    }

    public function toNaryad($type=null, $itemsquantity=[])
    {
        $marker = ErpNaryad::find()->where(['order_id' => $this->id])->one();
        if ($marker) return false;
        
        $items=$this->json('items');
        if ($items) {
            $this->status=$this::STATUS_READY_TO_WORK;
            $this->save();
            switch ($type) {
                case 'group':
                    //объединить по артикулам
                    $naryaditems=[];
                    foreach ($items as $key => $value) {
                        $naryaditems[$value['article']][]=$value;
                    }
                    foreach ($naryaditems as $key => $value) {
                        $naryad=new ErpNaryad;
                        $naryad->order_id=$this->id;
                        $naryad->article=$key;
                        $naryad->items=Json::encode($value);
                        if (!$naryad->save()) return false;
                    }
                    return true;
                    break;
                
                case 'one':
                    //разбить по одному артикулу
                    $fromsklad=[];
                    foreach ($items as $key => $value) {
                        $quantity=count($itemsquantity) ? $itemsquantity[$key] : $value['quantity'];
                        if ($quantity>0){
                            while ($quantity>=1) {
                                @$position++;
                                $value['quantity']=1;
                                $naryad=new ErpNaryad;
                                $naryad->barcode='B'.$this->id.'D'.$position;
                                $naryad->order_id=$this->id;
                                $naryad->article=$value['article'];
                                $naryad->items=Json::encode([$value]);
                                $naryad->save();
                                $quantity--;
                            }
                        } else {
                            @$position++;
                            $value['quantity']=1;
                            $naryad=new ErpNaryad;
                            $naryad->barcode='B'.$this->id.'D'.$position;
                            $naryad->order_id=$this->id;
                            $naryad->article=$value['article'];
                            $naryad->items=Json::encode([$value]);
                            $naryad->status=$naryad::STATUS_FROM_SKLAD;
                            $naryad->save();
                            $fromsklad[]=$naryad->id;
                        }
                    }
                    Yii::$app->session->setFlash('ERP_FROMSKLAD_NARYADS', $fromsklad);
                    return true;
                    break;
                
                default:
                    //распределить как есть
                    foreach ($items as $key => $value) {
                        $naryad=new ErpNaryad;
                        $naryad->order_id=$this->id;
                        $naryad->article=$value['article'];
                        $naryad->items=Json::encode([$value]);
                        $naryad->save();
                    }
                    
                    return true;
                    break;
            }

        }
        return false;
    }

    public function reserve($barcode)
    {
//        try {
//
//            return $reserve = Yii::$app->cache->getOrSet('reserve-1c-'.$barcode, function () use ($barcode) {
//                $client = new \SoapClient("http://46.42.17.89/laitovo/ws/WebSync?wsdl");
//                $soap =  $client->GetBallance(array(
//                    "BarCode"=>$barcode,
//                    "Hash"=>'b$F~WBg*kR1MU$1IDswOq~WhaWMxjiR~',
//                ));
//                return $soap->return;
//            },60*15);
//        } catch (\Exception $e) {
//            return 0;
//        }
        return 0;
    }

    public function reserveNew($article)
    {

        try {
            $articles = [];
            //3. Разбиваем артикул на составляющие с цеот
            $value = explode('-', $article);

            //Артикул, для которого мы ищмем аналоги, по умолчанию попадает в массив возращаемых значений
            $articles[] = $article;

            //Если артикул начинаеться с "OT" то мы не ищем по нему аналоги и возвращается массив со значем только текущего артикула
            //В противном случае мы приступаем к поиску аналогов.
            if ($value[0] != 'OT') {

                $valueSearch = $value[0];
                $valueSearch = str_replace('FW', 'ПШ', $valueSearch);
                $valueSearch = str_replace('FV', 'ПФ', $valueSearch);
                $valueSearch = str_replace('FD', 'ПБ', $valueSearch);
                $valueSearch = str_replace('RD', 'ЗБ', $valueSearch);
                $valueSearch = str_replace('RV', 'ЗФ', $valueSearch);
                $valueSearch = str_replace('BW', 'ЗШ', $valueSearch);

                //Ищем автомобиль
                $car =  Cars::find()->where(['article' => @$value[2]])->one();
                //Находим аналоги по оконному проему
                $analogs = $car->getAnalogs()->where(['like','json','%'.$valueSearch.'%',false])->all();

                //если аналоги существуют
                if ($analogs) {

                    //обходим список аналогов
                    foreach ($analogs as $analog) {

                        //заменяем данные из текущего артикула на необходимые данные аналоги, а именно
                        //заменяем значение артикула автомобиля, зашитое в артикуле
                        $value[2] = $analog->analog->article;
                        //заменяем значение первой буквы марки автомобиля, защитое в артикуле, по правилам генерации артикула с Laitovo.ru
                        //Использую вспомогательную функцию getTranslit() для перевода русских марок в нужное написание
                        $value[1] = $analog->analog->getTranslit(mb_substr($analog->car->mark,0,1));
                        //Получаем новый артикул путем соединения всех данных воедино пи помощи функция склеивания осколков
                        $newarticle = implode('-',$value);
                        //Добавляем новый артикул в массив результирующих артикулов
                        $articles[] = $newarticle;
                    }
                }
            }

            $state = null;
            foreach ($articles as $article)
            {
                $command = Yii::$app->db->createCommand('
                SELECT a.storage_id, c.type, a.upn_id, a.literal FROM logistics_storage_state as a
                LEFT JOIN logistics_naryad as b ON a.upn_id = b.id
                LEFT JOIN logistics_storage as c ON a.storage_id = c.id
                WHERE b.article = :article and b.reserved_id is null
                order by a.created_at
                LIMIT 1;');
                $command->bindValue(':article', $article);
                $state = $command->queryOne();
                if ($state) break;
            }

            if (!$state) throw new \Exception();

            return @$state['upn_id'] . '<br>' . (@$state['type']  == Storage::TYPE_SUMP ? '<span class="text-danger">' . @$state['literal']  . ' <br>!!!ЧЕРЕЗ ОТК</span>' : '<span class="text-success">' . @$state['literal']  . '</span>');
        } catch (\Exception $e) {
            return '';
        }
    }


    public function getCategory()
    {
        $category = $this->json('category');
        return isset($category) ? $category : ''; 
    }

    public function recieveAnalogs($article=null)
    {
        $analogs = [];
        $search = null;
        if (!$article) return $analogs;

        $value = explode('-', $article);
        if ($value[0] != 'OT')
            $search = @$value[2];

        if (isset($search))
        {
            $car = Cars::find()->where(['article' => $search])->one();
            $analogs = $car ? $car->analogs : [];
        }

        return $analogs;
    }
    
    static function prioritets()
    {
        return [
            7=>'Низкий',
            6=>'Обычный',
            //5=>'Комплектация',
            4=>'Срочный',
            3=>'Большой заказ',
            //2=>'TopSpeed',
            //1=>'Срочный-Переделка',
        ];
    }

    static function prioritetName($sort)
    {
        if ($sort == 2) return 'TopSpeed';
        if ($sort == 3) return 'Большой заказ';
        if ($sort == 4) return 'Срочный';
        if ($sort == 6) return 'Обычный';
        if ($sort == 7) return 'Низкий';
        return 'Нет приоритета';
    }

    /**
     * Общее количество заказов
     *
     * @return int
     */
    public static function countAll()
    {
        return self::find()->count();
    }

    /**
     * Количество заказов на производстве
     *
     * @return int
     */
    public static function countOnProduction()
    {
        return self::find()->where(['or',
            ['in', 'status', [self::STATUS_READY_TO_WORK, self::STATUS_IN_WORK]],
            ['status' => null]
        ])->count();
    }
}
