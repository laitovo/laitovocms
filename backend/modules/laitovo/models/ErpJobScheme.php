<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use common\models\user\User;
use common\models\laitovo\ProductionScheme;

/**
 * This is the model class for table "{{%laitovo_erp_job_scheme}}".
 *
 * @property integer $id
 * @property integer $location_id
 * @property integer $type_id
 * @property integer $production_scheme_id
 * @property integer $job_count
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property ErpLocation $location
 * @property ErpScheme $scheme
 * @property ErpJobType $type
 */
class ErpJobScheme extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_job_scheme}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id', 'type_id', 'production_scheme_id', 'job_count', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['rule'], 'string', 'max' => 255],
            [['location_id'], 'exist', 'skipOnEmpty' => false, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['production_scheme_id'], 'exist', 'skipOnEmpty' => false, 'targetClass' => ProductionScheme::className(), 'targetAttribute' => ['production_scheme_id' => 'id']],
            [['type_id'], 'exist', 'skipOnEmpty' => false, 'targetClass' => ErpJobType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'location_id' => Yii::t('app', 'Участок'),
            'type_id' => Yii::t('app', 'Вид работ'),
            'production_scheme_id' => Yii::t('app', 'Производственный цикл'),
            'rule' => Yii::t('app', 'Правило'),
            'job_count' => Yii::t('app', 'Количество работ'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
        ];
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionScheme()
    {
        return $this->hasOne(ProductionScheme::className(), ['id' => 'production_scheme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ErpJobType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }
}
