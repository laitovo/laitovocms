<?php

namespace backend\modules\laitovo\models;

use Yii;

/**
 * This is the model class for table "laitovo_erp_location_workplace_register".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $workplace_id
 * @property integer $register_at
 * @property string $action_type
 *
 * @property LaitovoErpUser $user
 * @property LaitovoErpLocationWorkplace $workplace
 */
class ErpLocationWorkplaceRegister extends \common\models\laitovo\ErpLocationWorkplaceRegister
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'workplace_id', 'register_at'], 'integer'],
            [['action_type'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['workplace_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocationWorkplace::className(), 'targetAttribute' => ['workplace_id' => 'id']],
        ];
    }
}
