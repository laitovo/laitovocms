<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 13.09.17
 * Time: 15:41
 */

namespace backend\modules\laitovo\models;

use common\models\laitovo\ErpReportCard;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use common\models\laitovo\ErpUserPosition;
use common\models\laitovo\Weekend;
use common\models\laitovo\Config;
use Yii;

class ErpReportCartTable extends Model
{
    public $dateTo;
    public $dateFrom;

    private $_productionWorkplace = null;
    private $_monthRate = null;
    private $_config = null;

    public function dateFromTime(){
        return strtotime($this->dateFrom);
    }

    public function dateToTime(){
        return strtotime($this->dateTo);
    }

    public function rule()
    {
        return  [
            [['dateTo', 'dateFrom'], 'date']
        ];
    }


    public function attributeLabels()
    {
        return [
            'dateFrom'=>'Начало периода',
            'dateTo'=>'Конец периода'
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getUsersInPosition()
    {
        $user_position  = ArrayHelper::map(ErpUserPosition::find()->all(),'user_id','user_id');
        return ErpUser::find()->where(['in', 'id', $user_position])->all();
    }


    public function getDateAction($date ,$user_id)
    {
        return new DateAction($date, $user_id);
    }


    /**
     * @return array
     */
    public function getDateList()
    {
        return DateAction::datePeriodListArray($this->dateFrom, $this->dateTo);
    }

    /**
     *
     */
    public function allUsersResultArray()
    {
        $resultArr = [];
        foreach ($this->getUsersInPosition() as $user){
            $resultArr[$user->id] = $this->resultArray($user);
        }
        return $resultArr;
    }


    public function getArrayDataProvider()
    {
        return new ArrayDataProvider([
                'allModels' => $this->allUsersResultArray(),
                'sort' => [
                     'attributes' => ['username'],
                     'defaultOrder'=>['username' => SORT_ASC],
                ],
                'pagination' => [
                'pageSize' => count($this->allUsersResultArray()),
      ],
        ]);
    }

    /**
     * @return array
     */
    public function getProductionWorkplace()
    {
        if ($this->_productionWorkplace === null) {
            $workplace = ErpLocationWorkplace::find()->joinWith('location l')->where(['l.type'=>ErpLocation::TYPE_PRODUCTION])->all();
            $this->_productionWorkplace =  ArrayHelper::map($workplace, 'id', 'id');
        }
        return $this->_productionWorkplace;
    }

    /**
     * @param $date
     * время активное на производстве
     */
    public function getActiveInProduction($date, $user_id)
    {
        $time = 0;
        $maxDate = 0;//наибольшая метка времени

        //ищем все производственные рабочие места локации на которые работник встал в течение дня
        $productionActives = ErpLocationWorkplaceRegister::find()
            ->where(['and',
                ['user_id'=>$user_id],
                ['in', 'workplace_id', $this->getProductionWorkplace()],
                ['action_type'=>ErpLocationWorkplaceRegister::TYPE_ACTIVE],
                ['>','register_at', strtotime($date)],
                ['<','register_at', strtotime($date)+(60*60*15)]
            ])
            ->orderBy('id ASC')
            ->all();

        if($productionActives)
        {
            foreach ($productionActives as $productionActive)
            {

                //ищем ближайшую по дате запись о том что сотрудник покнул рабочее место или стал неактивным на рабочем месте

                if(isset($productionActive) && $productionActive->workplace_id)
                {
                    $productionInactives = ErpLocationWorkplaceRegister::find()
                        ->where(['and',
                            ['user_id'=>$user_id],
                            [ 'workplace_id'=>$productionActive->workplace_id],
                            ['action_type'=>ErpLocationWorkplaceRegister::TYPE_INACTIVE],
                            ['>','register_at', $maxDate==0 ? strtotime($date) : $maxDate],
                            ['<','register_at', strtotime($date)+(60*60*20)]
                        ])
                        ->orderBy('id asc')
                        ->one();

                    if(isset($productionInactives))
                    {
                        if($productionActive->register_at && $productionInactives->register_at && $productionActive->register_at>$maxDate)
                        {
                            $maxDate = $productionInactives->register_at;
                            $time+= $maxDate-$productionActive->register_at;
                        }
                        if($productionActive->register_at && $productionInactives->register_at && $productionActive->register_at<$maxDate && $productionInactives->register_at>$maxDate)
                        {
                            $time+= $productionInactives->register_at - $maxDate;
                            $maxDate = $productionInactives->register_at;
                        }
                    }
                }
            }
        }
        return $time;
    }

    /**
     * @param $time
     * @return string
     */
    public function getHourMinute($time)
    {
        return DateAction::hour_minute($time);
    }

    /**
     * @param $user_id
     * @return array
     */
    public function resultArray($user)
    {
        $user_id = $user->id;
        $date_list_document = $this->getDocumentDateArray($user_id);

        $resultArr = [];
        $sum_time_in_support = 0;
        $sum_time_in_production = 0;

        if($this->getDateList() && count($this->getDateList())>0)
        {
            foreach ($this->getDateList() as $date)
            {
                $key_date = 0;//флаг обозначающий что это число занято документом
                if(count($date_list_document)){
                    foreach ($date_list_document as $key=>$value){
                        if(in_array($date,$value)){
                            $key_date=$key;
                            break;
                        }
                    }
                }
                if($key_date){
                    $resultArr['in_production'][$date]=$key_date;
                    $resultArr['final_difference'][$date]=$key_date;
                    if((string)$key_date===ErpDocumentCause::BISINESS_TRIP){
                        $sum_time_in_support +=8*3600;
                    }
                }else{
                    $reportIn = ErpLocationWorkplaceRegister::find()->where(['and',
                        ['user_id'=>$user_id],
                        ['action_type'=>ErpLocationWorkplaceRegister::TYPE_IN],
                        ['>','register_at', strtotime($date)],
                        ['<','register_at', strtotime($date)+(60*60*20)]
                    ])->orderBy('id DESC')
                        ->one();

                    $reportOut = ErpLocationWorkplaceRegister::find()->where(['and',
                        ['user_id'=>$user_id],
                        ['action_type'=>ErpLocationWorkplaceRegister::TYPE_OUT],
                        ['>','register_at', strtotime($date)],
                        ['<','register_at', strtotime($date)+(60*60*20)]
                    ])->orderBy('id DESC')
                        ->one();

                    $sum_time_in_production += $this->getActiveInProduction($date, $user_id);

                    if(isset($reportOut) && isset($reportIn) && $reportOut->register_at && $reportIn->register_at){
                        $sum_time_in_support +=  ($reportOut->register_at - $reportIn->register_at - 3600) - $this->getActiveInProduction($date, $user_id) >0 ? ($reportOut->register_at - $reportIn->register_at - 3600) - $this->getActiveInProduction($date, $user_id) :0;
                    }
                }
            }

            $resultArr['username'] = $user->name;
            $resultArr['sum_hour_in_support'] = $sum_time_in_support>0 ? $this->getHourMinute($sum_time_in_support) : 0;
            $resultArr['sum_day_in_support'] = $sum_time_in_support>0 ? $this->getDay($sum_time_in_support) : 0;
            $resultArr['sum_price_in_support'] = $this->getFactPriceSum($sum_time_in_support, $this->getSumPriceInSupport($user_id)).'('. $this->getSumPriceInSupport($user_id).')';
            $resultArr['sum_hour_in_production'] = $sum_time_in_production>0 ? $this->getHourMinute($sum_time_in_production) : 0;
            $resultArr['sum_day_in_production'] = $sum_time_in_production>0 ? $this->getDay($sum_time_in_production) : 0;
            $resultArr['sum_price_in_production'] = $this->getPriceNaryad($user_id);

            $resultArr['total_hour'] = $this->getHourMinute($sum_time_in_support+$sum_time_in_production);
            $resultArr['total_day'] = $this->getDay(($sum_time_in_support+$sum_time_in_production));
            $resultArr['total_sum'] = $this->getFactPriceSum($sum_time_in_support, $this->getSumPriceInSupport($user_id))+$this->getPriceNaryad($user_id) . '(' . $this->getSumPriceInSupport($user_id) . ')';
        }
        return $resultArr;
    }

    /**
     * @return array
     */
    public function getDocumentDateArray($user_id){
        $resultArr=[];
        //список документов которые подходят по дате
        $document_list = ErpDocumentCause::find()->where(['and',
                ['user_id'=>$user_id],
                ['or',
                    ['and',
                        ['>=','date_start',$this->dateFrom],
                        ['<=','date_start',$this->dateTo],
                    ],
                    ['and',
                        ['>=','date_finish',$this->dateFrom],
                        ['<=','date_finish', $this->dateTo],
                    ]
                ]
            ]
        )->all();

        if(count($document_list)){
            foreach ($document_list as $document){
                if(isset( $resultArr[$document->type])){
                    $resultArr[$document->type]=ArrayHelper::merge($resultArr[$document->type],DateAction::datePeriodListArray($document->date_start,$document->date_finish));
                }else{
                    $resultArr[$document->type]=DateAction::datePeriodListArray($document->date_start,$document->date_finish);
                }
            }
        }
        return $resultArr;
    }


    public function monthRate()
    {
        if ($this->_monthRate === null) {
            $result = 0;
            $date_action =  new DateAction($this->dateFrom);
            $date_array =  $date_action->getDateArray();
            $date_weekend = ArrayHelper::map(Weekend::find()->all(),'id','weekend');
            foreach ($date_array as $date)
            {
                if(!in_array($date,$date_weekend))
                    $result +=8;
            }
            $this->_monthRate = $result;
        }

        return $this->_monthRate;
    }


    public function getDay($time)
    {
        return round($time/(3600*8),2);
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getSumPriceInSupport($user_id)
    {
        $query = Yii::$app->db->createCommand('
            SELECT (IFNULL(salary_one,0)+IFNULL(salary_two,0)+IFNULL(car_compensation,0)+IFNULL(surcharge,0)
            +IFNULL(qualification,0)+IFNULL(premium,0)+IFNULL(kpi_one,0)+IFNULL(kpi_two,0)+IFNULL(kpi_three,0)) as sum
            FROM laitovo_erp_position as p
            
            left join laitovo_erp_user_position as up   on up.position_id = p.id
            
            where up.user_id = "'. $user_id .'"')
            ->queryOne();
        return isset($query['sum']) ? $query['sum'] : 0;
    }


    public function getFactPriceSum($time, $sum_price)
    {
        return round(($sum_price/$this->monthRate()) * ($time/(3600)) ,2);
    }


    public function getPriceNaryad($user_id)
    {
        if ($this->_config === null) {
            $this->_config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
        }
        $config = $this->_config;

        //             средняя дневная норма                цена нормированного коэффициента
        if($config && $config->json('averageRate') && $config->json('hourrate')) {
            $reports = (new \yii\db\Query())
                ->select([
                    'SUM("' . $config->json('averageRate') . '"/report.job_rate*report.job_count*"' . $config->json('hourrate') . '") as naryad_sum'
                ])
                ->from('laitovo_erp_users_report as report')
                ->where([
                    'and',
                    ['>', 'report.created_at', $this->dateFromTime()],
                    ['<', 'report.created_at', $this->dateToTime()],
                    ['report.user_id'=>$user_id]
                ])
                ->groupBy(['user_id'])
                ->one();
        }
        $reworks = (new \yii\db\Query())
            ->select([
                'SUM(rework.deduction) as deduction'])
            ->from('laitovo_rework_act as rework')
            ->where(['and',
                ['>', 'rework.created_at', $this->dateFromTime()],
                ['<', 'rework.created_at', $this->dateToTime()],
                ['made_user_id'=>$user_id]
            ])
            ->groupBy(['made_user_id'])
            ->one();

        $total = (isset($reports['naryad_sum']) ? $reports['naryad_sum'] : 0) - (isset($reworks['deduction']) ? $reworks['deduction'] : 0 );
        return round($total,2);
    }


}
