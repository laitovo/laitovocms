<?php
namespace backend\modules\laitovo\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%laitovo_erp_location}}".
 */
class ErpLocationQueue extends \common\models\ErpLocationQueue
{
	private $naryad;
	private $prioritet;

    public function rules()
    {
        return [
            [['location_id'], 'integer'],
            [['location_id'], 'unique'],
            [['location_id'], 'required'],
            [['json'], 'string'],
        ]
    }

    public function setNaryad($value)
    {

    }

    public function setPrioritet($value)
    {

    }
}
