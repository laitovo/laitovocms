<?php

namespace backend\modules\laitovo\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "laitovo_erp_location_workplace_type".
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property User $author
 * @property User $updater
 */
class ErpLocationWorkplaceType extends \common\models\laitovo\ErpLocationWorkplaceType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'author_id', 'updater_id','location_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }
}
