<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\laitovo\Cars;
use common\models\laitovo\CarClips;
use common\models\laitovo\CarsAnalog;
use common\models\laitovo\ClipsScheme;
use common\models\laitovo\CarsAct;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * Cars
 */
class CarsCheckUpdateForm extends Model
{
    public $elem;
    //step1
    public $article;
    public $mark;
    public $mark_en;
    public $model;
    public $model_en;
    public $generation;
    public $carcass;
    public $doors;
    public $firstyear;
    public $lastyear;
    public $modification;
    public $modification_en;
    public $country;
    public $country_en;
    public $category;
    public $mastered;
    public $checked;

    public $clips='[]';
    public $analogs='[]';

    public $fw1;
    public $fv1;
    public $fd1;
    public $rd1;
    public $rv1;
    public $bw1;

    //step2
    public $fw_status;
    public $fw_date;
    public $fw_scheme;

    //step3
    public $fv_date;
    public $fv_dlina;
    public $fv_visota;
    public $fv_magnit;
    public $fv_laitovo_standart_status;
    public $fv_laitovo_dontlookb_status;
    public $fv_chiko_standart_status;
    public $fv_chiko_standart_natyag;
    public $fv_chiko_dontlookb_status;

    public $fv_laitovo_standart_scheme;
    public $fv_laitovo_standart_schemem;
    public $fv_chiko_standart_scheme;
    public $fv_chiko_standart_schemem;
    private $_fv_laitovo_standart_scheme=null;
    private $_fv_laitovo_standart_schemem=null;
    private $_fv_chiko_standart_scheme=null;
    private $_fv_chiko_standart_schemem=null;
   
    //step4
    public $fd_date;
    public $fd_dlina;
    public $fd_visota;
    public $fd_magnit;
    public $fd_obshivka;
    public $fd_hlyastik;
    public $fd_laitovo_standart_status;
    public $fd_laitovo_short_status;
    public $fd_laitovo_smoke_status;
    public $fd_laitovo_mirror_status;
    public $fd_laitovo_mirror_maxdlina;
    public $fd_laitovo_mirror_maxvisota;
    public $fd_laitovo_dontlooks_status;
    public $fd_laitovo_dontlookb_status;
    public $fd_chiko_standart_status;
    public $fd_chiko_standart_natyag;
    public $fd_chiko_short_status;
    public $fd_chiko_short_natyag;
    public $fd_chiko_smoke_status;
    public $fd_chiko_smoke_natyag;
    public $fd_chiko_mirror_status;
    public $fd_chiko_mirror_maxdlina;
    public $fd_chiko_mirror_maxvisota;
    public $fd_chiko_mirror_natyag;
    public $fd_chiko_dontlooks_status;
    public $fd_chiko_dontlookb_status;
    public $fd_moscitka_standart_status;
    public $fd_chikomagnet_status;
    public $fd_laitovowithmagnets_standart_status;
    public $fd_chikomagnet_magnitov;

    public $fd_laitovo_standart_scheme;
    public $fd_laitovo_standart_schemem;
    public $fd_laitovo_standart_schemet;
    public $fd_chiko_standart_scheme;
    public $fd_chiko_standart_schemem;
    public $fd_chiko_standart_schemet;
    private $_fd_laitovo_standart_scheme=null;
    private $_fd_laitovo_standart_schemem=null;
    private $_fd_laitovo_standart_schemet=null;
    private $_fd_chiko_standart_scheme=null;
    private $_fd_chiko_standart_schemem=null;
    private $_fd_chiko_standart_schemet=null;

    public $fd_laitovo_short_scheme;
    public $fd_laitovo_short_schemem;
    public $fd_laitovo_short_schemet;
    public $fd_chiko_short_scheme;
    public $fd_chiko_short_schemem;
    public $fd_chiko_short_schemet;
    private $_fd_laitovo_short_scheme=null;
    private $_fd_laitovo_short_schemem=null;
    private $_fd_laitovo_short_schemet=null;
    private $_fd_chiko_short_scheme=null;
    private $_fd_chiko_short_schemem=null;
    private $_fd_chiko_short_schemet=null;

    public $fd_laitovo_smoke_scheme;
    public $fd_laitovo_smoke_schemem;
    public $fd_laitovo_smoke_schemet;
    public $fd_chiko_smoke_scheme;
    public $fd_chiko_smoke_schemem;
    public $fd_chiko_smoke_schemet;
    private $_fd_laitovo_smoke_scheme=null;
    private $_fd_laitovo_smoke_schemem=null;
    private $_fd_laitovo_smoke_schemet=null;
    private $_fd_chiko_smoke_scheme=null;
    private $_fd_chiko_smoke_schemem=null;
    private $_fd_chiko_smoke_schemet=null;

    public $fd_laitovo_mirror_scheme;
    public $fd_laitovo_mirror_schemem;
    public $fd_laitovo_mirror_schemet;
    public $fd_chiko_mirror_scheme;
    public $fd_chiko_mirror_schemem;
    public $fd_chiko_mirror_schemet;
    private $_fd_laitovo_mirror_scheme=null;
    private $_fd_laitovo_mirror_schemem=null;
    private $_fd_laitovo_mirror_schemet=null;
    private $_fd_chiko_mirror_scheme=null;
    private $_fd_chiko_mirror_schemem=null;
    private $_fd_chiko_mirror_schemet=null;

    public $fd_install_direction;
    public $fd_with_recess;
    public $fd_use_tape;

    //step5
    public $rd_date;
    public $rd_dlina;
    public $rd_visota;
    public $rd_magnit;
    public $rd_obshivka;
    public $rd_hlyastik;
    public $rd_laitovo_standart_status;
    public $rd_laitovo_dontlooks_status;
    public $rd_laitovo_dontlookb_status;
    public $rd_chiko_standart_status;
    public $rd_chiko_standart_natyag;
    public $rd_chiko_dontlooks_status;
    public $rd_chiko_dontlookb_status;
    public $rd_chikomagnet_status;
    public $rd_moscitka_standart_status;
    public $rd_laitovowithmagnets_standart_status;
    public $rd_chikomagnet_magnitov;

    public $rd_laitovo_standart_scheme;
    public $rd_laitovo_standart_schemem;
    public $rd_laitovo_standart_schemet;
    public $rd_chiko_standart_scheme;
    public $rd_chiko_standart_schemem;
    public $rd_chiko_standart_schemet;
    private $_rd_laitovo_standart_scheme=null;
    private $_rd_laitovo_standart_schemem=null;
    private $_rd_laitovo_standart_schemet=null;
    private $_rd_chiko_standart_scheme=null;
    private $_rd_chiko_standart_schemem=null;
    private $_rd_chiko_standart_schemet=null;

    public $rd_install_direction;
    public $rd_with_recess;
    public $rd_use_tape;

    //step6
    public $rv_date;
    public $rv_dlina;
    public $rv_visota;
    public $rv_magnit;
    public $rv_obshivka;
    public $rv_openwindow;
    public $rv_openwindowtrue;
    public $rv_hlyastik;
    public $rv_laitovo_standart_status;
    public $rv_laitovo_standart_forma;
    public $rv_laitovo_dontlooks_status;
    public $rv_laitovo_dontlookb_status;
    public $rv_chiko_standart_status;
    public $rv_chiko_standart_forma;
    public $rv_chiko_standart_natyag;
    public $rv_chiko_dontlookb_status;
    public $rv_chiko_dontlooks_status;

    public $rv_laitovo_standart_scheme;
    public $rv_laitovo_standart_schemem;
    public $rv_chiko_standart_scheme;
    public $rv_chiko_standart_schemem;
    private $_rv_laitovo_standart_scheme=null;
    private $_rv_laitovo_standart_schemem=null;
    private $_rv_chiko_standart_scheme=null;
    private $_rv_chiko_standart_schemem=null;

    public $rv_laitovo_triangul_scheme;
    public $rv_laitovo_triangul_schemem;
    public $rv_chiko_triangul_scheme;
    public $rv_chiko_triangul_schemem;
    private $_rv_laitovo_triangul_scheme=null;
    private $_rv_laitovo_triangul_schemem=null;
    private $_rv_chiko_triangul_scheme=null;
    private $_rv_chiko_triangul_schemem=null;

    //step7
    public $bw_date;
    public $bw_dlina;
    public $bw_visota;
    public $bw_magnit;
    public $bw_obshivka;
    public $bw_openwindow;
    public $bw_openwindowtrue;
    public $bw_chastei;
    public $bw_hlyastik;
    public $bw_simmetr;
    public $bw_gabarit;
    public $bw_laitovo_standart_status;
    public $bw_laitovo_dontlooks_status;
    public $bw_chiko_standart_status;
    public $bw_chiko_standart_natyag;
    public $bw_chiko_dontlookb_status;

    public $bw_laitovo_standart_scheme;
    public $bw_laitovo_standart_schemem;
    public $bw_chiko_standart_scheme;
    public $bw_chiko_standart_schemem;
    private $_bw_laitovo_standart_scheme=null;
    private $_bw_laitovo_standart_schemem=null;
    private $_bw_chiko_standart_scheme=null;
    private $_bw_chiko_standart_schemem=null;

    public $bw_laitovo_double_scheme;
    public $bw_laitovo_double_schemem;
    public $bw_chiko_double_scheme;
    public $bw_chiko_double_schemem;    
    private $_bw_laitovo_double_scheme=null;
    private $_bw_laitovo_double_schemem=null;
    private $_bw_chiko_double_scheme=null;
    private $_bw_chiko_double_schemem=null;

    //step8
    public $fields=[];

    public $auto;
    private $act;
    public $_json=[];
    private $_clipses=[];

    private $act_before;

    const SCENARIO_STEP_CREATE = 'create';
    public $SCENARIO_STEP_FINISH;

    public $_magnit=[''=>'','Да'=>'Да','Нет'=>'Нет'];
    public $_openwindow=[''=>'','Да'=>'Да','Нет'=>'Нет'];
    public $_openwindowtrue=[''=>'','Да'=>'Да','Нет'=>'Нет'];
    public $_obshivka=[
        ''=>'',
        'Комбинированная'=>'Комбинированная',
        'Металл'=>'Металл',
        'Накладная'=>'Накладная',
        'Пластик'=>'Пластик',
        'Резина'=>'Резина',
    ];
    public $_natyag=[
        ''=>'',
        'NORM'=>'NORM',
        'MIN'=>'MIN',
    ];
    public $_formafv=[
        ''=>'',
        'Квадратная'=>'Квадратная',
        'Треугольная'=>'Треугольная',
    ];
    public $_magnitov=[
        ''=>'',
        'Стандарт1'=>'Стандарт 1',
        'Стандарт2'=>'Стандарт 2',
    ];
    public $_carcass=[
        ''=>'',
        'Внедорожник'=>'Внедорожник',
        'Грузовик'=>'Грузовик',
        'Кабриолет'=>'Кабриолет',
        'Компактвэн'=>'Компактвэн',
        'Кроссовер'=>'Кроссовер',
        'Купе'=>'Купе',
        'Лифтбэк'=>'Лифтбэк',
        'Микроавтобус'=>'Микроавтобус',
        'Минивэн'=>'Минивэн',
        'Пикап'=>'Пикап',
        'Седан'=>'Седан',
        'Универсал'=>'Универсал',
        'Фургон'=>'Фургон',
        'Хетчбэк'=>'Хетчбэк',
        'Фастбэк'=>'Фастбэк',
    ];
    public $_category=[
        ''=>'',
        'A'=>'A',
        'B'=>'B',
        'C'=>'C',
        'D'=>'D',
    ];
    public $_install_direction=[
        ''=>'',
        'Изнутри'=>'Изнутри',
        'Снаружи'=>'Снаружи',
    ];
    public $_with_recess=[''=>'','Да'=>'Да','Нет'=>'Нет'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mark','mark_en','model','model_en','carcass','doors','firstyear','category'], 'required','on'=>'create'],
            [['mark','carcass','doors','firstyear','category'], 'required','on'=>'step1'],

            [['fw_status'], 'boolean','on'=>'step2'],
            [['fw_date'], 'string', 'max' => 255,'on'=>'step2'],
            [['fw_scheme'], 'string', 'max' => 255,'on'=>'step21'],
            
            [['fv_date','fv_dlina','fv_visota','fv_magnit'], 'string', 'max' => 255,'on'=>'step3'],
            [['fv_dlina','fv_visota'], 'required','on'=>'step3', 'when' => function($model) {
                return $model->fv_date;
            }],
            [['fv_laitovo_standart_scheme','fv_laitovo_standart_schemem','fv_chiko_standart_scheme','fv_chiko_standart_schemem'], 'string', 'on'=>'step31'],

            [['fd_date','fd_dlina','fd_visota','fd_magnit','fd_obshivka','fd_hlyastik'], 'string', 'max' => 255,'on'=>'step4'],
            [['fd_dlina','fd_visota'], 'required','on'=>'step4', 'when' => function($model) {
                return $model->fd_date;
            }],
            [['fd_laitovo_standart_scheme','fd_laitovo_standart_schemem','fd_laitovo_standart_schemet','fd_chiko_standart_scheme','fd_chiko_standart_schemem','fd_chiko_standart_schemet'], 'string', 'on'=>'step41'],
            [['fd_laitovo_short_scheme','fd_laitovo_short_schemem','fd_laitovo_short_schemet','fd_chiko_short_scheme','fd_chiko_short_schemem','fd_chiko_short_schemet'], 'string', 'on'=>'step42'],
            [['fd_laitovo_smoke_scheme','fd_laitovo_smoke_schemem','fd_laitovo_smoke_schemet','fd_chiko_smoke_scheme','fd_chiko_smoke_schemem','fd_chiko_smoke_schemet'], 'string', 'on'=>'step43'],
            [['fd_laitovo_mirror_scheme','fd_laitovo_mirror_schemem','fd_laitovo_mirror_schemet','fd_chiko_mirror_scheme','fd_chiko_mirror_schemem','fd_chiko_mirror_schemet'], 'string', 'on'=>'step44'],

            [['rd_date','rd_dlina','rd_visota','rd_magnit','rd_obshivka','rd_hlyastik'], 'string', 'max' => 255,'on'=>'step5'],
            [['rd_dlina','rd_visota'], 'required','on'=>'step5', 'when' => function($model) {
                return $model->rd_date;
            }],
            [['rd_laitovo_standart_scheme','rd_laitovo_standart_schemem','rd_laitovo_standart_schemet','rd_chiko_standart_scheme','rd_chiko_standart_schemem','rd_chiko_standart_schemet'], 'string', 'on'=>'step51'],

            [['rv_date','rv_dlina','rv_visota','rv_magnit','rv_obshivka','rv_openwindow','rv_openwindowtrue','rv_hlyastik'], 'string', 'max' => 255,'on'=>'step6'],
            [['rv_dlina','rv_visota'], 'required','on'=>'step6', 'when' => function($model) {
                return $model->rv_date;
            }],
            [['rv_laitovo_standart_scheme','rv_laitovo_standart_schemem','rv_chiko_standart_scheme','rv_chiko_standart_schemem'], 'string', 'on'=>'step61'],
            [['rv_laitovo_triangul_scheme','rv_laitovo_triangul_schemem','rv_chiko_triangul_scheme','rv_chiko_triangul_schemem'], 'string', 'on'=>'step62'],

            [['bw_date','bw_dlina','bw_visota','bw_magnit','bw_obshivka','bw_openwindow','bw_openwindowtrue','bw_chastei','bw_hlyastik'], 'string', 'max' => 255,'on'=>'step7'],
            [['bw_dlina','bw_visota'], 'required','on'=>'step7', 'when' => function($model) {
                return $model->bw_date;
            }],
            [['bw_laitovo_standart_scheme','bw_laitovo_standart_schemem','bw_chiko_standart_scheme','bw_chiko_standart_schemem'], 'string', 'on'=>'step71'],
            [['bw_laitovo_double_scheme','bw_laitovo_double_schemem','bw_chiko_double_scheme','bw_chiko_double_schemem'], 'string', 'on'=>'step72'],

            [['fields'], 'required','on'=>'step8'],

            [['clips'], 'string', 'on'=>'step9'],

            [['analogs'], 'string', 'on'=>'step10'],

            [['mark','mark_en','model','model_en','carcass','modification','modification_en','country','country_en','category'], 'string', 'max' => 255],
            [['mark','mark_en','model','model_en','generation','doors','firstyear','lastyear','modification','modification_en','country','country_en'], 'trim'],
            [['generation','doors','firstyear','lastyear'], 'number'],

            [['fw_status'], 'boolean'],
            [['fw_date'], 'string', 'max' => 255],
            [['fw_scheme'], 'string', 'max' => 255],

            [['fv_date','fv_dlina','fv_visota','fv_magnit','fv_chiko_standart_natyag'], 'string', 'max' => 255],
            [['fv_laitovo_standart_status','fv_laitovo_dontlookb_status','fv_chiko_standart_status','fv_chiko_dontlookb_status'], 'boolean'],

            [['fd_date','fd_dlina','fd_visota','fd_magnit','fd_obshivka','fd_hlyastik','fd_laitovo_mirror_maxdlina','fd_laitovo_mirror_maxvisota','fd_chiko_standart_natyag','fd_chiko_short_natyag','fd_chiko_smoke_natyag','fd_chiko_mirror_maxdlina','fd_chiko_mirror_maxvisota','fd_chiko_mirror_natyag','fd_chikomagnet_magnitov'], 'string', 'max' => 255],
            [['fd_laitovo_standart_status','fd_laitovo_short_status','fd_laitovo_smoke_status','fd_laitovo_mirror_status','fd_laitovo_dontlooks_status','fd_laitovo_dontlookb_status','fd_chiko_standart_status','fd_chiko_short_status','fd_chiko_smoke_status','fd_chiko_mirror_status','fd_chiko_dontlooks_status','fd_chiko_dontlookb_status','fd_chikomagnet_status','fd_laitovowithmagnets_standart_status','fd_moscitka_standart_status'], 'boolean'],
            [['fd_use_tape'],'boolean'],

            [['rd_date','rd_dlina','rd_visota','rd_magnit','rd_obshivka','rd_hlyastik','rd_chiko_standart_natyag','rd_chikomagnet_magnitov'], 'string', 'max' => 255],
            [['rd_laitovo_standart_status','rd_laitovo_dontlooks_status','rd_laitovo_dontlookb_status','rd_chiko_standart_status','rd_chiko_dontlooks_status','rd_chiko_dontlookb_status','rd_chikomagnet_status','rd_laitovowithmagnets_standart_status','rd_moscitka_standart_status'], 'boolean'],
            [['rd_use_tape'],'boolean'],

            [['rv_date','rv_dlina','rv_visota','rv_magnit','rv_obshivka','rv_openwindow','rv_openwindowtrue','rv_hlyastik','rv_laitovo_standart_forma','rv_chiko_standart_forma','rv_chiko_standart_natyag'], 'string', 'max' => 255],
            [['rv_laitovo_standart_status','rv_laitovo_dontlooks_status','rv_laitovo_dontlookb_status','rv_chiko_standart_status','rv_chiko_dontlookb_status','rv_chiko_dontlooks_status'], 'boolean'],

            [['bw_date','bw_dlina','bw_visota','bw_magnit','bw_obshivka','bw_openwindow','bw_openwindowtrue','bw_chastei','bw_hlyastik','bw_chiko_standart_natyag'], 'string', 'max' => 255],
            [['bw_simmetr'], 'boolean'],
            [['bw_gabarit'], 'boolean'],
            [['bw_laitovo_standart_status','bw_laitovo_dontlooks_status','bw_chiko_standart_status','bw_chiko_dontlookb_status'], 'boolean'],

            [['fields'], 'each','rule' => ['string']],
            [['clips','analogs'], 'string'],
            [['clips','analogs'], 'default', 'value' => '[]'],

            [['fv_laitovo_standart_scheme','fv_laitovo_standart_schemem','fv_chiko_standart_scheme','fv_chiko_standart_schemem'], 'string'],
            [['fd_laitovo_standart_scheme','fd_laitovo_standart_schemem','fd_laitovo_standart_schemet','fd_chiko_standart_scheme','fd_chiko_standart_schemem','fd_chiko_standart_schemet'], 'string'],
            [['fd_laitovo_short_scheme','fd_laitovo_short_schemem','fd_laitovo_short_schemet','fd_chiko_short_scheme','fd_chiko_short_schemem','fd_chiko_short_schemet'], 'string'],
            [['fd_laitovo_smoke_scheme','fd_laitovo_smoke_schemem','fd_laitovo_smoke_schemet','fd_chiko_smoke_scheme','fd_chiko_smoke_schemem','fd_chiko_smoke_schemet'], 'string'],
            [['fd_laitovo_mirror_scheme','fd_laitovo_mirror_schemem','fd_laitovo_mirror_schemet','fd_chiko_mirror_scheme','fd_chiko_mirror_schemem','fd_chiko_mirror_schemet'], 'string'],
            [['rd_laitovo_standart_scheme','rd_laitovo_standart_schemem','rd_laitovo_standart_schemet','rd_chiko_standart_scheme','rd_chiko_standart_schemem','rd_chiko_standart_schemet'], 'string'],
            [['rv_laitovo_standart_scheme','rv_laitovo_standart_schemem','rv_chiko_standart_scheme','rv_chiko_standart_schemem'], 'string'],
            [['rv_laitovo_triangul_scheme','rv_laitovo_triangul_schemem','rv_chiko_triangul_scheme','rv_chiko_triangul_schemem'], 'string'],
            [['bw_laitovo_standart_scheme','bw_laitovo_standart_schemem','bw_chiko_standart_scheme','bw_chiko_standart_schemem'], 'string'],
            [['bw_laitovo_double_scheme','bw_laitovo_double_schemem','bw_chiko_double_scheme','bw_chiko_double_schemem'], 'string'],
            [['fw1','fv1','fd1','rd1','rv1','bw1'], 'boolean'],

            [['fd_install_direction','rd_install_direction', 'fd_with_recess','rd_with_recess'], 'string'],
            [['fd_install_direction','fd_with_recess'], 'required','on'=>'step4'],
            [['rd_install_direction','rd_with_recess'], 'required','on'=>'step5'],
        ];
    }

    public function clips($id)
    {
        if (!isset($this->_clipses[$id])){
            $this->_clipses[$id]='';
            if ($id)
            {
                $ar = explode("/", $id);
                $type = $ar[1];
                $clips=$this->auto->getClips()->andWhere(['type'=>$type])->andWhere(['name'=>$ar[0]])->one();
            }
            if ($id && $clips)
                $this->_clipses[$id]=$clips->fullname;
        }
        return $this->_clipses[$id];
    }

    public function clipsSave($id)
    {
        if (!isset($this->_clipses[$id])){
            $this->_clipses[$id]='';
            $clips=$this->auto->getClips()->andWhere(['id'=>$id])->one();
            if ($clips)
                $this->_clipses[$id]=$clips->fullname;
        }
        return $this->_clipses[$id];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Полное наименование'),
            'article' => Yii::t('app', 'Артикул'),
            'mark' => Yii::t('app', 'Марка'),
            'mark_en' => Yii::t('app', 'Марка (EN)'),
            'model' => Yii::t('app', 'Модель'),
            'model_en' => Yii::t('app', 'Модель (EN)'),
            'generation' => Yii::t('app', 'Поколение'),
            'carcass' => Yii::t('app', 'Кузов'),
            'doors' => Yii::t('app', 'Дверей'),
            'firstyear' => Yii::t('app', 'Год начала производства'),
            'lastyear' => Yii::t('app', 'Год окончания производства'),
            'modification' => Yii::t('app', 'Модификация'),
            'modification_en' => Yii::t('app', 'Модификация (EN)'),
            'country' => Yii::t('app', 'Страна производства'),
            'country_en' => Yii::t('app', 'Страна производства (EN)'),
            'category' => Yii::t('app', 'Категория цены'),

            'mastered' => Yii::t('app', 'Освоена'),
            'checked' => Yii::t('app', 'Проверена'),

            'fw1' => Yii::t('app', 'Лобовое окно'),
            'fv1' => Yii::t('app', 'Передняя форточка'),
            'fd1' => Yii::t('app', 'Переднее боковое окно'),
            'rd1' => Yii::t('app', 'Заднее боковое окно'),
            'rv1' => Yii::t('app', 'Задняя форточка'),
            'bw1' => Yii::t('app', 'Заднее ветровое окно'),

            'clips' => Yii::t('app', 'Крепления'),
            'analogs' => Yii::t('app', 'Аналоги'),

            'fw_status' => Yii::t('app', 'Включено / Выключено'),
            'fw_date' => Yii::t('app', 'Дата освоения'),

            'fv_date' => Yii::t('app', 'Дата освоения'),
            'fv_dlina' => Yii::t('app', 'Длина'),
            'fv_visota' => Yii::t('app', 'Высота'),
            'fv_magnit' => Yii::t('app', 'Возможность установить магнитные держатели'),
            'fv_laitovo_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'fv_laitovo_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),
            'fv_chiko_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'fv_chiko_standart_natyag' => Yii::t('app', 'Натяжение'),
            'fv_chiko_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),

            'fd_date' => Yii::t('app', 'Дата освоения'),
            'fd_dlina' => Yii::t('app', 'Длина'),
            'fd_visota' => Yii::t('app', 'Высота'),
            'fd_magnit' => Yii::t('app', 'Возможность установить магнитные держатели'),
            'fd_obshivka' => Yii::t('app', 'Обшивка проема'),
            'fd_hlyastik' => Yii::t('app', 'Хлястик'),
            'fd_laitovo_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_laitovo_short_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_laitovo_smoke_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_laitovo_mirror_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_laitovo_mirror_maxdlina' => Yii::t('app', 'Максимальная длина выреза'),
            'fd_laitovo_mirror_maxvisota' => Yii::t('app', 'Максимальная высота выреза'),
            'fd_laitovo_dontlooks_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_laitovo_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_chiko_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_chiko_standart_natyag' => Yii::t('app', 'Натяжение'),
            'fd_chiko_short_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_chiko_short_natyag' => Yii::t('app', 'Натяжение'),
            'fd_chiko_smoke_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_chiko_smoke_natyag' => Yii::t('app', 'Натяжение'),
            'fd_chiko_mirror_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_chiko_mirror_maxdlina' => Yii::t('app', 'Максимальная длина выреза'),
            'fd_chiko_mirror_maxvisota' => Yii::t('app', 'Максимальная высота выреза'),
            'fd_chiko_mirror_natyag' => Yii::t('app', 'Натяжение'),
            'fd_chiko_dontlooks_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_chiko_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_chikomagnet_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_moscitka_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_laitovowithmagnets_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'fd_chikomagnet_magnitov' => Yii::t('app', 'Кол-во магнитов'),
            'fd_install_direction' => Yii::t('app', 'Установка экрана изнутри / снаружи'),
            'fd_with_recess' => Yii::t('app', 'С западением'),
            'fd_use_tape' => Yii::t('app', 'Использовать крепления на скотче'),

            'rd_date' => Yii::t('app', 'Дата освоения'),
            'rd_dlina' => Yii::t('app', 'Длина'),
            'rd_visota' => Yii::t('app', 'Высота'),
            'rd_magnit' => Yii::t('app', 'Возможность установить магнитные держатели'),
            'rd_obshivka' => Yii::t('app', 'Обшивка проема'),
            'rd_hlyastik' => Yii::t('app', 'Хлястик'),
            'rd_laitovo_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_laitovo_dontlooks_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_laitovo_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_chiko_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_chiko_standart_natyag' => Yii::t('app', 'Натяжение'),
            'rd_chiko_dontlooks_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_chiko_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_chikomagnet_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_moscitka_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_laitovowithmagnets_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'rd_chikomagnet_magnitov' => Yii::t('app', 'Кол-во магнитов'),
            'rd_install_direction' => Yii::t('app', 'Установка экрана изнутри / снаружи'),
            'rd_with_recess' => Yii::t('app', 'С западением'),
            'rd_use_tape' => Yii::t('app', 'Использовать крепления на скотче'),

            'rv_date' => Yii::t('app', 'Дата освоения'),
            'rv_dlina' => Yii::t('app', 'Длина'),
            'rv_visota' => Yii::t('app', 'Высота'),
            'rv_magnit' => Yii::t('app', 'Возможность установить магнитные держатели'),
            'rv_obshivka' => Yii::t('app', 'Обшивка проема'),
            'rv_openwindow' => Yii::t('app', 'Стекло открывается'),
            'rv_openwindowtrue' => Yii::t('app', 'Установленный экран дает возможность открыть окно'),
            'rv_hlyastik' => Yii::t('app', 'Хлястик'),
            'rv_laitovo_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'rv_laitovo_standart_forma' => Yii::t('app', 'Форма'),
            'rv_laitovo_dontlooks_status' => Yii::t('app', 'Включено / Выключено'),
            'rv_laitovo_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),
            'rv_chiko_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'rv_chiko_standart_forma' => Yii::t('app', 'Форма'),
            'rv_chiko_standart_natyag' => Yii::t('app', 'Натяжение'),
            'rv_chiko_dontlooks_status' => Yii::t('app', 'Включено / Выключено'),
            'rv_chiko_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),

            'bw_date' => Yii::t('app', 'Дата освоения'),
            'bw_dlina' => Yii::t('app', 'Длина'),
            'bw_visota' => Yii::t('app', 'Высота'),
            'bw_magnit' => Yii::t('app', 'Возможность установить магнитные держатели'),
            'bw_obshivka' => Yii::t('app', 'Обшивка проема'),
            'bw_openwindow' => Yii::t('app', 'Стекло открывается'),
            'bw_openwindowtrue' => Yii::t('app', 'Установленный экран дает возможность открыть окно'),
            'bw_chastei' => Yii::t('app', 'Кол-во частей экрана'),
            'bw_hlyastik' => Yii::t('app', 'Хлястик'),
            'bw_simmetr' => Yii::t('app', 'Экран симметричный'),
            'bw_gabarit' => Yii::t('app', 'Вмещается в габарит европы'),
            'bw_laitovo_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'bw_laitovo_dontlooks_status' => Yii::t('app', 'Включено / Выключено'),
            'bw_chiko_standart_status' => Yii::t('app', 'Включено / Выключено'),
            'bw_chiko_standart_natyag' => Yii::t('app', 'Натяжение'),
            'bw_chiko_dontlookb_status' => Yii::t('app', 'Включено / Выключено'),

        ];
    }
    //Данная форма создается только по акту
    public function __construct($act_id, $config = [])
    {
        //По акту мы должны получить автомобиль, и если такого автомобиля нет дать исключение
        $this->act = CarsAct::findOne($act_id);

        if (($this->auto=Cars::find()->where(['id'=>$this->act->car_id])->one())!==null && $this->act) {

            //крепления
            // $clips=[];
            // if ($this->auto->clips){
            //     foreach ($this->auto->clips as $row) {
            //         $clips[]=[
            //             'id'=>$row->id,
            //             'fullname'=>$row->fullname,
            //             'name'=>$row->name,
            //             'type'=>$row->type,
            //         ];
            //     }
            // }
            $this->clips=$this->act->json('after')['clips'];
            //аналоги
            // $analogs=[];
            // if ($this->auto->analogs){
            //     foreach ($this->auto->analogs as $row) {
            //         $analogs[]=[
            //             'car'=>$row->analog_id,
            //             'name'=>$row->analog->name,
            //             'elements'=>$row->json('elements'),
            //         ];
            //     }
            // }
            $this->analogs=$this->act->json('after')['analogs'];
            //пф
            $this->fv_laitovo_standart_scheme=$this->act->json('after')['fv_laitovo_standart_scheme'];

            if (($this->_fv_laitovo_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ПФ'])->one())==null){
                $this->_fv_laitovo_standart_scheme=new ClipsScheme;
                $this->_fv_laitovo_standart_scheme->car_id=$this->auto->id;
                $this->_fv_laitovo_standart_scheme->brand='Laitovo';
                $this->_fv_laitovo_standart_scheme->type='Стандарт';
                $this->_fv_laitovo_standart_scheme->type_clips='Простые';
                $this->_fv_laitovo_standart_scheme->window='ПФ';
            }

            $this->fv_laitovo_standart_schemem=$this->act->json('after')['fv_laitovo_standart_schemem'];

            if (($this->_fv_laitovo_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ПФ'])->one())==null){
                $this->_fv_laitovo_standart_schemem=new ClipsScheme;
                $this->_fv_laitovo_standart_schemem->car_id=$this->auto->id;
                $this->_fv_laitovo_standart_schemem->brand='Laitovo';
                $this->_fv_laitovo_standart_schemem->type='Стандарт';
                $this->_fv_laitovo_standart_schemem->type_clips='Магнитные';
                $this->_fv_laitovo_standart_schemem->window='ПФ';
            }

            $this->fv_chiko_standart_scheme=$this->act->json('after')['fv_chiko_standart_scheme'];

            if (($this->_fv_chiko_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ПФ'])->one())==null){
                $this->_fv_chiko_standart_scheme=new ClipsScheme;
                $this->_fv_chiko_standart_scheme->car_id=$this->auto->id;
                $this->_fv_chiko_standart_scheme->brand='Chiko';
                $this->_fv_chiko_standart_scheme->type='Стандарт';
                $this->_fv_chiko_standart_scheme->type_clips='Простые';
                $this->_fv_chiko_standart_scheme->window='ПФ';
            }

            $this->fv_chiko_standart_schemem=$this->act->json('after')['fv_chiko_standart_schemem'];

            if (($this->_fv_chiko_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ПФ'])->one())==null){
                $this->_fv_chiko_standart_schemem=new ClipsScheme;
                $this->_fv_chiko_standart_schemem->car_id=$this->auto->id;
                $this->_fv_chiko_standart_schemem->brand='Chiko';
                $this->_fv_chiko_standart_schemem->type='Стандарт';
                $this->_fv_chiko_standart_schemem->type_clips='Магнитные';
                $this->_fv_chiko_standart_schemem->window='ПФ';
            }


            //пб - standart
            $this->fd_laitovo_standart_scheme=$this->act->json('after')['fd_laitovo_standart_scheme'];

            if (($this->_fd_laitovo_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_standart_scheme=new ClipsScheme;
                $this->_fd_laitovo_standart_scheme->car_id=$this->auto->id;
                $this->_fd_laitovo_standart_scheme->brand='Laitovo';
                $this->_fd_laitovo_standart_scheme->type='Стандарт';
                $this->_fd_laitovo_standart_scheme->type_clips='Простые';
                $this->_fd_laitovo_standart_scheme->window='ПБ';
            }

            $this->fd_laitovo_standart_schemem=$this->act->json('after')['fd_laitovo_standart_schemem'];

            if (($this->_fd_laitovo_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_standart_schemem=new ClipsScheme;
                $this->_fd_laitovo_standart_schemem->car_id=$this->auto->id;
                $this->_fd_laitovo_standart_schemem->brand='Laitovo';
                $this->_fd_laitovo_standart_schemem->type='Стандарт';
                $this->_fd_laitovo_standart_schemem->type_clips='Магнитные';
                $this->_fd_laitovo_standart_schemem->window='ПБ';
            }

            $this->fd_laitovo_standart_schemet=$this->act->json('after')['fd_laitovo_standart_schemet'];

            if (($this->_fd_laitovo_standart_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Скотч','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_standart_schemet=new ClipsScheme;
                $this->_fd_laitovo_standart_schemet->car_id=$this->auto->id;
                $this->_fd_laitovo_standart_schemet->brand='Laitovo';
                $this->_fd_laitovo_standart_schemet->type='Стандарт';
                $this->_fd_laitovo_standart_schemet->type_clips='Скотч';
                $this->_fd_laitovo_standart_schemet->window='ПБ';
            }

            $this->fd_chiko_standart_scheme=$this->act->json('after')['fd_chiko_standart_scheme'];

            if (($this->_fd_chiko_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_standart_scheme=new ClipsScheme;
                $this->_fd_chiko_standart_scheme->car_id=$this->auto->id;
                $this->_fd_chiko_standart_scheme->brand='Chiko';
                $this->_fd_chiko_standart_scheme->type='Стандарт';
                $this->_fd_chiko_standart_scheme->type_clips='Простые';
                $this->_fd_chiko_standart_scheme->window='ПБ';
            }

            $this->fd_chiko_standart_schemem=$this->act->json('after')['fd_chiko_standart_schemem'];

            if (($this->_fd_chiko_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_standart_schemem=new ClipsScheme;
                $this->_fd_chiko_standart_schemem->car_id=$this->auto->id;
                $this->_fd_chiko_standart_schemem->brand='Chiko';
                $this->_fd_chiko_standart_schemem->type='Стандарт';
                $this->_fd_chiko_standart_schemem->type_clips='Магнитные';
                $this->_fd_chiko_standart_schemem->window='ПБ';
            }

            $this->fd_chiko_standart_schemet=$this->act->json('after')['fd_chiko_standart_schemet'];

            if (($this->_fd_chiko_standart_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Скотч','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_standart_schemet=new ClipsScheme;
                $this->_fd_chiko_standart_schemet->car_id=$this->auto->id;
                $this->_fd_chiko_standart_schemet->brand='Chiko';
                $this->_fd_chiko_standart_schemet->type='Стандарт';
                $this->_fd_chiko_standart_schemet->type_clips='Скотч';
                $this->_fd_chiko_standart_schemet->window='ПБ';
            }

            //пб - short
            $this->fd_laitovo_short_scheme=$this->act->json('after')['fd_laitovo_short_scheme'];

            if (($this->_fd_laitovo_short_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Укороченный','type_clips'=>'Простые','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_short_scheme=new ClipsScheme;
                $this->_fd_laitovo_short_scheme->car_id=$this->auto->id;
                $this->_fd_laitovo_short_scheme->brand='Laitovo';
                $this->_fd_laitovo_short_scheme->type='Укороченный';
                $this->_fd_laitovo_short_scheme->type_clips='Простые';
                $this->_fd_laitovo_short_scheme->window='ПБ';
            }

            $this->fd_laitovo_short_schemem=$this->act->json('after')['fd_laitovo_short_schemem'];

            if (($this->_fd_laitovo_short_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Укороченный','type_clips'=>'Магнитные','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_short_schemem=new ClipsScheme;
                $this->_fd_laitovo_short_schemem->car_id=$this->auto->id;
                $this->_fd_laitovo_short_schemem->brand='Laitovo';
                $this->_fd_laitovo_short_schemem->type='Укороченный';
                $this->_fd_laitovo_short_schemem->type_clips='Магнитные';
                $this->_fd_laitovo_short_schemem->window='ПБ';
            }

            $this->fd_laitovo_short_schemet=$this->act->json('after')['fd_laitovo_short_schemet'];

            if (($this->_fd_laitovo_short_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Укороченный','type_clips'=>'Скотч','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_short_schemet=new ClipsScheme;
                $this->_fd_laitovo_short_schemet->car_id=$this->auto->id;
                $this->_fd_laitovo_short_schemet->brand='Laitovo';
                $this->_fd_laitovo_short_schemet->type='Укороченный';
                $this->_fd_laitovo_short_schemet->type_clips='Скотч';
                $this->_fd_laitovo_short_schemet->window='ПБ';
            }

            $this->fd_chiko_short_scheme=$this->act->json('after')['fd_chiko_short_scheme'];

            if (($this->_fd_chiko_short_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Укороченный','type_clips'=>'Простые','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_short_scheme=new ClipsScheme;
                $this->_fd_chiko_short_scheme->car_id=$this->auto->id;
                $this->_fd_chiko_short_scheme->brand='Chiko';
                $this->_fd_chiko_short_scheme->type='Укороченный';
                $this->_fd_chiko_short_scheme->type_clips='Простые';
                $this->_fd_chiko_short_scheme->window='ПБ';
            }

            $this->fd_chiko_short_schemem=$this->act->json('after')['fd_chiko_short_schemem'];

            if (($this->_fd_chiko_short_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Укороченный','type_clips'=>'Магнитные','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_short_schemem=new ClipsScheme;
                $this->_fd_chiko_short_schemem->car_id=$this->auto->id;
                $this->_fd_chiko_short_schemem->brand='Chiko';
                $this->_fd_chiko_short_schemem->type='Укороченный';
                $this->_fd_chiko_short_schemem->type_clips='Магнитные';
                $this->_fd_chiko_short_schemem->window='ПБ';
            }

            $this->fd_chiko_short_schemet=$this->act->json('after')['fd_chiko_short_schemet'];

            if (($this->_fd_chiko_short_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Укороченный','type_clips'=>'Скотч','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_short_schemet=new ClipsScheme;
                $this->_fd_chiko_short_schemet->car_id=$this->auto->id;
                $this->_fd_chiko_short_schemet->brand='Chiko';
                $this->_fd_chiko_short_schemet->type='Укороченный';
                $this->_fd_chiko_short_schemet->type_clips='Скотч';
                $this->_fd_chiko_short_schemet->window='ПБ';
            }

            //пб - smoke
            $this->fd_laitovo_smoke_scheme=$this->act->json('after')['fd_laitovo_smoke_scheme'];

            if (($this->_fd_laitovo_smoke_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'ВырезДляКурящих','type_clips'=>'Простые','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_smoke_scheme=new ClipsScheme;
                $this->_fd_laitovo_smoke_scheme->car_id=$this->auto->id;
                $this->_fd_laitovo_smoke_scheme->brand='Laitovo';
                $this->_fd_laitovo_smoke_scheme->type='ВырезДляКурящих';
                $this->_fd_laitovo_smoke_scheme->type_clips='Простые';
                $this->_fd_laitovo_smoke_scheme->window='ПБ';
            }

            $this->fd_laitovo_smoke_schemem=$this->act->json('after')['fd_laitovo_smoke_schemem'];

            if (($this->_fd_laitovo_smoke_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'ВырезДляКурящих','type_clips'=>'Магнитные','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_smoke_schemem=new ClipsScheme;
                $this->_fd_laitovo_smoke_schemem->car_id=$this->auto->id;
                $this->_fd_laitovo_smoke_schemem->brand='Laitovo';
                $this->_fd_laitovo_smoke_schemem->type='ВырезДляКурящих';
                $this->_fd_laitovo_smoke_schemem->type_clips='Магнитные';
                $this->_fd_laitovo_smoke_schemem->window='ПБ';
            }

            $this->fd_laitovo_smoke_schemet=$this->act->json('after')['fd_laitovo_smoke_schemet'];

            if (($this->_fd_laitovo_smoke_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'ВырезДляКурящих','type_clips'=>'Скотч','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_smoke_schemet=new ClipsScheme;
                $this->_fd_laitovo_smoke_schemet->car_id=$this->auto->id;
                $this->_fd_laitovo_smoke_schemet->brand='Laitovo';
                $this->_fd_laitovo_smoke_schemet->type='ВырезДляКурящих';
                $this->_fd_laitovo_smoke_schemet->type_clips='Скотч';
                $this->_fd_laitovo_smoke_schemet->window='ПБ';
            }

            $this->fd_chiko_smoke_scheme=$this->act->json('after')['fd_chiko_smoke_scheme'];

            if (($this->_fd_chiko_smoke_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'ВырезДляКурящих','type_clips'=>'Простые','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_smoke_scheme=new ClipsScheme;
                $this->_fd_chiko_smoke_scheme->car_id=$this->auto->id;
                $this->_fd_chiko_smoke_scheme->brand='Chiko';
                $this->_fd_chiko_smoke_scheme->type='ВырезДляКурящих';
                $this->_fd_chiko_smoke_scheme->type_clips='Простые';
                $this->_fd_chiko_smoke_scheme->window='ПБ';
            }

            $this->fd_chiko_smoke_schemem=$this->act->json('after')['fd_chiko_smoke_schemem'];

            if (($this->_fd_chiko_smoke_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'ВырезДляКурящих','type_clips'=>'Магнитные','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_smoke_schemem=new ClipsScheme;
                $this->_fd_chiko_smoke_schemem->car_id=$this->auto->id;
                $this->_fd_chiko_smoke_schemem->brand='Chiko';
                $this->_fd_chiko_smoke_schemem->type='ВырезДляКурящих';
                $this->_fd_chiko_smoke_schemem->type_clips='Магнитные';
                $this->_fd_chiko_smoke_schemem->window='ПБ';
            }

            $this->fd_chiko_smoke_schemet=$this->act->json('after')['fd_chiko_smoke_schemet'];

            if (($this->_fd_chiko_smoke_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'ВырезДляКурящих','type_clips'=>'Скотч','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_smoke_schemet=new ClipsScheme;
                $this->_fd_chiko_smoke_schemet->car_id=$this->auto->id;
                $this->_fd_chiko_smoke_schemet->brand='Chiko';
                $this->_fd_chiko_smoke_schemet->type='ВырезДляКурящих';
                $this->_fd_chiko_smoke_schemet->type_clips='Скотч';
                $this->_fd_chiko_smoke_schemet->window='ПБ';
            }

            //пб - mirror
            $this->fd_laitovo_mirror_scheme=$this->act->json('after')['fd_laitovo_mirror_scheme'];

            if (($this->_fd_laitovo_mirror_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'ВырезДляЗеркала','type_clips'=>'Простые','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_mirror_scheme=new ClipsScheme;
                $this->_fd_laitovo_mirror_scheme->car_id=$this->auto->id;
                $this->_fd_laitovo_mirror_scheme->brand='Laitovo';
                $this->_fd_laitovo_mirror_scheme->type='ВырезДляЗеркала';
                $this->_fd_laitovo_mirror_scheme->type_clips='Простые';
                $this->_fd_laitovo_mirror_scheme->window='ПБ';
            }

            $this->fd_laitovo_mirror_schemem=$this->act->json('after')['fd_laitovo_mirror_schemem'];

            if (($this->_fd_laitovo_mirror_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'ВырезДляЗеркала','type_clips'=>'Магнитные','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_mirror_schemem=new ClipsScheme;
                $this->_fd_laitovo_mirror_schemem->car_id=$this->auto->id;
                $this->_fd_laitovo_mirror_schemem->brand='Laitovo';
                $this->_fd_laitovo_mirror_schemem->type='ВырезДляЗеркала';
                $this->_fd_laitovo_mirror_schemem->type_clips='Магнитные';
                $this->_fd_laitovo_mirror_schemem->window='ПБ';
            }


            $this->fd_laitovo_mirror_schemet=$this->act->json('after')['fd_laitovo_mirror_schemet'];

            if (($this->_fd_laitovo_mirror_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'ВырезДляЗеркала','type_clips'=>'Скотч','window'=>'ПБ'])->one())==null){
                $this->_fd_laitovo_mirror_schemet=new ClipsScheme;
                $this->_fd_laitovo_mirror_schemet->car_id=$this->auto->id;
                $this->_fd_laitovo_mirror_schemet->brand='Laitovo';
                $this->_fd_laitovo_mirror_schemet->type='ВырезДляЗеркала';
                $this->_fd_laitovo_mirror_schemet->type_clips='Скотч';
                $this->_fd_laitovo_mirror_schemet->window='ПБ';
            }

            $this->fd_chiko_mirror_scheme=$this->act->json('after')['fd_chiko_mirror_scheme'];

            if (($this->_fd_chiko_mirror_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'ВырезДляЗеркала','type_clips'=>'Простые','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_mirror_scheme=new ClipsScheme;
                $this->_fd_chiko_mirror_scheme->car_id=$this->auto->id;
                $this->_fd_chiko_mirror_scheme->brand='Chiko';
                $this->_fd_chiko_mirror_scheme->type='ВырезДляЗеркала';
                $this->_fd_chiko_mirror_scheme->type_clips='Простые';
                $this->_fd_chiko_mirror_scheme->window='ПБ';
            }

            $this->fd_chiko_mirror_schemem=$this->act->json('after')['fd_chiko_mirror_schemem'];

            if (($this->_fd_chiko_mirror_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'ВырезДляЗеркала','type_clips'=>'Магнитные','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_mirror_schemem=new ClipsScheme;
                $this->_fd_chiko_mirror_schemem->car_id=$this->auto->id;
                $this->_fd_chiko_mirror_schemem->brand='Chiko';
                $this->_fd_chiko_mirror_schemem->type='ВырезДляЗеркала';
                $this->_fd_chiko_mirror_schemem->type_clips='Магнитные';
                $this->_fd_chiko_mirror_schemem->window='ПБ';
            }

            $this->fd_chiko_mirror_schemet=$this->act->json('after')['fd_chiko_mirror_schemet'];

            if (($this->_fd_chiko_mirror_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'ВырезДляЗеркала','type_clips'=>'Скотч','window'=>'ПБ'])->one())==null){
                $this->_fd_chiko_mirror_schemet=new ClipsScheme;
                $this->_fd_chiko_mirror_schemet->car_id=$this->auto->id;
                $this->_fd_chiko_mirror_schemet->brand='Chiko';
                $this->_fd_chiko_mirror_schemet->type='ВырезДляЗеркала';
                $this->_fd_chiko_mirror_schemet->type_clips='Скотч';
                $this->_fd_chiko_mirror_schemet->window='ПБ';
            }




            //зб
            $this->rd_laitovo_standart_scheme=$this->act->json('after')['rd_laitovo_standart_scheme'];

            if (($this->_rd_laitovo_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ЗБ'])->one())==null){
                $this->_rd_laitovo_standart_scheme=new ClipsScheme;
                $this->_rd_laitovo_standart_scheme->car_id=$this->auto->id;
                $this->_rd_laitovo_standart_scheme->brand='Laitovo';
                $this->_rd_laitovo_standart_scheme->type='Стандарт';
                $this->_rd_laitovo_standart_scheme->type_clips='Простые';
                $this->_rd_laitovo_standart_scheme->window='ЗБ';
            }

            $this->rd_laitovo_standart_schemem=$this->act->json('after')['rd_laitovo_standart_schemem'];

            if (($this->_rd_laitovo_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ЗБ'])->one())==null){
                $this->_rd_laitovo_standart_schemem=new ClipsScheme;
                $this->_rd_laitovo_standart_schemem->car_id=$this->auto->id;
                $this->_rd_laitovo_standart_schemem->brand='Laitovo';
                $this->_rd_laitovo_standart_schemem->type='Стандарт';
                $this->_rd_laitovo_standart_schemem->type_clips='Магнитные';
                $this->_rd_laitovo_standart_schemem->window='ЗБ';
            }

            $this->rd_laitovo_standart_schemet=$this->act->json('after')['rd_laitovo_standart_schemet'];

            if (($this->_rd_laitovo_standart_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Скотч','window'=>'ЗБ'])->one())==null){
                $this->_rd_laitovo_standart_schemet=new ClipsScheme;
                $this->_rd_laitovo_standart_schemet->car_id=$this->auto->id;
                $this->_rd_laitovo_standart_schemet->brand='Laitovo';
                $this->_rd_laitovo_standart_schemet->type='Стандарт';
                $this->_rd_laitovo_standart_schemet->type_clips='Скотч';
                $this->_rd_laitovo_standart_schemet->window='ЗБ';
            }

            $this->rd_chiko_standart_scheme=$this->act->json('after')['rd_chiko_standart_scheme'];

            if (($this->_rd_chiko_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ЗБ'])->one())==null){
                $this->_rd_chiko_standart_scheme=new ClipsScheme;
                $this->_rd_chiko_standart_scheme->car_id=$this->auto->id;
                $this->_rd_chiko_standart_scheme->brand='Chiko';
                $this->_rd_chiko_standart_scheme->type='Стандарт';
                $this->_rd_chiko_standart_scheme->type_clips='Простые';
                $this->_rd_chiko_standart_scheme->window='ЗБ';
            }

            $this->rd_chiko_standart_schemem=$this->act->json('after')['rd_chiko_standart_schemem'];

            if (($this->_rd_chiko_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ЗБ'])->one())==null){
                $this->_rd_chiko_standart_schemem=new ClipsScheme;
                $this->_rd_chiko_standart_schemem->car_id=$this->auto->id;
                $this->_rd_chiko_standart_schemem->brand='Chiko';
                $this->_rd_chiko_standart_schemem->type='Стандарт';
                $this->_rd_chiko_standart_schemem->type_clips='Магнитные';
                $this->_rd_chiko_standart_schemem->window='ЗБ';
            }

            $this->rd_chiko_standart_schemet=$this->act->json('after')['rd_chiko_standart_schemet'];

            if (($this->_rd_chiko_standart_schemet=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Скотч','window'=>'ЗБ'])->one())==null){
                $this->_rd_chiko_standart_schemet=new ClipsScheme;
                $this->_rd_chiko_standart_schemet->car_id=$this->auto->id;
                $this->_rd_chiko_standart_schemet->brand='Chiko';
                $this->_rd_chiko_standart_schemet->type='Стандарт';
                $this->_rd_chiko_standart_schemet->type_clips='Скотч';
                $this->_rd_chiko_standart_schemet->window='ЗБ';
            }

            //зф - standart
            $this->rv_laitovo_standart_scheme=$this->act->json('after')['rv_laitovo_standart_scheme'];

            if (($this->_rv_laitovo_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ЗФ'])->one())==null){
                $this->_rv_laitovo_standart_scheme=new ClipsScheme;
                $this->_rv_laitovo_standart_scheme->car_id=$this->auto->id;
                $this->_rv_laitovo_standart_scheme->brand='Laitovo';
                $this->_rv_laitovo_standart_scheme->type='Стандарт';
                $this->_rv_laitovo_standart_scheme->type_clips='Простые';
                $this->_rv_laitovo_standart_scheme->window='ЗФ';
            }

            $this->rv_laitovo_standart_schemem=$this->act->json('after')['rv_laitovo_standart_schemem'];

            if (($this->_rv_laitovo_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ЗФ'])->one())==null){
                $this->_rv_laitovo_standart_schemem=new ClipsScheme;
                $this->_rv_laitovo_standart_schemem->car_id=$this->auto->id;
                $this->_rv_laitovo_standart_schemem->brand='Laitovo';
                $this->_rv_laitovo_standart_schemem->type='Стандарт';
                $this->_rv_laitovo_standart_schemem->type_clips='Магнитные';
                $this->_rv_laitovo_standart_schemem->window='ЗФ';
            }

            $this->rv_chiko_standart_scheme=$this->act->json('after')['rv_chiko_standart_scheme'];

            if (($this->_rv_chiko_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ЗФ'])->one())==null){
                $this->_rv_chiko_standart_scheme=new ClipsScheme;
                $this->_rv_chiko_standart_scheme->car_id=$this->auto->id;
                $this->_rv_chiko_standart_scheme->brand='Chiko';
                $this->_rv_chiko_standart_scheme->type='Стандарт';
                $this->_rv_chiko_standart_scheme->type_clips='Простые';
                $this->_rv_chiko_standart_scheme->window='ЗФ';
            }

            $this->rv_chiko_standart_schemem=$this->act->json('after')['rv_chiko_standart_schemem'];

            if (($this->_rv_chiko_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ЗФ'])->one())==null){
                $this->_rv_chiko_standart_schemem=new ClipsScheme;
                $this->_rv_chiko_standart_schemem->car_id=$this->auto->id;
                $this->_rv_chiko_standart_schemem->brand='Chiko';
                $this->_rv_chiko_standart_schemem->type='Стандарт';
                $this->_rv_chiko_standart_schemem->type_clips='Магнитные';
                $this->_rv_chiko_standart_schemem->window='ЗФ';
            }

            //зф - triangul
            $this->rv_laitovo_triangul_scheme=$this->act->json('after')['rv_laitovo_triangul_scheme'];

            if (($this->_rv_laitovo_triangul_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Треугольная','type_clips'=>'Простые','window'=>'ЗФ'])->one())==null){
                $this->_rv_laitovo_triangul_scheme=new ClipsScheme;
                $this->_rv_laitovo_triangul_scheme->car_id=$this->auto->id;
                $this->_rv_laitovo_triangul_scheme->brand='Laitovo';
                $this->_rv_laitovo_triangul_scheme->type='Треугольная';
                $this->_rv_laitovo_triangul_scheme->type_clips='Простые';
                $this->_rv_laitovo_triangul_scheme->window='ЗФ';
            }

            $this->rv_laitovo_triangul_schemem=$this->act->json('after')['rv_laitovo_triangul_schemem'];

            if (($this->_rv_laitovo_triangul_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Треугольная','type_clips'=>'Магнитные','window'=>'ЗФ'])->one())==null){
                $this->_rv_laitovo_triangul_schemem=new ClipsScheme;
                $this->_rv_laitovo_triangul_schemem->car_id=$this->auto->id;
                $this->_rv_laitovo_triangul_schemem->brand='Laitovo';
                $this->_rv_laitovo_triangul_schemem->type='Треугольная';
                $this->_rv_laitovo_triangul_schemem->type_clips='Магнитные';
                $this->_rv_laitovo_triangul_schemem->window='ЗФ';
            }

            $this->rv_chiko_triangul_scheme=$this->act->json('after')['rv_chiko_triangul_scheme'];

            if (($this->_rv_chiko_triangul_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Треугольная','type_clips'=>'Простые','window'=>'ЗФ'])->one())==null){
                $this->_rv_chiko_triangul_scheme=new ClipsScheme;
                $this->_rv_chiko_triangul_scheme->car_id=$this->auto->id;
                $this->_rv_chiko_triangul_scheme->brand='Chiko';
                $this->_rv_chiko_triangul_scheme->type='Треугольная';
                $this->_rv_chiko_triangul_scheme->type_clips='Простые';
                $this->_rv_chiko_triangul_scheme->window='ЗФ';
            }

            $this->rv_chiko_triangul_schemem=$this->act->json('after')['rv_chiko_triangul_schemem'];

            if (($this->_rv_chiko_triangul_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Треугольная','type_clips'=>'Магнитные','window'=>'ЗФ'])->one())==null){
                $this->_rv_chiko_triangul_schemem=new ClipsScheme;
                $this->_rv_chiko_triangul_schemem->car_id=$this->auto->id;
                $this->_rv_chiko_triangul_schemem->brand='Chiko';
                $this->_rv_chiko_triangul_schemem->type='Треугольная';
                $this->_rv_chiko_triangul_schemem->type_clips='Магнитные';
                $this->_rv_chiko_triangul_schemem->window='ЗФ';
            }



            //зш - standart
            $this->bw_laitovo_standart_scheme=$this->act->json('after')['bw_laitovo_standart_scheme'];

            if (($this->_bw_laitovo_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ЗШ'])->one())==null){
                $this->_bw_laitovo_standart_scheme=new ClipsScheme;
                $this->_bw_laitovo_standart_scheme->car_id=$this->auto->id;
                $this->_bw_laitovo_standart_scheme->brand='Laitovo';
                $this->_bw_laitovo_standart_scheme->type='Стандарт';
                $this->_bw_laitovo_standart_scheme->type_clips='Простые';
                $this->_bw_laitovo_standart_scheme->window='ЗШ';
            }

            $this->bw_laitovo_standart_schemem=$this->act->json('after')['bw_laitovo_standart_schemem'];

            if (($this->_bw_laitovo_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ЗШ'])->one())==null){
                $this->_bw_laitovo_standart_schemem=new ClipsScheme;
                $this->_bw_laitovo_standart_schemem->car_id=$this->auto->id;
                $this->_bw_laitovo_standart_schemem->brand='Laitovo';
                $this->_bw_laitovo_standart_schemem->type='Стандарт';
                $this->_bw_laitovo_standart_schemem->type_clips='Магнитные';
                $this->_bw_laitovo_standart_schemem->window='ЗШ';
            }

            $this->bw_chiko_standart_scheme=$this->act->json('after')['bw_chiko_standart_scheme'];

            if (($this->_bw_chiko_standart_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Простые','window'=>'ЗШ'])->one())==null){
                $this->_bw_chiko_standart_scheme=new ClipsScheme;
                $this->_bw_chiko_standart_scheme->car_id=$this->auto->id;
                $this->_bw_chiko_standart_scheme->brand='Chiko';
                $this->_bw_chiko_standart_scheme->type='Стандарт';
                $this->_bw_chiko_standart_scheme->type_clips='Простые';
                $this->_bw_chiko_standart_scheme->window='ЗШ';
            }

            $this->bw_chiko_standart_schemem=$this->act->json('after')['bw_chiko_standart_schemem'];

            if (($this->_bw_chiko_standart_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт','type_clips'=>'Магнитные','window'=>'ЗШ'])->one())==null){
                $this->_bw_chiko_standart_schemem=new ClipsScheme;
                $this->_bw_chiko_standart_schemem->car_id=$this->auto->id;
                $this->_bw_chiko_standart_schemem->brand='Chiko';
                $this->_bw_chiko_standart_schemem->type='Стандарт';
                $this->_bw_chiko_standart_schemem->type_clips='Магнитные';
                $this->_bw_chiko_standart_schemem->window='ЗШ';
            }

            $this->bw_laitovo_double_scheme=$this->act->json('after')['bw_laitovo_double_scheme'];

            //зш - double
            if (($this->_bw_laitovo_double_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт 2 части','type_clips'=>'Простые','window'=>'ЗШ'])->one())==null){
                $this->_bw_laitovo_double_scheme=new ClipsScheme;
                $this->_bw_laitovo_double_scheme->car_id=$this->auto->id;
                $this->_bw_laitovo_double_scheme->brand='Laitovo';
                $this->_bw_laitovo_double_scheme->type='Стандарт 2 части';
                $this->_bw_laitovo_double_scheme->type_clips='Простые';
                $this->_bw_laitovo_double_scheme->window='ЗШ';
            }

            $this->bw_laitovo_double_schemem=$this->act->json('after')['bw_laitovo_double_schemem'];

            if (($this->_bw_laitovo_double_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Laitovo','type'=>'Стандарт 2 части','type_clips'=>'Магнитные','window'=>'ЗШ'])->one())==null){
                $this->_bw_laitovo_double_schemem=new ClipsScheme;
                $this->_bw_laitovo_double_schemem->car_id=$this->auto->id;
                $this->_bw_laitovo_double_schemem->brand='Laitovo';
                $this->_bw_laitovo_double_schemem->type='Стандарт 2 части';
                $this->_bw_laitovo_double_schemem->type_clips='Магнитные';
                $this->_bw_laitovo_double_schemem->window='ЗШ';
            }

            $this->bw_chiko_double_scheme=$this->act->json('after')['bw_chiko_double_scheme'];

            if (($this->_bw_chiko_double_scheme=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт 2 части','type_clips'=>'Простые','window'=>'ЗШ'])->one())==null){
                $this->_bw_chiko_double_scheme=new ClipsScheme;
                $this->_bw_chiko_double_scheme->car_id=$this->auto->id;
                $this->_bw_chiko_double_scheme->brand='Chiko';
                $this->_bw_chiko_double_scheme->type='Стандарт 2 части';
                $this->_bw_chiko_double_scheme->type_clips='Простые';
                $this->_bw_chiko_double_scheme->window='ЗШ';
            }

            $this->bw_chiko_double_schemem=$this->act->json('after')['bw_chiko_double_schemem'];

            if (($this->_bw_chiko_double_schemem=$this->auto->getSchemes()->andWhere(['brand'=>'Chiko','type'=>'Стандарт 2 части','type_clips'=>'Магнитные','window'=>'ЗШ'])->one())==null){
                $this->_bw_chiko_double_schemem=new ClipsScheme;
                $this->_bw_chiko_double_schemem->car_id=$this->auto->id;
                $this->_bw_chiko_double_schemem->brand='Chiko';
                $this->_bw_chiko_double_schemem->type='Стандарт 2 части';
                $this->_bw_chiko_double_schemem->type_clips='Магнитные';
                $this->_bw_chiko_double_schemem->window='ЗШ';
            }



            $this->_json=Json::decode($this->act->json('after')['auto']['json'] ? : '{}');

            $this->mastered=$this->auto->mastered;
            $this->checked=$this->auto->checked;

            $this->article=$this->act->json('after')['auto']['article'];
            $this->mark=$this->act->json('after')['auto']['mark'];
            $this->mark_en=$this->_json['mark_en'];
            $this->model=$this->act->json('after')['auto']['model'];
            $this->model_en=$this->_json['model_en'];
            $this->generation=$this->act->json('after')['auto']['generation'];
            $this->carcass=$this->act->json('after')['auto']['carcass'];
            $this->doors=$this->act->json('after')['auto']['doors'];
            $this->firstyear=$this->act->json('after')['auto']['firstyear'];
            $this->lastyear=$this->act->json('after')['auto']['lastyear'];
            $this->modification=$this->act->json('after')['auto']['modification'];
            $this->modification_en=$this->_json['modification_en'];
            $this->country=$this->act->json('after')['auto']['country'];
            $this->country_en=$this->_json['country_en'];
            $this->category=$this->act->json('after')['auto']['category'];

            $this->fw1=$this->_json['fw1'];
            $this->fv1=$this->_json['fv1'];
            $this->fd1=$this->_json['fd1'];
            $this->rd1=$this->_json['rd1'];
            $this->rv1=$this->_json['rv1'];
            $this->bw1=$this->_json['bw1'];

            $this->fw_status=@$this->_json['_fw']['status'] ? : 0;
            $this->fw_date=@$this->_json['_fw']['date'] ? : null;
            $this->fw_scheme=@$this->_json['_fw']['scheme'] ? Json::encode($this->_json['_fw']['scheme']) : null;

            $this->fv_date=@$this->_json['_fv']['date'] ? : null;
            $this->fv_dlina=@$this->_json['_fv']['dlina'] ? : null;
            $this->fv_visota=@$this->_json['_fv']['visota'] ? : null;
            $this->fv_magnit=@$this->_json['_fv']['magnit'] ? : null;
            $this->fv_laitovo_standart_status=@$this->_json['_fv']['laitovo']['standart']['status'] ? : 0;
            $this->fv_laitovo_dontlookb_status=@$this->_json['_fv']['laitovo']['dontlookb']['status'] ? : 0;
            $this->fv_chiko_standart_status=@$this->_json['_fv']['chiko']['standart']['status'] ? : 0;
            $this->fv_chiko_standart_natyag=@$this->_json['_fv']['chiko']['standart']['natyag'] ? : null;
            $this->fv_chiko_dontlookb_status=@$this->_json['_fv']['chiko']['dontlookb']['status'] ? : 0;
            
            $this->fd_date=@$this->_json['_fd']['date'] ? : null;
            $this->fd_dlina=@$this->_json['_fd']['dlina'] ? : null;
            $this->fd_visota=@$this->_json['_fd']['visota'] ? : null;
            $this->fd_magnit=@$this->_json['_fd']['magnit'] ? : null;
            $this->fd_obshivka=@$this->_json['_fd']['obshivka'] ? : null;
            $this->fd_hlyastik=@$this->_json['_fd']['hlyastik'];
            $this->fd_laitovo_standart_status=@$this->_json['_fd']['laitovo']['standart']['status'] ? : 0;
            $this->fd_laitovo_short_status=@$this->_json['_fd']['laitovo']['short']['status'] ? : 0;
            $this->fd_laitovo_smoke_status=@$this->_json['_fd']['laitovo']['smoke']['status'] ? : 0;
            $this->fd_laitovo_mirror_status=@$this->_json['_fd']['laitovo']['mirror']['status'] ? : 0;
            $this->fd_laitovo_mirror_maxdlina=@$this->_json['_fd']['laitovo']['mirror']['maxdlina'] ? : null;
            $this->fd_laitovo_mirror_maxvisota=@$this->_json['_fd']['laitovo']['mirror']['maxvisota'] ? : null;
            $this->fd_laitovo_dontlooks_status=@$this->_json['_fd']['laitovo']['dontlooks']['status'] ? : 0;
            $this->fd_laitovo_dontlookb_status=@$this->_json['_fd']['laitovo']['dontlookb']['status'] ? : 0;
            $this->fd_chiko_standart_status=@$this->_json['_fd']['chiko']['standart']['status'] ? : 0;
            $this->fd_chiko_standart_natyag=@$this->_json['_fd']['chiko']['standart']['natyag'] ? : null;
            $this->fd_chiko_short_status=@$this->_json['_fd']['chiko']['short']['status'] ? : 0;
            $this->fd_chiko_short_natyag=@$this->_json['_fd']['chiko']['short']['natyag'] ? : null;
            $this->fd_chiko_smoke_status=@$this->_json['_fd']['chiko']['smoke']['status'] ? : 0;
            $this->fd_chiko_smoke_natyag=@$this->_json['_fd']['chiko']['smoke']['natyag'] ? : null;
            $this->fd_chiko_mirror_status=@$this->_json['_fd']['chiko']['mirror']['status'] ? : 0;
            $this->fd_chiko_mirror_maxdlina=@$this->_json['_fd']['chiko']['mirror']['maxdlina'] ? : null;
            $this->fd_chiko_mirror_maxvisota=@$this->_json['_fd']['chiko']['mirror']['maxvisota'] ? : null;
            $this->fd_chiko_mirror_natyag=@$this->_json['_fd']['chiko']['mirror']['natyag'] ? : null;
            $this->fd_chiko_dontlooks_status=@$this->_json['_fd']['chiko']['dontlooks']['status'] ? : 0;
            $this->fd_chiko_dontlookb_status=@$this->_json['_fd']['chiko']['dontlookb']['status'] ? : 0;
            $this->fd_chikomagnet_status=@$this->_json['_fd']['chikomagnet']['status'] ? : 0;
            $this->fd_moscitka_standart_status=@$this->_json['_fd']['moscitka']['standart']['status'] ? : 0;
            $this->fd_laitovowithmagnets_standart_status=@$this->_json['_fd']['laitovowithmagnets']['standart']['status'] ? : 0;
            $this->fd_chikomagnet_magnitov=@$this->_json['_fd']['chikomagnet']['magnitov'] ? : null;
            $this->fd_install_direction=@$this->_json['_fd']['install_direction'] ? : null;
            $this->fd_with_recess=@$this->_json['_fd']['with_recess'] ? : null;
            $this->fd_use_tape=@$this->_json['_fd']['use_tape'] ? : 0;

            $this->rd_date=@$this->_json['_rd']['date'] ? : null;
            $this->rd_dlina=@$this->_json['_rd']['dlina'] ? : null;
            $this->rd_visota=@$this->_json['_rd']['visota'] ? : null;
            $this->rd_magnit=@$this->_json['_rd']['magnit'] ? : null;
            $this->rd_obshivka=@$this->_json['_rd']['obshivka'] ? : null;
            $this->rd_hlyastik=@$this->_json['_rd']['hlyastik'] ? : null;
            $this->rd_laitovo_standart_status=@$this->_json['_rd']['laitovo']['standart']['status'] ? : 0;
            $this->rd_laitovo_dontlooks_status=@$this->_json['_rd']['laitovo']['dontlooks']['status'] ? : 0;
            $this->rd_laitovo_dontlookb_status=@$this->_json['_rd']['laitovo']['dontlookb']['status'] ? : 0;
            $this->rd_chiko_standart_status=@$this->_json['_rd']['chiko']['standart']['status'] ? : 0;
            $this->rd_chiko_standart_natyag=@$this->_json['_rd']['chiko']['standart']['natyag'] ? : null;
            $this->rd_chiko_dontlooks_status=@$this->_json['_rd']['chiko']['dontlooks']['status'] ? : 0;
            $this->rd_chiko_dontlookb_status=@$this->_json['_rd']['chiko']['dontlookb']['status'] ? : 0;
            $this->rd_chikomagnet_status=@$this->_json['_rd']['chikomagnet']['status'] ? : 0;
            $this->rd_moscitka_standart_status=@$this->_json['_rd']['moscitka']['standart']['status'] ? : 0;
            $this->rd_laitovowithmagnets_standart_status=@$this->_json['_rd']['laitovowithmagnets']['standart']['status'] ? : 0;
            $this->rd_chikomagnet_magnitov=@$this->_json['_rd']['chikomagnet']['magnitov'] ? : null;
            $this->rd_install_direction=@$this->_json['_rd']['install_direction'] ? : null;
            $this->rd_with_recess=@$this->_json['_rd']['with_recess'] ? : null;
            $this->rd_use_tape=@$this->_json['_rd']['use_tape'] ? : 0;

            $this->rv_date=@$this->_json['_rv']['date'] ? : null;
            $this->rv_dlina=@$this->_json['_rv']['dlina'] ? : null;
            $this->rv_visota=@$this->_json['_rv']['visota'] ? : null;
            $this->rv_magnit=@$this->_json['_rv']['magnit'] ? : null;
            $this->rv_obshivka=@$this->_json['_rv']['obshivka'] ? : null;
            $this->rv_openwindow=@$this->_json['_rv']['openwindow'] ? : null;
            $this->rv_openwindowtrue=@$this->_json['_rv']['openwindowtrue'] ? : null;
            $this->rv_hlyastik=@$this->_json['_rv']['hlyastik'] ? : null;
            $this->rv_laitovo_standart_status=@$this->_json['_rv']['laitovo']['standart']['status'] ? : 0;
            $this->rv_laitovo_standart_forma=@$this->_json['_rv']['laitovo']['standart']['forma'] ? : null;
            $this->rv_laitovo_dontlooks_status=@$this->_json['_rv']['laitovo']['dontlooks']['status'] ? : 0;
            $this->rv_laitovo_dontlookb_status=@$this->_json['_rv']['laitovo']['dontlookb']['status'] ? : 0;
            $this->rv_chiko_standart_status=@$this->_json['_rv']['chiko']['standart']['status'] ? : 0;
            $this->rv_chiko_standart_forma=@$this->_json['_rv']['chiko']['standart']['forma'] ? : null;
            $this->rv_chiko_standart_natyag=@$this->_json['_rv']['chiko']['standart']['natyag'] ? : null;
            $this->rv_chiko_dontlooks_status=@$this->_json['_rv']['chiko']['dontlooks']['status'] ? : 0;
            $this->rv_chiko_dontlookb_status=@$this->_json['_rv']['chiko']['dontlookb']['status'] ? : 0;

            $this->bw_date=@$this->_json['_bw']['date'] ? : null;
            $this->bw_dlina=@$this->_json['_bw']['dlina'] ? : null;
            $this->bw_visota=@$this->_json['_bw']['visota'] ? : null;
            $this->bw_magnit=@$this->_json['_bw']['magnit'] ? : null;
            $this->bw_obshivka=@$this->_json['_bw']['obshivka'] ? : null;
            $this->bw_openwindow=@$this->_json['_bw']['openwindow'] ? : null;
            $this->bw_openwindowtrue=@$this->_json['_bw']['openwindowtrue'] ? : null;
            $this->bw_chastei=@$this->_json['_bw']['chastei'] ? : null;
            $this->bw_hlyastik=@$this->_json['_bw']['hlyastik'] ? : null;            
            $this->bw_simmetr=isset($this->_json['_bw']['simmetr']) ? $this->_json['_bw']['simmetr'] : null;
            $this->bw_gabarit=isset($this->_json['_bw']['gabarit']) ? $this->_json['_bw']['gabarit'] : null;
            $this->bw_laitovo_standart_status=@$this->_json['_bw']['laitovo']['standart']['status'] ? : 0;
            $this->bw_laitovo_dontlooks_status=@$this->_json['_bw']['laitovo']['dontlooks']['status'] ? : 0;
            $this->bw_chiko_standart_status=@$this->_json['_bw']['chiko']['standart']['status'] ? : 0;
            $this->bw_chiko_standart_natyag=@$this->_json['_bw']['chiko']['standart']['natyag'] ? : null;
            $this->bw_chiko_dontlookb_status=@$this->_json['_bw']['chiko']['dontlookb']['status'] ? : 0;

            foreach ($this->auto->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=isset($this->_json[$value['fields']['id']['value']]) ? $this->_json[$value['fields']['id']['value']] : '';
            }

        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        $this->act_before=$this->act->json('before');
        // $this->act_before['auto']=$this->auto->attributes;

        parent::__construct($config);
    }

    public function save() 
    { 
        if ($this->validate() && $this->scenario == $this->SCENARIO_STEP_FINISH && $this->act->checked == 2) { 

            $json = $this->_json; 
 
            $auto = $this->auto; 

            $act= $this->act; 
 
            $auto->mark=$this->mark; 
            $json['mark_en']=$this->mark_en; 
            $auto->model=$this->model; 
            $json['model_en']=$this->model_en; 
            $auto->generation=$this->generation; 
            $auto->carcass=$this->carcass; 
            $auto->doors=$this->doors; 
            $auto->firstyear=$this->firstyear; 
            $auto->lastyear=$this->lastyear; 
            $auto->modification=$this->modification; 
            $json['modification_en']=$this->modification_en; 
            $auto->country=$this->country; 
            $json['country_en']=$this->country_en; 
            $auto->category=$this->category; 
            $auto->name=$auto->fullname; 
 
            $json['_fw']['status']=$this->fw_status; 
            $json['_fw']['date']=$this->fw_date; 
            if ($this->fw_scheme) 
                $json['_fw']['scheme']=Json::decode($this->fw_scheme); 
 
            $json['_fv']['date']=$this->fv_date; 
            $json['_fv']['dlina']=$this->fv_dlina; 
            $json['_fv']['visota']=$this->fv_visota; 
            $json['_fv']['magnit']=$this->fv_magnit; 
            $json['_fv']['laitovo']['standart']['status']=$this->fv_laitovo_standart_status; 
            $json['_fv']['laitovo']['dontlookb']['status']=$this->fv_laitovo_dontlookb_status; 
            $json['_fv']['chiko']['standart']['status']=$this->fv_chiko_standart_status; 
            $json['_fv']['chiko']['standart']['natyag']=$this->fv_chiko_standart_natyag; 
            $json['_fv']['chiko']['dontlookb']['status']=$this->fv_chiko_dontlookb_status; 
 
            $json['_fd']['date']=$this->fd_date; 
            $json['_fd']['dlina']=$this->fd_dlina; 
            $json['_fd']['visota']=$this->fd_visota; 
            $json['_fd']['magnit']=$this->fd_magnit; 
            $json['_fd']['obshivka']=$this->fd_obshivka; 
            $json['_fd']['hlyastik']=$this->fd_hlyastik; 
            $json['_fd']['laitovo']['standart']['status']=$this->fd_laitovo_standart_status; 
            $json['_fd']['laitovo']['short']['status']=$this->fd_laitovo_short_status; 
            $json['_fd']['laitovo']['smoke']['status']=$this->fd_laitovo_smoke_status; 
            $json['_fd']['laitovo']['mirror']['status']=$this->fd_laitovo_mirror_status; 
            $json['_fd']['laitovo']['mirror']['maxdlina']=$this->fd_laitovo_mirror_maxdlina; 
            $json['_fd']['laitovo']['mirror']['maxvisota']=$this->fd_laitovo_mirror_maxvisota; 
            $json['_fd']['laitovo']['dontlooks']['status']=$this->fd_laitovo_dontlooks_status; 
            $json['_fd']['laitovo']['dontlookb']['status']=$this->fd_laitovo_dontlookb_status; 
            $json['_fd']['chiko']['standart']['status']=$this->fd_chiko_standart_status; 
            $json['_fd']['chiko']['standart']['natyag']=$this->fd_chiko_standart_natyag; 
            $json['_fd']['chiko']['short']['status']=$this->fd_chiko_short_status; 
            $json['_fd']['chiko']['short']['natyag']=$this->fd_chiko_short_natyag; 
            $json['_fd']['chiko']['smoke']['status']=$this->fd_chiko_smoke_status; 
            $json['_fd']['chiko']['smoke']['natyag']=$this->fd_chiko_smoke_natyag; 
            $json['_fd']['chiko']['mirror']['status']=$this->fd_chiko_mirror_status; 
            $json['_fd']['chiko']['mirror']['maxdlina']=$this->fd_chiko_mirror_maxdlina; 
            $json['_fd']['chiko']['mirror']['maxvisota']=$this->fd_chiko_mirror_maxvisota; 
            $json['_fd']['chiko']['mirror']['natyag']=$this->fd_chiko_mirror_natyag; 
            $json['_fd']['chiko']['dontlooks']['status']=$this->fd_chiko_dontlooks_status; 
            $json['_fd']['chiko']['dontlookb']['status']=$this->fd_chiko_dontlookb_status; 
            $json['_fd']['chikomagnet']['status']=$this->fd_chikomagnet_status; 
            $json['_fd']['moscitka']['standart']['status']=$this->fd_moscitka_standart_status; 
            $json['_fd']['laitovowithmagnets']['standart']['status']=$this->fd_laitovowithmagnets_standart_status; 
            $json['_fd']['chikomagnet']['magnitov']=$this->fd_chikomagnet_magnitov;
            $json['_fd']['install_direction']=$this->fd_install_direction;
            $json['_fd']['with_recess']=$this->fd_with_recess;
            $json['_fd']['use_tape']=$this->fd_use_tape;

            $json['_rd']['date']=$this->rd_date; 
            $json['_rd']['dlina']=$this->rd_dlina; 
            $json['_rd']['visota']=$this->rd_visota; 
            $json['_rd']['magnit']=$this->rd_magnit; 
            $json['_rd']['obshivka']=$this->rd_obshivka; 
            $json['_rd']['hlyastik']=$this->rd_hlyastik; 
            $json['_rd']['laitovo']['standart']['status']=$this->rd_laitovo_standart_status; 
            $json['_rd']['laitovo']['dontlooks']['status']=$this->rd_laitovo_dontlooks_status; 
            $json['_rd']['laitovo']['dontlookb']['status']=$this->rd_laitovo_dontlookb_status; 
            $json['_rd']['chiko']['standart']['status']=$this->rd_chiko_standart_status; 
            $json['_rd']['chiko']['standart']['natyag']=$this->rd_chiko_standart_natyag; 
            $json['_rd']['chiko']['dontlooks']['status']=$this->rd_chiko_dontlooks_status; 
            $json['_rd']['chiko']['dontlookb']['status']=$this->rd_chiko_dontlookb_status; 
            $json['_rd']['chikomagnet']['status']=$this->rd_chikomagnet_status; 
            $json['_rd']['moscitka']['standart']['status']=$this->rd_moscitka_standart_status; 
            $json['_rd']['laitovowithmagnets']['standart']['status']=$this->rd_laitovowithmagnets_standart_status; 
            $json['_rd']['chikomagnet']['magnitov']=$this->rd_chikomagnet_magnitov;
            $json['_rd']['install_direction']=$this->rd_install_direction;
            $json['_rd']['with_recess']=$this->rd_with_recess;
            $json['_rd']['use_tape']=$this->rd_use_tape;

            $json['_rv']['date']=$this->rv_date; 
            $json['_rv']['dlina']=$this->rv_dlina; 
            $json['_rv']['visota']=$this->rv_visota; 
            $json['_rv']['magnit']=$this->rv_magnit; 
            $json['_rv']['obshivka']=$this->rv_obshivka; 
            $json['_rv']['openwindow']=$this->rv_openwindow; 
            $json['_rv']['openwindowtrue']=$this->rv_openwindowtrue; 
            $json['_rv']['hlyastik']=$this->rv_hlyastik; 
            $json['_rv']['laitovo']['standart']['status']=$this->rv_laitovo_standart_status; 
            $json['_rv']['laitovo']['standart']['forma']=$this->rv_laitovo_standart_forma; 
            $json['_rv']['laitovo']['dontlooks']['status']=$this->rv_laitovo_dontlooks_status; 
            $json['_rv']['laitovo']['dontlookb']['status']=$this->rv_laitovo_dontlookb_status; 
            $json['_rv']['chiko']['standart']['status']=$this->rv_chiko_standart_status; 
            $json['_rv']['chiko']['standart']['forma']=$this->rv_chiko_standart_forma; 
            $json['_rv']['chiko']['standart']['natyag']=$this->rv_chiko_standart_natyag; 
            $json['_rv']['chiko']['dontlooks']['status']=$this->rv_chiko_dontlooks_status; 
            $json['_rv']['chiko']['dontlookb']['status']=$this->rv_chiko_dontlookb_status; 
 
            $json['_bw']['date']=$this->bw_date; 
            $json['_bw']['dlina']=$this->bw_dlina; 
            $json['_bw']['visota']=$this->bw_visota; 
            $json['_bw']['magnit']=$this->bw_magnit; 
            $json['_bw']['obshivka']=$this->bw_obshivka; 
            $json['_bw']['openwindow']=$this->bw_openwindow; 
            $json['_bw']['openwindowtrue']=$this->bw_openwindowtrue; 
            $json['_bw']['chastei']=$this->bw_chastei; 
            $json['_bw']['hlyastik']=$this->bw_hlyastik;             
            $json['_bw']['simmetr']=$this->bw_simmetr; 
            $json['_bw']['gabarit']=$this->bw_gabarit;
            $json['_bw']['laitovo']['standart']['status']=$this->bw_laitovo_standart_status;
            $json['_bw']['laitovo']['dontlooks']['status']=$this->bw_laitovo_dontlooks_status; 
            $json['_bw']['chiko']['standart']['status']=$this->bw_chiko_standart_status; 
            $json['_bw']['chiko']['standart']['natyag']=$this->bw_chiko_standart_natyag; 
            $json['_bw']['chiko']['dontlookb']['status']=$this->bw_chiko_dontlookb_status; 
 
            foreach ($auto->fields as $key => $value) { 
                $json[$value['fields']['id']['value']]=isset($this->fields[$value['fields']['id']['value']]) ? $this->fields[$value['fields']['id']['value']] : ''; 
            } 
            $json['fw1']=$this->fw1; 
            $json['fv1']=$this->fv1; 
            $json['fd1']=$this->fd1; 
            $json['rd1']=$this->rd1; 
            $json['rv1']=$this->rv1; 
            $json['bw1']=$this->bw1; 
             
            $this->auto->json=Json::encode($json); 
            
            $number = $act->json('number');
            $act->json=Json::encode(['number'=>$this->act->json('number'),'before'=>$this->act->json('before'),'after'=>$this->attributes]);
            //Деление на освоение и внесение изменений
            $act->checked = null; 
            $act->save(); 
             
            return true; 

        } else { 
            return false; 
        } 
    }

    public function delete()
    {
        return $this->auto->delete();
    }
}
