<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ErpNaryadSearch extends Model
{

    public $article;
    public $window;
    public $windowType;
    public $tkan;
    public $sort;


    public function rules()
    {
       return [

           [[  'window', 'windowType', 'tkan', 'sort' ], 'safe'],

      ];
   }


  public function scenarios()
  {
      // bypass scenarios() implementation in the parent class
      return Model::scenarios();
  }

    public function search($params)
    {
        $query = ErpNaryad::find();

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {
            $prepareString = preg_replace('/\D+/',' ', Yii::$app->request->get('search'));
            $prepareString = preg_replace('/\s+/', ' ', $prepareString);
            $array = explode(' ',$prepareString);
            if (is_array($array) && count($array) > 1) {
                $query->andWhere(['logist_id'=> $array]);
            } else {
                $query->andFilterWhere([
                    'or',
                    ['like', 'barcode', str_replace('/', 'D', Yii::$app->request->get('search'))],
                ]);
            }
        }

        if ($validator->validate(Yii::$app->request->get('search-by-order'), $error)) {
            $prepareString = preg_replace('/\D+/',' ', Yii::$app->request->get('search-by-order'));
            $prepareString = preg_replace('/\s+/', ' ', $prepareString);
            $array = explode(' ',$prepareString);
            if (is_array($array) && count($array) > 1) {
                $query->andWhere(['order_id'=> $array]);
            } else {
                $query->andFilterWhere(['order_id' => trim($prepareString)]);
            }
        }
        if (!isset($params['search']) || $params['search'] == '') {
            switch ($params['show'] ?? null) {
                case 'onProduction':
                    $query->andWhere(['or',
                            ['in', 'status', [ErpNaryad::STATUS_IN_WORK, ErpNaryad::STATUS_IN_PAUSE]],
                            ['status' => null]
                        ]
                    );
                    break;
                case 'onPause':
                    $query->andWhere(['status' => ErpNaryad::STATUS_IN_PAUSE]);
                    break;
                default:
                    break;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['barcode'=>SORT_ASC]]
        ]);

        if ($this->load($params)) {
            if ($this->window) {
                $query->andFilterWhere(['like', 'article', $this->window . '-%-%-%-%', false]);
            }
            if ($this->tkan) {
                $query->andFilterWhere(['like', 'article', '%-%-%-%-' . $this->tkan, false]);
            }
            if ($this->windowType) {
                $query->andFilterWhere(['like', 'article', '%-%-%-' . $this->windowType . '-%', false]);
            }
            if ($this->sort) {
                $query->andFilterWhere(['sort' => $this->sort]);
            }
        }

        return $dataProvider;
    }
}
