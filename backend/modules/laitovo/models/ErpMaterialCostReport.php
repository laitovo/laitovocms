<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 7/17/17
 * Time: 4:18 PM
 */

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use backend\modules\laitovo\models\Materials;
use common\models\laitovo\MaterialProductionScheme;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;

class ErpMaterialCostReport extends Materials
{
    private $dateFrom;
    private $dateTo;
    public $product_id;
    public $unit;
    public $status;
    public $location;
    public $search;
    public $user;


    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'required'],
            [['product_id', 'unit', 'location'], 'integer'],
            [['status'],'boolean'],
            [['user'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => 'Начало периода *(00:00:00)',
            'dateTo' => 'Конец периода * (23:59:59)',
            'product_id' => 'Вид продукта',
            'unit' => 'Вид расчета',
            'status' => 'Не отображать нулевые позиции',
            'location' => 'Выбрать участок',
            'user' => 'Выбрать сотрудника',
        ];
    }

    public function setDateFrom($value)
    {
        $this->dateFrom = Yii::$app->formatter->asTimestamp($value);
    }

    public function getDateFrom()
    {
        return $this->dateFrom ?  $this->dateFrom : mktime(0, 0, 0, date('m'), 1);
    }

    public function setDateTo($value)
    {
        $this->dateTo = Yii::$app->formatter->asTimestamp($value);
    }

    public function getDateTo()
    {
        return  $this->dateTo? $this->dateTo :  mktime(23, 59, 59);
    }


    public function generateReport()
    {

        return  Materials::find()->joinWith('material');
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function naryadCount()
    {
        $naryads = ErpNaryad::find()->where(['and',
            ['>', 'created_at', $this->getDateFrom()],
            ['<', 'created_at', $this->getDateTo()],
            ['status' => ErpNaryad::STATUS_READY],
        ]);

        if($this->product_id==null)
            return $count = $naryads->count();
        else
            $product = ErpProductType::findOne($this->product_id);
            if(isset($product) && $product->rule)
            {
                $naryadCount = 0;

                foreach ($naryads->all() as $naryad) {
                    if ($product->rule && preg_match($product->rule, $naryad->article)) {
                        $naryadCount++;
                    }
                }
                return $naryadCount;
            }
    }


    public function report($search=null)
    {
        $results=[];
        $total_sum = 0;
        $dispatcher = ErpLocation::findOne(Yii::$app->params['erp_dispatcher']);
        $arrayData =[];
//        if($this->user)
//        {
//            $user = ErpUser::findOne($this->user);
//            $naryads_id=[];
//            $naryads = ErpNaryad::find()->where(['and',
//                ['>', 'created_at', $this->getDateFrom()],
//                ['<', 'created_at', $this->getDateTo()],
//                ['status' => ErpNaryad::STATUS_READY],
//                ['like','json','%'.$user->name.'%',false]])
//               // ->select('id', 'json')
//                ->all();
//
//            foreach($naryads as $naryad) {
//                if ($naryad->json) {
//                    $jsons = Json::decode($naryad->json);
//                    //проходимся циклом по полю json и ищем дату получения Работником наряда
//                    if ($jsons['log']) {
//
//                        foreach ($jsons['log'] as $json) {
//
//                            if ($json['user'] == $user->name && $json['location_from'] && $json['location_from'] !== $dispatcher) {
//                                break;
//                            }
//                        }
//
//                        $location = ErpLocation::find()->where(['like', 'name', $json['location_from']])->one();
//
//                                if($this->product_id==null)
//                                {
//                                    $products = ErpProductType::find()->all();
//                                    if(isset($products))
//                                    {
//                                        foreach ($products as $product) {
//                                            if ($product->rule && preg_match($product->rule, $naryad->article)) {
//                                                break;
//                                            }
//                                        }
//                                                $materialSchemes = MaterialProductionScheme::find()->where(['production_scheme_id' => $product->production_scheme_id, 'location_id' => $location->id])->all();
//                                                if(isset($materialSchemes))
//                                                    foreach ($materialSchemes as $materialScheme)
//                                                    {
//
//                                                        if(array_key_exists($materialScheme->material_parameter_id,$arrayData))
//                                                        {
//
//                                                            if ($materialScheme->material_count && $materialScheme->materialParameter->getCoefficient_settlement() && $materialScheme->materialParameter->getAccounting_unit_price())
//                                                            {
//                                                                if ($this->unit == 1) {
//                                                                    $count = $materialScheme->material_count * $materialScheme->materialParameter->getCoefficient_settlement() * $materialScheme->materialParameter->getAccounting_unit_price();
//
//                                                                } else {
//                                                                    $count = $materialScheme->material_count * $materialScheme->materialParameter->getCoefficient_settlement();
//                                                                }
//                                                                $arrayData[$materialScheme->material_parameter_id]['count'] += round($count, 2);
//                                                            }
//                                                        }else{
//                                                            $arrayData[$materialScheme->material_parameter_id]['materialName'] = $materialScheme->materialParameter->materialName;
//                                                            if ($materialScheme->material_count && $materialScheme->materialParameter->getCoefficient_settlement() && $materialScheme->materialParameter->getAccounting_unit_price())
//                                                            {
//                                                                if ($this->unit == 1) {
//                                                                    $count = $materialScheme->material_count * $materialScheme->materialParameter->getCoefficient_settlement() * $materialScheme->materialParameter->getAccounting_unit_price();
//                                                                    $total_sum+=$count;
//                                                                } else {
//                                                                    $count = $materialScheme->material_count * $materialScheme->materialParameter->getCoefficient_settlement();
//                                                                    $total_sum+=$count*$materialScheme->materialParameter->getAccounting_unit_price();
//
//                                                                }
//                                                                $arrayData[$materialScheme->material_parameter_id]['count'] = round($count, 2);
//                                                                $arrayData[$materialScheme->material_parameter_id]['id'] = $materialScheme->material_parameter_id;
//                                                                $arrayData[$materialScheme->material_parameter_id]['unit'] = $materialScheme->materialParameter->getAccountingUnitName();
//                                                            }
//                                                        }
//                                                    }
//
//                                            }
//                                        }
//
//
//
//
//
//                    }
//                }
//
//            }
//
//            return [ 'arrayData'=>$arrayData, 'total_sum'=>$total_sum];

           // $naryads = ErpNaryad::find()->where(['in', 'id', $naryads_id])->groupBy('article')->select(['article', 'count(id) as count'])->all();
//        }else{

            $naryads = ErpNaryad::find()->where(['and',
                ['>', 'created_at', $this->getDateFrom()],
                ['<', 'created_at', $this->getDateTo()],
                ['status' => ErpNaryad::STATUS_READY],
            ])->groupBy('article')->select(['article', 'count(id) as count'])->all();
     //   }


        if($search) {
            if($this->location){
                $materials =  Materials::find()->joinWith(['material', 'materialLocations'])->where(['and',['or',
                    ['like', 'material.name', $search],
                    // ['id'=>Yii::$app->request->get('search')],
                    ['like', 'parameter', $search]],
                    ['laitovo_erp_location_id'=>$this->location]
                ])->all();
            }else {
                $materials = Materials::find()->joinWith('material')->where(['or',
                    ['like', 'material.name', $search],
                    // ['id'=>Yii::$app->request->get('search')],
                    ['like', 'parameter', $search],
                ])->all();
            }
        }else{
            if($this->location)
            {
                $materials =  Materials::find()->joinWith(['material', 'materialLocations'])->where(['laitovo_erp_location_id'=>$this->location])->all();
            }else {
                $materials = Materials::find()->joinWith('material')->all();
            }
        }

        if($this->product_id !=null)
        {
            $product=ErpProductType::findOne($this->product_id);
            foreach ($naryads as $naryad)
            {
                if ($product->rule && preg_match($product->rule, $naryad->article)) {
                        if(array_key_exists($product->production_scheme_id,$results)) {
                            $results[$product->production_scheme_id]+=$naryad->count;
                        }else {
                            $results[$product->production_scheme_id] = $naryad->count;
                        }
                        
                    }

            }
        }else{
            $products=ErpProductType::find()->all();
            foreach ($naryads as $naryad)
            {

                foreach ($products as $product) {
                    if ($product->rule && preg_match($product->rule, $naryad->article)) {
                        if(array_key_exists($product->production_scheme_id,$results)) {
                            $results[$product->production_scheme_id]+=$naryad->count;
                        }else {
                            $results[$product->production_scheme_id] = $naryad->count;
                        }
                        break;
                    }

                }
            }
        }

        foreach ($materials as $material) {
            $total_count=0;

             foreach ($results as $key=>$value ) {
                 $count = 0;
                $materialSchemes = MaterialProductionScheme::find()->where(['production_scheme_id' => $key, 'material_parameter_id' => $material->id])->all();
                if (isset($materialSchemes)) {
                    foreach ($materialSchemes as $materialScheme) {
                        if ($materialScheme->material_count && $material->getCoefficient_settlement() && $material->getAccounting_unit_price()) {
                            if ($this->unit == 1) {
                                $count += $materialScheme->material_count * $material->getCoefficient_settlement() * $material->getAccounting_unit_price();

                            } else {
                                $count += $materialScheme->material_count * $material->getCoefficient_settlement();
                            }
                        }
                    }
                    $total_count += $count * $value;
                }
             }
             if(($this->status && $total_count!=0) || !$this->status) {
                 $arrayData[$material->id]['materialName'] = $material->materialName;

                 if ($this->unit == 1) {
                     $arrayData[$material->id]['unit'] = 'Рубли';
                     $total_sum += round($total_count, 2);
                 } else {
                     $arrayData[$material->id]['unit'] = $material->getAccountingUnitName();
                     $total_sum += round($total_count, 2) * $material->getAccounting_unit_price();

                 }
                 $arrayData[$material->id]['count'] = round($total_count, 2);
                 $arrayData[$material->id]['id'] = $material->id;

             }

        }

        return [ 'arrayData'=>$arrayData, 'total_sum'=>$total_sum];

    }


}