<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 09.10.17
 * Time: 8:18
 */

namespace backend\modules\laitovo\models;

class AdditionalProduct
{
//    private $dontLook;
//    private $laitBag;
//    private $weelCover;
//    private $seatCover;

    public function getTemplate()
    {
        $count = ErpNaryad::find()->where(['and',
            ['or',
                ['status' => ErpNaryad::STATUS_IN_WORK],
                ['status' => ErpNaryad::STATUS_IN_PAUSE],
                ['and',
                    ['or',['status' => null],['status' => '']],
                    ['is not','start',null]
                ]
            ],
            ['isNeedTemplate'=> true],
        ])->count('id');
        return $count;
    }

    public function getDontlook()
    {
        $count = ErpNaryad::find()->where(['and',
           ['or',
               ['status' => ErpNaryad::STATUS_IN_WORK],
               ['status' => ErpNaryad::STATUS_IN_PAUSE],
                ['and',
                    ['or',['status' => null],['status' => '']],
                    ['is not','start',null]
                ]
           ],
           ['or',
               ['like', 'article', '%-%-%-5-%', false],
               ['like', 'article', '%-%-%-4-3', false],
           ]])->count('id');
        return $count;
    }

    public function getLaitBag()
    {
        $count = ErpNaryad::find()->where(['and',
            ['or',
                ['status' => ErpNaryad::STATUS_IN_WORK],
                ['status' => ErpNaryad::STATUS_IN_PAUSE],
                ['and',
                    ['or',['status' => null],['status' => '']],
                    ['is not','start',null]
                ]
            ],
            ['or',
                ['REGEXP', 'article', 'OT-(1215|1214|1190|1189)-47-.+'],
                ['REGEXP', 'article', 'OT-(1209|1213).+'],
            ]])->count('id');
        return $count;
    }

    public function getWeelCover()
    {
        $count = ErpNaryad::find()->where(['and',
            ['or',
                ['status' => ErpNaryad::STATUS_IN_WORK],
                ['status' => ErpNaryad::STATUS_IN_PAUSE],
                ['and',
                    ['or',['status' => null],['status' => '']],
                    ['is not','start',null]
                ]
            ],
            ['or',
                ['REGEXP', 'article', 'OT-(673|674|675).+'],
            ]])->count('id');
        return $count;
    }

    public function getSeatCover()
    {
        $count = ErpNaryad::find()->where(['and',
            ['or',
                ['status' => ErpNaryad::STATUS_IN_WORK],
                ['status' => ErpNaryad::STATUS_IN_PAUSE],
                ['and',
                    ['or',['status' => null],['status' => '']],
                    ['is not','start',null]
                ]
            ],
            ['or',
                ['REGEXP', 'article', 'OT-1211.+'],
            ]])->count('id');
        return $count;
    }
}