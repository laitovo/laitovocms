<?php

namespace backend\modules\laitovo\models;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;


/**
*  Модель для отображения экрана логистики
*/
class ErpLogisticReport
{
    /**
     * @param $dateFrom
     * @param $dateTo
     * @return object
     */
    public function getData($dateFrom,$dateTo)
    {
        $dateFrom = $dateFrom + 0;
        $dateTo = $dateTo + 24*60*60 - 1;

        $all = $this->getCountAll($dateFrom,$dateTo);
        $prod = $this->getCountProduction($dateFrom,$dateTo);
        $storage = $all - $prod;
        $percentProd = round($all == 0 ? 0 : ($prod/$all)*100,0);
        $percentStorage = round($all == 0 ? 0 : ($storage/$all)*100,0);

        return (object)$result = [
            'title' => 'Логистика',
            'all' => $all,
            'prod' => $prod,
            'storage' => $storage,
            'percentProd' => $percentProd,
            'percentStorage' => $percentStorage,
        ];
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return int|string
     */
    private function getCountAll($dateFrom,$dateTo)
    {
        $orderIds = OrderEntry::find()
            ->select('order_id')
            ->where(['>','created_at',$dateFrom])
            ->andWhere(['<','created_at',$dateTo])
            ->distinct('order_id')
            ->column();

        $filteredOrderIds = Order::find()
            ->select('id')
            ->where(['id' => $orderIds])
            ->andWhere(['processed' => 1])
            ->column();

        return OrderEntry::find()
            ->where(['order_id' => $filteredOrderIds])
            ->count();
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return int|string
     */
    private function getCountProduction($dateFrom,$dateTo)
    {
        $orderIds = OrderEntry::find()
            ->select('order_id')
            ->where(['>','created_at',$dateFrom])
            ->andWhere(['<','created_at',$dateTo])
            ->groupBy('order_id')
            ->column();

        $filteredOrderIds = Order::find()
            ->select('id')
            ->where(['id' => $orderIds])
            ->andWhere(['processed' => 1])
            ->column();

         return OrderEntry::find()
            ->alias('t')
            ->joinWith('upn.erpNaryad workOrder', false)
            ->where(['t.order_id' => $filteredOrderIds])
            ->andWhere('`logistics_naryad`.`reserved_at` <= `workOrder`.`created_at`')
            ->count();
    }
    

}