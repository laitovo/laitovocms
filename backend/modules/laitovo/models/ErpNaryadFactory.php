<?php
namespace backend\modules\laitovo\models;

use common\models\laitovo\Cars;
use common\models\logistics\OrderEntry;
use yii\helpers\Json;

class ErpNaryadFactory
{
    /**
     * @param $article
     * @param $title
     * @param $num
     * @param $barcode
     * @param $comment
     * @return ErpNaryad
     */
    public function create($article,$title,$num,$barcode,$comment,$settext,$itemtext,$sort = null)
    {

        $carAricle      = $this->_getArticleCar($article);
        $artilceBarcode = $this->_getArticleBarcode($article);
        $window         = $this->_getWindow($article);
        $window2         = $this->_getWindow2($article);
        $typeWindow     = $this->_getTypeWindow($article);
        $cloth          = $this->_getCloth($article);

        $car=Cars::find()->where(['article'=>$carAricle])->one();

        $response['name']= $title ? $title : '';
        $response['article']= $article;
        $response['barcode']= $artilceBarcode;
        $response['car']='';
        $response['lekalo']='';
        $response['natyag']='';
        $response['visota']='';
        $response['magnit']='';
        $response['hlyastik']='';
        $response['comment']='';
        $response['quantity']= 1;
        $response['settext']= $settext;
        $response['itemtext']= $itemtext;
        if ($car) {
            $response['car']=$car->name;
            $response['lekalo']=$car->json('nomerlekala');
            if ($cloth==5){//chiko
                switch ($typeWindow) {
                    case '2':
                        $type='smoke';
                        break;
                    case '3':
                        $type='mirror';
                        break;
                    case '6':
                        $type='short';
                        break;

                    default:
                        $type='standart';
                        break;
                }
                $response['natyag']=@$car->json('_'.$window)['chiko'][$type]['natyag'];
            }
            $response['visota']=@$car->json('visota'.$window2);
            $response['magnit']=@$car->json('_'.$window)['magnit'];
            $response['hlyastik']=@$car->json('_'.$window)['hlyastik'];
            $response['comment']=$car->json('info') . $comment;
        }

        $erpnaryad=new ErpNaryad;
        $erpnaryad->barcode= $barcode;
        $erpnaryad->logist_id= $num;
        $erpnaryad->article=$article;
        $erpnaryad->items=Json::encode([$response]);
        if ($sort) {
            $erpnaryad->sort=$sort;
        }
        $erpnaryad->save();

        return $erpnaryad;
    }

    public function createFromOrderEntry(OrderEntry $orderEntry,$num,$barcode,$comment,$sort = null)
    {
        $article = $orderEntry->article;

        $carAricle      = $this->_getArticleCar($article);
        $artilceBarcode = $this->_getArticleBarcode($article);
        $window         = $this->_getWindow($article);
        $window2         = $this->_getWindow2($article);
        $typeWindow     = $this->_getTypeWindow($article);
        $cloth          = $this->_getCloth($article);

        $car=Cars::find()->where(['article'=>$carAricle])->one();

        $response['name']= $orderEntry->name ? $orderEntry->name : '';
        $response['article']= $article;
        $response['barcode']= $artilceBarcode;
        $response['car']='';
        $response['lekalo']='';
        $response['natyag']='';
        $response['visota']='';
        $response['magnit']='';
        $response['hlyastik']='';
        $response['comment']='';
        $response['quantity']= 1;
        $response['settext']= $orderEntry->settext;
        $response['itemtext']= $orderEntry->itemtext;
        if ($car) {
            $response['car']=$car->name;
            $response['lekalo']=$car->json('nomerlekala');
            if ($cloth==5){//chiko
                switch ($typeWindow) {
                    case '2':
                        $type='smoke';
                        break;
                    case '3':
                        $type='mirror';
                        break;
                    case '6':
                        $type='short';
                        break;

                    default:
                        $type='standart';
                        break;
                }
                $response['natyag']=@$car->json('_'.$window)['chiko'][$type]['natyag'];
            }
            $response['visota']=@$car->json('visota'.$window2);
            $response['magnit']=@$car->json('_'.$window)['magnit'];
            $response['hlyastik']=@$car->json('_'.$window)['hlyastik'];
            $response['comment']=$car->json('info') . $comment;
        }

        $erpnaryad=new ErpNaryad;
        $erpnaryad->barcode= $barcode;
        $erpnaryad->logist_id= $num;
        $erpnaryad->article=$article;
        $erpnaryad->withoutLabels = $orderEntry->withoutLabels;
        $erpnaryad->halfStake = $orderEntry->halfStake;
        $erpnaryad->items=Json::encode([$response]);
        if ($sort) {
            $erpnaryad->sort=$sort;
        }
        $erpnaryad->save();

        return $erpnaryad;
    }

    public function createAndPause($article,$title,$num,$barcode,$comment,$settext,$itemtext,$sort = null)
    {

        $carAricle      = $this->_getArticleCar($article);
        $artilceBarcode = $this->_getArticleBarcode($article);
        $window         = $this->_getWindow($article);
        $window2         = $this->_getWindow2($article);
        $typeWindow     = $this->_getTypeWindow($article);
        $cloth          = $this->_getCloth($article);

        $car=Cars::find()->where(['article'=>$carAricle])->one();

        $response['name']= $title ? $title : '';
        $response['article']= $article;
        $response['barcode']= $artilceBarcode;
        $response['car']='';
        $response['lekalo']='';
        $response['natyag']='';
        $response['visota']='';
        $response['magnit']='';
        $response['hlyastik']='';
        $response['comment']='';
        $response['quantity']= 1;
        $response['settext']= $settext;
        $response['itemtext']= $itemtext;
        if ($car) {
            $response['car']=$car->name;
            $response['lekalo']=$car->json('nomerlekala');
            if ($cloth==5){//chiko
                switch ($typeWindow) {
                    case '2':
                        $type='smoke';
                        break;
                    case '3':
                        $type='mirror';
                        break;
                    case '6':
                        $type='short';
                        break;

                    default:
                        $type='standart';
                        break;
                }
                $response['natyag']=@$car->json('_'.$window)['chiko'][$type]['natyag'];
            }
            $response['visota']=@$car->json('visota'.$window2);
            $response['magnit']=@$car->json('_'.$window)['magnit'];
            $response['hlyastik']=@$car->json('_'.$window)['hlyastik'];
            $response['comment']=$car->json('info') . $comment;
        }

        $erpnaryad=new ErpNaryad;
        $erpnaryad->barcode= $barcode;
        $erpnaryad->status= ErpNaryad::STATUS_IN_PAUSE;
        $erpnaryad->logist_id= $num;
        $erpnaryad->article=$article;
        $erpnaryad->items=Json::encode([$response]);
        if ($sort) {
            $erpnaryad->sort=$sort;
        }
        $erpnaryad->save();

        return $erpnaryad;
    }

    private function _getArticleBarcode($article)
    {
        $value = explode('-', $article);
        switch (count($value)) {
            case 5:
                return $value[0]
                    . str_pad($value[2], 4, "0", STR_PAD_LEFT)
                    . str_pad($value[3], 2, "0", STR_PAD_LEFT)
                    . $value[4];

            case 4:
                return $value[0]
                    . str_pad($value[1], 4, "0", STR_PAD_LEFT)
                    . str_pad($value[2], 2, "0", STR_PAD_LEFT)
                    . $value[3];

            default:
                return '';
        }
    }

    private function _getArticleCar($article)
    {
        $value = explode('-', $article);
        switch (count($value)) {
            case 5:
                return $value[2];

            default:
                return '';
        }
    }

    private function _getTypeWindow($article)
    {
        $value = explode('-', $article);
        switch (count($value)) {
            case 5:
                return $value[3];

            default:
                return '';
        }
    }

    private function _getWindow($article)
    {
        $value = explode('-', $article);
        switch (count($value)) {
            case 5:
                $window2 = mb_strtolower($value[0]);
                return $window2;

            default:
                return '';
        }
    }

    private function _getWindow2($article)
    {
        $value = explode('-', $article);
        switch (count($value)) {
            case 5:
                $window2 = mb_strtolower($value[0]);
                $window2 = str_replace('fw', 'ps', $window2);
                $window2 = str_replace('fv', 'pf', $window2);
                $window2 = str_replace('fd', 'pb', $window2);
                $window2 = str_replace('rd', 'zb', $window2);
                $window2 = str_replace('rv', 'zf', $window2);
                $window2 = str_replace('bw', 'zs', $window2);
                return $window2;

            default:
                return '';
        }
    }

    private function _getCloth($article)
    {
        $value = explode('-', $article);
        switch (count($value)) {
            case 5:
                return $value[4];

            default:
                return '';
        }
    }
}