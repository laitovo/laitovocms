<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Класс которые отвечает за распечатку инструкций для биззона.
 *
 * Class CarsSchemeNew
 * @package backend\modules\laitovo\models
 */
class CarsSchemeNew extends Model
{
    /**
     * @var CarsForm $model - модель формы карточки автомобиля
     */
    public $model;

    /**
     * @var string $brand - бренд, к которому относится продукция [Laitovo,Chiko]
     */
    public $brand;

    /**
     * @var string $type - вид исполнения продукта [Стандарт, Укороченный, ВырезДляКурящих, ВырезДляЗеркала, Треугольная, Стандарт 2 части]
     */
    public $type;

    /**
     * @var string $type_clips - вид креплений клипс [Простые, Магнитные]
     */
    public $type_clips;

    /**
     * @var string $window - вид оконного проема [ПШ,ПФ,ПБ,ЗБ,ЗФ,ЗШ]
     */
    public $window;

    /**
     * @var string $im - изображение, на которое будет накладыватся схема.
     */
    private $im;

    public function __construct($config = [])
    {

        parent::__construct($config);
    }

    public function clips($value)
    {
        $name = explode('/', $this->model->clips($value))[0];
        if (is_numeric($name)) {
            return '№' . $name;
        }
        return ' ' . $name;
    }

    public function construct()
    {

        if (!$this->model)
            return false;
        $schemimg=[];
        $map=[];
        $scheme=$this->model->auto->getSchemes()->andWhere(['brand'=>$this->brand,'type'=>$this->type,'type_clips'=>$this->type_clips,'window'=>$this->window])->one();
        if ($scheme){
            $map=Json::decode($scheme->json)['scheme'];
        }

        $s1=40;
        $s2=30;
        $s3=10;
        $s4=$s1-5;

        if ($this->window=='ПФ'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/fv_scheme.jpg');
            $s1=55;
            $s2=40;
            $s3=12;
            $s4=47;

            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[13*1882/100-5,43*1345/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[23*1882/100-5,28*1345/100]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[35*1882/100+5,18*1345/100]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[44*1882/100+5,26*1345/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[41*1882/100,36*1345/100+10]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[38*1882/100-10,46*1345/100+5]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[25*1882/100,56*1345/100]] : '';

            }
        }
        if ($this->window=='ПБ' && $this->type=='Стандарт'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/fd_scheme.jpg');
            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[22*1051/100,37*753/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[22*1051/100-3,30*753/100]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[21*1051/100+3,24*753/100]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[33*1051/100,16*753/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[50*1051/100+3,19*753/100]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[65*1051/100+5,27*753/100]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[75*1051/100,41*753/100]] : '';
                $map[7] ? $schemimg[]=['name'=>$this->clips($map[7]), 'coordinats'=>[60*1051/100,45*753/100]] : '';
                $map[8] ? $schemimg[]=['name'=>$this->clips($map[8]), 'coordinats'=>[44*1051/100,45*753/100]] : '';
                $map[9] ? $schemimg[]=['name'=>$this->clips($map[9]), 'coordinats'=>[30*1051/100,45*753/100]] : '';

            }
        }
        if ($this->window=='ПБ' && $this->type=='Укороченный'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/fd_u_scheme.jpg');

            $s1=55;
            $s2=40;
            $s3=13;
            $s4=42;

            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[24*1662/100,35*1169/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[23*1662/100+5,27*1169/100]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[23*1662/100,19*1169/100+5]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[35*1662/100+5,16*1169/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[48*1662/100-5,18*1169/100]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[48*1662/100,40*1169/100+5]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[35*1662/100+8,40*1169/100+5]] : '';

            }
        }
        if ($this->window=='ПБ' && $this->type=='ВырезДляКурящих'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/fd_k_scheme.jpg');

            $s1=55;
            $s2=40;
            $s3=13;
            $s4=42;

            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[24*1662/100,35*1169/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[23*1662/100+5,27*1169/100]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[23*1662/100,19*1169/100+5]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[35*1662/100+5,16*1169/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[48*1662/100-5,18*1169/100]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[72*1662/100,36*1169/100+5]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[59*1662/100,40*1169/100+5]] : '';
                $map[7] ? $schemimg[]=['name'=>$this->clips($map[7]), 'coordinats'=>[48*1662/100,40*1169/100+5]] : '';
                $map[8] ? $schemimg[]=['name'=>$this->clips($map[8]), 'coordinats'=>[36*1662/100-5,40*1169/100+5]] : '';

            }
        }
        if ($this->window=='ПБ' && $this->type=='ВырезДляЗеркала'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/fd_z_scheme.jpg');

            $s1=55;
            $s2=40;
            $s3=13;
            $s4=42;

            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[24*1662/100,35*1169/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[23*1662/100+5,27*1169/100]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[23*1662/100,19*1169/100+5]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[35*1662/100+5,16*1169/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[48*1662/100-5,18*1169/100]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[59*1662/100,22*1169/100-5]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[48*1662/100,40*1169/100+5]] : '';
                $map[7] ? $schemimg[]=['name'=>$this->clips($map[7]), 'coordinats'=>[36*1662/100-5,40*1169/100+5]] : '';

            }
        }
        if ($this->window=='ЗБ'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/rd_scheme.jpg');
            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[23*1060/100,38*759/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[23*1060/100-5,33*759/100+5]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[23*1060/100-10,29*759/100+3]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[35*1060/100-8,19*759/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[48*1060/100-5,14*759/100+5]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[60*1060/100+5,13*759/100+3]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[72*1060/100,21*759/100+3]] : '';
                $map[7] ? $schemimg[]=['name'=>$this->clips($map[7]), 'coordinats'=>[72*1060/100,27*759/100+8]] : '';
                $map[8] ? $schemimg[]=['name'=>$this->clips($map[8]), 'coordinats'=>[72*1060/100+3,33*759/100+8]] : '';
                $map[9] ? $schemimg[]=['name'=>$this->clips($map[9]), 'coordinats'=>[63*1060/100+10,41*759/100]] : '';
                $map[10] ? $schemimg[]=['name'=>$this->clips($map[10]), 'coordinats'=>[50*1060/100-5,41*759/100]] : '';
                $map[11] ? $schemimg[]=['name'=>$this->clips($map[11]), 'coordinats'=>[35*1060/100-10,41*759/100]] : '';

            }
        }
        if ($this->window=='ЗФ' && $this->type=='Стандарт'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/rv_scheme.jpg');

            $s1=45;
            $s2=30;
            $s3=10;
            $s4=35;

            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[25*1905/100+5,47*1361/100+5]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[28*1905/100+5,39*1361/100+3]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[31*1905/100+5,32*1361/100]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[40*1905/100,26*1361/100+5]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[54*1905/100+5,25*1361/100+10]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[68*1905/100+5,25*1361/100+10]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[77*1905/100+10,31*1361/100]] : '';
                $map[7] ? $schemimg[]=['name'=>$this->clips($map[7]), 'coordinats'=>[76*1905/100,40*1361/100]] : '';
                $map[8] ? $schemimg[]=['name'=>$this->clips($map[8]), 'coordinats'=>[74*1905/100+5,48*1361/100+5]] : '';
                $map[9] ? $schemimg[]=['name'=>$this->clips($map[9]), 'coordinats'=>[64*1905/100+10,55*1361/100]] : '';
                $map[10] ? $schemimg[]=['name'=>$this->clips($map[10]), 'coordinats'=>[51*1905/100,55*1361/100]] : '';
                $map[11] ? $schemimg[]=['name'=>$this->clips($map[11]), 'coordinats'=>[36*1905/100,55*1361/100]] : '';

            }
        }
        if ($this->window=='ЗФ' && $this->type=='Треугольная'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/rv_t_scheme.jpg');
            $s1=55;
            $s2=40;
            $s3=12;
            $s4=47;

            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[13*1882/100-5,43*1345/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[23*1882/100-5,28*1345/100]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[35*1882/100+5,18*1345/100]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[44*1882/100+5,26*1345/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[41*1882/100,36*1345/100+10]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[38*1882/100-10,46*1345/100+5]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[25*1882/100,56*1345/100]] : '';

            }
        }
        if ($this->window=='ЗШ' && $this->type=='Стандарт'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/bw_scheme.jpg');

            $s1=65;
            $s2=40;
            $s3=17;
            $s4=47;

            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[17*1660/100,35*1170/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[16*1660/100-5,27*1170/100]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[25*1660/100,17*1170/100-5]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[36*1660/100+5,16*1170/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[49*1660/100,16*1170/100+3]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[62*1660/100,16*1170/100]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[73*1660/100,16*1170/100+10]] : '';
                $map[7] ? $schemimg[]=['name'=>$this->clips($map[7]), 'coordinats'=>[82*1660/100,26*1170/100+10]] : '';
                $map[8] ? $schemimg[]=['name'=>$this->clips($map[8]), 'coordinats'=>[81*1660/100+5,35*1170/100]] : '';
                $map[9] ? $schemimg[]=['name'=>$this->clips($map[9]), 'coordinats'=>[73*1660/100,39*1170/100+5]] : '';
                $map[10] ? $schemimg[]=['name'=>$this->clips($map[10]), 'coordinats'=>[62*1660/100,39*1170/100]] : '';
                $map[11] ? $schemimg[]=['name'=>$this->clips($map[11]), 'coordinats'=>[49*1660/100-5,39*1170/100]] : '';
                $map[12] ? $schemimg[]=['name'=>$this->clips($map[12]), 'coordinats'=>[36*1660/100,39*1170/100+5]] : '';
                $map[13] ? $schemimg[]=['name'=>$this->clips($map[13]), 'coordinats'=>[25*1660/100,39*1170/100+5]] : '';

            }
        }
        if ($this->window=='ЗШ' && $this->type=='Стандарт 2 части'){
            $im = imagecreatefromjpeg(Yii::getAlias('@webroot').'/img/bw_2_scheme.jpg');
            if ($scheme) {

                $map[0] ? $schemimg[]=['name'=>$this->clips($map[0]), 'coordinats'=>[16*1120/100-5,58*796/100]] : '';
                $map[1] ? $schemimg[]=['name'=>$this->clips($map[1]), 'coordinats'=>[16*1120/100,48*796/100]] : '';
                $map[2] ? $schemimg[]=['name'=>$this->clips($map[2]), 'coordinats'=>[17*1120/100-5,39*796/100]] : '';
                $map[3] ? $schemimg[]=['name'=>$this->clips($map[3]), 'coordinats'=>[24*1120/100-5,33*796/100]] : '';
                $map[4] ? $schemimg[]=['name'=>$this->clips($map[4]), 'coordinats'=>[30*1120/100,33*796/100]] : '';
                $map[5] ? $schemimg[]=['name'=>$this->clips($map[5]), 'coordinats'=>[37*1120/100,33*796/100]] : '';
                $map[6] ? $schemimg[]=['name'=>$this->clips($map[6]), 'coordinats'=>[43*1120/100,39*796/100]] : '';
                $map[7] ? $schemimg[]=['name'=>$this->clips($map[7]), 'coordinats'=>[43*1120/100,48*796/100]] : '';
                $map[8] ? $schemimg[]=['name'=>$this->clips($map[8]), 'coordinats'=>[43*1120/100,58*796/100]] : '';
                $map[9] ? $schemimg[]=['name'=>$this->clips($map[9]), 'coordinats'=>[37*1120/100,65*796/100]] : '';
                $map[10] ? $schemimg[]=['name'=>$this->clips($map[10]), 'coordinats'=>[30*1120/100,65*796/100]] : '';
                $map[11] ? $schemimg[]=['name'=>$this->clips($map[11]), 'coordinats'=>[23*1120/100,65*796/100]] : '';
                $map[12] ? $schemimg[]=['name'=>$this->clips($map[12]), 'coordinats'=>[56*1120/100,57*796/100]] : '';
                $map[13] ? $schemimg[]=['name'=>$this->clips($map[13]), 'coordinats'=>[56*1120/100,48*796/100]] : '';
                $map[14] ? $schemimg[]=['name'=>$this->clips($map[14]), 'coordinats'=>[56*1120/100,39*796/100]] : '';
                $map[15] ? $schemimg[]=['name'=>$this->clips($map[15]), 'coordinats'=>[62*1120/100,33*796/100]] : '';
                $map[16] ? $schemimg[]=['name'=>$this->clips($map[16]), 'coordinats'=>[69*1120/100,33*796/100]] : '';
                $map[17] ? $schemimg[]=['name'=>$this->clips($map[17]), 'coordinats'=>[76*1120/100,33*796/100]] : '';
                $map[18] ? $schemimg[]=['name'=>$this->clips($map[18]), 'coordinats'=>[82*1120/100+5,39*796/100]] : '';
                $map[19] ? $schemimg[]=['name'=>$this->clips($map[19]), 'coordinats'=>[83*1120/100,48*796/100]] : '';
                $map[20] ? $schemimg[]=['name'=>$this->clips($map[20]), 'coordinats'=>[83*1120/100+5,58*796/100]] : '';
                $map[21] ? $schemimg[]=['name'=>$this->clips($map[21]), 'coordinats'=>[76*1120/100,65*796/100]] : '';
                $map[22] ? $schemimg[]=['name'=>$this->clips($map[22]), 'coordinats'=>[69*1120/100,65*796/100]] : '';
                $map[23] ? $schemimg[]=['name'=>$this->clips($map[23]), 'coordinats'=>[62*1120/100,65*796/100]] : '';

            }
        }
        if (isset($im)){
            $red = imagecolorallocate($im, 0x00, 0x00, 0x00);
            $black = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);
            $font_file = Yii::getAlias('@webroot').'/font/arial.ttf';

            foreach ($schemimg as $key => $value) {

                $x=$value['coordinats'][0]-20;
                $y=$value['coordinats'][1];
                $k1 = 12;
                $k2 = $k1/3;
                imagefilledrectangle($im, $x-$k1, $y-$k1, $x+$s1+40+$k1, $y+$s1+$k1, $red);
                imagefilledrectangle($im, $x-$k2, $y-$k2, $x+$s1+40+$k2, $y+$s1+$k2, $black);
                imagefttext($im, $s2, 0, $x+$s3, $y+$s4, $red, $font_file, $value['name']);

            }

            $this->im=$im;
            return true;
        }
        return false;
    }

    public function render()
    {
        if ($this->construct()){

            header('Content-Type: image/png');

            imagepng($this->im);
            imagedestroy($this->im);

        }

    }

    public function file()
    {
        if ($this->construct()) {
            if (!file_exists(Yii::getAlias('@webroot').'/assets/scheme')) {
                mkdir(Yii::getAlias('@webroot').'/assets/scheme', 0777, true);
            }
            $file=md5($this->model->auto->id.$this->brand.$this->type.$this->type_clips.$this->window).'.png';
            imagepng($this->im,Yii::getAlias('@webroot').'/assets/scheme/'.$file);
            imagedestroy($this->im);
            return '/assets/scheme/'.$file;
        }
    }
}
