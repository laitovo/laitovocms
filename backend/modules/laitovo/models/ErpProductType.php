<?php

namespace backend\modules\laitovo\models;

use Yii;
use common\models\User;
use common\models\laitovo\ProductionScheme;
use yii\helpers\ArrayHelper;
use backend\modules\logistics\models\StorageLiteral;


/**
 * This is the model class for table "laitovo_erp_product_type".
 *
 * @property integer $id
 * @property string $title
 * @property string $rule
 * @property integer $author_id
 * @property integer $created_at
 * @property integer $productionScheme [ Карточка продукта ]
 *
 * @property User $author
 */
class ErpProductType extends \common\models\laitovo\ErpProductType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // обязательные параметры
            [['article', 'title'], 'required'],
            [['author_id', 'created_at', 'production_scheme_id'], 'integer'],
            [['title','title_en','rule', 'article','category'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['production_scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionScheme::className(), 'targetAttribute' => ['production_scheme_id' => 'id']],
     
        ];
    }

    /**
     * @param $location_id
     * @return mixed|null
     */
    public function recieveGroupByLocation($location_id)
    {
        $groups =$this->hasMany(ErpProductGroup::className(), ['id' => 'group_id'])
            ->viaTable('laitovo_erp_product_group_relation', ['product_id' => 'id'])->all();

        if ($groups)
            foreach ($groups as $group) {
                if ($location_id && $group->location_id == $location_id)
                    return $group->id;
            }

        return null;
    }    

    public function isBelongToAGroup($group_id)
    {
        if ($this->groups)
            foreach ($this->groups as $group) {
                if ($group->id == $group_id) return true;
            }
        return false;
    }
    
    /*
     * выделяем артикул согласно правилу
     */
    public function getArticleRule()
    {
        if($this->rule)
        {
            $value = explode('-', $this->rule);
            if($value)
            {
                if (isset($value[0])) preg_match('/\((.+)\)/', $value[0], $oneElement);
                if (isset($value[3])) preg_match('/\((.+)\)/', $value[3], $twoElement);
                if (isset($value[4])) preg_match('/\((.+)\)/', $value[4], $threeElement);
               return (isset($oneElement[1]) ? $oneElement[1] : '/')
                    .'-/-/-'
                    .(isset($twoElement[1]) ? $twoElement[1] : '/')
                    .'-'
                    .(isset($threeElement[1]) ? $threeElement[1] : '/');
            }
        }
        return false;
    }

    static function getProductTypeList()
    {
        $product_types = ErpProductType::find()->all();
        return  ArrayHelper::map($product_types, 'id', 'title');
    }

    public function getLiteralByStorage($storage_id)
    {
        $row = (new \yii\db\Query())
            ->from('logistics_storage_product')
            ->where(['product_id' => $this->id])
            ->andWhere(['storage_id' => $storage_id])
            ->one();

        $literal = StorageLiteral::findOne($row->literal_id);
        return $literal->title;
    }
}
