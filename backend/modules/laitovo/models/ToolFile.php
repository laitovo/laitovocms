<?php
namespace backend\modules\laitovo\models;

use yii\base\Model;
use yii\web\UploadedFile;

class ToolFile extends Model
{
    /**
    * @var UploadedFile
    */
    public $imageFile;
    public $degreeFactor = 1;
    public $lineFactor = 0;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'txt'],
            [['degreeFactor'], 'number', 'min' => 0.7, 'max' => 1.3],
            [['lineFactor'], 'number', 'min' => -10, 'max' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFile' => 'Файл с AdobeViewer с расширением .txt',
            'degreeFactor' => 'Поправочный коэффициент углов (от 0.7 до 1.3)',
            'lineFactor' => 'Поправочная величина (от 10 мм до -10мм)',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            echo '<pre>';
            var_dump($this->imageFile);
            echo '</pre>';
            die;
            $this->imageFile->saveAs('@backend/uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }

    public function readFile()
    {
        if ($this->validate()) {
            $string = file_get_contents($this->imageFile->tempName);
            $array = explode('-------------------', $string);
            $newArray = [];
            foreach ($array as $item) {
                $newItem = [];
                //Получаем номер отрезка
                preg_match('/Отрезок № ([0-9]*).*/', $item, $matches);
                $newItem['number'] = $matches[1];
                //Получаем угол поворота
                preg_match('/Угол = ([0-9,\,]*).*/', $item, $matches);
                $newItem['degree'] = str_replace(',','.',$matches[1]);
                //Получаем длину полилинии и единицы измерения
                preg_match('/Длина полилинии = ([0-9,\,]*)\\s*(\\S*).*/', $item, $matches);
                $newItem['length'] = str_replace(',','.',$matches[1]);
                $newItem['unit'] = $matches[2];
                $newArray[] = $newItem;
            }

            $reverseArray = array_reverse($newArray, true);
            //Пересчет длин отрезков
            $beforeLength = 0;
            foreach ($reverseArray as $key => $item) {
                $length = $item['length'] - $beforeLength + $this->lineFactor;
                $beforeLength = $item['length'];
                $newArray[$key]['length'] = $length;
            }

            $resultString = '';
            foreach ($newArray as $newItem)
            {
                $string = 'Отрезок № ' . $newItem['number'] . '<br>';
                $string .= 'Длина отрезка = ' . number_format( $newItem['length'],2) . ' ' . $newItem['unit'] . '<br>';
                $modAngle = $newItem['degree'] == 0 ? '"Отрезать"' : round(((180 - $newItem['degree']) * $this->degreeFactor),2);
                $string .= 'Угол = ' . $modAngle . '°' . '<br>';
                $string .= '-------------------' . '<br>';
                $resultString .= $string;
            }
//            echo '<pre>';
//            print_r($array);
//            print_r($resultString);
//            echo '</pre>';
//            die;
            return $resultString;
        } else {
            return '';
        }
    }
}