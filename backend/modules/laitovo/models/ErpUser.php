<?php

namespace backend\modules\laitovo\models;

use backend\modules\laitovo\modules\premium\models\Premium;
use common\models\laitovo\Config;
use common\models\laitovo\ErpFine;
use common\models\laitovo\ErpNaryadLog;
use common\models\laitovo\ErpUserPosition;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%laitovo_erp_user}}".
 */
class ErpUser extends \common\models\laitovo\ErpUser
{
    public $window;
    public $windowType;
    public $tkan;
    private $dateFrom;
    private $dateTo;
    public $dismissed_id;


    public function setDateFrom($value)
    {
        $this->dateFrom =strtotime($value);
    }

    public function getDateFrom()
    {
        return $this->dateFrom ?  $this->dateFrom : mktime(0, 0, 0, date('m'), 1);
    }

    public function setDateTo($value)
    {
        $this->dateTo = strtotime($value);
    }

    public function getDateTo()
    {
        return  $this->dateTo? $this->dateTo :  mktime(23, 59, 59);
    }

//    public function setDateWork($value)
//    {
//        $this->date_work = strtotime($value);
//    }
//
//    public function getDateWork()
//    {
//        return  $this->date_work? $this->date_work :  mktime(23, 59, 59);
//    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id'], 'integer'],
//            [['date_dismissed'],'string'],
            [['factor'], 'integer'],
            [['dismissed_id'],'safe'],
            [['json'], 'string'],
            [['name', 'status','type'], 'string', 'max' => 255],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [[  'window', 'windowType', 'tkan', 'dateFrom', 'dateTo' ], 'safe'],
            ['trainee', 'boolean'],
            ['canCreateTemplate', 'boolean'],
            ['canCreateDontlook', 'boolean'],
            ['otherPricing', 'boolean'],
            ];
    }

    public function searchViewNaryads($params)
    {
      $querys = $this->getNaryads()->where(['and',
          ['is not','location_id', null],
          ['start' => null],
          ['or',['status' => ErpNaryad::STATUS_IN_WORK],['status' => ErpNaryad::STATUS_IN_PAUSE]],
      ])->all();
     
      if($this->load($params)){

          if($this->window)
          {
             $querys = $this->filterWindow($querys, $this->window);
          }

          if($this->tkan)   
          {
              $querys = $this->filterTkan($querys, $this->tkan);
          }
              
          if($this->windowType)   
          {
              $querys = $this->filterWindowType($querys, $this->windowType); 
          }
       }
        
      return $dataProvider = new ArrayDataProvider([
            'allModels' => $querys,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'=>[
                'attributes' => ['created_at', 'updated_at'],
            ]
              ]);
    }
    
    
    //фильтр по заданному периоду
    public function filterDate($querys, $dateFrom, $dateTo)
    {
         $q = [];
         $dateFrom = Yii::$app->formatter->asTimestamp($dateFrom);
         $dateTo = Yii::$app->formatter->asTimestamp($dateTo) + (60 * 60 * 24 - 1);
             
               foreach($querys as $query)
               {                  
                   if($query->json)
                   {
                       $jsons = Json::decode($query->json);
                       //проходимся циклом по полю json и ищем дату получения Работником наряда
                        if($jsons['log'])
                        {
                         
                            foreach($jsons['log'] as $json)
                            {
                               
                                if($json['user'] == $this->name) 
                                {
                                      $json['date'];
                                }
                               
                            }
                        }
                   }
                   // если дата удовлетворяет входящим условиям то записываем стрку в новый массив 
                   if($json['date'] > $dateFrom && $json['date']< $dateTo)
                   {
                     $q[] = $query;
                   }
                }
               return $q; 
    }
    
    
   
    //фильтр по оконному проему
    public function filterWindow($querys, $window)
    {
        $q = [];
        foreach($querys as $query)
                 {
                     if($query->article)
                     {
                         $val = explode('-', $query->article);

                             if($val[0]== $window)
                             {
                                 $q[] = $query;
                             }
                     }
                 }
       return $q;
    }
   
    

    //фильтр по ткани
    public function filterTkan($querys, $tkan)
    {
        $q = [];
        foreach($querys as $query)
                 {
                     if($query->article)
                     {
                         $val = explode('-', $query->article);

                             if($val[4]== $tkan)
                             {
                                 $q[] = $query;
                             }
                     }
                 }
       return  $q;
    }
  
 
    
    //фильтр по продукции
    public function filterWindowType($querys, $windowType)
    {
        $q = [];
        foreach($querys as $query)
                 {
                     if($query->article)
                     {
                         $val = explode('-', $query->article);

                             if($val[3]== $windowType)
                             {
                                 $q[] = $query;
                             }
                     }
                 }
      return   $q;
    }
    
    
    static function userInWork()
    {
        $user = ErpUser::find()->where(['and',
            ['status'=>self::STATUS_WORK],
            ['or',
                ['!=', 'location_id', Yii::$app->params['erp_dispatcher']],
                ['location_id'=>null]

            ]])->orderBy('name')
            ->all();
        if(isset($user))
            return ArrayHelper::map($user, 'id', 'name');
    }


    static function userInWorkProduction()
    {
        $user = self::find()->where(['status'=>self::STATUS_WORK, 'type'=>self::TYPE_PRODUCTION])->andWhere(['not in', 'location_id', Yii::$app->params['erp_dispatcher']])->all();
        if(isset($user))
            return ArrayHelper::merge([''=>''], ArrayHelper::map($user, 'id', 'name'));
    }

    static function userInWorkSupport()
    {
        $user = self::find()->where(['status'=>self::STATUS_WORK, 'type'=>self::TYPE_SUPPORT])->andWhere(['not in', 'location_id', Yii::$app->params['erp_dispatcher']])->all();
        if(isset($user))
            return ArrayHelper::merge([''=>''], ArrayHelper::map($user, 'id', 'name'));
    }


    
    
    static function userNameInWork()
    {
        $user = ErpUser::find()->where(['status'=>self::STATUS_WORK])->andWhere(['not in', 'location_id', Yii::$app->params['erp_dispatcher']])->all();
        if(isset($user))
            return ArrayHelper::map($user, 'name', 'name');
    }
    
    
    
    static function dispatcherInWork()
    {
        $user = ErpUser::find()->where(['status'=>self::STATUS_WORK, 'location_id'=>Yii::$app->params['erp_dispatcher']])->all();
        if(isset($user))
            return ArrayHelper::map($user, 'id', 'name');
    }


    static function allUserNameList()
    {
        $user = ErpUser::find()->all();
        if(isset($user))
            return ArrayHelper::map($user, 'id', 'name');
    }


    //heksweb interface
    //return array of objects OR null
    public function getProductionPlansWorkplaces()
    {
        $locations = ErpLocation::getAllProductionIdsAsArray();
        $plans = $this->getWorkplacePlansIdsAsArr();
        return ErpLocationWorkplace::find()
                        ->where(['and',
                            ['in','location_id',$locations],
                            ['in','id',$plans]
                        ])->all();
    }

    //return array of objects OR null
    public function getSupportPlansWorkplaces()
    {
        $locations = ErpLocation::getAllSupportIdsAsArray();
        $plans = $this->getWorkplacePlansIdsAsArr();
        return ErpLocationWorkplace::find()
                        ->where(['and',
                            ['in','location_id',$locations],
                            ['in','id',$plans]
                        ])->all();
    }

    //Возвращает тип последней записи в журнале
    //регистрации среди записей о Приходе и уходе
    public function getTypeOfVisitStatus()
    {
        return ErpLocationWorkplaceRegister::find()->where(['and',
            ['user_id' => $this->id],
            ['or',
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_IN],
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_OUT],
            ],
        ])      
        ->orderBy([
            'register_at'=> SORT_DESC,
            'id' => SORT_DESC
        ])
        ->one();
    }


    //Возвращает тип последней записи в журнале
    //регистрации среди записей о Приходе и уходе
    public function isOnWork()
    {
        $visit = $this->getTypeOfVisitStatus();
        if ($visit && $visit->action_type == ErpLocationWorkplaceRegister::TYPE_IN)
            return true;
        return false;
    }

    //Возвращает тип последней записи в журнале
    //регистрации среди записей о Приходе и уходе
    public function isOutWork()
    {
        $visit = $this->getTypeOfVisitStatus();
        if (!$visit || ($visit && $visit->action_type == ErpLocationWorkplaceRegister::TYPE_OUT))
            return true;
        return false;
    }

    //проверка на то что непроизводственный работник забыл отметить уход возвращает временнную метку регистрации на рабочем месте
    public function isNoOutWorkYesterday()
    {
        $visit = $this->getTypeOfVisitStatus();
        if ($visit && $visit->action_type == ErpLocationWorkplaceRegister::TYPE_IN && $visit->register_at < time()-(60*60*15))
            return $visit->register_at;
        return false;
    }



    //Возвращает тип последней записи в журнале
    //регистрации среди записей о Приходе и уходе
    public function getTypeOfActivityStatus()
    {
        return ErpLocationWorkplaceRegister::find()->where(['and',
            ['user_id' => $this->id],
            ['or',
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_INACTIVE],
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_ACTIVE],
            ],
        ])      
        ->orderBy([
            'register_at'=> SORT_DESC,
            'id' => SORT_DESC
        ])
        ->one();
    }

    public  function getTypeOfActionStatusMoveFrom(){
        return ErpLocationWorkplaceRegister::find()->where(['and',
            ['user_id' => $this->id],
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_MOVE_FROM],
            ])
            ->orderBy([
                'id' => SORT_DESC
            ])
            ->one();
    }


    public  function getTypeOfActionStatusMoveTo(){
        return ErpLocationWorkplaceRegister::find()->where(['and',
            ['user_id' => $this->id],
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_MOVE_TO],
            ])
            ->orderBy([
                'id' => SORT_DESC
            ])
            ->one();
    }

    //Возвращает тип последней записи в журнале
    //регистрации среди записей о Приходе и уходе
    public function getTypeOfActionStatus()
    {
        return ErpLocationWorkplaceRegister::find()->where(['and',
            ['user_id' => $this->id],
            ['or',
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_MOVE_FROM],
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_MOVE_TO],
            ],
        ])      
        ->orderBy([
//            'register_at'=> SORT_DESC,
            'id' => SORT_DESC
        ])
        ->one();
    }

    //Возвращает тип последней записи в журнале
    //регистрации среди записей о Приходе и уходе
    public function getTypeOfCommandStatus()
    {
        return ErpLocationWorkplaceRegister::find()->where(['and',
            ['user_id' => $this->id],
            ['or',
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_SEND_FROM],
                ['action_type' => ErpLocationWorkplaceRegister::TYPE_SEND_TO],
            ],
        ])      
        ->orderBy([
//            'register_at'=> SORT_DESC,
            'id' => SORT_DESC
        ])
        ->one();
    }


    public function hasProductionPlanWorkplaces()
    {
        if ($this->getProductionPlansWorkplaces())
            return true;
    }

    public function hasSupportPlanWorkplaces()
    {
        if ($this->getSupportPlansWorkplaces())
            return true;
    }

    public function initProductionRegistration()
    {
        $workplaces = $this->getProductionPlansWorkplaces();
        if ($workplaces)
            foreach ($workplaces as $workplace) {
                $workplace->user_id = $this->id;
                $workplace->activity = 1;
                $workplace->last_activity = time();
                $workplace->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_TO;
                $register->register_at = time();
                $register->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_ACTIVE;
                $register->register_at = time();
                $register->save();

                $location = $workplace->location_id;    
            }

        $this->location_id = $location;
        return $this->save();
    }

    public function initProductionDeRegistration()
    {
        $workplaces = $this->getProductionWorkplaces();

        if ($workplaces)
            foreach ($workplaces as $workplace) {
                $workplace->user_id = null;
                $workplace->activity = 0;
                $workplace->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                $register->register_at = time();
                $register->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                $register->register_at = time();
                $register->save();
            }
        return true;
    }

    public function initSupportRegistration()
    {
        $location = null;
        $workplaces = $this->getSupportPlansWorkplaces();

        if ($workplaces)
            foreach ($workplaces as $workplace) {
                $workplace->user_id = $this->id;
                $workplace->activity = 1;
                $workplace->last_activity = time();
                $workplace->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_TO;
                $register->register_at = time();
                $register->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_ACTIVE;
                $register->register_at = time();
                $register->save();

                $location = $workplace->location_id;
            }

        if ($location)
            $this->location_id = $location;
        return $this->save();
    }

    public function initSupportDeRegistration()
    {
        $workplaces = $this->getSupportWorkplaces();

        if ($workplaces)
            foreach ($workplaces as $workplace) {
                $workplace->user_id = null;
                $workplace->activity = 0;
                $workplace->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                $register->register_at = time();
                $register->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                $register->register_at = time();
                $register->save();
            }
        return true;
    }



    public function initComeToWork()
    {
        $register = new ErpLocationWorkplaceRegister();
        $register->workplace_id = null;
        $register->user_id = $this->id;
        $register->action_type = ErpLocationWorkplaceRegister::TYPE_IN;
        $register->register_at = time();
        return $register->save();
    }

    public function initComeOutWork()
    {
        $register = new ErpLocationWorkplaceRegister();
        $register->workplace_id = null;
        $register->user_id = $this->id;
        $register->action_type = ErpLocationWorkplaceRegister::TYPE_OUT;
        $register->register_at = time();
        return $register->save();
    }

//снятие с рабочего места для работников забывших отметить уход
///////////////////////////////////////////////////////////////////////



    public function initProductionDeRegistrationYestarday($time)
    {
        $workplaces = $this->getProductionWorkplaces();

        if ($workplaces)
            foreach ($workplaces as $workplace) {
                $workplace->user_id = null;
                $workplace->activity = 0;
                $workplace->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                $register->register_at = $time+10;
                $register->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                $register->register_at = $time+20;
                $register->save();
            }
        return true;
    }


    public function initSupportDeRegistrationYestarday($time)
    {
        $workplaces = $this->getSupportWorkplaces();

        if ($workplaces)
            foreach ($workplaces as $workplace) {
                $workplace->user_id = null;
                $workplace->activity = 0;
                $workplace->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                $register->register_at = $time+30;
                $register->save();

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id;
                $register->user_id = $this->id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                $register->register_at = $time+40;
                $register->save();
            }
        return true;
    }



    public function initComeOutWorkYestarday($time)
    {
        $register = new ErpLocationWorkplaceRegister();
        $register->workplace_id = null;
        $register->user_id = $this->id;
        $register->action_type = ErpLocationWorkplaceRegister::TYPE_OUT;
        $register->register_at = $time+60;
        return $register->save();
    }





    ////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////

    public function getProductionWorkplaces()
    {
        $locations = ErpLocation::getAllProductionIdsAsArray();
        return $workplaces =
                    $this->getWorkplaces()
                        ->where(['and',
                            ['in','location_id',$locations],
                        ])->all();
    }

    public function onProductionWorkplaces()
    {
        if ($this->getProductionWorkplaces())
            return true;
        return false;
    }

    public function getSupportWorkplaces()
    {
        $locations = ErpLocation::getAllSupportIdsAsArray();
        return $workplaces =
                    $this->getWorkplaces()
                        ->where(['and',
                            ['in','location_id',$locations],
                        ])->all();
    }

    public function onSupportWorkplaces()
    {
        if ($this->getSupportWorkplaces())
            return true;
        return false;
    }


    public function hasCommandMoveFrom()
    {
        $regSend = $this->getTypeOfCommandStatus();
        $regMove = $this->getTypeOfActionStatusMoveFrom();

        //если последняя запись о команде снятия и она не выполнена
        if  ($regSend 
            && $regSend->action_type == ErpLocationWorkplaceRegister::TYPE_SEND_FROM
            && (!$regMove 
                || ($regMove 
                   && $regMove->register_at <= $regSend->register_at
                   ) 
               )
            ) return true;
        return false;
    }

    public function hasCommandMoveTo()
    {
        $regSend = $this->getTypeOfCommandStatus();
        $regMove = $this->getTypeOfActionStatusMoveTo();

        if  ($regSend 
            && $regSend->action_type == ErpLocationWorkplaceRegister::TYPE_SEND_TO
            && (!$regMove 
                || ($regMove 
                    && $regMove->register_at <= $regSend->register_at
                   ) 
               )
            ) return true;

        return false;
    }


    public function initProductionReRegistration()
    {
        $location = null;
        $prodCurIds = [];
        $prodPlanIds = [];
        $suppCurIds = [];
        $suppPlanIds = [];

        //Рабочие места
        $prodCurWorkplaces = $this->getProductionWorkplaces();
        $prodPlanWorkplaces = $this->getProductionPlansWorkplaces();

        $suppCurWorkplaces = $this->getSupportWorkplaces();
        $suppPlanWorkplaces = $this->getSupportPlansWorkplaces();

        //собираем массивы айдишников
        if ($prodCurWorkplaces) $prodCurIds = ArrayHelper::map($prodCurWorkplaces, 'id', 'id');
        if ($prodPlanWorkplaces) $prodPlanIds = ArrayHelper::map($prodPlanWorkplaces, 'id', 'id');

        if ($suppCurWorkplaces) $suppCurIds = ArrayHelper::map($suppCurWorkplaces, 'id', 'id');
        if ($suppPlanWorkplaces) $suppPlanIds = ArrayHelper::map($suppPlanWorkplaces, 'id', 'id');

        //если для сотрудника есть запланированные рабочие места
        if (count($prodPlanWorkplaces))
        {
            //снятие
            if ($prodCurWorkplaces)
                foreach ($prodCurWorkplaces as $curWorkplace) {
                    if (!in_array($curWorkplace->id, $prodPlanIds)) {
                        $curWorkplace->user_id = null;
                        $curWorkplace->activity = 0;
                        $curWorkplace->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $curWorkplace->id;
                        $register->user_id = $this->id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                        $register->register_at = time();
                        $register->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $curWorkplace->id;
                        $register->user_id = $this->id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                        $register->register_at = time();
                        $register->save();
                    }
                }

            //снятие с непроизводственных
            if ($suppCurWorkplaces)
                foreach ($suppCurWorkplaces as $supportPlace) {
                    $supportPlace->user_id = null;
                    $supportPlace->activity = 0;
                    $supportPlace->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $supportPlace->id;
                    $register->user_id = $this->id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                    $register->register_at = time();
                    $register->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $supportPlace->id;
                    $register->user_id = $this->id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                    $register->register_at = time();
                    $register->save();
                }


            //постановка
            if ($prodPlanWorkplaces) {
                foreach ($prodPlanWorkplaces as $planWorkplace) {
                    if (!in_array($planWorkplace->id, $prodCurIds)) {
                        $planWorkplace->user_id = $this->id;
                        $planWorkplace->activity = 1;
                        $planWorkplace->last_activity = time();
                        $planWorkplace->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $planWorkplace->id;
                        $register->user_id = $this->id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_TO;
                        $register->register_at = time();
                        $register->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $planWorkplace->id;
                        $register->user_id = $this->id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_ACTIVE;
                        $register->register_at = time();
                        $register->save();

                        $location = $planWorkplace->location_id;
                    }
                }

                if ($location) {
                    $this->location_id = $location;
                    $this->save();
                }
            }
        }



        if(!count($prodPlanWorkplaces) && count($suppPlanWorkplaces))
        {
            //снятие
            if ($prodCurWorkplaces)
                foreach ($prodCurWorkplaces as $curWorkplace) {
                    $curWorkplace->user_id = null;
                    $curWorkplace->activity = 0;
                    $curWorkplace->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $curWorkplace->id;
                    $register->user_id = $this->id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                    $register->register_at = time();
                    $register->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $curWorkplace->id;
                    $register->user_id = $this->id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                    $register->register_at = time();
                    $register->save();
                }

            if ($suppCurWorkplaces)
                foreach ($suppCurWorkplaces as $curWorkplace) {
                    if (!in_array($curWorkplace->id, $suppPlanIds)) {
                        $curWorkplace->user_id = null;
                        $curWorkplace->activity = 0;
                        $curWorkplace->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $curWorkplace->id;
                        $register->user_id = $this->id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                        $register->register_at = time();
                        $register->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $curWorkplace->id;
                        $register->user_id = $this->id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                        $register->register_at = time();
                        $register->save();
                    }
                }

            // постановка
            if($suppPlanWorkplaces){
                foreach ($suppPlanWorkplaces as $supportPlan)
                {
                    if (!in_array($supportPlan->id, $suppCurIds)) {
                        $supportPlan->user_id = $this->id;
                        $supportPlan->activity = 1;
                        $supportPlan->last_activity = time();
                        $supportPlan->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $supportPlan->id;
                        $register->user_id = $this->id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_TO;
                        $register->register_at = time();
                        $register->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $supportPlan->id;
                        $register->user_id = $this->id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_ACTIVE;
                        $register->register_at = time();
                        $register->save();

                        $location = $supportPlan->location_id;
                    }
                }

                if ($location) {
                    $this->location_id = $location;
                    $this->save();
                }
            }
        }
        
        //сдесь делаем проверку, есть ли на работнике рабочие места и снимаем для него локацию если нет рабочих мест
        if (!$this->onProductionWorkplaces() && !$this->onSupportWorkplaces()){
            $this->location_id = null;
            $this->save();
        }
    }


    public function initSupportReRegistration()
    {
        $curIds = [];
        $planIds = [];

        $curWorkplaces = $this->getSupportWorkplaces();
        $planWorkplaces = $this->getSupportPlansWorkplaces();

        if ($curWorkplaces) $curIds = ArrayHelper::map($curWorkplaces,'id','id');
        if ($planWorkplaces) $planIds = ArrayHelper::map($planWorkplaces,'id','id');

        //снятие
        if ($curWorkplaces)
            foreach ($curWorkplaces as $curWorkplace) {
                if (!in_array($curWorkplace->id, $planIds)){
                    $curWorkplace->user_id = null;
                    $curWorkplace->activity = 0;
                    $curWorkplace->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $curWorkplace->id;
                    $register->user_id = $this->id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                    $register->register_at = time();
                    $register->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $curWorkplace->id;
                    $register->user_id = $this->id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                    $register->register_at = time();
                    $register->save();
                }
            }

        //постановка
        if ($planWorkplaces){
            foreach ($planWorkplaces as $planWorkplace) {
                if (!in_array($planWorkplace->id, $curIds)){
                    $planWorkplace->user_id = $this->id;
                    $planWorkplace->activity = 1;
                    $planWorkplace->last_activity = time();
                    $planWorkplace->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $planWorkplace->id;
                    $register->user_id = $this->id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_TO;
                    $register->register_at = time();
                    $register->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $planWorkplace->id;
                    $register->user_id = $this->id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_ACTIVE;
                    $register->register_at = time();
                    $register->save();

                    $location = $planWorkplace->location_id;
                }
            }

            $this->location_id = $location;
            $this->save();
        }


        //сдесь делаем проверку, есть ли на работнике рабочие места и снимаем для него локацию если нет рабочих мест
        if (!$this->onProductionWorkplaces() && !$this->onSupportWorkplaces()){
            $this->location_id = null;
            $this->save();
        }
    }

    public function hasNaryadsOnHands()
    {
        if ($this->getNaryads()->where(['and',
                ['location_id' => $this->location_id],
                ['status' => ErpNaryad::STATUS_IN_WORK],
            ])->one())
            return true;
        return false;           
    }

    public function hasLowNaryadsOnHands()
    {
        $count =  $this->getNaryads()->where(['and',
                ['location_id' => $this->location_id],
                ['status' => ErpNaryad::STATUS_IN_WORK],
            ])->count();

        return $count <= 2;
    }

    /**
     * @return bool
     */
    public function hasNaryadsOnHandsOld()
    {
        if ($this->getNaryads()->where(['and',
                ['start' => null],
                ['is not','location_id',null],
                ['status' => ErpNaryad::STATUS_IN_WORK],
                ['<','updated_at',time() - 60*60*24],
            ])->one())
            return true;
        return false;
    }


    public function getUserSupportWorkplaces()
    {
        $locations = ErpLocation::getAllSupportIdsAsArray();
        return $workplaces =
                    $this->getWorkplaces()
                        ->where(['and',
                            ['in','location_id',$locations],
                            ['user_id'=>$this->id]
                        ])->all();
    }


    public function getUserSupportPlansWorkplaces()
    {
        $locations = ErpLocation::getAllSupportIdsAsArray();
        return $workplaces =
                        ErpLocationWorkplace::find()
                        ->where(['and',
                            ['in','location_id',$locations],
                            ['user_id'=>$this->id]
                        ])->all();
    }


    public function getNaryadSum()
    {
        $sum_naryad = ErpUsersReport::find()->where(['and',
            ['user_id'=>$this->id],
            ['>', 'created_at', $this->getDateFrom()],
            ['<', 'created_at', $this->getDateTo()],
        ])->sum('job_price');

        return $sum_naryad ? round($sum_naryad,2) : 0;
    }

    public function getNaryadSumToday()
    {
        $averageRate = Config::findOne(2)->json('averageRate') ?: 0;
        $normal = Config::findOne(2)->json('hourrate')?:0 ;

        $sum1 =  ErpUsersReport::find()
            ->select(new Expression('SUM('. $averageRate .' / job_rate * job_count * ' . $normal . ')'))
            ->where(['and',
                ['user_id' => $this->id],
                ['is not', 'naryad_id', null],
                ['>=', 'created_at', strtotime("today")],
            ])
            ->groupBy(['user_id'])
            ->scalar() ?: 0;

        $sum2 = Premium::find()->where(['and',
            ['is_paid'=>true],
            ['user_id' => $this->id],
            ['>=', 'created_at', strtotime("today")],
        ])->sum('amount') ?: 0;

        return $sum1 + $sum2;
    }

    public function getNaryadSumMonth()
    {
        $firstDay = mktime(0, 0, 0, date("m"), 1, date("y"));
        $secondDay = mktime(23, 59, 59, date("m"), date("t"), date("y"));

        $averageRate = Config::findOne(2)->json('averageRate') ?: 0;
        $normal = Config::findOne(2)->json('hourrate')?:0 ;

        $sum1 =  ErpUsersReport::find()
            ->select(new Expression('SUM('. $averageRate .' / job_rate * job_count * ' . $normal . ')'))
            ->where(['and',
                ['user_id' => $this->id],
                ['>=', 'created_at', $firstDay],
                ['<=', 'created_at', $secondDay],
            ])
            ->groupBy(['user_id'])
            ->scalar() ?: 0;

        $sum2 = Premium::find()->where(['and',
            ['is_paid'=>true],
            ['user_id' => $this->id],
            ['>=', 'created_at', $firstDay],
            ['<=', 'created_at', $secondDay],
        ])->sum('amount') ?: 0;

        return $sum1 + $sum2;
    }
    public function getAddSumMonth()
    {
        $firstDay = mktime(0, 0, 0, date("m"), 1, date("y"));
        $secondDay = mktime(23, 59, 59, date("m"), date("t"), date("y"));

        $averageRate = Config::findOne(2)->json('averageRate') ?: 0;
        $normal = Config::findOne(2)->json('hourrate')?:0 ;

        return ErpUsersReport::find()
            ->select(new Expression('SUM('. $averageRate .' / job_rate * job_count * ' . $normal . ')'))
            ->where(['and',
                ['user_id' => $this->id],
                ['is not', 'additional_naryad_id', null],
                ['>=', 'created_at', $firstDay],
                ['<=', 'created_at', $secondDay],
            ])
            ->groupBy(['user_id'])
            ->scalar() ?: 0;
    }

    public function getFineSumMonth()
    {
        $firstDay = mktime(0, 0, 0, date("m"), 1, date("y"));
        $secondDay = mktime(23, 59, 59, date("m"), date("t"), date("y"));
        return ErpFine::find()->where(['and',
            ['user_id' => $this->id],
            ['>=', 'created_at', $firstDay],
            ['<=', 'created_at', $secondDay],
        ])->sum('amount') ?: 0;
    }

    public function getNaryadCountToday()
    {
        $dateFrom = strtotime("today");
        $dateTo   = strtotime("tomorrow") - 1;
        return  ErpNaryadLog::find()
            ->select('workOrderId')
            ->where(['!=','locationFromId',Yii::$app->params['erp_dispatcher']])
            ->andWhere(['workerId' => $this->id])
            ->andWhere(['>=','date',$dateFrom])
            ->andWhere(['<=','date',$dateTo])
            ->count();
    }



    public function getReworkSum()
    {
        $sum_rework = LaitovoReworkAct::find()->where(['and',
            ['made_user_id'=>$this->id],
            ['>', 'laitovo_rework_act.created_at', $this->getDateFrom()],
            ['<', 'laitovo_rework_act.created_at', $this->getDateTo()],
        ])->sum('deduction');

        return $sum_rework ? round($sum_rework,2) : 0;
    }

    public function getReworkSumMonth()
    {
        $firstDay = mktime(0, 0, 0, date("m"), 1, date("y"));
        $secondDay = mktime(23, 59, 59, date("m"), date("t"), date("y"));

        return  LaitovoReworkAct::find()->where(['and',
            ['made_user_id'=>$this->id],
            ['>=', 'laitovo_rework_act.created_at', $firstDay],
            ['<=', 'laitovo_rework_act.created_at', $secondDay],
        ])->sum('deduction') ?: 0;
    }

    public function getPremiumSum()
    {
        $sum_rework = Premium::find()->where(['and',
            ['is_paid'=>true],
            ['user_id'=>$this->id],
            ['>', 'created_at', $this->getDateFrom()],
            ['<', 'created_at', $this->getDateTo()],
        ])->sum('amount');

        return $sum_rework ? round($sum_rework,2) : 0;
    }

    public function getTotalSum()
    {
        $total_sum = $this->getNaryadSum()-$this->getReworkSum()+$this->getPremiumSum();
        return $total_sum? round($total_sum,2) : 0;
    }

    public function getNaryadCount()
    {
        $count_naryad = ErpUsersReport::find()->where(['and',
            ['user_id'=>$this->id],
            ['>', 'created_at', $this->getDateFrom()],
            ['<', 'created_at', $this->getDateTo()],
        ])->count('naryad_id');
        return $count_naryad ? $count_naryad :0;
    }


    static function userListWorkNotManning()
    {
        $manning_users = ArrayHelper::map(ErpUserPosition::find()->all(),'user_id','user_id');
        if($manning_users)
            return ArrayHelper::merge([''=>''],ArrayHelper::map(self::find()->where(['and',['status'=>ErpUser::STATUS_WORK],['not in','id', $manning_users]])->all(), 'id', 'name'));
        return ArrayHelper::merge([''=>''],ArrayHelper::map(self::find()->where(['status'=>ErpUser::STATUS_WORK])->all(), 'id', 'name'));

    }


    static function userListWork()
    {
        return ArrayHelper::merge([''=>''],ArrayHelper::map(self::find()->where(['status'=>ErpUser::STATUS_WORK])->all(), 'id', 'name'));
    }


    public function dismissed()
    {
        if($this->dismissed_id && $this->date_dismissed)
        {
            $user_dismissed = self::findOne($this->dismissed_id);
            $user_dismissed->status = self::STATUS_DISMISSED;
            $user_dismissed->date_dismissed = $this->date_dismissed;
            if($user_dismissed->save())return true;
        }
    }

    public function getUserProductionPlanWorkplaces(){
        $locations = ErpLocation::getAllProductionIdsAsArray();
        return ArrayHelper::map(
            $this->getWorkplacePlans()->joinWith('workplace w')->where(['in','w.location_id',$locations])->all(),
            'workplace_id','workplace_id');

    }


    public function getUserPlanWorkplaceLocation(){
        return ArrayHelper::map(ErpLocationWorkplace::find()->where(['in','id', $this->getUserProductionPlanWorkplaces()])->all(),'location_id','location_id');
    }


    public function locationPlanInLocation(){
        $plan_locations = $this->getUserPlanWorkplaceLocation();
        if(count($plan_locations)){
            foreach ($plan_locations as $location)
            {
                if($location!=$this->location_id)
                    return false;
            }
        }else{
            return false;
        }
        return true;
    }

    /**
     * Выдача по лекалу для дениса (Временно постоянная фигня)
     * @return array
     */
    public static function getAllWorkersLekalo($location_id)
    {
        if (!$location_id)
            return [];

        $users = self::find()
            ->alias('t')
            ->joinWith('location location')
            ->where(['t.status' => self::STATUS_WORK])
            ->andWhere(['location.type' => \backend\modules\laitovo\models\ErpLocation::TYPE_PRODUCTION])
            ->andWhere(['location.id' => $location_id])
            ->all();

        $list = [];

        foreach ($users as $user) {
            if ($user->isOnWork()) {
                $list[] = $user;
            }
        }

        return $list;
    }

    /**
     * Выдача по лекалу для дениса (Временно постоянная фигня)
     * @return array
     */
    public static function getAllWorkersLekaloFull()
    {
        $users = self::find()
            ->alias('t')
            ->joinWith('location location')
            ->joinWith('location location')
            ->where(['t.status' => self::STATUS_WORK])
            ->andWhere(['location.type' => \backend\modules\laitovo\models\ErpLocation::TYPE_PRODUCTION])
            ->andWhere(['location.id' => Yii::$app->params['erp_izgib']])
            ->all();

        return $users;
    }

    /**
     * Выдача по литере для дениса (Временно постоянная фигня)
     * @return array
     */
    public static function getAllWorkersLiteral()
    {
        $users = self::find()
            ->alias('t')
            ->joinWith('location location')
            ->joinWith('location location')
            ->where(['t.status' => self::STATUS_WORK])
            ->andWhere(['location.type' => \backend\modules\laitovo\models\ErpLocation::TYPE_PRODUCTION])
            ->andWhere(['location.id' => Yii::$app->params['erp_shveika']])
            ->all();

        $list = [];

        foreach ($users as $user) {
            if ($user->isOnWork()) {
                $list[] = $user;
            }
        }

        return $list;
    }
}