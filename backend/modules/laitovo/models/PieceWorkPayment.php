<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 7/26/17
 * Time: 8:56 AM
 */

namespace backend\modules\laitovo\models;


use backend\modules\laitovo\modules\premium\models\Premium;
use common\models\laitovo\ErpFine;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\laitovo\Config;
use yii\data\ArrayDataProvider;

class PieceWorkPayment extends Model
{
    private $dateFrom1;
    private $dateTo1;
    private $dismissed;
    private $nullPosition;

    public $rework_naryad_id;
    public $rework_article;
    public $rework_deduction;
    public $rework_created_at;
    public $rework_window;
    public $rework_tkan;
    public $rework_product_type;
    public $rework_location_id;

    public $window;
    public $tkan;
    public $naryad_id;
    public $article;
    public $product_type;
    public $naryad_sum;
    public $created_at;


    public function rules()
    {
        return [
            [['dateFrom1', 'dateTo1'], 'required'],
            [['dismissed','nullPosition'], 'integer'],
            [['rework_window','rework_tkan', 'window', 'tkan'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'dateFrom1' => 'Начало периода *(00:00:00)',
            'dateTo1' => 'Конец периода * (23:59:59)',
            'dismissed' => 'Не отображать уволеных',
            'nullPosition' => 'Не отображать нулевые позиции',
        ];
    }


    public function setDateFrom1($value)
    {
        $this->dateFrom1 =strtotime($value);
    }

    public function getDateFrom1()
    {
        return $this->dateFrom1 ?  $this->dateFrom1 : mktime(0, 0, 0, date('m'), 1);
    }

    public function setDateTo1($value)
    {
        $this->dateTo1 = strtotime($value)+(3600*23+60*59+59);
    }

    public function getDateTo1()
    {
        return  $this->dateTo1? $this->dateTo1 :  mktime(23, 59, 59);
    }

    public function getDismissed()
    {
        return !isset($this->dismissed) ? true : $this->dismissed;

    }
    public function setDismissed($value)
    {
        $this->dismissed = $value;
    }
    public function getNullPosition()
    {
        return !isset($this->nullPosition) ? true : $this->nullPosition;

    }
    public function setNullPosition($value)
    {
        $this->nullPosition = $value;
    }
    public function getDateStr(){
        return date('d-m-Y', $this->getDateFrom1());
    }


    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * собираем главный отчет
     */

    public function generalReport($type)
    {

        $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
        //             средняя дневная норма                цена нормированного коэффициента
        if($config && $config->json('averageRate') && $config->json('hourrate')) {


            $users = ErpUser::find()->where(['type' =>$type])->orderBy('id DESC')->all();

            if ($this->getDismissed()) {
                $users = ErpUser::find()->where(['and',
                    ['type' => $type],
                    ['!=', 'status', ErpUser::STATUS_DISMISSED]
                ])->orderBy('id DESC')->all();
            }

            $addReports = (new \yii\db\Query())
                ->select([
                    'user_id',
                    'SUM("' . $config->json('averageRate') . '"/report.job_rate*report.job_count*"' . $config->json('hourrate') . '") as naryad_sum'])
                ->from('laitovo_erp_users_report as report')
                ->where(['and',
                    ['>=', 'report.created_at', $this->getDateFrom1()],
                    ['<=', 'report.created_at', $this->getDateTo1()],
                ])
                ->andWhere(['is not', 'additional_naryad_id', null])
                ->groupBy(['user_id'])
                ->all();

            $reports = (new \yii\db\Query())
                ->select([
                    'user_id',
                    'SUM("' . $config->json('averageRate') . '"/report.job_rate*report.job_count*"' . $config->json('hourrate') . '") as naryad_sum'])
                ->from('laitovo_erp_users_report as report')
                ->where(['and',
                    ['>=', 'report.created_at', $this->getDateFrom1()],
                    ['<=', 'report.created_at', $this->getDateTo1()],
                ])
                ->groupBy(['user_id'])
                ->all();

            $reworks = (new \yii\db\Query())
                ->select([
                    'made_user_id',
                    'SUM(rework.deduction) as deduction'])
                ->from('laitovo_rework_act as rework')
                ->where(['and',
                    ['>=', 'rework.created_at', $this->getDateFrom1()],
                    ['<=', 'rework.created_at', $this->getDateTo1()],
                ])
                ->groupBy(['made_user_id'])
                ->all();

            $fines = (new \yii\db\Query())
                ->select([
                    'user_id',
                    'SUM(fine.amount) as amount'])
                ->from('laitovo_erp_fine as fine')
                ->where(['and',
                    ['>=', 'fine.created_at', $this->getDateFrom1()],
                    ['<=', 'fine.created_at', $this->getDateTo1()],
                ])
                ->groupBy(['user_id'])
                ->all();

            $premiums = (new \yii\db\Query())
                ->select([
                    'user_id',
                    'SUM(premium.amount) as amount'])
                ->from('laitovo_erp_premium as premium')
                ->where(['and',
                    ['is_paid' => true],
                    ['>=', 'premium.created_at', $this->getDateFrom1()],
                    ['<=', 'premium.created_at', $this->getDateTo1()],
                ])
                ->groupBy(['user_id'])
                ->all();

            $arrayReport = $this->createArrayGeneralReport($users, $reports, $reworks, $fines, $premiums, $addReports);
            return $arrayReport;
        }
    }

    //собираем массив для главного отчета
    public function createArrayGeneralReport($users=[], $reports = [], $reworks = [], $fines=[], $premiums = [], $addReports = [])
    {
        $arrayData = [];
        $sum_naryad = 0;
        $sum_add_naryad = 0;
        $sum_deduction = 0;
        $sum_total = 0;
        $sum_fines = 0;
        $sum_premiums = 0;
        if (isset($users) && count($users)!=0) {
            foreach ($users as $user) {
                $arrayData[$user->id]['id'] = $user->id;
                $arrayData[$user->id]['name'] = $user->getName();
                $arrayData[$user->id]['naryad_sum'] = 0;
                $arrayData[$user->id]['add_naryad_sum'] = 0;
                $arrayData[$user->id]['deduction'] = 0;
                $arrayData[$user->id]['fine'] = 0;
                $arrayData[$user->id]['premium'] = 0;

                foreach ($reports as $report) {
                    if (isset($report['user_id']) && $report['user_id'] == $user->id) {
                        if (isset($report['naryad_sum'])) {
                            $arrayData[$user->id]['naryad_sum'] = round($report['naryad_sum'], 2);
                            break;
                        }
                    }
                }

                foreach ($addReports as $addReport) {
                    if (isset($addReport['user_id']) && $addReport['user_id'] == $user->id) {
                        if (isset($addReport['naryad_sum'])) {
                            $arrayData[$user->id]['add_naryad_sum'] = round($addReport['naryad_sum'], 2);
                            break;
                        }
                    }
                }

                foreach ($reworks as $rework) {
                    if (isset($rework['made_user_id']) && $rework['made_user_id'] == $user->id) {
                        if (isset($rework['deduction'])) {
                            $arrayData[$user->id]['deduction'] = round($rework['deduction'], 2);
                            break;
                        }
                    }
                }

                foreach ($fines as $fine){
                    if (isset($fine['user_id']) && $fine['user_id'] == $user->id){
                        if (isset($fine['amount'])) {
                            $arrayData[$user->id]['fine'] = round($fine['amount'], 2);
                            break;
                        }
                    }
                }

                foreach ($premiums as $premium){
                    if (isset($premium['user_id']) && $premium['user_id'] == $user->id){
                        if (isset($premium['amount'])) {
                            $arrayData[$user->id]['premium'] = round($premium['amount'], 2);
                            break;
                        }
                    }
                }



                $arrayData[$user->id]['total_sum'] = round($arrayData[$user->id]['naryad_sum'] - $arrayData[$user->id]['deduction']-$arrayData[$user->id]['fine'] + $arrayData[$user->id]['premium'], 2);
                $sum_naryad += $arrayData[$user->id]['naryad_sum'];
                $sum_add_naryad += $arrayData[$user->id]['add_naryad_sum'];
                $sum_deduction += $arrayData[$user->id]['deduction'];
                $sum_fines += $arrayData[$user->id]['fine'];
                $sum_premiums += $arrayData[$user->id]['premium'];
                $sum_total += $arrayData[$user->id]['total_sum'];
            }

        }

        //записываем итоговые суммы в массив
        $arr =[];
        $arr[0]['id'] = '1';
        $arr[0]['name'] = '';
        $arr[0]['naryad_sum'] = $sum_naryad;
        $arr[0]['add_naryad_sum'] = $sum_add_naryad;
        $arr[0]['deduction'] = $sum_deduction;
        $arr[0]['fine'] = $sum_fines;
        $arr[0]['premium'] = $sum_premiums;
        $arr[0]['total_sum'] = $sum_total;


        if($this->getNullPosition()){
            $newArr = [];
            foreach ($arrayData as $key=>$value)
            {
                 if($value['naryad_sum'] !=0 || $value['premium'] !=0  || $value['deduction'] !=0){
                     $newArr[$key]=$value;
                 }
            }
            $result = array_merge($newArr, $arr);
        }else{
            $result = array_merge($arrayData, $arr);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
            'sort' => ['defaultOrder' => [
                'id' => SORT_ASC,
            ],
                'attributes' => ['id', 'name', 'naryad_sum' , 'deduction', 'fine', 'premium', 'total_sum'],
            ],
            'pagination' => [
                'pageSize' => count($result),
            ],
        ]);

        return $dataProvider;

    }

    /**
     * собираем отчет по дням
     */
    public function dayListReport($user_id)
    {


        $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
        //             средняя дневная норма                цена нормированного коэффициента
        if($config && $config->json('averageRate') && $config->json('hourrate')) {

            //забираем  из таблицы отчетов суммы за наряды по датам
            $addReports = (new \yii\db\Query())
                ->select([
                    'FROM_UNIXTIME(report.created_at, "%Y-%m-%d") as date',
                    'SUM("' . $config->json('averageRate') . '"/report.job_rate*report.job_count*"' . $config->json('hourrate') . '") as naryad_sum'])
                ->from('laitovo_erp_users_report as report')
                ->where(['and',
                    ['report.user_id'=>$user_id],
                    ['>', 'report.created_at', $this->getDateFrom1()],
                    ['<', 'report.created_at', $this->getDateTo1()],
                ])
                ->andWhere(['is not', 'additional_naryad_id', null])
                ->groupBy('date')
                ->all();

            //забираем  из таблицы отчетов суммы за наряды по датам
            $reports = (new \yii\db\Query())
                ->select([
                    'FROM_UNIXTIME(report.created_at, "%Y-%m-%d") as date',
                    'SUM("' . $config->json('averageRate') . '"/report.job_rate*report.job_count*"' . $config->json('hourrate') . '") as naryad_sum'])
                ->from('laitovo_erp_users_report as report')
                ->where(['and',
                    ['report.user_id'=>$user_id],
                    ['>', 'report.created_at', $this->getDateFrom1()],
                    ['<', 'report.created_at', $this->getDateTo1()],
                ])
                ->groupBy('date')
                ->all();

             //забираем из таблицы переделок суммы штрафов по датам для пользователя
            $reworks  = (new \yii\db\Query())
                ->select([
                    'FROM_UNIXTIME(created_at, "%Y-%m-%d") as date',
                    'SUM(rework.deduction) as deduction'])
                ->from('laitovo_rework_act as rework')

                ->where(['and',
                    ['made_user_id'=>$user_id],
                    ['>', 'rework.created_at',  $this->getDateFrom1()],
                    ['<', 'rework.created_at',  $this->getDateTo1()],
                ])
                ->groupBy(['date'])
                ->all();

            $fines = (new \yii\db\Query())
                ->select([
                    'FROM_UNIXTIME(created_at, "%Y-%m-%d") as date',
                    'SUM(amount) as amount'])
                ->from('laitovo_erp_fine')
                ->where(['and',
                    ['user_id'=>$user_id],
                    ['>', 'created_at', $this->getDateFrom1()],
                    ['<', 'created_at', $this->getDateTo1()],
                ])
                ->groupBy(['date'])
                ->all();

            $premiums = (new \yii\db\Query())
                ->select([
                    'FROM_UNIXTIME(created_at, "%Y-%m-%d") as date',
                    'SUM(amount) as amount'])
                ->from('laitovo_erp_premium')
                ->where(['and',
                    ['is_paid'=>true],
                    ['user_id'=>$user_id],
                    ['>', 'created_at', $this->getDateFrom1()],
                    ['<', 'created_at', $this->getDateTo1()],
                ])
                ->groupBy(['date'])
                ->all();

            //собираем массив дат что бы отображались все даты

            $arrayDayList = $this->createArrayDayListReport( $reports, $reworks, $fines, $premiums, $addReports);
            return $arrayDayList;
        }
    }

   //собираем массив для отчета по дням
    public function createArrayDayListReport( $reports=[], $reworks, $fines=[], $premiums = [], $addReports = [])
    {
        $arrayData = [];
        $date=[];
        $sum_naryad = 0;
        $sum_add_naryad = 0;
        $sum_deduction = 0;
        $sum_fine = 0;
        $sum_premium = 0;
        $sum_total = 0;
        //собираем массив со всеми датами
        foreach ($reports as $report)
        {
            $date[] = $report['date'];
        }
        foreach ($reworks as $rework)
        {
            $date[] = $rework['date'];
        }
        foreach ($fines as $fine)
        {
            $date[] = $fine['date'];
        }
        foreach ($premiums as $premium)
        {
            $date[] = $premium['date'];
        }
//        die(print_r($fines));
    //убираем повторяющиеся даты
        $dates = array_unique($date);
    //собираем главный массив
        foreach ($dates as $date)
        {
            $arrayData[$date]['created_at'] = $date;
            $arrayData[$date]['naryad_sum'] = 0;
            $arrayData[$date]['add_naryad_sum'] = 0;
            $arrayData[$date]['deduction'] = 0;
            $arrayData[$date]['fine'] = 0;
            $arrayData[$date]['premium'] = 0;

            foreach ($reports as $report){
                if(isset($report['date']) && $report['date'] == $date){
                    if ($report['naryad_sum']) {
                        $arrayData[$date]['naryad_sum'] = round($report['naryad_sum'], 2);
                        break;
                    }
                }
            }

            foreach ($addReports as $addReport){
                if(isset($addReport['date']) && $addReport['date'] == $date){
                    if ($addReport['naryad_sum']) {
                        $arrayData[$date]['add_naryad_sum'] = round($addReport['naryad_sum'], 2);
                        break;
                    }
                }
            }

            foreach ($reworks as $rework) {
                if (isset($rework['date']) && $rework['date'] == $date) {
                    if ($rework['deduction']) {
                        $arrayData[$date]['deduction'] = round($rework['deduction'], 2);
                        break;
                    }
                }
            }

            foreach ($fines as $fine) {
                if (isset($fine['date']) && $fine['date'] == $date) {
                    if ($fine['amount']) {
                        $arrayData[$date]['fine'] = round($fine['amount'], 2);
                        break;
                    }
                }
            }

            foreach ($premiums as $premium) {
                if (isset($premium['date']) && $premium['date'] == $date) {
                    if ($premium['amount']) {
                        $arrayData[$date]['premium'] = round($premium['amount'], 2);
                        break;
                    }
                }
            }

            $arrayData[$date]['total_sum'] = round($arrayData[$date]['naryad_sum'] - $arrayData[$date]['deduction']-$arrayData[$date]['fine'] + $arrayData[$date]['premium'],2) ;
            $sum_naryad+=$arrayData[$date]['naryad_sum'];
            $sum_add_naryad+=$arrayData[$date]['add_naryad_sum'];
            $sum_deduction+=$arrayData[$date]['deduction'];
            $sum_fine+=$arrayData[$date]['fine'];
            $sum_premium+=$arrayData[$date]['premium'];
            $sum_total+=$arrayData[$date]['total_sum'];
        }
        $arr =[];
        $arr[0]['created_at'] = '';
        $arr[0]['naryad_sum'] = $sum_naryad;
        $arr[0]['add_naryad_sum'] = $sum_add_naryad;
        $arr[0]['deduction'] = $sum_deduction;
        $arr[0]['fine'] = $sum_fine;
        $arr[0]['premium'] = $sum_premium;
        $arr[0]['total_sum'] = $sum_total;
        $result = array_merge($arrayData, $arr);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_ASC,
                ],
                'attributes' => ['created_at', 'naryad_sum' , 'deduction', 'total_sum'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $dataProvider;

    }



//таблица с нарядами за которые получил деньги
    public function dayReportNaryad($user_id, $date, $params)
    {
        $date_from = strtotime($date);
        $date_to = $date_from+(60 * 60 * 24 - 1);
        $sum_naryad = 0;

        $query = ErpUsersReport::find()->where(['and',
                ['laitovo_erp_users_report.user_id'=>$user_id],
                ['>', 'laitovo_erp_users_report.created_at', $date_from],
                ['<', 'laitovo_erp_users_report.created_at', $date_to],
            ])->joinWith(['naryad', 'additionalNaryad']);

        if($this->load($params)){
            if ($this->window) $query->andFilterWhere(['like', 'article',$this->window.'-%-%-%-%',false]);
            if ($this->tkan)$query->andFilterWhere(['like', 'article','%-%-%-%-'.$this->tkan, false]);
        }
        $reports = $query->all();
        if($reports)
        {
            foreach ($reports as $report)
            {
                $sum_naryad+=$report->naryadSum;
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query'=>$query
        ]);

        return [
            'dataProvider'=>$dataProvider,
            'sum_naryad'=>$sum_naryad
        ];

    }


    //таблица с нарядами за которые вычли брак
    public function dayReportRework($user_id, $date, $params)
    {
        $date_from = strtotime($date);
        $date_to = $date_from+(60 * 60 * 24 - 1);
        $sum_deduction = 0;
        $query = LaitovoReworkAct::find()->where(['and',
            ['made_user_id'=>$user_id],
            ['>', 'laitovo_rework_act.created_at', $date_from],
            ['<', 'laitovo_rework_act.created_at', $date_to],
        ])->joinWith('naryad');

        if($this->load($params)){
            if ($this->rework_window) $query->andFilterWhere(['like', 'article',$this->rework_window.'-%-%-%-%',false]);
            if ($this->rework_tkan)$query->andFilterWhere(['like', 'article','%-%-%-%-'.$this->rework_tkan, false]);
        }
        $reworks = $query->all();
        if($reworks)
        {
            foreach ($reworks as $rework)
            {
                $sum_deduction+=$rework->deduction;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query'=>$query
        ]);

        return [
            'dataProvider'=>$dataProvider,
            'sum_deduction'=>$sum_deduction
        ];

    }


    public function dayReportFine($user_id, $date)
    {
        $date_from = strtotime($date);
        $date_to = $date_from+(60 * 60 * 24 - 1);
        $sum_fine = 0;
        $query = ErpFine::find()->where(['and',
            ['user_id'=>$user_id],
            ['>', 'created_at', $date_from],
            ['<', 'created_at', $date_to],
        ]);

        $fines = $query->all();
        foreach ($fines as $fine){
            $sum_fine += $fine->amount;
        }

        $dataProvider = new ActiveDataProvider([
            'query'=>$query
        ]);

        return [
            'dataProvider'=>$dataProvider,
            'sum_fine'=>$sum_fine
        ];
    }

    public function dayReportPremium($user_id, $date)
    {
        $date_from = strtotime($date);
        $date_to = $date_from+(60 * 60 * 24 - 1);
        $sum_premium = 0;
        $query = Premium::find()->where(['and',
            ['is_paid' => true],
            ['user_id'=>$user_id],
            ['>', 'created_at', $date_from],
            ['<', 'created_at', $date_to],
        ]);

        $premiums = $query->all();
        foreach ($premiums as $premium){
            $sum_premium += $premium->amount;
        }

        $dataProvider = new ActiveDataProvider([
            'query'=>$query,
            'sort'=>false
        ]);

        return [
            'dataProvider'=>$dataProvider,
            'sum_premium'=>$sum_premium
        ];
    }

    public function getDate(){
        $dateAction = new DateAction($this->getDateStr());
        return $dateAction->getMonth().'-'.$dateAction->getEar();
    }


}