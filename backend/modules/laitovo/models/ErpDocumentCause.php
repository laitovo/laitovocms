<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/28/17
 * Time: 2:24 PM
 */

namespace backend\modules\laitovo\models;



use yii\data\ActiveDataProvider;

class ErpDocumentCause extends \common\models\laitovo\ErpDocumentCause
{
    public function rules()
    {
        return [
            [['title','date_start','date_finish','type','user_id'],'required'],
            ['user_id', 'integer'],
            [['date_start', 'date_finish'],'validateDate']
        ];
    }


    public function validateDate($attribute,$params){
        $document_list = self::find()->where(['and',
            ['user_id'=>$this->user_id],
            ['or',
                ['and',
                    ['<=','date_start',$this->date_start ],
                    ['>=','date_finish',$this->date_start ],
                ],
                ['and',
                    ['<=','date_start',$this->date_finish],
                    ['>=','date_finish',$this->date_finish],
                ],
                ['and',
                    ['>=','date_start',$this->date_start],
                    ['<=','date_finish',$this->date_finish],
                ],
            ]
        ])->one();
        if($document_list)
            $this->addError($attribute, 'Выбранный промежуток времени пересекается с уже существующими документами');
    }

    public function document_list($date, $user_id){
        $document_list = self::find()->where(['and',
            ['user_id'=>$user_id],
            ['or',
                ['and',
                    ['>=', 'date_start', $date],
                    ['<=', 'date_start', date('Y-m-t', strtotime($date))],
                ],
                ['and',
                    ['>=', 'date_finish', $date],
                    ['<=', 'date_finish', date('Y-m-t', strtotime($date))],
                ],
                ['and',
                    ['<','date_start',$date],
                    ['>','date_finish',$date],
                ]
            ]
        ]);

        $document_provider = new ActiveDataProvider([
            'query'=>$document_list
        ]);
        return $document_provider;
    }

}