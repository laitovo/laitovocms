<?php

namespace backend\modules\laitovo\models;

use Yii;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "laitovo_erp_location_workplace_plan".
 *
 * @property integer $id
 * @property integer $workplace_id
 * @property integer $user_id
 * @property integer $time_from
 * @property integer $time_to
 * @property integer $created_at
 * @property integer $author_id
 *
 * @property User $author
 * @property LaitovoErpUser $user
 * @property LaitovoErpLocationWorkplace $workplace
 */
class ErpLocationWorkplacePlan extends \common\models\laitovo\ErpLocationWorkplacePlan
{
    private $viewTimeFrom;
    private $viewTimeTo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['viewTimeFrom', 'viewTimeTo','user_id'], 'required'],
            [['workplace_id', 'user_id','created_at', 'author_id'], 'integer'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['workplace_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocationWorkplace::className(), 'targetAttribute' => ['workplace_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [

                'viewTimeFrom' => 'Время начала',
                'viewTimeTo' => 'Время окончания',
            ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkplace()
    {
        return $this->hasOne(ErpLocationWorkplace::className(), ['id' => 'workplace_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function setViewTimeFrom($value)
    {
        $this->time_from = Yii::$app->formatter->asTimestamp($value);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function setViewTimeTo($value)
    {
        $this->time_to = Yii::$app->formatter->asTimestamp($value);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewTimeFrom()
    {
        return $this->time_from ? Yii::$app->formatter->asDateTime($this->time_from) : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewTimeTo()
    {
        return $this->time_to ? Yii::$app->formatter->asDateTime($this->time_to) : null;
    }

    public function getTimeIn()
    {
        $registerIn = ErpLocationWorkplaceRegister::find()->where(['user_id'=>$this->user_id, 'action_type'=>ErpLocationWorkplaceRegister::TYPE_IN])->orderBy('id DESC')->limit(1)->one();
        if(isset($registerIn) && $registerIn->register_at && $registerIn->register_at > time()-(60*60*15))
        {
            return $registerIn->register_at;
        }
    }
}
