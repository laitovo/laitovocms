<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;

/**
 * ErpLocationWorkplaceRegisterSearch represents the model behind the search form about `backend\modules\laitovo\models\ErpLocationWorkplaceRegister`.
 */
class ErpLocationWorkplaceRegisterSearch extends ErpLocationWorkplaceRegister
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'workplace_id', 'register_at'], 'integer'],
            [['action_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErpLocationWorkplaceRegister::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'workplace_id' => $this->workplace_id,
            'register_at' => $this->register_at,
        ]);

        $query->andFilterWhere(['like', 'action_type', $this->action_type]);

        return $dataProvider;
    }
}
