<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/18/18
 * Time: 11:01 AM
 */
namespace backend\modules\laitovo\models;

use common\models\laitovo\CarClips;
use Yii;
use backend\helpers\ArticleHelper;

/**
 * Класс для подгрузки картинок с примерами установки клипс.
 *
 * Class CarsPictures
 * @package backend\modules\laitovo\models
 */
class CarsPictures
{
    /**
     * @var $_clips array
     */
    private $_clips;

    /**
     * @var $_clips array
     */
    private $_clipsOnly;

    /**
     * @var $_magnets array
     */
    private $_magnets;

    /**
     * @var $_window string
     */
    private $_window;

    /**
     * @var $_number string
     */
    private $_number;
    /**
     * @var $_number string
     */
    private $_isRvSquare;
    /**
     * @var $_number string
     */
    private $_isRvTriangular;

    /**
     * @var $_bwSklad boolean
     */
    private $_bwSklad;

    /**
     * @var $_isMagnet boolean
     */
    private $_isMagnet;

    /**
     * @var $_isOnMagnets boolean
     */
    private $_isOnMagnets;


    /**
     * @var $_isTape boolean
     */
    private $_isTape;

    /**
     * @var $_isMagnet boolean
     */
    private $_isOnlyMagnets;
    /**
     * @var array Вспомогательная переменная для кэширования функции getPicNumbers()
     */
    private $_picNumbers = [];

    public function __construct($article)
    {
        /**
         * @var $clips array
         *
         * Массив клипс и их типов вида [ 'name' => 'type' ]
         */
//        $this->_clips = ArticleHelper::getClipsWithTypes($article);
        $this->_clips = ArticleHelper::getClipsWithSidesAndTypes($article);
        $this->_clipsOnly = ArticleHelper::getClipsOnly($article);
        $this->_magnets = ArticleHelper::getMagnets($article);
        $this->_window = ArticleHelper::getWindowEn($article);
        $this->_isRvSquare = ArticleHelper::isRVSquare($article);
        $this->_isRvTriangular = ArticleHelper::isRvTriangular($article);
        $this->_bwSklad  = ArticleHelper::isBwSklad($article);
        $this->_isMagnet = ArticleHelper::isMagnet($article);
        $this->_isOnMagnets = ArticleHelper::isOnMagnets($article);
        $this->_isTape = ArticleHelper::isTape($article);
        $this->_isOnlyMagnets = empty($this->_clipsOnly) && !empty($this->_magnets);

        $this->_number = $this->getNumber($article);
    }

    /**
     * Получить изображения
     */
    public function getImages()
    {
        $result = [];
        $picNumber = $this->_number;

        if ($this->_isOnlyMagnets && !$this->_isMagnet && !$this->_isOnMagnets) {
            return $result;
        }


        if ($this->_isMagnet) {
            if ($this->_window == 'RD') {
                $item['src'] = '/img/instructions/rdmagnet.jpg';
                $item['num'] = ++$picNumber;
                $item['name'] = null;
                $item['side'] = null;
                $result[] = (object)$item;
            }
            if ($this->_window == 'FD') {
                $item['src'] = '/img/instructions/fdmagnet.jpg';
                $item['num'] = ++$picNumber;
                $item['name'] = null;
                $item['side'] = null;
                $result[] = (object)$item;
            }
            $item['src'] = '/img/instructions/allmagnet.jpg';
            $item['num'] = ++$picNumber;
            $item['name'] = null;
            $item['side'] = null;
            $result[] = (object)$item;

            return $result;
        }


        if ($this->_isOnMagnets) {
            if ($this->_window == 'RD') {
                $item['src'] = '/img/instructions/rdmagnet.jpg';
                $item['num'] = ++$picNumber;
                $item['name'] = null;
                $item['side'] = null;
                $result[] = (object)$item;
            }
            if ($this->_window == 'FD') {
                $item['src'] = '/img/instructions/fdmagnet.jpg';
                $item['num'] = ++$picNumber;
                $item['name'] = null;
                $item['side'] = null;
                $result[] = (object)$item;
            }
            $item['src'] = '/img/instructions/onmagnets.jpg';
            $item['num'] = ++$picNumber;
            $item['name'] = null;
            $item['side'] = null;
            $result[] = (object)$item;

            return $result;
        }

        if ($this->_bwSklad) {
            $item['src'] = $this->_getImgSrcBwSklad();
            $item['num'] = ++$picNumber;
            $item['name'] = null;
            $item['side'] = null;
            $result['bwSklad'] = (object)$item;
        }

//        foreach ($this->_clips as $clip) {
//            $item['src'] = $this->_getImgSrc($clip,$this->_window);
//            $item['num'] = ++$picNumber;
//            if (!isset($result[$clip])) $result[$clip] = (object)$item;
//        }
        $imagesByCLips = $this->_getImagesByClips();
        foreach ($imagesByCLips as $picture) {
            $picture->num = ++$picNumber;
        }

        $this->_setPicNumbers($imagesByCLips);

        $result = array_merge($result, $imagesByCLips);

        if (empty($this->_clips) && !$this->_isMagnet && !$this->_isOnlyMagnets && !$this->_isOnMagnets) {
            $item['src'] = $this->_getImgSrcNoClips($this->_window);
            $item['num'] = ++$picNumber;
            $item['name'] = null;
            $item['side'] = null;
            $result[] = (object)$item;
        }

        return $result;
    }

    /**
     * Функция возвращает массив с номерами картинок, сгруппированными по типу крепления клипс.
     * Данная группировка используется для того, чтобы выводить текст вида "как показано на рис. 3, 4, 5.".
     *
     * @return array
     */
    public function getPicNumbers()
    {
        if (!$this->_picNumbers) {
            $this->getImages();
        }

        return $this->_picNumbers;
    }

    /**
     * @param array $imagesByCLips
     */
    private function _setPicNumbers(array $imagesByCLips)
    {
        $this->_picNumbers = [];
        foreach ($imagesByCLips as $picture) {
            if (!isset($this->_picNumbers[$picture->type])) $this->_picNumbers[$picture->type] = [];
            if (!in_array($picture->num, $this->_picNumbers[$picture->type])) $this->_picNumbers[$picture->type][] = $picture->num;
        }
    }

    /**
     * Функция возвращает картинки для установки клипс
     *
     * @return array
     */
    private function _getImagesByClips()
    {
        $result = [];
        /**
         * @var $sources array
         *
         * Массив для фильтрации картинок-дублей
         */
        $sources = [];
        foreach ($this->_clips as $side => $types) {
            foreach ($types as $name => $type) {
                $source = $this->_getImgSrc($type, $this->_window, $side);
                if (in_array($source, $sources) || !$source) {
                    continue; //Убираем дубли картинок
                }
                $sources[] = $source;

                $item['type'] = $type;
                $item['name'] = $name;
                $item['side'] = $side;
                $item['src']  = $source;
                $item['num']  = 0;

                /**
                 * @var $key string
                 *
                 * Ключ для сортировки картинок, чтобы смежные по смыслу картинки отображались рядом
                 */
                $key = $type == 'V' ? $type : $name . '_' . $type . '_' . $side;

                if (!isset($result[$key])) {
                    $result[$key] = (object)$item;
                }
            }
        }

        ksort($result);

        return $result;
    }

//    /**
//     * Отдает необходимую картинку в зависимости от того типа крепления и оконного проема
//     *
//     * @param $type
//     * @param $window
//     * @return string
//     */
//    private function _getImgSrc($type,$window)
//    {
//        if ($type == 'A' && $window == 'BW') {
//            $src = '/img/instructions/aabw.jpg';
//        }elseif (($type == 'A' || $type == 'AA') && $window == 'RV' && $this->_isRvTriangular) {
//            $src = '/img/instructions/aarv.jpg';
//        }elseif (($type == 'A' || $type == 'AA') && $window == 'RV') {
//            $src = '/img/instructions/aarvsquare.jpg';
//        }elseif (($type == 'A' || $type == 'AA') && $window == 'FV') {
//            $src = '/img/instructions/aafv.jpg';
//        }elseif ($type == 'A') {
//            $src = '/img/instructions/atype.jpg';
//        }elseif ($type == 'B') {
//            $src = '/img/instructions/b.jpg';
//        }elseif ($type == 'C') {
//            $src = '/img/instructions/c.jpg';
//        }elseif ($type == 'V') {
//            $src = '/img/instructions/v.jpg';
//        }elseif ($type == 'AA' && $window == 'BW') {
//            $src = '/img/instructions/aabw.jpg';
//        }elseif ($type == 'AA') {
//            $src = '/img/instructions/aatype.jpg';
//        }elseif ($type == 'CC') {
//            $src = '/img/instructions/cc.jpg';
//        }else{
//            $src = '';
//        }
//
//        return $src;
//    }
    /**
     * Отдает необходимую картинку в зависимости от типа крепления, оконного проема и стороны
     *
     * @param $type
     * @param $window
     * @param $side
     * @return string
     */
    private function _getImgSrc($type, $window, $side)
    {
        if ($type == 'T' && $window == 'FD' && $side == CarClips::SIDE_LEFT) {
            $src = '/img/instructions/fd-tape-1.jpg';
        }elseif ($type == 'T' && $window == 'FD' && $side == CarClips::SIDE_TOP) {
            $src = '/img/instructions/fd-tape-2.jpg';
        }elseif  ($type == 'T' && $window == 'FD' && $side == CarClips::SIDE_RIGHT) {
            $src = '/img/instructions/fd-tape-3.jpg';
        }elseif  ($type == 'T' && $window == 'FD' && $side == CarClips::SIDE_BOTTOM) {
            $src = '/img/instructions/fd-tape-4.jpg';
        }elseif ($type == 'T' && $window == 'RD' && $side == CarClips::SIDE_LEFT) {
            $src = '/img/instructions/rd-tape-1.jpg';
        }elseif ($type == 'T' && $window == 'RD' && $side == CarClips::SIDE_TOP) {
            $src = '/img/instructions/rd-tape-2.jpg';
        }elseif  ($type == 'T' && $window == 'RD' && $side == CarClips::SIDE_RIGHT) {
            $src = '/img/instructions/rd-tape-3.jpg';
        }elseif  ($type == 'T' && $window == 'RD' && $side == CarClips::SIDE_BOTTOM) {
            $src = '/img/instructions/rd-tape-4.jpg';
        }elseif ($type == 'A' && $window == 'FD' && $side == CarClips::SIDE_LEFT) {
            $src = '/img/instructions/FD-1-A.jpg';
        }elseif ($type == 'A' && $window == 'FD' && $side == CarClips::SIDE_TOP) {
            $src = '/img/instructions/FD-2-A.jpg';
        }elseif  ($type == 'A' && $window == 'RD' && $side == CarClips::SIDE_TOP) {
            $src = '/img/instructions/RD-2-A.jpg';
        }elseif  ($type == 'A' && $window == 'RD' && $side == CarClips::SIDE_RIGHT) {
            $src = '/img/instructions/RD-3-A.jpg';
        }elseif  (($type == 'A' || $type == 'B') && $window == 'BW') {
            $src = '/img/instructions/aabw.jpg';
        }elseif (($type == 'A' || $type == 'AA') && $window == 'RV' && $this->_isRvTriangular) {
            $src = '/img/instructions/aarv.jpg';
        }elseif (($type == 'A' || $type == 'AA') && $window == 'RV') {
            $src = '/img/instructions/aarvsquare.jpg';
        }elseif (($type == 'A' || $type == 'AA') && $window == 'FV') {
            $src = '/img/instructions/aafv.jpg';
        }elseif ($type == 'A') {
            $src = '/img/instructions/atype.jpg';
        }elseif ($type == 'B' && $window == 'FD' && $side == CarClips::SIDE_LEFT) {
            $src = '/img/instructions/FD-1-B.jpg';
        }elseif ($type == 'B' && $window == 'FD' && $side == CarClips::SIDE_TOP) {
            $src = '/img/instructions/FD-2-B.jpg';
        }elseif ($type == 'B' && $window == 'RD' && $side == CarClips::SIDE_TOP) {
            $src = '/img/instructions/RD-2-B.jpg';
        }elseif ($type == 'B' && $window == 'RD' && $side == CarClips::SIDE_RIGHT) {
            $src = '/img/instructions/RD-3-B.jpg';
//        }elseif ($type == 'B' && $window !== 'BW' && $window !== 'RV') {
        }elseif ($type == 'B' && $window !== 'BW') {
            $src = '/img/instructions/b.jpg';
        }elseif ($type == 'C' && $window == 'FD' && $side == CarClips::SIDE_LEFT) {
            $src = '/img/instructions/FD-1-C.jpg';
        }elseif ($type == 'C' && $window == 'FD' && $side == CarClips::SIDE_TOP) {
            $src = '/img/instructions/FD-2-C.jpg';
        }elseif ($type == 'C' && $window == 'FD' && $side == CarClips::SIDE_RIGHT) {
            $src = '/img/instructions/FD-3-C.jpg';
        }elseif ($type == 'C' && $window == 'RD' && $side == CarClips::SIDE_TOP) {
            $src = '/img/instructions/RD-2-C.jpg';
        }elseif ($type == 'C' && $window == 'RD' && $side == CarClips::SIDE_RIGHT) {
            $src = '/img/instructions/RD-3-C.jpg';
        }elseif ($type == 'C' && $window !== 'BW' && $window !== 'RV') {
            $src = '/img/instructions/c.jpg';
        }elseif ($type == 'V') {
            $src = '/img/instructions/v.jpg';
        }elseif ($type == 'AA' && $window == 'RV') {
            $src = '/img/instructions/RV-AA.jpg';
        }elseif ($type == 'AA' && $window == 'BW') {
            $src = '/img/instructions/aabw.jpg';
        }elseif ($type == 'AA') {
            $src = '/img/instructions/aatype.jpg';
        }elseif ($type == 'CC' && $window == 'FD' && $side == CarClips::SIDE_BOTTOM) {
            $src = '/img/instructions/FD-4-CC.jpg';
        }elseif ($type == 'CC' && $window == 'RD' && $side == CarClips::SIDE_BOTTOM) {
            $src = '/img/instructions/RD-4-CC.jpg';
        }elseif ($type == 'CC') {
            $src = '/img/instructions/cc.jpg';
        }else{
            $src = '';
        }

        return $src;
    }

    /**
     * Отдает необходимую картинку в зависимости от того типа крепления и оконного проема
     *
     * @param $window
     * @return string
     */
    private function _getImgSrcNoClips($window)
    {
        if ($window == 'BW') {
            $src = '/img/instructions/bwnoclips.jpg';
        }elseif ($window == 'FV') {
            $src = '/img/instructions/fvnoclips.jpg';
        }elseif ($window == 'RV' && $this->_isRvTriangular) {
            $src = '/img/instructions/rvnoclips.jpg';
        }elseif ($window == 'RD') {
            $src = '/img/instructions/rvsquarenoclips.jpg';
        }elseif ($window == 'RV') {
            $src = '/img/instructions/rvsquarenoclips.jpg';
        }else{
            $src = '';
        }

        return $src;
    }

    /**
     * Отдает необходимую картинку в зависимости от того типа крепления и оконного проема
     *
     * @return string
     */
    private function _getImgSrcBwSklad()
    {
        $src = '/img/instructions/bwsklad.jpg';

        return $src;
    }

    private function getNumber($article)
    {
        $window = ArticleHelper::getWindowEn($article);
        switch ($window)  {
            case 'FD':
                $num = 1;
                break;
            case 'RD':
                $num = 1;
                break;
            case 'RV':
                $num = 1;
                break;
            case 'FV':
                $num = 1;
                break;
            case 'BW':
                $num = 1;
                break;
            case 'FW':
                $num = 1;
                break;
            default:
                $num = 1;
                break;
        }
        if ($this->_isMagnet) $num = 1;
        if ($this->_isOnMagnets) $num = 0;

        return $num;
    }
}