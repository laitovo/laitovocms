<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/11/17
 * Time: 8:29 AM
 */

namespace backend\modules\laitovo\models;


use common\models\laitovo\ErpReportCard;
use common\models\laitovo\Weekend;
use yii\base\Model;
use DatePeriod;
use DateInterval;
use DateTime;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use Yii;

class ReportCard extends Model
{

    private $dateFrom;
    private $dataArray;

    public function getDateTo()
    {
        return  $this->dateFrom ? date('Y-m-t',strtotime($this->getDateFrom())) :  date('Y-m-t');
    }

    public function setDateFrom($value)
    {
        $this->dateFrom = $value;
    }

    public function getDateFrom()
    {
        if($this->dateFrom)
        {
            if(count(explode('.', $this->dateFrom))>1){
                $time = explode('.', $this->dateFrom);
            }else{
                $time = explode('-', $this->dateFrom);
            }
            if(isset($time[1]))
                return date('Y-m-d',mktime(0, 0, 0, $time[1], 1,$time[2]));
            else
                return date('Y-m-d',mktime(0, 0, 0, date('m'), 1));
        }
        return date('Y-m-d',mktime(0, 0, 0, date('m'), 1));
    }

    public function rules()
    {
        return [
            [['dateFrom'], 'safe'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'dateFrom' => Yii::t('app', '* любой день нужного месяца'),
        ];
    }

    public function getTitle()
    {
        $m='';
        $value = explode('-', $this->getDateFrom());
        if($value[1]){
            switch ($value[1]) {
                case 1:
                    $m = 'январь';
                    break;
                case 2:
                    $m = 'февраль';
                    break;
                case 3:
                    $m = 'март';
                    break;
                case 4:
                    $m = 'апрель';
                    break;
                case 5:
                    $m = 'май';
                    break;
                case 6:
                    $m = 'июнь';
                    break;
                case 7:
                    $m = 'июль';
                    break;
                case 8:
                    $m = 'август';
                    break;
                case 9:
                    $m = 'сентябрь';
                    break;
                case 10:
                    $m = 'октябрь';
                    break;
                case 11:
                    $m = 'ноябрь';
                    break;
                case 12:
                    $m = 'декабрь';
                    break;
            }
        }
        return 'Табель за ' . $m . ' ' . ($value[0] ? $value[0] : '');
    }

    //получаем массив дат по дням за выбраный промежуток времени
    public function getDateArray()
    {
        if ($this->dataArray) return $this->dataArray;

        $begin = new DateTime($this->getDateFrom());
        $end = new DateTime($this->getDateTo());
        $end = $end->modify( '+1 day' );

        $period = new DatePeriod($begin, new DateInterval('P1D'), $end);
        $this->dataArray = array_map(
            function($item){
                return $item->format('Y-m-d');
            },
            iterator_to_array($period)
        );
        return $this->dataArray;
    }


    public function getProductionWorkplace()
    {
        $workplace = ErpLocationWorkplace::find()->joinWith('location l')->where(['l.type'=>ErpLocation::TYPE_PRODUCTION])->all();
        if($workplace)
            return ArrayHelper::map($workplace, 'id', 'id');
    }

    /**
     * @return array
     * Итоговый массив для провайдера
     */
    public function resultArray($user_id)
    {
        $resultArr = [
            'final_difference'=>[
                'label'=>'раб. час',
            ],
            'time_in'=>[
                'label'=>'приход',
            ],
            'time_out'=>[
                'label'=>'уход',
            ],
            'in_production'=>[
                'label'=>'сделка',
            ],
        ];
        $sum_fact = 0;

        $dataArray = $this->getDateArray();
        if($dataArray && count($dataArray)>0)
        {
            $firstDate = $dataArray[0];
            $lastDate = $dataArray[count($dataArray)-1];

            $reportIn = ErpLocationWorkplaceRegister::find()->where(['and',
                ['user_id'=>$user_id],
                ['action_type'=> ErpLocationWorkplaceRegister::TYPE_IN],
                ['>=','register_at', strtotime($firstDate)],
                ['<=','register_at', strtotime($lastDate)+(60*60*20)]
            ])
                ->select(['register_at', new Expression("DATE_FORMAT(FROM_UNIXTIME(`register_at`), '%Y-%m-%d') as humanDate")])
                ->orderBy('id DESC')
                ->indexBy(function($row) {
                    return $row['humanDate'];
                })
                ->asArray()
                ->all();

            $reportOut = ErpLocationWorkplaceRegister::find()->where(['and',
                ['user_id'=>$user_id],
                ['action_type'=> ErpLocationWorkplaceRegister::TYPE_OUT],
                ['>=','register_at', strtotime($firstDate)],
                ['<=','register_at', strtotime($lastDate)+(60*60*20)]
            ])
                ->select(['register_at', new Expression("DATE_FORMAT(FROM_UNIXTIME(`register_at`), '%Y-%m-%d') as humanDate")])
                ->orderBy('id ASC')
                ->indexBy(function($row) {
                    return $row['humanDate'];
                })
                ->asArray()
                ->all();

            foreach ($dataArray as $date)
            {
                $resultArr['time_in'][$date] = isset($reportIn[$date]['register_at']) ? date('H.i', $reportIn[$date]['register_at']) : 0;
                $resultArr['time_out'][$date] = isset($reportOut[$date]['register_at']) ? date('H.i', $reportOut[$date]['register_at']) : 0;

                $resultArr['in_production'][$date] = ($timeInProd = $this->getActiveInProduction($date, $user_id)) ? date("H.i", mktime(0, 0, $timeInProd )) : 0;

                if ( isset($reportIn[$date]['register_at']) && isset($reportOut[$date]['register_at']) )
                {
                    $final_difference = ($reportOut[$date]['register_at'] - $reportIn[$date]['register_at'] - 3600) - $resultArr['in_production'][$date];

                    $resultArr['final_difference'][$date] =  $final_difference > 0 ? $this->getHourMinute($final_difference) : 0;

                    $sum_fact +=  ($reportOut[$date]['register_at'] - $reportIn[$date]['register_at'] - 3600) - $resultArr['in_production'][$date] > 0 ? ($reportOut[$date]['register_at'] - $reportIn[$date]['register_at'] - 3600) - $resultArr['in_production'][$date] : 0;
                }else{
                    $resultArr['final_difference'][$date] = 0;
                }
            }

            $resultArr['final_difference']['fact_sum'] = $sum_fact> 0 ? $this->getHourMinute($sum_fact) : 0;
            $resultArr['time_in']['fact_sum'] = '';
            $resultArr['time_out']['fact_sum'] = '';
            $resultArr['in_production']['fact_sum'] = '';

            $resultArr['final_difference']['month_rate'] = $this->monthRate();
            $resultArr['time_in']['month_rate'] = '';
            $resultArr['time_out']['month_rate'] = '';
            $resultArr['in_production']['month_rate'] = '';

        }
        return $resultArr;
    }

    /**
     * @param $date
     * время активное на производстве
     */
    public function getActiveInProduction($date, $user_id)
    {
        $time = 0;
        $maxDate = 0;//наибольшая метка времени

        //ищем все производственные рабочие места локации на которые работник встал в течение дня
        $productionActives = ErpLocationWorkplaceRegister::find()
            ->where(['and',
                ['user_id'=>$user_id],
                ['in', 'workplace_id', $this->getProductionWorkplace()],
                ['action_type'=>ErpLocationWorkplaceRegister::TYPE_ACTIVE],
                ['>','register_at', strtotime($date)],
                ['<','register_at', strtotime($date)+(60*60*15)]
            ])
            ->orderBy('id ASC')
            ->all();

        if($productionActives)
        {
            foreach ($productionActives as $productionActive)
            {

                //ищем ближайшую по дате запись о том что сотрудник покнул рабочее место или стал неактивным на рабочем месте

                if(isset($productionActive) && $productionActive->workplace_id)
                {
                    $productionInactives = ErpLocationWorkplaceRegister::find()
                        ->where(['and',
                            ['user_id'=>$user_id],
                            [ 'workplace_id'=>$productionActive->workplace_id],
                            ['action_type'=>ErpLocationWorkplaceRegister::TYPE_INACTIVE],
                            ['>','register_at', $maxDate==0 ? strtotime($date) : $maxDate],
                            ['<','register_at', strtotime($date)+(60*60*20)]
                        ])
                        ->orderBy('id asc')
                        ->one();

                    if(isset($productionInactives))
                    {
                        if($productionActive->register_at && $productionInactives->register_at && $productionActive->register_at>$maxDate)
                        {
                            $maxDate = $productionInactives->register_at;
                            $time+= $maxDate-$productionActive->register_at;
                        }
                        if($productionActive->register_at && $productionInactives->register_at && $productionActive->register_at<$maxDate && $productionInactives->register_at>$maxDate)
                        {
                            $time+= $productionInactives->register_at - $maxDate;
                            $maxDate = $productionInactives->register_at;
                        }
                    }
                }
            }
        }
        return $time;
    }


    public function getArrayDataProvider($user_id)
    {
        $arrayDataProvider = new ArrayDataProvider([
            'allModels' => $this->resultArray($user_id),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $arrayDataProvider;
    }

    public function littleLabel($date)
    {
        $value = explode('-',$date);
        if(isset($value[2]) && $value[2])
            return $value[2];
    }


    public function getHourMinute($time)
    {
        $hour = floor($time/3600);
        $sec = $time - ($hour*3600);
        $min = floor($sec/60);
        return $hour.'.'.$min;
    }


    public function monthRate()
    {
        $result = 0;
        $date_array = $this->getDateArray();
        $date_weekend = ArrayHelper::map(Weekend::find()->all(),'id','weekend');
        foreach ($date_array as $date)
        {
            if(!in_array($date,$date_weekend))
                $result +=8;
        }
        return $result;
    }

    /**
     * @param $user_id
     * @return bool
     */
    public function getAccessUpdateCardForm($user_id)
    {
        $card_report = ErpReportCard::find()->where(['user_id'=>$user_id,'month_year'=>$this->getDateFrom()])->one();
        //$dateFrom = strtotime($this->getDateFrom());
        //$date = mktime(0, 0, 0, date('m'), 1);
        //return $card_report  || ($dateFrom >= $date) ? false:true;
        return $card_report ? false : true;
    }


}