<?php

namespace backend\modules\laitovo\models;

use Yii;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;
use backend\modules\laitovo\models\ErpProductPerformance;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "laitovo_erp_location_workplace".
 *
 * @property integer $id
 * @property integer $location_id
 * @property integer $location_number
 * @property string $type
 * @property integer $user_id
 *
 * @property LaitovoErpLocation $location
 * @property LaitovoErpUser $user
 */
class ErpLocationWorkplace extends \common\models\laitovo\ErpLocationWorkplace
{
    private $curDate;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id','user_id','activity','last_activity','type_id'], 'integer'],
            [['location_number','property'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocationWorkplaceType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function getCurDate()
    {
        return $this->curDate ? Yii::$app->formatter->asDatetime($this->curDate) : mktime(0,0,0);
    }

    public function setCurDate($value)
    {
        $this->curDate = Yii::$app->formatter->asTimestamp();
    }

    public static function withdrawActivity()
    {
        //ФУНКЦИЯ ДЛЯ СНЯТИЯ АКТИВНОСТИ И УСТАНОВКИ СООТВЕТСВУЮЩИХ ЗАПИСЕЙ
        //снятие активности более 3 часов
        $ids = [];

        $locations = ErpLocation::find()
            ->where(['type' => ErpLocation::TYPE_PRODUCTION])
            ->all();

        if ($locations)
            $ids = ArrayHelper::map($locations,'id','id');

        //код для снятия активности
        //активные рабоче места, с производственных участков, у которых установлен пользователь
        //и последняя активность более 3 часов назад
        $workplaces = self::find()
            ->andWhere(['<','last_activity',time()-3*60*60])
            ->andWhere(['in','location_id',$ids])
            ->andWhere(['not in','location_id',[18,19]])
            ->andWhere(['IS NOT','user_id',NULL])
            ->andWhere(['!=','activity',0])
            ->all();

        //если они есть, тогда
        if ($workplaces)
        {
            foreach ($workplaces as $workplace) {
                //снимаем активность у места
                $workplace->activity = 0;
                $workplace->save();

                //делаем запись в журнал регистрации
                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id; //место
                $register->user_id = $workplace->user_id; //пользователь
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE; //статус
                $register->register_at = $workplace->last_activity; //время регистрации = времени последней активности
                $register->save();
            }
        }

        $workplaces = self::find()
            ->andWhere(['<','last_activity',time()-18*60*60])
            ->andWhere(['in','location_id',[18,19]])
            ->andWhere(['IS NOT','user_id',NULL])
            ->andWhere(['!=','activity',0])
            ->all();

        //если они есть, тогда
        if ($workplaces)
        {
            foreach ($workplaces as $workplace) {
                //снимаем активность у места
                $workplace->activity = 0;
                $workplace->save();

                //делаем запись в журнал регистрации
                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id; //место
                $register->user_id = $workplace->user_id; //пользователь
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE; //статус
                $register->register_at = $workplace->last_activity; //время регистрации = времени последней активности
                $register->save();
            }
        }

        //код для снятия регистрации
        //неактивные рабочем места, где последняя активность более 6 часов, у которых установлен пользователь

        $workplaces = self::find()
            ->andWhere(['<','last_activity',time()-6*60*60])
            ->andWhere(['in','location_id',$ids])
            ->andWhere(['IS NOT','user_id',NULL])
            ->andWhere(['activity' => 0])
            ->all();

        if ($workplaces)
        {
            $users = []; //массив пользователей
            foreach ($workplaces as $workplace) {
                //снимаем пользователя с участка
                //делаем запись в журнал регистрации

                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace->id; //место
                $register->user_id = $workplace->user_id; //пользователь
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM; //статус
                $register->register_at = $workplace->last_activity; //время регистрации = времени последней активности
                $register->save();

                //добавляем пользователя в массив по условию
                if (!in_array($workplace->user_id, $users))
                {
                    $users[$workplace->user_id]['id'] = $workplace->user_id;
                    $users[$workplace->user_id]['last_activity'] = $workplace->last_activity;
                }

                $workplace->user_id = null;
                $workplace->save();
            }

            //на выходе получаем массив пользователей которым надо прописать уход
            foreach ($users as $user) {
                $register = new ErpLocationWorkplaceRegister();
                // $register->workplace_id = $workplace->id; //место
                $register->user_id = $user['id']; //пользователь
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_OUT; //статус
                $register->register_at = $user['last_activity']; //время регистрации = времени последней активности
                if ($register->save())
                {
                    //здесь код для подсчета производительности человека
                    //если человек ушел тогда делаем следующее
                    //заглядываем в табличку производительности
                    $rows = ErpProductPerformance::find()
                    ->where(['user_id' => $user['id']])
                    ->all();

                    //Проверка
                    $reg = ErpLocationWorkplaceRegister::find()
                        ->where(['user_id' => $user['id']])
                        ->andWhere(['action_type' => ErpLocationWorkplaceRegister::TYPE_IN])
                        ->orderBy('register_at DESC')
                        ->one();

                    $dayTime = ($user['last_activity'] - $reg->register_at);

                    if (!$reg || ($dayTime > 60*60*18))
                    {
                        //обнуляем счетчик дневных нарядов
                        foreach ($rows as $row) {
                            $row->day_count = 0;
                            $row->save();
                        }
                    }
                    else
                    {
                        $daySum = 0;
                        foreach ($rows as $row) {
                            //сначала получим общую сумму дневных нарядов
                            $daySum += ($row->day_count ? $row->day_count :0);
                        }

                        if (!$daySum)
                            continue; //переходим к следующему пользователю
                        //на выходе имемм суммы выполненых нарядов за смену(от прихода до ухода)
                        //перебираем строки по второму разу
                        foreach ($rows as $row) {
                            //чтение
                            $fullTime = ($row->full_time ? $row->full_time :0); //вся сумма рабочего времени
                            $fullCount = ($row->count_naryads ? $row->count_naryads :0); //все выполненые наряды
                            $day_count = ($row->day_count ? $row->day_count :0); //дневное количество нарядов
                            
                            //расчет
                            $newFullTime = round($fullTime + (($dayTime/$daySum)*$day_count));
                            $newCountNaryads = $fullCount + $day_count;
                            $newAverageTime = $newCountNaryads ?round($newFullTime / $newCountNaryads) : 0;

                            //запись
                            $row->count_naryads = $newCountNaryads;
                            $row->full_time = $newFullTime;
                            $row->day_count = 0;
                            $row->average_time = $newAverageTime;
                            $row->save();
                        }
                    }
                }
            }
        }
    }

    public function getTimeIn()
    {
        $registerIn = ErpLocationWorkplaceRegister::find()->where(['user_id'=>$this->user_id, 'action_type'=>ErpLocationWorkplaceRegister::TYPE_IN])->orderBy('id DESC')->one();
        if(isset($registerIn) && $registerIn->register_at && $registerIn->register_at > time()-(60*60*15))
        {
            return $registerIn->register_at;
        }
    }
}
