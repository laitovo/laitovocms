<?php

namespace backend\modules\laitovo\models;

use common\models\laitovo\Config;
use common\models\laitovo\ErpNaryadLog;
use core\logic\DatePeriodManager;
use core\logic\UpnGroup;
use Yii;
use yii\db\Expression;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%laitovo_erp_location}}".
 */
class ErpLocation extends \common\models\laitovo\ErpLocation
{
    public $window;
    public $windowType;
    public $tkan;
    public $sort;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['name'], 'required'],
            [['name'], 'unique'],
            [['type'], 'required'],
            [['json'], 'string'],
            [['name', 'status', 'type'], 'string', 'max' => 255],
            [['window', 'windowType', 'tkan'], 'safe'],
        ];
    }

    public function getNaryadTimeout()
    {
        $config = Config::findOne(2);
        $part1 =  ErpNaryad::find()->where(['and',
                ['is not','order_id', null],
                ['location_id' => $this->id],
                ['<', 'created_at', time() - $config->json('timeout') * 60],
            ])->count();

        $part2 = ErpNaryad::find()->where(['and',
                ['is not','order_id', null],
                ['<', 'created_at', time() - $config->json('timeout') * 60],
                ['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]],
                ['start' => $this->id],
            ])->count();
        return $part1 + $part2;
    }

    public function getNaryadsCreated($dateFrom, $dateTo)
    {
        #Передаем даты в интервале между которыми будем искать наряды
        #Получаем все наряды, которые необходимо достать из базы данных.
        $dateTo += (60 * 60 * 24 - 1);

        $query = ErpNaryadLog::find()
            ->where(['locationFromId' => $this->id])
            ->andWhere(['>=','date',$dateFrom])
            ->andWhere(['<=','date',$dateTo]);

        if ($this->id == Yii::$app->params['erp_shveika']) {
            $stochkaIds = ErpNaryad::find()->select('id')->where(['stochka' => true])->column();

            if (!empty($stochkaIds))
                $query->andWhere(['not in', 'workOrderId',$stochkaIds]);
        }

        return $query->count();
    }

    public function getNaryadsCreatedAF($dateFrom, $dateTo)
    {
        #Передаем даты в интервале между которыми будем искать наряды
        #Получаем все наряды, которые необходимо достать из базы данных.
        $dateTo += (60 * 60 * 24 - 1);

        $ids =  ErpNaryadLog::find()
            ->select(['workOrderId'])
            ->where(['locationFromId' => $this->id])
            ->andWhere(['>=','date',$dateFrom])
            ->andWhere(['<=','date',$dateTo])
            ->column();

        return ErpNaryad::find()->where(['id' => $ids])->andWhere(['like','article','OT-2079-%',false])->count();
    }

    /**
     * Получение списка сданных нарядов в разрезе работников и количества
     *
     * @param $dateFrom
     * @param $dateTo
     * @return ArrayDataProvider
     */
    public function getNaryadsCreatedByUser($dateFrom, $dateTo)
    {
        $dateTo += (60 * 60 * 24 - 1);

        $models =  ErpNaryadLog::find()
            ->select(['workerName as user', new Expression('count(workerId) as count'),'workerId'])
            ->where(['locationFromId' => $this->id])
            ->andWhere(['>=','date',$dateFrom])
            ->andWhere(['<=','date',$dateTo])
            ->groupBy('workerId')
            ->asArray()
            ->all();

        return new ArrayDataProvider([
            'allModels' => $models,
            'sort' => [
                'attributes' => ['user', 'count'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    /**
     * Получение списка сданных нарядов в разрезе работников и количества
     *
     * @param $dateFrom
     * @param $dateTo
     * @return ArrayDataProvider
     */
    public function getNaryadsCreatedByUserAF($dateFrom, $dateTo)
    {
        $dateTo += (60 * 60 * 24 - 1);

        $models =  ErpNaryadLog::find()->alias('log')
            ->leftJoin(ErpNaryad::tableName(),ErpNaryad::tableName() . '.id = log.workOrderId')
            ->select(['log.workerName as user', new Expression('count(log.workerId) as count'),'workerId'])
            ->where(['log.locationFromId' => $this->id])
            ->andWhere(['>=','log.date',$dateFrom])
            ->andWhere(['<=','log.date',$dateTo])
            ->andWhere(['like',ErpNaryad::tableName() . '.article','OT-2079-%',false])
            ->groupBy('log.workerId')
            ->asArray()
            ->all();

        return new ArrayDataProvider([
            'allModels' => $models,
            'sort' => [
                'attributes' => ['user', 'count'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }


    public function getArrayNaryadsCreatedByUserList($dateFrom, $dateTo, $user)
    {
        #Передаем даты в интервале между которыми будем искать наряды
        #Получаем все наряды, которые необходимо достать из базы данных.
        $dateTo += (60 * 60 * 24 - 1);

        $workOrderIds =  ErpNaryadLog::find()
            ->select(['workOrderId'])
            ->where(['locationFromId' => $this->id])
            ->andWhere(['>=','date',$dateFrom])
            ->andWhere(['<=','date',$dateTo])
            ->andWhere(['workerName' => $user])
            ->column();

        return new ArrayDataProvider([
            'allModels' => ErpNaryad::find()->where(['id' => $workOrderIds])->all(),
            'sort' => [
                'attributes' => ['id', 'username', 'email'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    public function getArrayNaryadsCreatedByUserListAF($dateFrom, $dateTo, $user)
    {
        #Передаем даты в интервале между которыми будем искать наряды
        #Получаем все наряды, которые необходимо достать из базы данных.
        $dateTo += (60 * 60 * 24 - 1);

        $workOrderIds =  ErpNaryadLog::find()->alias('log')
            ->leftJoin(ErpNaryad::tableName(),ErpNaryad::tableName() . '.id = log.workOrderId')
            ->select(['workOrderId'])
            ->where(['log.locationFromId' => $this->id])
            ->andWhere(['>=','log.date',$dateFrom])
            ->andWhere(['<=','log.date',$dateTo])
            ->andWhere(['like',ErpNaryad::tableName() . '.article','OT-2079-%',false])
            ->andWhere(['workerName' => $user])
            ->column();

        return new ArrayDataProvider([
            'allModels' => ErpNaryad::find()->where(['id' => $workOrderIds])->all(),
            'sort' => [
                'attributes' => ['id', 'username', 'email'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }


    public function workOrdersWasStart($dateFrom, $dateTo)
    {
        $count = 0;
        //Все наряды, с которыми происходило движение за период.
        $workOrders = ErpNaryad::find()
            ->where(['>', 'updated_at', $dateFrom])
            ->andWhere(['<', 'updated_at', time()])
            ->orderBy('id')
            ->all();

        //Выходим если нет ни одного наряда.
        if (!$workOrders) return $count;

        //Здесь начинаем набирать необходимую нам статистику.
        foreach ($workOrders as $workOrder) {
            //Получаем необходимое поле
            $log = $workOrder->json('log');
            //Нам нужно отработать несколько условий.
            //1. Условие. У которых по схеме дальше наш участок.
            if (($scheme = $workOrder->scheme) && ($scheme->getStartFrom() == $this->id) && $workOrder->created_at > $dateFrom && $workOrder->created_at < $dateTo + (60 * 60 * 24 - 1)) {
                $count++;
                continue;
            }

            if ($workOrder->start && $workOrder->start == $this->id && $workOrder->updated_at > $dateFrom && $workOrder->updated_at < $dateTo + (60 * 60 * 24 - 1)) {
                $count++;
                continue;
            }
            //Перебираем все логи в системе
            if (!is_array($log)) continue;
            foreach ($log as $key => $row) {
                if (
                    $row['location_to'] === $this->name
                    &&
                    (
                        (
                            isset($log[($key - 1)])
                            && $log[($key - 1)]['date'] > $dateFrom
                            && $log[($key - 1)]['date'] < ($dateTo + (60 * 60 * 24 - 1))
                        )
                        ||
                        (
                            (
                                $row['location_from'] === null || !isset($log[($key - 1)])
                            )
                            && $workOrder->created_at > $dateFrom
                            && $workOrder->created_at < ($dateTo + (60 * 60 * 24 - 1))
                        )
                    )
                ) {
                    $count++;
                    break;
                }
            }
        }

        return $count;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNaryadstostart()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id']);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWholeWork()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWholeWorkOnMagnets()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['REGEXP','article','.+-.-.+-(67|71|72|70)-.+']);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWithExpressPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 2]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWithBigOrderPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 3]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWithCompletionPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 5]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWithStandardPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 6]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды для автофильтра
     */
    public function getAutoFilterWorkOrders()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['like','article','%OT-2079%', false]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды для автофильтра
     */
    public function getLBWorkOrders()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['or',['like','article','%OT-1189%', false],['like','article','%OT-1190%', false]]);
    }



    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWithMainPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 4]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWithLowPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 7]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды - переделки
     */
    public function getNaryadsRework()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['rework' => 1]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNaryadsByPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->select(new Expression("COUNT('id')"))
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->groupBy('sort')
            ->indexBy('sort')
            ->column();
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды со склада отстойника
     */
    public function getNaryadsFromSump()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['isFromSump' => true]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды со склада отстойника
     */
    public function getNaryadsForStorage()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['order_id' => null]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWithAutoSpeed()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['autoSpeed' => 1]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsWithCompletionFlag()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['isCompletion' => true]);
    }

    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые в работе и на паузе на участке, кроме отмененных
     */
    public function getNaryadsMoveAct()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere('actNumber is not null');
    }


    public function getCountNaryadsMoreOneDayInWork()
    {
        return 0;

        $all = $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['IS NOT','order_id',null]) //За исключением пополнения склада
            ->all();

        foreach ($all as $key => $one) {
            $time = DatePeriodManager::getDiffOfDates($one->created_at, time());
            $q1 = $time / (1 * 60 * 60 * 24); //Если коэффциент 1  и больше тогда, наряд более 1 дня назад был создан
            $q2 = $time / (2 * 60 * 60 * 24); //Если коффициент 1 и больше тогда, наряд более 2 дней назад был создан, если меньше, то мнее 2х дней назад
            //Тогда чтобы определить наряды от одного до 2х дней мы должны удалить наряды, которые либо меньше 1 в к1 либо больше 1 в к2
            if ($q1 < 1 || $q2 > 1) {
                unset($all[$key]);
            }
        }

        return count($all);
    }

    public function getNaryadsMoreOneDayInWork()
    {
        return [];

        $all = $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['IS NOT','order_id',null]) //За исключением пополнения склада
            ->all();

        foreach ($all as $key => $one) {
            $time = DatePeriodManager::getDiffOfDates($one->created_at, time());
            $q1 = $time / (1 * 60 * 60 * 24); //Если коэффциент 1  и больше тогда, наряд более 1 дня назад был создан
            $q2 = $time / (2 * 60 * 60 * 24); //Если коффициент 1 и больше тогда, наряд более 2 дней назад был создан, если меньше, то мнее 2х дней назад
            //Тогда чтобы определить наряды от одного до 2х дней мы должны удалить наряды, которые либо меньше 1 в к1 либо больше 1 в к2
            if ($q1 < 1 || $q2 > 1) {
                unset($all[$key]);
            }
        }

        return $all ?: [];
    }


    public static function getNaryadsWholeWorkSum($ids = [])
    {
        $query = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['!=', 'location_id', Yii::$app->params['erp_dispatcher']])
            ->andWhere(['!=', 'location_id', Yii::$app->params['erp_label']]);
        if (!empty($ids))
            $query->andWhere(['location_id' => $ids]);

        return $query->count();
    }


    /**
     * heksweb@gamil.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые на локации на паузе
     */
    public function getNaryadsInPause()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['status' => ErpNaryad::STATUS_IN_PAUSE])
            ->andWhere(['or', ['start' => null], ['start' => '']]);
    }

    /**
     * Наряды, запущенные по новой схеме работы логистики
     * @return \yii\db\ActiveQuery
     */
    public function getNaryadsWithNewScheme()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id'])
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['is_new_scheme' => true]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить наряды, которые должны выдасться на участок из облака (распечататься)
     */
    public function getNaryadsToStartFromCloud()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => '']])
            ->andWhere(['or', ['location_id' => null], ['location_id' => '']]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWholeWork()
    {
        $query = $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]]);

        if ($this->id == Yii::$app->params['erp_otk']) {
            $query->andWhere(['NOT REGEXP','article','OT-(1189|1190)-.+']);
        }

        return $query;
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWholeWorkOnMagnets()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['REGEXP','article','.+-.-.+-(67|71|72|70)-.+']);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWithExpressPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['sort' => 2]);
    }

    /**
     * test query
     *
     * @return \yii\db\ActiveQuery
     */

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNaryadsToStartByPriority()
    {
        $query = $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->select(new Expression("COUNT('id')"))
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]]);

        if ($this->id == Yii::$app->params['erp_otk']) {
            $query->andWhere(['NOT REGEXP','article','OT-(1189|1190)-.+']);
        }

        return $query->groupBy('sort')
        ->indexBy('sort')
        ->column();
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWithBigOrderPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['sort' => 3]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWithCompletionPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['sort' => 5]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWithStandardPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['sort' => 6]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getAutoFilterWorkOrdersToStart()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['like','article','%OT-2079%', false]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getLBWorkOrdersToStart()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['or',['like','article','%OT-1189%', false],['like','article','%OT-1190%', false]]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWithMainPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['sort' => 4]);
    }


    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWithLowPriority()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['sort' => 7]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartRework()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['rework' => 1]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды со склада отстойника
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartFromSump()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['isFromSump' => true]);
    }


    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды со склада отстойника
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartForStorage()
    {
        $query = $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['order_id' => null]);

        if ($this->id == Yii::$app->params['erp_otk']) {
            $query->andWhere(['NOT REGEXP','article','OT-(1189|1190)-.+']);
        }

        return $query;
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWithAutoSpeed()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['autoSpeed' => 1]);
    }
    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartWithCompletionFlag()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['isCompletion' => true]);
    }


    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые должны быть выданы на участок за исключением отмененных.
     *  В этот список входят наряды в облаке, наряды на паузе, наряды у диспетчера.
     */
    public function getNaryadsToStartMoveAct()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere('actNumber is not null');
    }

    public function getCountNaryadsMoreOneDayToStart()
    {
        return 0;

//        $all = $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
//            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
//            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
//            ->all();
//
//        foreach ($all as $key => $one) {
//            $time = DatePeriodManager::getDiffOfDates($one->created_at, time());
//            $q1 = $time / (1 * 60 * 60 * 24); //Если коэффциент 1  и больше тогда, наряд более 1 дня назад был создан
//            $q2 = $time / (2 * 60 * 60 * 24); //Если коффициент 1 и больше тогда, наряд более 2 дней назад был создан, если меньше, то мнее 2х дней назад
//            //Тогда чтобы определить наряды от одного до 2х дней мы должны удалить наряды, которые либо меньше 1 в к1 либо больше 1 в к2
//            if ($q1 < 1 || $q2 > 1) {
//                unset($all[$key]);
//            }
//        }
//
//        return count($all);
    }

    public function getNaryadsMoreOneDayToStart()
    {
        return [];

        $all = $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->all();

        foreach ($all as $key => $one) {
            $time = DatePeriodManager::getDiffOfDates($one->created_at, time());
            $q1 = $time / (1 * 60 * 60 * 24); //Если коэффциент 1  и больше тогда, наряд более 1 дня назад был создан
            $q2 = $time / (2 * 60 * 60 * 24); //Если коффициент 1 и больше тогда, наряд более 2 дней назад был создан, если меньше, то мнее 2х дней назад
            //Тогда чтобы определить наряды от одного до 2х дней мы должны удалить наряды, которые либо меньше 1 в к1 либо больше 1 в к2
            if ($q1 < 1 || $q2 > 1) {
                unset($all[$key]);
            }
        }

        return $all ?: [];
    }

    /**
     * Наряды, запущенные по новой схеме работы логистики
     * @return \yii\db\ActiveQuery
     */
    public function getNaryadsToStartWithNewScheme()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['is_new_scheme' => true]);
    }

    /**
     * heksweb@gmail.com
     * @return \yii\db\ActiveQuery
     * Discription: Получить все наряды, которые либо стоят на паузе, либо ожидают чего либо для выдачи и не могут быть выданы.
     */
    public function getNaryadsToStartInDelay()
    {
        switch ($this->id) {
            case Yii::$app->params['erp_izgib']:
                //получаем наряды на локациях изгиба и отк
                $stopTemplate= [];

                //Находим наряды которые находятся на участке отк. Лекала которые ими используются нельзя выдать.
                $naryadsLekalo = ErpNaryad::find()
                    ->select('json')
                    ->where(['and',
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['location_id' => Yii::$app->params['erp_otk']]
//                    ['or',
//                        ['location_id' => Yii::$app->params['erp_izgib']],
//                        ['location_id' => Yii::$app->params['erp_otk']]
//                    ],
                ])->all();

                foreach ($naryadsLekalo as $naryadLe) {
                    $lekalo = $naryadLe->lekalo ? (int)$naryadLe->lekalo : 0;
                    if (!in_array($lekalo, $stopTemplate))
                        $stopTemplate[] = $lekalo;
                }

                //Задержка, так как для отк нужно лекало
                ##########################################
                $otkLocation = ErpLocation::find()->where(['id' => Yii::$app->params['erp_otk']])->one();
                $otkWorkplaces = $otkLocation->recieveActiveWorkplaces();
                if ($otkWorkplaces && count($otkWorkplaces) > 1) {

                    $templateWorkOrders = ErpNaryad::find()
                        ->select(['id','logist_id','order_id','json'])
                        ->where(['and',
                            ['status'=>ErpNaryad::STATUS_IN_WORK],
                            ['start'=>Yii::$app->params['erp_otk']]
                        ])->all();

                    $resWorkOrders = [];
                    $exc = [];

                    foreach ($templateWorkOrders as $mainKey => $workOrder) {

                        if (in_array($workOrder->id,$exc)) continue;
                        //Если нету группы то пофиг
                        $upn = $workOrder->upn;
                        if (!$upn || !$upn->group) {
                            $resWorkOrders[]= $workOrder;
                            continue;
                        }
                        //Если в группе только 1 позиция то пофиг
                        $group = UpnGroup::getAnotherFromGroup($upn);
                        if (empty($group)) {
                            $resWorkOrders[]= $workOrder;
                            continue;
                        }
                        //Если в группе больше 1 ой позиции, тогда смотрим, а имеем ли мы прав

                        $ids = ArrayHelper::map($group,'id','id');
                        $groupWorkOrders = ErpNaryad::find()
                            ->where(['and',
                                ['order_id' => $workOrder->order_id],
                                ['in','logist_id',$ids],
                                ['start' => Yii::$app->params['erp_otk']],
                                ['status' => ErpNaryad::STATUS_IN_WORK],
                                ['location_id' => Yii::$app->params['erp_dispatcher']],
                            ])
                            ->all();

                        if (count($ids) !== count($groupWorkOrders)) {
                            $searchExist = ErpNaryad::find()
                                ->where(['and',
                                    ['order_id' => $workOrder->order_id],
                                    ['in','logist_id',$ids],
                                    ['and',
                                        ['or',
                                            ['and',['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                                            ['status' => null],
                                        ],
                                    ],
                                    ['or',['!=', 'location_id', Yii::$app->params['erp_otk']],['location_id' => null]],
                                    ['or',['!=', 'start', Yii::$app->params['erp_otk']],['start' => null]],
                                ])
                                ->count();
                            if (!$searchExist) {
                                $resWorkOrders[] = $workOrder;
                                foreach ($groupWorkOrders as $row) {
                                    $resWorkOrders[] = $row;
                                    if (!in_array($row->id,$exc)) $exc[] = $row->id;
                                }
                            }
                        }else{
                            $resWorkOrders[] = $workOrder;
                            foreach ($groupWorkOrders as $row) {
                                $resWorkOrders[] = $row;
                                if (!in_array($row->id,$exc)) $exc[] = $row->id;
                            }
                        }
                    }

                    $templates = [];

                    foreach ($resWorkOrders as $templateWorkOrder) {
                        $lekalo = $templateWorkOrder->lekalo ? (int)$templateWorkOrder->lekalo : 0;
                        if (isset($templates[$lekalo])) {
                            $templates[$lekalo]++;
                            if ($templates[$lekalo] > 10)
                                if (!in_array($lekalo, $stopTemplate))
                                    $stopTemplate[] = $lekalo;
                        } else {
                            $templates[$lekalo] = 1;
                        }
                    }
                }

                ##########################################
                //

                //Получаем наряды, которые должны стартовать с текущей локации кроме отмененных
                //и нарядов на паузе
                $naryadsStart = $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
                    ->select(['id','json'])
                    ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK]])
                    ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
                    ->all();

                $idYes = [];
                foreach ($naryadsStart as $naryadSt) {
                    $lekalo = $naryadSt->lekalo ? (int)$naryadSt->lekalo : 0;
                    if (in_array($lekalo, $stopTemplate)) {
                        $idYes[] = $naryadSt->id;
                    }
                }

                return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
                    ->orWhere(['status' => ErpNaryad::STATUS_IN_PAUSE])
                    ->orWhere(['and', ['or',['status' => null], ['status' => ErpNaryad::STATUS_IN_WORK]], ['IN', 'id', $idYes]]);

            case Yii::$app->params['erp_okleika']:
                $ids = $this->getAllNaryadsForPlanPropertyArrayOkleikaToStartIds();

                return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
                    ->orWhere(['and',
                        ['status' => ErpNaryad::STATUS_IN_PAUSE],
                        ['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]]
                    ])
                    ->orWhere(['id' => $ids]);
                //Надо добавить поиск по свойствам столов и тканям к выдаче

            case Yii::$app->params['erp_otk']:
                //получаем наряды на локациях изгиба и отк
                $stopTemplate = [];

                $naryadsLekalo = ErpNaryad::find()
                    ->select('json')
                    ->where(['and',
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['location_id' => Yii::$app->params['erp_izgib']],
//                    ['or',
//                        ['location_id' => Yii::$app->params['erp_izgib']],
//                        ['location_id' => Yii::$app->params['erp_otk']]
//                    ],
                ])->all();

                foreach ($naryadsLekalo as $naryadLe) {
                    $lekalo = $naryadLe->lekalo ? (int)$naryadLe->lekalo : 0;
                    if (!in_array($lekalo, $stopTemplate))
                        $stopTemplate[] = $lekalo;
                }
                //Получаем наряды, которые должны стартовать с текущей локации кроме отмененных
                //и нарядов на паузе
                $naryadsStart = $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
                    ->select(['id','json','article','logist_id','order_id'])
                    ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK]])
                    ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
                    ->all();

                $idYes = [];
                foreach ($naryadsStart as $naryadSt) {
                    #Установили флаг - тру означает что все ок и он не в задержке
                    $flag = true;
                    #Если есть свопадение по лекалу, то он в задержке
                    $lekalo = $naryadSt->lekalo ? (int)$naryadSt->lekalo : 0;
                    if (in_array($lekalo, $stopTemplate)) {
                        $idYes[] = $naryadSt->id;
                        continue;
                    }

                    #Проверяем на комплектацию
                    if ($naryadSt->window && $naryadSt->window !== 'OT' && $flag) {
                        //Если нету группы то пофиг
                        $upn = $naryadSt->upn;
                        if (!$upn || !$upn->group) continue;

                        //Если в группе только 1 позиция то пофиг
                        $upnGroup = $upn->group;
                        $group = UpnGroup::getAnotherFromGroup($upn);

                        if (empty($group)) continue;

                        $ids = ArrayHelper::map($group, 'id', 'id');
                        $groupWorkOrders = ErpNaryad::find()
                            ->where(['and',
                                ['order_id' => $naryadSt->order_id],
                                ['in', 'logist_id', $ids],
                                ['start' => Yii::$app->params['erp_otk']],
                                ['status' => ErpNaryad::STATUS_IN_WORK],
                                ['location_id' => Yii::$app->params['erp_dispatcher']],
                            ])
                            ->all();

                        if (count($ids) == count($groupWorkOrders)) continue;

                        $searchExist = ErpNaryad::find()
                            ->where(['and',
                                ['order_id' => $naryadSt->order_id],
                                ['in', 'logist_id', $ids],
                                ['and', ['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                                ['or', ['!=', 'location_id', Yii::$app->params['erp_otk']], ['location_id' => null]],
                                ['or', ['!=', 'start', Yii::$app->params['erp_otk']], ['start' => null]],
                            ])
                            ->count();

                        if ($searchExist) {
                            $flag = false;
                        }
                    }

                    if (!$flag) $idYes[] = $naryadSt->id;
                }

                return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
                    ->orWhere(['status' => ErpNaryad::STATUS_IN_PAUSE])
                    ->orWhere(['and', ['status' => ErpNaryad::STATUS_IN_WORK], ['IN', 'id', $idYes]]);

            default:
                return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
                    ->andWhere(['status' => ErpNaryad::STATUS_IN_PAUSE])
                    ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]]);
        }
    }

    public function getNaryadsToStartInPause()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id'])
            ->andWhere(['status' => ErpNaryad::STATUS_IN_PAUSE])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]]);
    }

    public function searchViewOnLocation($params)
    {
        $query = $this->getNaryadsWholeWork();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params)) {
            if ($this->window) $query->andFilterWhere(['like', 'article', $this->window . '-%-%-%-%', false]);
            if ($this->tkan) $query->andFilterWhere(['like', 'article', '%-%-%-%-' . $this->tkan, false]);
            if ($this->windowType) $query->andFilterWhere(['like', 'article', '%-%-%-' . $this->windowType . '-%', false]);
            if ($this->sort) $query->andFilterWhere(['sort' => $this->sort]);

            return $dataProvider;
        }

        return $dataProvider;
    }

    public function searchViewNotGiven($params)
    {
        $query = $this->getNaryadsToStartWholeWork();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params)) {
            if ($this->window) $query->andFilterWhere(['like', 'article', $this->window . '-%-%-%-%', false]);
            if ($this->tkan) $query->andFilterWhere(['like', 'article', '%-%-%-%-' . $this->tkan, false]);
            if ($this->windowType) $query->andFilterWhere(['like', 'article', '%-%-%-' . $this->windowType . '-%', false]);
            if ($this->sort) $query->andWhere(['sort' => $this->sort]);
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchViewOverdue($params)
    {
        $config = Config::findOne(2);
        $query = ErpNaryad::find()->where(['or',
            ['and',
                ['location_id' => $this->id],
                ['<', 'created_at', time() - $config->json('timeout') * 60],
            ],
            ['and',
                ['<', 'created_at', time() - $config->json('timeout') * 60],
                ['location_id' => null],
                ['or', ['status' => null], ['status' => '']],
                ['start' => $this->id],
            ]
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params)) {
            if ($this->window) $query->andFilterWhere(['like', 'article', $this->window . '-%-%-%-%', false]);
            if ($this->tkan) $query->andFilterWhere(['like', 'article', '%-%-%-%-' . $this->tkan, false]);
            if ($this->windowType) $query->andFilterWhere(['like', 'article', '%-%-%-' . $this->windowType . '-%', false]);
            if ($this->sort) $query->andFilterWhere(['sort' => $this->sort]);
            return $dataProvider;
        }
        return $dataProvider;

    }


    public function searchViewNaryads($params)
    {
        $query = $this->getNaryads();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params)) {
            if ($this->window) $query->andFilterWhere(['like', 'article', $this->window . '-%-%-%-%', false]);
            if ($this->tkan) $query->andFilterWhere(['like', 'article', '%-%-%-%-' . $this->tkan, false]);
            if ($this->windowType) $query->andFilterWhere(['like', 'article', '%-%-%-' . $this->windowType . '-%', false]);
            if ($this->sort) $query->andFilterWhere(['sort' => $this->sort]);
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchViewStartNaryads($params)
    {
        $query = $this->getNaryadstostart();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params)) {
            if ($this->window) $query->andFilterWhere(['like', 'article', $this->window . '-%-%-%-%', false]);
            if ($this->tkan) $query->andFilterWhere(['like', 'article', '%-%-%-%-' . $this->tkan, false]);
            if ($this->windowType) $query->andFilterWhere(['like', 'article', '%-%-%-' . $this->windowType . '-%', false]);
            if ($this->sort) $query->andFilterWhere(['sort' => $this->sort]);
            return $dataProvider;
        }
        return $dataProvider;
    }


    public function recieveActiveWorkplaces()
    {
        $activeWorkplaces = ErpLocationWorkplace::find()
            ->where(['and',
                ['location_id' => $this->id],
                ['activity' => 1],
                ['is not', 'user_id', NULL],
            ])
            ->all();

        return $activeWorkplaces ? $activeWorkplaces : null;

    }

    public function recieveActiveUsersCount()
    {
        //ищем всех пользователей, которые находятся на активных рабочих местах локации
        $activeWorkplaces = $this->recieveActiveWorkplaces();

        $users = [];

        if ($activeWorkplaces) {
            foreach ($activeWorkplaces as $activeWorkplace) {
                if ($activeWorkplace->user_id && !in_array($activeWorkplace->user_id, $users))
                    $users[] = $activeWorkplace->user_id;
            }
        }

        return count($users) ? count($users) : 0;
    }

    public static function recieveActiveUsersCountSum($ids = [])
    {
        //ищем всех пользователей, которые находятся на активных рабочих местах локации
        $activeWorkplacesQuery = ErpLocationWorkplace::find()
            ->joinWith('location l')
            ->where(['and',
                ['activity' => 1],
                ['is not', 'user_id', NULL],
                ['l.type' => ErpLocation::TYPE_PRODUCTION]
            ]);

        if (!empty($ids))
            $activeWorkplacesQuery->andWhere(['l.id' => $ids]);

        $activeWorkplaces = $activeWorkplacesQuery->all();

        $users = [];

        if ($activeWorkplaces) {
            foreach ($activeWorkplaces as $activeWorkplace) {
                if ($activeWorkplace->user_id && !in_array($activeWorkplace->user_id, $users))
                    $users[] = $activeWorkplace->user_id;
            }
        }

        return count($users) ? count($users) : 0;
    }

    /*Функция для определния нарядов, которые стартавали с данного участка за определенный период*/
    public function recieveCurentStartCount($dateFrom, $dateTo)
    {
        $dateFrom = Yii::$app->formatter->asTimestamp($dateFrom);
        $dateTo = Yii::$app->formatter->asTimestamp($dateTo);
        //получим все наряды, которые были созданы за определенный период
        $creatingNaryads = ErpNaryad::find()
            ->where(['and',
                ['>', 'created_at', $dateFrom],
                ['<', 'created_at', $dateTo + (60 * 60 * 24 - 1)],
            ])
            ->all();

        //получив все созданные наряды за определенный период мы их должны обойти, выбрав из них наряды, которые находятся в облаке на данном участке
        //а также наряды которые уже выданы на участок 

        $count = 0;
        foreach ($creatingNaryads as $creatingNaryad) {
            //проверяем статус его и старт. Если ста
            if (($scheme = $creatingNaryad->scheme) && ($scheme->getStartFrom() == $this->id)) {
                $count++;
            }
        }
        //возвращаем счетчик
        return $count;
    }


    static function locationList()
    {
        $location = self::find()->all();
        if (isset($location))
            return ArrayHelper::merge(['' => ''], ArrayHelper::map($location, 'id', 'name'));
    }


    static function getCountNaryadsWithMainPriority()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 4])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['sort' => 4])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsFromSump()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['isFromSump' => true])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['isFromSump' => true])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsForStorage()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['order_id' => null])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['order_id' => null])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsWithLowPriority()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 7])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['sort' => 7])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsWithExpressPriority()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 2])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['sort' => 2])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsWithBigOrderPriority()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 3])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['sort' => 3])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsWithCompletionPriority()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 5])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['sort' => 5])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsWithStandardPriority()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['sort' => 6])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['sort' => 6])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsRework()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['rework' => 1])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['rework' => 1])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsWithAutoSpeed()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['autoSpeed' => 1])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['autoSpeed' => 1])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountNaryadsMoveAct()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere('actNumber is not null')
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere('actNumber is not null')
            ->count();

        return $inWait + $inWork;
    }

    static function getCountAutoFilterWorkOrders()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['like','article','%OT-2079%', false])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['like','article','%OT-2079%', false])
            ->count();

        return $inWait + $inWork;
    }

    static function getCountLBWorkOrders()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['or',['like','article','%OT-1189%', false],['like','article','%OT-1190%', false]])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['or',['like','article','%OT-1189%', false],['like','article','%OT-1190%', false]])
            ->count();

        return $inWait + $inWork;
    }


    static function getCountNaryadsMoreOneDay()
    {
        return 0;

        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->all();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->all();

        $all = ArrayHelper::merge($inWait, $inWork);
        foreach ($all as $key => $one) {
            $time = DatePeriodManager::getDiffOfDates($one->created_at, time());
            $q1 = $time / (1 * 60 * 60 * 24); //Если коэффциент 1  и больше тогда, наряд более 1 дня назад был создан
            $q2 = $time / (2 * 60 * 60 * 24); //Если коффициент 1 и больше тогда, наряд более 2 дней назад был создан, если меньше, то мнее 2х дней назад
            //Тогда чтобы определить наряды от одного до 2х дней мы должны удалить наряды, которые либо меньше 1 в к1 либо больше 1 в к2
            if ($q1 < 1 || $q2 > 1) {
                unset($all[$key]);
            }
        }

        return count($all);
    }

    static function getCountNaryadsWithNewScheme()
    {
        $inWork = ErpNaryad::find()
            ->andWhere(['or', ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['start' => null], ['start' => '']])
            ->andWhere(['is_new_scheme' => true])
            ->count();

        $inWait = ErpNaryad::find()
            ->andWhere(['or', ['status' => null], ['status' => ''], ['status' => ErpNaryad::STATUS_IN_WORK], ['status' => ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or', ['location_id' => null], ['location_id' => ''], ['location_id' => Yii::$app->params['erp_dispatcher']]])
            ->andWhere(['and', ['IS NOT', 'start', null], ['!=', 'start', '']])
            ->andWhere(['is_new_scheme' => true])
            ->count();

        return $inWait + $inWork;
    }

    public function getAllNaryadsForPlan()
    {
        $result = $this->getNaryadsWholeWork()->count() + $this->getNaryadsToStartWholeWork()->count();
        return $result ? $result : 0;
    }

    public function getAllNaryadsForPlanOnMagnets()
    {
        $result = $this->getNaryadsWholeWorkOnMagnets()->count() + $this->getNaryadsToStartWholeWorkOnMagnets()->count();
        return $result ? $result : 0;
    }

    public function getAllNaryadsForPlanPropertyArrayOkleika()
    {
        $resultArr = [];
        $loc = [];
        $start = [];
        $onLocation = $this->getNaryadsWholeWork()->all();
        $forStart = $this->getNaryadsToStartWholeWork()->all();

        if ($onLocation)
            foreach ($onLocation as $naryad) {
                if ( (($articleTkan = $naryad->articleTkan) == 5) && ($naryad->windowBW() || $naryad->isNoName()) )
                    $articleTkan = '5C';
                elseif ($articleTkan == 5)
                    $articleTkan = '5T';

                @$resultArr[$articleTkan]++;
                @$loc[$articleTkan]++;

                if (preg_match('/FD-\w+-\d+-\d+-2/', $naryad->article) === 1) {
                    $articleTkan = '2ПБ';
                    @$resultArr[$articleTkan]++;
                    @$loc[$articleTkan]++;
                }
            }

        if ($forStart)
            foreach ($forStart as $naryad) {
                if ( (($articleTkan = $naryad->articleTkan) == 5) && ($naryad->windowBW() || $naryad->isNoName()) )
                    $articleTkan = '5C';
                elseif ($articleTkan == 5)
                    $articleTkan = '5T';

                @$resultArr[$articleTkan]++;
                @$start[$articleTkan]++;

                if (preg_match('/FD-\w+-\d+-\d+-2/', $naryad->article) === 1) {
                    $articleTkan = '2ПБ';
                    @$resultArr[$articleTkan]++;
                    @$start[$articleTkan]++;
                }
            }

        natsort($resultArr);

        $rename = [
            4 => '1.5 La',
            2 => '2 La',
            8 => '1.25 La',
            3 => 'Бифл'
        ];

        $resultStr = '';
        foreach ($resultArr as $key => $value) {
            if ($key !== '2ПБ')
                $resultStr .= '<span style="font-weight:bold;font-size:1.5em">Ткань: ' . $key . (isset($rename[$key]) ? ' [' . $rename[$key] . '] ' : '') . '-' . $value . ' шт. (<span data-toggle="tooltip" data-original-title="Находятся на участке">' . (@$loc[$key] ?? 0) . '</span> - <span data-toggle="tooltip" data-original-title="На диспетчере">'. (@$start[$key] ?? 0) .'</span>)</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        if (isset($resultArr['2ПБ']))
            $resultStr .= '<br><span style="font-weight:bold;font-size:1.5em; color: #0b96e5;">Ткань: ' . '2 (Передние боковые)' . ' - ' . $resultArr['2ПБ'] . ' шт. (<span data-toggle="tooltip" data-original-title="Находятся на участке">' . (@$loc['2ПБ'] ?? 0) . '</span> - <span data-toggle="tooltip" data-original-title="На диспетчере">'. (@$start['2ПБ'] ?? 0) .'</span>)</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

        return @$resultStr;
    }

    public function getAllNaryadsForPlanPropertyArrayOkleikaToStartIds()
    {
        $propertiesWorkplace = [];
        $properties = [];
        $naryadTkan = [];
        $tkanArray = [];
        $forStart = $this->getNaryadsToStartWholeWork()->select(['id','article'])->all();

        if ($forStart)
            foreach ($forStart as $naryad) {
                $tkan = $naryad->articleTkan;
                $naryadTkan[$tkan][] = $naryad->id;
                if (!in_array($tkan, $tkanArray))
                    $tkanArray[] = $tkan;
            }

        $workplaces = $this->recieveActiveWorkplaces();
        if ($workplaces) {
            foreach ($workplaces as $workplace) {
                if ($workplace->property == 'Все ткани')
                    return [];
                $propertiesWorkplace[] = $workplace->property;
            }

            if (count($propertiesWorkplace))
                foreach ($propertiesWorkplace as $stringProperty) {
                    $list = explode(',', $stringProperty);
                    foreach ($list as $key => $value) {
                        $properties[] = intval(trim($value));
                    }
                }
        }

        $diff = array_diff($tkanArray,$properties);
        $ids = [];
        foreach ($diff as $value) {
            $ids = array_merge($ids, $naryadTkan[$value]);
        }

        return $ids;
    }


}
