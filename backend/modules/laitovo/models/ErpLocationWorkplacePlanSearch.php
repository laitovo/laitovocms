<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use backend\modules\laitovo\models\ErpLocationWorkplacePlan;

/**
 * ErpLocationWorkplacePlanSearch represents the model behind the search form about `backend\modules\laitovo\models\ErpLocationWorkplacePlan`.
 */
class ErpLocationWorkplacePlanSearch extends ErpLocationWorkplacePlan
{
    private $curDate;
    private $curDateMin;
    private $curDateMax;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'workplace_id', 'user_id', 'time_from', 'time_to', 'created_at', 'author_id'], 'integer'],
            [['curDate','curDateMax','curDateMin'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getCurDate()
    {
        return $this->curDate ? $this->curDate : mktime(0,0,0);
    }


    public function setCurDate($value)
    {
        $this->curDate = Yii::$app->formatter->asTimestamp($value);
    }

    public function getCurDateMin()
    {
        return $this->curDate ? mktime(0, 0, 0, date("m", $this->curDate), date("d", $this->curDate), date("y", $this->curDate)): mktime(0, 0, 0);
    }

    public function getCurDateMax()
    {
        return $this->curDate ? mktime(23, 59, 59, date("m", $this->curDate), date("d", $this->curDate), date("y", $this->curDate)): mktime(23, 59, 59) ;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'curDate' => 'Дата планирования'
            ]
        );
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErpLocationWorkplacePlan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andWhere(['and',
            // ['>','time_from',Yii::$app->formatter->asTimestamp($this->getCurDateMin())],
            // ['<','time_from',Yii::$app->formatter->asTimestamp($this->getCurDateMax())],
            // ['>','time_to',Yii::$app->formatter->asTimestamp($this->getCurDateMin())],
            ['<','time_to',Yii::$app->formatter->asTimestamp($this->getCurDateMax())],
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'workplace_id' => $this->workplace_id,
            'user_id' => $this->user_id,
            'time_to' => $this->time_to,
            'created_at' => $this->created_at,
            'author_id' => $this->author_id,
        ]);

        return $dataProvider;
    }
}
