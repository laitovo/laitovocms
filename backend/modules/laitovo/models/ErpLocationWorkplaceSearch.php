<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\laitovo\models\ErpLocationWorkplace;

/**
 * ErpLocationWorkplaceSearch represents the model behind the search form about `backend\modules\laitovo\models\ErpLocationWorkplace`.
 */
class ErpLocationWorkplaceSearch extends ErpLocationWorkplace
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'location_id', 'location_number', 'user_id','type_id'], 'integer'],
            // [[], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErpLocationWorkplace::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                // количество пунктов на странице
                'pageSize' => 100,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'location_id' => $this->location_id,
            'location_number' => $this->location_number,
            'user_id' => $this->user_id,
            'type_id' => $this->type_id,
        ]);

        return $dataProvider;
    }


    public function getActiveCountProduction()
    {
         $count = ErpLocationWorkplace::find()->joinWith(['location l'])->where(['l.type'=>ErpLocation::TYPE_PRODUCTION, 'activity'=>true])->select('user_id')->groupBy('user_id')->all();
         return count($count);
    }

    public function getActiveCountSupport()
    {
        $count = ErpLocationWorkplace::find()->joinWith(['location l'])->where(['l.type'=>ErpLocation::TYPE_SUPPROT, 'activity'=>true])->select('user_id')->groupBy('user_id')->all();
        return count($count);
    }

    public function locationActiveCount($location_id)
    {
        $count = ErpLocationWorkplace::find()->joinWith(['location l'])->where(['activity'=>true, 'location_id'=>$location_id])->select('user_id')->groupBy('user_id')->all();
        return count($count) ;
    }
}
