<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\laitovo\models\ErpNaryad;


class LocationReportSearch extends Model
{
    public $dateFrom;
    public $dateTo;
    public $location;

    public $article;
    public $status;
    public $count;
    public $window;
    public $windowType;
    public $tkan;
    public $sort;


    public function rules()
    {
       return [

           [[  'window', 'windowType', 'tkan' , 'status', 'location', 'sort' ], 'safe'],
           [['dateFrom', 'dateTo'], 'date','format'=>'dd.mm.YY'],
      ];
   }

   public function attributeLabels()
  {
      return [
          'dateFrom' => Yii::t('app', 'Начало периода *(00:00:00)'),
          'dateTo' => Yii::t('app', 'Конец периода * (23:59:59)'),
          'location' => Yii::t('app', 'Участок'),
      ];
  }

  public function scenarios()
  {
      // bypass scenarios() implementation in the parent class
      return Model::scenarios();
  }

  public function search($params)
  {
       $query = ErpNaryad::find()->where(['or',['status'=>ErpNaryad::STATUS_READY], ['status'=>ErpNaryad::STATUS_FROM_SKLAD]]);;

      if(($this->load($params) && $this->dateFrom && $this->dateTo) || ($this->dateFrom && $this->dateTo)) {

             $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom);
             $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo) + (60 * 60 * 24 - 1);
             $query -> andWhere(['and',
                  ['>', 'updated_at', $dateFrom],
                  ['<', 'updated_at', $dateTo]
              ]);
         }
         if($this->load($params) && $this->location){
           $query -> andWhere(['like','json','%"location_from":'.$this->location.'%',false]);
         }


      $dataProvider = new ActiveDataProvider([
              'query' => $query,
      ]);

      $dataProvider->setSort([
      'attributes' => [
         // 'window',
          'article',
         // 'windowType',
         // 'tkan',
          'count',
        //  'status'
          ],
      ]);

      $dp = $dataProvider;

             if($this->load($params)){
                  if ($this->window) $query->andFilterWhere(['like', 'article',$this->window.'-%-%-%-%',false]);
                  if ($this->tkan)$query->andFilterWhere(['like', 'article','%-%-%-%-'.$this->tkan, false]);
                  if ($this->windowType)$query->andFilterWhere(['like', 'article','%-%-%-'.$this->windowType.'-%', false]);
                  if ($this->status)$query->andFilterWhere(['like', 'status', $this->status]);
                  if ($this->sort)$query->andFilterWhere(['sort'=>$this->sort]);

                  $dp = $dataProvider;
            }

    $ts = $query->count();
    $result = [];
    $result['dataProvider'] = $dp;
    $result['totalSumm'] = $ts;
    return $result;
  }
}
