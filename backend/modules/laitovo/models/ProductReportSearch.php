<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\laitovo\models\ErpNaryad;


class ProductReportSearch extends Model
{

       public $dateFrom;
       public $dateTo;
       public $country;



       public $article;
       public $status;
       public $count;
       public $window;
       public $windowType;
       public $tkan;
       public $sort;

       public $article1;
       public $status1;
       public $count1;
       public $window1;
       public $windowType1;
       public $tkan1;
       public $sort1;

       public $count2;
       public $window2;
       public $windowType2;
       public $tkan2;
       public $article2;
       public $status2;
       public $sort2;


       public function rules()
    {
        return [

            [[  'window', 'windowType', 'tkan' , 'status','sort', 'country' ], 'safe'],
            [[  'window1', 'windowType1', 'tkan1' , 'status1', 'count1', 'sort1' ], 'safe'],
            [[  'status2', 'sort2' ], 'safe'],
            [['dateFrom', 'dateTo'], 'date','format'=>'dd.mm.YY'],

        ];
    }

     public function attributeLabels()
    {
        return [
            'dateFrom' => Yii::t('app', 'Начало периода *(00:00:00)'),
            'dateTo' => Yii::t('app', 'Конец периода * (23:59:59)'),
            'country' => Yii::t('app', 'Страна'),
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getTotalSumm($query)
    {
        $naryads = $query->all();
        $totalCount = 0;
        foreach ($naryads as $naryad) {

            $totalCount += $naryad->count;
        }
        return $totalCount ?: 'Ничего не найдено';
    }


    public function searchGeneralProduct($params)
    {
         $query = ErpNaryad::find()->joinWith('order o')->where(['not like','laitovo_erp_naryad.article','%-%-%-5-%', false])
                 ->andWhere(['not like','laitovo_erp_naryad.article','%-%-%-4-%', false])
                 ->andWhere(['not like','laitovo_erp_naryad.article','%-%-%-49-%',false])
                 ->andWhere(['not like','laitovo_erp_naryad.article','%-%-%-55-%',false])
                 ->andWhere(['not like','laitovo_erp_naryad.article','OT'])
                 ->andWhere(['or',['laitovo_erp_naryad.status'=>ErpNaryad::STATUS_READY], ['laitovo_erp_naryad.status'=>ErpNaryad::STATUS_FROM_SKLAD]]);

         if(($this->load($params) && $this->dateFrom && $this->dateTo) || ($this->dateFrom && $this->dateTo)) {

               $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom);
               $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo) + (60 * 60 * 24 - 1);
               $query -> andWhere(['and',
                    ['>', 'laitovo_erp_naryad.updated_at', $dateFrom],
                    ['<', 'laitovo_erp_naryad.updated_at', $dateTo]
                ]);
         }

         if($this->country && $this->country == 2)
         {
              $query->andWhere(['like', 'o.json', '%Hamburg%', false]);
         }

         if($this->country && $this->country == 1)
         {
             $query->andWhere(['not like', 'o.json', '%Hamburg%', false]);
         }




         $query->groupBy(['laitovo_erp_naryad.article']);
         $query->select(['laitovo_erp_naryad.article',  'laitovo_erp_naryad.status', 'count'=>'COUNT(laitovo_erp_naryad.id)']);

        $dataProvider = new ActiveDataProvider([
                'query' => $query,
        ]);

        $dataProvider->setSort([
        'attributes' => [
           // 'window',
            'article',
           // 'windowType',
           // 'tkan',
            'count',
          //  'status'
            ],
        ]);

        $dp = $dataProvider;

               if($this->load($params)){
                    if ($this->window) $query->andFilterWhere(['like', 'laitovo_erp_naryad.article',$this->window.'-%-%-%-%',false]);
                    if ($this->tkan)$query->andFilterWhere(['like', 'laitovo_erp_naryad.article','%-%-%-%-'.$this->tkan, false]);
                    if ($this->windowType)$query->andFilterWhere(['like', 'laitovo_erp_naryad.article','%-%-%-'.$this->windowType.'-%', false]);
                    if ($this->status)$query->andFilterWhere(['like', 'laitovo_erp_naryad.status', $this->status]);
                   

                    $dp = $dataProvider;
              }

      $ts = $this->getTotalSumm($query);
      $result = [];
      $result['dataProvider'] = $dp;
      $result['totalSumm'] = $ts;
      return $result;
    }

     public function searchAdditionalProduct($params){

         $query = ErpNaryad::find()
                  ->joinWith('order o')
                  ->andWhere(['or',['like','laitovo_erp_naryad.article','%-%-%-4-%',false],['like','laitovo_erp_naryad.article','%-%-%-5-%',false], ['like','laitovo_erp_naryad.article','%-%-%-49-%',false] ])
                  ->andWhere(['or',['laitovo_erp_naryad.status'=>ErpNaryad::STATUS_READY], ['laitovo_erp_naryad.status'=>ErpNaryad::STATUS_FROM_SKLAD]]);

        if(($this->load($params) && $this->dateFrom && $this->dateTo) || ($this->dateFrom && $this->dateTo)) {

               $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom);
               $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo) + (60 * 60 * 24 - 1);
               $query -> andWhere(['and',
                    ['>', 'laitovo_erp_naryad.updated_at', $dateFrom],
                    ['<', 'laitovo_erp_naryad.updated_at', $dateTo]
                ]);
           }

         if($this->country && $this->country == 2)
         {
             $query->andWhere(['like', 'o.json', '%Hamburg%', false]);
         }

         if($this->country && $this->country == 1)
         {
             $query->andWhere(['not like', 'o.json', '%Hamburg%', false]);
         }

                $query->groupBy(['laitovo_erp_naryad.article', 'laitovo_erp_naryad.status']);
                $query->select(['laitovo_erp_naryad.article',  'laitovo_erp_naryad.status', 'count'=>'COUNT(laitovo_erp_naryad.id)']);

        $dataProvider = new ActiveDataProvider([
                'query' => $query,
        ]);

        $dataProvider->setSort([
        'attributes' => [
            //'window1',
            'article1'=>[
                'asc' => ['laitovo_erp_naryad.article' => SORT_ASC],
                'desc' => ['laitovo_erp_naryad.article' => SORT_DESC,]
            ],

           'count1'=>[
                'asc' => ['count' => SORT_ASC],
                'desc' => ['count' => SORT_DESC,]
            ],

            ],
        ]);

        $dp = $dataProvider;

        if($this->load($params)){
                if ($this->window1) $query->andFilterWhere(['like', 'laitovo_erp_naryad.article',$this->window1.'-%-%-%-%',false]);
                if ($this->tkan1)$query->andFilterWhere(['like', 'laitovo_erp_naryad.article','%-%-%-%-'.$this->tkan1, false]);
                if ($this->windowType1)$query->andFilterWhere(['like', 'laitovo_erp_naryad.article','%-%-%-'.$this->windowType1.'-%', false]);
                if ($this->status1)$query->andFilterWhere(['like', 'laitovo_erp_naryad.status', $this->status1]);
               

                $dp = $dataProvider;
            }
            $ts = $this->getTotalSumm($query);
            $result = [];
            $result['dataProvider'] = $dp;
            $result['totalSumm'] = $ts;
            return $result;
    }
    public function searchOtherProduct($params){

         $query = ErpNaryad::find()
                  ->joinWith('order o')
                  ->andWhere(['like','laitovo_erp_naryad.article','OT'])
                  ->andWhere(['or',['laitovo_erp_naryad.status'=>ErpNaryad::STATUS_READY], ['laitovo_erp_naryad.status'=>ErpNaryad::STATUS_FROM_SKLAD]]);

        if(($this->load($params) && $this->dateFrom && $this->dateTo) || ($this->dateFrom && $this->dateTo)) {

               $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom);
               $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo) + (60 * 60 * 24 - 1);
               $query -> andWhere(['and',
                    ['>', 'laitovo_erp_naryad.updated_at', $dateFrom],
                    ['<', 'laitovo_erp_naryad.updated_at', $dateTo]
                ]);
           }
        if($this->country && $this->country == 2)
        {
            $query->andWhere(['like', 'o.json', '%Hamburg%', false]);
        }

        if($this->country && $this->country == 1)
        {
            $query->andWhere(['not like', 'o.json', '%Hamburg%', false]);
        }

                $query->groupBy(['laitovo_erp_naryad.article', 'laitovo_erp_naryad.status']);
                $query->select(['laitovo_erp_naryad.article',  'laitovo_erp_naryad.status', 'count'=>'COUNT(laitovo_erp_naryad.id)']);

        $dataProvider = new ActiveDataProvider([
                'query' => $query,
        ]);

        $dataProvider->setSort([
        'attributes' => [

             'article2'=>[
                'asc' => ['article' => SORT_ASC],
                'desc' => ['article' => SORT_DESC,]
            ],

            'count2'=>[
                'asc' => ['count' => SORT_ASC],
                'desc' => ['count' => SORT_DESC,]
            ],

            ],
        ]);

       $dp = $dataProvider;

       if($this->load($params)){
               // if ($this->article) $query->andFilterWhere(['like', 'article','%'.$this->article.'%', false]);
                if ($this->window2) $query->andFilterWhere(['like', 'laitovo_erp_naryad.article',$this->window2.'-%-%-%-%',false]);
                if ($this->tkan2)$query->andFilterWhere(['like', 'laitovo_erp_naryad.article','%-%-%-%-'.$this->tkan2, false]);
                if ($this->windowType2)$query->andFilterWhere(['like', 'laitovo_erp_naryad.article','%-%-%-'.$this->windowType2.'-%', false]);
                if ($this->status2)$query->andFilterWhere(['like', 'laitovo_erp_naryad.status', $this->status2]);
               
                $dp = $dataProvider;
            }
      $ts = $this->getTotalSumm($query);
      $result = [];
      $result['dataProvider'] = $dp;
      $result['totalSumm'] = $ts;
      return $result;
    }


 }
