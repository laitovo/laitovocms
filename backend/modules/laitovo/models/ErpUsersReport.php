<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use common\models\User;
use common\models\laitovo\Config;

/**
 * This is the model class for table "{{%laitovo_erp_users_report}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $location_id
 * @property integer $naryad_id
 * @property integer $job_type_id
 * @property integer $job_count
 * @property double $job_rate
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property ErpJobType $jobType
 * @property ErpLocation $location
 * @property ErpNaryad $naryad
 * @property ErpUser $user
 */
class ErpUsersReport extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_users_report}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'location_id', 'naryad_id', 'job_type_id', 'created_at', 'updated_at', 'author_id', 'updater_id', 'additional_naryad_id'], 'integer'],
            [['job_rate', 'job_price', 'job_count'], 'number'],
            [['job_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpJobType::className(), 'targetAttribute' => ['job_type_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['naryad_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpNaryad::className(), 'targetAttribute' => ['naryad_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['additional_naryad_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpAdditionalWork::className(), 'targetAttribute' => ['additional_naryad_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Работник'),
            'location_id' => Yii::t('app', 'Участок'),
            'naryad_id' => Yii::t('app', 'Наряд'),
            'job_type_id' => Yii::t('app', 'Вид работ'),
            'job_count' => Yii::t('app', 'Количество'),
            'job_rate' => Yii::t('app', 'Коэффициент'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
        ];
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobType()
    {
        return $this->hasOne(ErpJobType::className(), ['id' => 'job_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNaryad()
    {
        return $this->hasOne(ErpNaryad::className(), ['id' => 'naryad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalNaryad()
    {
        return $this->hasOne(ErpAdditionalWork::className(), ['id' => 'additional_naryad_id']);
    }




    public function getNaryadSum()
    {
        $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
        //             средняя дневная норма                цена нормированного коэффициента
        if($config && $config->json('averageRate') && $config->json('hourrate')!=0 && $this->job_rate!=0 && $this->job_count)
        {
            $sum = $config->json('averageRate')/$this->job_rate*$this->job_count*$config->json('hourrate');
            return $sum ? round($sum, 2) : 0;
        }
    }

    static function getPriceNaryadUser($user_id, $date_from, $date_to)
    {
        $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
        //             средняя дневная норма                цена нормированного коэффициента
        if($config && $config->json('averageRate') && $config->json('hourrate')) {
            $reports = (new \yii\db\Query())
                ->select([
                    'SUM("' . $config->json('averageRate') . '"/report.job_rate*report.job_count*"' . $config->json('hourrate') . '") as naryad_sum'
                ])
                ->from('laitovo_erp_users_report as report')
                ->where([
                    'and',
                    ['>=', 'report.created_at', $date_from],
                    ['<=', 'report.created_at', $date_to],
                    ['report.user_id'=>$user_id]
                ])
                ->groupBy(['user_id'])
                ->one();
        }
        $reworks = (new \yii\db\Query())
            ->select([
                'SUM(rework.deduction) as deduction'])
            ->from('laitovo_rework_act as rework')
            ->where(['and',
                ['>', 'rework.created_at', $date_from],
                ['<', 'rework.created_at', $date_to],
                ['made_user_id'=>$user_id]
            ])
            ->groupBy(['made_user_id'])
            ->one();

        $total = (isset($reports['naryad_sum']) ? $reports['naryad_sum'] : 0) - (isset($reworks['deduction']) ? $reworks['deduction'] : 0 );
        return round($total,2);
    }
}
