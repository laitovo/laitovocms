<?php

namespace backend\modules\laitovo\models;

use backend\modules\laitovo\models\ErpLocation;
use common\models\laitovo\MaterialParameters;
use backend\modules\laitovo\models\ErpNaryad;




/**
 * Description of ErpMaterialReport
 *
 * @author User
 */
class ErpMaterialReport extends common\models\laitovo\ErpMaterialReport
{
   public function rules()
    {
        return [
            [['user_id', 'location_id', 'naryad_id', 'material_parameter_id', 'material_count', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['material_parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialParameters::className(), 'targetAttribute' => ['material_parameter_id' => 'id']],
            [['naryad_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpNaryad::className(), 'targetAttribute' => ['naryad_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }  
}
