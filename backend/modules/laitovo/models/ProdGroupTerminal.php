<?php

namespace backend\modules\laitovo\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

/**
 * Class ErpTerminal
 * @package backend\modules\laitovo\models
 *
 * @property ErpUser $user [ работник ]
 * @property string $uniqueWorkOrder [ Список уникальных нарядов ]
 */
class ProdGroupTerminal extends Model
{
    ############################################# Properties ###########################################################
    #########
    /**
     * @var integer|null $location_id [ уникальный идентификатор рабочего участка ( ErpLocation->id либо null ) ]
     */
    public $location_id;

    /**
     * @var integer|null $user_id [ уникальный идентификатор работника ( ErpUser->id либо null ) ]
     */
    public $user_id;

    /**
     * @var array $workOrders [ список идентификаторов нарядов, с которыми осуществляются действия ( ErpNaryad->id[] либл [] ) ]
     */
    public $workOrders = [];

    /**
     * @var array $printableWorkOrders [ список идентификаторов нарядов, которые должны быть распечатаны по отдельности формой наряда ( ErpNaryad->id[] либл [] ) ]
     */
    public $printableWorkOrders = [];

    /**
     * @var array $printRegister [ список идентификаторов нарядов, которые должны быть распечатаны в составе реестра ]
     */
    public $printRegister = [];

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'Сотрудник'),
            'workOrders' => Yii::t('app', 'Наряды'),
            'group' => Yii::t('app', 'Группировать'),
        ];
    }

    #########
    ############################################# Properties ###########################################################


    ############################################# Relations ############################################################
    #########

    /**
     * Возвращает пользователя по идентификатору
     * @return ErpUser|static
     */
    public function getUser()
    {
        return $this->user_id ? ErpUser::findOne($this->user_id) : new ErpUser;
    }

    #########
    ############################################# Relations ############################################################


    ############################################# Validation ###########################################################
    ##########
    /**
     * Список правил валидации для модели
     * - workOrders  :
     *      обязательный параметр, работа модели происходит только при выбранных нарядах,
     *      каждый из списка нарядов - целое число,
     *      должна существовать модель наряда с таким уникальным идентификатором,
     *      наряды должно быть либо со статусом null, пустая строка, в работе, старт должен быть равен текущей локации,
     * - user_id     :
     *      тип - целое число, в базе данных должен существовать работник с таким уникальным идентификатором,
     *      проверить - есть ли на текущем пользователе несданные наряды
     * - location_id : тип - целое число, в базе данных должен существовать участок с таким уникальным идентификатором
     * - group       : тип - булево значение
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workOrders'], 'required', 'message'=>'{attribute} не выбраны'],
            [['workOrders'], 'each','rule' => ['integer']],
            [['workOrders'], 'each','rule' => [
                'exist',
                'skipOnError' => true,
                'targetClass' => ErpNaryad::className(),
                'filter' => ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK],['status' => ErpNaryad::STATUS_IN_PAUSE],['start' => $this->location_id]],
                'targetAttribute' => 'id'
            ]],

            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => 'id'],
            [['user_id'], 'checkoutWorkOrdersOnUser'],

            [['location_id'], 'integer'],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * Кастомная проверка на наличие нарядов на пользователе.
     * Если на пользователе есть несданные наряды, то невозможно человеку выдать наряды
     *
     * @param $attribute
     */
    public function checkoutWorkOrdersOnUser($attribute)
    {
        if ($this->user_id && $this->location_id) {
            if ((count($workOrders = ErpNaryad::find()
                    ->where(['and',
                        ['location_id'=>$this->location_id],
                        ['status' => ErpNaryad::STATUS_IN_WORK],
                        ['user_id' => $this->user_id]
                    ])->all()) != null && $this->location_id !== Yii::$app->params['erp_okleika']))
            {
                $this->addError($attribute, Yii::t('app', 'У вас есть не сданные наряды',['attribute'=>$this->getAttributeLabel($attribute)]));
            }
        }
    }

    ##########
    ############################################# Validation ###########################################################


    ############################################# Functions ############################################################
    ##########

    /**
     * Инициализируем список нарядов для обработки из сохраненных cookies
     *
     * ErpTerminal constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->workOrders = Yii::$app->request->cookies->getValue('Erp_ProdGroupTerminal_WorkOrders')?:[];
        parent::__construct($config);
    }

    /**
     * Сохраняем список нарядов в cookies на 10 секунд
     */
    public function saveProperties()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Erp_ProdGroupTerminal_WorkOrders',
            'value' => $this->workOrders,
            'expire' => time() + 10,
        ]));
    }

    /**
     * Метод - который выдает наряды в ручную - для ручной выдачи нарядов на работы
     * @return bool
     */
    public function handOutWorkOrders()
    {
        if ($this->validate()){
            $literal = false;
           foreach ($this->workOrders as $value) {
               if ( ($workOrder = ErpNaryad::findOne($value) ) != null && !$workOrder->location_id && $workOrder->start) {
                   if (!$literal) $literal = $workOrder->recieveLiteral();
                   if (!$workOrder->literal) {
                       $workOrder->literal = $literal ? $literal : 1;
                       $workOrder->literal_date = time();
                   }
                   $workOrder->nextPlace($this->user_id);
               } elseif ( ($workOrder = ErpNaryad::findOne($value)) != null && $workOrder->location_id && $workOrder->start) {
                   $workOrder->nextPlace($this->user_id);
               }
           }
            return true;
        }
        return false;
    }

    /**
     * Метод - который выдает наряды в ручную - для ручной выдачи нарядов на работы
     * @return bool
     */
    public function moveProdGroupWorkOrders()
    {
        if ($this->validate()){
            $prodGroup = false;
           foreach ($this->workOrders as $value) {
               if ( ($workOrder = ErpNaryad::findOne($value) ) != null && !$workOrder->location_id && $workOrder->start) {
                   if (!$prodGroup) $prodGroup = $workOrder->generateprodGroupNumber();
                       $workOrder->prodGroup = $prodGroup;
                   $workOrder->nextPlace($this->user_id);
               }
           }
            return $prodGroup;
        }
        return false;
    }

    /**
     * Метод - который используется для автоматической выдачи нарядов на участок
     *  Либо распечатываются сами наряды (printableWorkOrders), либо распечатывается реестр нарядов (printRegister)
     * @return bool
     */
    public function issueAndPrintOutWorkOrders()
    {
        if ($this->validate()){
            $literal = false;
            foreach ($this->workOrders as $value) {
                if ( ($workOrder = ErpNaryad::findOne($value) ) != null && !$workOrder->location_id) {
                    if (!$literal) $literal = $workOrder->recieveLiteral();
                    if (!$workOrder->literal) {
                        $workOrder->literal = $literal ? $literal : 1;
                        $workOrder->literal_date = time();
                    }
                    $this->printableWorkOrders[] = $value;
                    $workOrder->nextPlace($this->user_id);
                } elseif ( ($workOrder = ErpNaryad::findOne($value)) != null && $workOrder->location_id) {
                    $this->printRegister[] = $value;
                    $workOrder->nextPlace($this->user_id);
                }
            }
            return true;
        }
        return false;
    }

    public function getUniqueWorkOrder()
    {
        $unique = [];
        foreach ($this->workOrders as $value) {
            $workOrder = ErpNaryad::findOne($value);
            $unique[$workOrder->article] = $workOrder;
        }

        if (empty($unique)) return null;
        $res = ArrayHelper::map($unique,'id','id');
        return implode(',',$res);
    }

    ##########
    ############################################# Functions ############################################################

}
