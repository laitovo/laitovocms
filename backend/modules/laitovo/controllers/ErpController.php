<?php

namespace backend\modules\laitovo\controllers;

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\models\AdditionalProduct;
use backend\modules\laitovo\models\MoveActTerminal;
use backend\modules\laitovo\models\ProdGroupTerminal;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use common\models\laitovo\ErpNaryadLog;
use core\logic\DistributionProductionLiterals;
use core\logic\UpnGroup;
use core\models\article\Article;
use core\services\SParentUpn;
use core\services\SProductionLiteral;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\modules\laitovo\models\ErpOrder;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpTerminal;
use backend\modules\laitovo\models\ErpMainTerminal;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpTextileReport;
use backend\modules\laitovo\models\ErpLocationWorkplace;
use backend\modules\laitovo\models\ErpLocationWorkplacePlan;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;
use backend\modules\laitovo\models\ErpLogisticReport;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Config;

/**
 * Default controller for the `laitovo erp` module
 */
class ErpController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            #Ограничиваю доступ к отчету для пользователей
                            $users = [40,41,25];
                            if ($action->id == 'index' && in_array(Yii::$app->user->getId(),$users)) {
                                return false;
                            }

                            $config = Config::findOne(2);
                            $lekaloFlag = false;
                            $handleGive = false;
                            $literalFlag = false;
                            if ($config && ($statusLekalo = $config->json('onLekalo'))) {
                                $lekaloFlag = true;
                            }
                            if ($config && ($statusLekalo = $config->json('handleGive'))) {
                                $handleGive = true;
                            }
                            if ($config && ($statusLiteral = $config->json('onLiteral'))) {
                                $literalFlag = true;
                            }

                            $users2 = [2,3,21,48,36, 173];
                            if ($action->id == 'main-lekalo-terminal' && !$lekaloFlag && !in_array(Yii::$app->user->getId(),$users2)) {
                                return false;
                            }
                            if ($action->id == 'main-lekalo-otk-terminal' && !$lekaloFlag && !in_array(Yii::$app->user->getId(),$users2)) {
                                return false;
                            }
                            if ($action->id == 'main-literal-terminal' && !$literalFlag && !in_array(Yii::$app->user->getId(),$users2)) {
                                return false;
                            }
                            if (in_array($action->id,['main-terminal','main-user-terminal']) && !$handleGive && !in_array(Yii::$app->user->getId(),$users2)) {
                                return false;
                            }
                            
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        ErpLocationWorkplace::withdrawActivity(); 
        #Для получения статистики мы должны получить массив следующего вида
        $logisticsReport = new ErpLogisticReport();
        $model = new ErpTextileReport();
        $addProduct = new AdditionalProduct();

        if ($model->load(Yii::$app->request->post())) {
            $plantcontrol = new ActiveDataProvider([
                'query' => ErpLocation::find()
                    ->where(['<>','id',Yii::$app->params['erp_dispatcher']])
                    ->andWhere(['<>','id',Yii::$app->params['erp_label']])
                    ->andWhere(['type' => ErpLocation::TYPE_PRODUCTION])
                    ->andWhere(['id' => [1,2,3,4,6]])
                    ->orderBy('sort ASC'),
            ]);

            $plantcontrol2 = new ActiveDataProvider([
                'query' => ErpLocation::find()
                    ->where(['id' => [18,19]])
                    ->andWhere(['type' => ErpLocation::TYPE_PRODUCTION])
                    ->orderBy('sort ASC'),
            ]);

            $logisticsData = $logisticsReport->getData($model->dateFrom,$model->dateTo);

            return $this->render('index', [
                'plantcontrol' => $plantcontrol,
                'plantcontrol2' => $plantcontrol2,
                'model' => $model,
                'logisticsData' =>  $logisticsData,
                'addProduct' =>  $addProduct,
                'prodLiteralStat' => (new SProductionLiteral())->statistics()
            ]);
        }else{
            $model->dateFrom = mktime(0,0,0);
            $model->dateTo = mktime(0,0,0);
            $plantcontrol = new ActiveDataProvider([
                'query' => ErpLocation::find()
                    ->where(['<>','id',Yii::$app->params['erp_dispatcher']])
                    ->andWhere(['<>','id',Yii::$app->params['erp_label']])
                    ->andWhere(['id' => [1,2,3,4,6]])
                    ->andWhere(['type' => ErpLocation::TYPE_PRODUCTION])
                    ->orderBy('sort ASC'),
            ]);
            $plantcontrol2 = new ActiveDataProvider([
                'query' => ErpLocation::find()
                    ->where(['id' => [18,19]])
                    ->andWhere(['type' => ErpLocation::TYPE_PRODUCTION])
                    ->orderBy('sort ASC'),
            ]);

            $logisticsData = $logisticsReport->getData($model->dateFrom,$model->dateTo);

            return $this->render('index', [
                'plantcontrol' => $plantcontrol,
                'plantcontrol2' => $plantcontrol2,
                'model' => $model,
                'logisticsData' =>  $logisticsData,
                'addProduct' =>  $addProduct,
                'prodLiteralStat' => (new SProductionLiteral())->statistics()
            ]);
        }
    }

    /**
     * Терминал для работы на участках
     * @return type
     */
    public function actionTerminalVidacha()
    {
        $model = new ErpTerminal();

        $vidacha = new ErpMainTerminal();
        $vidacha->location_id = $model->location_id;
        $vidacha->user_id = $model->user_id;
        $vidano = $vidacha->workOrders;

        $navidachu=$model->getNaryadsNaVidachu();
        if ($navidachu){
            $vidacha->workOrders=ArrayHelper::map($navidachu,'id','id');
            $navidachu=count($vidacha->workOrders);
        } else {
            $navidachu=0;
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->saveProperties();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $nextNaryad = false;
        $poiskNaryad ='';
        if ($nextNaryad = $model->getNaryadsNaVidachuByOne())
        {
            $poiskNaryad = trim(str_replace('Наряд','',$nextNaryad->name));
            $poiskNaryad = mb_substr($poiskNaryad, 5, mb_strlen($poiskNaryad));
        }     

        $naryads = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['or',['location_id'=>$model->location_id],['in','id',$model->workOrders]])->orderBy('updated_at desc'),
        ]);

        $naryadsOnUser = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['and',['location_id'=>$model->location_id],['user_id' => $model->user_id],['status' => ErpNaryad::STATUS_IN_WORK]])->orderBy('updated_at desc'),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $config = Config::findOne(2);
        $limit = (int)$config->json('timeoutOnTable') ?: 60;

        $naryadsInTopSpeed = null;
        if ($model->user_id && $model->location_id == Yii::$app->params['erp_okleika']) {
            $clothArray = $model->clothValues;
            $clothCanDo = $model->getCountWorkplacesForCloth($model->user,[]);
            $workOrders = $model->getListOfWorkOrdersForLocation($model->location_id,[2]);
            $workOrders = $model->filterByClothAndWorkplace($workOrders,$clothCanDo);
            $ids = ArrayHelper::map($workOrders,'id','id');
            $naryadsInTopSpeed = new ActiveDataProvider([
                'query' => ErpNaryad::find()->where(['id' => $ids])->orderBy('sort,updated_at'),
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            $model->clothValues = $clothArray;
        }
//        $naryadsOnUserDelay = new ActiveDataProvider([
//            'query' => ErpNaryad::find()->where(['and',
//                ['status'=>ErpNaryad::STATUS_IN_WORK],
//                ['location_id'=>$model->location_id],
//                ['user_id'=>$model->user_id],
//                ['<','updated_at', time()-60*60*1.5] /*Кроме нарядов на паузе*/
//            ])->orderBy('sort,updated_at'),
//            'pagination' => [
//                'pageSize' => 100,
//            ],
//        ]);

        $naryadsOnUserIdArray = ErpNaryad::find()->where(['and',['location_id'=>$model->location_id],['user_id' => $model->user_id],['status' => ErpNaryad::STATUS_IN_WORK]])->orderBy('updated_at desc')->all();

        $ids = [];
        if ($naryadsOnUserIdArray){
            $ids=ArrayHelper::map($naryadsOnUserIdArray,'id','id');
        }

        $naryadsToVidacha = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['in','id',$vidacha->workOrders])->andWhere(['not in','id',$ids])->orderBy('sort,id'),
        ]);

        return $this->render('terminal-vidacha', [
            'model' => $model,
            'naryads' => $naryads,
            'vidano' => $vidano,
            'navidachu' => $navidachu,
            'naryadsOnUser' => $naryadsOnUser,
            'naryadsToVidacha' => $naryadsToVidacha,
            'poiskNaryad' => $poiskNaryad,
            'nextNaryad' => $nextNaryad,
            'naryadsInTopSpeed' => $naryadsInTopSpeed
//            'naryadsOnUserDelay' => $naryadsOnUserDelay,
        ]);

    }


     /**
     * Терминал для работы на участках
     * @return type
     */
    public function actionTerminalSdacha()
    {
        $model = new ErpTerminal();

        $vidacha = new ErpMainTerminal();
        $vidacha->location_id = $model->location_id;
        $vidacha->user_id = $model->user_id;
        $vidano = $vidacha->workOrders;

        $navidachu=$model->getNaryadsNaVidachu();
        if ($navidachu){
            $vidacha->workOrders=ArrayHelper::map($navidachu,'id','id');
            $navidachu=count($vidacha->workOrders);
        } else {
            $navidachu=0;
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->saveProperties();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $nextNaryad = false;
        $poiskNaryad ='';
        if ($nextNaryad = $model->getNaryadsNaVidachuByOne())
        {
            $poiskNaryad = trim(str_replace('Наряд','',$nextNaryad->name));
            $poiskNaryad = mb_substr($poiskNaryad, 5, mb_strlen($poiskNaryad));
        }     

        $naryads = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['or',['location_id'=>$model->location_id],['in','id',$model->workOrders]])->orderBy('updated_at desc'),
        ]);

        $naryadsOnUser = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['and',['location_id'=>$model->location_id],['user_id' => $model->user_id],['status' => ErpNaryad::STATUS_IN_WORK]])->orderBy('updated_at desc'),
        ]);

        $naryadsOnUserIdArray = ErpNaryad::find()->where(['and',['location_id'=>$model->location_id],['user_id' => $model->user_id],['status' => ErpNaryad::STATUS_IN_WORK]])->orderBy('updated_at desc')->all();

        $ids = [];
        if ($naryadsOnUserIdArray){
            $ids=ArrayHelper::map($naryadsOnUserIdArray,'id','id');
        }

        $naryadsToVidacha = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['in','id',$vidacha->workOrders])->andWhere(['not in','id',$ids])->orderBy('sort,id'),
        ]);

        return $this->render('terminal-sdacha', [
            'model' => $model,
            'naryads' => $naryads,
            'vidano' => $vidano,
            'navidachu' => $navidachu,
            'naryadsOnUser' => $naryadsOnUser,
            'naryadsToVidacha' => $naryadsToVidacha,
            'poiskNaryad' => $poiskNaryad,
            'nextNaryad' => $nextNaryad,
        ]);

    }

    /**
     * Терминал для регистрации на рабочих местах
     * @return type
     */
    public function actionTerminalRegister()
    {
        $model = new ErpTerminal();

        $vidacha = new ErpMainTerminal();
        $vidacha->location_id = $model->location_id;
        $vidacha->user_id = $model->user_id;
        $vidano = $vidacha->workOrders;

        $navidachu=$model->getNaryadsNaVidachu();
        if ($navidachu){
            $vidacha->workOrders=ArrayHelper::map($navidachu,'id','id');
            $navidachu=count($vidacha->workOrders);
        } else {
            $navidachu=0;
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->saveProperties();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $nextNaryad = false;
        $poiskNaryad ='';
        if ($nextNaryad = $model->getNaryadsNaVidachuByOne())
        {
            $poiskNaryad = trim(str_replace('Наряд','',$nextNaryad->name));
            $poiskNaryad = mb_substr($poiskNaryad, 5, mb_strlen($poiskNaryad));
        }     

        $naryads = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['or',['location_id'=>$model->location_id],['in','id',$model->workOrders]])->orderBy('updated_at desc'),
        ]);

        $naryadsOnUser = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['and',['location_id'=>$model->location_id],['user_id' => $model->user_id],['status' => ErpNaryad::STATUS_IN_WORK]])->orderBy('updated_at desc'),
        ]);

        $naryadsOnUserIdArray = ErpNaryad::find()->where(['and',['location_id'=>$model->location_id],['user_id' => $model->user_id],['status' => ErpNaryad::STATUS_IN_WORK]])->orderBy('updated_at desc')->all();

        $ids = [];
        if ($naryadsOnUserIdArray){
            $ids=ArrayHelper::map($naryadsOnUserIdArray,'id','id');
        }

        $naryadsToVidacha = new ActiveDataProvider([
            'query' => ErpNaryad::find()->where(['in','id',$vidacha->workOrders])->andWhere(['not in','id',$ids])->orderBy('sort,id'),
        ]);

        return $this->render('terminal-register', [
            'model' => $model,
            'naryads' => $naryads,
            'vidano' => $vidano,
            'navidachu' => $navidachu,
            'naryadsOnUser' => $naryadsOnUser,
            'naryadsToVidacha' => $naryadsToVidacha,
            'poiskNaryad' => $poiskNaryad,
            'nextNaryad' => $nextNaryad,
        ]);

    }


    /**
     * Терминал для выдачи нарядов
     * @return type
     */
    public function actionMainTerminal()
    {
        $model = new ErpMainTerminal();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->handOutWorkOrders()){
                Yii::$app->session->setFlash('ERP_START_NARYADS', $model->workOrders);
                Yii::$app->session->setFlash('success', 'Выданы наряды: '. implode(', ', $model->workOrders));
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                Yii::$app->session->setFlash('error', implode(', ', $errors));
            }
            return $this->refresh();
        }

        $query = ErpNaryad::find()->where(['and',['or',['location_id'=>null],['location_id'=>7]],['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]]])->orderBy('sort,id');
       
        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

           $query->andWhere(['or',
               ['like','barcode',str_replace('/', 'D', Yii::$app->request->get('search'))],
           ]);
       }

       $query->groupBy('order_id');
        $naryads = new ActiveDataProvider([
            'query' => $query
            ]);

        return $this->render('main-terminal', [
            'model' => $model,
            'naryads' => $naryads,
        ]);

    }



    /**
     * Терминал для выдачи нарядов на сотрудника
     * @return string|Response
     */
    public function actionMainUserTerminal()
    {
        $model = new ErpMainTerminal();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if (!$model->user_id) {
                Yii::$app->session->setFlash('error','Не указан пользователь!!!!!');
                return $this->refresh();
            }
            if ($model->handOutWorkOrders()){
                Yii::$app->session->setFlash('ERP_START_NARYADS', $model->workOrders);
                Yii::$app->session->setFlash('success', 'Выданы наряды: '. implode(', ', $model->workOrders));
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                Yii::$app->session->setFlash('error', implode(', ', $errors));
            }
            return $this->refresh();
        }

//        $query = ErpNaryad::find()->where(['and',['is not','order_id',null],['or',['location_id'=>null],['location_id'=>7]],['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],['prodGroup' => null],['actNumber' => null]]);
        $query = ErpNaryad::find()->where(['and',['or',['location_id'=>null],['location_id'=>7]],['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]],['prodGroup' => null],['actNumber' => null]]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

           $query->andWhere(['or',
               ['like','barcode',str_replace('/', 'D', Yii::$app->request->get('search'))],
           ]);
       }

       $query->groupBy('order_id');
        $naryads = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['sort'=>SORT_ASC]]
        ]);

        return $this->render('main-user-terminal', [
            'model' => $model,
            'naryads' => $naryads,
        ]);

    }

    /**
     * Терминал для выдачи нарядов на сотрудника по лекалу
     * @return string|Response
     */
    public function actionMainLekaloTerminal()
    {
        $model = new ErpMainTerminal();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if (!$model->user_id) {
                Yii::$app->session->setFlash('error', 'Обязательно укажите пользователя, на которого хотите выдать наряды. Если список пуст, то нет ни одно активнго (работающего) пользователя на швейке, готового получить наряды');
                return $this->refresh();
            }

            if ($model->handOutWorkOrders()){
                Yii::$app->session->setFlash('ERP_START_NARYADS', $model->workOrders);
                Yii::$app->session->setFlash('success', 'Выданы наряды: '. implode(', ', $model->workOrders));
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                Yii::$app->session->setFlash('error', implode(', ', $errors));
            }
            return $this->refresh();
        }

        $query = ErpNaryad::find()->where(['and',['or',['location_id'=>null],['location_id'=>7]],['start' => Yii::$app->params['erp_izgib']],['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]]])->orderBy('sort,id');

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {
            $prepareString = preg_replace('/\D+/',' ', Yii::$app->request->get('search'));
            $prepareString = preg_replace('/\s+/', ' ', $prepareString);
            $array = explode(' ',trim($prepareString));
            if (is_array($array) && count($array) > 1) {
                $query->andWhere(['order_id'=> $array]);
            } else {
                if (!empty(trim($prepareString)) && is_numeric(trim($prepareString))) {
                    $query->andFilterWhere(['order_id' => trim($prepareString)]);
                }else{
                    $sourceInNumbers = ErpNaryad::find()
                        ->select('order_id')
                        ->where(['and',
                            ['or',
                                ['location_id'=>null],
                                ['location_id'=>7]
                            ],
                            ['start' => Yii::$app->params['erp_izgib']],
                            ['or',
                                ['status'=>null],
                                ['status'=>''],
                                ['status' => ErpNaryad::STATUS_IN_WORK]
                            ]])
                        ->distinct()
                        ->column();
                    $orders = Order::find()->select('source_innumber')->where(['source_innumber' => $sourceInNumbers])->andWhere(['or',
                        ['like','username',Yii::$app->request->get('search')],
                        ['like','export_username',Yii::$app->request->get('search')]
                    ])->distinct()->column();
                    $query->andWhere(['order_id'=> $orders]);
                }
            }
        }

        //Получили все наряды, которые нам необходимо выдать на изгиб
        $workOrders = $query->all();

        $result = [];
        $templates = [];
        $workOrdersByTemplates = [];
        //Сгруппируем вся наряды по лекалу и отсортируем по количеству нарядов.
        foreach ($workOrders as $workOrder) {
            $lekalo = (int)$workOrder->lekalo ? : 0;
            $workOrdersByTemplates[$lekalo][] = $workOrder;
            @$templates[$lekalo]++;
        }

        arsort($templates);

        //Отсортируем по наибольшему количеству нарядов на одно лекало.
        foreach ($templates as $key => $value) {
            $result[] =
                [
                    'lekalo' => $key,
                    'naryads' => $workOrdersByTemplates[$key],
                     'count' => $value
                ];
        }

        if ( ($sort = Yii::$app->request->get('sort')) && $sort === 'speed' ) {
            //Сортируем группы нарядов по срочности
            uasort($result, function ($a, $b) {
                $a['test'] = $b['test'] = 'test';
                return ErpNaryad::compareByPriority($a['naryads'], $b['naryads']);
            });
        }


        $naryads = new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);

        return $this->render('main-lekalo-terminal', [
            'model' => $model,
            'naryads' => $naryads,
            'admin' =>  ($admin = Yii::$app->request->get('admin') ? true : false ),
        ]);

    }

    public function actionMainLekaloOtkTerminal()
    {
        $model = new ErpMainTerminal();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if (!$model->user_id) {
                Yii::$app->session->setFlash('error', 'Обязательно укажите пользователя, на которого хотите выдать наряды. Если список пуст, то нет ни одно активнго (работающего) пользователя на швейке, готового получить наряды');
                return $this->refresh();
            }

            if ($model->handOutWorkOrders()){
                Yii::$app->session->setFlash('ERP_START_NARYADS', $model->workOrders);
                Yii::$app->session->setFlash('success', 'Выданы наряды: '. implode(', ', $model->workOrders));
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                Yii::$app->session->setFlash('error', implode(', ', $errors));
            }
            return $this->refresh();
        }

        $query = ErpNaryad::find()->where(['and',
            ['location_id'=>7],
            ['start' => Yii::$app->params['erp_otk']],
            ['status' => ErpNaryad::STATUS_IN_WORK]
        ])->orderBy('sort,id');


        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andWhere(['or',
                ['like','barcode',str_replace('/', 'D', Yii::$app->request->get('search'))],
            ]);
        }

        //Получили все наряды, которые нам необходимо выдать на изгиб
        $workOrders = $query->all();

        $result = [];
        $templates = [];
        $workOrdersByTemplates = [];
        //Сгруппируем вся наряды по лекалу и отсортируем по количеству нарядов.

        //Блокированные лекала
        $showStopTemplates  = $this->_updateSession('erp_controller_otk_terminal_show_stop_templates', 'update_erp_controller_otk_terminal_show_stop_templates', true);

        $stopTemplates = [];
        if ($showStopTemplates) {
            $templateWorkOrders = ErpNaryad::find()->where(['and',
                ['status'=>ErpNaryad::STATUS_IN_WORK],
                ['or',
                    ['location_id'=>Yii::$app->params['erp_izgib']],
                    ['location_id'=>Yii::$app->params['erp_otk']]
                ],
            ])->all();

            foreach ($templateWorkOrders as $templateWorkOrder) {
                $lekalo = $templateWorkOrder->lekalo ? (int)$templateWorkOrder->lekalo : 0;
                if (!in_array($lekalo,$stopTemplates)) {
                    $stopTemplates[] = $lekalo;
                }
            }
        }

        foreach ($workOrders as $workOrder) {
            #Если есть свопадение по лекалу, то он в задержке
            $lekalo = $workOrder->lekalo ? (int)$workOrder->lekalo : 0;
            if (in_array($lekalo, $stopTemplates)) {
                continue;
            }

            #Проверяем на комплектацию
            if ($workOrder->window && $workOrder->window !== 'OT') {
                //Если нету группы то пофиг
                $upn = $workOrder->upn;
                if (!$upn || !$upn->group) {
                    $workOrdersByTemplates[$lekalo][] = $workOrder;
                    @$templates[$lekalo]++;
                    continue;
                };

                $group = UpnGroup::getAnotherFromGroup($upn);

                if (empty($group)) {
                    $workOrdersByTemplates[$lekalo][] = $workOrder;
                    @$templates[$lekalo]++;
                    continue;
                }

                $ids = ArrayHelper::map($group, 'id', 'id');
                $groupWorkOrders = ErpNaryad::find()
                    ->where(['and',
                        ['order_id' => $workOrder->order_id],
                        ['in', 'logist_id', $ids],
                        ['start' => Yii::$app->params['erp_otk']],
                        ['status' => ErpNaryad::STATUS_IN_WORK],
                        ['location_id' => Yii::$app->params['erp_dispatcher']],
                    ])
                    ->all();

                if (count($ids) == count($groupWorkOrders)) {
                    $workOrdersByTemplates[$lekalo][] = $workOrder;
                    @$templates[$lekalo]++;
                    continue;
                }

                $searchExist = ErpNaryad::find()
                    ->where(['and',
                        ['order_id' => $workOrder->order_id],
                        ['in', 'logist_id', $ids],
                        ['and', ['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                        ['or', ['!=', 'location_id', Yii::$app->params['erp_otk']], ['location_id' => null]],
                        ['or', ['!=', 'start', Yii::$app->params['erp_otk']], ['start' => null]],
                    ])
                    ->count();

                $searchExistInPause = ErpNaryad::find()
                    ->where(['and',
                        ['order_id' => $workOrder->order_id],
                        ['in', 'logist_id', $ids],
                        ['status' => ErpNaryad::STATUS_IN_PAUSE],
                        ['or',
                            ['location_id' => Yii::$app->params['erp_otk']],
                            ['start' => Yii::$app->params['erp_otk']]
                        ],
                    ])
                    ->count();

                if (!$searchExist && !$searchExistInPause) {
                    $workOrdersByTemplates[$lekalo][] = $workOrder;
                    @$templates[$lekalo]++;
                    continue;
                }
            } else {
                $workOrdersByTemplates[$lekalo][] = $workOrder;
                @$templates[$lekalo]++;
                continue;
            }
        }

        arsort($templates);

        //Отсортируем по наибольшему количеству нарядов на одно лекало.
        foreach ($templates as $key => $value) {
            $result[] =
                [
                    'lekalo' => $key,
                    'naryads' => $workOrdersByTemplates[$key],
                     'count' => $value
                ];
        }

        if ( ($sort = Yii::$app->request->get('sort')) && $sort === 'speed' ) {
            //Сортируем группы нарядов по срочности
            uasort($result, function ($a, $b) {
                $a['test'] = $b['test'] = 'test';
                return ErpNaryad::compareByPriority($a['naryads'], $b['naryads']);
            });
        }


        $naryads = new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);

        return $this->render('main-lekalo-otk-terminal', [
            'model' => $model,
            'naryads' => $naryads,
            'showStopTemplates' => $showStopTemplates,
            'admin' =>  ($admin = Yii::$app->request->get('admin') ? true : false ),
        ]);

    }

    /**
     * Терминал для выдачи нарядов на сотрудника по лекалу
     * @return string|Response
     */
    public function actionMoveActTerminal($order)
    {
        $model = new MoveActTerminal();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if (!$model->user_id) {
                Yii::$app->session->setFlash('error', 'Обязательно укажите пользователя, на которого хотите выдать наряды. Если список пуст, то нет ни одно активнго (работающего) пользователя на швейке, готового получить наряды');
                return $this->refresh();
            }

            if (($actNumber = $model->moveActWorkOrders())){
                Yii::$app->session->setFlash('MOVE_ERP_START_NARYADS', $model->uniqueWorkOrder);
                Yii::$app->session->setFlash('MOVE_ACT_BARCODE', ErpNaryad::MOVE_ACT_BARCODE_PREFIX . $actNumber);
                Yii::$app->session->setFlash('success', 'Выданы наряды: '. implode(', ', $model->workOrders));
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                Yii::$app->session->setFlash('error', implode(', ', $errors));
            }
            return $this->refresh();
        }

        $query = ErpNaryad::find()->with('upn')->where(['and',['actNumber' => null],['order_id' => $order],['or',['location_id'=>null],['location_id'=>7]],['start' => Yii::$app->params['erp_izgib']],['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK],['status' => ErpNaryad::STATUS_IN_PAUSE]]])->orderBy('sort,id');

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andWhere(['or',
                ['like','barcode',str_replace('/', 'D', Yii::$app->request->get('search'))],
            ]);
        }

        //Получили все наряды, которые нам необходимо выдать на изгиб
        $workOrders = $query->all();

        $result = [];
        $templates = [];
        $workOrdersByTemplates = [];
        //Сгруппируем вся наряды по лекалу и отсортируем по количеству нарядов.
        foreach ($workOrders as $workOrder) {
            $group = $workOrder->upn->group;
            $lekalo = (int)$workOrder->lekalo ? : 0;
            $workOrdersByTemplates[$lekalo][$group][] = $workOrder;
            @$templates[$lekalo]++;
        }

        arsort($templates);

        //Отсортируем по наибольшему количеству нарядов на одно лекало.
        foreach ($templates as $key => $value) {
            $result[] =
                [
                    'lekalo' => $key,
                    'naryads' => $workOrdersByTemplates[$key],
                    'count' => $value
                ];
        }

        if ( ($sort = Yii::$app->request->get('sort')) && $sort === 'speed' ) {
            //Сортируем группы нарядов по срочности
            uasort($result, function ($a, $b) {
                $a['test'] = $b['test'] = 'test';
                return ErpNaryad::compareByPriority($a['naryads'], $b['naryads']);
            });
        }

        $naryads = new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);

        $already_exist = ErpNaryad::find()
            ->select('actNumber')
            ->distinct()
            ->where(['order_id' => $order])
            ->andWhere(['is not', 'actNumber', null])
            ->asArray()->all();

        return $this->render('move-act-terminal', [
            'model' => $model,
            'naryads' => $naryads,
            'admin' =>  ($admin = Yii::$app->request->get('admin') ? true : false ),
            'prefix_barcode' => ErpNaryad::MOVE_ACT_BARCODE_PREFIX,
            'already_exist' => $already_exist
        ]);

    }

    /**
     * Терминал для выдачи нарядов на сотрудника по лекалу
     * @return string|Response
     */
    public function actionMoveGroupTerminal($order)
    {
        $model = new ProdGroupTerminal();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if (!$model->user_id) {
                Yii::$app->session->setFlash('error', 'Обязательно укажите пользователя, на которого хотите выдать наряды. Если список пуст, то нет ни одно активнго (работающего) пользователя на швейке, готового получить наряды');
                return $this->refresh();
            }

            if (($prodGroupNumber = $model->moveProdGroupWorkOrders())){
                Yii::$app->session->setFlash('PROD_GROUP_START_NARYADS', $model->uniqueWorkOrder);
                Yii::$app->session->setFlash('PROD_GROUP_BARCODE', ErpNaryad::PROD_GROUP_BARCODE_PREFIX . $prodGroupNumber);
                Yii::$app->session->setFlash('success', 'Выданы наряды: '. implode(', ', $model->workOrders));
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                Yii::$app->session->setFlash('error', implode(', ', $errors));
            }
            return $this->refresh();
        }

        $query = ErpNaryad::find()->with('upn')->where(['and',['actNumber' => null],['prodGroup' => null],['order_id' => $order],['or',['location_id'=>null],['location_id'=>7]],['start' => Yii::$app->params['erp_izgib']],['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK],['status' => ErpNaryad::STATUS_IN_PAUSE]]])->orderBy('article');

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andWhere(['or',
                ['like','barcode',str_replace('/', 'D', Yii::$app->request->get('search'))],
            ]);
        }

        $poliPlast = false;
        $erpOrder = ErpOrder::findOne($order);
        if ($erpOrder && $erpOrder->json('username') == 'ПК Полипласт ООО') {
            $poliPlast = true;
        }
        //Получили все наряды, которые нам необходимо выдать на изгиб
        $workOrders = $query->all();

        $result = [];
        $templates = [];
        $workOrdersByTemplates = [];
        //Сгруппируем вся наряды по лекалу и отсортируем по количеству нарядов.
        $indexes = [];
        $oldItemText = null;
        $sameItemText = [];
        foreach ($workOrders as $workOrder) {
            $itemtext = isset($workOrder->json('items')[0]['itemtext']) ? $workOrder->json('items')[0]['itemtext'] : '';
            if ($itemtext && (($oldItemText !== null && $oldItemText === $itemtext) ||  $oldItemText === null)) {
                $sameItemText[] = $workOrder;
            }
            $oldItemText = $itemtext;
            if ($itemtext && !$poliPlast) continue;
            $group = $workOrder->upn->group;
            if ($group) continue;
            $article = $workOrder->article;
            $lekalo = (int)$workOrder->lekalo ? : 0;
            $index = $indexes[$article] ?? 1;
            $articleKey = $article .  "_" . $index;
            $workOrdersByTemplates[$lekalo][$articleKey][] = $workOrder;
            if (count($workOrdersByTemplates[$lekalo][$articleKey]) == 10)
                isset($indexes[$article]) ? $indexes[$article]++ : $indexes[$article] = 2;
            @$templates[$lekalo]++;
        }

        if (count($sameItemText) === count($workOrders)) {
            unset($sameItemText);
            $templates = [];
            $workOrdersByTemplates = [];
            //Сгруппируем вся наряды по лекалу и отсортируем по количеству нарядов.
            $indexes = [];
            foreach ($workOrders as $workOrder) {
                $group = $workOrder->upn->group;
                if ($group) continue;
                $article = $workOrder->article;
                $lekalo = (int)$workOrder->lekalo ? : 0;
                $index = $indexes[$article] ?? 1;
                $articleKey = $article .  "_" . $index;
                $workOrdersByTemplates[$lekalo][$articleKey][] = $workOrder;
                if (count($workOrdersByTemplates[$lekalo][$articleKey]) == 10)
                    isset($indexes[$article]) ? $indexes[$article]++ : $indexes[$article] = 2;
                @$templates[$lekalo]++;
            }
        }

        arsort($templates);

        //Отсортируем по наибольшему количеству нарядов на одно лекало.
        foreach ($templates as $key => $value) {
            $result[] =
                [
                    'lekalo' => $key,
                    'naryads' => $workOrdersByTemplates[$key],
                    'count' => $value
                ];
        }

        if ( ($sort = Yii::$app->request->get('sort')) && $sort === 'speed' ) {
            //Сортируем группы нарядов по срочности
            uasort($result, function ($a, $b) {
                $a['test'] = $b['test'] = 'test';
                return ErpNaryad::compareByPriority($a['naryads'], $b['naryads']);
            });
        }

        $naryads = new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);

        $already_exist = ErpNaryad::find()
            ->select('prodGroup')
            ->distinct()
            ->where(['order_id' => $order])
            ->andWhere(['is not', 'prodGroup', null])
            ->asArray()->all();

        return $this->render('move-group-terminal', [
            'model' => $model,
            'naryads' => $naryads,
            'admin' =>  ($admin = Yii::$app->request->get('admin') ? true : false ),
            'prefix_barcode' => ErpNaryad::PROD_GROUP_BARCODE_PREFIX,
            'already_exist' => $already_exist
        ]);

    }

    /**
     * Ручная выдача нарядов по литере
     * @return mixed
     */
    public function actionMainLiteralTerminal()
    {
        $model = new ErpMainTerminal();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if (!$model->user_id) {
                Yii::$app->session->setFlash('error', 'Обязательно укажите пользователя, на которого хотите выдать наряды. Если список пуст, то нет ни одно активнго (работающего) пользователя на изгибе, готового получить наряды');
                return $this->refresh();
            }

            if ($model->handOutWorkOrders()){
                Yii::$app->session->setFlash('ERP_START_NARYADS', $model->workOrders);
                Yii::$app->session->setFlash('success', 'Выданы наряды: '. implode(', ', $model->workOrders));
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                Yii::$app->session->setFlash('error', implode(', ', $errors));
            }
            return $this->refresh();
        }

        $query = ErpNaryad::find()->where(['and',['or',['location_id'=>null],['location_id'=>Yii::$app->params['erp_dispatcher']]],['start' => Yii::$app->params['erp_shveika']],['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]]])->orderBy('sort,id');

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andWhere(['or',
                ['like','barcode',str_replace('/', 'D', Yii::$app->request->get('search'))],
            ]);
        }

        //Получили все наряды, которые нам необходимо выдать на швейку
        $workOrders = $query->all();

        $result = [];
        $templates = [];
        $workOrdersByTemplates = [];
        //Сгруппируем вся наряды по литере и отсортируем по количеству нарядов.
        foreach ($workOrders as $workOrder) {
            $literal = $workOrder->valueLiteral ?: 0;
            $workOrdersByTemplates[$literal][] = $workOrder;
            @$templates[$literal]++;
        }

        arsort($templates);

        //Отсортируем по наибольшему количеству нарядов на одну литеру.
        foreach ($templates as $key => $value) {
            $result[] =
                [
                    'literal' => $key,
                    'naryads' => $workOrdersByTemplates[$key],
                    'count' => $value
                ];
        }

        //Сортируем группы нарядов по срочности
        uasort($result, function ($a, $b) {
            $a['test'] = $b['test'] = 'test';
            return ErpNaryad::compareByPriority($a['naryads'], $b['naryads']);
        });

        $naryads = new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);

        return $this->render('main-literal-terminal', [
            'model' => $model,
            'naryads' => $naryads,
        ]);

    }

    public function actionPrintBoth($id,$id2)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $id=explode(',', $id);
        $content='';
        $break='';
        $print=[];

        foreach ($id as $key => $value) {
            $model = ErpNaryad::findOne($value);

            if ($model && $model->reestr && !in_array($value, $print)){
                $group=ErpNaryad::find()->where(['reestr'=>$model->reestr])->all();

                if (count($print)){
                    $break.=".reestr-break-$value {page-break-before:always;}";
                }

                $print[]=$model->id;
                $content .= $this->renderPartial('../erp-naryad/reestr',[
                    'model' => $model,
                    'group' => $group,
                ]);
                foreach ($group as $row) {
                    if (count($print)){
                        $break.=".page-break-$row->id {page-break-before:always;}";
                    }

                    $print[]=$row->id;
                    $content .= $this->renderPartial('../erp-naryad/print',[
                        'model' => $row,
                    ]);
                }

            } elseif ($model && !in_array($value, $print)) {

                if (count($print)){
                    $break.=".page-break-$value {page-break-before:always;}";
                }

                $print[]=$model->id;

                if ($model->article == 'OT-1041-40-1') {
                    $content .= $this->renderPartial('../erp-naryad/print_casper',[
                        'model' => $model,
                    ]);
                } elseif (preg_match('/OT-2079/', $model->article) === 1) {
                    $content .= $this->renderPartial('../erp-naryad/print_af_cover',[
                        'model' => $model,
                    ]);
                } else {
                    $content .= $this->renderPartial('../erp-naryad/print',[
                        'model' => $model,
                    ]);
                }
            }
        }


        $id2=explode(',', $id2);
        $print2=[];

        //Здесь выполнить проверку, является ли все id нарядами с одной литеры
        $prodLiteral = SProductionLiteral::checkCommonLiteral($id);
        if ($prodLiteral) {
            $content .= "<h3 style='text-align: center'>Возьмите все наряды с литеры <span style='font-size: 3em; font-weight: bold;'>$prodLiteral</span></h3>";
        }

        $sortedId = (new Query())->from(ErpNaryad::tableName() . 'as t')
            ->leftJoin(Naryad::tableName() . ' as upn',['upn.id' => 't.logist_id'])
            ->where(['t.id' => $id])
            ->select('t.id')
            ->orderBy('upn.group ASC, t.id ASC')
            ->column();

        $sortedId = SProductionLiteral::orderWorkOrdersByLiteralLog($sortedId);

        foreach ($sortedId as $key => $value) {
            $model = ErpNaryad::findOne($value);

            $next = ($place = $model->scheme->keytoid[$model->scheme->fromto[$model->scheme->idtokey[$model->location_id ?: 'Start']]]) == 'End' ? null : $place;

            if ($next == Yii::$app->params['erp_clipsi'] && !ArticleHelper::isOnMagnets($model->article) && (!count($model->getClips()) || ArticleHelper::isTape($model->article)))
            {
                $next = ($place = $model->scheme->keytoid[$model->scheme->fromto[$model->scheme->idtokey[Yii::$app->params['erp_clipsi'] ?: 'Start']]]) == 'End' ? null : $place;
            }else{
                $next = ($place = $model->scheme->keytoid[$model->scheme->fromto[$model->scheme->idtokey[$model->location_id ?: 'Start']]]) == 'End' ? null : $place;
            }

            if ($model && !in_array($value, $print2)) {

                if (count($print) && $value = $sortedId[0]){
                    $break.=".page-break-$value {page-break-before:always;}";
                }elseif (count($print)){
                    $break.=".page-break-$value {page-break-before:auto;}";

                }

                $print[]=$model->id;
                $content .= $this->renderPartial('../erp-naryad/printreestr',[
                    'model' => $model,
                    'next' => $next,
                ]);
            }

        }

        echo '<style>'.$break.'</style>';

        echo $content;
    }


    public function actionPrint($id)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $id=explode(',', $id);
        $content='';
        $break='';
        $print=[];

        foreach ($id as $key => $value) {
            $model = ErpNaryad::findOne($value);

            if ($model && $model->reestr && !in_array($value, $print)){
                $group=ErpNaryad::find()->where(['reestr'=>$model->reestr])->all();

                if (count($print)){
                    $break.=".reestr-break-$value {page-break-before:always;}";
                }

                $print[]=$model->id;
                $content .= $this->renderPartial('../erp-naryad/reestr',[
                    'model' => $model,
                    'group' => $group,
                ]);
                foreach ($group as $row) {
                    if (count($print)){
                        $break.=".page-break-$row->id {page-break-before:always;}";
                    }

                    $print[]=$row->id;
                    $content .= $this->renderPartial('../erp-naryad/print',[
                        'model' => $row,
                    ]);
                }

            } elseif ($model && !in_array($value, $print)) {

                if (count($print)){
                    $break.=".page-break-$value {page-break-before:always;}";
                }

                $print[]=$model->id;
                /**
                 * Костыль для комбинезонов
                 */
                if ($model->article == 'OT-1041-40-1') {
                    $content .= $this->renderPartial('../erp-naryad/print_casper',[
                        'model' => $model,
                    ]);
                } elseif (preg_match('/OT-2079/', $model->article) === 1) {
                    $content .= $this->renderPartial('../erp-naryad/print_af_cover',[
                        'model' => $model,
                    ]);
                } else {
                    $content .= $this->renderPartial('../erp-naryad/print',[
                        'model' => $model,
                    ]);
                }
            }
        }

        $content .= '<style>'.$break.'</style>';

        return $content;
    }

    public function actionMoveActPrint($actBarcode)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $actNumber = ErpNaryad::findAct($actBarcode);
        if (!$actNumber) return $this->renderContent('Акта с таким номером не существует!');

        $workOrders = ErpNaryad::find()->where(['actNumber' => $actNumber])->all();
        if (empty($workOrders)) return $this->renderContent('Не найдено ни одного артикула для этого акта!');

        $articles = [];
        $elements = [];
        $count = [];
        foreach ($workOrders as $workOrder) {
            if (!in_array($workOrder->article,$articles)) {
                $articles[] = $workOrder->article;
                $elements[] = $workOrder;
            }
            isset($count[$workOrder->article]) ? $count[$workOrder->article]++ : $count[$workOrder->article] = 1;
        }

        $one         = $elements[0];
        $client      = $one->order->json('username');
        $orderNumber = $one->order_id;
        $car         = $one->car;
        $template    = $one->template;
        $workerName =  $one->location_id == Yii::$app->params['erp_izgib'] ? $one->user->name : '';


        return $this->renderPartial('label_move_act',[
            'barcode'     => $actBarcode,
            'number'      => $actNumber,
            'elements'    => $elements,
            'client'      => $client,
            'orderNumber' => $orderNumber,
            'car'         => $car,
            'template'    => $template,
            'count'       => $count,
            'workerName'  => $workerName,

        ]);
    }

    public function actionMoveGroupPrint($groupBarcode)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $prodGroup = ErpNaryad::findProdGroup($groupBarcode);
        if (!$prodGroup) return $this->renderContent('Группы с таким номером не существует!');

        $workOrders = ErpNaryad::find()->where(['prodGroup' => $prodGroup])->all();
        if (empty($workOrders)) return $this->renderContent('Не найдено ни одного артикула для этого акта!');

        $articles = [];
        $elements = [];
        $count = 0;
        foreach ($workOrders as $workOrder) {
            $count++;
            if (!in_array($workOrder->article,$articles)) {
                $articles[] = $workOrder->article;
                $elements[] = $workOrder;
            }
        }

        return $this->renderPartial('label_prod_group',[
            'model'      => array_shift($workOrders),
            'number'      => $prodGroup,
            'barcode'     => $groupBarcode,
            'count'       => $count,
        ]);
    }

    /**
     * Функция для генерации печатной формы этикеток для сожержимого Акта нарядов
     *
     * @param $actBarcode
     * @return string
     */
    public function actionMoveActPrintLabel($actBarcode)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $actNumber = ErpNaryad::findAct($actBarcode);
        if (!$actNumber) return $this->renderContent('Акта с таким номером не существует!');

        $workOrders = ErpNaryad::find()->where(['actNumber' => $actNumber])->all();
        if (empty($workOrders)) return $this->renderContent('Не найдено ни одного артикула для этого акта!');

        $articles = [];
        $elements = [];
        $count = [];
        foreach ($workOrders as $workOrder) {
            if (!in_array($workOrder->article,$articles)) {
                $articles[] = $workOrder->article;
                $elements[] = $workOrder;
            }
            isset($count[$workOrder->article]) ? $count[$workOrder->article]++ : $count[$workOrder->article] = 1;
        }

        $article = new Article($articles[0]);
        $carName = $article->carName;

        $limit = count($workOrders) / count($elements);
        $count = $limit;
        $content = '';

        for ($i = 1; $i<= $limit; $i++) {
            $content .= $this->renderPartial('label_move_act_prod_new',[
                'barcode'     => $actBarcode,
                'actNumber'   => $actNumber,
                'number'      => $i
            ]);

            $content .= "<p style='page-break-after: always;'>&nbsp;</p>";

            $content .= $this->renderPartial('label_move_act_prod_new_car',[
                'carName' => $carName,
                'window' => (count($articles) > 1 ? 'ЗПС' : $article->window)
            ]);

            $count--;
            if ($count) $content .= "<p style='page-break-after: always;'>&nbsp;</p>";
        }

        return $content;
    }

    /**
     * Функция для генерации печатной формы этикеток для сожержимого Акта нарядов
     *
     * @param $actBarcode
     * @return string
     */
    public function actionPrintCarLabel($order)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $workOrders = ErpNaryad::find()->where(['order_id' => $order])->all();
        if (empty($workOrders)) return $this->renderContent('Не найдено ни одного артикула для этого акта!');

        $content = '';
        $count = count($workOrders);
        foreach ($workOrders as $workOrder) {
            $article = new Article($workOrder->article);
            $carName = $article->carName;

            $content .= $this->renderPartial('label_move_act_prod_new_car',[
                'carName' => $carName,
                'window' => $article->window
            ]);

            $count--;
            if ($count) $content .= "<p style='page-break-after: always;'>&nbsp;</p>";
        }

        return $content;
    }

    /**
     * Функция для генерации печатной формы этикеток для сожержимого Акта нарядов
     *
     * @param $actBarcode
     * @return string
     */
    public function actionProdGroupPrintLabel($groupBarcode)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        /** @var $workOrders ErpNaryad[] */

        $prodGroup = ErpNaryad::findProdGroup($groupBarcode);
        if (!$prodGroup) return $this->renderContent('Группы с таким номером не существует!');

        $workOrders = ErpNaryad::find()->where(['prodGroup' => $prodGroup])->all();
        if (empty($workOrders)) return $this->renderContent('Не найдено ни одного артикула для этой группы артикулов!');

        $count = count($workOrders);
        $content = '';

        if (@$workOrders[0]->order->json('username') == 'ПК Полипласт ООО' ) {
            $article = new Article($workOrders[0]->article);
            $carName = $article->carName;
            foreach ($workOrders as $workOrder) {
                $content .= $this->renderPartial('label_move_act_prod_new',[
                    'barcode'     => $workOrder->barcode,
                    'actNumber'   => $workOrder->prodGroup,
                    'number'      => $workOrder->logist_id
                ]);

                $content .= "<p style='page-break-after: always;'>&nbsp;</p>";

                $content .= $this->renderPartial('label_move_act_prod_new_car',[
                    'carName' => $carName,
                    'window' => $article->window
                ]);

                $count--;
                if ($count) $content .= "<p style='page-break-after: always;'>&nbsp;</p>";
            }
            return $content;
        }

        foreach ($workOrders as $workOrder) {
            $content .= $this->renderPartial('/erp-naryad/label_export_new',[
                'model' => $workOrder,
            ]);
            $count--;
            if ($count) $content .= "<p style='page-break-after: always;'>&nbsp;</p>";
        }

        return $content;
    }


    /**
     * Функция для генерации печатной формы этикеток для сожержимого Акта нарядов
     *
     * @param $addGroupBarcode
     * @return string
     */
    public function actionAddGroupPrintLabel($addGroupBarcode)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        /** @var $workOrders ErpNaryad[] */

        $addGroup = ErpNaryad::findAddGroup($addGroupBarcode);
        if (!$addGroup) return $this->renderContent('Реестра с таким номером не существует!');

        $workOrders = ErpNaryad::find()->where(['addGroup' => $addGroup])->all();
        if (empty($workOrders)) return $this->renderContent('Не найдено ни одного артикула для этой группы артикулов!');

        $count = count($workOrders);
        $content = '';

        foreach ($workOrders as $workOrder) {
            $content .= $this->renderPartial('/erp-naryad/label_export_new',[
                'model' => $workOrder,
            ]);
            $count--;
            if ($count) $content .= "<p style='page-break-after: always;'>&nbsp;</p>";
        }

        return $content;
    }


    public function actionPrintReestr($id)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $id=explode(',', $id);

        $content='';
        $break='';
        $print=[];

        //Здесь выполнить проверку, является ли все id нарядами с одной литеры
        $prodLiteral = SProductionLiteral::checkCommonLiteral($id);
        if ($prodLiteral) {
            $content .= "<h3 style='text-align: center'>Возьмите все наряды с литеры <span style='font-size: 3em; font-weight: bold;'>$prodLiteral</span></h3>";
        }

        $sortedId = (new Query())->from(ErpNaryad::tableName() . 'as t')
            ->leftJoin(Naryad::tableName() . ' as upn',['upn.id' => 't.logist_id'])
            ->where(['t.id' => $id])
            ->select('t.id')
            ->orderBy('upn.group ASC, t.id ASC')
            ->column();

        $sortedId = SProductionLiteral::orderWorkOrdersByLiteralLog($sortedId);

        foreach ($sortedId as $key => $value) {
            $model = ErpNaryad::findOne($value);
            // $user = $model->user->name;
            $next = ($place = $model->scheme->keytoid[$model->scheme->fromto[$model->scheme->idtokey[$model->location_id ?: 'Start']]]) == 'End' ? null : $place;

            if ($next == Yii::$app->params['erp_clipsi'] && !ArticleHelper::isOnMagnets($model->article) && (!count($model->getClips()) || ArticleHelper::isTape($model->article)))
            {
                $next = ($place = $model->scheme->keytoid[$model->scheme->fromto[$model->scheme->idtokey[Yii::$app->params['erp_clipsi'] ?: 'Start']]]) == 'End' ? null : $place;
            }else{
                $next = ($place = $model->scheme->keytoid[$model->scheme->fromto[$model->scheme->idtokey[$model->location_id ?: 'Start']]]) == 'End' ? null : $place;
            }

            if ($model && !in_array($value, $print)) {

                if (count($print)){
                    $break.=".page-break-$value {page-break-before:auto;}";
                }

                $print[]=$model->id;
                $content .= $this->renderPartial('../erp-naryad/printreestr',[
                    'model' => $model,
                    'next' => $next,
                    // 'user' => $user,
                ]);
            }

        }

        $content .= '<style>'.$break.'</style>';

        return $content;
    }

    public function actionPrintAddGroup($id)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $id=explode(',', $id);
        $content='';
        $break='';
        $print=[];

        foreach ($id as $key => $value) {
            $addGroupNumber = ErpNaryad::findAddGroup($value);
            $workOrders = ErpNaryad::find()->where(['addGroup' => $addGroupNumber])->all();
            if ($workOrders && !in_array($value, $print)) {
                if (count($print)) {
                    $break.=".page-break-$value {page-break-before:always;}";
                }
            }
            $print[] = $value;
            /** @var ErpNaryad $naryad */
            $naryad = $workOrders[0];
            $content .= $this->renderPartial('../erp-naryad/print-add-group',[
                'article' => $naryad->article,
                'title' => $naryad->json('items')[0]['name'],
                'barcode' => $value,
                'number' => $addGroupNumber,
                'workOrders' => $workOrders
            ]);
        }

        $content .= '<style>'.$break.'</style>';

        return $content;
    }


    /**
     * Работа сканера на участке выдачи наряда
     * @param string $barcode штрихкод
     * @throws Exception
     */
    public function actionSearchVidacha($barcode)
    {
        //Данное действие только для Аякс запроса
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = new ErpTerminal();
        $barcode = $model->toLatin($barcode);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status']='error';
        $response['message']='Не найдено';

        //Все что можно будет искать через данный терминал  - это штрихкод участка, пользователя, наряда
        $location=ErpLocation::find()->where(['barcode'=>$barcode])->one();
        $user=ErpUser::find()->where(['barcode'=>$barcode])->one();
        $naryad=ErpNaryad::find()->where(['barcode'=>$barcode])->one();
        $act = ErpNaryad::findAct($barcode);
        $prodGroup = ErpNaryad::findProdGroup($barcode);
        $addGroup = ErpNaryad::findAddGroup($barcode);

        //Локацию ищем только для того, чтобы сменить локацию пользователя и установить локацию для терминала.
        //Локация устанавливаеться на весь срок времени, пока неизмениться
        if ($location){//поиск участка
            $model->location_id=$location->id;
            //регистрация пользователя на участке
            // if ($model->user_id && ($user=ErpUser::findOne($model->user_id))!=null) {
            //     $user->location_id=$model->location_id;
            //     $user->save();
            // }
            if ($model->validate()){
                $response['status']='success';
                $response['message']=$model->user->name.' - '. $location->name;
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                $response['message']=implode(', ', $errors);
            }
        //Ищем сотрудника для того чтобы установить и локацию и сотрудника для терминала    
        } elseif ($user){//поиск сотрудника
            //если 2 ой пик
            //выдача по одной бирке пользователя
            if ($model->user_id == $user->id && $user->location_id  && $model->location_id == $user->location->id)
            {
                if ($model->user->hasNaryadsOnHandsOld()) {
                    $response['status']='danger';
                    $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                } elseif ($model->validate()) {
                    $vidacha = new ErpMainTerminal();
                    $vidacha->user_id = $model->user->id;
                    $vidacha->location_id = $model->location_id;
                    $naryads = $model->getNaryadsNaVidachu();

                    ///Костыль для раскоря органайзеров
                    /// Если это участок ракрой и все наряды являются одним артикулом и это органайзеры, тогда флаг ставим в true;
                    /// Это услолвие работает только тогда, когда только одного вида артикулы находятся на выдачу на участке
                    if ($model->location_id == 19 && $naryads) {

                        $orgFlag = true;
                        $articleBefore = null;
                        foreach ($naryads as $naryad) {
                            if (preg_match('/OT-(1189|1190|1214)/', $naryad->article) !== 1 || ($articleBefore !== null && $articleBefore !== $naryad->article)) {
                                $orgFlag = false;
                                break;
                            }
                            $articleBefore = $naryad->article;
                        }

                        $vidacha->workOrders = ArrayHelper::map($naryads, 'id', 'id');

                        if ($orgFlag) {
                            //Если это все органайзеры, тогда разбить наряды на части по 20 единиц, и для каждой части задать общий признак
                            //Например группа дополнительной продукции.
                            if ($vidacha->issueAndPrintOutAddGroups(8)) {
                                $response['status'] = 'success';
                                $response['message'] = 'Вы получили наряды в виде реестров'; /*№: ' . implode(', ', $result);*/
                                $response['addGroup'] = $vidacha->printAddGroup;
                                $vidacha->saveProperties();
                            } else {
                                $errors = [];
                                foreach ($vidacha->getErrors() as $key => $value) {
                                    $errors[] = $value[0];
                                }
                                $response['message'] = implode(', ', $errors);
                            }

                        }else{
                            if ($vidacha->issueAndPrintOutWorkOrders()) {
                                $response['status'] = 'success';
                                $response['message'] = 'Вы получили наряды'; /*№: ' . implode(', ', $result);*/
                                $response['vidacha'] = $vidacha->printableWorkOrders;
                                $response['reestr'] = $vidacha->printRegister;
                                $vidacha->saveProperties();
                            } else {
                                $errors = [];
                                foreach ($vidacha->getErrors() as $key => $value) {
                                    $errors[] = $value[0];
                                }
                                $response['message'] = implode(', ', $errors);
                            }
                        }

                    }elseif ($model->location_id == 22 && $naryads) {

                        $orgFlag = true;
                        $articleBefore = null;
                        foreach ($naryads as $naryad) {
                            if (preg_match('/OT-(2061)/', $naryad->article) !== 1 || ($articleBefore !== null && $articleBefore !== $naryad->article)) {
                                $orgFlag = false;
                                break;
                            }
                            $articleBefore = $naryad->article;
                        }

                        $vidacha->workOrders = ArrayHelper::map($naryads, 'id', 'id');

                        if ($orgFlag) {
                            //Если это все органайзеры, тогда разбить наряды на части по 20 единиц, и для каждой части задать общий признак
                            //Например группа дополнительной продукции.
                            if ($vidacha->issueAndPrintOutAddGroups(15)) {
                                $response['status'] = 'success';
                                $response['message'] = 'Вы получили наряды в виде реестров'; /*№: ' . implode(', ', $result);*/
                                $response['addGroup'] = $vidacha->printAddGroup;
                                $vidacha->saveProperties();
                            } else {
                                $errors = [];
                                foreach ($vidacha->getErrors() as $key => $value) {
                                    $errors[] = $value[0];
                                }
                                $response['message'] = implode(', ', $errors);
                            }

                        }else{
                            if ($vidacha->issueAndPrintOutWorkOrders()) {
                                $response['status'] = 'success';
                                $response['message'] = 'Вы получили наряды'; /*№: ' . implode(', ', $result);*/
                                $response['vidacha'] = $vidacha->printableWorkOrders;
                                $response['reestr'] = $vidacha->printRegister;
                                $vidacha->saveProperties();
                            } else {
                                $errors = [];
                                foreach ($vidacha->getErrors() as $key => $value) {
                                    $errors[] = $value[0];
                                }
                                $response['message'] = implode(', ', $errors);
                            }
                        }

                    }else {
                        if ($naryads) {
                            $vidacha->workOrders = ArrayHelper::map($naryads, 'id', 'id');
                        }

                        if ($vidacha->issueAndPrintOutWorkOrders()) {
                            // foreach ($vidacha->print as $naryad_id) {
                            //     $str = trim(str_replace('Наряд', '', ErpNaryad::findOne($naryad_id)->name));
                            //     $str = mb_substr($str, 5, mb_strlen($str));
                            //     $result[] = $str;
                            // }
                            $response['status'] = 'success';
                            $response['message'] = 'Вы получили наряды'; /*№: ' . implode(', ', $result);*/
                            $response['vidacha'] = $vidacha->printableWorkOrders;
                            $response['reestr'] = $vidacha->printRegister;
                            $vidacha->saveProperties();
                        } else {
                            $errors = [];
                            foreach ($vidacha->getErrors() as $key => $value) {
                                $errors[] = $value[0];
                            }
                            $response['message'] = implode(', ', $errors);
                        }
                    }

                } else {
                    $errors = [];
                    foreach ($model->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }                
            } else {
                //нулевой пик
                //Сюда входят сообщения типа для вас не зарагестрировано место
                //Сюда входит приход и уход работников, для которых зарегестрировано место только под основную работу
                if ($user->isOutWork() && !$user->hasProductionPlanWorkplaces() && !$user->hasSupportPlanWorkplaces()) {
                    $response['status']='danger';
                    $response['message']='Для Вас нет запланированных рабочих мест.<br> Обратитесь к руководителю для уточнения.';
                    return $response;
                }

                if ($user->isOutWork() && ($user->hasProductionPlanWorkplaces() || $user->hasSupportPlanWorkplaces())) {
                    $response['status']='danger';
                    $response['message']='Обратитесь к терминалу сдачи для регистрации на участке!';
                    return $response;
                }

                //второй алгоритм
                if ($user->isOnWork() && $user->hasProductionPlanWorkplaces() && !$user->onProductionWorkplaces() && $user->hasNaryadsOnHands() ){
                    $model->user_id = $user->id;
                    $model->location_id = null;
                    $model->saveProperties();
                    $response['status']='danger';
                    $response['message']='Вас назначили на другое рабочее место. <br>'
                                        .'Сдайте наряды, которые у Вас на руках и вы успешно перейдете!';
                    return $response;
                }

                if ($user->isOnWork() && !$user->hasProductionPlanWorkplaces() && $user->hasSupportPlanWorkplaces() && !$user->onProductionWorkplaces() && $user->hasNaryadsOnHands() ){
                                    $model->user_id = $user->id;
                                    $model->location_id = null;
                                    $model->saveProperties();
                                    $response['status']='danger';
                                    $response['message']='Вас назначили на другое рабочее место. <br>'
                                                        .'Сдайте наряды, которые у Вас на руках и вы успешно перейдете!';
                                    return $response;
                }

                if ($user->isOnWork() && $user->onProductionWorkplaces() && $user->hasNaryadsOnHands()
                    &&  ($user->location_id!=Yii::$app->params['erp_okleika'] || ($user->location_id==Yii::$app->params['erp_okleika'] && !$user->locationPlanInLocation()))
                    &&  ($user->hasCommandMoveFrom() || $user->hasCommandMoveTo())) {
                    $model->user_id = $user->id;
                    $model->location_id = null;
                    $model->saveProperties();
                    $response['status']='warning';
                    $response['message']='Вас назначили на другое рабочее место. <br>'
                                        .'Сдайте наряды, которые у Вас на руках и вы успешно перейдете!';
                    return $response;
                }
                if ($user->isOnWork() && !$user->hasProductionPlanWorkplaces() && $user->hasSupportPlanWorkplaces() && !$user->onProductionWorkplaces() && $user->hasNaryadsOnHands() ){
                    $model->user_id = $user->id;
                    $model->location_id = null;
                    $model->saveProperties();
                    $response['status']='danger';
                    $response['message']='Вас назначили на другое рабочее место. <br>'
                        .'Сдайте наряды, которые у Вас на руках и вы успешно перейдете!';
                    return $response;
                }

                //второй алгоритм
                if ($user->isOnWork() && $user->hasProductionPlanWorkplaces() && !$user->onProductionWorkplaces() && !$user->hasNaryadsOnHands() ){
                    $user->initProductionReRegistration();
                    $model->user_id = $user->id;
                    $model->location_id = $user->location_id;
                    $model->saveProperties();
                    $response['status']='success';
                    $response['message']='Вы успешно зарегестрированы на производстве. Можете приступить к получению нарядов !';
                    return $response;
                }

                if ($user->isOnWork() && $user->onProductionWorkplaces()
                    &&  (!$user->hasNaryadsOnHands() || ($user->location_id==Yii::$app->params['erp_okleika'] &&  $user->locationPlanInLocation()))
                    &&  ($user->hasCommandMoveFrom() || $user->hasCommandMoveTo())
                ) {
                    $user->initProductionReRegistration();
                    $model->user_id = $user->id;
                    $model->location_id = $user->location_id;
                    $model->saveProperties();
                    $response['status']='warning';
                    $response['message']='! Внимание! Ваши рабочие места были изменены'
                                        . ' Если возникает затруднение обратитесь к руководителю'
                                        . ' Для получения нарядов щелкните бэйджиком второй раз';
                    return $response;
                }



                //второй алгоритм
                if ($user->isOnWork() && !$user->hasProductionPlanWorkplaces() && !$user->onProductionWorkplaces() && !$user->hasNaryadsOnHands() && $user->hasSupportPlanWorkplaces() && $user->onSupportWorkplaces()){
                    $response['status']='danger';
                    $response['message']='Обратитесь к терминалу сдачи для ухода с работы!';
                    return $response;
                }


                //пятый алгоритм - алгоритм в случае если она на работе и небыло никаких команд //рабочий алгоритм
                if ($user->isOnWork() && ($user->onProductionWorkplaces() || $user->onSupportWorkplaces()) &&  !$user->hasCommandMoveFrom() && !$user->hasCommandMoveTo()) {
                    $model->user_id = $user->id;
                    $model->location_id = @$user->location_id;
                    $model->saveProperties();
                    $response['status']='success';
                    $response['message']= @$user->name.'<br>Для получения нарядов пикните бэйджиком второй раз!<br>'
                                          .'Для ухода с работы обратитесь к терминалу сдачи!';
                    return $response;
                }

                if ($user->isOnWork() && !$user->onProductionWorkplaces() && !$user->hasProductionPlanWorkplaces() && !$user->hasNaryadsOnHands() && !$user->onSupportWorkplaces() && $user->hasSupportPlanWorkplaces()) {
                    $user->initSupportReRegistration();
                    $model->user_id = $user->id;
                    $model->location_id = $user->location_id;
                    $model->saveProperties();
                    $response['status']='warning';
                    $response['message']='! Внимание! Вы вернулись на прежнее место'
                        . ' Если возникает затруднение обратитесь к руководителю'
                        . ' Для получения нарядов щелкните бэйджиком второй раз';
                    return $response;
                }

            }
        } elseif ($naryad) {//поиска наряда
            //если наряд пикали без пользователя, тогда хотели проверить где сейчас наряд
            $config3 = Config::findOne(2);
            $limit3 = (int)$config3->json('timeoutOnTable') ?: 60;
            if (!$model->user_id )
            {
                if (!$naryad->location_id && ($naryad->status == ErpNaryad::STATUS_READY))
                {
                    $response['status']='success';
                    $response['message']=$naryad->name
                    .'. <span style="color:yellow">Статус: '.$naryad->status.'</span>';
                }elseif(!$naryad->location_id && ($naryad->status == ErpNaryad::STATUS_CANCEL)){
                    $response['status']='danger';
                    $response['message']=$naryad->name
                    .'. <span style="color:yellow">Статус: '.$naryad->status.'</span>';
                }elseif(!$naryad->location_id && ($naryad->status == ErpNaryad::STATUS_IN_PAUSE)){
                    $response['status']='danger';
                    $response['message']=$naryad->name
                    .'. <span style="color:yellow">Статус: '.$naryad->status.'</span>';
                }elseif(!$naryad->location_id && ($naryad->status == null || $naryad->status == '') && $naryad->start){
                    $response['status']='danger';
                    $response['message']=$naryad->name
                    .'. <span style="color:yellow">Статус: Ожидается к выдаче на участок '. ErpLocation::findOne($naryad->start)->name.' </span>';
                }elseif($naryad->location_id){
                    $response['status']='success';
                    $response['message']=$naryad->name
                        .'. <span style="color:black">Участок: '.$naryad->location->name.'</span>'
                        .'. <span style="color:yellow">Статус: '.$naryad->status.'</span>'
                        .'. <span style="color:blue">Работник: '.($naryad->user_id ? $naryad->user->name : 'не задан').'</span>';
                }
            //функционал выдачи наряда на человека путем пропикивания наряда. Данный функционал только для оклейки
            }elseif ($model->validate() && $naryad->start == $model->location_id ){
//                Внутренняя логика - только участок оклейки может получить наряд таким образом
                if ($model->location_id == Yii::$app->params['erp_okleika'])
                {
                    $greenLight = true;

                    $naryadsOnUser=ErpNaryad::find()
                    ->where(['and',
                        ['location_id'=>$model->location_id], /*Все наряды текущей локации*/
                        ['status' => ErpNaryad::STATUS_IN_WORK], /*Кроме нарядов на паузе*/
                        ['user_id' => $model->user_id], /*Кроме нарядов на паузе*/
                        ['isAdd' => true] /*Кроме нарядов на паузе*/
                    ])->count();


                    if ($model->user_id !== 58 && $naryadsOnUser > 12) $greenLight = false;

                    if ($greenLight) {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }else{
                        if ($model->user->hasNaryadsOnHandsOld()) {
                            $response['status']='danger';
                            $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                        } else {
                            $response['status'] = 'danger';
                            $response['message'] = 'Сначала сдай все старые наряды!';
                        }
                    }

                //Если же это какой то иной участок, ты предупреждаем пользователя, что он так поступать не может
                } elseif ($model->location_id == 18 && preg_match('/OT-(2079)/', $naryad->article) === 1 && preg_match('/Фильтры/', $model->location->recieveActiveWorkplacesProperties($model->user_id)) === 1) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } else {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }
                } elseif ($model->location_id == 18 && preg_match('/OT-(1189)/', $naryad->article) === 1 && preg_match('/Органайзеры/', $model->location->recieveActiveWorkplacesProperties($model->user_id)) === 1) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } else {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }
                } elseif ($model->location_id == 18 && preg_match('/OT-(1211)/', $naryad->article) === 1 && preg_match('/ЧехолНаСпинку/', $model->location->recieveActiveWorkplacesProperties($model->user_id)) === 1) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } else {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }
                } elseif (in_array($model->location_id,[18,6]) && preg_match('/OT-(2061)/', $naryad->article) === 1 && preg_match('/Автоодеяло/', $model->location->recieveActiveWorkplacesProperties($model->user_id)) === 1) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } else {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }
                } elseif (in_array($model->location_id,[24,23,21,20]) && preg_match('/OT-(2061)/', $naryad->article) === 1) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } else {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }
                } elseif ($model->location_id == Yii::$app->params['erp_shveika']) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } elseif ($naryad->status != ErpNaryad::STATUS_IN_WORK && $naryad->status) {
                        $response['status']='danger';
                        $response['message']='Вы не можете получить данный наряд № '.mb_strtolower($naryad->name) . ' ! Он на паузе!';
                    } else {
                        $articleObj = new Article($naryad->article);
                        if ($articleObj->isDontLook() && $model->user->canCreateDontlook) {
                            $naryad->nextPlaceAdd($model->user->id);
                            $model->workOrders[]=$naryad->id;
                            $response['status']='success';
                            $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                        } elseif (!$model->user->canCreateDontlook) {
                            $response['status'] = 'danger';
                            $response['message'] = 'В программе указано, что Вам не разрешено шить Don\'t look';
                        } else {
                            $response['status'] = 'danger';
                            $response['message'] = 'Нельзя получать никакие другие наряды на себя, кроме Don\'t look!';
                        }
                    }
                } elseif ($model->location_id == Yii::$app->params['erp_izgib']) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } elseif ($naryad->status != ErpNaryad::STATUS_IN_WORK && $naryad->status) {
                        $response['status']='danger';
                        $response['message']='Вы не можете получить данный наряд № '.mb_strtolower($naryad->name) . ' ! Он на паузе!';
                    } else {
                        $articleObj = new Article($naryad->article);
                        if ($articleObj->isDontLook()) {
                            $naryad->nextPlaceAdd($model->user->id);
                            $model->workOrders[]=$naryad->id;
                            $response['status']='success';
                            $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                        } else {
                            $response['status'] = 'danger';
                            $response['message'] = 'Нельзя получать никакие другие наряды на себя, кроме доунт луков!';
                        }
                    }
                } elseif ($model->location_id == Yii::$app->params['erp_otk'] && preg_match('/OT-(1189)/', $naryad->article) === 1) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } else {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }
                } elseif ($model->location_id == Yii::$app->params['erp_otk'] && preg_match('/OT-(2061)/', $naryad->article) === 1) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } else {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }
                } elseif ($model->location_id == Yii::$app->params['erp_otk'] && preg_match('/OT-(1211)/', $naryad->article) === 1) {
                    if ($model->user->hasNaryadsOnHandsOld()) {
                        $response['status']='danger';
                        $response['message']=$model->user->name.' , у Вас есть наряды, которые были получены более 24 часов назад. Сдайте их или обратитесь к диспетчеру';
                    } else {
                        $naryad->nextPlaceAdd($model->user->id);
                        $model->workOrders[]=$naryad->id;
                        $response['status']='success';
                        $response['message']=$model->user->name.' получил '.mb_strtolower($naryad->name);
                    }
                }else{
                    $response['status']='danger';
                    $response['message']='Нельзя получать наряды по одному. Используй штрихкод для получения!!!';
                }
            //функционал предупреждения пользователя о том, что наряды в данном терминале только сдаються.
            }elseif ($model->validate() && $naryad->location_id == $model->location_id && $naryad->updated_at < (time() + 60*$limit3) && $model->location_id == Yii::$app->params['erp_okleika']) {
                $naryad->updated_at = time();
                $naryad->save();
                $response['status']='success';
                $response['message']='Спасибо Вам. Мы поняли, что вы контролируете движение данного наряда через вверенный Вам участок';
//            } elseif ($model->validate() && $model->location_id == Yii::$app->params['erp_izgib'] && $naryad->location_id == $model->location_id && $naryad->user_id == $model->user_id) {
//                if ($model->user->hasLowNaryadsOnHands()) {
//                    $naryad->endPlace($model->user_id);
//                    Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['isAnotherPlaced' => true],['id' => $naryad->id])->execute();
//                    $model->workOrders[] = $naryad->id;
//                    $response['message'] = $model->user->name
//                        . ' сдал ' . mb_strtolower($naryad->name)
//                        . ' на участок '
//                        . "<span style = 'color:#b22222;font-size:1.4em;text-transform: uppercase;font-weight:900' >" . ($naryad->start ? ErpLocation::findOne($naryad->start)->name : "Склад") . "</span>";
//
//                    //Попытка распределить наряд по литере.
//                    $productionLiteralResponse = (new DistributionProductionLiterals())->distribute($naryad->barcode);
//                    $response['status'] = $productionLiteralResponse['status'];
//                    $response['message'] .= '<br>' . $productionLiteralResponse['message'];
//                } else {
//                    $response['status'] = 'danger';
//                    $response['message'] = $model->user->name
//                        . ' , Вы не можете сдавать наряды в этом терминале, если у вас на руках их больше чем 2!';
//                }
            }elseif ($model->validate() && !$naryad->start
                && ($naryad->location_id !== null || $naryad->location_id !== '' || $naryad->location_id !== Yii::$app->params['erp_dispatcher'])){
                $response['status']='danger';
                $response['message']='В данном терминале невозможно сдавать наряды. Это терминал выдачи !!!';
            //если же условие на выдачу вручную не прошло, выводим ошибки
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                $response['message']=implode(', ', $errors);
            }
        } elseif ($act) {//печать нарядов из облака
            if (!$model->user_id) {
                $response['status'] = 'danger';
                $response['message'] = 'Это акт приема передачи';
                //здесь функционал сдачи наряда. Сдать модем используя разную логику
            } elseif ($model->validate()) {
                //первый алгоритм непонятный это для швейки
                //Если швейка, пользователь который получил равен пользователю который сдает
                $naryads = ErpNaryad::find()->where(['actNumber' => $act])->all();

                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    $userIds = null;
                    foreach ($naryads as $naryad) {

                        if ($naryad->start != $model->location_id) {
                            $transaction->rollBack();
                            $response['status'] = 'danger';
                            $response['message'] = 'Что то пошло не так ';
                            return $response;
                        }
                        if (!$userIds)
                            $userIds = ErpNaryadLog::getUsersIds($naryad->id);
//                        if (in_array($model->user_id,$userIds) && $model->location_id == Yii::$app->params['erp_otk']) {
//                            $transaction->rollBack();
//                            $response['status'] = 'danger';
//                            $response['message'] = 'Вы не можете получить наряды для проверки на ОТК, так как вы их уже изготавливали';
//                            return $response;
//                        }
                    }

                    $vidacha = new ErpMainTerminal();
                    $vidacha->user_id = $model->user->id;
                    $vidacha->location_id = $model->location_id;
                    if ($naryads) {
                        $vidacha->workOrders = ArrayHelper::map($naryads, 'id', 'id');
                    }

                    if ($vidacha->issueAndPrintOutWorkOrders()) {
                        // foreach ($vidacha->print as $naryad_id) {
                        //     $str = trim(str_replace('Наряд', '', ErpNaryad::findOne($naryad_id)->name));
                        //     $str = mb_substr($str, 5, mb_strlen($str));
                        //     $result[] = $str;
                        // }
                        $response['status'] = 'success';
                        $response['message'] = 'Вы получили наряды в количестве: ' . count($naryads) . ' ед. по акту приема передачи № ' . $act; /*№: ' . implode(', ', $result);*/
//                        $response['vidacha'] = $vidacha->printableWorkOrders;
//                        $response['reestr'] = $vidacha->printRegister;
                        $vidacha->saveProperties();

                        $transaction->commit();
                    }
                } catch (\Exception $exception) {
                    $transaction->rollBack();
                    $response['status'] = 'danger';
                    $response['message'] = 'Что то пошло не так ';
                    return $response;
                }
            } else {
                if ($model->user_id) {
                    $errors = [];
                    foreach ($model->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }
            }
        } elseif ($prodGroup) {//печать нарядов из облака
            if (!$model->user_id) {
                $response['status'] = 'danger';
                $response['message'] = 'Это группа нарядов';
                //здесь функционал сдачи наряда. Сдать модем используя разную логику
            } elseif ($model->validate()) {
                //первый алгоритм непонятный это для швейки
                //Если швейка, пользователь который получил равен пользователю который сдает
                $naryads = ErpNaryad::find()->where(['prodGroup' => $prodGroup])->all();

                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    $userIds = null;
                    foreach ($naryads as $naryad) {

                        if ($naryad->start != $model->location_id) {
                            $transaction->rollBack();
                            $response['status'] = 'danger';
                            $response['message'] = 'Что то пошло не так ';
                            return $response;
                        }
                        if (!$userIds)
                            $userIds = ErpNaryadLog::getUsersIds($naryad->id);
//                        if (in_array($model->user_id,$userIds) && $model->location_id == Yii::$app->params['erp_otk'] && $prodGroup != 120) {
//                            $transaction->rollBack();
//                            $response['status'] = 'danger';
//                            $response['message'] = 'Вы не можете получить наряды для проверки на ОТК, так как вы их уже изготавливали';
//                            return $response;
//                        }
                    }

                    $vidacha = new ErpMainTerminal();
                    $vidacha->user_id = $model->user->id;
                    $vidacha->location_id = $model->location_id;
                    if ($naryads) {
                        $vidacha->workOrders = ArrayHelper::map($naryads, 'id', 'id');
                    }

                    if ($vidacha->issueAndPrintOutWorkOrders()) {
                        // foreach ($vidacha->print as $naryad_id) {
                        //     $str = trim(str_replace('Наряд', '', ErpNaryad::findOne($naryad_id)->name));
                        //     $str = mb_substr($str, 5, mb_strlen($str));
                        //     $result[] = $str;
                        // }
                        $response['status'] = 'success';
                        $response['message'] = 'Вы получили ГРУППУ нарядов в количестве: ' . count($naryads) . ' ед.  № ' . $prodGroup; /*№: ' . implode(', ', $result);*/
//                        $response['vidacha'] = $vidacha->printableWorkOrders;
//                        $response['reestr'] = $vidacha->printRegister;
                        $vidacha->saveProperties();

                        $transaction->commit();
                    }
                } catch (\Exception $exception) {
                    $transaction->rollBack();
                    $response['status'] = 'danger';
                    $response['message'] = 'Что то пошло не так ';
                    return $response;
                }
            } else {
                if ($model->user_id) {
                    $errors = [];
                    foreach ($model->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }
            }
        } elseif ($addGroup) {//печать нарядов из облака
            if (!$model->user_id) {
                $response['status'] = 'danger';
                $response['message'] = 'Это реестр на доп. продукцию!!!';
                //здесь функционал сдачи наряда. Сдать модем используя разную логику
            } elseif ($model->validate()) {
                //первый алгоритм непонятный это для швейки
                //Если швейка, пользователь который получил равен пользователю который сдает
                $naryads = ErpNaryad::find()->where(['addGroup' => $addGroup])->all();

                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    $userIds = null;
                    foreach ($naryads as $naryad) {

                        if ($naryad->start != $model->location_id) {
                            $transaction->rollBack();
                            $response['status'] = 'danger';
                            $response['message'] = 'Что то пошло не так ';
                            return $response;
                        }
                        if (!$userIds)
                            $userIds = ErpNaryadLog::getUsersIds($naryad->id);
//                        if (in_array($model->user_id,$userIds) && $model->location_id == Yii::$app->params['erp_otk'] && $prodGroup != 120) {
//                            $transaction->rollBack();
//                            $response['status'] = 'danger';
//                            $response['message'] = 'Вы не можете получить наряды для проверки на ОТК, так как вы их уже изготавливали';
//                            return $response;
//                        }
                    }

                    $vidacha = new ErpMainTerminal();
                    $vidacha->user_id = $model->user->id;
                    $vidacha->location_id = $model->location_id;
                    if ($naryads) {
                        $vidacha->workOrders = ArrayHelper::map($naryads, 'id', 'id');
                    }

                    if ($vidacha->issueAndPrintOutWorkOrders()) {
                        // foreach ($vidacha->print as $naryad_id) {
                        //     $str = trim(str_replace('Наряд', '', ErpNaryad::findOne($naryad_id)->name));
                        //     $str = mb_substr($str, 5, mb_strlen($str));
                        //     $result[] = $str;
                        // }
                        $response['status'] = 'success';
                        $response['message'] = 'Вы получили реестр дополнительной продкции на : ' . count($naryads) . ' нарядов.  № ' . $addGroup; /*№: ' . implode(', ', $result);*/
//                        $response['vidacha'] = $vidacha->printableWorkOrders;
//                        $response['reestr'] = $vidacha->printRegister;
                        $vidacha->saveProperties();

                        $transaction->commit();
                    }
                } catch (\Exception $exception) {
                    $transaction->rollBack();
                    $response['status'] = 'danger';
                    $response['message'] = 'Что то пошло не так ';
                    return $response;
                }
            } else {
                if ($model->user_id) {
                    $errors = [];
                    foreach ($model->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }
            }
        } elseif ($barcode==md5('VIDACHNARYADOV')) {//печать нарядов из облака
            if ($model->validate()) {
                $vidacha = new ErpMainTerminal();
                $vidacha->user_id = $model->user->id;
                $vidacha->location_id = $model->location_id;
                $naryads = $model->getNaryadsNaVidachu();
                if ($naryads) {
                    $vidacha->workOrders = ArrayHelper::map($naryads, 'id', 'id');
                }

                if ($vidacha->issueAndPrintOutWorkOrders()) {
                    // foreach ($vidacha->print as $naryad_id) {
                    //     $str = trim(str_replace('Наряд', '', ErpNaryad::findOne($naryad_id)->name));
                    //     $str = mb_substr($str, 5, mb_strlen($str));
                    //     $result[] = $str;
                    // }
                    $response['status'] = 'success';
                    $response['message'] = 'Вы получили наряды'; /*№: ' . implode(', ', $result);*/
                    $response['vidacha'] = $vidacha->printableWorkOrders;
                    $response['reestr'] = $vidacha->printRegister;
                    $vidacha->saveProperties();
                } else {
                    $errors = [];
                    foreach ($vidacha->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }
            } else {
                $errors = [];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[] = $value[0];
                }
                $response['message'] = implode(', ', $errors);
            }
        }
        $model->saveProperties();

        return $response;
    }


    /**
     * Работа сканера на участках
     * @param string $barcode штрихкод
     * @return json
     */
    public function actionSearchSdacha($barcode)
    {
        //Данное действие только для Аякс запроса
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = new ErpTerminal();
        $barcode = $model->toLatin($barcode);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status']='error';
        $response['message']='Не найдено';

        //Все что можно будет искать через данный терминал  - это штрихкод участка, пользователя, наряда
        $location=ErpLocation::find()->where(['barcode'=>$barcode])->one();
        $user=ErpUser::find()->where(['barcode'=>$barcode])->one();
        $upn=Naryad::find()->where(['barcode'=>$barcode])->one();
        $naryad=ErpNaryad::find()->where(['barcode'=>$barcode])->one();
        if (!$naryad && $upn) {
            $naryad = ErpNaryad::find()->where(['logist_id'=>$upn->id])->one();
        }
        $act = ErpNaryad::findAct($barcode);
        $prodGroup = ErpNaryad::findProdGroup($barcode);
        $addGroup = ErpNaryad::findAddGroup($barcode);

        //Локацию ищем только для того, чтобы сменить локацию пользователя и установить локацию для терминала.
        //Локация устанавливаеться на весь срок времени, пока неизмениться
        if ($location){//поиск участка
            $model->location_id=$location->id;
            //регистрация пользователя на участке
            // if ($model->user_id && ($user=ErpUser::findOne($model->user_id))!=null) {
            //     $user->location_id=$model->location_id;
            //     $user->save();
            // }
            if ($model->validate()){
                $response['status']='success';
                $response['message']=$model->user->name.' - '. $location->name;
            } else {
                $errors=[];
                foreach ($model->getErrors() as $key => $value) {
                    $errors[]=$value[0];
                }
                $response['message']=implode(', ', $errors);
            }
        //Ищем сотрудника для того чтобы установить и локацию и сотрудника для терминала    
        } elseif ($user){//поиск сотрудника
            //нулевой пик
            //Сюда входят сообщения типа для вас не зарагестрировано место
            //Сюда входит приход и уход работников, для которых зарегестрировано место только под основную работу
            if ($model->user_id !== $user->id){

                //если пользователь запланирован на непроизводственные места и находится на непроизв рабочем месте и забыл отметить уход
                if($user->hasSupportPlanWorkplaces() && $user->isNoOutWorkYesterday() && $user->isOnWork())
                {
                    //отмечаем уход через минуту после последней регистрации
                    $user->initProductionDeRegistrationYestarday($user->isNoOutWorkYesterday());
                    $user->initSupportDeRegistrationYestarday($user->isNoOutWorkYesterday());
                    $user->initComeOutWorkYestarday($user->isNoOutWorkYesterday());
                    $model->user_id = null;
                    $model->location_id = null;
                    $model->saveProperties();
                    $response['status']='success';
                    $response['message']= $user->name.'<br>'
                        .'<span class="text-danger" style="font-size:1em;font-weight:bold">В предыдущий день уход не был зафиксирован. Последний день не был учтен</span><br>'
                        .'1. - Для регистрации на сегодняшний день пропикайте бэйджиком два раза!';
                    return $response;
                }


                if ($user->isOutWork() && !$user->hasProductionPlanWorkplaces() && !$user->hasSupportPlanWorkplaces()) {
                    $response['status']='danger';
                    $response['message']='Для Вас нет запланированных рабочих мест.<br> Обратитесь к руководителю для уточнения.';
                    return $response;
                }

                if ($user->isOutWork()){
                    $model->user_id = $user->id;
                    $model->location_id = null;
                    $model->saveProperties();
                    $response['status']='success';
                    $response['message']='Здравствуйте, '.$user->name.'<br>'
                                        .'Ваш статус - НЕ ЗАРЕГЕСТРИРОВАН НА РАБОТЕ <br>'
                                        .'Для регистрации на рабочих местах пропикайте второй раз.';
                    return $response;
                }

                if ($user->isOnWork() && !$user->hasProductionPlanWorkplaces() && !$user->onProductionWorkplaces() && !$user->hasNaryadsOnHands() && $user->onSupportWorkplaces()){
                    $model->user_id = $user->id;
                    $model->location_id = @$user->location_id;
                    $model->saveProperties();
                    $response['status']='success';
                    $response['message']= $user->name.'<br>'
                                        .'Ваш статус - ЗАРЕГЕСТРИРОВАН НА РАБОЧИХ МЕСТАХ<br>'
                                        .'1. - Для ухода с работы пропикайте бэйджиком второй раз!<br>';
                    return $response;
                }

                if ($user->isOnWork() && $user->hasProductionPlanWorkplaces() && !$user->onProductionWorkplaces() && !$user->hasNaryadsOnHands() && $user->onSupportWorkplaces()){
                    $model->user_id = $user->id;
                    $model->location_id = @$user->location_id;
                    $model->saveProperties();
                    $response['status']='success';
                    $response['message']= $user->name.'<br>'
                                        .'Ваш статус - ЗАРЕГЕСТРИРОВАН НА РАБОЧИХ МЕСТАХ<br>'
                                        .'1. - Для ухода с работы пропикайте бэйджиком второй раз!<br>'
                                        .'2. - Для получения нарядов на производство обратитесь к терминалу выдачи!<br>';
                    return $response;
                }

                if ($user->isOnWork()){
                    $model->user_id = $user->id;
                    $model->location_id = @$user->location_id;
                    $model->saveProperties();
                    $response['status']='success';
                    $response['message']= $user->name.'<br>'
                                        .'Ваш статус - ЗАРЕГЕСТРИРОВАН НА РАБОЧИХ МЕСТАХ<br>'
                                        .'1. - Для ухода с работы пропикайте бэйджиком второй раз!<br>'
                                        .'2. - Для сдачи наряда пикните штрихкод наряда!.';
                    return $response;
                }

            }

            //приход на работу производственного работника
            if ($model->user_id == $user->id && $user->isOutWork() && $user->hasProductionPlanWorkplaces() && !$user->onProductionWorkplaces()){
                $user->initComeToWork();
                $user->initSupportRegistration();
                $user->initProductionRegistration();
                $model->user_id = $user->id;
                $model->location_id = $user->location_id;
                $model->saveProperties();
                $response['status']='success';
                $response['message']= $user->name.'<br>'
                                     .'<span class="text-danger" style="font-size:2em;font-weight:bold">ПРИХОД</span><br>'
                                     .'Ваш статус - ЗАРЕГЕСТРИРОВАН НА РАБОЧИХ МЕСТАХ<br>'
                                     .'Дата регистрации '. Yii::$app->formatter->asDateTime(time()).'!<br>'
                                     .'1. - Чтобы уйти с работы пропикайте бэйджиком второй раз!<br>'
                                     .'2. - Для сдачи наряда пикните штрихкод наряда!<br>'
                                     . ($user->hasNaryadsOnHands() ? '<span class="text-danger" style="display:inline-block; background-color: black; font-size:2em;font-weight:bold">У ВАС есть НЕ СДАННЫЕ наряды на руках!!!</span>' : '');
                return $response;
            }

            //приход на работу непроизводственного работника
            if ($model->user_id == $user->id && $user->isOutWork() && !$user->hasProductionPlanWorkplaces() && $user->hasSupportPlanWorkplaces() && !$user->onSupportWorkplaces()){
                $user->initComeToWork();
                $user->initSupportRegistration();
                $model->user_id = null;
                $model->location_id = null;
                $model->saveProperties();
                $response['status']='success';
                $response['message']= $user->name.'<br>'
                                     .'Ваш статус - ЗАРЕГЕСТРИРОВАН НА РАБОЧИХ МЕСТАХ<br>'
                                     .'<span class="text-danger" style="font-size:2em;font-weight:bold">ПРИХОД</span><br>'
                                     .'Дата регистрации '. Yii::$app->formatter->asDateTime(time()).'!<br>';
                return $response;
            }

            //уход с работы производственного работника
            if ($model->user_id == $user->id && $user->isOnWork() && $user->hasProductionPlanWorkplaces() && $user->onProductionWorkplaces()){
                $user->initProductionDeRegistration();
                $user->initSupportDeRegistration();
                $user->initComeOutWork();
                $model->user_id = null;
                $model->location_id = null;
                $model->saveProperties();
                $response['status']='success';
                $response['message']='До свидания, '.$user->name.'<br>'
                                     .'<span class="text-danger" style="font-size:2em;font-weight:bold">УХОД</span><br>'
                                    .'Ваш статус - НЕ ЗАРЕГЕСТРИРОВАН НА РАБОЧИХ МЕСТАХ<br>'
                                    .'Ваш уход зарегестрирован в '. Yii::$app->formatter->asDateTime(time()).'!<br>'
                                    .'1. - Чтобы вернуться на работу пропикайте бэйджиком второй раз!<br>'
                                    . ($user->hasNaryadsOnHands() ? '<span class="text-danger" style="display:inline-block; background-color: black; font-size:2em;font-weight:bold">У ВАС есть НЕ СДАННЫЕ наряды на руках!!!</span>' : '');
                return $response;
            }


            //уход с работы производственного работника невозможне в случае если на нем есть несданные наряды
//            if ($model->user_id == $user->id && $user->isOnWork() && $user->hasProductionPlanWorkplaces() && $user->onProductionWorkplaces() && $user->hasNaryadsOnHands()){
//                $model->user_id = null;
//                $model->location_id = null;
//                $model->saveProperties();
//                $response['status']='danger';
//                $response['message']= 'У Вас есть не сданные наряды!<br>'
//                                     .'Вы не можете уйти с работы пока не разберетесь с нарядами, которые у ВАС на руках!!!';
//                return $response;
//            }

            //уход с работы непроизводственного работника работника
            if ($model->user_id == $user->id && $user->isOnWork() && !$user->hasProductionPlanWorkplaces() && !$user->onProductionWorkplaces() && !$user->hasNaryadsOnHands() && $user->onSupportWorkplaces()){
                $user->initProductionDeRegistration();
                $user->initSupportDeRegistration();
                $user->initComeOutWork();
                $model->user_id = null;
                $model->location_id = null;
                $model->saveProperties();
                $response['status']='success';
                $response['message']='До свидания, '.$user->name.'<br>'
                                    .'<span class="text-danger" style="font-size:2em;font-weight:bold">УХОД</span><br>'
                                    .'Ваш статус - НЕ ЗАРЕГЕСТРИРОВАН НА РАБОЧИХ МЕСТАХ<br>'
                                    .'Ваш уход зарегестрирован в '. Yii::$app->formatter->asDateTime(time()).'!<br>';
                return $response;
            }
        } elseif ($upn && ($cheholNaryad = ErpNaryad::find()->where(['logist_id' => $upn->id])->one()) && $model->user_id && preg_match('/OT-(1211)/', $cheholNaryad->article) === 1 && $model->location_id == Yii::$app->params['erp_otk']) {
            if ($cheholNaryad  && $model->location_id == $cheholNaryad->location_id
                && $model->user->id == $cheholNaryad->user_id
                && $cheholNaryad->endPlace($model->user_id) ) {

                //Приходуем органайзеры сразу на склад органайзеров
                $storage = Storage::findOne(6);
                if ($storage && !StorageState::find()->where(['upn_id' => $upn->id])->exists()) {
                    $row = new StorageState();
                    $row->upn_id = $upn->id;
                    $row->storage_id = $storage->id;
                    $row->literal = 'TE-A3000';
                    $row->save();

                    Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['distributed' => true, 'distributed_date' => time()],['id' => $cheholNaryad->id])->execute();
                }

                $model->workOrders[] = $cheholNaryad->id;
                $response['status'] = 'success';
                $response['message'] = $model->user->name
                    . ' сдал ' . mb_strtolower($cheholNaryad->name)
                    . ' на участок '
                    . "<span style = 'color:#B22222;font-size:1.4em;text-transform: uppercase;font-weight:900' >" . ($cheholNaryad->start ? ErpLocation::findOne($cheholNaryad->start)->name : "Склад") . "</span>";
            }
        } elseif ($naryad) {//поиска наряда
            /**
             * @var ErpNaryad $naryad
             */
            //если наряд пикали без пользователя, тогда хотели проверить где сейчас наряд

            //Распечатка этикетки - эксперементально
            if ((!$model->user_id || $naryad->user_id != $model->user_id) && $naryad->location_id == Yii::$app->params['erp_izgib']) {
                if (!$model->labelPrint) {
                    $model->labelPrint = $naryad->id;
                    $model->user_id = null;
                    $model->saveProperties();
                } elseif ($model->labelPrint && $model->labelPrint == $naryad->id) {
                    $response['status'] = 'success';
                    $response['etiketka'] = $naryad->id;
                    $response['message'] = 'Печать этикетки для '
                        . mb_strtolower($naryad->name) . '!!!';
                    $model->labelPrint = null;
                    $model->user_id = null;
                    $model->saveProperties();
                    return $response;
                }
            }

            if (!$model->user_id) {
                if (!$naryad->location_id && ($naryad->status == ErpNaryad::STATUS_READY)) {
                    $response['status'] = 'success';
                    $response['message'] = $naryad->name
                        . '. <span style="color:yellow">Статус: ' . $naryad->status . '</span>';
                } elseif (!$naryad->location_id && ($naryad->status == ErpNaryad::STATUS_CANCEL)) {
                    $response['status'] = 'danger';
                    $response['message'] = $naryad->name
                        . '. <span style="color:yellow">Статус: ' . $naryad->status . '</span>';
                } elseif (!$naryad->location_id && ($naryad->status == ErpNaryad::STATUS_IN_PAUSE)) {
                    $response['status'] = 'danger';
                    $response['message'] = $naryad->name
                        . '. <span style="color:yellow">Статус: ' . $naryad->status . '</span>';
                } elseif (!$naryad->location_id && ($naryad->status == null || $naryad->status == '') && $naryad->start) {
                    $response['status'] = 'danger';
                    $response['message'] = $naryad->name
                        . '. <span style="color:yellow">Статус: Ожидается к выдаче на участок ' . ErpLocation::findOne($naryad->start)->name . ' </span>';
               } elseif ($naryad->location_id) {
                    $response['status'] = 'success';
                    $response['message'] = $naryad->name
                        . '. <span style="color:black">Участок: ' . $naryad->location->name . '</span>'
                        . '. <span style="color:yellow">Статус: ' . $naryad->status . '</span>'
                        . '. <span style="color:blue">Работник: ' . ($naryad->user_id ? $naryad->user->name : 'не задан') . '</span>';

                    $logs = ErpNaryadLog::find()
                        ->select(['locationFromName','workerName'])
                        ->where(['workOrderId' => $naryad->id])
                        ->andWhere(['<>','locationFromId',7])
                        ->andWhere(['is not','locationFromId',null])
                        ->orderBy('date')
                        ->all();

                    if ($logs) {
                        $response['message'] .= '<br> <hr><span style="color:darkblue;">История наряда:</span> <br>';

                        foreach ($logs as $log) {
                            $response['message'] .= "<span style='color:black;'>{$log->locationFromName}</span> - {$log->workerName} <br>";
                        }
                    }

                    if ($prodliteral = SProductionLiteral::workOrderInfo($naryad->id, $naryad->start))
                        $response['message'] .= "<hr><br> ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $prodliteral  </span>";
                }
                //здесь функционал сдачи наряда. Сдать модем используя разную логику
            } elseif ($model->validate()) {
                //Костыль для двойного пропикивания органайзеров
                if (preg_match('/OT-(1189|1190|2061)/', $naryad->article) === 1 && (!$model->doubleWorkOrderClickId || $model->doubleWorkOrderClickId !== $naryad->id)) {
                    $response['error'] = 'warning';
                    $response['message'] = 'Вы пикнули штрихкод одиночного наряда № ' . $naryad->logist_id . '! Пикните еще раз, чтобы сдать!' ;
                    $model->doubleWorkOrderClickId = $naryad->id;
                    $model->saveProperties();
                    return $response;
                } elseif (preg_match('/OT-(1189|1190|2061)/', $naryad->article) === 1 && $model->doubleWorkOrderClickId === $naryad->id) {
                    $model->doubleWorkOrderClickId = null;
                    $model->saveProperties();
                }
                //первый алгоритм непонятный это для швейки
                //Если швейка, пользователь который получил равен пользователю который сдает
                if ($naryad->status == ErpNaryad::STATUS_IN_PAUSE) {
                    $response['error'] = 'danger';
                    $response['message'] = 'ВНИМАНИЕ!!! Наряд на ПАУЗЕ !!! Обратитесь к ДИСПЕТЧЕРУ!!!';
                } elseif ($model->location_id == Yii::$app->params['erp_okleika']
                    && $model->user->id == $naryad->user_id
                    && $model->location_id == $naryad->location_id
                    && ($lastDate = ErpNaryadLog::find()->select('date')->where(['locationToId' => Yii::$app->params['erp_okleika']])->andWhere(['workOrderId' => $naryad->id])->scalar()) && $lastDate > time() - 60*10 ) {
                    $response['status'] = 'danger';
                    $response['message'] = $model->user->name . ' Вы получили наряд ' . mb_strtolower($naryad->name) . ' менее 10 минут назад и поэтому не можете его сдать!';
                    // Если это оклейка тогда
//                } elseif ($model->location_id == 18
//                    && preg_match('/OT-(1211)/', $naryad->article) !== 1
//                    && $model->user->id == $naryad->user_id
//                    && $model->location_id == $naryad->location_id
//                    && ($lastDate = ErpNaryadLog::find()->select('date')->where(['locationToId' => 18])->andWhere(['workOrderId' => $naryad->id])->scalar()) && $lastDate > time() - 60*60 ) {
//                    $response['status'] = 'danger';
//                    $response['message'] = $model->user->name . ' Вы получили наряд ' . mb_strtolower($naryad->name) . ' менее часа назад и поэтому не можете его сдать!';
//                    // Если это оклейка тогда
                } elseif ($model->location_id == Yii::$app->params['erp_shveika']
                    && $model->user->id == $naryad->user_id
                    && $model->location_id == $naryad->location_id
                    && $naryad->isNeedTemplate) {
                    if ($naryad->endPlaceTemplate($model->user->id)) {
                        $model->workOrders[] = $naryad->id;
                        $response['status'] = 'success';
                        $response['message'] = $model->user->name . ' Вы успешно изготовли лекало для ' . mb_strtolower($naryad->name) . ' Заработная плата начислена.';
                    } else {
                        $model->workOrders[] = $naryad->id;
                        $response['status'] = 'danger';
                        $response['message'] = $model->user->name . '. Ваше лекало еще не принято диспетчером. Передайте ему лекало, чтобы он его заерегестрировал в системе учета швейных лекал. После этого вы успешно сдадите ' . mb_strtolower($naryad->name);
                    }
                    // Если это оклейка тогда
                } elseif ($model->location_id == Yii::$app->params['erp_okleika']
                    && $model->location_id == $naryad->location_id
                    && $naryad->endPlace($model->user_id)) {
                    $model->workOrders[] = $naryad->id;
                    $response['status'] = 'success';
                    $response['message'] = $model->user->name
                        . ' сдал ' . mb_strtolower($naryad->name)
                        . ' на участок '
                        . "<span style = 'color:#B22222;font-size:1.4em;text-transform: uppercase;font-weight:900' >" . ($naryad->start ? ErpLocation::findOne($naryad->start)->name : "Склад") . "</span>";
                    //Попытка распределить наряд по литере.
                    $productionLiteralResponse = (new DistributionProductionLiterals())->distribute($naryad->barcode);
                    $response['status'] = $productionLiteralResponse['status'];
                    $response['message'] .= '<br>' . $productionLiteralResponse['message'];

                    // Если это лейбл тогда проверка по пользователю тоже отсутсвтует
                } elseif ($model->location_id == Yii::$app->params['erp_otk']
                    && $model->location_id == $naryad->location_id) {

                    $upn = Naryad::find()->where(['id'=>$naryad->logist_id])->one();
                    if (!$upn) {
                        $response['status']='danger';
                        $response['message']= 'Данный UPN не существует!!! Обратитесь к диспетчеру!!!';
                    }

                    //Костыль для сдачи органайзеров по наряду на ОТК
                    if (preg_match('/OT-(1189)/', $naryad->article) === 1) {
                        if ($model->user->id == $naryad->user_id
                            && $naryad->endPlace($model->user_id) ) {

                            //Приходуем органайзеры сразу на склад органайзеров
                            $storage = Storage::findOne(6);
                            if ($storage && !StorageState::find()->where(['upn_id' => $upn->id])->exists()) {
                                $row = new StorageState();
                                $row->upn_id = $upn->id;
                                $row->storage_id = $storage->id;
                                $row->literal = 'TE-A2000';
                                $row->save();

                                Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['distributed' => true, 'distributed_date' => time()],['id' => $naryad->id])->execute();
                            }

                            if ($upn->reserved_id === null) {

                                //Нам надо получить заявки, которые еще не упакованы
                                $shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
                                $shipments = $shipmentManager->findWhereColumn('id',['or',['logisticsStatus' => 'shipped'],['logisticsStatus' => 'packed'],['logisticsStatus' => 'handled']]);

                                //Сначала находим все неотгруженные позиции органайзеров
                                $orderEntryIds = OrderEntry::find()
                                    ->select('id')
                                    ->where('order_id is not NULL')
                                    ->andWhere('is_deleted is NULL')
                                    ->andWhere(['>','order_id',30000])
                                    ->andWhere(['article' => $naryad->article])
                                    ->andWhere(['or','shipment_id not IN (' .implode(',',$shipments) .')','shipment_id is NULL'] )
                                    ->distinct()
                                    ->column();

                                //Далее из этого списка выбираем позиции, которые уже лежат на складе
                                $naryadIds = Naryad::find()
                                    ->select('id')
                                    ->where(['reserved_id' => $orderEntryIds])
                                    ->column();

                                $storageIds = StorageState::find()
                                    ->select('upn_id')
                                    ->where(['upn_id' => $naryadIds])
                                    ->column();

                                $orderEntryIdsNotInStorage = Naryad::find()
                                    ->select('reserved_id')
                                    ->where(['id' => $naryadIds])
                                    ->andWhere(['not in','id',$storageIds])
                                    ->column();

                                //Находим все заказы, которым принадлежат данные позиции
                                $orderIds = OrderEntry::find()
                                    ->select('order_id')
                                    ->where(['id' => $orderEntryIdsNotInStorage])
                                    ->distinct()
                                    ->column();

                                //Находим позицию заказа, для которого зарезервируем текущий органайзер
                                $orderEntryForReserve = null;

                                $team =  \Yii::$app->team->id;

                                //Сначала ищем среди заказов физиков
                                $individualOrder = Order::find()
                                    ->where(['team_id' => $team])
                                    ->andWhere(['id' => $orderIds])
                                    ->andWhere(['category' => 'Физик'])
                                    ->orderBy('id')
                                    ->one();

                                if ($individualOrder) {
                                    $orderEntryForReserve = OrderEntry::find()
                                        ->where('is_deleted is NULL')
                                        ->andWhere(['order_id' => $individualOrder->id])
                                        ->andWhere(['article' => $naryad->article])
                                        ->andWhere(['id' => $orderEntryIdsNotInStorage])
                                        ->one();
                                } else {
                                    //Если не нашли у физиков, ищем у оптовиков
                                    $optOrder = Order::find()
                                        ->where(['team_id' => $team])
                                        ->andWhere(['id' => $orderIds])
                                        ->andWhere(['!=','category','Физик'])
                                        ->orderBy('id')
                                        ->one();
                                    if ($optOrder) {
                                        $orderEntryForReserve = OrderEntry::find()
                                            ->where('is_deleted is NULL')
                                            ->andWhere(['order_id' => $optOrder->id])
                                            ->andWhere(['article' => $naryad->article])
                                            ->andWhere(['id' => $orderEntryIdsNotInStorage])
                                            ->one();
                                    }
                                }

                                //Если была найдена позиция, перерезервируем
                                if ($orderEntryForReserve) {
                                    $currentNaryadId = @$orderEntryForReserve->upn->id;
                                    if ($currentNaryadId)
                                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['order_id' => null, 'sort' => 7, 'autoSpeed' => 0],['logist_id' => $currentNaryadId])->execute();

                                    Yii::$app->db->createCommand()->update(Naryad::tableName(),['reserved_id' => null],['reserved_id' => $orderEntryForReserve->id])->execute();
                                    Yii::$app->db->createCommand()->update(Naryad::tableName(),['reserved_id' => $orderEntryForReserve->id],['id' => $naryad->logist_id])->execute();
                                }
                            }


                            $model->workOrders[] = $naryad->id;
                            $response['status'] = 'success';
                            $response['message'] = $model->user->name
                                . ' сдал ' . mb_strtolower($naryad->name)
                                . ' на участок '
                                . "<span style = 'color:#B22222;font-size:1.4em;text-transform: uppercase;font-weight:900' >" . ($naryad->start ? ErpLocation::findOne($naryad->start)->name : "Склад") . "</span>";
                        }
                    } else {
                        //Если пикаем штрихкод upn тогда ищем группу
                        $equipment = $model->equipment;
                        /** @var $upn Naryad */

                        try {
                            $all = UpnGroup::getAllFromGroup($upn);
                            //Если пустая комплектация, тогда
                            if (count($all) > 1) {

                                $ids = ArrayHelper::map($all,'id','id');
                                $newId = $upn->id;

                                if (empty($equipment)) {
                                    $equipment[] = $newId;
                                    $model->equipment = $equipment;
                                    $model->saveProperties();
                                    $diff = array_diff($ids,$equipment);
                                    $response['status']='success';
                                    $response['message']= 'КОМПЛЕКТАЦИЯ: доложите данные UPN\'ы в комплект : ' . implode(', ',$diff);
                                }else{
                                    $diff = array_diff($equipment,$ids);
                                    if (count($diff)) {
                                        $response['status']='danger';
                                        $response['message']= 'Вы пикнули UPN из другой комплектации!!!';
                                        return $response;
                                    }
                                    if (!in_array($newId,$equipment)) {
                                        $equipment[] = $newId;
                                    }else{
                                        $diff = array_diff($ids,$equipment);
                                        $model->saveProperties();
                                        $response['status']='danger';
                                        $response['message']= 'КОМПЛЕКТАЦИЯ: ВНИМАНИЕ!!! вы уже пикали данный UPN !!! Пикнете UPNы :'  . implode(', ',$diff);
                                        return $response;
                                    }
                                    $diff = array_diff($ids,$equipment);
                                    if (count($diff)) {
                                        $model->equipment = $equipment;
                                        $model->saveProperties();
                                        $response['status']='success';
                                        $response['message']= 'КОМПЛЕКТАЦИЯ: доложите данные UPN\'ы в комплект : ' . implode(', ',$diff);
                                    }else{
                                        if (($parentUpn = SParentUpn::getCommonParent($ids))) {
                                            $model->equipment = [];
                                            $model->saveProperties();
                                            $response['status']='success';
                                            $response['message']= 'КОМПЛЕКТАЦИЯ: вы успешно скомплектовали все комплекты : ( ' . implode(', ',$ids) . ' )';
                                            $response['upnLabel'] = $parentUpn->id;
                                            return $response;
                                        }
                                        $parentUpn = SParentUpn::create($ids);
                                        if ($parentUpn) {
                                            $model->equipment = [];
                                            $model->saveProperties();
                                            $response['status']='success';
                                            $response['message']= 'КОМПЛЕКТАЦИЯ: вы успешно скомплектовали все комплекты : ( ' . implode(', ',$ids) . ' )';
                                            $response['upnLabel'] = $parentUpn->id;
                                        } else {
                                            $model->equipment = [];
                                            $model->saveProperties();
                                            $response['status']='danger';
                                            $response['message']= 'КОМПЛЕКТАЦИЯ: Ну удалось скомплектовать. Прошизошла ошибка. : ( ' . implode(', ',$ids) . ' )';
                                        }
                                    }
                                }
                            } else {
                                $model->workOrders[] = $naryad->id;
                                $response['status'] = 'success';
                                $response['etiketka'] = $naryad->id;
                                $response['message'] = 'Печать этикетки для '
                                    . mb_strtolower($naryad->name) . '!!! Сдача наряда происходит на СКЛАДЕ !!!';
                            }
                        }catch (\Exception $exception){
                            $response['status']='danger';
                            $response['message']= $exception->getMessage();
                        }
                    }

                } elseif ($model->location_id == $naryad->location_id
                    && $model->user->id == $naryad->user_id
                    && $naryad->endPlace($model->user_id)) {
                    //Костыль стирания группы дополнительной продукции, если наряд здается по отдельному штрихкоду, не в составе группы
                    if ($naryad->addGroup) {
                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['addGroup' => null],['id' => $naryad->id])->execute();
                    }
                    $model->workOrders[] = $naryad->id;
                    $response['status'] = 'success';
                    $response['message'] = $model->user->name
                        . ' сдал ' . mb_strtolower($naryad->name)
                        . ' на участок '
                        . "<span style = 'color:#B22222;font-size:1.4em;text-transform: uppercase;font-weight:900' >" . ($naryad->start ? ErpLocation::findOne($naryad->start)->name : "Склад") . "</span>";

                    //Попытка распределить наряд по литере.
                    $productionLiteralResponse = (new DistributionProductionLiterals())->distribute($naryad->barcode);
                    $response['status'] = $productionLiteralResponse['status'];
                    $response['message'] .= '<br>' . $productionLiteralResponse['message'];

                    if (!$naryad->location_id && preg_match('/OT-(2079)/', $naryad->article) !== 1)
                        $response['etiketka'] = $naryad->id;
                } elseif($model->user->id == $naryad->user_id && ($prodliteral = SProductionLiteral::workOrderInfo($naryad->id, $naryad->start))) {
                    $response['error'] = 'success';
                    $response['message'] = "ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $prodliteral  </span>";
                } else {
                    $response['error'] = 'danger';
                    $response['message'] = 'Этот наряд не для сдачи !!!';
                }

            } else {
                if ($model->user_id) {
                    $errors = [];
                    foreach ($model->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }
            }
        }elseif ($barcode==md5('VIDACHNARYADOV')) {//печать нарядов из облака
            $response['status']='danger';
            $response['message']='Данный терминал нарядов не выдает. Это терминал для садчи !!!';
        }elseif ($prodGroup) {//печать нарядов из облака
            if (!$model->user_id) {
                $groupWorkOrder = ErpNaryad::find()->where(['prodGroup' => $prodGroup])->one();

                if (!$groupWorkOrder) {
                    $response['status'] = 'danger';
                    $response['message'] = 'Это реестр группы одинаковых нарядов ! В ней нет ни одного наряда!';
                    return $response;
                }

                if (!$groupWorkOrder->location_id && ($groupWorkOrder->status == ErpNaryad::STATUS_READY)) {
                    $response['status'] = 'success';
                    $response['message'] = $prodGroup
                        . '. <span style="color:yellow">Статус: ' . $groupWorkOrder->status . '</span>';
                } elseif (!$groupWorkOrder->location_id && ($groupWorkOrder->status == ErpNaryad::STATUS_IN_PAUSE)) {
                    $response['status'] = 'danger';
                    $response['message'] = $prodGroup
                        . '. <span style="color:yellow">Статус: ' . $groupWorkOrder->status . '</span>';
                } elseif (!$groupWorkOrder->location_id && ($groupWorkOrder->status == null || $groupWorkOrder->status == '') && $groupWorkOrder->start) {
                    $response['status'] = 'danger';
                    $response['message'] = $prodGroup
                        . '. <span style="color:yellow">Статус: Ожидается к выдаче на участок ' . ErpLocation::findOne($groupWorkOrder->start)->name . ' </span>';
                } elseif ($groupWorkOrder->location_id) {
                    $response['status'] = 'success';
                    $response['message'] = $groupWorkOrder->name
                        . '. <span style="color:black">Участок: ' . $groupWorkOrder->location->name . '</span>'
                        . '. <span style="color:yellow">Статус: ' . $groupWorkOrder->status . '</span>'
                        . '. <span style="color:blue">Работник: ' . ($groupWorkOrder->user_id ? $groupWorkOrder->user->name : 'не задан') . '</span>';

                    $logs = ErpNaryadLog::find()
                        ->select(['locationFromName','workerName'])
                        ->where(['workOrderId' => $groupWorkOrder->id])
                        ->andWhere(['<>','locationFromId',7])
                        ->andWhere(['is not','locationFromId',null])
                        ->orderBy('date')
                        ->all();

                    if ($logs) {
                        $response['message'] .= '<br> <hr><span style="color:darkblue;">История наряда:</span> <br>';

                        foreach ($logs as $log) {
                            $response['message'] .= "<span style='color:black;'>{$log->locationFromName}</span> - {$log->workerName} <br>";
                        }
                    }

                    if ($prodliteral = SProductionLiteral::workOrderInfo($groupWorkOrder->id, $groupWorkOrder->start))
                        $response['message'] .= "<hr><br> ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  10em; color: red'>  $prodliteral  </span>";
                }
                //здесь функционал сдачи наряда. Сдать модем используя разную логику
                //здесь функционал сдачи наряда. Сдать модем используя разную логику
            } elseif ($model->validate()) {
                if ($model->location_id == Yii::$app->params['erp_otk']) {
                    $response['status'] = 'success';
                    $response['etiketkaProdGroup'] = ErpNaryad::PROD_GROUP_BARCODE_PREFIX . $prodGroup;
                    $response['message'] = 'Печать этикеток для группы нарядов'
                        . $prodGroup . '!!! Сдача нарядов происходит на СКЛАДЕ !!!';
                    return $response;
                }

                //первый алгоритм непонятный это для швейки
                //Если швейка, пользователь который получил равен пользователю который сдает
                $naryads = ErpNaryad::find()->where(['prodGroup' => $prodGroup])->all();

                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    foreach ($naryads as $naryad) {

                        if ($model->location_id != $naryad->location_id
                            || $model->user->id != $naryad->user_id || !$naryad->endPlace($model->user_id)) {
                            $transaction->rollBack();
                            if ($model->user->id == $naryad->user_id && ($prodliteral = SProductionLiteral::workOrderInfo($naryad->id, $naryad->start))) {
                                $response['error'] = 'success';
                                $response['message'] = "ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $prodliteral  </span>";
                            } else {
                                $response['status'] = 'danger';
                                $response['message'] = 'Что то пошло не так ';
                            }
                            return $response;
                        }
                    }

                    $transaction->commit();

                    $response['status'] = 'success';
                    $response['message'] = $model->user->name
                        . ' сдал группу нарядов № ' . $prodGroup
                        . ' на участок '
                        . "<span style = 'color:#B22222;font-size:1.4em;text-transform: uppercase;font-weight:900' >" . ($naryads[0]->start ? ErpLocation::findOne($naryads[0]->start)->name : "Склад") . "</span>";

                    $productionLiteralResponse = (new DistributionProductionLiterals())->distribute(ErpNaryad::PROD_GROUP_BARCODE_PREFIX . $prodGroup);
                    $response['status'] = $productionLiteralResponse['status'];
                    $response['message'] .= '<br>' . $productionLiteralResponse['message'];

                } catch (\Exception $exception) {
                    $transaction->rollBack();
                    $response['status'] = 'danger';
                    $response['message'] = 'Что то пошло не так ';
                    return $response;
                }
            } else {
                if ($model->user_id) {
                    $errors = [];
                    foreach ($model->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }
            }
        }elseif ($act) {
            if (!$model->user_id) {
                    $response['status'] = 'danger';
                    $response['message'] = 'Это акт приема передачи';
                //здесь функционал сдачи наряда. Сдать модем используя разную логику
            } elseif ($model->validate()) {
                if ($model->location_id == Yii::$app->params['erp_otk']) {
                    $response['status'] = 'success';
                    $response['etiketkaAct'] = ErpNaryad::MOVE_ACT_BARCODE_PREFIX . $act;
                    $response['message'] = 'Печать этикетки для акта'
                        . $act . '!!! Сдача наряда происходит на СКЛАДЕ !!!';
                    return $response;
                }

                //первый алгоритм непонятный это для швейки
                //Если швейка, пользователь который получил равен пользователю который сдает
                $naryads = ErpNaryad::find()->where(['actNumber' => $act])->all();

                    $transaction = \Yii::$app->db->beginTransaction();

                    try {
                        foreach ($naryads as $naryad) {

                            if ($model->location_id != $naryad->location_id
                                || $model->user->id != $naryad->user_id || !$naryad->endPlaceMoveAct($model->user_id)) {
                                $transaction->rollBack();
                                $response['status'] = 'danger';
                                $response['message'] = 'Что то пошло не так ';
                                return $response;
                            }
                        }

                        $transaction->commit();

                        $response['status'] = 'success';
                        $response['message'] = $model->user->name
                            . ' сдал акт приема передачи № ' . $act
                            . ' на участок '
                            . "<span style = 'color:#B22222;font-size:1.4em;text-transform: uppercase;font-weight:900' >" . ($naryads[0]->start ? ErpLocation::findOne($naryads[0]->start)->name : "Склад") . "</span>";

                    } catch (\Exception $exception) {
                        $transaction->rollBack();
                        $response['status'] = 'danger';
                        $response['message'] = 'Что то пошло не так ';
                        return $response;
                    }
            } else {
                if ($model->user_id) {
                    $errors = [];
                    foreach ($model->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }
            }
        }elseif ($addGroup) {
            if (!$model->user_id) {
                    $response['status'] = 'danger';
                    $response['message'] = 'Это реестр дополнительной продукции!!!';
                //здесь функционал сдачи наряда. Сдать модем используя разную логику
            } elseif ($model->validate()) {
                if (!$model->doubleAddGroupClickId || $model->doubleAddGroupClickId !== $addGroup) {
                    $response['error'] = 'info';
                    $response['message'] = 'Вы пикнули Реестр дополнительной продукции № ' . $addGroup . '! Пикните еще раз, чтобы сдать!' ;
                    $model->doubleAddGroupClickId = $addGroup;
                    $model->saveProperties();
                    return $response;
                } else {
                    $model->doubleAddGroupClickId = null;
                    $model->saveProperties();
                }

                if ($model->location_id == Yii::$app->params['erp_otk']) {
                    $response['status'] = 'success';
                    $response['etiketkaAddGroup'] = ErpNaryad::ADD_GROUP_BARCODE_PREFIX . $addGroup;
                    $response['message'] = 'Печать этикетки для реестра доп продукции № '
                        . $addGroup . '!!! Сдача наряда происходит на СКЛАДЕ !!!';
                    return $response;
                }

                //первый алгоритм непонятный это для швейки
                //Если швейка, пользователь который получил равен пользователю который сдает
                $naryads = ErpNaryad::find()->where(['addGroup' => $addGroup])->all();


                    $transaction = \Yii::$app->db->beginTransaction();

                    try {
                        foreach ($naryads as $naryad) {

                            if ($model->location_id != $naryad->location_id
                                || $model->user->id != $naryad->user_id || !$naryad->endPlace($model->user_id)) {
                                $transaction->rollBack();
                                $response['status'] = 'danger';
                                $response['message'] = 'Не получилось сдать с участка';
                                return $response;
                            }
                        }

                        $transaction->commit();

                        $response['status'] = 'success';
                        $response['message'] = $model->user->name
                            . ' сдал(а) реестр дополнительной продукции № ' . $addGroup
                            . ' на участок '
                            . "<span style = 'color:#B22222;font-size:1.4em;text-transform: uppercase;font-weight:900' >" . ($naryads[0]->start ? ErpLocation::findOne($naryads[0]->start)->name : "Склад") . "</span>";

                    } catch (\Exception $exception) {
                        $transaction->rollBack();
                        $response['status'] = 'danger';
                        $response['message'] = 'Произошла ошибка в работе приложения';
                        return $response;
                    }
            } else {
                if ($model->user_id) {
                    $errors = [];
                    foreach ($model->getErrors() as $key => $value) {
                        $errors[] = $value[0];
                    }
                    $response['message'] = implode(', ', $errors);
                }
            }
        }

        $model->saveProperties();

        return $response;
    }

    /**
    * Работа сканера на участке выдачи наряда
    * @param string $barcode штрихкод
    * @return json
    */
    public function actionSearchRegister($barcode)
    {
        //Данное действие только для Аякс запроса
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = new ErpTerminal();
        $barcode = $model->toLatin($barcode);

        $response['status']='danger';
        $response['message'] = 'не найдено';  

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $user=ErpUser::find()->where(['barcode'=>$barcode])->one();

        if ($user){//поиск сотрудника
            //если треминал еще не установлен пользователь
            if ($model->user_id !== $user->id)
            {
                //устанавливаем терминалу пользователя и локацию
                $model->user_id=$user->id;

                //если есть запись о приходе тогда предалгаем уйти с работы
                $register = ErpLocationWorkplaceRegister::find()
                    ->where(['user_id' => $model->user_id])
                    ->andWhere(['!=','status', ErpLocationWorkplace::TYPE_ACTIVE])
                    ->andWhere(['!=','status', ErpLocationWorkplace::TYPE_INACTIVE])
                    ->orderBy('register_at DESC')
                    ->one();
                    //если существует такая запись в журнале, и если ее тип приход, тогда
                if ($register && $register->action_type == ErpLocationWorkplaceRegister::TYPE_IN)
                {
                    $response['status']='danger';
                    $response['message'] = $user->name.'. Вы действительно собираетесь покинуть рабочее место ???'; 
                }else{
                    $workplacePlan = ErpLocationWorkplacePlan::find()
                        ->Where(['and',
                        ['user_id' => $model->user_id],
                        ['<','time_from',time() + 60*60],
                        ['>','time_to',time() - 60*60],
                        ])
                        ->one();

                    if (!$workplacePlan)
                    {
                        $workplacePlan = ErpLocationWorkplacePlan::find()
                        ->Where(['and',
                        ['user_id' => $model->user_id],
                        ])->orderBy('time_to desc')
                        ->one();
                    }

                    if ($workplacePlan){ //если ему запланировано сегодня место, тогда он заерегестрируеться на участке
                        $workplace = $workplacePlan->workplace;

                        $response['status']='success';
                        $response['message'] = 'Здравствуйте, '.$user->name.'. Вам предназначено '.$workplace->location_number.' на учатске '.$workplace->location->name;
                    }else{
                        $response['status']='success';
                        $response['message'] = 'Здравствуйте, '.$user->name.'. На Вас не запланировано работы на сегодня!';                    
                    } 
                }
            }else{

                //если есть запись о приходе тогда предалгаем уйти с работы
                $register = ErpLocationWorkplaceRegister::find()
                    ->where(['user_id' => $model->user_id])
                    ->andWhere(['!=','status', ErpLocationWorkplace::TYPE_ACTIVE])
                    ->andWhere(['!=','status', ErpLocationWorkplace::TYPE_INACTIVE])
                    ->orderBy('register_at DESC')
                    ->one();
                    //если существует такая запись в журнале, и если ее тип приход, тогда
                if ($register && $register->action_type == ErpLocationWorkplaceRegister::TYPE_IN)
                {
                    $workplace = ErpLocationWorkplace::find()->where(['user_id' => $model->user_id])->one();
                    if ($workplace) {
                        $workplace->user_id = null;
                        $workplace->activity = 0;
                        $workplace->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $workplace->id;
                        $register->user_id = $model->user_id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_OUT;
                        $register->register_at = time();
                        $register->save();
                    }

                    $response['status']='danger';
                    $response['message'] = $user->name.'. Вы покинули рабочее место!'; 
                }else{
                    $workplacePlan = ErpLocationWorkplacePlan::find()
                        ->Where(['and',
                        ['user_id' => $model->user_id],
                        ['<','time_from',time() + 60*60],
                        ['>','time_to',time() - 60*60],
                        ])
                        ->one();

                    if (!$workplacePlan)
                    {
                        $workplacePlan = ErpLocationWorkplacePlan::find()
                        ->Where(['and',
                        ['user_id' => $model->user_id],
                        ])->orderBy('time_to desc')
                        ->one();
                    }

                    if ($workplacePlan){ //если ему запланировано сегодня место, тогда он заерегестрируеться на участке
                        $workplace = $workplacePlan->workplace;
                        $workplace->user_id = $model->user_id;
                        $workplace->activity = 1;
                        $workplace->last_activity = time();
                        $workplace->save();

                        $user = $workplace->user;
                        $user->location_id = $workplace->location_id;
                        $user->save();

                        $register = new ErpLocationWorkplaceRegister();
                        $register->workplace_id = $workplace->id;
                        $register->user_id = $model->user_id;
                        $register->action_type = ErpLocationWorkplaceRegister::TYPE_IN;
                        $register->register_at = time();
                        $register->save();

                        $response['status']='success';
                        $response['message'] = 'Здравствуйте, вы поступили на рабочее место';
                    }else{
                        $response['status']='success';
                        $response['message'] = 'Здравствуйте, '.$user->name.'. На Вас не запланировано работы на сегодня!';                    
                    } 
                }
                //онулируем пользователя
                $model->user_id = null;
            }
            
            $model->saveProperties();

        }

        return $response;
    }

    public function finishWork($user_id)
    {
        $workplace_register = ErpLocationWorkplaceRegister::find()->where(['and',
                ['user_id' => $user_id],
                ['or',
                    ['action_type'=>ErpLocationWorkplaceRegister::TYPE_ACTIVE],
                    ['action_type'=>ErpLocationWorkplaceRegister::TYPE_INACTIVE]
                ]])->orderBy('register_at DESC')->one();

        if ($workplace_register->action_type == ErpLocationWorkplaceRegister::TYPE_ACTIVE) {
            $this->saveRegister($user_id, $workplace_register->workplace_id, ErpLocationWorkplaceRegister::TYPE_INACTIVE );
        }

        $workplace_register = ErpLocationWorkplaceRegister::find()->where(['and',
            ['user_id' => $user_id],
            ['or',
                ['action_type'=>ErpLocationWorkplaceRegister::TYPE_MOVE_FROM],
                ['action_type'=>ErpLocationWorkplaceRegister::TYPE_MOVE_TO, ]
            ]])->orderBy('register_at DESC')->one();

        if ($workplace_register->action_type == ErpLocationWorkplaceRegister::TYPE_MOVE_TO) {
            $workplace = ErpLocationWorkplace::findOne($workplace_register->workplace_id);
            $workplace->user_id = null;
            $workplace->save();
            $this->saveRegister($user_id, $workplace_register->workplace_id, ErpLocationWorkplaceRegister::TYPE_MOVE_FROM );
        }

        $workplace_register = ErpLocationWorkplaceRegister::find()->where(['and',
            ['user_id' => $user_id],
            ['or',
                ['action_type'=>ErpLocationWorkplaceRegister::TYPE_OUT],
                ['action_type'=>ErpLocationWorkplaceRegister::TYPE_IN]
            ]])->orderBy('id DESC')->one();

//
        if ($workplace_register->action_type == ErpLocationWorkplaceRegister::TYPE_IN) {

           if($this->saveRegister($user_id,null, ErpLocationWorkplaceRegister::TYPE_OUT )){
               return true;

           }


        }
    }

    public function saveRegister($user_id, $workplace_id=null, $action_type)
    {
        $register = new ErpLocationWorkplaceRegister();
        $register->user_id = $user_id;
        $register->workplace_id = $workplace_id;
        $register->action_type = $action_type;
        $register->register_at = time();
        if($register->save())
            return true;
    }

    /**
     * @param $cloth Функция для установки ткани для выдачи (донабора)
     */
    public function actionSetVidachaCloth($cloth)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = [];

        $model = new ErpTerminal();
        if ($model->user_id) {
            $model->cloth = $cloth;
            $model->window = null;
            $model->saveProperties();

            $response['status']='success';
            $response['message'] = 'Вы выбрали ткань № '. $cloth . '!';
        }else{
            $response['status']='error';
            $response['message'] = 'Еще раз пропикайте бейджик!';
        }

        return $response;
    }

    /**
     * @param $cloth Функция для установки ткани для выдачи (донабора)
     */
    public function actionSetVidachaWindow($window)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = [];

        $model = new ErpTerminal();
        if ($model->user_id) {
            $model->window = $window;
            $model->saveProperties();

            $response['status']='success';
            $response['message'] = 'Вы выбрали оконный проем "'. $model->windowValues[$window] . '"!';
            $workOrders = $model->getNaryadsNaVidachu();
            if ($workOrders) {
                foreach ($workOrders as $workOrder) {
                    $literalInfo = SProductionLiteral::workOrderInfo($workOrder->id, $workOrder->start);
                    $number = $workOrder->logist_id;
                    $naryadName = str_replace((mb_substr($number, 4, mb_strlen($number))),
                        ('<b style="font-size: 2em;color:black">' . mb_substr($number, 4, mb_strlen($number)) . '</b>'), $number);
                    $response['message'] .= "<br> <span style='font-size: 4em;'>{$naryadName}&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<span style='color:red;font-size: 2em;font-weight: bold'>$literalInfo</span></span>";
                }
            } else {
                $response['message'] .= "<br> <span style='color:red;'>По данному фильтру - нет нарядов!!!</span>";
            }
        }else{
            $response['status']='error';
            $response['message'] = 'Еще раз пропикайте бейджик!';
        }

        return $response;
    }

    private function _updateSession($name, $pageFlag, $defaultValue = false)
    {
        $session      = Yii::$app->session;
        $remember     = Yii::$app->request->get($pageFlag) ?: Yii::$app->request->post($pageFlag);
        $sessionValue = !$remember ? $session->get($name, $defaultValue) : (Yii::$app->request->get($name, false) ?: Yii::$app->request->post($name, false));
        $session->set($name, $sessionValue);

        return $sessionValue;
    }

}
