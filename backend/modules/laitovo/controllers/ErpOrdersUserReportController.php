<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 26.09.17
 * Time: 12:40
 */

namespace backend\modules\laitovo\controllers;

use yii\web\Controller;
use backend\modules\laitovo\models\ErpOrdersUserReport;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ErpOrdersUserReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return in_array(Yii::$app->user->getId(), [37,21,2,11,27,30,56,36]);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new ErpOrdersUserReport();
        if(Yii::$app->request->get()){
            $model->load(Yii::$app->request->get());
        }
        $dataProvider = $model->getArrayDataProvider();

        return $this->render('index',['model'=>$model, 'dataProvider' => $dataProvider]);
    }
}