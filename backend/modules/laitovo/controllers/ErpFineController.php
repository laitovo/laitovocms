<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 19.09.17
 * Time: 11:39
 */

namespace backend\modules\laitovo\controllers;

use common\models\laitovo\ErpFine;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use Yii;

class ErpFineController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
           'query'=>ErpFine::find()->orderBy('id DESC')
        ]);

        return $this->render('index',['dataProvider'=>$dataProvider]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ErpFine();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }

        return $this->render('create',['model'=>$model]);
    }

    /**
     * Updates an existing ErpFine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = ErpFine::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing ErpFine model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       ErpFine::findOne($id)->delete();
       return $this->redirect(['index']);
    }
}