<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 09.10.17
 * Time: 11:12
 */

namespace backend\modules\laitovo\controllers;

use backend\modules\laitovo\models\PatternRegister;
use common\models\laitovo\Cars;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

class PatternRegisterController extends Controller
{
    public function actionIndex($car=null)
    {
        $model = new PatternRegister();
        if ($car) {
            $model->car = $car;
        }
        if (Yii::$app->request->get()) {
            $model->load(Yii::$app->request->get());
        }
        $query = $model->search();

        $dataProvider = new ActiveDataProvider([
           'query' => $query
        ]);

        $statistics = null;
        if (isset($_GET['sort']) && preg_match('/FW|FV|FD|RD|RV|BW/',$_GET['sort']) === 1 ) {
            $statistics = $dataProvider->totalCount;
        }

        $sort = $dataProvider->getSort();
        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'FW' => [
                'asc' => ['article' => SORT_ASC],
                'desc' => ['article' => SORT_DESC],
            ],
            'FV' => [
                'asc' => ['article' => SORT_ASC],
                'desc' => ['article' => SORT_DESC],
            ],
            'FD' => [
                'asc' => ['article' => SORT_ASC],
                'desc' => ['article' => SORT_DESC],
            ],
            'RD' => [
                'asc' => ['article' => SORT_ASC],
                'desc' => ['article' => SORT_DESC],
            ],
            'RV' => [
                'asc' => ['article' => SORT_ASC],
                'desc' => ['article' => SORT_DESC],
            ],
            'BW' => [
                'asc' => ['article' => SORT_ASC],
                'desc' => ['article' => SORT_DESC],
            ],
        ]);

        $dataProvider->setSort($sort);

        return $this->render('index', ['dataProvider' => $dataProvider, 'model' =>$model,'statistics' => $statistics]);
    }

    public function actionCreate($car_id, $type, $window)
    {
        $car = Cars::findOne($car_id);
        $model = new PatternRegister();
        $model->car_id = $car_id;
        $this->loadModel($model, $window);
        switch ($type) {
            case 'bk' :
                $model->product_type = PatternRegister::BK;
                break;
            case 'sd' :
                $model->product_type = PatternRegister::SD;
                break;
        }
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'car' => $car->article]);
        }
        return $this->render('create', ['model'=>$model, 'car' => $car]);
    }

    public function loadModel($model, $window) {
        switch ($window) {
            case 'FD' :
                $model->window_type = PatternRegister::FD;
                break;
            case 'FV' :
                $model->window_type = PatternRegister::FV;
                break;
            case 'RD' :
                $model->window_type = PatternRegister::RD;
                break;
            case 'RV' :
                $model->window_type = PatternRegister::RV;
                break;
            case 'FW' :
                $model->window_type = PatternRegister::FW;
                break;
            case 'BW' :
                $model->window_type = PatternRegister::BW;
                break;
        }
    }
}