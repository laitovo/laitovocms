<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use backend\modules\laitovo\models\ErpLocation;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;
use common\models\laitovo\Config;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpStorageInfo;
use yii\helpers\ArrayHelper;

/**
 * ErpLocationController implements the CRUD actions for ErpLocation model.
 */
class ErpLocationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpLocation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ErpLocation::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpLocation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = $model->searchViewNaryads(Yii::$app->request->queryParams);

        $startnaryadsProvider = $model->searchViewStartNaryads(Yii::$app->request->queryParams);

        $usersProvider = new ActiveDataProvider([
            'query' => $model->getUsers(),
        ]);

        $workplaceProvider =  new ActiveDataProvider([
            'query' => $model->getWorkplaces(),
        ]);

        $usersRateProvider = new ActiveDataProvider([
            'query' => $model->getUsersRate(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'startnaryadsProvider' => $startnaryadsProvider,
            'naryadsProvider' => $naryadsProvider,
            'usersProvider' => $usersProvider,
            'usersRateProvider' => $usersRateProvider,
            'workplaceProvider' => $workplaceProvider,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewOnLocation($id)
    {
        //$model = $this->findModel($id);
        $model = $this->findModel($id);;
        $naryadsProvider = $model->searchViewOnLocation(Yii::$app->request->queryParams);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=> $model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewThrowLocationByDate($location,$dFrom,$dTo,$af = false)
    {
        $model = $this->findModel($location);

        $naryadsProvider = $af ? $model->getNaryadsCreatedByUserAF($dFrom,$dTo) : $model->getNaryadsCreatedByUser($dFrom,$dTo);

        return $this->render('throw_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewThrowLocationByDateUserList($location,$user,$dFrom,$dTo,$af = false)
    {
        $model = $this->findModel($location);

        $naryadsProvider = $af ? $model->getArrayNaryadsCreatedByUserListAF($dFrom,$dTo,$user) : $model->getArrayNaryadsCreatedByUserList($dFrom,$dTo,$user);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model' => $model,
        ]);
    }



    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewNotGiven($id)
    {
        $literals = [];

        $model = $this->findModel($id);
        $naryadsProvider = $model->searchViewNotGiven(Yii::$app->request->queryParams);

        $literalNaryads = $model->getNaryadsToStartWholeWork()->all();

        foreach ($literalNaryads as $naryad) {
            if (isset($literals[$naryad->literal]))
            {
                $literals[$naryad->literal]['count']++;
            }
            else
            {
                $literals[$naryad->literal]['literal'] = $naryad->getValueLiteral();
                $literals[$naryad->literal]['count'] = 1;
            }
        }
        ksort($literals);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'literals'=>$literals,
            'model'=>$model,
        ]);
    }



    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewOnStorage()
    {
        $literals = [];

        $naryadsProvider = new ActiveDataProvider([
            'query' => ErpStorageInfo::getUpnOnDispathcerQuery(),
        ]);

        $literalNaryads = ErpStorageInfo::getUpnOnDispathcerQuery()->all();

        foreach ($literalNaryads as $naryad) {
            if (isset($literals[$naryad->literal]))
            {
                $literals[$naryad->literal]['count']++;
            }
            else
            {
                $literals[$naryad->literal]['literal'] = $naryad->getValueLiteral();
                $literals[$naryad->literal]['count'] = 1;
            }
        }
        ksort($literals);

        return $this->render('on_storage_view', [
            'naryadsProvider' => $naryadsProvider,
            'literals'=>$literals,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewInDelay($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartInDelay(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewInPause($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsInPause(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartInPause($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartInPause(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewExpressPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsWithExpressPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewBigOrderPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsWithBigOrderPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewCompletionPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsWithCompletionPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewCompletionFlag($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsWithCompletionFlag(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewStandardPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsWithStandardPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewAutoFilter($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getAutoFilterWorkOrders(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewLaitbag($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getLBWorkOrders(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartExpressPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartWithExpressPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewAutoFilterToStart($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getAutoFilterWorkOrdersToStart(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewLaitbagToStart($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getLBWorkOrdersToStart(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewMainPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsWithMainPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartMainPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartWithMainPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartBigOrderPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartWithBigOrderPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartCompletionPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartWithCompletionPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartCompletionFlag($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartWithCompletionFlag(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartStandardPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartWithStandardPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewRework($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsRework(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartRework($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartRework(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewLowPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsWithLowPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewAutoSpeed($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsWithAutoSpeed(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewMoveAct($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsMoveAct(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewOneDay($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ArrayDataProvider([
            'models' => $model->getNaryadsMoreOneDayInWork(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewFromSump($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsFromSump(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewForStorage($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsForStorage(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartOneDay($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ArrayDataProvider([
            'models' => $model->getNaryadsMoreOneDayToStart(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartFromSump($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartFromSump(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartForStorage($id)
    {
        $model = $this->findModel($id);

        $this->pauseAction($model->getNaryadsToStartForStorage()->all());

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartForStorage(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartLowPriority($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartWithLowPriority(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartAutoSpeed($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartWithAutoSpeed(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewToStartMoveAct($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartMoveAct(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewFromCloud($id)
    {
        $model = $this->findModel($id);

        $naryadsProvider = new ActiveDataProvider([
            'query' => $model->getNaryadsToStartFromCloud(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model'=>$model,
        ]);
    }


    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewOverdue($id)
    {
        $model = $this->findModel($id);
        $naryadsProvider = $model->searchViewOverdue(Yii::$app->request->queryParams);

        return $this->render('on_location_view', [
            'naryadsProvider' => $naryadsProvider,
            'model' => $model,
        ]);
    }


    public function actionPrint($id)
    {
        $model = $this->findModel($id);

        $content = $this->renderPartial('print',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}',
            'options' => ['title' => $model->name],
        ]);

        return $pdf->render();
    }

    public function actionPrintVidacha()
    {
        $model = new ErpLocation;
        $model->name = 'Выдача нарядов';
        $model->barcode = md5('VIDACHNARYADOV');

        $content = $this->renderPartial('print',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}',
            'options' => ['title' => $model->name],
        ]);

        return $pdf->render();
    }

    /**
     * Creates a new ErpLocation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpLocation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpLocation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ErpLocation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpLocation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpLocation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpLocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function pauseAction($naryads)
    {
        $pause = Yii::$app->request->get('pause');
        foreach ($naryads as $naryad) {
            if ($naryad->status != ErpNaryad::STATUS_IN_PAUSE && $pause == 'Да') {
                $naryad->status = ErpNaryad::STATUS_IN_PAUSE;
                $naryad->save();
            }

            if ($naryad->status == ErpNaryad::STATUS_IN_PAUSE && $pause == 'Нет') {
                $naryad->status = ErpNaryad::STATUS_IN_WORK;
                $naryad->save();
            }
        }
    }
}
