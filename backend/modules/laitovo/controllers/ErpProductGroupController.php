<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use backend\modules\laitovo\models\ErpProductGroup;
use backend\modules\laitovo\models\ErpProductType;
use backend\modules\laitovo\models\ErpProductGroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ErpProductGroupController implements the CRUD actions for ErpProductGroup model.
 */
class ErpProductGroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpProductGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ErpProductGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpProductGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $query = $model->getProducts();
        $query2 = $model->getOddProducts();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider2 = new ActiveDataProvider([
            'query' => $query2,
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'dataProvider2' => $dataProvider2,
        ]);
    }

    /**
     * Creates a new ErpProductGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpProductGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpProductGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ErpProductGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Link an existing ErpProductGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionLink($id,$product_id)
    {
        //Получаем данные через гет запрос
        //Находим модельку группы
        $model = $this->findModel($id);
        $product = ErpProductType::findOne($product_id);
        //Добавляем связь
        $model->link('products',$product);
        return $this->redirect(['view', 'id' => $model->id,'#' => 'tables']);
    }

    /**
     * Link an existing ErpProductGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionLinkAll($id)
    {
        //Получаем данные через гет запрос
        //Находим модельку группы
        $model = $this->findModel($id);
        $products = ErpProductType::find()->all();
        foreach ($products as $product) {
            if (!$product->isBelongToAGroup($model->id))
            $model->link('products',$product);
        }
        //Добавляем связь
        return $this->redirect(['view', 'id' => $model->id,'#' => 'tables']);
    }

    /**
     * Link an existing ErpProductGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUnLink($id,$product_id)
    {
        //Получаем данные через гет запрос
        //Находим модельку группы
        $model = $this->findModel($id);
        $product = ErpProductType::findOne($product_id);
        //Добавляем связь
        $model->unlink('products',$product,true);
        return $this->redirect(['view', 'id' => $model->id,'#' => 'tables']);
    }

    /**
     * Link an existing ErpProductGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUnLinkAll($id)
    {
        //Получаем данные через гет запрос
        //Находим модельку группы
        $model = $this->findModel($id);
        $products = ErpProductType::find()->all();
        foreach ($products as $product) {
            if ($product->isBelongToAGroup($model->id))
            $model->unlink('products',$product,true);
        }
        //Добавляем связь
        return $this->redirect(['view', 'id' => $model->id,'#' => 'tables']);
    }

    /**
     * Finds the ErpProductGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpProductGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpProductGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
