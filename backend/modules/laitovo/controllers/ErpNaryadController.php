<?php

namespace backend\modules\laitovo\controllers;

use core\logic\DistributionProductionLiterals;
use Yii;
use backend\modules\laitovo\models\ErpNaryad;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;
use backend\modules\laitovo\models\ErpNaryadSearch;
use yii\web\Response;

/**
 * ErpNaryadController implements the CRUD actions for ErpNaryad model.
 */
class ErpNaryadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => false,
                        'actions' => ['delete'],
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpNaryad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $show = $this->_updateSession('erpNaryad_index_show', 'update_erpNaryad_index_show', 'all');
        $searchModel = new ErpNaryadSearch();
        $params = array_merge(Yii::$app->request->queryParams, ['show' => $show]);
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'dataProvider'      => $dataProvider,
            'searchModel'       => $searchModel,
            'show'              => $show,
            'countAll'          => ErpNaryad::countAll(),
            'countOnProduction' => ErpNaryad::countOnProduction(),
            'countOnPause'      => ErpNaryad::countOnPause(),
        ]);
    }

    /**
     * Displays a single ErpNaryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (in_array(Yii::$app->user->getId(), [40,41,120]))
            throw new ForbiddenHttpException("Штраф 5000 рублей");

        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ErpNaryad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpNaryad();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpNaryad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $beforeLocation = $model->location_id;
        $loaded = $model->load(Yii::$app->request->post());

        if (Yii::$app->user->getId() != 21 && $loaded && $model->location_id != $beforeLocation) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'НЕ удалось выполнить сохранение наряда!'));
            return $this->render('update', [
                'model' => $model,
            ]);
        } elseif ($loaded && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCancel($id)
    {
        $model = $this->findModel($id);
        if ($model->cancel()){
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    public function actionPause($id)
    {
        $model = $this->findModel($id);
        if ($model->pause()){
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    public function actionUnpause($id)
    {
        $model = $this->findModel($id);
        if ($model->unpause()){
            if ($model->location_id && $model->location_id != Yii::$app->params['erp_dispatcher'] && $model->location_id !== Yii::$app->params['erp_otk']) {
                if ($model->endPlace($model->user_id, true)) {
                    //Попытка распределить наряд по литере.
                    $productionLiteralResponse = (new DistributionProductionLiterals())->distribute($model->barcode);
                    Yii::$app->session->setFlash($productionLiteralResponse['status'], $productionLiteralResponse['message']);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    /**
     * Deletes an existing ErpNaryad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPauseFromGrid($id)
    {
        $model = $this->findModel($id);
        if ($model->pause())
            return $this->redirect(['erp/terminal-sdacha']);
        return $this->redirect(['erp/terminal-sdacha']);
    }

    /**
     * Finds the ErpNaryad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpNaryad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpNaryad::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrint($id)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $model = $this->findModel($id);

        if ($model->article == 'OT-1041-40-1') {
            return $this->renderPartial('print_casper',[
                'model' => $model,
            ]);
        } elseif (preg_match('/OT-2079/', $model->article) === 1) {
            return $this->renderPartial('print_af_cover',[
                'model' => $model,
            ]);
        } else {
            return $this->renderPartial('print', [
                'model' => $model,
            ]);
        }
    }

    public function actionPrintWithManual($id)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $model = $this->findModel($id);

        $response =  $this->renderPartial('print',[
            'model' => $model,
        ]);

        $response.="<style>.page-break-$id {page-break-before:always;}</style>";

        if ($model->isExport() && $model->windowBW())
        {
            $response.= $this->renderPartial('print/print-manual-eu-bw',[
                'model' => $model,
              ]);
        }elseif ($model->isExport() && $model->getBrand()!='Chiko')
        {
            $response.= $this->renderPartial('print/print-manual-eu',[
                'model' => $model,
              ]);
        }elseif ($model->isExport() && $model->getChikoInMagnet())
        {
            $response.= $this->renderPartial('print/print-manual-eu-chiko-in-magnet',[
                  'model' => $model,
                ]);
        }elseif ($model->isExport() && $model->laitovoInMagnet())
        {
            $response.= $this->renderPartial('print/print-manual-eu-laitovo-in-magnet',[
                  'model' => $model,
                ]);
        }elseif($model->isExport() && $model->getBrand()=='Chiko')
        {
            $response.= $this->renderPartial('print/print-manual-eu-chiko',[
                'model' => $model,
            ]);
        }elseif (!$model->isExport() && $model->windowBW())
        {
            $response.= $this->renderPartial('print/print-manual-bw',[
            'model' => $model,
            ]);
        }elseif (!$model->isExport() && !$model->getChikoInMagnet() && !$model->chikoMagnet() && !$model->vizorMagnet() && !$model->vizorInMagnet() && $model->getBrand()=='Chiko')
        {
            $response.= $this->renderPartial('print/print-manual-chiko',[
            'model' => $model,
            ]);
        }elseif ($model->getChikoInMagnet() && !$model->isExport())
        {
           $response.= $this->renderPartial('print/print-manual-chiko-in-magnet',[
           'model' => $model,
         ]);
        }elseif ($model->chikoMagnet() && !$model->isExport())
        {
           $response.= $this->renderPartial('print/print-manual-chiko-magnet',[
           'model' => $model,
         ]);
        }elseif ($model->laitovoMagnet() && !$model->isExport())
        {
           $response.= $this->renderPartial('print/print-manual-laitovo-magnet',[
           'model' => $model,
         ]);
        }elseif ($model->laitovoInMagnet() && !$model->isExport())
        {
            $response.= $this->renderPartial('print/print-manual-laitovo-in-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->getChikoInMagnet())
        {
            $response.= $this->renderPartial('print/print-manual-chiko-in-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->laitovoMagnet())
        {
            $response.= $this->renderPartial('print/print-manual-laitovo-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->chikoMagnet())
        {
            $response.= $this->renderPartial('print/print-manual-chiko-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->laitovoInMagnet())
        {
            $response.= $this->renderPartial('print/print-manual-laitovo-in-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->mosquitoNet())
        {
            $response.= $this->renderPartial('print/print-manual-mosquito-net',[
              'model' => $model,
            ]);
        }elseif ($model->vizorMagnet()&& $model->getBrand()=='Chiko')
        {
            $response.= $this->renderPartial('print/print-manual-chiko-visor-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->vizorInMagnet()&& $model->getBrand()=='Chiko')
        {
            $response.= $this->renderPartial('print/print-manual-chiko-vizor-in-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->vizorMagnet()&& $model->getBrand()=='Laitovo')
        {
            $response.= $this->renderPartial('print/print-manual-laitovo-vizor-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->vizorInMagnet()&& $model->getBrand()=='Laitovo')
        {
            $response.= $this->renderPartial('print/print-manual-laitovo-vizor-in-magnet',[
              'model' => $model,
            ]);
        }else{
            $response.= $this->renderPartial('print/print-manual-1',[
              'model' => $model,
            ]);
        }

        return $response;
    }


    public function actionPrintLabel($id)
    {
        $model = $this->findModel($id);

//        if ($model->isHindu())
//            return $this->renderPartial('label_hindu',[
//                'model' => $model,
//            ]);
//
//        if ($model->isExport())
//            return $this->renderPartial('label_export_new',[
//                'model' => $model,
//            ]);

        return $this->renderPartial('label_export_new',[
            'model' => $model,
        ]);
    }

    public function actionPrintLabelTestBarcode($id)
    {
        $model = $this->findModel($id);

        return $this->renderPartial('label_export_new_test',[
            'model' => $model,
        ]);
    }

    public function actionPrintOrgLabel($type = 0)
    {
        return $this->renderPartial('orgnaizer_label', ['type' => $type]);
    }

    public function actionPrintOrgDownLabel($type = 0)
    {
        return $this->renderPartial('orgnaizer_down_label', ['type' => $type]);
    }

    public function actionPrintKzaLabel($type = 0)
    {
        return $this->renderPartial('org_kza_label', ['type' => $type]);
    }

    public function actionPrintOrgFilterLabel($type = 0)
    {
        return $this->renderPartial('org_filter_label', ['type' => $type]);
    }

    public function actionPrintBlanketLabel($type = 0)
    {
        return $this->renderPartial('blanket_label', ['type' => $type]);
    }

    public function actionCoverLabel()
    {
        return $this->renderPartial('cover_label');
    }

    public function actionPrintLabelTest($id)
    {
        $model = $this->findModel($id);

        if ($model->isHindu())
            return $this->renderPartial('label_hindu',[
                'model' => $model,
            ]);

        if ($model->isExport())
            return $this->renderPartial('label_export_new',[
                'model' => $model,
            ]);

        return $this->renderPartial('label_export_new',[
            'model' => $model,
        ]);

    }

    public function actionPrintManual($id)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $model = $this->findModel($id);
//        $search = Yii::$container->get('core\logic\FindCustomManualByArticle');
//        $search->search($model->article);
//        if ($search->exists()) {
//            return $this->redirect(['/laitovo/instruction/printing/print-any', 'carId' => $search->getCarId(), 'instructionTypeId' => $search->getInstructionTypeId()]);
//        }
            if ($model->isExport() && $model->windowBW())
        {
            return $this->renderPartial('print/print-manual-eu-bw',[
                'model' => $model,
              ]);
        }elseif ($model->isExport() && $model->getChikoInMagnet())
        {
            return $this->renderPartial('print/print-manual-eu-chiko-in-magnet',[
                  'model' => $model,
                ]);
        }elseif ($model->isExport() && $model->laitovoInMagnet())
        {
            return $this->renderPartial('print/print-manual-eu-laitovo-in-magnet',[
                  'model' => $model,
                ]);
        }elseif ($model->isExport() && $model->getBrand()!='Chiko')
            {
                return $this->renderPartial('print/print-manual-eu',[
                    'model' => $model,
                ]);
        }elseif($model->isExport() && $model->getBrand()=='Chiko')
        {
            return $this->renderPartial('print/print-manual-eu-chiko',[
                'model' => $model,
            ]);
        }elseif (!$model->isExport() && $model->windowBW())
        {
            return $this->renderPartial('print/print-manual-bw',[
            'model' => $model,
            ]);
        }elseif (!$model->isExport() && !$model->getChikoInMagnet() && !$model->chikoMagnet() && !$model->vizorMagnet() && !$model->vizorInMagnet() && $model->getBrand()=='Chiko')
        {
            return $this->renderPartial('print/print-manual-chiko',[
            'model' => $model,
            ]);
        }elseif ($model->getChikoInMagnet() && !$model->isExport())
        {
           return $this->renderPartial('print/print-manual-chiko-in-magnet',[
           'model' => $model,
         ]);
        }elseif ($model->chikoMagnet() && !$model->isExport())
        {
           return $this->renderPartial('print/print-manual-chiko-magnet',[
           'model' => $model,
         ]);
        }elseif ($model->laitovoMagnet() && !$model->isExport())
        {
           return $this->renderPartial('print/print-manual-laitovo-magnet',[
           'model' => $model,
         ]);
        }elseif ($model->laitovoInMagnet() && !$model->isExport())
        {
            return $this->renderPartial('print/print-manual-laitovo-in-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->getChikoInMagnet())
        {
            return $this->renderPartial('print/print-manual-chiko-in-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->laitovoMagnet())
        {
            return $this->renderPartial('print/print-manual-laitovo-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->chikoMagnet())
        {
            return $this->renderPartial('print/print-manual-chiko-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->laitovoInMagnet())
        {
            return $this->renderPartial('print/print-manual-laitovo-in-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->mosquitoNet())
        {
            return $this->renderPartial('print/print-manual-mosquito-net',[
              'model' => $model,
            ]);
        }elseif ($model->vizorMagnet()&& $model->getBrand()=='Chiko')
        {
            return $this->renderPartial('print/print-manual-chiko-visor-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->vizorInMagnet()&& $model->getBrand()=='Chiko')
        {
            return $this->renderPartial('print/print-manual-chiko-vizor-in-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->vizorMagnet()&& $model->getBrand()=='Laitovo')
        {
            return $this->renderPartial('print/print-manual-laitovo-vizor-magnet',[
              'model' => $model,
            ]);
        }elseif ($model->vizorInMagnet()&& $model->getBrand()=='Laitovo')
        {
            return $this->renderPartial('print/print-manual-laitovo-vizor-in-magnet',[
              'model' => $model,
            ]);
        }
            return $this->renderPartial('print/print-manual-1',[
              'model' => $model,
            ]);
    }

    public function actionPrintManualNew($id)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $model = $this->findModel($id);
        $lang = $model->isExport() ? 'eu' : 'ru';
        //Печать для чеха
        if (($order = $model->order) && preg_match('/Auto Hotarek/', $order->json('username')) === 1)
            $lang = 'cs';
        //Печать для чеха
        if (($order = $model->order) && preg_match('/Автофемили/', $order->json('username')) === 1)
            $lang = 'cs';
        //AG Auto
        if (($order = $model->order) && preg_match('/AG Auto/', $order->json('username')) === 1)
            $lang = 'eu';

        return $this->redirect(['/laitovo/universal-instruction/default/print','article' => $model->article,'lang' => $lang]);
//        return $this->renderPartial('print/universal/print-manual-universal',[
//            'model' => $model,
//        ]);
    }

    /**
     * Постановка наряда на паузу
     * (ajax-функция)
     *
     * @param int $id
     * @return array
     */
    public function actionAjaxPause($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        return [
            'status' => $model->pause(),
        ];
    }

    /**
     * Снятие наряда с паузы
     * (ajax-функция)
     *
     * @param int $id
     * @return array
     */
    public function actionAjaxUnpause($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        return [
            'status' => $model->unpause(),
        ];
    }

    /**
     * Изменение срочности наряда
     * (ajax-функция)
     *
     * @param int $id
     * @param int $sort
     * @return array
     */
    public function actionAjaxUpdatePriority($id, $sort)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $result = $model->updatePriority($sort);

        return [
            'status'  => $result['status'],
            'message' => $result['message'],
        ];
    }

    private function _updateSession($name, $pageFlag, $defaultValue = false)
    {
        $session      = Yii::$app->session;
        $remember     = Yii::$app->request->post($pageFlag);
        $sessionValue = $remember ? Yii::$app->request->post($name, $defaultValue) : $session->get($name, $defaultValue);
        $session->set($name, $sessionValue);

        return $sessionValue;
    }

    public function actionCountVelkro()
    {
        $naryadsBefore = ErpNaryad::find()->where(['or',
            ['and',
                ['location_id' => [2,3]],
                ['status' => ErpNaryad::STATUS_IN_WORK],
            ],
            ['and',
                ['start' => [2,3,4]],
                ['or',
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['status' => null],
                ],
            ],
        ])->all();

        $count = 0;
        foreach ($naryadsBefore as $item) {
            if ($item->velkroFlag())
                $count++;
        }

        echo '<pre>';
        echo $count;
        echo '</pre>';
    }

    public function actionVelkroList()
    {
        $naryadsBefore = ErpNaryad::find()->where(['and',
                ['start' => 4],
                ['or',
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['status' => null],
                ],
        ])->all();

        foreach ($naryadsBefore as $item) {
            if ($item->velkroFlag())
                echo "<a href='https://biz-zone.biz/laitovo/erp-naryad/view?id={$item->id}'>$item->logist_id</a><br>";
        }
    }
}
