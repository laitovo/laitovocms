<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use backend\modules\laitovo\models\ErpJobScheme;
use backend\modules\laitovo\models\ErpJobType;
use common\models\laitovo\ProductionScheme;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;

/**
 * ErpJobSchemeController implements the CRUD actions for ErpJobScheme model.
 */
class ErpJobSchemeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpScheme models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => \backend\modules\laitovo\models\ErpScheme::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays ErpJobScheme models.
     * @param integer $scheme_id
     * @return mixed
     */
    public function actionView_job_schemes($scheme_id)
    {
        
        $dataProvider = new ActiveDataProvider([
            'query' => ErpJobScheme::find()->andWhere(['scheme_id' => $scheme_id]),
        ]);

        return $this->render('view_job_schemes', [
            'dataProvider' => $dataProvider,
            'scheme' => \common\models\laitovo\ErpScheme::findOne($scheme_id),
        ]);
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);

        $content = $this->renderPartial('print',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}', 
            'options' => ['title' => $model->name],
        ]);

        return $pdf->render(); 
    }

    /**
     * Creates a new ErpJobScheme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpJobScheme();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view_job_schemes', 'scheme_id' => $model->production_scheme_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionRegisterJobTypeInLocation($production_scheme_id, $location_id)
    {
        $query1 = ErpJobScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id])->andWhere(['not in', 'job_count', 0]);
        $query2 = ErpJobType::find()->where(['location_id'=>$location_id])->andWhere(['not in', 'id', ArrayHelper::map($query1->all(), 'id', 'type_id')])->orderBy('title ASC');
      
        $post = Yii::$app->request->post();
      //  die(print_r($post['job_type'], true));
              
        if(isset($post['job_type']))
        {
            $this->registerGroupe($location_id, $production_scheme_id, $post);
            $query1 = ErpJobScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id])->andWhere(['not in', 'job_count', 0]);
            $query2 = ErpJobType::find()->where(['location_id'=>$location_id])->andWhere(['not in', 'id', ArrayHelper::map($query1->all(), 'id', 'type_id')])->orderBy('title ASC');
           
        }
        if(isset($post['unregister']) && isset($post['type_id']) && isset($post['count']))
        {
            $this->unRegister($location_id, $production_scheme_id, $post['type_id'], $post['count']);
            $query1 = ErpJobScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id])->andWhere(['not in', 'job_count', 0]);
            $query2 = ErpJobType::find()->where(['location_id'=>$location_id])->andWhere(['not in', 'id', ArrayHelper::map($query1->all(), 'id', 'type_id')])->orderBy('title ASC');
        }
        if(isset($post['upregister'])  && isset($post['type_id']) && isset($post['count']))
        {
            $this->upRegister($location_id, $production_scheme_id, $post['type_id'], $post['count']);
            $query1 = ErpJobScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id])->andWhere(['not in', 'job_count', 0]);
            $query2 = ErpJobType::find()->where(['location_id'=>$location_id])->andWhere(['not in', 'id', ArrayHelper::map($query1->all(), 'id', 'type_id')])->orderBy('title ASC');
        }
        
        $jobTypeSchemeProvider = new ActiveDataProvider([
            'query' => $query1,
        ]);
      
        $jobTypeProvider = new ActiveDataProvider([
            'query' => $query2,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        
        
        return $this->render('register-job-type-in-location', [
            'jobTypeProvider' => $jobTypeProvider,
            'jobTypeSchemeProvider' => $jobTypeSchemeProvider,
            'production_scheme_id' => $production_scheme_id,
            'location_id' => $location_id,          
        ]);
    }
    
    
    
     /*
     * Добавляем список видов работ согласно выбранным чекбоксам
     * 
     */
    public function registerGroupe($location_id, $production_scheme_id, $post=[])
    {
         if(isset($post['job_type']))
         {
             //die(print_r($post, true));
             foreach($post['job_type'] as $job_id)
             {
                $jobType = ErpJobScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id, 'type_id'=>$job_id])->one();
                if(!isset($jobType))
                {
                    $jobScheme = new ErpJobScheme();
                    $jobScheme->production_scheme_id = $production_scheme_id;
                    $jobScheme->location_id = $location_id;
                    $jobScheme->job_count = 1;
                    $jobScheme->type_id = $job_id;
                    if(!$jobScheme->save())
                    return false;
                }
             }
             return true;
         }
    }
    
    
    
/*
 *Счетчик работ + 
 * 
 */    
    public function upRegister($location_id, $production_scheme_id, $type_id, $count)
    {
        $jobType = ErpJobScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id, 'type_id'=>$type_id])->one();
         if($jobType)
        {   
            $jobType->job_count = $count+1;
            if($jobType->save())return true;
        }
         return false; 
    }
    
    
  
 /*
 *Счетчик работ -, когда остается единица то при нажатии удаляется
 * 
 */   
    public function unRegister($location_id, $production_scheme_id, $type_id, $count)
    {
        $jobType = ErpJobScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id, 'type_id'=>$type_id])->one();
        if($jobType)
        {   
            if($count-1==0)
            {
              if($jobType->delete())return true;  
            }
            $jobType->job_count = $count-1;    
            if($jobType->save())return true;
        }
        return false;
    }

    /**
     * Updates an existing ErpJobScheme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'scheme' => $model->scheme,
            ]);
        }
    }

    /**
     * Deletes an existing ErpJobScheme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['erp-job-types/view', 'id' => $model->type_id]);
    }

    /**
     * Finds the ErpJobScheme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpJobScheme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpJobScheme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * 
     * Редактирование в модальном окне
     */
    public function actionModalUpdate($id)
    {
        $model = ErpJobType::findOne($id);

        if ($model->load(Yii::$app->request->post()))
        {  
           $model->save();
        }
        return $this->renderPartial('modal-update', [
            'model' => $model,
        ]);
    }
}
