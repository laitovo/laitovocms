<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Cars;
use common\models\laitovo\CarsAct;
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsCheckForm;
use backend\modules\laitovo\models\CarsCheckUpdateForm;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;

/**
 * CarsactController implements the CRUD actions for CarsAct model.
 */
class CarsactController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['test'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['test'],
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CarsAct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Cars::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
                // ['id'=>Yii::$app->request->get('search')],
                ['article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => new CarsForm,
        ]);
    }



    /**
     * Lists all CarsAct models.
     * @return mixed
     */
    public function actionIndexChanges()
    {
        // $query = CarsAct::receiveChangesActCheckedQuery();

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        // ]);

        // $validator = new \yii\validators\StringValidator();

        // if ($validator->validate(Yii::$app->request->get('search'), $error)) {
        //     $query->joinWith('car as car');

        //     $query->andFilterWhere(['or', 
        //         ['like', 'car.name', Yii::$app->request->get('search')],
        //         // ['car.id'=>Yii::$app->request->get('search')],
        //         ['car.article'=>Yii::$app->request->get('search')],
        //     ]);
        // }

        // return $this->render('_changes_index', [
        //     'dataProvider' => $dataProvider,
        // ]);
        $query = Cars::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
                // ['id'=>Yii::$app->request->get('search')],
                ['article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('_changes_index', [
            'dataProvider' => $dataProvider,
            'model' => new CarsForm,
        ]);
    }


    /**
     * Lists all CarsAct models.
     * @return mixed
     */
    public function actionIndexMastering()
    {
        // $query = CarsAct::receiveMasteringActCheckedQuery();

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        // ]);

        // $validator = new \yii\validators\StringValidator();

        // if ($validator->validate(Yii::$app->request->get('search'), $error)) {
        //     $query->joinWith('car as car');

        //     $query->andFilterWhere(['or', 
        //         ['like', 'car.name', Yii::$app->request->get('search')],
        //         // ['car.id'=>Yii::$app->request->get('search')],
        //         ['car.article'=>Yii::$app->request->get('search')],
        //     ]);
        // }

        // return $this->render('_mastering_index', [
        //     'dataProvider' => $dataProvider,
        // ]);
        $query = Cars::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
                // ['id'=>Yii::$app->request->get('search')],
                ['article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('_mastering_index', [
            'dataProvider' => $dataProvider,
            'model' => new CarsForm,
        ]);
    }

    /**
     * Displays a single CarsAct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=Cars::findOne($id);

        $newCarsAct = new ActiveDataProvider([
            'query'=>CarsAct::find()->where(['car_id'=>$id, 'parent_id'=>null])
        ]);

        $masteringData = new ActiveDataProvider([
            'query' => CarsAct::find()->where(['car_id' => $id])->andWhere(['type'=> CarsAct::TYPE_MASTERING]),
             'sort'=> ['defaultOrder' => ['created_at'=>SORT_ASC]]
        ]);

        $changesData = new ActiveDataProvider([
            'query' => CarsAct::find()->where(['and',
                ['car_id' => $id],
                ['is not','parent_id', null],
            ]),
             'sort'=> ['defaultOrder' => ['created_at'=>SORT_ASC]]
        ]);

        return $this->render('view', [
            'model' => $model,
            'masteringData' => $masteringData,
            'changesData' => $changesData,
            'newCarsAct'=> $newCarsAct
        ]);
    }

    /**
     * Displays a single CarsAct model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewMastering($id)
    {
        $model=Cars::findOne($id);

        $dataProvider = new ActiveDataProvider([
            'query' => CarsAct::find()->where(['car_id' => $id])->andWhere(['type'=> CarsAct::TYPE_MASTERING]),
             'sort'=> ['defaultOrder' => ['created_at'=>SORT_ASC]]
        ]);


        return $this->render('_mastering_view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarsAct model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewChanges($id)
    {
        $model=Cars::findOne($id);

        $dataProvider = new ActiveDataProvider([
            'query' => CarsAct::find()->where(['and',
                ['car_id' => $id],
                ['or',
                    ['type'=> CarsAct::TYPE_CHANGES],
                    ['type' => null],
                ]
            ]),
             'sort'=> ['defaultOrder' => ['created_at'=>SORT_ASC]]
        ]);


        return $this->render('_changes_view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionActos($id,$p=null)
    {
        $model=$this->findModel($id);
        if ($model->parent_id)
            return $this->redirect(['act','id'=>$id,'p'=>$p]);

        switch ($p) {
            case null:
                $title='АКТ ОСВОЕНИЯ НОВОГО АВТОМОБИЛЯ № ';
                $content = $this->renderPartial('actos',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '1':
                $title='Приложение №1 к АКТУ ОНА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p1',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                $content = strtr($content, ['M/A'=>'M','M/B'=>'M','M/C'=>'M','M/M'=>'M','M/V'=>'M','M/P'=>'M']);
                break;
            case '2':
                $title='Приложение №2 к АКТУ ОНА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p2',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '3':
                $title='Приложение №3 к АКТУ ОНА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p3',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '4':
                $title='Приложение №4 к АКТУ ОНА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p4',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '5':
                $title='Приложение №5 к АКТУ ОНА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p5',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            default:
                throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
                break;
        }

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}', 
            'options' => ['title' => $title],
        ]);

        return $pdf->render(); 
    }

    public function actionActosLast($id,$p=null)
    {
        $model=CarsAct::find()->where(['car_id' => $id])->orderBy('id DESC')->one();

        switch ($p) {
            case null:
                $title='АКТ ТЕКУЩЕГО СОСТОЯНИЯ АВТОМОБИЛЯ № ';
                $content = $this->renderPartial('actos',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '1':
                $title='Приложение №1 к АКТУ ТСА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p1',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                $content = strtr($content, ['M/A'=>'M','M/B'=>'M','M/C'=>'M','M/M'=>'M','M/V'=>'M','M/P'=>'M']);
                break;
            case '2':
                $title='Приложение №2 к АКТУ ТСА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p2',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '3':
                $title='Приложение №3 к АКТУ ТСА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p3',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '4':
                $title='Приложение №4 к АКТУ ТСА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p4',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '5':
                $title='Приложение №5 к АКТУ ТСА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p5',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '7':
                $title='Приложение №7 к АКТУ ТСА № '.$model->json('after')['article'];
                $content = $this->renderPartial('p7',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            default:
                throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
                break;
        }

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}',
            'options' => ['title' => $title],
        ]);

        return $pdf->render();
    }

    public function actionTest($id)
    {
        $model=$this->findModel($id);
        header('Content-Type: application/json');
        echo json_encode(\yii\helpers\Json::decode($model->json), JSON_PRETTY_PRINT);
    }


    public function actionAct($id,$p=null)
    {
        $model=$this->findModel($id);
        if (!$model->parent_id)
            return $this->redirect(['actos','id'=>$id,'p'=>$p]);


        if (Yii::$app->request->isPost){
            $validator = new \yii\validators\StringValidator();

            if ($validator->validate(Yii::$app->request->post('beforecomment'), $error)) {
                $model->setjson('beforecomment',Yii::$app->request->post('beforecomment'));
            }
            if ($validator->validate(Yii::$app->request->post('aftercomment'), $error)) {
                $model->setjson('aftercomment',Yii::$app->request->post('aftercomment'));
            }
            if ($validator->validate(Yii::$app->request->post('korect_fw'), $error)) {
                $model->setjson('korect_fw',Yii::$app->request->post('korect_fw'));
            }
            if ($validator->validate(Yii::$app->request->post('korect_fv'), $error)) {
                $model->setjson('korect_fv',Yii::$app->request->post('korect_fv'));
            }
            if ($validator->validate(Yii::$app->request->post('korect_fd'), $error)) {
                $model->setjson('korect_fd',Yii::$app->request->post('korect_fd'));
            }
            if ($validator->validate(Yii::$app->request->post('korect_rd'), $error)) {
                $model->setjson('korect_rd',Yii::$app->request->post('korect_rd'));
            }
            if ($validator->validate(Yii::$app->request->post('korect_rv'), $error)) {
                $model->setjson('korect_rv',Yii::$app->request->post('korect_rv'));
            }
            if ($validator->validate(Yii::$app->request->post('korect_bw'), $error)) {
                $model->setjson('korect_bw',Yii::$app->request->post('korect_bw'));
            }
            if ($validator->validate(Yii::$app->request->post('korect_pk'), $error)) {
                $model->setjson('korect_pk',Yii::$app->request->post('korect_pk'));
            }
            return $this->refresh();
        }

        switch ($p) {
            case null:
                $title='Акт внесения изменений № '.$model->json('after')['article'].'/'.$model->json('number');
                if (
                    is_null($model->json('beforecomment')) 
                    || is_null($model->json('aftercomment'))
                    || is_null($model->json('korect_fw'))
                    || is_null($model->json('korect_fv'))
                    || is_null($model->json('korect_fd'))
                    || is_null($model->json('korect_rd'))
                    || is_null($model->json('korect_rv'))
                    || is_null($model->json('korect_bw'))
                    || is_null($model->json('korect_pk'))
                    ){
                    return $this->render('act',[
                        'edit' => true,
                        'model' => $model,
                        'title' => $title,
                        'after' => $model->json('after'),
                        'before' => $model->json('before'),
                    ]);
                }
                $content = $this->renderPartial('act',[
                    'edit' => false,
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '1':
                $title='Приложение №1 к АКТУ ВИ № '.$model->json('after')['article'].'/'.$model->json('number');
                $content = $this->renderPartial('p1',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                $content = strtr($content, ['M/A'=>'M','M/B'=>'M','M/C'=>'M','M/M'=>'M','M/V'=>'M','M/P'=>'M']);
                break;
            case '2':
                $title='Приложение №2 к АКТУ ВИ № '.$model->json('after')['article'].'/'.$model->json('number');
                $content = $this->renderPartial('p2',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '3':
                $title='Приложение №3 к АКТУ ВИ № '.$model->json('after')['article'].'/'.$model->json('number');
                $content = $this->renderPartial('p3',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '4':
                $title='Приложение №4 к АКТУ ВИ № '.$model->json('after')['article'].'/'.$model->json('number');
                $content = $this->renderPartial('p4',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '5':
                $title='Приложение №5 к АКТУ ВИ № '.$model->json('after')['article'].'/'.$model->json('number');
                $content = $this->renderPartial('p5',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            case '6':
                $title='Приложение №6 к АКТУ ВИ № '.$model->json('after')['article'].'/'.$model->json('number');
                $content = $this->renderPartial('p6',[
                    'model' => $model,
                    'title' => $title,
                    'after' => $model->json('after'),
                    'before' => $model->json('before'),
                ]);
                break;
            
            default:
                throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
                break;
        }


        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}', 
            'options' => ['title' => $title],
        ]);

        return $pdf->render(); 
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionChangeCheckRework($id)
    {
        $message = [];
        $message = Yii::$app->request->post('message');
        $message = implode(', ', $message ? $message : []);
        $act = CarsAct::findOne($id);
        if ($act->checked == 1 || !$message)
            return $this->redirect(['check-view','id'=>$id]);   
        $act->checked = 2;
        $json = Json::decode($act->json);
        $json['reworkmsg'] = $message;
        $act->json = Json::encode($json);
        $act->save();
        return $this->redirect(['check-view','id'=>$act->id]);
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionCheckMastering()
    {
        $query = CarsAct::receiveMasteringActForCheckQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {
            $query->joinWith('car as car');

            $query->andFilterWhere(['or', 
                ['like', 'car.name', Yii::$app->request->get('search')],
                // ['car.id'=>Yii::$app->request->get('search')],
                ['car.article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('_check_index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionReworkMastering()
    {
        $query = CarsAct::receiveMasteringActForReworkQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {
            $query->joinWith('car as car');

            $query->andFilterWhere(['or', 
                ['like', 'car.name', Yii::$app->request->get('search')],
                // ['car.id'=>Yii::$app->request->get('search')],
                ['car.article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('_check_index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionUpdateChanges($id,$step=0,$elem=null)
    {
        $model=new CarsCheckUpdateForm($id);
        $model->elem = $elem;
        if (Yii::$app->request->get('steps')){
            $model->scenario = 'step'.Yii::$app->request->get('steps')[$step-1];
            $model->SCENARIO_STEP_FINISH='step'.Yii::$app->request->get('steps')[count(Yii::$app->request->get('steps'))-1];
        } else {
            $model->scenario = 'step'.$step;
            $model->SCENARIO_STEP_FINISH=$model->scenario;
        }

        if ($load=Yii::$app->session->get('CarsCheckFormUpdate'.$id)){
            $model->attributes=$load;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['check-view','id'=>$id]);
        } else {

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                Yii::$app->session->set('CarsCheckFormUpdate'.$id,$model->attributes);

                if (Yii::$app->request->get('steps')){
                    return $this->redirect(ArrayHelper::merge(['update-changes','id'=>$id,'step'=>$step+1,'elem'=>$elem],['steps'=>Yii::$app->request->get('steps')]) );
                } else {
                    return $this->redirect(['update-changes','id'=>$id,'step'=>$step+1,'elem'=>$elem] );
                }
            }

            if ($step==0){

                return $this->redirect(['check-view','id'=>$id]);

            }

            return $this->render('_check_update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionCheckChanges()
    {
        $query = CarsAct::receiveChangesActForCheckQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {
            $query->joinWith('car as car');

            $query->andFilterWhere(['or', 
                ['like', 'car.name', Yii::$app->request->get('search')],
                // ['car.id'=>Yii::$app->request->get('search')],
                ['car.article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('_check_index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionReworkChanges()
    {
        $query = CarsAct::receiveChangesActForReworkQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {
            $query->joinWith('car as car');

            $query->andFilterWhere(['or', 
                ['like', 'car.name', Yii::$app->request->get('search')],
                // ['car.id'=>Yii::$app->request->get('search')],
                ['car.article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('_check_index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionCheckView($id)
    {
        $model=new CarsCheckForm($id);

        Yii::$app->session->remove('CarsFormUpdate'.$id);

        $clipsProvider = new ArrayDataProvider([
            'allModels' => Json::decode($model->act->json('after')['clips']),
            'sort' => [
                'attributes' => ['id', 'name', 'type'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $clipsProvider_b = new ArrayDataProvider([
            'allModels' => Json::decode($model->act->json('before')['clips']),
            'sort' => [
                'attributes' => ['id', 'name', 'type'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $analogsProvider = new ArrayDataProvider([
            'allModels' => Json::decode($model->act->json('after')['analogs']),
            'sort' => [
                'attributes' => [ 'name', 'elements'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $analogsProvider_b = new ArrayDataProvider([
            'allModels' => Json::decode($model->act->json('before')['analogs']),
            'sort' => [
                'attributes' => [ 'name', 'elements'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['cars/view','id'=>$model->act->car_id]);

        // $clipsProvider = new ArrayDa([
        //     'query' => $model->auto->getClips(),
        // ]);

        // $analogsProvider = new ActiveDataProvider([
        //     'query' => $model->auto->getAnalogs(),
        // ]);

        return $this->render('_check_view', [
            'model' => $model,
            'clipsProvider' => $clipsProvider,
            'clipsProvider_b' => $clipsProvider_b,
            'analogsProvider' => $analogsProvider,
            'analogsProvider_b' => $analogsProvider_b,
        ]);
    }

    /**
     * Finds the CarsAct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarsAct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarsAct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionCheckViewAct($id)
    {
        $model=new CarsCheckForm($id);

        $clipsProvider = new ArrayDataProvider([
            'allModels' => Json::decode($model->act->json('after')['clips']),
            'sort' => [
                'attributes' => ['id', 'name', 'type'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $clipsProvider_b = new ArrayDataProvider([
            'allModels' => Json::decode($model->act->json('before')['clips']),
            'sort' => [
                'attributes' => ['id', 'name', 'type'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $analogsProvider = new ArrayDataProvider([
            'allModels' => Json::decode($model->act->json('after')['analogs']),
            'sort' => [
                'attributes' => [ 'name', 'elements'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $analogsProvider_b = new ArrayDataProvider([
            'allModels' => Json::decode($model->act->json('before')['analogs']),
            'sort' => [
                'attributes' => [ 'name', 'elements'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('_check_view', [
            'model' => $model,
            'clipsProvider' => $clipsProvider,
            'clipsProvider_b' => $clipsProvider_b,
            'analogsProvider' => $analogsProvider,
            'analogsProvider_b' => $analogsProvider_b,
            'hiddenButton'=>true
        ]);
    }
}
