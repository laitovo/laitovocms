<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.04.2017
 * Time: 16:49
 */

namespace backend\modules\laitovo\controllers;

use core\services\SPolyInstructions;
use Yii;
use yii\web\Controller;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpTerminal;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\NotFoundHttpException;

class PrintController extends Controller
{
    public function actionTerminal()
    {
        $model = new ErpTerminal();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        return $this->render('terminal', [
            'model' => $model,

        ]);

    }

    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = new ErpTerminal();
        $barcode = $model->toLatin($barcode);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $actNumber = ErpNaryad::findAct($barcode);

        if ($actNumber) {
            $workOrders = ErpNaryad::find()->where(['actNumber' => $actNumber])->all();
            if (empty($workOrders)) return $this->renderContent('Не найдено ни одного артикула для этого акта!');

            $articles = [];
            $elements = [];
            $count = [];
            foreach ($workOrders as $workOrder) {
                if (!in_array($workOrder->article,$articles)) {
                    $articles[] = $workOrder->article;
                    $elements[] = $workOrder;
                }
                isset($count[$workOrder->article]) ? $count[$workOrder->article]++ : $count[$workOrder->article] = 1;
            }


            $one         = $elements[0];
//            $orderNumber = $one->order_id;
            $carArticle  = $one->carArticle;
            if (($instructionFilePath = SPolyInstructions::findInstruction('RHS-' . $carArticle))) {
                $response['status'] = 'success';
                $response['message'] = 'Распечатана инструкция для акта № ' . $actNumber;
                $response['file'] = $instructionFilePath;
                return $response;
            };
            if (count($articles) == 1 && ($instructionFilePath = SPolyInstructions::findInstruction($articles[0]))) {
                $response['status'] = 'success';
                $response['message'] = 'Распечатана инструкция для полипласта на артикул ' . $articles[0];
                $response['file'] = $instructionFilePath;
                return $response;
            };
        }

        $workOrder = null;
        $prodNumber = ErpNaryad::findProdGroup($barcode);
        if ($prodNumber) {
            $workOrders = ErpNaryad::find()->where(['prodGroup' => $prodNumber])->all();
            if (empty($workOrders)) return $this->renderContent('Не найдено ни одного артикула для этой группы нарядов!');

            $workOrder = array_shift($workOrders);
        }

        $naryad = $workOrder ?: ErpNaryad::find()->where(['barcode' => $barcode])->one();

        //Вставка на распечатку инструкции полипаста
        if ($naryad && $naryad->order && $naryad->order->json('username') == 'ПК Полипласт ООО') {
            if (($instructionFilePath = SPolyInstructions::findInstruction($naryad->article))) {
                $response['status'] = 'success';
                $response['message'] = 'Распечатана инструкция для полипласта на артикул ' . $naryad->article;
                $response['file'] = $instructionFilePath;
                return $response;
            };
        }

        if ($naryad && $naryad->json('items')[0]['car']) {//поиска наряда
            $model->workOrders[] = $naryad->id;
            $response['status'] = 'success';
            $response['message'] = 'распечатана инструкция для ' . mb_strtolower($naryad->name);
            $response['manual'] = $naryad->id;

        } elseif ($naryad && !$naryad->json('items')[0]['car']) {
            $response['status'] = 'error';
            $response['message'] = 'Для данного наряда нет инструкции';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Наряд не найден';
        }

        return $response;
    }

}