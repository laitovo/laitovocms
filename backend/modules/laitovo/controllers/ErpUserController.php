<?php

namespace backend\modules\laitovo\controllers;

use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;
use backend\modules\laitovo\models\ErpNaryad;
use Exception;
use Throwable;
use Yii;
use backend\modules\laitovo\models\ErpUser;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;
use backend\modules\laitovo\models\ReportCard;
use yii\web\Response;

/**
 * ErpUserController implements the CRUD actions for ErpUser model.
 */
class ErpUserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query=ErpUser::find();

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'id', Yii::$app->request->get('search')],
                ['like', 'name',Yii::$app->request->get('search')],
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (in_array(Yii::$app->user->getId(), [40,41,120]))
            throw new ForbiddenHttpException("Штраф 5000 рублей");

        $query = ErpLocationWorkplaceRegister::find();
        $query->where(['user_id' => $id]);
        $query->andWhere(['in','action_type',[ErpLocationWorkplaceRegister::TYPE_IN,ErpLocationWorkplaceRegister::TYPE_OUT]]);
        $query->orderBy(['register_at' => SORT_DESC]);
        $journalProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $report = new ReportCard();
        $model=$this->findModel($id);

        $naryadsProvider = $model->searchViewNaryads(Yii::$app->request->queryParams);

        if(!empty(Yii::$app->request->get('ErpUser')['dateFrom']))
            $report->dateFrom = Yii::$app->request->get('ErpUser')['dateFrom'];

        return $this->render('view', [
            'naryadsProvider' => $naryadsProvider,
            'journalProvider' => $journalProvider,
            'model' => $model,
            'report'=>$report
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionAjaxPause($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findModel($id);
            $workOrders = ErpNaryad::find()->where(['and',
                ['user_id' => $model->id],
                ['status' => ErpNaryad::STATUS_IN_WORK],
                ['location_id' => $model->location_id],
                ['start' => null],
            ])->all();

            foreach ($workOrders as $workOrder) {
                $workOrder->pause();
            }
            $transaction->commit();
            $status = true;
        } catch (Exception $e) {
            $transaction->rollBack();
            $status = false;
        } catch (Throwable $e) {
            $transaction->rollBack();
            $status = false;
        }

        return [
            'status' => $status,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionAjaxUnpause($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;


        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findModel($id);
            $workOrders = ErpNaryad::find()->where(['and',
                ['user_id' => $model->id],
                ['status' => ErpNaryad::STATUS_IN_PAUSE],
                ['location_id' => $model->location_id],
                ['start' => null],
            ])->all();

            foreach ($workOrders as $workOrder) {
                $workOrder->unpause();
            }
            $transaction->commit();
            $status = true;
        } catch (Exception $e) {
            $transaction->rollBack();
            $status = false;
        } catch (Throwable $e) {
            $transaction->rollBack();
            $status = false;
        }

        return [
            'status' => $status,
        ];
    }


    public function actionPrint($id)
    {
        $model = $this->findModel($id);

        $content = $this->renderPartial('print',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}', 
            'options' => ['title' => $model->name],
        ]);

        return $pdf->render(); 
    }

    /**
     * Creates a new ErpUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpUser();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ErpUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
