<?php

namespace backend\modules\laitovo\controllers;

use backend\modules\laitovo\models\ErpNaryad;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\laitovo\MaterialParameters;
use common\models\laitovo\UnitAccounting;
use common\models\laitovo\UnitSettlement;
use backend\modules\laitovo\models\Materials;
use common\models\laitovo\MaterialProductionScheme;
use backend\modules\laitovo\models\ErpLocation;
use yii\helpers\ArrayHelper;
use common\models\laitovo\UnitMaterials;
use backend\modules\laitovo\models\ErpMaterialCostReport;
use common\models\laitovo\Material;
use yii\data\ArrayDataProvider;


/**
 * MaterialController implements the CRUD actions for MaterialForm model.
 */
class MaterialController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MaterialForm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ErpLocation::find()->where(['not in','id',7]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MaterialForm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id); 
        
        return $this->render('view', [
            'model' => $model,
                     
        ]);
    }
   

    /**
     * Creates a new MaterialForm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Materials();
             
        if ($model->load(Yii::$app->request->post()) &&  $model->save())
            {   
                return $this->redirect(['view', 'id' => $model->id]);
            } 
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    
    
   

    /**
     * Updates an existing MaterialForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {   
        $model = Materials::findOne($id);
       
        if ($model->load(Yii::$app->request->post())&& $model->save())
        {  
            return $this->redirect(['view', 'id' => $id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    

    /**
     * Deletes an existing MaterialForm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MaterialForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MaterialForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MaterialParameters::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
   
    public function actionMaterialList()
    {
         $dataProvider = new ActiveDataProvider([
            'query' => MaterialParameters::find(),
        ]);

        return $this->render('material-list', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    
    
    public function actionMaterials()
    {
        $query = MaterialParameters::find()->joinWith('material');
        
         
        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'material.name', Yii::$app->request->get('search')],
                // ['id'=>Yii::$app->request->get('search')],
                ['like','parameter', Yii::$app->request->get('search')],
            ]);
        }
         $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('materials', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRegisterMaterialInLocation($production_scheme_id, $location_id)
    {
        $post = Yii::$app->request->post();
       // die(print_($post, true));
        if (isset($post['unregister']) && isset($post['material_id']))
        {
            $this->unRegister($location_id, $production_scheme_id, $post['material_id']);
        }
        
        if(isset($post['material_count']) && !isset($post['unregister']))
        {
            if($this->materialCount($location_id, $production_scheme_id, $post['material_count']))
                return $this->redirect(['/laitovo/production/view', 'id' => $production_scheme_id]);        
        }
        
        $query1 = MaterialProductionScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id]);
        $query2 = MaterialParameters::find()->where(['not in', 'id', ArrayHelper::map($query1->all(), 'id', 'material_parameter_id')])->joinWith(['materialLocations'=>function($q)use($location_id){      
                        $q->andWhere(['laitovo_erp_location_id'=>$location_id]);
                    }]);
        $materialSchemeProvider = new ActiveDataProvider([
            'query' => $query1,
        ]);
    
        $materialProvider = new ActiveDataProvider([
            'query' => $query2,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        
        return $this->render('register-material-in-location', [
            'materialSchemeProvider' => $materialSchemeProvider,
            'materialProvider' => $materialProvider,
            'production_scheme_id' => $production_scheme_id,
            'location_id' => $location_id,
            'flag'=>$query1->all()
        ]);        
    }
    
    
    /*
     * записываем количество материалов
     * 
     */
    public function materialCount($location_id, $production_scheme_id, $post=[])
    {
        if(isset($post))
        {
            foreach($post as $key=>$value)
            {
                $material = MaterialProductionScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id, 'material_parameter_id'=>$key])->one();
                if(isset($material->id))
                {
                    $material->material_count = $value;
                   if(!$material->save())
                        return false;
                }
            }
            return true;
        }
    }

    /*
 * Удаляем материалы из группы при нажатии на кнопку
 * 
 */    
     public function unRegister($location_id, $production_scheme_id, $material_id)
    {
        $materialType = MaterialProductionScheme::find()->where(['production_scheme_id'=>$production_scheme_id, 'location_id'=>$location_id, 'material_parameter_id'=>$material_id])->one();
        if($materialType)
        {   
            $materialType->delete();
            return true;
        }
        return false;
    }
    
    public function actionUpdateCount($material_id)
    {
        $material = MaterialProductionScheme::findOne($material_id);
        if(Yii::$app->request->isPost && Yii::$app->request->post('MaterialProductionScheme')['material_count']!==null)
        {   
            $material->material_count = Yii::$app->request->post('MaterialProductionScheme')['material_count'];
            $material->save();       
        }
        return $this->renderPartial('count_form',['model'=>$material]);
    
    }
    
    
    
     public function actionCreateMaterialUnit()
    {
        $model= new UnitMaterials();
        if($model->load(Yii::$app->request->post()))
        {
            $model->save();
           
        }
        return $this->renderPartial('create-unit-material',['model'=>$model]);
    }
    
    
    
     public function actionCreateMaterialName()
    {
        $model= new Material();
        if($model->load(Yii::$app->request->post()))
        {
            $model->save();
        }
        return $this->renderPartial('material-name',['model'=>$model]);
    }
    
    /*
     * Редактирование через модалку 
     * 
     */
    public function actionModalUpdate($id)
    {   
        $model = Materials::findOne($id);
       
        if ($model->load(Yii::$app->request->post()))
        {  
           $model->save();
        }
        return $this->renderPartial('modal-update', [
            'model' => $model,
        ]);
    }
    
    /*
     * Журнал расхода материала
     */
    public function actionReportMaterialCost()
    {

        $model = new ErpMaterialCostReport();
        $model->load(Yii::$app->request->get());

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $result =  $model->report(Yii::$app->request->get('search'));
        }else{
            $result= $model->report();
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $result['arrayData'],
            'sort' => [
                'attributes' => ['materialName', 'unit', 'count'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);



        return $this->render('report-material-cost', [
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'total_sum'=>$result['total_sum']
        ]);
    }
}
