<?php

namespace backend\modules\laitovo\controllers;

use backend\modules\laitovo\models\ToolFile;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;
use Yii;

/**
 * ErpLocationWorkplaceRegisterController implements the CRUD actions for ErpLocationWorkplaceRegister model.
 */
class ToolController extends Controller
{

    /**
     * Lists all ErpLocationWorkplaceRegister models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ToolFile();
        $string = '';
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($string = $model->readFile()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Файл успешно загружен!'));
                return $this->render('tool', ['model' => $model, 'result' => $string]);
            }
        }

        return $this->render('tool', ['model' => $model, 'result' => $string]);
    }

    /**
     * Lists all ErpLocationWorkplaceRegister models.
     * @return array
     * @throws HttpException
     */
    public function actionAjaxFile()
    {
        if (!Yii::$app->request->isAjax)
            throw new HttpException(403);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new ToolFile();
        $model->load(Yii::$app->request->post());
        $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
        return [
            'success' => $model->readFile(),
            'error' => ''
        ];
    }
}
