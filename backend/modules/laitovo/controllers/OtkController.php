<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\modules\laitovo\models\OtkTerminal;
use backend\modules\laitovo\models\OtkCreate;
use backend\modules\laitovo\models\ErpUser;

/**
 * Default controller for the `laitovo` module
 */
class OtkController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new OtkTerminal();
        $model2 = new OtkCreate();


        if ($model2->load(Yii::$app->request->post()) && $narayd_id=$model2->save()) {
            // return $this->redirect(['erp-naryad/print-with-manual', 'id' => $narayd_id]);
            return $this->render('_form_success',['id' => $narayd_id]);
        }

        $model2->user_id = $model->user_id;

        return $this->render('index',['model' => $model,'model2' => $model2]);
    }


    public function actionSaveNaryad()
    {
        //ищем из поста
        //возвращаем принт наряд, принт этикетка, 
        //возвращаем
    }

    public function actionBackUser()
    {
        $model = new OtkTerminal();
        $model->deleteProperties();
        Yii::$app->response->redirect(['laitovo/otk/index'],302,false);
        // return $this->render('index');

    }

    public function actionSearch($barcode)
    {
        //Данное действие только для Аякс запроса
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = new OtkTerminal();
        $barcode = $model->toLatin($barcode);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status']='error';
        $response['message']='Не найдено';

        //Все что можно будет искать через данный терминал  - это штрихкод участка, пользователя, наряда
        $user=ErpUser::find()->where(['barcode'=>$barcode])->one();

        //если пользователь найден, то возвращаем сообщение о том что пользователь найден
        if ($user){ 
            $model->user_id = $user->id;
            $model->saveProperties();
            $response['status'] = 'success';
            $response['message'] = $user->name; /*№: ' . implode(', ', $result);*/
        } else {
            $errors = [];
            foreach ($model->getErrors() as $key => $value) {
                $errors[] = $value[0];
            }
            $response['message'] = implode(', ', $errors);
        }   


        return $response;
    }


}
