<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use backend\modules\laitovo\models\ErpJobTypeRate;
use backend\modules\laitovo\models\ErpJobType;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;

/**
 * ErpJobTypeRateController implements the CRUD actions for ErpJobTypeRate model.
 */
class ErpJobTypeRateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpJobTypeRate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ErpJobTypeRate::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpJobTypeRate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $type = ErpJobType::findOne($model->type_id);

        return $this->render('view', [
            'model' => $model,
            'type' => $type,
        ]);
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);

        $content = $this->renderPartial('print',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}', 
            'options' => ['title' => $model->name],
        ]);

        return $pdf->render(); 
    }

    /**
     * Creates a new ErpJobTypeRate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type_id)
    {
        $model = new ErpJobTypeRate();
        $type = ErpJobType::findOne($type_id);
        $model->type_id = $type->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {



            return $this->render('create', [
                'model' => $model,
                'type' => $type,
            ]);
        }
    }

    /**
     * Updates an existing ErpJobTypeRate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $type = $model->type;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'type' => $type,
            ]);
        }
    }

    /**
     * Deletes an existing ErpJobTypeRate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['erp-job-types/view', 'id' => $model->type_id]);
    }

    /**
     * Finds the ErpJobTypeRate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpJobTypeRate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpJobTypeRate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
