<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use backend\modules\laitovo\models\ErpAdditionalWork;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\laitovo\Config;
use \backend\modules\laitovo\models\ErpJobType;
use backend\modules\laitovo\models\ErpUsersReport;

/**
 * ErpAdditionalWorkController implements the CRUD actions for ErpAdditionalWork model.
 */
class ErpAdditionalWorkController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpAdditionalWork models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ErpAdditionalWork::find(),
        ]);

        $dataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpAdditionalWork model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Подтверждение начисления заработной платы за наряд на работы.
     *
     * @param $id
     * @param string $redirect
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionConfirm($id,$redirect='view')
    {
        $model = $this->findModel($id);
        $model->confirmed = 1;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            if (!$model->save()) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('app', 'Не удалось подтвердить наряд!'));
                return $this->redirect([$redirect, 'id' => $model->id]);
            }

            $report = ErpUsersReport::find()->where(['additional_naryad_id' => $model->id])->one();
            if (!$report) {
                $report = new ErpUsersReport();
                $report->additional_naryad_id =  $model->id;

            }
            $report->user_id = $model->user_id;
            $report->location_id = $model->location_id ? $model->location_id : null;
            $report->job_type_id = $model->job_type ? $model->job_type : null;
            $report->job_count = $model->job_count ? $model->job_count : null;
            $report->job_price = $model->job_price ? $model->job_price : null;
            $report->job_rate = $model->job_type ? $model->jobType->rate : null;

            if (!$report->save()) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('app', 'Не сделать запись о заработной плате в журнал!'));
                return $this->redirect([$redirect, 'id' => $model->id]);
            }

            $transaction->commit();

        }catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect([$redirect, 'id' => $model->id]);

        }

        Yii::$app->session->setFlash('success', Yii::t('app', 'Вы успешно подтведили начисление заработной платы на наряд на работы!'));
        return $this->redirect([$redirect, 'id' => $model->id]);
    }

    /**
     * Creates a new ErpAdditionalWork model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpAdditionalWork();

        if ($model->load(Yii::$app->request->post()))
            {
                if($model->job_count == null) $model->job_count = 1;
                if($model->job_price == null)
                {
                     $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
                //                       средняя дневная норма                         цена нормированного коэффициента
                    $model->job_price = $config->json('averageRate')/ $model->jobType->rate *  $config->json('hourrate');
                }
                if($model->save())
                {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    

    /**
     * Updates an existing ErpAdditionalWork model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
             if($model->job_count == null) $model->job_count = 1;
                if($model->job_price == null)
                {
                     $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
                //                       средняя дневная норма                         цена нормированного коэффициента
                    $model->job_price = $config->json('averageRate')/ $model->jobType->rate *  $config->json('hourrate');
                }
                if($model->save())
                {
                    if($model->status == 0)
                    {
                        $report = ErpUsersReport::find()->where(['additional_naryad_id'=>$id])->one();
                        if($report)$report->delete();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Закрыть наряд - указать его выполненным
     *
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionFinish($id)
    {
        $model = $this->findModel($id);
        $model->finish_at = time();
        $model->status = 1;
        if ($model->save() && $model->status) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Наряд закрыт успешно!'));
        }else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Закрыть наряд не получилось. Что то пошло не так!'));
        };

        return $this->redirect(['view','id'=> $model->id]);
    }

    /**
     * Deletes an existing ErpAdditionalWork model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpAdditionalWork model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpAdditionalWork the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpAdditionalWork::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrintAdditionalNaryad($id)
    {
        $model = $this->findModel($id);

        return $this->renderPartial('print-additional-naryad', ['model'=>$model]);
    }
}
