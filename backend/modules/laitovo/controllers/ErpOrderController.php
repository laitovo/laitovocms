<?php

namespace backend\modules\laitovo\controllers;

use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\StorageState;
use Yii;
use backend\modules\laitovo\models\ErpOrder;
use common\models\laitovo\Cars;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\order\Product;
use backend\helpers\BarcodeHelper;
use yii\web\Response;
use yii\base\Module;

/**
 * ErpOrderController implements the CRUD actions for ErpOrder model.
 */
class ErpOrderController extends Controller
{
    private $_pauseOrder;
    private $_accelerateOrder;

    public function __construct($id, Module $module, array $config = [])
    {
        $this->_pauseOrder = Yii::$container->get('core\logic\PauseOrder');
        $this->_accelerateOrder = Yii::$container->get('core\logic\AccelerateOrder');

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => false,
                        'actions' => ['delete'],
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionCheckState($article)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = [];
        $response['status'] = 'info';

        $model = new ErpOrder();
        $response['message'] = ($mes = $model->reserveNew($article)) == '' ? 'Не нахожу данного артикула на остатках!!!' : $mes ;

        return $response;
    }

    public function actionWriteOff($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $barcode = BarcodeHelper::toLatin($barcode);

        $response = [];
        $response['status'] = 'error';
        $response['message'] = 'Невозможно списать с остатков, обратитесь к IT службе!!!';


        $upn = Naryad::find()->where(['barcode' => $barcode])->one();
        if (!$upn) {
            $response['message'] = 'Такого UPN не существует в базе. Скорее всего не правильно считали штрихкод, повторите попытку!!!';
            return $response;
        }

        $state = StorageState::find()->where(['upn_id' => $upn->id])->one();
        if (!$state) {
            $response['message'] = 'Данный UPN не найден на остатках склада!!! Невозможно произвести списание. Возможно он уже был списан ранее';
            return $response;
        }

        if ($state->delete()) {
            $response['status'] = 'success';
            $response['message'] = 'Списание произошло успешно!!! UPN № ' . $upn->id . 'Успешно списан с остатков склада';
            return $response;
        }

        return $response;
    }
    
    
    public function actionNext()
    {
        $next=ErpOrder::find()->where(['or',['status'=>null],['status'=>'']])->orderBy('sort,created_at')->one();
        if ($next){
            return $this->redirect(['view','id'=>$next->id]);
        } else {
            return $this->redirect(['index']);
        }
    }
    /**
     * Lists all ErpOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $search = Yii::$app->request->get('search');
        $show = $this->_updateSession('erpOrder_index_show', 'erpOrder_index_show', 'all');

        $query=ErpOrder::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate($search, $error)) {

            $query->andFilterWhere(['or', 
                ['like','id',$search],
                ['like','order_id',$search],
            ]);
        }
        if (!isset($search) || $search == '') {
            switch ($show) {
                case 'onProduction':
                    $query->andWhere(['or',
                        ['in', 'status', [ErpOrder::STATUS_READY_TO_WORK, ErpOrder::STATUS_IN_WORK]],
                        ['status' => null]
                    ]);
                    break;
                case 'onPause':
                    $query->andWhere(['in', 'id', $this->_pauseOrder->getPausedOrdersIds()]);
                    break;
                default:
                    break;
            }
        }

        return $this->render('index', [
            'model'             => new ErpOrder,
            'dataProvider'      => $dataProvider,
            'show'              => $show,
            'countAll'          => ErpOrder::countAll(),
            'countOnProduction' => ErpOrder::countOnProduction(),
            'countOnPause'      => $this->_pauseOrder->getPausedOrdersCount(),
            'pauseOrder'        => $this->_pauseOrder,
            'accelerateOrder'   => $this->_accelerateOrder
        ]);
    }

    /**
     * Displays a single ErpOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (in_array(Yii::$app->user->getId(), [40,41,120]))
            throw new ForbiddenHttpException("Штраф 5000 рублей");

        $model = $this->findModel($id);

        return $this->render('view', [
            'model'        => $model,
            'orderEntries' => $this->_getOrderEntriesData($id)
        ]);
    }

    /**
     * Creates a new ErpOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view','id'=>$model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view','id'=>$model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionToNaryad($id)
    {
        set_time_limit(0);
        $model = $this->findModel($id);

        if ($model->toNaryad('one', Yii::$app->request->post('items'))) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Наряды сформированы.'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Невозможно сформировать наряды.'));
        }
        return $this->redirect(['view','id'=>$model->id]);
    }

    /**
     * Deletes an existing ErpOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $article='%';
        $article.=mb_substr($barcode, 0,2,'utf-8');
        $article.='-%-';
        $article.=(int) mb_substr($barcode, 2,4,'utf-8');
        $article.='-';
        $article.=(int) mb_substr($barcode, 6,2,'utf-8');
        $article.='-';
        $article.=(int) mb_substr($barcode, 8,1,'utf-8');
        $article.='%';

        $window=mb_substr($barcode, 0,2,'utf-8');
        $window2=mb_strtolower($window);
        $window2=str_replace('fw', 'ps', $window2);
        $window2=str_replace('fv', 'pf', $window2);
        $window2=str_replace('fd', 'pb', $window2);
        $window2=str_replace('rd', 'zb', $window2);
        $window2=str_replace('rv', 'zf', $window2);
        $window2=str_replace('bw', 'zs', $window2);

        $cararticle=(int) mb_substr($barcode, 2,4,'utf-8');
        $typewindow=(int) mb_substr($barcode, 6,2,'utf-8');
        $color=(int) mb_substr($barcode, 8,1,'utf-8');

        $response['status']='error';
        $response['message']='Товар не найден';

        $product=Product::find()->where(['like' ,'article',$article, false])->one();
        $car=Cars::find()->where(['article'=>$cararticle])->one();

        if ($product){
            $response['status']='success';
            $response['message']='Товар добавлен';
            $response['name']=$product->name;
            $response['article']=$product->article;
            $response['barcode']=$barcode;
            $response['car']='';
            $response['lekalo']='';
            $response['natyag']='';
            $response['visota']='';
            $response['magnit']='';
            $response['hlyastik']='';
            $response['comment']='';
            if ($car) {
                $response['car']=$car->name;
                $response['lekalo']=$car->json('nomerlekala');
                if ($color==5){//chiko
                    switch ($typewindow) {
                        case '2':
                            $type='smoke';
                            break;
                        case '3':
                            $type='mirror';
                            break;
                        case '6':
                            $type='short';
                            break;
                        
                        default:
                            $type='standart';
                            break;
                    }
                    $response['natyag']=@$car->json('_'.mb_strtolower($window))['chiko'][$type]['natyag'];
                }
                $response['visota']=@$car->json('visota'.mb_strtolower($window2));
                $response['magnit']=@$car->json('_'.mb_strtolower($window))['magnit'];
                $response['hlyastik']=@$car->json('_'.mb_strtolower($window))['hlyastik'];
                $response['comment']=$car->json('info');
            }
        }

        return $response;
    }

    /**
     * Постановка заказа на паузу
     * (ajax-функция)
     *
     * @param int $id
     * @return array
     */
    public function actionAjaxPause($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $result = $this->_pauseOrder->pause($model);

        return [
            'status'  => $result['status'],
            'message' => $result['message']
        ];
    }

    /**
     * Снятие заказа с паузы
     * (ajax-функция)
     *
     * @param int $id
     * @return array
     */
    public function actionAjaxUnpause($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $result = $this->_pauseOrder->unpause($model);

        return [
            'status'  => $result['status'],
            'message' => $result['message']
        ];
    }

    /**
     * Изменение срочности заказа
     * (ajax-функция)
     *
     * @param int $id
     * @param int $sort
     * @return array
     */
    public function actionAjaxUpdatePriority($id, $sort)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $result = $this->_accelerateOrder->updatePriority($model, $sort);

        return [
            'status'  => $result['status'],
            'message' => $result['message'],
        ];
    }

    /**
     * Finds the ErpOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
    }

    private function _updateSession($name, $pageFlag, $defaultValue = false)
    {
        $session      = Yii::$app->session;
        $remember     = Yii::$app->request->post($pageFlag);
        $sessionValue = $remember ? Yii::$app->request->post($name, $defaultValue) : $session->get($name, $defaultValue);
        $session->set($name, $sessionValue);

        return $sessionValue;
    }

    /**
     * Костыль для страницы "Просмотр заказа"
     * todo: вынести в модель между контроллером и менеджером
     *
     * @param int $sourceInnumber
     * @return array
     */
    private function _getOrderEntriesData($sourceInnumber)
    {
        $shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $order = \backend\modules\logistics\models\Order::find()->where(['source_innumber' => $sourceInnumber])->one();
        if ($order) {
            $orderEntries = \backend\modules\logistics\models\OrderEntry::find()->select(['id','article', 'shipment_id','is_deleted'])->where(['order_id' => $order->id])->orderBy('id')->asArray()->all();
        }else{
            $orderEntries = [];
        }
        $data = [];
        foreach($orderEntries as $orderEntry) {
            $upn = Naryad::find()->where(['reserved_id' => $orderEntry['id']])->one();
            $article = $orderEntry['article'];
            $shipment = $shipmentManager->findById($orderEntry['shipment_id']);
            $orderEntryData = [
                'isShipped' => $shipment && $shipmentManager->isShipped($shipment),
                'isDeleted' => $orderEntry['is_deleted'],
                'person'    => @$upn->responsible_person,
                'date'      => \Yii::$app->formatter->asDatetime(@$upn->created_at),
                'upn'       => @$upn->id,
            ];
            $data[$article][] = $orderEntryData;
        }

        return $data;
    }
}
