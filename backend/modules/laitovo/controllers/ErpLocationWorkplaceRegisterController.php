<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use backend\modules\laitovo\models\ErpLocationWorkplace;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegisterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ErpLocationWorkplaceRegisterController implements the CRUD actions for ErpLocationWorkplaceRegister model.
 */
class ErpLocationWorkplaceRegisterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpLocationWorkplaceRegister models.
     * @return mixed
     */
    public function actionIndex()
    {
        //збрасываем активность перед просмотром журнала
        ErpLocationWorkplace::withdrawActivity();

        $searchModel = new ErpLocationWorkplaceRegisterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpLocationWorkplaceRegister model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpLocationWorkplaceRegister model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpLocationWorkplaceRegister();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new ErpLocationWorkplaceRegister model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateFromUser($user)
    {
        $model = new ErpLocationWorkplaceRegister();
        $model->user_id = $user;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Запись успешно добавлена !'));
            return $this->redirect(['create-from-user', 'user' => $model->user_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ErpLocationWorkplaceRegister model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['erp-user/view', 'id' => $model->user_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ErpLocationWorkplaceRegister model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpLocationWorkplaceRegister model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpLocationWorkplaceRegister the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpLocationWorkplaceRegister::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    /**
     * Форма ручной активации работника на рабочем месте
     */

    public function actionManualRegistration()
    {
        $model = new ErpLocationWorkplaceRegister();
        if($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect('index');
        return $this->render('manual-register', ['model'=>$model]);
    }
}
