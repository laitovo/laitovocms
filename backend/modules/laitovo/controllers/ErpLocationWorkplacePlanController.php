<?php

namespace backend\modules\laitovo\controllers;

use Yii;
use backend\modules\laitovo\models\ErpLocationWorkplace;
use backend\modules\laitovo\models\ErpLocationWorkplacePlan;
use backend\modules\laitovo\models\ErpLocationWorkplacePlanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ErpLocationWorkplacePlanController implements the CRUD actions for ErpLocationWorkplacePlan model.
 */
class ErpLocationWorkplacePlanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpLocationWorkplacePlan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ErpLocationWorkplacePlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpLocationWorkplacePlan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpLocationWorkplacePlan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpLocationWorkplacePlan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new ErpLocationWorkplacePlan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateForWorkplace($workplace)
    {
        // это действие используем для планирования только вспомогательных рабочих мест
        $workplaceObj = ErpLocationWorkplace::findOne($workplace);
        $locationObj = $workplaceObj->location;
        if ($locationObj->isProduction())
           return $this->redirect(['erp-location-workplace/monitor-plan']);

        $model = new ErpLocationWorkplacePlan();
        $model->workplace_id = $workplace;

        $searchModel = new ErpLocationWorkplacePlanSearch();
        $searchModel->workplace_id = $workplace;
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        if ($model->load(Yii::$app->request->post())) {
            //удаляем все планы с текущего рабочего места
            $delPlans = ErpLocationWorkplacePlan::find()->where(['workplace_id' => $workplace])->all();
            foreach ($delPlans as $value) {
                $value->delete();
            }
            //удаляем планы с других вспомогательных рабочих места
            $delPlans = ErpLocationWorkplacePlan::find()->where(['user_id' => $model->user_id])->all();
            foreach ($delPlans as $value) {
                $innerWorkplace = ErpLocationWorkplace::findOne($value->workplace_id);
                $innerLocation  = $innerWorkplace->location;
                if ($innerLocation->isSupport())
                    $value->delete();
            }
            //добавляем новый план
            $model->save();
            
            //делаем возможжность для нового заполнения
            $model = new ErpLocationWorkplacePlan();
            $model->workplace_id = $workplace;
        }

        return $this->render('create_for', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing ErpLocationWorkplacePlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ErpLocationWorkplacePlan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpLocationWorkplacePlan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpLocationWorkplacePlan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpLocationWorkplacePlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
