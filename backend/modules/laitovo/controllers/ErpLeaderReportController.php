<?php

namespace backend\modules\laitovo\controllers;

use backend\modules\laitovo\models\ErpOrder;
use backend\modules\laitovo\models\ErpOrderTimeReport;
use core\logic\UsingTemplateReport;
use Yii;
use backend\modules\laitovo\models\ErpTextileReport;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpTimeoutReport;
use backend\modules\laitovo\models\ErpDangerTimeoutReport;
use backend\modules\laitovo\models\ProductReportSearch;
use backend\modules\laitovo\models\AdditionalProductReportSearch;
use backend\modules\laitovo\models\OtherProductReportSearch;
use backend\modules\laitovo\models\LocationReportSearch;


/**
 * ErpLeaderReportController implements the CRUD actions for ErpJobLocationRate model.
 */
class ErpLeaderReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays a single ErpJobLocationRate model.
     * @param integer $id
     * @return mixed
     */

    public function actionTextileReport()
    {
        ini_set('memory_limit', '384M');

        $model = new ErpTextileReport();

        if ($model->load(Yii::$app->request->post() )) {

            return $this->render('_textile', [
                    'model' => $model,
                    'ArrayDataProvider' => $model->extractData(),
                    'count' => $model->calcTotal(),
            ]);
        }else{
            #Это надо решить похзже и перенести установку в модель
            $model->dateFrom = mktime(0,0,0);
            $model->dateTO = mktime(0,0,0);

            $ArrayDataProvider = $model->extractData();
            return $this->render('_textile', [
                    'ArrayDataProvider' => $ArrayDataProvider,
                    'model' => $model,
                    'count' => $model->calcTotal(),
            ]);
        }
    }

    public function actionOrderTimeReport()
    {
        $model = new ErpOrderTimeReport();


        if ($model->load(Yii::$app->request->post())) {

            $objReport = $model->generateReport();

            $ArrayDataProvider = new ArrayDataProvider([
                'allModels' => $objReport['arResult'],
                'sort' => [
                    'attributes' => ['title', 'count'],
                ],
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('_ordertime', [
                'model' => $model,
                'ArrayDataProvider' => $ArrayDataProvider ,
                'objReport' => $objReport ,
            ]);
        }else{

            $model->dateFrom = mktime(0,0,0,date('m'),1);
            $model->dateTo = mktime(0,0,0);

            $objReport = $model->generateReport();

            $ArrayDataProvider = new ArrayDataProvider([
                'allModels' => $objReport['arResult'],
                'sort' => [
                    'attributes' => ['title', 'count','countCur'],
                ],
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('_ordertime', [
                'model' => $model,
                'ArrayDataProvider' => $ArrayDataProvider ,
                'objReport' => $objReport,
            ]);

        }

    }

    public function actionOrderTimeReportView($dateFrom,$dateTo,$index)
    {
        $model = new ErpOrderTimeReport();

        $ArrayDataProvider = new ArrayDataProvider([
            'allModels' => $model->receiveNaryads($dateFrom,$dateTo,$index),
            'sort' => [
                'attributes' => ['id', 'name','status','created_at','updated_at', 'sort'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('_ordertime_view', [
            'model' => $model,
            'flag' => 1,
            'ArrayDataProvider' => $ArrayDataProvider ,
        ]);
    }

    public function actionOrderTimeReportViewCur($dateFrom,$dateTo,$index)
    {
        $model = new ErpOrderTimeReport();

        $ArrayDataProvider = new ArrayDataProvider([
            'allModels' => $model->receiveNaryadsCur($dateFrom,$dateTo,$index),
            'sort' => [
                'attributes' => ['id', 'name','status','created_at','updated_at'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('_ordertime_view', [
            'model' => $model,
            'flag' => 2,
            'ArrayDataProvider' => $ArrayDataProvider ,
        ]);
    }

    public function actionProductReport(){

        $searchModel = new ProductReportSearch();
        $searchModel->dateFrom = mktime(0,0,0,date('m'),1);
        $searchModel->dateTo = mktime(0,0,0);
        $dataGeneralProvider = $searchModel->searchGeneralProduct(Yii::$app->request->queryParams)['dataProvider'];
        $dataAdditionalProvider = $searchModel->searchAdditionalProduct(Yii::$app->request->queryParams)['dataProvider'];
        $dataOtherProvider = $searchModel->searchOtherProduct(Yii::$app->request->queryParams)['dataProvider'];
        $totalGeneralSumm = $searchModel->searchGeneralProduct(Yii::$app->request->queryParams)['totalSumm'];
        $totalAdditionalSumm = $searchModel->searchAdditionalProduct(Yii::$app->request->queryParams)['totalSumm'];
        $totalOtherSumm = $searchModel->searchOtherProduct(Yii::$app->request->queryParams)['totalSumm'];

        return $this->render('product-report',[
            'searchModel' => $searchModel,
            'dataGeneralProvider' => $dataGeneralProvider,
            'dataAdditionalProvider'=> $dataAdditionalProvider,
            'dataOtherProvider' =>$dataOtherProvider,
            'totalGeneralSumm' => $totalGeneralSumm,
            'totalAdditionalSumm' => $totalAdditionalSumm,
            'totalOtherSumm' => $totalOtherSumm
        ]);
    }

    public function actionShowNaryads($article, $dateFrom,$dateTo,$status)
    {
      $dateFrom = Yii::$app->formatter->asTimestamp($dateFrom);
      $dateTo = Yii::$app->formatter->asTimestamp($dateTo) + (60 * 60 * 24 - 1);

      $query = ErpNaryad::find()->where(['article'=>$article])
            ->andWhere(['status'=> $status])
            ->andWhere(['and',
              ['>', 'updated_at', $dateFrom],
              ['<', 'updated_at', $dateTo]
            ]);

      $dataProvider = new ActiveDataProvider([
        'query' => $query
      ]);
      return $this->render('_show_naryads', [
            'dataProvider' => $dataProvider,
            ]);
    }

    public function actionLocationReport()
    {
      $searchModel = new LocationReportSearch();
      $searchModel->dateFrom = mktime(0,0,0,date('m'),1);
      $searchModel->dateTo = mktime(0,0,0);

      $dataProvider = $searchModel->search(Yii::$app->request->queryParams)['dataProvider'];
      $totalSumm = $searchModel->search(Yii::$app->request->queryParams)['totalSumm'];

      return $this->render('location-report',[
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
          'totalSumm' => $totalSumm
      ]);
    }


    /*Отчет по текущим нарядам на человеке*/

    public function actionTimeoutReport($hard = null)
    {
        if ($hard) //если нужен подробнйы
        {
            $searchModel = new ErpTimeoutReport();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('_timeout_report', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            $searchModel = new ErpTimeoutReport();
            $dataProvider = $searchModel->searchLite(Yii::$app->request->queryParams);

            return $this->render('_timeout_report_lite', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

    }


    public function actionDangerTimeoutReport($hard = null)
    {
        if ($hard) //если нужен подробнйы
        {
            $searchModel = new ErpDangerTimeoutReport();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('_danger_timeout_report', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            $searchModel = new ErpDangerTimeoutReport();
            $dataProvider = $searchModel->searchLite(Yii::$app->request->queryParams);

            return $this->render('_danger_timeout_report_lite', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

    }

    public function actionDangerPauseTimeoutReport($hard = null)
    {
        if ($hard) //если нужен подробнйы
        {
            $searchModel = new ErpDangerTimeoutReport();
            $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);

            return $this->render('_danger_timeout_report', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            $searchModel = new ErpDangerTimeoutReport();
            $dataProvider = $searchModel->searchLite2(Yii::$app->request->queryParams);

            return $this->render('_danger_timeout_report_lite', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

    }

    public function actionDangerUserTimeoutReport($hard = null)
    {
        if ($hard) //если нужен подробнйы
        {
            $searchModel = new ErpDangerTimeoutReport();
            $dataProvider = $searchModel->search3(Yii::$app->request->queryParams);

            return $this->render('_danger_timeout_report', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            $searchModel = new ErpDangerTimeoutReport();
            $dataProvider = $searchModel->searchLite3(Yii::$app->request->queryParams);

            return $this->render('_danger_timeout_report_lite', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

    }

    /**
     * Отчет по остаткам в биззоне.
     * @return string
     */
    public function actionBalance(){
        return $this->render('_balance', [
        ]);
    }

    /**
     * Генерация отчета в html об исопльзовании лекал за последний год
     */
    public function actionUsingTemplateStat()
    {
        $provider = UsingTemplateReport::execute();
        return $this->render('using_templates_stat', [
            'provider' => $provider
        ]);
    }
}
