<?php

namespace backend\modules\laitovo\controllers;

use backend\modules\admin\models\User;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpLocationWorkplace;
use backend\modules\laitovo\models\ErpLocationWorkplacePlan;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;
use backend\modules\laitovo\models\ErpLocationWorkplaceSearch;
use backend\modules\laitovo\models\ErpLocationWorkplacePlanSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\Response;


/**
 * ErpLocationWorkplaceController implements the CRUD actions for ErpLocationWorkplace model.
 */
class ErpLocationWorkplaceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErpLocationWorkplace models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ErpLocationWorkplaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpLocationWorkplace model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new ErpLocationWorkplacePlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());


        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new ErpLocationWorkplace model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpLocationWorkplace();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Creates a new ErpLocationWorkplace model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateFrom($from,$id)
    {
        $model = new ErpLocationWorkplace();
        $model->location_id = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['erp-location/view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing ErpLocationWorkplace model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['monitor', 'location_id' => $model->location_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ErpLocationWorkplace model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing ErpLocationWorkplace model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteFromLocation($id,$from)
    {
        $this->findModel($id)->delete();
            return $this->redirect(['erp-location/view','id'=>$from,'#'=>'workplaces']);
    }

    /**
     * Finds the ErpLocationWorkplace model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpLocationWorkplace the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpLocationWorkplace::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Displays a single ErpLocationWorkplace model.
     * @param integer $id
     * @return mixed
     */
    public function actionMonitor($location_id = null)
    {
        //Збрасываем активность рабочих мест
        ErpLocationWorkplace::withdrawActivity();

        $searchModel = new ErpLocationWorkplaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_monitor', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'location_id' => $location_id,
        ]);

    }
    /**
     * Displays a single ErpLocationWorkplace model.
     * @param integer $id
     * @return mixed
     */
    public function actionMonitorPlan()
    {
        //Збрасываем активность рабочих мест

        $searchModel = new ErpLocationWorkplaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_monitor_plan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCancelUser($workplace_id = null)
    {
        $step = Yii::$app->request->post('step');
        if(!isset($step))
        {
            return $this->renderPartial('_cancel_user', [
                'status' => 'confirm',
                'workplace_id' => $workplace_id,
            ]);  
        }else{
            $workplace = ErpLocationWorkplace::findOne(Yii::$app->request->post('workplace_id'));
            if ($workplace)
            {
                if ($workplace->user_id)
                {
                    //даем команду на снятие с участка
                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $workplace->id;
                    $register->user_id = $workplace->user_id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_SEND_FROM;
                    $register->register_at = time();
                    $register->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $workplace->id;
                    $register->user_id = $workplace->user_id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                    $register->register_at = time();
                    $register->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $workplace->id;
                    $register->user_id = $workplace->user_id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                    $register->register_at = time();
                    $register->save();
                }

                $workplace->user_id = null;
                $workplace->activity = 0;
                $workplace->save();

                //ищем планирование и удаляем его
                $plan = ErpLocationWorkplacePlan::find()
                    ->where(['workplace_id' => $workplace->id])
                    ->one();

                if ($plan)
                    $plan->delete();

                return $this->renderPartial('_cancel_user', [
                    'status' => 'success',
                ]);  
            }

            return $this->renderPartial('_cancel_user', [
                //params
            ]); 
        }
    }

    /**
     *
     * @param $location_id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionCancelUsersFromWorkplaces($location_id = null)
    {
        if (!in_array(Yii::$app->user->getId(), [2, 21, 36])) {
            throw new ForbiddenHttpException("Недостаточно прав!");
        }
        
        $query = ErpLocation::find()
            ->where(['type' => ErpLocation::TYPE_PRODUCTION]);

        if ($location_id)
            $query->andWhere(['id' => $location_id]);

        $locations = $query->all();

        foreach ($locations as $location) {
            /** @var $location ErpLocation */
            $workplaces = $location->getWorkplaces()->all();
            foreach ($workplaces as $workplace) {
                if ($workplace->user_id) {
                    //даем команду на снятие с участка
                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $workplace->id;
                    $register->user_id = $workplace->user_id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_SEND_FROM;
                    $register->register_at = time();
                    $register->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $workplace->id;
                    $register->user_id = $workplace->user_id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_INACTIVE;
                    $register->register_at = time();
                    $register->save();

                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $workplace->id;
                    $register->user_id = $workplace->user_id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_MOVE_FROM;
                    $register->register_at = time();
                    $register->save();

                    $user = ErpUser::findOne($workplace->user_id);
                    $user->location_id = null;
                    $user->save();
                }

                $workplace->user_id = null;
                $workplace->activity = 0;
                $workplace->save();

                //ищем планирование и удаляем его
                $plan = ErpLocationWorkplacePlan::find()
                    ->where(['workplace_id' => $workplace->id])
                    ->one();

                if ($plan)
                    $plan->delete();
            }
        }

        return $this->redirect(['monitor', 'location_id' => $location_id]);
    }

    /**
     * Displays a single ErpLocationWorkplace model.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeUser($user=null,$workplace=null, $location_type=null)
    {   
        //Здесь мы должны определить по какому сценрию идем
        //1. Если передан пользователь, то мы его перемещаем

        $strUser = $user;
        if (!isset($strUser)) $strUser = Yii::$app->request->post('user');

        $strWorkplace = $workplace;
        if (!isset($strWorkplace)) $strWorkplace = Yii::$app->request->post('workplace');

        //здесь рабиваем на 2 сценария
        if (isset($strUser))
        {
            //внутри каждого алгоритма все разибваеться на расСтеповку (по шаговую реализацию)
            $step = Yii::$app->request->post('step');
            if (!isset($step))
            {
                $locations = 
                    ArrayHelper::map(ErpLocation::find()->all(),'id','name');
                return $this->renderPartial('_change_user', [
                    'user' => $strUser, 
                    'locations' => $locations, 
                ]);
            }
            elseif ($step && $step == 2)
            {
                $location_id = Yii::$app->request->post('location');
                $location = ErpLocation::findOne(['id'=>$location_id]);
                $workplaces = ArrayHelper::map(ErpLocationWorkplace::find()
                    ->where(['location_id' => $location])
                    ->all(),'id','location_number');
                return $this->renderPartial('_change_user', [
                    'user' => $strUser,
                    'step' => $step,
                    'workplaces' => $workplaces,
                    'location' => $location,
                ]);  
            }
            elseif ($step && $step == 3)
            {
                $workplace_id = Yii::$app->request->post('workplace_id'); 
 
                //получим пользователя 
                $objUser = ErpUser::findOne(['id'=> $strUser]); 
                $objWorkplace = ErpLocationWorkplace::findOne(['id' => $workplace_id]); 

 
                $delWorkplaces = ErpLocationWorkplacePlan::find()->where(['user_id' => $strUser ])->orWhere(['workplace_id' => $workplace_id])->all(); 

                foreach ($delWorkplaces as $delWorkplace) { 
                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $delWorkplace->workplace_id;
                    $register->user_id = $delWorkplace->user_id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_SEND_FROM;
                    $register->register_at = time();
                    $register->save();
                    
                    $delWorkplace->delete();
                } 

                $objWorkplaceOlan = new ErpLocationWorkplacePlan(); 
                $objWorkplaceOlan->user_id = $strUser;
                $objWorkplaceOlan->workplace_id = $workplace_id; 
                $objWorkplaceOlan->time_from = mktime(6,0,0); 
                $objWorkplaceOlan->time_to = mktime(20,0,0); 
                $objWorkplaceOlan->save();

                //сделать запись о смене событий
                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $workplace_id;
                $register->user_id = $strUser;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_SEND_TO;
                $register->register_at = time();
                $register->save();
 
                // $objUser->location_id = $objWorkplace->location_id; 
                // $objUser->save(); 
 
                // $objWorkplace->user_id = $objUser->id; 
                // $objWorkplace->save(); 
                return $this->renderPartial('_change_user', [
                    'user' => $strUser,
                    'step' => $step,
                ]);  
            }   
        }
        elseif (isset($strWorkplace))
        {
            $step = Yii::$app->request->post('step');
            if (!isset($step))
            {

                $users = ErpUser::userInWork() ;

                return $this->renderPartial('_change_user', [
                    'workplace' => $strWorkplace, 
                    'users' => $users, 
                ]);
            }
            elseif ($step && $step == 2)
            {
                $user_id = Yii::$app->request->post('user_id'); 
                $delFlag = Yii::$app->request->post('delete');

                //ищем все запланированные места для данного человка
                $planWorkplaces = ErpLocationWorkplacePlan::find()->where(['user_id' => $user_id ])->all();
                $curPlanWorkplace = ErpLocationWorkplacePlan::find()->where(['workplace_id' => $strWorkplace])->one();

                //в любом случае для текущего места даелаем запись, если что то есть
                if ($curPlanWorkplace)
                {
                    $register = new ErpLocationWorkplaceRegister();
                    $register->workplace_id = $curPlanWorkplace->workplace_id;
                    $register->user_id = $curPlanWorkplace->user_id;
                    $register->action_type = ErpLocationWorkplaceRegister::TYPE_SEND_FROM;
                    $register->register_at = time();
                    $register->save();

                    $curPlanWorkplace->delete();
                }

                //в слуае если мы не добавляем а изменяем рабочее место тогда
                if($delFlag)
                {
                    foreach ($planWorkplaces as $planWorkplace) { 
                        //сделать запись о смене событий
                        $workplaceObj =  ErpLocationWorkplace::findOne($planWorkplace->workplace_id);
                        $locationObj =  $workplaceObj->location;
                        if ($locationObj->isProduction())
                        {
                            $register = new ErpLocationWorkplaceRegister();
                            $register->workplace_id = $planWorkplace->workplace_id;
                            $register->user_id = $planWorkplace->user_id;
                            $register->action_type = ErpLocationWorkplaceRegister::TYPE_SEND_FROM;
                            $register->register_at = time();
                            $register->save();

                            $planWorkplace->delete();
                        }
                    }  
                }

                $objWorkplaceOlan = new ErpLocationWorkplacePlan(); 
                $objWorkplaceOlan->user_id = $user_id;
                $objWorkplaceOlan->workplace_id = $strWorkplace; 
                $objWorkplaceOlan->time_from = mktime(6,0,0); 
                $objWorkplaceOlan->time_to = mktime(20,0,0); 
                $objWorkplaceOlan->save();

                //сделать запись о смене событий
                $register = new ErpLocationWorkplaceRegister();
                $register->workplace_id = $strWorkplace;
                $register->user_id = $user_id;
                $register->action_type = ErpLocationWorkplaceRegister::TYPE_SEND_TO;
                $register->register_at = time();
                $register->save();

                return $this->renderPartial('_change_user', [
                    'workplace' => $strWorkplace,
                    'step' => $step,
                ]);  
            } 
        }
    }


}
