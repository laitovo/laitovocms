<?php

namespace backend\modules\laitovo\controllers;

use backend\helpers\ArticleHelper;
use backend\helpers\SchemeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use Yii;
use common\models\laitovo\Cars;
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsFormSearch;
use backend\modules\laitovo\models\CarsScheme;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;

/**
 * CarsController implements the CRUD actions for Cars model.
 */
class CarsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            // [
            //     'class' => 'yii\filters\PageCache',
            //     'only' => ['view'],
            //     'duration' => 60,
            //     'variations' => [
            //         Yii::$app->request->get('id'),
            //         Yii::$app->session->get('CarsFormUpdate'.Yii::$app->request->get('id')),
            //     ],
            //     'dependency' => [
            //         'class' => 'yii\caching\DbDependency',
            //         'sql' => 'SELECT MAX(updated_at) FROM laitovo_cars where id='.intval(Yii::$app->request->get('id')),
            //     ],
            // ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['delete','test'],
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            #Ограничиваю доступ к отчету для пользователей
                            $users = [21,2,14,13];
                            if ($action->id == 'report' && !in_array(Yii::$app->user->getId(),$users))
                                return false;
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionTest($limit)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");
        $off=[34,71,670,455,642,643,648,776,784,791,149,1156,1172,1191,1192,1166,1320,1319,1321,1323,1322,1324,1325,1326,1327,1328,1329,1330,1331,1336,1337,1338,1339,1340,1341,1342,1343,1344,1346,1347,1348,1349,1350,1352,1354,1355,1356,1357,1358,1359,1361,1362,1363,1364,1365,1366,1367,1368,1369,1370,1371,1372,1373,1374,1375,1376,1377,1380,1381,1382,1383,1384,1385,1386,1387,1388,1389,1390,1392,1395,1396,1397,1398,1399,1401,1404,1405,1406,1407,1408,1409,1410,1414,1415,1416,1418,1419,1420,1421,1424,1425,1426,1427,1428,1429,1431,1432,1433,1435,1438,1440,1441,1448,1451,1452,1456,1457,1461,1486,1487,1489,1490,1483,1494,1500,1501,1502,1503,1504,1505,1506,1507,1508,1509,1511,1512];
        for ($i=1+$limit*100; $i <= 100*$limit+100; $i++) { 
            $model=new CarsForm($i);
            if (!$model->auto->acts && !in_array($model->auto->article, $off)){
                $model->scenario = 'step1';
                $model->SCENARIO_STEP_FINISH=$model->scenario;
                if($model->save()){
                    // echo $i.'-ok<br>';
                } else{
                    echo $i.'-error<br>';
                    var_dump($model->getErrors());
                    echo '<br>';
                }
            }
        }
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;

        $hideAr = Yii::$app->request->post("hide");
        //Поверяем была ли засабмичена форма.
        if ($hideAr) {
            if (isset($hideAr['value'])) {
                // добавление новой куки в HTTP-ответ
                $session->set('hidelekalo',true);
            }else{
                $session->remove('hidelekalo');
            }
        }

        $hide = $session->get('hidelekalo', false);

        $query = Cars::find();
        $cars = new Cars();

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or',
                ['like', 'name', Yii::$app->request->get('search')],
                // ['id'=>Yii::$app->request->get('search')],
                ['article'=>Yii::$app->request->get('search')],
            ]);
        }

        $articlesearch = Yii::$app->request->post("articlesearch");
        //Поверяем была ли засабмичена форма.
        if ($articlesearch) {
            $query->andFilterWhere(['article'=>$articlesearch]);
        }

        $lekalo = Yii::$app->request->post("lekalosearch");
        //Поверяем была ли засабмичена форма.

        $dataProvider = new ArrayDataProvider([
            'allModels' => $cars->carsAllArray($query,$hide,$lekalo),
            'sort' => [
                'attributes' => ['name', 'dlinapb1','nomerlekala','typekrep','category','carcass'],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => new CarsForm,
            'checked' => $hide,
        ]);
    }

    public function actionInstruction()
    {
        $model = Yii::$app->request->post('instruction');
        $uploader = Yii::$app->fileManager;
        $key = $uploader->uploadPublicFile('myfile',"/instructons/".$model['id']);

        return $this->redirect(['view', 'id' => $model['id']]);
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionCheck()
    {
        $query = Cars::receiveNotCheckedCarsQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
                // ['id'=>Yii::$app->request->get('search')],
                ['article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('check', [
            'dataProvider' => $dataProvider,
            'model' => new CarsForm,
        ]);
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionCheckRework()
    {
        $query = Cars::receiveNotCheckedReworkCarsQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
                // ['id'=>Yii::$app->request->get('search')],
                ['article'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('check', [
            'dataProvider' => $dataProvider,
            'model' => new CarsForm,
        ]);
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionChangeCheck($id)
    {
        $car = Cars::findOne($id);
        $car->checked = 1;
        $car->checked_date = time();
        $car->checked_author = Yii::$app->user->getId();
        $car->save();
        return $this->redirect(['view','id'=>$car->id]);
    }


    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionChangeCheckRework($id)
    {
        $message = [];
        $message = Yii::$app->request->post('message');
        $message = implode(', ', $message ? $message : []);
        $car = Cars::findOne($id);
        if ($car->checked == 1 || !$message)
            return $this->redirect(['view','id'=>$car->id]);   
        $car->checked = 2;
        $json = Json::decode($car->json);
        $json['reworkmsg'] = $message;
        $car->json = Json::encode($json);
        $car->save();
        return $this->redirect(['view','id'=>$car->id]);
    }

    /**
     * Creates a new Cars model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarsForm();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update','id'=>$model->auto->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Cars model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$step=0,$elem=null)
    {
        $model=new CarsForm($id);
        $model->elem = $elem;
        if (Yii::$app->request->get('steps')){
            $model->scenario = 'step'.Yii::$app->request->get('steps')[$step-1];
            $model->SCENARIO_STEP_FINISH='step'.Yii::$app->request->get('steps')[count(Yii::$app->request->get('steps'))-1];
        } else {
            $model->scenario = 'step'.$step;
            $model->SCENARIO_STEP_FINISH=$model->scenario;
        }

        if ($load=Yii::$app->session->get('CarsFormUpdate'.$id)){
            $model->attributes=$load;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view','id'=>$id]);
        } else {

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                Yii::$app->session->set('CarsFormUpdate'.$id,$model->attributes);
                if (Yii::$app->request->get('steps')){
                    return $this->redirect(ArrayHelper::merge(['update','id'=>$id,'step'=>$step+1,'elem'=>$elem],['steps'=>Yii::$app->request->get('steps')]) );
                } else {
                    return $this->redirect(['update','id'=>$id,'step'=>$step+1,'elem'=>$elem] );
                }
            }

            if ($step==0){

                return $this->redirect(['view','id'=>$id]);

            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionView($id)
    {
        $model=new CarsForm($id);

        Yii::$app->session->remove('CarsFormUpdate'.$id);

        $clipsProvider = new ActiveDataProvider([
            'query' => $model->auto->getClips(),
        ]);

        $analogsProvider = new ActiveDataProvider([
            'query' => $model->auto->getAnalogs(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'clipsProvider' => $clipsProvider,
            'analogsProvider' => $analogsProvider,
        ]);
    }

    public function actionScheme($car,$brand,$type,$type_clips,$window)
    {
        $model=new CarsForm($car);
        $scheme= new CarsScheme();
        $scheme->model=$model;
        $scheme->brand=$brand;
        $scheme->type=$type;
        $scheme->type_clips=$type_clips;
        $scheme->window=$window;
        return $scheme->render();
    }

    /**
     * Deletes an existing Cars model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=new CarsForm($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionReport()
    {
        if (Yii::$app->request->isPost && Yii::$app->request->post())
        {
            $post = Yii::$app->request->post();
            //Здесь сохраняем модель
            $model = Cars::find()->where(['id' => $post['id']])->one();
            $json = Json::decode($model->json);

            $json['reload'] = isset($post['reload']) ? $post['reload'] : 0;
            if (isset($post['reload'])) $json['reload_manager'] =  Yii::$app->user->getId();
            if (isset($post['reload'])) $json['reload_time'] =  time();

            $json['fw'] = isset($post['fw']) ? $post['fw'] : 0;
            $json['fv'] = isset($post['fv']) ? $post['fv'] : 0;
            $json['fd'] = isset($post['fd']) ? $post['fd'] : 0;
            $json['rd'] = isset($post['rd']) ? $post['rd'] : 0;
            $json['rv'] = isset($post['rv']) ? $post['rv'] : 0;
            $json['bw'] = isset($post['bw']) ? $post['bw'] : 0;

            $json['_fw']['status'] = isset($post['fw']) ? $post['fw'] : 0;
            
            $json['_fv']['laitovo']['standart']['status']  =isset($post['fv']) ?  $post['fv'] : 0 ;
            $json['_fv']['laitovo']['dontlookb']['status'] =isset($post['fv']) ?  $post['fv'] : 0 ;
            $json['_fv']['chiko']['standart']['status']    =isset($post['fv']) ?  $post['fv'] : 0 ;
            $json['_fv']['chiko']['dontlookb']['status']   =isset($post['fv']) ?  $post['fv'] : 0 ;
            $json['_fd']['moscitka']['standart']['status']  =isset($post['fd_laitovo_standart_status'])  ?  $post['fd_laitovo_standart_status'] : 0 ;

            $json['_fd']['laitovo']['standart']['status']  =isset($post['fd_laitovo_standart_status'])  ?  $post['fd_laitovo_standart_status'] : 0 ;
            $json['_fd']['laitovo']['short']['status']     =isset($post['fd_laitovo_short_status'])     ?  $post['fd_laitovo_short_status'] : 0 ;
            $json['_fd']['laitovo']['smoke']['status']     =isset($post['fd_laitovo_smoke_status'])     ?  $post['fd_laitovo_smoke_status'] : 0 ;
            $json['_fd']['laitovo']['mirror']['status']    =isset($post['fd_laitovo_mirror_status'])    ?  $post['fd_laitovo_mirror_status'] : 0 ;
            $json['_fd']['laitovo']['dontlooks']['status'] =isset($post['fd_laitovo_dontlooks_status']) ?  $post['fd_laitovo_dontlooks_status'] : 0 ;
            $json['_fd']['laitovo']['dontlookb']['status'] =isset($post['fd_laitovo_dontlookb_status']) ?  $post['fd_laitovo_dontlookb_status'] : 0 ;
            $json['_fd']['chiko']['standart']['status']    =isset($post['fd_chiko_standart_status'])    ?  $post['fd_chiko_standart_status'] : 0 ;
            $json['_fd']['chiko']['short']['status']       =isset($post['fd_chiko_short_status'])       ?  $post['fd_chiko_short_status'] : 0 ;
            $json['_fd']['chiko']['smoke']['status']       =isset($post['fd_chiko_smoke_status'])       ?  $post['fd_chiko_smoke_status'] : 0 ;
            $json['_fd']['chiko']['mirror']['status']      =isset($post['fd_chiko_mirror_status'])      ?  $post['fd_chiko_mirror_status'] : 0 ;
            $json['_fd']['chikomagnet']['status']          =isset($post['fd_chikomagnet_status'])       ?  $post['fd_chikomagnet_status'] : 0 ;
            $json['_fd']['laitovowithmagnets']['standart']['status']         =isset($post['fd_laitovowithmagnets_standart_status'])       ?  $post['fd_laitovowithmagnets_standart_status'] : 0 ;
            
            $json['_fd']['moscitka']['standart']['status']  =isset($post['rd_laitovo_standart_status'])  ?  $post['rd_laitovo_standart_status'] : 0 ;
            
            $json['_rd']['laitovo']['standart']['status']  =isset($post['rd_laitovo_standart_status'])  ?  $post['rd_laitovo_standart_status'] : 0 ;
            $json['_rd']['laitovo']['dontlooks']['status'] =isset($post['rd_laitovo_dontlooks_status']) ?  $post['rd_laitovo_dontlooks_status'] : 0 ;
            $json['_rd']['laitovo']['dontlookb']['status'] =isset($post['rd_laitovo_dontlookb_status']) ?  $post['rd_laitovo_dontlookb_status'] : 0 ;
            //ЗШ чико устанавливаем по лайтово кроме магнета
            $json['_rd']['chiko']['standart']['status']    =isset($post['rd_laitovo_standart_status'])  ?  $post['rd_laitovo_standart_status'] : 0 ;
            
            $json['_rd']['chikomagnet']['status']          =isset($post['rd_chikomagnet_status'])       ?  $post['rd_chikomagnet_status'] : 0 ;
            $json['_rd']['laitovowithmagnets']['standart']['status'] =isset($post['rd_laitovowithmagnets_standart_status'])       ?  $post['rd_laitovowithmagnets_standart_status'] : 0 ;
            
            $json['_rv']['laitovo']['standart']['status']  =isset($post['rv_laitovo_standart_status'])  ?  $post['rv_laitovo_standart_status'] : 0 ;
            $json['_rv']['laitovo']['dontlooks']['status'] =isset($post['rv_laitovo_dontlooks_status']) ?  $post['rv_laitovo_dontlooks_status'] : 0 ;
            $json['_rv']['laitovo']['dontlookb']['status'] =isset($post['rv_laitovo_dontlookb_status']) ?  $post['rv_laitovo_dontlookb_status'] : 0 ;
            
            $json['_rv']['chiko']['standart']['status']    =isset($post['rv_laitovo_standart_status'])  ?  $post['rv_laitovo_standart_status'] : 0 ;
            $json['_rv']['chiko']['dontlooks']['status']   =isset($post['rv_laitovo_dontlooks_status']) ?  $post['rv_laitovo_dontlooks_status'] : 0 ;
            $json['_rv']['chiko']['dontlookb']['status']   =isset($post['rv_laitovo_dontlookb_status']) ?  $post['rv_laitovo_dontlookb_status'] : 0 ;
            
            $json['_bw']['laitovo']['standart']['status']  =isset($post['bw_laitovo_standart_status'])  ?  $post['bw_laitovo_standart_status'] : 0 ;
            $json['_bw']['laitovo']['dontlooks']['status'] =isset($post['bw_laitovo_dontlooks_status']) ?  $post['bw_laitovo_dontlooks_status'] : 0 ;
            $json['_bw']['chiko']['standart']['status']    =isset($post['bw_laitovo_standart_status'])  ?  $post['bw_laitovo_standart_status']  : 0 ;
            $json['_bw']['chiko']['dontlookb']['status']   =isset($post['bw_chiko_dontlookb_status'])   ?  $post['bw_chiko_dontlookb_status'] : 0 ;


            $model->json = Json::encode($json);
            $model->save();
        }

        $search =  new CarsFormSearch();
        $dataProvider = $search->search(Yii::$app->request->queryParams);

        return $this->render('_report', [
            'dataProvider' => $dataProvider,
            'model' => new CarsForm,
            'searchmodel' => $search,
        ]);
    }

    public function actionBlanketEdit()
    {
        $session = Yii::$app->session;
        $query = Cars::find();
        $cars = new Cars();

        $validator = new \yii\validators\StringValidator();

        $search = Yii::$app->request->get("search");
        $emptyBlanket = Yii::$app->request->get("emptyBlanket");

        $idsYes = Yii::$app->request->post('idsYes')??[];
        if (empty($idsYes)) {
            $idsYes = $session->get('BlanketIdsYes',[]);
        }
        $idsToAdd = Yii::$app->request->post('idsToAdd')??[];
        $idsYes = ArrayHelper::merge($idsYes,$idsToAdd);


        $idsToRemove = Yii::$app->request->post('idsToRemove')??[];
        $idsYes = array_diff($idsYes,$idsToRemove);

        $session->set('BlanketIdsYes',$idsYes);

        if ($validator->validate($search, $error)) {

            $query->andFilterWhere(
                ['like', 'name', $search]
                // ['id'=>Yii::$app->request->get('search')],
            );
        }

        $dataProviderYes = new ActiveDataProvider([
            'query' => Cars::find()->where(['in','id',$idsYes]),
            'sort' => [
                'attributes' => ['name', 'blanket_default'],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if ($emptyBlanket) {
            $query = Cars::find()->where(['not in','id',$idsYes])->andWhere(['blanket_default' => null])->andFilterWhere(['like', 'name', $search]);
        } else {
            $query = Cars::find()->where(['not in','id',$idsYes])->andFilterWhere(['like', 'name', $search]);
        }
        $dataProviderNo = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['name', 'blanket_default'],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        return $this->render('blanket_edit', [
            'dataProviderNo' => $dataProviderNo,
            'dataProviderYes' => $dataProviderYes,
            'model' => new CarsForm,
            'searchModel' => new Cars(['name' => $search]),
            'idsYes'      => $idsYes,
        ]);
    }

    public function actionSaveBlanket()
    {
        $idsYes = Yii::$app->request->post('idsYes')??[];
        $blanket = Yii::$app->request->post("Blanket");

        if (!$blanket || empty($idsYes)) {
            Yii::$app->session->setFlash('error','Не указаны автомобили, либо номер одеяла');
            return $this->redirect('blanket-edit');
        }

        $session = Yii::$app->session;
        $session->set('BlanketIdsYes',[]);

        $cars = Cars::find()->where(['in','id',$idsYes])->all();
        foreach ($cars as $car) {
            $car->blanket_default = $blanket;
            $car->save();
        }

        Yii::$app->session->setFlash('success','Для всех автомобилей успешно проставлено ' . $blanket);

        return $this->redirect('blanket-edit');
    }

    /**
     * @return string Отчет ТОП ЛЕКАЛ
     */
    public function actionTemplateReport()
    {
        $result = [];
        $date = mktime(0, 0, 0, date('n')-9, 1, date('y'));
        $articles = ErpNaryad::find()->select('article')
            ->where([' >','created_at',$date])
            ->andWhere(['not like','article','OT-%'])
            ->andWhere(['status' => ErpNaryad::STATUS_READY])->column();

        $searchStr = implode(' ',$articles);

        $matches = [];
        preg_match_all('/(FW|FV|FD|RD|RV|BW)-\w+-(\d+)-\d+-\d+/',$searchStr,$matches);
        foreach ($matches[2] as $carArticle) {
            isset($result[$carArticle]) ? $result[$carArticle]++ : $result[$carArticle] = 1;
        }

        $result2 = [];
        foreach ($result as $carArticle => $count) {
            $carObj = Cars::find()->where(['article' => $carArticle])->one();
            if (!$carObj) continue;
            isset($result2[$carObj->templateNumber]) ? $result2[$carObj->templateNumber] += $count : $result2[$carObj->templateNumber] = $count;
        }

        arsort($result2);

        $result3 = [];
        $pos = 0;
        foreach ($result2 as $lekalo => $count) {
            $car = [
                'carName' => Cars::find()->where(['templateNumber' => $lekalo])->select('name')->column(),
                'lekalo' => $lekalo,
                'pos'    => ++$pos
            ];
            $result3[] = $car;
        }

        $provider = new ArrayDataProvider([
            'allModels' => $result3,
            'sort' => [
                'attributes' => ['carName', 'lekalo','count'],
            ],
        ]);

        return $this->render('template_report',['provider' => $provider]);
    }
}

