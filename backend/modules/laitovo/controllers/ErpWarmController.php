<?php

namespace backend\modules\laitovo\controllers;

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\models\AdditionalProduct;
use backend\modules\laitovo\models\MoveActTerminal;
use backend\modules\laitovo\models\ProdGroupTerminal;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use common\models\laitovo\ErpNaryadLog;
use core\logic\DistributionProductionLiterals;
use core\logic\UpnGroup;
use core\models\article\Article;
use core\services\SParentUpn;
use core\services\SProductionLiteral;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\modules\laitovo\models\ErpOrder;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpTerminal;
use backend\modules\laitovo\models\ErpMainTerminal;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpTextileReport;
use backend\modules\laitovo\models\ErpLocationWorkplace;
use backend\modules\laitovo\models\ErpLocationWorkplacePlan;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;
use backend\modules\laitovo\models\ErpLogisticReport;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Config;

/**
 * Default controller for the `laitovo erp` module
 */
class ErpWarmController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            #Ограничиваю доступ к отчету для пользователей
                            $users = [40,41,25];
                            if ($action->id == 'index' && in_array(Yii::$app->user->getId(),$users)) {
                                return false;
                            }
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        ErpLocationWorkplace::withdrawActivity(); 
        #Для получения статистики мы должны получить массив следующего вида
        $logisticsReport = new ErpLogisticReport();
        $model = new ErpTextileReport();
        $addProduct = new AdditionalProduct();

        if ($model->load(Yii::$app->request->post())) {
            $plantcontrol = new ActiveDataProvider([
                'query' => ErpLocation::find()
                    ->where(['<>','id',Yii::$app->params['erp_dispatcher']])
                    ->andWhere(['<>','id',Yii::$app->params['erp_label']])
                    ->andWhere(['type' => ErpLocation::TYPE_PRODUCTION])
                    ->andWhere(['id' => [20,21,22,23,24,6]])
                    ->orderBy('sort ASC'),
            ]);


            $logisticsData = $logisticsReport->getData($model->dateFrom,$model->dateTo);

            return $this->render('index', [
                'plantcontrol' => $plantcontrol,
                'model' => $model,
                'logisticsData' =>  $logisticsData,
                'addProduct' =>  $addProduct,
            ]);
        }else{
            $model->dateFrom = mktime(0,0,0);
            $model->dateTo = mktime(0,0,0);
            $plantcontrol = new ActiveDataProvider([
                'query' => ErpLocation::find()
                    ->where(['<>','id',Yii::$app->params['erp_dispatcher']])
                    ->andWhere(['<>','id',Yii::$app->params['erp_label']])
                    ->andWhere(['id' => [20,21,22,23,24,6]])
                    ->andWhere(['type' => ErpLocation::TYPE_PRODUCTION])
                    ->orderBy('sort ASC'),
            ]);

            $logisticsData = $logisticsReport->getData($model->dateFrom,$model->dateTo);

            return $this->render('index', [
                'plantcontrol' => $plantcontrol,
                'model' => $model,
                'logisticsData' =>  $logisticsData,
                'addProduct' =>  $addProduct,
            ]);
        }
    }
}
