<?php

namespace backend\modules\admin;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\admin\controllers';

    public function init()
    {
		\Yii::$app->layout='2columns';
        parent::init();

        // custom initialization code goes here
    }
}
