<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * This is the model class for table "team".
 */
class Team extends \common\models\team\Team
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['json'], 'string'],
            [['name', 'image'], 'string', 'max' => 255]
        ];
    }
}
