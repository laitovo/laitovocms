<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 */
class User extends \common\models\user\User
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['website_id'], 'integer'],
            [['subscribe', 'status'], 'boolean'],
            [['json'], 'string'],
            ['email', 'email'],
            [['name', 'username', 'email', 'phone', 'role', 'image', 'language', 'timezone'], 'string', 'max' => 255]
        ];
    }

}
