<?php

namespace backend\modules\admin\controllers;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use core\models\realizationLog\RealizationLog;
use returnModule\core\models\ReturnJournal;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UpnController implements the CRUD actions for Team model.
 */
class UpnController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['terminal','search'],
                        'allow' => true,
                        'roles' => ['readTeam'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionTerminal()
    {
        return $this->render('terminal');
    }

    public function actionSearch($upn)
    {
        //Основная логика работы с введенным занечением
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /**
         * Получение информации о UPN
         */
        //1. Проверяем, существует ли вообще такой UPN в таблице
        /** @var $upn Naryad */
        $upn = Naryad::find()->where(['id' => $upn])->one();
        if (!$upn) {
            $response['status'] = 'error';
            $response['message'] = 'Данного UPN не существует в базе!!!';
            $response['html'] = 'Данного UPN не существует в базе !!!';
            return $response;
        }

        //2. Проверяем, не является ли это родительским UPN (ParentUpn)
        if ($upn->isParent) {
            $upns = $upn->children;
        } else {
            $upns = [$upn];
        }

        $data = [];
        foreach ($upns as $item) {
            $data[$item->id]['upn'] = $item;
            $data[$item->id]['returns'] = ReturnJournal::find()->where(['or',
                ['upn' => $item->id],
                ['result_upn_id' => $item->id]
            ])->all();
            $data[$item->id]['realizations'] = RealizationLog::find()->where(['upnId' => $item->id])->all();
            $data[$item->id]['workOrder'] = ErpNaryad::find()->where(['logist_id' => $item->id])->one();;
        }

        $response = [];
        $response['upn'] = $upn;
        $response['status'] = 'success';
        $response['message'] = 'Информация о UPN успешно получена.';
        $response['html'] = $this->renderPartial('_terminal_answer', [
            'data' => $data
        ]);

        return $response;
    }


}
