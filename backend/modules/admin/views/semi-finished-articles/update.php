<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model core\models\semiFinishedArticles\SemiFinishedArticles */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Semi Finished Articles',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Semi Finished Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->render('@backendViews/admin/views/menu');
?>
<div class="semi-finished-articles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
