<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model core\models\semiFinishedArticles\SemiFinishedArticles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="semi-finished-articles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'carArticle')->textInput() ?>

    <?= $form->field($model, 'mask')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'literal')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
