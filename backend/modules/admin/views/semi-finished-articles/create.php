<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model core\models\semiFinishedArticles\SemiFinishedArticles */

$this->title = Yii::t('app', 'Create Semi Finished Articles');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Semi Finished Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/admin/views/menu');
?>
<div class="semi-finished-articles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
