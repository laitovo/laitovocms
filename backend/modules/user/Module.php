<?php

namespace backend\modules\user;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\user\controllers';

    public function init()
    {
		\Yii::$app->layout='2columns';
        parent::init();

        // custom initialization code goes here
    }
}
