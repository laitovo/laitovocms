<?php

namespace backend\modules\order\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\Region;
use common\models\team\Team;
use yii\web\NotFoundHttpException;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;
use common\models\team\Organization;
use yii\helpers\ArrayHelper;

/**
 * team
 */
class ClientForm extends Model
{

    public $name;
    public $country;
    public $region;
    public $city;
    public $organization;
    public $organization_fields;
    public $discount;
    public $fields=[];

    public $team;
    private $_json=[];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['region','country','city'], 'number'],
            [['discount'], 'number','min'=>0,'max'=>100],
            [['name'], 'string', 'max' => 255],
            [['organization'], 'string'],
            ['organization', 'default', 'value' => '[]'],
            [['fields'], 'each','rule' => ['string']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название'),
            'organization' => Yii::t('app', 'Реквизиты организации'),
            'country' => Yii::t('app', 'Страна'),
            'region' => Yii::t('app', 'Регион'),
            'city' => Yii::t('app', 'Город'),
            'discount' => Yii::t('app', 'Скидка (%)'),
        ];
    }

    public function __construct($team_id=null, $config = [])
    {
        $_org=new Organization;
        $this->organization_fields=$_org->fields;

        if ($team_id===null){

            $this->team=new Team;

            foreach ($this->team->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=null;
            }

        } elseif ( ($this->team=Yii::$app->team->getTeam()->getClients()->andWhere(['id'=>$team_id])->one())!==null ) {

            $this->name=$this->team->name;
            $this->discount=$this->team->discount;
            $this->_json=Json::decode($this->team->json ? $this->team->json : '{}');

            $this->country=isset($this->_json['country']) ? $this->_json['country'] : null;
            $this->region=isset($this->_json['region']) ? $this->_json['region'] : null;
            $this->city=isset($this->_json['city']) ? $this->_json['city'] : null;

            foreach ($this->team->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=isset($this->_json[$value['fields']['id']['value']]) ? $this->_json[$value['fields']['id']['value']] : '';
            }

            $organization=[];
            if ($this->team->organizations){
                foreach ($this->team->organizations as $row) {
                    $orginfo=Json::decode($row->json ? $row->json : '{}');
                    $org=[];
                    foreach ($row->fields as $key => $value) {
                        $org[$value['fields']['id']['value']]=isset($orginfo[$value['fields']['id']['value']]) ? $orginfo[$value['fields']['id']['value']] : '';
                    }
                    $organization[]=ArrayHelper::merge([
                        'id'=>$row->id,
                        'name'=>$row->name,
                    ],$org);
                }
            }
            $this->organization=Json::encode($organization);

        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }


    public function save()
    {
        if ($this->validate()) {
            $team = $this->team;

            $team->name=$this->name;
            $team->discount=$this->discount;

            $json = $this->_json;

            $json['country']=(int)$this->country;
            $json['region']=(int)$this->region;
            $json['city']=(int)$this->city;

            foreach ($team->fields as $key => $value) {
                $json[$value['fields']['id']['value']]=isset($this->fields[$value['fields']['id']['value']]) ? $this->fields[$value['fields']['id']['value']] : '';
            }

            $team->json=Json::encode($json);

            if ($team->save()) {

                if ($this->organization){
                    $organization=Json::decode($this->organization);
                    foreach ($organization as $row) {
                        if (!($row['id'] && ($org=Organization::findOne($row['id']))!==null && $org->team_id==$team->id))
                            $org=new Organization;
                        $org->team_id=$team->id;
                        $org->name=$row['name'];
                        $orginfo=Json::decode($org->json ? $org->json : '{}');

                        foreach ($org->fields as $key => $value) {
                            $orginfo[$value['fields']['id']['value']]=isset($row[$value['fields']['id']['value']]) ? $row[$value['fields']['id']['value']] : '';
                        }

                        $org->json=Json::encode($orginfo);

                        if ($org->save())
                            $itemsid[$org->id]=$org->id;
                    }
                    if ($team->organizations){
                        foreach ($team->organizations as $row) {
                            if (!isset($itemsid[$row->id])){
                                $row->delete();
                            }
                        }
                    }
                }
                return true;

            }

        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->team->delete();
    }
}
