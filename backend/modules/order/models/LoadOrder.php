<?
namespace backend\modules\order\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use common\models\order\Order;
use common\models\order\Item;
use common\models\order\Product;

class LoadOrder extends Model
{
    public $file;
    public $order_id;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'csv, txt'],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $handle = @fopen($this->file->tempName, "r");
            if ($handle) {
                $order=new Order;
                $order->team_id=Yii::$app->team->id;
                $order->save();
                $this->order_id=$order->id;

                while (!feof($handle)) {
                    $line = explode(';', fgets($handle));
                    if ($line && count($line)==3){
                        $item=new Item;
                        $item->order_id=$order->id;
                        $item->name=$line[0];
                        if (($product=Product::find()->where(['article'=>$line[0],'team_id'=>Yii::$app->team->id])->one())!==null){
                            $item->product_id=$product->id;
                            $item->name=$product->name;
                        }
                        $item->price=$line[1];
                        $item->quantity=$line[2];
                        $item->save();
                    }
                }
                fclose($handle);
                return true;
            } else {
                return false;
            }
        } else {
            foreach ($this->getErrors() as $key => $value) {
                Yii::$app->session->setFlash('error', $value[0]);
            }
            return false;
        }
    }
}