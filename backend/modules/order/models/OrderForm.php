<?php

namespace backend\modules\order\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use common\models\order\Order;
use common\models\order\Item;
use common\models\order\Product;
use yii\web\NotFoundHttpException;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;

/**
 * Order
 */
class OrderForm extends Model
{

    public $items='[]';
    public $organization;
    public $user;
    public $clientteam;
    public $clientorganization;
    public $manager;
    public $country;
    public $region;
    public $city;
    public $fields=[];
    public $discount;
    public $currency='RUB';
    public $nds=18;
    public $nds_in=0;
    public $cur=[
        'RUB'=>'RUB',
        'EUR'=>'EUR',
        'USD'=>'USD',
        'KZT'=>'KZT',
    ];

    public $order=null;
    private $_json=[];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nds'], 'number', 'min'=>0],
            [['nds_in'], 'boolean'],
            [['items','currency'], 'string'],
            [['organization','user','clientorganization', 'clientteam', 'manager', 'region','country','city'], 'number'],
            [['discount'], 'number','min'=>0,'max'=>100],
            ['organization', 'in', 'range' => ArrayHelper::getColumn($this->order->team->organizations,'id')],
            ['manager', 'in', 'range' => ArrayHelper::getColumn($this->order->team->users,'id')],

            ['items', 'default', 'value' => '[]'],
            [['items'], 'validateItems'],
            [['fields'], 'each','rule' => ['string']],
        ];
    }

    public function validateItems($attribute, $params)
    {
        $number = new NumberValidator();
        $string = new StringValidator();
        $required = new RequiredValidator();

        if (!$this->hasErrors()) {
            if ($this->items){
                try {
                    $items=Json::decode($this->items);
                    if ($items){
                        foreach ($items as $row) {
                            if (($number->validate($row['id'], $error) || $row['id']==null)
                                && ($number->validate($row['product_id'], $error) || $row['product_id']==null)
                                && ($number->validate($row['retail'], $error) || $row['retail']==null)
                                && ($required->validate($row['price'], $error) && $row['price']>=0)
                                && $number->validate($row['price'], $error) 
                                && ($required->validate($row['quantity'], $error) && $row['quantity']>0) 
                                && $number->validate($row['quantity'], $error) 
                                && $required->validate($row['name'], $error) 
                                && $string->validate($row['name'], $error) 
                                && ($string->validate($row['properties'], $error) || $row['properties']==null) ) {
                            } else {
                                $this->addError($attribute, Yii::t('yii', 'Invalid data received for parameter "{param}".', ['param'=>$this->getAttributeLabel($attribute)]));
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $this->addError($attribute, Yii::t('yii', 'Invalid data received for parameter "{param}".', ['param'=>$this->getAttributeLabel($attribute)]));
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('app', 'Номер заказа'),
            'created_at' => Yii::t('app', 'Дата заказа'),
            'items' => Yii::t('app', 'Позиции заказа'),
            'organization' => Yii::t('app', 'Организация - поставщик'),
            'user' => Yii::t('app', 'Покупатель (физ. лицо)'),
            'clientteam' => Yii::t('app', 'Контрагент'),
            'clientorganization' => Yii::t('app', 'Организация - покупатель'),
            'manager' => Yii::t('app', 'Менеджер'),
            'country' => Yii::t('app', 'Страна'),
            'region' => Yii::t('app', 'Регион'),
            'city' => Yii::t('app', 'Город'),
            'discount' => Yii::t('app', 'Скидка (%)'),
            'nds' => Yii::t('app', 'Ставка НДС (%)'),
            'nds_in' => Yii::t('app', 'НДС включен в цену'),
            'currency' => Yii::t('app', 'Валюта'),
            'quantity' => Yii::t('app', 'Кол-во'),
            'sum' => Yii::t('app', 'Сумма без НДС'),
            'sum1' => Yii::t('app', 'Сумма'),
        ];
    }

    public function __construct($order_id=null, $config = [])
    {
        if ($order_id===null){

            $this->order=new Order;
            $this->order->team_id=Yii::$app->team->id;
            $this->manager=Yii::$app->user->id;

            foreach ($this->order->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=null;
            }

        } elseif ( ($this->order=Order::find()->where(['id'=>$order_id,'team_id'=>Yii::$app->team->id])->one())!==null ) {

            $this->organization=$this->order->organization_id;
            $this->user=$this->order->user_id;
            $this->clientteam=$this->order->client_team_id;
            $this->clientorganization=$this->order->client_organization_id;
            $this->manager=$this->order->manager_id;
            $this->discount=$this->order->discount;

            $this->_json=Json::decode($this->order->json ? $this->order->json : '{}');

            $this->region=isset($this->_json['region']) ? $this->_json['region'] : null;
            $this->country=isset($this->_json['country']) ? $this->_json['country'] : null;
            $this->city=isset($this->_json['city']) ? $this->_json['city'] : null;
            $this->currency=isset($this->_json['currency']) ? $this->_json['currency'] : null;
            $this->nds=isset($this->_json['nds']) ? $this->_json['nds'] : null;
            $this->nds_in=isset($this->_json['nds_in']) ? $this->_json['nds_in'] : null;

            $items=[];
            if ($this->order->items){
                foreach ($this->order->items as $row) {
                    $items[]=[
                        'id'=>$row->id,
                        'product_id'=>$row->product_id,
                        'name'=>$row->name,
                        'price'=>$row->price,
                        'retail'=>$row->retail,
                        'quantity'=>$row->quantity,
                        'properties'=>$row->properties,
                        'lock'=>(int)$row->lock,
                    ];
                }
            }
            $this->items=Json::encode($items);

            foreach ($this->order->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=isset($this->_json[$value['fields']['id']['value']]) ? $this->_json[$value['fields']['id']['value']] : '';
            }

        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $order = $this->order;
            $order->organization_id = $this->organization;
            $order->user_id = $this->user;
            $order->client_team_id = $this->clientteam;
            $order->client_organization_id = $this->clientorganization;
            $order->manager_id = $this->manager;
            $order->discount = $this->discount;
            $json = $this->_json;

            $json['region']=(int)$this->region;
            $json['country']=(int)$this->country;
            $json['city']=(int)$this->city;
            $json['currency']=$this->currency;
            $json['nds']=(float)$this->nds;
            $json['nds_in']=(int)$this->nds_in;
            $order->region_id = $this->city ? : ($this->region ? : ($this->country ? : null));

            foreach ($order->fields as $key => $value) {
                $json[$value['fields']['id']['value']]=isset($this->fields[$value['fields']['id']['value']]) ? $this->fields[$value['fields']['id']['value']] : '';
            }

            $order->json=Json::encode($json);

            if ($order->save()){
                if ($this->items){
                    $items=Json::decode($this->items);
                    foreach ($items as $row) {
                        if (!($row['id'] && ($item=Item::findOne($row['id']))!==null && $item->order_id==$order->id))
                            $item=new Item;
                        $item->order_id=$order->id;
                        $item->product_id=null;
                        if ($row['product_id'] && ($product=Product::findOne($row['product_id']))!==null && $product->team_id==$order->team_id)
                            $item->product_id=$row['product_id'];
                        $item->name=$row['name'];
                        $item->price=$row['price'];
                        $item->retail=$row['retail'];
                        $item->quantity=$row['quantity'];
                        $item->properties=$row['properties'];
                        $item->lock=isset($row['lock']) ? (int)$row['lock'] : 0;
                        $item->currency=$this->currency;
                        $item->nds=$this->nds;
                        $item->nds_in=$this->nds_in;
                        if ($item->save())
                            $itemsid[$item->id]=$item->id;
                    }
                    if ($order->items){
                        foreach ($order->items as $row) {
                            if (!isset($itemsid[$row->id])){
                                $row->delete();
                            }
                        }
                    }
                }
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->order->delete();
    }
}
