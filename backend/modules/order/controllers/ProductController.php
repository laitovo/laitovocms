<?php

namespace backend\modules\order\controllers;

use Yii;
use backend\modules\order\models\ProductForm;
use common\models\order\Product;
use common\models\order\Category;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('order');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Product::find()->where(['product.team_id'=>Yii::$app->team->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();
        $number = new \yii\validators\NumberValidator();

        if ($validator->validate(Yii::$app->request->get('categories'), $error)) {
            $categories=explode(',', Yii::$app->request->get('categories'));
            if ($categories){
                $parentcat=[];
                foreach ($categories as $key) {
                    if ($number->validate($key, $error) && ($category=Category::find()->where(['id'=>$key,'team_id'=>Yii::$app->team->id])->one())!==null) {
                        $parentcat[$category->parent_id][]=$key;
                    }
                }
                foreach ($parentcat as $key => $value) {
                    $query->innerJoin(['s'.$key => Product::find()->joinWith('categories')->where(['category.id'=>$value])],'product.id=s'.$key.'.id');
                }
            }
        }

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'product.name', Yii::$app->request->get('search')],
                ['like', 'product.article', Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => new ProductForm,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
            return Yii::$app->request->post('redirect') ? $this->redirect([Yii::$app->request->post('redirect')]) : $this->redirect(['update','id'=>$model->product->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new ProductForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
            return Yii::$app->request->post('redirect') ? $this->redirect([Yii::$app->request->post('redirect')]) : $this->refresh();
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = new ProductForm($id);
        $model->delete();

        return $this->redirect(['index']);
    }
}
