<?php

namespace backend\modules\order\controllers;

use Yii;
use backend\modules\order\models\OrderForm;
use backend\modules\order\models\LoadOrder;
use common\models\order\Order;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use common\models\order\Product;
use common\models\order\Category;
use kartik\mpdf\Pdf;
use yii\web\BadRequestHttpException;

/**
 * DefaultController implements the CRUD actions for Order model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('order');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $load=new LoadOrder;

        if (Yii::$app->request->isPost) {
            $load->file = UploadedFile::getInstance($load, 'file');
            if ($load->upload()) {
                return $this->redirect(['update','id'=>$load->order_id]);
            }
        }

        $query = Order::find()->joinWith('items')->select('order.*, sum(quantity) as quantity, sum(price*quantity) as sum')->groupBy('order.id')->where(['team_id'=>Yii::$app->team->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);
        $dataProvider->sort->attributes['sum']=[
            'asc' => ['sum' => SORT_ASC],
            'desc' => ['sum' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['quantity']=[
            'asc' => ['quantity' => SORT_ASC],
            'desc' => ['quantity' => SORT_DESC],
        ];

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['number'=>Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'load' => $load,
            'model' => new OrderForm,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($copy=null)
    {
        $model = new OrderForm();

        if ($copy){
            $copymodel = new OrderForm($copy);
            $model->attributes=$copymodel->attributes;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
            return Yii::$app->request->post('redirect') ? $this->redirect([Yii::$app->request->post('redirect')]) : $this->redirect(['update','id'=>$model->order->id]);
        } else {

            $query = Product::find()->where(['product.team_id'=>Yii::$app->team->id]);

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);
            
            $validator = new \yii\validators\StringValidator();
            $number = new \yii\validators\NumberValidator();

            if ($validator->validate(Yii::$app->request->get('categories'), $error)) {
                $categories=explode(',', Yii::$app->request->get('categories'));
                if ($categories){
                    $parentcat=[];
                    foreach ($categories as $key) {
                        if ($number->validate($key, $error) && ($category=Category::find()->where(['id'=>$key,'team_id'=>Yii::$app->team->id])->one())!==null) {
                            $parentcat[$category->parent_id][]=$key;
                        }
                    }
                    foreach ($parentcat as $key => $value) {
                        $query->innerJoin(['s'.$key => Product::find()->joinWith('categories')->where(['category.id'=>$value])],'product.id=s'.$key.'.id');
                    }
                }
            }
            if ($validator->validate(Yii::$app->request->get('search'), $error)) {

                $query->andFilterWhere(['or', 
                    ['like', 'product.name', Yii::$app->request->get('search')],
                    ['like', 'product.article', Yii::$app->request->get('search')],
                ]);
            }

            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new OrderForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
            return Yii::$app->request->post('redirect') ? $this->redirect([Yii::$app->request->post('redirect')]) : $this->refresh();
        } else {

            $query = Product::find()->where(['product.team_id'=>Yii::$app->team->id]);

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);
            
            $validator = new \yii\validators\StringValidator();
            $number = new \yii\validators\NumberValidator();

            if ($validator->validate(Yii::$app->request->get('categories'), $error)) {
                $categories=explode(',', Yii::$app->request->get('categories'));
                if ($categories){
                    $parentcat=[];
                    foreach ($categories as $key) {
                        if ($number->validate($key, $error) && ($category=Category::find()->where(['id'=>$key,'team_id'=>Yii::$app->team->id])->one())!==null) {
                            $parentcat[$category->parent_id][]=$key;
                        }
                    }
                    foreach ($parentcat as $key => $value) {
                        $query->innerJoin(['s'.$key => Product::find()->joinWith('categories')->where(['category.id'=>$value])],'product.id=s'.$key.'.id');
                    }
                }
            }
            if ($validator->validate(Yii::$app->request->get('search'), $error)) {

                $query->andFilterWhere(['or', 
                    ['like', 'product.name', Yii::$app->request->get('search')],
                    ['like', 'product.article', Yii::$app->request->get('search')],
                ]);
            }

            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionInvoice($id)
    {
        $model = new OrderForm($id);

        try {
            $content = $this->renderPartial('invoice/invoice_'.$model->order->organization->id,[
                'model' => $model,
                'sum' => 0,
                'nds' => 0,
            ]);  
        } catch (\Exception $e) {
            $content = $this->renderPartial('invoice',[
                'model' => $model,
                'sum' => 0,
                'nds' => 0,
            ]);    
        }

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_items { border: 1px solid; border-collapse: collapse;text-align:center;}table.invoice_items td, table.invoice_items th { border: 1px solid;text-align:center;}', 
            'options' => ['title' => 'Счет № '.$model->order->number.' от '.Yii::$app->formatter->asDate($model->order->created_at)],
        ]);

        return $pdf->render(); 
    }

    public function actionTorg12($id)
    {
        $model = new OrderForm($id);

        if (!($model->order->clientTeam && $model->order->clientOrganization)){
            throw new BadRequestHttpException(Yii::t('app', 'Не хватает данных для формирования накладной'));
        }

        try {
            $content = $this->renderPartial('torg12/torg12_'.$model->order->organization->id,[
                'model' => $model,
                'sum' => 0,
                'nds' => 0,
                'count' => 0,
                'total' => 0,
            ]);
        } catch (\Exception $e) {
            $content = $this->renderPartial('torg12',[
                'model' => $model,
                'sum' => 0,
                'nds' => 0,
                'count' => 0,
                'total' => 0,
            ]);
        }

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_LANDSCAPE, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // 'cssInline' => 'body { width: 297mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 12px;}table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }table.invoice_com_items { border: 1px solid; border-collapse: collapse;text-align: center;}table.invoice_com_items td, table.invoice_com_items th { border: 1px solid;}', 
            // 'options' => ['title' => 'Счет № '.$model->order->number.' от '.Yii::$app->formatter->asDate($model->order->created_at)],
        ]);

        return $content; 
        // return $pdf->render(); 
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = new OrderForm($id);
        $model->delete();

        return $this->redirect(['index']);
    }
}
