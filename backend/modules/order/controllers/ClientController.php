<?php

namespace backend\modules\order\controllers;

use Yii;
use backend\modules\order\models\ClientForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ClientController implements the CRUD actions for Team model.
 */
class ClientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('order');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query=Yii::$app->team->getTeam()->getClients();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => new ClientForm,
        ]);
    }

    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ClientForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->team->team->link('clients', $model->team);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
            return Yii::$app->request->post('redirect') ? $this->redirect([Yii::$app->request->post('redirect')]) : $this->redirect(['update','id'=>$model->team->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Team model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new ClientForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
            return Yii::$app->request->post('redirect') ? $this->redirect([Yii::$app->request->post('redirect')]) : $this->refresh();
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = new ClientForm($id);
        $model->delete();

        return $this->redirect(['index']);
    }
}
