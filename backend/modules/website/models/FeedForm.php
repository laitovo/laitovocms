<?php

namespace backend\modules\website\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\website\Feed;
use common\models\website\Website;
use yii\web\NotFoundHttpException;

/**
 * Feed
 */
class FeedForm extends Model
{

    public $name;

    public $feed;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название'),
        ];
    }

    public function __construct($feed_id=null, $website_id=null, $config = [])
    {
        if ($feed_id===null && Website::find()->where(['id'=>$website_id,'team_id'=>Yii::$app->team->id])->one()!==null ) {

            $this->feed=new Feed;
            $this->feed->website_id=$website_id;

        } elseif ($feed_id && ($this->feed=Feed::find()->joinWith('website')->where(['{{%feed}}.id'=>$feed_id,'{{%website}}.team_id'=>Yii::$app->team->id])->one())!==null ) {

            $this->name=$this->feed->name;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $feed = $this->feed;

            $feed->name=$this->name;

            return $feed->save();
        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->feed->delete();
    }
}
