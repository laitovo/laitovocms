<?php

namespace backend\modules\website\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\website\Website;
use common\models\order\Order;
use common\models\order\Item;
use common\models\order\Product;
use yii\web\NotFoundHttpException;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;

/**
 * Order
 */
class OrderForm extends Model
{

    public $items='[]';
    public $person;
    public $email;
    public $phone;

    public $order=null;
    private $_json=[];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['items'], 'string'],
            ['items', 'default', 'value' => '[]'],
            [['email'], 'email'],
            [['person', 'email', 'phone'], 'string', 'max' => 255],
            [['person','email','phone'], 'filter', 'filter' => 'trim'],
            [['items'], 'validateItems'],
        ];
    }

    public function validateItems($attribute, $params)
    {
        $number = new NumberValidator();
        $string = new StringValidator();
        $required = new RequiredValidator();

        if (!$this->hasErrors()) {
            if ($this->items){
                try {
                    $items=Json::decode($this->items);
                    if ($items){
                        foreach ($items as $row) {
                            if (($number->validate($row['id'], $error) || $row['id']==null)
                                && ($number->validate($row['product_id'], $error) || $row['product_id']==null)
                                && ($number->validate($row['retail'], $error) || $row['retail']==null)
                                && ($required->validate($row['price'], $error) && $row['price']>=0)
                                && $number->validate($row['price'], $error) 
                                && ($required->validate($row['quantity'], $error) && $row['quantity']>0) 
                                && $number->validate($row['quantity'], $error) 
                                && $required->validate($row['name'], $error) 
                                && $string->validate($row['name'], $error) 
                                && $string->validate($row['properties'], $error) ) {
                            } else {
                                $this->addError($attribute, Yii::t('yii', 'Invalid data received for parameter "{param}".', ['param'=>$this->getAttributeLabel($attribute)]));
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $this->addError($attribute, Yii::t('yii', 'Invalid data received for parameter "{param}".', ['param'=>$this->getAttributeLabel($attribute)]));
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'items' => Yii::t('app', 'Позиции заказа'),
            'person' => Yii::t('app', 'ФИО'),
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Телефон'),
        ];
    }

    public function __construct($order_id=null, $website_id=null, $config = [])
    {
        if ($order_id===null && Website::find()->where(['id'=>$website_id,'team_id'=>Yii::$app->team->id])->one()!==null ) {

            $this->order=new Order;
            $this->order->team_id=Yii::$app->team->id;
            $this->order->website_id=$website_id;

        } elseif ( ($this->order=Order::find()->where(['id'=>$order_id,'team_id'=>Yii::$app->team->id])->one())!==null ) {

            $items=[];
            if ($this->order->items){
                foreach ($this->order->items as $row) {
                    $items[]=[
                        'id'=>$row->id,
                        'product_id'=>$row->product_id,
                        'name'=>$row->name,
                        'price'=>$row->price,
                        'retail'=>$row->retail,
                        'quantity'=>$row->quantity,
                        'properties'=>$row->properties,
                    ];
                }
            }
            $this->items=Json::encode($items);

            $this->_json=Json::decode($this->order->json ? $this->order->json : '{}');

            $this->person=isset($this->_json['person']) ? $this->_json['person'] : null;
            $this->email=isset($this->_json['email']) ? $this->_json['email'] : null;
            $this->phone=isset($this->_json['phone']) ? $this->_json['phone'] : null;

        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $order = $this->order;
            $json = $this->_json;

            $json['person']=$this->person;
            $json['email']=$this->email;
            $json['phone']=$this->phone;

            $order->json=Json::encode($json);

            if ($order->save()){
                if ($this->items){
                    $items=Json::decode($this->items);
                    foreach ($items as $row) {
                        if (!($row['id'] && ($item=Item::findOne($row['id']))!==null && $item->order_id==$order->id))
                            $item=new Item;
                        $item->order_id=$order->id;
                        $item->product_id=null;
                        if ($row['product_id'] && ($product=Product::findOne($row['product_id']))!==null && $product->team_id==$order->team_id)
                            $item->product_id=$row['product_id'];
                        $item->name=$row['name'];
                        $item->price=$row['price'];
                        $item->retail=$row['retail'];
                        $item->quantity=$row['quantity'];
                        $item->properties=$row['properties'];
                        if ($item->save())
                            $itemsid[$item->id]=$item->id;
                    }
                    if ($order->items){
                        foreach ($order->items as $row) {
                            if (!isset($itemsid[$row->id])){
                                $row->delete();
                            }
                        }
                    }
                }
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->order->delete();
    }
}
