<?php

namespace backend\modules\website\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\website\Page;
use common\models\website\Website;
use yii\web\NotFoundHttpException;

/**
 * Page
 */
class PageForm extends Model
{

    public $name;

    public $page;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Заголовок'),
        ];
    }

    public function __construct($page_id=null, $website_id=null, $config = [])
    {
        if ($page_id===null && Website::find()->where(['id'=>$website_id,'team_id'=>Yii::$app->team->id])->one()!==null ) {

            $this->page=new Page;
            $this->page->website_id=$website_id;

        } elseif ($page_id && ($this->page=Page::find()->joinWith('website')->where(['{{%page}}.id'=>$page_id,'{{%website}}.team_id'=>Yii::$app->team->id])->one())!==null ) {

            $this->name=$this->page->name;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $page = $this->page;

            $page->name=$this->name;

            return $page->save();
        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->page->delete();
    }
}
