<?php

namespace backend\modules\website\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\website\Website;
use yii\web\NotFoundHttpException;

/**
 * Website
 */
class WebsiteForm extends Model
{

    public $name;
    public $subdomain;
    public $langs='[]';

    public $website;
    private $_json=[];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'subdomain'], 'required'],
            [['name', 'subdomain'], 'string', 'max' => 255],
            ['subdomain', 'unique', 
                'targetClass' => '\common\models\website\Website',
                'filter' => ['not',['id' => $this->website->id]],
            ],
            [['langs'], 'string'],
            ['langs', 'default', 'value' => '[]'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название сайта'),
            'subdomain' => Yii::t('app', 'Поддомен'),
            'langs' => Yii::t('app', 'Языки'),
        ];
    }

    public function __construct($website_id=null, $config = [])
    {
        if ($website_id===null){

            $this->website=new Website;
            $this->website->team_id=Yii::$app->team->id;

        } elseif ( ($this->website=Website::find()->where(['id'=>$website_id,'team_id'=>Yii::$app->team->id])->one())!==null ) {

            $this->name=$this->website->name;
            $this->subdomain=$this->website->subdomain;

            $this->_json=Json::decode($this->website->json ? $this->website->json : '{}');

            $this->langs=isset($this->_json['langs']) ? $this->_json['langs'] : null;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $website = $this->website;
            $json = $this->_json;

            $json['langs']=$this->langs;
            $website->json=Json::encode($json);

            $website->name=$this->name;
            $website->subdomain=$this->subdomain;

            return $website->save();
        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->website->delete();
    }
}
