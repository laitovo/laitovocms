<?php

namespace backend\modules\website;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\website\controllers';

    public function init()
    {
		\Yii::$app->layout='2columns';
        parent::init();

        // custom initialization code goes here
    }
}
