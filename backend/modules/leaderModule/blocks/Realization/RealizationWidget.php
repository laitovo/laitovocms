<?php

namespace backend\modules\leaderModule\blocks\Realization;

//use backend\modules\leaderModule\blocks\Indicators\helpers\ChunkHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\leaderModule\blocks\Indicators\models\ProductionReport;
use backend\modules\leaderModule\blocks\Indicators\ServiceIndicators;
use backend\modules\leaderModule\blocks\Realization\models\ReturnNoteRepo;
use backend\modules\leaderModule\blocks\Realization\models\SellingRepo;
use core\models\returnNote\ReturnNote;
use DateTime;
use yii\base\Widget;
use Yii;
use yii\db\Connection;
use yii\db\Query;
use yii\httpclient\Client;

class RealizationWidget extends Widget
{
    /** @var string|null */
    private $period;
    private $days = [];
    /** @var DateTime */
    private $first_day;
    /** @var DateTime */
    private $last_day;
    /** @var string */
    private $current_year;
    private $locales = ['ru', 'de'];
    // 0 - физики, 1 - опт
    private $types = [0, 1];
    // максимальное кол-во столбцов в графике
    // private  $num_partition = 30;

    public function init()
    {
        $this->period = Yii::$app->request->get('period');
        $this->setDate();
        parent::init();
    }

    public function run()
    {
        /**
         * Как выглядит сборный массив данных для отображения пользователю
         *
         * $result = [
         *      $locale =>
         *       $year => [
         *       'sunNoNds' => '',
         *       'sumWithNds' => '',
         *       'sumBonus' => '',
         *       'sumArticles' => '',
         *       'priceOnArticle' => '',
         *       'topArticles' => [
         *              [
         *                  'article' => '',
         *                  'price' => '',
         *              ]
         *          ]
         *       ],
         */

        $data['data'] = $this->getData();
        $period = $this->first_day->format('Y-m-d') . ' - ' . $this->last_day->format('Y-m-d');
        $data['period'] = $period;
        $data['first_day'] = $this->first_day->format('Y-m-d');
        $data['last_day'] = $this->last_day->format('Y-m-d');
        return $this->render('index',$data);
    }

    /**
     * Функция для установки периода для получения статистики.
     *
     * @throws \Exception
     */
    private function setDate()
    {
        $this->current_year = date('Y', time());

        $first_day_post = Yii::$app->request->post('first_day');
        $last_day_post = Yii::$app->request->post('last_day');

        if($first_day_post && $last_day_post) {
            $this->first_day = new DateTime($first_day_post);
            $this->last_day = new DateTime($last_day_post);
            return;
        }

        if($this->period == 'prev_month') {
            $first_day = new DateTime('first day of previous month');
            $last_day = new DateTime('last day of previous month');
            $this->first_day = new DateTime($first_day->format('Y-m-d'));
            $this->last_day = new DateTime($last_day->format('Y-m-d'));
            return;
        }

        if($this->period == 'year') {
            $this->current_year = date('Y', time());
            $this->first_day = new DateTime($this->current_year . '-01-01');
            $this->last_day = new DateTime($this->current_year . '-12-31');
            return;
        }

        if($this->period == 'prev_year') {
            $this->current_year = date('Y', strtotime('-1 year'));
            $this->first_day = new DateTime($this->current_year . '-01-01');
            $this->last_day = new DateTime($this->current_year . '-12-31');
            return;
        }

        $this->first_day = new DateTime((new DateTime('first day of this month'))->format('Y-m-d'));
        $this->last_day = new DateTime(date('Y-m-d', time()));
    }


    /**
     * Функция получения данных для отображения
     *
     * @return array
     */
    private function getData()
    {
        $start_year = $this->current_year - 1;

        $first_day = $this->first_day->format('m-d');
        $last_day = $this->last_day->format('m-d');

        $result = [];

        $locales = SellingRepo::localeList();
        for ($year = $start_year; $year <= $this->current_year; $year++) {
            foreach ($locales as $locale) {
                $dateFrom = strtotime($year . '-' . $first_day . ' 00:00:00');
                $dateTo = strtotime($year . '-' . $last_day . ' 23:59:59');
                $result[$locale]['Все'][$year]['realizations'] = SellingRepo::execute($locale, $dateFrom, $dateTo);
            }
            /**
             * Добавляем информацию по конкретным клиентам
             * Клиент - Hamburg
             */
            $result['ru']['Hamburg'][$year]['realizations'] = SellingRepo::execute('ru', $dateFrom, $dateTo, 110005);
            $result['ru']['Прочие(РФ)'][$year]['realizations'] = SellingRepo::execute('ru', $dateFrom, $dateTo, null, [110005]);

            foreach ($locales as $locale) {
                $dateFrom = strtotime($year . '-' . $first_day . ' 00:00:00');
                $dateTo = strtotime($year . '-' . $last_day . ' 23:59:59');
                $result[$locale]['Все'][$year]['returns'] = ReturnNoteRepo::execute($locale, $dateFrom, $dateTo);
            }

            $result['ru']['Hamburg'][$year]['returns'] = ReturnNoteRepo::execute('ru', $dateFrom, $dateTo, 110005);
            $result['ru']['Прочие(РФ)'][$year]['returns'] = ReturnNoteRepo::execute('ru', $dateFrom, $dateTo, null, [110005]);
        }


        return $result;
    }

}