<?php
/**
 * @var \yii\web\View $this
 * @var array $storage
 * @var array $output
 * @var array $income
 * @var array $debt
 * @var array $stat_all
 * @var array $stat_person
 * @var array $stat_company
 * @var array $summary
 * @var array $labels
 * @var string $materials
 * @var string $period
 * @var string $first_day
 * @var string $last_day
 * @var array $data
 */

use kartik\date\DatePicker;
use yii\helpers\Html;

$css = '
.grey-report {
    background-color: rgba(212, 212, 212, 0.84);
}
.red-report {
    background-color: rgba(212, 11, 23, 0.88);
}
.blue-report {
    background-color: rgba(6, 77, 212, 0.81);
}
.two-block, .third-block {
    padding-top: 40px;
}

.summary1 {
    width: 150px;
}

.summary2 {
    width: 100px;
}

.summary3 {
    width: 50px;
}

.summary1, .summary2, .summary3 {
    height: 15px;
    display: inline-block;
    margin-right: 20px;
}
.datepicker .disabled.day {
    visibility: hidden;
}';

$this->registerCss($css);
$currentDate = date('z', time());
$endDate = date('L', time()) == 0 ? 364 - $currentDate : 365 - $currentDate;
?>

<hr>

<div class="row">
    <?= Html::beginForm(['index']) ?>
    <div class="col-md-2">
        <?= DatePicker::widget([
            'name' => 'first_day',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => $first_day,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true,
                'startDate' => '-' . $currentDate . 'd',
                'endDate' => '+' . $endDate . 'd',
            ],
            'options' => [
                'placeholder' => 'Поле От:',
                'autocomplete' => 'off',
                'required' => true
            ]
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= DatePicker::widget([
            'name' => 'last_day',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => $last_day,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true,
                'startDate' => '-' . $currentDate . 'd',
                'endDate' => '+' . $endDate . 'd',
            ],
            'options' => [
                'placeholder' => 'Поле До:',
                'autocomplete' => 'off',
                'required' => true
            ]
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::submitButton('Выбрать период', ['class' => 'btn btn-success']) ?>
    </div>
    <?= Html::endForm() ?>
    <div class="col-md-6">
        <?= Html::a('Текущий месяц', ['index'], ['class' => 'btn btn-info']) ?> |
        <?= Html::a('Предыдущий месяц', ['index', 'period' => 'prev_month'], ['class' => 'btn btn-warning prev-month']) ?> |
        <?= Html::a('Год', ['index', 'period' => 'year'], ['class' => 'btn btn-danger year-report']) ?> |
        <?= Html::a('Предыдущий Год', ['index', 'period' => 'prev_year'], ['class' => 'btn btn-primary year-report']) ?>
    </div>
</div>

<hr>

<div>
    <h4 class="text-center">Реализация</h4>
    <p>
        <span style="display: inline-block; background-color: #76838f; width: 40px; height: 10px"></span> - сумма реализаций
        <br>
        <span style="display: inline-block; background-color: red; width: 40px; height: 10px"></span> - сумма возвратов
    </p>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 15%">Регион</th>
            <th style="width: 15%">Клиент</th>
            <th style="width: 10%">Год</th>
            <th>СуммаБезНдс</th>
            <th>КоличествоАртикулов (Автошторки)</th>
            <th>СредняяЦенаАртикула (Автошторки)</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $locale => $clientData):?>
            <?php
            $isEcho = null;
            $localeLabel = $locale  == 'ru' ? 'ИП Григорьев Д.Л.' : 'Laitovo Manufactory';
            $currency = $locale  == 'ru' ? 'рублей' : 'евро';
            ?>
            <?php foreach ($clientData as $clientName => $localeData):?>
                <?php
                $isEchoClient = null;
                $clientLabel = $clientName;
                ?>
                <?php foreach ($localeData as $year => $value):?>
                    <tr>
                        <?php
                        if (!$isEcho):
                            $count = 2 * count($clientData);
                            echo "<td rowspan='$count' style='vertical-align: middle'>$localeLabel</td>";
                            ($isEcho = true);
                        endif;
                        ?>
                        <?php
                        if (!$isEchoClient):
                            echo "<td rowspan='2' style='vertical-align: middle'>$clientLabel</td>";
                            ($isEchoClient = true);
                        endif;
                        ?>
                        <td><?= $year?></td>
                        <td><?= number_format($value['realizations']['sumNoNds'],2,',',' ')  . ' ' . $currency?><br>
                            <span style="color:red; font-size: small" >
                                ( <?= number_format($value['returns']['sumNoNds'],2,',',' ')  . ' ' . $currency?> )
                            </span>
                        </td>
                        <td><?= number_format($value['realizations']['sumCount'],0,',',' ')  . ' ' . 'шт'?></td>
                        <td><?= number_format($value['realizations']['avgPrice'],2,',',' ') . ' ' . $currency?></td>
    <!--                    <td>--><?//= number_format($value,2,',',' ') . " $currency"?><!--</td>-->
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</pre>
