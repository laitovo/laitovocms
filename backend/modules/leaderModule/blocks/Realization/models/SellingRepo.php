<?php

namespace backend\modules\leaderModule\blocks\Realization\models;

use core\models\selling\Selling;
use core\models\selling\SellingItem;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class SellingRepo
{
    /**
     * Основная функция получение массива данных
     *
     * @param $locale
     * @param $dateFrom
     * @param $dateTo
     * @param $clientId
     * @param $notClientId
     * @return array
     */
    public  static function execute($locale, $dateFrom, $dateTo, $clientId = null, $notClientId = [])
    {

        /**
         * Для каждого региона получаем информацию о суммах рализации
         */
        /**
         * Основные суммы
         */
        $query = Selling::find()
            ->where(['locale' => $locale])
            ->select("SUM(sumWithoutNds) as sumNoNds, SUM(sumWithNds) as sumNds, SUM(discountWithNds) as sumBonus");
        if ($dateFrom)
            $query->andWhere(['>=','date',$dateFrom]);
        if ($dateTo)
            $query->andWhere(['<=','date',$dateTo]);
        if ($clientId)
            $query->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))
            $query->andWhere(['not in','clientId',$notClientId]);
        $sumLocaleData = $query->asArray()->all();
        $data = $sumLocaleData[0];

        /**
         * Общая информация по артикулам
         */
        $query = SellingItem::find()
            ->alias('items')
            ->leftJoin('core_selling','items.sellingId = core_selling.id')
            ->where(['locale' => $locale])
            ->andWhere(['not like','article','OT-%',false]) //Статистика только по основной продукции ( автошторки )
            ->andWhere(['not like','article','%-%-%-5-%',false]) //Статистика только по основной продукции ( автошторки )
            ->andWhere(['not like','article','%-%-%-4-%',false]) //Статистика только по основной продукции ( автошторки )
            ->select(new Expression('SUM(count) as sumCount, ROUND((SUM(costWithoutNds)/SUM(count)),2) as avgPrice'));
        if ($dateFrom)
            $query->andWhere(['>=','core_selling.date',$dateFrom]);
        if ($dateTo)
            $query->andWhere(['<=','core_selling.date',$dateTo]);
        if ($clientId)
            $query->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))
            $query->andWhere(['not in','clientId',$notClientId]);

        $itemsInfo = $query->asArray()->all();

        $data = ArrayHelper::merge($data,$itemsInfo[0]);


        /**
         * Топ артикулов
         */
        $query = SellingItem::find()
            ->alias('items')
            ->leftJoin('core_selling','items.sellingId = core_selling.id')
            ->where(['locale' => $locale]);
            if ($locale == 'ru') {
                $query->andWhere(['like','article','FD-%-%-1-%',false]); //Статистика только по основной продукции ( автошторки )
            }elseif ($locale == 'eu') {
                $query->andWhere(['like','article','RD-%-%-1-%',false]); //Статистика только по основной продукции ( автошторки )
            }
            $query->select(new Expression('items.article as article, SUM(items.count) as sumCount,  ROUND(SUM(items.costWithoutNds)/SUM(items.count),2) as averageCost'));
        if ($dateFrom)
            $query->andWhere(['>=','core_selling.date',$dateFrom]);
        if ($dateTo)
            $query->andWhere(['<=','core_selling.date',$dateTo]);
        if ($clientId)
            $query->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))
            $query->andWhere(['not in','clientId',$notClientId]);

        $topItems = $query->groupBy('article')
            ->orderBy('SUM(items.count) desc')
            ->limit(5)
            ->asArray()
            ->all();

        $data['topItems'] = $topItems;

        return $data;
    }

    public static function localeList() {
        /**
         * Получаем все регионы, в разрезе которых будем получать данные
         */
        return $locales = Selling::find()->select('locale')->distinct()->column();
    }
}