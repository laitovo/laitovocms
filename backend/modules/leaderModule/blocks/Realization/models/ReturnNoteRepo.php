<?php

namespace backend\modules\leaderModule\blocks\Realization\models;

use core\models\returnNote\ReturnNote;
use core\models\selling\Selling;
use core\models\selling\SellingItem;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class ReturnNoteRepo
{
    /**
     * Основная функция получение массива данных
     *
     * @param $locale
     * @param $dateFrom
     * @param $dateTo
     * @param $clientId
     * @param $notClientId
     * @return array
     */
    public  static function execute($locale, $dateFrom, $dateTo, $clientId = null, $notClientId = [])
    {

        /**
         * Для каждого региона получаем информацию о суммах рализации
         */
        /**
         * Основные суммы
         */
        $query = ReturnNote::find()
            ->where(['locale' => $locale])
            ->select("SUM(sumWithoutNds) as sumNoNds, SUM(sumWithNds) as sumNds");
        if ($dateFrom)
            $query->andWhere(['>=','date',$dateFrom]);
        if ($dateTo)
            $query->andWhere(['<=','date',$dateTo]);
        if ($clientId)
            $query->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))
            $query->andWhere(['not in','clientId',$notClientId]);
        $sumLocaleData = $query->asArray()->all();
        $data = $sumLocaleData[0];

        return $data;
    }

    public static function localeList() {
        /**
         * Получаем все регионы, в разрезе которых будем получать данные
         */
        return $locales = ReturnNote::find()->select('locale')->distinct()->column();
    }
}