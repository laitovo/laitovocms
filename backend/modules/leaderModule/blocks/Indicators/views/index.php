<?php
/**
 * @var \yii\web\View $this
 * @var array $storage
 * @var array $output
 * @var string $materials
 */

?>

<div>
    <h4 class="text-center">Выпуск продукции</h4>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th style="width: 15%">Регион</th>
                <th style="width: 15%">Раздел</th>
                <th style="width: 10%">Год</th>
                <th style="width: 30%; text-align: center">Выпуск, артикулов</th>
                <th style="width: 30%; text-align: center">Выпуск, себестоимость</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $isEcho = null;
        ?>
        <?php foreach ($output as $year => $value):?>
            <tr>
                <?php if (!$isEcho):
                    echo "<td rowspan='2'>ИП Григорьев Д.Л.</td>";
                    echo "<td rowspan='2'>Итого</td>";
                    $isEcho = true;
                endif;?>
                <td><?= $year?></td>
                <td style='text-align: center'><?= $value['count']?></td>
                <td style='text-align: center'><?= number_format($value['sum'],2,',',' ') . " рублей"?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div>
    <h4 class="text-center">Остаток на складе ГП</h4>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 30%;">Регион</th>
            <th style="width: 10%;">Год</th>
            <th style="width: 30%;text-align: center">Количество артикулов</th>
            <th style="width: 30%;text-align: center">Себестоимость</th>
        </tr>
        </thead>
        <tbody>
        <?php
            $sumCount = 0;
            $sumSum = 0;
        ?>
        <?php foreach ($storage as $locale => $value):?>
            <?php

            $isEcho = null;
            $label = $locale  == 'ru' ? 'ИП Григорьев Д.Л.' : 'Laitovo Manufactory';
            $currency = $locale  == 'ru' ? 'рублей' : 'рублей';
            ?>
                <tr>
                    <td  style='vertical-align: middle'><?= $label?></td>
                    <td  style='vertical-align: middle'>2020 (сейчас)</td>
                    <td style="text-align: center"><?= $value['count'] ?> шт</td>
                    <td style="text-align: center">
                        <?= number_format($value['sum'],2,',',' ') . " $currency"?>
                        <? if (isset($value['sum2'])) : ?>
                            / <span title="По ценам продаж"><?= number_format($value['sum2'],2,',',' ') . " $currency"?></span>
                        <? endif; ?>
                    </td>
                </tr>
            <?php
                $sumCount += $value['count'];
                $sumSum += $value['sum'];
            ?>
        <?php endforeach; ?>
            <tr>
                <td  style='vertical-align: middle'>Всего:</td>
                <td  style='vertical-align: middle'>2020 (сейчас)</td>
                <td style="text-align: center"><?= $sumCount ?> шт</td>
                <td style="text-align: center"><?= number_format($sumSum,2,',',' ') . " $currency"?></td>
            </tr>
        </tbody>
    </table>
</div>

<div>
    <h4 class="text-center">Материалы</h4>
    <table class="table table-striped">
        <tbody>
            <tr>
                <td>Всего на основном складе материалов</td>
                <td><?= number_format($materials,2,',',' ') . " руб"?></td>
            </tr>
        </tbody>
    </table>
</div>







