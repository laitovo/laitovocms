<?php
/**
 * @var \yii\web\View $this
 * @var array $summary
 * @var string $locale
 */
$color = [
    1 => 'grey-report',
    2 => 'red-report',
    3 => 'blue-report',
];
$currency = $locale == 'ru' ? 'RUB' : 'EUR';
$current_year = null;
$prev_year = null;
?>

<div class="row">
    <div class="col-md-6">
        <table>
            <?php $i = 1 ?>
            <?php foreach (array_reverse($summary, true) as $year => $sum) : ?>
                <?php
                if($i == 1) {
                    $current_year = $sum;
                }
                if($i == 2) {
                    $prev_year = $sum;
                }
                ?>
                <tr>
                    <td style="text-align:right; vertical-align: middle">
                        <span class="summary<?= $i ?> <?= $color[$i] ?>"></span>
                    </td>
                    <td style="vertical-align: middle"><?= $year ?> год</td>
                    <td style="padding-left: 30px;"><?= Yii::$app->formatter->asCurrency($sum, $currency) ?></td>
                </tr>
                <?php $i++ ?>
            <?php endforeach ?>
        </table>
    </div>
    <div class="col-md-6">
        <?php if($prev_year && $current_year) : ?>
            <h4>
                Прирост = <?= round( ($current_year / $prev_year) -1, 2) ?> (<?= "$current_year / $prev_year) -1" ?>)
            </h4>
        <?php endif ?>
    </div>
</div>
