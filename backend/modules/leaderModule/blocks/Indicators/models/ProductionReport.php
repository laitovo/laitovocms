<?php

namespace backend\modules\leaderModule\blocks\Indicators\models;

use backend\modules\leaderModule\models\BaseActiveRecord;
use Yii;

/**
 * @property integer $id
 * @property integer $date
 * @property string $locale
 * @property double $sum
 * @property integer $type
 * @property integer $order_id
 */
class ProductionReport extends BaseActiveRecord
{
    public static function tableName()
    {
        return 'leader_module_production_report';
    }

    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    public function rules()
    {
        return [
            [['date', 'type', 'order_id', 'sum', 'locale'], 'required'],
            [['date', 'type', 'order_id'], 'integer'],
            [['sum'], 'number'],
            [['locale'], 'string', 'max' => 2],
        ];
    }
}
