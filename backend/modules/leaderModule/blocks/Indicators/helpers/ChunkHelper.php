<?php


namespace backend\modules\leaderModule\blocks\Indicators\helpers;


class ChunkHelper
{
    const ACTION_SUM = 'sum';
    const ACTION_NULL = 'null';

    public static function create(int $dividend, int $divisor)
    {
        if($dividend < $divisor) {
            $divisor = $dividend;
        }
        $chunk = intdiv($dividend, $divisor);
        $balance = $dividend % $divisor;

        $chunks = [];

        for ($i = 0; $i < $divisor; $i++) {
            $chunks[$i] = $chunk;
        }

        for ($i = 0; $i < $balance; $i++) {
            $chunks[$i]++;
        }
        
        return $chunks;
    }

    public static function createIndex(array $chunks, array $input_arr, string $action)
    {
        if($action != self::ACTION_SUM && $action != self::ACTION_NULL) {
            throw new \DomainException('Unknown action');
        }
        $offset = 0;
        $result = [];
        foreach ($chunks as $length) {
            $arr = array_slice($input_arr, $offset, $length);
            $keys = array_keys($arr);
            $last = count($keys) - 1;
            $index = $keys[0] != $keys[$last] ? "$keys[0] - $keys[$last]" : $keys[0];
            $result[$index] = $action == self::ACTION_SUM ? array_sum($arr) : $index;
            $offset += $length;
        }
        return $result;
    }
}