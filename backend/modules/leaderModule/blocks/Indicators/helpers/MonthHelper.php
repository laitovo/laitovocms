<?php


namespace backend\modules\leaderModule\blocks\Indicators\helpers;


class MonthHelper
{
    public static function get($date)
    {
        return date('d.m', $date);
    }
}