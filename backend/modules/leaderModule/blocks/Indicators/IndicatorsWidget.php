<?php

namespace backend\modules\leaderModule\blocks\Indicators;

//use backend\modules\leaderModule\blocks\Indicators\helpers\ChunkHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\leaderModule\blocks\Indicators\models\ProductionReport;
use DateTime;
use yii\base\Widget;
use Yii;
use yii\db\Connection;
use yii\db\Query;
use yii\httpclient\Client;

class IndicatorsWidget extends Widget
{
    /** @var string|null */
    private $period;
    private $days = [];
    /** @var DateTime */
    private $first_day;
    /** @var DateTime */
    private $last_day;
    /** @var string */
    private $current_year;
    private $locales = ['ru', 'de'];
    // 0 - физики, 1 - опт
    private $types = [0, 1];
    // максимальное кол-во столбцов в графике
    // private  $num_partition = 30;

    public function init()
    {
        $this->period = Yii::$app->request->get('period');
        $this->setDate();
        parent::init();
    }

    public function run()
    {
        $output = $this->getOutputData();
        $storage = $this->getStorageData();
        $materials = $this->getMaterialData();


        $period = $this->first_day->format('Y-m-d') . ' - ' . $this->last_day->format('Y-m-d');
        $data = compact('output','storage','materials');

        return $this->render('index', $data);

    }


    private function setDate()
    {
        $this->current_year = date('Y', time());

        $first_day_post = Yii::$app->request->post('first_day');
        $last_day_post = Yii::$app->request->post('last_day');

        if($first_day_post && $last_day_post) {
            $this->first_day = new DateTime($first_day_post);
            $this->last_day = new DateTime($last_day_post);
            return;
        }

        if($this->period == 'prev_month') {
            $first_day = new DateTime('first day of previous month');
            $last_day = new DateTime('last day of previous month');
            $this->first_day = new DateTime($first_day->format('Y-m-d'));
            $this->last_day = new DateTime($last_day->format('Y-m-d'));
            return;
        }

        if($this->period == 'year') {
            $this->current_year = date('Y', time());
            $this->first_day = new DateTime($this->current_year . '-01-01');
            $this->last_day = new DateTime($this->current_year . '-12-31');
            return;
        }

        if($this->period == 'prev_year') {
            $this->current_year = date('Y', strtotime('-1 year'));
            $this->first_day = new DateTime($this->current_year . '-01-01');
            $this->last_day = new DateTime($this->current_year . '-12-31');
            return;
        }

        $this->first_day = new DateTime((new DateTime('first day of this month'))->format('Y-m-d'));
        $this->last_day = new DateTime(date('Y-m-d', time()));
    }

    private function getOutputData()
    {
        $start_year = $this->current_year - 1;

        $first_day = $this->first_day->format('m-d')  . ' 00:00:00';
        $last_day = $this->last_day->format('m-d') . ' 23:59:59';

        $first_day_int = $this->first_day->getTimestamp();
        $last_day_int = $this->last_day->getTimestamp();

        $index = $this->first_day->format('d.m');
        $this->days[$index] = $index;
        for ($i = $first_day_int; $i < $last_day_int;) {
            $i += 3600 * 24;
            $index = date('d.m', $i);
            $this->days[$index] = $index;
        }

        $result = [];

        for ($year = $start_year; $year <= $this->current_year; $year++) {
            $countArticles = ErpNaryad::find()
                ->where(['>=', 'ready_date', strtotime($year . '-' . $first_day)])
                ->andWhere(['<=', 'ready_date', strtotime($year . '-' . $last_day)])
                ->andWhere(['not like','article', 'OT-%', false])
                ->count();

            $sum1 =
                ErpNaryad::find()
                    ->where(['>=', 'ready_date', strtotime($year . '-' . $first_day)])
                    ->andWhere(['<=', 'ready_date', strtotime($year . '-' . $last_day)])
                    ->andWhere(['or',['like','article', 'FD-%-5', false],['like','article', 'RD-%-5', false]])
                    ->count() * 350;

            $sum2 =
                ErpNaryad::find()
                    ->where(['>=', 'ready_date', strtotime($year . '-' . $first_day)])
                    ->andWhere(['<=', 'ready_date', strtotime($year . '-' . $last_day)])
                    ->andWhere(['or',['like','article', 'FD-%-5', false],['like','article', 'RD-%-5', false]])
                    ->andWhere(['not like','article', '%-5', false])
                    ->count() * 500;

            $sum3 =
                ErpNaryad::find()
                    ->where(['>=', 'ready_date', strtotime($year . '-' . $first_day)])
                    ->andWhere(['<=', 'ready_date', strtotime($year . '-' . $last_day)])
                    ->andWhere(['or',['like','article', 'RV-%', false],['like','article', 'FV-%', false]])
                    ->count() * 100;

            $sum4 =
                ErpNaryad::find()
                    ->where(['>=', 'ready_date', strtotime($year . '-' . $first_day)])
                    ->andWhere(['<=', 'ready_date', strtotime($year . '-' . $last_day)])
                    ->andWhere(['like','article', 'BW-%', false])
                    ->count() * 230;

            $resSum = array_sum([$sum1,$sum2,$sum3,$sum4]);
            $result[$year]['sum'] = $resSum;
            $result[$year]['count'] = $countArticles;
        }


        return $result;
    }

    private function getStorageData()
    {
        $result = [];
        $countStorage = (new Query())
            ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
            ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
            ->where(['not like','logistics_naryad.article', 'OT-%', false])
            ->andWhere('logistics_naryad.reserved_id is NULL')
            ->count();

        $result['ru']['count'] = $countStorage;

        $sum1 = (new Query())
            ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
            ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
            ->where('logistics_naryad.reserved_id is NULL')
            ->andWhere(['or',['like','article', 'FD-%-5', false],['like','article', 'RD-%-5', false]])
            ->count()*420;

        $sum2 = (new Query())
            ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
            ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
            ->where('logistics_naryad.reserved_id is NULL')
            ->andWhere(['or',['like','article', 'FD-%', false],['like','article', 'RD-%', false]])
            ->andWhere(['not like','article', '%-5', false])
            ->count()*650;

        $sum3 = (new Query())
            ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
            ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
            ->where('logistics_naryad.reserved_id is NULL')
            ->andWhere(['or',['like','article', 'RV-%', false],['like','article', 'FV-%', false]])
            ->count() * 171;

        $sum4 = (new Query())
            ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
            ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
            ->where('logistics_naryad.reserved_id is NULL')
            ->andWhere(['like','article', 'BW-%', false])
            ->count() * 260;

        $result['ru']['sum'] = array_sum([$sum1,$sum2,$sum3,$sum4]);

        $sum1 = (new Query())
                ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
                ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
                ->where('logistics_naryad.reserved_id is NULL')
                ->andWhere(['or',['like','article', 'FD-%-5', false],['like','article', 'RD-%-5', false]])
                ->count()*2150;

        $sum2 = (new Query())
                ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
                ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
                ->where('logistics_naryad.reserved_id is NULL')
                ->andWhere(['like','article', 'FD-%', false])
                ->andWhere(['not like','article', '%-5', false])
                ->count()*3599;

        $sum3 = (new Query())
                ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
                ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
                ->where('logistics_naryad.reserved_id is NULL')
                ->andWhere(['like','article', 'RD-%', false])
                ->andWhere(['not like','article', '%-5', false])
                ->count()*3099;

        $sum4 = (new Query())
                ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
                ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
                ->where('logistics_naryad.reserved_id is NULL')
                ->andWhere(['or',['like','article', 'RV-%', false],['like','article', 'FV-%', false]])
                ->andWhere(['not like','article', '%-5', false])
                ->count() * 499;

        $sum5 = (new Query())
                ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
                ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
                ->where('logistics_naryad.reserved_id is NULL')
                ->andWhere(['or',['like','article', 'RV-%-5', false],['like','article', 'FV-%-5', false]])
                ->count() * 360;


        $sum6 = (new Query())
                ->select("count(logistics_storage_state.id)")->from('logistics_storage_state')
                ->leftJoin('logistics_naryad','logistics_naryad.id = logistics_storage_state.upn_id')
                ->where('logistics_naryad.reserved_id is NULL')
                ->andWhere(['like','article', 'BW-%', false])
                ->count() * 1500;

        $result['ru']['sum2'] = array_sum([$sum1,$sum2,$sum3,$sum4,$sum5,$sum6]);



        $storage = Yii::$app->params['obmen_storage'];
        $db_storage = new Connection([
            'dsn' => 'mysql:host=' . $storage['host'] . ';port=' . $storage['port'] . ';dbname=' . $storage['dbname'],
            'username' => $storage['username'],
            'password' => $storage['password'],
            'charset' => 'utf8',
        ]);

        $query = (new Query())
            ->from('current')
            ->where('reserved_id is NULL')
            ->andWhere(['storage_id' => 2]);

        $countStorage = (new Query())
            ->from('current')
            ->where('reserved_id is NULL')
            ->andWhere(['storage_id' => 2])
            ->andWhere(['not like','catalog_id', 'OT-%', false])
            ->count('id',$db_storage);

        $result['eu']['count'] = $countStorage;

        /**
         * Вычисляем сумму
         */
        $sum1 = (new Query())
                ->from('current')
                ->where('reserved_id is NULL')
                ->andWhere(['storage_id' => 2])
                ->andWhere(['or',['like','catalog_id', 'FD-%-5', false],['like','catalog_id', 'RD-%-5', false]])
                ->count('id',$db_storage)*350;

        $sum2 = (new Query())
                ->from('current')
                ->where('reserved_id is NULL')
                ->andWhere(['storage_id' => 2])
                ->andWhere(['or',['like','catalog_id', 'FD-%', false],['like','catalog_id', 'RD-%', false]])
                ->andWhere(['not like','catalog_id', '%-5', false])
                ->count('id',$db_storage)*500;

        $sum3 = (new Query())
                ->from('current')
                ->where('reserved_id is NULL')
                ->andWhere(['storage_id' => 2])
                ->andWhere(['or',['like','catalog_id', 'RV-%', false],['like','catalog_id', 'FV-%', false]])
                ->count('id',$db_storage) * 100;

        $sum4 = (new Query())
                ->from('current')
                ->where('reserved_id is NULL')
                ->andWhere(['storage_id' => 2])
                ->andWhere(['like','catalog_id', 'BW-%', false])
                ->count('id',$db_storage) * 230;

        $result['eu']['sum'] = array_sum([$sum1,$sum2,$sum3,$sum4]);

        return $result;
    }

    private function getMaterialData()
    {
        $clients = Yii::$app->params['obmen_clients'];
        $db_clients = new Connection([
            'dsn' => 'mysql:host=' . $clients['host'] . ';port=' . $clients['port'] . ';dbname=' . $clients['dbname'],
            'username' => $clients['username'],
            'password' => $clients['password'],
            'charset' => 'utf8',
        ]);

        $rows = (new Query())
            ->select('r.result_sum as sum')
            ->from('registry_materials' . ' r')
            ->leftJoin('nomenclature' . ' n', 'r.nomenclature_id = n.id')
            ->leftJoin('nomenclature_characteristics_relation' . ' ncr', 'n.id = ncr.nomenclature_id')
            ->leftJoin('nomenclature_characteristics' . ' nch', 'ncr.characteristic_id = nch.id')
            ->leftJoin('nomenclature_storage' . ' s', 'r.storage_id = s.id')
            ->where(['r.active' => 1])
            ->andWhere(['>', 'r.result_num', 0])
            ->andWhere(['in','r.storage_id',[1]])
            ->orderBy(['s.name' => SORT_ASC, 'n.name' => SORT_ASC])
            ->column($db_clients);

        return array_sum($rows);
    }
}