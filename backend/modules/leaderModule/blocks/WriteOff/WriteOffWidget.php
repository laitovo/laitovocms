<?php

namespace backend\modules\leaderModule\blocks\WriteOff;


use backend\modules\leaderModule\blocks\WriteOff\models\WriteOffRepo;
use core\models\receipt\Receipt;
use core\models\writeOff\WriteOff;
use DateTime;
use yii\base\Widget;
use Yii;

class WriteOffWidget extends Widget
{
    /** @var string|null */
    private $period;
    private $days = [];
    /** @var DateTime */
    private $first_day;
    /** @var DateTime */
    private $last_day;
    /** @var string */
    private $current_year;
    private $locales = ['ru', 'de'];
    // 0 - физики, 1 - опт
    private $types = [0, 1];
    private $yearsList = [];
    private $currencyList = [];
    // максимальное кол-во столбцов в графике
    // private  $num_partition = 30;

    public function init()
    {
        $this->period = Yii::$app->request->get('period');
        $this->setDate();
        parent::init();
    }

    public function run()
    {
        /**
         * Как выглядит сборный массив данных для отображения пользователю
         *
         * $result = [
         *      $locale =>
         *       $year => [
         *       'sunNoNds' => '',
         *       'sumWithNds' => '',
         *       'sumBonus' => '',
         *       'sumArticles' => '',
         *       'priceOnArticle' => '',
         *       'topArticles' => [
         *              [
         *                  'article' => '',
         *                  'price' => '',
         *              ]
         *          ]
         *       ],
         */

        $data['data'] = $this->getData();
        $data['yearsList'] = $this->yearsList;
        $data['currencyList'] = $this->currencyList;

        return $this->render('index',$data);
    }

    /**
     * Функция для установки периода для получения статистики.
     *
     * @throws \Exception
     */
    private function setDate()
    {
        $this->current_year = date('Y', time());

        $first_day_post = Yii::$app->request->post('first_day');
        $last_day_post = Yii::$app->request->post('last_day');

        if($first_day_post && $last_day_post) {
            $this->first_day = new DateTime($first_day_post);
            $this->last_day = new DateTime($last_day_post);
            return;
        }

        if($this->period == 'prev_month') {
            $first_day = new DateTime('first day of previous month');
            $last_day = new DateTime('last day of previous month');
            $this->first_day = new DateTime($first_day->format('Y-m-d'));
            $this->last_day = new DateTime($last_day->format('Y-m-d'));
            return;
        }

        if($this->period == 'year') {
            $this->current_year = date('Y', time());
            $this->first_day = new DateTime($this->current_year . '-01-01');
            $this->last_day = new DateTime($this->current_year . '-12-31');
            return;
        }

        if($this->period == 'prev_year') {
            $this->current_year = date('Y', strtotime('-1 year'));
            $this->first_day = new DateTime($this->current_year . '-01-01');
            $this->last_day = new DateTime($this->current_year . '-12-31');
            return;
        }

        $this->first_day = new DateTime((new DateTime('first day of this month'))->format('Y-m-d'));
        $this->last_day = new DateTime(date('Y-m-d', time()));
    }


    /**
     * Функция получения данных для отображения
     *
     * @return array
     */
    private function getData()
    {
        $start_year = $this->current_year - 1;

        $first_day = $this->first_day->format('m-d');
        $last_day = $this->last_day->format('m-d');

        $result = [];

        $locales = WriteOffRepo::localeList();
        for ($year = $start_year; $year <= $this->current_year; $year++) {

            $this->yearsList[] = $year;

            $dateFrom = strtotime($year . '-' . $first_day . ' 00:00:00');
            $dateTo = strtotime($year . '-' . $last_day . ' 23:59:59');

            foreach (WriteOffRepo::currencyList() as $currency) {
                foreach ($locales as $locale) {
                    $result[$locale]['Все клиенты'][$year][$currency]['write-off'] = WriteOffRepo::execute($locale, $currency, $dateFrom, $dateTo, [WriteOff::MONEY_RETURN_TYPE, WriteOff::OTHER_TYPE]);
                    $this->currencyList[$currency] = $currency;
                }
            }

            $this->currencyList['RUB'] = 'RUB';
            $this->currencyList['EUR'] = 'EUR';

        }

        return $result;
    }

}