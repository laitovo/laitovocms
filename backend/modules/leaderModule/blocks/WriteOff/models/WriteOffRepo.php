<?php

namespace backend\modules\leaderModule\blocks\WriteOff\models;

use core\models\writeOff\WriteOff;

class WriteOffRepo
{
    /**
     * Основная функция получение массива данных
     *
     * @param $locale
     * @param $currency
     * @param $type
     * @param $dateFrom
     * @param $dateTo
     * @param null|integer $clientId
     * @param array $notClientId
     * @return false|string|null
     */
    public  static function execute($locale, $currency, $dateFrom, $dateTo, $type = [], $clientId = null, $notClientId = [])
    {

        /**
         * Для каждого региона получаем информацию о суммах рализации
         */
        /**
         * Основные суммы
         */
        $query = WriteOff::find()
            ->where(['locale' => $locale])
            ->andWhere(['currency' => $currency])
            ->select("SUM(total) as total");
        if (!empty($type))
            $query->andWhere(['in','type',$type]);
        if ($dateFrom)
            $query->andWhere(['>=','date',$dateFrom]);
        if ($dateTo)
            $query->andWhere(['<=','date',$dateTo]);
        if ($clientId)
            $query->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))
            $query->andWhere(['not in','clientId',$notClientId]);

        return $query->scalar();
    }

    public static function localeList() {
        /**
         * Получаем все регионы, в разрезе которых будем получать данные
         */
        return $locales = WriteOff::find()->select('locale')->distinct()->column();
    }

    public static function currencyList() {
        /**
         * Получаем все регионы, в разрезе которых будем получать данные
         */
        return $locales = WriteOff::find()->select('currency')->distinct()->column();
    }
}