<?php

namespace backend\modules\leaderModule\blocks\EuropeProfitability;

use yii\base\Widget;
use yii\helpers\Json;
use yii\httpclient\Client;

class EuropeProfitabilityWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('index', ['data' => $this->getData()] );
    }

    /**
     * Функция получения данных для отображения
     *
     * @return array
     */
    private function getData()
    {
        $dateFrom = date('Y-m-d H:i:s', mktime(0, 0, 0,  1, 1,date('Y')));
        $dateTo = date('Y-m-d H:i:s',mktime(23, 59, 59));

        $result = [
            'costs' => 0,
            'margin' => 0
        ];

        $host = \Yii::$app->params['laitovo-eu'];
        $client = new Client();
        $response = $client->createRequest()
            ->addHeaders(['Accept' => 'text/html,*/*'])
            ->setMethod('get')
            ->setUrl("$host/api/profitability/sum")
            ->setData(['dateFrom' => $dateFrom,'dateTo' => $dateTo])
            ->send();

        if (!empty($response->content)) {
            $data = Json::decode($response->content);
            if (!empty($data['sum'])) {
                $result['costs'] = $data['sum'];
            }
        }

        $response = $client->createRequest()
            ->addHeaders(['Accept' => 'text/html,*/*'])
            ->setMethod('get')
            ->setUrl("$host/api/profitability/margin")
            ->setData(['dateFrom' => $dateFrom,'dateTo' => $dateTo])
            ->send();

        if (!empty($response->content)) {
            $data = Json::decode($response->content);
            if (!empty($data['margin'])) {
                $result['margin'] = $data['margin'];
            }
        }

        return $result;
    }

}