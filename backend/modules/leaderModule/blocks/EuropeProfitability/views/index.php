<?php
/**
 * @var View $this
 * @var array $data
 */

use yii\web\View;

?>

<div>
    <h4 class="text-center">Доходность Европы (нарастающим итогом с начала года), евро</h4>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th class="text-center">Валовая маржа</th>
            <th class="text-center">Расходы</th>
            <th class="text-center">Доходность</th>
        </tr>
        </thead>
        <tbody>
                <tr>
                    <td class="text-center"><?= number_format($data['margin'],2,',', ' ')?></td>
                    <td class="text-center"><?= number_format($data['costs'],2,',', ' ')?></td>
                    <td class="<?= ($data['margin'] - $data['costs']) < 0 ? "text-danger" : "text-success" ?> text-center">
                        <?= number_format( $data['margin'] - $data['costs'],2,',', ' ')?>
                    </td>
                </tr>
        </tbody>
    </table>
</div>

