<?php
/**
 * @var \yii\web\View $this
 * @var array $data
 * @var array $yearsList
 * @var array $currencyList
 */

?>

<div>
    <h4 class="text-center">Поступления денежных средств на расчетный счет и в кассу</h4>

    <span style="display: inline-block; background-color: #76838f; width: 40px; height: 10px"></span> - сумма поступлений за минусом комиссий<br>
    <span style="display: inline-block; background-color: red; width: 40px; height: 10px"></span>  - сумма возвратов клиентам за период<br>
    <span style="display: inline-block; background-color: #8D021F; width: 40px; height: 10px"></span>  -  сумма всех комиссий за период

    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 15%">Регион</th>
            <th style="width: 15%">Клиент</th>
            <th style="width: 10%">Год</th>
            <?php foreach ($currencyList as $currencyItem):?>
                <th style='text-align: center'><?= $currencyItem?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $locale => $clientData):?>

            <?php
                $localeLabel = $locale  == 'ru' ? 'ИП Григорьев Д.Л.' : 'Laitovo Manufactory';
                $isEcho = null;
            ?>

            <?php foreach ($clientData as $clientName => $localeData):?>
                <?php
                $isEchoClient = null;
                $clientLabel = $clientName;
                ?>
                <?php foreach ($localeData as $year => $value):?>
                    <tr>
                        <?php
                        if (!$isEcho):
                            $count = 2 * count($clientData);
                            echo "<td rowspan='$count' style='vertical-align: middle'>$localeLabel</td>";
                            ($isEcho = true);
                        endif;
                        ?>
                        <?php
                        if (!$isEchoClient):
                            echo "<td rowspan='2' style='vertical-align: middle'>$clientLabel</td>";
                            ($isEchoClient = true);
                        endif;
                        ?>
                        <td><?= $year?></td>
                        <?php foreach ($value as $currency => $sum):
                            $commission = $sum['commission'] ?? 0;
                            ?>
                            <td style='text-align: center'><?= number_format($sum['receipt'] - $commission,2,',',' ')?>
                                <? if (array_key_exists('write-off',$sum)): ?>
                                <br>
                                <span style="color:red; font-size: small" >
                                    ( <?= number_format($sum['write-off'],2,',',' ') ?> )
                                </span>
                                <? endif; ?>
                                <? if ($commission): ?>
                                    <br>
                                    <span style="color:#8D021F; font-size: small" >
                                        ( <?= number_format($sum['commission'],2,',',' ') ?> )
                                    </span>
                                <? endif;?>
                            </td>

                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
