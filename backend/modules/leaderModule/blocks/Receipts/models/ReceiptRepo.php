<?php

namespace backend\modules\leaderModule\blocks\Receipts\models;

use core\models\receipt\Receipt;
use core\models\selling\Selling;
use core\models\selling\SellingItem;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class ReceiptRepo
{
    /**
     * Основная функция получение массива данных
     *
     * @param $locale
     * @param $currency
     * @param $mode
     * @param $dateFrom
     * @param $dateTo
     * @param null|integer $clientId
     * @param array $notClientId
     * @return false|string|null
     */
    public  static function execute($locale, $currency, $mode, $dateFrom, $dateTo, $clientId = [], $notClientId = [])
    {

        /**
         * Для каждого региона получаем информацию о суммах рализации
         */
        /**
         * Основные суммы
         */
        $query = Receipt::find()
            ->where(['locale' => $locale])
            ->andWhere(['currency' => $currency])
            ->andWhere(['mode' => $mode])
            ->select("SUM(total) as total");
        if ($dateFrom)
            $query->andWhere(['>=','date',$dateFrom]);
        if ($dateTo)
            $query->andWhere(['<=','date',$dateTo]);
        if (!empty($clientId))
            $query->andWhere(['in','clientId',$clientId]);
        if (!empty($notClientId))
            $query->andWhere(['not in','clientId',$notClientId]);
        if ($locale == 'eu')
            $query->andWhere(['!=','fromType','Amazon.de']);

        return $query->scalar();
    }

    public static function localeList() {
        /**
         * Получаем все регионы, в разрезе которых будем получать данные
         */
        return $locales = Receipt::find()->select('locale')->orderBy('locale asc')->distinct()->column();
    }

    public static function currencyList() {
        /**
         * Получаем все регионы, в разрезе которых будем получать данные
         */
        return $locales = Receipt::find()->select('currency')->distinct()->orderBy('currency desc')->column();
    }
}