<?php
/**
 * @var \yii\web\View $this
 * @var array $data
 * @var array $yearsList
 * @var array $currencyList
 */

?>

<div>
    <h4 class="text-center">Поступления денежных средств на расчетный счет и в кассу от реализации продукции минус расходы</h4>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 15%">Регион</th>
            <th style="width: 15%">Раздел</th>
            <th style="width: 10%">Год</th>
            <?php foreach ($currencyList as $currencyItem):?>
                <th style='text-align: center'><?= $currencyItem?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $locale => $clientData):?>

            <?php
            $localeLabel = $locale  == 'ru' ? 'ИП Григорьев Д.Л.' : 'Laitovo Manufactory';
            $isEcho = null;
            ?>

            <?php foreach ($clientData as $clientName => $localeData):?>
                <?php
                $isEchoClient = null;
                $clientLabel = $clientName;
                ?>
                <?php foreach ($localeData as $year => $value):?>
                    <tr>
                        <?php
                        if (!$isEcho):
                            $count = 2 * count($clientData);
                            echo "<td rowspan='$count' style='vertical-align: middle'>$localeLabel</td>";
                            ($isEcho = true);
                        endif;
                        ?>
                        <?php
                        if (!$isEchoClient):
                            echo "<td rowspan='2' style='vertical-align: middle'>$clientLabel</td>";
                            ($isEchoClient = true);
                        endif;
                        ?>
                        <td><?= $year?></td>
                        <?php foreach ($value as $currency => $sum):
                            ?>
                            <td style='text-align: center'>
                                <?if ($sum['writeOff'] == null && $sum['receipt'] == null) :?>
                                    ---
                                <? else: ?>
                                    <?= number_format($sum['receipt'] - $sum['writeOff'],2,',',' ')?>
                                <? endif;?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>

        </tbody>
    </table>

</div>
