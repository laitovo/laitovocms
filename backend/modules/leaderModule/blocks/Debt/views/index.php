<?php
/**
 * @var \yii\web\View $this
 * @var array $data
 */

?>

<div>
    <h4 class="text-center">Дебиторская и кредиторская задолженность во взаиморасчетах с покупателями</h4>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 15%">Регион</th>
            <th style="width: 15%">Клиент</th>
            <th style="width: 10%">Год</th>
            <th>Дебиторка (Нам должны)</th>
            <th>Кредиторка (Мы должны)</th>
            <th>Итого</th>
        </tr>
<!--        <tr>-->
<!--            <th rowspan="2" style='vertical-align: middle'>Регион</th>-->
<!--            <th rowspan="2" style='vertical-align: middle'>Год</th>-->
<!--            <th colspan="3" style='vertical-align: middle; text-align: center'>Дебиторка</th>-->
<!--            <th colspan="3" style='vertical-align: middle; text-align: center'>Кредиторка</th>-->
<!--            <th colspan="3" style='vertical-align: middle; text-align: center'>Итого</th>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th>Начало</th>-->
<!--            <th>Конец</th>-->
<!--            <th>Изменение</th>-->
<!--            <th>Начало</th>-->
<!--            <th>Конец</th>-->
<!--            <th>Изменение</th>-->
<!--            <th>Начало</th>-->
<!--            <th>Конец</th>-->
<!--            <th>Изменение</th>-->
<!--        </tr>-->
        </thead>
        <tbody>
        <?php foreach ($data as $locale => $clientData):?>
            <?php
            $isEcho = null;
            $localeLabel = $locale  == 'ru' ? 'ИП Григорьев Д.Л.' : 'Laitovo Manufactory';
            $currency = $locale  == 'ru' ? 'рублей' : 'евро';
            ?>
            <?php foreach ($clientData as $clientName => $localeData):?>
                <?php
                $isEchoClient = null;
                $clientLabel = $clientName;
                ?>
                <?php foreach ($localeData as $year => $value):?>
                    <tr>
                        <?php
                        if (!$isEcho):
                            $count = 2 * count($clientData);
                            echo "<td rowspan='$count' style='vertical-align: middle'>$localeLabel</td>";
                            ($isEcho = true);
                        endif;
                        ?>
                        <?php
                        if (!$isEchoClient):
                            echo "<td rowspan='2' style='vertical-align: middle'>$clientLabel</td>";
                            ($isEchoClient = true);
                        endif;
                        ?>
                        <td><?= $year?></td>
    <!--                    <td>--><?//= number_format($value['dataCreditStart'],2,',',' ') . " $currency"?><!--</td>-->
                        <td><?= number_format($value['dataDebitEnd'],2,',',' ') . " $currency"?></td>
    <!--                    <td>--><?//= number_format($value['dataCreditDiff'],2,',',' ') . " $currency"?><!--</td>-->
    <!--                    <td>--><?//= number_format($value['dataDebitStart'],2,',',' ') . " $currency"?><!--</td>-->
                        <td><?= number_format($value['dataCreditEnd'],2,',',' ') . " $currency"?></td>
    <!--                    <td>--><?//= number_format($value['dataDebitDiff'],2,',',' ') . " $currency"?><!--</td>-->
    <!--                    <td>--><?//= number_format($value['dataResultStart'],2,',',' ') . " $currency"?><!--</td>-->
                        <td><?= number_format($value['dataResultEnd'],2,',',' ') . " $currency"?></td>
    <!--                    <td>--><?//= number_format($value['dataResultDiff'],2,',',' ') . " $currency"?><!--</td>-->
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


<!--<pre>--><?php
//
//    print_r($data);
//
//    ?>
<!--</pre>-->
