<?php

namespace backend\modules\leaderModule\blocks\Debt\models;

use core\models\debt\Debt;
use core\models\selling\Selling;
use core\models\selling\SellingItem;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class DebtRepo
{
    /**
     * Основная функция получение массива данных
     *
     * @param $locale
     * @param $dateFrom
     * @param $dateTo
     * @param $clientId
     * @param $notClientId
     * @return array
     */
    public  static function execute($locale, $dateFrom, $dateTo, $clientId = null, $notClientId = [])
    {

        /**
         * TODO проверка наличия обязательных параметро Выбора дат.
         * Дебиторка на начало периода
         * Дебиторка на конец периода
         * Изменение дебиторки
         * Также по кредитроке
         * И итоговый финансовый результат. В минусе или плюсе и на сколько по совокупности.
         */

        /**
         * Для каждого региона получаем информацию о суммах рализации
         */
        /**
         * Основные суммы
         */
        $subQuery1 = Debt::find()
            ->select("clientId, sum(total) as total")
            ->where(['locale' => $locale])
            ->andWhere(['<=','date',$dateFrom])
            ->groupBy('clientId')
            ->having('sum(total) < 0');

        if ($clientId) $subQuery1->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))  $subQuery1->andWhere(['not in','clientId',$notClientId]);


        $dataCreditStart = abs((new Query())
            ->select("sum(total)")
            ->from(['a' => $subQuery1])
            ->scalar());

        $subQuery2 = Debt::find()
            ->select("clientId, sum(total) as total")
            ->where(['locale' => $locale])
            ->andWhere(['<=','date',$dateTo])
            ->groupBy('clientId')
            ->having('sum(total) < 0');

        if ($clientId) $subQuery2->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))  $subQuery2->andWhere(['not in','clientId',$notClientId]);

        $dataCreditEnd = abs((new Query())
            ->select("sum(total)")
            ->from(['a' => $subQuery2])
            ->scalar());

        $dataCreditDiff = $dataCreditEnd - $dataCreditStart;

        $subQuery3 = Debt::find()
            ->select("clientId, sum(total) as total")
            ->where(['locale' => $locale])
            ->andWhere(['<=','date',$dateFrom])
            ->groupBy('clientId')
            ->having('sum(total) > 0');

        if ($clientId) $subQuery3->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))  $subQuery3->andWhere(['not in','clientId',$notClientId]);

        $dataDebitStart = (new Query())
            ->select("sum(total)")
            ->from(['a' => $subQuery3])
            ->scalar();

        $subQuery4 = Debt::find()
            ->select("clientId, sum(total) as total")
            ->where(['locale' => $locale])
            ->andWhere(['<=','date',$dateTo])
            ->groupBy('clientId')
            ->having('sum(total) > 0');

        if ($clientId) $subQuery4->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))  $subQuery4->andWhere(['not in','clientId',$notClientId]);

        $dataDebitEnd = (new Query())
            ->select("sum(total)")
            ->from(['a' => $subQuery4])
            ->scalar();

        $dataDebitDiff = $dataDebitEnd - $dataDebitStart;

        $dataResultStartQuery = Debt::find()
            ->select("sum(total) as total")
            ->where(['locale' => $locale])
            ->andWhere(['<=','date',$dateFrom]);

        if ($clientId) $dataResultStartQuery->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))  $dataResultStartQuery->andWhere(['not in','clientId',$notClientId]);

        $dataResultStart = $dataResultStartQuery->scalar();

        $dataResultEndQuery = Debt::find()
            ->select("sum(total) as total")
            ->where(['locale' => $locale])
            ->andWhere(['<=','date',$dateTo]);

        if ($clientId) $dataResultEndQuery->andWhere(['clientId' => $clientId]);
        if (!empty($notClientId))  $dataResultEndQuery->andWhere(['not in','clientId',$notClientId]);

        $dataResultEnd = $dataResultEndQuery->scalar();

        $dataResultDiff = $dataResultEnd - $dataResultStart;

        return compact('dataCreditStart',
            'dataCreditEnd',
            'dataCreditDiff',
            'dataDebitStart',
            'dataDebitEnd',
            'dataDebitDiff',
            'dataResultStart',
            'dataResultEnd',
            'dataResultDiff'
        );
    }

    public static function localeList() {
        /**
         * Получаем все регионы, в разрезе которых будем получать данные
         */
        return $locales = Selling::find()->select('locale')->distinct()->column();
    }
}