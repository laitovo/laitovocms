<?php
/**
 * @var View $this
 * @var array $data
 */

use yii\web\View;

?>

<style>
    table.styled tr:last-child td {
        color: black;
        font-size: larger;
    }
</style>

<div>
    <h4 class="text-center">Состояние Европейского склада</h4>
    <table class="table table-striped table-bordered table-hover styled">
        <thead>
        <tr>
            <th rowspan="2">Секции литер</th>
            <th colspan="3" class="text-center">Литеры в секции</th>
            <th colspan="4" class="text-center">Комлпекты в секции</th>
        </tr>
        <tr>
            <th>Всего</th>
            <th>Заполненных</th>
            <th>Пустых</th>
            <th>План</th>
            <th>Факт</th>
            <th>Свободно</th>
            <th>Непродающиеся</th>
        </tr>
        </thead>
        <tbody>
            <? foreach ($data as $row):?>
                <tr>
                    <td><?= $row['id']?></td>
                    <td><?= $row['literalCommonCount']?></td>
                    <td><?= $row['literalNotEmptyCount']?></td>
                    <td><?= $row['literalEmptyCount']?></td>

                    <td><?= $row['setCountMax']?></td>
                    <td><?= $row['setCountNotEmpty']?></td>
                    <td><?= $row['setCountEmpty']?></td>
                    <td><?= $row['setCountNotUsed']?></td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>
</div>

