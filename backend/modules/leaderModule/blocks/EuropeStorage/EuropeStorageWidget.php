<?php

namespace backend\modules\leaderModule\blocks\EuropeStorage;

use yii\base\Widget;
use yii\helpers\Json;
use yii\httpclient\Client;

class EuropeStorageWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('index', ['data' => $this->getData()] );
    }

    /**
     * Функция получения данных для отображения
     *
     * @return array
     */
    private function getData()
    {
        $literals = \Yii::$app->cache->get('europe_remote_storage_data') ?: [];

        if (empty($literals)) {
            $sections = [
                [1,14],
                [15,28],
                [29,42],
                [43,56],
                [57,70],
                [71,84],
                [84,150],
            ];

            foreach ($sections as $section) {
                $host = \Yii::$app->params['remoteStorage']['host'];
                $client = new Client();
                $response = $client->createRequest()
                    ->setHeaders(['Authorization' => 'Bearer ' . \Yii::$app->params['remoteStorage']['token'][2]])
                    ->setMethod('get')
                    ->setUrl("$host/kits-in-literal")
                    ->setData(['fromLiteral' => array_shift($section),'toLiteral' => array_shift($section)])
                    ->send();

                if (!empty($response->content)) {
                    $data = Json::decode($response->content);
                    if (!empty($data['literalList'])) {
                        $literals[] = $data;
                    }
                }
            }
        }


        \Yii::$app->cache->set('europe_remote_storage_data',$literals,60*60*24);

        $total = [
            'id' => '',

            'literalCommonCount' => 0,
            'literalNotEmptyCount' => 0,
            'literalEmptyCount' => 0,

            'setCountMax' => 0,
            'setCountNotEmpty' => 0,
            'setCountEmpty' => 0,
            'setCountNotUsed' => 0,
        ];

        foreach ($literals as $key => $item) {
            $section = [
                'id' => "{$item['fromLiteral']} - {$item['toLiteral']}",

                'literalCommonCount' => $item['commonLiteralsCount'],
                'literalNotEmptyCount' =>  $item['notEmptyLiteralsCount'],
                'literalEmptyCount' => $item['emptyLiteralsCount'],

                'setCountMax' => ($maxSetCount = ($key < 6 ? $item['commonLiteralsCount'] * 6 : 0)),
                'setCountNotEmpty' => ($setCountNotEmpty = ($item['count_kit'] + $item['count_no_kit'])),
                'setCountEmpty' => $maxSetCount - $setCountNotEmpty,
                'setCountNotUsed' => $item['not_used_kits'] + $item['not_used_no_kits'],
            ];

            $total['id'] = 'ИТОГО';

            $total['literalCommonCount'] += $section['literalCommonCount'];
            $total['literalNotEmptyCount'] += $section['literalNotEmptyCount'];
            $total['literalEmptyCount'] += $section['literalEmptyCount'];

            $total['setCountMax'] += $section['setCountMax'];
            $total['setCountNotEmpty'] += $section['setCountNotEmpty'];
            $total['setCountEmpty'] += $section['setCountEmpty'];
            $total['setCountNotUsed'] += $section['setCountNotUsed'];

            $result[] = $section;
        }

        $result[] = $total;

        return $result;
    }

}