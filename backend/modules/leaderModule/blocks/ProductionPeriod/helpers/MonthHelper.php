<?php


namespace backend\modules\leaderModule\blocks\ProductionPeriod\helpers;


class MonthHelper
{
    public static function get($date)
    {
        return date('d.m', $date);
    }
}