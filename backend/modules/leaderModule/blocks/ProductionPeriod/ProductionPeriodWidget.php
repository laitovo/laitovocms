<?php

namespace backend\modules\leaderModule\blocks\ProductionPeriod;

//use backend\modules\leaderModule\blocks\ProductionPeriod\helpers\ChunkHelper;
use backend\modules\leaderModule\blocks\ProductionPeriod\models\ProductionReport;
use DateTime;
use yii\base\Widget;
use Yii;

class ProductionPeriodWidget extends Widget
{
    /** @var string|null */
    private $period;
    private $days = [];
    /** @var DateTime */
    private $first_day;
    /** @var DateTime */
    private $last_day;
    /** @var string */
    private $current_year;
    private $locales = ['ru', 'de'];
    // 0 - физики, 1 - опт
    private $types = [0, 1];
    // максимальное кол-во столбцов в графике
    // private  $num_partition = 30;

    public function init()
    {
        $this->period = Yii::$app->request->get('period');
        $this->setDate();
        parent::init();
    }

    public function run()
    {
        $data = $this->getData();

        $stat_all = [
            'ru' => [],
            'de' => [],
        ];

        $stat_person = [
            'ru' => [],
            'de' => [],
        ];

        $stat_company = [
            'ru' => [],
            'de' => [],
        ];

        $summary = [
            'ru' => [],
            'de' => [],
        ];

//        $chunks = ChunkHelper::create(count($this->days), $this->num_partition);
//        $this->days = ChunkHelper::createIndex($chunks, $this->days, ChunkHelper::ACTION_NULL);
//
//        foreach ($data as $datum) {
//            foreach ($datum as $item) {
//                /** @var ServiceProductionPeriod $item */
//                $item->createChunks($chunks);
//            }
//        }

        foreach ($this->days as $day) {
            $labels[] = $day;
            foreach ($data as $locale => $datum) {
                foreach ($datum as $year => $item) {
                    /** @var ServiceProductionPeriod $item */
                    $stat_all[$locale][$year][] = $item->days_all[$day];
                    $stat_person[$locale][$year][] = $item->days_person[$day];
                    $stat_company[$locale][$year][] = $item->days_company[$day];
                    $summary[$locale]['all'][$year] = $item->sum_company + $item->sum_person;
                    $summary[$locale]['company'][$year] = $item->sum_company ;
                    $summary[$locale]['person'][$year] = $item->sum_person;
                }
            }
        }

        $period = $this->first_day->format('Y-m-d') . ' - ' . $this->last_day->format('Y-m-d');
        $data = compact('stat_all', 'stat_person', 'stat_company', 'summary', 'period', 'labels');
        $data['first_day'] = $this->first_day->format('Y-m-d');
        $data['last_day'] = $this->last_day->format('Y-m-d');
        return $this->render('index', $data);
    }

    private function getData()
    {
        $start_year = $this->current_year - 2;

        $first_day = $this->first_day->format('m-d');
        $last_day = $this->last_day->format('m-d');

        $first_day_int = $this->first_day->getTimestamp();
        $last_day_int = $this->last_day->getTimestamp();

        $format = $this->period == 'year' ? 'F' : 'd.m';

        $index = $this->first_day->format($format);;
        $this->days[$index] = $index;
        for ($i = $first_day_int; $i < $last_day_int;) {
            $i += 3600 * 24;
            $index = date($format, $i);
            $this->days[$index] = $index;
        }


        $result = [];
        $days_arr = $this->days;
        for ($year = $start_year; $year <= $this->current_year; $year++) {
            foreach ($this->locales as $locale) {
                foreach ($this->types as $type) {
                    $days = ProductionReport::find()
                        ->select('SUM(`sum`) AS sum, type, date')
                        ->where(['locale' => $locale])
                        ->andWhere(['type' => $type])
                        ->andWhere(['>=', 'date', strtotime($year . '-' . $first_day)])
                        ->andWhere(['<=', 'date', strtotime($year . '-' . $last_day)])
                        ->groupBy('date')
                        ->asArray()
                        ->all();
                    $service = $result[$locale][$year] ?? new ServiceProductionPeriod();
                    $service->type = (boolean)$type;
                    $result[$locale][$year] = $service->setData(compact('year', 'days', 'days_arr','format'));
                }
            }
        }

        return $result;
    }

    private function setDate()
    {
        $this->current_year = date('Y', time());

        $first_day_post = Yii::$app->request->post('first_day');
        $last_day_post = Yii::$app->request->post('last_day');

        if($first_day_post && $last_day_post) {
            $this->first_day = new DateTime($first_day_post);
            $this->last_day = new DateTime($last_day_post);
            return;
        }

        if($this->period == 'prev_month') {
            $first_day = new DateTime('first day of previous month');
            $last_day = new DateTime('last day of previous month');
            $this->first_day = new DateTime($first_day->format('Y-m-d'));
            $this->last_day = new DateTime($last_day->format('Y-m-d'));
            return;
        }

        if($this->period == 'year') {
            $this->current_year = date('Y', time());
            $this->first_day = new DateTime($this->current_year . '-01-01');
            $this->last_day = new DateTime($this->current_year . '-12-31');
            return;
        }

        if($this->period == 'prev_year') {
            $this->current_year = date('Y', strtotime('-1 year'));
            $this->first_day = new DateTime($this->current_year . '-01-01');
            $this->last_day = new DateTime($this->current_year . '-12-31');
            return;
        }

        $this->first_day = new DateTime((new DateTime('first day of this month'))->format('Y-m-d'));
        $this->last_day = new DateTime(date('Y-m-d', time()));
    }
}