<?php

namespace backend\modules\leaderModule\blocks\ProductionPeriod;


use backend\modules\leaderModule\blocks\ProductionPeriod\helpers\ChunkHelper;

class ServiceProductionPeriod
{
    /** @var int */
    public $year;
    /** @var float */
    public $sum_company = 0;
    /** @var float */
    public $sum_person = 0;
    /** @var array */
    public $days_all = [];
    /** @var array */
    public $days_company = [];
    /** @var array */
    public $days_person = [];
    /** @var bool */
    public $type;

    public function setData(array $data)
    {
        $this->year = $data['year'];
        $days_person = [];
        $days_company = [];

        foreach ($data['days'] as $day) {
            $sum = round($day['sum'], 2);
            $index = date($data['format'], $day['date']);
            if($this->type == 0) {
                isset($days_person[$index]) ? $days_person[$index] += $sum : $days_person[$index] = $sum;
                $this->sum_person += $sum;
            } else {
                isset($days_company[$index]) ? $days_company[$index] += $sum : $days_company[$index] = $sum;
                $this->sum_company += $sum;
            }
            if(!isset($this->days_all[$index])) {
                $this->days_all[$index] = round($day['sum'], 2);
            } else {
                $this->days_all[$index] += round($day['sum'], 2);
            }
        }

        foreach ($data['days_arr'] as $date) {
            if($this->type == 0) {
                $this->days_person[$date] = $days_person[$date] ?? 0;
            } else {
                $this->days_company[$date] = $days_company[$date] ?? 0;
            }
            if(!isset($this->days_all[$date])) {
                $this->days_all[$date] = 0;
            }
        }

        return $this;
    }

    public function createChunks(array $chunks)
    {
        $this->days_person = ChunkHelper::createIndex($chunks, $this->days_person, ChunkHelper::ACTION_SUM);
        $this->days_company = ChunkHelper::createIndex($chunks, $this->days_company, ChunkHelper::ACTION_SUM);
        $this->days_all = ChunkHelper::createIndex($chunks, $this->days_all, ChunkHelper::ACTION_SUM);
    }
}