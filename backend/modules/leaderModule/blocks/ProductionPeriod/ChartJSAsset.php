<?php

namespace backend\modules\leaderModule\blocks\ProductionPeriod;

use yii\web\AssetBundle;

class ChartJSAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/chartjs';
    public $js = ['dist/Chart.js'];
}