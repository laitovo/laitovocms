<?php
/**
 * @var \yii\web\View $this
 * @var array $stat_all
 * @var array $stat_person
 * @var array $stat_company
 * @var array $summary
 * @var array $labels
 * @var string $period
 * @var string $first_day
 * @var string $last_day
 */

use kartik\date\DatePicker;
use yii\helpers\Html;

$css = '
.grey-report {
    background-color: rgba(212, 212, 212, 0.84);
}
.red-report {
    background-color: rgba(212, 11, 23, 0.88);
}
.blue-report {
    background-color: rgba(6, 77, 212, 0.81);
}
.two-block, .third-block {
    padding-top: 40px;
}

.summary1 {
    width: 150px;
}

.summary2 {
    width: 100px;
}

.summary3 {
    width: 50px;
}

.summary1, .summary2, .summary3 {
    height: 15px;
    display: inline-block;
    margin-right: 20px;
}
.datepicker .disabled.day {
    visibility: hidden;
}';

$this->registerCss($css);
$currentDate = date('z', time());
$endDate = date('L', time()) == 0 ? 364 - $currentDate : 365 - $currentDate;
?>

<div class="row">
    <?= Html::beginForm(['index']) ?>
    <div class="col-md-2">
        <?= DatePicker::widget([
            'name' => 'first_day',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => $first_day,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true,
                'startDate' => '-' . $currentDate . 'd',
                'endDate' => '+' . $endDate . 'd',
            ],
            'options' => [
                'placeholder' => 'Поле От:',
                'autocomplete' => 'off',
                'required' => true
            ]
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= DatePicker::widget([
            'name' => 'last_day',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => $last_day,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true,
                'startDate' => '-' . $currentDate . 'd',
                'endDate' => '+' . $endDate . 'd',
            ],
            'options' => [
                'placeholder' => 'Поле До:',
                'autocomplete' => 'off',
                'required' => true
            ]
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::submitButton('Выбрать период', ['class' => 'btn btn-success']) ?>
    </div>
    <?= Html::endForm() ?>
    <div class="col-md-6">
        <?= Html::a('Текущий месяц', ['index'], ['class' => 'btn btn-info']) ?> |
        <?= Html::a('Предыдущий месяц', ['index', 'period' => 'prev_month'], ['class' => 'btn btn-warning prev-month']) ?> |
        <?= Html::a('Год', ['index', 'period' => 'year'], ['class' => 'btn btn-danger year-report']) ?> |
        <?= Html::a('Предыдущий Год', ['index', 'period' => 'prev_year'], ['class' => 'btn btn-primary year-report']) ?>
    </div>

</div>

<h3 class="text-center">Поступившие, неотмененные заказы</h3>
<?php foreach ($stat_all as $locale => $data) : ?>

    <div class="report">

        <h3 style="padding-top: 30px; text-align: center;"><?= strtoupper($locale) ?></h3>

        <h4 style="text-align: center;">Период: <?= $period ?></h4>

        <div class="first-block">
            <?= $this->render('production-period', compact('labels', 'data', 'locale')) ?>
        </div>

        <div class="two-block">
            <?= $this->render('summary', [
                'summary' => $summary[$locale]['all'],
                'locale' => $locale
            ]) ?>
        </div>

        <div class="third-block">
            <div class="row">
                <div class="col-md-6">
                    <h3>Физические лица</h3>
                    <?= $this->render('production-period', [
                        'labels' => $labels,
                        'data' => $stat_person[$locale],
                        'locale' => $locale
                    ]) ?>

                    <?= $this->render('summary_small', [
                        'summary' => $summary[$locale]['person'],
                        'locale' => $locale
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <h3>Опт</h3>
                    <?= $this->render('production-period', [
                        'labels' => $labels,
                        'data' => $stat_company[$locale],
                        'locale' => $locale
                    ]) ?>

                    <?= $this->render('summary_small', [
                        'summary' => $summary[$locale]['company'],
                        'locale' => $locale
                    ]) ?>
                </div>
            </div>

        </div>
    </div>
<?php endforeach ?>
<hr>
<br>








