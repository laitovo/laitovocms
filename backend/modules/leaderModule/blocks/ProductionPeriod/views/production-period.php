<?php
/**
 * @var \yii\web\View $this
 * @var array $data
 * @var array $labels
 * @var string $locale
 */

use backend\modules\leaderModule\blocks\ProductionPeriod\ChartWidget;

$colors = require dirname(__DIR__) . '/params/color.php';

$dataWidget = [
    'labels' => $labels,
    'datasets' => []
];

foreach ($data as $year => $datum) {
    $dataWidget['datasets'][] = [
        'data' => $datum,
        'label' => "Заказы за $year год",
        'backgroundColor' => $colors[$year]['back'],
        'borderColor' => $colors[$year]['border'],
    ];
}

echo ChartWidget::widget([
    'data' => $dataWidget,
    'currency' => $locale == 'ru' ? 'RUB' : 'EUR'
]);



