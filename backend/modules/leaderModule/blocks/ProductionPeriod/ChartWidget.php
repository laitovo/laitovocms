<?php

namespace backend\modules\leaderModule\blocks\ProductionPeriod;

use yii\bootstrap\Widget;
use yii\helpers\Json;

class ChartWidget extends Widget
{
    const TYPE_LINE = 'line';
    const TYPE_BAR = 'bar';
    const TYPE_RADAR = 'radar';
    const TYPE_POLAR_AREA = 'polarArea';
    const TYPE_PIE = 'pie';
    const TYPE_DOUGHNUT = 'doughnut';
    const TYPE_BUBBLE = 'bubble';

    public $type;
    public $data = [];
    public $options = [];
    public $currency;

    public function init()
    {
        parent::init();
        $this->type = $this->type ?: self::TYPE_BAR;
        $this->data = Json::encode($this->data);
        $this->options = Json::encode($this->options);
    }

    public function run()
    {
        $this->registerScript();
        echo "<canvas id='$this->id'></canvas>";
    }

    public function registerScript()
    {
        $view = $this->getView();
        ChartJSAsset::register($view);
        $js = "
        $(function() {
            let ctx = $('#{$this->id}');
            let formatter = new Intl.NumberFormat('ru-RU', {
                style: 'currency',
                currency: '{$this->currency}',
            });            
            new Chart(ctx, {
                type: '{$this->type}',
                data: {$this->data},
                options: {
                    tooltips: {
                        mode: 'label',
                        callbacks: {
                            label: function (tooltipItem) {
                                console.log(tooltipItem);
                                return formatter.format(tooltipItem.yLabel);
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                callback: function (value, index, values) {
                                    let num = Intl.NumberFormat('ru-RU').format(value / 1000);
                                    return num + ' тыс.';
                                }
                            }
                        }],
                    }
                }
            });
        });
        ";
        $view->registerJs($js);
    }
}