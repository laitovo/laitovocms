<?php

namespace backend\modules\leaderModule\models;

use common\models\user\User;

/**
 * This is the model class for table "leader_module_users".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $block_id
 * @property integer $sort
 * @property bool $active
 * @property Module $module
 * @property User $user
 */
class UserModule extends BaseActiveRecord
{
    use Sort;

    public $module_arr_all = [];
    public $module_arr_current = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leader_module_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'block_id'], 'required'],
            [['user_id', 'block_id', 'sort'], 'integer'],
            ['module_arr_current', 'safe'],
            [['active'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'Выберите пользователя',
            'module_arr_current' => 'Модули для формирования отчета'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Module::class, ['id' => 'block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserModule::className(), ['id' => 'user_id']);
    }

    public static function getUserArr()
    {
        $query = User::find()->select(['name', 'id'])->indexBy('id');
        if ($ids = self::find()->select('user_id')->distinct()->column()) {
            $query->where(['not in', 'id', $ids]);
        }
        return $query->column();
    }

    public static function getModuleArrAll()
    {
        return Module::find()
            ->select(['title', 'id'])
            ->indexBy('id')
            ->column();
    }

    public static function getModuleArrCurrent($user_id = null)
    {
        $model = new self;
        if ($user_id) {
            $model->user_id = $user_id;
            $model->module_arr_current = Module::find()
                ->alias('mb')
                ->select(['mb.id'])
                ->joinWith('userModule um')
                ->where(['um.user_id' => $user_id])
                ->orderBy('um.sort')
                ->column();
        }

        return $model;
    }
}
