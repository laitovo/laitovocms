<?php

namespace backend\modules\leaderModule\models;

/**
 * This is the model class for table "leader_module_blocks".
 *
 * @property integer $id
 * @property string $description
 * @property string $widget
 * @property string $title
 * @property UserModule[] $userModule
 */
class Module extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leader_module_blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'widget'], 'required'],
            [['title', 'description', 'widget'], 'string', 'max' => 255],
            ['widget', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModule()
    {
        return $this->hasMany(UserModule::class, ['block_id' => 'id']);
    }
}
