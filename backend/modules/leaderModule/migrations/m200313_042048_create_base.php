<?php

use yii\db\Migration;

class m200313_042048_create_base extends Migration
{
    private $user_table = '{{%leader_module_users}}';
    private $items = '{{%leader_module_blocks}}';
    private $tableOptions;

    public function safeUp()
    {
        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->items, [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
        ], $this->tableOptions);

        $this->createTable($this->user_table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'block_id' => $this->integer(),
            'sort' => $this->smallInteger(3)
        ], $this->tableOptions);

        $this->addForeignKey('fk-leader_module_users_user-id', $this->user_table, 'user_id', '{{%user}}', 'id', 'CASCADE');

        $this->addForeignKey('fk-leader_module_users_block-id', $this->user_table, 'block_id', $this->items, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable($this->user_table);
        $this->dropTable($this->items);
    }
}
