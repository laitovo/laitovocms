<?php


class m200317_074848_production extends \yii\db\Migration
{
    protected $table = '{{%leader_module_production_report}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'date' => $this->integer(),
            'locale' => $this->char(2),
            'sum' => $this->float(),
            'type' => $this->boolean()->comment('Оптовик - 1, физик - 0'),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
