<?php

use yii\db\Migration;

class m200314_145904_change_leader_module extends Migration
{
    private $user_table = '{{%leader_module_users}}';
    private $items = '{{%leader_module_blocks}}';

    public function safeUp()
    {
        $this->addColumn($this->user_table, 'active', $this->boolean());
        $this->addColumn($this->items, 'description', $this->string());
        $this->addColumn($this->items, 'widget', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->user_table, 'active');
        $this->dropColumn($this->items, 'description');
        $this->dropColumn($this->items, 'widget');
    }
}
