<?php

use yii\db\Migration;
use backend\modules\leaderModule\blocks\ProductionPeriod\models\ProductionReport;
class m200327_074845_add_order_id extends Migration
{
    protected $table = '{{%name_table}}';
    protected $tableOptions;

    public function safeUp()
    {
        $this->addColumn(ProductionReport::tableName(), 'order_id', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn(ProductionReport::tableName(), 'order_id');
    }
}
