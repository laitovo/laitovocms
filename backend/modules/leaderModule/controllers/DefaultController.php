<?php

namespace backend\modules\leaderModule\controllers;

use backend\modules\leaderModule\models\Module;

/**
 * Контроллер монитора руководителя
 *
 * Class DefaultController
 * @package backend\modules\leaderModule\controllers
 */
class DefaultController extends BaseController
{
    /**
     * Рендерит основное представление монитора руководителя.
     * [ Для всех модулей, доступных пользователю, вызывает отрисовку ( рендеринг )
     * и соединяем в едниый готовый ответ ( конкотенируем ) ]
     *
     * @return string
     */
    public function actionIndex()
    {
        $modules = Module::find()->select('widget')->where(['id' => $this->user_modules])->column();
        $html = null;
        if($modules) {
            foreach ($modules as $module) {
                $html .= $module::widget();
            }
        }
        return $this->render('index', ['html' => $html]);
    }
}
