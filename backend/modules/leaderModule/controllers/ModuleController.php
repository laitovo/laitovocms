<?php

namespace backend\modules\leaderModule\controllers;

use backend\modules\leaderModule\models\Module;
use backend\modules\leaderModule\models\UserModule;
use yii\filters\VerbFilter;
use Yii;

class ModuleController extends BaseController
{
    /**
     * @var Module
     */
    private $modelModule;
    /**
     * @var UserModule
     */
    private $userModule;

    public function __construct($id, $module, Module $modelModule, UserModule $userModule)
    {
        parent::__construct($id, $module);
        $this->modelModule = $modelModule;
        $this->userModule = $userModule;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $models = $this->modelModule::find()->all();
        return $this->render('index', compact('models'));
    }

    public function actionView($id)
    {
        return $this->render(
            'view', [
                'model' => $this->modelModule->getItem($id),
            ]
        );
    }

    public function actionCreate()
    {
        $model = $this->modelModule;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'create', [
                    'model' => $model,
                ]
            );
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->modelModule->getItem($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'update', [
                    'model' => $model,
                ]
            );
        }
    }

    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = $this->modelModule->getItem($id);
            if ($items = $this->userModule::find()->where(['block_id' => $model->id])->all()) {
                /** @var UserModule $item */
                foreach ($items as $item) {
                    $this->userModule->deleteSortItem('sort', $item->sort, 'user_id', $item->user_id);
                }
            }
            $model->delete();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }
}