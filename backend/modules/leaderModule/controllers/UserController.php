<?php

namespace backend\modules\leaderModule\controllers;

use backend\modules\leaderModule\models\UserModule;
use common\models\user\User;
use yii\filters\VerbFilter;
use Yii;
use yii\web\NotFoundHttpException;

class UserController extends BaseController
{

    /**
     * @var UserModule
     */
    private $userModule;

    public function __construct($id, $module, UserModule $userModule)
    {
        parent::__construct($id, $module);
        $this->userModule = $userModule;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $ids = [];
        $users = [];
        $modules = [];

        /** @var UserModule[] $modules_arr */
        $modules_arr = $this->userModule::find()
            ->alias('um')
            ->joinWith('module m')
            ->orderBy('um.sort')
            ->all();

        if ($modules_arr) {
            foreach ($modules_arr as $item) {
                $ids[] = $item->user_id;
                $modules[$item->user_id][] = $item->module->title;
            }

            $users = User::find()
                ->select('name, id')
                ->where(['id' => $ids])
                ->indexBy('id')
                ->column();
        }

        return $this->render('index', compact('users', 'modules'));
    }

    public function actionView($user_id)
    {
        return $this->render('view', compact('user_id'));
    }

    public function actionCreate()
    {
        $model = clone $this->userModule;

        $user_arr = $model::getUserArr();

        $model->module_arr_all = $model::getModuleArrAll();

        $user_name = null;

        if ($model->load(Yii::$app->request->post())) {
            $sort = 1;
            foreach ($model->module_arr_current as $item) {
                $new_model = clone $this->userModule;
                $new_model->user_id = $model->user_id;
                $new_model->block_id = $item;
                $new_model->sort = $sort;
                $new_model->active = 1;
                $new_model->saveItem();
                $sort++;
            }
            return $this->redirect(['index', 'user_id' => $model->user_id]);
        }
        return $this->render(
            'create', compact('model', 'user_arr', 'user_name')
        );
    }

    public function actionUpdate($user_id)
    {
        $user_name = User::find()->select('name')->where(['id' => $user_id])->scalar();
        $model = $this->userModule::getModuleArrCurrent($user_id);
        $user_arr = [];
        $model->module_arr_all = $model::getModuleArrAll();
        $start_arr = $model->module_arr_current;

        if ($model->load(Yii::$app->request->post())) {
            // ищем эл-ты, которые есть в БД, но нет в новом текущем массиве, и удаляем их
            $diff_old = array_diff($start_arr, $model->module_arr_current);
            if ($diff_old) {
                $this->userModule::deleteAll(['user_id' => $user_id, 'block_id' => $diff_old]);
                foreach ($diff_old as $key => $item) {
                    unset($start_arr[$key]);
                }
            }

            // ищем новые эл-ты в текущем массиве
            $diff_new = array_diff($model->module_arr_current, $start_arr);
            $sort = 1;
            if ($diff_new) {
                // добавляем в начало списка (мы выше объявили, что $sort = 1)
                foreach ($diff_new as $item) {
                    $new_model = clone $this->userModule;
                    $new_model->user_id = $model->user_id;
                    $new_model->block_id = $item;
                    $new_model->sort = $sort;
                    $new_model->active = 1;
                    $new_model->saveItem();
                    $sort++;
                }
            }
            if($start_arr) {
                // перестраиваем сортировку у старых эл-тов
                foreach ($start_arr as $key => $item) {
                    $this->userModule::updateAll(['sort' => $sort], ['block_id' => $item, 'user_id' => $model->user_id]);
                    $sort++;
                }
            }

            return $this->redirect(['index', 'user_id' => $model->user_id]);
        }

        return $this->render(
            'update', compact('model', 'user_arr', 'user_name')
        );
    }

    public function actionChangeActive($id, $active)
    {
        $this->userModule::updateAll(['active' => $active], ['id' => $id]);
    }

    public function actionDelete($user_id)
    {
        $this->userModule::deleteAll(['user_id' => $user_id]);
        return $this->redirect(['index']);
    }
}