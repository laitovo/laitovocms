<?php

namespace backend\modules\leaderModule\controllers;

use backend\modules\leaderModule\models\Module;
use backend\modules\leaderModule\models\UserModule;
use common\models\user\User;
use yii\web\Controller;
use Yii;
use yii\web\ForbiddenHttpException;

class BaseController extends Controller
{
    /** @var User */
    protected $user_modules;

    /**
     * Инициализация базового контроллера, которая выполняет следующие задачи
     *  - Собрать все доступные для пользователя модули
     *  - Если для пользовтеля, который зашел в отчет не выделено ни одного модуля, значит ему запрещен доступ
     *
     * @throws ForbiddenHttpException
     */
    public function init()
    {
        if (!Yii::$app->user->identity->isRoot()) {
            $user_id = Yii::$app->user->identity->getId();
            if (!UserModule::find()->where(['user_id' => $user_id])->exists()) {
                throw new ForbiddenHttpException();
            }
            $this->user_modules = UserModule::find()->select('block_id')->where(['user_id' => $user_id])->column();
        } else {
            $this->user_modules = Module::find()->select('id')->column();
        }
        parent::init();
    }
}