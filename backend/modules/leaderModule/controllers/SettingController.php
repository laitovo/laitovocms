<?php

namespace backend\modules\leaderModule\controllers;

use backend\modules\leaderModule\models\Module;
use backend\modules\leaderModule\models\UserModule;
use common\models\user\User;
use Yii;

class SettingController extends BaseController
{
    /**
     * @var Module
     */
    private $modelModule;
    /**
     * @var UserModule
     */
    private $userModule;

    public function __construct($id, $module, Module $modelModule, UserModule $userModule)
    {
        parent::__construct($id, $module);
        $this->modelModule = $modelModule;
        $this->userModule = $userModule;
    }

    public function actionIndex($user_id)
    {
        $user_name = User::find()->select('name')->where(['id' => $user_id])->scalar();
        $models = $this->modelModule::find()
            ->alias('m')
            ->joinWith('userModule um')
            ->where(['um.user_id' => $user_id])
            ->orderBy('um.sort')
            ->all();

        return $this->render('index', compact('models', 'user_name'));
    }

    public function actionSort()
    {
        $data = new \stdClass();
        $data->id = Yii::$app->request->post('id');
        $data->sort = Yii::$app->request->post('sort');
        $this->userModule->changeSort($data, 'sort', 'user_id');
    }
}