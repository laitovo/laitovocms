<?php

/**
 * @var yii\web\View $this
 * @var backend\modules\leaderModule\models\UserModule $model
 * @var array $user_arr
 * @var string $user_name
 */

$this->title = 'Update Leader Module User';
$this->params['breadcrumbs'][] = ['label' => 'Leader Module Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="leader-module-user-form">
    <?= $this->render('_form', compact('model', 'user_name', 'user_arr')) ?>
</div>

