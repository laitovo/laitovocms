<?php
/**
 * @var $this yii\web\View
 * @var $model backend\modules\leaderModule\models\UserModule
 * @var $form yii\widgets\ActiveForm
 * @var array $user_arr
 * @var array $modules_arr
 * @var string $user_name
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>

<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin(); ?>

        <?php if (!$model->user_id) : ?>
            <?= $form->field($model, 'user_id')->widget(Select2::class, [
                'data' => $user_arr,
                'model' => $model,
                'language' => 'ru',
                'options' => ['placeholder' => '...'],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]) ?>
        <?php else : ?>
            <h4><?= $user_name ?></h4>
            <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
        <?php endif ?>

        <?= $form->field($model, 'module_arr_current')->widget(Select2::class, [
            'model' => $model,
            'language' => 'ru',
            'data' => $model::getModuleArrAll(),
            'options' => ['placeholder' => '...', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->user_id ? 'Update' : 'Create', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

