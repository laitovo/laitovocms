<?php
/**
 * @var \yii\web\View $this
 * @var array $users
 * @var array $modules
 */

use yii\helpers\Html;
use core\helpers\StringHelper;

$this->title = 'Пользователи модулей для руководителя';
$this->params['breadcrumbs'][]['label'] = $this->title;

$js = "
$(function(){
    $('.hide_show').click(function(e){
        e.preventDefault();
        $(this).next().toggle();
    });
});
";
$this->registerJs($js, \yii\web\View::POS_END);
?>
<style>
    .table.table-bordered td {
        vertical-align: middle;
    }
</style>
<p>
    <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<h3>Созданные пользователи</h3>
<div class="row">
    <div class="col-md-12">
        <?php if ($users) : ?>
            <table class="table table-bordered">
                <?php foreach ($users as $user_id => $user_name) : ?>
                    <tr>
                        <td>
                            <p><?= $user_name ?></p>
                            <p>
                                <?= Html::a('Перейти к отчету', ['/leader-module/default', 'user_id' => $user_id]) ?>
                            </p>
                            <p>
                                <?= Html::a('Перейти к настройкам', ['/leader-module/setting', 'user_id' => $user_id]) ?>
                            </p>
                        </td>
                        <td>
                            <?= Html::a('Модули пользователя', '#', ['class' => 'hide_show']) ?>
                            <div style="padding-top: 10px; display: none" class="hide_block">
                                <?php foreach ($modules[$user_id] as $item) : ?>
                                    <p><?= $item ?></p>
                                <?php endforeach ?>
                            </div>
                        </td>
                        <td>
                            <?= Html::a(
                                '<i class="fa fa-refresh"></i>',
                                ['update', 'user_id' => $user_id]
                            ) ?>
                        </td>
                        <td>
                            <?= Html::a(
                                '<i class="fa fa-trash"></i>',
                                ['delete', 'user_id' => $user_id], [
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => 'Вы уверены?',
                                        'title' => 'Удалить пользователя'
                                    ]
                                ]
                            ) ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </table>
        <?php else : ?>
            <h4>Users not found</h4>
        <?php endif ?>
    </div>
</div>


