<?php
/**
 * @var \yii\web\View $this
 * @var string $html
 */
$this->title = 'Монитор руководителя';
$this->params['breadcrumbs'][]['label'] = $this->title;
?>
<div class="user-default-index">
    <?= $html ?>
</div>

