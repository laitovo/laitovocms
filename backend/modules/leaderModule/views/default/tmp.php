<?php
/**
 * @var \yii\web\View $this
 * @var array $stat_all
 * @var array $stat_person
 * @var array $stat_company
 * @var array $summary
 * @var string $period
 * @var string $labels
 */

use backend\modules\leaderModule\widgets\chart\ChartWidget;
use backend\modules\leaderModule\widgets\handlers\ProductionPeriod\aseets\Asset;

Asset::register($this);

$dataWeatherTwo = [
    'labels' => $labels,
    'datasets' => [
        [
            'data' => $stat_all['ru'][2018],
            'label' => "Продажи за 2018 год",
            'fill' => true,
            'lineTension' => 0.1,
            'backgroundColor' => "rgba(6, 77, 212, 0.8)",
            'borderColor' => "rgba(6, 77, 212, 1)",
            'stack' => 'qwerty'
        ],
        [
            'data' => $stat_all['ru'][2019],
            'label' => "Продажи за 2019 год",
            'fill' => true,
            'lineTension' => 0.1,
            'backgroundColor' => "rgba(212, 11, 23, 0.8)",
            'borderColor' => "rgba(212, 11, 23, 1)",
            'stack' => 'qwerty'
        ],
        [
            'data' => $stat_all['ru'][2020],
            'label' => "Продажи за 2020 год",
            'fill' => true,
            'lineTension' => 0.1,
            'backgroundColor' => "rgba(212, 212, 212, 0.8)",
            'borderColor' => "rgba(212, 212, 212, 1)",
            'stack' => 'qwerty'
        ]
    ]
];

echo ChartWidget::widget([
    'type' => ChartWidget::TYPE_BAR,
    'data' => $dataWeatherTwo,
    'options' => [
        'scales' => [
            'xAxes' => [
                'stacked' => true
            ]
        ],
    ]
]);









