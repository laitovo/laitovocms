<?php
/**
 * @var yii\web\View $this
 * @var backend\modules\leaderModule\models\Module $model
 */

$this->title = 'Create Leader Modules';
$this->params['breadcrumbs'][] = [
    'label' => 'Leader Modules', 'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leader-module-block-create">
    <?= $this->render(
        '_form', [
            'model' => $model,
        ]
    ) ?>

</div>
