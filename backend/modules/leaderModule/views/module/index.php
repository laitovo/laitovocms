<?php
/**
 * @var yii\web\View $this
 * @var backend\modules\leaderModule\models\Module[] $models
 */

use yii\helpers\Html;

$this->title = 'Модули для руководителя';
$this->params['breadcrumbs'][]['label'] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Создать модуль', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <h3>Созданные модули</h3>
        <table class="table table-bordered">
            <?php foreach ($models as $model) : ?>
                <tr>
                    <td><?= $model->title ?></td>
                    <td><?= $model->description ?></td>
                    <td><?= $model->widget ?></td>
                    <td>
                        <?= Html::a(
                            '<i class="fa fa-refresh"></i>',
                            ['update', 'id' => $model->id]
                        ) ?>
                    </td>
                    <td>
                        <?= Html::a(
                            '<i class="fa fa-trash"></i>',
                            ['delete', 'id' => $model->id], [
                                'data' => [
                                    'method' => 'post',
                                    'confirm' => 'Вы уверены?',
                                    'title' => 'Удалить модуль'
                                ]
                            ]
                        ) ?>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>
