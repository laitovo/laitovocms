<?php

/**
 * @var yii\web\View $this
 * @var backend\modules\leaderModule\models\Module $model
 */

$this->title = 'Update Leader Module: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Leader Module Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="leader-module-block-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
