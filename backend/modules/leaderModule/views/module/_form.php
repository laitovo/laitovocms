<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\leaderModule\models\Module */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-4">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title')->textInput(
            ['maxlength' => true]
        ) ?>

        <?= $form->field($model, 'widget')->textInput(
            ['maxlength' => true]
        ) ?>

        <?= $form->field($model, 'description')->textInput(
            ['maxlength' => true]
        ) ?>

        <div class="form-group">
            <?= Html::submitButton(
                'Ok', ['class' => 'btn btn-primary']
            ) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

