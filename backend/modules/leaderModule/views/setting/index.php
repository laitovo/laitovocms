<?php
/**
 * @var \yii\web\View $this
 * @var \backend\modules\leaderModule\models\Module[] $models
 * @var string $user_name
 */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Настройки модулей для руководителя';
$this->params['breadcrumbs'][]['label'] = $this->title;

$js = "
$(function() {
    $('.btn.no-active').hide();
    $('.change-active').click(function() {
        $(this).hide();
        $(this).siblings('.change-active').show();
        $.post($(this).data('url'));
    });
});
$(function () {
    let tableSortable = $('.sortable-table tbody');
    let item, url;
    if (tableSortable.length === 1) {
        item = tableSortable;
        url = tableSortable.parent('.sortable-table').data('url');
    }
    if (item) {
        item.sortable({
            containment: 'parent',
            cursor: 'move',
            update: updateHandler,
            delay: 200
        });

        function updateHandler(event, ui) {
            $.post(url, {id: ui.item.data('id'), sort: ui.item.index() + 1});
        }
    }
});
";
$this->registerJs($js, \yii\web\View::POS_END);
?>

<h3><?= $user_name ?></h3>
<p>Для сортировки модулей перетаскиваем мышкой строки таблицы</p>
<?php if($models) : ?>
    <table data-url="<?= Url::to(['sort']) ?>"
           class="table table-bordered sortable-table">
        <tbody>
        <?php foreach ($models as $model) : ?>
            <tr data-id="<?= $model->userModule[0]->id ?>">
                <td><?= $model->title ?></td>
                <td><?= $model->description ?></td>
                <td style="width: 150px;">
                    <?php $class = $model->userModule[0]->active ? 'active' : 'no-active'; ?>
                    <?= Html::button(
                        'Active',
                        [
                            'class' => 'btn btn-success change-active ' . $class,
                            'data' => [
                                'url' => Url::to([
                                    '/leader-module/user/change-active',
                                    'id' => $model->userModule[0]->id,
                                    'active' => 0
                                ])
                            ]
                        ]
                    ) ?>
                    <?php $class = !$model->userModule[0]->active ? 'active' : 'no-active'; ?>
                    <?= Html::button(
                        'No Active',
                        [
                            'class' => 'btn btn-danger change-active ' . $class,
                            'data' => [
                                'url' => Url::to([
                                    '/leader-module/user/change-active',
                                    'id' => $model->userModule[0]->id,
                                    'active' => 1
                                ])
                            ]
                        ]
                    ) ?>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
<?php else : ?>
    <h4>Модули не найдены</h4>
<?php endif ?>

