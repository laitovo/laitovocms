<?php

namespace backend\modules\leaderModule\widgets;

use yii\jui\Widget;
use yii\helpers\Url;
use Yii;

class Menu extends Widget
{
    public function run()
    {
        $moduleId = Yii::$app->controller->module->id;
        $controller_id = Yii::$app->controller->id;
        $id = Yii::$app->user->identity->getId();
        $menu = [
            [
                'url' => Url::to(["/$moduleId/default/index", 'user_id' => $id]),
                'label' => 'Просмотр модулей',
                'active' => $controller_id == 'default' ? 'active' : null
            ],
            [
                'url' => Url::to(["/$moduleId/setting/index", 'user_id' => $id]),
                'label' => 'Настройка модулей',
                'active' => $controller_id == 'setting' ? 'active' : null
            ],
        ];

        Yii::$app->view->params['menuItems'][0]['label']
            = '<span class="lead">Отчеты для руководителя</span>';

        foreach ($menu as $item) {
            Yii::$app->view->params['menuItems'][] = $item;
        }

        if (Yii::$app->user->identity->isRoot()) {
            Yii::$app->view->params['menuItems'][] = [
                'label' => 'Администрирование',
                'items' => [
                    [
                        'label' => 'Users',
                        'url' => Url::to("/$moduleId/user/index"),
                        'active' => $controller_id == 'user' ? 'active' : null
                    ],
                    [
                        'label' => 'Modules',
                        'url' => Url::to("/$moduleId/module/index"),
                        'active' => $controller_id == 'module' ? 'active' : null
                    ]
                ]
            ];
        }
    }
}