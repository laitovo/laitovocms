<?php

namespace backend\modules\leaderModule;


/**
 * leader-module module definition class
 */
class LeaderModule extends \yii\base\Module
{
    public function init()
    {
        \Yii::$app->layout = '2columns';
        parent::init();
    }
}
