<?php
namespace returnModule\config;

use yii\base\Component;

class ReturnConfig extends Component
{
    /** @var string токен пользователя с помощью которого мы будем общаться с основным приложением */
    public $ownerUserToken;
    /** @var array  Если у пользователя админки одна из этих ролей, ему можно добавлять др. пользователей в систему возврата и определять их роли (кладовщик, техподдержка, менеджер ...)*/
    public $mainRoles = [
        'root',
        'superadmin',
        'admin',
    ];
    /** @var string нужен в для формирования меню */
    public $moduleId;
    /** @var bool */
    public $use_ale10257_translate = false;
    /** @var string */
    public $path_main_layout;
    /** @var string */
    public $apiDataOrderUrl;
    /** @var string */
    public $token;
    /** @var string */
    public $ownerUrlGetUpn;
    /** @var string */
    public $setBadUser;
}