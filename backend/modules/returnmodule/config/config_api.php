<?php
return [
    /**
     * Вместо http://localhost/return/ в dev и prod окружениях должны быть реальные адреса
     * Не теряем слеш в конце адреса!
     */
    'apiDataOrderUrl' => $_ENV['HOST_LAITOVO_RU'] . '/return/get/',
    'setBadUser' => $_ENV['HOST_LAITOVO_RU'] . '/return/setBadUser',
    /**
     * поле ownerUrlGetUpn
     * ожидается массив
     [
        'order_number' => 2290469,
        'upn' => 111,
        'article' => 'FD-F-123-1-8',
     ];
     */
    'ownerUrlGetUpn' => '/ajax/order-by-upn',
    // токен реальный для laitovo.ru(eu)
    'token' => $_ENV['RETURN_MODULE_LAITOVO_RU_TOKEN']
];