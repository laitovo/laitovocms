<?php

namespace returnModule\config;

use yii\base\Application;
use yii\base\BootstrapInterface;

class SetUpReturn implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     *
     * В родительском приложении в файле YourIdentityClass должны быть реализованы два метода
     *
     * public function getEmail()
       {
          return $user_email;
       }
     *
     * public function getRole()
       {
           return или строку admin (если пользователь родительского приложения админ) или no_admin;
       }
     *
     */
    public function bootstrap($app)
    {
        $moduleId = 'return';
        $app->setModule($moduleId, [
            'class' => 'returnModule\ReturnModule',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ]);
        $returnConfig = [
            'class' => 'returnModule\config\ReturnConfig',
            'moduleId' => $moduleId,
//            'use_ale10257_translate' => true,
            'path_main_layout' => '@app/views/layouts/main.php',
        ];
        // файл под игнором
        $config_api = require __DIR__ . '/config_api.php';
        $app->setComponents([
            'returnConfig' => array_merge($returnConfig, $config_api),
        ]);
        $app->urlManager->addRules($this->rules());
        $app->timeZone = 'Europe/Moscow';
    }

    private function rules()
    {
        return [];
    }
}