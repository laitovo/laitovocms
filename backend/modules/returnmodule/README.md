##Поключение и настройка модуля возвратов в родительском приложении

* Клонируем проект  
```git clone git@bitbucket.org:proctoleha/returnmodule.git your_dir```

* В конфиге приложения прописываем алиас (меняем путь на свой) 
```
'returnModule' => '@app/returnModule',
```
  
* В конфиге в секции bootstrap подключаем файл настроек модуля:  
```
    'bootstrap' => [
        'returnModule\config\SetUpReturn'
    ],
```

* В конфиге консольного приложения прописываем миграции модуля (измените пути на свои):  
```
    'controllerMap' => [
        // Миграции returnModule
        'return-module' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'migration_return_module',
            'migrationPath' => '@app/returnModule/migrations',
            'templateFile' =>  '@app/returnModule/migrations/template/template.php'
        ],
    ],
```

* Применяем миграции  
```php yii return-module```

* Изучаем файл модуля  
```path_your_dir_module/config/config_api_example.php```  
Копируем его как config.php и прописываем свои настройки

* Изучаем файл модуля  
```path_your_dir_module/config/ReturnConfig.php```  
И прописываем свои настройки

В браузере переходим по адресу: your-site.com/return, логинимся как администратор, настраиваем пользователей и роли.




