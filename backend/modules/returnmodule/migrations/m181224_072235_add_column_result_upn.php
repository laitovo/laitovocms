<?php

use yii\db\Migration;

class m181224_072235_add_column_result_upn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('return_journal', 'result_upn_id', $this->integer()->comment('Созданный в результате возврата UPN'));
        $this->addForeignKey('return_journal_result_upn_id', 'return_journal', 'result_upn_id', '{{%logistics_naryad}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('return_journal_result_upn_id', 'return_journal');
        $this->dropColumn('return_journal', 'result_upn_id');
    }
}
