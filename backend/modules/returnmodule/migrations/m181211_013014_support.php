<?php

use yii\db\Migration;

class m181211_013014_support extends Migration
{
    protected $table = '{{%return_support}}';
    protected $tableOptions;

    public function safeUp()
    {
        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'return_journal_id' => $this->integer(),
            'resume' => $this->boolean(),
            'created_at' => $this->integer(),
        ],
        $this->tableOptions);

        $this->addForeignKey('return_journal_support-id', $this->table, 'return_journal_id', '{{%return_journal}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'return_journal_support-id',
            $this->table
        );
        return $this->dropTable($this->table);
    }
}
