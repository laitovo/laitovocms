<?php

use yii\db\Migration;

class m181211_005900_journal extends Migration
{
    protected $table = '{{%return_journal}}';
    protected $tableOptions;

    public function safeUp()
    {
        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'upn' => $this->string(20)->unique(),
            'article' => $this->string(20)->notNull(),
            'car' => $this->string(),
            'product' => $this->string(),
            'source' => $this->integer(3),
            'why' => $this->integer(3),
            'status' => $this->integer(3),
            'client_id' => $this->integer(),
            'client_name_email' => $this->string(),
            'url_client' => $this->string(),
            'url_order' => $this->string(),
            'url_claim' => $this->string(),
            'order_number' => $this->integer(),
            'order_type' => $this->boolean(),
            'claim' => $this->string(),
            'barcode' => $this->string(),
            'package_integrity' => $this->boolean(),
            'comment' => $this->text(),
            'resolution' => $this->integer(3)->defaultValue(0),
            'location' => $this->integer(3),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'date' => $this->integer(),
        ],
        $this->tableOptions);

        //$this->addForeignKey('fk-name-id', $this->table, 'this_child_id', '{{%parent_table}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
