<?php

use yii\db\Migration;

/**
 * Handles the creation of table `return_user_return_role`.
 * Has foreign keys to the tables:
 *
 * - `return_user`
 * - `return_role`
 */
class m181203_074743_create_junction_table_for_return_user_and_return_role_tables extends Migration
{
    private $table = 'return_user_role';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table = 'return_user_role';
        
        $this->createTable($this->table, [
            'return_user_id' => $this->integer(),
            'return_role_id' => $this->integer(),
            'PRIMARY KEY(return_user_id, return_role_id)',
        ]);

        // creates index for column `return_user_id`
        $this->createIndex(
            'idx-return_user_return_role-return_user_id',
            $this->table,
            'return_user_id'
        );

        // add foreign key for table `return_user`
        $this->addForeignKey(
            'fk-return_user_return_role-return_user_id',
            $this->table,
            'return_user_id',
            'return_user',
            'id',
            'CASCADE'
        );

        // creates index for column `return_role_id`
        $this->createIndex(
            'idx-return_user_return_role-return_role_id',
            $this->table,
            'return_role_id'
        );

        // add foreign key for table `return_role`
        $this->addForeignKey(
            'fk-return_user_return_role-return_role_id',
            $this->table,
            'return_role_id',
            'return_role',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `return_user`
        $this->dropForeignKey(
            'fk-return_user_return_role-return_user_id',
            $this->table
        );

        // drops index for column `return_user_id`
        $this->dropIndex(
            'idx-return_user_return_role-return_user_id',
            $this->table
        );

        // drops foreign key for table `return_role`
        $this->dropForeignKey(
            'fk-return_user_return_role-return_role_id',
            $this->table
        );

        // drops index for column `return_role_id`
        $this->dropIndex(
            'idx-return_user_return_role-return_role_id',
            $this->table
        );

        $this->dropTable($this->table);
    }
}
