<?php

use yii\db\Migration;

/**
 * Handles the creation of table `return_cheif`.
 */
class m190415_111505_create_return_cheif_table extends Migration
{
    protected $table = '{{%return_chief}}';
    protected $tableOptions;

    public function safeUp()
    {
        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'return_journal_id' => $this->integer(),
            'resume_material' => $this->boolean()->notNull()->comment('Решение по материалам'),
            'resume_money' => $this->boolean()->notNull()->comment('Решение по деньгам'),
            'result_sum' => $this->float()->notNull()->comment('Сумма денег к возврату клиенту'),
            'created_at' => $this->integer(),
        ],
            $this->tableOptions);

        $this->addForeignKey('return_chief_return_journal-id', $this->table, 'return_journal_id', '{{%return_journal}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'return_chief_return_journal-id',
            $this->table
        );
        return $this->dropTable($this->table);
    }
}
