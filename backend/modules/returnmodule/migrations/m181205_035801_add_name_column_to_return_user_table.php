<?php

use yii\db\Migration;

/**
 * Handles adding name to table `return_user`.
 */
class m181205_035801_add_name_column_to_return_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('return_user', 'name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('return_user', 'name');
    }
}
