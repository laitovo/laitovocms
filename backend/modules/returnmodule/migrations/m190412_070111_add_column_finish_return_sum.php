<?php

use yii\db\Migration;

class m190412_070111_add_column_finish_return_sum extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%return_sales_department}}', 'result_sum', $this->double()->notNull()->defaultValue(0)->comment('Сумма к выплате клиенту за возврат'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%return_sales_department}}', 'result_sum');
    }
}
