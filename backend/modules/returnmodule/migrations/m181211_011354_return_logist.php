<?php

use yii\db\Migration;

class m181211_011354_return_logist extends Migration
{
    protected $table = '{{%return_logist}}';
    protected $tableOptions;

    public function safeUp()
    {
        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'return_journal_id' => $this->integer(),
            'paid' => $this->boolean(),
            'paid_sum' => $this->float(),
            'no_paid_sum' => $this->float(),
            'shipping_amount' => $this->float(),
            'return_sum' => $this->float(),
            'cash_on_delivery' => $this->float(),
            'other_sum' => $this->float(),
            'all_sum' => $this->float(),
            'created_at' => $this->integer(),
        ],
        $this->tableOptions);

        $this->addForeignKey('return_journal-id', $this->table, 'return_journal_id', '{{%return_journal}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'return_journal-id',
            $this->table
        );
        return $this->dropTable($this->table);
    }
}
