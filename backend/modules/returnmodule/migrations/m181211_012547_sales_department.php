<?php

use yii\db\Migration;

class m181211_012547_sales_department extends Migration
{
    protected $table = '{{%return_sales_department}}';
    protected $tableOptions;

    public function safeUp()
    {
        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'return_journal_id' => $this->integer(),
            'resume' => $this->boolean(),
            'created_at' => $this->integer(),
        ],
        $this->tableOptions);

        $this->addForeignKey('return_journal_sale-id', $this->table, 'return_journal_id', '{{%return_journal}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'return_journal_sale-id',
            $this->table
        );
        return $this->dropTable($this->table);
    }
}
