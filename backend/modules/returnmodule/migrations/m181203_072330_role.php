<?php

use yii\db\Migration;

class m181203_072330_role extends Migration
{
    protected $table = '{{%return_role}}';
    protected $tableOptions;

    public function safeUp()
    {
        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'role' => $this->string(255),
            'name' => $this->string(255)
        ],
        $this->tableOptions);

        $this->insert($this->table, ['name' => 'Техслужба', 'role' => 'support']);
        $this->insert($this->table, ['name' => 'Ответственный за склад', 'role' => 'storageUser']);
        $this->insert($this->table, ['name' => 'Менеджер', 'role' => 'manager']);
        $this->insert($this->table, ['name' => 'Логист', 'role' => 'logist']);
        $this->insert($this->table, ['name' => 'Руководитель', 'role' => 'chief']);

        //$this->addForeignKey('fk-name-id', $this->table, 'this_child_id', '{{%parent_table}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
