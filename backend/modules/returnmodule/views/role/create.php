<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model returnModule\core\models\ReturnRole */

$this->title = 'Создать роль для системы возвратов';
$this->params['breadcrumbs'][] = ['label' => 'Роли пользователей для системы возвратов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

