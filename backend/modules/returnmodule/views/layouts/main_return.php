<?php
/**
 * @var \yii\web\View $this
 * @var string $content
 */

use returnModule\widgets\Alert;
use returnModule\assets\ReturnAsset;

ReturnAsset::register($this);
?>
<?php $this->beginContent(Yii::$app->returnConfig->path_main_layout); ?>
<?= Alert::widget(['options' => ['class' => 'return-alert']]) ?>
<?= $content ?>
<?php $this->endContent(); ?>