<?php
/**
 * @var \yii\web\View $this
 * @var array $data
 */

use returnModule\ReturnModule;
use yii\helpers\Html;
$this->registerJs($this->render('js'), \yii\web\View::POS_END);
?>
<h4><?= ReturnModule::t('Клиент') ?>: <?= $data['user']['user_name'] . '; ' . $data['user']['user_email'] ?></h4>
<hr>
<?php if($data['result']) : ?>
    <table class="table table-bordered-return">
        <? foreach($data['result'] as $item) : ?>
            <tr>
                <td>
                    <h5><?= ReturnModule::t('Номер заказа') . ': ' . $item['order']['order_number'] ?></h5>
                </td>
                <td>
                    <a target="_blank" href="<?= $item['order']['url'] ?>"><?= ReturnModule::t('Просмотр') ?></a>
                </td>
                <td class="text-center">
                    <?= $this->render('_add', [
                        'target' => '#return-order-id',
                        'action' => '#by-order',
                        'item' => $item['order']['order_number'],
                        'msg' => ReturnModule::t('Получить информацию по номеру заказа'),
                    ]) ?>
                </td>
            </tr>
            <?php if($item['claim']) : ?>
                <? foreach($item['claim'] as $claim) : ?>
                    <tr>
                        <td>
                            <h5><?= ReturnModule::t('Номер рекламации') . ': ' . $claim['claim_number'] ?></h5>
                        </td>
                        <td>
                            <a target="_blank" href="<?= $claim['url'] ?>"><?= ReturnModule::t('Просмотр') ?></a>
                        </td>
                        <td class="text-center">
                            <?= $this->render('_add', [
                                'target' => '#return-claim-id',
                                'action' => '#by-claim',
                                'item' => $claim['claim_number'],
                                'msg' => ReturnModule::t('Получить информацию по номеру рекламации'),
                            ]) ?>
                        </td>
                    </tr>
                <? endforeach ?>
            <?php endif ?>
        <? endforeach ?>
    </table>
    <hr>
<?php else : ?>
    <h4><?= ReturnModule::t('Нет данных по заказам') ?></h4>
<?php endif ?>
