<?php
/**
 * @var \yii\web\View $this
 * @var string $target
 * @var string $action
 * @var string $item
 * @var string $msg
 */
use yii\helpers\Html;
?>
<?= Html::a('<i class="fa fa-plus-square-o"></i>',
    '#', [
        'title' => $msg,
        'class' => 'get-data-right get-any',
        'data-num' => $item,
        'data-target' => $target,
        'data-action' => $action,
    ]) ?>
