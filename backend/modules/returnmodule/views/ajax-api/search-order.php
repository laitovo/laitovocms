<?php
/**
 * @var \yii\web\View $this
 * @var array $data
 * @var string $title
 * @var string $search
 * @var string|false $upn
 * @var string|false $article
 */

use returnModule\ReturnModule;
use yii\helpers\Html;

$moduleId = Yii::$app->returnConfig->moduleId;
$this->registerJs($this->render('js'), \yii\web\View::POS_END);
$js = "
$(function () {
    $('.add-item').click(function () {
        let target = $(this).data('target');
        $(target).val($(this).data('item'));        
    });
});
";
$this->registerJs($js, \yii\web\View::POS_END);
?>
<?php if ($upn) : ?>
    <h4>
        UPN: <?= $upn ?>, <?= ReturnModule::t('Артикул') ?>:
        <?= $article ?>,
        <?= ReturnModule::t('Заказ') ?>: <?= $search ?>
    </h4>
    <hr>
    <h4><?= $title . ' ' . $search ?></h4>
<?php else : ?>
    <h4><?= $title . ' ' . $search ?></h4>
<?php endif ?>
<hr>
<?php if (!empty($data['user'])) : ?>
    <p>
        <?= ReturnModule::t('Клиент') . ': ' . $data['user']['user_name'] ?>
        <a class="inline-return" target="_blank"
           href="<?= $data['user']['url'] ?>"><?= ReturnModule::t('Просмотр') ?></a>
    </p>
<?php else : ?>
    <h5><?= ReturnModule::t('Нет данных о пользователе') ?></h5>
<?php endif ?>
<hr>
<?php if (!empty($data['order'])) : ?>
    <p>
        <?= ReturnModule::t('Номер заказа') . ': ' . $data['order']['order_number'] ?>
        <span class="inline-return">
            <?= $data['order']['order_type'] ? ReturnModule::t('Опт') : ReturnModule::t('Розница') ?>
        </span>
        <a class="inline-return" target="_blank" href="<?= $data['order']['url'] ?>">
            <?= ReturnModule::t('Просмотр') ?>
        </a>
    </p>
<?php else : ?>
    <h5><?= ReturnModule::t('Нет данных о заказе') ?></h5>
<?php endif ?>
<hr>
<?php if (!empty($data['products'])) : ?>
    <h4><?= ReturnModule::t('Заказанные товары') ?></h4>
    <table class="table table-bordered-retur">
        <tr>
            <th><?= ReturnModule::t('UPN') ?></th>
            <th><?= ReturnModule::t('Артикул') ?></th>
            <th><?= ReturnModule::t('Имя') ?></th>
            <th><?= ReturnModule::t('Кол-во') ?></th>
            <th></th>
        </tr>
        <? foreach ($data['dataProductUpn'] as $item) : ?>
            <tr>
                <td><?= $item['upn'] ?></td>
                <td><?= $item['article'] ?></td>
                <td><?= $item['product_name'] ?></td>
                <td><?= $item['count'] ?></td>
                <td class="text-center">
                    <?php if($item['disabled']): ?>

                    <?php else:?>
                        <i onclick="
                            let barcodeField = $('#return_barcode');
                            let articleField = $('#article-return');
                            let hiddenInput = $('input[name=\'StorageForm[upn]\']');
                            barcodeField.val($(this).attr('data-item'));
                            hiddenInput.val($(this).attr('data-upn'));
                            articleField.val($(this).attr('data-article'));
                            " data-item="<?= $item['upnBarcode'] ?>" data-article="<?= $item['article'] ?>"  data-upn=<?= $item['upn']?> title="<?= 'Выбрать' ?>"
                                   class="fa fa-plus add-item"></i>
                    <?php endif;?>
                </td>
            </tr>
        <? endforeach ?>
    </table>
    <hr>
<?php else : ?>
    <h5><?= ReturnModule::t('Нет данных о товарах') ?></h5>
    <hr>
<?php endif ?>
<?php if (!empty($data['claim'])) : ?>
    <?php $i = 0 ?>
    <? foreach ($data['claim'] as $item) : ?>
        <?php if ($i == 0) : ?>
            <p>
                <?= ReturnModule::t('Номер рекламации') . ': ' . $item['claim_number'] ?>
                <a class="inline-return" target="_blank" href="<?= $item['url'] ?>">
                    <?= ReturnModule::t('Просмотр') ?>
                </a>
            </p>
            <hr>
        <?php else : ?>
            <?php if ($i == 1) : ?>
                <h4><?= ReturnModule::t('Внимание! У заказа несколько рекламаций!') ?></h4>
            <?php endif ?>
            <table class="table table-bordered-return">
                <tr>
                    <td><h5><?= ReturnModule::t('Номер рекламации') . ': ' . $item['claim_number'] ?></h5></td>
                    <td>
                        <a target="_blank" href="<?= $item['url'] ?>"><?= ReturnModule::t('Просмотр') ?></a>
                    </td>
                    <td class="text-center">
                        <?= $this->render('_add', [
                            'target' => '#return-claim-id',
                            'action' => '#by-claim',
                            'item' => $item['claim_number'],
                            'msg' => ReturnModule::t('Получить информацию по номеру рекламации'),
                        ]) ?>
                    </td>
                </tr>
            </table>
        <?php endif ?>
        <?php $i++ ?>
    <? endforeach ?>
<?php else : ?>
    <h5><?= ReturnModule::t('Нет данных о рекламации') ?></h5>
    <hr>
<?php endif ?>

