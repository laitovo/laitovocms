<?php
/**
 * @var \yii\web\View $this
 * @var array $data
 * @var string $title
 * @var string $search
 */

use returnModule\ReturnModule;
use yii\bootstrap\Html;

$moduleId = Yii::$app->returnConfig->moduleId;
$js = "$('#search-user-result').change(function () {
            if ($(this).val()) {
                let obj = {
                    key: $(this).data('key'),
                    value: $(this).val()
                };
                $.post($(this).data('url'), obj, function (data) {
                    if (data.user_id && data.user_name) {
                        $('#search-user').val(data.user_name);
                        $('#user_id').val(data.user_id);
                    }
                    $('#search-result').html(data.html);
                });
            }
        });";
$this->registerJs($js, \yii\web\View::POS_END);
?>

<?php if(!empty($data['error'])) : ?>
    <p style="border: 1px solid #f00; color: #f00; padding: 5px;"><?= ReturnModule::t($data['error']) ?></p>
<?php endif ?>

<?php if(!empty($data['users'])) : ?>
    <?= Html::label(ReturnModule::t('Результаты поиска клиента'), null, ['style' => 'color: #ff5833;']) ?>
    <?= Html::dropDownList('', null, $data['users'], [
        'prompt' => ReturnModule::t('Выберите клиента'),
        'class' => 'form-control',
        'id' => 'search-user-result',
        'data-url' => '/' . $moduleId . '/ajax-api/get-by-user-id',
        'data-key' => 'user_id',
    ]) ?>
<?php endif ?>


