$('.get-data-right').click(function (e) {
    e.preventDefault();
    let target = $(this).data('target');
    let action = $(this).data('action');
    $(target).val($(this).data('num'));
    $(action).click();
});