<?php
/**
 * @var \yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var \returnModule\core\search\JournalSearch $searchModel
 * @var string $controllerTitle
 */

use returnModule\core\models\ReturnJournal;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use returnModule\ReturnModule;

$this->title = ReturnModule::t($controllerTitle);
$this->params['breadcrumbs'][] = $this->title;

$filter_status = ArrayHelper::map(ReturnJournal::STATUS, 'value', 'name');
foreach ($filter_status as $key => $item) {
    $filter_status[$key] = ReturnModule::t($item);
}
?>

<div class="row">
    <div class="col-md-12">
        <p><?= Html::a(ReturnModule::t('Сбросить фильтры'), ['index']) ?></p>
        <div class="box-return">
            <div class="box-body-return">
                <?= GridView::widget([
                    'tableOptions' => ['class' => 'table table-hover'],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'rowOptions' => function ($model) {
                        /** @var \returnModule\core\models\ReturnJournal $model */
                        if ($model->show_btn) {
                            return ['class' => 'success'];
                        }
                        return [];
                    },
                    'columns' => [
                        'id',
                        'upn',
                        'article',
                        'client_name_email',
                        [
                            'attribute' => 'created_at',
                            'value' => function ($model) {
                                return \Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d H:i');
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model) {
                                /** @var \returnModule\core\models\ReturnJournal $model */
                                return ReturnModule::t($model->statusName);
                            },
                            'filter' => $filter_status,
                        ],
                        [
                            'value' => function ($model) {
                                return Html::a(ReturnModule::t('Просмотр'), ['view', 'id' => $model->id],
                                    ['class' => 'btn btn-info btn-xs']);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'value' => function ($model) {
                                /** @var \returnModule\core\models\ReturnJournal $model */
                                if ($model->show_btn) {
                                    return Html::a(ReturnModule::t('Обработать'), ['update', 'id' => $model->id],
                                        ['class' => 'btn btn-warning btn-xs']);
                                }
                                return false;
                            },
                            'format' => 'raw',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
