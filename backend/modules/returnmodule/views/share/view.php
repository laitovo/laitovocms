<?php
/**
 * @var \yii\web\View $this
 * @var int $id
 * @var string $controllerTitle
 */

use returnModule\ReturnModule;
use returnModule\assets\PrintActAsset;
use yii\helpers\Html;

PrintActAsset::register($this);

$generator = new Picqer\Barcode\BarcodeGeneratorSVG();

$this->title = ReturnModule::t('Акт возврата {#id}', ['#id' => "#$id"]);
$class_col = 'col-md-12';
if (!empty($controllerTitle)) {
    $this->params['breadcrumbs'][] = ['label' => ReturnModule::t($controllerTitle), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    $class_col = 'col-md-6';
}
?>

<?if (isset($data['printUrl'])):?>
    <a class="btn btn-sm btn-primary" href="#" onclick="printJS('<?=$data['printUrl']?>');"><i class="fa fa-print"></i> Распечатать этикетку для склада</a>
<?endif;?>
<?if (isset($data['literal'])):?>
    Литера :<span class="lead"> <?=$data['literal']?></span>
<?endif;?>
<br>
<hr>
<a id="print-return" class="btn btn-sm btn-success" href="#"><i class="fa fa-print"></i> Распечатать акт</a>



<? foreach ($data['urls'] as $item) : ?>
    <?= Html::a($item['str'], $item['url'],
        [
            'class' => 'btn btn-default btn-sm',
            'target' => '_blank',
        ]) ?>
<? endforeach ?>

<hr>
<div class="row">
    <div class="<?= $class_col ?>">
        <div id="print-act">
            <h3><?= $this->title ?></h3> <?=$generator->getBarcode($data['barcode'], $generator::TYPE_CODE_128, 2)?>
            <hr>
            <div class="box-return">
                <div class="box-body-return">
                    <? foreach ($data['act'] as $item) : ?>
                        <?php
                        /** @var \returnModule\core\services\DtoJournal $item */
                        $class = $item->processed ? null : ' class="no-processed"';
                        ?>
                        <div<?= $class ?>>
                            <h4><?= $item->title ?></h4>
                            <table class="table-bordered-return table">
                                <? foreach ($item->data as $value) : ?>
                                    <tr>
                                        <td style="width: 50%;"><?= $value['name'] ?></td>
                                        <td><?= $value['value'] ?></td>
                                    </tr>
                                <? endforeach ?>
                            </table>
                        </div>
                    <? endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>

