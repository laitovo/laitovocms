<?php
/**
 * @var \yii\web\View $this
 * @var \returnModule\core\forms\ManagerForm $model
 * @var string $act
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use returnModule\ReturnModule;

$this->title = ReturnModule::t('Обработка возврата руководителем');
$this->params['breadcrumbs'][] = ['label' => ReturnModule::t('Монитор руководителя'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-6">
        <h2><?= $this->title ?></h2>
        <div class="box-return">
            <div class="box-body-return">
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($model, 'resume_material')->radioList($model->arr) ?>
                <?= $form->field($model, 'resume_money')->radioList($model->arr1) ?>
                <div class="form-group">
                    <?= $form->field($model,'result_sum') ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(ReturnModule::t('Отправить данные'), ['class' => 'btn btn-success']) ?>
                </div>
                <?php $form::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <?= $act ?>
    </div>
</div>
