<?php
/**
 * @var \yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var \returnModule\core\search\JournalSearch $searchModel
 * @var string $controllerTitle
 */
?>
<?= $this->render('/share/index',
    ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'controllerTitle' => $controllerTitle]) ?>