<?php
/**
 * @var \yii\web\View $this
 * @var \returnModule\core\forms\StorageForm $model
 */
use yii\helpers\Html;
$labels = $model->twoJournalPart();
?>
<?= Html::activeInput('hidden', $model, 'resolution') ?>
<p><strong><?= $labels['resolution'] ?>: <?= $model->resolution_array[$model->resolution] ?></strong></p>
