<?php
/**
 * @var \yii\web\View $this
 * @var \returnModule\core\forms\StorageForm $model
 * @var string $act
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use returnModule\ReturnModule;
use returnModule\assets\ResolutionAsset;
use yii\helpers\Url;

ResolutionAsset::register($this);

$this->title = ReturnModule::t('Завершить обработку возврата');
$this->params['breadcrumbs'][] = ['label' => ReturnModule::t('Монитор кладовщика'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$prompt = ReturnModule::t('Выбрать');
?>
<div class="row">
    <div class="col-md-6">
        <h2><?= $this->title ?></h2>
        <div class="box-return">
            <div class="box-body-return">
                <?php $form = ActiveForm::begin([
                    'options' => ['id' => 'resolution-storage']
                ]) ?>
                <?= $form->field($model, 'package_integrity')->radioList($model->package_integrity_array) ?>
                <div id="resolution-block">
                    <?php if ($model->resolution) : ?>
                        <?= $this->render('_resolution', ['model' => $model]) ?>
                    <?php endif ?>
                </div>
                <?= $form->field($model, 'id')->hiddenInput([
                    'id' => 'resolution-id',
                    'data-resolution' => Url::to([
                        'resolution',
                        'id' => $model->id
                    ])
                ])->label(false) ?>
                <div class="form-group">
                    <?= Html::submitButton(ReturnModule::t('Отправить данные'),
                        ['class' => 'btn btn-success', 'id' => 'resolution-btn']) ?>
                </div>
                <?php $form::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <?= $act ?>
    </div>
</div>