<?php
/**
 * @var \yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var \returnModule\core\search\JournalSearch $searchModel
 * @var string $controllerTitle
 */
use returnModule\ReturnModule;
?>
<?= \yii\helpers\Html::a(ReturnModule::t('Зарегистрировать возврат'), ['create'], ['class' => 'btn btn-success']) ?>
<hr>

<?= $this->render('/share/index',
    ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'controllerTitle' => $controllerTitle]) ?>