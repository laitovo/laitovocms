<?php
/**
 * @var \yii\web\View $this
 * @var \returnModule\core\forms\StorageForm $model
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use returnModule\ReturnModule;
use returnModule\assets\CreateAsset;

CreateAsset::register($this);

$this->title = ReturnModule::t('Регистрация возврата');
$this->params['breadcrumbs'][] = ['label' => ReturnModule::t('Монитор кладовщика'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$prompt = ReturnModule::t('Выбрать');
$moduleId = Yii::$app->returnConfig->moduleId;
?>
<h2><?= $this->title ?></h2>
<div class="row">
    <div class="col-md-6">
        <div class="box-return">
            <div class="box-body-return">
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($model, 'barcode')->textInput([
                    'id' => 'return_barcode',
                    'data-url' => Yii::$app->returnConfig->ownerUrlGetUpn,
                    'data-action' => '#by-order',
                    'data-target' => '#return-order-id',
                ]) ?>
                <?= $form->field($model, 'upn')->hiddenInput(['id' => 'return_upn'])->label(false) ?>
                <?= $form->field($model, 'article')->textInput(['id' => 'article-return']) ?>
                <div class="form-group">
                    <?= Html::activeLabel($model, 'order_number') ?>
                    <div class="wrap-input">
                        <?= Html::activeInput('text', $model, 'order_number',
                            ['class' => 'form-control search-data', 'id' => 'return-order-id']) ?>
                        <?= Html::a('<i class="fa fa-plus-square-o"></i>', '/' . $moduleId . '/ajax-api/get-by-order', [
                            'title' => ReturnModule::t('Получить информацию по номеру заказа'),
                            'class' => 'get-data-return get-any',
                            'data-key' => 'order_id',
                            'data-int' => ReturnModule::t('Поле Номер заказа должно быть целым числом'),
                            'data-target' => '#return-order-id',
                            'id' => 'by-order',
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::activeLabel($model, 'claim') ?>
                    <div class="wrap-input">
                        <?= Html::activeInput('text', $model, 'claim',
                            ['class' => 'form-control search-data', 'id' => 'return-claim-id']) ?>
                        <?= Html::a('<i class="fa fa-plus-square-o"></i>', '/' . $moduleId . '/ajax-api/get-by-claim', [
                            'title' => ReturnModule::t('Получить информацию по номеру рекламации'),
                            'class' => 'get-data-return get-any',
                            'data-key' => 'claim_id',
                            'data-int' => ReturnModule::t('Поле Номер рекламации должно быть целым числом'),
                            'data-target' => '#return-claim-id',
                            'id' => 'by-claim',
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::activeLabel($model, 'client_name_email') ?>
                    <div class="wrap-input">
                        <?= Html::activeInput('text', $model, 'client_name_email',
                            ['id' => 'search-user', 'class' => 'form-control search-data']) ?>
                        <?= Html::a('<i class="fa fa-user-o"></i>',
                            '/' . $moduleId . '/ajax-api/get-by-user-name', [
                                'title' => ReturnModule::t('Получить информацию по имени, или емайлу клиента'),
                                'class' => 'get-data-user get-any',
                                'data-key' => 'search_user',
                                'data-target' => '#search-user',
                            ]) ?>
                    </div>
                </div>
                <div class="form-group" id="user-search-result"></div>
                <?= $form->field($model, 'client_id')->hiddenInput(['id' => 'user_id'])->label(false) ?>
                <?= $form->field($model, 'url_client')->hiddenInput(['id' => 'url_client'])->label(false) ?>
                <?= $form->field($model, 'url_order')->hiddenInput(['id' => 'url_order'])->label(false) ?>
                <?= $form->field($model, 'url_claim')->hiddenInput(['id' => 'url_claim'])->label(false) ?>
                <?= $form->field($model, 'order_type')->hiddenInput(['id' => 'order_type'])->label(false) ?>
                <?= $form->field($model, 'contractorId')->hiddenInput(['id' => 'contractorId'])->label(false) ?>
                <?= $form->field($model, 'source')->dropDownList($model->source_arr, ['prompt' => $prompt]) ?>
                <?= $form->field($model, 'why')->dropDownList($model->why_array, ['prompt' => $prompt]) ?>
                <?= $form->field($model, 'comment', ['enableAjaxValidation' => true])->textarea() ?>
                <div class="form-group">
                    <?= Html::submitButton(ReturnModule::t('Зарегистрировать возврат'),
                        ['class' => 'btn btn-success']) ?>
                    <? //= Html::resetButton(ReturnModule::t('Очистить форму'), ['class' => 'btn btn-danger']) ?>
                </div>
                <?php $form::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box-return">
            <div class="box-body-return" id="search-result"></div>
        </div>
    </div>
</div>