<?php
/**
 * @var \yii\web\View $this
 * @var int $id
 */
use returnModule\ReturnModule;

$this->params['breadcrumbs'][] = ['label' => ReturnModule::t('Журнал возвратов'), 'url' => ['index']];

echo $this->render('/share/view', ['data' => $data, 'id' => $id]);