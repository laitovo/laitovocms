<?php
/**
 * @var \yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var \returnModule\core\search\JournalSearch $searchModel
 */

use yii\helpers\Html;
use returnModule\ReturnModule;
use yii\grid\GridView;
use returnModule\core\models\ReturnJournal;
use yii\helpers\ArrayHelper;

$this->title = ReturnModule::t('Журнал возвратов');
$this->params['breadcrumbs'][] = $this->title;
$filter = ArrayHelper::map(ReturnJournal::RESOLUTION, 'value', 'name');
foreach ($filter as $key => $item) {
    $filter_resolution[$key] = ReturnModule::t($item);
}
$filter_status = ArrayHelper::map(ReturnJournal::STATUS, 'value', 'name');
foreach ($filter_status as $key => $item) {
    $filter_status[$key] = ReturnModule::t($item);
}
?>
<p><?= Html::a(ReturnModule::t('Сбросить фильтры'), ['index']) ?></p>
<div class="row">
    <div class="col-md-12"
        <div class="box-return">
            <div class="box-body-return">
                <?= GridView::widget([
                    'tableOptions' => ['class' => 'table table-hover'],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        'upn',
                        'article',
                        'client_name_email',
                        [
                            'attribute' => 'created_at',
                            'value' => function ($model) {
                                return \Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d H:i');
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model) {
                                /** @var \returnModule\core\models\ReturnJournal $model */
                                return ReturnModule::t($model->statusName);
                            },
                            'filter' => $filter_status,
                        ],
                        [
                            'attribute' => 'resolution',
                            'value' => function ($model) {
                                /** @var \returnModule\core\models\ReturnJournal $model */
                                return ReturnModule::t($model->resolutionName);
                            },
                            'filter' => $filter_resolution,
                        ],
                        [
                            'value' => function ($model) {
                                return Html::a(ReturnModule::t('Просмотр'), ['view', 'id' => $model->id],
                                    ['class' => 'btn btn-info btn-xs']);
                            },
                            'format' => 'raw',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
