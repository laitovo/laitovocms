<?php
/**
 * @var \yii\web\View $this
 * @var \returnModule\core\forms\StorageForm $model
 * @var string $act
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use returnModule\ReturnModule;

$this->title = ReturnModule::t('Обработка возврата логистом');
$this->params['breadcrumbs'][] = ['label' => ReturnModule::t('Монитор логиста'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-6">
        <h2><?= $this->title ?></h2>
        <div class="box-return">
            <div class="box-body-return" >
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($model, 'paid')->checkbox(['disabled' => true]) ?>
                <?= $form->field($model, 'paid_sum')->textInput(['disabled' => true]) ?>
                <?= $form->field($model, 'no_paid_sum')->textInput(['disabled' => true]) ?>
                <?= $form->field($model, 'shipping_amount')->textInput(['disabled' => true]) ?>
                <?= $form->field($model, 'return_sum')->textInput(['disabled' => true]) ?>
                <?= $form->field($model, 'cash_on_delivery')->textInput(['disabled' => true]) ?>
                <?= $form->field($model, 'other_sum') ?>
                <div class="form-group">
                    <?= Html::submitButton(ReturnModule::t('Отправить данные'), ['class' => 'btn btn-success']) ?>
                </div>
                <?php $form::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <?= $act ?>
    </div>
</div>
