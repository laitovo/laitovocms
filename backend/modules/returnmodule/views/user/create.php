<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model returnModule\core\models\ReturnUser */

$this->title = 'Создать пользователя для системы возвратов';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи системы возвратов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="return-user-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
