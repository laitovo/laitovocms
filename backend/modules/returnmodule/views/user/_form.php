<?php
/**
 * @var $this yii\web\View
 * @var $model \returnModule\core\forms\UserForm
 * @var $form yii\widgets\ActiveForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="row">
    <div class="col-md-3">
        <?php $form = ActiveForm::begin() ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'email', ['enableAjaxValidation' => true]) ?>
        <?= $form->field($model, 'checkedRoles')->checkboxList($model->roles) ?>
        <div class="form-group">
            <?= Html::submitButton('Ok', ['class' => 'btn btn-success']) ?>
        </div>
        <?php $form->end() ?>
    </div>
</div>