<?php
/**
 * @var \yii\web\View $this
 * @var \returnModule\core\forms\IssueForm $model
 */

use yii\bootstrap\ActiveForm;
use returnModule\ReturnModule;
use yii\helpers\Html;

$this->title = $model->action == 'create-issue' ?
    ReturnModule::t('Выдать возврат') : ReturnModule::t('Принять возврат');
$this->params['breadcrumbs'][] = ['label' => ReturnModule::t('Склад возвратов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h2><?= $this->title ?></h2>

<div class="row">
    <div class="col-md-4">
        <div class="box-return">
            <div class="box-body-return">
                <?php $form = ActiveForm::begin([
                    'action' => $model->action,
                ]) ?>

                <?= $form->field($model, 'barcode') ?>

                <?php if($model->location_arr) : ?>
                    <?= $form->field($model, 'location')->dropDownList($model->location_arr) ?>
                <?php endif ?>

                <div class="form-group">
                    <?= Html::submitButton('Ok', ['class' => 'btn btn-success']) ?>
                </div>

                <?php $form::end() ?>
            </div>
        </div>
    </div>
</div>




