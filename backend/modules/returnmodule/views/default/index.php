<?php
/**
 * @var \yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var \returnModule\core\search\JournalSearch $searchModel
 */

use yii\helpers\Html;
use returnModule\ReturnModule;
use yii\grid\GridView;
use returnModule\core\models\ReturnJournal;
use yii\helpers\ArrayHelper;

$this->title = ReturnModule::t('Склад возвратов');
$this->params['breadcrumbs'][] = $this->title;
$filter = ArrayHelper::map(ReturnJournal::STATUS, 'value', 'name');
foreach ($filter as $key => $item) {
    if ($key != ReturnJournal::STATUS['end']['value']) {
        $filter_status[$key] = ReturnModule::t($item);
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <?= Html::a(ReturnModule::t('Выдать возврат'), ['create-issue'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(ReturnModule::t('Принять возврат'), ['update-issue'], ['class' => 'btn btn-success']) ?>
    </div>
</div>
<hr>
<p><?= Html::a(ReturnModule::t('Сбросить фильтры'), ['index']) ?></p>
<div class="row">
    <div class="col-md-12">
        <div class="box-return">
            <div class="box-body-return">
                <?= GridView::widget([
                    'tableOptions' => ['class' => 'table table-hover'],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        'upn',
                        'article',
                        [
                            'attribute' => 'created_at',
                            'value' => function ($model) {
                                return \Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d H:i');
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model) {
                                /** @var \returnModule\core\models\ReturnJournal $model */
                                return ReturnModule::t($model->statusName);
                            },
                            'filter' => $filter_status,
                        ],
                        [
                            'label' => ReturnModule::t('Местонахождение'),
                            'value' => function ($model) {
                                /** @var \returnModule\core\models\ReturnJournal $model */
                                return $model->getLocationName();
                            },
                        ],
                        [
                            'value' => function ($model) {
                                return Html::a(ReturnModule::t('Просмотр'), ['view', 'id' => $model->id],
                                    ['class' => 'btn btn-info']);
                            },
                            'format' => 'raw',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
