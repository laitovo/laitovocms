<?php

namespace returnModule\controllers;

use returnModule\core\services\JournalService;
use returnModule\core\services\LogistServise;
use returnModule\core\services\UserService;
use returnModule\ReturnModule;
use Yii;

class MonitorLogistController extends BaseController
{
    use TraitReturnController;

    /** @var array */
    protected $accessRoles = ['logist', 'chief'];

    /** @var LogistServise */
    private $logistServise;
    /** @var string */
    private $controllerTitle = 'Монитор логиста';

    public function __construct(
        $id,
        $module,
        UserService $userService,
        JournalService $journalService,
        LogistServise $logistServise
    ) {
        parent::__construct($id, $module, $userService, $journalService);
        $this->logistServise = $logistServise;
    }

    public function actionUpdate($id)
    {
        $model = $this->logistServise->getForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $this->logistServise->updateForm($model);
                Yii::$app->session->setFlash('success', ReturnModule::t('Данные обновлены успешно!'));
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $model,
            'act' => $this->getAct($id),
        ]);
    }
}