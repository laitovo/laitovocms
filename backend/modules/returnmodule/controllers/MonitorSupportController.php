<?php

namespace returnModule\controllers;

use returnModule\core\search\JournalSearch;
use returnModule\core\services\JournalService;
use returnModule\core\services\SupportService;
use returnModule\core\services\UserService;
use returnModule\ReturnModule;
use Yii;

class MonitorSupportController extends BaseController
{
    use TraitReturnController;

    protected $accessRoles = ['support'];

    /** @var string */
    private $controllerTitle = 'Монитор техслужбы';
    /** @var SupportService */
    private $supportService;

    public function __construct(
        $id,
        $module,
        UserService $userService,
        JournalService $journalService,
        SupportService $supportService
    ) {
        parent::__construct($id, $module, $userService, $journalService);
        $this->supportService = $supportService;
    }

    public function actionIndex()
    {
        $searchModel = new JournalSearch($this->accessRoles);
        $dataProvider = $searchModel->searchSupport(Yii::$app->request->queryParams);

        return $this->render('/share/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'controllerTitle' => $this->controllerTitle
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->supportService->getForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $this->supportService->updateForm($model);
                Yii::$app->session->setFlash('success', ReturnModule::t('Данные обновлены успешно!'));
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $model,
            'act' => $this->getAct($id),
        ]);
    }
}