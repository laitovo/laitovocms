<?php

namespace returnModule\controllers;

use returnModule\core\search\JournalSearch;
use returnModule\core\services\JournalService;
use returnModule\core\services\StorageService;
use returnModule\core\services\UserService;
use Yii;
use returnModule\ReturnModule;
use yii\web\Response;
use yii\widgets\ActiveForm;

class MonitorStorageController extends BaseController
{
    use TraitReturnController;

    protected $accessRoles = ['storageUser'];

    /** @var StorageService */
    private $storageService;
    /** @var string */
    private $controllerTitle = 'Монитор кладовщика';

    public function __construct(
        $id,
        $module,
        UserService $userService,
        StorageService $storageService,
        JournalService $journalService
    ) {
        parent::__construct($id, $module, $userService, $journalService);
        $this->storageService = $storageService;
    }

    public function actionIndex()
    {
        $searchModel = new JournalSearch($this->accessRoles);
        $dataProvider = $searchModel->searchStorage(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'controllerTitle' => $this->controllerTitle
        ]);
    }

    public function actionCreate()
    {
        $model = $this->storageService->getForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $this->storageService->createReturn($model);
                Yii::$app->session->setFlash('success', ReturnModule::t('Возврат зарегистрирован успешно!'));
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $model = $this->storageService->getUpdateForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $msg = $this->storageService->update($model);
                if ($msg) {
                    Yii::$app->session->setFlash('success', ReturnModule::t($msg));
                }
                return $this->redirect(['view','id' => $id]);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }

        }

        return $this->render('update', ['model' => $model, 'act' => $this->getAct($id),]);
    }

    public function actionResolution($id)
    {
        $model = $this->storageService->getUpdateForm($id);
        $resolution = $this->storageService->resolution($id, Yii::$app->request->post('package_integrity'));
        if ($resolution) {
            $model->resolution = $resolution;
            $resolution = $this->renderPartial('_resolution', ['model' => $model]);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'resolution' => $resolution,
        ];
    }
}
