<?php

namespace returnModule\controllers;

use returnModule\core\search\JournalSearch;
use returnModule\core\services\ChiefService;
use returnModule\core\services\JournalService;
use returnModule\core\services\UserService;
use returnModule\ReturnModule;
use Yii;

class ChiefController extends BaseController
{
    use TraitReturnController;

    protected $accessRoles = ['chief'];

    /** @var string */
    private $controllerTitle = 'Монитор руководителя';

    /** @var ChiefService */
    private $chiefService;

    public function __construct(
        $id,
        $module,
        UserService $userService,
        JournalService $journalService,
        ChiefService $chiefService
    ) {
        parent::__construct($id, $module, $userService, $journalService);
        $this->chiefService = $chiefService;
    }

    public function actionIndex()
    {
        $searchModel = new JournalSearch($this->accessRoles);
        $dataProvider = $searchModel->searchChief(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'controllerTitle' => $this->controllerTitle
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->chiefService->getForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $this->chiefService->updateForm($model);
                Yii::$app->session->setFlash('success', ReturnModule::t('Данные обновлены успешно!'));
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $model,
            'act' => $this->getAct($id),
        ]);
    }
}