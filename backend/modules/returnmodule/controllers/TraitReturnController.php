<?php

namespace returnModule\controllers;

use returnModule\core\search\JournalSearch;
use Yii;

trait TraitReturnController
{
    public function actionIndex()
    {
        $searchModel = new JournalSearch($this->accessRoles);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/share/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'controllerTitle' => $this->controllerTitle
        ]);
    }

    public function actionView($id)
    {
        return $this->render('/share/view', [
            'id' => $id,
            'controllerTitle' => $this->controllerTitle,
            'data' => $this->journalService->getJournal($id)
        ]);
    }

    protected function getAct($id)
    {
        $data = $this->journalService->getJournal($id);
        return $this->renderAjax('/share/view', compact('id', 'data'));
    }
}