<?php

namespace returnModule\controllers;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use returnModule\core\models\ReturnJournal;
use yii\httpclient\Client;
use yii\web\Controller;
use Yii;
use yii\web\Response;
use returnModule\ReturnModule;

class AjaxApiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'get-by-order' => ['POST'],
                    'get-by-claim' => ['POST'],
                    'get-by-user-name' => ['POST'],
                    'get-by-user-id' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();

        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function actionGetByOrder()
    {
        return $this->returnDataOrder($this->createDataRequest('order'), ReturnModule::t('Результаты поиска по номеру заказа'));
    }

    public function actionGetByClaim()
    {
        return $this->returnDataOrder($this->createDataRequest('claim'), ReturnModule::t('Результаты поиска по номеру рекламации'));
    }

    private function returnDataOrder($dataArr, $title)
    {
        $data = $dataArr['data'];
        $search = $dataArr['search'];
        if (!empty($data['order'])) {
            $result['order_id'] = $data['order']['order_number'];
            $result['url_order'] = $data['order']['url'];
            $result['order_type'] = $data['order']['order_type'];
        }
        if (!empty($data['claim'])) {
            $result['claim_id'] = $data['claim'][0]['claim_number'];
            $result['url_claim'] = $data['claim'][0]['url'];
        }
        if (!empty($data['user'])) {
            $result['user_name'] = $data['user']['user_name'] . '; ' . $data['user']['user_email'];
            $result['user_id'] = $data['user']['user_id'];
            $result['url_client'] = $data['user']['url'];
            $result['contractorId'] = $data['user']['contractorId'];
        }
        if (!empty($data['products'])) {
            $dataProductUpn = [];
            $logistOrderId = Order::find()->where(['source_innumber' => $data['order']['order_number']])->select('id')->scalar();
            $logistOrderEntry = OrderEntry::find()->where(['order_id' => $logistOrderId])->andWhere(['is_deleted' => null])->all();
            foreach ($logistOrderEntry as $item) {
                $dataProductUpn[] = [
                    'article' => $item->article,
                    'product_name' => $item->name,
                    'count' => 1,
                    'upn' => $item->upn ? $item->upn->id : null,
                    'upnBarcode' => $item->upn ? $item->upn->barcode : null,
                    'disabled' => !$item->upn || ReturnJournal::find()->where(['upn' => $item->upn->id])->andWhere(['order_number' => $data['order']['order_number']])->exists(),
                ];
            }
            $data['dataProductUpn'] = $dataProductUpn;
        }
        $upn = Yii::$app->request->post('upn');
        $article = Yii::$app->request->post('article');
        $result['html'] = $this->renderAjax('search-order', [
            'data' => $data,
            'title' => $title,
            'search' => $search,
            'upn' => $upn,
            'article' => $article,
        ]);
        return $result;
    }

    public function actionGetByUserName()
    {
        $dataArr = $this->createDataRequest('searchUser');
        return [
            'html' => $this->renderAjax('search-user', ['data' => $dataArr['data']]),
        ];
    }

    private function createDataRequest($url)
    {
        $key = Yii::$app->request->post('key');
        $value = Yii::$app->request->post('value');

        $data['token'] = Yii::$app->returnConfig->token;
        $data[$key] = $value;

        $client = new Client([
            'baseUrl' => Yii::$app->returnConfig->apiDataOrderUrl . $url,
        ]);
        /** @var \yii\httpclient\Response $response */
        $response = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('POST')
            ->setData($data)->send();
        if ($response->isOk) {
            return [
                'data' => $response->data,
                'search' => $value
            ];
        }

        return $response->statusCode;
    }

    public function actionGetByUserId()
    {
        $dataArr = $this->createDataRequest('userOrders');
        $data = $dataArr['data'];
        $user = $data['user'];
        return [
            'user_id' => !empty($user['user_id']) ? $user['user_id'] : false,
            'user_name' => !empty($user['user_name']) ? $user['user_name'] . '; ' .$user['user_email']  : false,
            'html' => $this->renderAjax('search-user-order', ['data' => $data]),
        ];
    }
}