<?php

namespace returnModule\controllers;

use returnModule\core\forms\IssueForm;
use returnModule\core\search\JournalSearch;
use returnModule\core\services\CurrentService;
use returnModule\core\services\JournalService;
use returnModule\core\services\UserService;
use returnModule\ReturnModule;
use Yii;

class DefaultController extends BaseController
{
    use TraitReturnController;

    protected $accessRoles = ['storageUser', 'chief'];

    /** @var string  */
    private $controllerTitle = 'Склад возвратов';
    /** @var CurrentService */
    private $currentService;

    public function __construct(
        $id,
        $module,
        UserService $userService,
        CurrentService $currentService,
        JournalService $journalService
    ) {
        parent::__construct($id, $module, $userService, $journalService);
        $this->currentService = $currentService;
    }

    public function actionIndex()
    {
        $searchModel = new JournalSearch($this->accessRoles);
        $dataProvider = $searchModel->searchStorage(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexWriteOff()
    {
        $searchModel = new JournalSearch($this->accessRoles);
        $dataProvider = $searchModel->searchStorageWriteOff(Yii::$app->request->queryParams);

        return $this->render('index_write_off', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateIssue()
    {
        $form = $this->currentService->getCreateIssueForm();
        $form->action = 'create-issue';
        return $this->issue($form);
    }

    public function actionUpdateIssue()
    {
        $form = $this->currentService->getUpdateIssueForm();
        $form->action = 'update-issue';
        return $this->issue($form);
    }

    private function issue(IssueForm $form)
    {
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->currentService->updateIssueForm($form);
                Yii::$app->session->setFlash('success', ReturnModule::t('Данные обновлены успешно!'));
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('issue', ['model' => $form]);
    }
}
