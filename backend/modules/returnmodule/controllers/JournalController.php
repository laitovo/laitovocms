<?php

namespace returnModule\controllers;

use returnModule\core\search\JournalSearch;
use returnModule\core\services\CurrentService;
use returnModule\core\services\JournalService;
use returnModule\core\services\UserService;
use Yii;

class JournalController extends BaseController
{
    use TraitReturnController;

    protected $accessRoles = ['*'];

    /** @var string  */
    private $controllerTitle = 'Журнал возвратов';
    /** @var CurrentService */
    private $currentService;

    public function __construct(
        $id,
        $module,
        UserService $userService,
        CurrentService $currentService,
        JournalService $journalService
    ) {
        parent::__construct($id, $module, $userService, $journalService);
        $this->currentService = $currentService;
    }

    public function actionIndex()
    {
//        $role = $this->userService->checkRole();
//        $admin = in_array('admin', $role);
//        if (in_array('support', $role)) { //
//            $this->redirect(['monitor-support/index']);
//        }elseif (in_array('storageUser', $role)) {
//            $this->redirect(['monitor-storage/index']);
//        }elseif (in_array('manager', $role)) {
//            $this->redirect(['monitor-manager/index']);
//        }elseif (in_array('logist', $role)) {
//            $this->redirect(['monitor-logist/index']);
//        }elseif (in_array('chief', $role) || $admin) {
            $searchModel = new JournalSearch($this->accessRoles);
            $dataProvider = $searchModel->searchAll(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
//        }
    }
}