<?php

namespace returnModule\controllers;

use returnModule\core\search\JournalSearch;
use returnModule\core\services\JournalService;
use returnModule\core\services\ManagerService;
use returnModule\core\services\UserService;
use returnModule\ReturnModule;
use Yii;

class MonitorManagerController extends BaseController
{
    use TraitReturnController;

    protected $accessRoles = ['manager'];

    /** @var string */
    private $controllerTitle = 'Монитор отдела продаж';
    /** @var ManagerService */
    private $managerService;

    public function __construct(
        $id,
        $module,
        UserService $userService,
        ManagerService $managerService,
        JournalService $journalService
    ) {
        parent::__construct($id, $module, $userService, $journalService);
        $this->managerService = $managerService;
    }

    public function actionIndex()
    {
        $searchModel = new JournalSearch($this->accessRoles);
        $dataProvider = $searchModel->searchManager(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        return $this->render('/share/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'controllerTitle' => $this->controllerTitle
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->managerService->getForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $this->managerService->updateForm($model);
                Yii::$app->session->setFlash('success', ReturnModule::t('Данные обновлены успешно!'));
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $model,
            'act' => $this->getAct($id),
        ]);
    }
}