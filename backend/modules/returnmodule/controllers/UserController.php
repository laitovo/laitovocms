<?php
namespace returnModule\controllers;

use returnModule\core\models\ReturnUser;
use returnModule\core\services\UserService;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;

class UserController extends Controller
{
    /** @var UserService */
    protected $userService;
    /** @var ReturnUser */
    private $returnUser;

    public function __construct($id, $module, UserService $userService, ReturnUser $returnUser)
    {
        $this->userService = $userService;
        $this->returnUser = $returnUser;
        parent::__construct($id, $module);
    }

    public function init()
    {
        if (!$this->userService->checkAdmin()) {
            throw new ForbiddenHttpException();
        }
        parent::init();
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReturnUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->returnUser::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = $this->userService->getUserForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if (Yii::$app->request->isPost) {
            $this->userService->createUpdateUserForm(Yii::$app->request->post());
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->userService->getUserForm($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if (Yii::$app->request->isPost) {
            $this->userService->createUpdateUserForm(Yii::$app->request->post(), $id);
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->returnUser->deleteItem($id);
        return $this->redirect(['index']);
    }
}