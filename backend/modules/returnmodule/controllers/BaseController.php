<?php

namespace returnModule\controllers;

use returnModule\core\services\JournalService;
use returnModule\core\services\SupportService;
use returnModule\core\services\UserService;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class BaseController extends Controller
{
    /** @var array */
    protected $roles;
    /** @var array */
    protected $accessRoles = [];

    /** @var UserService */
    protected $userService;
    /** @var JournalService */
    protected $journalService;

    public function __construct(
        $id,
        $module,
        UserService $userService,
        JournalService $journalService
    ) {
        $this->userService = $userService;
        $this->journalService = $journalService;
        parent::__construct($id, $module);
    }

    public function init()
    {
        parent::init();

        $this->roles = $this->userService->checkRole();
        if (!in_array('admin', $this->roles)) {
            $bool = false;
            foreach ($this->roles as $role) {
                foreach ($this->accessRoles as $item) {
                    if ($item == '*') {
                        $bool = true;
                        break 2;
                    }
                }
                if (in_array($role, $this->accessRoles)) {
                    $bool = true;
                    break;
                }
            }
            if (!$bool) {
                throw new ForbiddenHttpException();
            }
        }
    }
}