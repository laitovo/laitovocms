<?php

namespace returnModule\core\forms;

use returnModule\core\models\ReturnRole;
use returnModule\core\models\ReturnUser;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class UserForm extends Model
{
    /** @var int */
    public $id;
    /** @var string */
    public $email;
    /** @var string */
    public $name;
    /** @var array */
    public $roles;
    /** @var array */
    public $checkedRoles = [];

    public function init()
    {
        $this->roles = ArrayHelper::map(ReturnRole::find()->all(), 'id', 'name');
    }

    public function rules()
    {
        $data = [
            [['email', 'name', 'checkedRoles'], 'required'],
            ['name', 'string'],
            ['email', 'email'],
            ['email', 'validateOwnerEmail']
        ];
        if ($this->id === null) {
            $data[] = [['email'], 'unique', 'targetClass' => ReturnUser::class];
        } else {
            $data[] = [['email'], 'unique', 'targetClass' => ReturnUser::class, 'filter' => ['<>', 'id', $this->id]];
        }
        return $data;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя пользователя',
            'email' => 'Email (должен совпадать с почтой пользователя из основного приложения)',
            'checkedRoles' => 'Роли',
        ];
    }

    public function getUpdateForm($id)
    {
        $model = ReturnUser::find()->with('returnRoles')->where(['id' => $id])->one();
        if (!$model) {
            throw new NotFoundHttpException('User with id ' . $id . ' not found!');
        }
        $form = new self;
        $form->id = $model->id;
        $form->name = $model->name;
        $form->email = $model->email;
        $form->checkedRoles = ArrayHelper::getColumn($model->returnRoles, 'id');
        return $form;
    }

    public function validateOwnerEmail($attribute, $params)
    {
        if (!Yii::$app->user->identity->checkByEmail($this->$attribute)) {
            $this->addError($attribute, 'Пользователь с email ' . $this->$attribute . ' не найден в основном приложении!');
        }
    }
}