<?php
namespace returnModule\core\forms;

use returnModule\core\models\ReturnJournal;
use returnModule\ReturnModule;
use yii\base\Model;

class IssueForm extends Model
{
    /** @var int */
    public $id;
    /** @var string */
    public $barcode;
    /** @var int */
    public $location;
    /** @var array */
    public $location_arr = [];
    /** @var string */
    public $action;


    public function rules()
    {
        $data = [
            ['barcode', 'checkBarcode'],
        ];
        if ($this->location_arr) {
            $data[] = ['location', 'checkNull'];
            $data[] = ['location', 'required'];
        } else {
            $data[] = ['location', 'checkNotNull'];
        }
        return $data;
    }

    public function attributeLabels()
    {
        return [
            'location' => ReturnModule::t('Местонахождение'),
        ];
    }

    public function createLocation()
    {
        $this->location_arr = ReturnJournal::getLocationRoles();
    }

    /**
     * @param string $attribute barcode
     */
    public function checkBarcode($attribute)
    {
        $check = ReturnJournal::find()->where(['barcode' => $this->$attribute])->andWhere(['or',['is', 'resolution', null],['resolution' => 0]])->count();
        if (!$check) {
            $this->addError($attribute, ReturnModule::t('Barcode не найден!'));
        }
    }

    /**
     * @param string $attribute location
     */
    public function checkNull($attribute)
    {
        $check = $this->getCheck($attribute);
        if ($check !== null) {
            $this->addError($attribute, ReturnModule::t('Возврат уже выдан'));
        }
    }

    /**
     * @param string $attribute location
     */
    public function checkNotNull($attribute)
    {
        $check = $this->getCheck($attribute);
        if ($check === null) {
            $this->addError($attribute, ReturnModule::t('Возврат еще не выдан'));
        }
    }

    private function getCheck($attribute)
    {
        return ReturnJournal::find()->select($attribute)->where(['barcode' => $this->barcode])->scalar();
    }
}