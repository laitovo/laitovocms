<?php
namespace returnModule\core\forms;

use returnModule\helpers\GetDataForJournal;
use returnModule\ReturnModule;
use yii\base\Model;

class ChiefForm extends Model
{
    /** @var int */
    public $return_journal_id;
    /** @var bool */
    public $resume_material;
    /** @var bool */
    public $resume_money;
    /** @var bool */
    public $result_sum;
    /** @var array */
    public $arr = [];
    /** @var array */
    public $arr1 = [];

    public function init()
    {
        $this->arr[1] = ReturnModule::t('Оприходовать');
        $this->arr[0] = ReturnModule::t('Списать');

        $this->arr1[1] = ReturnModule::t('Вернуть деньги');
        $this->arr1[0] = ReturnModule::t('Не возвращать');
    }

    public function rules()
    {
        return [
            ['resume_material', 'boolean'],
            ['resume_material', 'required'],
            ['resume_money', 'boolean'],
            ['resume_money', 'required'],
            ['result_sum', 'number'],
            ['result_sum', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'resume_material' => ReturnModule::t('Решение по материалам'),
            'resume_money' => ReturnModule::t('Решение по деньгам'),
            'result_sum' => ReturnModule::t('Итоговая сумма выплаты'),
        ];
    }

    public function getDataForJournal($returnChief)
    {
        if (empty($returnChief)) {
            $returnChief = $this;
        }

        $data = GetDataForJournal::get($returnChief, $this->attributeLabels());

        foreach ($data as $key => $item) {
            if ($item['field'] == 'resume_material' && in_array($data[$key]['value'],array_keys($this->arr),true) ) {
                $data[$key]['value'] = $this->arr[$data[$key]['value']];
            }
            if ($item['field'] == 'resume_money' && in_array($data[$key]['value'],array_keys($this->arr1),true) ) {
                $data[$key]['value'] = $this->arr1[$data[$key]['value']];
            }
        }
        return $data;
    }

}