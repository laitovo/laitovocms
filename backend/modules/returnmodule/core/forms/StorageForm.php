<?php

namespace returnModule\core\forms;

use returnModule\helpers\GetDataForJournal;
use returnModule\core\models\ReturnLogist;
use returnModule\helpers\InsertValuesHelper;
use yii\base\Model;
use returnModule\core\models\ReturnJournal;
use returnModule\ReturnModule;

class StorageForm extends Model
{
    /** @var int */
    public $id;
    /** @var string */
    public $upn;
    /** @var string */
    public $barcode;
    /** @var string */
    public $article;
    /** @var integer */
    public $order_number;
    /** @var bool */
    public $order_type;
    /** @var integer */
    public $claim;
    /** @var string */
    public $why;
    /** @var string */
    public $source;
    /** @var string */
    public $resolution;
    /** @var string */
    public $comment;
    /** @var int */
    public $client_id;
    /** @var string */
    public $client_name_email;
    /** @var string */
    public $url_client;
    /** @var string */
    public $url_order;
    /** @var string */
    public $url_claim;
    /** @var bool */
    public $package_integrity;
    /** @var string */
    public $created_at;
    /** @var string */
    public $updated_at;
    /** @var int */
    public $status;
    /** @var int */
    public $result_upn_id;
    /** @var int */
    public $contractorId;

    public $source_arr = [];
    public $why_array = [];
    public $resolution_array = [];
    public $package_integrity_array = [];

    public function init()
    {
        foreach (ReturnJournal::SOURCE as $item) {
            $this->source_arr[$item['value']] = ReturnModule::t($item['name']);
        }
        foreach (ReturnJournal::WHY as $item) {
            $this->why_array[$item['value']] = ReturnModule::t($item['name']);
        }
        foreach (ReturnJournal::RESOLUTION as $item) {
            $this->resolution_array[$item['value']] = ReturnModule::t($item['name']);
        }
        $this->package_integrity_array[1] = ReturnModule::t('Да');
        $this->package_integrity_array[0] = ReturnModule::t('Нет');
    }

    public function rules()
    {
        $data = [
            [
                [
                    'upn',
                    'barcode',
                    'article',
                    'order_number',
                    'why',
                    'source',
                    'resolution',
                    'claim',
                    'comment',
                    'client_name_email',
                    'url_claim',
                    'url_client',
                    'url_order'
                ],
                'string'
            ],
            [
                [
                    'upn',
                    'article',
                    'order_number',
                    'why',
                    'source',
                    'resolution',
                    'claim',
                    'client_name_email',
                    'url_claim',
                    'url_client',
                    'url_order'
                ],
                'trim'
            ],
            [['order_number', 'client_id', 'result_upn_id', 'contractorId'], 'integer'],
            [['package_integrity', 'order_type'], 'boolean'],
            [['created_at', 'updated_at', 'status'], 'safe'],
            ['upn', 'unique', 'targetClass' => ReturnJournal::class],
            ['upn', 'default', 'value' => null]
        ];

        if ($this->id) {
            $data[] = [['package_integrity', 'resolution'], 'required'];
        } else {
            $data[] = [['article', 'source', 'why'], 'required'];
        }

        return $data;
    }

    public function attributeLabels()
    {
        $data = [
            'updated_at' => ReturnModule::t('Дата обновления'),
            'barcode' => ReturnModule::t('Upn (штрихкод)'),
        ];
        return array_merge($this->firstJournalPart(), $this->twoJournalPart(), $data);
    }

    public function firstJournalPart()
    {
        return [
            'created_at' => ReturnModule::t('Дата создания'),
            'upn' => 'UPN',
            'article' => ReturnModule::t('Артикул'),
            'order_number' => ReturnModule::t('Номер заказа'),
            'client_name_email' => ReturnModule::t('Имя, email клиента'),
            'claim' => ReturnModule::t('Номер рекламации'),
            'why' => ReturnModule::t('Причина возврата'),
            'source' => ReturnModule::t('Источник, откуда пришел заказ'),
            'comment' => ReturnModule::t('Комментарий'),
        ];
    }

    public function twoJournalPart()
    {
        return [
            'package_integrity' => ReturnModule::t('Целостность упаковки'),
            'resolution' => ReturnModule::t('Решение по возврату'),
            'updated_at' => ReturnModule::t('Завершено'),
        ];
    }

    public function getDataForJournal(ReturnJournal $returnJournal)
    {
        $data = GetDataForJournal::get($returnJournal, $this->firstJournalPart());
        foreach ($data as $key => $item) {
            if ($item['field'] == 'created_at') {
                $data[$key]['value'] = \Yii::$app->formatter->asDate($item['value'], 'php:Y-m-d H:i');
            }
            if ($item['field'] == 'source') {
                $data[$key]['value'] = ReturnModule::t($returnJournal->getSourceName());
            }
            if ($item['field'] == 'why') {
                $data[$key]['value'] = ReturnModule::t($returnJournal->getWhyName());
            }
        }
        return $data;
    }

    public function getDataForJournalTwo(ReturnJournal $returnJournal)
    {
        $data = [];
        $arr = $this->twoJournalPart();
        $data[] = $this->setValue($arr['package_integrity'], $returnJournal->package_integrity);
        $data[] = $this->setValue($arr['resolution'], $returnJournal->resolution, $returnJournal->resolutionName);
        $r = $returnJournal->resolution;
        $a = ReturnJournal::RESOLUTION['no_resolution']['value'];
        $b = ReturnJournal::RESOLUTION['resolution']['value'];
        if ($r != $a && $r != $b) {
            $updated_at = $returnJournal->updated_at ? \Yii::$app->formatter->asDate($returnJournal->updated_at,
                'php:Y-m-d H:i') : null;
            $data[] = $this->setValue($arr['updated_at'], $updated_at);
        }

        return $data;
    }

    private function setValue($name, $value, $string = null)
    {
        $data = [
            'name' => $name
        ];
        if ($value === null) {
            $data['value'] = ReturnModule::t('Не задано');
            return $data;
        }
        if (is_numeric($value) && $value > 1 && $string) {
            $data['value'] = $string;
            return $data;
        }
        if ($value == 0 || $value == 1) {
            $data['value'] = $value ? ReturnModule::t('Да') : ReturnModule::t('Нет');
            return $data;
        }
        if (is_string($value)) {
            $data['value'] = $value;
            return $data;
        }
        throw new \DomainException(__METHOD__);
    }
}