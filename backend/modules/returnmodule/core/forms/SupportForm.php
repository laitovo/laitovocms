<?php
namespace returnModule\core\forms;

use returnModule\core\models\ReturnSupport;
use returnModule\ReturnModule;
use yii\base\Model;

class SupportForm extends Model
{
    /** @var int */
    public $return_journal_id;
    /** @var bool */
    public $resume;
    /** @var array */
    public $arr = [];

    public function init()
    {
        $this->arr[1] = ReturnModule::t('Оприходовать');
        $this->arr[0] = ReturnModule::t('Списать');
    }

    public function rules()
    {
        return [
            ['resume', 'boolean'],
            ['resume', 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'resume' => ReturnModule::t('Решение'),
        ];
    }

    public function getDataForJournal($returnSupport)
    {
        if (empty($returnSupport)) {
            $returnSupport = $this;
        }
        $data[0] = [
            'name' => ReturnModule::t('Решение техслужбы'),
        ];
        if ($returnSupport->resume !== null) {
            $data[0]['value'] = $returnSupport->resume ?
                ReturnModule::t($this->arr[1]) : ReturnModule::t($this->arr[0]);
        } else {
            $data[0]['value'] = ReturnModule::t('Не задано');
        }
        return $data;
    }

}