<?php
namespace returnModule\core\forms;

use returnModule\helpers\GetDataForJournal;
use returnModule\ReturnModule;
use yii\base\Model;

class LogistForm extends Model
{
    /** @var int */
    public $id;
    /** @var int */
    public $return_journal_id;
    /** @var int */
    public $paid;
    /** @var double */
    public $paid_sum;
    /** @var double */
    public $no_paid_sum;
    /** @var double */
    public $shipping_amount;
    /** @var double */
    public $return_sum;
    /** @var double */
    public $cash_on_delivery;
    /** @var double */
    public $other_sum;
    /** @var double */
    public $all_sum;

    public function rules()
    {
        return [
            [['return_journal_id',], 'integer'],
            ['paid', 'boolean'],
            [['paid_sum', 'no_paid_sum', 'shipping_amount', 'return_sum', 'cash_on_delivery', 'other_sum', 'all_sum'], 'number'],
            ['other_sum', 'default', 'value' => 0],
        ];
    }

    public function attributeLabels()
    {
        return [
            'paid' => ReturnModule::t('Оплачено'),
            'paid_sum' => ReturnModule::t('Сумма оплачено'),
            'no_paid_sum' => ReturnModule::t('Сумма не оплачено'),
            'shipping_amount' => ReturnModule::t('Сумма за доставку'),
            'return_sum' => ReturnModule::t('Сумма за возврат'),
            'cash_on_delivery' => ReturnModule::t('Наложенный платеж'),
            'other_sum' => ReturnModule::t('Прочие'),
        ];
    }

    public function getDataForJournal($returnLogist)
    {
        if (empty($returnLogist)) {
            $returnLogist = $this;
        }
        $data = GetDataForJournal::get($returnLogist, $this->attributeLabels());
        $sum = 0;
        foreach ($data as $key => $item) {
            if ($item['field'] == 'return_sum') {
                $sum = $item['value'];
            }
            //$sum += $item['value'];
            if ($item['field'] == 'paid') {
                $data[$key]['value'] = $data[$key]['value'] == 1 ? ReturnModule::t('Да') :
                    ReturnModule::t('Нет');
            }
        }
        $data[] = [
            'name' => ReturnModule::t('Итого'),
            'value' => round($sum, 2),
        ];
        return $data;
    }
}