<?php

namespace returnModule\core\forms;

use returnModule\core\models\ReturnSalesDepartment;
use returnModule\ReturnModule;
use yii\base\Model;

class ManagerForm extends Model
{
    /** @var boolean */
    public $resume;
    /** @var double */
    public $result_sum;
    /** @var array */
    public $arr = [];
    /** @var int */
    public $return_journal_id;

    /** @var double */
    public $logistics_result_sum;

    public function init()
    {
        $this->arr[1] = ReturnModule::t('Да');
        $this->arr[0] = ReturnModule::t('Нет');
    }

    public function rules()
    {
        return [
            ['resume', 'boolean'],
            ['resume', 'required'],
            ['result_sum', 'double'],
            ['result_sum', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'resume' => ReturnModule::t('Принять возврат'),
            'result_sum' => ReturnModule::t('Сумма к возврату клиенту, руб.'),
            'logistics_result_sum' => ReturnModule::t('* Справочная сумма к возврату, руб.'),
        ];
    }

    public function getDataForJournal($salesDepartment)
    {
        if (empty($salesDepartment)) {
            $salesDepartment = $this;
        }
        $data[0] = [
            'name' => ReturnModule::t('Решение отдела продаж'),
        ];
        if ($salesDepartment->resume !== null) {
            $data[0]['value'] = $salesDepartment->resume ?
                ReturnModule::t('Принять возврат') : ReturnModule::t('Отклонить');
        } else {
            $data[0]['value'] = ReturnModule::t('Не задано');
        }
        $data[1] = [
            'name' => ReturnModule::t('Сумма к возврату клиенту, руб'),
        ];
        $data[1]['value'] = $salesDepartment->result_sum;

        return $data;
    }
}