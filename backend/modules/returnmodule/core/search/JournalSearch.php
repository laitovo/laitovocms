<?php

namespace returnModule\core\search;

use returnModule\core\models\ReturnJournal;
use returnModule\ReturnModule;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class JournalSearch extends Model
{
    private $roles;

    public $id;
    public $upn;
    public $article;
    public $created_at;
    public $status;
    public $resolution;
    public $client_name_email;

    public function __construct(array $roles = [], $config = [])
    {
        parent::__construct($config);
        if ($roles) {
            $this->roles = ReturnJournal::getRolesStatus($roles);
        }
    }

    public function rules()
    {
        return [
            [['id', 'status', 'resolution'], 'integer'],
            [['upn', 'article','client_name_email'], 'string'],
            ['created_at', 'safe'],
        ];
    }

    public function search($params)
    {
        $dataProvider = $this->searchWhere(null, $params);
        return $this->sortSearch($dataProvider);
    }

    public function searchStorage($params)
    {
        $chief = ReturnJournal::RESOLUTION['resolution']['value'];
        $no_resolution = ReturnJournal::RESOLUTION['no_resolution']['value'];
        $write_off = ReturnJournal::RESOLUTION['write-off']['value'];
        $status = ReturnJournal::STATUS['end']['value'];
        $where = "(`resolution` = $no_resolution OR `resolution` = $chief OR `resolution` = $write_off) AND `status` <> $status";
        $dataProvider =  $this->searchWhere($where, $params);
        return $this->sortSearch($dataProvider);
    }

    public function searchStorageWriteOff($params)
    {
        $main = ReturnJournal::RESOLUTION['write-off']['value'];
        $status = ReturnJournal::STATUS['end']['value'];
        $where = "`resolution` = $main and `status` = $status";
        $dataProvider =  $this->searchWhere($where, $params);
        return $this->sortSearch($dataProvider);
    }

    public function searchManager($params)
    {
        $chief = ReturnJournal::RESOLUTION['resolution']['value'];
        $no_resolution = ReturnJournal::RESOLUTION['no_resolution']['value'];
        $where = "`resolution` = $no_resolution OR `resolution` = $chief";
        $dataProvider =  $this->searchWhere($where, $params);
        return $this->sortSearch($dataProvider);
    }

    public function searchSupport($params)
    {
        $chief = ReturnJournal::RESOLUTION['resolution']['value'];
        $no_resolution = ReturnJournal::RESOLUTION['no_resolution']['value'];
        $where = "`resolution` = $no_resolution OR `resolution` = $chief";
        $dataProvider =  $this->searchWhere($where, $params);
        return $this->sortSearch($dataProvider);
    }

    public function searchChief($params)
    {
        $chief = ReturnJournal::RESOLUTION['resolution']['value'];
        $no_resolution = ReturnJournal::RESOLUTION['no_resolution']['value'];
        $where = "`resolution` = $no_resolution OR `resolution` = $chief";
        $dataProvider =  $this->searchWhere($where, $params);
        return $this->sortSearch($dataProvider);
    }

    public function searchAll($params)
    {
        return $this->searchWhere(null, $params);
    }

    private function sortSearch(ActiveDataProvider $dataProvider)
    {
        /** @var ReturnJournal[] $models */
        $dataProvider->setPagination(false);
        $models = $dataProvider->getModels();
        if ($models) {
            $return_role = [];
            $return__no_role = [];
            foreach ($models as $key => $model) {
                if (in_array($model->status, $this->roles)) {
                    $model->show_btn = true;
                    $return_role[] = $model;
                    unset($models[$key]);
                } else {
                    if (!$model->resolution) {
                        $return__no_role[] = $model;
                        unset($models[$key]);
                    }
                }
            }
            $models = array_merge($return_role, $return__no_role, $models);
            $keys = [];
            foreach ($models as $model) {
                $keys[] = $model->id;
            }
            $dataProvider->setKeys($keys);
            $dataProvider->setModels($models);
        }
        return $dataProvider;
    }

    private function searchWhere($where, $params)
    {
        $query = ReturnJournal::find()->where($where);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $this->setFilter($query);
        return $dataProvider;
    }

    private function setFilter(ActiveQuery $query)
    {
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['resolution' => $this->resolution]);
        $query->andFilterWhere(['like', 'upn', $this->upn]);
        $query->andFilterWhere(['like', 'article', $this->article]);
        $query->andFilterWhere(['like', 'client_name_email', $this->client_name_email]);
    }
}