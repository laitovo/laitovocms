<?php
namespace returnModule\core\services;

use returnModule\core\forms\ChiefForm;
use returnModule\core\models\ReturnChief;
use returnModule\core\models\ReturnJournal;
use returnModule\core\models\ReturnSalesDepartment;
use returnModule\core\models\ReturnSupport;
use yii\httpclient\Client;

class ChiefService
{
    /** @var ReturnJournal */
    private $returnJournal;
    /** @var ReturnChief */
    private $returnChief;
    /** @var ChiefForm */
    private $chiefForm;
    /** @var ReturnSalesDepartment */
    private $salesDepartment;
    /** @var ReturnSupport */
    private $returnSupport;

    public function __construct(
        ReturnJournal $returnJournal,
        ReturnChief $returnChief,
        ReturnSalesDepartment $salesDepartment,
        ReturnSupport $returnSupport,
        ChiefForm $chiefForm
    )
    {
        $this->returnJournal = $returnJournal;
        $this->returnChief = $returnChief;
        $this->chiefForm = $chiefForm;
        $this->salesDepartment = $salesDepartment;
        $this->returnSupport = $returnSupport;
    }

    public function getForm($return_journal_id)
    {
        $check = $this->returnJournal::find()->where(['id' => $return_journal_id])->count();
        if (!$check) {
            throw new \DomainException('Not found $return_journal_id in ' . __METHOD__);
        }
        $form = $this->chiefForm;

        $managerData = $this->salesDepartment::find()->where(['return_journal_id' => $return_journal_id])->one();
        if ($managerData) {
            $form->result_sum = $managerData->result_sum;
            $form->resume_money =  $managerData->result_sum == 0 ? 0 : 1;
            $supportData = $this->returnSupport::find()->where(['return_journal_id' => $return_journal_id])->one();
            if ($supportData) {
                $form->resume_material = $supportData->resume && $managerData->resume ? 1 : 0;
            }

        }

        $form->return_journal_id = $return_journal_id;
        return $form;
    }

    /**
     * @param ChiefForm $chiefForm
     * @throws \yii\db\Exception
     */
    public function updateForm(ChiefForm $chiefForm)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = $this->returnJournal->getItem($chiefForm->return_journal_id);
            $this->returnChief->return_journal_id = $model->id;
            $this->returnChief->resume_material = $chiefForm->resume_material;
            $this->returnChief->resume_money = $chiefForm->resume_money;
            $this->returnChief->result_sum = $chiefForm->result_sum;
            $this->returnChief->saveItem();

            $model->status =  $model::STATUS['finish_storage']['value'];
            //Если сказано было списать, тогда однозначно списываем возврат.
            if (!$chiefForm->resume_material) {
                $model->resolution = $model::RESOLUTION['write-off']['value'];
            }
            $model->saveItem();

            // отправляем данные в клиентское приложение
            $client = new Client();
            $url = \Yii::$app->params['client_url'] . 'return';
            /** @var \yii\httpclient\Response $response */
            $response = $client->createRequest()
                ->setHeaders(['Authorization' => 'Bearer ' . \Yii::$app->user->identity->authKey])
                ->setMethod('POST')
                ->setFormat(Client::FORMAT_JSON)
                ->setUrl($url)
                ->setData([
                    'contractorId' => $model->contractorId,
                    'numberBiz' => $model->id,
                    'total' => $this->returnChief->result_sum,
                    'date' => $model->created_at,
                ])
                ->send();
            if (!$response->isOk) {
                throw new \DomainException($response->content);
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \DomainException($e->getMessage());
        }
    }
}