<?php
namespace returnModule\core\services;

use returnModule\core\forms\UserForm;
use returnModule\core\models\ReturnRole;
use returnModule\core\models\ReturnUser;
use returnModule\core\models\ReturnUserRole;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\Session;
use Yii;

class UserService
{
    /** @var Session */
    private $session;
    /** @var ReturnRole */
    private $returnRole;
    /** @var string */
    private $session_key = 'return_role';
    /** @var UserForm */
    private $userForm;
    /** @var ReturnUser */
    private $returnUser;
    /** @var ReturnUserRole */
    private $returnUserRole;

    public function __construct(
        Session $session,
        ReturnRole $returnRole,
        UserForm $userForm,
        ReturnUser $returnUser,
        ReturnUserRole $returnUserRole)
    {
        $this->session = $session;
        $this->returnRole = $returnRole;
        $this->userForm = $userForm;
        $this->returnUser = $returnUser;
        $this->returnUserRole = $returnUserRole;
    }

    public function checkRole()
    {
        $session = $this->session;

//        if (!$session->has($this->session_key)) {
            if (Yii::$app->user->identity->isRoot() || $this->checkAdmin()) {
                $roles = ['admin'];
            } else {
                $email = Yii::$app->user->identity->getEmail();
                $checkEmail = $this->returnRole::find()
                    ->alias('rr')->joinWith('returnUsers ru')->where(['ru.email' => $email])->all();
                if (!$checkEmail) {
                    throw new ForbiddenHttpException();
                }
                $roles = ArrayHelper::getColumn($checkEmail, 'role');
            }
            $session->set($this->session_key, $roles);
            return $roles;
//        }
//        return $session->get($this->session_key);
    }

    public function checkAdmin()
    {
        $checkMain = Yii::$app->user->identity->getRole();
        $mainRoles = Yii::$app->returnConfig->mainRoles;
        return in_array($checkMain, $mainRoles) || Yii::$app->user->identity->getId() == 36;
    }

    public function changeRole($role)
    {
        if (!$this->checkAdmin()) {
            throw new ForbiddenHttpException();
        }
        $roles = array_keys(Yii::$app->returnConfig->roles);
        if (!in_array($role, $roles)) {
            throw new \DomainException('Role ' . $role . ' not found!');
        }
        $session = $this->session;
        $roles = [$role];
        $session->set($this->session_key, $roles);
        return $roles;
    }

    public function getUserForm(int $id = null)
    {
        if ($id === null) {
            return new $this->userForm;
        }
        return $this->userForm->getUpdateForm($id);
    }

    public function createUpdateUserForm(array $data, int $userId = null)
    {
        $model = $userId === null ? $this->returnUser : $this->returnUser->getItem($userId);
        /** @var UserForm $form */
        $form = $userId === null ? $this->userForm : $this->userForm->getUpdateForm($userId);
        if ($form->load($data) && $form->validate()) {
            $model->name = $form->name;
            $model->email = $form->email;
            $model->saveItem();
            $userId = $model->id;
            $this->returnUserRole::deleteAll(['return_user_id' => $userId]);
            foreach ($form->roles as $roleId => $role) {
                if (in_array($roleId, $form->checkedRoles)) {
                    /** @var ReturnUserRole $returnUserRole */
                    $returnUserRole = new $this->returnUserRole;
                    $returnUserRole->return_user_id = $userId;
                    $returnUserRole->return_role_id = $roleId;
                    $returnUserRole->saveItem();
                }
            }
        }
    }
}