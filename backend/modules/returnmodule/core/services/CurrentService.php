<?php

namespace returnModule\core\services;

use returnModule\core\forms\IssueForm;
use returnModule\core\models\ReturnJournal;

class CurrentService
{
    /** @var IssueForm */
    private $issueForm;
    /** @var ReturnJournal */
    private $returnJournal;

    public function __construct(IssueForm $issueForm, ReturnJournal $returnJournal) {
        $this->issueForm = $issueForm;
        $this->returnJournal = $returnJournal;
    }

    public function getCreateIssueForm()
    {
        $form = $this->issueForm;
        $form->createLocation();
        return $form;
    }

    public function getUpdateIssueForm()
    {
        return $this->issueForm;
    }

    public function updateIssueForm(IssueForm $form)
    {
        $model = $this->returnJournal::find()->where(['barcode' => $form->barcode])->one();
        if (!$model) {
            throw new \DomainException('Return not found ' . __METHOD__);
        }
        if ($form->location) {
            $model->location = $form->location;
        } else {
            $model->location = null;
        }

        $model->saveItem();
    }
}