<?php
namespace returnModule\core\services;

use returnModule\core\forms\SupportForm;
use returnModule\core\models\ReturnJournal;
use returnModule\core\models\ReturnSalesDepartment;
use returnModule\core\models\ReturnSupport;

class SupportService
{
    /** @var ReturnJournal */
    private $returnJournal;
    /** @var ReturnSupport */
    private $returnSupport;
    /** @var ReturnSalesDepartment */
    private $returnSalesDepartment;
    /** @var SupportForm */
    private $supportForm;

    public function __construct(
        ReturnJournal $returnJournal,
        ReturnSupport $returnSupport,
        ReturnSalesDepartment $returnSalesDepartment,
        SupportForm $supportForm
    )
    {
        $this->returnJournal = $returnJournal;
        $this->returnSupport = $returnSupport;
        $this->returnSalesDepartment = $returnSalesDepartment;
        $this->supportForm = $supportForm;
    }

    public function getForm($return_journal_id)
    {
        $check = $this->returnJournal::find()->where(['id' => $return_journal_id])->count();
        if (!$check) {
            throw new \DomainException('Not found $return_journal_id in ' . __METHOD__);
        }
        $form = $this->supportForm;
        $form->return_journal_id = $return_journal_id;
        return $form;
    }

    public function updateForm(SupportForm $managerForm)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = $this->returnJournal->getItem($managerForm->return_journal_id);
            $this->returnSupport->return_journal_id = $model->id;
            $this->returnSupport->resume = $managerForm->resume;
            $this->returnSupport->saveItem();

            /** @var ReturnSalesDepartment $salesData*/
            $salesData = $this->returnSalesDepartment::find()->where(['return_journal_id' => $managerForm->return_journal_id])->one();
            if (!$salesData) {
                throw new \DomainException('Not found $return_journal_id in ' . __METHOD__);
            }

//            $model->status = $salesData->result_sum ? $model::STATUS['chief']['value'] : $model::STATUS['finish_storage']['value'] ;
            $model->status = $model::STATUS['chief']['value'];
//            $model->status = $model::STATUS['finish_storage']['value'] ;
//            if (!$this->returnSupport->resume) {
//                $model->resolution = $model::RESOLUTION['resolution']['value'];
//            }
            $model->saveItem();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \DomainException($e->getMessage());
        }
    }
}