<?php
namespace returnModule\core\services;

use backend\modules\logistics\models\Naryad;
use core\logic\SStorageState;

/**
 * Сервис исполнения решения по возврату
 * Class SResolution
 * @package returnModule\core\services
 */
class SResolution
{
    /**
     * Функция отвечает за пориходование на основной склад
     *
     * @param $article
     * @param $upn
     * @return bool
     */
    public static function putStorage($article,$upn)
    {
        if (!$article) return false;
        if (!$upn) {
            return SStorageState::addStateForArticle($article);
        }

        return SStorageState::addStateForUpn($upn);
    }

    /**
     * Функция отвечает за пориходование на склад-отстойник
     *
     * @param $article
     * @param $upn
     * @return bool
     */
    public static function putSump($article,$upn)
    {
        if (!$article) return false;
        if (!$upn) {
            return SStorageState::addStateForArticle($article,true);
        }

        return SStorageState::addStateForUpn($upn,true);
    }
}