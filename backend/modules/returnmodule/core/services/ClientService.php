<?php

namespace returnModule\core\services;

use yii\httpclient\Client;
use Yii;

class ClientService
{
    /** @var \yii\httpclient\Request  */
    private $request;

    public function __construct()
    {
        $client = new Client([
            'baseUrl' => Yii::$app->returnConfig->apiUrl,
        ]);
        $this->request = $client
            ->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('POST')
            ->addHeaders(['Authorization' => 'Bearer ' . Yii::$app->returnConfig->ownerUserToken]);
    }

    public function getRole()
    {
        /** @var \yii\httpclient\Response $response */
        $response = $this->request->setData(['get' => 'role'])->send();
        if ($response->isOk) {
            $data = $response->data;
            return $data['role'];
        }

    }
}