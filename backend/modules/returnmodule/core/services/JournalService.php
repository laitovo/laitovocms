<?php
namespace returnModule\core\services;

use returnModule\core\forms\ChiefForm;
use returnModule\core\forms\LogistForm;
use returnModule\core\forms\ManagerForm;
use returnModule\core\forms\StorageForm;
use returnModule\core\forms\SupportForm;
use returnModule\core\models\ReturnJournal;
use returnModule\ReturnModule;

class JournalService
{
    /** @var ReturnJournal */
    private $returnJournal;
    /** @var StorageForm */
    private $storageForm;
    /** @var LogistForm */
    private $logistForm;
    /** @var ManagerForm */
    private $managerForm;
    /** @var SupportForm */
    private $supportForm;
    /** @var ChiefForm */
    private $chiefForm;

    public function __construct(
        ReturnJournal $returnJournal,
        StorageForm $storageForm,
        LogistForm $logistForm,
        ManagerForm $managerForm,
        SupportForm $supportForm,
        ChiefForm $chiefForm
    )
    {

        $this->returnJournal = $returnJournal;
        $this->storageForm = $storageForm;
        $this->logistForm = $logistForm;
        $this->managerForm = $managerForm;
        $this->supportForm = $supportForm;
        $this->chiefForm = $chiefForm;
    }

    public function getJournal($id = null)
    {
        if ($id) {
            $model = $this->returnJournal::find()
                ->with('returnLogist')
                ->with('returnSalesDepartment')
                ->with('returnSupport')
                ->where(['id' => $id])
                ->one();
        } else {
            $model = $this->returnJournal;
        }

        $result = [
            'urls' => [],
            'act' => [],
        ];

        if ($model->url_order) {
            $result['urls']['order'] = [
                'str' => ReturnModule::t('Заказ (просмотр)'),
                'url' => $model->url_order,
            ];
        }

        if ($model->url_claim) {
            $result['urls']['claim'] = [
                'str' => ReturnModule::t('Рекламация (просмотр)'),
                'url' => $model->url_claim,
            ];
        }

        if ($model->url_client) {
            $result['urls']['client'] = [
                'str' => ReturnModule::t('Клиент (просмотр)'),
                'url' => $model->url_client,
            ];
        }

        if ($model->result_upn_id && ($upn = $model->resultUpn) && ($state = $upn->storageState)) {
            $result['printUrl'] = "/logistics/naryad/print-single-label?id={$state->upn_id}&literal={$state->literal}";
            $result['literal'] = $state->literal;
        }

        $currentStatus = $model->status;
        $result['barcode'] = $model->barcode;

        $result['act'][] = new DtoJournal([
            'data' => $this->storageForm->getDataForJournal($model),
            'title' => ReturnModule::t('Регистрация возврата'),
            'processed' => true,
        ]);

        $formStatus = ReturnJournal::STATUS['start_logist']['value'];
        $result['act'][] = new DtoJournal([
            'data' => $this->logistForm->getDataForJournal($model->returnLogist),
            'title' => ReturnModule::t('Обработка логистом'),
            'processed' => $this->checkStatus($formStatus, $currentStatus),
        ]);

        $formStatus = ReturnJournal::STATUS['start_manager']['value'];
        $result['act'][] = new DtoJournal([
            'data' => $this->managerForm->getDataForJournal($model->returnSalesDepartment),
            'title' => ReturnModule::t('Отдел продаж'),
            'processed' => $this->checkStatus($formStatus, $currentStatus),
        ]);

        $formStatus = ReturnJournal::STATUS['start_support']['value'];
        $result['act'][] = new DtoJournal([
            'data' => $this->supportForm->getDataForJournal($model->returnSupport),
            'title' => ReturnModule::t('Техслужба'),
            'processed' => $this->checkStatus($formStatus, $currentStatus),
        ]);

        $formStatus = ReturnJournal::STATUS['chief']['value'];
        $result['act'][] = new DtoJournal([
            'data' => $this->chiefForm->getDataForJournal($model->returnChief),
            'title' => ReturnModule::t('Руководитель'),
            'processed' => $this->checkStatus($formStatus, $currentStatus),
        ]);

        $formStatus = ReturnJournal::STATUS['finish_storage']['value'];
        $result['act'][] = new DtoJournal([
            'data' => $this->storageForm->getDataForJournalTwo($model),
            'title' => ReturnModule::t('Решение кладовщика'),
            'processed' => $this->checkStatus($formStatus, $currentStatus),
        ]);

        return $result;
    }

    private function checkStatus($first, $two)
    {
        return $first < $two;
    }
}