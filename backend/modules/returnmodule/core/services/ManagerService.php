<?php

namespace returnModule\core\services;

use returnModule\core\forms\ManagerForm;
use returnModule\core\models\ReturnJournal;
use returnModule\core\models\ReturnLogist;
use returnModule\core\models\ReturnSalesDepartment;

class ManagerService
{
    /** @var ReturnJournal */
    private $returnJournal;
    /** @var ReturnSalesDepartment */
    private $salesDepartment;
    /** @var ReturnLogist */
    private $returnLogistics;
    /** @var ManagerForm */
    private $managerForm;

    public function __construct(
        ReturnJournal $returnJournal,
        ReturnSalesDepartment $salesDepartment,
        ReturnLogist $returnLogistics,
        ManagerForm $managerForm
    ) {
        $this->returnJournal = $returnJournal;
        $this->salesDepartment = $salesDepartment;
        $this->returnLogistics = $returnLogistics;
        $this->managerForm = $managerForm;
    }

    public function getForm($return_journal_id)
    {
        $check = $this->returnJournal::find()->where(['id' => $return_journal_id])->count();
        if (!$check) {
            throw new \DomainException('Not found $return_journal_id in ' . __METHOD__);
        }

        $logisticsData = $this->returnLogistics::find()->where(['return_journal_id' => $return_journal_id])->one();
        if (!$logisticsData) {
            throw new \DomainException('Not found $return_journal_id in ' . __METHOD__);
        }
        $form = $this->managerForm;
        $form->return_journal_id = $return_journal_id;
        $form->logistics_result_sum = $logisticsData->paid_sum > $logisticsData->return_sum ? $logisticsData->return_sum : 0;
        return $form;
    }

    public function updateForm(ManagerForm $managerForm)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = $this->returnJournal->getItem($managerForm->return_journal_id);
            $this->salesDepartment->return_journal_id = $model->id;
            $this->salesDepartment->resume = $managerForm->resume;
            $this->salesDepartment->result_sum = $managerForm->result_sum;
            $this->salesDepartment->saveItem();
            $model->status = ($model->why == ReturnJournal::WHY['not_collected']['value'] ? ReturnJournal::STATUS['finish_storage']['value'] : ReturnJournal::STATUS['start_support']['value']);
            if (!$this->salesDepartment->resume) {
                $model->resolution = $model::RESOLUTION['resolution']['value'];
            }
            $model->saveItem();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \DomainException($e->getMessage());
        }
    }
}