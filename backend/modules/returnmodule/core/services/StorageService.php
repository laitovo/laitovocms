<?php

namespace returnModule\core\services;

use common\models\logistics\StorageState;
use returnModule\core\forms\StorageForm;
use returnModule\core\models\ReturnJournal;
use returnModule\core\models\ReturnLogist;
use returnModule\ReturnModule;
use yii\httpclient\Client;
use Yii;

class StorageService
{
    /** @var ReturnJournal */
    private $returnJournal;
    /** @var StorageForm */
    private $storageForm;
    /** @var ReturnLogist */
    private $returnLogist;

    public function __construct(ReturnJournal $returnJournal, StorageForm $storageForm, ReturnLogist $returnLogist)
    {
        $this->returnJournal = $returnJournal;
        $this->storageForm = $storageForm;
        $this->returnLogist = $returnLogist;
    }

    public function getForm()
    {
        return $this->storageForm;
    }

    public function createReturn(StorageForm $form)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($form->why == ReturnJournal::WHY['other']['value']) {
                if (!$form->comment) {
                    throw new \DomainException(ReturnModule::t('Если причина возврата Прочее - поле комментарий обязательно для заполнения'));
                }
            }
            $model = $this->returnJournal;
            $model->upn = $form->upn;
            $model->article = $form->article;
            $model->source = $form->source;
            $model->why = $form->why;
            $model->claim = $form->claim;
            $model->resolution = ReturnJournal::RESOLUTION['no_resolution']['value'];
            $model->client_id = $form->client_id;
            $model->contractorId = $form->contractorId;
            $model->client_name_email = $form->client_name_email;
            $model->order_number = $form->order_number;
            $model->claim = $form->claim;
            $model->order_type = $form->order_type;
            $model->status = $form->order_type  ? ReturnJournal::STATUS['start_manager']['value'] :
                ($form->why == ReturnJournal::WHY['not_collected']['value'] ? ReturnJournal::STATUS['finish_storage']['value'] : ReturnJournal::STATUS['start_manager']['value']);
            $model->url_claim = $form->url_claim;
            $model->url_order = $form->url_order;
            $model->url_client = $form->url_client;
            $model->saveItem();
            $barcode = $model->id;
            $length = strlen($barcode);
            if ($length < 5) {
                for ($i = 0; $i < 5 - $length; $i++) {
                    $barcode = '0' . $barcode;
                }
            }
            $model->barcode = 'R' . $barcode;
            $model->saveItem();
            $dataLogist = $this->dataLogist($model->order_number, $model->article);
            if ($dataLogist) {
                $this->returnLogist->paid_sum = $dataLogist['paid_sum'];
                $this->returnLogist->no_paid_sum = $dataLogist['no_paid_sum'];
                $this->returnLogist->shipping_amount = $dataLogist['shipping_amount'];
                $this->returnLogist->cash_on_delivery = $dataLogist['cash_on_delivery'] ? $dataLogist['full_order_amount']
                    : 0;
                $this->returnLogist->return_sum = $dataLogist['return_sum'];
                $this->returnLogist->paid = (($dataLogist['full_order_amount'] - $dataLogist['paid_sum']) <= 0);
                $this->returnLogist->return_journal_id = $model->id;
                $this->returnLogist->saveItem();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \DomainException($e->getMessage());
        }
    }

    private function dataLogist($order_id, $article)
    {
        $data = [
            'token' => Yii::$app->returnConfig->token,
            'order_id' => $order_id,
            'article' => $article,
        ];

        $client = new Client([
            'baseUrl' => Yii::$app->returnConfig->apiDataOrderUrl . 'dataLogist',
        ]);
        /** @var \yii\httpclient\Response $response */
        $response = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('POST')
            ->setData($data)->send();
        if ($response->isOk) {
            return $response->data;
        }
        return false;
    }

    public function getUpdateForm($id)
    {
        $model = $this->returnJournal::find()->where(['id' => $id])->one();
        if (!$model) {
            throw new \DomainException('Not found data in ' . __METHOD__);
        }
        $this->storageForm->id = $id;
        $this->storageForm->resolution = $model->resolution;
        $this->storageForm->package_integrity = $model->package_integrity;
        return $this->storageForm;
    }

    public function update(StorageForm $form)
    {
        $model = $this->returnJournal->getItem($form->id);
        $model->package_integrity = $form->package_integrity;
        $model->resolution = $form->resolution;
        $model->status = $form->resolution == $model::RESOLUTION['resolution']['value'] ?
            $model::STATUS['chief']['value'] : $model::STATUS['end']['value'];
        $model->saveItem();
        if ($model->status == $model::STATUS['end']['value'] && $model->why == $model::WHY['not_collected']['value']) {
            if ($model->client_id) {
                $client = new Client();
                $data = [
                    'token' => Yii::$app->returnConfig->token,
                    'user_id' => $model->client_id,
                ];
                /** @var \yii\httpclient\Response $response */
                $client->createRequest()
                    ->setUrl(Yii::$app->returnConfig->setBadUser)
                    ->setFormat(Client::FORMAT_JSON)
                    ->setMethod('POST')
                    ->setData($data)->send();
            }
        }
        $msg = null;
        if ($model->resolution == ReturnJournal::RESOLUTION['sump']['value'] && ($state = SResolution::putSump($model->article,$model->upn))) {
            /**
             * @var $state StorageState
             */
            $model->result_upn_id = $state->upn_id;
            $model->saveItem();
            $msg = 'Возврат перенесен в отстойник';

        }
        if ($model->resolution == ReturnJournal::RESOLUTION['debit']['value'] && ($state = SResolution::putStorage($model->article,$model->upn))) {
            $model->result_upn_id = $state->upn_id;
            $model->saveItem();
            $msg = 'Возврат оприходован на склад';
        }
        if ($model->resolution == ReturnJournal::RESOLUTION['resolution']['value']) {
            $msg = 'Возврат ожидает решения руководителя';
        }
        if ($model->resolution == ReturnJournal::RESOLUTION['write-off']['value']) {
            $msg = 'Возврат должен быть помещен на склад списаний возвратов !!!';
        }
        return $msg;
    }

    public function resolution($id, $package_integrity)
    {
        $model = $this->returnJournal->getItem($id);
        if (!$model->resolution && $package_integrity) {
            return ReturnJournal::RESOLUTION['debit']['value'];
        }
        if (!$model->resolution && !$package_integrity) {
            return ReturnJournal::RESOLUTION['sump']['value'];
        }
        $model->package_integrity = $package_integrity;
        $model->saveItem();
        return false;
    }
}