<?php
namespace returnModule\core\services;


use returnModule\core\forms\LogistForm;
use returnModule\core\models\ReturnJournal;
use returnModule\core\models\ReturnLogist;

class LogistServise
{
    /** @var ReturnLogist */
    private $returnLogist;
    /** @var LogistForm */
    private $logistForm;
    /** @var ReturnJournal */
    private $returnJournal;

    public function __construct(ReturnLogist $returnLogist, LogistForm $logistForm, ReturnJournal $returnJournal)
    {
        $this->returnLogist = $returnLogist;
        $this->logistForm = $logistForm;
        $this->returnJournal = $returnJournal;
    }

    public function getForm($return_journal_id)
    {
        $check = $this->returnJournal::find()->where(['id' => $return_journal_id])->count();
        if (!$check) {
            throw new \DomainException('Not found $return_journal_id in ' . __METHOD__);
        }
        $model = $this->returnLogist::find()->where(['return_journal_id' => $return_journal_id])->one();
        $form = $this->logistForm;
        if (!$model) {
            $model = $this->returnLogist;
            $model->return_journal_id = $return_journal_id;
            foreach ($form->attributeLabels() as $key => $item) {
                if ($key != 'other_sum') {
                    $model->$key = 0;
                }
            }
            $model->saveItem();
        }
        $form->id = $model->id;
        foreach ($form->attributeLabels() as $key => $item) {
            if ($key != 'other_sum') {
                $form->$key = $model->$key;
            }
        }

        return $form;
    }

    public function updateForm(LogistForm $form)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = $this->returnLogist->getItem($form->id);
            $model->other_sum = $form->other_sum;
            $returnJournal = $this->returnJournal->getItem($model->return_journal_id);
            $returnJournal->status = $returnJournal::STATUS['start_manager']['value'];
            $returnJournal->saveItem();
            $model->saveItem();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \DomainException($e->getMessage());
        }

    }
}