<?php
namespace returnModule\core\services;

use yii\base\BaseObject;

class DtoJournal extends BaseObject
{
    /** @var array */
    public $data;
    /** @var string */
    public $title;
    /** @var bool */
    public $processed;
}