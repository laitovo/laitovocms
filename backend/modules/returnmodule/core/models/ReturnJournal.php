<?php

namespace returnModule\core\models;

use backend\modules\logistics\models\Naryad;
use returnModule\ReturnModule;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "return_journal".
 *
 * @property int $id
 * @property string $upn
 * @property string $article
 * @property string $car
 * @property string $product
 * @property int $source
 * @property int $why
 * @property int $status
 * @property int $client_id
 * @property string $client_name_email
 * @property string $url_client
 * @property string $url_order
 * @property string $url_claim
 * @property int $order_number
 * @property bool $order_type
 * @property string $claim
 * @property string $barcode
 * @property int $package_integrity
 * @property int $resolution
 * @property int $comment
 * @property int $location
 * @property int $created_at
 * @property int $updated_at
 * @property int $result_upn_id
 * @property string $date
 * @property int $contractorId
 *
 * @property ReturnLogist $returnLogist
 * @property ReturnSalesDepartment $returnSalesDepartment
 * @property ReturnSupport $returnSupport
 * @property ReturnChief $returnChief
 *
 * @property string $statusName
 * @property string $sourceName
 * @property string $whyName
 * @property string $resolutionName
 * @property array $locationRoles
 */
class ReturnJournal extends BaseModel
{
    /** @var bool */
    public $show_btn = false;

    const SOURCE = [
        'tc' => [
            'name' => 'Транспортная компания',
            'value' => 0
        ],
        'other' => [
            'name' => 'Прочее',
            'value' => 10
        ],
    ];

    const WHY = [
        'claim' => [
            'name' => 'Рекламация',
            'value' => 0
        ],
        'no_claim' => [
            'name' => 'Возврат без рекламации',
            'value' => 10
        ],
        'not_collected' => [
            'name' => 'Не забрано из ТК',
            'value' => 20
        ],
        'other' => [
            'name' => 'Прочее',
            'value' => 30,
        ],
    ];

    const STATUS = [
        'start_logist' => [
            'name' => 'Ожидает обработку логистом',
            'value' => 10,
            'roles' => ['logist'],
        ],
        'start_manager' => [
            'name' => 'Ожидает обработку менеджером',
            'value' => 20,
            'roles' => ['manager'],
        ],
        'start_support' => [
            'name' => 'Ожидает решения техслужбы',
            'value' => 30,
            'roles' => ['support'],
            'location' => 'Техслужба',
        ],
        'finish_storage' => [
            'name' => 'Ожидает конечную обработку кладовщиком',
            'value' => 40,
            'roles' => ['storageUser'],
        ],
        'chief' => [
            'name' => 'Ожидает решения руководителя',
            'value' => 50,
            'roles' => ['chief'],
        ],
        'end' => [
            'name' => 'Обработка завершена',
            'value' => 60,
        ],
    ];

    const RESOLUTION = [
        'no_resolution' => [
            'name' => 'Нет решения',
            'value' => 0
        ],
        'debit' => [
            'name' => 'Оприходовать',
            'value' => 10
        ],
        'sump' => [
            'name' => 'Отстойник',
            'value' => 20
        ],
        'resolution' => [
            'name' => 'Согласовать с руководителем',
            'value' => 30,
        ],
        'write-off' => [
            'name' => 'Списать возврат',
            'value' => 40,
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'return_journal';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => ReturnModule::t('Номер возврата'),
            'article' => ReturnModule::t('Артикул'),
            'created_at' => ReturnModule::t('Дата поступления'),
            'status' => ReturnModule::t('Статус'),
            'resolution' => ReturnModule::t('Решение'),
            'result_upn_id' => ReturnModule::t('Оприходованный UPN'),
            'client_name_email' => ReturnModule::t('Информация о клиенте'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article','upn'], 'required'],
            [
                [
                    'status',
                    'client_id',
                    'order_number',
                    'package_integrity',
                    'source',
                    'why',
                    'resolution',
                    'location',
                    'order_type',
                    'result_upn_id',
                ],
                'integer'
            ],
            [['created_at', 'updated_at', 'date'], 'safe'],
            [['upn', 'article'], 'string', 'max' => 20],
            ['comment', 'string'],
            [
                [
                    'car',
                    'product',
                    'claim',
                    'barcode',
                    'client_name_email',
                    'url_claim',
                    'url_client',
                    'url_order',
                ],
                'string',
                'max' => 255
            ],
        ];
    }

    public static function getLocationRoles()
    {
        $data = [];
        foreach (self::STATUS as $item) {
            if (!empty($item['location'])) {
                $data[$item['value']] = ReturnModule::t($item['location']);
            }
        }
        return $data;
    }

    public function getLocationName()
    {
        if ($this->location === null) {
            return ReturnModule::t('На складе');
        }

        $location = $this->location;
        foreach (self::STATUS as $item) {
            if ($item['value'] == $location && !empty($item['location'])) {
                return ReturnModule::t($item['location']);
            }
        }

        throw new \DomainException('Location not found in' . __METHOD__);
    }

    public function getStatusName()
    {
        return $this->getNameConst(self::STATUS, 'status');
    }

    public function getSourceName()
    {
        return $this->getNameConst(self::SOURCE, 'source');
    }

    public function getWhyName()
    {
        return $this->getNameConst(self::WHY, 'why');
    }

    public function getResolutionName()
    {
        return $this->getNameConst(self::RESOLUTION, 'resolution');
    }

    private function getNameConst($const, $var)
    {
        foreach ($const as $item) {
            if ($item['value'] === $this->$var) {
                return $item['name'];
            }
        }
        throw new \DomainException('Unknown value ' . __METHOD__);
    }

    public static function getRolesStatus(array $roles)
    {
        $data = [];
        foreach (self::STATUS as $item) {
            if (isset($item['roles'])) {
                foreach ($item['roles'] as $role) {
                    if (in_array($role, $roles)) {
                        $data[] = $item['value'];
                    }
                }
            }
        }
        return $data;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnLogist()
    {
        return $this->hasOne(ReturnLogist::class, ['return_journal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnSalesDepartment()
    {
        return $this->hasOne(ReturnSalesDepartment::class, ['return_journal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnSupport()
    {
        return $this->hasOne(ReturnSupport::class, ['return_journal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnChief()
    {
        return $this->hasOne(ReturnChief::class, ['return_journal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultUpn()
    {
        return $this->hasOne(Naryad::class, ['id' => 'result_upn_id']);
    }

}
