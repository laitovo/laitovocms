<?php

namespace returnModule\core\models;

/**
 * This is the model class for table "return_user_role".
 *
 * @property int $return_user_id
 * @property int $return_role_id
 *
 * @property ReturnRole $returnRole
 * @property ReturnUser $returnUser
 */
class ReturnUserRole extends BaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'return_user_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['return_user_id', 'return_role_id'], 'required'],
            [['return_user_id', 'return_role_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'return_user_id' => 'Return User ID',
            'return_role_id' => 'Return Role ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnRole()
    {
        return $this->hasOne(ReturnRole::class, ['id' => 'return_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnUser()
    {
        return $this->hasOne(ReturnUser::class, ['id' => 'return_user_id']);
    }

}
