<?php

namespace returnModule\core\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "return_role".
 *
 * @property int $id
 * @property string $role
 * @property string $name
 *
 * @property ReturnUserRole[] $returnUserRoles
 * @property ReturnUser[] $returnUsers
 */
class ReturnRole extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'return_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'name'], 'required'],
            [['role', 'name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'role' => 'Роль',
            'name' => 'Имя роли',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnUserRoles()
    {
        return $this->hasMany(ReturnUserRole::class, ['return_role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnUsers()
    {
        return $this->hasMany(ReturnUser::class, ['id' => 'return_user_id'])->viaTable('return_user_role', ['return_role_id' => 'id']);
    }
}
