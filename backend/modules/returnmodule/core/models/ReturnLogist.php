<?php

namespace returnModule\core\models;


/**
 * This is the model class for table "return_logist".
 *
 * @property int $id
 * @property int $return_journal_id
 * @property int $paid
 * @property double $paid_sum
 * @property double $no_paid_sum
 * @property double $shipping_amount
 * @property double $return_sum
 * @property double $cash_on_delivery
 * @property double $other_sum
 * @property double $all_sum
 * @property int $created_at
 *
 * @property ReturnJournal $returnJournal
 */
class ReturnLogist extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'return_logist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['return_journal_id'], 'integer'],
            [['paid_sum', 'no_paid_sum', 'shipping_amount', 'return_sum', 'cash_on_delivery', 'other_sum', 'all_sum'], 'number'],
            [['created_at'], 'default', 'value' => time()],
            [['paid'], 'boolean'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnJournal()
    {
        return $this->hasOne(ReturnJournal::class, ['id' => 'return_journal_id']);
    }

}
