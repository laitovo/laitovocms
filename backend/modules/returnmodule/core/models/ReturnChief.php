<?php

namespace returnModule\core\models;


/**
 * This is the model class for table "return_logist".
 *
 * @property int     $id
 * @property int     $return_journal_id
 * @property double  $result_sum
 * @property boolean $resume_material
 * @property boolean $resume_money
 * @property int     $created_at
 *
 * @property ReturnJournal $returnJournal
 */
class ReturnChief extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%return_chief}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['return_journal_id'], 'integer'],
            [['result_sum'], 'number'],
            [['created_at'], 'default', 'value' => time()],
            [['resume_material','resume_money'], 'boolean'],
            [['resume_material','resume_money'], 'required'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnJournal()
    {
        return $this->hasOne(ReturnJournal::class, ['id' => 'return_journal_id']);
    }

}
