<?php

namespace returnModule\core\models;

/**
 * This is the model class for table "return_user".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 *
 * @property ReturnUserRole[] $returnUserRoles
 * @property ReturnRole[] $returnRoles
 */
class ReturnUser extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'return_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnUserRoles()
    {
        return $this->hasMany(ReturnUserRole::class, ['return_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnRoles()
    {
        return $this->hasMany(ReturnRole::class, ['id' => 'return_role_id'])->viaTable('return_user_role', ['return_user_id' => 'id']);
    }

}
