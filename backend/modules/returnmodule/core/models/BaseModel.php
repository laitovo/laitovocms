<?php
namespace returnModule\core\models;

use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    public function getItem(int $id)
    {
        if (!$object = static::findOne($id)) {
            throw new \DomainException($this->strClass() . '. Not found!');
        }
        return $object;
    }

    public function saveItem()
    {
        if (!static::save()) {
            throw new \DomainException($this->strClass() . '. Saving is error: ' . json_encode($this->errors, JSON_UNESCAPED_UNICODE));
        }
    }

    public function deleteItem($id)
    {
        $model = $this->getItem($id);
        if (!$model->delete()) {
            throw new \DomainException($this->strClass() . ' .Delete is error!');
        }
    }

    protected function strClass()
    {
        return 'Class: ' . get_class($this);
    }
}