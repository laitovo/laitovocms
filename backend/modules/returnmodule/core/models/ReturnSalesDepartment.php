<?php

namespace returnModule\core\models;


/**
 * This is the model class for table "return_sales_department".
 *
 * @property int $id
 * @property int $return_journal_id
 * @property bool $resume
 * @property double $result_sum
 * @property int $created_at
 *
 * @property ReturnJournal $returnJournal
 */
class ReturnSalesDepartment extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'return_sales_department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['return_journal_id',], 'integer'],
            [['return_journal_id',], 'unique'],
            [['result_sum',], 'double'],
            ['resume', 'boolean'],
            [['created_at'], 'default', 'value' => time()],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnJournal()
    {
        return $this->hasOne(ReturnJournal::class, ['id' => 'return_journal_id']);
    }

}
