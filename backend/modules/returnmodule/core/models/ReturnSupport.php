<?php

namespace returnModule\core\models;


/**
 * This is the model class for table "return_support".
 *
 * @property int $id
 * @property int $return_journal_id
 * @property int $resume
 * @property string $created_at
 *
 * @property ReturnJournal $returnJournal
 */
class ReturnSupport extends BaseModel
{
    public static function tableName()
    {
        return 'return_support';
    }

    public function rules()
    {
        return [
            [['return_journal_id',], 'integer'],
            [['resume'], 'boolean'],
            [['resume'], 'required'],
            [['created_at'], 'default', 'value' => time()],
        ];
    }

    public function getReturnJournal()
    {
        return $this->hasOne(ReturnJournal::class, ['id' => 'return_journal_id']);
    }

}
