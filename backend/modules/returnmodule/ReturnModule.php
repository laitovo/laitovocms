<?php

namespace returnModule;

use Yii;

class ReturnModule extends \yii\base\Module
{
//    public $layout = 'main_return';
    public $defaultRoute = '/journal/index';

    /** {@inheritdoc} */
    public function init()
    {
        \Yii::$app->layout='2columns';
        parent::init();
    }

    public static function t($message, $params = [])
    {
//        if (Yii::$app->returnConfig->use_ale10257_translate) {
//            return \ale10257\translate\models\TService::t($message, $params);
//        }
        $placeholders = [];
        if ($params) {
            foreach ($params as $name => $value) {
                $placeholders['{' . $name . '}'] = $value;
            }
        }
        return !$placeholders ? $message : strtr($message, $placeholders);
    }
}
