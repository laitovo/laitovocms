<?php

namespace returnModule\assets;

use yii\web\AssetBundle;

class ReturnAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/';
    public $js = ['js/return.js'];
    public $css = ['css/return.css'];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}