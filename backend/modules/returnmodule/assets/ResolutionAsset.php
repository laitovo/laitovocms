<?php
namespace returnModule\assets;

use yii\web\AssetBundle;

class ResolutionAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/';
    public $js = [
        'js/resolution.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}