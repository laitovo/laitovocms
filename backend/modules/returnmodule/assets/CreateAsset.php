<?php
namespace returnModule\assets;

use yii\web\AssetBundle;

class CreateAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/';
    public $js = [
        'js/jquery-code-scanner.js',
        'js/create_return.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init() {
        parent::init();

        if (YII_DEBUG && !\Yii::$app->request->isPjax) {
            $this->publishOptions['forceCopy'] = true;
        }
    }
}