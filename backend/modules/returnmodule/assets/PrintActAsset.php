<?php
namespace returnModule\assets;

use yii\web\AssetBundle;

class PrintActAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/';
    public $js = [
        'js/print.min.js',
        'js/print_act.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}