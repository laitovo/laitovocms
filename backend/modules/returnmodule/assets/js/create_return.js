$(function () {
    let order = $('#return-order-id');
    let claim = $('#return-claim-id');
    let search = $('#search-result');
    let userSearchResult = $('#user-search-result');
    let returnBarcode = $('#return_barcode');
    let returnUpn = $('#return_upn');
    let articleReturn = $('#article-return');
    let orderType = $('#order_type');
    let article;
    let upn;

    $('.search-data').keypress(function (e) {
        if (e.which === 13) {
            e.preventDefault();
            $(this).next().click();
        }
    });

    function clear () {
        articleReturn.val('');
        order.val('');
        claim.val('');
        $('#search-user').val('');
        $('#user_id').val('');
        $('#url_client').val('');
        $('#url_claim').val('');
        $('#url_order').val('');
        returnUpn.val('');
        orderType.val('');
    }
    $('.get-data-return, .get-data-user').click(function (e) {
        let target = $(this).data('target');
        let int = $(this).data('int');
        let key = $(this).data('key');
        let $this = $(this);
        e.preventDefault();
        if (int !== undefined) {
            if (!$.isNumeric($(target).val())) {
                alert(int);
                return false;
            }
        }
        let obj = {
            key: key,
            value: $(target).val(),
            upn : upn,
            article: article
        };
        clear();
        userSearchResult.hide();
        $.post($(this).attr('href'), obj, function (data) {
            if ($this.hasClass('get-data-return')) {
                if (data.order_id) {
                    order.val(data.order_id);
                    orderType.val(data.order_type);
                    if (upn) {
                        returnUpn.val(upn);
                    }
                    if (article) {
                        articleReturn.val(article)
                    }
                } else {
                    upn = undefined;
                    article = undefined;
                    returnBarcode.val('');
                }
                if (data.claim_id) {
                    claim.val(data.claim_id);
                }
                if (data.user_name) {
                    $('#search-user').val(data.user_name);
                }

                if (data.url_order) {
                    $('#url_order').val(data.url_order);
                }

                if (data.url_client) {
                    $('#url_client').val(data.url_client);
                }

                if (data.url_claim) {
                    $('#url_claim').val(data.url_claim);
                }

                if (data.user_id) {
                    $('#user_id').val(data.user_id);
                }

                if (data.contractorId) {
                    $('#contractorId').val(data.contractorId);
                }
                search.html(data.html);
            }

            if ($this.hasClass('get-data-user')) {
                userSearchResult.html(data.html).show(500);
            }
        });
    });
    returnBarcode.on('change',function () {
        var element = $(this);
        let code = element.val();
        let obj = {
            barcode: code
        };
        $.post(element.data('url'), obj, function (data) {
            let target = element.data('target');
            let action = element.data('action');
            if (data.article === undefined || data.order_number === undefined || data.upn === undefined) {
                alert('Error! Data not found!');
                return false;
                }
                article = data.article;
            upn = data.upn;
            $(target).val(data.order_number);
            $(action).click();
            });
    });
});
