$(function () {
    let form = $('#resolution-storage');
    let url = $('#resolution-id').data('resolution');
    let resolution = $('#resolution-block');
    let resolutionBtn = $('#resolution-btn');
    $('#resolution-storage input[type="radio"]').change(function () {
        $.post(url, {package_integrity: $(this).val()}, function (data) {
            if (data.resolution) {
                resolution.html(data.resolution);
            }
        });
    });
});