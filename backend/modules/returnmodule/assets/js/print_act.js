$(function () {
    $(document).on('click', '#print-return', function (e) {
        e.preventDefault();
        printJS({
            printable: 'print-act',
            type: 'html',
            scanStyles : true,
        });
    });
});