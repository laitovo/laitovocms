<?php

namespace returnModule\widgets;

use returnModule\core\models\ReturnJournal;
use returnModule\core\services\UserService;
use yii\base\Widget;
use returnModule\ReturnModule;
use yii\helpers\Url;

class ReturnMenu extends Widget
{
    /** @var UserService */
    private $userService;

    public function __construct(UserService $userService, array $config = [])
    {
        parent::__construct($config);
        $this->userService = $userService;
    }

    public function run()
    {
        $role = $this->userService->checkRole();
        $admin = in_array('admin', $role);
        $moduleId = \Yii::$app->returnConfig->moduleId;
        $chief = ReturnJournal::RESOLUTION['resolution']['value'];
        $no_resolution = ReturnJournal::RESOLUTION['no_resolution']['value'];

        $write_off = ReturnJournal::RESOLUTION['write-off']['value'];
        $write_off_status = ReturnJournal::STATUS['end']['value'];
        $where_write_off = "`resolution` = $write_off and `status` = $write_off_status";

        $whereStorage = "`resolution` = $no_resolution OR `resolution` = $chief";
        if (in_array('support', $role)) { //
            $data[] =
                [
                    'url' => Url::to("/$moduleId/monitor-support/index"),
                    'text' => ReturnModule::t('Монитор техслужбы'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['start_support']['value']])->count(),
                ];
            $data[] = [
                'url' => Url::to("/$moduleId/journal/index"),
                'text' => ReturnModule::t('Журнал возвратов'),
                'count' => (int)ReturnJournal::find()->count(),
            ];
        }elseif (in_array('storageUser', $role)) {
            $data[] =
                [
                    'url' => Url::to("/$moduleId/default/index"),
                    'text' => ReturnModule::t('Склад возвратов'),
                    'count' => (int)ReturnJournal::find()->where($whereStorage)->count(),
                ];
            $data[] =
                [
                    'url' => Url::to("/$moduleId/default/index-write-off"),
                    'text' => ReturnModule::t('Склад списаний'),
                    'count' => (int)ReturnJournal::find()->where($where_write_off)->count(),
                ];
            $data[] =
                [
                    'url' => Url::to("/$moduleId/monitor-storage/index"),
                    'text' => ReturnModule::t('Монитор кладовщика'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['finish_storage']['value']])->count(),
                ];
            $data[] = [
                'url' => Url::to("/$moduleId/journal/index"),
                'text' => ReturnModule::t('Журнал возвратов'),
                'count' => (int)ReturnJournal::find()->count(),
            ];
        }elseif (in_array('manager', $role)) {
            $data[] =
                [
                    'url' => Url::to("/$moduleId/monitor-manager/index"),
                    'text' => ReturnModule::t('Монитор отдела продаж'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['start_manager']['value']])->count(),
                ];
            $data[] = [
                'url' => Url::to("/$moduleId/journal/index"),
                'text' => ReturnModule::t('Журнал возвратов'),
                'count' => (int)ReturnJournal::find()->count(),
            ];
        }elseif (in_array('logist', $role)) {
            $data[] =
                [
                    'url' => Url::to("/$moduleId/default/index"),
                    'text' => ReturnModule::t('Склад возвратов'),
                    'count' => (int)ReturnJournal::find()->where($whereStorage)->count(),
                ];
            $data[] =
                [
                    'url' => Url::to("/$moduleId/default/index-write-off"),
                    'text' => ReturnModule::t('Склад списаний'),
                    'count' => (int)ReturnJournal::find()->where($where_write_off)->count(),
                ];
            $data[] =
                [
                    'url' => Url::to("/$moduleId/monitor-logist/index"),
                    'text' => ReturnModule::t('Монитор логиста'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['start_logist']['value']])->count(),
                ];
        }elseif (in_array('chief', $role)) {
            $data[] =
                [
                    'url' => Url::to("/$moduleId/chief/index"),
                    'text' => ReturnModule::t('Монитор руководителя'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['chief']['value']])->count(),
                ];
            $data[] = [
                'url' => Url::to("/$moduleId/journal/index"),
                'text' => ReturnModule::t('Журнал возвратов'),
                'count' => (int)ReturnJournal::find()->count(),
            ];
//            $data[] =
//                [
//                    'url' => Url::to("/$moduleId/default/index"),
//                    'text' => ReturnModule::t('Склад возвратов'),
//                    'count' => (int)ReturnJournal::find()->where($whereStorage)->count(),
//                ];
//            $data[] =
//                [
//                    'url' => Url::to("/$moduleId/monitor-storage/index"),
//                    'text' => ReturnModule::t('Монитор кладовщика'),
//                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['finish_storage']['value']])->count(),
//                ];
//            $data[] =
//                [
//                    'url' => Url::to("/$moduleId/monitor-manager/index"),
//                    'text' => ReturnModule::t('Монитор отдела продаж'),
//                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['start_manager']['value']])->count(),
//                ];
//            $data[] =
//                [
//                    'url' => Url::to("/$moduleId/monitor-support/index"),
//                    'text' => ReturnModule::t('Монитор техслужбы'),
//                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['start_support']['value']])->count(),
//                ];
        } elseif ($admin) {
            $data = [
                [
                    'url' => Url::to("/$moduleId/default/index"),
                    'text' => ReturnModule::t('Склад возвратов'),
                    'count' => (int)ReturnJournal::find()->where($whereStorage)->count(),
                ],
                [
                    'url' => Url::to("/$moduleId/default/index-write-off"),
                    'text' => ReturnModule::t('Склад списаний'),
                    'count' => (int)ReturnJournal::find()->where($where_write_off)->count(),
                ],
                [
                    'url' => Url::to("/$moduleId/journal/index"),
                    'text' => ReturnModule::t('Журнал возвратов'),
                    'count' => (int)ReturnJournal::find()->count(),
                ],
                [
                    'url' => Url::to("/$moduleId/chief/index"),
                    'text' => ReturnModule::t('Акты для руководителя'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['chief']['value']])->count(),
                ],
                [
                    'url' => Url::to("/$moduleId/monitor-storage/index"),
                    'text' => ReturnModule::t('Монитор кладовщика'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['finish_storage']['value']])->count(),
                ],
                //[
                //    'url' => Url::to("/$moduleId/monitor-logist/index"),
                //    'text' => ReturnModule::t('Монитор логиста'),
                //    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['start_logist']['value']])->count(),
                //],
                [
                    'url' => Url::to("/$moduleId/monitor-manager/index"),
                    'text' => ReturnModule::t('Монитор отдела продаж'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['start_manager']['value']])->count(),
                ],
                [
                    'url' => Url::to("/$moduleId/monitor-support/index"),
                    'text' => ReturnModule::t('Монитор техслужбы'),
                    'count' => (int)ReturnJournal::find()->where(['status' => ReturnJournal::STATUS['start_support']['value']])->count(),
                ],
            ];
        }

        return $this->render('menu2', ['data' => $data,'admin' => $admin, 'moduleId' => $moduleId]);
    }
}

