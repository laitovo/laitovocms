<?php
/**
 * @var \yii\web\View $this
 * @var string $moduleId
 * @var bool $admin
 * @var array $data
 */
use yii\helpers\Url;
use returnModule\ReturnModule;
?>
<ul class="sidebar-menu tree" data-widget="tree">
    <li class="treeview menu-open">
        <a href="#"><i class="fa fa-bars"></i><span><?= ReturnModule::t('Модуль возврата') ?></span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <? foreach($data as $item) : ?>
                <li>
                    <a href="<?= $item['url'] ?>">
                        <i class="fa fa-circle-o"></i>
                        <span><?= $item['text'] ?> (<?= $item['count'] ?>)</span>
                    </a>
                </li>
            <? endforeach ?>
            <?php if($admin) : ?>
                <li>
                    <a title="" href="<?= Url::to("/$moduleId/user") ?>">
                        <i class="fa fa-circle-o"></i>
                        <span><?= ReturnModule::t('Пользователи системы') ?></span>
                    </a>
                </li>
                <li>
                    <a title="" href="<?= Url::to("/$moduleId/role") ?>">
                        <i class="fa fa-circle-o"></i>
                        <span><?= ReturnModule::t('Роли') ?></span>
                    </a>
                </li>
            <?php endif ?>
        </ul>
    </li>
</ul>