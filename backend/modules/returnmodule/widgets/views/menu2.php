<?php
/**
 * @var \yii\web\View $this
 * @var string $moduleId
 * @var bool $admin
 * @var array $data
 */
use yii\helpers\Url;
use returnModule\ReturnModule;

$this->params['menuItems'][0]['label'] = '<span class="lead">Возвраты</span>';

foreach($data as $item) {
    $this->params['menuItems'][] = [
            'label' => $item['text'] . ' (' . $item['count'] . ')',
            'url'   => $item['url'],
    ];
}
if($admin) {
    $this->params['menuItems'][] = [
            'label' => ReturnModule::t('Пользователи системы'),
            'url'   => Url::to("/$moduleId/user")
    ];
    $this->params['menuItems'][] = [
            'label' => ReturnModule::t('Роли'),
            'url'   => Url::to("/$moduleId/role")
    ];
}


