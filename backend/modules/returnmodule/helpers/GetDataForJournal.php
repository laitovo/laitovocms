<?php
namespace returnModule\helpers;

use returnModule\ReturnModule;

class GetDataForJournal
{
    public static function get($model, $arr)
    {
        $data = [];
        foreach ($arr as $key => $item) {
            if ($model->$key === null) {
                $value = ReturnModule::t('Не задано');
            } else{
                $value = $model->$key;
            }
            $data[] = [
                'name' => $item,
                'value' => $value,
                'field' => $key
            ];
        }
        return $data;
    }
}