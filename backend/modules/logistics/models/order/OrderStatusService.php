<?php

namespace backend\modules\logistics\models\order;

use backend\modules\logistics\models\Order;

/**
 * Данный сервис работает со статусами заказа (заявки)
 *
 * Class OrderStatusService
 * @package backend\modules\logistics\models\order
 */
class OrderStatusService
{
    /**
     * Пометка (или отмена пометки) логистам что заказ должен быть взвешен.
     * Данная операция разрешена только если заказ ещё не обработан логистом.
     * Для пометки меняем весовой статус заказа на "Со взвешиванием", а логистический статус заказа на "Ожидает взвешивания" (или "Взвешен", если вес уже вбит).
     * Для отмены пометки меняем весовой статус заказа на "Без взвешивания", а логистический статус заказа на "Не обработан".
     *
     * @param mixed $order Объект "Заказ"
     * @return bool Результат операции
     * @throws \Exception
     */
    public function updateWeightStatus($order)
    {
        $this->_throwIfInvalidType($order);
        if ($order->logistics_status == Order::LOGISTICS_STATUS_HANDLED) {
            return false;
        }
        if ($order->weight_status == Order::WEIGHT_STATUS_WITHOUT_WEIGHING) {
            $order->weight_status = Order::WEIGHT_STATUS_WITH_WEIGHING;
            $order->logistics_status = $order->weight ? Order::LOGISTICS_STATUS_WEIGHED : Order::LOGISTICS_STATUS_WAIT_FOR_WEIGHING;
        } else {
            $order->weight_status = Order::WEIGHT_STATUS_WITHOUT_WEIGHING;
            $order->logistics_status = Order::LOGISTICS_STATUS_NOT_HANDLED;
        }
        $result = $order->save();

        return $result;
    }

    /**
     * Пометка логистам что заказ взвешен.
     * Для данной операции должен быть выставлен весовой статус.
     *
     * @param mixed $order Объект "Заказ"
     * @return bool Результат операции
     */
    public function applyStatusWeighed($order)
    {
        $this->_throwIfInvalidType($order);
        if ($order->logistics_status == Order::LOGISTICS_STATUS_HANDLED) {
            return false;
        }
        if ($order->weight_status != Order::WEIGHT_STATUS_WITH_WEIGHING) {
            return false;
        }
        $order->logistics_status = Order::LOGISTICS_STATUS_WEIGHED;
        $result = $order->save();

        return $result;
    }

    /**
     * Пометка логистам что заказ ожидает взвешивания.
     * Для данной операции должен быть выставлен весовой статус.
     *
     * @param mixed $order Объект "Заказ"
     * @return bool Результат операции
     */
    public function applyStatusWeightForWeighing($order)
    {
        $this->_throwIfInvalidType($order);
        if ($order->logistics_status == Order::LOGISTICS_STATUS_HANDLED) {
            return false;
        }
        if ($order->weight_status != Order::WEIGHT_STATUS_WITH_WEIGHING) {
            return false;
        }
        $order->logistics_status = Order::LOGISTICS_STATUS_WAIT_FOR_WEIGHING;
        $result = $order->save();

        return $result;
    }

    /**
     * Пометка что заказ обработан логистом.
     * Данная операция необратима.
     * По идее после этого логист не может редактировать заказ.
     *
     * @param mixed $order Объект "Заказ"
     * @return bool Результат операции
     */
    public function handle($order)
    {
        $this->_throwIfInvalidType($order);
        if (!$this->_canBeHandled($order)) {
            return false;
        }
        $order->logistics_status = Order::LOGISTICS_STATUS_HANDLED;
        $result = $order->save();

        return $result;
    }

    /**
     * Проверка, обработан ли заказ логистом
     *
     * @param mixed $order Объект "Заказ"
     * @return bool Результат операции
     */
    public function isHandled($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->logistics_status == Order::LOGISTICS_STATUS_HANDLED;
    }

    /**
     * Приоритет логистического статуса.
     * Может использоваться при сортировке списка заказов.
     *
     * @param string|int $logisticsStatus
     * @return int
     */
    public function getLogisticsStatusPriority($logisticsStatus) {
        if (!array_key_exists($logisticsStatus, Order::$logisticsStatusPriorities)) {
            return 0;
        }

        return Order::$logisticsStatusPriorities[$logisticsStatus] ?? 0;
    }

    /**
     * Проверка что заказ может быть обработан
     *
     * @param $order
     * @return bool
     */
    private function _canBeHandled($order)
    {
        if (!$order->shipment_date) {
            return false;
        }
        if (!$order->track_code) {
            return false;
        }
        if ($order->weight_status == Order::WEIGHT_STATUS_WITH_WEIGHING && !$order->weight) {
            return false;
        }

        return true;
    }

    /**
     * Проверка на тип переданного заказа
     *
     * @param $order
     * @throws \Exception
     */
    private function _throwIfInvalidType($order) {
        if (!($order instanceof Order)) {
            throw new \Exception('В сервис для работы со статусами передан неверный класс заказа');
        }
    }
}