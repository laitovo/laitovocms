<?php

namespace backend\modules\logistics\models\order;

use backend\modules\logistics\models\Order;

/**
 * Репозиторий объектов "Заказ". Отвечает за работу с БД.
 *
 * Class OrderRepository
 * @package backend\modules\logistics\models\order
 */
class OrderRepository
{
    /**
     * Функция возвращает заказ по переданному идентификатору
     *
     * @param $orderId
     * @return Order|null
     */
    public function getOrderById($orderId) {
        return Order::findOne($orderId);
    }
}