<?php

namespace backend\modules\logistics\models\order;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;

/**
 * Данный сервис предоставляет функционал для работы с ценами заказа (заявки)
 *
 * Class OrderPriceService
 * @package backend\modules\logistics\models\order
 */
class OrderPriceService
{
    /**
     * Рассчет суммы заказа
     * При расчете суммируются цены отдельных позиций данного заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return int Сумма заказа
     */
    public function getItemsPrice($order)
    {
        $this->_throwIfInvalidType($order);
        $orderItems = OrderEntry::find()->where(['and',
            ['order_id' => $order->id],
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ]
        ])->all();
        $totalPrice = 0;
        foreach ($orderItems as $orderItem) {
            $totalPrice += $orderItem->price;
        }

        return $totalPrice;
    }

    /**
     * Общая сумма, полученная за весь заказ наложенным платежом.
     * Наложенный платеж - это платеж, который совершается после получения товара, а не до него.
     *
     * @param $order
     * @return double
     */
    public function getCodTotalPrice($order)
    {
        $this->_throwIfInvalidType($order);

        return $this->getCodItemsPrice($order) + $this->getCodDeliveryPrice($order);
    }

    /**
     * Общая сумма, полученная за товар наложенным платежом.
     *
     * @param $order
     * @return double
     */
    public function getCodItemsPrice($order)
    {
        $this->_throwIfInvalidType($order);
        if ($order->payment_status == Order::PAYMENT_STATUS_COD) {
            return $this->getItemsPrice($order);
        }

        return 0;
    }

    /**
     * Общая сумма, полученная за доставку наложенным платежом.
     *
     * @param $order
     * @return double
     */
    public function getCodDeliveryPrice($order)
    {
        $this->_throwIfInvalidType($order);
        if ($order->delivery_status != Order::DELIVERY_STATUS_COD) {
            return 0;
        }
        if (empty($order->delivery_price)) {
            return 0;
        }

        return $order->delivery_price;
    }

    /**
     * Проверка на тип переданного заказа
     *
     * @param $order
     * @throws \Exception
     */
    private function _throwIfInvalidType($order) {
        if (!($order instanceof Order)) {
            throw new \Exception('В сервис для работы с ценами передан неверный класс заказа');
        }
    }
}