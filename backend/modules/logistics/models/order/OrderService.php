<?php

namespace backend\modules\logistics\models\order;

use backend\modules\logistics\models\Order;

/**
 * Данный сервис предоставляет доступ к свойствам заказа (заявки)
 *
 * Class OrderService
 * @package backend\modules\logistics\models\order
 */
class OrderService
{
    /**
     * Идентификатор заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return int|null
     */
    public function getId($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->id ?? null;
    }

    /**
     * Внутренний номер источника заказа (Идентификатор заявки внутри другого сайта).
     * Приложение работает со множеством источников (laitovo.ru, laitovo.de, ...), которые присылают заявки на обработку заказа.
     * Данное свойство позволяет определить на каком источнике создана заявка.
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getSourceInternalNumber($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->source_innumber ?? null;
    }

    /**
     * Логистический статус заказа (Обработан, Не обработан, Ожидается взвешивание, ...)
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getLogisticsStatus($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->logistics_status ?? null;
    }

    /**
     * Заголовок (читабельное значение) логистического статуса
     *
     * @return string|null
     */
    public function getLogisticsStatusTitle($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->logisticsStatuses[$order->logistics_status] ?? null;
    }

    /**
     * Наименование транспортной компании
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getTransportCompanyName($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->delivery ?? null;
    }

    /**
     * Весовой статус (Со взвешиванием / Без взвешивания)
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getWeightStatusTitle($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->weightStatuses[$order->weight_status] ?? null;
    }

    /**
     * Назначено ли взвешивание данному заказу
     *
     * @param Order $order Объект "Заказ"
     * @return bool
     */
    public function hasWeighing($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->weight_status ?? false;
    }

    /**
     * ФИО клиента
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getClientName($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->username ?? null;
    }

    /**
     * Телефон клиента
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getClientPhone($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->userphone ?? null;
    }

    /**
     * Адрес клиента
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getClientAddress($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->address ?? null;
    }

    /**
     * Статус оплаты (не оплачено, оплачено, наложка)
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getPaymentStatusTitle($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->paymentStatuses[$order->payment_status] ?? null;
    }

    /**
     * Тип доставки (платная, бесплатная)
     *
     * @param Order $order Объект "Заказ"
     * @return null
     */
    public function getDeliveryTypeTitle($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->deliveryTypes[$order->delivery_type] ?? null;
    }

    /**
     * Сумма доставки
     *
     * @param Order $order Объект "Заказ"
     * @return double
     */
    public function getDeliveryPrice($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->delivery_price ?? null;
    }

    /**
     * Статус доставки
     *
     * @param Order $order Объект "Заказ"
     * @return mixed
     */
    public function getDeliveryStatusTitle($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->deliveryStatuses[$order->delivery_status] ?? null;
    }

    /**
     * Комментарий из заказа
     *
     * @param Order $order Объект "Заказ"
     * @return mixed
     */
    public function getComment($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->comment ?? null;
    }

    /**
     * Дата отгрузки
     *
     * @param Order $order Объект "Заказ"
     * @return mixed
     */
    public function getShipmentDate($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->shipment_date ?? null;
    }

    /**
     * Сохранение даты отгрузки
     *
     * @param Order $order Объект "Заказ"
     * @param int|null $shipment_date
     * @return bool
     */
    public function setShipmentDate($order, $shipment_date)
    {
        $this->_throwIfInvalidType($order);
        $order->shipment_date = $shipment_date;
        $result = $order->save();

        return $result;
    }

    /**
     * Трэк-код
     *
     * @param Order $order Объект "Заказ"
     * @return mixed
     */
    public function getTrackCode($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->track_code ?? null;
    }

    /**
     * Сохранение трэк-кода
     *
     * @param Order $order Объект "Заказ"
     * @param string|null $track_code
     * @return bool
     */
    public function setTrackCode($order, $track_code)
    {
        $this->_throwIfInvalidType($order);
        $order->track_code = $track_code;
        $result = $order->save();

        return $result;
    }

    /**
     * Вес
     *
     * @param Order $order Объект "Заказ"
     * @return mixed
     */
    public function getWeight($order)
    {
        $this->_throwIfInvalidType($order);

        return $order->weight ?? null;
    }

    /**
     * Сохранение веса
     *
     * @param Order $order Объект "Заказ"
     * @param string|null $weight
     * @return mixed
     */
    public function setWeight($order, $weight)
    {
        $this->_throwIfInvalidType($order);
        $order->weight = $weight;
        $result = $order->save();

        return $result;
    }

    /**
     * Проверка на тип переданного заказа
     *
     * @param mixed $order Объект "Заказ"
     * @throws \Exception
     */
    private function _throwIfInvalidType($order) {
        if (!($order instanceof Order)) {
            throw new \Exception('В сервис для работы со свойствами передан пустой заказ');
        }
    }
}