<?php

namespace backend\modules\logistics\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\logistics\models\Sources;

/**
 * SourcesSearch represents the model behind the search form about `backend\modules\logistics\models\Sources`.
 */
class SourcesSearch extends Sources
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'team_id', 'created_at', 'author_id', 'updated_at', 'updater_id'], 'integer'],
            [['title', 'key','type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sources::find()->where(['team_id' => Yii::$app->team->getId()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith('author');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // 
            return $dataProvider;
        }

        //Изменение сортировки
        $sort = $dataProvider->getSort();
        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'author.name' => [
                'asc' => ['user.name' => SORT_ASC],
                'desc' => ['user.name' => SORT_DESC],
                'label' => 'Работник'
            ],
        ]);

        $dataProvider->setSort($sort);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'team_id' => $this->team_id,
            'created_at' => $this->created_at,
            'author_id' => $this->author_id,
            'updated_at' => $this->updated_at,
            'updater_id' => $this->updater_id,
        ]);

        //для поиска по типам как человекопонятный интерфейс
        $query = $this->addTypeFilter($query);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'key', $this->key]);

        return $dataProvider;
    }


    /**
     * [addTypeFilter Добавляет фильтр по атрибуту type]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-21
     * @anotherdate 2017-10-21T10:27:23+0300
     * @return      [Class ActiveQuery]                   [Возвращает объект запроса с накинутым на него фильтром]
     */
    public function addTypeFilter($query)
    {
        $types = [];
        if ($this->type) {
            foreach ($this->types as $key => $val) {
               if (mb_stripos($val,$this->type) !== false) {
                  $types[] = $key;
               };
            }
            if (!count($types)) $types[] = null;
        }

        return $query->andFilterWhere(['in','type',$types]);
    }
}
