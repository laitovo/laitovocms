<?php

namespace backend\modules\logistics\models\report;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Класс преставялет собой модель поиска для отчетка по остаткам склада в Biz-zone
 *
 * Class StorageReport
 * @package backend\modules\logistics\models\report
 */
class StorageReport extends Model
{
    public $carName;

    public function attributeLabels()
    {
        return [
            'carName' => \Yii::t('app','Наименование автомобиля')
        ];
    }

    public function rules()
    {
        return [
            ['carName','safe']
        ];
    }

    public function getProvider($params)
    {
         $this->load($params);


//        var_dump($params);
//        var_dump($this->carName);
//        die;
         $storageIds = Storage::find()->select('id')->where(['or',['type'=> Storage::TYPE_REVERSE],['type'=> Storage::TYPE_TIME]])->column();

         $query = (new \yii\db\Query())
            ->select([
                'car.id as `carId`',
                'car.article as `carArticle`',
                'car.name as `carName`',
                'count(nar.article) as count',
                'SUM(CASE WHEN nar.article LIKE \'FW-%\' THEN 1 ELSE 0 END) AS FW',
                'SUM(CASE WHEN nar.article LIKE \'FV-%\' THEN 1 ELSE 0 END) AS FV',
                'SUM(CASE WHEN nar.article LIKE \'FD-%\' THEN 1 ELSE 0 END) AS FD',
                'SUM(CASE WHEN nar.article LIKE \'RD-%\' THEN 1 ELSE 0 END) AS RD',
                'SUM(CASE WHEN nar.article LIKE \'RV-%\' THEN 1 ELSE 0 END) AS RV',
                'SUM(CASE WHEN nar.article LIKE \'BW-%\' THEN 1 ELSE 0 END) AS BW'
            ])
            ->from('laitovo_cars as car')
            ->innerJoin('logistics_naryad as nar','nar.article LIKE concat(\'%-%-\',car.article,\'-%-%\')')
            ->innerJoin('logistics_storage_state as state','nar.id = state.upn_id')
            ->where('state.storage_id in ('.implode(',',$storageIds).')')
            ->andWhere('nar.reserved_id is null');
         if ($this->carName) {
             $query->andWhere(['LIKE','car.name',$this->carName]);
         }
//            ->where('state.storage_id in (:storages)',[':storages' => implode(',',$storageIds)])
        $query->groupBy('car.id');


        $provider =  new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $provider->getSort();

        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'carArticle' => [
                'asc' => ['carArticle' => SORT_ASC],
                'desc' => ['carArticle' => SORT_DESC],
            ],
        ]);

        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'carName' => [
                'asc' => ['carName' => SORT_ASC],
                'desc' => ['carName' => SORT_DESC],
            ],
        ]);

        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'carId' => [
                'asc' => ['carId' => SORT_ASC],
                'desc' => ['carId' => SORT_DESC],
            ],
        ]);

        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'count' => [
                'asc' => ['count' => SORT_ASC],
                'desc' => ['count' => SORT_DESC],
            ],
        ]);

        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'FW' => [
                'asc' => ['FW' => SORT_ASC],
                'desc' => ['FW' => SORT_DESC],
            ],
            'FV' => [
                'asc' => ['FV' => SORT_ASC],
                'desc' => ['FV' => SORT_DESC],
            ],
            'FD' => [
                'asc' => ['FD' => SORT_ASC],
                'desc' => ['FD' => SORT_DESC],
            ],
            'RD' => [
                'asc' => ['RD' => SORT_ASC],
                'desc' => ['RD' => SORT_DESC],
            ],
            'RV' => [
                'asc' => ['RV' => SORT_ASC],
                'desc' => ['RV' => SORT_DESC],
            ],
            'BW' => [
                'asc' => ['BW' => SORT_ASC],
                'desc' => ['BW' => SORT_DESC],
            ],
        ]);

        $sort->defaultOrder = ['carName' => SORT_ASC];

        $provider->setSort($sort);

        return $provider;
    }

    public function getViewProvider($id)
    {
        $storageIds = Storage::find()->select('id')->where(['or',['type'=> Storage::TYPE_REVERSE],['type'=> Storage::TYPE_TIME]])->column();

        $subQuery = (new \yii\db\Query())
            ->select('article,COUNT(article) as count')
            ->from('logistics_order_entry')
            ->where(['>','created_at',time()-60*60*24*30])
            ->andWhere(['or',['is_deleted'=>false],['is_deleted'=>null]])
            ->groupBy(['article']);

        $query = (new \yii\db\Query())
            ->select([
//                'car.id as `carId`',
//                'car.name as `carName`',
                'IFNULL(entries.count,0) as `statistics`',
                'nar.article as `article`',
                'count(nar.article) as count',
//                'SUM(CASE WHEN nar.article LIKE \'FW-%\' THEN 1 ELSE 0 END) AS FW',
//                'SUM(CASE WHEN nar.article LIKE \'FV-%\' THEN 1 ELSE 0 END) AS FV',
//                'SUM(CASE WHEN nar.article LIKE \'FD-%\' THEN 1 ELSE 0 END) AS FD',
//                'SUM(CASE WHEN nar.article LIKE \'RD-%\' THEN 1 ELSE 0 END) AS RD',
//                'SUM(CASE WHEN nar.article LIKE \'RV-%\' THEN 1 ELSE 0 END) AS RV',
//                'SUM(CASE WHEN nar.article LIKE \'BW-%\' THEN 1 ELSE 0 END) AS BW'
            ])
            ->from('laitovo_cars as car')
            ->innerJoin('logistics_naryad as nar','nar.article LIKE concat(\'%-%-\',car.article,\'-%-%\')')
            ->innerJoin('logistics_storage_state as state','nar.id = state.upn_id')
            ->leftJoin(['entries' => $subQuery],'entries.article = nar.article')
            ->where('state.storage_id in ('.implode(',',$storageIds).')')
            ->andWhere('car.id = :id',[':id' => $id])
            ->andWhere('nar.reserved_id is null')

//            ->where('state.storage_id in (:storages)',[':storages' => implode(',',$storageIds)])
            ->groupBy('car.id, nar.article');

        $provider =  new ActiveDataProvider([
            'pagination' => [
                'pageSize' => 20,
            ],
            'query' => $query,
        ]);

        $sort = $provider->getSort();

        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'article' => [
                'asc' => ['article' => SORT_ASC],
                'desc' => ['article' => SORT_DESC],
            ],
        ]);

        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'statistics' => [
                'asc' => ['statistics' => SORT_ASC],
                'desc' => ['statistics' => SORT_DESC],
            ],
        ]);

        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'count' => [
                'asc' => ['count' => SORT_ASC],
                'desc' => ['count' => SORT_DESC],
            ],
        ]);

        $provider->setSort($sort);

        return $provider;
    }

    public function getArticleProvider($article)
    {
        $storageIds = Storage::find()->select('id')->where(['or',['type'=> Storage::TYPE_REVERSE],['type'=> Storage::TYPE_TIME]])->column();
        $articles[] = $article;

        //query к складу с upn и артикулами

        $query = StorageState::find()->alias('t')
            ->joinWith('upn')
            ->leftJoin('laitovo_erp_naryad as workorder','logistics_naryad.id = workorder.logist_id')
            ->where(['in','t.storage_id',$storageIds])
            ->andwhere(['in','logistics_naryad.article',$articles])
            ->andwhere(['logistics_naryad.reserved_id' => null]);

//        echo '<pre>';
//        var_dump($query->all());
//        echo '</pre>';


//        die;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

}