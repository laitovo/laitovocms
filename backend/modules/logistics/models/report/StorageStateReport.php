<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 04.01.18
 * Time: 9:59
 */

namespace backend\modules\logistics\models\report;

use backend\modules\logistics\models\Naryad;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\logistics\models\StorageState;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * StorageLiteralSearch represents the model behind the search form about `backend\modules\logistics\models\StorageLiteral`.
 */
class StorageStateReport extends StorageState
{
    public $count;
    public $upnArticle;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'storage_id', 'upn_id'], 'integer'],
            [['title','literal','upnArticle'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StorageState::find()->joinWith('upn');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'id' => $this->id,
//            'storage_id' => $this->storage_id,
//            'capacity' => $this->capacity,
//            'created_at' => $this->created_at,
//            'author_id' => $this->author_id,
//            'updated_at' => $this->updated_at,
//            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['literal' => $this->literal]);
        $query->andFilterWhere(['upn_id' => $this->upn_id]);
        $query->andFilterWhere(['logistics_naryad.article' => $this->upnArticle]);

        $sort = $dataProvider->getSort();
        $sort->attributes = ArrayHelper::merge($sort->attributes, [
            'upnReserved' => [
                'asc' => ['logistics_naryad.reserved_id' => SORT_ASC],
                'desc' => ['logistics_naryad.reserved_id' => SORT_DESC],
            ],
            'upnArticle' => [
                'asc' => ['logistics_naryad.article' => SORT_ASC],
                'desc' => ['logistics_naryad.article' => SORT_DESC],
            ],
        ]);

        $sort->defaultOrder= ['upnArticle' => SORT_ASC, 'upnReserved' => SORT_DESC];

        $dataProvider->setSort($sort);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchReserved($params)
    {
        $subQuery = Naryad::find()->where('reserved_id is not null')->column();
        $query = StorageState::find()->with('upn')->where(['in','upn_id',$subQuery])->andWhere(['like','literal','%A-%',false]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'id' => $this->id,
//            'storage_id' => $this->storage_id,
//            'capacity' => $this->capacity,
//            'created_at' => $this->created_at,
//            'author_id' => $this->author_id,
//            'updated_at' => $this->updated_at,
//            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['literal' => $this->literal]);
        $query->andFilterWhere(['upn_id' => $this->upn_id]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchNotReserved($params)
    {
        $subQuery = Naryad::find()->where('reserved_id is null')->column();
        $query = StorageState::find()->with('upn')->where(['in','upn_id',$subQuery])->andWhere(['like','literal','%A-%',false]);;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'id' => $this->id,
//            'storage_id' => $this->storage_id,
//            'capacity' => $this->capacity,
//            'created_at' => $this->created_at,
//            'author_id' => $this->author_id,
//            'updated_at' => $this->updated_at,
//            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['literal' => $this->literal]);
        $query->andFilterWhere(['upn_id' => $this->upn_id]);

        return $dataProvider;
    }

    public function searchAsArray($params)
    {
        $query = self::find();
        $query->select(['storage_id','literal','count' => 'COUNT(literal)']);
        $query->groupBy('literal');
        //Костыль для проведения инвентаризации
        if (isset($_GET['inv'])) {
            switch ($_GET['inv']) {
                case 1:
                    $query->orderBy(new Expression("CAST(REPLACE(literal, 'СКЛАД-','') AS UNSIGNED) asc"));
                    break;
                case -1:
                    $query->orderBy(new Expression("CAST(REPLACE(literal, 'СКЛАД-','') AS UNSIGNED) desc"));
                    break;
                default:
                    $query->orderBy(new Expression("CAST(REPLACE(literal, 'СКЛАД-','') AS UNSIGNED) asc"));
            }
        }

        $this->load($params);

        $query->andFilterWhere(['like', 'literal', $this->literal]);
        $query->andFilterWhere(['upn_id' => $this->upn_id]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort(false);

        return $dataProvider;
    }
}