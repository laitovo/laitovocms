<?php

namespace backend\modules\logistics\models\orderDocument;

use backend\modules\logistics\models\Order;
use common\models\LogActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Модель "Документ логистического заказа"
 *
 * Class OrderDocument
 * @package backend\modules\logistics\models\orderDocument
 */
class OrderDocument extends LogActiveRecord
{
    /**
     * Наименование таблицы в базе данных
     *
     * @return string
     */
    public static function tableName()
    {
        return 'logistics_order_document';
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'file_key'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['order_id'], 'integer', 'message' => 'Это поле должно быть целым числом'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id'], 'message' => 'Заказ не найден'],
        ];
    }

    /**
     * Поведения (примеси)
     * Автоматически заполняем поля uploaded_at, author_id
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['uploaded_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                ],
            ],
        ];
    }
}