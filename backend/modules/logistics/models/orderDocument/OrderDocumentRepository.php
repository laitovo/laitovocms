<?php

namespace backend\modules\logistics\models\orderDocument;

use Yii;

/**
 * Репозиторий объектов "Документ заказа". Отвечает за работу с БД.
 *
 * Class OrderDocumentRepository
 * @package backend\modules\logistics\models\orderDocument
 */
class OrderDocumentRepository
{
    /**
     * @var mixed Сервис для загрузки и удаления файлов
     */
    private $_fileManager;
    /**
     * @var OrderDocumentFileValidator Валидатор загружаемых файлов
     */
    private $_fileValidator;

    public function __construct()
    {
        $this->_fileManager = Yii::$app->fileManager;
        $this->_fileValidator = new OrderDocumentFileValidator();
    }

    /**
     * Функция возвращает документ по переданному идентификатору
     *
     * @param int $orderDocumentId Идентификатор документа
     * @return OrderDocument|null
     */
    public function getOrderDocumentById($orderDocumentId) {
        return OrderDocument::findOne($orderDocumentId);
    }

    /**
     * Функция возвращает документы по переданному идентификатору заказа
     *
     * @param int $orderId Идентификатор заказа
     * @return array
     */
    public function getDocumentsByOrderId($orderId)
    {
        return OrderDocument::find()->where(['order_id' => $orderId])->all();
    }

    /**
     * Сохранение документа
     *
     * @param mixed $orderDocument Объект "Документ заказа"
     * @param string|bool $fieldName Поле формы, из которого берём файл
     *
     * @return mixed
     */
    public function save($orderDocument, $fieldName = false)
    {
        $this->_throwIfInvalidType($orderDocument);
        if (!$orderDocument->id) {
            return $this->_insert($orderDocument, $fieldName);
        }

        return $this->_update($orderDocument);
    }

    private function _insert($orderDocument, $fieldName)
    {
        if (!$this->_fileValidator->validateFile($fieldName)) {
            return false;
        }
        if (!($orderDocument->file_key = $this->_fileManager->uploadTeamFile($fieldName, 'logist/documents'))) {
            return false;
        }

        return $orderDocument->save();
    }

    private function _update($orderDocument)
    {
        return $orderDocument->save();
    }

    public function delete($orderDocument)
    {
        $this->_throwIfInvalidType($orderDocument);
        if (!$this->_fileManager->deleteFile($orderDocument->file_key)) {
            return false;
        }

        return $orderDocument->delete();
    }

    /**
     * Проверка на тип переданного объекта
     *
     * @param mixed $orderDocument Объект "Документ заказа"
     * @throws \Exception
     */
    private function _throwIfInvalidType($orderDocument) {
        if (!($orderDocument instanceof OrderDocument)) {
            throw new \Exception('В репозиторий передан неверный тип документа');
        }
    }
}