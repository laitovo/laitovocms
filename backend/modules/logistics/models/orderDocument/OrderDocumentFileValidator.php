<?php

namespace backend\modules\logistics\models\orderDocument;

use common\models\LogActiveRecord;
use yii\web\UploadedFile;

/**
 * Валидатор загружаемых файлов.
 *
 * Class OrderDocumentFileValidator
 * @package backend\modules\logistics\models\orderDocument
 */
class OrderDocumentFileValidator extends LogActiveRecord
{
    public $uploadedFile;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['uploadedFile'], 'file', 'extensions' => 'pdf'],
        ];
    }

    /**
     * Основная функция валидатора.
     * Достаём файл из формы, делаем его своим атрибутом, валидируем средствами ActiveRecord.
     *
     * @param $fieldName
     * @return bool
     */
    public function validateFile($fieldName)
    {
        $this->uploadedFile = UploadedFile::getInstanceByName($fieldName);

        return $this->validate();
    }
}