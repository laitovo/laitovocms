<?php

namespace backend\modules\logistics\models\orderDocument;

use Yii;

/**
 * Данный сервис предоставляет доступ к свойствам документов заказа
 *
 * Class OrderEntryService
 * @package backend\modules\logistics\models\orderDocument
 */
class OrderDocumentService
{
    /**
     * @var mixed Сервис для работы с файлами
     */
    private $_fileManager;

    /**
     * В конструкторе инициализируем необходимые сервисы приложения
     *
     * OrderService constructor.
     */
    public function __construct()
    {
        $this->_fileManager = Yii::$app->fileManager;
    }

    /**
     * Идентификатор
     *
     * @param OrderDocument $orderDocument Объект "Документ заказа"
     * @return int
     */
    public function getId($orderDocument)
    {
        $this->_throwIfInvalidType($orderDocument);

        return $orderDocument->id;
    }

    /**
     * Ключ для доступа к файлу
     *
     * @param OrderDocument $orderDocument Объект "Документ заказа"
     * @return string
     */
    public function getFileKey($orderDocument)
    {
        $this->_throwIfInvalidType($orderDocument);

        return $orderDocument->file_key;
    }

    /**
     * Ссылка для просмотра файла
     *
     * @param OrderDocument $orderDocument объект "Документ заказа"
     * @return string
     */
    public function getFileUrlForView($orderDocument)
    {
        $this->_throwIfInvalidType($orderDocument);

        return $this->_fileManager->getFileUrlForView($orderDocument->file_key);
    }

    /**
     * Ссылка для скачивания файла
     *
     * @param OrderDocument $orderDocument объект "Документ заказа"
     * @return string
     */
    public function getFileUrlForDownload($orderDocument)
    {
        $this->_throwIfInvalidType($orderDocument);

        return $this->_fileManager->getFileUrlForDownload($orderDocument->file_key);
    }

    /**
     * Размер файла в байтах
     *
     * @param OrderDocument $orderDocument объект "Документ заказа"
     * @return int
     */
    public function getFileSize($orderDocument)
    {
        $this->_throwIfInvalidType($orderDocument);

        return $this->_fileManager->getFileSize($orderDocument->file_key);
    }

    /**
     * Оригинальное имя, с которым загружался файл
     *
     * @param OrderDocument $orderDocument объект "Документ заказа"
     * @return string
     */
    public function getFileOriginalName($orderDocument)
    {
        $this->_throwIfInvalidType($orderDocument);

        return $this->_fileManager->getFileOriginalName($orderDocument->file_key);
    }

    /**
     * Задаем id заказа
     *
     * @param OrderDocument $orderDocument Объект "Документ заказа"
     * @param int $orderId
     */
    public function setOrderId($orderDocument, $orderId) {
        $this->_throwIfInvalidType($orderDocument);

        $orderDocument->order_id = $orderId;
    }

    /**
     * Задаем ключ для доступа к файлу
     *
     * @param OrderDocument $orderDocument Объект "Документ заказа"
     * @param string $fileKey
     */
    public function setFileKey($orderDocument, $fileKey) {
        $this->_throwIfInvalidType($orderDocument);

        $orderDocument->file_key = $fileKey;
    }

    /**
     * Проверка на тип переданного документа заказа
     *
     * @param mixed $orderDocument
     * @throws \Exception
     */
    private function _throwIfInvalidType($orderDocument) {
        if (!($orderDocument instanceof OrderDocument)) {
            throw new \Exception('В сервис для работы со свойствами передан неверный класс документа заказа');
        }
    }
}