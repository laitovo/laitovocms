<?php

namespace backend\modules\logistics\models\orderDocument;

/**
 * Сервис для создания объектов "Документ заказа"
 *
 * Class OrderDocumentFactory
 * @package backend\modules\logistics\models\orderDocument
 */
class OrderDocumentFactory
{
    /**
     * Простое создание документа
     *
     * @return OrderDocument
     */
    public function create()
    {
        return new OrderDocument();
    }
}