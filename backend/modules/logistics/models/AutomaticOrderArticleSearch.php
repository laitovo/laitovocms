<?php

namespace backend\modules\logistics\models;

use common\models\laitovo\CarsAnalog;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\logistics\ArticleStatistics;
use backend\modules\laitovo\models\ErpProductType;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\models\Naryad;
use common\models\laitovo\Cars;
use backend\modules\logistics\models\AutomaticOrder;
use yii\helpers\Json;

/**
 * AutomaticOrderData class
 *
 * Модель отвечает за формирование статистики по артикулу
 * 
 */
class AutomaticOrderArticleSearch
{
    public $article;                 //артикул
    public $title;                 //артикул
    public $carName;                 //артикул
    public $carNameWithAnalogs = [];//артикул
    public $carArticle;                 //артикул
    public $articleWithAnalogs = []; //список артикулов с аналогами
    public $saleLast;                //продано в последний месяц
    public $salePrevious;            //продано в предпослений месяц
    public $saleRate;                //частота реализации
    public $balanceFact;             //Фактические остатки на складах
    public $balancePlan;             //Планируемый остаток на складах
    public $salePlan;                //Планируемая реализация
    //Показатели по группам
    public $increase;                //Послендний не пиковый прирост
    public $groupName;               //Наименование группы
    public $makeRate;                //Коэффициент скорости производства
    public $productId;                //Коэффициент скорости производства

    public $upn;                     //некий UPN. который можно зарезервировать

    private $_product;               //Группа продуктов к которому относим артикул
    private $_fixRate;               //Группа продуктов к которому относим артикул
    private $_minFrequency;          //Группа продуктов к которому относим артикул

    private $_team; //Команда


    public function __construct($article, $team = null)
    {
        $this->_team = $team ?: Yii::$app->team->getId();
        //Получаем настройки автозаказа
        $conf   = AutomaticOrder::receive($this->_team);
        //инициализируем праматеры из настроек
        $this->_fixRate      = $conf->fixRate   ? $conf->fixRate  : 2;
        $this->_minFrequency = $conf->frequency ? $conf->frequency: 3;

        $this->article = $article;
    	$this->carName = $this->_getCarName($this->article);

    	$this->articleWithAnalogs = $this->_getListWithAnalogsArticles($article);
    	//Реализация предыдущего месяца
    	$month1      = $this->_getArticlesByMonth("-1 month",$this->articleWithAnalogs);
    	$month2      = $this->_getArticlesByMonth("-2 month",$this->articleWithAnalogs);
    	$month3      = $this->_getArticlesByMonth("-3 month",$this->articleWithAnalogs);
    	$month4      = $this->_getArticlesByMonth("-4 month",$this->articleWithAnalogs);
    	$month5      = $this->_getArticlesByMonth("-5 month",$this->articleWithAnalogs);
    	$month6      = $this->_getArticlesByMonth("-6 month",$this->articleWithAnalogs);
    	$balanceFact = $this->_getFactBalance($this->articleWithAnalogs);


    	$this->upn = $this->_getUpnForReserv($this->articleWithAnalogs);

    	//Объединяем элементы обоих массивов по ключам
    	$statistics = ArrayHelper::merge($month1,$month2,$month3,$month4,$month5,$month6,$balanceFact);

    	$sumMonth1      = 0;  
    	$sumMonth2      = 0;  
    	$sumMonth3      = 0;  
    	$sumMonth4      = 0;  
    	$sumMonth5      = 0;  
    	$sumMonth6      = 0;  
    	$sumBalanceFact = 0; 

    	foreach ($statistics as $key => $value) {
	    	$month1      = isset($value['-1 month'])     ? $value['-1 month']    : 0;
	    	$month2      = isset($value['-2 month'])     ? $value['-2 month']    : 0;
	    	$month3      = isset($value['-3 month'])     ? $value['-3 month']    : 0;
	    	$month4      = isset($value['-4 month'])     ? $value['-4 month']    : 0;
	    	$month5      = isset($value['-5 month'])     ? $value['-5 month']    : 0;
	    	$month6      = isset($value['-6 month'])     ? $value['-6 month']    : 0;
	    	$balanceFact = isset($value['balanceFact'])  ? $value['balanceFact'] : 0;

	    	$sumMonth1      += $month1;  
	    	$sumMonth2      += $month2;  
	    	$sumMonth3      += $month3;  
	    	$sumMonth4      += $month4;  
	    	$sumMonth5      += $month5;  
	    	$sumMonth6      += $month6;  
	    	$sumBalanceFact += $balanceFact; 
    	}

    	$this->saleLast     = $sumMonth1;
    	$this->salePrevious = $sumMonth2;

    	$saleRate           = 0;
    	if ($sumMonth1 > 0) $saleRate++;  
    	if ($sumMonth2 > 0) $saleRate++;  
    	if ($sumMonth3 > 0) $saleRate++;  
    	if ($sumMonth4 > 0) $saleRate++;  
    	if ($sumMonth5 > 0) $saleRate++;  
    	if ($sumMonth6 > 0) $saleRate++;  
    	$this->saleRate = $saleRate;

    	$this->balanceFact = $sumBalanceFact;

        $this->_product = $this->_getProductGroup($this->article);

        if ($this->_product) {

            $this->productId =  @$this->_product->id;

            $this->title =  @$this->_product->title;

            $this->groupName = $this->_getGroupName($this->_product);

            $this->increase = round($this->saleLast > $this->salePrevious ? $this->_getGroupIncrease($this->_product,2) : $this->_getGroupIncrease($this->_product,1),2);

            $this->makeRate = $this->_getGroupRate($this->_product);

            $this->salePlan = round($this->saleLast > $this->salePrevious ? $this->_getSalePlan($this->salePrevious,$this->increase) : $this->_getSalePlan($this->saleLast,$this->increase),0);

            $this->balancePlan = $this->_getPlanBalance($this->_fixRate,$this->salePlan,$this->makeRate,$this->saleRate,$this->_minFrequency);

        }
    }

    /**
     * [getListWithAnalogsArticles Функция позволет получить список артикулов аналогов для получения общей статистики]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-12-18
     * @anotherdate 2017-12-18T08:35:53+0300
     * @param       [string]                   $article [Артикул]
     * @return      [array]                             [Список артикулов]
     */
    private function _getListWithAnalogsArticles($article)
    {
        $articles = [];
         //3. Разбиваем артикул на составляющие с цеот
        $value = explode('-', $article);
        
        //Артикул, для которого мы ищмем аналоги, по умолчанию попадает в массив возращаемых значений 
        $articles[] = $article;

        //Если артикул начинаеться с "OT" то мы не ищем по нему аналоги и возвращается массив со значем только текущего артикула
        //В противном случае мы приступаем к поиску аналогов.
        if ($value[0] != 'OT') {

            $valueSearch = $value[0];
            $valueSearch = str_replace('FW', 'ПШ', $valueSearch);
            $valueSearch = str_replace('FV', 'ПФ', $valueSearch);
            $valueSearch = str_replace('FD', 'ПБ', $valueSearch);
            $valueSearch = str_replace('RD', 'ЗБ', $valueSearch);
            $valueSearch = str_replace('RV', 'ЗФ', $valueSearch);
            $valueSearch = str_replace('BW', 'ЗШ', $valueSearch);

            //Ищем автомобиль
            $car =  Cars::find()->where(['article' => @$value[2]])->one();
            //Находим аналоги по оконному проему

            $analogsIds = $this->_searchAnalogs($car->id,$valueSearch);
            if ( ($deleteKey = array_search($car->id,$analogsIds)) !== false ) {
                unset($analogsIds[$deleteKey]);
            }

            $analogs = Cars::find()->where(['in','id',$analogsIds])->all();

            //если аналоги существуют
            if ($analogs) {

                //обходим список аналогов
                foreach ($analogs as $analog) {

                    //заменяем данные из текущего артикула на необходимые данные аналоги, а именно
                    //заменяем значение артикула автомобиля, зашитое в артикуле
                    $value[2] = $analog->article;
                    //заменяем значение первой буквы марки автомобиля, защитое в артикуле, по правилам генерации артикула с Laitovo.ru
                    //Использую вспомогательную функцию getTranslit() для перевода русских марок в нужное написание
                    $value[1] = $analog->getTranslit(mb_substr($analog->mark,0,1));
                    //Получаем новый артикул путем соединения всех данных воедино пи помощи функция склеивания осколков
                    $newarticle = implode('-',$value);
                    //Добавляем новый артикул в массив результирующих артикулов
                    $articles[] = $newarticle;

                    $this->carNameWithAnalogs[] = $analog->name;
                }
            }
        }

        return $articles;
    }

    /**
     * [getArticlesByMonth Функция позволяет получить статистику за период]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [version]
     * @date        2017-12-18
     * @anotherdate 2017-12-18T08:47:48+0300
     * @param       [type]                   $monthValue [description]
     * @param       array                    $articles   [description]
     * @return      [type]                               [description]
     */
    private function _getArticlesByMonth($monthValue,$articles=[])
    {
    	$result = [];
        $month  = strtotime($monthValue);
        $date_f  = mktime (  0, 0, 0, date("n",$month),                 1, date("Y",$month));
        $date_to = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
            ['in','article', $articles],
        ]);
        $query->groupBy(['article']);
        $query->select(['article','count' => 'COUNT(id)']);
        $rows = $query->asArray()->all();
        foreach ($rows as $key => $value) {
            $result[$value['article']][$monthValue] = $value['count']; 
        }
        return $result;
    }

    /**
     * [_getFactBalance Функция для получения фактических остатков по артикулу]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [version]
     * @date        2017-12-18
     * @anotherdate 2017-12-18T09:28:11+0300
     * @param       array                    $articles [Список артикулов по которым мы будем искать остатки на складе]
     * @return      array                              [Список артикулов с остатками]
     */
    private function _getFactBalance($articles=[])
    {
    	$result = [];

        $storagesArray = Storage::find()->where(['in','type',[Storage::TYPE_REVERSE,Storage::TYPE_TIME]])->all();
        $idsSt = ArrayHelper::map($storagesArray,'id','id');

        $query = StorageState::find()->alias('t')->where(['in','t.storage_id',$idsSt])->joinWith('upn');
        $query->select(['upn_id']);
        $rows = $query->all();

        $ids = ArrayHelper::map($rows,'upn_id','upn_id');

        $query = Naryad::find();
        $query->where(['in','id',$ids]);
        $query->andWhere(['in','article', $articles]);
        $query->andWhere(['reserved_id' => null]);
        $query->groupBy(['article']);
        $query->select(['article','count' => 'COUNT(article)']);
        $rows = $query->asArray()->all();

        foreach ($rows as $key => $value) {
            $result[$value['article']]['balanceFact'] = $value['count'];
        }
        return $result;
    }

    /**
     * Фукцния должна вернуть upn для выдачи
     * @param array $articles
     * @return array
     */
    private function _getUpnForReserv($articles=[])
    {
        $result = [];

        $storagesArray = Storage::find()->where(['in','type',[Storage::TYPE_REVERSE,Storage::TYPE_TIME]])->all();
        $idsSt = ArrayHelper::map($storagesArray,'id','id');

        $query = StorageState::find()->alias('t')->where(['in','t.storage_id',$idsSt])->joinWith('upn');

//        $query = StorageState::find()->joinWith('upn');
        $query->select(['upn_id']);
        $rows = $query->all();

        $ids = ArrayHelper::map($rows,'upn_id','upn_id');

        $query = Naryad::find();
        $query->where(['in','id',$ids]);
        $query->andWhere(['in','article', $articles]);
        $query->andWhere(['reserved_id'=> null]);
        $query->orderBy(['created_at' => SORT_DESC]);
        $upn = $query->one();

        return $upn;
    }

    /**
     * [_getProductGroup description]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [version]
     * @date        2017-12-19
     * @anotherdate 2017-12-19T09:12:57+0300
     * @param       String                   $article [Артикул]
     * @return      ErpProductType|null               [Объект продукта либо null]
     */
    private  function _getProductGroup($article)
    {
        if (!$article) return null;
        $products= ErpProductType::find()->all();
        foreach ($products as $product) {
            if ($product->rule && preg_match($product->rule, $article)) {
                return $product;
            }
        }
        return null;
    }

    /**
     * [_getGroupName Функция для получения наименования группы]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-12-19
     * @anotherdate 2017-12-19T09:30:26+0300
     * @param       ErpProductType           $product [Экземпляр продукта]
     * @return      string                            [Наименование группы]
     */
    private function _getGroupName(ErpProductType $product)
    {
        return $product->title;
    }

    /**
     * [_getIncrease Функция по получения последнего непикового прироста в зависимости от переданных параметров]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [version]
     * @date        2017-12-19
     * @anotherdate 2017-12-19T09:45:20+0300
     * @param       ErpProductType           $product [description]
     * @param       integer                  $type    [description]
     * @return      double                            [Коэффцициент прироста]
     */
    private function _getGroupIncrease(ErpProductType $product, $type)
    {

        //Получим правило для поиска в базе
        $rule = $product->rule;
        $rule = trim($rule,'#');
        $rule = str_replace('\w', '[[:alnum:]]', $rule);
        $rule = str_replace('\d', '[[:digit:]]', $rule);

        //Определим количество реализаций за -1 year - 2 month
        $month  = strtotime('-14 month');
        $date_f  = mktime (  0, 0, 0, date("n",$month),                 1, date("Y",$month));
        $date_to = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
            ['REGEXP','article', $rule],
        ]);
        $month14 = $query->count();

        //Определим количество реализаций за -1 year - 1 month
        $month  = strtotime('-13 month');
        $date_f  = mktime (  0, 0, 0, date("n",$month),                 1, date("Y",$month));
        $date_to = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
            ['REGEXP','article', $rule],
        ]);
        $month13 = $query->count();

        //Определим количество реализаций за -1 year + 30 month
        $month  = strtotime('-12 month');
        $date_f  = $month;
        $date_to = $month + 60*60*24*30;
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
            ['REGEXP','article', $rule],
        ]);
        $days30 = $query->count();

        //в зависимости от типа непикового прироста отдаем разные отношения
        switch ($type) {
            case 1:
                return $month13 ? ($days30 / $month13)-1 : 0;

            case 2:
                return $month14 ? ($days30 / $month14)-1 : 0;
        }
    }

    private function _getGroupRate(ErpProductType $product)
    {
        //Получим правило для поиска в базе
        $rule = $product->rule;
        $rule = trim($rule,'#');
        $rule = str_replace('\w', '[[:alnum:]]', $rule);
        $rule = str_replace('\d', '[[:digit:]]', $rule);

        $month1  = strtotime('-1 month');
        $month2  = strtotime('-6 month');
        $date_f  = mktime (  0, 0, 0, date("n",$month1),                 1, date("Y",$month1));
        $date_to = mktime ( 23,59,59, date("n",$month2),date('t', $month2), date("Y",$month2));
        $query = ErpNaryad::find();
        $query->where(['and',
            ['status' => ErpNaryad::STATUS_READY],
            ['<=','created_at', $date_f],
            ['>=','created_at', $date_to],
            ['REGEXP','article', $rule],

        ]);
        $query->groupBy(['article']);
        $query->select(['article','rate' => 'AVG((updated_at - created_at))']);
        $rows = $query->asArray()->all();

        $rate = 0; 
        $count = 0;
        foreach ($rows as $key => $value) {
            $rate += $value['rate'];
            $count++;
        }

        return $count ? $rate / $count / (60*60*24*30) : 0;
    }

    private function _getSalePlan($sale,$increase)
    {
        return round($sale + ($sale * $increase));
    }

    private function _getPlanBalance($fixRate,$salePlan,$makeRate,$saleRate,$minFrequency)
    {
        $planB = $saleRate > $minFrequency ? 1 : 0;
        return ceil($salePlan * $makeRate * $fixRate) > 0 ?  ceil($salePlan * $makeRate * $fixRate) : $planB;
    }

    public function getFixRate()
    {
        return $this->_fixRate ? $this->_fixRate : 0;
    }

    private function _getCarName($article)
    {
        $value = explode('-', $article);
        if ($value[0] != 'OT') {
            $car =  Cars::find()->where(['article' => @$value[2]])->one();
            $this->carArticle = $car ? $car->article : null;
                return $car ? $car->name : 'Общее';
        }
    }

    /**
     * @return bool
     */
    public function isNeeded()
    {
        if ($this->saleRate >= 1) {
            return true;
        }
        return false;
    }


    private function _searchAnalogs ($car_id,$type,$ids = []) {
        $ids2 = [];
        $ids2[] = $car_id;
        $analogs = CarsAnalog::find()->where(['car_id' => $car_id])->all();
        foreach ($analogs as $analog) {
            $json=Json::decode($analog->json ? $analog->json : '{}');
            $types = isset($json['elements']) ? $json['elements'] : [];
            if (!in_array($analog->analog_id, $ids2) && in_array($type,$types)) {
                $ids2[] = $analog->analog_id;
            }
        }

        //Находим расхождения в массивах
        $diff = array_diff($ids2, $ids);

        // if ($car_id == 638) {
        //     var_dump('IDS-2');
        //     var_dump($ids2);
        //     var_dump('IDS');
        //     var_dump($ids);
        //     var_dump('DIFF');
        //     var_dump($diff);
        //     var_dump(empty($diff));
        //     die();
        // }

        if (count($diff)) {
            foreach ($diff as $key => $value) {
                $values = $this->_searchAnalogs($value,$type,array_merge($diff, $ids));
                if ($values) {
                    foreach ($values as $value) {
                        if (!in_array($value, $diff)) {
                            $diff[] = $value;
                        }
                    }
                }

            }
        }


        //возвращаем либо массив либо ложь
        return empty($diff) ? false : $diff;
    }
}