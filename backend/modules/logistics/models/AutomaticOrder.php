<?php

namespace backend\modules\logistics\models;

use Yii;
use \common\models\User;
use \backend\modules\laitovo\models\ErpNaryad;

/**
 * This is the model class for table "logistics_automatic_order".
 *
 * @property integer $id
 * @property integer $team_id
 * @property integer $measuringInterval
 * @property integer $unitOfPeriod
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property User $updater
 */
class AutomaticOrder extends \common\models\logistics\AutomaticOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'measuringInterval', 'unitOfPeriod', 'created_at', 'author_id', 'updated_at', 'updater_id','frequency','fixRate'], 'integer'],
            [['team_id'], 'unique'],
            [['fixRate'], 'integer', 'min' => 1,'message' => 'Должен быть больше нуля'],
            [['frequency'], 'integer', 'min' => 1, 'max' => 6, 'message' => 'Выберите значение от 1 до 6 включительно'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }

    /**
     * [receive Функция позволяет получить конкретный модуль автозаказа для конкретной команды или создать новый, если он еще не был создан]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-11-09
     * @anotherdate 2017-11-09T12:52:47+0300
     * @return      [AutomaticOrder]                   [Экземпляр класса настроек автозаказа]
     */
    public static function receive($team)
    {
        $model = self::find()->where(['team_id' => $team])->one();
        return $model ? $model : new AutomaticOrder();
    }

    /**
     * [getIntervalInSeconds Функция возвращает значение интервала в секундах]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-11-09
     * @anotherdate 2017-11-09T13:12:05+0300
     * @return      [integer]                   [интервал измерения в секундах]
     */
    public function getIntervalInSeconds()
    {
        return $this->measuringInterval * $this->unitOfPeriod; 
    }

    /**
     * [getCountRealizationInTheInterval Функция для получения колияества реализаций в интервал измерения]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-11-09
     * @anotherdate 2017-11-09T13:39:59+0300
     * @param       [string]                   $article [Артикул]
     * @return      [type]                            [Количество реализаций в интервал измерения]
     */
    public static function getCountRealizationInTheInterval($article)
    {
        if ($article == null or $article == '') return 0;
        //Получить интервал за который мы смотрим реализацию
        $model = self::receive(Yii::$app->team->getId());
        $interval = $model->getIntervalInSeconds();
        //Получить статистику по артикулу
        $count = ErpNaryad::find()->where(['and',
            ['article' => $article],
            ['<','created_at',time()],
            ['>','created_at',time()-$interval],
        ])->count();

        return $count ? $count : 0;
    }

    //
    public static function getSumPeakCountRealizationInTheInteral($article)
    {
        # code...
    }
}
