<?php

namespace backend\modules\logistics\models;

use \common\models\team\Team;
use \common\models\User;

/**
 * This is the model class for table "logistics_order".
 *
 * @property integer $id
 * @property integer $team_id
 * @property integer $source_id
 * @property string  $responsible_person
 * @property string  $comment
 * @property string  $category
 * @property string  $useremail
 * @property string  $userphone
 * @property string  $username
 * @property string  $address
 * @property string  $delivery
 * @property string  $manager
 * @property string  $source_innumber
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 * @property boolean $is_need_ttn
 * @property boolean $isCOD [ Является заказом, оплачиваемым наложенным плотежом ]
 * @property string $export_type [ Является заказом, оплачиваемым наложенным плотежом ]
 *
 * @property OrderEntry[] $orderEntries
 * @property OrderEntry[] $notDeletedOrderEntries
 *
 * @property User $author
 * @property Team $team
 * @property User $updater
 */
class Order extends \common\models\logistics\Order
{
    #Логистический статус
    const LOGISTICS_STATUS_NOT_HANDLED = 'not_handled';
    const LOGISTICS_STATUS_WAIT_FOR_WEIGHING = 'wait_for_weighing';
    const LOGISTICS_STATUS_WEIGHED = 'weighed';
    const LOGISTICS_STATUS_HANDLED = 'handled';

    #Статус оплаты
    const PAYMENT_STATUS_NOT_PAID = 'not_paid';
    const PAYMENT_STATUS_PAID = 'paid';
    const PAYMENT_STATUS_COD = 'C.O.D.';

    #Статус доставки
    const DELIVERY_STATUS_NOT_PAID = 'not_paid';
    const DELIVERY_STATUS_PAID = 'paid';
    const DELIVERY_STATUS_COD = 'C.O.D.';

    #Тип доставки
    const DELIVERY_TYPE_FREE = 'free';
    const DELIVERY_TYPE_PAID = 'paid';

    #Весовой статус
    const WEIGHT_STATUS_WITH_WEIGHING = true;
    const WEIGHT_STATUS_WITHOUT_WEIGHING = false;

    public static $logisticsStatuses = [
        '' => '',
        self::LOGISTICS_STATUS_NOT_HANDLED => 'Не обработан',
        self::LOGISTICS_STATUS_WAIT_FOR_WEIGHING => 'Ожидается взвешивание',
        self::LOGISTICS_STATUS_WEIGHED => 'Взвешен',
        self::LOGISTICS_STATUS_HANDLED => 'Обработан',
    ];

    public static $logisticsStatusPriorities = [
        self::LOGISTICS_STATUS_NOT_HANDLED => 1,
        self::LOGISTICS_STATUS_WAIT_FOR_WEIGHING => 2,
        self::LOGISTICS_STATUS_WEIGHED => 3,
        self::LOGISTICS_STATUS_HANDLED => 4,
    ];

    public static $paymentStatuses = [
        '' => '',
        self::PAYMENT_STATUS_NOT_PAID => 'Не оплачено',
        self::PAYMENT_STATUS_PAID => 'Оплачено',
        self::PAYMENT_STATUS_COD => 'Наложка',
    ];

    public static $deliveryStatuses = [
        '' => '',
        self::DELIVERY_STATUS_NOT_PAID => 'Не оплачено',
        self::DELIVERY_STATUS_PAID => 'Оплачено',
        self::DELIVERY_STATUS_COD => 'Наложка',
    ];

    public static $deliveryTypes = [
        '' => '',
        self::DELIVERY_TYPE_FREE => 'Бесплатная',
        self::DELIVERY_TYPE_PAID => 'Платная',
    ];

    public static $weightStatuses = [
        '' => '',
        self::WEIGHT_STATUS_WITH_WEIGHING => 'Со взвешиванием',
        self::WEIGHT_STATUS_WITHOUT_WEIGHING => 'Без взвешивания',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'source_id', 'created_at', 'author_id', 'updated_at', 'updater_id', 'shipment_date'], 'integer'],
            [['remoteStorage'], 'integer', 'min' => 0, 'max' => 255, 'message' => 'Неверный диапазон'],
            [['comment', 'track_code','export_username'], 'string'],
            [['paid','bonus','sender_id','receiver_id','payer_id'], 'safe'],
            [['responsible_person','source_innumber'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sources::className(), 'targetAttribute' => ['source_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
            [['weight'], 'number'],
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Sources::className(), ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderEntries()
    {
        return $this->hasMany(OrderEntry::className(), ['order_id' => 'id']);
    }

    public function getLogisticsStatuses() {
        return self::$logisticsStatuses;
    }

    public function getLogisticsStatusPriorities() {
        return self::$logisticsStatusPriorities;
    }

    public function getPaymentStatuses() {
        return self::$paymentStatuses;
    }

    public function getDeliveryStatuses() {
        return self::$deliveryStatuses;
    }

    public function getDeliveryTypes() {
        return self::$deliveryTypes;
    }

    public function getWeightStatuses() {
        return self::$weightStatuses;
    }
}
