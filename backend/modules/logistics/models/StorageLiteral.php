<?php

namespace backend\modules\logistics\models;

use Yii;
use \common\models\User;

/**
 * This is the model class for table "logistics_storage_literal".
 *
 * @property integer $id
 * @property string $title
 * @property integer $storage_id
 * @property integer $capacity
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property LogisticsStorage $storage
 * @property User $updater
 */
class StorageLiteral extends \common\models\logistics\StorageLiteral
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['storage_id', 'capacity', 'created_at', 'author_id', 'updated_at', 'updater_id','default_quantity'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storage::className(), 'targetAttribute' => ['storage_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }
}
