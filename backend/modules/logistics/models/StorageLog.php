<?php

namespace backend\modules\logistics\models;

use Yii;
use \common\models\User;
use \backend\modules\logistics\models\Storage;
use \backend\modules\logistics\models\Order;
use \backend\modules\logistics\models\OrderEntry;
use \backend\modules\logistics\models\Naryad;

/**
 * This is the model class for table "logistics_storage_log".
 *
 * @property integer $id
 * @property integer $storage_id
 * @property string $literal
 * @property integer $upn_id
 * @property integer $reserved
 * @property integer $order_id
 * @property string $action
 * @property string $comment
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property LogisticsOrder $order
 * @property LogisticsStorage $storage
 * @property User $updater
 * @property LogisticsNaryad $upn
 */
class StorageLog extends \common\models\logistics\StorageLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storage_id', 'upn_id', 'reserved', 'reserved_id', 'created_at', 'author_id', 'updated_at', 'updater_id'], 'integer'],
            [['literal', 'action', 'comment'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['reserved_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderEntry::className(), 'targetAttribute' => ['reserved_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storage::className(), 'targetAttribute' => ['storage_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
            [['upn_id'], 'exist', 'skipOnError' => true, 'targetClass' => Naryad::className(), 'targetAttribute' => ['upn_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderEntry()
    {
        return $this->hasOne(OrderEntry::className(), ['id' => 'reserved_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpn()
    {
        return $this->hasOne(Naryad::className(), ['id' => 'upn_id']);
    }

}
