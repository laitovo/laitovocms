<?php

namespace backend\modules\logistics\models;

use common\models\laitovo\CarsAnalog;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\AutomaticOrder;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\models\Naryad;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpProductType;
use common\models\logistics\ArticleStatistics;
use \common\models\laitovo\Cars;
use yii\helpers\Json;

class AutomaticOrderMonitor extends Model
{
    public $storage = [];
    public $array   = [];
    public $group   = [];
    public $proizv  = [];
    public $realiz  = [];
    private $_fixRate;
    private $_minFrequency;

    public function __construct($config = [])
    {
        //Получаем настройки автозаказа
        $some   = AutomaticOrder::receive(Yii::$app->team->getId());
        //инициализируем праматеры из настроек
        $this->_fixRate      = $some->fixRate   ? $some->fixRate  : 2;
        $this->_minFrequency = $some->frequency ? $some->frequency: 3;
        //Дернем фактические остатки
        $query = StorageState::find()->joinWith('upn');
        $query->select(['upn_id']);
        $rows = $query->all();

        $ids = ArrayHelper::map($rows,'upn_id','upn_id');

        $query = Naryad::find();
        $query->where(['in','id',$ids]);
        $query->groupBy(['article']);
        $query->select(['article','count' => 'COUNT(article)']);
        $rows = $query->asArray()->all();

        foreach ($rows as $key => $value) {
            $this->storage[$value['article']] = $value['count'];
        }

        //достаем произведенные артикулы за период
        $month  = strtotime("-6 month");
        $month2  = strtotime("-1 month");

        $firstday = mktime ( 0,0,0, date("n",$month),1, date("Y",$month));
        $lastday = mktime ( 23,59,59, date("n",$month2),date('t', $month2), date("Y",$month2));

        $query = ErpNaryad::find();
        $query->where(['and',
            ['status' => ErpNaryad::STATUS_READY],
            ['<=','created_at', $lastday],
            ['>=','created_at', $firstday],
        ]);
        $query->groupBy(['article']);
        $query->select(['article','rate' => 'AVG((updated_at - created_at))']);
        $rows = $query->asArray()->all();

        foreach ($rows as $key => $value) {
            $this->proizv[$value['article']] = $value['rate']/(60*60*24*30);
        }

        //достаем произведенные артикулы за период
        $month  = time();

        $firstday = mktime ( 0,0,0, date("n",$month),1, date("Y",$month));
        $lastday = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));

        $query = ErpNaryad::find();
        $query->where(['and',
            ['or',
                ['status' => ErpNaryad::STATUS_READY],
                ['status' => ErpNaryad::STATUS_FROM_SKLAD]
            ],
            ['<=','created_at', $lastday],
            ['>=','created_at', $firstday],
        ]);
        $query->groupBy(['article']);
        $query->select(['article','count' => 'COUNT(article)']);
        $rows = $query->asArray()->all();

        foreach ($rows as $key => $value) {
            $this->realiz[$value['article']] = $value['count'];
        }

        //нам надо запросить все ратикулы за минус месяц
        //нам надо запросить все ратикулы за минус 2месяц
        //нам надо запросить все ратикулы за минус 12месяц
        //нам надо запросить все ратикулы за минус 13месяц
        //нам надо запросить все ратикулы за минус 14месяц
        // ПО АРТИКУЛЬНАЯ ВЫБОРКА
        $this->array = self::getArticlesByMonth("-1 month" ,$this->array);
        $this->array = self::getArticlesByMonth("-2 month" ,$this->array);
        $this->array = self::getArticlesByMonth("-3 month" ,$this->array);
        $this->array = self::getArticlesByMonth("-4 month" ,$this->array);
        $this->array = self::getArticlesByMonth("-5 month" ,$this->array);
        $this->array = self::getArticlesByMonth("-6 month" ,$this->array);
        $this->array = self::getArticlesByMonth("-13 month",$this->array);
        $this->array = self::getArticlesByMonth("-14 month",$this->array);
        $this->array = self::getArticlesByDaysUp("-12 month",$this->array);
        $this->array = self::joinWithStorageBalance($this->array,$this->storage);
        $this->array = self::joinWithProizv($this->array,$this->proizv);

        /**
         * Опишем Алгоритм переключения по аналогам и не по аналогам.
         */

        //1. Обходим весь полученный массив $this->array 
        //>>>
        $customArray = [];
        foreach ($this->array as $key => $value1) {
            //2. Массив в который буду вкладывать весь перечень артикулов c аналогоами
            //>>>
            $articles = self::getListWithAnalogsArticles($key);

            $resultStart = 0;
            $sumMonth1  = 0;
            $sumMonth2  = 0;
            $sumMonth3  = 0;
            $sumMonth4  = 0;
            $sumMonth5  = 0;
            $sumMonth6  = 0;
            $sumMonth13 = 0;
            $sumMonth14 = 0;
            $sum30days = 0;
            $sumFact    = 0;
            $sumRate    = 0;
            $ratecount    = 0;
            //3. Начиоаем перебирать этот массив искать эти значения в нашем уже скомпанованом
            //>>>
            foreach ($articles as $value) {
                if (isset($this->array[$value])) {
                    //Считаем сумму реализаций
                    $month1=isset($this->array[$value]['-1 month'])   ? $this->array[$value]['-1 month'] : 0;
                    $month2=isset($this->array[$value]['-2 month'])   ? $this->array[$value]['-2 month'] : 0;
                    $month3=isset($this->array[$value]['-3 month'])   ? $this->array[$value]['-3 month'] : 0;
                    $month4=isset($this->array[$value]['-4 month'])   ? $this->array[$value]['-4 month'] : 0;
                    $month5=isset($this->array[$value]['-5 month'])   ? $this->array[$value]['-5 month'] : 0;
                    $month6=isset($this->array[$value]['-6 month'])   ? $this->array[$value]['-6 month'] : 0;
                    $month13=isset($this->array[$value]['-13 month']) ? $this->array[$value]['-13 month'] : 0;
                    $month14=isset($this->array[$value]['-14 month']) ? $this->array[$value]['-14 month'] : 0;
                    $days30=isset($this->array[$value]['+30 days']) ? $this->array[$value]['+30 days'] : 0;
                    $fact=isset($this->array[$value]['balanceFact'])  ? $this->array[$value]['balanceFact'] : 0;
                    $rate=isset($this->array[$value]['rate'])  ? $this->array[$value]['rate'] : 0;
                    if (isset($this->array[$value]['rate'])) {
                        $ratecount++;
                    } 


                    $resultCur = $month1 + $month2 + $month3 + $month4 + $month5 + $month6;

                    if ($resultStart < $resultCur) {
                        $resultStart = $resultCur;
                        $article = $value;
                    }

                    $sumMonth1  += $month1;
                    $sumMonth2  += $month2;
                    $sumMonth3  += $month3;
                    $sumMonth4  += $month4;
                    $sumMonth5  += $month5;
                    $sumMonth6  += $month6;
                    $sumMonth13 += $month13;
                    $sumMonth14 += $month14;
                    $sum30days  += $days30;
                    $sumFact    += $fact;
                    $sumRate    += $rate;
                } 
            }

            $customArray[$article]['-1 month'] = $sumMonth1;
            $customArray[$article]['-2 month'] = $sumMonth2;
            $customArray[$article]['-3 month'] = $sumMonth3;
            $customArray[$article]['-4 month'] = $sumMonth4;
            $customArray[$article]['-5 month'] = $sumMonth5;
            $customArray[$article]['-6 month'] = $sumMonth6;
            $customArray[$article]['-13 month'] = $sumMonth13;
            $customArray[$article]['-14 month'] = $sumMonth14;
            $customArray[$article]['+30 days'] = $sum30days;
            $customArray[$article]['balanceFact'] = $sumFact;
            $customArray[$article]['article'] = $article;
            $customArray[$article]['rate'] = $ratecount ? $sumRate/$ratecount : 0;

        }

        $this->array = $customArray;

        //увеличим память
        ini_set("memory_limit","256M");

        $productTypes= ErpProductType::find()->where(['automatic' => 1])->all();
        $aliasNum = 0;
        foreach ($this->array as $key => $value) {
            foreach ($productTypes as $product) {
                if ($product->rule && preg_match($product->rule, $key)) {
                    if (!isset($this->group[$product->id])) {
                        @$this->group[$product->id]['title'] = $product->title;
                        @$this->group[$product->id]['id'] = $product->id;
                        @$this->group[$product->id]['alias'] = ++$aliasNum;
                        if (preg_match('#^(OT)-\S+$#' , $key)) {
                            @$this->group[$product->id]['type'] = 1;
                        }else{
                            @$this->group[$product->id]['type'] = 2;
                        }
                    } 
                    if (isset($this->array[$key]['rate']) && $this->array[$key]['rate']) {
                        @$this->group[$product->id]['sumrate'] += $this->array[$key]['rate'];
                        @$this->group[$product->id]['countrate']++;
                    }
                    foreach ($value as $key1 => $value1) {
                        @$this->group[$product->id][$key1] += $value1;
                    }
                    $this->array[$key]['groupId'] = $product->id;
                    $this->array[$key]['groupTitle'] = $product->title;
                    break;
                }
            }
        }


        foreach ($this->group as $key => $value) {
            $sumrate = isset($value['sumrate'])   ? $value['sumrate'] : 0;
            $countrate = isset($value['countrate']) ? $value['countrate'] : 0;
            $rate = $countrate ? $sumrate/$countrate : 0;
            $this->group[$key]['rate'] = $rate;
        }
        //здесь у нас массив с идентификаторами группы для каждого артикула отдельно

        // var_dump($this->group);
        // var_dump($this->array);
        // die();

        parent::__construct($config);
    }

    static public function getListWithAnalogsArticles($article)
    {
        $articles = [];
         //3. Разбиваем артикул на составляющие с цеот
        $value = explode('-', $article);
        
        //Артикул, для которого мы ищмем аналоги, по умолчанию попадает в массив возращаемых значений 
        $articles[] = $article;

        //Если артикул начинаеться с "OT" то мы не ищем по нему аналоги и возвращается массив со значем только текущего артикула
        //В противном случае мы приступаем к поиску аналогов.
        if ($value[0] != 'OT') {
            
            $value[0] = str_replace('FW', 'ПШ', $value[0]);
            $value[0] = str_replace('FV', 'ПФ', $value[0]);
            $value[0] = str_replace('FD', 'ПБ', $value[0]);
            $value[0] = str_replace('RD', 'ЗБ', $value[0]);
            $value[0] = str_replace('RV', 'ЗФ', $value[0]);
            $value[0] = str_replace('BW', 'ЗШ', $value[0]);

            //Ищем автомобиль
            $car =  Cars::find()->where(['article' => @$value[2]])->one();

            if (!$car) return $articles;
            //Находим аналоги по оконному проему

            $analogsIds = self::_searchAnalogs($car->id,$value[0]);
            if ( ($deleteKey = array_search($car->id,$analogsIds)) !== false ) {
                unset($analogsIds[$deleteKey]);
            }

            $analogs = Cars::find()->where(['in','id',$analogsIds])->all();

            //если аналоги существуют
            if ($analogs) {

                //обходим список аналогов
                foreach ($analogs as $analog) {

                    //заменяем данные из текущего артикула на необходимые данные аналоги, а именно
                    //заменяем значение артикула автомобиля, зашитое в артикуле
                    $value[2] = $analog->article;
                    //заменяем значение первой буквы марки автомобиля, защитое в артикуле, по правилам генерации артикула с Laitovo.ru
                    //Использую вспомогательную функцию getTranslit() для перевода русских марок в нужное написание
                    $value[1] = $analog->getTranslit(mb_substr($analog->mark,0,1));
                    //Получаем новый артикул путем соединения всех данных воедино пи помощи функция склеивания осколков
                    $newarticle = implode('-',$value);
                    //Добавляем новый артикул в массив результирующих артикулов
                    $articles[] = $newarticle;
                }
            }
        }

        return $articles;
    }

    public function getIncreaseByGroup($groupId,$type = 'last')
    {
        switch ($type) {
            case 'before':
                $key = "-14 month";
                break;
            case 'last':
                $key = "-13 month";
                break;
            
            default:
                # code...
                break;
        }

        $start = isset($this->group[$groupId][$key]) ? $this->group[$groupId][$key] : 0;
        $end   = isset($this->group[$groupId]["+30 days"]) ? $this->group[$groupId]["+30 days"] : 0;

        if ($start == 0) return 0;
        $increase =  round(($end / $start) - 1,2);

        return $increase;
    }


    public function getIncreaseSumByGroup($type,$kind)
    {
        switch ($type) {
            case 'before':
                $key = "-14 month";
                break;
            case 'last':
                $key = "-13 month";
                break;
            
            default:
                # code...
                break;
        }

        $sumStart = 0;
        $sumEnd = 0;
        foreach ($this->group as $value) {
            if (@$value['type'] == $kind) {
                $sumStart += isset($value[$key])       ? $value[$key]       : 0;
                $sumEnd   += isset($value["+30 days"]) ? $value["+30 days"] : 0;
            }
        }

        if ($sumStart == 0) return 0;
        $increase =  round(($sumEnd / $sumStart) - 1,2);

        return $increase;
    }


    public function getIncreaseByArticle($article)
    {
        $type = 'last';
        $period1 = "-1 month";
        $period2 = "-2 month";

        $count1 = isset($this->array[$article][$period1])  ? $this->array[$article][$period1] : 0;
        $count2 = isset($this->array[$article][$period2]) ? $this->array[$article][$period2] : 0;

        if ($count1 > $count2) {
            $type = 'before';
        }

        $groupid = isset($this->array[$article]['groupId']) ? $this->array[$article]['groupId'] : 0;
        
        return $this->getIncreaseByGroup($groupid,$type);
    }

    public function getIncreaseSumByArticle()
    {
        $type = 'last';
        $period1 = "-1 month";
        $period2 = "-2 month";

        $sum = 0;
        foreach ($this->array as $key => $value) {
            $count1 = isset($value[$period1]) ? $value[$period1] : 0;
            $count2 = isset($value[$period2]) ? $value[$period2] : 0;

            if ($count1 > $count2) {
                $type = 'before';
            }

            $groupid = isset($value['groupId']) ? $value['groupId'] : 0;
            
            $sum += $this->getIncreaseByGroup($groupid,$type);
        }
        return round($sum,2);
    }

    public function getRealizationByGroup($groupId,$type = 'last')
    {
        switch ($type) {
            case 'before':
                $key = "-14 month";
                break;
            case 'last':
                $key = "-13 month";
                break;
            case 'next':
                $key = "+30 days";
                break;
            
            default:
                # code...
                break;
        }

        return isset($this->group[$groupId][$key]) ? $this->group[$groupId][$key] : 0;
    }


    public function getRealizationSumByGroup($type,$kind)
    {
        switch ($type) {
            case 'before':
                $key = "-14 month";
                break;
            case 'last':
                $key = "-13 month";
                break;
            case 'next':
                $key = "+30 days";
                break;
            
            default:
                # code...
                break;
        }

        $sum = 0;
        foreach ($this->group as $value) {
            if ($value['type'] == $kind) {
                $sum += isset($value[$key]) ? $value[$key] : 0;
            }
        }

        return $sum;
    }


    public function getRealizationByArticle($article,$type = 'last',$light = false)
    {
        $marker = false;
        $before = isset($this->array[$article]["-2 month"]) ? $this->array[$article]["-2 month"] : 0;
        $last   = isset($this->array[$article]["-1 month"]) ? $this->array[$article]["-1 month"] : 0;
        if ($type == 'last' && $last <= $before) $marker = true;
        if ($type == 'before' && $last >= $before) $marker = true;
        switch ($type) {
            case 'before':
                $key = "-2 month";
                break;
            case 'last':
                $key = "-1 month";
                break;
            
            default:
                # code...
                break;
        }


        // var_dump($article);
        // var_dump($type);
        // // var_dump($this->array);

        // die();
        $answer = isset($this->array[$article][$key]) ? $this->array[$article][$key] : 0;
        if ($marker && $light) return '<span style="color:red">' . $answer . '</span>';
        return $answer;
    }


    public function getRealizationSumByArticle($type = 'last')
    {

        switch ($type) {
            case 'before':
                $key = "-2 month";
                break;
            case 'last':
                $key = "-1 month";
                break;
            
            default:
                # code...
                break;
        }

        $sum = 0;
        foreach ($this->array as $key1 => $value) {
            $sum += isset($value[$key]) ? $value[$key] : 0;
        }
        // var_dump($article);
        // var_dump($type);
        // // var_dump($this->array);

        // die();
        return $sum;
    }


    public function getPlanByArticle($article)
    {
        $type = 'last';
        $period1 = "-1 month";
        $period2 = "-2 month";

        $count1 = isset($this->array[$article][$period1])  ? $this->array[$article][$period1] : 0;
        $count2 = isset($this->array[$article][$period2])  ? $this->array[$article][$period2] : 0;

        if ($count1 > $count2) {
            $type = 'before';
        }

        $groupid = isset($this->array[$article]['groupId']) ? $this->array[$article]['groupId'] : 0;
        
        $result = $this->getRealizationByArticle($article,$type);
        $increase = $this->getIncreaseByArticle($article);

        // if ($article == 'BW-A-10-1-5') {
        //     var_dump($result);
        //     var_dump($increase);
        //     die();
        // }

        return round($result + ($result * $increase));
    }


    public function getSumPlanByArticle()
    {
        
        $sum = 0;
        foreach ($this->array as $key => $value) {
            $sum += $this->getPlanByArticle($value['article']);
        }

        return $sum;
        // if ($article == 'BW-A-10-1-5') {
        //     var_dump($result);
        //     var_dump($increase);
        //     die();
        // }

    }


    public function getFactBalance($article)
    {
        return isset($this->storage[$article]) ? $this->storage[$article] : 0;
    }


    public function getSumFactBalance()
    {
        return array_sum($this->storage);
    }

    public function getCarName($article)
    {
        $value = explode('-', $article);
        if ($value[0] != 'OT') {
            $car =  Cars::find()->where(['article' => @$value[2]])->one();
                return $car ? $car->name : 'Общее';
        }
    }

    public function getCarAnalogsCount($article)
    {
        $value = explode('-', $article);
        if ($value[0] != 'OT') {

            switch ($value[0]) {
                case 'FW':
                    $type = 'ПШ';
                    break;
                case 'FV':
                    $type = 'ПФ';
                    break;
                case 'FD':
                    $type = 'ПБ';
                    break;
                case 'RD':
                    $type = 'ЗБ';
                    break;
                case 'RV':
                    $type = 'ЗФ';
                    break;
                case 'BW':
                    $type = 'ЗШ';
                    break;
              
                default:
                    $type = '';
                    break;
            }

            $car =  Cars::find()->where(['article' => @$value[2]])->one();
                return $car ? $car->getAnalogs()->where(['like','json','%'.$type.'%',false])->count() : 0;
        }
    }

    public function getCarAnalogsNames($article)
    {
        $value = explode('-', $article);
        if ($value[0] != 'OT') {
       
            switch ($value[0]) {
                case 'FW':
                    $type = 'ПШ';
                    break;
                case 'FV':
                    $type = 'ПФ';
                    break;
                case 'FD':
                    $type = 'ПБ';
                    break;
                case 'RD':
                    $type = 'ЗБ';
                    break;
                case 'RV':
                    $type = 'ЗФ';
                    break;
                case 'BW':
                    $type = 'ЗШ';
                    break;
              
                default:
                    $type = '';
                    break;
            }

            $str = '';
            $car =  Cars::find()->where(['article' => @$value[2]])->one();
            $analogs =  $car ? $car->getAnalogs()->where(['like','json','%'.$type.'%',false])->all() : 0;
            if ($analogs) {
                foreach ($analogs as $analog) {
                    $anCar = Cars::findOne($analog->analog_id);
                    if ($anCar) {
                        $str .= $anCar->fullEnName . '.......';
                    }
                }
            }
        }

        return $str;
    }

    public function getFrequenceByArticle($article)
    {
        $sum = 0;
        if (isset($this->array[$article]["-1 month"]) && $this->array[$article]["-1 month"]) $sum++; 
        if (isset($this->array[$article]["-2 month"]) && $this->array[$article]["-2 month"]) $sum++; 
        if (isset($this->array[$article]["-3 month"]) && $this->array[$article]["-3 month"]) $sum++; 
        if (isset($this->array[$article]["-4 month"]) && $this->array[$article]["-4 month"]) $sum++; 
        if (isset($this->array[$article]["-5 month"]) && $this->array[$article]["-5 month"]) $sum++; 
        if (isset($this->array[$article]["-6 month"]) && $this->array[$article]["-6 month"]) $sum++; 
        return $sum;
    }

    public function getGroupAliasByArticle($groupId)
    {
        return isset($this->group[$groupId]['alias']) ? $this->group[$groupId]['alias'] : '';
    }

    public function getShareOfBalance()
    {
        $in  = 0;
        $out = 0;
        foreach ($this->realiz as $key => $value) {
            if (!preg_match('#^(OT)-\S+$#' , $key)) {
                if ($this->getPlanStorageBalance($key)) {
                    $in ++;
                } else {
                    $out ++;
                }
            }
        }
        $all = $in+$out;
        
        if ($all == 0) {
            return '--------------';
        }
        
        return round($in/$all*100,2) . ' %';
    }

    public function getShareOfBalance2($type = '')
    {
        $in  = 0;
        $out = 0;
        foreach ($this->realiz as $key => $value) {
            if (!preg_match('#^(OT)-\S+$#' , $key)) {
                if ($this->getPlanStorageBalance($key)) {
                    $in += $value;
                } else {
                    $out+= $value;
                }
            }
        }
        $all = $in+$out;

        switch ($type) {
            case 'in':
                return $in;
                break;
            
            case 'out':
                return $out;
                break;

            case 'all':
                return $all;
                break;

            default:
                if ($all == 0) {
                    return '--------------';
                }
                
                return round($in/$all*100,2) . ' %';
                break;
        }
        

    }

    static private function getArticlesByMonth($monthValue,$articles=[])
    {
        $month  = strtotime($monthValue);
        $date_f  = mktime (  0, 0, 0, date("n",$month),                 1, date("Y",$month));
        $date_to = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
        ]);
        $query->groupBy(['article']);
        $query->select(['article','count' => 'COUNT(id)']);
        $rows = $query->asArray()->all();
        foreach ($rows as $key => $value) {
            $articles[$value['article']][$monthValue] = $value['count']; 
            $articles[$value['article']]['article'] = $value['article']; 
        }
        return $articles;
    }


    static private function getArticlesByDaysUp($monthValue,$articles=[])
    {
        $month  = strtotime($monthValue);
        $date_f  = $month;
        $date_to = $month+30*60*60*24;
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
        ]);
        $query->groupBy(['article']);
        $query->select(['article','count' => 'COUNT(id)']);
        $rows = $query->asArray()->all();
        foreach ($rows as $key => $value) {
            $articles[$value['article']]["+30 days"] = $value['count']; 
            $articles[$value['article']]['article'] = $value['article']; 
        }
        return $articles;
    }

    static function getMonthTitle($type)
    {
        $str_month=array(
            1 =>"Январь",
            2 =>"Февраль",
            3 =>"Март",
            4 =>"Апрель",
            5 =>"Май",
            6 =>"Июнь",
            7 =>"Июль",
            8 =>"Август",
            9 =>"Сентябрь",
            10=>"Октябрь",
            11=>"Ноябрь",
            12=>"Декабрь"
        );
        
        switch ($type) {
            case 'yearbefore':
                $key = "-14 month";
                $month  = strtotime($key);
                return $str_month[date("n",$month)];
                break;
            case 'yearlast':
                $key = "-13 month";
                $month  = strtotime($key);
                return $str_month[date("n",$month)];
                break;
            case 'before':
                $key = "-2 month";
                $month  = strtotime($key);
                return $str_month[date("n",$month)];
                break;
            case 'last':
                $key = "-1 month";
                $month  = strtotime($key);
                return $str_month[date("n",$month)];
                break;
            case 'yearnext':
                $month  = strtotime("+1 month");
                return date("d.m",time()) . ' / ' . date("d.m",$month);
                break;
            case 'next':
                $month1 = strtotime("-11 month");
                $month2  = strtotime("-12 month");
                return date("d.m",$month2) . ' / ' . date("d.m",$month1);
                break;
        }


    }


    static private function joinWithStorageBalance($array,$storage)
    {
        foreach ($storage as $key => $value) {
            $array[$key]['balanceFact'] = $value;
        }

        return $array;
    }

    static private function joinWithProizv($array,$proizv)
    {
        foreach ($proizv as $key => $value) {
            $array[$key]['rate'] = $value;
        }

        return $array;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }


    //Лист артикулов скгруппированых по складам
    public function getFromStorage()
    {
        $array = [];
        foreach ($this->array as $key => $value) {
            if (($this->getFactBalance($value['article']) 
                || $this->getPlanByArticle($value['article']) 
                || $this->getRealizationByArticle($value['article'],$type = 'last')
                || $this->getRealizationByArticle($value['article'],$type = 'before')
            ) && isset($value['groupId']) && $this->group[$value['groupId']]['type'] == 2) {
                $array[$key] = $value;
                $array[$key]['realizationPlan']   = $this->getPlanByArticle($array[$key]['article']);
                $array[$key]['balanceFact']       = $this->getFactBalance($array[$key]['article']);
                $array[$key]['balancePlan']       = $this->getPlanStorageBalance($array[$key]['article']);
                $array[$key]['increase']          = $this->getIncreaseByArticle($array[$key]['article']) ;
                $array[$key]['rate']              = round($this->getProizvRate($array[$key]['groupId']),8);
                $array[$key]['fixrate']           = $this->getFixRate();
                $array[$key]['frequence']         = $this->getFrequenceByArticle($array[$key]['article']);
                $array[$key]['groupAlias']        = $this->getGroupAliasByArticle($array[$key]['groupId']);
            }
        }

        $provider = new ArrayDataProvider([
            'allModels' => $array,
            'sort' => [
                'attributes' => ['car','groupId','groupTitle','article','realizationLast','realizationBefore','increase','realizationPlan','balanceFact','balancePlan','rate','fixrate','frequence'],
                'defaultOrder' => [
                    'realizationPlan' => SORT_DESC,
                ]
            ],
        ]);
        // $provider->pagination = false;
        return $provider;

    }

    //Лист артикулов скгруппированых по складам
    public function getFromStorageNew()
    {
        //Сначала необходимо найти артикулы с не нулевой плановой реализацией\
        $month1  = strtotime("-2 month");
        $month2  = strtotime("-1 month");

        $firstday = mktime ( 0,0,0, date("n",$month1),1, date("Y",$month1));
        $lastday = mktime ( 23,59,59, date("n",$month2),date('t', $month2), date("Y",$month2));

        $query = ArticleStatistics::find()->where(['and',
            ['<=','created_at', $lastday],
            ['>=','created_at', $firstday],
        ]);
        $query->groupBy(['article']);
        $query->select(['article']);
        $rows = $query->asArray()->all();
        $result = [];
        foreach ($rows as $row) {
            if (self::getRealizationPlan($row['article'])) {
                $result[]=$row['article'];
            };
        }

        // $query1 = StorageState::find();;
        // $query1->select(['article']);
        // $query1->groupBy(['article']);
        // $query1->asArray()->all();
        // var_dump($query1)


        // $query = Naryad::find()->joinWith(['storageState state']);
        // // $query->where(['is not','state.storage_id',null]);
        // $query->having(['>','COUNT(state.id)',0]);
        // $query->groupBy(['article']);
        // $query->select(['article','count' => 'COUNT(state.id)']);

        // $provider = new ActiveDataProvider([
        //     'query' => $query
        // ]);

        // return $provider;
    }



    public function getUpnsFromOrder()
    {
        $provider = new ActiveDataProvider([
            'query' => Naryad::find()->joinWith(['source'])->where(['logistics_sources.type' => 2]),
        ]);

        return $provider;
    }

    public function getUpnsFromDispatcher()
    {
        $provider = new ActiveDataProvider([
            'query' => Naryad::find()->joinWith(['source'])->joinWith(['erpNaryad'])->where(['logistics_sources.type' => 1]),

            // 'query' => ErpNaryad::find()
            //             ->where(['and',
            //                 ['!=','logist_id',''],
            //                 ['is not','logist_id',null],
            //                 ['status' => ErpNaryad::STATUS_READY],
            //             ]),
            // 'pagination' => [
            //     'pageSize' => 5,
            // ],
            // 'sort' => false,
        ]);

        return $provider; 
    }

    //датапровайдер для реализации
    public static function getRealizationData()
    {
        $provider = new ActiveDataProvider([
            'query' => OrderEntry::find(),
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false,
        ]);

        return $provider;
    }

    public function getStockCirculationBalanceData()
    {
        $provider = new ActiveDataProvider([
            'query' => StorageState::find()->where(['storage_id' => 1]),
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false,
        ]);

        return $provider;
    }


    public function getStockTemporaryBalanceData()
    {
        $provider = new ActiveDataProvider([
            'query' => StorageState::find()->where(['storage_id' => 2]),
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false,
        ]);

        return $provider;
    }

    public static function getUpnsData()
    {
        $provider = new ActiveDataProvider([
            'query' => Naryad::find(),
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false,
        ]);

        return $provider;
    }

    public static function getUpnsForComingData()
    {
        $provider = new ActiveDataProvider([
            'query' => ErpNaryad::find()
                        ->where(['and',
                            ['!=','logist_id',''],
                            ['is not','logist_id',null],
                            ['status' => ErpNaryad::STATUS_READY],
                        ]),
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false,
        ]);

        return $provider;        
    }

    //реализации за последний месяц
    public function getLastRealization($article)
    {
        $month  = strtotime("-1 month");

        $firstday = mktime ( 0,0,0, date("n",$month),1, date("Y",$month));
        $lastday = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));

        return ArticleStatistics::find()->where(['and',
            ['article' => $article],
            ['<=','created_at', $lastday],
            ['>=','created_at', $firstday],
        ])->count();
    }

    //реализации за предпоследний месяц
    public function getPredLastRealization($article)
    {
        $month  = strtotime("-2 month");

        $firstday = mktime ( 0,0,0, date("n",$month),1, date("Y",$month));
        $lastday = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));

        return ArticleStatistics::find()->where(['and',
            ['article' => $article],
            ['<=','created_at', $lastday],
            ['>=','created_at', $firstday],
        ])->count();
    }

    public function getRealizationPlan($article)
    {
        $count = self::getLastRealization($article);
        $count2 = self::getPredLastRealization($article);
        $result = $count;

        if ($count > $count2) {
            $result = $count2;
        }

        $increase = self::getLastOptimalIncrease($article);
        return round($result + ($result * $increase));

        // if ($result>0) {
        //     return $value;
        // }elseif($result == 0 && $increase > 0) {
        //     return 1*$increase;
        // }else{
        //     return 0;
        // }
    }


    public function getProizvRate2($article)
    {
        if (isset($this->proizv[$article])) { 
            return $this->proizv[$article];
        }

        //достаем произведенные артикулы за период
        $month  = strtotime("-6 month");
        $month1  = strtotime("-1 month");

        $firstday = mktime ( 0,0,0, date("n",$month),1, date("Y",$month));
        $lastday = mktime ( 23,59,59, date("n",$month1),date('t', $month1), date("Y",$month1));

        $query = ErpNaryad::find();
        $query->where(['and',
            ['status' => ErpNaryad::STATUS_READY],
            ['article' => $article],
            ['<=','created_at', $lastday],
            ['>=','created_at', $firstday],
        ]);
        $query->groupBy(['article']);
        $query->select(['article','rate' => 'AVG((updated_at - created_at))']);
        $rows = $query->asArray()->all();

        return isset($rows[$article]) ? $rows[$article]/(60*60*24*30) : 0;
    }


    public function getProizvRate($groupId)
    {
        return isset($this->group[$groupId]['rate']) ? $this->group[$groupId]['rate'] : 0;
    }

    public function getMeanProizvRate()
    {
        $sum = 0;
        $count = 0;
        foreach ($this->proizv as $key => $value) {
            $sum += $value;
            $count++;
        }
        if ($count) {
            return round($sum/$count,2);
        }
        return 0;
    }

    public function getFixRate()
    {
        return $this->_fixRate;
    }

    public function getMinFrequency()
    {
        return $this->_minFrequency;
    }


    public function getPlanStorageBalance($article)
    {
        $groupID = isset($this->array[$article]['groupId']) ? $this->array[$article]['groupId'] : null;
        //что такое плановый баланс
        
        // это количество на реализацию * на производительность * поправочный коэффициент
        $rate = $this->getFixRate();
        $plan = $this->getPlanByArticle($article);
        $productionRate = $groupID ? $this->getProizvRate($groupID) : 0;
        $planB = $this->getFrequenceByArticle($article) > $this->getMinFrequency() ? 1 : 0;

        return ceil($plan * $productionRate * $rate) > 0 ?  ceil($plan * $productionRate * $rate) : $planB;
        //высчитывается как средий срок изготовления артикула
    }


    public function getSumPlanStorageBalance()
    {
        $sum = 0;
        foreach ($this->array as $key => $value) {
            $sum += $this->getPlanStorageBalance($key);
        }
        return $sum;
    }   


    public function getStatisticByGroupsMain()
    {
        $array = [];
        foreach ($this->group as $key => $value) {
            if ($value['type'] == 2) {
                $array[$key] = $value;
                $array[$key]['realization']   = $this->getRealizationByGroup($array[$key]['id'],'before') 
                                                . ' / ' 
                                                . $this->getRealizationByGroup($array[$key]['id'],'last');
                $array[$key]['realization30'] = $this->getRealizationByGroup($array[$key]['id'],'next');
                $array[$key]['increase']      = $this->getIncreaseByGroup($array[$key]['id'],'before') 
                                                . ' / ' 
                                                . $this->getIncreaseByGroup($array[$key]['id'],'last');
            }
        }
        return $provider = new ArrayDataProvider([
            'allModels' => $array,
            'sort' => [
                'attributes' => ['title', 'id','realization','realization30','increase','alias'],
                'defaultOrder' => [
                    'alias' => SORT_ASC,
                    // 'realization30' => SORT_DESC,
                ]
            ],
        ]);
    }


    public function getStatisticByGroupsOther()
    {
        $array = [];
        foreach ($this->group as $key => $value) {
            if ($value['type'] == 1) {
                $array[$key] = $value;
                $array[$key]['realization']   = $this->getRealizationByGroup($array[$key]['id'],'before') 
                                                . ' / ' 
                                                . $this->getRealizationByGroup($array[$key]['id'],'last');
                $array[$key]['realization30'] = $this->getRealizationByGroup($array[$key]['id'],'next');
                $array[$key]['increase']      = $this->getIncreaseByGroup($array[$key]['id'],'before') 
                                                . ' / ' 
                                                . $this->getIncreaseByGroup($array[$key]['id'],'last');
            }
        }
        return $provider = new ArrayDataProvider([
            'allModels' => $array,
            'sort' => [
                'attributes' => ['title', 'id','realization','realization30','increase','alias'],
                'defaultOrder' => [
                    'alias' => SORT_ASC,
                    // 'realization30' => SORT_DESC,
                ]
            ],
        ]);
    }


    public static function _searchAnalogs ($car_id,$type,$ids = []) {
        $ids2 = [];
        $ids2[] = $car_id;
        $analogs = CarsAnalog::find()->where(['car_id' => $car_id])->all();
        foreach ($analogs as $analog) {
            $json=Json::decode($analog->json ? $analog->json : '{}');
            $types = isset($json['elements']) ? $json['elements'] : [];
            if (!in_array($analog->analog_id, $ids2) && in_array($type,$types)) {
                $ids2[] = $analog->analog_id;
            }
        }

        //Находим расхождения в массивах
        $diff = array_diff($ids2, $ids);

        // if ($car_id == 638) {
        //     var_dump('IDS-2');
        //     var_dump($ids2);
        //     var_dump('IDS');
        //     var_dump($ids);
        //     var_dump('DIFF');
        //     var_dump($diff);
        //     var_dump(empty($diff));
        //     die();
        // }

        if (count($diff)) {
            foreach ($diff as $key => $value) {
                $values = self::_searchAnalogs($value,$type,array_merge($diff, $ids));
                if ($values) {
                    foreach ($values as $value) {
                        if (!in_array($value, $diff)) {
                            $diff[] = $value;
                        }
                    }
                }

            }
        }


        //возвращаем либо массив либо ложь
        return empty($diff) ? false : $diff;
    }


}
