<?php

namespace backend\modules\logistics\models;

use Yii;
use \common\models\team\Team;
use \common\models\User;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "logistics_storage".
 *
 * @property integer $id
 * @property integer $team_id
 * @property string $title
 * @property integer $type
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property Team $team
 * @property User $updater
 */
class Storage extends \common\models\logistics\Storage
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_storage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'type', 'created_at', 'author_id', 'updated_at', 'updater_id'], 'integer'],
            [['type','title','literal','logic'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['literal'], 'string', 'max' => 1],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }

    public function getLiteral($article)
    {
        $type = '';
        if ($this->type == self::TYPE_REVERSE) {
            if (preg_match('#^(OT)-\S+$#' , $article)) {
                
            }
            if (preg_match('#^(FW)-\S+$#' , $article)) {
                
            }
            if (preg_match('#^(FV|RV)-\S+$#' , $article)) {
                $type = 'A';
            }
            if (preg_match('#^(FD|RD)-\S+$#' , $article)) {
                $type = 'B';
            }
            if (preg_match('#^(BW)-\S+$#' , $article)) {
                $type = 'C';
            }
        }

        if ($this->type == self::TYPE_TIME) {
            if (preg_match('#^(OT)-\S+$#' , $article)) {
                
            }
            if (preg_match('#^(FW)-\S+$#' , $article)) {
                
            }
            if (preg_match('#^(FV|RV)-\S+$#' , $article)) {
                $type = 'AT';
            }
            if (preg_match('#^(FD|RD)-\S+$#' , $article)) {
                $type = 'BT';
            }
            if (preg_match('#^(BW)-\S+$#' , $article)) {
                $type = 'CT';
            }
        }

        $query = $this->hasMany(StorageState::className(), ['storage_id' => 'id']);
        $query->where(['like','literal',$type.'%',false]);
        $query->having(['<','COUNT(literal)',10]);
        $query->groupBy(['literal']);
        $query->select(['literal','count' => 'COUNT(literal)']);
        $query->orderby(['literal'=> SORT_ASC]);
        $result = $query->one();

        if ($result) return $result->literal;

        if (!$result) {
            $query = $this->hasMany(StorageState::className(), ['storage_id' => 'id']);
            $query->where(['like','literal',$type.'%',false]);
            $query->select(['literal']);
            $query->orderby(['literal' => SORT_DESC]);
            $result = $query->one();
        }
        $postfix = str_replace($type, "", $result->literal);

        return $result ? $type . ($postfix + 1) : $type . '1';
    }

    public function getLiteral2($product_id)
    {
        //Получаем первую часть литеры
        $part1 = $this->typeLiteral;
        $part2 = $this->literal; //Временно решение

        //Получаем третью часть литеры
        $row = (new \yii\db\Query())
            ->from('logistics_storage_product')
            ->where(['product_id' => $product_id])
            ->andWhere(['storage_id' => $this->id])
            ->one();

        $literal = StorageLiteral::findOne($row['literal_id']);

        $part3 = $literal->title;
        $capacity = $literal->capacity ? $literal->capacity : 10;
        $quantity = $literal->default_quantity ? $literal->default_quantity : 10;
        $type = $part1 . $part2 . "-" . $part3;

        //сначала выбираем все задействованные литеры на складе
        $query = $this->hasMany(StorageState::className(), ['storage_id' => 'id']);
        $query->where(['like','literal',$type.'%',false]);
        $query->groupBy(['literal']);
        $query->select(['literal','count' => 'COUNT(id)']);
        $result = $query->asArray()->all();

        //создать массив из возможных литер с нулевыми начениями количества
        $checkNumbers = [];
        for ($i = 1; $i <= $quantity; $i++) {
            if (!in_array($i, $checkNumbers)) {
                $checkNumbers[$i] = 0;
            }
        }

        //Для этого получившегося с базы массива получить все что есть
        $newResult = [];
        if (count($result)) {
            foreach ($result as $value) {
                preg_match_all("/\d+/", $value['literal'], $matches);
                $newResult[$matches[0][0]] = $value['count'];
            }
        }

        //Получаем итоговое значение с литерами
        foreach ($checkNumbers as $key => $value) {
            if (isset($newResult[$key])) {
                $checkNumbers[$key] = $newResult[$key];
            }
        }

        foreach ($newResult as $key => $value) {
            if (!isset($checkNumbers[$key])) {
                $checkNumbers[$key] = $newResult[$key];
            }
        }

        //Сортируем получившийся массив по ключам
        ksort($checkNumbers);

        $num = null;

        $num =  max(array_keys($checkNumbers)) + 1;

        foreach ($checkNumbers as $key => $value) {
            if ($value < $capacity) {
                $num = $key;
                break;
            }
        }

        //автоматическое увеличение количества литер
        if ($num && $num > $literal->default_quantity) {
            $literal->default_quantity = $num;
            $literal->save();
        }

        return $num ? $type . $num : $type . '1';
    }

    /**
     * Алгоритм поиска наименее заполненной литеры
     *
     * @param $product_id
     * @param $oversized
     * @return mixed|string|null
     */
    public function getLiteral3($product_id, $oversized)
    {
        /**
         * Получаем первую часть литеры по типу склада
         */
        $part1 = $this->typeLiteral;

        /**
         * Получаем вторую часть литеры, устанавливаемую вручную
         */
        $part2 = $this->literal;

        //Если это склад пополнения автошторок
        if ($part1 . $part2 == 'TA') {

            if ($oversized) {
                $startLiteral = 300;
                $endLiteral   = 309;
            } else {
                $startLiteral = 1;
                $endLiteral   = 225;
            }
            /**
             * Получаем часть литеры, зависящую от продукта, который хотим положить
             */
            $row = (new \yii\db\Query())
                ->from('logistics_storage_product')
                ->where(['product_id' => $product_id])
                ->andWhere(['storage_id' => $this->id])
                ->one();
            $literal = StorageLiteral::findOne($row['literal_id']);
            $part3 = $literal->title;

            /** @var integer $capacity  Вместимость одной литеры*/
            $capacity = $literal->capacity ? $literal->capacity : 10;

            /**
             * Собрали необходимую часть
             */
            $type = 'СКЛАД-';

            /**
             *  В качестве результтат запроса, получили литеру такогоже типа и количество, которое на них числиться
             */
            $query = $this->hasMany(StorageState::className(), ['storage_id' => 'id']);
            $query->where(['like','literal',$type.'%',false]);
            $query->andWhere("CAST(REPLACE(`literal`,'" . $type . "','') AS UNSIGNED) >= $startLiteral");
            $query->andWhere("CAST(REPLACE(`literal`,'" . $type . "','') AS UNSIGNED) <= $endLiteral");
            $query->groupBy(['literal']);
            $query->select(['literal','count' => 'COUNT(id)']);
            $notEmptyLiterals = $query->asArray()->all();

            //Мы получили все литеры с соответсвующим количеством
            //Теперь мы должны получить ключи
            $numbers = [];
            $notEmptyLiteralNumbers = [];
            $num = null;
            $min = null;

            /**
             *  Если с таким типом существовала хотя бы одна литера тогда,
             * перебираем литеру, ищем числовое значение, его записываем в номера, например 1,3,5.
             * Если литера в интервале возможных значений, тогда мы ее добавляем в возможный результат
             */
            if (count($notEmptyLiterals)) {
                foreach ($notEmptyLiterals as $value) {
                    preg_match_all("/\d+/", $value['literal'], $matches);
                    $keyNumber = (int)$matches[0][0];
                    $numbers[] = $keyNumber;
                    if ( $keyNumber >= $startLiteral  && $keyNumber <= $endLiteral ) {
                        $notEmptyLiteralNumbers[$keyNumber] = (int)$value['count'];
                    }
                }
            }

            $emptyLiteralNumbers = [];
            for ($i = $startLiteral; $i <= $endLiteral; $i++) {
                if (!in_array($i, $numbers)) {
                    $emptyLiteralNumbers[] = $i;
                }
            }

            $minEmptyLiteralNumber = null;
            if (count($emptyLiteralNumbers)) {
                $minEmptyLiteralNumber = min($emptyLiteralNumbers);
            }

            //Если есть непустые литеры
            if (count($notEmptyLiteralNumbers)) {
                //Получаем номера литер, которые заполнены не полность
                $notFullFilledLiteralNumbers = [];
                foreach ($notEmptyLiteralNumbers as $number => $count) {
                    if ($count < $capacity) {
                        $notFullFilledLiteralNumbers[] = $number;
                    }
                }
                //Если есть литеры, которые заполнены не полностью
                if (count($notFullFilledLiteralNumbers)) {
                    $num = min($notFullFilledLiteralNumbers);
                    if ($minEmptyLiteralNumber !== null && $minEmptyLiteralNumber < $num) {
                        $num = $minEmptyLiteralNumber;
                    }
                //Если все не пустые литеры заполнены под завязку
                } else {
                    //Если есть доступная пустая литера, тогда отдаем ее
                    if ($minEmptyLiteralNumber !== null) {
                        $num = $minEmptyLiteralNumber;
                        //В противном случае возвращаме null
                    } else {
                        return null;
                    }
                }
            } else {
                $num = $minEmptyLiteralNumber;
            }

            return $num ? $type . $num : null;
        } else {
            /**
             * Получаем часть литеры, зависящую от продукта, который хотим положить
             */
            $row = (new \yii\db\Query())
                ->from('logistics_storage_product')
                ->where(['product_id' => $product_id])
                ->andWhere(['storage_id' => $this->id])
                ->one();
            $literal = StorageLiteral::findOne($row['literal_id']);
            $part3 = $literal->title;

            /** @var integer $capacity  Вместимость одной литеры*/
            $capacity = $literal->capacity ? $literal->capacity : 10;

            /** @var integer $quantity  Количество литер данного типа*/
            $quantity = $literal->default_quantity ? $literal->default_quantity : 10;

            /**
             * Собрали необходимую часть
             */
            $type = $part1 . $part2 . "-" . $part3;

            /**
             *  В качестве результтат запроса, получили литеру такогоже типа и количество, которое на них числиться
             */
            $query = $this->hasMany(StorageState::className(), ['storage_id' => 'id']);
            $query->where(['like','literal',$type.'%',false]);
            $query->groupBy(['literal']);
            $query->select(['literal','count' => 'COUNT(id)']);
            $result = $query->asArray()->all();

            //Мы получили все литеры с соответсвующим количеством
            //Теперь мы должны получить ключи
            $numbers = [];
            $newResult = [];
            $num = null;
            $min = null;


            /**
             *  Если с таким типом существовала хотя бы одна литера тогда,
             * перебираем литеру, ищем числовое значение, его записываем в номера, например 1,3,5.
             * Если литера в интервале возможных значений, тогда мы ее добавляем в возможный результат
             */
            if (count($result)) {
                foreach ($result as $value) {
                    preg_match_all("/\d+/", $value['literal'], $matches);
                    $keyNumber = (int)$matches[0][0];
                    $numbers[] = $keyNumber;
                    if ( 0 < $keyNumber && $keyNumber <= $quantity) {
                        $newResult[$keyNumber] = (int)$value['count'];
                    }
                }
            }

            $checkNumbers = [];
            for ($i = 1; $i <= $quantity; $i++) {
                if (!in_array($i, $numbers)) {
                    $checkNumbers[] = $i;
                }
            }

            $numCheck = null;
            if (count($checkNumbers)) {
                $numCheck = min($checkNumbers);
            }

            if (count($newResult)) {
                $notBusy = [];
                foreach ($newResult as $index => $count) {
                    if ($count < $capacity) {
                        $notBusy[] = $index;
                    }
                }
                if (count($notBusy)) {
                    $num = min($notBusy);
                    if ($numCheck !== null && $numCheck < $num) {
                        $num = $numCheck;
                    }
                } else {
                    //тогда надо создавать новую литеру и увеливать складу значение литеры
                    if ($numCheck !== null) {
                        $num = $numCheck;
                    } else {
                        $literal->default_quantity = $quantity + 1;
                        $literal->save();
                        $num = $literal->default_quantity;
                    }
                }
            } else {
                $num = $numCheck;
            }

            return $num ? $type . $num : $type . '1';
        }

    }

    /**
     * Алгоритм поиска литеры с конкретным заказом.
     *
     * @param integer $countArticles
     * @return string
     */
    public function getNotUseLiteral($countArticles)
    {
        /**
         * Получаем первую часть литеры по типу склада
         */
        $part1 = $this->typeLiteral;

        /**
         * Получаем вторую часть литеры, устанавливаемую вручную
         */
        $part2 = $this->literal;

        /**
         * Получаем часть литеры, зависящую от количества в заказе, в которую хотиv положить.
         */
        if ($countArticles <= 5) {
            $part3 = 'S';
        }else {
            $part3 = 'M';
        }

        /**
         * Собрали необходимую часть
         */
        $type = $part1 . $part2 . "-" . $part3;

        /**
         *  В качестве результтат запроса, получили литеру такогоже типа и количество, которое на них числиться
         */
        $query = $this->hasMany(StorageState::className(), ['storage_id' => 'id']);
        $query->where(['like','literal',$type.'%',false]);
        $query->groupBy(['literal']);
        $query->select(['literal','count' => 'COUNT(id)']);
        $result = $query->asArray()->all();

        //Мы получили все литеры с соответсвующим количеством
        //Теперь мы должны получить ключи
        $numbers = [];
        $num = null;


        /**
         *  Если с таким типом существовала хотя бы одна литера тогда,
         * перебираем литеру, ищем числовое значение, его записываем в номера, например 1,3,5.
         * Если литера в интервале возможных значений, тогда мы ее добавляем в возможный результат
         */

        //Вычислили все литеры на складе 1,3,5
        if (count($result)) {
            foreach ($result as $value) {
                preg_match_all("/\d+/", $value['literal'], $matches);
                $keyNumber = (int)$matches[0][0];
                $numbers[] = $keyNumber;
            }
        }

        if (count($numbers)) {
            $maxNumber = max($numbers); //5

            $checkNumbers = [];
            for ($i = 1; $i <= $maxNumber; $i++) {
                if (!in_array($i, $numbers)) {
                    $checkNumbers[] = $i; //2,4
                }
            }

            $minNumber = null;
            if (count($checkNumbers)) {
                $minNumber = min($checkNumbers);
            }

            $num = $minNumber ?: $maxNumber + 1;
        }


        // var_dump($capacity);
        // var_dump($min);
        // var_dump('$product_id');
        // var_dump('$product_id');
        // var_dump($product_id);
        // var_dump('row');
        // var_dump($row);
        // var_dump('literal');
        // var_dump($literal);
        // var_dump('type');
        // var_dump($type);
        // var_dump('result');
        // var_dump($result);
        // var_dump('numbers');
        // var_dump($numbers);
        // var_dump('newResult');
        // var_dump($newResult);
        // var_dump('checkNumbers');
        // var_dump($checkNumbers);
        // var_dump('min');
        // var_dump($min);
        // var_dump('num');
        // var_dump($num);
        // die();

        return $num ? $type . $num : $type . '1';
    }

    /**
     * Получение транзитной литеры
     *
     * @return string|null
     */
    public function getTempLiteral($oversized)
    {
        if (!$oversized) {
            $startLiteral = 1;
            $endLiteral = 126;
            $literalCount = 4;
        } else {
            $startLiteral = 127;
            $endLiteral = 182;
            $literalCount = 6;
        }

        /**
         * @var $type [ Перфикс литеры на складе ]
         */
        $type = "TT-T";


        //Для текущего склада находим все непустые литеры данного типа $type с указанием количество артикулов, которые в них лежат
        //Результат запроса - таблица вида  - литера : количество в литере.
        $query = $this->hasMany(StorageState::className(), ['storage_id' => 'id']);
        $query->where(['like','literal',$type.'%',false]);
        $query->andWhere("CAST(REPLACE(`literal`,'" . $type . "','') AS UNSIGNED) >= $startLiteral");
        $query->andWhere("CAST(REPLACE(`literal`,'" . $type . "','') AS UNSIGNED) <= $endLiteral");
        $query->groupBy(['literal']);
        $query->select(['literal','count' => 'COUNT(id)']);
        $query->orderBy(new Expression("CAST(REPLACE(`literal`,'" . $type . "','') AS UNSIGNED)"));

        /**
         * @var $notEmptyLiterals array [ НЕПУСТЫЕ литеры, которые уже задействованы под хранение на данном складе ]
         */
        $notEmptyLiterals = $query->asArray()->all();

        /**
         * @var $numbers [ Список все номеров НЕПУСТЫХ литер на скалде ]
         */
        $numbers = [];

        /**
         * @var $num null|integer [ Номер литеры, которую мы вернем в качестве результата выполнения функции ]
         */
        $num = null;

        // Если на скалде есть НЕПУСТЫЕ литеры
        if (count($notEmptyLiterals)) {

            /**
             * @var $minFilledLiteralCount [ Минимальное количество артикулов, содержашихся в литере]
             */
            $minFilledLiteralCount = 10000000000;

            /**
             * @var $minFilledLiteral null|string [ Минимально заполненная литера и самая маленькая по порядку из НЕПУСТЫХ ЛИТЕР]
             */
            $minFilledLiteral = null;

            /**
             * @var $minNotFilledLiteral null|string [ Самая маленькая литера по порядку из ПУСТЫХ ЛИТЕР ]
             */
            $minNotFilledLiteral = null;

            /**
             * Тогда перебираем массив литер,
             * Выделяем их числовое обозначение без типа.
             * Записываем его в список номеров литер.
             *
             * Перебериаля массив литер, мы сразу ищем наименее заполненную литеру
             *
             * Если это литера с номером до 181 и в ней лежит меньше 4х арттикулов (3,2,1)
             * и если минимальное количество в литере больше чем количество в текущей литере,
             * тогда мы назначем текущую проверяемую литеру, как минимально наполненную
             */
            //Перебирая список НЕПУСТЫХ литер склада
            foreach ($notEmptyLiterals as $notEmptyLiteral) {
                preg_match_all("/\d+/", $notEmptyLiteral['literal'], $matches);
                $keyNumber = (int)$matches[0][0];
                //помещаяем числовые значения литер в список номеров литер
                $numbers[] = $keyNumber;

                //А также ищем наименне заполненную литеру отфильтровывя их по 2м призанкам :
                // - в литере менее 4 артикулов и номер литеры менее 181
                if ($minFilledLiteralCount > +$notEmptyLiteral['count'] && +$notEmptyLiteral['count'] < $literalCount & $keyNumber <= $endLiteral) {
                    $minFilledLiteral = $notEmptyLiteral['literal'];
                    $minFilledLiteralCount = +$notEmptyLiteral['count'];
                }
            }

            /**
             * @var $maxNumber integer [ Максимальный номер из списка заполненных литер ]
             */
            $maxNumber = max($numbers);

            /**
             * @var $emptyLiteralNumbers [ Список номеров пустых литер, которые стоят в порядке до самой последней заполненной литеры ]
             */
            $emptyLiteralNumbers = [];
            //Начиная с первой литеры по порядку до последней заполненной, ищем пустые литеры в этом интервале и записываем их порядковые номера
            for ($i = $startLiteral; $i <= $maxNumber; $i++) {
                if (!in_array($i, $numbers)) {
                    $emptyLiteralNumbers[] = $i; //2,4
                }
            }

            //Если есть пустые литеры в инетрвале  до последней заполненной литеры, тогда номером запрашиваемой литеры является минимальный порядковый номер пустой литеры
            if (count($emptyLiteralNumbers)) {
                $num = min($emptyLiteralNumbers);
            //В противном случае, если склад заполен не полностью и соотвественно максимальный номер заполненной литеры меньше 180,
            //Тогда номером запрашиваемой литеры является следующий за последней заполенной литерой
            } else if ($maxNumber < $endLiteral) {
                $num = $maxNumber + 1;
            }
            //Если номер запрашиваемой литеры нам известен, отдаем запрашиваему литеру.
            if ($num)
                $minNotFilledLiteral = $type . $num;

            //Если у нас есть хотя бы одна пустая литера, мы отдадим пустую литеру с минимальным порядковым значением
            if ($minNotFilledLiteral) {
                return $minNotFilledLiteral;
            //Если же у нас нет пустых литер, а только не до конца заполненные литеры, тогда отдадим литеру с минимальным количеством артикулов на ней
                // и минимальным порядковым номером
            } else if ($minFilledLiteral) {
                return $minFilledLiteral;
            } else {
                //Если склад полон, то есть все литеры по 180 заполнены полностью, литеры никакой не будет выделено.
                return null;
            }

        } else {
            //Если же на складе только ПУСТЫЕ литеры, тогда отдаем первую по порядку.
            return $type . $startLiteral;
        }
    }

    public function getCommonLiteral()
    {
        /**
         * Получаем первую часть литеры по типу склада
         */
        $part1 = 'T';

        /**
         * Получаем вторую часть литеры, устанавливаемую вручную
         */
        $part2 = 'T';

        /**
         * Получаем часть литеры, зависящую от количества в заказе, в которую хотиv положить.
         */

        $part3 = 'T';


        /**
         * Собрали необходимую часть
         */
        $type = $part1 . $part2 . "-" . $part3;

        /**
         *  В качестве результтат запроса, получили литеру такогоже типа и количество, которое на них числиться
         */
        $query = $this->hasMany(StorageState::className(), ['storage_id' => 'id']);
        $query->where(['like','literal',$type.'%',false]);
        $query->groupBy(['literal']);
        $query->select(['literal','count' => 'COUNT(id)']);
        $result = $query->asArray()->all();

        //Мы получили все литеры с соответсвующим количеством
        //Теперь мы должны получить ключи
        $numbers = [];
        $num = null;


        /**
         *  Если с таким типом существовала хотя бы одна литера тогда,
         * перебираем литеру, ищем числовое значение, его записываем в номера, например 1,3,5.
         * Если литера в интервале возможных значений, тогда мы ее добавляем в возможный результат
         */

        //Вычислили все литеры на складе 1,3,5
        if (count($result)) {
            foreach ($result as $value) {
                preg_match_all("/\d+/", $value['literal'], $matches);
                $keyNumber = (int)$matches[0][0];
                $numbers[] = $keyNumber;
            }
        }

        if (count($numbers)) {
            $maxNumber = max($numbers); //5

            $checkNumbers = [];
            for ($i = 1; $i <= $maxNumber; $i++) {
                if (!in_array($i, $numbers)) {
                    $checkNumbers[] = $i; //2,4
                }
            }

            $minNumber = null;
            if (count($checkNumbers)) {
                $minNumber = min($checkNumbers);
            }

            $num = $minNumber ?: $maxNumber + 1;
        }


        // var_dump($capacity);
        // var_dump($min);
        // var_dump('$product_id');
        // var_dump('$product_id');
        // var_dump($product_id);
        // var_dump('row');
        // var_dump($row);
        // var_dump('literal');
        // var_dump($literal);
        // var_dump('type');
        // var_dump($type);
        // var_dump('result');
        // var_dump($result);
        // var_dump('numbers');
        // var_dump($numbers);
        // var_dump('newResult');
        // var_dump($newResult);
        // var_dump('checkNumbers');
        // var_dump($checkNumbers);
        // var_dump('min');
        // var_dump($min);
        // var_dump('num');
        // var_dump($num);
        // die();

        return $num ? $type . $num : $type . '1';
    }

    public function getActualLiteral($product_id, $oversized = false)
    {
        switch ($this->logic) {
            case self::LOGIC_IN_SERIES:
                return $this->getLiteral2($product_id);

            case self::LOGIC_DISTRIBUTED:
                return $this->getLiteral3($product_id, $oversized);
        }
    }

    public static function findStorageForProduct($product_id,$storageType)
    {
        if ($product_id === null || $storageType === null) return null;

        $storages = self::find()->where(['type' => $storageType])->all();
        $ids = ArrayHelper::map($storages,'id','id');

        $row = (new \yii\db\Query())
            ->from('logistics_storage_product')
            ->where(['product_id' => $product_id])
            ->andWhere(['in','storage_id',$ids])
            ->one();

        return $row ? self::findOne($row['storage_id']) : null;
    }
}
