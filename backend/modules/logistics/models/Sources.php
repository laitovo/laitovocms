<?php

namespace backend\modules\logistics\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "logistics_sources".
 *
 * @property integer $id
 * @property integer $team_id
 * @property string $title
 * @property integer $type
 * @property string $key
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property Team $team
 * @property User $updater
 */
class Sources extends \common\models\logistics\Sources
{
    /**
     * Константы типов источников.
     */
    const TYPE_INTERNAL_SUBDIVISION = 1; //Внутренний - подразделение
    const TYPE_EXTERNAL_SITE        = 2; //Внешний    - сайт
    const TYPE_EXTERNAL_MODULE      = 3; //Внешний    - сторонний логистический модуль

    /**
     * [$types Возвращает человекопонятные наименования типов источников нарядов]
     * @var [array]
     */
    public $types = [
        '' => '',
        self::TYPE_INTERNAL_SUBDIVISION => 'Внутренний (подразделение)',
        self::TYPE_EXTERNAL_SITE        => 'Внешний (сайт)',
        self::TYPE_EXTERNAL_MODULE      => 'Внешний (сторонний логистический модуль)',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'type', 'created_at', 'author_id', 'updated_at', 'updater_id'], 'integer'],
            [['title','type'], 'required'],
            [['title'], 'titleValidate'],
            [['title', 'key'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\team\Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }

    /**
     * [titleValidate Кастомный валидатор значения атрибута 'title']
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-21
     * @anotherdate 2017-10-21T09:10:10+0300
     * @param       [attribute]                   $attribute [Атрибут, который валидируется при помощи данной функции]
     * @param       [array]                       $params    [Дополнительные параметры для валидации атрибута]
     * @return      [boolean]                                [Возвращает TRUE если значение уникально внутри команды]
     */
    public function titleValidate($attribute, $params)
    {
        $query = self::find()->where(['and',
            ['title' => $this->title],
            ['team_id' => Yii::$app->team->id]
        ]);
        //если это не создание а изменение, проверяем на уникальность без учета текущей записи
        if ($this->id) {
            $query->andWhere(['!=','id',$this->id]);
        }

        $row = $query->one();

        if ($row) {
            $this->addError($attribute, Yii::t('app', 'Такое наименование уже существует. Выберите другое.'));
        }
    }

    /**
     * [asList Функция выводит все источники в качестве массива id -> наименование]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-23
     * @anotherdate 2017-10-23T15:32:54+0300
     * @return      [array]                   [Массив значений id => title]
     */
    public static function asList()
    {
        return ArrayHelper::merge([''=>''],ArrayHelper::map(self::find()->all(),'id','title'));
    }

}
