<?php

namespace backend\modules\logistics\models;

use core\models\article\Article;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use \backend\modules\logistics\models\Sources;
use \backend\modules\logistics\models\Storage;
use \backend\modules\logistics\models\StorageState;
use backend\modules\laitovo\models\ErpProductType;
use \common\models\team\Team;
use \common\models\User;
use \backend\modules\laitovo\models\ErpNaryad;
use \common\models\laitovo\Cars;

/**
 * This is the model class for table "logistics_naryad".
 *
 * @property integer $id
 * @property integer $team_id
 * @property string $article
 * @property integer $source_id
 * @property integer $reserved_id
 * @property integer $reserved_at
 * @property string $responsible_person
 * @property string $comment
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 * @property string  $barcode
 * @property boolean $isParent
 * @property integer $pid
 * @property Naryad  $children

 *
 * @property User $author
 * @property LogisticsSources $source
 * @property Team $team
 * @property User $updater
 */
class Naryad extends \common\models\logistics\Naryad
{
    private $_packed;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'source_id', 'created_at', 'author_id', 'updated_at', 'updater_id','status'], 'integer'],
            [['isParent'], 'boolean'],
            [['laitovoSimple'], 'boolean'],
            [['comment'], 'string'],
            [['article', 'responsible_person'], 'string', 'max' => 255],
            [['article'], 'validateArticle'],
            [['source_id'], 'required'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sources::className(), 'targetAttribute' => ['source_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storage::className(), 'targetAttribute' => ['storage_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }

    /**
     * Изменная валидация поля артикул
     *
     * @param $attribute
     * @param $params
     */
    public function validateArticle($attribute, $params)
    {
        if (empty($this->article) && !$this->isParent)
            $this->addError($attribute, Yii::t('app', 'Необходимо указать артикул'));
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'responsible_person',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId() ? Yii::$app->user->identity->name: '';
                },
            ],
        ]);
    }

    public function afterFind()
    {
        $this->_packed = $this->packed;
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Sources::className(), ['id' => 'source_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageState()
    {
        return $this->hasOne(StorageState::className(), ['upn_id' => 'id']);
    }

    /**
     * [getName Получить человекопонятное наименование наряда]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-23
     * @anotherdate 2017-10-23T13:50:42+0300
     * @return      [string]                   [Строковое значение имени наряда]
     */
    public function getName()
    {
        return $this->id ? Yii::t('app','Наряд № ') . $this->id : '';
    }

    public function afterSave($insert, $changedAttributes)
    {
//        if ($this->_packed != $this->packed && $this->packed) {
//            if ($this->orderEntry) {
//                $order = $this->orderEntry->order;
//                if ($order) {
//                    $flag = true;
//                    $elements = $order->orderEntries;
//                    foreach ($elements as $element) {
//                        if (!$element->upn || !$element->upn->packed) {
//                            $flag = false;
//                            break;
//                        }
//                    }
//                    if ($flag) {
//                        $order->packed = 1;
//                        $order->save();
//                    }
//                }
//            }
//        }

        if (!$this->barcode) {
            $this->barcode = 'L' . $this->id;
            $this->save();
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * [getArticleBarcode Функция, согласно которой из артикула генерировать штрихкод для производства]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-26
     * @anotherdate 2017-10-26T10:13:01+0300
     * @param       [string]                   $article [Арткул, который нужно транслировать в barcode]
     * @return      [string]                            [Шрихкод продукта]
     */
    public function getArticleBarcode($article)
    {
        $value = explode('-', $this->article);
        switch (count($value)) {
            case 5:
            return $value[0] 
                   . str_pad($value[2], 4, "0", STR_PAD_LEFT)
                   . str_pad($value[3], 2, "0", STR_PAD_LEFT)
                   . $value[4];

            case 4:
            return $value[0] 
                   . str_pad($value[1], 4, "0", STR_PAD_LEFT)
                   . str_pad($value[2], 2, "0", STR_PAD_LEFT)
                   . $value[3];
            
            default:
                return '';
        }
    }

    /**
     * [getAricleCar Функция возвращает значение машины из артикула, если в нем оно зашито]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-10-26
     * @anotherdate 2017-10-26T10:16:33+0300
     * @param       [string]                   $article [Артикул]
     * @return      [string]                            [Артикул автомобиля]
     */
    public function getArticleCar($article)
    {
        $value = explode('-', $this->article);
        switch (count($value)) {
            case 5:
            return $value[2];
            
            default:
                return '';
        }
    }


    public function getCarName()
    {
        $value = explode('-', $this->article);
        if ($value[0] != 'OT') {
            $car =  Cars::find()->where(['article' => @$value[2]])->one();
                return $car ? $car->name : 'Общее';
        }
        return null;
    }

    public function getCar()
    {
        $value = explode('-', $this->article);
        if ($value[0] != 'OT') {
            $car =  Cars::find()->where(['article' => @$value[2]])->one();
                return $car ? $car : null;
        }
        return null;
    }

    public function getPlanLiteral()
    {
        $literal = 'Не определена!!!';

        //Статситику по артикулу
        $objArticle = new Article($this->article);

        //Получим планируемый баланс
        $plan = $objArticle->balancePlan;

        //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
        $storageType = ($plan && $plan>0) ? Storage::TYPE_REVERSE : Storage::TYPE_TIME;

        $productId = $objArticle->product ? $objArticle->product->id : null;
        //По Виду продукта определяем склад
        if ($productId) {
            $storage = Storage::findStorageForProduct($productId,$storageType);
            //Внутри склада определяем литеру
            $literal = $storage->getLiteral2($productId);
        }

        return $literal;
    }


    public function getFactLiteral()
    {
        $literal = 'Не распределено!!!';
        $state = StorageState::find()->where(['upn_id' => $this->id])->one();
        if ($state) {
            $literal = $state->literal . " | " .  Yii::$app->formatter->asDateTime($state->created_at);
        }

        return $literal;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(),['id' => 'pid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(self::className(),['pid' => 'id'])->orderBy('article');
    }


}
