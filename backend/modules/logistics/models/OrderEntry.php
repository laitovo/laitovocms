<?php

namespace backend\modules\logistics\models;

use core\models\box\Box;
use Yii;
use common\models\logistics\Order;
use common\models\User;
use backend\modules\logistics\models\StorageState;

/**
 * This is the model class for table "logistics_order_entry".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $title
 * @property string $article
 * @property integer $quantity
 * @property string $comment
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 * @property integer $boxId
 * @property integer $shipment_id
 * @property integer $setIndex [ Индекс комплекта в заказе ]
 *
 * @property User $author
 * @property Order $order
 * @property User $updater
 */
class OrderEntry extends \common\models\logistics\OrderEntry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'quantity', 'created_at', 'author_id', 'updated_at', 'updater_id','boxId','setIndex'], 'integer'],
            [['comment'], 'string'],
            ['withoutLabels', 'boolean'],
            ['halfStake', 'boolean'],
            [['title', 'article'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
            [['boxId'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['boxId' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getUpn()
//    {
//        return $this->hasOne(StorageState::className(), ['reserved_id' => 'id']);
//    }
}
