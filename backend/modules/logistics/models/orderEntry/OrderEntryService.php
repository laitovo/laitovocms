<?php

namespace backend\modules\logistics\models\orderEntry;

use backend\modules\logistics\models\OrderEntry;

/**
 * Данный сервис предоставляет доступ к свойствам элементов заказа
 *
 * Class OrderEntryService
 * @package backend\modules\logistics\models\orderEntry
 */
class OrderEntryService
{
    /**
     * Артикул
     *
     * @param OrderEntry $orderEntry Объект "Элемент заказа"
     * @return string|null
     */
    public function getArticle($orderEntry)
    {
        $this->_throwIfInvalidType($orderEntry);

        return $orderEntry->article ?? null;
    }

    /**
     * Наименование
     *
     * @param OrderEntry $orderEntry Объект "Элемент заказа"
     * @return string|null
     */
    public function getName($orderEntry)
    {
        $this->_throwIfInvalidType($orderEntry);

        return $orderEntry->name ?? null;
    }

    /**
     * Цена
     *
     * @param OrderEntry $orderEntry Объект "Элемент заказа"
     * @return double|null
     */
    public function getPrice($orderEntry)
    {
        $this->_throwIfInvalidType($orderEntry);

        return $orderEntry->price ?? null;
    }

    /**
     * Проверка на тип переданного элемента заказа
     *
     * @param $orderEntry
     * @throws \Exception
     */
    private function _throwIfInvalidType($orderEntry) {
        if (!($orderEntry instanceof OrderEntry)) {
            throw new \Exception('В сервис для работы со свойствами передан неверный класс элемента заказа');
        }
    }
}