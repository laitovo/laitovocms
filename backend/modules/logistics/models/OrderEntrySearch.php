<?php

namespace backend\modules\logistics\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\logistics\models\OrderEntry;

/**
 * OrderEntrySearch represents the model behind the search form about `backend\modules\logistics\models\OrderEntry`.
 */
class OrderEntrySearch extends OrderEntry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'quantity', 'created_at', 'author_id', 'updated_at', 'updater_id'], 'integer'],
            [['title', 'article', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderEntry::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'quantity' => $this->quantity,
            'created_at' => $this->created_at,
            'author_id' => $this->author_id,
            'updated_at' => $this->updated_at,
            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
