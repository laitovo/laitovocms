<?php

namespace backend\modules\laitovo\models;

use Yii;


/**
 * Модель Автозаказа. В ней будет вся логика автоматического заказа и автоматического распределения на склады.
 */
class AutomaticOrder extends Model
{
    /**
     * [$measuringInterval Интервал измерения, единица периода]
     * @var [integer]
     */
    public $measuringInterval;

    /**
     * [$unitOfPeriod Единица измерения периода, в секундах (можем выбрать дней, недель, часов, месяцев)]
     * @var [integer]
     */
    public $unitOfPeriod;

    /**
     * [$saleMonth Количество месяцев, в котором была совершена реализация]
     * @var [integer]
     */
    public $saleMonthCount;

    /**
     * [$salesRate Частота реализаций в рассматриваемый период. Не может превышать 1]
     * @var [double]
     */
    public $salesRate;

    /**
     * [$peakValues Пиковые значения реализаций]
     * @var [integer]
     */
    public $peakValues;

    /**
     * [$pealValueOnClient Пиковая реализация на одного клиента]
     * @var [integer]
     */
    public $pealValueOnClient;

    /**
     * [$amountOfReplenishment Размер пополняемого статка]
     * @var [integer]
     */
    public $amountOfReplenishment;

    /**
     * [$amountOfReplenishment Средний срок производства и доствки, единиц периода]
     * @var [double]
     */
    public $productionAndDeliveryPeriod;

    /**
     * [$minSaleInPeriod Минимальное количество реализаций в единицу периода]
     * @var [integer]
     */
    public $minSaleInPeriod;

    /**
     * [$revolvingStorageResidue Остаток на оборотном складе]
     * @var [double]
     */
    public $revolvingStorageResidue;

    /**
     * [$temporaryWarehouseResidue Остаток на временном складе]
     * @var [double]
     */
    public $temporaryWarehouseResidue;

    /**
     * [$revolvingStorages Массив идентификаторов оборотных складов, с которыми работает автозаказ]
     * @var array
     */
    public $revolvingStorages = [];

    /**
     * [$temporaryStorages Массив идентификаторов временных складов, с которыми работает автозаказ]
     * @var array
     */
    public $temporaryStorages = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //rules
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            //labels
        ];
    }

    //Распределение с диспетчер-склада
    public static function distribute()
    {
        # code...
    }

    //Отправка задания на пополнение
    public static function replenishment()
    {
        # code...
    }

    //Отправка задания на пополнение
    public static function distributeByUpn()
    {
        # code...
    }

    //Анализировать задания на пополнение
    public static function analysisByArticle()
    {
        # code...
    }
   
}
