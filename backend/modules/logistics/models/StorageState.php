<?php

namespace backend\modules\logistics\models;

use Yii;
use \common\models\User;
use \backend\modules\logistics\models\StorageLiteral;
use \backend\modules\logistics\models\Order;
use \backend\modules\logistics\models\OrderEntry;
use \backend\modules\logistics\models\Naryad;

/**
 * This is the model class for table "logistics_storage_state".
 *
 * @property integer $id
 * @property integer $storage_id
 * @property string $literal
 * @property integer $upn_id
 * @property integer $reserved
 * @property integer $order_id
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property LogisticsOrder $order
 * @property LogisticsStorage $storage
 * @property User $updater
 * @property LogisticsNaryad $upn
 */
class StorageState extends \common\models\logistics\StorageState
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storage_id', 'upn_id', 'reserved', 'reserved_id', 'created_at', 'author_id', 'updated_at', 'updater_id'], 'integer'],
            [['literal'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['reserved_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderEntry::className(), 'targetAttribute' => ['reserved_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storage::className(), 'targetAttribute' => ['storage_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
            [['upn_id'], 'exist', 'skipOnError' => true, 'targetClass' => Naryad::className(), 'targetAttribute' => ['upn_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderEntry()
    {
        return $this->hasOne(OrderEntry::className(), ['id' => 'reserved_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpn()
    {
        return $this->hasOne(Naryad::className(), ['id' => 'upn_id']);
    }

    public function getLiteralObj()
    {
        $literalObj = null;
        preg_match('/\w{2}\-(\w)\d/', $this->literal, $m);
        if (isset($m[1])) {
            $literalObj = StorageLiteral::find()->where(['storage_id'=> $this->storage_id])->andWhere(['title' => $m[1]])->one();
        }
        return $literalObj;

    }

    public function afterSave($insert, $changedAttributes)
    {
        //Надо написать функцию, которая после сохранения будет лезть в order и простовлять статус ready = true
//        $upn = $this->upn;
//        //Проверяем зарезервирован ли этот upn
//        if ($upn->orderEntry) {
//            $order = $upn->orderEntry->order;
//            if ($order) {
//                $flag = true;
//                $elements = $order->notDeletedOrderEntries;
//                foreach ($elements as $element) {
//                    if (!$element->upn || !$element->upn->storageState) {
//                        $flag = false;
//                        break;
//                    }
//                }
//                if ($flag) {
//                    $order->ready = true;
//                    $order->save();
//                }
//            }
//        }
        return parent::afterSave($insert, $changedAttributes);
    }
}
