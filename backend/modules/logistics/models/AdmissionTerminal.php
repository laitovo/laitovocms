<?php
namespace backend\modules\logistics\models;

use Yii;
use yii\base\Model;

/**
 * Терминал для оприходывания нарядов
 */
class AdmissionTerminal extends Model
{
    public $quantity;
    public $quantityEnter;
    public $article;
    public $naryad;
    public $upns = [];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //rules
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            //labels
        ];
    }

    public function __construct($config = [])
    {

        $this->quantity=Yii::$app->request->cookies->getValue('ADMISSION_TERMINAL_QUANTITY');
        $this->quantityEnter=Yii::$app->request->cookies->getValue('ADMISSION_TERMINAL_QUANTITY_ENTER');
        $this->article=Yii::$app->request->cookies->getValue('ADMISSION_TERMINAL_ARTICLE');
        $this->naryad=Yii::$app->request->cookies->getValue('ADMISSION_TERMINAL_NARYAD');
        $this->upns=Yii::$app->request->cookies->getValue('ADMISSION_TERMINAL_UPNS')?:[];

        parent::__construct($config);
    }

    public function saveProperties()
    {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ADMISSION_TERMINAL_QUANTITY',
            'value' => $this->quantity,
            'expire' => time() + 20,

        ]));
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ADMISSION_TERMINAL_QUANTITY_ENTER',
            'value' => $this->quantityEnter,
            'expire' => time() + 20,

        ]));
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ADMISSION_TERMINAL_ARTICLE',
            'value' => $this->article,
            'expire' => time() + 20,

        ]));
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ADMISSION_TERMINAL_NARYAD',
            'value' => $this->naryad,
            'expire' => time() + 20,

        ]));
    }

    public function saveList()
    {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ADMISSION_TERMINAL_UPNS',
            'value' => $this->upns,
            'expire' => time() + 20,
        ]));
    }

    public function deleteProperties()
    {
        $this->quantity = null;
        $this->quantityEnter = null;
        $this->article = null;
        $this->naryad = null;
        $this->saveProperties();
    }

    public function toLatin($str) 
    {
        $tr = array(
            "Й"=>"q","Ц"=>"w","У"=>"e","К"=>"r","Е"=>"t","Н"=>"y","Г"=>"u","Ш"=>"i","Щ"=>"o","З"=>"p","Ф"=>"a","Ы"=>"s","В"=>"d","А"=>"f","П"=>"g","Р"=>"h","О"=>"j","Л"=>"k","Д"=>"l","Я"=>"z","Ч"=>"x","С"=>"c","М"=>"v","И"=>"b","Т"=>"n","Ь"=>"m",
            "й"=>"q","ц"=>"w","у"=>"e","к"=>"r","е"=>"t","н"=>"y","г"=>"u","ш"=>"i","щ"=>"o","з"=>"p","ф"=>"a","ы"=>"s","в"=>"d","а"=>"f","п"=>"g","р"=>"h","о"=>"j","л"=>"k","д"=>"l","я"=>"z","ч"=>"x","с"=>"c","м"=>"v","и"=>"b","т"=>"n","ь"=>"m"
        );
        return strtr($str,$tr);
    }
}
