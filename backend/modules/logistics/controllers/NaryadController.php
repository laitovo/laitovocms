<?php

namespace backend\modules\logistics\controllers;

use backend\modules\logistics\models\StorageState;
use core\services\SEan;
use Yii;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\NaryadSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NaryadController implements the CRUD actions for Naryad model.
 */
class NaryadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Naryad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NaryadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Naryad model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('view', [
                'model' => $this->findModel($id),
            ]); 
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Naryad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Naryad();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return Yii::$app->response->redirect(['logistics/naryad/view', 'id' => $model->id],302,false);
            } else {
                return $this->renderPartial('create', [
                    'model' => $model,
                ]);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Naryad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->response->redirect(['logistics/naryad/view', 'id' => $model->id],302,false);
            } else {
                return $this->renderPartial('update', [
                    'model' => $model,
                ]);
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrintLabel($id)
    {
        $model = $this->findModel($id);

        return $this->renderPartial('label_parent',[
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrintFromStorageLabel($article)
    {
        $subQuery = Naryad::find()->where(['article' => $article])->column();
        $states = StorageState::find()->with('upn')->where(['in','upn_id',$subQuery])->all();
        $content = '';
        $count = count($states);
        foreach ($states as $state) {
            $content .= $this->renderPartial('label_upn',[
                'model' => $state->upn,
            ]);
            $count--;
            if ($count) $content .= "<p style='page-break-before: always;'>&nbsp;</p>";
        }
        return $content;
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrintArticleLabel($barcode)
    {

        return $this->renderPartial('label_barcode',[
            'barcode' => $barcode,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrintFullParentLabel($id)
    {
        $model = $this->findModel($id);
        if (!$model->isParent) return $this->renderContent('Этот UPN не комлпект!!!');

        $children = $model->children;
        $articles = [];
        foreach ($children as $child) {
            $articles[] = $child->article;
        }

        $ean = SEan::getEanForSet($articles);

        return $this->renderPartial('label_full_parent',[
            'model' => $model,
            'articles' => $articles,
            'ean' => $ean,
        ]);
    }




    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrintSingleLabel($id)
    {
        $model = $this->findModel($id);

        return $this->renderPartial('label_upn',[
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Naryad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->redirect(['logistics/naryad/index'],302,false);
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Naryad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Naryad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Naryad::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
