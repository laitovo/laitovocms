<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 04.01.18
 * Time: 10:01
 */
namespace backend\modules\logistics\controllers;

use Yii;
use backend\modules\logistics\models\report\StorageStateReport;
use yii\web\Controller;

/**
 * ReportController implements report actions.
 */
class ReportController extends Controller
{
    /**
     * Lists all StorageLiteral models.
     * @return mixed
     */
    public function actionStorageStateReport()
    {
        $searchModel = new StorageStateReport();
        $dataProvider = $searchModel->searchAsArray(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('_storage_state_report', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        if ($searchModel->upn_id && $dataProvider->totalCount) {
            return $this->redirect(['storage-state-intro-report', 'literal' => null, 'upn_id' => $searchModel->upn_id]);
        }

        return $this->render('_storage_state_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStorageStateIntroReport($literal = null, $upn_id = null)
    {
        $searchModel = new StorageStateReport();
        $searchModel->literal = $literal;
        $searchModel->upn_id = $upn_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('_storage_state_intro_report', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('_storage_state_intro_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    ####################################################################################################

    public function actionStorageStateReserved()
    {
        $searchModel = new StorageStateReport();
        $dataProvider = $searchModel->searchReserved(Yii::$app->request->queryParams);

        return $this->render('_storage_state_reserved', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStorageStateNotReserved()
    {
        $searchModel = new StorageStateReport();
        $dataProvider = $searchModel->searchNotReserved(Yii::$app->request->queryParams);

        return $this->render('_storage_state_not_reserved', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}