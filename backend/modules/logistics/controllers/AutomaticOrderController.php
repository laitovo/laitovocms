<?php

namespace backend\modules\logistics\controllers;

use Yii;
use backend\modules\logistics\models\AutomaticOrder;
use backend\modules\logistics\models\AutomaticOrderSearch;
use backend\modules\logistics\models\AutomaticOrderMonitor;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\StorageState;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpProductType;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AutomaticOrderController implements the CRUD actions for AutomaticOrder model.
 */
class AutomaticOrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AutomaticOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = AutomaticOrder::receive(Yii::$app->team->getId());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app','Новые настройки автозаказа успешно сохранены.'));
        }
    
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single AutomaticOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AutomaticOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AutomaticOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AutomaticOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AutomaticOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionMonitor()
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        $model = new AutomaticOrderMonitor();
        // var_dump($model->array);
        return $this->render('_monitor', [
            'realization'  => AutomaticOrderMonitor::getRealizationData(),
            'circulationBalance' => AutomaticOrderMonitor::getStockCirculationBalanceData(),
            'temporaryBalance'        => AutomaticOrderMonitor::getStockTemporaryBalanceData(),
            'upns'  => AutomaticOrderMonitor::getUpnsData(),
            'comingUpns'  => AutomaticOrderMonitor::getUpnsForComingData(),
            'fromOrder'  => AutomaticOrderMonitor::getUpnsFromOrder(),
            'fromDispatcher'  => AutomaticOrderMonitor::getUpnsFromDispatcher(),
            'fromStorage'  => $model->getFromStorage(),
            'groupsMain' =>$model->getStatisticByGroupsMain(),
            'groupsOther' =>$model->getStatisticByGroupsOther(),
            'model' =>$model,
        ]);
    }


    //Некоторое количество методов для теста работы автозаказа
    //
    public function actionAddArticle()
    {
        $article = new OrderEntry();
        $article->article = 'FD-A-768-1-2';
        $article->save();
        return $this->redirect(['monitor']);
    }

    public function actionAddInUpn()
    {
        $naryad = new Naryad();
        $naryad->article = 'FD-A-768-1-2';
        $naryad->team_id = Yii::$app->team->getId();
        $naryad->source_id = 1;
        $naryad->save();
        
        return $this->redirect(['monitor']);
    }

    public function actionAddUpn($article)
    {
        $naryad = new Naryad();
        $naryad->article = $article;
        $naryad->team_id = Yii::$app->team->getId();
        $naryad->source_id = 1;
        $naryad->save();
        // var_dump($naryad);
        // die(); 
        return $this->redirect(['monitor']);
    }

    public function actionAddReserv($article,$id)
    {
        $some = OrderEntry::findOne($id);
        if (!$some) {
            return $this->redirect(['monitor']);
        }

        if ($some->upn) {
            return $this->redirect(['monitor']);
        }

        $row = StorageState::find()->joinWith(['upn'])->where(['and',
            ['logistics_storage_state.storage_id' => 1],
            ['logistics_naryad.article' => $article],
            ['or',['logistics_storage_state.reserved' => null],['logistics_storage_state.reserved' => '']],
        ])->one();

        if ($row) {
            $row->reserved = 1;
            $row->reserved_id= $id;
            $row->save();
        }
        
        // var_dump($naryad);
        // die(); 
        return $this->redirect(['monitor']);
    }

    public function actionSendOborot($upn)
    {
        $row = new StorageState();
        $row->upn_id = $upn;
        $row->storage_id = 1;
        $row->save();
        // var_dump($naryad);
        // die(); 
        return $this->redirect(['monitor']);
    }

    public function actionSendProizv($id)
    {
        $narayd = ErpNaryad::find()->where(['logist_id' => $id])->one();
        if ($narayd) {
            $narayd->status = ErpNaryad::STATUS_READY;
            $narayd->literal = 1;
            $narayd->literal_date = time();
            $narayd->save();
        }
        // var_dump($naryad);
        // die(); 
        return $this->redirect(['monitor']);
    }


    public function actionSendTemporary($upn)
    {
        $row = new StorageState();
        $row->upn_id = $upn;
        $row->storage_id = 2;
        $row->save();
        // var_dump($naryad);
        // die(); 
        return $this->redirect(['monitor']);
    }

    public function actionSendRemove($upn)
    {
        $pos = StorageState::find()->where(['upn_id' => $upn])->one();
        $pos->storage_id = 1;
        $pos->save();
        // var_dump($naryad);
        // die(); 
        return $this->redirect(['monitor']);
    }


    /**
     * Finds the AutomaticOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AutomaticOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AutomaticOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * [actionGroupSpec Рендерит представление для распределения видов продукта на пополняемые и нет]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [1.0]
     * @date        2017-12-12
     * @anotherdate 2017-12-12T13:20:42+0300
     */
    public function actionGroupSpec()
    {
        if (Yii::$app->request->post('idYes')) {
            foreach (Yii::$app->request->post('idYes') as $value) {
                $product = ErpProductType::findOne($value);
                if ($product) {
                    $product->automatic = 1;
                    $product->save();
                }
            }
        }

        if (Yii::$app->request->post('idNo')) {
            foreach (Yii::$app->request->post('idNo') as $value) {
                $product = ErpProductType::findOne($value);
                if ($product) {
                    $product->automatic = 0;
                    $product->save();
                }
            }
        }

        $query = ErpProductType::find()->where('automatic = 1');

        $dataProviderYes = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);


        $query = ErpProductType::find()->where('automatic is null or automatic = 0');

        $dataProviderNo = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('_group_spec',[
            'dataProviderYes'=>$dataProviderYes,
            'dataProviderNo'=>$dataProviderNo,
        ]);
    }
}
