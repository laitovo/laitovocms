<?php

namespace backend\modules\logistics\controllers;

use core\logic\CheckStoragePosition;
use core\logic\UpnGroup;
use core\models\article\Article;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use backend\modules\laitovo\models\ErpTerminal;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\models\AdmissionTerminal;
use \common\models\laitovo\Cars;
use backend\modules\laitovo\models\ErpProductType;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `laitovo` module
 */
class AdmissionTerminalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('logistics');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $terminal = new AdmissionTerminal();
        $upns = $terminal->upns;

        $storagesArray = Storage::find()->where(['in','type',[Storage::TYPE_REVERSE,Storage::TYPE_TIME]])->all();
        $ids = ArrayHelper::map($storagesArray,'id','id');

        $dataProvider = new ActiveDataProvider([
            'query' => StorageState::find()->where(['in','storage_id',$ids])->orderBy(['created_at' => SORT_DESC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'upns' => $upns
        ]);
    }

    /**
     * Renders the sump-index view for the module
     * @return string
     */
    public function actionSumpIndex()
    {
        $terminal = new AdmissionTerminal();
        $upns = $terminal->upns;

        $storagesArray = Storage::find()->where(['in','type',[Storage::TYPE_SUMP]])->all();

        $ids = ArrayHelper::map($storagesArray,'id','id');

        $dataProvider = new ActiveDataProvider([
            'query' => StorageState::find()->where(['in','storage_id',$ids])->orderBy(['created_at' => SORT_DESC]),
        ]);

        return $this->render('_sump_index', [
            'dataProvider' => $dataProvider,
            'upns' => $upns
        ]);
    }

    /**
     * [actionSearch description]
     * Функция, которая обрабатывает поиск терминала по штрихкоду
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [version]
     * @date        2017-12-21
     * @anotherdate 2017-12-21T09:15:08+0300
     * @param       [type]                   $string [description]
     * @return      [type]                           [description]
     */
    public function actionSearch($barcode)
    {
        //Данное действие только для Ajax - запроса
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status']='error';
        $response['message']='Не найдено';

        //Ищем наряд на диспетчер склад
        $model = new AdmissionTerminal();
        $quantity = ctype_digit($barcode) ? (int)$barcode : 0;
        $barcode = $model->toLatin($barcode);
        $barcode = mb_strtoupper($barcode);


        $quantitativeBarcode = CheckStoragePosition::getQuantitativeBarcode();
        $quantitativeBarcode = mb_strtoupper($quantitativeBarcode);
        $quantityEnter = $barcode == $quantitativeBarcode ? true : false;

        if (mb_stripos($barcode,'-') !== false) {
            $response['status']='error';
            $response['message']='Нужно вводить штрихкод, а не сам артикул !!!';
            return $response;
        }

        $naryad=ErpNaryad::find()
            ->where(['barcode'   =>$barcode])
            ->andWhere(['distributed'=>0])
            ->andWhere(['or',['status'=>ErpNaryad::STATUS_READY],['status'=>ErpNaryad::STATUS_FROM_SKLAD]])
            ->one();


        $naryadOld=ErpNaryad::find()
            ->where(['barcode'   =>$barcode])
            ->andWhere(['distributed'=>1])
            ->andWhere(['or',['status'=>ErpNaryad::STATUS_READY],['status'=>ErpNaryad::STATUS_FROM_SKLAD]])
            ->one();


        //Так же мы можем передать штрихкод арткиула
        //Тогда нам надо определить является ли это штрихкодом модели
        //Определять будем по принципу первых двух букв и общей длины
        $value = [];
        $article = null;
        $window = mb_substr($barcode,0,2);
        $carArticle = ltrim(mb_substr($barcode,2,4),0);
        $cloth = ($res = mb_substr($barcode,6,2)) == '00' ? '0' : ltrim(mb_substr($barcode,6,2),0);
        $type = mb_substr($barcode,8);
        //Если в артикуле первые 2 символа соответсвуют следующим то это артикул
        if (in_array($window, ['FW','FD','FV','RD','RV','BW']) && mb_strlen($barcode) == 9) {
            $car =  Cars::find()->where(['article' => $carArticle])->one();
            if ($car) {
                $mark = $car->getTranslit(mb_substr($car->mark,0,1));
            }
            $value[] = $window;
            $value[] = $mark;
            $value[] = $carArticle;
            $value[] = $cloth;
            $value[] = $type;
            $article = implode('-',$value);
        }elseif(in_array($window, ['OT']) && mb_strlen($barcode) == 9) {

            $value[] = $window;
            $value[] = $carArticle;
            $value[] = $cloth;
            $value[] = $type;
            $article = implode('-',$value);
        }

        if ($article) {
            if (!$model->article || $article != $model->article || ($article == $model->article && $model->quantityEnter)) {
                $model->article = $article;
                $model->saveProperties();
                $response['status']='success';
                $response['message']='Вы желаете оприходовать по номеру артикула ' . $article . '?';
                return $response;
            }elseif ($article == $model->article && !$model->quantityEnter){
                $storage = null;
                $prod = null;

                //Статситику по артикулу
                $objArticle = new Article($article);

                //Получим планируемый баланс
                $plan = $objArticle->balancePlan;
                $fact = $objArticle->balanceFact;

                //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
                $storageType = ($plan && $plan>0 && $plan > $fact) || mb_substr($model->article,0,2) == 'OT' ? Storage::TYPE_REVERSE : Storage::TYPE_TIME;

                $productId = $objArticle->product ? $objArticle->product->id : null;

                if ($productId) {
                    $storage = Storage::findStorageForProduct($productId,$storageType);
                }

                if (!$storage) {
                    //Устанавливаем ответ по умолчанию
                    $response['status']='error';
                    $response['message']='Для данного продукта не определено место хранения!!!';
                    return $response;
                }

                //Внутри склада определяем литеру
                $literal = $storage->getActualLiteral($productId);

                //Содание UPN для наряда
                $logist = new Naryad();
                $logist->article = $article;
                $logist->team_id = Yii::$app->team->getId();
                $logist->source_id = 2;
                $logist->save();
                
                //Ставим, что данный наряд распределен с диспечер-склад
                $row = new StorageState();
                $row->upn_id = $logist->id;
                $row->storage_id = $storage->id;
                $row->literal = $literal;
                $row->save();
                
                $model->upns[] = $logist->id;
                $model->saveList();
                $model->deleteProperties();
                //Если наряд с таким штрихокодом найден, тогда мы создаем
                $response['status']='success';
                $response['message']="ЛИТЕРА : {$literal}";

                if (!CheckStoragePosition::isStorageArticle($article)) {
                    $response['etiketka']['model'] = $logist->id;
                    $response['etiketka']['literal'] = $literal;
                }

            }

        }elseif ($naryad) {
            if (!$model->naryad || $naryad->id != @$model->naryad) {
                $model->naryad = $naryad->id;
                $model->saveProperties();
                $response['status']='success';
                $response['message']='Вы желаете оприходовать по номеру наряда? ' . $naryad->name . '?';
                return $response;
            }elseif ($model->naryad== $naryad->id){
                $storage = null;
                $prod = null;

                //Статситику по артикулу
                $objArticle = new Article($naryad->article);

                //Получим планируемый баланс
                $plan = $objArticle->balancePlan;
                $fact = $objArticle->balanceFact;

                //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
                $storageType = ($plan && $plan>0 && $plan > $fact) || mb_substr($model->article,0,2) == 'OT' ? Storage::TYPE_REVERSE : Storage::TYPE_TIME;

                $productId = $objArticle->product ? $objArticle->product->id : null;

                if ($productId) {
                    $storage = Storage::findStorageForProduct($productId,$storageType);
                }

                if (!$storage) {
                    //Устанавливаем ответ по умолчанию
                    $response['status']='error';
                    $response['message']='Для данного продукта не определено место хранения!!!';
                    return $response;
                }

                //По артикулу определяем вид продукта
                if ($naryad->upn && ($state = UpnGroup::getStorageStateOfGroup($naryad->upn))) {
                    $storage = $state->storage;
                    $literal = $state->literal;
                }
                else {
                    $literal = $storage->getActualLiteral($productId);
                }

                //Содание UPN для наряда

                if (!$naryad->logist_id) {
                    $logist = new Naryad();
                    $logist->article = $naryad->article;
                    $logist->source_id = 2;
                    $logist->team_id = Yii::$app->team->getId();
                    $logist->save();
                    $naryad->logist_id = $logist->id;
                    $naryad->save();
                }
                
                //Ставим, что данный наряд распределен с диспечер-склад
                $row = new StorageState();
                $row->upn_id = $naryad->logist_id;
                $row->storage_id = $storage->id;
                $row->literal = $literal;
                $row->save();
                
                $naryad->distributed = 1;
                $naryad->save();

                $model->upns[] = $naryad->logist_id;
                $model->saveList();
                $model->deleteProperties();
                //Если наряд с таким штрихокодом найден, тогда мы создаем
                $response['status']='success';
                $response['message']="ЛИТЕРА : {$literal}";
                $response['etiketka']['model'] = $naryad->logist_id;
                $response['etiketka']['literal'] = $literal;

                //Мы должны наряду проставить галочку о том, что он был принят с производства на склад.
                //Куда проставим запись о том,  что он на складе ??
                //добавим новое поле.
                
                //И на складе сделать запись о том, что он там находится.
            }
        }elseif ($naryadOld) {
            $state = StorageState::find()->where(['upn_id' => $naryadOld->logist_id])->one();
            if ($state) {
                $response['message'] = "Данный UPN уже на остатках. Необходимо положить его на литеру!!!";
            }else{
                $response['message'] = "Данный наряд необходимо оприходовать по артикулу (Этикете на самих шторках)";
            }
            return $response;
        }elseif ($quantityEnter) {
            //Если в памяти хранится артикул
            if ($model->article && CheckStoragePosition::isStorageArticle($model->article) && !$model->quantityEnter) {
                $model->quantityEnter = true;
                $model->saveProperties();
                $response['status']='success';
                $response['message']='Введите количество позиций артикула '. $model->article .', которое хотите оприходовать на склад!';
                return $response;
            }elseif ($model->article && CheckStoragePosition::isStorageArticle($model->article) && $model->quantityEnter && $model->quantity) {
                $storage = null;
                $prod = null;
                $quantityVal = $model->quantity;
                $article = $model->article;
                //Статситику по артикулу
                $objArticle = new Article($article);

                //Получим планируемый баланс
                $plan = $objArticle->balancePlan;
                $fact = $objArticle->balanceFact;

                //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
                $storageType = ($plan && $plan>0 && $plan > $fact) || mb_substr($model->article,0,2) == 'OT' ? Storage::TYPE_REVERSE : Storage::TYPE_TIME;

                $productId = $objArticle->product ? $objArticle->product->id : null;

                if ($productId) {
                    $storage = Storage::findStorageForProduct($productId,$storageType);
                }

                if (!$storage) {
                    //Устанавливаем ответ по умолчанию
                    $response['status']='error';
                    $response['message']='Для данного продукта не определено место хранения!!!';
                    return $response;
                }

                for ($i = 1; $i <= $model->quantity; $i++) {
                    //Внутри склада определяем литеру
                    $literal = $storage->getActualLiteral($productId);

                    //Содание UPN для наряда
                    $logist = new Naryad();
                    $logist->article = $article;
                    $logist->team_id = Yii::$app->team->getId();
                    $logist->source_id = 2;
                    $logist->save();

                    //Ставим, что данный наряд распределен с диспечер-склад
                    $row = new StorageState();
                    $row->upn_id = $logist->id;
                    $row->storage_id = $storage->id;
                    $row->literal = $literal;
                    $row->save();

                    $model->upns[] = $logist->id;
                }

                $model->saveList();

                $response['status']='success';
                $response['message']='Вы успешно оприходовали артикул ' . $article . ', в количестве ' . $quantityVal. ' штук!';
                $model->deleteProperties();
                return $response;
            }

            //Если введен штрихкод для воода количества тогда мы сначала проверяем, а есть ли у модели
            $response['status']='success';
            $response['message']='Введен штрихкод для оприходывания по количеству.';
            return $response;
        }elseif ($quantity) {
            //Если в памяти хранится артикул
            if ($model->article && CheckStoragePosition::isStorageArticle($model->article) && $model->quantityEnter) {
                $model->quantity = $quantity;
                $model->saveProperties();
                $response['status']='success';
                $response['message']='Вы действительно желаете оприходовать артикул'. $model->article .', в количестве ' . $model->quantity . ' штук!';
                return $response;
            }
        }



        return $response;
        //Полученная строка - это штрих код
        //
        //После получения строки мы должны определить, а являеться ли это UPN
        //Если это действующий UPN, тогда проверяем а где он находится и не стоит ли у него галочка - отгружен а склад.
        //Если он на склад не отгружен, тогда мы ищем склад на который можно грузить данный UPN
        //Далее, мы получаем литеру, на которую мы будем грузить данный артикул
        //Делаем запись на складе, что на данный момент данный юпн находиться на таком то складе.
        //Отдаем литеру записи и они идут вешать на склад.
        //При этом отдаем на распечатку некий документ.
    }


    /**
     * [actionSearch description]
     * Функция, которая обрабатывает поиск терминала по штрихкоду
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [version]
     * @date        2017-12-21
     * @anotherdate 2017-12-21T09:15:08+0300
     * @param       [type]                   $string [description]
     * @return      [type]                           [description]
     */
    public function actionSumpSearch($barcode)
    {
        //Данное действие только для Ajax - запроса
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status']='error';
        $response['message']='Не найдено';

        //Ищем наряд на диспетчер склад
        $model = new AdmissionTerminal();
        $barcode = $model->toLatin($barcode);
        $barcode = mb_strtoupper($barcode);

        if (mb_stripos($barcode,'-') !== false) {
            $response['status']='error';
            $response['message']='Нужно вводить штрихкод, а не сам артикул !!!';
            return $response;
        }

        $value = [];
        $article = null;
        $window = mb_substr($barcode,0,2);
        $carArticle = ltrim(mb_substr($barcode,2,4),0);
        $cloth = ltrim(mb_substr($barcode,6,2),0);
        $type = mb_substr($barcode,8);
        //Если в артикуле первые 2 символа соответсвуют следующим то это артикул
        if (in_array($window, ['FW','FD','FV','RD','RV','BW'])) {
            $car =  Cars::find()->where(['article' => $carArticle])->one();
            if ($car) {
                $mark = $car->getTranslit(mb_substr($car->mark,0,1));
            }
            $value[] = $window;
            $value[] = $mark;
            $value[] = $carArticle;
            $value[] = $cloth;
            $value[] = $type;
            $article = implode('-',$value);
        }elseif(in_array($window, ['OT'])) {
            $value[] = $window;
            $value[] = $carArticle;
            $value[] = $cloth;
            $value[] = $type;
            $article = implode('-',$value);
        }

        if ($article) {
            if (!$model->article || $article != $model->article) {
                $objArticle = new Article($article);
//                if ($objArticle->isNeeded()) {
//                    $response['status']='danger';
//                    $response['message']='Данный артикул планируется к реализации и его невозможно отправить в отстойник!!!';
//                }else{
                    $response['status']='success';
                    $response['message']='Данный артикул можно отправить в отстойник! Вы действительно желаете это сделать ?!!!';
                    $model->article = $article;
                    $model->saveProperties();
//                }
                return $response;
            }elseif ($article == $model->article){
                $storage = null;
                $prod = null;

                //Статситику по артикулу
                $objArticle = new Article($article);

                //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
                $storageType = Storage::TYPE_SUMP;

                $productId = $objArticle->product ? $objArticle->product->id : null;

                if ($productId) {
                    $storage = Storage::findStorageForProduct($productId,$storageType);
                }

                if (!$storage) {
                    //Устанавливаем ответ по умолчанию
                    $response['status']='error';
                    $response['message']='Для данного продукта не определено место хранения!!!';
                    return $response;
                }

                //Внутри склада определяем литеру
                $literal = $storage->getActualLiteral($productId);

                //Содание UPN для наряда
                $logist = new Naryad();
                $logist->article = $article;
                $logist->source_id = 2;
                $logist->team_id = Yii::$app->team->getId();
                $logist->save();

                //Ставим, что данный наряд распределен с диспечер-склад
                $row = new StorageState();
                $row->upn_id = $logist->id;
                $row->storage_id = $storage->id;
                $row->literal = $literal;
                $row->save();

                $model->upns[] = $logist->id;
                $model->saveList();
                $model->deleteProperties();
                //Если наряд с таким штрихокодом найден, тогда мы создаем
                $response['status']='success';
                $response['message']="ЛИТЕРА : {$literal}";
                $response['etiketka']['model'] = $logist->id;
                $response['etiketka']['literal'] = $literal;
            }
        }




        return $response;
        //Полученная строка - это штрих код
        //
        //После получения строки мы должны определить, а являеться ли это UPN
        //Если это действующий UPN, тогда проверяем а где он находится и не стоит ли у него галочка - отгружен а склад.
        //Если он на склад не отгружен, тогда мы ищем склад на который можно грузить данный UPN
        //Далее, мы получаем литеру, на которую мы будем грузить данный артикул
        //Делаем запись на складе, что на данный момент данный юпн находиться на таком то складе.
        //Отдаем литеру записи и они идут вешать на склад.
        //При этом отдаем на распечатку некий документ.
    }

    public function actionPrintLabel($id,$literal)
    {
        $model = Naryad::findOne($id);

        return $this->renderPartial('print_label',[
            'model' => $model,
            'literal' => $literal,
        ]);
    }
}
