<?php

namespace backend\modules\logistics\controllers;

use Yii;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\OrderEntrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderEntryController implements the CRUD actions for OrderEntry model.
 */
class OrderEntryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderEntry models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new OrderEntrySearch();
        $searchModel->order_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        $order = Order::findOne($id);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'order' => $order,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'order' => $order,
        ]);
    }

    /**
     * Displays a single OrderEntry model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('view', [
                'model' => $this->findModel($id),
            ]); 
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrderEntry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new OrderEntry();
        $order = Order::findOne($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return Yii::$app->response->redirect(['logistics/order/view', 'id' => $model->id],302,false);
            } else {
                return $this->renderPartial('create', [
                    'model' => $model,
                    'order' => $order,
                ]);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'order' => $order,
            ]);
        }
    }

    /**
     * Updates an existing OrderEntry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $order = Order::findOne($model->order_id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->response->redirect(['logistics/order/view', 'id' => $model->id],302,false);
            } else {
                return $this->renderPartial('update', [
                    'model' => $model,
                    'order' => $order,
                ]);
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'order' => $order,
            ]);
        }
    }

    /**
     * Deletes an existing OrderEntry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->redirect(['logistics/order/index'],302,false);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrderEntry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderEntry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderEntry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
