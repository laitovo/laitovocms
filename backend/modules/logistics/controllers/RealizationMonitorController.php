<?php
namespace backend\modules\logistics\controllers;

use yii\web\Controller;

/**
 * Class RealizationMonitorController
 * @package backend\modules\logistics\controllers
 */
class RealizationMonitorController extends Controller
{
    /**
     * Index action, witch jast a render index file
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}