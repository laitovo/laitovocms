<?php

namespace backend\modules\logistics\controllers;

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\models\CarsForm;
use backend\modules\logistics\models\report\StorageReport;
use common\models\laitovo\Cars;
use yii\web\Controller;

class StorageReportController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new StorageReport();
        $provider = $searchModel->getProvider(\Yii::$app->request->get());


        return $this->render('index', [
            'searchModel' => $searchModel,
            'provider' => $provider,
        ]);
//        $dataProvider = $searchModel->searchAsArray(Yii::$app->request->queryParams);
    }

    public function actionView($id)
    {
        $searchModel = new StorageReport();
        $provider = $searchModel->getViewProvider($id);
        $car = Cars::findOne($id);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'provider' => $provider,
            'carName' => $car? $car->name :'Автомобиль неизвестен',
        ]);
//        $dataProvider = $searchModel->searchAsArray(Yii::$app->request->queryParams);
    }

    public function actionArticle($article)
    {
        $searchModel = new StorageReport();
        $provider = $searchModel->getArticleProvider($article);

        return $this->renderPartial('_article', [
            'searchModel' => $searchModel,
            'provider' => $provider,
            'article' => $article,
        ]);
//        $dataProvider = $searchModel->searchAsArray(Yii::$app->request->queryParams);
    }
}