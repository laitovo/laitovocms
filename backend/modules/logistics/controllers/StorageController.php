<?php

namespace backend\modules\logistics\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\laitovo\models\ErpProductType;
use yii\data\ActiveDataProvider;

/**
 * StorageController implements the CRUD actions for Storage model.
 */
class StorageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Storage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StorageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Storage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('idYes')) {
            $literals = Yii::$app->request->post('idYesLiteral');
            foreach (Yii::$app->request->post('idYes') as $value) {
                $product = ErpProductType::findOne($value);
                if ($product && isset($literals[$value]) && $literals[$value] !== '') {
                    $model->link('products',$product,['literal_id' => $literals[$value]]);
                }
            }
        }

        if (Yii::$app->request->post('idNo')) {
            foreach (Yii::$app->request->post('idNo') as $value) {
                $product = ErpProductType::findOne($value);
                if ($product) {
                    $model->unlink('products',$product,true);
                }
            }
        }

        $query = $model->getLiterals();

        $dataProviderLiterals = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $literals = ArrayHelper::merge([''=>''],ArrayHelper::map($model->getLiterals()->all(),'id','title'));

        $query = $model->getProducts();

        $dataProviderYes = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);


        $query = $model->getNoProducts();

        $dataProviderNo = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);


        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('view', [
                'model' => $model,
                'literals' => $literals,
                'dataProviderYes'=>$dataProviderYes,
                'dataProviderNo'=>$dataProviderNo,
                'dataProviderLiterals'=>$dataProviderLiterals,
            ]); 
        }

        return $this->render('view', [
            'model' => $model,
            'literals' => $literals,
            'dataProviderYes'=>$dataProviderYes,
            'dataProviderNo'=>$dataProviderNo,
            'dataProviderLiterals'=>$dataProviderLiterals,
        ]);


    }

    /**
     * Creates a new Storage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Storage();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return Yii::$app->response->redirect(['logistics/storage/view', 'id' => $model->id],302,false);
            } else {
                return $this->renderPartial('create', [
                    'model' => $model,
                ]);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Storage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->response->redirect(['logistics/storage/view', 'id' => $model->id],302,false);
            } else {
                return $this->renderPartial('update', [
                    'model' => $model,
                ]);
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Storage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->redirect(['logistics/storage/index'],302,false);
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Storage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Storage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Storage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
