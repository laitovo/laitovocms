<?php

namespace backend\modules\logistics\controllers;

use core\logic\CheckStoragePosition;
use core\models\storageProductType\StorageProductType;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Default controller for the `laitovo` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('logistics');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionPrintStorageBarcode()
    {
        $positions = StorageProductType::find()->select(['title','article','barcode'])->all();
        $barcode = CheckStoragePosition::getQuantitativeBarcode();
        return $this->renderPartial('print',[
            'positions' => $positions,
            'barcode' => $barcode,
        ]);
    }

    /**
     * Renders the index view for the module
     *
     * @param $article
     * @return string
     */
    public function actionPrintStorageArticle($article)
    {
        $position = StorageProductType::find()->where(['article' => $article])->one();
        if (!$position)
            return $this->renderContent('Не удалось найти продукт на складе, с артикулом : ' . $article);

        return $this->renderPartial('print_one',[
            'position' => $position,
        ]);
    }
}
