<?php

namespace backend\modules\logistics\modules\childMonitor\controllers;

use backend\modules\logistics\modules\childMonitor\models\Bind;
use backend\modules\logistics\modules\childMonitor\models\CreateShipment;
use backend\modules\logistics\modules\childMonitor\models\Index;
use backend\modules\logistics\modules\childMonitor\models\ViewOrder;
use yii\web\Controller;
use yii\base\Module;
use yii\web\NotFoundHttpException;
use Yii;

class DefaultController extends Controller
{
    private $_index;
    private $_createShipment;
    private $_bind;
    private $_viewOrder;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_index          = new Index();
        $this->_createShipment = new CreateShipment();
        $this->_bind           = new Bind();
        $this->_viewOrder      = new ViewOrder();

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $storageList[] = (object)[
            'name' => 'Hamburg',
            'value' => 1
        ];
        $storageList[] = (object)[
            'name' => 'Moscow',
            'value' => 2
        ];

        $search         = $this->_updateSession('childMonitor_index_search', 'update_childMonitor_index_search');
        $autoRefresh    = $this->_updateSession('childMonitor_index_autoRefresh', 'update_childMonitor_index_autoRefresh', true);
        $onlyReady      = $this->_updateSession('childMonitor_index_onlyReady', 'update_childMonitor_index_onlyReady', false);
        $currentStorage = $this->_updateSession('childMonitor_index_storage', 'update_childMonitor_storage');
        $provider       = $this->_index->getProvider([
            'search'  => $search,
            'storage' => $currentStorage,
            'onlyReady' => $onlyReady
        ]);

        return $this->render('index', [
            'provider'    => $provider,
            'search'      => $search,
            'autoRefresh' => $autoRefresh,
            'onlyReady'   => $onlyReady,
            'storageList' => $storageList,
            'currentStorage' => $currentStorage?:0
        ]);
    }

    public function actionViewOrder($orderId)
    {
        if (!($order = $this->_viewOrder->findOrder($orderId))) {
            throw new NotFoundHttpException('Заказ не найден');
        }

        return $this->render('view-order', [
            'order' => $order,
            'orderItemsProvider' => $this->_viewOrder->getOrderEntriesProvider($orderId),
        ]);
    }

    public function actionCreateShipment($orderId, $nextPage)
    {
        $nextPage = ($nextPage == 'fill') ? 'fill-shipment' : 'view-shipment';
        if (!($order = $this->_createShipment->findOrder($orderId))) {
            throw new NotFoundHttpException('Заказ не найден');
        }
        if ($shipment = $this->_createShipment->findShipmentByOrder($order)) {
            return $this->redirect([$nextPage, 'shipmentId' => $this->_createShipment->getShipmentId($shipment)]);
        }
        if (!($shipment = $this->_createShipment->createShipmentByOrder($order))) {
            Yii::$app->session->setFlash('error', $this->_createShipment->getCreateShipmentError());
            return $this->redirect(['index']);
        } else {
            return $this->redirect([$nextPage, 'shipmentId' => $this->_createShipment->getShipmentId($shipment)]);
        }
    }

    public function actionViewShipment($shipmentId)
    {
        return $this->redirect(['/logistics/shipment-monitor/default/view', 'id' => $shipmentId]);
    }

    public function actionFillShipment($shipmentId)
    {
        if (!($shipment = $this->_bind->findShipment($shipmentId))) {
            throw new NotFoundHttpException('Отгрузка не найдена');
        }
        $search = $this->_updateSession('childMonitor_fillShipment_search', 'update_childMonitor_fillShipment_search');
        $currentStorage = $this->_updateSession('childMonitor_index_storage', 'update_childMonitor_storage');
        $onlyReady      = $this->_updateSession('childMonitor_index_onlyReady', 'update_childMonitor_index_onlyReady');
        if (!Yii::$app->request->isAjax) {
            $this->_bind->addBindings([
                'shipmentId' => $shipmentId,
                'idsYesNew'  => Yii::$app->request->post('idsYesNew'),
            ]);
        }

        return $this->render('fill-shipment/index', [
            'shipmentId'  => $shipmentId,
            'providerYes' => $this->_bind->getProviderYes($shipmentId),
            'providerNo'  => $this->_bind->getProviderNo($shipmentId, $currentStorage, $onlyReady, $search),
            'search'      => $search,
            'onlyReady'   => $onlyReady
        ]);
    }

    public function actionReduceShipment($shipmentId)
    {
        if (!($shipment = $this->_bind->findShipment($shipmentId))) {
            throw new NotFoundHttpException('Отгрузка не найдена');
        }
        if (!Yii::$app->request->isAjax) {
            $this->_bind->removeBindings([
                'shipmentId'    => $shipmentId,
                'idsNoNew'      => Yii::$app->request->post('idsNoNew'),
            ]);
        }

        return $this->render('reduce-shipment/index', [
            'shipmentId'  => $shipmentId,
            'providerYes' => $this->_bind->getProviderYes($shipmentId),
            'providerNo'  => $this->_bind->getProviderNoNew($shipmentId),
        ]);
    }

    public function actionSaveBindings($shipmentId)
    {
        if (!($shipment = $this->_bind->findShipment($shipmentId))) {
            throw new NotFoundHttpException('Отгрузка не найдена');
        }
        if (!$this->_bind->saveBindings([
            'shipmentId' => $shipmentId,
        ])) {
            Yii::$app->session->setFlash('error', $this->_bind->getSavingError());
            return $this->redirect(['index']);
        }

        return $this->redirect(['/logistics/shipment-monitor/default/view', 'id' => $shipmentId]);
    }

    private function _updateSession($name, $pageFlag, $defaultValue = false)
    {
        $session      = Yii::$app->session;
        $remember     = Yii::$app->request->get($pageFlag) ?: Yii::$app->request->post($pageFlag);
        $sessionValue = !$remember ? $session->get($name, $defaultValue) : (Yii::$app->request->get($name, false) ?: Yii::$app->request->post($name, false));
        $session->set($name, $sessionValue);

        return $sessionValue;
    }
}