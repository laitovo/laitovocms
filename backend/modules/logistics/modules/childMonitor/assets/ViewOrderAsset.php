<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\modules\logistics\modules\childMonitor\assets;

use yii\web\AssetBundle;

class ViewOrderAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/logistics/modules/childMonitor/assets/sources';

    public $css = [
        'css/view-order.css',
    ];
    public $js = [
        'js/view-order.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
//    public $publishOptions = [
//        'forceCopy'=>true,
//    ];
}
