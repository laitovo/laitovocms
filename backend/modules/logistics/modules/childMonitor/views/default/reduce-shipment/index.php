<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var yii\web\View $this */
/* @var int|null $shipmentId */
/* @var $providerYes yii\data\ArrayDataProvider */
/* @var $providerNo yii\data\ArrayDataProvider */

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Разъединить');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Монитор логистики'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    .checkbox-custom label::before {
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
</style>

<?= Html::a('<i class="icon wb-check"></i> ' . Yii::t('app', $shipmentId ? 'Сохранить' : 'Создать приказ'), ['save-bindings', 'shipmentId' => $shipmentId], ['class' => 'btn btn-primary', 'id' => 'save-bindings-button']) ?>

<?php $form = ActiveForm::begin(['id' => 'bind-form']); ?>
    <div id="bind-form-content-block" class="hide"></div>
<?php ActiveForm::end(); ?>
<hr>
<div class="row">
    <div class="col-md-6">
        <?= Html::button ('Убрать выбранные', [
            'class' => 'btn btn-danger pull-right',
            'id' => 'remove-bindings-button',
            'disabled' => true
        ]) ?>
        <h4>Позиции в приказе на отгрузку</h4>
        <? Pjax::begin(['id' => 'pjax-yes', 'enablePushState' => false]); ?>
        <?= ListView::widget([
            'dataProvider' => $providerYes,
            'itemView' => '_order-yes',
            'id' => 'table-yes',
        ]) ?>
        <? Pjax::end(); ?>
    </div>
    <div class="col-md-6">
        <h4>Позиции, готовые к отгрузке</h4>
        <? Pjax::begin(['id' => 'pjax-no', 'enablePushState' => false]); ?>
        <?= ListView::widget([
            'dataProvider' => $providerNo,
            'itemView' => '_order-no',
            'id' => 'table-no',
        ]) ?>
        <? Pjax::end(); ?>
    </div>
</div>

<?php Yii::$app->view->registerJs('
    $("#remove-bindings-button").click(function() {
        fillBindForm();
        $("#bind-form").submit();
    });
    $("#pjax-yes").on("pjax:end", function() {
        $("#pjax-yes [data-toggle=tooltip]").tooltip();
        showBindButton();
    });
    $("#pjax-no").on("pjax:end", function() {
        $("#pjax-no [data-toggle=tooltip]").tooltip();
    });
    $("body").on("change","#table-yes .checkbox-select-order",function(e){
        showBindButton();
    });
    function showBindButton() {
        if ($("#table-yes .checkbox-select-order:checked").length > 0) {
            $("#remove-bindings-button").prop("disabled", false);
        } else {
            $("#remove-bindings-button").prop("disabled", true);
        }
    }
    function fillBindForm() {
        $("#bind-form-content-block").html("");
        $("#table-yes .checkbox-select-order-entry:checked").each(function(){
            $("#bind-form-content-block").append($(this).clone());
        });
    }
', \yii\web\View::POS_END);
?>
