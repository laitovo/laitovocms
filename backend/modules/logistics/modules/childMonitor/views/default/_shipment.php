<?php
use yii\helpers\Html;
/* @var $model array */
?>

<style>
    .bg-handled * {
        background-color: lightgrey;
    }
    .bg-ready * {
        background-color: rgb(70, 190, 138);
        color:white;
    }
    .bg-handled a *, .bg-handled .tooltip, .bg-ready a *, .bg-ready .tooltip {
        background-color: transparent;
    }
    .bg-handled .tooltip-inner, .bg-ready .tooltip-inner {
        background-color: #32434D;
    }
</style>

<hr>
<div style="font-size: 0.84em" class="<?=$model['isAfterHandling'] ? 'bg-handled' : ($model['areAllPositionsReadyToPack'] ? 'bg-ready' : '')?>">
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center text-nowrap">№ Отгрузки</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <?php if ($model['shipmentId']): ?>
                        <?=$model['shipmentId']?>
                    <?php else:?>
                        <em class="text-danger text-nowrap">(не задано)</em>
                    <?php endif;?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center text-nowrap">Статус</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <?php if ($model['status']): ?>
                        <?=$model['status']?>
                    <?php else:?>
                        <em class="text-danger text-nowrap">(не задано)</em>
                    <?php endif;?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-9">

    <?foreach ($model['orders'] as $order):?>
        <div class="col-md-4">
            <div class="col-md-5 text-center">
                <table class="table">
                    <thead>
                    <th class="text-center">№ Заказа</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div onclick = '$("#order-<?=@$order['orderId']?>").slideToggle( "slow", function() {});'>
                                <?= @$order['sourceId'] ? str_replace((mb_substr(@$order['sourceId'], 4, mb_strlen(@$order['sourceId']))),
                                    ('<b style="font-size: 1.4em;font-weight: bold; color: red;">' . mb_substr(@$order['sourceId'], 4, mb_strlen(@$order['sourceId'])) . '</b>'), @$order['sourceId']) : @@$order['sourceId'] ?>
                                <br>
                                <span class="text-nowrap">от <?= date('d.m.Y',$order['createdAt']) ?></span>
                                <?php if ($order['isNeedTtn']): ?>
                                    <strong class="text-nowrap" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Обязательно приложить ТТН к заказу')?>"><?=Yii::t('app', '(с ТТН)')?></strong>
                                <?php endif;?>
                                <?php if ($order['isCOD']): ?>
                                    <strong class="text-nowrap text-danger" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Оплата заказа производится наложенным платежем')?>"><?=Yii::t('app', '(Наложка)')?></strong>
                                <?php endif;?>
                            </div>
                            <?= Html::a('<i class="fa fa-eye"></i>', ['view-order', 'orderId' => $order['orderId']], [
                                'target' => '_blank',
                                'class' => 'btn btn-xs btn-icon btn-outline btn-round ' . ($model['areAllPositionsReadyToPack'] ? 'btn-default' : 'btn-primary'),
                                'style' => 'margin-top:3px;',
                                'data-pjax' => '',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => Yii::t('app', 'Просмотр заказа')]); ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-4 text-center">
                <table class="table">
                    <thead>
                    <th class="text-center">Клиент</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td style=""><?= $order['client']?> <?= $order['exportClientName'] ? '(' . $order['exportClientName'] .  ')' : ''?></td>
                    </tr>
                    <tr>
                        <td><?= $order['category']?> <?= $order['exportCategory'] ? '(' . $order['exportCategory'] .  ')' : ''?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3 text-center">
                <table class="table">
                    <thead>
                    <th class="text-center">Доставка</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?= $order['delivery']?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-8">
            <div class="col-md-12" style="display: none" id="order-<?=@$order['orderId']?>">
                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <thead>
                            <th colspan="2">Общая информация</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Заказ №</td>
                                <td><?= $order['sourceId']?></td>
                            </tr>
                            <tr>
                                <td>Менеджер&nbsp;:</td>
                                <td><?= $order['manager']?></td>
                            </tr>
                            <tr>
                                <td>Адрес доставки&nbsp;:</td>
                                <td><?= $order['address']?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <thead>
                            <th colspan="2">Информация о клиенте</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td>ФИО&nbsp;:</td>
                                <td><?= $order['client']?></td>
                            </tr>
                            <tr>
                                <td>Телефон&nbsp;:</td>
                                <td><?= $order['phone']?></td>
                            </tr>
                            <tr>
                                <td>Email&nbsp;:</td>
                                <td><?= $order['email']?></td>
                            </tr>
                            <tr>
                                <td>Категория&nbsp;:</td>
                                <td><?= $order['category']?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-hover table-default">
                    <thead>
                    <th>Автомобиль</th>
                    <th>Артикул</th>
                    <th>Наименование</th>
                    <th>UPN</th>
                    <th>Местонахождение</th>
                    </thead>
                    <tbody>
                    <?foreach ($order['elements'] as $element):?>
                        <tr class="<?= (!$model['isAfterHandling'] && @$element['readyToPack']) ? 'success' : ''?>">
                            <td><?=$element['car']?></td>
                            <td style="white-space:nowrap"><?=$element['article']?></td>
                            <td><?=$element['title']?></td>
                            <td class="text-nowrap">
                                <?=$element['upn']?>
                            </td>
                            <td><?=$element['location']?></td>
                        </tr>
                    <?endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
    <?endforeach;?>

    </div>
    <div class="col-md-1 text-center text-nowrap">
        <?php if ($model['isAfterHandling']): ?>
            <?= Html::a('<i class="fa fa-eye"></i>', ['view-shipment', 'shipmentId' => $model['shipmentId']], [
                'class' => 'btn btn-xs btn-icon btn-outline btn-round btn-primary',
                'style' => 'margin-top:5px;margin-left:2px;',
                'data-toggle' => "tooltip",
                'data-original-title' => Yii::t('app', 'Просмотр отгрузки')
            ]); ?>
        <?php else:?>
            <?php if ($model['shipmentId']): ?>
                <?= Html::a('<i class="fa fa-check"></i>', ['view-shipment', 'shipmentId' => $model['shipmentId']], [
                    'class' => 'btn btn-xs btn-icon btn-outline btn-round ' . ($model['areAllPositionsReadyToPack'] ? 'btn-default' : 'btn-primary'),
                    'style' => 'margin-top:5px;margin-left:2px;',
                    'data-toggle' => "tooltip",
                    'data-original-title' => Yii::t('app', 'Закрыть отгрузку')
                ]); ?>
                <?= Html::a('<i class="fa fa-object-group"></i>', ['fill-shipment', 'shipmentId' => $model['shipmentId']], [
                    'class' => 'btn btn-xs btn-icon btn-outline btn-round ' . ($model['areAllPositionsReadyToPack'] ? 'btn-default' : 'btn-primary'),
                    'style' => 'margin-top:5px;margin-left:2px;',
                    'data-toggle' => "tooltip",
                    'data-original-title' => Yii::t('app', 'Объединить')
                ]); ?>
                <?php if (count($model['orders']) > 1): ?>
                    <?= Html::a('<i class="fa fa-object-ungroup"></i>', ['reduce-shipment', 'shipmentId' => $model['shipmentId']], [
                        'class' => 'btn btn-xs btn-icon btn-outline btn-round ' . ($model['areAllPositionsReadyToPack'] ? 'btn-default' : 'btn-primary'),
                        'style' => 'margin-top:5px;margin-left:2px;',
                        'data-toggle' => "tooltip",
                        'data-original-title' => Yii::t('app', 'Разъединить')
                    ]); ?>
                <?php endif;?>
            <?php else:?>
                <?= Html::a('<i class="fa fa-check"></i>', ['create-shipment', 'orderId' => $model['orders'][0]['orderId'], 'nextPage' => 'view'], [
                    'class' => 'btn btn-xs btn-icon btn-outline btn-round ' . ($model['areAllPositionsReadyToPack'] ? 'btn-default' : 'btn-primary'),
                    'style' => 'margin-top:5px;margin-left:2px;',
                    'data-toggle' => "tooltip", 
                    'data-original-title' => Yii::t('app', 'Закрыть отгрузку')
                ]); ?>
                <?= Html::a('<i class="fa fa-object-group"></i>', ['create-shipment', 'orderId' => $model['orders'][0]['orderId'], 'nextPage' => 'fill'], [
                    'class' => 'btn btn-xs btn-icon btn-outline btn-round ' . ($model['areAllPositionsReadyToPack'] ? 'btn-default' : 'btn-primary'),
                    'style' => 'margin-top:5px;margin-left:2px;',
                    'data-toggle' => "tooltip", 
                    'data-original-title' => Yii::t('app', 'Объединить')
                ]); ?>
            <?php endif;?>
        <?php endif;?>
    </div>
    <div class="clearfix"></div>
</div>
