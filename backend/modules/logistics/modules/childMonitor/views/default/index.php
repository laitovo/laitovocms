<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $provider yii\data\ArrayDataProvider */
/* @var $search string */
/* @var $autoRefresh bool */
/* @var $storageList array */
/* @var $currentStorage integer */
/* @var $onlyReady bool */

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Монитор логистики');

$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    .checkbox-custom label::before {
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
</style>

<?php Pjax::begin(['id' => 'pjax-shipments-list','timeout' => 5000, 'enablePushState' => false]); ?>

<?php ActiveForm::begin([
    'id'      => 'search-form',
    'action'  => [''],
    'method'  => 'get',
    'options' => [
        'data-pjax' => true
    ]
]); ?>

<div class="row">
    <div class="form-group col-md-2">
        <?= Html::tag('div',
            Html::checkbox("childMonitor_index_autoRefresh", $autoRefresh, ['id' => 'checkbox-auto-refresh'])
            .Html::tag('label','Автообновление', ['for' => 'checkbox-auto-refresh']),
            [
                'class' => 'checkbox-custom checkbox-primary text-left',
                'onchange' => '$("#search-form").submit();'
            ]
        ) ?>
        <?= Html::hiddenInput('update_childMonitor_index_autoRefresh',true); ?>
        <?= Html::button(Yii::t('app', 'Обновить'), [
            'id' => 'button-refresh',
            'class' => 'btn btn-outline btn-round btn-primary',
            'onclick' => '$.pjax.reload({container : "#pjax-shipments-list"});'
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::input($type = 'text','childMonitor_index_search',$value = $search, $options = ['class' => 'form-control', 'id' => 'search-textbox', 'placeholder' => 'Поиск']); ?>
        <?= Html::hiddenInput('update_childMonitor_index_search',true); ?>
    </div>
    <div class="col-md-2">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::button('Очистить', ['class' => 'btn btn-sm btn-default', 'onclick' => '
            $("#search-textbox").val("");
            $("#search-form").submit();
        ']) ?>
    </div>
    <div class="col-md-2">
        <?= Html::tag('div',
            Html::checkbox("childMonitor_index_onlyReady", $onlyReady, ['id' => 'checkbox-auto-only-ready'])
            .Html::tag('label','Только готовые', ['for' => 'checkbox-auto-only-ready']),
            [
                'class' => 'checkbox-custom checkbox-primary text-left',
                'onchange' => '$("#search-form").submit();'
            ]
        ) ?>
        <?= Html::hiddenInput('update_childMonitor_index_onlyReady',true); ?>
    </div>
    <div class="col-md-2">
        <?= Html::hiddenInput('update_childMonitor_storage',true); ?>
        <?= Html::hiddenInput('childMonitor_index_storage',$currentStorage,['id' => 'storage-id']); ?>
        <?foreach ($storageList as $storage):?>
            <?= Html::button($storage->name, ['class' => 'btn btn-sm ' . ($storage->value == $currentStorage ? 'btn-success' : 'btn-default'), 'onclick' => '
                $("#storage-id").val(' . $storage->value .');
                $("#search-form").submit();
            ']) ?>
        <?endforeach;?>
    </div>

</div>

<?php ActiveForm::end(); ?>

<?= ListView::widget([
    'dataProvider' => $provider,
    'itemView' => '_shipment',
]) ?>

<?php Pjax::end(); ?>

<?php Yii::$app->view->registerJs('
    setInterval(function() {
        if ($("#checkbox-auto-refresh").prop("checked")) {
            $.pjax.reload({container : "#pjax-shipments-list"});
        }
    }, 360*1000);

    $("#pjax-shipments-list").on("pjax:end", function() {
        $("#pjax-shipments-list [data-toggle=tooltip]").tooltip();
    })
', \yii\web\View::POS_END);
?>