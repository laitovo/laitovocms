<?php

namespace backend\modules\logistics\modules\childMonitor\models;

use yii\data\ArrayDataProvider;
use Yii;

class Index
{
    private $_orderService;
    private $_orderEntryService;
    private $_upnService;
    private $_shipmentManager;

    public function __construct()
    {
        $this->_orderService      = new OrderService();
        $this->_orderEntryService = new OrderEntryService();
        $this->_upnService        = new UpnService();
        $this->_shipmentManager   = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function getProvider($params)
    {
        $storage = $params['storage']??null;
        $onlyReady = $params['onlyReady']??false;

        $models = $this->_findOrders($storage);
        $models = $this->_prepareOrders($models);
        $models = $this->_sortOrders($models);
        $models = $this->_groupOrders($models);
        if ($search = $params['search']) {
            $models = $this->_search($search, $models);
        }
        if ($onlyReady) {
            $models = $this->_onlyReady($models);
        }

        return $this->_asProvider($models);
    }

    private function _findOrders($storage)
    {
        return $this->_orderService->findOrdersForLogist($storage);
    }

    private function _prepareOrders($orders)
    {
        $data = [];
        foreach ($orders as $order) {
            $data[] = $this->_prepareOrder($order);
        }

        return $data;
    }

    private function _prepareOrderEntries($orderEntries)
    {
        $data = [];
        foreach ($orderEntries as $orderEntry) {
            $data[] = $this->_prepareOrderEntry($orderEntry);
        }

        return $data;
    }

    private function _prepareOrder($order)
    {
        $row = [];

        $orderId                    = $this->_orderService->getId($order);
        $manager                    = $this->_orderService->getManager($order);
        $delivery                   = $this->_orderService->getDelivery($order);
        $address                    = $this->_orderService->getAddress($order);
        $createdAt                  = $this->_orderService->getCreatedAt($order);
        $clientName                 = $this->_orderService->getClientName($order);
        $clientPhone                = $this->_orderService->getClientPhone($order);
        $clientEmail                = $this->_orderService->getClientEmail($order);
        $clientCategory             = $this->_orderService->getClientCategory($order);
        $orderSourceId              = $this->_orderService->getSourceNumber($order);
        $isNeedTtn                  = $this->_orderService->isNeedTtn($order);
        $isCOD                      = $this->_orderService->isCOD($order);
        $shipment                   = $this->_orderService->getShipment($order);
        $elements                   = $this->_prepareOrderEntries($this->_orderService->getOrderEntriesNotDeleted($order));
        $areAllPositionsReadyToPack = $this->_areAllPositionsReadyToPack($elements);

        $exportClientName           = $this->_orderService->getExportClientName($order);
        $exportCategory             = $this->_orderService->getExportClientCategory($order);

        $row['orderId']                    = $orderId;
        $row['manager']                    = $manager;
        $row['delivery']                   = $delivery;
        $row['address']                    = $address;
        $row['createdAt']                  = $createdAt;
        $row['client']                     = $clientName;
        $row['phone']                      = $clientPhone;
        $row['email']                      = $clientEmail;
        $row['category']                   = $clientCategory;
        $row['sourceId']                   = $orderSourceId;
        $row['isNeedTtn']                  = $isNeedTtn;
        $row['isCOD']                      = $isCOD;
        $row['shipment']                   = $shipment;
        $row['elements']                   = $elements;
        $row['areAllPositionsReadyToPack'] = $areAllPositionsReadyToPack;
        $row['exportClientName']           = $exportClientName;
        $row['exportCategory']             = $exportCategory;

        return $row;
    }

    private function _prepareOrderEntry($orderEntry)
    {
        $row = [];

        $article     = $this->_orderEntryService->getArticle($orderEntry);
        $title       = $this->_orderEntryService->getTitle($orderEntry);
        $car         = $this->_orderEntryService->getCar($orderEntry);
        $shipmentId  = $this->_orderEntryService->getShipmentId($orderEntry);
        $upn         = $this->_orderEntryService->getReservedUpn($orderEntry);
        $upnId       = !$upn ? null : $this->_upnService->getId($upn);
        $location    = !$upn ? null : $this->_upnService->getLocation($upn);
        $readyToPack = !$upn ? null : $this->_upnService->isReadyToPackage($upn);

        $row['article']      = $article;
        $row['title']        = $title;
        $row['car']          = $car;
        $row['shipmentId']   = $shipmentId;
        $row['upn']          = $upnId;
        $row['location']     = $location;
        $row['readyToPack']  = $readyToPack;

        return $row;
    }

    /**
     * Сортировка заказов.
     * Cначала спускаем вниз обработанные.
     * Затем сортируем по степени готовности к упаковке на складе.
     *
     * @param array $orders
     * @return array
     */
    private function _sortOrders($orders)
    {
        uasort($orders, function ($a, $b) {
            $shipment1 = $a['shipment'];
            $shipment2 = $b['shipment'];
            $isAfterHandling1 = $shipment1 && !$this->_shipmentManager->isBeforeHandling($shipment1);
            $isAfterHandling2 = $shipment2 && !$this->_shipmentManager->isBeforeHandling($shipment2);
            $count1 = $this->_getReadyToPackElementsCount($a);
            $count2 = $this->_getReadyToPackElementsCount($b);
            $count1no = count($a['elements']) - $count1;
            $count2no = count($b['elements']) - $count2;
            switch (true) {
                case !$isAfterHandling1 && $isAfterHandling2:
                    return -1;
                case $isAfterHandling1 && !$isAfterHandling2:
                    return 1;
                case !$count1 && !$count2:
                    return 0;
                case $count1 && !$count2:
                    return -1;
                case !$count1 && $count2:
                    return 1;
                case $count1no < $count2no:
                    return -1;
                case $count1no > $count2no:
                    return 1;
                default:
                    return 0;
            }
        });

        return $orders;
    }

    /**
     * Группируем приказы по отгрузкам
     *
     * @param array $orders
     * @return array
     */
    private function _groupOrders($orders)
    {
        $result = [];
        $key = 0;
        foreach ($orders as $order) {
            $shipmentId = !$order['shipment'] ? null : $this->_shipmentManager->getId($order['shipment']);
            if (!$shipmentId) {
                $key++;
                $result['empty' . $key] = [
                    'shipmentId' => null,
                    'status'     => null,
                    'isAfterHandling'  => null,
                    'areAllPositionsReadyToPack'  => $order['areAllPositionsReadyToPack'],
                    'orders'     => [$order]
                ];
                continue;
            }
            if (!isset($result[$shipmentId])) {
                $shipment = $order['shipment'];
                $result[$shipmentId]['shipmentId'] = $shipmentId;
                $result[$shipmentId]['status'] = $this->_shipmentManager->getLogisticsStatusLabel($shipment);
                $result[$shipmentId]['isAfterHandling'] = !$this->_shipmentManager->isBeforeHandling($shipment);
                $result[$shipmentId]['areAllPositionsReadyToPack'] = true;
            }
            if (!$order['areAllPositionsReadyToPack']) {
                $result[$shipmentId]['areAllPositionsReadyToPack'] = false;
            }
            $result[$shipmentId]['orders'][] = $order;
        }

        return $result;
    }

    /**
     * @param string $search
     * @param array $models
     * @return array
     */
    private function _search($search, $models)
    {
        $searchPattern = '/' . preg_quote(mb_strtolower($search)) . '/';
        $result = [];
        foreach ($models as $shipmentId => $shipment) {
            $parts   = [];
            $parts[] = $shipment['shipmentId'] ?: '(не задано)';
            $parts[] = $shipment['status'] ?: '(не задано)';
            foreach ($shipment['orders'] as $order) {
                $parts[] = $order['manager'];
                $parts[] = $order['delivery'];
                $parts[] = $order['address'];
                $parts[] = date('d.m.Y', $order['createdAt']);
                $parts[] = $order['client'];
                $parts[] = $order['phone'];
                $parts[] = $order['email'];
                $parts[] = $order['category'];
                $parts[] = $order['sourceId'];
                if ($order['isNeedTtn']) {
                    $parts[] = '(С ТТН)';
                }
                if ($order['isCOD']) {
                    $parts[] = '(наложка)';
                }
            }
            if (!preg_match($searchPattern, mb_strtolower(implode('|', $parts)))) {
                continue;
            }
            $result[$shipmentId] = $shipment;
        }

        return $result;
    }

    /**
     * Только полностью готовые наряды
     *
     * @param $orders
     * @return array
     */
    private function _onlyReady($orders)
    {
        $data = [];

        foreach ($orders as $order) {
            if ($order['areAllPositionsReadyToPack']) {
                $data[] = $order;
            }
        }

        return $data;
    }

    private function _getReadyToPackElementsCount($orders)
    {
        $count = 0;
        foreach ($orders['elements'] as $element) {
            if ($element['readyToPack']) {
                $count++;
            }
        }

        return $count;
    }

    private function _areAllPositionsReadyToPack($elements)
    {
        foreach ($elements as $element) {
            if (!$element['readyToPack']) {
                return false;
            }
        }

        return true;
    }

    private function _asProvider($models, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $models,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}


