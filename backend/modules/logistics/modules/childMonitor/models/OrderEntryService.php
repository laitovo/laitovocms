<?php

namespace backend\modules\logistics\modules\childMonitor\models;

use common\models\logistics\OrderEntry;
use Exception;
use Yii;
use common\models\User;

class OrderEntryService
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function getId($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->id;
    }

    public function getArticle($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->article;
    }

    public function getTitle($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->name;
    }

    public function getCar($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->car ? $orderEntry->car : '';
    }

    public function getReservedUpn($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->upn;
    }

    public function getShipmentId($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->shipment_id;
    }

    public function getName($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->name;
    }

    public function getPrice($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->price;
    }

    public function getIsDeleted($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->is_deleted;
    }

    public function getDeletedAt($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return $orderEntry->deleted_at;
    }

    public function getShipment($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        return !$orderEntry->shipment_id ? null : $this->_shipmentManager->findById($orderEntry->shipment_id);
    }

    public function getDestroyer($orderEntry)
    {
        return User::findOne($orderEntry->deleted_by);
    }

    public function saveShipmentId($orderEntry, $shipmentId)
    {
        $this->_instanceOf($orderEntry);
        $orderEntry->shipment_id = $shipmentId;

        return $orderEntry->save();
    }

    private function _instanceOf($orderEntry)
    {
        if (!($orderEntry instanceof OrderEntry)) {
            throw new Exception('Переданное значение должно быть элементом логистической заявки');
        }
    }
}


