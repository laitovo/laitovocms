<?php

namespace backend\modules\logistics\modules\childMonitor\models;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Storage;
use common\models\logistics\Naryad;
use Exception;
use Yii;

class UpnService
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager    = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function getId($upn)
    {
        $this->_instanceOf($upn);

        return $upn->id;
    }

    public function isReadyToPackage($upn)
    {
        $this->_instanceOf($upn);

        return (($state = $upn->storageState) != null && ($storage =$state->storage) != null && ($storage->type != Storage::TYPE_SUMP));
    }

    public function getLocation($upn)
    {
        $this->_instanceOf($upn);

        if ( ($orderEntry = $upn->orderEntry) && ($shipmentId = $orderEntry->shipment_id) && ($shipment = $this->_shipmentManager->findById($shipmentId)) ) {
            if ($this->_shipmentManager->isHandled($shipment)) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipmentId]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipmentId .' ]. Ожидается упаковка : ' . date('d.m.Y H:i:s', $this->_shipmentManager->getHandledAt($shipment)) . '</a>';
            } elseif($this->_shipmentManager->isPacked($shipment)) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipmentId]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipmentId .' ]. Упакован, ожидается отгрузка : ' . date('d.m.Y H:i:s', $this->_shipmentManager->getPackedAt($shipment)) . '</a>';
            } elseif($this->_shipmentManager->isShipped($shipment)) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipmentId]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipmentId .' ]. Отгружен : ' . date('d.m.Y H:i:s', $this->_shipmentManager->getShippedAt($shipment)) . '</a>';
            }
        }

//        $state = StorageState::find()->where(['upn_id' => $upn->id])->one();
//        $naryad = ErpNaryad::find()->where(['logist_id' => $upn->id])->one();
        $state = $upn->storageState;
        $naryad = $upn->erpNaryad;

        //Если текущий upn находится на складе - тогда пишим склад
        if ($state) {
            if ($state->storage) {
                $reservedAt = max(@$naryad->distributed_date, @$upn->reserved_at);
                $reservedAt = $reservedAt ? date('d.m.Y H:i:s', $reservedAt) : null;
                $literal = $state->literal;
                $str =  ($literal && $reservedAt) ? ($literal . ' : ' . $reservedAt) : $literal;
                if ($state->storage->type == Storage::TYPE_SUMP) {
                    return $str ." <br> Отстойник. Ожидает проверки ОТК";
                }
                return $str;
            } else {
                return 'Склад';
            }
        }
        //Если теккущий upn находиться на производстве - пишем локацию
        if ($naryad)
        {
            if ($naryad->status == ErpNaryad::STATUS_READY) return 'Диспетчер-Склад : ' . date('d.m.Y H:i:s', $naryad->updated_at);
            return @$naryad->location && !$naryad->locationstart ? @$naryad->location->name : 'Диспетчер : '.@$naryad->locationstart->name;
        }

        return 'Ожидется обработка';
    }

    private function _instanceOf($element)
    {
        if (!($element instanceof Naryad)) {
            throw new Exception('Переданное значение должно быть элементом логистического наряда');
        }
    }
}


