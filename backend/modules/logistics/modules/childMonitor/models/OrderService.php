<?php

namespace backend\modules\logistics\modules\childMonitor\models;

use backend\modules\logistics\models\Order;
use common\models\logistics\OrderEntry;
use core\entities\logisticsShipment\models\Shipment;
use Exception;
use Yii;

class OrderService
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }
    
    /**
     * @param int $id
     * @return Order|null
     */
    public function findById($id)
    {
        return Order::findOne($id);
    }

    /**
     * Заказы для логиста (заказы, которые ожидают оформления заявки в транспортную компанию)
     *
     * @param $storage
     * @return array|\yii\db\ActiveRecord[]
     */
    public function findOrdersForLogist($storage)
    {
        if (!$storage) return [];

        $shipmentIds = $this->_shipmentManager->findWhereColumn('id', ['or',
            ['logisticsStatus' => 'shipped'],
            ['logisticsStatus' => 'packed'],
        ]);

        $orderIds = OrderEntry::find()
            ->select('order_id')
            ->where(['and',
                ['is_deleted' => null],
                ['not', ['order_id' => null]],
                ['or',
                    ['not in', 'shipment_id', $shipmentIds],
                    ['shipment_id' => null]
                ],
            ])
            ->groupBy('order_id')
            ->column();

        $query = Order::find()
            ->with('notDeletedOrderEntries.upn.orderEntry')
            ->with('notDeletedOrderEntries.upn.storageState.storage')
            ->with('notDeletedOrderEntries.upn.erpNaryad.location')
            ->with('notDeletedOrderEntries.upn.erpNaryad.locationstart')
            ->where(['and',
                ['team_id' => Yii::$app->team->id],
                ['processed' => true],
                ['shipped' => null],
                ['in', 'id', $orderIds],
                ['remoteStorage' => $storage],
            ])
            ->orderBy('created_at');

        return $query->all();
    }


    /**
     * Заказы для логиста (заказы, которые ожидают оформления заявки в транспортную компанию)
     *
     * @param $currentStorage
     * @return array|\yii\db\ActiveRecord[]
     */
    public function findOrdersForBind($currentStorage)
    {
        if (!$currentStorage) return [];
        $shipmentIds = $this->_shipmentManager->findWhereColumn('id', ['or',
            ['logisticsStatus' => 'shipped'],
            ['logisticsStatus' => 'packed'],
        ]);

        $orderIds = OrderEntry::find()
            ->select('order_id')
            ->where(['and',
                ['is_deleted' => null],
                ['not', ['order_id' => null]],
                ['or',
                    ['not in', 'shipment_id', $shipmentIds],
                    ['shipment_id' => null]
                ],
            ])
            ->groupBy('order_id')
            ->column();

        $query = Order::find()
            ->with('notDeletedOrderEntries.upn.orderEntry')
            ->with('notDeletedOrderEntries.upn.storageState.storage')
            ->with('notDeletedOrderEntries.upn.erpNaryad.location')
            ->with('notDeletedOrderEntries.upn.erpNaryad.locationstart')
            ->where(['and',
                ['team_id' => Yii::$app->team->id],
                ['processed' => true],
                ['shipped' => null],
                ['in', 'id', $orderIds],
                ['remoteStorage' => $currentStorage]
            ])
            ->orderBy('created_at');

        return $query->all();
    }

    public function getId($order)
    {
        $this->_instanceOf($order);

        return $order->id;
    }

    public function getManager($order)
    {
        $this->_instanceOf($order);

        return $order->manager;
    }

    public function getDelivery($order)
    {
        $this->_instanceOf($order);

        return $order->delivery;
    }

    public function getAddress($order)
    {
        $this->_instanceOf($order);

        return $order->address;
    }

    public function getInnerComment($order)
    {
        $this->_instanceOf($order);

        return $order->comment;
    }

    public function getCreatedAt($order)
    {
        $this->_instanceOf($order);

        return $order->created_at;
    }

    public function getClientName($order)
    {
        $this->_instanceOf($order);

        return $order->username;
    }

    public function getExportClientName($order)
    {
        $this->_instanceOf($order);

        return $order->export_username;
    }

    public function getClientPhone($order)
    {
        $this->_instanceOf($order);

        return $order->userphone;
    }

    public function getClientEmail($order)
    {
        $this->_instanceOf($order);

        return $order->useremail;
    }

    public function getClientCategory($order)
    {
        $this->_instanceOf($order);

        return $order->category;
    }

    public function getExportClientCategory($order)
    {
        $this->_instanceOf($order);

        return $order->export_type;
    }

    public function getSourceNumber($order)
    {
        $this->_instanceOf($order);

        return $order->source_innumber;
    }

    public function getTransportCompanyName($order)
    {
        $this->_instanceOf($order);

        return $order->delivery;
    }

    public function isNeedTtn($order)
    {
        $this->_instanceOf($order);

        return $order->is_need_ttn;
    }

    public function isCOD($order)
    {
        $this->_instanceOf($order);

        return $order->isCOD;
    }

    public function getOrderEntries($order)
    {
        $this->_instanceOf($order);

        return $order->orderEntries;
    }

    public function getOrderEntriesNotDeleted($order)
    {
        $this->_instanceOf($order);

        return $order->notDeletedOrderEntries;
    }

    /**
     * Статус оплаты (не оплачено, оплачено, наложка)
     *
     * @param Order $order Объект "Заказ"
     * @return string|null
     */
    public function getPaymentStatus($order)
    {
        $this->_instanceOf($order);

        return $order->paymentStatuses[$order->payment_status] ?? null;
    }

    /**
     * Тип доставки (платная, бесплатная)
     *
     * @param Order $order Объект "Заказ"
     * @return null
     */
    public function getDeliveryType($order)
    {
        $this->_instanceOf($order);

        return $order->deliveryTypes[$order->delivery_type] ?? null;
    }

    /**
     * Статус доставки
     *
     * @param Order $order Объект "Заказ"
     * @return mixed
     */
    public function getDeliveryStatus($order)
    {
        $this->_instanceOf($order);

        return $order->deliveryStatuses[$order->delivery_status] ?? null;
    }

    /**
     * Сумма доставки
     *
     * @param Order $order Объект "Заказ"
     * @return double
     */
    public function getDeliveryPrice($order)
    {
        $this->_instanceOf($order);

        return $order->delivery_price;
    }

    /**
     * Рассчет суммы заказа
     * При расчете суммируются цены отдельных позиций данного заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return int Сумма заказа
     */
    public function getItemsPrice($order)
    {
        $this->_instanceOf($order);
        $totalPrice = 0;
        foreach ($order->notDeletedOrderEntries as $orderItem) {
            $totalPrice += $orderItem->price;
        }

        return $totalPrice;
    }

    /**
     * Общая сумма, полученная за весь заказ наложенным платежом.
     * Наложенный платеж - это платеж, который совершается после получения товара, а не до него.
     *
     * @param $order
     * @return double
     */
    public function getCodTotalPrice($order)
    {
        $this->_instanceOf($order);

        return $this->getCodItemsPrice($order) + $this->getCodDeliveryPrice($order);
    }

    /**
     * Общая сумма, полученная за товар наложенным платежом.
     *
     * @param $order
     * @return double
     */
    public function getCodItemsPrice($order)
    {
        $this->_instanceOf($order);
        if ($order->payment_status == Order::PAYMENT_STATUS_COD) {
            return $this->getItemsPrice($order);
        }

        return 0;
    }

    /**
     * Общая сумма, полученная за доставку наложенным платежом.
     *
     * @param $order
     * @return double
     */
    public function getCodDeliveryPrice($order)
    {
        $this->_instanceOf($order);
        if ($order->delivery_status != Order::DELIVERY_STATUS_COD) {
            return 0;
        }
        if (empty($order->delivery_price)) {
            return 0;
        }

        return $order->delivery_price;
    }

    /**
     * Отгрузка для заказа определяется как первая попавшаяся отгрузка, содержащая позиции из данного заказа
     *
     * @param Order $order
     * @return Shipment|null
     */
    public function getShipment($order)
    {
        $this->_instanceOf($order);

        foreach ($order->notDeletedOrderEntries as $orderEntry) {
            if ($orderEntry->shipment_id && ($shipment = $this->_shipmentManager->findById($orderEntry->shipment_id))) {
                return $shipment;
            }
        }

        return null;
    }

    private function _instanceOf($orderEntry)
    {
        if (!($orderEntry instanceof Order)) {
            throw new Exception('Переданное значение должно быть элементом логистической заявки');
        }
    }
}


