<?php

use yii\helpers\Html;
use core\models\shipment\Shipment;
use yii\widgets\ActiveForm;

$this->render('@backendViews/logistics/views/menu');
$this->title = Yii::t('app', 'Замер параметров отгрузки');

/**
* @var $model Shipment
 */

?>
<?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться в монитор склада'), ['index'], ['class' => 'btn btn-info']) ?>
<hr>
<div class="erp-location-workplace-form">
    <?php $form = ActiveForm::begin(); ?>

    <? if ($model->hasSizing) {
        echo $form->field($model, 'size')->textInput(['placeholder' => 'Укажите габариты отгрузки']);
    }?>

    <? if ($model->hasWeighing) {
        echo $form->field($model, 'weight')->textInput(['type' => 'number', 'min' => 0, 'placeholder' => 'Укажите вес отгрузки, кг']);
    }?>

    <div class="form-group">
        <?= Html::submitButton( 'Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= $this->render('_shipment_info', ['model' => $model]); ?>

