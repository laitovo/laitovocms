<?php

use core\models\box\Box;
use yii\helpers\Url;
use yii\helpers\Html;
use core\models\shipment\Shipment;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\modules\warehouseMonitor\models\ElementData;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/**
 * @var $model Shipment
 * @var $order Order
 */

$script = <<<SCRIPT
//    setInterval(function() {
//        if ($("#checkbox-auto-refresh").prop("checked")) {
//            $.pjax.reload({container : "#myPjax", timeout: false});
//        }
//    }, 360*1000);
//    
    $("#myPjax").on("pjax:end", function() {
      $("#myPjax [data-toggle=tooltip]").tooltip();
    })

    $('#myModal').on('shown.bs.modal', function () {
      $('#barcode-scaner').focus();
    });
    
    $('#myModal').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#myPjax', timeout: false});
    });
//    
//    $('#myModal3').on('hidden.bs.modal', function () {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    });
//    
//    $('#myModal3').on('shown.bs.modal', function () {
//      $('#barcode-scaner-2').focus();
//    });
//    
    $('#modal-ship-one').on('shown.bs.modal', function () {
      $('#modal-ship-one .barcode-scaner').focus();
    });
    
    $('#modal-ship-one').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#myPjax', timeout: false});
    });
//    
//    $('#modal-update-weight').on('hidden.bs.modal', function () {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    });
//    
//    $('#modal-update-size').on('hidden.bs.modal', function () {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    });
//    
//    $("#modal-rework-shipment").on("pjax:end", function() {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    })
//    
//    $("#modal-danger").on('hidden.bs.modal', function() {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    })
SCRIPT;
$this->registerJs($script);

?>

<?php Pjax::begin(['id' => 'myPjax', 'enablePushState' => false]); ?>

    <div style="font-size: 0.84em">
        <div class="shipment">
            <div class="col-md-2 text-center">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center text-nowrap">№ Отгрузки</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href=" <?= Yii::$app->urlManager->createUrl(['/logistics/shipment-monitor/default/view','id'=>$model->id])?>" target="_blank" data-pjax="0"><?= $model->id ?></a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 text-center">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center text-nowrap">Транспортная компания</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?= $model->transportCompany ?  $model->transportCompany->name : 'Не указана'?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 text-center">
                <table class="table">
                    <thead>
                    <th class="text-center">Документы</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= Html::button('<span class="fa fa-file" style="font-size:2em;" title="' . Yii::t('app', 'Печать документов логистики') . '" data-toggle="tooltip" data-placement="top"></span>', [
                                'class' => 'btn btn-sm btn-inverse',
                                'data' => [
                                    'shipment-id' => $model->id,
                                ],
                                'onclick' => '
                                var shipmentId = $(this).data("shipment-id");
                                $.ajax({
                                    url: "/logistics/implementation-monitor/default/ajax-get-shipment-documents",
                                    dataType: "json",
                                    data: {
                                        "shipmentId": shipmentId
                                    },
                                    success: function (data) {
                                        fillDocumentsTable(data.documents);
                                        $("#modal-print-documents").modal("show");
                                    },
                                    error: function() {
                                        notie.alert(3, "Не удалось загрузить документы", 3);
                                    }
                                });
                            ',
                            ]);?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 text-center">
                <table class="table">
                    <thead>
                    <th class="text-center">Реестр</th>
                    </thead>
                    <tbody>
                    <tr class="lead">
                        <td class="text-center">
                            <button class="btn btn-sm btn-inverse" title="<?= Yii::t('app', 'Распечатать реестр') ?>" data-toggle="tooltip" data-placement="top" onclick="printUrl('<?= Url::to(["/logistics/implementation-monitor/default/registry-shipment-print-by-box"]) . '?id=' . $model->id ?>');">
                                <span style="font-size:2em;color:blueviolet;" class="glyphicon glyphicon-print"></span>
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 text-center">
                <table class="table">
                    <thead>
                    <th class="text-center">Отгрузка</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?= $model->shipmentDate ? Yii::$app->formatter->asDate($model->shipmentDate, 'dd.MM.yyyy') : null ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 text-center">
                <table class="table">
                    <thead>
                    <th class="text-center">Обработка</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?php if($model->isHandled()): ?>
                                <span style="font-size: 3.2em" class="glyphicon glyphicon-remove text-warning"></span>
                            <?php elseif($model->isShipped()): ?>
                                <span>Приказ отгружен</span>
                            <?php elseif($model->isOnStorageRequest()): ?>
                                <span style="font-size: 3.2em" class="glyphicon glyphicon-remove text-warning"></span>
                            <?php elseif($model->isPacked()): ?>
                                <?= Html::button('<span class="fa fa-truck" style="font-size:2em;" title="' . Yii::t('app', 'Отгрузить приказ') . '" data-toggle="tooltip" data-placement="top"></span>', [
                                    'class' => 'btn btn-sm btn-inverse ship-button',
                                    'data' => [
                                        'shipment-id' => $model->id,
                                    ],
                                    'onclick' => '
                                    $("#modal-ship-one .barcode-scaner").data("shipment-id", $(this).data("shipment-id"));
                                    $("#modal-ship-one .shipment-number").html($(this).data("shipment-id"));
                                    $("#modal-ship-one").modal("show");
                                ',
                                ]);?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
        <? foreach ($model->boxes as $key => $box):?>
            <div class="box">
                <div class="col-md-1 text-center">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="text-center text-nowrap">№ Коробки</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?= $box->id ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-8 text-center">
                    <?foreach ($box->orders as $order):?>
                        <div class="col-md-2 text-center">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center text-nowrap">№ Заказа</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td onclick = '
                                            $("#shipment-<?=@$model->id?>-order-<?=@$order->id?>").slideToggle( "slow", function() {});
                                            $(".shipment-<?=@$model->id?>-order-<?=@$order->id?>-name-hide").fadeToggle( "slow", function() {});
                                            '>
                                        <?= @$order->source_innumber ? str_replace((mb_substr(@$order->source_innumber, 4, mb_strlen(@$order->source_innumber))),
                                            ('<b style="font-size: 1.4em;font-weight: bold; color: red;">' . mb_substr(@$order->source_innumber, 4, mb_strlen(@$order->source_innumber)) . '</b>'), @$order->source_innumber) : @@$order->source_innumber ?>
                                        <?php if ($order->is_need_ttn): ?>
                                            <strong class="text-nowrap text-warning" style="font-size:2em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Обязательно приложить ТТН к заказу')?>"><?=Yii::t('app', '(с ТТН)')?></strong>
                                        <?php endif;?>
                                        <?php if ($order->isCOD): ?>
                                            <strong class="text-nowrap text-danger" style="font-size:2em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Оплата заказа производится наложенным платежем')?>"><?=Yii::t('app', '(Наложка)')?></strong>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?php if ($order->ozon_posting_number): ?>
                                    <tr>
                                        <td>
                                            <button class="btn btn-sm btn-info" title="<?= Yii::t('app', 'Этикетка OZON') ?>" data-toggle="tooltip" data-placement="top" onclick="printUrl('<?= Url::to(["print-ozon", 'postingNumber' => $order->ozon_posting_number, 'clientId' => $order->laitovo_user_id]) ?>');">
                                                <span style="font-size:2em;color:white;" class="glyphicon glyphicon-print"></span> <span style="font-size:2em;color:white;">Ozon</span>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-2 text-center">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">Клиент</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td style=""><?= $order->username?> <?= $order->export_username ? '(' . $order->export_username .  ')' : ''?></td>
                                </tr>
                                <tr>
                                    <td><?= $order->category?> <?= $order->export_type ? '(' . $order->export_type .  ')' : ''?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-8 text-center">
                            <div class="row" style="display:none;" id="shipment-<?=@$model->id?>-order-<?=@$order->id?>">
                                <div class="col-md-6 text-center">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th colspan="2">Общая информация</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Заказ №</td>
                                            <td><?= $order->source_innumber?></td>
                                        </tr>
                                        <tr>
                                            <td>Менеджер&nbsp;:</td>
                                            <td><?= $order->manager?></td>
                                        </tr>
                                        <tr>
                                            <td>Способ доставки&nbsp;:</td>
                                            <td><?= $order->delivery?></td>
                                        </tr>
                                        <tr>
                                            <td>Адрес доставки&nbsp;:</td>
                                            <td><?= $order->address?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6 text-center">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th colspan="2">Информация о клиенте</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>ФИО&nbsp;:</td>
                                            <td><?= $order->username?></td>
                                        </tr>
                                        <tr>
                                            <td>Телефон&nbsp;:</td>
                                            <td><?= $order->userphone?></td>
                                        </tr>
                                        <tr>
                                            <td>Email&nbsp;:</td>
                                            <td><?= $order->useremail?></td>
                                        </tr>
                                        <tr>
                                            <td>Категория&nbsp;:</td>
                                            <td><?= $order->category?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <table class="table table-hover table-default">
                                        <thead>
                                        <th>Автомобиль</th>
                                        <th>Артикул</th>
                                        <th class="shipment-<?=@$model->id?>-order-<?=@$order->id?>-name-hide" style="display:none;">Наименование</th>
                                        <th>UPN</th>
                                        <th>Местонахождение</th>
                                        </thead>
                                        <tbody>
                                        <?foreach (ElementData::create($order,$box) as $parentIndex => $elements):?>
                                            <?if ($parentIndex):?>
                                                <tr>
                                                    <td colspan="4">
                                                        <b style="font-size: 1.5em">Комлпект № <?=$parentIndex?></b>
                                                        <button class="btn btn-sm btn-inverse" title="<?= Yii::t('app', 'Распечатать этикетку для склада европы') ?>" data-toggle="tooltip" data-placement="top" onclick="printUrl('<?= Url::to(['/logistics/naryad/print-full-parent-label','id' => $parentIndex])?>');">
                                                            <span style="font-size:2em;color:blue;" class="glyphicon glyphicon-print"></span>
                                                        </button>
                                                    </td>
                                                </tr>
                                            <?endif;?>
                                            <?foreach ($elements as $element):?>
                                                <?php $flag = @$element->readyToPack && @$element->packed; ?>
                                                <tr class="<?= $flag ? 'success' : ''?>">
                                                    <td><?=$element->car?></td>
                                                    <td style="white-space:nowrap">
                                                        <?=$element->article?>
                                                        <?php
//                                                        if ($element['storage']) {
//                                                            $attention = $element['hasBalanceControl'] && $element['minBalance'] > $element['factBalance'];
//                                                            echo Html::tag('span', '(' . $element['factBalance'] . ')', [
//                                                                'class' => !$attention ? '' : 'text-danger',
//                                                                'style' => !$attention ? '' : 'font-size:1.3em;font-weight:bold;',
//                                                                'data-toggle' => 'tooltip',
//                                                                'data-title'  => Yii::t('app', !$attention ? 'Фактический остаток' : 'Фактический остаток (Внимание! Меньше минимального!)')
//                                                            ]);
//                                                        }
//                                                        ?>
                                                    </td>
                                                    <td class="shipment-<?=@$model->id?>-order-<?=@$order->id?>-name-hide" style="display: none"><?=$element->name?></td>
                                                    <td>
                                                        <?= @$element['upn'] ? str_replace((mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn']))),
                                                            ('<b style="font-size: 1.4em;font-weight: 500; color: green;">' . mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn'])) . '</span>'), @$element['upn']) : @$element['upn'] ?>
                                                    </td>
                                                    <td style="font-size: 1.4em;font-weight: 500; color: black;"><?=$element['location']?></td>
                                                </tr>
                                            <?endforeach;?>
                                            <?if ($parentIndex):?>
                                                <tr>
                                                    <td colspan="4" style="padding: 0px;" class="bg-info">&nbsp</td>
                                                </tr>
                                            <?endif;?>
                                        <?endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    <?endforeach;?>
                </div>
                <div class="col-md-1 text-center">
                    <table class="table">
                        <thead>
                        <th class="text-center">Реестр</th>
                        </thead>
                        <tbody>
                        <tr class="lead">
                            <td class="text-center">
                                <button class="btn btn-sm btn-inverse" title="<?= Yii::t('app', 'Распечатать реестр') ?>" data-toggle="tooltip" data-placement="top" onclick="printUrl('<?= Url::to(["/logistics/implementation-monitor/default/registry-box-print"]) . "?id=" . $box->id ?>');">
                                    <span style="font-size:2em;color:blueviolet;" class="glyphicon glyphicon-print"></span>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-1 text-center">
                    <table class="table">
                        <thead>
                        <th class="text-center">Реестр</th>
                        </thead>
                        <tbody>
                        <tr class="lead">
                            <td class="text-center">
                                <button class="btn btn-sm btn-inverse" title="<?= Yii::t('app', 'Распечатать упаковочный лист') ?>" data-toggle="tooltip" data-placement="top"  onclick="printUrl('<?= Url::to(["/logistics/implementation-monitor/default/registry-box-package-print"]) . "?id=" . $box->id ?>');">
                                    <span style="font-size:2em;color:green;" class="glyphicon glyphicon-print"></span>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-1 text-center">
                    <table class="table">
                        <thead>
                        <th class="text-center">Упаковать коробку</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <?php if($box->status == Box::STATUS_READY_TO_PACK): ?>
                                    <?= Html::a('<span style="font-size: 3.2em" class="glyphicon glyphicon-ok text-success"></span>',Url::to(['/logistics/implementation-monitor/default/pack-box-new','id' => $box->id ]),$options = [
                                        'class'            => 'load-order',
                                        'data-toggle'      => 'modal',
                                        'data-target'      => '#myModal',
                                        'data-shipment-id' => $model->id,
                                        'data-box-id'      => $box->id,
                                        'data-has-upns'    => 1,
                                        'onclick'          => '
                                    $("#order-content").load($(this).attr("href"));
                                    $("#hidden-href").attr("href", $(this).attr("href"));
                                    $("#hidden-href").attr("data-shipment-id", $(this).attr("data-shipment-id"));
                                    $("#hidden-href").attr("data-box-id", $(this).attr("data-box-id"));
                                    
                                    $("#pack-button").attr("data-shipment-id", $(this).attr("data-shipment-id"));
                                    $("#pack-button").attr("data-box-id", $(this).attr("data-box-id"));
                                    if ($(this).attr("data-has-upns")) {
                                        $("#pack-button").addClass("hide");
                                    } else {
                                        $("#pack-button").removeClass("hide");
                                    }
                                    
                                    $("#button-open-rework-form").attr("data-shipment-id", $(this).attr("data-shipment-id"));
                                    $("#button-open-rework-form").attr("data-box-id", $(this).attr("data-box-id"));
                                ',
                                    ]) ?>
                                <?php elseif ($box->status == Box::STATUS_NOT_HANDLED): ?>
                                    <?= Html::button('<span class="fa fa-times text-danger" style="font-size:2em;" title="' . Yii::t('app', 'Упаковать коробку') . '" data-toggle="tooltip" data-placement="top"></span>', [
                                        'class' => 'btn btn-sm btn-inverse ship-button',
                                    ]);?>
                                <?php elseif ($box->status == Box::STATUS_PACKED): ?>
                                    <button class="btn btn-sm btn-inverse" title="<?= Yii::t('app', 'Коробка упакована') ?>" data-toggle="tooltip" data-placement="top" onclick="printUrl('<?= Url::to(["/logistics/implementation-monitor/default/literal-box-print"]) . "?id=" . $box->id ?>');">
                                        <span style="font-size:2em;" class="fa fa-cubes"></span>
                                    </button>
                                <?php endif; ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        <?endforeach;?>
    </div>

<?php Pjax::end(); ?>

<?php
Modal::begin([
    'header' => '<h2>Монитор упаковки</h2>',
    'options' => [
        'id' => 'myModal',
    ],
    'size' => 'modal-lg'
]);

echo Html::button(Yii::t('app',  'Упаковать'), [
    'id' => 'pack-button',
    'class' => 'btn btn-primary',
    'style' => 'margin-bottom:10px;',
    'onclick' => '
        $.get( "'. Url::to(['pack-shipment-without-upns']) .'", {shipmentId: $(this).attr("data-shipment-id")}, function(data) {
            if (!data.status) {
                notie.alert(3,"Не удалось выполнить команду",3);
                return;
            }
            notie.alert(1,"Успешное выполнение",2);
            $.get("'. Url::to(['registry-shipment-print']) . '?id=" + $("#hidden-href").attr("data-shipment-id"), function(data) {
                myWindow = window.open();
                myWindow.document.write(data);
                myWindow.print();
                myWindow.close();
            });
            $("#myModal").modal("hide");
        });
    '
]);

//echo Html::button(Yii::t('app',  'Не могу найти UPN'), [
//    'id' => 'button-open-rework-form',
//    'class' => 'btn btn-warning',
//    'style' => 'margin-bottom:10px;',
//    'onclick' => '
//        var shipmentId = $(this).data("shipment-id");
//        $("#block-rework-list").load("/logistics/implementation-monitor/default/rework-shipment-form?shipmentId=" + shipmentId);
//        $("#myModal").modal("hide");
//        $("#modal-rework-shipment").modal("show");
//    '
//]);

//Подгрузить представление для получения заявок
echo Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control','id' => 'barcode-scaner',
    'onchange' => '
        $.get( "'. Url::to(['/logistics/implementation-monitor/default/search-box']) .'", {shipmentId: $("#hidden-href").attr("data-shipment-id"), boxId: $("#hidden-href").attr("data-box-id"), barcode: $(this).val()}, function(data) {
            if (data.status == "success") {
                if (data.message) {
                    notie.alert(1, data.message,2);
                } else {
                    notie.alert(1,"Успешное выполнение",2);
                }
            } else if (data.status == "warning") {
                if (data.message) {
                    notie.alert(2, data.message,2);
                } else {
                    notie.alert(2,"Предупреждение",2);
                }
            } else if (data.status == "error" && data.printLabel == true) {
                notie.alert(1,"Распечатана этикетка. Наклейте ее на продукт и пропикайте ее",2);
            } else if (data.status == "error") {
                notie.alert(3,data.message,15);
            } else {
                notie.alert(2,"Непонятная ошибка, обратитесь к IT-службе",2);
            }
            $("#order-content").load( $("#hidden-href").attr("href") , function() {
                $("#barcode-scaner").focus();
                $("#barcode-scaner").val("");
            });
            if (data.boxStatus == true) {
                $.get("'. Url::to(['/logistics/implementation-monitor/default/literal-box-print']) . '?id=" + $("#hidden-href").attr("data-box-id"), function(data) {
                    myWindow = window.open();
                    myWindow.document.write(data);
                    myWindow.print();
                    myWindow.close();
                });
                $("#myModal").modal("hide");
            }
            if (data.printLabel == true) {
                $.get("'. Url::to(['/logistics/naryad/print-single-label']) . '?id=" + data.upnId, function(data) {
                    myWindow = window.open();
                    myWindow.document.write(data);
                    myWindow.print();
                    myWindow.close();
                });
            }
            if (data.printParentLabel == true) {
                $.get("'. Url::to(['/logistics/naryad/print-full-parent-label']) . '?id=" + data.parentId, function(data) {
                    myWindow = window.open();
                    myWindow.document.write(data);
                    myWindow.print();
                    myWindow.close();
                });
            }
        });
   ']);
echo Html::a('', $url = '', $options = ['class' => 'hidden' , 'id' => 'hidden-href']);
echo Html::tag('div','',['id' => 'order-content']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Документы</h2>',
    'options' => [
        'id' => 'modal-print-documents',
    ],
    'size' => 'modal-sm'
]);

echo '<div class="documents"></div>';

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Отгрузка приказа № <span class="shipment-number"></span></h2>',
    'options' => [
        'id' => 'modal-ship-one',
    ],
    'size' => 'modal-sm'
]);


//Подгрузить представление для получения заявок
echo Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control barcode-scaner',
    'placeholder' => 'Штрихкод приказа',
    'onchange' => '
        $.get( "'. Url::to(['/logistics/implementation-monitor/default/search-ship-one']) .'", {shipmentId: $(this).data("shipment-id"), barcode: $(this).val()}, function( data ) {
             if (data.status == true) {
                 notie.alert(1,"Приказ успешно отгружен",2);
                 $("#modal-ship-one").modal("hide");
             }else if (data.status == false) {
                  notie.alert(3, data.error ? data.error : "Приказ НЕ отгружен. Что то пошло не так",2);
             }else {
                  notie.alert(2,"Непонятная ошибка, обратитесь к IT-службе",2);
             }
        });
        $(this).val("");
    ']);

Modal::end();
?>

<?php Yii::$app->view->registerJs('
    //Функция заполняет таблицу документов в модальном окне печати
    function fillDocumentsTable(documents) {
        html = "<table>";
        documents.forEach(function(doc) {
            html += "<tr>";
            html += "<td style=\"min-width:200px;\">" + doc.name + "</td>";
            html += "<td>";

            var iframeId = "iframe-" + doc.id;
            html += "<iframe id=\'" + iframeId + "\' src=\'" + doc.url + "\' style=\'display:none;\'></iframe>";
            html += "<button class=\'btn btn-sm btn-inverse print-document-button\' title=\'Распечатать документ\' data-toggle=\'tooltip\' data-placement=\'top\' onclick=\'printIframeContent(\"" + iframeId + "\");\'>";
            html += "<span style=\'font-size:2em;color:blueviolet;\' class=\'glyphicon glyphicon-print\'></span>";
            html += "</button>";
            
//            //Альтернативный вариант печати через printJs: 
//            html += "<button class=\'btn btn-sm btn-inverse print-document-button\' title=\'Распечатать документ\' data-toggle=\'tooltip\' data-placement=\'top\' onclick=\'printJS(\"" + doc.url + "\")\'>";
//            html += "<span style=\'font-size:2em;color:blueviolet;\' class=\'glyphicon glyphicon-print\'></span>";
//            html += "</button>";
            
            html += "</td>";
            html += "</tr>";
        });
        html += "</table>";
        $("#modal-print-documents .documents").html(documents.length ? html : "Документы не найдены");
    }
    
    //Вывод на печать содержимого iframe
    function printIframeContent(iframeId) {
        var iframe = document.getElementById(iframeId);
        iframe.focus();
        iframe.contentWindow.print();
    }
    
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>

