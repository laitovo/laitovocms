<?php

use core\models\shipment\Shipment;
use yii\helpers\Html;

/**
 * @var Shipment $model
 */

/**
 * Подсветка отгрузки различными цветами
 */
$colorCLass = "";
if ($model->isPacked() && ($model->hasWeighing || $model->hasSizing) && !$model->documentsArePrinted) {
    //Если после габаритов упаковывалась
    $colorCLass = "bg-warning";
} elseif ($model->isPacked() ) {
    //Если просто упаковывалась
    $colorCLass = "bg-success";
} elseif (!$model->isPacked() && $model->documentsArePrinted) {
    //Если просто упаковывалась
    $colorCLass = "bg-info";
}

?>
<div style="">
    <div class="shipment">
            <table class="table table-bordered <?= $colorCLass?>">
                <thead>
                <tr>
                    <th class="text-left text-nowrap">№ Отгрузки</th>
                    <th class="text-left text-nowrap">Транспортная компания</th>
                    <th class="text-left text-nowrap">Дата отгрузки</th>
                    <th class="text-left text-nowrap">Кол-во артикулов</th>
                    <th class="text-left text-nowrap">Статус</th>
                    <th class="text-left text-nowrap">Действие</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-left text-nowrap"><?= $model->id ?>
                        <? if ($model->isNeedTTN()) :?>
                            <br>
                        <strong class="text-nowrap" style="font-size:1.5em;font-weight:bold;color: red" data-toggle="tooltip" title="<?=Yii::t('app', 'Обязательно приложить ТТН к заказу')?>"><?=Yii::t('app', '(с ТТН)')?></strong>
                        <? endif; ?>
                    </td>
                    <td class="text-left text-nowrap"><?= $model->transportCompany ? $model->transportCompany->name : 'Не указана' ?></td>
                    <td class="text-left text-nowrap"><?= $model->shipmentDate ? date('d.m.y',$model->shipmentDate) : '' ?></td>
                    <?php
                        $entries = $model->entriesForPack;
                        $class = $entries < 7 ? '' : 'text-danger';
                    ?>
                    <td class="text-left text-nowrap <?= $class?>"><?= $entries ?></td>
                    <td class="text-left text-nowrap"><?= $model->logisticsStatusName ?></td>
                    <td class="text-left text-nowrap">
                        <?php if($model->isHandled()): ?>
                            <?= Html::a('<span class="fa fa-cubes text-success"></span> ',
                                ['update', 'id' => $model->id],
                                [
                                    'class' => '',
                                    'style' => 'font-size:2em;cursor:pointer; border: 1px solid grey; padding: 2px',
                                    'data' => [
                                        'toggle' => 'tooltip',
                                        'title' => Yii::t('app', 'Упаковать'),
                                        'pjax' => 0
                                    ],
                                ]) ?>
                        <?php elseif($model->isOnStorageRequest()): ?>
                            <?php if($model->hasWeighing && !$model->weight): ?>
                                <?= Html::a('<i class="fa fa-balance-scale weight-edit-button text-danger"></i> ',
                                    ['sample', 'id' => $model->id],
                                    [
                                        'class' => '',
                                        'style' => 'font-size:2em;cursor:pointer; border: 1px solid grey; padding: 2px',
                                        'data' => [
                                            'toggle' => 'tooltip',
                                            'title' => Yii::t('app', 'Установить вес'),
                                            'pjax' => 0
                                        ],
                                    ]) ?>
                            <?php endif; ?>
                            <?php if($model->hasSizing && !$model->size): ?>
                                <?= Html::a(Html::img('/img/ruler.png',[
                                    'style' => 'position:relative;top:-5px;width:24px;height:24px;opacity:0.7;cursor:pointer;',
                                    ]),
                                    ['sample', 'id' => $model->id],
                                    [
                                        'class' => '',
                                        'style' => 'font-size:2em;cursor:pointer; border: 1px solid grey; padding: 2px',
                                        'data' => [
                                            'toggle' => 'tooltip',
                                            'title' => Yii::t('app', 'Установить габариты'),
                                            'pjax' => 0
                                        ],
                                    ]) ?>
                            <?php endif; ?>
                        <?php elseif($model->isPacked()): ?>
<!--                            --><?//= Html::button('<span class="fa fa-truck" style="font-size:2em;" title="' . Yii::t('app', 'Отгрузить приказ') . '" data-toggle="tooltip" data-placement="top"></span>', [
//                                'class' => 'btn btn-sm btn-inverse ship-button',
//                                'data' => [
//                                    'shipment-id' => $model->id,
//                                    'pjax' => 0
//                                ],
//                                'onclick' => '
//                                        $("#modal-ship-one .barcode-scaner").data("shipment-id", $(this).data("shipment-id"));
//                                        $("#modal-ship-one .shipment-number").html($(this).data("shipment-id"));
//                                        $("#modal-ship-one").modal("show");
//                                    ',
//                            ]);?>
                            <?= Html::a('<span class="fa fa-truck" style="font-size:2em;" title="' . Yii::t('app', 'Отгрузить приказ') . '" data-toggle="tooltip" data-placement="top"></span>',
                                ['update', 'id' => $model->id],
                                [
                                    'class' => 'btn btn-sm ship-button',
                                    'style' => 'color:white;border:1px solid',
                                    'data' => [
//                                        'toggle' => 'tooltip',
//                                        'title' => Yii::t('app', 'Отгрузить приказ'),
                                        'pjax' => 0
                                    ],
                                ]) ?>
                        <?php else: ?>
                            <?= Html::a('<span class="fa fa-cubes text-success"></span> ',
                                ['update', 'id' => $model->id],
                                [
                                    'class' => '',
                                    'style' => 'font-size:2em;cursor:pointer; border: 1px solid grey; padding: 2px',
                                    'data' => [
                                        'toggle' => 'tooltip',
                                        'title' => Yii::t('app', 'Упаковать'),
                                        'pjax' => 0
                                    ],
                                ]) ?>
                        <? endif; ?>
                    </td>
                </tr>
                </tbody>
            </table>
    </div>
</div>