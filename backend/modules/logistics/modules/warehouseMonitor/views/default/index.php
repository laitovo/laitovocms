<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\bootstrap\Modal;
use yii\web\View;

$this->render('@backendViews/logistics/views/menu');
$this->title = Yii::t('app', 'Заказы к отгрузке');

/**
 * @var $provider ActiveDataProvider [ Список отгрузок ]
 *
 * @var $filter string [ Фильтр, по которому сейчас производится фильтрация ]
 * @var $filterCounts array [ Счетчики в разрезе фильтров ]
 * @var $filterCompany array [ Список транспортных компаний ]
 * @var $companyFilter string [ Фильтр по транспортной компании]
 * @var $autoRefresh boolean  [ Включено/Выключено Автообновление ]
 * @var $search string [ Строка, по которой производится поиск ]
 */

$script = <<<SCRIPT
    setInterval(function() {
        if ($("#checkbox-auto-refresh").prop("checked")) {
            $.pjax.reload({container : "#main", timeout: false});
        }
    }, 60*1000);
//    
//    $("#myPjax").on("pjax:end", function() {
//      $("#myPjax [data-toggle=tooltip]").tooltip();
//    })
//
//    $('#myModal').on('shown.bs.modal', function () {
//      $('#barcode-scaner').focus();
//    });
//    
    $('#myModal').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#main', timeout: false});
    });
    
    $('#myModal3').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#main', timeout: false});
    });
    
    $('#myModal3').on('shown.bs.modal', function () {
      $('#barcode-scaner-2').focus();
    });
    
    $('#modal-ship-one').on('shown.bs.modal', function () {
      $('#modal-ship-one .barcode-scaner').focus();
    });
    
    $('#modal-ship-one').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#main', timeout: false});
    });
//    
//    $('#modal-update-weight').on('hidden.bs.modal', function () {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    });
//    
//    $('#modal-update-size').on('hidden.bs.modal', function () {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    });
//    
//    $("#modal-rework-shipment").on("pjax:end", function() {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    })
//    
//    $("#modal-danger").on('hidden.bs.modal', function() {
//      $.pjax.reload({container : '#myPjax', timeout: false});
//    })
SCRIPT;
$this->registerJs($script);

?>

<?php
/**
 * Оборачиваем всю страницу в PJAX контейнер для возможности перезагружать содержимое без перезагрузки самой страницы.
 */
?>
<?php Pjax::begin(['id' => 'main','timeout' => 5000, 'enablePushState' => false]); ?>

<?php
/**
 * Кнопка для массового проведения реализаций в конце рабочего дня.
 */
?>
    <div class="row">
        <div class="col-md-2 col-sm-4 col-xs-6">
            <?= Html::button(Yii::t('app',  'Провести реализацию'), [
                'class' => 'btn btn-icon btn-round ' . ( 'btn-info'),
                'onclick' => '
                    $("#myModal3").modal("show");
                ',
            ]) ?>
        </div>
        <div class="col-md-offset-6 col-md-2 col-sm-4 col-xs-6">
            <?= Html::a(
                Html::tag('span', 'RU : ' . $filterCounts['needToOrderRu'], [
                    'class' => 'badge',
                    'style' => 'font-size:1em;' . (1 ? 'background:#E9967A;' : 'background:#fff;color:#76838f;'),
                ]) . Yii::t('app', ' Необработано заявок') . ' ' .

                Html::tag('span', 'EU : ' . $filterCounts['needToOrderEu'], [
                    'class' => 'badge',
                    'style' => 'font-size:1em;' . (1 ? 'background:#E9967A;' : 'background:#fff;color:#76838f;'),
                ]),['/logistics/logistics-monitor/default/index'],
                [
                    'class'               => 'btn btn-default',
                    'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                    'data-toggle'         => 'tooltip',
                    'data-original-title' => Yii::t('app', 'Количество не оформленных заявок на готовые заказы'),
                    'target'              => '_blank',
                    'data-pjax'           => '0',
                ]
            ); ?>
        </div>
    </div>

<hr>

<?php ActiveForm::begin([
    'id'      => 'search-form',
    'action'  => [''],
    'method'  => 'post',
    'options' => [
        'data-pjax' => true
    ]
]); ?>

<?php
/**
 * Блок кнопок для фильтрации отгрузок по статусам.
 */
?>
<div class="row">
    <div class="col-md-12">
        <?= Html::button(
            Html::tag('span', $filterCounts['ship'], [
                'class' => 'badge',
                'style' => 'font-size:1em;' . (1 ? 'background:#3aa99e;' : 'background:#fff;color:#76838f;'),
            ]) . Yii::t('app', ' К отгрузке'),
            [
                'class'               => 'btn button-filter ' . ($filter == 'ship' ? 'active btn-danger' : 'btn-default'),
                'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                'data-toggle'         => 'tooltip',
                'data-original-title' => Yii::t('app', 'Должно быть отгружено сегодня'),
                'data-filter'         => 'ship',
            ]
        ); ?>
        <?= Html::button(
            Html::tag('span', $filterCounts['carried'], [
                'class' => 'badge',
                'style' => 'font-size:1em;' . (1 ? 'background:#f2a654;' : 'background:#fff;color:#76838f;'),
            ]) . Yii::t('app', ' Не отгружено'),
            [
                'class'               => 'btn button-filter ' . ($filter == 'carried' ? 'active btn-danger' : 'btn-default'),
                'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                'data-toggle'         => 'tooltip',
                'data-original-title' => Yii::t('app', 'Было запланировано к отгрузке, но не было отгружено в рамках планируемой даты'),
                'data-filter'         => 'carried',
            ]
        ); ?>
        <?= Html::button(
            Html::tag('span', $filterCounts['packedForShip'], [
                'class' => 'badge',
                'style' => 'font-size:1em;' . (1 ? 'background:#46be8a;' : 'background:#fff;color:#76838f;'),
            ]) . Yii::t('app', ' Упаковано'),
            [
                'class'               => 'btn button-filter ' . ($filter == 'packedForShip' ? 'active btn-danger' : 'btn-default'),
                'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                'data-toggle'         => 'tooltip',
                'data-original-title' => Yii::t('app', 'Упаковано отгрузок из тех, что должны уехать сегодня'),
                'data-filter'         => 'packedForShip',
            ]
        ); ?>
        <?= Html::button(
            Html::tag('span', $filterCounts['nextShip'], [
                'class' => 'badge',
                'style' => 'font-size:1em;' . (1 ? 'background:#77d6e1;' : 'background:#fff;color:#76838f;'),
            ]) . Yii::t('app', ' Отгрузки на следующие дни') . ' ' .

            Html::tag('span', $filterCounts['nextShip'] - $filterCounts['nextShipNotReady'], [
                'class' => 'badge',
                'style' => 'font-size:1em;' . (1 ? 'background:#f2a654;' : 'background:#fff;color:#76838f;'),
            ]),
            [
                'class'               => 'btn button-filter ' . ($filter == 'nextShip' ? 'active btn-danger' : 'btn-default'),
                'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                'data-toggle'         => 'tooltip',
                'data-original-title' => Yii::t('app', 'Должно быть отгружено в следующие дни'),
                'data-filter'         => 'nextShip',
            ]
        ); ?>


        <?= Html::hiddenInput('warehouseMonitor_default_filter', $filter, ['id' => 'hidden-filter']) ?>
        <?= Html::hiddenInput('update_warehouseMonitor_default_filter', true, ['id' => 'hidden-filter-checker']) ?>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <? foreach ($filterCompany as $companyId => $company) : ?>
        <?= Html::button(
             Yii::t('app', "$company"),
            [
                'class'               => 'btn button-filter-company ' . ($companyFilter == $companyId ? 'active btn-danger' : 'btn-default'),
                'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                'data-toggle'         => 'tooltip',
                'data-original-title' => Yii::t('app', 'Должно быть отгружено сегодня'),
                'data-filter'         => $companyId,
            ]
        ); ?>
        <?endforeach;?>
        <?= Html::hiddenInput('warehouseMonitor_default_companyFilter', $companyFilter, ['id' => 'hidden-filter-company']) ?>
        <?= Html::hiddenInput('update_warehouseMonitor_default_companyFilter', true, ['id' => 'hidden-filter-company-checker']) ?>
    </div>
</div>

    <hr>



<div class="row">
    <?php
    /**
     * Блок с настройкой автообновления страницы, а также кнопкой принудительного автообновления страницы.
     */
    ?>
    <div class="form-group col-md-2">
        <?= Html::tag('div',
            Html::checkbox("implementationMonitor_default_autoRefresh", $autoRefresh, ['id' => 'checkbox-auto-refresh'])
            .Html::tag('label','Автообновление', ['for' => 'checkbox-auto-refresh']),
            [
                'class' => 'checkbox-custom checkbox-primary text-left',
                'onchange' => '$("#search-form").submit();'
            ]
        ) ?>
        <?= Html::hiddenInput('update_implementationMonitor_default_autoRefresh',true); ?>
        <?= Html::button(Yii::t('app', 'Обновить'), [
            'id' => 'button-refresh',
            'class' => 'btn btn-outline btn-round btn-primary',
            'onclick' => '
                    $.pjax.reload({container : "#main", timeout: false});
                '
        ]) ?>
    </div>
    <?php
    /**
     * Блок с полем поиска по перечню отгрузок. Дополнительный способ фильтрации отгрузок по ключевым словам.
     */
    ?>
    <div class="col-md-2">
        <?= Html::input($type = 'text','implementationMonitor_default_search',$value = $search, $options = ['class' => 'form-control', 'id' => 'search-textbox', 'placeholder' => 'Поиск']); ?>
        <?= Html::hiddenInput('update_implementationMonitor_default_search',true); ?>
    </div>
    <div class="col-md-2">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::button('Очистить', ['class' => 'btn btn-sm btn-default', 'onclick' => '
                $("#search-textbox").val("");
                $("#search-form").submit();
            ']) ?>
    </div>

    <?php
    /**
     * Блок для привлечения внимания кладовщика. Показывает информацию о низком уровне остатков на складе.
     */
    ?>
    <?if(true):?>
        <div class="col-md-3">
            <span class="text-warning" style="font-size:1.2em;font-weight:bold;"><?=Yii::t('app', 'Присутствуют позиции с низким уровнем остатков!')?></span>
            <br>
            <?=Html::a(Yii::t('app', 'Посмотреть'), Url::to(['/logistics/implementation-monitor/default/low-balance-list']), [
                'class' => 'btn btn-outline btn-round btn-sm btn-warning',
                'style' => 'margin-top:5px;',
                'data-toggle' => 'modal',
                'data-target' => '#modal-low-balance',
                'onclick' => '
                        $("#block-low-balance-content").load($(this).attr("href"));
                    '
            ])?>
        </div>
    <?endif;?>
</div>

<?php ActiveForm::end(); ?>

<hr>

<?php
    try {
        echo ListView::widget([
            'dataProvider' => $provider,
            'itemView' => '_shipment',
        ]) ;
    }catch (Exception $exception) {
        echo Yii::t('app','Не удалось отобразить список с отгрузками');
    }
?>

<?php //var_dump($provider); ?>

<?php Pjax::end(); ?>

<?php
Modal::begin([
    'header' => '<h2>Отгрузка реестров</h2>',
    'options' => [
        'id' => 'myModal3',
    ],
    'size' => 'modal-lg'
]);

//Подгрузить представление для получения заявок
echo Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control','id' => 'barcode-scaner-2',
    'onchange' => '
        $.get( "'. Url::to(['/logistics/implementation-monitor/default/search-reestr']) .'", {barcode: $(this).val()}, function( data ) {
             if (data.status == true) {
                 notie.alert(1,"Реестр успешно отгружен",2);
             }else if (data.status == false) {
                  notie.alert(3,"Реестр НЕ отгружен. Что то пошло не так",2);
             }else {
                  notie.alert(2,"Непонятная ошибка, обратитесь к IT-службе",2);
             }
        });
        setTimeout(function(){
            $("#barcode-scaner-2").focus(); 
            $("#barcode-scaner-2").val("");
            $.pjax.reload({container : \'#main\', timeout: false})
        }, 1000);
    ']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Позиции с низким уровнем остатков</h2>',
    'options' => [
        'id' => 'modal-low-balance',
    ],
    'size' => 'modal-lg'
]);

echo Html::tag('div','',['id' => 'block-low-balance-content']);

Modal::end();
?>

<?php Yii::$app->view->registerJs('
    $("body").on("click", ".button-filter",function(e){
        $(".button-filter").prop("disabled", true);
        var html = \'<i class="fa fa-spinner fa-spin" style="font-size:20px;"></i>\'
        $(this).attr("title", "' . Yii::t('app', 'Подождите...') . '").find(".badge").html(html);
        var value = $(this).hasClass("active") ? "" : $(this).data("filter");
        $("#hidden-filter").val(value);
        $("#search-form").submit();
    });
    
        $("body").on("click", ".button-filter-company",function(e){
        $(".button-filter-company").prop("disabled", true);
        var html = \'<i class="fa fa-spinner fa-spin" style="font-size:20px;"></i>\'
        $(this).attr("title", "' . Yii::t('app', 'Подождите...') . '").find(".badge").html(html);
        var value = $(this).hasClass("active") ? "" : $(this).data("filter");
        $("#hidden-filter-company").val(value);
        $("#search-form").submit();
    });
', View::POS_END);
