<?php

use yii\helpers\Html;
use core\models\shipment\Shipment;
use yii\helpers\Url;

$this->render('@backendViews/logistics/views/menu');
$this->title = Yii::t('app', 'Замер параметров отгрузки');

/**
* @var $model Shipment
 */

?>
<?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться в монитор склада'), ['index'], ['class' => 'btn btn-info']) ?>
<hr>

<?//= Html::checkbox('documentsArePrinted', $model->documentsArePrinted, ['id' => 'check-documents']) ?>
<?//= Html::label($model->getAttributeLabel('documentsArePrinted'),'check-documents') ?>

<?=
    Html::tag('div',
    Html::checkbox(false,  $model->documentsArePrinted, [
        'onchange'=>'$.get("' . Url::to(['/ajax/shipment-print-docs']) . '",{"shipmentId":' . $model->id .',"value": ($(this).is(":checked") ? 1 : 0)},function(data){
                if (data.status == "success" && data.value == 1) {
                    notie.alert(1,data.message,15);
                } else if (data.status == "success" && data.value == 0) {
                    notie.alert(3, data.message ,15);
                } else {
                    notie.alert(2, data.message ,15);
                }
            });',
        'id' => 'check-documents',
    ])
    .Html::label($model->getAttributeLabel('documentsArePrinted'), 'check-documents'),
    [
        'class' => 'checkbox-custom checkbox-primary text-left',
    ]
);
?>

<?= $this->render('_shipment_info', ['model' => $model]); ?>

