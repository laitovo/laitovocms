<?php
namespace backend\modules\logistics\modules\warehouseMonitor\models;

use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use common\models\logistics\Order;
use common\models\logistics\Naryad;
use backend\modules\laitovo\models\ErpNaryad;
use common\models\logistics\OrderEntry;
use core\models\box\Box;
use core\models\realizationLog\RealizationLog;
use Exception;

class ElementData
{
    public static function create(Order $order, Box $box)
    {
        $result = [];
        $elements = $order->notDeletedOrderEntries;
        foreach ($elements as $element) {
            if ($element->boxId != $box->id) continue;
            /**
             * @var $upn Naryad
             */
            $upn = $element->upn;
            if ($upn) {
                $resElement = new ElementDto();
                $resElement->car = $element->article == 'OT-678-0-0' ? "{$element->name} ({$element->itemtext})" : $element->car;
                $resElement->name = $element->name;
                $resElement->readyToPack = (($state = $upn->storageState) != null && ($storage =$state->storage) != null && ($storage->type != Storage::TYPE_SUMP));
                $resElement->location = self::getLocation($element);
                $resElement->packed = $upn->packed;
                $resElement->upn = $upn->id;
                $resElement->article = $element->article;
                $result[$upn->pid][] = $resElement;
            }
        }

        return $result;
    }

    private static function getLocation($orderEntry)
    {
        //Проверка на переданное значение
        if ($orderEntry !== null && !($orderEntry instanceof OrderEntry))
            throw new Exception('Переданное значение должно быть элементов логистической заявки');

        if ($orderEntry === null) return null;

        $literal = RealizationLog::find()->where(['orderEntryId'=> $orderEntry->id])->select('literal')->scalar();
        if ($literal)
            return "Отгружен $literal";

        //Если текущий upn находится на складе - тогда пишим склад
        $upn = $orderEntry->upn;
        $state = StorageState::find()->where(['upn_id' => $upn->id])->one();

        if ($state) return $state->storage ? ($state->literal) : 'Склад';
        //Если теккущий upn находиться на производстве - пишем локацию
        $naryad = ErpNaryad::find()->where(['logist_id' => $upn->id])->one();

        if ($naryad)
        {
            if ($naryad->status == ErpNaryad::STATUS_READY) return 'Диспетчер-Склад';
            return @$naryad->location && !$naryad->locationstart ? @$naryad->location->name : 'Диспетчер : '.@$naryad->locationstart->name;
        }

        return 'Ожидется обработка';
    }
}