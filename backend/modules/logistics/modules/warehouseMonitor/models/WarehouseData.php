<?php

namespace backend\modules\logistics\modules\warehouseMonitor\models;

use backend\modules\logistics\models\Order;
use common\models\logistics\OrderEntry;
use core\models\box\Box;
use core\models\shipment\Shipment;
use core\models\transportCompany\TransportCompany;
use core\services\SBox;
use core\services\SShipmentSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class WarehouseData
 * @package backend\modules\logistics\modules\warehouseMonitor\models
 */

class WarehouseData
{

    public function getDataStorage($params)
    {
        /**
         * 3 строчки для определения отгрузок, которые необходимо показывать в мониторе склада.
         * В Мониторе склада показываются обработанные отгрузки, упакованные отгрузки а также отгруки не обработанные,
         * но которые содержат коробки, которые можно уже упаковать.
         */
        $boxes = Box::find()->readyToPack();
        $boxesIds = ArrayHelper::map($boxes,'id','id');
        $shipmentAdditionalIds = SBox::getShipmentIdsForBoxes($boxesIds);

        /**
         * Также смотрим присланные нам параметры. Если есть параметр фильтр, то в зависимсти от него у нас будет разный запрос к отгрузкам.
         */
//        $shipments = $this->_findStorageShipments($params,$shipmentAdditionalIds);

        return new ActiveDataProvider([
            'query'=> $this->_findStorageShipments($params,$shipmentAdditionalIds)
        ]);
//
//
//        foreach ($shipments as $shipment) {
//
//            $shipmentId = $this->_shipmentManager->getId($shipment);
//            if (!isset($data[$shipmentId])) {
//                $data[$shipmentId] = $this->_addShipmentRow($shipment);
//            }
//            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
//            foreach ($orderEntries as $orderEntry) {
//                $orderId = $this->_elementService->getOrderId($orderEntry);
//                $boxId = $orderEntry->boxId ?? 0;
//                $boxStatus = $this->_getBoxStatus($orderEntry);
//                if (!isset($data[$shipmentId]['boxes'][$boxId])) {
//                    $data[$shipmentId]['boxes'][$boxId]['status'] = $boxStatus;
//                    $data[$shipmentId]['boxes'][$boxId]['id'] = $boxId;
//                }
//                if (!isset($data[$shipmentId]['boxes'][$boxId]['orders'][$orderId])){
////                if (!isset($data[$shipmentId]['orders'][$orderId])) {
//                    $order = $this->_ordersRepository->getOrderById($orderId);
////                    $data[$shipmentId]['orders'][$orderId] = $this->_addOrderRow($order);
////                    $data[$shipmentId]['orders'][$orderId]['elements'] = [];
//                    $data[$shipmentId]['boxes'][$boxId]['orders'][$orderId] = $this->_addStorageOrderRow($order);
//                }
////                $data[$shipmentId]['orders'][$orderId]['elements'][] = $this->_addOrderElementRow($orderEntry);
//                $element = $this->_addStorageOrderElementRow($orderEntry);
//                $element['factBalance'] = $this->_getStorageBalance->getFactBalanceByArticle($element['article']);
//                $element['hasBalanceControl'] = $this->_getStorageBalance->isArticleOnBalanceControl($element['article']);
//                $element['minBalance'] = $this->_getStorageBalance->getMinBalanceByArticle($element['article']);
//                $parentIndex = $element['parentIndex'] ?: 0;
//                $data[$shipmentId]['boxes'][$boxId]['orders'][$orderId]['elements'][$parentIndex][] = $element;
//            }
//        }
//
//        $data = $this->_sortStorageShipments($data);
//
////        var_dump($data);
////        die;
//        $data = $this->_asProvider($data);

//        return $shipments;
//        return $dataProvider;
    }

    /**
     * Фнукция, которая ищет список отгрузок, в зависимсоти от фильтра.
     *
     * @param $params
     * @param array $additionalIds
     * @return \core\models\shipment\ShipmentQuery
     */
    private function _findStorageShipments($params,$additionalIds = [])
    {
        $query = Shipment::find();

        switch ($params['filter']) {
            case 'ship':
                $query->mustBeShipped(time());
                break;
            case 'carried':
                $query->carriedOver(time());
                break;
            case 'packedForShip':
                $query->packedForShip(time());
                break;
            case 'nextShip':
                $query->mustBeShipped(time(),true);
//                if (!empty($additionalIds))
//                    $query->orIds($additionalIds);
                break;
            default:
                $query->beforeShipping();
                if (!empty($additionalIds))
                    $query->orIds($additionalIds);
        }

        $ids = $query->distinct()->select('id')->column();

        if (($search = $params['search'])) {
            $ids = SShipmentSearch::search($search,$ids);
        }

        $query =  Shipment::find()->byIds($ids)->andWhere(['!=','logisticsStatus',Shipment::LOGISTICS_STATUS_SHIPPED])->storageSort();

        return ($params['companyFilter']) ? $query->andWhere(['transportCompanyId' => $params['companyFilter']]) : $query;
    }

    /**
     * Функция для подсчета отгрузок в разрезе фильтров
     *
     * @return array
     */
    public function filterCounts()
    {
        $filtersCount = [];
        $filtersCount['ship']             = Shipment::find()->mustBeShipped(time())->count();
        $filtersCount['carried']          = Shipment::find()->carriedOver(time())->count();
        $filtersCount['packedForShip']    = Shipment::find()->packedForShip(time())->count();
        $filtersCount['nextShip']         = Shipment::find()->mustBeShipped(time(),true)->count();
        $filtersCount['nextShipNotReady'] = Shipment::find()->mustBeShippedNotReady(time(),true)->count();
        $filtersCount['needToOrderRu']    = $this->needToOrderRu();
        $filtersCount['needToOrderEu']    = $this->needToOrderEu();
        return $filtersCount;
    }

    /**
     * Список возможных фильтров по транспортным компаниям
     * @return array
     */
    public function filterCompany()
    {
        return TransportCompany::find()->select('name')->distinct()->indexBy('id')->column();
    }

    /**
     * Количество не оформленных заявок в системе РУ
     *
     * @return bool|int|string|null
     */
    private function needToOrderRu()
    {
        /**
         * Сначала посчитаем заказы, которые не объеденины в отгрузки и их число = числу потенциальных заявок
         */
        $orderIdsNull = OrderEntry::find()
            ->joinWith('upn.storageState')
            ->select('order_id')
            ->distinct()
            ->where(['and',
                ['logistics_storage_state.literal' => null],
                ['is_deleted' => null],
                ['not', ['order_id' => null]],
                ['shipment_id' => null]
            ])
            ->column();

        $orderIds = OrderEntry::find()
            ->select('order_id')
            ->distinct()
            ->where(['and',
                ['is_deleted' => null],
                ['not', ['order_id' => null]],
                ['not in', 'order_id', $orderIdsNull],
                ['shipment_id' => null]
            ])
            ->column();

        $query = Order::find()
            ->where(['and',
                ['team_id' => Yii::$app->team->id],
                ['processed' => true],
                ['shipped' => null],
                ['in', 'id', $orderIds],
                ['!=','remoteStorage',2],
            ]);

        $part1 = $query->count();

        /**
         * Теперь находим отгрузки в статусе not_handled
         * где заказы с remotestorage != 2 и где у все
         */
        $nullState = Shipment::find()->alias('shipment')
            ->joinWith('notDeletedOrderEntries.upn.storageState as state')
            ->where(['state.literal' => null])
            ->andWhere(['shipment.logisticsStatus' => Shipment::LOGISTICS_STATUS_NOT_HANDLED])
            ->select('shipment.id')
            ->distinct()->column();

        $notHandledShipments = Shipment::find()
            ->where(['logisticsStatus' => Shipment::LOGISTICS_STATUS_NOT_HANDLED])
            ->select('id')
            ->column();

        $orderIds = OrderEntry::find()
            ->where(['shipment_id' => $notHandledShipments])
            ->select('order_id')
            ->distinct()->column();

        $orderIdsFiltered = Order::find()
            ->select('id')
            ->where(['id' => $orderIds])
            ->andWhere(['processed' => true])
            ->andWhere(['shipped' => null])
            ->andWhere(['team_id' => Yii::$app->team->id])
            ->andWhere(['!=','logistics_order.remoteStorage',2])
            ->column();

        $shipmentIds = OrderEntry::find()
            ->select('shipment_id')
            ->where(['order_id' => $orderIdsFiltered])
            ->andWhere(['shipment_id' => $notHandledShipments])
            ->distinct()
            ->column();

        $shipmentList = array_diff($shipmentIds, $nullState);

        $part2 =  count($shipmentList);

        return $part1 + $part2;
    }

    /**
     * Количество неоформленных заявок в EU системе
     *
     * @return bool|int|string|null
     */
    private function needToOrderEu()
    {
        /**
         * Сначала посчитаем заказы, которые не объеденины в отгрузки и их число = числу потенциальных заявок
         */
        $orderIdsNull = OrderEntry::find()
            ->joinWith('upn.storageState')
            ->select('order_id')
            ->distinct()
            ->where(['and',
                ['logistics_storage_state.literal' => null],
                ['is_deleted' => null],
                ['not', ['order_id' => null]],
                ['shipment_id' => null]
            ])
            ->column();

        $orderIds = OrderEntry::find()
            ->select('order_id')
            ->distinct()
            ->where(['and',
                ['is_deleted' => null],
                ['not', ['order_id' => null]],
                ['not in', 'order_id', $orderIdsNull],
                ['shipment_id' => null]
            ])
            ->column();

        $query = Order::find()
            ->where(['and',
                ['remoteStorage' => 2],
                ['team_id' => Yii::$app->team->id],
                ['processed' => true],
                ['shipped' => null],
                ['in', 'id', $orderIds],
            ]);

        $part1 = $query->count();

        /**
         * Теперь находим отгрузки в статусе not_handled
         * где заказы с remotestorage != 2 и где у все
         */
        $nullState = Shipment::find()->alias('shipment')
            ->joinWith('notDeletedOrderEntries.upn.storageState as state')
            ->where(['state.literal' => null])
            ->andWhere(['shipment.logisticsStatus' => Shipment::LOGISTICS_STATUS_NOT_HANDLED])
            ->select('shipment.id')
            ->distinct()->column();

        $notHandledShipments = Shipment::find()
            ->where(['logisticsStatus' => Shipment::LOGISTICS_STATUS_NOT_HANDLED])
            ->select('id')
            ->column();

        $orderIds = OrderEntry::find()
            ->where(['shipment_id' => $notHandledShipments])
            ->select('order_id')
            ->distinct()->column();

        $orderIdsFiltered = Order::find()
            ->select('id')
            ->where(['id' => $orderIds])
            ->andWhere(['processed' => true])
            ->andWhere(['shipped' => null])
            ->andWhere(['team_id' => Yii::$app->team->id])
            ->andWhere(['logistics_order.remoteStorage' => 2])
            ->column();

        $shipmentIds = OrderEntry::find()
            ->select('shipment_id')
            ->where(['order_id' => $orderIdsFiltered])
            ->andWhere(['shipment_id' => $notHandledShipments])
            ->distinct()
            ->column();

        $shipmentList = array_diff($shipmentIds, $nullState);

        $part2 =  count($shipmentList);

        return $part1 + $part2;
    }
}