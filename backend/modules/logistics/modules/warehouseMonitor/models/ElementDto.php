<?php
namespace backend\modules\logistics\modules\warehouseMonitor\models;

class ElementDto extends  \yii\base\Model
{
    public $readyToPack;
    public $packed;
    public $car;
    public $article;
    public $name;
    public $location;
    public $upn;

}