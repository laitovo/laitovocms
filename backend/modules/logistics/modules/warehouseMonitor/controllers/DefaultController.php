<?php

namespace backend\modules\logistics\modules\warehouseMonitor\controllers;

use backend\modules\logistics\modules\warehouseMonitor\models\WarehouseData;
use core\helpers\UpdateSessionTrait;
use core\models\shipment\Shipment;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\web\Controller;
use Yii;
use yii\web\RangeNotSatisfiableHttpException;

/**
 * Default controller for the `laitovo` module
 */
class DefaultController extends Controller
{
    /**
     * Используем данный трейт для записи в сессию необходимых параметров.
     */
    use UpdateSessionTrait;

    /**
     * Основное действие для монитора.
     */
    public function actionIndex()
    {
        /**
         * @var $filter string [ Один из 4 статусов для отгрузок, по которому должны быть отфильтрованы отгрузки в списке ]
         */
        $filter       = $this->_updateSession('warehouseMonitor_default_filter','update_warehouseMonitor_default_filter');

        /**
         * @var $companyFilter string [ Фильтр по транспортной компании]
         */
        $companyFilter       = $this->_updateSession('warehouseMonitor_default_companyFilter','update_warehouseMonitor_default_companyFilter');

        /**
         * @var $search string [ Строка поиска, по которой должны быть отфильтрованы отгрузки в списке отгрузок ]
         */
        $search       = $this->_updateSession('implementationMonitor_default_search', 'update_implementationMonitor_default_search');

        /**
         * @var $autoRefresh boolean [ Свойство определяет, должна ли перезагружатся автоматически страница в мониторе. По умолчанию Да - должна
         * .... Значение ищется в сессии и изначально подставляется от туда ]
         */
        $autoRefresh  = $this->_updateSession('implementationMonitor_default_autoRefresh', 'update_implementationMonitor_default_autoRefresh', true);


        /**
         * Получение списка отгрузок, которые должны быть отражены в мониторе логистики.
         */
        $data = new WarehouseData();
        $provider = $data->getDataStorage([
            'search'        => $search,
            'filter'        => $filter,
            'companyFilter' => $companyFilter,
        ]);


        return $this->render('index',[
            'provider'     => $provider,
            'search'       => $search,
            'filter'       => $filter,
            'filterCounts' => $data->filterCounts(),
            'autoRefresh'  => $autoRefresh,
            'filterCompany'  => $data->filterCompany(),
            'companyFilter'  => $companyFilter,
        ]);
    }

    /**
     * @todo Сделать вывод отгрузки для упаковки перед замером габаритов и взвешиванием
     *
     * Данное действие позволяет передать вес или габариты по отгрузке.
     * @param $id [ Идентификатор отгрузки ]
     * @return string
     */
    public function actionSample($id)
    {
        $model = Shipment::findOne($id);
        if (!$model) {
            $this->redirect('index');
            Yii::$app->session->setFlash('error', 'Отгрузка не найдена');
        }

        if ($model->load(Yii::$app->request->post()) && ($model->logisticsStatus = Shipment::LOGISTICS_STATUS_ON_STORAGE_RESPONSE) && $model->save()) {
            Yii::$app->session->setFlash('error', 'Габариты успешно указаны');
            return $this->redirect(['index']);
        } else {
            return $this->render(
                'sample',[
                'model' => $model,
            ]);
        }
   }

    /**
     * Действие по упаковке.
     *
     * @param $id
     * @return string
     */
   public function actionUpdate($id)
   {
       ini_set("max_execution_time", "0");
       ini_set("memory_limit","-1");

       $model = Shipment::findOne($id);
       if (!$model) {
           $this->redirect('index');
           Yii::$app->session->setFlash('error', 'Отгрузка не найдена');
       }

       return $this->render('update',[
           'model' => $model,
       ]);
   }

    /**
     * @throws Exception
     * @throws InvalidConfigException
     * @throws RangeNotSatisfiableHttpException
     */
    public function actionPrintOzon($postingNumber, $clientId)
    {
        $credentials = Yii::$app->params['ozon_integration'][$clientId] ?? null;
        if (empty($credentials))
            return false;

        $key = implode('-',['ozon-label', $clientId, $postingNumber]);

        if (\Yii::$app->cache->exists($key))
            return Yii::$app->response->sendContentAsFile(
                Yii::$app->cache->get($key),
                "$key.pdf",
                ['inline' => true, 'mimeType' => 'application/pdf']
            );

        $client = new Client();
        $url = 'https://api-seller.ozon.ru/v2/posting/fbs/package-label';
        $headers = [
            'Client-Id' =>  $credentials['Client-Id'],
            'Api-Key' => $credentials['Api-Key'] ,
        ];
        $request = $client->createRequest()
            ->addHeaders($headers)
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('post')
            ->setUrl($url)
            ->setData(['posting_number' => [$postingNumber]]);

        $response = $request->send();

        if ($response->isOk) {
            Yii::$app->cache->set($key, $response->content, 604800);
            return Yii::$app->response->sendContentAsFile(
                $response->content,
                'test.pdf',
                ['inline' => true, 'mimeType' => 'application/pdf']
            );
        }
        return true;
    }

}