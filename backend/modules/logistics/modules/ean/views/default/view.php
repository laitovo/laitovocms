<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model core\models\ean\Ean */

$this->title = $model->ean;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список EAN'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');

?>
<div class="ean-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $model->ean], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->ean], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить данный EAN?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ean',
            'carArticle',
            'carTitle',
            'type',
        ],
    ]) ?>

</div>
