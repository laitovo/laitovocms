<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model core\models\ean\Ean */

$this->title = Yii::t('app', 'Добавить EAN');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список EAN'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');

?>
<div class="ean-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
