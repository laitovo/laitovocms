<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model core\models\ean\Ean */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ean-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ean')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carArticle')->textInput() ?>

    <?= $form->field($model, 'carTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList($model->TypesList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
