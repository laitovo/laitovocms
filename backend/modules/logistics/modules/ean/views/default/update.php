<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model core\models\ean\Ean */

$this->title = Yii::t('app', 'Изменить {modelClass}: ', [
    'modelClass' => 'Ean',
]) . $model->ean;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список EAN'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ean, 'url' => ['view', 'id' => $model->ean]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');

$this->render('@backendViews/logistics/views/menu');

?>
<div class="ean-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
