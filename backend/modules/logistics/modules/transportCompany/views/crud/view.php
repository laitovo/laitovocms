<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var yii\web\View $this */
/* @var mixed $model */

$this->title = Yii::t('app', 'Просмотр транспортной компании');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Транспортные компании'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');

?>

<div class="form-group">
    <?= Html::a('<i class="icon wb-edit"></i> ' . Yii::t('app','Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('<i class="icon wb-trash"></i> ' . Yii::t('app','Удалить'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger deleteconfirm',
        'data' => [
            'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
            'method' => 'post',
        ],
    ]) ?>
    <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
</div>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'id',
            'label'     => 'ID',
        ],
        [
            'attribute' => 'name',
            'label'     => 'Наименование',
        ],
        [
            'attribute' => 'trackingLink',
            'label'     => 'Ссылка для отслеживания',
            'format'    => 'raw',
            'value'     => function($data) {
                return in_array($data->trackingLink, [null, ''])  ? null : Html::a($data->trackingLink, $data->trackingLink, ['target' => '_blank']);
            },
        ],
        [
            'attribute' => 'createdAt',
            'label'     => 'Дата создания',
        ],
        [
            'attribute' => 'authorName',
            'label'     => 'Автор',
        ],
        [
            'attribute' => 'updatedAt',
            'label'     => 'Дата изменения',
        ],
        [
            'attribute' => 'updaterName',
            'label'     => 'Редактор',
        ],
    ],
]) ?>