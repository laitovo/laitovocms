<?php

/* @var yii\web\View $this */
/* @var mixed $model */

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Добавить транспортную компанию');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Транспортные компании'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'isNew' => true,
]) ?>
    