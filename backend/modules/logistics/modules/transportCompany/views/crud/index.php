<?php

use backend\widgets\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $provider yii\data\ArrayDataProvider */
/* @var $attributes array */

$this->title = Yii::t('app', 'Транспортные компании');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');

?>

<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'summary' => false,
    'dataProvider' => $provider,
    'show' => $attributes,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'name',
        [
            'attribute' => 'trackingLink',
            'format'    => 'raw',
            'value'     => function($data) {
                return in_array($data->trackingLink, [null, '']) ? null : Html::a($data->trackingLink, $data->trackingLink, ['target' => '_blank']);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ]
    ]
]) ?>
