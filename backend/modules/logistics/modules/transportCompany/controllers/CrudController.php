<?php

namespace backend\modules\logistics\modules\transportCompany\controllers;

use backend\modules\logistics\modules\transportCompany\models\crud\Create;
use backend\modules\logistics\modules\transportCompany\models\crud\Delete;
use backend\modules\logistics\modules\transportCompany\models\crud\Update;
use backend\modules\logistics\modules\transportCompany\models\crud\View;
use backend\modules\logistics\modules\transportCompany\models\crud\Index;
use yii\web\Controller;
use yii\base\Module;
use Yii;
use yii\web\NotFoundHttpException;

class CrudController extends Controller
{
    private $_index;
    private $_view;
    private $_create;
    private $_update;
    private $_delete;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_index  = new Index();
        $this->_view   = new View();
        $this->_create = new Create();
        $this->_update = new Update();
        $this->_delete = new Delete();

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'provider'   => $this->_index->getProvider(),
            'attributes' => $this->_index->getAttributes(),
        ]);
    }

    public function actionView($id)
    {
        if (!($model = $this->_view->findModel($id))) {
            throw new NotFoundHttpException('Элемент не найден');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate()
    {
        $model = $this->_create->createModel();
        if ($id = $this->_create->save($model, Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        if (!($model = $this->_update->findModel($id))) {
            throw new NotFoundHttpException('Элемент не найден');
        }
        if ($id = $this->_update->save($model, Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        if (!($model = $this->_delete->findModel($id))) {
            throw new NotFoundHttpException('Элемент не найден');
        }
        $this->_delete->deleteModel($model);

        return $this->redirect(['index']);
    }
}
