<?php

namespace backend\modules\logistics\modules\transportCompany\models\crud;

use yii\data\ArrayDataProvider;
use Yii;

class Index
{
    private $_transportCompanyManager;

    public function __construct()
    {
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');
    }

    public function getProvider()
    {
        return new ArrayDataProvider([
            'key'       => $this->_transportCompanyManager->getPrimaryKey(),
            'allModels' => $this->_transportCompanyManager->findAll(),
            'sort'      => [
                'attributes' => $this->getAttributes(),
            ],
        ]);
    }

    public function getAttributes()
    {
        return [
            'id',
            'name',
            'trackingLink'
        ];
    }
}


