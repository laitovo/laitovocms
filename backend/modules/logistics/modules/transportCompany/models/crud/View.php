<?php

namespace backend\modules\logistics\modules\transportCompany\models\crud;

use Yii;

class View
{
    private $_transportCompanyManager;

    public function __construct()
    {
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');
    }

    public function findModel($id)
    {
       return $this->_prepareModel($this->_transportCompanyManager->findById($id));
    }

    private function _prepareModel($model)
    {
        if (!$model) {
            return null;
        }

        $id = $this->_transportCompanyManager->getId($model);
        $name = $this->_transportCompanyManager->getName($model);
        $trackingLink = $this->_transportCompanyManager->getTrackingLink($model);
        $createdAt = $this->_transportCompanyManager->getCreatedAt($model);
        $createdAt = !$createdAt ? null : date('d.m.Y H:i:s', $createdAt);
        $authorName = $this->_transportCompanyManager->getRelatedAuthor($model)->name ?? null;
        $updatedAt = $this->_transportCompanyManager->getUpdatedAt($model);
        $updatedAt = !$updatedAt ? null : date('d.m.Y H:i:s', $updatedAt);
        $updaterName = $this->_transportCompanyManager->getRelatedUpdater($model)->name ?? null;

        return (object)[
            'id'           => $id,
            'name'         => $name,
            'trackingLink' => $trackingLink,
            'createdAt'    => $createdAt,
            'authorName'   => $authorName,
            'updatedAt'    => $updatedAt,
            'updaterName'  => $updaterName,
        ];
    }
}


