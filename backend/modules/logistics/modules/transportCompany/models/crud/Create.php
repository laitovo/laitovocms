<?php

namespace backend\modules\logistics\modules\transportCompany\models\crud;

use Yii;

class Create
{
    private $_transportCompanyManager;

    public function __construct()
    {
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');
    }

    public function createModel()
    {
        return $this->_transportCompanyManager->create();
    }

    public function save($model, $params)
    {
        if (!$this->_transportCompanyManager->fillByForm($model, $params) || !$this->_transportCompanyManager->save($model)) {
            return false;
        }

        return $this->_transportCompanyManager->getId($model);
    }
}


