<?php

namespace backend\modules\logistics\modules\transportCompany\models\crud;

use Yii;

class Delete
{
    private $_transportCompanyManager;

    public function __construct()
    {
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');
    }

    public function findModel($id)
    {
        return $this->_transportCompanyManager->findById($id);
    }

    public function deleteModel($model)
    {
        return $this->_transportCompanyManager->delete($model);
    }
}


