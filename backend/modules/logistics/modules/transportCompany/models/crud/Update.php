<?php

namespace backend\modules\logistics\modules\transportCompany\models\crud;

use Yii;

class Update
{
    private $_transportCompanyManager;

    public function __construct()
    {
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');
    }

    public function findModel($id)
    {
        return $this->_transportCompanyManager->findById($id);
    }

    public function save($model, $params)
    {
        if (!$this->_transportCompanyManager->fillByForm($model, $params) || !$this->_transportCompanyManager->save($model)) {
            return false;
        }

        return $this->_transportCompanyManager->getId($model);
    }
}


