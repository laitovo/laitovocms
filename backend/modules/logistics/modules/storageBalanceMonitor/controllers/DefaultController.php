<?php

namespace backend\modules\logistics\modules\storageBalanceMonitor\controllers;

use backend\modules\logistics\modules\storageBalanceMonitor\models\Index;
use backend\modules\logistics\modules\storageBalanceMonitor\models\Replenishment;
use yii\web\Controller;
use yii\base\Module;
use yii\web\Response;
use Yii;

class DefaultController extends Controller
{
    private $_index;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_index = new Index();

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'provider'   => $this->_index->getProvider(),
            'attributes' => $this->_index->getAttributes(),
        ]);
    }

    public function actionSave($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = $this->_index->save($id, Yii::$app->request->post());

        return [
            'status' => $result['status'],
            'message' => $result['message'],
        ];
    }

    /**
     * Функция пополнения
     *
     * @param $article
     * @param $count
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionReplenish($article,$count)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $message = '';
        $status = 'success';
        $data = Replenishment::replenish($article,$count);
        if (!$data) {
            $status = 'error';
        }
        if (!$data['replenished_count']) {
            $status = 'error';
            $message = 'Нельзя на Раскрой выдавать 2 разных артикула!!!';
        }


        return array_merge(['status' => $status, 'message' => $message], $data);
    }
}