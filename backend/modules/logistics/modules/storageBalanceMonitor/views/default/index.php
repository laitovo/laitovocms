<?php

/**
 * @var $provider \yii\data\ArrayDataProvider
 * @var $attributes array
 */

use backend\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = Yii::t('app', 'Монитор контроля остатков');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');

?>

<?php Pjax::begin(['id' => 'pjax-storage-product-types']); ?>

    <?php Yii::$app->view->registerCss('
        .checkbox-custom label::before { 
            box-shadow: 0 0 5px rgba(0,0,0,0.5);
        }
    '); ?>

    <?= GridView::widget([
        'dataProvider' => $provider,
        'show' => array_merge($attributes,['addCount','inProduction','Save','Add']),
        'rowOptions' => function($data) {
            if ($data['hasBalanceControl'] && ($data['factBalance'] < $data['minBalance'])) {
                return ['class' => 'danger'];
            }
            return [];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'label' => Yii::t('app', 'Наименование'),
                'format' => 'raw',
                'value' => function ($data) {
                    $prefix = '';
                    if (preg_match('/OT-(1189)/', $data['article']) === 1) {
                        $prefix = '&nbsp;[реестр по 8]&nbsp;';
                    } else if (preg_match('/OT-1214-47-3/', $data['article']) === 1) {
                        $prefix = '&nbsp;[реестр по 8]&nbsp;';
                    } else if (preg_match('/OT-(2061)/', $data['article']) === 1) {
                        $prefix = '&nbsp;[реестр по 15]&nbsp;';
                    }
                    if ($prefix)
                        return "<span data-toggle='tooltip' data-original-title='$prefix'>{$data['title']}</span>";
                    else
                        return $data['title'];
                }

            ],
            [
                'attribute' => 'article',
                'label' => Yii::t('app', 'Артикул')
            ],
            [
                'attribute' => 'barcode',
                'label' => Yii::t('app', 'Штрихкод')
            ],
            [
                'attribute' => 'factBalance',
                'label' => Yii::t('app', 'Факт-й остаток'),
                'format' => 'raw',
                'value' => function ($data) {
                    $flag = false;
                    if (preg_match('/OT-(1189)/', $data['article']) === 1) {
                        $flag = true;
                    }
                    if ($flag)
                        return "Потребность:&nbsp;[&nbsp;{$data['needToProduction']}&nbsp;]&nbsp;<br>Под&nbsp;заказ:&nbsp;<a href='" . Url::to(['/logistics/report/storage-state-intro-report', 'StorageStateReport[upnArticle]' => $data['article'] ]) . "'>{$data['factBalanceReserved']}</a> <br> Пополнение:&nbsp;<a href='" . Url::to(['/logistics/report/storage-state-intro-report', 'StorageStateReport[upnArticle]' => $data['article']]) . "'>{$data['factBalance']}</a>";
                    else
                        return "Под&nbsp;заказ:&nbsp;{$data['factBalanceReserved']} <br> Пополнение:&nbsp;{$data['factBalance']}";

                }
            ],
            [
                'attribute' => 'inProduction',
                'label' => Yii::t('app', 'В производстве'),
                'format' => 'raw',
                'value' => function ($data) {
                    $toOrders = \backend\modules\laitovo\models\ErpNaryad::find()->where(['and',
                        ['article' => $data['article']],
                        ['is not','logist_id',null],
                        ['is not','order_id',null],
                        ['or',
                            ['status' => null],
                            ['status' => \obmen\models\laitovo\ErpNaryad::STATUS_IN_WORK],
                            ['status' => \obmen\models\laitovo\ErpNaryad::STATUS_IN_PAUSE],
                            ['and',['status' => \obmen\models\laitovo\ErpNaryad::STATUS_READY],['distributed' => 0]],
                        ]
                    ])->count();

                    $flag = false;
                    if (preg_match('/OT-(1189)/', $data['article']) === 1) {
                        $flag = true;
                        $toOrdersOtk = \backend\modules\laitovo\models\ErpNaryad::find()->where(['and',
                            ['article' => $data['article']],
                            ['is not','logist_id',null],
                            ['is not','order_id',null],
                            ['or',
                                ['location_id' => 6],
                                ['start' => 6],
                            ],
                            ['or',
                                ['status' => null],
                                ['status' => \obmen\models\laitovo\ErpNaryad::STATUS_IN_WORK],
                                ['status' => \obmen\models\laitovo\ErpNaryad::STATUS_IN_PAUSE],
                                ['and',['status' => \obmen\models\laitovo\ErpNaryad::STATUS_READY],['distributed' => 0]],
                            ]
                        ])->count();

                        $toStorageOtk = \backend\modules\laitovo\models\ErpNaryad::find()->where(['and',
                            ['article' => $data['article']],
                            ['is not','logist_id',null],
                            ['order_id' => null],
                            ['or',
                                ['location_id' => 6],
                                ['start' => 6],
                            ],
                            ['or',
                                ['status' => null],
                                ['status' => \obmen\models\laitovo\ErpNaryad::STATUS_IN_WORK],
                                ['status' => \obmen\models\laitovo\ErpNaryad::STATUS_IN_PAUSE],
                                ['and',['status' => \obmen\models\laitovo\ErpNaryad::STATUS_READY],['distributed' => 0]],
                            ]
                        ])->count();
                    }


                    $toStorage = \backend\modules\laitovo\models\ErpNaryad::find()->where(['and',
                        ['article' => $data['article']],
                        ['is not','logist_id',null],
                        ['order_id' => null],
                        ['or',
                            ['status' => null],
                            ['status' => \obmen\models\laitovo\ErpNaryad::STATUS_IN_WORK],
                            ['status' => \obmen\models\laitovo\ErpNaryad::STATUS_IN_PAUSE],
                            ['and',['status' => \obmen\models\laitovo\ErpNaryad::STATUS_READY],['distributed' => 0]],
                        ]
                    ])->count();

                    if ($flag) {
                        return "Всего:&nbsp;" . ($toOrders + $toStorage) . "&nbsp;<span title='На участке - ОТК'>(" . ($toOrdersOtk + $toStorageOtk) . ")</span>";
                    } else {
                        return "Под&nbsp;заказ:&nbsp;$toOrders <br> Пополнение:&nbsp;$toStorage";
                    }

                }
            ],
            [
                'attribute' => 'minBalance',
                'label' => Yii::t('app', 'Мин-й остаток'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::input('number','count', $data['minBalance'], [
                        'class' => 'form-control textbox-min-balance',
                        'min' => '0',
                        'data-default-value' => $data['minBalance']
                    ]);
                }
            ],
            [
                'attribute' => 'hasBalanceControl',
                'label' => Yii::t('app', 'Контроль'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::tag('div',
                        Html::checkbox("hasBalanceControl", $data['hasBalanceControl'], [
                            'class' => 'checkbox-has-balance-control',
                            'data-default-value' => $data['hasBalanceControl']
                        ]) .
                        Html::tag('label','', [
                            'for' => 'checkbox-has-balance-control'
                        ]),
                        [
                            'class' => 'checkbox-custom checkbox-primary text-left',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'canBeProduced',
                'label' => Yii::t('app', 'Через пр-во'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::tag('div',
                        Html::checkbox("canBeProduced", $data['canBeProduced'], [
                            'class' => 'checkbox-can-be-produced',
                            'data-default-value' => $data['canBeProduced']
                        ]) .
                        Html::tag('label','', [
                            'for' => 'checkbox-can-be-produced'
                        ]),
                        [
                            'class' => 'checkbox-custom checkbox-primary text-left',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'Save',
                'label' => Yii::t('app', 'Сохранить'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::button(Yii::t('app', 'Сохранить'), [
                        'class' => 'btn btn-success save-button btn-sm',
                        'data-id' => $data['id'],
                        'disabled' => true
                    ]);
                }
            ],
            [
                'attribute' => 'addCount',
                'label' => Yii::t('app', 'Кол-во для пополнения'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::input('number','count', 5, [
                        'class' => 'form-control textbox-add-count',
                        'min' => '0',
                        'data-default-value' => 5
                    ]);
                }
            ],
            [
                'attribute' => 'Add',
                'label' => Yii::t('app', 'Пополнить'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::button(Yii::t('app', 'Пополнить'), [
                        'class' => 'btn btn-primary replenish-button btn-sm',
                        'data-id' => $data['id'],
                        'data-article' => $data['article'],
                        'disabled' => !$data['canBeProduced']  && preg_match('/OT-(1189)/', $data['article']) !== 1,
                    ]);
                }
            ],
        ],
    ]) ?>

    <?php Yii::$app->view->registerJs('
        //Данная функция отвечает за обработку нажатия кнопки "Сохранить"
        $(".save-button").click(function() {
            var id = $(this).data("id");
            var minBalance = $(this).closest("tr").find(".textbox-min-balance").val();
            var hasBalanceControl = $(this).closest("tr").find(".checkbox-has-balance-control").prop("checked") ? 1 : "";
            var canBeProduced = $(this).closest("tr").find(".checkbox-can-be-produced").prop("checked") ? 1 : "";
            $(this).prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:20px;\"></i>");
            $.ajax({
                url: "/logistics/storage-balance-monitor/default/save?id=" + id,
                dataType: "json",
                method: "post",
                data: {
                    "minBalance": minBalance,
                    "hasBalanceControl": hasBalanceControl,
                    "canBeProduced": canBeProduced,
                },
                success: function (data) {
                    $.pjax.reload({container: "#pjax-storage-product-types"});
                    if (!data.status) {
                        notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                        return;
                    }
                    notie.alert(1, data.message ? data.message : "Успешно выполнено", 3);
                },
                error: function() {
                    $.pjax.reload({container: "#pjax-storage-product-types"});
                    notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                }
            });
        });
        
        //Данная функция отвечает за обработку изменения значения полей
        $(".textbox-min-balance, .checkbox-has-balance-control, .checkbox-can-be-produced").change(function() {
            var button = $(this).closest("tr").find(".save-button");
            showSaveButton(button);
        });
        
        //Данная функция отвечает за отображение кнопки "Сохранить"
        function showSaveButton(button) {
            var textbox                   = button.closest("tr").find(".textbox-min-balance");
            var checkboxHasBalanceControl = button.closest("tr").find(".checkbox-has-balance-control");
            var checkboxCanBeProduced     = button.closest("tr").find(".checkbox-can-be-produced");
            var minBalance                = textbox.val();
            var defaultMinBalance         = textbox.data("default-value");
            var hasBalanceControl         = checkboxHasBalanceControl.prop("checked") ? 1 : "";
            var defaultHasBalanceControl  = checkboxHasBalanceControl.data("default-value");
            var canBeProduced             = checkboxCanBeProduced.prop("checked") ? 1 : "";
            var defaultCanBeProduced      = checkboxCanBeProduced.data("default-value");
            if ((minBalance == defaultMinBalance) && (hasBalanceControl == defaultHasBalanceControl) && (canBeProduced == defaultCanBeProduced)) {
                button.prop("disabled", true);
            } else {
                button.prop("disabled", false);
            }
        }
    ', View::POS_END); ?>


<?php Yii::$app->view->registerJs('
    //Данная функция отвечает за обработку нажатия кнопки "Пополнить"
    $(".replenish-button").click(function() {
        var button = $(this);
        var article = $(this).data("article");
        var count = $(this).closest("tr").find(".textbox-add-count").val();
        var handle_function = function(){
            button.prop( "disabled", true );
            $.ajax({
                url: "/logistics/storage-balance-monitor/default/replenish",
                dataType: "json",
                data: {
                    "article": article,
                    "count": count
                },
                success: function (data) {
                    if (data.status != "success") {
                        notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    }
                    notie.alert(1, "Успешно выполнено пополнение артикула <span style=\'color: red\'>" + data.replenished_article + "</span> в количестве " + data.replenished_count + " шт", 15);
                    if (data.print_link) {
                        console.log("ссылка для печати: " + data.print_link);
                    } else {
                        setTimeout(function() { window.location=window.location;},1000);
                    }
                },
                error: function() {
                    notie.alert(3, "Не удалось выполнить команду", 15);
                }
            });
        };
        notie.confirm("Вы действительно хотите пополнить данные позиции?", "Да", "Нет", handle_function);
    });
', \yii\web\View::POS_END);
?>

<?php Pjax::end();?>
