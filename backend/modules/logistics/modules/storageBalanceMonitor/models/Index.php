<?php

namespace backend\modules\logistics\modules\storageBalanceMonitor\models;

use backend\modules\logistics\models\OrderEntry;
use yii\data\ArrayDataProvider;
use Yii;

class Index
{
    private $_getStorageBalance;
    private $_storageProductTypeManager;

    public function __construct()
    {
        $this->_storageProductTypeManager = Yii::$container->get('core\entities\logisticsStorageProductType\StorageProductTypeManager');
        $this->_getStorageBalance         = Yii::$container->get('core\logic\GetStorageBalance');
    }

    public function getProvider()
    {
        $models = $this->_findModels();
        $models = $this->_prepareModels($models);

        return $this->_asProvider($models, $this->getAttributes());
    }

    public function getAttributes()
    {
        return [
            'title',
            'article',
            'barcode',
            'factBalance',
            'minBalance',
            'hasBalanceControl',
            'canBeProduced',
        ];
    }

    public function save($id, $attributes)
    {
        if (!($model = $this->_storageProductTypeManager->findById($id))) {
            return [
                'status'  => false,
                'message' => Yii::t('app', 'Объект не найден')
            ];
        }
        $this->_storageProductTypeManager->setAttributes($model, $attributes);
        if (!$this->_storageProductTypeManager->save($model)) {
            return [
                'status'  => false,
                'message' => Yii::t('app', 'Не удалось сохранить')
            ];
        }

        return [
            'status'  => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    private function _findModels()
    {
        return $this->_storageProductTypeManager->findBy(['hide' => false]);
    }

    private function _prepareModels($models)
    {
        $data = [];
        foreach ($models as $model) {
            $data[] = $this->_prepareModel($model);
        }

        return $data;
    }

    private function _prepareModel($model)
    {
        $row['id']                  = $this->_storageProductTypeManager->getId($model);
        $row['title']               = $this->_storageProductTypeManager->getTitle($model);
        $row['article']             = $this->_storageProductTypeManager->getArticle($model);
        $row['barcode']             = $this->_storageProductTypeManager->getBarcode($model);
        $row['hasBalanceControl']   = $this->_storageProductTypeManager->getHasBalanceControl($model);
        $row['canBeProduced']       = $this->_storageProductTypeManager->getCanBeProduced($model);
        $row['minBalance']          = (int)$this->_storageProductTypeManager->getMinBalance($model);
        $row['factBalance']         = $this->_getStorageBalance->getFactBalanceByArticle($row['article']);
        $row['factBalanceReserved'] = $this->_getStorageBalance->getFactBalanceReservedByArticle($row['article']);
        $row['needToProduction']    = $this->_needToProduction($row['article']);

        return $row;
    }

    private function _needToProduction($article)
    {
        if (preg_match('/OT-(1189)/', $article) === 1) {

            //Нам надо получить заявки, которые еще не упакованы
            $shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
            $shipments = $shipmentManager->findWhereColumn('id', ['logisticsStatus' => 'shipped']);

            //Сначала находим все неотгруженные позиции органайзеров
            return OrderEntry::find()
                ->select('id')
                ->where('order_id is not NULL')
                ->andWhere('is_deleted is NULL')
                ->andWhere(['>', 'order_id', 30000])
                ->andWhere(['article' => $article])
                ->andWhere(['or', 'shipment_id not IN (' . implode(',', $shipments) . ')', 'shipment_id is NULL'])
                ->count();

//            //Далее из этого списка выбираем позиции, которые уже лежат на складе
//            $naryadIds = Naryad::find()
//                ->select('id')
//                ->where(['reserved_id' => $orderEntryIds])
//                ->column();
//
//            $storageIds = StorageState::find()
//                ->select('upn_id')
//                ->where(['upn_id' => $naryadIds])
//                ->column();
//
//            $orderEntryIdsNotInStorage = Naryad::find()
//                ->select('reserved_id')
//                ->where(['id' => $naryadIds])
//                ->andWhere(['not in', 'id', $storageIds])
//                ->column();
//
//            //Находим все заказы, которым принадлежат данные позиции
//            return OrderEntry::find()
//                ->select('order_id')
//                ->where(['id' => $orderEntryIdsNotInStorage])
//                ->count();
        }

        return 0;
    }
    private function _asProvider($models, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $models,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}


