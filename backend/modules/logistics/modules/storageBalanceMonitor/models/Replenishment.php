<?php
namespace backend\modules\logistics\modules\storageBalanceMonitor\models;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpScheme;
use \backend\modules\logistics\models\Naryad;
use \backend\modules\laitovo\models\ErpNaryadFactory;
use common\models\laitovo\ErpProductType;
use \core\logic\CheckStoragePosition;
use Yii;

class Replenishment
{
    /**
     * Функция отвечает за пополнение
     *
     * @param $article
     * @param $count
     * @return array
     * @throws \yii\db\Exception
     */
    public static function replenish($article,$count)
    {
        $connection = \Yii::$app->db;

        #1 Получим схему артикула.
        $start = null;
        foreach (ErpProductType::find()->all() as $product) {
            if ($product->rule && preg_match($product->rule, $article)) {
                $productionScheme = $product->productionScheme;
                $scheme = ErpScheme::findOne($productionScheme->scheme_id);
                $start = $scheme ? $scheme->keytoid[$scheme->fromto[$scheme->idtokey['Start']]] : '';
                break;
            }
        }

        #Если пополняем на раскрой, тогда проверяем, а нет ли на раскрое других артикулов
        if ($start == 19 && ErpNaryad::find()->andWhere(['!=','article',$article])->andWhere(['start' => 19])->andWhere(['or',['status' => ErpNaryad::STATUS_IN_WORK], ['status' => null]])->exists()) {
            return [
                'replenished_count'   => 0,
                'replenished_article' => $article,
                'print_link' => null
            ];
        }


        $transaction = $connection->beginTransaction();
        try {
            $ids = [];
            for ($i = 1; $i <= $count; $i++) {
                $naryad = new Naryad();
                $naryad->article = $article;
                $naryad->team_id = \Yii::$app->team->id;
                $naryad->comment = "Пополнение склада";
                $naryad->source_id = 1;
                $naryad->save();

                $title = CheckStoragePosition::getStorageTitle($article);
                $factory = new ErpNaryadFactory();
                $workOrderBarcode = BarcodeHelper::upnToWorkOrderBarcode($naryad->barcode);
                $erpNaryad = $factory->create($article,$title,$naryad->id,$workOrderBarcode,$naryad->comment,'','',7);
                $ids[] = $erpNaryad->id;
            }

            $transaction->commit();

            return [
                'replenished_count'   => $count,
                'replenished_article' => $article,
                'print_link' => null
                //'print_link' => ( preg_match('/OT-1189/', $article) === 1 ? Yii::$app->urlManager->createAbsoluteUrl(['/laitovo/erp/print','id' => implode(",", $ids)]) : null )
            ];//test

        }catch (\Exception $e) {
            $transaction->rollBack();
        }

        return [];
    }
}