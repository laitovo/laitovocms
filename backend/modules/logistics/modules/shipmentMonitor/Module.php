<?php

namespace backend\modules\logistics\modules\shipmentMonitor;

/**
 * logistics module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\logistics\modules\shipmentMonitor\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        \Yii::$app->layout='2columns';
        parent::init();

        // custom initialization code goes here
    }
}
