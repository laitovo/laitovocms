<?php

namespace backend\modules\logistics\modules\shipmentMonitor\assets;

use yii\web\AssetBundle;

class ViewAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/logistics/modules/shipmentMonitor/assets/sources';

    public $css = [
        'css/view.css',
    ];
    public $js = [
        'js/view.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
//    public $publishOptions = [
//        'forceCopy'=>true,
//    ];
}
