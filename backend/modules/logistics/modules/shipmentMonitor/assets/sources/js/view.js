$(document).ready(function() {
    crossDeletedPositions();
    showStorageRequestButton();
});

$("#pjax-positions").on("pjax:end", function() {
    crossDeletedPositions();
});

//Данная функция отвечает за обработку нажатия кнопки "Провести в 1С"
$("#account-button").click(function() {
    var shipmentId = $(this).data("shipment-id");
    var handle_function = function () {
        $.ajax({
            url: "/logistics/shipment-monitor/default/account",
            dataType: "json",
            data: {
                "id": shipmentId
            },
            success: function (data) {
                if (!data.status) {
                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    return;
                }
                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                $("#account-button").remove();
            },
            error: function() {
                notie.alert(3, "Не удалось выполнить команду.", 3);
            }
        });
    };
    notie.confirm("Документ-реализация действительно проведен в 1С?", "Да", "Нет", handle_function);
});

//Данная функция отвечает за обработку нажатия кнопки "Обработать приказ"
$("#handle-button").click(function() {
    var shipmentId = $(this).data("shipment-id");
    var handle_function = function () {
        $.ajax({
            url: "/logistics/shipment-monitor/logist/handle",
            dataType: "json",
            data: {
                "shipmentId": shipmentId
            },
            success: function (data) {
                if (!data.status) {
                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду. Проверьте заполненность полей. Проверьте наличие всех позиции на складе.", 3);
                    return;
                }
                preventEditing();
                $("#dates-block").html("Обработан: " + data.handledAt);
                notie.alert(1, "Успешно выполнено", 1);
            },
            error: function() {
                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
            }
        });
    };
    notie.confirm("Вы действительно хотите обработать приказ?", "Да", "Нет", handle_function);
});

//Данная функция отвечает за обработку нажатия кнопки "Отменить приказ"
$("#cancel-button").click(function() {
    var shipmentId = $(this).data("shipment-id");
    var handle_function = function () {
        $.ajax({
            url: "/logistics/shipment-monitor/logist/cancel",
            dataType: "json",
            data: {
                "shipmentId": shipmentId
            },
            success: function (data) {
                if (data.status != "success") {
                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    return;
                }
                preventEditing();
                notie.alert(1, "Успешно выполнено", 1);
            },
            error: function() {
                notie.alert(3, "Не удалось выполнить команду", 3);
            }
        });
    };
    notie.confirm("Вы действительно хотите отменить приказ?", "Да", "Нет", handle_function);
});

//Данная функция отвечает за обработку нажатия кнопки "Составить запрос на склад"
$("#button-prepare-storage-request").click(function() {
    $("#modal-storage-request").modal("show");
});

//Данная функция отвечает за обработку нажатия кнопки "Отправить" в модальном окне "Запрос на склад"
$("#button-send-storage-request").click(function() {
    var shipmentId = $(this).data("shipment-id");
    var hasWeighing = $("#checkbox-has-weighing").prop("checked") ? 1 : '';
    var hasSizing = $("#checkbox-has-sizing").prop("checked") ? 1 : '';
    var update_function = function () {
        $.ajax({
            url: "/logistics/shipment-monitor/logist/send-storage-request",
            dataType: "json",
            data: {
                "shipmentId": shipmentId,
                "hasWeighing": hasWeighing,
                "hasSizing": hasSizing
            },
            success: function (data) {
                if (!data.status) {
                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    return;
                }
                if (hasWeighing) {
                    $('.weight-status').html("Со взвешиванием");
                    $('.weight-edit-block').removeClass('hide');
                }
                if (hasSizing) {
                    $('.size-edit-block').removeClass('hide');
                }
                $("#button-prepare-storage-request").addClass("hide");
                $("#button-cancel-storage-request").removeClass("hide");
                $("#modal-storage-request").modal("hide");
                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
            },
            error: function() {
                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
            }
        });
    };
    notie.confirm('Вы действительно хотите отправить запрос на склад?', "Да", "Нет", update_function);
});

//Данная функция отвечает за обработку нажатия кнопки "Отменить запрос на склад"
$("#button-cancel-storage-request").click(function() {
    var shipmentId = $(this).data("shipment-id");
    var update_function = function () {
        $.ajax({
            url: "/logistics/shipment-monitor/logist/cancel-storage-request",
            dataType: "json",
            data: {
                "shipmentId": shipmentId
            },
            success: function (data) {
                if (!data.status) {
                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    return;
                }
                $('.weight-status').html("Без взвешивания");
                $('.weight-edit-block').addClass('hide');
                $('.size-edit-block').addClass('hide');
                $("#button-prepare-storage-request").removeClass("hide");
                $("#button-cancel-storage-request").addClass("hide");
                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
            },
            error: function() {
                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
            }
        });
    };
    notie.confirm('Вы действительно хотите отменить запрос на склад?', "Да", "Нет", update_function);
});

//Данная функция отвечает за обработку нажатия кнопки "Редактировать" в блоке для редактирования свойства
$(".edit-block .edit-button").click(function() {
    savePrevValue($(this).closest(".edit-block").find(".property-field"));
    changeEditBlockState($(this).closest(".edit-block"));
});

//Данная функция отвечает за обработку нажатия кнопки "Отменить" в блоке для редактирования свойства
$(".edit-block .cancel-button").click(function() {
    backToPrevValue($(this).closest(".edit-block").find(".property-field"));
    changeEditBlockState($(this).closest(".edit-block"));
});

//Данная функция отвечает за сохранение наименования транспортной компании
$(".transport-company-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var shipmentId = property_field.data("shipment-id");
    var transportCompanyId = property_field.val();
    $.ajax({
        url: "/logistics/shipment-monitor/logist/save-transport-company",
        dataType: "json",
        data: {
            "shipmentId": shipmentId,
            "transportCompanyId": transportCompanyId
        },
        success: function (data) {
            if (data.status != "success") {
                notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                return;
            }
            notie.alert(1, "Успешно сохранено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Не удалось сохранить", 3);
        }
    });
});

//Данная функция отвечает за сохранение комментария
$(".comment-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var shipmentId = property_field.data("shipment-id");
    var comment = property_field.val();
    $.ajax({
        url: "/logistics/shipment-monitor/logist/save-comment",
        dataType: "json",
        data: {
            "shipmentId": shipmentId,
            "comment": comment
        },
        success: function (data) {
            if (data.status != "success") {
                notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                return;
            }
            notie.alert(1, "Успешно сохранено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Не удалось сохранить", 3);
        }
    });
});

//Данная функция отвечает за сохранение даты отгрузки
$(".shipment-date-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var shipmentId = property_field.data("shipment-id");
    var shipmentDate = property_field.val();
    $.ajax({
        url: "/logistics/shipment-monitor/logist/save-shipment-date",
        dataType: "json",
        data: {
            "shipmentId": shipmentId,
            "shipmentDate": shipmentDate
        },
        success: function (data) {
            if (data.status != "success") {
                notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                return;
            }
            notie.alert(1, "Успешно сохранено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Не удалось сохранить", 3);
        }
    });
});

//Данная функция отвечает за сохранение трек-кода
$(".track-code-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var shipmentId = property_field.data("shipment-id");
    var trackCode = property_field.val();
    $.ajax({
        url: "/logistics/shipment-monitor/logist/save-track-code",
        dataType: "json",
        data: {
            "shipmentId": shipmentId,
            "trackCode": trackCode
        },
        success: function (data) {
            if (data.status != "success") {
                notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                return;
            }
            notie.alert(1, "Успешно сохранено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Не удалось сохранить", 3);
        }
    });
});

//Данная функция отвечает за сохранение веса
$(".weight-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var shipmentId = property_field.data("shipment-id");
    var weight = property_field.val();
    $.ajax({
        url: "/logistics/shipment-monitor/logist/save-weight",
        dataType: "json",
        data: {
            "shipmentId": shipmentId,
            "weight": weight
        },
        success: function (data) {
            if (!data.status) {
                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                return;
            }
            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
        }
    });
});

//Данная функция отвечает за сохранение габаритов
$(".size-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var shipmentId = property_field.data("shipment-id");
    var size = property_field.val();
    $.ajax({
        url: "/logistics/shipment-monitor/logist/save-size",
        dataType: "json",
        data: {
            "shipmentId": shipmentId,
            "size": size
        },
        success: function (data) {
            if (!data.status) {
                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                return;
            }
            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
        }
    });
});

//Данная функция отвечает за сохранение суммы доставки
$(".delivery-price-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var shipmentId = property_field.data("shipment-id");
    var deliveryPrice = property_field.val();
    var isDeliveryFree = $("#checkbox-free-delivery").prop("checked") ? 1 : '';
    $.ajax({
        url: "/logistics/shipment-monitor/logist/save-delivery-price",
        dataType: "json",
        data: {
            "shipmentId": shipmentId,
            "deliveryPrice": deliveryPrice,
            "isDeliveryFree": isDeliveryFree
        },
        success: function (data) {
            if (data.status != "success") {
                notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                return;
            }
            notie.alert(1, "Успешно сохранено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Не удалось сохранить", 3);
        }
    });
});

//Данная функция отвечает за сохранение суммы доставки
$("#checkbox-free-delivery").change(function() {
    var shipmentId = $(this).data("shipment-id");
    var isDeliveryFree = $(this).prop("checked") ? 1 : '';
    var deliveryPrice = '';
    var checkbox = $(this);
    checkbox.prop("checked", !isDeliveryFree);
    var handle_function = function () {
        checkbox.prop("checked", isDeliveryFree);
        $.ajax({
            url: "/logistics/shipment-monitor/logist/save-delivery-price",
            dataType: "json",
            data: {
                "shipmentId": shipmentId,
                "deliveryPrice": deliveryPrice,
                "isDeliveryFree": isDeliveryFree
            },
            success: function (data) {
                if (data.status != "success") {
                    notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                    return;
                }
                if (isDeliveryFree) {
                    $("#checkbox-free-delivery").prop("checked", true);
                    $(".delivery-price-edit-block .property-field").val(0);
                    $(".delivery-price-edit-block .edit-block:not(.closed) .cancel-button").click();
                    $(".delivery-price-edit-block .edit-block .edit-button").addClass("hide");
                } else {
                    $("#checkbox-free-delivery").prop("checked", false);
                    $(".delivery-price-edit-block .property-field").val("");
                    $(".delivery-price-edit-block .edit-block .edit-button").removeClass("hide");
                }
                notie.alert(1, "Успешно сохранено", 1);
            },
            error: function() {
                notie.alert(3, "Не удалось сохранить", 3);
            }
        });
    };
    notie.confirm("Вы уверены, что доставка" + (isDeliveryFree ? "" : " не") + " бесплатна?", "Да", "Нет", handle_function);
});

//Данная функция отвечает за сохранение свойства - наложенный платеж
$("#checkbox-cash-on-delivery").change(function() {
    var shipmentId = $(this).data("shipment-id");
    var isCashOnDelivery = $(this).prop("checked") ? 1 : '';
    var checkbox = $(this);
    checkbox.prop("checked", !isCashOnDelivery);
    var handle_function = function () {
        checkbox.prop("checked", isCashOnDelivery);
        $.ajax({
            url: "/logistics/shipment-monitor/logist/save-cash-on-delivery",
            dataType: "json",
            data: {
                "shipmentId": shipmentId,
                "isCashOnDelivery": isCashOnDelivery
            },
            success: function (data) {
                if (data.status != "success") {
                    notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                    return;
                }
                if (isCashOnDelivery) {
                    $("#checkbox-cash-on-delivery").prop("checked", true);
                } else {
                    $("#checkbox-cash-on-delivery").prop("checked", false);
                }
                notie.alert(1, "Успешно сохранено", 1);
            },
            error: function() {
                notie.alert(3, "Не удалось сохранить", 3);
            }
        });
    };
    notie.confirm("Вы уверены, что оплата будет " + (isCashOnDelivery ? "" : "НЕ") + " наложным платежом?", "Да", "Нет", handle_function);
});

//Данная функция отвечает за переключение галочек в модальном окне "Запрос на склад"
$("#checkbox-has-weighing, #checkbox-has-sizing").change(function() {
    showStorageRequestButton();
});

//Данная функция отвечает за отображение кнопки "Отправить" в модальном окне "Запрос на склад"
function showStorageRequestButton() {
    if (!$("#checkbox-has-weighing").prop("checked") && !$("#checkbox-has-sizing").prop("checked")) {
        $("#button-send-storage-request").prop("disabled", true);
    } else {
        $("#button-send-storage-request").prop("disabled", false);
    }
}

//Данная функция отвечает за раскрытие/закрытие блока для редактирования свойства
function changeEditBlockState(edit_block) {
    var property_field = edit_block.find(".property-field");
    if (edit_block.hasClass("closed")) {
        edit_block.find(".edit-button").addClass("hidden");
        edit_block.find(".save-button, .cancel-button").removeClass("hidden");
        property_field.prop("disabled", false).focus();
        edit_block.removeClass("closed").addClass("opened");
    } else {
        edit_block.find(".edit-button").removeClass("hidden");
        edit_block.find(".save-button, .cancel-button").addClass("hidden");
        property_field.prop("disabled", true);
        edit_block.removeClass("opened").addClass("closed");
    }
}

//Данная функция отвечает за сохранение текущего состояния свойства
function savePrevValue(property_field) {
    property_field.data("prev-value", property_field.val());
}

//Данная функция отвечает за возвращение свойства в предыдущее состояние
function backToPrevValue(property_field) {
    property_field.val(property_field.data("prev-value"));
}

//Данная функция отвечает за проверку было ли изменено свойство
function isValueUpdated(property_field) {
    return property_field.val() !== property_field.data("prev-value");
}

//Данная функция запрещает редактирование приказа
function preventEditing() {
    $('.edit-block:not(.closed) .cancel-button').click();
    $('.hide-if-cant-edit').addClass('hide');
    $('#file-input').fileinput('disable');
}

//Данная функция зачеркивает удалённые позиции в таблице "ТОВАР"
function crossDeletedPositions() {
    $("tr.crossed").each(function(){
        var deletionInfo = $(this).data("deletion-info");
        $(this).before("<tr><td colspan='6'><div style='position:relative;top:25px;opacity:0.7;height:3px;background:red;' data-toggle='tooltip' data-title='"+deletionInfo+"'></div></td></tr>");
    });
    $("#pjax-positions [data-toggle=tooltip]").tooltip();
}
