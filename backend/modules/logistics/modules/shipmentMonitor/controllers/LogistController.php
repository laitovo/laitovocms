<?php

namespace backend\modules\logistics\modules\shipmentMonitor\controllers;

use backend\modules\logistics\modules\shipmentMonitor\models\LogistService;
use yii\web\Controller;
use Exception;
use yii\web\Response;
use Yii;
use yii\base\Module;

class LogistController extends Controller
{
    private $_logistService;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_logistService = new LogistService();

        parent::__construct($id, $module, $config);
    }

    /**
     * Пометка что логист завершил работу над отгрузкой
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @return array
     */
    public function actionHandle($shipmentId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = $this->_logistService->handle($shipmentId);

        return [
            'status' => $result['status'],
            'handledAt' => $result['handledAt']
        ];
    }

    /**
     * Отмена отгрузки
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @return array
     */
    public function actionCancel($shipmentId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_logistService->cancel($shipmentId);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status,
        ];
    }

    /**
     * Отправка запроса на склад
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param bool $hasWeighing Необходимость взвешивания
     * @param bool $hasSizing Необходимость измерения габаритов
     * @return array
     */
    public function actionSendStorageRequest($shipmentId, $hasWeighing, $hasSizing)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = $this->_logistService->sendStorageRequest($shipmentId, $hasWeighing, $hasSizing);

        return [
            'status' => $result['status'],
            'message' => $result['message'],
        ];
    }

    /**
     * Отмена запроса на склад
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @return array
     */
    public function actionCancelStorageRequest($shipmentId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = $this->_logistService->cancelStorageRequest($shipmentId);

        return [
            'status' => $result['status'],
            'message' => $result['message'],
        ];
    }

    /**
     * Сохранение наименования транспортной компании
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param string $transportCompanyId Идентификатор транспортной компании
     * @return array
     */
    public function actionSaveTransportCompany($shipmentId, $transportCompanyId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_logistService->saveTransportCompany($shipmentId, $transportCompanyId);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Сохранение комментария
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param string $comment Комментарий
     * @return array
     */
    public function actionSaveComment($shipmentId, $comment)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_logistService->saveComment($shipmentId, $comment);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Сохранение даты отгрузки
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param string $shipmentDate Дата отгрузки
     * @return array
     */
    public function actionSaveShipmentDate($shipmentId, $shipmentDate)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_logistService->saveShipmentDate($shipmentId, $shipmentDate);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Сохранение трэк-кода
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param string $trackCode Трэк-код
     * @return array
     */
    public function actionSaveTrackCode($shipmentId, $trackCode)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_logistService->saveTrackCode($shipmentId, $trackCode);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Сохранение веса
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param double $weight Вес
     * @return array
     */
    public function actionSaveWeight($shipmentId, $weight)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'status' => $this->_logistService->saveWeight($shipmentId, $weight)
        ];
    }

    /**
     * Сохранение габаритов
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param double $size Габариты
     * @return array
     */
    public function actionSaveSize($shipmentId, $size)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'status' => $this->_logistService->saveSize($shipmentId, $size)
        ];
    }

    /**
     * Сохранение суммы доставки
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param double $deliveryPrice Сумма доставки
     * @return array
     */
    public function actionSaveDeliveryPrice($shipmentId, $deliveryPrice, $isDeliveryFree)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_logistService->saveDeliveryPrice($shipmentId, $deliveryPrice, $isDeliveryFree);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Сохранение суммы доставки
     * (ajax-функция)
     *
     * @param int $shipmentId Идентификатор отгрузки
     * @param boolean $isCashOnDelivery Отправлено наложенным платежом
     * @return array
     */
    public function actionSaveCashOnDelivery($shipmentId, $isCashOnDelivery)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_logistService->saveCashOnDelivery($shipmentId, $isCashOnDelivery);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Загрузка нового документа
     * (ajax-функция)
     *
     * @return array
     * @throws Exception
     */
    public function actionUploadDocument()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = $this->_logistService->uploadDocument(Yii::$app->request->post('shipmentId'), 'file');
        if (!$data) {
            throw new Exception('Не удалось загрузить документ');
        }

        return array_merge(['status' => 'success'], $data);
    }

    /**
     * Удаление документа
     * (ajax-функция)
     *
     * @return array
     * @throws Exception
     */
    public function actionDeleteDocument()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = $this->_logistService->deleteDocument(Yii::$app->request->post('id'));
        if (!$result) {
            throw new Exception('Не удалось удалить документ');
        }

        return [
            'status' => 'success'
        ];
    }
}