<?php

namespace backend\modules\logistics\modules\shipmentMonitor\controllers;

use backend\modules\logistics\modules\shipmentMonitor\models\Accounting;
use backend\modules\logistics\modules\shipmentMonitor\models\Bind;
use backend\modules\logistics\modules\shipmentMonitor\models\Index;
use backend\modules\logistics\modules\shipmentMonitor\models\View;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\Module;
use Yii;
use yii\web\Response;

class DefaultController extends Controller
{
    private $_index;
    private $_view;
    private $_bind;
    private $_accounting;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_index      = new Index();
        $this->_view       = new View();
        $this->_bind       = new Bind();
        $this->_accounting = new Accounting();

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'provider'             => $this->_index->getProvider(),
            'attributes'           => $this->_index->getAttributes(),
            'attributesWithLabels' => $this->_index->getAttributesWithLabels(),
        ]);
    }

    public function actionView($id)
    {
        if (!($model = $this->_view->findModel($id))) {
            throw new NotFoundHttpException('Элемент не найден');
        }

        return $this->render('view', [
            'model'                => $model,
            'provider'             => $this->_view->getProvider($id),
            'attributes'           => $this->_view->getAttributes(),
            'fileWidgetData'       => $this->_view->getFileWidgetData($id),
            'transportCompanies'   => $this->_view->getTransportCompanies(),
        ]);
    }

    public function actionBind($id = null)
    {
        $this->_bind->updateBindings([
            'idsYes'     => Yii::$app->request->post('idsYes'),
            'idsNo'      => Yii::$app->request->post('idsNo'),
            'idsYesNew'  => Yii::$app->request->post('idsYesNew'),
            'idsNoNew'   => Yii::$app->request->post('idsNoNew'),
            'onlyAdd'    => Yii::$app->request->post('onlyAdd'),
            'onlyRemove' => Yii::$app->request->post('onlyRemove'),
        ]);

        return $this->render('bind', [
            'id'          => $id,
            'providerYes' => $this->_bind->getProviderYes($id),
            'providerNo'  => $this->_bind->getProviderNo($id),
            'idsYes'      => $this->_bind->getIdsYes(),
            'idsNo'       => $this->_bind->getIdsNo(),
        ]);
    }

    public function actionSaveBindings($id = null)
    {
        if (!$shipmentId = $this->_bind->saveBindings([
            'shipmentId' => $id,
            'idsYes'     => Yii::$app->request->post('idsYes'),
            'idsNo'      => Yii::$app->request->post('idsNo'),
        ])) {
            Yii::$app->session->setFlash('error', $this->_bind->getSavingError());
            return $id ? $this->redirect(['bind','id' => $id]) : $this->redirect(['bind']);
        } else {
            return $this->redirect(['view', 'id' => $shipmentId]);
        }
    }

    public function actionAccounting()
    {
        return $this->render('accounting', [
            'provider'             => $this->_accounting->getProvider(),
            'attributes'           => $this->_accounting->getAttributes(),
            'attributesWithLabels' => $this->_accounting->getAttributesWithLabels(),
        ]);
    }

    public function actionAccount($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = $this->_accounting->account($id);

        return [
            'status' => $data['status'],
            'message' => $data['message']
        ];
    }
}