<?php

namespace backend\modules\logistics\modules\shipmentMonitor\models;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use Yii;
use yii\data\ArrayDataProvider;
use Exception;

class Bind
{
    private $_shipmentManager;
    private $_idsYes = [];
    private $_idsNo = [];

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function getProviderYes($shipmentId)
    {
        //todo: сделать через OrderEntryManager
        $orderEntries = OrderEntry::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['or',
                ['and',
                    ['shipment_id' => $shipmentId],
                    ['not', ['shipment_id' => null]]
                ],
                ['in', 'id', $this->_idsYes]
            ],
            ['not in', 'id', $this->_idsNo]
        ])->all();

        return $this->_getOrdersProviderByOrderEntries($orderEntries);
    }

    public function getProviderNo($shipmentId)
    {
        $orders = Order::find()->where(['and',['processed' => true],['shipped' => null]])->all();
        $orderIds = [];
        foreach ($orders as $order) {
            $orderIds[] = $order->id;
        }
        $orderEntries = OrderEntry::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['in', 'order_id', $orderIds],
            ['or',
                ['shipment_id' => null],
                ['in', 'id', $this->_idsNo]
            ],
            ['not in', 'id', $this->_idsYes]
        ])->all();

        return $this->_getOrdersProviderByOrderEntries($orderEntries);
    }

    public function getIdsYes()
    {
        return $this->_idsYes;
    }

    public function getIdsNo()
    {
        return $this->_idsNo;
    }

    public function updateBindings($params)
    {
        $params = $this->_prepareParams($params);
        if ($params['onlyAdd']) {
            $this->_addBindings($params);
        } elseif ($params['onlyRemove']) {
            $this->_removeBindings($params);
        }
    }

    public function saveBindings($params)
    {
        $params = $this->_prepareParams($params);
        $ids = $this->_getOrderEntryIds($params['shipmentId']);
        $ids = $this->_updateOrderEntryIds($ids, $params['idsYes'], $params['idsNo']);
        if (!$params['shipmentId'] && !count($ids)) {
            return false;
        }
        $shipmentId = $params['shipmentId'] ?: $this->_createShipment();
        $this->_saveOrderEntryIds($shipmentId, $ids);

        return count($ids) ? $shipmentId : false;
    }

    public function getSavingError()
    {
        return Yii::t('app', 'Приказ на отгрузку должен содержать хотя бы одну позицию');
    }

    private function _getOrderEntryIds($shipmentId)
    {
        $orderEntryIds = [];
        if ($shipmentId) {
            $orderEntries = OrderEntry::find()->where(['and',
                ['or',
                    ['is_deleted' => false],
                    ['is_deleted' => null]
                ],
                ['shipment_id' => $shipmentId]
            ])->all();
            foreach ($orderEntries as $orderEntry) {
                $orderEntryIds[] = $orderEntry->id;
            }
        }

        return $orderEntryIds;
    }

    private function _updateOrderEntryIds($orderEntryIds, $idsYes, $idsNo)
    {
        $orderEntryIds = array_merge($orderEntryIds, $idsYes);
        $orderEntryIds = array_diff($orderEntryIds, $idsNo);

        return array_unique($orderEntryIds);
    }

    private function _saveOrderEntryIds($shipmentId, $orderEntryIds)
    {
        $orderEntries = OrderEntry::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['shipment_id' => $shipmentId],
            ['not in', 'id', $orderEntryIds],
        ])->all();
        foreach ($orderEntries as $orderEntry) {
            $orderEntry->shipment_id = null;
            $orderEntry->save();
        }
        foreach ($orderEntryIds as $orderEntryId) {
            $orderEntry = OrderEntry::findOne($orderEntryId);
            $orderEntry->shipment_id = $shipmentId;
            $orderEntry->save();
        }
    }

    private function _createShipment()
    {
        $entity = $this->_shipmentManager->create();
        if (!$this->_shipmentManager->save($entity)) {
            throw new Exception('Не удалось создать приказ на отгрузку');
        }

        return $this->_shipmentManager->getId($entity);
    }

    private function _addBindings($params)
    {
        $params['idsNoNew'] = [];
        $this->_idsYes = array_diff(array_merge($params['idsYes'], $params['idsYesNew']), $params['idsNoNew']);
        $this->_idsNo  = array_diff(array_merge($params['idsNo'], $params['idsNoNew']), $params['idsYesNew']);
    }

    private function _removeBindings($params)
    {
        $params['idsYesNew'] = [];
        $this->_idsYes = array_diff(array_merge($params['idsYes'], $params['idsYesNew']), $params['idsNoNew']);
        $this->_idsNo  = array_diff(array_merge($params['idsNo'], $params['idsNoNew']), $params['idsYesNew']);
    }

    private function _prepareParams($params)
    {
        if (empty($params['idsYes'])) {
            $params['idsYes'] = [];
        }
        if (empty($params['idsNo'])) {
            $params['idsNo'] = [];
        }
        if (empty($params['idsYesNew'])) {
            $params['idsYesNew'] = [];
        }
        if (empty($params['idsNoNew'])) {
            $params['idsNoNew'] = [];
        }
        if (empty($params['onlyAdd'])) {
            $params['onlyAdd'] = null;
        }
        if (empty($params['onlyRemove'])) {
            $params['onlyRemove'] = null;
        }
        if (empty($params['shipmentId'])) {
            $params['shipmentId'] = null;
        }

        return $params;
    }

    private function _getOrdersProviderByOrderEntries($orderEntries)
    {
        $orders = [];
        foreach ($orderEntries as $orderEntry) {
            if (!isset($orders[$orderEntry->order_id])) {
                $order = $orderEntry->order;
                $orders[$orderEntry->order_id]['id'] = $order->id;
                $orders[$orderEntry->order_id]['number'] = $order->source_innumber;
                $orders[$orderEntry->order_id]['orderEntries'] = [];
            }
            $orders[$orderEntry->order_id]['orderEntries'][] = $orderEntry;
        }

        return $this->_asProvider($orders, ['number']);
    }

    private function _asProvider($models, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $models,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}


