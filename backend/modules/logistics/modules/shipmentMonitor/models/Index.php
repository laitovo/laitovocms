<?php

namespace backend\modules\logistics\modules\shipmentMonitor\models;

use yii\data\ArrayDataProvider;
use Yii;

class Index
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function getProvider()
    {
        $models = $this->_shipmentManager->findAll();
        $models = $this->_prepareModels($models);
        $models = $this->_sortByLogisticsStatus($models);

        return $this->_asProvider($models, $this->getAttributes());
    }

    public function getAttributes()
    {
        return [
            'id',
            'logisticsStatus',
            'hasWeighing'
        ];
    }

    public function getAttributesWithLabels()
    {
        return [
            ['attribute' => 'id', 'label' => 'Отгрузка №'],
            ['attribute' => 'logisticsStatus', 'label' => 'Статус'],
        ];
    }

    private function _prepareModels($models)
    {
        $preparedModels = [];
        foreach ($models as $model) {
            $preparedModels[] = $this->_prepareModel($model);
        }

        return $preparedModels;
    }

    private function _prepareModel($model)
    {
        if (!$model) {
            return null;
        }
        $preparedModel['id']                      = $this->_shipmentManager->getId($model);
        $preparedModel['logisticsStatus']         = $this->_shipmentManager->getLogisticsStatusLabel($model);
        $preparedModel['logisticsStatusPriority'] = $this->_shipmentManager->getLogisticsStatusPriority($model);
        $preparedModel['isHandled']               = $this->_shipmentManager->isHandled($model);

        return $preparedModel;
    }

    private function _sortByLogisticsStatus($models, $ascOrDesc = 'ASC')
    {
        $ascOrDesc = strtoupper($ascOrDesc);
        if (!in_array($ascOrDesc, ['ASC', 'DESC'])) {
            $ascOrDesc = 'ASC';
        }
        uasort($models, function ($a, $b) use ($ascOrDesc) {
            switch (true) {
                case $a['logisticsStatusPriority'] > $b['logisticsStatusPriority']:
                    return $ascOrDesc == 'ASC' ? 1 : -1;
                case $a['logisticsStatusPriority'] < $b['logisticsStatusPriority']:
                    return $ascOrDesc == 'ASC' ? -1 : 1;
                default:
                    return 0;
            }
        });

        return $models;
    }

    private function _asProvider($models, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $models,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}


