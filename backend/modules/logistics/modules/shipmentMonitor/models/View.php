<?php

namespace backend\modules\logistics\modules\shipmentMonitor\models;

use common\models\User;
use core\logic\ShipmentPositionsData;
use Yii;
use yii\data\ArrayDataProvider;

class View
{
    private $_shipmentManager;
    private $_transportCompanyManager;
    private $_shipmentDocumentManager;
    private $_fileManager;

    public function __construct()
    {
        $this->_shipmentManager         = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');
        $this->_shipmentDocumentManager = Yii::$container->get('core\entities\logisticsShipmentDocument\ShipmentDocumentManager');
        $this->_fileManager             = Yii::$app->fileManager;
    }

    public function findModel($id)
    {
        $model = $this->_shipmentManager->findById($id);

        return $this->_prepareShipment($model);
    }

//    public function getProvider($id)
//    {
//        $model = $this->_shipmentManager->findById($id);
//        if (!$this->_shipmentManager->isCanceled($model)) {
//            $orderEntries = $this->_shipmentManager->getRelatedOrderEntries($model);
//        } else {
//            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesFromLog($model);
//        }
//        $orderEntries = $this->_prepareOrderEntries($orderEntries);
//
//        return $this->_asProvider($orderEntries, $this->getAttributes());
//    }

    public function getProvider($id)
    {
        $model = $this->_shipmentManager->findById($id);
        if (!$this->_shipmentManager->isCanceled($model)) {
            $orderEntries = $this->_shipmentManager->getRelatedOrderEntries($model);
        } else {
            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesFromLog($model);
        }

        return $this->_getOrdersProviderByOrderEntries($orderEntries);
    }

    public function getAttributes()
    {
        return [
            'orderNumber',
            'transportCompanyName',
            'article',
            'name',
            'size',
            'price',
        ];
    }

    public function getAttributesWithLabels()
    {
        return [
            ['attribute' => 'orderNumber', 'label' => '№ Заказа'],
            ['attribute' => 'transportCompanyName', 'label' => 'Транспортная компания'],
            ['attribute' => 'article', 'label' => 'Артикул'],
            ['attribute' => 'name', 'label' => 'Наименование'],
            ['attribute' => 'size', 'label' => 'Габариты'],
            ['attribute' => 'price', 'label' => 'Цена'],
        ];
    }

    public function getFileWidgetData($id)
    {
        $shipment                      = $this->_shipmentManager->findById($id);
        $shipmentDocuments             = $this->_shipmentManager->getRelatedShipmentDocuments($shipment);
        $shipmentDocuments             = $this->_prepareShipmentDocuments($shipmentDocuments);
        $shipmentDocumentsWidgetConfig = $this->_asFileWidgetData($shipmentDocuments);

        return $shipmentDocumentsWidgetConfig;
    }

    public function getTransportCompanies()
    {
        $data = [];
        foreach($this->_transportCompanyManager->findAll() as $transportCompany) {
            $data[$this->_transportCompanyManager->getId($transportCompany)] = $this->_transportCompanyManager->getName($transportCompany);
        }

        return $data;
    }

    private function _getOrdersProviderByOrderEntries($orderEntries)
    {
        $orders = [];
        foreach ($orderEntries as $orderEntry) {
            if (!isset($orders[$orderEntry->order_id])) {
                $order = $orderEntry->order;
                $orders[$orderEntry->order_id]['number'] = $order->source_innumber;
                $orders[$orderEntry->order_id]['deliveryPrice'] = $this->_formatPrice($order->delivery_price);
                $orders[$orderEntry->order_id]['comment'] = $order->comment;
                $orders[$orderEntry->order_id]['delivery'] = $order->delivery;
                $orders[$orderEntry->order_id]['category'] = $order->category;
                $orders[$orderEntry->order_id]['address'] = $order->address;
                $orders[$orderEntry->order_id]['client'] = $order->username;
                $orders[$orderEntry->order_id]['phone'] = $order->userphone;
                $orders[$orderEntry->order_id]['isNeedTtn'] = $order->is_need_ttn;
                $orders[$orderEntry->order_id]['isCOD'] = $order->isCOD;
                $orders[$orderEntry->order_id]['paid'] = $order->paid;
                $orders[$orderEntry->order_id]['orderEntries'] = [];
            }
            $orders[$orderEntry->order_id]['orderEntries'][] = $this->_prepareOrderEntry($orderEntry);
        }

        return $this->_asProvider($orders, ['number']);
    }

    private function _prepareShipment($model)
    {
        if (!$model) {
            return null;
        }
        $preparedModel['id']                      = $this->_shipmentManager->getId($model);
        $preparedModel['logisticsStatus']         = $this->_shipmentManager->getLogisticsStatusLabel($model);
        $preparedModel['transportCompanyName']    = $this->_shipmentManager->getTransportCompanyName($model);
        $preparedModel['transportCompanyId']      = $this->_shipmentManager->getTransportCompanyId($model);
        $preparedModel['hasWeighing']             = $this->_shipmentManager->getHasWeighing($model);
        $preparedModel['hasSizing']               = $this->_shipmentManager->getHasSizing($model);
        $preparedModel['weightStatus']            = $this->_shipmentManager->getHasWeighing($model) ? 'Со взвешиванием' : 'Без взвешивания';
        $preparedModel['canBeEdited']             = $this->_shipmentManager->isBeforeHandling($model);
        $preparedModel['canBeAccounted']          = $this->_shipmentManager->canBeAccounted($model);
        $preparedModel['comment']                 = $this->_shipmentManager->getComment($model);
        $preparedModel['shipmentDate']            = $this->_shipmentManager->getShipmentDate($model);
        $preparedModel['trackCode']               = $this->_shipmentManager->getTrackCode($model);
        $preparedModel['weight']                  = $this->_shipmentManager->getWeight($model);
        $preparedModel['size']                    = $this->_shipmentManager->getSize($model);
        $preparedModel['handledAt']               = $this->_shipmentManager->getHandledAt($model);
        $preparedModel['packedAt']                = $this->_shipmentManager->getPackedAt($model);
        $preparedModel['shippedAt']               = $this->_shipmentManager->getShippedAt($model);
        $preparedModel['accountedAt']             = $this->_shipmentManager->getAccountedAt($model);
        $preparedModel['deliveryPrice']           = $this->_shipmentManager->getDeliveryPrice($model);
        $preparedModel['isDeliveryFree']          = $this->_shipmentManager->getIsDeliveryFree($model);
        $preparedModel['isCashOnDelivery']        = $this->_shipmentManager->getIsCashOnDelivery($model);
        $preparedModel['positionsSumPrice']       = ShipmentPositionsData::getSumPrice($model);
        $preparedModel['positionsSumCount']       = ShipmentPositionsData::getCount($model);
        $preparedModel['positionsSumPaid']        = ShipmentPositionsData::getSumPaid($model);
        $preparedModel['positionsSumBonus']       = ShipmentPositionsData::getSumBonus($model);
        $preparedModel['totalPrice']              = $preparedModel['positionsSumPrice'] + $preparedModel['deliveryPrice'];

        return $preparedModel;
    }

    private function _prepareOrderEntries($models)
    {
        $preparedModels = [];
        foreach ($models as $model) {
            $preparedModels[] = $this->_prepareOrderEntry($model);
        }

        return $preparedModels;
    }

    private function _prepareOrderEntry($model)
    {
        if (!$model) {
            return null;
        }
        //todo: сделать через orderEntryManager
        $preparedModel['id']                   = $model->id;
        $preparedModel['orderId']              = $model->order->id ?? null;
        $preparedModel['orderNumber']          = $model->order->source_innumber ?? null;
        $preparedModel['transportCompanyName'] = $model->order->delivery ?? null;
        $preparedModel['article']              = $model->article;
        $preparedModel['name']                 = $model->name;
        $preparedModel['size']                 = '50X50X50'; //test
        $preparedModel['price']                = $this->_formatPrice($model->price);
        $preparedModel['isDeleted']            = $model->is_deleted;
        $preparedModel['deletionInfo']         = !$model->is_deleted ? '' : Yii::t('yii', 'Позиция удалена (' . implode(', ',
            array_filter([
                date('d.m.Y', $model->deleted_at),
                (User::findOne($model->deleted_by)->name ?? null)
            ])
        )) . ')';

        return $preparedModel;
    }

    private function _prepareShipmentDocuments($models)
    {
        $preparedModels = [];
        foreach ($models as $model) {
            $preparedModels[] = $this->_prepareShipmentDocument($model);
        }

        return $preparedModels;
    }

    private function _prepareShipmentDocument($model)
    {
        if (!$model) {
            return null;
        }
        $fileKey                         = $this->_shipmentDocumentManager->getFileKey($model);
        $preparedModel['id']             = $this->_shipmentDocumentManager->getId($model);
        $preparedModel['urlForView']     = $this->_fileManager->getFileUrlForView($fileKey);
        $preparedModel['urlForDownload'] = $this->_fileManager->getFileUrlForDownload($fileKey);
        $preparedModel['size']           = $this->_fileManager->getFileSize($fileKey);
        $preparedModel['originalName']   = $this->_fileManager->getFileOriginalName($fileKey);

        return $preparedModel;
    }

    private function _formatPrice($price)
    {
        return $price ? $price . ' руб' : 0;
    }

    private function _asProvider($models, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $models,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }

    private function _asFileWidgetData($models)
    {
        $widgetData['urls'] = $widgetData['configs'] = [];
        foreach ($models as $model) {
            $widgetData['urls'][] = $model['urlForView'];
            $widgetData['configs'][] = [
                'type'        => 'pdf',
                'extra'       => ['id' => $model['id']],
                'downloadUrl' => $model['urlForDownload'],
                'size'        => $model['size'],
                'caption'     => $model['originalName'],
            ];
        }

        return (object)$widgetData;
    }
}


