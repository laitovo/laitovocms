<?php

namespace backend\modules\logistics\modules\shipmentMonitor\models;

use yii\data\ArrayDataProvider;
use Yii;

class Accounting
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function getProvider()
    {
        $models = $this->_shipmentManager->findShipmentsCanBeAccounted();
        $models = $this->_prepareModels($models);
        $models = $this->_sortByLogisticsStatus($models);

        return $this->_asProvider($models, $this->getAttributes());
    }

    public function getAttributes()
    {
        return [
            'id',
            'transportCompanyName',
            'logisticsStatus',
        ];
    }

    public function getAttributesWithLabels()
    {
        return [
            ['attribute' => 'id',                   'label' => 'Отгрузка №'],
            ['attribute' => 'transportCompanyName', 'label' => 'Транспортная компания'],
            ['attribute' => 'logisticsStatus',      'label' => 'Статус'],
        ];
    }

    public function account($id)
    {
        if (!($model = $this->_shipmentManager->findById($id))) {
            return [
                'status'  => false,
                'message' => 'Не найден приказ на отгрузку',
            ];
        }
        if ($this->_shipmentManager->getAccountedIn1C($model)) {
            return [
                'status'  => false,
                'message' => 'Данная команда уже выполнена ранее',
            ];
        }
        if (!$this->_shipmentManager->canBeAccounted($model)) {
            return [
                'status'  => false,
                'message' => 'Данная отгрузка не может быть проведена в 1С',
            ];
        }
        if (!$this->_shipmentManager->account($model)) {
            return [
                'status'  => false,
                'message' => 'Не удалось выполнить команду',
            ];
        }

        return [
            'status' => true,
            'message' => 'Успешно выполнено',
        ];
    }

    private function _prepareModels($models)
    {
        $preparedModels = [];
        foreach ($models as $model) {
            $preparedModels[] = $this->_prepareModel($model);
        }

        return $preparedModels;
    }

    private function _prepareModel($model)
    {
        if (!$model) {
            return null;
        }
        $preparedModel['id']                      = $this->_shipmentManager->getId($model);
        $preparedModel['logisticsStatus']         = $this->_shipmentManager->getLogisticsStatusLabel($model);
        $preparedModel['logisticsStatusPriority'] = $this->_shipmentManager->getLogisticsStatusPriority($model);
        $preparedModel['transportCompanyName']    = $this->_shipmentManager->getTransportCompanyName($model);

        return $preparedModel;
    }

    private function _sortByLogisticsStatus($models, $ascOrDesc = 'ASC')
    {
        $ascOrDesc = strtoupper($ascOrDesc);
        if (!in_array($ascOrDesc, ['ASC', 'DESC'])) {
            $ascOrDesc = 'ASC';
        }
        uasort($models, function ($a, $b) use ($ascOrDesc) {
            switch (true) {
                case $a['logisticsStatusPriority'] > $b['logisticsStatusPriority']:
                    return $ascOrDesc == 'ASC' ? 1 : -1;
                case $a['logisticsStatusPriority'] < $b['logisticsStatusPriority']:
                    return $ascOrDesc == 'ASC' ? -1 : 1;
                default:
                    return 0;
            }
        });

        return $models;
    }

    private function _asProvider($models, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $models,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}


