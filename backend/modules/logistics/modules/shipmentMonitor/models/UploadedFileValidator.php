<?php

namespace backend\modules\logistics\modules\shipmentMonitor\models;

use common\models\LogActiveRecord;
use yii\web\UploadedFile;

/**
 * Валидатор загружаемых файлов.
 *
 * Class UploadedFileValidator
 * @package backend\modules\logistics\modules\shipmentMonitor\models
 */
class UploadedFileValidator extends LogActiveRecord
{
    public $uploadedFile;

    public function rules()
    {
        return [
            [['uploadedFile'], 'file', 'extensions' => 'pdf'],
        ];
    }

    /**
     * Основная функция валидатора.
     * Достаём файл из формы, делаем его своим атрибутом, валидируем средствами ActiveRecord.
     *
     * @param $fieldName
     * @return bool
     */
    public function validateFile($fieldName)
    {
        $this->uploadedFile = UploadedFile::getInstanceByName($fieldName);

        return $this->validate();
    }
}