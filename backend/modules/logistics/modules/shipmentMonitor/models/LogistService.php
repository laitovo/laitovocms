<?php

namespace backend\modules\logistics\modules\shipmentMonitor\models;

use Exception;
use Yii;

class LogistService
{
    private $_shipmentManager;
    private $_shipmentDocumentManager;
    private $_fileManager;
    private $_uploadedFileValidator;
    private $_handleShipment;
    private $_cancelShipment;

    public function __construct()
    {
        $this->_shipmentManager         = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_shipmentDocumentManager = Yii::$container->get('core\entities\logisticsShipmentDocument\ShipmentDocumentManager');
        $this->_handleShipment          = Yii::$container->get('core\logic\HandleShipment');
        $this->_cancelShipment          = Yii::$container->get('core\logic\CancelShipment');
        $this->_fileManager             = Yii::$app->fileManager;
        $this->_uploadedFileValidator   = new UploadedFileValidator();
    }

    public function handle($shipmentId)
    {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "0");

        $shipment = $this->_findShipment($shipmentId);

        if (!$shipment) {
            return false;
        }
        $elements = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        /**
         * @var $needTTN bool Указывает, нужно ли выписывать оф. накладную
         */
        $needTTN = false;
        foreach ($elements as $element) {
            if (($order = $element->order) && $order->is_need_ttn) {
                $needTTN = true;
            }
            break;
        }


        if (!$this->_handleShipment->handle($shipment,$needTTN)) {
            return [
              'status' => false,
              'handledAt' => null
            ];
        }

        return [
            'status' => true,
            'handledAt' => date('d.m.Y H:i:s', $this->_shipmentManager->getHandledAt($shipment))
        ];
    }

    public function cancel($shipmentId)
    {
        $shipment = $this->_findShipment($shipmentId);
        $result = $this->_cancelShipment->cancel($shipment);

        return $result;
    }

    public function sendStorageRequest($shipmentId, $hasWeighing, $hasSizing)
    {
        $shipment = $this->_findShipment($shipmentId);
        /**
         * Мы не даем отправить запрос на склад, если не указана дата отгрузки
         */
        if (!$this->_shipmentManager->getShipmentDate($shipment)) {
            return [
                'status' => false,
                'message' => Yii::t('app', 'Укажите дату отгрузки, а уже потом пошлите запрос на склад!'),
            ];
        }

        if (!$this->_shipmentManager->sendStorageRequest($shipment, $hasWeighing, $hasSizing)) {
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось выполнить команду'),
            ];
        }

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    public function cancelStorageRequest($shipmentId)
    {
        $shipment = $this->_findShipment($shipmentId);
        if (!$this->_shipmentManager->cancelStorageRequest($shipment)) {
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось выполнить команду'),
            ];
        }

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    public function saveTransportCompany($shipmentId, $transportCompanyId)
    {
        $shipment = $this->_findShipment($shipmentId);
        $this->_shipmentManager->setTransportCompanyId($shipment, $transportCompanyId);
        $result = $this->_shipmentManager->save($shipment);

        return $result;
    }

    public function saveComment($shipmentId, $comment)
    {
        $shipment = $this->_findShipment($shipmentId);
        $this->_shipmentManager->setComment($shipment, $comment);
        $result = $this->_shipmentManager->save($shipment);

        return $result;
    }

    public function saveShipmentDate($shipmentId, $shipmentDate)
    {
        $shipment = $this->_findShipment($shipmentId);
        if ($shipmentDate) {
            $shipmentDate = strtotime($shipmentDate);
        }
        $this->_shipmentManager->setShipmentDate($shipment, $shipmentDate);
        $result = $this->_shipmentManager->save($shipment);

        return $result;
    }

    public function saveTrackCode($shipmentId, $trackCode)
    {
        $shipment = $this->_findShipment($shipmentId);
        $this->_shipmentManager->setTrackCode($shipment, $trackCode);
        $result = $this->_shipmentManager->save($shipment);

        return $result;
    }

    public function saveWeight($shipmentId, $weight)
    {
        $shipment = $this->_findShipment($shipmentId);
        $this->_shipmentManager->setWeight($shipment, $weight);
        $result = $this->_shipmentManager->save($shipment);

        return $result;
    }

    public function saveSize($shipmentId, $size)
    {
        $shipment = $this->_findShipment($shipmentId);
        $this->_shipmentManager->setSize($shipment, $size);
        $result = $this->_shipmentManager->save($shipment);

        return $result;
    }

    public function saveDeliveryPrice($shipmentId, $deliveryPrice, $isDeliveryFree)
    {
        $shipment = $this->_findShipment($shipmentId);
        $this->_shipmentManager->setIsDeliveryFree($shipment, !!$isDeliveryFree);
        if (!$isDeliveryFree) {
            $this->_shipmentManager->setDeliveryPrice($shipment, $deliveryPrice);
        } else {
            $this->_shipmentManager->setDeliveryPrice($shipment, 0);
        }
        $result = $this->_shipmentManager->save($shipment);

        return $result;
    }

    public function saveCashOnDelivery($shipmentId, $isCashOnDelivery)
    {
        $shipment = $this->_findShipment($shipmentId);
        $this->_shipmentManager->setIsCashOnDelivery($shipment, $isCashOnDelivery);
        $result = $this->_shipmentManager->save($shipment);

        return $result;
    }

    public function uploadDocument($shipmentId, $fieldName)
    {
        if (!$this->_findShipment($shipmentId)) {
            return false;
        }
        if (!$this->_uploadedFileValidator->validateFile($fieldName)) {
            return false;
        }
        $shipmentDocument = $this->_shipmentDocumentManager->create();
        $this->_shipmentDocumentManager->setShipmentId($shipmentDocument, $shipmentId);
        if (!$this->_shipmentDocumentManager->saveWithFile($shipmentDocument, $fieldName)) {
            return false;
        }

        return [
            'id' => $this->_shipmentDocumentManager->getId($shipmentDocument),
        ];
    }

    public function deleteDocument($shipmentDocumentId)
    {
        $shipmentDocument = $this->_findShipmentDocument($shipmentDocumentId);

        return $this->_shipmentDocumentManager->deleteWithFile($shipmentDocument);
    }

    private function _findShipment($shipmentId)
    {
        if (!($shipment = $this->_shipmentManager->findById($shipmentId))) {
            throw new Exception('Отгрузка не найдена');
        }

        return $shipment;
    }

    private function _findShipmentDocument($shipmentDocumentId)
    {
        if (!($shipmentDocument = $this->_shipmentDocumentManager->findById($shipmentDocumentId))) {
            throw new Exception('Документ не найден');
        }

        return $shipmentDocument;
    }
}