<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use yii\jui\DatePicker;
use backend\modules\logistics\modules\shipmentMonitor\assets\ViewAsset;
use kartik\file\FileInput;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model array */
/* @var $provider yii\data\ArrayDataProvider */
/* @var $attributes array */
/* @var $fileWidgetData object */
/* @var $transportCompanies array */

ViewAsset::register($this);

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app','Просмотр приказа на отгрузку');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Монитор приказов на отгрузку'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="form-group">
    <?php
        $hideEditingClass = !$model['canBeEdited'] ? ' hide' : '';
        echo Html::button(Yii::t('app','Обработать приказ'), ['id' => 'handle-button', 'class' => 'btn btn-success hide-if-cant-edit' . $hideEditingClass, 'data' => ['shipment-id' => $model['id']]]);
        echo Html::button(Yii::t('app','Отменить приказ'), ['id' => 'cancel-button', 'class' => 'btn btn-danger hide-if-cant-edit' . $hideEditingClass, 'data' => ['shipment-id' => $model['id']], 'style' => 'margin-left:4px;']);
    ?>
    <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться в монитор логистики'), ['/logistics/logistics-monitor/default/index'], ['class' => 'btn btn-info']) ?>
    <?php if ($model['canBeAccounted']): ?>
        <?= Html::button(Yii::t('app','Провести в 1С'), ['id' => 'account-button', 'class' => 'btn btn-primary', 'data' => ['shipment-id' => $model['id']], 'style' => 'margin-left:4px;']); ?>
    <?php endif; ?>
</div>

<?php
    $html = implode('', [
        !$model['handledAt']   ? '' : '<span style="margin-right:10px;">' . Yii::t('app', 'Обработан: ') . date('d.m.Y H:i:s', $model['handledAt']) . '</span>',
        !$model['packedAt']    ? '' : '<span style="margin-right:10px;">' . Yii::t('app', 'Упакован: ') . date('d.m.Y H:i:s', $model['packedAt']) . '</span>',
        !$model['shippedAt']   ? '' : '<span style="margin-right:10px;">' . Yii::t('app', 'Отгружен: ') . date('d.m.Y H:i:s', $model['shippedAt']) . '</span>',
        !$model['accountedAt'] ? '' : '<span style="margin-right:10px;">' . Yii::t('app', 'Проведен: ') . date('d.m.Y H:i:s', $model['accountedAt']) . '</span>'
    ]);
    echo '<div id="dates-block" style="margin-bottom:20px;font-style:italic;">' . $html . '</div>';
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'id',
            'label' => '№ Отгрузки:',
            'captionOptions' => ['style' =>  'width: 150px;'],
        ],
    ],
]) ?>

<?php
    $hideEditingClass = !$model['canBeEdited'] ? ' hide' : '';
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Транспортная компания:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass, $transportCompanies) {
                    $html = '<div class="edit-block closed">';
                    $html .= '<select class="form-control property-field" data-shipment-id="' . $data['id'] . '" disabled>';
                    $html .= '<option value="">' . Yii::t('app', 'Не выбрано') . '</option>';
                    foreach ($transportCompanies as $tcId => $tcName) {
                        $html .= '<option value="' . $tcId . '"' . (($tcId==$data['transportCompanyId']) ? ' selected' : '') . '>' . $tcName . '</option>';
                    }
                    $html .= '</select>';
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-if-cant-edit' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden transport-company-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);
?>

<div class="weight-status-block">
    <div class="grey">
        <div class="text-wrapper">Тип: <span class="text-nowrap weight-status"><?= $model['weightStatus'] ?></span></div>
    </div>
    <?php
        $hideClass = ($model['hasWeighing'] || $model['hasSizing'] || !$model['canBeEdited']) ? ' hide' : '';
        echo Html::button(Yii::t('app', 'Отправить запрос на склад'), [
            'id' => 'button-prepare-storage-request',
            'class'   => 'btn btn-primary hide-if-cant-edit' . $hideClass,
            'data'    => [
                'shipment-id' => $model['id'],
            ],
        ]);
        $hideClass = ((!$model['hasWeighing'] && !$model['hasSizing']) || !$model['canBeEdited']) ? ' hide' : '';
        echo Html::button(Yii::t('app', 'Отменить запрос на склад'), [
            'id' => 'button-cancel-storage-request',
            'class'   => 'btn btn-primary hide-if-cant-edit' . $hideClass,
            'data'    => [
                'shipment-id' => $model['id'],
            ],
        ]);
    ?>
</div>
<hr class="big-page-line">

ТОВАР:
<hr class="big-page-line">

<?php
    $hideEditingClass = !$model['canBeEdited'] ? ' hide' : '';
    echo Html::a('<i class="icon wb-edit"></i>', ['bind', 'id' => $model['id']], [
        'class' => 'btn btn-icon btn-outline btn-round btn-primary hide-if-cant-edit' . $hideEditingClass,
//        'style' => 'position:relative;top:30px;margin-top:-30px;',
        'style' => 'margin-bottom:10px;',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('app', 'Изменить список')]);
?>

<?php Pjax::begin(['id' => 'pjax-positions']); ?>
<!--    --><?//= GridView::widget([
//        'tableOptions' => ['class' => 'table table-hover'],
//        'summary' => false,
//        'dataProvider' => $provider,
//        'show' => $attributes,
//        'rowOptions' => function ($data) {
//            if ($data['isDeleted']) {
//                return [
//                    'class' => 'crossed',
//                    'data'  => [
//                        'deletion-info' => $data['deletionInfo']
//                    ]
//                ];
//            }
//        },
//        'columns' => [
//            [
//                'label'     => '№ Заказа',
//                'attribute' => 'orderNumber',
//                'format' => 'raw',
//                'value' => function ($data) {
//                    return $data['orderNumber'] ? Html::a(Html::encode($data['orderNumber']), ['/logistics/logistics-monitor/default/view-order', 'orderId' => $data['orderId']], ['target' => '_blank','data-pjax' => 0]) : null;
//                },
//            ],
//            [
//                'label'     => 'Транспортная компания',
//                'attribute' => 'transportCompanyName',
//            ],
//            [
//                'label'     => 'Артикул',
//                'attribute' => 'article',
//            ],
//            [
//                'label'     => 'Наименование',
//                'attribute' => 'name',
//            ],
//            [
//                'label'     => 'Габариты',
//                'attribute' => 'size',
//            ],
//            [
//                'label'     => 'Цена',
//                'attribute' => 'price',
//            ]
//        ],
//    ]) ?>
    <?= ListView::widget([
        'dataProvider' => $provider,
        'itemView' => '_order',
    ]) ?>
<?php Pjax::end(); ?>
<hr class="big-page-line">

<div class="col-md-4 col-sm-4">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'positionsSumPrice',
                'label' => 'Сумма позиций:',
                'format'=> 'raw',
                'value' => function($data) {
                    return '<span id="positions-sum-price-span">' . $data['positionsSumPrice'] . '</span>';
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
                'contentOptions' => ['style' =>  'padding-left: 20px;'],
            ],
        ],
    ]); ?>
</div>

<div class="col-md-4 col-sm-4">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'positionsSumPaid',
                'label' => 'Оплачено:',
                'format'=> 'raw',
                'value' => function($data) {
                    $html = '<span>';
                    $html .= $data['positionsSumPaid'];
                    if ($data['positionsSumBonus']) {
                        $html .= ' (бонусами: ' . $data['positionsSumBonus'] . ')';
                    }
                    $html .= '</span>';
                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
                'contentOptions' => ['style' =>  'padding-left: 20px;'],
            ],
        ],
    ]); ?>
</div>

<div class="col-md-4 col-sm-4">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'positionsSumCount',
                'label' => 'Кол-во позиций:',
                'format'=> 'raw',
                'value' => function($data) {
                    return '<span>' . $data['positionsSumCount'] . '</span>';
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
                'contentOptions' => ['style' =>  'padding-left: 20px;'],
            ],
        ],
    ]); ?>
</div>

<div class="clearfix"></div>
<div class="col-md-5 col-sm-6">
    <?php
    $hideEditingClass = !$model['canBeEdited'] || $model['isDeliveryFree']? ' hide' : '';
    echo DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view delivery-price-edit-block'],
        'attributes' => [
            [
                'label' => 'Сумма доставки:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block short closed">';
                    $html .= '<input class="form-control property-field" type="number" min="0" placeholder="(не задано)" value="' . $data['deliveryPrice'] . '" data-shipment-id="' . $data['id'] . '" disabled>';
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-if-cant-edit' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden delivery-price-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);
    ?>
</div>
<div class="col-md-7 col-sm-6">
    <?php
        $hideEditingClass = !$model['canBeEdited'] ? ' hide' : '';
        echo Html::tag('div',
            Html::checkbox("isDeliveryFree", $model['isDeliveryFree'], [
                'id' => 'checkbox-free-delivery',
                'data-shipment-id' => $model['id']
            ])
            .Html::tag('label','Доставка бесплатна', ['for' => 'checkbox-free-delivery']),
            [
                'class' => 'checkbox-custom checkbox-primary text-left hide-if-cant-edit' . $hideEditingClass,
                'onchange' => ''
            ]
        );
    ?>
</div>
    <div class="clearfix"></div>

<div class="col-md-7 col-sm-6">
    <?php
        $hideEditingClass = !$model['canBeEdited'] ? ' hide' : '';
        echo Html::tag('div',
            Html::checkbox("isCashOnDelivery", $model['isCashOnDelivery'], [
                'id' => 'checkbox-cash-on-delivery',
                'data-shipment-id' => $model['id']
            ])
            .Html::tag('label','Заказ оплачивается наложенным платежом', ['for' => 'checkbox-cash-on-delivery']),
            [
                'class' => 'checkbox-custom checkbox-primary text-left hide-if-cant-edit' . $hideEditingClass,
                'onchange' => ''
            ]
        );
    ?>
</div>
<div class="clearfix"></div>
<!--<div class="col-md-5 col-sm-6">-->
<!--    --><?//= DetailView::widget([
//        'model' => $model,
//        'attributes' => [
//            [
//                'attribute' => 'totalPrice',
//                'label' => 'ИТОГО:',
//                'format'=> 'raw',
//                'value' => function($data) {
//                    return '<span id="total-price-span">' . $data['totalPrice'] . '</span>';
//                },
//                'captionOptions' => ['style' =>  'width: 150px;'],
//                'contentOptions' => ['style' =>  'padding-left: 20px;'],
//            ],
//        ],
//    ]); ?>
<!--</div>-->
<!--<div class="clearfix"></div>-->
<hr class="big-page-line">

<?php
    $hideEditingClass = !$model['canBeEdited']  && Yii::$app->user->id !== 21 && Yii::$app->user->id !== 36 ? ' hide' : '';
    $hideWeightEditBlockClass = ($model['hasWeighing']) ? '' : ' hide';
    $hideSizeEditBlockClass = ($model['hasSizing']) ? '' : ' hide';
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Комментарий:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block closed">';
                    $html .= '<textarea class="form-control property-field" placeholder="(не задано)" data-shipment-id="' . $data['id'] . '" disabled>' . $data['comment'] . '</textarea>';
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-if-cant-edit' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden comment-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Дата отгрузки:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block short closed">';
                    $html .= DatePicker::widget([
                        'value' => $data['shipmentDate'] ? date('d.M.Y', $data['shipmentDate']) : null,
                        'options' => ['class' => 'form-control property-field', 'data' => ['shipment-id' => $data['id']], 'disabled' => true, 'placeholder' => '(не задано)'],
                        'dateFormat' => 'dd.MM.yyyy',
                    ]);
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-if-cant-edit' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden shipment-date-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Трек-код:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block short closed">';
                    $html .= '<input class="form-control property-field" type="text" placeholder="(не задано)" value="' . $data['trackCode'] . '" data-shipment-id="' . $data['id'] . '" disabled>';
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-if-cant-edit' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden track-code-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);
    echo DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view weight-edit-block'. $hideWeightEditBlockClass],
        'attributes' => [
            [
                'label' => 'Вес:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block short closed">';
                    $html .= '<input class="form-control property-field" type="number" placeholder="(не задано)" value="' . $data['weight'] . '" data-shipment-id="' . $data['id'] . '" disabled>';
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-if-cant-edit' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden weight-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);
    echo DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view size-edit-block'. $hideSizeEditBlockClass],
        'attributes' => [
            [
                'label' => 'Габариты:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block short closed">';
                    $html .= '<input class="form-control property-field" type="text" placeholder="(не задано)" value="' . $data['size'] . '" data-shipment-id="' . $data['id'] . '" disabled>';
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-if-cant-edit' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden size-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);
?>

<?php $form = ActiveForm::begin(); ?>

<h2>Документы:</h2>

<?= FileInput::widget([
    'name' => 'file',
    'options' => [
        'id' => 'file-input',
        'multiple' => true,
        'disabled' => !$model['canBeEdited'],
    ],
    'pluginOptions' => [
        'initialPreview' => $fileWidgetData->urls,
        'initialPreviewConfig' => $fileWidgetData->configs,
        'initialPreviewAsData' => true,
        'overwriteInitial' => false,
        'uploadUrl' => '/logistics/shipment-monitor/logist/upload-document',
        'uploadExtraData' => [
            'shipmentId' => $model['id'],
        ],
        'deleteUrl' => '/logistics/shipment-monitor/logist/delete-document',
        'allowedFileExtensions' => ['pdf'],
        'showCaption' => false,
        'showRemove' => false,
        'showClose' => false,
        'fileActionSettings' => [
            'showDrag' => false,
            'uploadClass' => 'btn btn-xs bg-success',
            'removeClass' => 'btn btn-xs bg-danger',
            'removeErrorClass' => 'btn btn-xs bg-danger',
            'zoomClass' => 'btn btn-xs bg-info',
            'downloadTitle' => 'Скачать файл',
            'downloadClass' => 'btn btn-xs bg-primary',
        ],
        'previewZoomButtonIcons' => [
            'borderless' => '<i class="glyphicon glyphicon-fullscreen"></i>',
        ],
        'previewZoomButtonClasses' => [
            'toggleheader' => 'hide',
            'fullscreen' => 'hide',
            'borderless' => 'btn btn-xs',
            'close' => 'btn btn-xs',
        ],
        'previewZoomButtonTitles' => [
            'borderless' => 'Переключить полноэкранный режим',
        ],
    ],
    'pluginEvents' => [
        'fileselect' => 'function() {
            showUploadButton();
         }',
        'fileremoved' => 'function() {
            showUploadButton();
         }',
        'fileuploaded' => 'function(event, data, thumbnailId) {
            rememberNewFileId(thumbnailId, data.response.id);
            setTimeout(showUploadButton, 100);
         }',
        'filesuccessremove' => 'function(event, thumbnailId) {
    		deleteNewFile(thumbnailId);
         }',
    ],
]);?>

<?php Yii::$app->view->registerJs('
    //Функция скрывает кнопку "Загрузить", если нечего загружать
    function showUploadButton() {
        var fileInput = $("#file-input");
        var uploadButton = $(".fileinput-upload.fileinput-upload-button");
        var filesToUploadCount = fileInput.fileinput("getFileStack").length;
        if (filesToUploadCount > 0) {
            uploadButton.removeClass("hide");
        } else {
            uploadButton.addClass("hide");
        }
    };

    //Запоминаем id свежезагруженного файла (чтобы потом его можно было удалить)
    function rememberNewFileId(thumbnailId, newFileId) {
        $("#" + thumbnailId).data("newFileId", newFileId);
    }

    //Получаем id свежезагруженного файла
    function getNewFileId(thumbnailId) {
        return $("#" + thumbnailId).data("newFileId");
    }

    //Удаление свежезагруженного файла
    function deleteNewFile(thumbnailId) {
        var fileId = getNewFileId(thumbnailId);
        $.ajax({
            type: "post",
            url: "/logistics/shipment-monitor/logist/delete-document",
            data: {"id": fileId},
            dataType: "json",
            error: function() {
                notie.alert(3, "Не удалось удалить документ", 3);
            }
        });
    }
', \yii\web\View::POS_END);
?>

<?php ActiveForm::end(); ?>

<?php
    Modal::begin([
        'header' => '<h2>' . Yii::t('app', 'Запрос на склад') . '</h2>',
        'options' => [
            'id' => 'modal-storage-request',
        ],
    ]);

    echo Html::tag('div',
        Html::checkbox("hasWeighing", false, [
            'id' => 'checkbox-has-weighing',
        ])
        .Html::tag('label', Yii::t('app', 'Взвешивание'), ['for' => 'checkbox-has-weighing']),
        [
            'class' => 'checkbox-custom checkbox-primary text-left',
        ]
    );
    echo Html::tag('div',
        Html::checkbox("hasSizing", false, [
            'id' => 'checkbox-has-sizing',
        ])
        .Html::tag('label', Yii::t('app', 'Габариты'), ['for' => 'checkbox-has-sizing']),
        [
            'class' => 'checkbox-custom checkbox-primary text-left',
        ]
    );
    echo Html::button(Yii::t('app', 'Отправить'), [
        'id' => 'button-send-storage-request',
        'class' => 'btn btn-primary',
        'data' => [
            'shipment-id' => $model['id'],
        ],
    ]);

    Modal::end();
?>