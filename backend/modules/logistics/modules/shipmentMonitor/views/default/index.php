<?php

use backend\widgets\GridView;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $provider yii\data\ArrayDataProvider */
/* @var $attributes array */
/* @var $attributesWithLabels array */

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Монитор приказов на отгрузку');

$this->params['breadcrumbs'][] = $this->title;

?>

<?= Html::a('<i class="icon wb-plus"></i>', ['bind'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover shipments-table'],
    'rowOptions'=> function ($data) {
        if ($data['isHandled']) {
            return ['class' => 'success'];
        }
    },
    'summary' => false,
    'dataProvider' => $provider,
    'show' => $attributes,
    'columns' => array_merge(
        $attributesWithLabels,
        [[
            'label' => 'Панель действий',
            'format' => 'raw',
            'value' => function($data) {
                return Html::a('<i class = "glyphicon glyphicon-eye-open"></i>', ['view', 'id' => $data['id']]);
            }
        ]]
    )
]) ?>
