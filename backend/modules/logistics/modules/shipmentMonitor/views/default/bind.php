<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var yii\web\View $this */
/* @var int|null $id */
/* @var $providerYes yii\data\ArrayDataProvider */
/* @var $providerNo yii\data\ArrayDataProvider */
/* @var array $idsYes */
/* @var array $idsNo */

$this->render('@backendViews/laitovo/views/menu');

$this->title = Yii::t('app', 'Добавление позиций для отгрузки');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Монитор приказов на отгрузку'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    .checkbox-custom label::before {
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
</style>

<p>
    <?= Html::button('<i class="icon wb-check"></i> ' . Yii::t('app', $id ? 'Сохранить' : 'Создать приказ'), ['class' => 'btn btn-primary', 'id' => 'save-bindings-button']) ?>
</p>

<? Pjax::begin(['enablePushState' => false]); ?>

<?php $form = ActiveForm::begin(['id' => 'bind-form', 'options' => ['data-pjax'=>'']]); ?>
    <input id="hidden-only-add" type="hidden" name="onlyAdd" value="">
    <input id="hidden-only-remove" type="hidden" name="onlyRemove" value="">
    <?php foreach($idsYes as $idYes): ?>
        <input type="hidden" name="idsYes[]" value="<?= $idYes ?>">
    <?php endforeach; ?>
    <?php foreach($idsNo as $idNo): ?>
        <input type="hidden" name="idsNo[]" value="<?= $idNo ?>">
    <?php endforeach; ?>
<hr>
    <div class="row">
        <div class="col-md-6">
            <?= Html::button ('Убрать выбранные', ['class' => 'btn btn-danger pull-right', 'id' => 'remove-bindings-button'] ) ?>
            <h4>Позиции в приказе на отгрузку</h4>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover no-table'],
                'dataProvider' => $providerYes,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'number',
                        'label' => 'Заказ',
                        'format' => 'raw',
                        'value' => function($data) {
                            return Html::tag('div',
                                Html::checkbox(null, false, [
                                    'onchange' => "$('.no-table .ordern" . $data['id'] . "').prop('checked', $(this).is(':checked'));",
                                ]) .
                                Html::tag('label',
                                    $data['number']
                                ),
                                [
                                    'class' => 'checkbox-custom checkbox-primary text-left',
                                ]
                            );
                        }
                    ],
                    [
                        'attribute' => 'orderEntries',
                        'label' => 'Наряды',
                        'format' => 'raw',
                        'value' => function($data) {
                            $html = '';
                            foreach ($data['orderEntries'] as $orderEntry) {
                                $html .= Html::tag('div',
                                    Html::checkbox('idsNoNew[]', $checked = false, ['value' => $orderEntry->id, 'class' => 'ordern' . $orderEntry->order_id]) .
                                    Html::tag('label',
//                                        $orderEntry->id
//                                        implode(' ', [$orderEntry->article, $orderEntry->car, $orderEntry->name]),
                                        $orderEntry->name,
                                        [
                                          'data-toggle' => 'tooltip',
                                          'data-placement' => 'top',
                                          'data-html' => 'true',
                                          'title' => implode('<br>', [$orderEntry->article, $orderEntry->car]),
                                        ]
                                    ),
                                    ['class' => 'checkbox-custom checkbox-primary text-left']
                                );
                            }

                            return $html;
                        }
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= Html::button ('Добавить выбранные', ['class' => 'btn btn-success pull-right', 'id' => 'add-bindings-button'] ) ?>
            <h4>Позиции, готовые к отгрузке</h4>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover yes-table'],
                'dataProvider' => $providerNo,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'number',
                        'label' => 'Заказ',
                        'format' => 'raw',
                        'value' => function($data) {
                            return Html::tag('div',
                                Html::checkbox(null, false, [
                                    'onchange' => "$('.yes-table .ordern" . $data['id'] . "').prop('checked', $(this).is(':checked'));",
                                ]) .
                                Html::tag('label',
                                    $data['number']
                                ),
                                [
                                    'class' => 'checkbox-custom checkbox-primary text-left',
                                ]
                            );
                        }
                    ],
                    [
                        'attribute' => 'orderEntries',
                        'label' => 'Наряды',
                        'format' => 'raw',
                        'value' => function($data) {
                            $html = '';
                            foreach ($data['orderEntries'] as $orderEntry) {
                                $html .= Html::tag('div',
                                    Html::checkbox('idsYesNew[]', $checked = false, ['value' => $orderEntry->id, 'class' => 'ordern' . $orderEntry->order_id]) .
                                    Html::tag('label',
//                                        $orderEntry->id
//                                        implode('<br>', [$orderEntry->article, $orderEntry->car, $orderEntry->name])
                                        $orderEntry->name,
                                        [
                                            'data-toggle' => 'tooltip',
                                            'data-placement' => 'top',
                                            'data-html' => 'true',
                                            'title' => implode('<br>', [$orderEntry->article, $orderEntry->car]),
                                        ]
                                    ),

                                    ['class' => 'checkbox-custom checkbox-primary text-left']
                                );
                            }

                            return $html;
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php ActiveForm::begin(['action' => ['save-bindings', 'id' => $id], 'id' => 'save-bindings-form']); ?>
    <?php foreach($idsYes as $idYes): ?>
        <input type="hidden" name="idsYes[]" value="<?= $idYes ?>">
    <?php endforeach; ?>
    <?php foreach($idsNo as $idNo): ?>
        <input type="hidden" name="idsNo[]" value="<?= $idNo ?>">
    <?php endforeach; ?>
<?php ActiveForm::end(); ?>

<?php Yii::$app->view->registerJs('
    $("#add-bindings-button").click(function() {
        addBindings();
    });
    $("#remove-bindings-button").click(function() {
        removeBindings();
    });
    $("#save-bindings-button").click(function() {
        saveBindings();
    });
    function addBindings() {
        $("#hidden-only-add").val(1);
        $("#hidden-only-remove").val("");
        $("#bind-form").submit();
    }
    function removeBindings() {
        $("#hidden-only-add").val("");
        $("#hidden-only-remove").val(1);
        $("#bind-form").submit();
    }
    function saveBindings() {
        $("#save-bindings-form").submit();
    }
', \yii\web\View::POS_END);
?>

<? Pjax::end(); ?>
