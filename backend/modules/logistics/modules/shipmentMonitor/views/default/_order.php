<?php
    use yii\widgets\DetailView;
?>
<hr>
<div style="font-size: 0.84em">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => array_merge([
            [
                'attribute' => 'number',
                'label' => '№ Заказа:',
                'format' => 'html',
                'value' => function($data) {
                    $string = $data['number'];
                    if ($data['isNeedTtn']) {
                        $string .= ' <strong class="text-nowrap" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="' . Yii::t('app', 'Обязательно приложить ТТН к заказу') . '">' . Yii::t('app', '(с ТТН)') . '</strong>';
                    }
                    if ($data['paid']) {
                        $string .= ' <strong class="text-nowrap text-danger" style="font-size:1.1em;font-weight:bold;" data-toggle="tooltip" title="' . Yii::t('app', 'Оплачено с учетом бонусов') . '">( Оплачено: ' . $data['paid'] . ' ) </strong>';
                    }
                    return $string;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
            [
                'attribute' => 'deliveryPrice',
                'label' => 'Сумма доставки:',
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
        !$model['comment'] ? [] : [
            [
                'attribute' => 'comment',
                'label' => 'Комментарий:',
                'captionOptions' => ['style' =>  'width: 150px;'],
            ]
        ],
        [
            [
                'attribute' => 'category',
                'label' => 'Категория:',
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
            [
                'attribute' => 'delivery',
                'format' => 'raw',
                'label' => 'ТК:',
                'value' => function($data) {
                    $string = $data['delivery'];
                    if ($data['isCOD']) {
                        $string .= ' <strong class="text-nowrap text-danger" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="' . Yii::t('app', 'Оплата заказа производится наложенным платежем') . '">' . Yii::t('app', '(Наложка)') . '</strong>';
                    }
                    return $string;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
            [
                'attribute' => 'address',
                'label' => 'Адрес доставки:',
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
            [
                'attribute' => 'client',
                'label' => 'Клиент:',
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
            [
                'attribute' => 'phone',
                'label' => 'Телефон:',
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ])
    ]) ?>
    <div class="row">
        <div class="col-md-12 text-center">
            <table class="table table-hover table-default">
                <thead>
                <th>Транспортная компания</th>
                <th>Артикул</th>
                <th>Наименование</th>
                <th>Габариты</th>
                <th>Цена</th>
                </thead>
                <tbody>
                <?foreach ($model['orderEntries'] as $element):?>
                    <tr <?php if ($element['isDeleted']): ?>class="crossed" data-deletion-info="<?=$element['deletionInfo']?>"<?php endif; ?>>
                        <td><?=$element['transportCompanyName']?></td>
                        <td><?=$element['article']?></td>
                        <td><?=$element['name']?></td>
                        <td><?=$element['size']?></td>
                        <td><?=$element['price']?></td>
                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
