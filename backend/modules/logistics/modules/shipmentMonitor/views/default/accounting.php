<?php

use backend\widgets\GridView;
use \yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $provider yii\data\ArrayDataProvider */
/* @var $attributes array */
/* @var $attributesWithLabels array */

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Проведение отгрузок в 1С');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Монитор приказов на отгрузку'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?php Pjax::begin(['id' => 'pjax-accounting-list','enablePushState' => false]); ?>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover shipments-table'],
        'summary' => false,
        'dataProvider' => $provider,
        'show' => $attributes,
        'columns' => array_merge(
            $attributesWithLabels,
            [[
                'label' => 'Панель действий',
                'format' => 'raw',
                'value' => function($data) {
                    $html  = Html::a('<i class="glyphicon glyphicon-eye-open"></i>', ['view', 'id' => $data['id']], [
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Просмотр',
                        'target' => '_blank',
                        'data-pjax' => '',
                    ]);
                    $html .= Html::tag('i','',[
                        'class' => 'fa fa-check account-button',
                        'style' => 'margin-left:5px;cursor:pointer;color:#3aa99e',
                        'data-id' => $data['id'],
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Провести в 1С',
                    ]);

                    return $html;
                }
            ]]
        )
    ]) ?>
<?php Pjax::end(); ?>

<?php Yii::$app->view->registerJs('
    $(document).on("click", ".account-button", function(){
        var id = $(this).data("id");
        var handle_function = function () {
            $.ajax({
                url: "/logistics/shipment-monitor/default/account",
                dataType: "json",
                data: {
                    "id": id
                },
                success: function (data) {
                    $.pjax.reload({container : "#pjax-accounting-list"});
                    if (!data.status) {
                        notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                        return;
                    }
                    notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                },
                error: function() {
                    notie.alert(3, "Не удалось выполнить команду.", 3);
                }
            });
        };
        notie.confirm("Документ-реализация действительно проведен в 1С?", "Да", "Нет", handle_function);
    });
', \yii\web\View::POS_END);
?>