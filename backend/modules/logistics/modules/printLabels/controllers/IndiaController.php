<?php

namespace backend\modules\logistics\modules\printLabels\controllers;

use backend\modules\logistics\modules\printLabels\models\SearchIndia;
use backend\modules\logistics\modules\printLabels\models\SearchIndiaPrint;
use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class IndiaController
 * @package backend\modules\logistics\modules\printLabels\controllers
 */
class IndiaController extends Controller
{
    public function actionIndex()
    {
        return $this->render('default');
    }

    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status'] ='success';
        $response['message']='Этикетка распечатана';
        $response['html']   ='';

        try{
            //Ищем наряд на диспетчер склад
            $search = new SearchIndia($barcode);
            $search->validate();
            $response['html'] = $this->renderPartial('label',[ 'model' => $search->getArticleObj()]);
        } catch (\DomainException $exception) {
            $response['status'] = 'error';
            $response['message'] = $exception->getMessage();
        }

        return $response;

    }

    /**
     * @param $barcode
     * @return string
     */
    public function actionPrint($barcode)
    {
        try{
            //Ищем наряд на диспетчер склад
            $search = new SearchIndiaPrint($barcode);
            $search->validate();
            return $this->renderPartial('label',[ 'model' => $search->getArticleObj()]);
        } catch (\DomainException $exception) {
            return $exception->getMessage();
        }

    }
}