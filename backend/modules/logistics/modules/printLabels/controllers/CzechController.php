<?php

namespace backend\modules\logistics\modules\printLabels\controllers;

use backend\modules\logistics\modules\printLabels\models\SearchCzech;
use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class CzechController
 * @package backend\modules\logistics\modules\printLabels\controllers
 */
class CzechController extends Controller
{
    public function actionIndex()
    {
        return $this->render('default');
    }

    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status'] ='success';
        $response['message']='Этикетка распечатана';
        $response['html']   ='';

        try{
            //Ищем наряд на диспетчер склад
            $search = new SearchCzech($barcode);
            $search->validate();
            $response['html'] = $this->renderPartial('label',[ 'model' => $search->getArticleObj()]);
        } catch (\DomainException $exception) {
            $response['status'] = 'error';
            $response['message'] = $exception->getMessage();
        }

        return $response;

    }
}