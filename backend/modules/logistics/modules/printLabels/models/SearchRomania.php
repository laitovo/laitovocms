<?php

namespace backend\modules\logistics\modules\printLabels\models;

use backend\helpers\ArticleHelper;
use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use common\models\laitovo\Cars;

class SearchRomania
{
    /**
     * @var string $_barcode Штрихкод, по которому нужно найти
     */
    private $_barcode;

    /**
     * Search constructor.
     * @param $barcode
     */
    public function __construct($barcode)
    {
        $barcode = BarcodeHelper::toLatin($barcode);
        $barcode = mb_strtoupper($barcode);
        $this->_barcode = $barcode;
    }

    /**
     * Валидация штрихкода
     */
    public function validate()
    {
        if (mb_stripos($this->_barcode,'-') !== false) {
            throw new \DomainException('Нужно вводить штрихкод, а не сам артикул !!!');
        }
        if ( $this->_isWorkOrderBarcode($this->_barcode) && !Naryad::find()->where(['barcode' => $this->_barcode])->one() ) {
            throw new \DomainException('По данному штрихкоду невозможно найти наряд, вводи артикул !!!');
        }
        if (!$this->_isWorkOrderBarcode($this->_barcode) && (!$this->_isArticleBarcode($this->_barcode) || !mb_strlen($this->_barcode) ) == 9) {
            throw new \DomainException('Данный штрихкод не является шрихкодом автошторок !!!');
        }
    }

    /**
     * Создает DTO объект для печати этикетки
     *
     * @return object
     */
    public function getArticleObj()
    {
        $result = [
            'title'   => "Нет наименования.ОШИБКА",
            'carcass' => "Нет кузова.ОШИБКА",
            'doors'   => "Нет дверей.ОШИБКА",
            'article' => "Нет артикула.ОШИБКА",
        ];

        $article = $this->createArticle();
        $car = ArticleHelper::getCarModel($article);
        /**
         * @var $car Cars
         */
        if ($car) {
            $result = [
                'title'   => $car->shortEnName,
                'carcass' => $car->enCarcass,
                'doors'   => $car->doors,
                'article' => $car->article,
            ];
        }

        return (object)$result;
    }


    private function _isArticleBarcode($barcode)
    {
        $str = mb_substr($barcode,0,2);
        return in_array($str, ['FW','FD','FV','RD','RV','BW']);
    }

    private function _isWorkOrderBarcode($barcode)
    {
        $str = mb_substr($barcode,0,1);
        return in_array($str, ['L']);
    }

    /**
     * @return null|string
     */
    private function createArticle()
    {
        $this->validate();

        if ($this->_isWorkOrderBarcode($this->_barcode)) {
            $upn = Naryad::find()->where(['barcode' => $this->_barcode])->one();
            $article = $upn->article;
        } elseif ($this->_isArticleBarcode($this->_barcode)) {
            $value = [];
            $article = null;
            $window = mb_substr($this->_barcode,0,2);
            $carArticle = ltrim(mb_substr($this->_barcode,2,4),0);
            $cloth = ($res = mb_substr($this->_barcode,6,2)) == '00' ? '0' : ltrim(mb_substr($this->_barcode,6,2),0);
            $type = mb_substr($this->_barcode,8);

            $car =  Cars::find()->where(['article' => $carArticle])->one();
            if (!$car) {
                throw new \DomainException('Автомобиль не найден в системе Biz-zone');
            }

            $value[] = $window;
            $value[] = $mark = $car->getTranslit(mb_substr($car->mark,0,1));
            $value[] = $carArticle;
            $value[] = $cloth;
            $value[] = $type;
            $article = implode('-',$value);
        } else {
            throw new \DomainException('Данный штрихкод не является шрихкодом автошторок !!!');
        }

        return $article;
    }
}