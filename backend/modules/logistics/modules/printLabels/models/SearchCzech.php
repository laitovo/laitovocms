<?php

namespace backend\modules\logistics\modules\printLabels\models;

use backend\helpers\ArticleHelper;
use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use common\models\laitovo\Cars;
use core\models\article\Article;

class SearchCzech
{
    /**
     * @var string $_barcode Штрихкод, по которому нужно найти
     */
    private $_barcode;

    /**
     * Search constructor.
     * @param $barcode
     */
    public function __construct($barcode)
    {
        $barcode = BarcodeHelper::toLatin($barcode);
        $barcode = mb_strtoupper($barcode);
        $this->_barcode = $barcode;
    }

    /**
     * Валидация штрихкода
     */
    public function validate()
    {
        if (!$this->searchUpn($this->_barcode)) {
            throw new \DomainException('Данный штрихкод не является шрихкодом UPN комплекта !!!');
        }
    }

    /**
     * Создает DTO объект для печати этикетки
     *
     * @return object
     */
    public function getArticleObj()
    {
        $result = [
            'carName'   => "Нет наименования.ОШИБКА",
            'article' => "Нет артикула.ОШИБКА",
        ];

        $article = $this->createArticle();
        $articleObj = new Article($article);
        $car = $articleObj->car;

        /**
         * @var $car Cars
         */
        if ($car) {

            $result = [
                'carName'   => $car->fullEnName,
                'article' => 'RHS-' . $articleObj->carArticle .'-1-5',
            ];
        }

        return (object)$result;
    }

    /**
     * Проверка что был пропикан Papa upn
     *
     * @param $barcode
     * @return array|\yii\db\ActiveRecord|null
     */
    private function searchUpn($barcode)
    {
        return Naryad::find()->where(['barcode' => $barcode,'isParent' => true])->one();
    }

    /**
     * @return mixed
     */
    private function createArticle()
    {
        $this->validate();

        /**
         * @var $upn Naryad
         */
        $upn = $this->searchUpn($this->_barcode);
        if ($upn && $upn->children) {
            foreach ($upn->children as $child) {
                return $child->article;
                break;
            }
        }

        throw new \DomainException('Не возможно получить артикул !!!');
    }
}