<?php
/**
 * @var $model object
 */

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>

<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"
       style="border: none;font-size: 14px;">
    <tbody>
    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 11px;font-family: 'Roboto', sans-serif;">
            <b>Imported & Marked By :</b> HOTTRACKS<br>
            <b>Address :</b> No. 17, Victoria Road,Bangalore - 560047<br>
            <b>Part No:</b>  LAITOVO SHADES <br>
            <b>Description:</b> <?= $model->carName ?> Full Set<br>
            <b>Date of Purchased  :</b> Mar-2019<br>
            <b>MRP :</b> RS 23,990/-  ( Inclusive of all taxes<br>
            <b>Manufactured By :</b> Individual Entrepreneur<br>
            Grigorev Dmitrii Lvovich, OGRN 311440123400034,156005 Russia<br>
            <b>Legal metrology packing licence no :</b><br> KAR155/11-12 <br>
        </td>
    </tr>
    </tbody>
</table>
