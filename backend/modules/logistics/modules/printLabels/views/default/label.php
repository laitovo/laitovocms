<?php

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>

<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;border-bottom: 1px solid;font-family: 'Roboto', sans-serif;">
            <b><?= $model->title ?></b>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 18px;border-bottom: 1px solid;font-family: 'Roboto', sans-serif;">
            <b><?= $model->article ?></b>
        </td>
    </tr>
    <tr>
        <td rowspan="2" style="border-collapse: collapse;padding: 5px;border-bottom: none;border: none;">
            <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 2) ?>
        </td>
    </tr>
    </tbody>
</table>
