<?php

/* @var $this yii\web\View */

$this->params['menuItems'][0]['label'] = '<span class="lead">Печать документов</span>';

$this->params['menuItems'][0]['items'][0]['label'] = 'Печать документов Европа';
//
$this->params['menuItems'][0]['items'][0]['items'][0]['label'] = Yii::t('app', 'Упак. лист на отгрузку');
$this->params['menuItems'][0]['items'][0]['items'][0]['url'] = ['/logistics/print-documents/default/pack-list-box'];

$this->params['menuItems'][0]['items'][0]['items'][1]['label'] = Yii::t('app', 'Упак. лист на отправку');
$this->params['menuItems'][0]['items'][0]['items'][1]['url'] = ['/logistics/print-documents/default/pack-list-shipment'];

$this->params['menuItems'][0]['items'][0]['items'][2]['label'] = Yii::t('app', 'Инвойс / Спецификация');
$this->params['menuItems'][0]['items'][0]['items'][2]['url'] = ['/logistics/print-documents/default/invoice'];

//

$this->params['menuItems'][0]['items'][0]['label'] = 'Печать документов Россия';
//
//$this->params['menuItems'][0]['items'][0]['items'][0]['label'] = Yii::t('app', 'Упак. лист на отгрузку');
//$this->params['menuItems'][0]['items'][0]['items'][0]['url'] = ['/logistics/automatic-order/monitor'];
//
//$this->params['menuItems'][0]['items'][0]['items'][1]['label'] = Yii::t('app', 'Упак. лист на отправку');
//$this->params['menuItems'][0]['items'][0]['items'][1]['url'] = ['/logistics/automatic-order/monitor'];
//
//$this->params['menuItems'][0]['items'][0]['items'][2]['label'] = Yii::t('app', 'Invoice');
//$this->params['menuItems'][0]['items'][0]['items'][2]['url'] = ['/logistics/automatic-order/monitor'];
//
//$this->params['menuItems'][0]['items'][0]['items'][3]['label'] = Yii::t('app', 'Спецификация');
//$this->params['menuItems'][0]['items'][0]['items'][3]['url'] = ['/logistics/automatic-order/monitor'];