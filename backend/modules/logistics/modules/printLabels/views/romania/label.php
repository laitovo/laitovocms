<?php
/**
 * @var $model object
 */

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>

<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;font-family: 'Roboto', sans-serif;">
            <b>Perdele auto ART-Luxury compatibile:</b><br>
            <b><?= $model->title ?></b>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;font-family: 'Roboto', sans-serif;">
            <b>Tip caroserie: <?= $model->carcass ?></b>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;font-family: 'Roboto', sans-serif;">
            <b>Nr. usi: <?= $model->doors ?></b>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;font-family: 'Roboto', sans-serif;">
            <b>Cod produs: <?= $model->article ?></b>
        </td>
    </tr>
    </tbody>
</table>
