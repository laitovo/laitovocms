<?php

/**
 * @var  $articlesProvider \yii\data\ArrayDataProvider Провайдер данных для артикулов
 */

use yii\grid\GridView;
use \yii\helpers\Html;
use yii\helpers\Url;
use \backend\widgets\ActiveForm;

$this->title = Yii::t('app', 'Монитор ручного автозаказа');

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
//$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');
$this->registerCss(" 
    .mini-size { font-size: 0.9em } 
    .filter {padding:10px}
    .window-filter-container {}    
");

?>
<?php
//\yii\widgets\Pjax::begin(['id' => 'myPjax']);
?>
<div class="row">
    <div class="col-sm-4">
        <div class="input-group input-group-sm">
            <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
            <?= Html::input($type='text', $name = null, $value = null, ['class' => 'form-control','id' => 'check-article','placeholder' => 'Введите артикул, например ... FD-V-530-1-2']); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <?= Html::a($text='Искать', $url = null, [
            'class' => 'btn btn-default btn-sm',
            'onclick' => 'if ($(\'#check-article\').val()) {searchArticle(),$(\'.check-article-info\').slideDown()} else {$(\'.check-article-info\').slideUp()};
                '
        ]); ?>
        <?= Html::button($content='Очистить', [
            'class' => 'btn btn-default btn-sm',
            'onclick' => '($(\'#check-article\').val(\'\')); $(\'.check-article-info\').slideUp();
                '
        ]); ?>
    </div>
    <div class="col-sm-4">
        <?= Html::input('number','count', 1, ['id' => 'count-textbox-search', 'min' => '1', 'style' => 'width: 50px;'])?>
        <?= Html::button('Пополнить', ['class' => 'btn btn-primary replenish-button-2 btn-sm', 'data-pjax' => 1])?>
    </div>
</div>
<div class="clearfix"></div>

<hr>
<div class="row check-article-info" style="display: none;">
    <!-- Область вывода статиски -->
    <table class="table table-hover">
        <thead>
        <tr>
            <th>(Аналоги) Автомобиль</th>
            <th>Наименование группы</th>
            <th>Артикул</th>
            <th>Аналоги</th>
            <th>РПП</th>
            <th>РП</th>
<!--            <th>ПНП</th>-->
            <th>ПР</th>
<!--            <th>КСП</th>-->
<!--            <th>КК</th>-->
            <th>ЧР</th>
            <th>ОП</th>
            <th>Продажи<br>(6 мес)</th>
            <th>Фактические остатки</th>
            <th>На производстве</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="auto-search"></td>
            <td class="group-search"></td>
            <td class="article-search"></td>
            <td class="analogs-search"></td>
            <td class="rpp-search"></td>
            <td class="rp-search"></td>
<!--            <td class="pnp-search"></td>-->
            <td class="pr-search"></td>
<!--            <td class="ksp-search"></td>-->
<!--            <td class="kk-search"></td>-->
            <td class="chr-search"></td>
            <td class="op-search"></td>
            <td class="stat-search"></td>
            <td class="of-search"></td>
            <td class="inp-search"></td>
        </tr>
        </tbody>
    </table>
</div>

<div class="row">
    <div id="work-orders-list"></div>
</div>


<div class="row">
    <div class="col-md-6">
        <!--Форма для фильтрации по оконному проему:начало-->
        <?= Html::beginForm();?>
        <div class="window-filter-container pull-left">
            <div class="pull-left filter">
                <?= Html::hiddenInput('Submit','true')?>
                <?= Html::tag('div',
                    Html::checkbox('Windows[]', $checked = in_array('FW',$windows), ['id' => 'FW','value' => 'FW']) .
                    Html::tag('label', 'Передняя шторка',['for' => 'FW']),
                    ['class' => 'checkbox-custom checkbox-primary text-left'])
                ?>
                <?= Html::tag('div',
                    Html::checkbox('Windows[]', $checked = in_array('FV',$windows), ['id' => 'FV','value' => 'FV']) .
                    Html::tag('label', 'Передние форточки',['for' => 'FV']),
                    ['class' => 'checkbox-custom checkbox-primary text-left'])
                ?>
                <?= Html::tag('div',
                    Html::checkbox('Windows[]', $checked = in_array('FD',$windows), ['id' => 'FD','value' => 'FD']) .
                    Html::tag('label', 'Передние боковые',['for' => 'FD']),
                    ['class' => 'checkbox-custom checkbox-primary text-left'])
                ?>
            </div>
            <div class="pull-left filter">
                <?= Html::tag('div',
                    Html::checkbox('Windows[]', $checked = in_array('RD',$windows), ['id' => 'RD','value' => 'RD']) .
                    Html::tag('label', 'Задние боковые',['for' => 'RD']),
                    ['class' => 'checkbox-custom checkbox-primary text-left'])
                ?>
                <?= Html::tag('div',
                    Html::checkbox('Windows[]', $checked = in_array('RV',$windows), ['id' => 'RV','value' => 'RV']) .
                    Html::tag('label', 'Задние форточки',['for' => 'RV']),
                    ['class' => 'checkbox-custom checkbox-primary text-left'])
                ?>
                <?= Html::tag('div',
                    Html::checkbox('Windows[]', $checked = in_array('BW',$windows), ['id' => 'BW','value' => 'BW']) .
                    Html::tag('label', 'Задняя шторка',['for' => 'BW']),
                    ['class' => 'checkbox-custom checkbox-primary text-left'])
                ?>
            </div>
            <div class="pull-left filter">
                <?= Html::tag('div',
                    Html::checkbox('Brands[]', $checked = in_array('Laitovo',$brands), ['id' => 'Laitovo','value' => 'Laitovo']) .
                    Html::tag('label', 'Laitovo',['for' => 'Laitovo']),
                    ['class' => 'checkbox-custom checkbox-primary text-left'])
                ?>
                <?= Html::tag('div',
                    Html::checkbox('Brands[]', $checked = in_array('Chiko',$brands), ['id' => 'Chiko','value' => 'Chiko']) .
                    Html::tag('label', 'Chiko',['for' => 'Chiko']),
                    ['class' => 'checkbox-custom checkbox-primary text-left'])
                ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <?
                    echo \kartik\select2\Select2::widget([
                        'name' => 'carName',
                        'value' => $carName,
                        'data' => array_merge(['' => ''],$allCars),
                        'options' => ['placeholder' => 'Выберите автомобиль']
                    ]);
                ?>
            </div>
            <div class="clearfix"></div>
            <br>
        </div>
        <div class="clearfix"></div>
        <?= Html::submitButton('Отфильтровать',['class' => 'btn btn-sm btn-primary btn-outline'])?>&nbsp;
        <?= Html::button('Сбросить фильтры',['class' => 'btn btn-sm btn-default btn-outline my-button-reset'])?>

        <?= Html::endForm();?>
    </div>

    <div class="col-md-6">
        <!--Форма для фильтрации по оконному проему:начало-->
        <? $form = ActiveForm::begin(['method'=>'post','action'=>'']);?>
        <div class="window-filter-container pull-left">
            <div class="pull-left filter">
                <?= $form->field($statistics, 'dateFrom')->widget(\yii\jui\DatePicker::className(), [
                    //'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => ['class' => 'form-control'],
                ]) ?>
            </div>
            <div class="pull-left filter">
                <?= $form->field($statistics, 'dateTo')->widget(\yii\jui\DatePicker::className(), [
                    //'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => ['class' => 'form-control'],
                ]) ?>
            </div>
            <p>Статистика:
                <ul>
                    <li>Прибыло : <?= $result->in?> ( ОТК : <?= $result->inOTK?> - ПОПОЛНЕНИЕ : <?= $result->inAdd?> ) </li>
                    <li>Убыло : <?= $result->out?> ( ОТК : <?= $result->outOTK?> - ПОПОЛНЕНИЕ : <?= $result->outAdd?> - ОСТОЙНИК : <?= $result->outSump?> <span data-toggle="tooltip" data-original-title="Количество нарядов с отстойника, которые переделывались начиная с учатска 'ИЗГИБ' (в том числе)">[<?= $result->outSumpRework?>]</span> )</li>
                </ul>
            </p>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <?= Html::submitButton('Отфильтровать',['class' => 'btn btn-sm btn-primary btn-outline'])?>&nbsp;
        <?= Html::button('Сбросить фильтры',['class' => 'btn btn-sm btn-default btn-outline my-button-reset-2'])?>

        <? ActiveForm::end()?>
    </div>
</div>

<!--Форма для фильтрации по оконному проему:конец-->
<hr>

<div class="mini-size">
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $articlesProvider,
    'columns' => [
        [
            'attribute' => 'article',
            'label' => 'Артикул',
        ],
        [
            'attribute' => 'car',
            'label' => 'Машина',
            'format' => 'raw',
            'value' => function($data) {
                return $data->analogs ?  '<span data-original-title="Аналоги:<br>'.$data->analogs.'" data-toggle="tooltip" data-placement="top" data-html="true">'.$data->car.'</span>'
                    : '<span data-original-title="Аналогов нет" data-toggle="tooltip" data-placement="top" data-html="true">'.$data->car.'</span>';
            },
        ],
        [
            'attribute' => 'title',
            'label' => 'Наименование',
        ],
//        [
//            'attribute' => 'demand_in_percent',
//            'label' => 'Потребность, %',
//            'format' => 'raw',
//            'value' => function ($data) {
//                return '<span class="needRelative">' . $data->demand_in_percent . '</span>';
//            }
//        ],
        [
            'attribute' => 'statistics',
            'label' => 'Статистика',
        ],
        [
            'attribute' => 'balancePlan',
            'label' => 'Остаток ПЛАН',
        ],
//        [
//            'attribute' => 'demand_in_pieces',
//            'label' => 'Потребность, абс',
//            'format' => 'raw',
//            'value' => function ($data) {
//                return '<span class="needAbsolute">' . $data->demand_in_pieces . '</span>';
//            }
//        ],
        [
            'attribute' => 'balance_on_storage',
            'label' => 'На складе',
            'format' => 'raw',
            'value' => function ($data) {
                return '<span class="fact">' . $data->balance_on_storage . '</span>';
            }
        ],
        [
            'attribute' => 'balance_in_make',
            'label' => 'На производстве',
            'format' => 'raw',
            'value' => function ($data) {
                return '<span class="prod">' . $data->balance_in_make . '</span>';
            }
        ],
        [
            'attribute' => 'balance_in_make',
            'label' => 'Дата обновления',
            'format' => 'raw',
            'value' => function ($data) {
                return '<span class="update">' . Yii::$app->formatter->asDatetime($data->update_date) . '</span>';
            }
        ],
        [
            'label' => 'Обновить запись',
            'format' => 'raw',
            'value' => function ($article) {
                return Html::button('Обновить', ['class' => 'btn btn-info btn-outline refresh-button btn-sm', 'data' => ['article' => $article->article], 'data-pjax' => 1]);
            }
        ],
        [
            'label' => 'Панель действий',
            'format' => 'raw',
            'value' => function ($article) {
                return Html::button('Пополнить', ['class' => 'btn btn-primary replenish-button btn-sm', 'data' => ['article' => $article->article], 'data-pjax' => 1]);
            }
        ],
        [
            'label' => 'Сколько заказать',
            'format' => 'raw',
            'value' => function($article) {
                return Html::input('number','count', $article->default_replenished_count, ['class' => 'count-textbox', 'min' => '1', 'style' => 'width: 50px;']);
            }
        ],
    ],
]) ?>
</div>
<?php Yii::$app->view->registerJs('
    //Данная функция отвечает за обработку нажатия кнопки "Сбросить фильтры"
    $(".my-button-reset").click(function() {
        var button = $(this);
        var form = button.closest("form");
        var submit = function() {
            form.submit();
        };
        var handle_function = function(){
            form.find(":checkbox").each(function(){
                $(this).prop("checked",false);
            });
            form.find("select").each(function(){
                $(this).children("option:selected").removeAttr("selected");
            });
            setTimeout(submit,200);
        };
        handle_function();
    });
    
        //Данная функция отвечает за обработку нажатия кнопки "Сбросить фильтры"
    $(".my-button-reset-2").click(function() {
        var button = $(this);
        var form = button.closest("form");
        var submit = function() {
            form.submit();
        };
        var handle_function = function(){
            form.find("input[type=text]").each(function(){
                $(this).val("");
                submit();
            });
        };
        handle_function();
    });
', \yii\web\View::POS_END);
?>
<?php Yii::$app->view->registerJs('
    //Данная функция отвечает за обработку нажатия кнопки "Обновить"
    $(".refresh-button").click(function() {
        var button = $(this);
        var article = $(this).data("article");
        var handle_function = function(){
            button.prop( "disabled", true );
            $.ajax({
                url: "/logistics/replenishment-monitor/default/refresh",
                dataType: "json",
                data: {
                    "article": article,
                },
                success: function (data) {
                    if (data.status != "success") {
                        notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                        button.prop( "disabled", false );
                    }else {
                        var tr = button.closest("tr");
                        tr.find(".needRelative").html(data.needRelative);
                        tr.find(".needAbsolute").html(data.needAbsolute);
                        tr.find(".fact").html(data.fact);
                        tr.find(".prod").html(data.prod);
                        tr.find(".update").html(data.update);
                        tr.css("background-color","LightCyan");
                        notie.alert(1, "Успешно выполнено обновление статистики артикула", 15);
                        button.remove();
                    }


                },
                error: function() {
                    notie.alert(3, "Не удалось выполнить команду", 15);
                    button.prop( "disabled", false );
                }
            });
        };
        handle_function();
    });
', \yii\web\View::POS_END);
?>
<?php Yii::$app->view->registerJs('
    //Данная функция отвечает за обработку нажатия кнопки "Пополнить"
    $(".replenish-button").click(function() {
        var button = $(this);
        var article = $(this).data("article");
        var count = $(this).closest("tr").find(".count-textbox").val();
        var handle_function = function(){
            button.prop( "disabled", true );
            $.ajax({
                url: "/logistics/replenishment-monitor/default/replenish",
                dataType: "json",
                data: {
                    "article": article,
                    "count": count
                },
                success: function (data) {
                    if (data.status != "success") {
                        notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    }
                    notie.alert(1, "Успешно выполнено пополнение артикула <span style=\'color: red\'>" + data.replenished_article + "</span> в количестве " + data.replenished_count + " шт", 15);
                    setTimeout(function() { window.location=window.location;},1000);
                },
                error: function() {
                    notie.alert(3, "Не удалось выполнить команду", 15);
                }
            });
        };
        notie.confirm("Вы действительно хотите обработать заказ?", "Да", "Нет", handle_function);
    });
', \yii\web\View::POS_END);
?>

<?php Yii::$app->view->registerJs('
    //Данная функция отвечает за обработку нажатия кнопки "Пополнить"
    $(".replenish-button-2").click(function() {
        var button = $(this);
        var article = $(this).data("article");
        var article = $("#check-article").val();
        var count = $(this).closest("tr").find(".count-textbox").val();
        var count = $("#count-textbox-search").val();
        console.log(article,count);
        var handle_function = function(){
            button.prop( "disabled", true );
            $.ajax({
                url: "/logistics/replenishment-monitor/default/replenish-and-pause",
                dataType: "json",
                data: {
                    "article": article,
                    "count": count
                },
                success: function (data) {
                    if (data.status != "success") {
                        notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    }
                    notie.alert(1, "Успешно выполнено пополнение артикула <span style=\'color: red\'>" + data.replenished_article + "</span> в количестве " + data.replenished_count + " шт. Список нарядов : " + data.replenished_article_work_orders , 60);
                    var prevHtml = $("#work-orders-list").html();
                    $("#work-orders-list").html(prevHtml + data.replenished_article + " : " + data.replenished_article_work_orders + "<br>");
                },
                error: function() {
                    notie.alert(3, "Не удалось выполнить команду", 15);
                }
            });
            button.prop( "disabled", false );
        };
        notie.confirm("Вы действительно хотите обработать заказ?", "Да", "Нет", handle_function);
   });
', \yii\web\View::POS_END);
?>

<?php
Yii::$app->view->registerJs('
var searchArticle = function() {
$.get("' . Url::to(['/ajax/automatic-article-search']) . '",{"article":$("#check-article").val()},function(data){
if (data.status == "error") {
notie.alert(3,data.message,3);
$(".auto-search").html("");
$(".group-search").html("");
$(".article-search").html("");
$(".analogs-search").html("");
$(".rpp-search").html("");
$(".rp-search").html("");
//$(".pnp-search").html("");
$(".pr-search").html("");
//$(".ksp-search").html("");
//$(".kk-search").html("");
$(".chr-search").html("");
$(".op-search").html("");
$(".stat-search").html("");
$(".of-search").html("");
$(".inp-search").html("");
} else {
notie.alert(1,"Успешно найдена статистика по артикулу",3);
$(".auto-search").html(data.message.auto);
$(".group-search").html(data.message.group);
$(".article-search").html(data.message.article);
$(".analogs-search").html(data.message.names);
$(".rpp-search").html(data.message.rpp);
$(".rp-search").html(data.message.rp);
//$(".pnp-search").html(data.message.pnp);
$(".pr-search").html(data.message.pr);
//$(".ksp-search").html(data.message.ksp);
//$(".kk-search").html(data.message.kk);
$(".chr-search").html(data.message.chr);
$(".op-search").html(data.message.op);
$(".stat-search").html(data.message.stat);
$(".of-search").html(data.message.of);
$(".inp-search").html(data.message.inp);
}
});
}
', \yii\web\View::POS_END);

?>

<?php
//\yii\widgets\Pjax::end();
?>
