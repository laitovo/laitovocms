<?php

namespace backend\modules\logistics\modules\replenishmentMonitor\controllers;

use backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticlesDtoProvider;
use backend\modules\logistics\modules\replenishmentMonitor\interfaces\ICommandService;
use backend\modules\logistics\modules\replenishmentMonitor\models\Statistics;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\base\Module;
use yii\web\Response;
use Yii;

class DefaultController extends Controller
{
    private $_articlesDtoProvider;
    private $_commandService;

    public function __construct(string $id, Module $module, IArticlesDtoProvider $articlesDtoProvider, ICommandService $commandService, array $config = [])
    {
        $this->_articlesDtoProvider = $articlesDtoProvider;
        $this->_commandService = $commandService;

        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            #Ограничиваю доступ к отчету для пользователей
                            $users = [2,21,120,48,14];
                            if (!in_array(Yii::$app->user->getId(),$users))
                                return false;
                            return Yii::$app->team->can('laitovo');
                        },
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $statistics = new Statistics();
        $result = $statistics->extractData(Yii::$app->request->post());

        $submit   = Yii::$app->request->post('Submit') ? true :false;
        $FWindows = Yii::$app->request->post('Windows') ?: [];
        $SWindows = Yii::$app->session->get('Windows') ?:[];
        $windows  = $submit ? $FWindows : $SWindows;
        Yii::$app->session->set('Windows',$windows);

        $FBrands = Yii::$app->request->post('Brands') ?: [];
        $SBrands = Yii::$app->session->get('Brands') ?:[];
        $brands  = $submit ? $FBrands : $SBrands;
        Yii::$app->session->set('Brands',$brands);

        $FCar = Yii::$app->request->post('carName') ?: '';
        $SCar = Yii::$app->session->get('carName') ?:'';
        $carName  = $submit ? $FCar : $SCar;
        Yii::$app->session->set('carName',$carName);
//        $cars    = Yii::$app->request->post('Cars')    ?:[];

        $articlesProvider = $this->_articlesDtoProvider->getArticlesAsDataProvider($windows,$brands,$carName);
        $allCars = $this->_articlesDtoProvider->getCarsArray();

        return $this->render('index', [
            'articlesProvider' => $articlesProvider,
            'windows' => $windows,
            'brands' => $brands,
            'statistics' => $statistics,
            'result' => $result,
            'allCars' => $allCars,
            'carName' => $carName,
//            'cars' => $cars,
        ]);
    }

    public function actionReplenish($article, $count)
    {
        ini_set('memory_limit', '256M');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $data = $this->_commandService->replenish($article, $count);
        if (!$data) {
            $status = 'error';
        }

        return array_merge(['status' => $status], $data);
    }

    public function actionReplenishAndPause($article, $count)
    {
        ini_set('memory_limit', '256M');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $data = $this->_commandService->replenishAndPause($article, $count);
        if (!$data) {
            $status = 'error';
        }

        return array_merge(['status' => $status], $data);
    }

    public function actionRefresh($article)
    {
        ini_set('memory_limit', '256M');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $data = $this->_commandService->refresh($article);
        if (!$data) {
            $status = 'error';
        }

        return array_merge(['status' => $status], $data);
    }
}