<?php

namespace backend\modules\logistics\modules\replenishmentMonitor\interfaces;

interface IConfigService
{
    public function getDefaultReplenishedCount();
}