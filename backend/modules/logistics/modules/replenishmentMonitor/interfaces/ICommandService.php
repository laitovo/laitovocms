<?php

namespace backend\modules\logistics\modules\replenishmentMonitor\interfaces;

interface ICommandService
{
    public function replenish($article_id, $count);
    public function replenishAndPause($article_id, $count);
    public function refresh($article_id);
}