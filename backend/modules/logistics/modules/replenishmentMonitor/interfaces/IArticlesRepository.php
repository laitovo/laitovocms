<?php

namespace backend\modules\logistics\modules\replenishmentMonitor\interfaces;

interface IArticlesRepository
{
    public function getArticles();
    public function getArticlesFilteredByWindows($windows,$brands,$carName);
}