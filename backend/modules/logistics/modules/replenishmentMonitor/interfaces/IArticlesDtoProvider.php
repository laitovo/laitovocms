<?php

namespace backend\modules\logistics\modules\replenishmentMonitor\interfaces;

interface IArticlesDtoProvider
{
    public function getArticlesAsDataProvider($windows,$brands,$carName);
    public function getCarsArray($windows,$brands);
}