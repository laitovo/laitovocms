<?php

namespace backend\modules\logistics\modules\replenishmentMonitor\interfaces;

interface IArticleService
{
    public function getId($article);
    public function getArticle($article);
    public function getCar($article);
    public function getTitle($article);
    public function getDemandInPercent($article);
    public function getDemandInPieces($article);
    public function getBalanceOnStorage($article);
    public function getBalanceInMake($article);
    public function getUpdateDate($article);
    public function getAnalogs($article);

}