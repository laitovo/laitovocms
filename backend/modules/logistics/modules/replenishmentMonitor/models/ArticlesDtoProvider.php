<?php

namespace backend\modules\logistics\modules\replenishmentMonitor\models;

use backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticleService;
use backend\modules\logistics\modules\replenishmentMonitor\interfaces\IArticlesRepository;
use backend\modules\logistics\modules\replenishmentMonitor\interfaces\IConfigService;
use yii\data\ArrayDataProvider;

class ArticlesDtoProvider
{
    private $_articlesRepository;
    private $_articleService;
    private $_configService;

    public function __construct(IArticlesRepository $articlesRepository, IArticleService $articleService, IConfigService $configService)
    {
        $this->_articlesRepository = $articlesRepository;
        $this->_articleService = $articleService;
        $this->_configService = $configService;
    }

    public function getArticlesAsDataProvider($windows = [],$brands = [],$carName = null)
    {
        return $this->_asProvider($this->getArticlesFilteredByWindows($windows,$brands,$carName),['statistics', 'balancePlan','balance_on_storage', 'balance_in_make']);
    }

    public function getCarsArray($windows = [],$brands = [],$carName = null)
    {
        $allCars = [];
        $rows = $this->getArticlesFilteredByWindows($windows,$brands,$carName);
        foreach ($rows as $row) {
            $carsString = $row->analogs;
            $carsArray = explode('<br>',$carsString);
            $indexCarsArray = [];
            foreach ($carsArray as $carsItem) {
                $indexCarsArray[$carsItem] = $carsItem;
            }
            $allCars = array_merge($allCars,$indexCarsArray);
            $allCars = array_unique($allCars);
        }
        asort($allCars);
        return $allCars;
    }


    public function getArticlesFilteredByWindows($windows,$brands,$carName)
    {
        $articles = $this->_articlesRepository->getArticlesFilteredByWindows($windows,$brands,$carName);
//        $articles = $this->_sortByDemandInPercent($articles, 'DESC');
        $dtoArticles = $this->_prepareArticles($articles);

        return $dtoArticles;
    }

    public function getArticles()
    {
        $articles = $this->_articlesRepository->getArticles();
//        $articles = $this->_sortByDemandInPercent($articles, 'DESC');
        $dtoArticles = $this->_prepareArticles($articles);

        return $dtoArticles;
    }

    private function _prepareArticles($articles)
    {
        $dtoArticles = [];
        foreach ($articles as $article) {
            $dtoArticles[] = $this->_createDTO($article);
        }

        return $dtoArticles;
    }

    private function _createDTO($article)
    {
        if (!$article) {
            return null;
        }
        $row['id'] = $this->_articleService->getId($article);
        $row['article'] = $this->_articleService->getArticle($article);
        $row['car'] = $this->_articleService->getCar($article);
        $row['title'] = $this->_articleService->getTitle($article);
        $row['analogs'] = $this->_articleService->getAnalogs($article);
        $row['demand_in_percent'] = $this->_formatPercentage($this->_articleService->getDemandInPercent($article));
        $row['demand_in_pieces'] = $this->_articleService->getDemandInPieces($article);
        $row['balance_on_storage'] = $this->_articleService->getBalanceOnStorage($article);
        $row['balance_in_make']    = $this->_articleService->getBalanceInMake($article);
        $row['update_date']    = $this->_articleService->getUpdateDate($article);
        $row['default_replenished_count'] = $this->_configService->getDefaultReplenishedCount();
        $row['statistics'] = $article->statistics;
        $row['balancePlan'] = $article->balancePlan;

        return (object)$row;
    }

    /**
     * Функция форматирует поля с процентами
     *
     * @param double|null $percentage
     * @return string
     */
    private function _formatPercentage($percentage)
    {
        if (!$percentage) {
            $percentage = 0;
        }

        return $percentage . '%';
    }

    /**
     * Функция сортирует артикулы по полю "Потребность в %"
     *
     * @param array $articles Массив объектов "Артикул"
     * @param string $ascOrDesc Порядок сортировки ("ASC" / "DESC")
     * @return array
     */
    private function _sortByDemandInPercent($articles, $ascOrDesc = 'ASC')
    {
        $ascOrDesc = strtoupper($ascOrDesc);
        if (!in_array($ascOrDesc, ['ASC', 'DESC'])) {
            $ascOrDesc = 'ASC';
        }
        uasort($articles, function ($article1, $article2) use ($ascOrDesc) {
            $percentage1 = $this->_articleService->getDemandInPercent($article1);
            $percentage2 = $this->_articleService->getDemandInPercent($article2);
            switch (true) {
                case $percentage1 > $percentage2:
                    return $ascOrDesc == 'ASC' ? 1 : -1;
                case $percentage1 < $percentage2:
                    return $ascOrDesc == 'ASC' ? -1 : 1;
                default:
                    return 0;
            }
        });

        return $articles;
    }

    /**
     * Функция оборачивает в дата провайдер некий массив
     *
     * @param $data
     * @param array $sort
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    private function _asProvider($data, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}