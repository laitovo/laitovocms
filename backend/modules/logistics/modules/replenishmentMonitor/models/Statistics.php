<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/23/18
 * Time: 8:49 AM
 */

namespace backend\modules\logistics\modules\replenishmentMonitor\models;

use Yii;
use yii\base\Model;
use backend\modules\laitovo\models\ErpNaryad;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;


class Statistics extends Model
{
    private $dateFrom;
    private $dateTo;


    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'date'],
        ];
    }

    public function setDateFrom($value)
    {
        $this->dateFrom = Yii::$app->formatter->asTimestamp($value);
    }

    public function getDateFrom()
    {
        return $this->dateFrom ?: mktime(0, 0, 0, date('m'), 1);
    }

    public function setDateTo($value)
    {
        $this->dateTo = Yii::$app->formatter->asTimestamp($value);
    }

    public function getDateTo()
    {
        return $this->dateTo ?: mktime(0,0,0);
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => Yii::t('app', 'Начало периода *(00:00:00)'),
            'dateTo' => Yii::t('app', 'Конец периода * (23:59:59)'),
        ];
    }

    public function extractData($params)
    {
        $this->load($params);

        //Всего прибы
        $in = ErpNaryad::find()->alias('t')->where(['and',
            ['>','t.distributed_date',$this->getDateFrom()],
            ['<','t.distributed_date',$this->getDateTo() + 24*60*60-1],
            ['t.order_id' => null],
            ['not like','t.article','OT-%',false],
        ])->count();

        $inOTK = ErpNaryad::find()->alias('t')->joinWith('upn')->where(['and',
            ['>','t.distributed_date',$this->getDateFrom()],
            ['<','t.distributed_date',$this->getDateTo() + 24*60*60-1],
            ['t.order_id' => null],
            ['not like','t.article','OT-%',false],
            ['logistics_naryad.responsible_person' => 'терминал выдачи'],
        ])->count();

        //Используя реализа
        $realizationLogManager  = Yii::$container->get('core\entities\logisticsRealizationLog\RealizationLogManager');

        /**
         * Получаем из лога все Upn, которые были реализованы за период.
         */
        $upns = $realizationLogManager->findWhereColumn('upnId',
            ['and',
                ['>','createdAt',$this->getDateFrom()],
                ['<','createdAt',$this->getDateTo() + 24*60*60-1],
            ]
        );

        $out = ErpNaryad::find()->alias('t')->where(['and',
            ['in','t.logist_id',$upns],
            ['or',['t.order_id' => null],['t.isFromSump' => true]],
            ['not like','t.article','OT-%',false],
        ])->count();

        $outSump = ErpNaryad::find()->alias('t')->joinWith('upn')->where(['and',
            ['in','t.logist_id',$upns],
            ['not like','t.article','OT-%',false],
            ['t.isFromSump' => true],
        ])->count();

        $outSumpRework = ErpNaryad::find()->alias('t')->joinWith('reworkActs as act')->where(['and',
            ['in','t.logist_id',$upns],
            ['not like','t.article','OT-%',false],
            ['t.isFromSump' => true],
            ['act.location_start' => Yii::$app->params['erp_izgib']],
        ])
        ->groupBy('t.logist_id')
        ->count();

        $outOTK = ErpNaryad::find()->alias('t')->joinWith('upn')->where(['and',
            ['in','t.logist_id',$upns],
            ['t.order_id' => null],
            ['not like','t.article','OT-%',false],
            ['logistics_naryad.responsible_person' => 'терминал выдачи'],
        ])->count();

        $result = [
            'in' => $in,
            'inOTK' => $inOTK,
            'inAdd' => $in - $inOTK,
            'out' => $out,
            'outOTK' => $outOTK,
            'outAdd' => $out - $outOTK - $outSump,
            'outSump' => $outSump,
            'outSumpRework' => $outSumpRework,
        ];

        return (object)$result;
    }
}