<?php

namespace backend\modules\logistics\modules\logisticsMonitor\controllers;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentsProvider;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemsProvider;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrdersProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\Module;

/**
 * Модуль "Монитор логистики". Основной контроллер.
 *
 * Class DefaultController
 * @package backend\modules\logistics\modules\logisticsMonitor\controllers
 */
class DefaultController extends Controller
{
    /**
     * @var IOrdersProvider DTO-провайдер заказов
     */
    private $_ordersProvider;
    /**
     * @var IOrderItemsProvider DTO-провайдер элементов заказа
     */
    private $_orderItemsProvider;
    /**
     * @var IOrderDocumentsProvider DTO-провайдер документов заказа
     */
    private $_orderDocumentsProvider;

    /**
     * DefaultController constructor.
     * @param string $id
     * @param Module $module
     * @param IOrdersProvider $ordersProvider Некий провайдер, предоставляющий заказы, готовые к отображению
     * @param IOrderItemsProvider $orderItemsProvider Некий провайдер, предоставляющий элементы заказа, готовые к отображению
     * @param IOrderDocumentsProvider $orderDocumentsProvider Некий провайдер, предоставляющий документы заказа, готовые к отображению
     * @param array $config
     */
    public function __construct(string $id, Module $module, IOrdersProvider $ordersProvider, IOrderItemsProvider $orderItemsProvider, IOrderDocumentsProvider $orderDocumentsProvider, array $config = [])
    {
        $this->_ordersProvider = $ordersProvider;
        $this->_orderItemsProvider = $orderItemsProvider;
        $this->_orderDocumentsProvider = $orderDocumentsProvider;

        parent::__construct($id, $module, $config);
    }

    /**
     * Просмотр списка заказов
     *
     * @return string
     */
    public function actionIndex()
    {
        $ordersProvider = $this->_ordersProvider->getOrdersAsDataProvider();

        return $this->render('index', [
            'ordersProvider' => $ordersProvider,
        ]);
    }

    /**
     * Просмотр одного заказа
     *
     * @param mixed $id Идентификатор заказа
     * @return string
     */
    public function actionView($id)
    {
        $order = $this->findModel($id);
        $orderItemsProvider = $this->_orderItemsProvider->getItemsByOrderIdAsDataProvider($id);
        $orderDocumentsWidgetData = $this->_orderDocumentsProvider->getDocumentsByOrderIdAsFileWidgetData($id);

        return $this->render('view', [
            'order' => $order,
            'orderItemsProvider' => $orderItemsProvider,
            'orderDocumentsWidgetData' => $orderDocumentsWidgetData,
        ]);
    }

    /**
     * Finds the MaterialProductionScheme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = $this->_ordersProvider->getOrderById($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}