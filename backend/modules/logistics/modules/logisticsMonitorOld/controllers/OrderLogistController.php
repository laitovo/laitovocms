<?php

namespace backend\modules\logistics\modules\logisticsMonitor\controllers;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderLogistService;
use yii\web\Controller;
use Exception;
use yii\web\Response;
use Yii;
use yii\base\Module;

/**
 * Модуль "Монитор логистики". Контроллер для обработки команд от логиста.
 *
 * Class OrderLogistController
 * @package backend\modules\logistics\modules\logisticsMonitor\controllers
 */
class OrderLogistController extends Controller
{
    /**
     * @var IOrderLogistService $_orderLogistService Сервис для работы логиста с заказом
     */
    private $_orderLogistService;

    /**
     * OrderLogistController constructor.
     * @param string $id
     * @param Module $module
     * @param IOrderLogistService $orderLogistService Некий сервис, предоставляющий функционал для работы логиста с заказом
     * @param array $config
     */
    public function __construct(string $id, Module $module, IOrderLogistService $orderLogistService, array $config = [])
    {
        $this->_orderLogistService = $orderLogistService;

        parent::__construct($id, $module, $config);
    }

    /**
     * Пометка что логист завершил работу над заказом
     * (ajax-функция)
     *
     * @param int $order_id Идентификатор заказа
     * @return array
     */
    public function actionHandle($order_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_orderLogistService->handle($order_id);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status,
        ];
    }

    /**
     * Пометка (или отмена пометки) для склада (кладовщика) что заказ должен быть взвешен
     * (ajax-функция)
     *
     * @param int $order_id Идентификатор заказа
     * @return array
     */
    public function actionUpdateWeightStatus($order_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $data = $this->_orderLogistService->updateWeightStatus($order_id);
        if (!$data) {
            $status = 'error';
        }

        return array_merge(['status' => $status], $data);
    }

    /**
     * Сохранение даты отгрузки заказа
     * (ajax-функция)
     *
     * @param int $order_id Идентификатор заказа
     * @param string $shipment_date Дата отгрузки
     * @return array
     */
    public function actionSetShipmentDate($order_id, $shipment_date)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_orderLogistService->setShipmentDate($order_id, $shipment_date);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Сохранение трэк-кода заказа
     * (ajax-функция)
     *
     * @param int $order_id Идентификатор заказа
     * @param string $track_code Трэк-код
     * @return array
     */
    public function actionSetTrackCode($order_id, $track_code)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_orderLogistService->setTrackCode($order_id, $track_code);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Сохранение веса заказа
     * (ajax-функция)
     *
     * @param int $order_id Идентификатор заказа
     * @param string $weight Вес
     * @return array
     */
    public function actionSetWeight($order_id, $weight)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = 'success';
        $result = $this->_orderLogistService->setWeight($order_id, $weight);
        if (!$result) {
            $status = 'error';
        }

        return [
            'status' => $status
        ];
    }

    /**
     * Загрузка нового документа
     * (ajax-функция)
     *
     * @return array
     * @throws Exception
     */
    public function actionUploadDocument()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = $this->_orderLogistService->uploadDocument(Yii::$app->request->post('orderId'), 'file');
        if (!$data) {
            throw new Exception('Не удалось загрузить документ');
        }

        return array_merge(['status' => 'success'], $data);
    }

    /**
     * Удаление документа
     * (ajax-функция)
     *
     * @return array
     * @throws Exception
     */
    public function actionDeleteDocument()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = $this->_orderLogistService->deleteDocument(Yii::$app->request->post('id'));
        if (!$result) {
            throw new Exception('Не удалось удалить документ');
        }

        return [
            'status' => 'success'
        ];
    }
}