<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\modules\logistics\modules\logisticsMonitor\assets;

use yii\web\AssetBundle;

class ViewAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/logistics/modules/logisticsMonitor/assets/sources';

    public $css = [
        'css/view.css',
    ];
    public $js = [
        'js/view.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
//    public $publishOptions = [
//        'forceCopy'=>true,
//    ];
}
