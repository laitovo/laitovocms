//Данная функция отвечает за обработку нажатия кнопки "Обработать"
$(".handle-order-button").click(function() {
    var order_id = $(this).data("order-id");
    var handle_function = function () {
        $.ajax({
            url: "/logistics/logistics-monitor/order-logist/handle",
            dataType: "json",
            data: {
                "order_id": order_id
            },
            success: function (data) {
                if (data.status != "success") {
                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    return;
                }
                preventEditing();
                notie.alert(1, "Успешно выполнено", 1);
            },
            error: function() {
                notie.alert(3, "Не удалось выполнить команду", 3);
            }
        });
    };
    notie.confirm("Вы действительно хотите обработать заказ?", "Да", "Нет", handle_function);
});

//Данная функция отвечает за обработку нажатия кнопки "Взвесить" (или "Отменить взвешивание") в блоке транспортной компании
$(".transport-company-block .weigh-button").click(function() {
    var order_id = $(this).data("order-id");
    var confirm_message = $(this).data("confirm-message");
    var update_function = function () {
        $.ajax({
            url: "/logistics/logistics-monitor/order-logist/update-weight-status",
            dataType: "json",
            data: {
                "order_id": order_id
            },
            success: function (data) {
                if (data.status != "success") {
                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    return;
                }
                applyWeightStatus(data.has_weighing, data.weight_status);
                notie.alert(1, "Успешно выполнено", 1);
            },
            error: function() {
                notie.alert(3, "Не удалось выполнить команду", 3);
            }
        });
    };
    notie.confirm(confirm_message, "Да", "Нет", update_function);
});

//Данная функция отвечает за обработку нажатия кнопки "Редактировать" в блоке для редактирования свойства
$(".edit-block .edit-button").click(function() {
    savePrevValue($(this).closest(".edit-block").find(".property-field"));
    changeEditBlockState($(this).closest(".edit-block"));
});

//Данная функция отвечает за обработку нажатия кнопки "Отменить" в блоке для редактирования свойства
$(".edit-block .cancel-button").click(function() {
    backToPrevValue($(this).closest(".edit-block").find(".property-field"));
    changeEditBlockState($(this).closest(".edit-block"));
});

//Данная функция отвечает за сохранение даты отгрузки
$(".shipment-date-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var order_id = property_field.data("order-id");
    var shipment_date = property_field.val();
    $.ajax({
        url: "/logistics/logistics-monitor/order-logist/set-shipment-date",
        dataType: "json",
        data: {
            "order_id": order_id,
            "shipment_date": shipment_date
        },
        success: function (data) {
            if (data.status != "success") {
                notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                return;
            }
            notie.alert(1, "Успешно сохранено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Не удалось сохранить", 3);
        }
    });
});

//Данная функция отвечает за сохранение трек-кода
$(".track-code-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var order_id = property_field.data("order-id");
    var track_code = property_field.val();
    $.ajax({
        url: "/logistics/logistics-monitor/order-logist/set-track-code",
        dataType: "json",
        data: {
            "order_id": order_id,
            "track_code": track_code
        },
        success: function (data) {
            if (data.status != "success") {
                notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                return;
            }
            notie.alert(1, "Успешно сохранено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Не удалось сохранить", 3);
        }
    });
});

//Данная функция отвечает за сохранение веса
$(".weight-save-button").click(function() {
    var edit_block = $(this).closest(".edit-block");
    var property_field = edit_block.find(".property-field");
    if (!isValueUpdated(property_field)) {
        changeEditBlockState(edit_block);
        return;
    }
    var order_id = property_field.data("order-id");
    var weight = property_field.val();
    $.ajax({
        url: "/logistics/logistics-monitor/order-logist/set-weight",
        dataType: "json",
        data: {
            "order_id": order_id,
            "weight": weight
        },
        success: function (data) {
            if (data.status != "success") {
                notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                return;
            }
            notie.alert(1, "Успешно сохранено", 1);
            changeEditBlockState(edit_block);
        },
        error: function() {
            notie.alert(3, "Не удалось сохранить", 3);
        }
    });
});

//Данная функция отвечает за раскрытие/закрытие блока для редактирования свойства
function changeEditBlockState(edit_block) {
    var property_field = edit_block.find(".property-field");
    if (edit_block.hasClass("closed")) {
        edit_block.find(".edit-button").addClass("hidden");
        edit_block.find(".save-button, .cancel-button").removeClass("hidden");
        property_field.prop("disabled", false).focus();
        edit_block.removeClass("closed").addClass("opened");
    } else {
        edit_block.find(".edit-button").removeClass("hidden");
        edit_block.find(".save-button, .cancel-button").addClass("hidden");
        property_field.prop("disabled", true);
        edit_block.removeClass("opened").addClass("closed");
    }
}

//Данная функция отвечает за сохранение текущего состояния свойства
function savePrevValue(property_field) {
    property_field.data("prev-value", property_field.val());
}

//Данная функция отвечает за возвращение свойства в предыдущее состояние
function backToPrevValue(property_field) {
    property_field.val(property_field.data("prev-value"));
}

//Данная функция отвечает за проверку было ли изменено свойство
function isValueUpdated(property_field) {
    return property_field.val() !== property_field.data("prev-value");
}

//Данная функция отвечает за отображение полей, которые зависят от весового статуса
function applyWeightStatus(has_weighing, weight_status) {
    var weigh_button = $(".transport-company-block .weigh-button");
    $('.transport-company-block .weight-status').html(weight_status);
    if (has_weighing) {
        weigh_button.find('.button-text').html(weigh_button.data('cancel-text'));
        weigh_button.data('confirm-message', weigh_button.data('cancel-confirm'));
        $('.weight-edit-block').removeClass('hide');
    } else {
        weigh_button.find('.button-text').html(weigh_button.data('applying-text'));
        weigh_button.data('confirm-message', weigh_button.data('applying-confirm'));
        $('.weight-edit-block').addClass('hide');
    }
}

//Данная функция запрещает редактирование заказа
function preventEditing() {
    $('.edit-block:not(.closed) .cancel-button').click();
    $('.hide-after-handling').addClass('hide');
    $('#file-input').fileinput('disable');
}
