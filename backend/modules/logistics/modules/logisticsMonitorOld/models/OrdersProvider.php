<?php

namespace backend\modules\logistics\modules\logisticsMonitor\models;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderService;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrdersRepository;
use yii\data\ArrayDataProvider;

/**
 * DTO-провайдер модуля "Монитор логистики", который знает как переделать некие сторонние заказы в нужный модулю формат.
 *
 * Class OrdersProvider
 * @package backend\modules\logistics\modules\logisticsMonitor\models
 */
class OrdersProvider
{
    /**
     * @var IOrdersRepository $_orderRepository Репозиторий заказов
     */
    private $_orderRepository;

    /**
     * @var IOrderService $_orderService Сервис для доступа к свойствам заказа
     */
    private $_orderService;

    /**
     * OrdersProvider constructor.
     * @param IOrdersRepository $orderRepository Некий репозиторий, предоставляющий провайдеру заказы в первозданном виде
     * @param IOrderService $orderService Некий сервис для доступа к свойствам заказа
     */
    public function __construct(IOrdersRepository $orderRepository, IOrderService $orderService)
    {
        $this->_orderRepository = $orderRepository;
        $this->_orderService = $orderService;
    }

    /**
     * Функция возвращает дата-провайдер с заказами, подготовленными к отображению
     *
     * @return ArrayDataProvider
     */
    public function getOrdersAsDataProvider()
    {
        $orders = $this->getOrders();
        $sortedOrders = $this->_sortByLogisticsStatus($orders);
        $ordersProvider = $this->_asProvider($sortedOrders);

        return $ordersProvider;
    }

    /**
     * Функция возвращает массив заказов, подготовленных к отображению
     *
     * @return object[] Массив DTO-объектов "Заказ"
     */
    public function getOrders()
    {
        $orders = $this->_orderRepository->getOrders();
        $data = $this->_prepareOrders($orders);

        return $data;
    }

    /**
     * Функция возвращает по переданному идентификатору заказ, подготовленный к отображению
     *
     * @param $id int Идентификатор заказа
     * @return object DTO-объект "Заказ"
     */
    public function getOrderById($id)
    {
        $order = $this->_orderRepository->getOrderById($id);
        $data = $this->_createDTO($order);

        return $data;
    }

    /**
     * Функция подготавливает заказы к отображению
     *
     * @param array $orders Массив объектов "Заказ"
     * @return object[] Массив DTO-объектов "Заказ"
     */
    private function _prepareOrders($orders)
    {
        $data = [];
        foreach ($orders as $order) {
            $data[] = $this->_createDTO($order);
        }

        return $data;
    }

    /**
     * Функция подготавливает заказ к отображению.
     * Заказ преобразуется в DTO-объект (просто object с некими свойствами для вьюшки).
     *
     * @param mixed $order Объектов "Заказ"
     * @return object|null DTO-объект "Заказ"
     */
    private function _createDTO($order)
    {
        if (!$order) {
            return null;
        }
        $row['id'] = $this->_orderService->getId($order);
        $row['number'] = $this->_orderService->getNumber($order);
        $row['logistics_status'] = $this->_orderService->getLogisticsStatus($order);
        $row['logistics_status_priority'] = $this->_orderService->getLogisticsStatusPriority($order);
        $row['is_handled'] = $this->_orderService->isHandled($order);
        $row['transport_company_name'] = $this->_orderService->getTransportCompanyName($order);
        $row['weight_status'] = $this->_orderService->getWeightStatus($order);
        $row['has_weighing'] = $this->_orderService->hasWeighing($order);
        $row['client_name'] = $this->_orderService->getClientName($order);
        $row['client_phone'] = $this->_orderService->getClientPhone($order);
        $row['client_address'] = $this->_orderService->getClientAddress($order);
        $row['items_price'] = $this->_formatPrice($this->_orderService->getItemsPrice($order));
        $row['payment_status'] = $this->_orderService->getPaymentStatus($order);
        $row['delivery_type'] = $this->_orderService->getDeliveryType($order);
        $row['delivery_price'] = $this->_formatPrice($this->_orderService->getDeliveryPrice($order));
        $row['delivery_status'] = $this->_orderService->getDeliveryStatus($order);
        $row['cod_total_price'] = $this->_formatPrice($this->_orderService->getCodTotalPrice($order));
        $row['cod_items_price'] = $this->_formatPrice($this->_orderService->getCodItemsPrice($order));
        $row['cod_delivery_price'] = $this->_formatPrice($this->_orderService->getCodDeliveryPrice($order));
        $row['comment'] = $this->_orderService->getComment($order);
        $row['shipment_date'] = $this->_orderService->getShipmentDate($order);
        $row['track_code'] = $this->_orderService->getTrackCode($order);
        $row['weight'] = $this->_orderService->getWeight($order);

        return (object)$row;
    }

    /**
     * Функция сортирует заказы по приоритету
     *
     * @param object[] $orders Массив DTO-объектов "Заказ"
     * @param string $ascOrDesc Порядок сортировки ("ASC" / "DESC")
     * @return mixed
     */
    private function _sortByLogisticsStatus($orders, $ascOrDesc = 'ASC')
    {
        $ascOrDesc = strtoupper($ascOrDesc);
        if (!in_array($ascOrDesc, ['ASC', 'DESC'])) {
            $ascOrDesc = 'ASC';
        }
        uasort($orders, function ($a, $b) use ($ascOrDesc) {
            switch (true) {
                case $a->logistics_status_priority > $b->logistics_status_priority:
                    return $ascOrDesc == 'ASC' ? 1 : -1;
                case $a->logistics_status_priority < $b->logistics_status_priority:
                    return $ascOrDesc == 'ASC' ? -1 : 1;
                default:
                    return 0;
            }
        });

        return $orders;
    }

    /**
     * Функция форматирует цены.
     *
     * @param mixed $price
     * @return string
     */
    private function _formatPrice($price)
    {
        if (!$price) {
            $price = 0;
        }

        return $price . ' руб';
    }

    /**
     * Функция оборачивает в дата провайдер некий массив
     * @param $data
     * @param array $sort
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    private function _asProvider($data, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}