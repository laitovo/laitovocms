<?php

namespace backend\modules\logistics\modules\logisticsMonitor\models;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemService;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderItemsRepository;
use yii\data\ArrayDataProvider;

/**
 * DTO-провайдер модуля "Монитор логистики", который знает как переделать некие сторонние элементы заказа в нужный модулю формат.
 *
 * Class OrderItemsProvider
 * @package backend\modules\logistics\modules\logisticsMonitor\models
 */
class OrderItemsProvider
{
    /**
     * @var IOrderItemsRepository $_orderItemsRepository Репозиторий элементов заказа
     */
    private $_orderItemsRepository;

    /**
     * @var IOrderItemService $_orderItemService Сервис для доступа к свойствам элементов заказа
     */
    private $_orderItemService;

    /**
     * OrdersProvider constructor.
     * @param IOrderItemsRepository $orderItemsRepository Некий репозиторий, предоставляющий провайдеру элементы заказа в первозданном виде
     * @param IOrderItemService $orderItemService Некий сервис для доступа к свойствам элементов заказа
     */
    public function __construct(IOrderItemsRepository $orderItemsRepository, IOrderItemService $orderItemService)
    {
        $this->_orderItemsRepository = $orderItemsRepository;
        $this->_orderItemService = $orderItemService;
    }

    /**
     * Функция возвращает дата-провайдер с элементами заказа, подготовленными к отображению
     *
     * @return ArrayDataProvider
     */
    public function getItemsByOrderIdAsDataProvider($id)
    {
        $orderItems = $this->getItemsByOrderId($id);
        $ordersItemsProvider = $this->_asProvider($orderItems);

        return $ordersItemsProvider;
    }

    /**
     * Функция возвращает массив подготовленных для отображения элементов заказа
     *
     * @param $id int Идентификатор заказа
     * @return object[] Массив dto-объектов "Элемент заказа"
     */
    public function getItemsByOrderId($id)
    {
        $orderItems = $this->_orderItemsRepository->getItemsByOrderId($id);
        $data = $this->_prepareOrderItems($orderItems);

        return $data;
    }

    /**
     * Функция подготавливает элементы заказа к отображению
     *
     * @param array $orderItems Массив элементов заказа в первозданном виде
     * @return array Массив dto-объектов "Элемент заказа"
     */
    private function _prepareOrderItems($orderItems)
    {
        $data = [];
        foreach ($orderItems as $orderItem) {
            $data[] = $this->_createDTO($orderItem);
        }

        return $data;
    }

    /**
     * Функция подготавливает элемент заказа к отображению.
     * Элемент заказа преобразуется в DTO-объект (просто object с некими свойствами для вьюшки).
     *
     * @param mixed $orderItem Элемент заказа в первозданном виде
     * @return object|null dto-объект "Элемент заказа"
     */
    private function _createDTO($orderItem)
    {
        if (!$orderItem) {
            return null;
        }
        $row['article'] = $this->_orderItemService->getArticle($orderItem);
        $row['name'] = $this->_orderItemService->getName($orderItem);
        $row['size'] = $this->_orderItemService->getSize($orderItem);
        $row['price'] = $this->_formatPrice($this->_orderItemService->getPrice($orderItem));

        return (object)$row;
    }

    /**
     * Функция форматирует цены.
     *
     * @param mixed $price
     * @return string
     */
    private function _formatPrice($price)
    {
        if (!$price) {
            $price = 0;
        }

        return $price . ' руб';
    }

    /**
     * Функция оборачивает в дата провайдер некий массив
     * @param $data
     * @param array $sort
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    private function _asProvider($data, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}