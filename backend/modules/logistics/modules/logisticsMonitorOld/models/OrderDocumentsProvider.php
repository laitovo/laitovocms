<?php

namespace backend\modules\logistics\modules\logisticsMonitor\models;

use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentService;
use backend\modules\logistics\modules\logisticsMonitor\interfaces\IOrderDocumentsRepository;

/**
 * DTO-провайдер модуля "Монитор логистики", который знает как переделать некие сторонние документы заказа в нужный модулю формат.
 *
 * Class OrderDocumentsProvider
 * @package backend\modules\logistics\modules\logisticsMonitor\models
 */
class OrderDocumentsProvider
{
    /**
     * @var IOrderDocumentsRepository Репозиторий документов заказа
     */
    private $_orderDocumentsRepository;
    /**
     * @var IOrderDocumentService Сервис для доступа к свойствам документа заказа
     */
    private $_orderDocumentService;

    /**
     * OrderDocumentsProvider constructor.
     * @param IOrderDocumentsRepository $orderDocumentsRepository Некий репозиторий, предоставляющий провайдеру документы заказа в первозданном виде
     * @param IOrderDocumentService $orderDocumentService Некий сервис для доступа к свойствам документа заказа
     */
    public function __construct(IOrderDocumentsRepository $orderDocumentsRepository, IOrderDocumentService $orderDocumentService)
    {
        $this->_orderDocumentsRepository = $orderDocumentsRepository;
        $this->_orderDocumentService = $orderDocumentService;
    }

    /**
     * Функция возвращает документы заказа, подготовленные к отображению в файловом виджете
     *
     * @return object
     */
    public function getDocumentsByOrderIdAsFileWidgetData($orderId)
    {
        $orderDocuments = $this->getDocumentsByOrderId($orderId);
        $orderDocumentsWidgetConfig = $this->_asFileWidgetData($orderDocuments);

        return $orderDocumentsWidgetConfig;
    }

    /**
     * Функция возвращает массив подготовленных для отображения документов заказа
     *
     * @param $orderId int Идентификатор заказа
     * @return object[] Массив dto-объектов "Документ заказа"
     */
    public function getDocumentsByOrderId($orderId)
    {
        $orderDocuments = $this->_orderDocumentsRepository->getDocumentsByOrderId($orderId);
        $dtoOrderDocuments = $this->_prepareOrderDocuments($orderDocuments);

        return $dtoOrderDocuments;
    }

    /**
     * Функция подготавливает документы заказа к отображению
     *
     * @param array $orderDocuments Массив документов заказа в первозданном виде
     * @return array Массив dto-объектов "Документ заказа"
     */
    private function _prepareOrderDocuments($orderDocuments)
    {
        $dtoOrderDocuments = [];
        foreach ($orderDocuments as $orderDocument) {
            $dtoOrderDocuments[] = $this->_createDTO($orderDocument);
        }

        return $dtoOrderDocuments;
    }

    /**
     * Функция подготавливает документ заказа к отображению.
     * Документ заказа преобразуется в DTO-объект (просто object с некими свойствами для вьюшки).
     *
     * @param mixed $orderDocument Документ заказа в первозданном виде
     * @return object|null dto-объект "Документ заказа"
     */
    private function _createDTO($orderDocument)
    {
        if (!$orderDocument) {
            return null;
        }
        $row['id'] = $this->_orderDocumentService->getId($orderDocument);
        $row['url_for_view'] = $this->_orderDocumentService->getFileUrlForView($orderDocument);
        $row['url_for_download'] = $this->_orderDocumentService->getFileUrlForDownload($orderDocument);
        $row['size'] = $this->_orderDocumentService->getFileSize($orderDocument);
        $row['original_name'] = $this->_orderDocumentService->getFileOriginalName($orderDocument);

        return (object)$row;
    }

    /**
     * Функция подготавливает dto-объекты к отображению в файловом виджете
     *
     * @param array $dtoOrderDocuments Массив dto-объектов "Документ заказа"
     * @return object
     */
    private function _asFileWidgetData($dtoOrderDocuments)
    {
        $widgetData['urls'] = $widgetData['configs'] = [];
        foreach ($dtoOrderDocuments as $dtoOrderDocument) {
            $widgetData['urls'][] = $dtoOrderDocument->url_for_view;
            $widgetData['configs'][] = [
                'type' => 'pdf',
                'extra' => ['id' => $dtoOrderDocument->id],
                'downloadUrl' => $dtoOrderDocument->url_for_download,
                'size' => $dtoOrderDocument->size,
                'caption' => $dtoOrderDocument->original_name,
            ];
        }

        return (object)$widgetData;
    }
}