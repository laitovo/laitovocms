<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

/**
 * Интерфейс репозитория элементов заказа для модуля "Монитор логистики".
 *
 * Interface IOrderItemsRepository
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrderItemsRepository
{
    /**
     * Функция возвращает элементы заказа по переданному идентификатору заказа
     *
     * @param int $orderId Идентификатор заказа
     * @return array Массив объектов "Элемент заказа"
     */
    public function getItemsByOrderId($orderId);
}