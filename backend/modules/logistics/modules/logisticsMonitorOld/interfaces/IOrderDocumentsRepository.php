<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

/**
 * Интерфейс репозитория документов заказа для модуля "Монитор логистики".
 *
 * Interface IOrderDocumentsRepository
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrderDocumentsRepository
{
    /**
     * Функция возвращает документы заказа по переданному идентификатору заказа
     *
     * @param int $orderId Идентификатор заказа
     * @return array Массив объектов "Документ заказа"
     */
    public function getDocumentsByOrderId($orderId);
}