<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

use yii\data\ArrayDataProvider;

/**
 * Интерфейс DTO-провайдера заказов для модуля "Монитор логистики".
 *
 * Interface IOrdersProvider
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrdersProvider
{
    /**
     * Функция возвращает дата-провайдер с заказами, подготовленными к отображению
     *
     * @return ArrayDataProvider
     */
    public function getOrdersAsDataProvider();

    /**
     * Функция возвращает по переданному идентификатору заказ, подготовленный к отображению
     *
     * @param $id int Идентификатор заказа
     * @return mixed Объект "Заказ"
     */
    public function getOrderById($id);
}