<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

/**
 * Интерфейс сервиса, предоставляющего модулю "Монитор логистики" доступ к свойствам элемента заказа.
 *
 * Interface IOrderItemService
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrderItemService
{
    /**
     * Артикул
     *
     * @param mixed $orderItem Объект "Элемент заказа"
     * @return string|null
     */
    public function getArticle($orderItem);

    /**
     * Наименование
     *
     * @param mixed $orderItem Объект "Элемент заказа"
     * @return string|null
     */
    public function getName($orderItem);

    /**
     * Габариты
     *
     * @param mixed $orderItem Объект "Элемент заказа"
     * @return string|null
     */
    public function getSize($orderItem);

    /**
     * Цена
     *
     * @param mixed $orderItem Объект "Элемент заказа"
     * @return double|null
     */
    public function getPrice($orderItem);
}