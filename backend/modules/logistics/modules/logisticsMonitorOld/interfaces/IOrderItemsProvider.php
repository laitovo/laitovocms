<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

use yii\data\ArrayDataProvider;

/**
 * Интерфейс DTO-провайдера элементов заказа для модуля "Монитор логистики".
 *
 * Interface IOrderItemsProvider
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrderItemsProvider
{
    /**
     * Функция возвращает дата-провайдер с элементами заказа, подготовленными к отображению
     *
     * @param $id int Идентификатор заказа
     * @return ArrayDataProvider
     */
    public function getItemsByOrderIdAsDataProvider($id);
}