<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

/**
 * Интерфейс сервиса, предоставляющего модулю "Монитор логистики" доступ к свойствам заказа.
 *
 * Interface IOrderService
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrderService
{
    /**
     * Идентификатор заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return int
     */
    public function getId($order);

    /**
     * Внутренний номер источника заказа (Идентификатор заявки внутри другого сайта)
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getNumber($order);

    /**
     * Логистический статус заказа (Обработан, Не обработан, Ожидается взвешивание, ...).
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getLogisticsStatus($order);

    /**
     * Приоритет логистического статуса заказа.
     * Необходим для сортировки списка заказов.
     *
     * @param mixed $order Объект "Заказ"
     * @return int
     */
    public function getLogisticsStatusPriority($order);

    /**
     * Проверка, закончил ли логист работу с заказом
     *
     * @param mixed $order Объект "Заказ"
     * @return bool
     */
    public function isHandled($order);

    /**
     * Наименование транспортной компании
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getTransportCompanyName($order);

    /**
     * Весовой статус (Со взвешиванием / Без взвешивания)
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getWeightStatus($order);

    /**
     * Назначено ли взвешивание данному заказу
     *
     * @param mixed $order Объект "Заказ"
     * @return bool
     */
    public function hasWeighing($order);

    /**
     * ФИО заказчика
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getClientName($order);

    /**
     * Телефон заказчика
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getClientPhone($order);

    /**
     * Адрес заказчика
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getClientAddress($order);

    /**
     * Сумма заказа.
     * Равняется сумме цен элементов данного заказа.
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getItemsPrice($order);

    /**
     * Статус оплаты товара (Не оплачено, Оплачено, Наложка)
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getPaymentStatus($order);

    /**
     * Тип доставки (Платная, Бесплатная)
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getDeliveryType($order);

    /**
     * Стоимость доставки
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getDeliveryPrice($order);

    /**
     * Статус оплаты доставки (Не оплачено, Оплачено, Наложка)
     *
     * @param mixed $order Объект "Заказ"
     * @return string
     */
    public function getDeliveryStatus($order);

    /**
     * Общая сумма, полученная за весь заказ наложенным платежом
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getCodTotalPrice($order);

    /**
     * Общая сумма, полученная за товар наложенным платежом
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getCodItemsPrice($order);

    /**
     * Общая сумма, полученная за доставку наложенным платежом
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getCodDeliveryPrice($order);

    /**
     * Комментарий к заказу
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getComment($order);

    /**
     * Дата отгрузки заказа (timestamp)
     *
     * @param mixed $order Объект "Заказ"
     * @return int|null
     */
    public function getShipmentDate($order);

    /**
     * Трэк-код заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return string|null
     */
    public function getTrackCode($order);

    /**
     * Вес заказа
     *
     * @param mixed $order Объект "Заказ"
     * @return double|null
     */
    public function getWeight($order);
}