<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

/**
 * Интерфейс DTO-провайдера документов заказа для модуля "Монитор логистики".
 *
 * Interface IOrderDocumentsProvider
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrderDocumentsProvider
{
    /**
     * Функция возвращает документы заказа, подготовленные к отображению в файловом виджете
     *
     * @param int $id Идентификатор заказа
     * @return object
     */
    public function getDocumentsByOrderIdAsFileWidgetData($id);
}