<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

/**
 * Интерфейс сервиса, предоставляющего модулю "Монитор логистики" функционал для обработки заказов логистом.
 *
 * Interface IOrderLogistService
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrderLogistService
{
    /**
     * Пометка что логист завершил работу над заказом
     *
     * @param int $order_id Идентификатор заказа
     * @return bool Результат операции
     */
    public function handle($order_id);

    /**
     * Пометка (или отмена пометки) для склада (кладовщика) что заказ должен быть взвешен
     *
     * @param int $order_id Идентификатор заказа
     * @return bool Результат операции
     */
    public function updateWeightStatus($order_id);

    /**
     * Сохранение даты отгрузки заказа
     *
     * @param int $order_id Идентификатор заказа
     * @param string|null $shipment_date Дата отгрузки заказа
     * @return bool Результат операции
     */
    public function setShipmentDate($order_id, $shipment_date);

    /**
     * Сохранение трэк-кода заказа
     *
     * @param int $order_id Идентификатор заказа
     * @param string|null $track_code Трэк-код заказа
     * @return bool Результат операции
     */
    public function setTrackCode($order_id, $track_code);

    /**
     * Сохранение веса заказа
     *
     * @param int $order_id Идентификатор заказа
     * @param string|null $weight Вес заказа
     * @return bool Результат операции
     */
    public function setWeight($order_id, $weight);

    public function uploadDocument($order_id, $fieldName);
    public function deleteDocument($documentId);
}