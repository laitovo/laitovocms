<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

/**
 * Интерфейс сервиса, предоставляющего модулю "Монитор логистики" доступ к свойствам документа заказа.
 *
 * Interface IOrderDocumentService
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrderDocumentService
{
    /**
     * Идентификатор документа
     *
     * @param mixed $orderDocument объект "Документ заказа"
     * @return int
     */
    public function getId($orderDocument);

    /**
     * Ссылка для просмотра файла
     *
     * @param mixed $orderDocument объект "Документ заказа"
     * @return string
     */
    public function getFileUrlForView($orderDocument);

    /**
     * Ссылка для скачивания файла
     *
     * @param mixed $orderDocument объект "Документ заказа"
     * @return string
     */
    public function getFileUrlForDownload($orderDocument);

    /**
     * Размер файла в байтах
     *
     * @param mixed $orderDocument объект "Документ заказа"
     * @return int
     */
    public function getFileSize($orderDocument);

    /**
     * Оригинальное имя, с которым загружался файл
     *
     * @param mixed $orderDocument объект "Документ заказа"
     * @return string
     */
    public function getFileOriginalName($orderDocument);
}