<?php

namespace backend\modules\logistics\modules\logisticsMonitor\interfaces;

/**
 * Интерфейс репозитория заказов для модуля "Монитор логистики".
 *
 * Interface IOrdersRepository
 * @package backend\modules\logistics\modules\logisticsMonitor\interfaces
 */
interface IOrdersRepository
{
    /**
     * Функция возвращает массив заказов
     *
     * @return array Массив объектов "Заказ"
     */
    public function getOrders();

    /**
     * Функция возвращает заказ по переданному идентификатору
     *
     * @param $id int Идентификатор заказа
     * @return mixed Объект "Заказ"
     */
    public function getOrderById($id);
}