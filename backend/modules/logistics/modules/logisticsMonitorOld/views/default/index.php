<?php

/**
 * @var  $ordersProvider \yii\data\ArrayDataProvider Провайдер данных для таблицы зявок
 */

use yii\grid\GridView;
use \yii\helpers\Html;

$this->title = Yii::t('app', 'Монитор логистики');

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
//$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');

?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover orders-table'],
    'dataProvider' => $ordersProvider,
    'columns' => [
        [
            'attribute' => 'number',
            'label' => 'Заказ №',
        ],
        [
            'label' => 'Статус',
            'value' => function($data) {
                return $data->logistics_status;
            },
            'contentOptions' => function($data) {
                return ['class' => 'is-handled', 'data' => ['is-handled' => $data->is_handled]];
            },
        ],
        [
            'label' => 'Панель действий',
            'format' => 'raw',
            'value' => function($data) {
                return Html::a('<i class = "glyphicon glyphicon-eye-open"></i>',['view','id' => $data->id]);
            }
        ],
    ],
]) ?>

<?php Yii::$app->view->registerJs('
    $(document).ready(function() {
        colorizeRows();
    });
    
    //Данная функция отвечает за раскрашивание строк в таблице заказов
    function colorizeRows() {
        $(".orders-table tr").each(function(){
             if ($(this).find("td.is-handled").data("is-handled")) {
                $(this).addClass("success");
             }
        })
    }
', \yii\web\View::POS_END);
?>
