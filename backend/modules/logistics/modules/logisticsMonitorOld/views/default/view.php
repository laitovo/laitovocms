<?php

/**
 * @var  $order object Заказ (заявка)
 */
/**
 * @var  $orderItemsProvider \yii\data\ArrayDataProvider Данные для таблицы "ТОВАР"
 */
/**
 * @var  $orderDocumentsWidgetData object Данные для файлового виджета "Документы"
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\jui\DatePicker;
use backend\modules\logistics\modules\logisticsMonitor\assets\ViewOrderAsset;
use kartik\file\FileInput;
use yii\widgets\ActiveForm;

ViewOrderAsset::register($this);

$this->title = Yii::t('app','Просмотр заказа');

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Монитор логистики'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');

?>

<div class="form-group">
    <?php
        $hideEditingClass = $order->is_handled ? ' hide' : '';
        echo Html::button(Yii::t('app','Обработать'), ['class' => 'btn btn-success handle-order-button hide-after-handling' . $hideEditingClass, 'data' => ['order-id' => $order->id]]);
    ?>
    <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
</div>

<?= DetailView::widget([
    'model' => $order,
    'attributes' => [
        [
            'attribute' => 'number',
            'label' => '№ Заказа:',
            'captionOptions' => ['style' =>  'width: 150px;'],
        ],
    ],
]) ?>

<div class="transport-company-block">
    <div class="transport-company-info">
        <div class="row">
            <div class="col-md-7">
                <div class="text-wrapper">Транспортная компания: <span class="text-nowrap"><?= $order->transport_company_name ?></span></div>
            </div>
            <div class="col-md-5">
                <div class="text-wrapper">Тип: <span class="text-nowrap weight-status"><?= $order->weight_status ?></span></div>
            </div>
        </div>
    </div>

    <?php
        $buttonApplyingText = Yii::t('app','Взвесить');
        $buttonApplyingConfirm = Yii::t('app','Вы уверены, что данный заказ должен быть взвешен?');
        $buttonCancelText = Yii::t('app','Отменить');
        $buttonCancelConfirm = Yii::t('app','Вы действительно хотите отменить взвешивание для данного заказа?');
        $buttonText = $order->has_weighing ? $buttonCancelText : $buttonApplyingText;
        $buttonConfirm = $order->has_weighing ? $buttonCancelConfirm : $buttonApplyingConfirm;
        $hideEditingClass = $order->is_handled ? ' hide' : '';
        echo Html::button('<i class="fa fa-balance-scale"></i>&nbsp;<span class="button-text"> ' . $buttonText . '</span>', [
            'class' => 'btn btn-primary weigh-button hide-after-handling' . $hideEditingClass,
            'data' => [
                'order-id' => $order->id,
                'applying-text' => $buttonApplyingText,
                'cancel-text' => $buttonCancelText,
                'applying-confirm' => $buttonApplyingConfirm,
                'cancel-confirm' => $buttonCancelConfirm,
                'confirm-message' => $buttonConfirm,
            ],
        ]);
    ?>

</div>

<hr>

<?= DetailView::widget([
    'model' => $order,
    'attributes' => [
        [
            'attribute' => 'client_name',
            'label' => 'ФИО:',
            'captionOptions' => ['style' =>  'width: 150px;'],
        ],
        [
            'attribute' => 'client_phone',
            'label' => 'ТЕЛЕФОН:',
        ],
        [
            'attribute' => 'client_address',
            'label' => 'АДРЕС:',
        ],
    ],
]) ?>

<hr>
ТОВАР:
<hr>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'summary' => false,
    'dataProvider' => $orderItemsProvider,
    'columns' => [
        [
            'attribute' => 'article',
            'label' => 'Артикул',
        ],
        [
            'attribute' => 'name',
            'label' => 'Наименование',
        ],
        [
            'attribute' => 'size',
            'label' => 'Габариты',
        ],
        [
            'attribute' => 'price',
            'label' => 'Цена на товар из заказа',
        ],
    ],
]) ?>

<hr>

<div class="row">
    <div class="col-sm-6">
        <?= DetailView::widget([
            'model' => $order,
            'attributes' => [
                [
                    'attribute' => 'items_price',
                    'label' => 'Сумма заказа:',
                    'captionOptions' => ['style' =>  'width: 150px;'],
                ],
                [
                    'label' => 'СТАТУС:',
                    'value' => function($data) {
                        return '(' . $data->payment_status . ')';
                    },
                ],
            ],
        ]) ?>
    </div>
    <div class="col-sm-6">
        <?= DetailView::widget([
            'model' => $order,
            'attributes' => [
                [
                    'attribute' => 'delivery_type',
                    'label' => 'Тип доставки:',
                    'captionOptions' => ['style' =>  'width: 150px;'],
                ],
                [
                    'attribute' => 'delivery_price',
                    'label' => 'Сумма доставки:',
                ],
                [
                    'label' => 'СТАТУС (доставка):',
                    'value' => function($data) {
                        return '(' . $data->delivery_status . ')';
                    },
                ],
            ],
        ]) ?>
    </div>
</div>

<hr>

<!--<div class="row">-->
<!--    <div class="col-md-4">-->
<!--        --><?//= DetailView::widget([
//            'model' => $order,
//            'attributes' => [
//                [
//                    'attribute' => 'paid_total_price',
//                    'label' => 'ИТОГО ОПЛАЧЕНО:',
//                    'captionOptions' => ['class' => 'text-success', 'style' =>  'width: 200px;'],
//                    'contentOptions' => ['class' => 'text-success'],
//                ],
//                [
//                    'attribute' => 'paid_items_price',
//                    'label' => 'ТОВАР:',
//                    'captionOptions' => ['class' => 'text-success', 'style' => 'padding-left:20px;'],
//                    'contentOptions' => ['class' => 'text-success'],
//                ],
//                [
//                    'attribute' => 'paid_delivery_price',
//                    'label' => 'Доставка:',
//                    'captionOptions' => ['class' => 'text-success', 'style' => 'padding-left:20px;'],
//                    'contentOptions' => ['class' => 'text-success'],
//                ],
//            ],
//        ]) ?>
<!--    </div>-->
<!--    <div class="col-md-4">-->
<!--        --><?//= DetailView::widget([
//            'model' => $order,
//            'attributes' => [
//                [
//                    'attribute' => 'unpaid_total_price',
//                    'label' => 'ИТОГО НЕ ОПЛАЧЕНО:',
//                    'captionOptions' => ['class' => 'text-danger', 'style' =>  'width: 200px;'],
//                    'contentOptions' => ['class' => 'text-danger'],
//                ],
//                [
//                    'attribute' => 'unpaid_items_price',
//                    'label' => 'ТОВАР:',
//                    'captionOptions' => ['class' => 'text-danger', 'style' => 'padding-left:20px;'],
//                    'contentOptions' => ['class' => 'text-danger'],
//                ],
//                [
//                    'attribute' => 'unpaid_delivery_price',
//                    'label' => 'Доставка:',
//                    'captionOptions' => ['class' => 'text-danger', 'style' => 'padding-left:20px;'],
//                    'contentOptions' => ['class' => 'text-danger'],
//                ],
//            ],
//        ]) ?>
<!--    </div>-->
<!--    <div class="col-md-4">-->
        <?= DetailView::widget([
            'model' => $order,
            'attributes' => [
                [
                    'attribute' => 'cod_total_price',
                    'label' => 'ИТОГО НАЛОЖКА:',
                    'captionOptions' => ['class' => 'text-violet', 'style' =>  'width: 200px;'],
                    'contentOptions' => ['class' => 'text-violet'],
                ],
                [
                    'attribute' => 'cod_items_price',
                    'label' => 'ТОВАР:',
                    'captionOptions' => ['class' => 'text-violet', 'style' => 'padding-left:20px;'],
                    'contentOptions' => ['class' => 'text-violet'],
                ],
                [
                    'attribute' => 'cod_delivery_price',
                    'label' => 'Доставка:',
                    'captionOptions' => ['class' => 'text-violet', 'style' => 'padding-left:20px;'],
                    'contentOptions' => ['class' => 'text-violet'],
                ],
            ],
        ]) ?>
<!--    </div>-->
<!--</div>-->
<!---->
<!--<hr>-->
<!---->
<?//= DetailView::widget([
//    'model' => $order,
//    'attributes' => [
//        [
//            'attribute' => 'received_total_price',
//            'label' => 'ИТОГО ПОЛУЧЕНО ОТ КЛИЕНТА:',
//            'captionOptions' => ['style' =>  'width: 250px;'],
//        ],
//    ],
//]) ?>

<hr>

<?= DetailView::widget([
    'model' => $order,
    'attributes' => [
        [
            'attribute' => 'comment',
            'label' => 'Комментарий из заказа:',
            'captionOptions' => ['style' =>  'width: 200px;'],
        ],
    ],
]) ?>

<?php
    $hideEditingClass = $order->is_handled ? ' hide' : '';
    $hideWeightEditBlockClass = $order->has_weighing ? '' : ' hide';
    echo DetailView::widget([
        'model' => $order,
        'attributes' => [
            [
                'label' => 'Дата отгрузки:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block closed">';
                    $html .= DatePicker::widget([
                        'value' => $data->shipment_date ? date('d.M.Y', $data->shipment_date) : null,
                        'options' => ['class' => 'form-control property-field shipment-date', 'data' => ['order-id' => $data->id], 'disabled' => true],
                        'dateFormat' => 'dd.MM.yyyy',
                    ]);
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-after-handling' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden shipment-date-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);

    echo DetailView::widget([
        'model' => $order,
        'attributes' => [
            [
                'label' => 'Трек-код:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block closed">';
                    $html .= '<input class="form-control property-field track-code" type="text" value="' . $data->track_code . '" data-order-id="' . $data->id . '" disabled>';
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-after-handling' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden track-code-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);

    echo DetailView::widget([
        'model' => $order,
        'options' => ['class' => 'table table-striped table-bordered detail-view weight-edit-block'. $hideWeightEditBlockClass],
        'attributes' => [
            [
                'label' => 'Вес:',
                'format'=> 'raw',
                'value' => function($data) use ($hideEditingClass) {
                    $html = '<div class="edit-block closed">';
                    $html .= '<input class="form-control property-field weight" type="text" value="' . $data->weight . '" data-order-id="' . $data->id . '" disabled>';
                    $html .= '<div class="action-buttons">';
                    $html .= '<i class = "fa fa-pencil-alt edit-button hide-after-handling' . $hideEditingClass . '"></i>';
                    $html .= '<i class = "fa fa-check save-button hidden weight-save-button"></i>';
                    $html .= '<i class = "fa fa-close cancel-button hidden"></i>';
                    $html .= '</div>';
                    $html .= '</div>';

                    return $html;
                },
                'captionOptions' => ['style' =>  'width: 150px;'],
            ],
        ],
    ]);
?>

<?php $form = ActiveForm::begin(); ?>

<h2>Документы:</h2>

<?= FileInput::widget([
    'name' => 'file',
    'options' => [
        'id' => 'file-input',
        'multiple' => true,
        'disabled' => $order->is_handled,
    ],
    'pluginOptions' => [
        'initialPreview' => $orderDocumentsWidgetData->urls,
        'initialPreviewConfig' => $orderDocumentsWidgetData->configs,
        'initialPreviewAsData' => true,
        'overwriteInitial' => false,
        'uploadUrl' => '/logistics/logistics-monitor/order-logist/upload-document',
        'uploadExtraData' => [
            'orderId' => $order->id,
        ],
        'deleteUrl' => '/logistics/logistics-monitor/order-logist/delete-document',
        'allowedFileExtensions' => ['pdf'],
        'showCaption' => false,
        'showRemove' => false,
        'showClose' => false,
        'fileActionSettings' => [
            'showDrag' => false,
            'uploadClass' => 'btn btn-xs bg-success',
            'removeClass' => 'btn btn-xs bg-danger',
            'removeErrorClass' => 'btn btn-xs bg-danger',
            'zoomClass' => 'btn btn-xs bg-info',
            'downloadTitle' => 'Скачать файл',
            'downloadClass' => 'btn btn-xs bg-primary',
        ],
        'previewZoomButtonIcons' => [
            'borderless' => '<i class="glyphicon glyphicon-fullscreen"></i>',
        ],
        'previewZoomButtonClasses' => [
            'toggleheader' => 'hide',
            'fullscreen' => 'hide',
            'borderless' => 'btn btn-xs',
            'close' => 'btn btn-xs',
        ],
        'previewZoomButtonTitles' => [
            'borderless' => 'Переключить полноэкранный режим',
        ],
    ],
    'pluginEvents' => [
        'fileselect' => 'function() { 
            showUploadButton();
         }',
        'fileremoved' => 'function() { 
            showUploadButton();
         }',
        'fileuploaded' => 'function(event, data, thumbnailId) {
            rememberNewFileId(thumbnailId, data.response.id);
            setTimeout(showUploadButton, 100);
         }',
        'filesuccessremove' => 'function(event, thumbnailId) {
    		deleteNewFile(thumbnailId);
         }',
    ],
]);?>

<?php Yii::$app->view->registerJs('
    //Функция скрывает кнопку "Загрузить", если нечего загружать
    function showUploadButton() {
        var fileInput = $("#file-input");
        var uploadButton = $(".fileinput-upload.fileinput-upload-button");
        var filesToUploadCount = fileInput.fileinput("getFileStack").length;
        if (filesToUploadCount > 0) {
            uploadButton.removeClass("hide");
        } else {
            uploadButton.addClass("hide");
        } 
    };
    
    //Запоминаем id свежезагруженного файла (чтобы потом его можно было удалить)
    function rememberNewFileId(thumbnailId, newFileId) {
        $("#" + thumbnailId).data("newFileId", newFileId);
    }
    
    //Получаем id свежезагруженного файла
    function getNewFileId(thumbnailId) {
        return $("#" + thumbnailId).data("newFileId");
    }
    
    //Удаление свежезагруженного файла
    function deleteNewFile(thumbnailId) {
        var fileId = getNewFileId(thumbnailId);
        $.ajax({
            type: "post",
            url: "/logistics/logistics-monitor/order-logist/delete-document",
            data: {"id": fileId},
            dataType: "json",
            error: function() {
                notie.alert(3, "Не удалось удалить документ", 3); 
            }
        });
    }
', \yii\web\View::POS_END);
?>

<?php ActiveForm::end(); ?>
