<?php

namespace backend\modules\logistics\modules\storageAnalytics\controllers;

use backend\modules\logistics\modules\storageAnalytics\models\Index;
use yii\web\Controller;
use yii\base\Module;

class DefaultController extends Controller
{
    private $_index;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_index = new Index();

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'provider'   => $this->_index->getProvider(),
            'attributes' => $this->_index->getAttributes(),
//            'attributesWithLabels' => $this->_index->getAttributesWithLabels(),
        ]);
    }
}