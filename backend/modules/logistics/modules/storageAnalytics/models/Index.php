<?php

namespace backend\modules\logistics\modules\storageAnalytics\models;

use Yii;
use yii\data\ArrayDataProvider;

class Index
{
    private $_storageAnalyticsManager;

    public function __construct()
    {
        $this->_storageAnalyticsManager = Yii::$container->get('core\entities\storageAnalytics\StorageAnalyticsManager');
    }

    public function getProvider()
    {
        return new ArrayDataProvider([
            'key'       => $this->_storageAnalyticsManager->getPrimaryKey(),
            'allModels' => $this->_prepareModels($this->_storageAnalyticsManager->findAll()),
            'sort'      => [
                'attributes' => $this->getAttributes(),
                'defaultOrder' => [
                    'realizationPlan' => SORT_DESC,
                ]
            ],
        ]);
    }

    public function getAttributes()
    {
        return $this->_storageAnalyticsManager->getAttributes();
    }

    public function getAttributesWithLabels()
    {
        return $this->_storageAnalyticsManager->getAttributesWithLabels();
    }

    private function _prepareModels($models)
    {
        $preparedModels = [];
        foreach ($models as $model) {
            $preparedModels[] = $this->_prepareModel($model);
        }

        return $preparedModels;
    }

    private function _prepareModel($model)
    {
        $preparedModel = $model->toArray();
        $preparedModel['carAnalogsNames'] = \backend\modules\logistics\models\AutomaticOrderMonitor::getCarAnalogsNames($model->article);
        $preparedModel['carAnalogsCount'] = \backend\modules\logistics\models\AutomaticOrderMonitor::getCarAnalogsCount($model->article);
        $preparedModel['groupAlias']      = 'test';

        return $preparedModel;
    }
}