<?php

use backend\widgets\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Аналитика склада');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');

?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'summary' => false,
    'dataProvider' => $provider,
    'show' => $attributes,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'car',
            'label' => '<span style="color:brown;font-weight:bold" title ="Аналоги продукта" data-toggle="tooltip" data-placement="top">(Аналоги)</span>  Автомобиль',
            'encodeLabel' => false,
            'format' => 'raw',
            'filter' => true,
            'value' => function($data) {
                return '<span style="color:brown;font-weight:bold" title ="' . $data['carAnalogsNames'] . '" data-toggle="tooltip" data-placement="top">( ' . $data['carAnalogsCount'] . ' )</span> - ' . $data['car'];
            }
        ],
        [
            'attribute' => 'type',
            'label' => 'Наим. Груп',
            'encodeLabel' => false,
            'filter' => true,
            'format' => 'html',
            'value' => function($data) {
                return $data['type'] ? '<span style="color:green;font-weight:bold">( ' . $data['groupAlias'] . ' )</span> - ' . $data['type'] : '---';
            }
        ],
        [
            'attribute' => 'article',
            'label' => 'Артикул',
            'encodeLabel' => false,
            'filter' => true,
        ],
//        [
//            'attribute' => 'realizationBefore',
//            'label' => '<span title="Реализация предпоследнего месяца, арт. (' . $model->getMonthTitle('before') .')" data-toggle="tooltip" data-placement="top">РПП</span>',
//            'encodeLabel' => false,
//            'format' => 'html',
//            'filter' => true,
//            'value' => function($data) use ($model) {
//                return $model->getRealizationByArticle($data['article'],'before',true) ;
//            }
//        ],
//        [
//            'attribute' => 'realizationLast',
//            'label' => '<span title="Реализация послднего месяца, арт. (' . $model->getMonthTitle('last') .')" data-toggle="tooltip" data-placement="top">РП</span>',
//            'encodeLabel' => false,
//            'filter' => true,
//            'format' => 'html',
//            'value' => function($data) use ($model) {
//                return $model->getRealizationByArticle($data['article'],'last',true) ;
//            }
//        ],
//        [
//            'attribute' => 'increase',
//            'label' => '<span title="Последний непиковый прирост, отн.ед." data-toggle="tooltip" data-placement="top">ПНП</span>',
//            'encodeLabel' => false,
//            'filter' => true,
//        ],
//        [
//            'attribute' => 'realizationPlan',
//            'label' => '<span title="План реализации +30дней" data-toggle="tooltip" data-placement="top">ПР</span>',
//            'encodeLabel' => false,
//            'filter' => true,
//        ],
//        [
//            'attribute' => 'rate',
//            'label' => '<span title="Коэффициент скорости производства" data-toggle="tooltip" data-placement="top">КСП</span>',
//            'encodeLabel' => false,
//            'filter' => true,
//        ],
//        [
//            'attribute' => 'fixrate',
//            'label' => '<span title="Коэффициент корректирующий" data-toggle="tooltip" data-placement="top">КК</span>',
//            'encodeLabel' => false,
//            'filter' => true,
//        ],
//        [
//            'attribute' => 'frequence',
//            'label' => '<span title="Частота реализации" data-toggle="tooltip" data-placement="top">ЧР</span>',
//            'encodeLabel' => false,
//            'filter' => true,
//        ],
//        [
//            'attribute' => 'balanceFact',
//            'label' => '<span title="Остатки фактические" data-toggle="tooltip" data-placement="top">ОФ</span>',
//            'encodeLabel' => false,
//            'filter' => true,
//        ],
//        [
//            'attribute' => 'balancePlan',
//            'label' => '<span title="Остатки плановые" data-toggle="tooltip" data-placement="top">ОП</span>',
//            'encodeLabel' => false,
//            'filter' => true,
//        ],
    ]
]) ?>
