<?php

namespace backend\modules\logistics\modules\naryadTransferMonitor\controllers;

use backend\modules\logistics\modules\naryadTransferMonitor\models\StorageStatesDtoProvider;
use backend\modules\logistics\modules\naryadTransferMonitor\models\NaryadsOnDispatcherDtoProvider;
use yii\web\Controller;
use yii\base\Module;
use Yii;
use yii\web\Response;
use backend\modules\logistics\modules\naryadTransferMonitor\models\Terminal;

class DefaultController extends Controller
{
    private $_naryadsOnDispatcherDtoProvider;
    private $_storageStatesDtoProvider;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_naryadsOnDispatcherDtoProvider = new NaryadsOnDispatcherDtoProvider();
        $this->_storageStatesDtoProvider = new StorageStatesDtoProvider();

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "0");

        $naryadsOnDispatcher = $this->_naryadsOnDispatcherDtoProvider->getNaryadsOnDispatcherAsDataProvider();
        $storageStates = $this->_storageStatesDtoProvider->getStorageStatesAsDataProvider();
        $terminal = new Terminal();
        return $this->render('index', [
            'naryadsOnDispatcher' => $naryadsOnDispatcher,
            'storageStates' => $storageStates,
            'idsOfNewNaryadsOnDispatcher' => $terminal->getIdsOfNewNaryadsOnDispatcher(),
            'idsOfNewStorageStates' => $terminal->getIdsOfNewStorageStates(),
            'currentTerminalUser' => $terminal->getCurrentUser(),
        ]);
    }

    public function actionPeep($barcode)
    {
        ini_set('memory_limit', '2048M');
        ini_set("max_execution_time", "0");

        Yii::$app->response->format = Response::FORMAT_JSON;
        $terminal = new Terminal();
        $terminal->peep($barcode);

        return [
            'status' => $terminal->getStatus() ? 'success' : 'error',
            'message' => $terminal->getMessageAsHtml(),
            'newStorageState' => $terminal->getNewStorageState(),
        ];
    }

    public function actionPrintLabel($id,$literal)
    {
        $model = \backend\modules\logistics\models\Naryad::findOne($id);

        return $this->renderPartial('print_label',[
            'model' => $model,
            'literal' => $literal,
        ]);
    }
}