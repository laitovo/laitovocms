<?php

/**
 * @var  $articlesProvider \yii\data\ArrayDataProvider Провайдер данных для артикулов
 */

use backend\widgets\GridView;
use \yii\helpers\Html;
use yii\helpers\Url;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Монитор пополнения склада');

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
//$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    $("#erp-terminal-form").on("ajaxComplete", function (event, messages) {
        document.location.reload();
    });

    setInterval(function(){ $(".reloadterminal").click();}, 60*1000);

    var barcode=$(\'#erpterminal-barcode\');

    function searchterminal(e)
    {
        peep(barcode.val());
        e.preventDefault();
    }
    
   
    function peep(barcode) {
        $.ajax({
            url: "/logistics/naryad-transfer-monitor/default/peep",
            dataType: "json",
            data: {
                "barcode": barcode
            },
            error: function() {
                notie.alert(3, "Возникла неизвестная ошибка, обратитесь к администратору", 15);
            },
            success: function (data) {
                $(".reloadterminal").click();
                if (data.status != "success") {
                    notie.alert(3, data.message, 15);
                    return;
                } 
                notie.alert(1, data.message,60);
                if (data.newStorageState) {
//                    printLabel(data.newStorageState.upnId, data.newStorageState.literal);
                }
            },
        });
    }
    
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erpterminal-barcode" && e.which == 13){
                searchterminal(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchterminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });
    
    
    function printLabel(upnId, literal) {
        $.get("/logistics/naryad-transfer-monitor/default/print-label?id=" + upnId + "&literal=" + literal, function(content) {
            myWindow = window.open();
            myWindow.document.write(content);
            myWindow.print();
            myWindow.close();
        });
    }
    ', \yii\web\View::POS_END);
?>

<style>
    h2 {
        font-size: 20px;
    }
    tr {
        height:49px;
    }
    .current-user-info {
        display: block;
        margin-bottom: 10px;
        font-size: 2em;
        font-weight: bold;
    }
</style>


<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <input type="text" id="erpterminal-barcode" placeholder="Поиск по штрихкоду..." class="form-control">
    </div>
</div>
<br>
<?php Pjax::begin(['timeout'=>5000]); ?>

<?= Html::a("Обновить", ['index'], ['class' => 'hidden reloadterminal']) ?>
<div class="row">
    <div class="col-md-12 text-center">
        <? if ($currentTerminalUser): ?>
            Текущий пользователь: <span class="current-user-info text-info"><?= $currentTerminalUser->name ?></span>
        <? endif ?>
    </div>
</div>
<?//= \core\logic\CheckStoragePosition::getQuantitativeBarcode();?>
<!--<br>-->
<?//= 'OT1220000'?>
<!--<br>-->
<?//= '5976db6802c46'?>
<div class="row">
    <div class="col-md-4 col-md-offset-1">
        <h2>Диспетчер-склад</h2>
        <?= GridView::widget([
            'summary'=> '',
            'show' => ['naryadNumber', 'article', 'readyDate'],
            'rowOptions' => function ($data) use ($idsOfNewNaryadsOnDispatcher) {
                 return [
                     'class' => in_array($data->id, $idsOfNewNaryadsOnDispatcher) ? 'success' : '',
                     'data' => [
                        'id' => $data->id
                     ],
                 ];
            },
            'dataProvider' => $naryadsOnDispatcher,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'naryadNumber',
                    'label' => '№ Наряда',
                ],
                [
                    'attribute' => 'article',
                    'label' => 'Артикул',
                ],
                [
                    'attribute' => 'readyDate',
                    'format'=> ['datetime', 'php:d.m.Y H:i'],
                    'label' => 'Дата готовности',
                ],
            ]]) ?>
    </div>
    <div class="col-md-5 col-md-offset-1">
        <h2>Склад</h2>
        <?= GridView::widget([
            'summary'=> '',
            'show' => ['naryadNumber', 'article', 'literal'],
            'rowOptions' => function ($data) use ($idsOfNewStorageStates) {
                return [
                    'class' => in_array($data->id, $idsOfNewStorageStates) ? 'success' : '',
                    'data' => [
                        'id' => $data->id
                    ],
                ];
            },
            'dataProvider' => $storageStates,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'naryadNumber',
                    'label' => '№ UPN',
                ],
                [
                    'attribute' => 'article',
                    'label' => 'Артикул',
                ],
                [
                    'attribute' => 'literal',
                    'label' => 'Литера',
                ],
                [
                    'label' => 'Кнопка для распечатки',
                    'format' => 'raw',
                    'value' => function ($storageState) {
                        return Html::button('Печать', [
                            'class' => 'btn btn-info print-button',
                            'style' => 'width: 100px; padding:4px 15px;',
                            'data' => ['naryadId' => $storageState->id],
                            'onclick' => 'printLabel(' . $storageState->upnId . ',"' . $storageState->literal. '");',
                        ]);
                    },
                    'contentOptions' => ['class' => 'text-center', 'style' => 'width: 150px;'],
                ],
            ]]) ?>
    </div>
</div>

<?php Pjax::end(); ?>
