<?php
use yii\helpers\Html;
use common\models\laitovo\ErpProductType;

$prod = null;
foreach (ErpProductType::find()->all() as $product) {
    if ($product->rule && preg_match($product->rule, $model->article)) {
        $prod = $product;
    }
}
$type = '';
if ($prod) {
    $type = $prod->title_en ? $prod->title_en : '';
}
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

//Если есть номер UPN
if ($model->id) {
    $name = str_replace((mb_substr($model->id, 4, mb_strlen($model->id))),
                ('<b style="font-size: 2em">' . mb_substr($model->id, 4, mb_strlen($model->id)) . '</b>'), $model->id);
}   

?>

<p class="page-break-<?= $model->id ?>"></p>
<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td style="border-collapse: collapse;padding: 0;border-bottom: none;border: none;font-size: 2.7em;">
                <i>UPN:</i> <?= $model->id ? $name : 'undefined' ?>
        </td>
    </tr>
    <tr>
        <td  style="border-collapse: collapse;padding: 0;border-bottom: none;border: none;" >
            <div style="display: inline-block; margin-left: auto;margin-right: auto;">
                <?= $generator->getBarcode(@$model->barcode, $generator::TYPE_CODE_128, 1) ?>
            </div>
        </td>
    </tr>
    <tr>
        <td style="border: none;font-size: 5.4em; font-weight: bold;"><?= $literal?></td>
    </tr>
    </tbody>
</table>
