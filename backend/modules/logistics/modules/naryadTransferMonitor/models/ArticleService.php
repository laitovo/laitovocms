<?php

namespace backend\modules\logistics\modules\naryadTransferMonitor\models;

use backend\helpers\BarcodeHelper;
use common\models\laitovo\Cars;

class ArticleService
{
    public function getArticleByBarcode($barcode)
    {
        $barcode = BarcodeHelper::toLatin($barcode);
        if (mb_stripos($barcode,'-') !== false) {
            return null;
        }

        $value = [];
        $article = null;

        $window = mb_substr($barcode,0,2);
        $carArticle = ltrim(mb_substr($barcode,2,4),0);
        $cloth = ($res = mb_substr($barcode,6,2)) == '00' ? '0' : ltrim(mb_substr($barcode,6,2),0);
        $type = mb_substr($barcode,8);

        if (in_array($window, ['FW','FD','FV','RD','RV','BW']) && mb_strlen($barcode) == 9) {
            $car =  Cars::find()->where(['article' => $carArticle])->one();
            $mark = null;
            if ($car) {
                $mark = $car->getTranslit(mb_substr($car->mark,0,1));
            }
            $value[] = $window;
            $value[] = $mark;
            $value[] = $carArticle;
            $value[] = $cloth;
            $value[] = $type;
            $article = implode('-',$value);
        }elseif(in_array($window, ['OT']) && mb_strlen($barcode) == 9) {
            $value[] = $window;
            $value[] = $carArticle;
            $value[] = $cloth;
            $value[] = $type;
            $article = implode('-',$value);
        }


        return (in_array($window, ['OT']) && mb_strlen($barcode) == 9) ? $article : null;
    }
}