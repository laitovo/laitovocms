<?php

namespace backend\modules\logistics\modules\naryadTransferMonitor\models;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use core\logic\CheckStoragePosition;
use core\models\article\Article;
use core\logic\UpnGroup;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

class Terminal
{
    private $_userService;
    private $_naryadService;
    private $_articleService;
    private $_storageStateService;
    private $_storageStatesDtoProvider;

    private $_idOfCurrentTerminalUser;
    private $_idsOfNewNaryadsOnDispatcher;
    private $_idsOfNewStorageStates;
    private $_idOfNaryadOnConfirm;
    private $_articleOnConfirm;
    private $_quantityEnter;
    private $_quantity;
    private $_isNaryadConfirmShowedInCurrentPeep;
    private $_isArticleConfirmShowedInCurrentPeep;
    private $_idOfStorageStateCreatedInCurrentPeep;
    private $_status;
    private $_message;

    public function __construct()
    {
        $this->_userService = new UserService();
        $this->_naryadService = new NaryadService();
        $this->_articleService = new ArticleService();
        $this->_storageStateService = new StorageStateService();
        $this->_storageStatesDtoProvider = new StorageStatesDtoProvider();
        $this->_loadProperties();
    }

    public function getIdsOfNewNaryadsOnDispatcher()
    {
        return $this->_idsOfNewNaryadsOnDispatcher;
    }

    public function getIdsOfNewStorageStates()
    {
        return $this->_idsOfNewStorageStates;
    }
    
    public function getStatus()
    {
        return $this->_status;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function getMessageAsHtml()
    {
        return nl2br($this->getMessage());
    }

    public function getNewStorageState()
    {
        return $this->_storageStatesDtoProvider->getStorageStateById($this->_idOfStorageStateCreatedInCurrentPeep);
    }

    public function getCurrentUser()
    {
        return $this->_userService->getUserById($this->_idOfCurrentTerminalUser);
    }

    /**
     * Осовная функция, которая позволяет обработать ввод штрихкода пользователем
     *
     * @param $barcode
     */
    public function peep($barcode)
    {

        $barcode = $this->_prepareBarcode($barcode);
        
        if ($user = $this->_getUserByBarcode($barcode)) {
            $this->_peepUser($user);
        } elseif ($upn = $this->_getUpnByBarcode($barcode)) {
            $this->_peepUpn($upn);
        } elseif ($act = $this->_getMoveActBarcode($barcode)) {
            $this->_peepMoveAct($act);
        } elseif ($prodGroup = $this->_getProdGroupBarcode($barcode)) {
            $this->_peepProdGroup($prodGroup);
        } elseif ($article = $this->_getArticleByBarcode($barcode)) {
            $this->_peepArticle($article);
        } elseif ($this->_isQuantitativeBarcode($barcode)) {
            $this->_peepQuantitativeBarcode();
        } elseif ($quantity = $this->_isQuantity($barcode)) {
            $this->_peepQuantity($quantity);
        } else {
            $this->_showFailBarcodeMessage($barcode);
        }
        $this->_afterPeep();
    }

    private function _peepUser($user)
    {
        $this->_saveUser($user);
        $this->_showHelloUserMessage($user);
    }

    private function _peepArticle($article)
    {
        if (!$this->_currentUserExists()) {
            $this->_showMessage('Зарегистрируйтесь, если хотите оприходовать артикул.');
        } else if (!$this->_currentUserIsStorekeeper()) {
            $this->_showMessage('Вы не можете оприходовать артикул, так как не являетесь кладовщиком.', false);
        } else if (!$this->_isArticleConfirmed($article)) {
            $this->_peepArticleNotConfirmed($article);
        } else {
            $this->_peepArticleConfirmed($article);
        }
    }

    private function _peepUpn($upn)
    {
        $naryads = [];

        if ($upn->isParent) {
            $children = $upn->children;
            if (empty($children)) {
                $this->_showMessage('В данном комплекте не содержится ни одной позиции!!!! С ним нельзя совершать никаких действий');
            } else {
                $naryads = ErpNaryad::find()->where(['in','logist_id',ArrayHelper::map($children,'id','id')])->orderBy('article')->all();
                if (empty($naryads) || count($naryads) != count($children)) {
                    $this->_showMessage('Данный комплект не может быть оприходован. Не все наряды на производство существуют !!!');
                } elseif (($messageLiteral = $this->isAlreadyInStorage($naryads))) {
                    $this->_showMessage("Положи В <span style='font-size: 6em'>$messageLiteral</span>");
                } elseif (!$this->_currentUserExists()) {
                    $this->_showMessage('Зарегистрируйтесь, если хотите сдать или оприходовать этот Upn.');
                } elseif ($this->_isNaryadOnInstanceUser($naryads)) {
                    $this->_peepNaryadToDispatcher($naryads);
                    if ($this->_status)
                        $this->_peepNaryadToStorage($naryads);
                } elseif ($this->_isNaryadOnUser($naryads)) {
                    $this->_peepNaryadToDispatcher($naryads);
                } elseif ($this->_isNaryadOnDispatcher($naryads)) {
                    $this->_peepNaryadToStorage($naryads);
                } else {
                    $this->_showFailNaryadMessage($naryads);
                }
            }
        } else {
            if ($upn->group && !empty(UpnGroup::getAnotherFromGroup($upn))) {
                $this->_showMessage('Данный UPN сдается и оприходуется только в составе комплекта!!! Пропикайте этикетку комплекта!!! Если вы не скомплектовали данный UPN, Скомплектуйте его!', false);
                return;
            }
            $naryads = ($workOrder = ErpNaryad::find()->where(['logist_id' => $upn->id])->one()) ? [$workOrder] : [];
            if (empty($naryads) && $upn->storageState) {
                $this->_showMessage("Положи В <span style='font-size: 6em'>" . $upn->storageState->literal . "</span>");
            }elseif (empty($naryads)) {
                $this->_showMessage('Данный UPN не может быть оприходован. Не существует наряда на производство !!!');
            } elseif (($messageLiteral = $this->isAlreadyInStorage($naryads))) {
                $this->_showMessage("Положи В <span style='font-size: 6em'>$messageLiteral</span>");
            } elseif (!$this->_currentUserExists()) {
                $this->_showMessage('Зарегистрируйтесь, если хотите сдать или оприходовать этот наряд.');
            } elseif ($this->_isNaryadOnInstanceUser($naryads)) {
                $this->_peepNaryadToDispatcher($naryads);
                if ($this->_status)
                    $this->_peepNaryadToStorage($naryads);
            } elseif ($this->_isNaryadOnUser($naryads)) {
                $this->_peepNaryadToDispatcher($naryads);
            } elseif ($this->_isNaryadOnDispatcher($naryads)) {
                $this->_peepNaryadToStorage($naryads);
            } else {
                $this->_showFailNaryadMessage($naryads);
            }
        }

    }

    private function _peepMoveAct($act)
    {
        $naryads = ErpNaryad::find()->where(['actNumber' => $act])->all();

        if (empty($naryads)) {
            $this->_showMessage('Данный UPN не может быть оприходован. Не существует наряда на производство !!!');
        } elseif (!$this->_currentUserExists()) {
            $this->_showMessage('Зарегистрируйтесь, если хотите сдать или оприходовать этот наряд.');
        } elseif ($this->_isNaryadOnInstanceUser($naryads)) {
            $this->_peepNaryadToDispatcher($naryads);
            if ($this->_status)
                $this->_peepNaryadToStorage($naryads);
        } elseif ($this->_isNaryadOnUser($naryads)) {
            $this->_peepNaryadToDispatcher($naryads,$act);
        } elseif ($this->_isNaryadOnDispatcher($naryads)) {
            $this->_peepNaryadToStorage($naryads,$act);
        } else {
            $this->_showFailNaryadMessage($naryads);
        }
    }

    private function _peepProdGroup($prodGroup)
    {
        $naryads = ErpNaryad::find()->where(['prodGroup' => $prodGroup])->all();

        if (empty($naryads)) {
            $this->_showMessage('Данный UPN не может быть оприходован. Не существует наряда на производство !!!');
        } elseif (!$this->_currentUserExists()) {
            $this->_showMessage('Зарегистрируйтесь, если хотите сдать или оприходовать этот наряд.');
        } elseif ($this->_isNaryadOnInstanceUser($naryads)) {
            $this->_peepNaryadToDispatcher($naryads);
            if ($this->_status)
                $this->_peepNaryadToStorage($naryads);
        } elseif ($this->_isNaryadOnUser($naryads)) {
            $this->_peepNaryadToDispatcher($naryads,$prodGroup);
        } elseif ($this->_isNaryadOnDispatcher($naryads)) {
            $this->_peepNaryadToStorage($naryads,$prodGroup);
        } else {
            $this->_showFailNaryadMessage($naryads);
        }
    }

    private function _peepNaryadToDispatcher($naryads,$act = false)
    {
        if (!$this->_moveNaryadToDispatcher($naryads)) {
            $this->_showMessage('Данный UPN не может быть сдан на склад.', false);
            return;
        }
        $this->_saveNewNaryadOnDispatcher($naryads);
        $act ? $this->_showMessage('Вы успешно сдали Акт № ' . $act .' на склад.') : $this->_showMessage('Вы успешно сдали UPN на склад.');
    }

    private function _peepNaryadToStorage($naryads,$act = false)
    {
        if (!$this->_currentUserIsStorekeeper()) {
            $this->_showMessage('Вы не можете оприходовать наряд, так как не являетесь кладовщиком.', false);
        } else if (!$this->_isNaryadConfirmed($naryads)) {
            $this->_peepNaryadToStorageNotConfirmed($naryads);
        } else {
            $this->_peepNaryadToStorageConfirmed($naryads,$act);
        }
    }

    private function _peepNaryadToStorageNotConfirmed($naryads)
    {
        $this->_saveNaryadOnConfirm($naryads);
        $this->_showNaryadConfirmMessage($naryads);
    }

    private function _peepNaryadToStorageConfirmed($naryads,$act = false)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $storageStates = [];
            if ($act) {
                if (!($literal = $this->_createStorageStateByNaryadAct($naryads, $this->_idOfCurrentTerminalUser))) {
                    $transaction->rollBack();
                    $this->_showMessage('Данный Акт не получилось оприходовать на склад.', false);
                    return;
                }
                $transaction->commit();
                $this->_showMessage('<span style="font-size: 8em;color: white;"> Акт №: '. $act .'<br> Литера: '. $literal .'</span>');
            } else {
                foreach ($naryads as $naryad) {
                    if (!($storageState = $this->_createStorageStateByNaryad($naryad, $this->_idOfCurrentTerminalUser))) {
                        $transaction->rollBack();
                        $this->_showMessage('Данный Upn не получилось оприходовать на склад.', false);
                        return;
                    }
                    $storageStates[] = $storageState;
                }
                $transaction->commit();
                //Допилить этот кусок
                $this->_saveNewStorageStates($storageStates);
                $this->_showMessage('<span style="font-size: 8em;color: white;">'. $this->_getStorageStatesInfo($storageStates) . '</span>');
            }
        }catch (\DomainException $exception) {
            $transaction->rollBack();
            $this->_showMessage($exception->getMessage());
        }
    }

    private function _peepArticleNotConfirmed($article)
    {
        $this->_saveArticleOnConfirm($article);
        $this->_showArticleConfirmMessage($article);
    }

    private function _peepArticleConfirmed($article)
    {
        if (!($storageState = $this->_createStorageStateByArticle($article))) {
            $this->_showMessage('Данный артикул не может быть оприходован.' . "\n" . $article, false);
            return;
        }
        $this->_saveNewStorageState($storageState);
        $this->_showMessage('Артикул успешно оприходован.' . "\n" . $article);
    }

    private function _afterPeep()
    {
        $this->_cleanDeclinedConfirms();
        $this->_saveProperties();
    }

    private function _saveUser($user) {
        $this->_deleteProperties();
        $this->_idOfCurrentTerminalUser = $this->_userService->getId($user);
        $this->_saveProperties();
    }

    private function _saveNewNaryadOnDispatcher($naryad)
    {
        $this->_idsOfNewNaryadsOnDispatcher[] = $this->_naryadService->getIds($naryad);
        $this->_saveProperties();
    }

    private function _saveNewStorageState($storageState)
    {
        $this->_idsOfNewStorageStates[] = $this->_storageStateService->getId($storageState);
        $this->_saveList();
        $this->_idOfStorageStateCreatedInCurrentPeep = $this->_storageStateService->getId($storageState);
    }

    private function _saveNewStorageStates($storageStates)
    {
        $this->_idsOfNewStorageStates[] = $this->_storageStateService->getIds($storageStates);
        $this->_saveList();
        $this->_idOfStorageStateCreatedInCurrentPeep = $this->_storageStateService->getIds($storageStates);
    }

    private function _saveNaryadOnConfirm($naryads)
    {
        $this->_idOfNaryadOnConfirm = $this->_naryadService->getIds($naryads);
        $this->_saveProperties();
        $this->_isNaryadConfirmShowedInCurrentPeep = true;
    }

    private function _saveArticleOnConfirm($article)
    {
        $this->_articleOnConfirm = $article;
        $this->_saveProperties();
        $this->_isArticleConfirmShowedInCurrentPeep = true;
    }

    private function _loadProperties()
    {
        $this->_idOfCurrentTerminalUser = Yii::$app->request->cookies->getValue('TERMINAL_USER');
        $this->_idsOfNewNaryadsOnDispatcher = Yii::$app->request->cookies->getValue('TERMINAL_NEW_NARYADS_ON_DISPATCHER') ?: [];
        $this->_idsOfNewStorageStates = Yii::$app->request->cookies->getValue('TERMINAL_NEW_STORAGE_STATES') ?: [];
        $this->_idOfNaryadOnConfirm = Yii::$app->request->cookies->getValue('TERMINAL_NARYAD_ON_CONFIRM');
        $this->_articleOnConfirm = Yii::$app->request->cookies->getValue('TERMINAL_ARTICLE_ON_CONFIRM');
        $this->_quantityEnter = Yii::$app->request->cookies->getValue('TERMINAL_QUANTITY_ENTER');
        $this->_quantity = Yii::$app->request->cookies->getValue('TERMINAL_QUANTITY_VAL');
    }

    private function _saveProperties()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'TERMINAL_USER',
            'value' => $this->_idOfCurrentTerminalUser,
            'expire' => time() + 120,
        ]));
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'TERMINAL_NARYAD_ON_CONFIRM',
            'value' => $this->_idOfNaryadOnConfirm,
            'expire' => time() + 60,
        ]));
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'TERMINAL_ARTICLE_ON_CONFIRM',
            'value' => $this->_articleOnConfirm,
            'expire' => time() + 60,
        ]));
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'TERMINAL_QUANTITY_ENTER',
            'value' => $this->_quantityEnter,
            'expire' => time() + 60,
        ]));
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'TERMINAL_QUANTITY_VAL',
            'value' => $this->_quantity,
            'expire' => time() + 60,
        ]));
    }

    private function _saveList(){
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'TERMINAL_NEW_NARYADS_ON_DISPATCHER',
            'value' => $this->_idsOfNewNaryadsOnDispatcher,
            'expire' => time() + 60,
        ]));
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'TERMINAL_NEW_STORAGE_STATES',
            'value' => $this->_idsOfNewStorageStates,
            'expire' => time() + 60,
        ]));
    }


    private function _deleteProperties()
    {
        $this->_idOfCurrentTerminalUser = null;
        $this->_idOfNaryadOnConfirm = null;
        $this->_articleOnConfirm = null;
        $this->_quantityEnter = null;
        $this->_quantity = null;
        $this->_saveProperties();
    }

    private function _cleanDeclinedConfirms() {
        if (!$this->_isNaryadConfirmShowedInCurrentPeep) {
            $this->_idOfNaryadOnConfirm = null;
        }
        if (!$this->_isArticleConfirmShowedInCurrentPeep && !$this->_quantityEnter) {
            $this->_articleOnConfirm = null;
        }
    }

    private function _showFailBarcodeMessage($barcode)
    {
        $this->_showMessage('Ничего не найдено по штрихкоду.' . "\n" . $barcode, false);
    }

    private function _showHelloUserMessage($user)
    {
        $this->_showMessage($user->name);
    }

    private function _showFailNaryadMessage($naryad)
    {
        $this->_showMessage('Данный наряд не может быть сдан или оприходован.', false);
    }

    private function _showNaryadConfirmMessage($naryad)
    {
        $this->_showMessage('Вы действительно хотите оприходовать данный UPN?');
    }

    private function _showArticleConfirmMessage($article)
    {
        $this->_showMessage('Вы действительно хотите оприходовать артикул?' . "\n" . $article);
    }

    private function _showMessage($message, $status = true)
    {
        $this->_status = $status;
        $this->_message = $message;
    }

    private function _currentUserExists()
    {
        return (bool)$this->_idOfCurrentTerminalUser;
    }

    private function _currentUserIsStorekeeper()
    {
        if (!$this->_currentUserExists()) {
            return false;
        }

        return true; // todo: Реализовать логику проверки кладовщика
    }

    private function _isNaryadConfirmed($naryads)
    {
        return $this->_naryadService->getIds($naryads) == $this->_idOfNaryadOnConfirm;
    }

    private function _isArticleConfirmed($article)
    {
        return $article == $this->_articleOnConfirm;
    }

    private function _createStorageStateByNaryad($naryad, $userId = null)
    {
        return $this->_storageStateService->createStorageStateByNaryad($naryad, $userId);
    }

    private function _createStorageStateByNaryadAct($naryads, $userId = null)
    {
        return $this->_storageStateService->createStorageStateByNaryadAct($naryads, $userId);
    }

    private function _createStorageStateByArticle($article)
    {
        return $this->_storageStateService->createStorageStateByArticle($article);
    }

    private function _moveNaryadToDispatcher($naryad)
    {
        return $this->_naryadService->moveNaryadToDispatcher($naryad,$this->_idOfCurrentTerminalUser);
    }

    private function _getUserByBarcode($barcode)
    {
        return $this->_userService->getUserByBarcode($barcode);
    }

    private function _getMoveActBarcode($barcode)
    {
        return ErpNaryad::findAct($barcode);
    }

    private function _getProdGroupBarcode($barcode)
    {
        return ErpNaryad::findProdGroup($barcode);
    }

    private function _getUpnByBarcode($barcode)
    {
        return $this->_naryadService->getUpnByBarcode($barcode);
    }

    private function _getNaryadByUpnId($upnId)
    {
        return $this->_naryadService->getNaryadByUpnId($upnId);
    }

    private function _getArticleByBarcode($barcode)
    {
        return $this->_articleService->getArticleByBarcode($barcode);
    }

    private function _isNaryadOnUser($naryad)
    {
        return $this->_naryadService->isNaryadOnUser($naryad, $this->_idOfCurrentTerminalUser);
    }

    private function _isNaryadOnInstanceUser($naryad)
    {
        return $this->_naryadService->isNaryadOnInstanceUser($naryad, $this->_idOfCurrentTerminalUser);
    }

    private function _isNaryadOnDispatcher($naryad)
    {
        return $this->_naryadService->isNaryadOnDispatcher($naryad);
    }

    private function _getNaryadInfo($naryad)
    {
        return $this->_naryadService->getNaryadInfo($naryad);
    }

    private function _getStorageStateInfo($storageState)
    {
        return $this->_storageStateService->getLiteral($storageState);
    }


    private function _getStorageStatesInfo($storageStates)
    {
        foreach ($storageStates as $storageState) {
            return $this->_storageStateService->getLiteral($storageState);
        }
    }

    private function _getStoragesStateInfo($storageStates)
    {
        return $this->_storageStateService->getCommonLiteral($storageStates);
    }
    #Мной добавленный функционал

    /**
     * Функция подготавливает переданное значение штрихкода к обработке и возвращает его
     *
     * @param string $barcode
     * @return string
     */
    private function _prepareBarcode($barcode)
    {
        $barcode = BarcodeHelper::toLatin($barcode);
        $barcode = mb_strtoupper($barcode);
        return $barcode;
    }

    /**
     * Функция проверяет, является ли переданный штрихкод, штрихкодом оприходывания складских позиций по количеству.
     * 
     * @param $barcode
     * @return bool
     */
    private function _isQuantitativeBarcode($barcode)
    {
        return $barcode == CheckStoragePosition::getQuantitativeBarcode() ?:false;
    }


    /**
     * Процедура пропикивания по штрихкоду пополнения склада
     *
     * @return bool
     */
    private function _peepQuantitativeBarcode()
    {
        $article       = $this->_articleOnConfirm;
        $quantityEnter = $this->_quantityEnter;
        $quantity      = $this->_quantity;


        if ($article && CheckStoragePosition::isStorageArticle($article) && !$quantityEnter) {
            $this->_quantityEnter = true;
            $this->_saveProperties();
            $this->_status  ='success';
            $this->_message ='Введите количество позиций артикула '. $article .', которое хотите оприходовать на склад!';
            return true;
        }elseif ($article && CheckStoragePosition::isStorageArticle($article) && $quantityEnter && $quantity) {
            $storage = null;
            $prod = null;
            //Статситику по артикулу
            $objArticle = new Article($article);

            //Получим планируемый баланс
            $plan = $objArticle->balancePlan;
            $fact = $objArticle->balanceFact;

            //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
            $storageType = ($plan && $plan>0 && $plan > $fact) || mb_substr($article,0,2) == 'OT' ? Storage::TYPE_REVERSE : Storage::TYPE_TIME;

            $productId = $objArticle->product ? $objArticle->product->id : null;

            if ($productId) {
                $storage = Storage::findStorageForProduct($productId,$storageType);
            }

            if (!$storage) {
                //Устанавливаем ответ по умолчанию
                $this->_status ='error';
                $this->_message='Для данного продукта не определено место хранения!!!';
                return false;
            }

            for ($i = 1; $i <= $this->_quantity; $i++) {
                //Внутри склада определяем литеру
                $literal = $storage->getActualLiteral($productId);

                //Содание UPN для наряда
                $logist = new Naryad();
                $logist->article = $article;
                $logist->team_id = Yii::$app->team->getId();
                $logist->source_id = 2;
                $logist->save();

                //Ставим, что данный наряд распределен с диспечер-склад
                $row = new StorageState();
                $row->upn_id = $logist->id;
                $row->storage_id = $storage->id;
                $row->literal = $literal;
                $row->save();

                $this->_idsOfNewStorageStates[] = $logist->id;
            }

            $this->_saveList();

            $this->_status  ='success';
            $this->_message ='Вы успешно оприходовали артикул ' . $article . ', в количестве ' . $quantity. ' штук!';

            $this->_deleteProperties();
//            $model->deleteProperties();
            return true;
        }

        //Если введен штрихкод для воода количества тогда мы сначала проверяем, а есть ли у модели
        $this->_status  ='success';
        $this->_message ='Введен штрихкод для оприходывания по количеству.';
        return true;
    }

    /**
     * Функция проверяет, является ли переданный штрихкод, целым числом.
     *
     * @param $barcode
     * @return bool
     */
    private function _isQuantity($barcode)
    {
        return ctype_digit($barcode) ? (int)$barcode : 0;
    }

    /**
     * Функция проверяет, является ли переданный штрихкод, целым числом.
     *
     * @param $barcode
     * @return bool
     */
    private function _peepQuantity($barcode)
    {
        $article       = $this->_articleOnConfirm;
        $quantityEnter = $this->_quantityEnter;
        $quantity      = $this->_quantity;

        if ($article && CheckStoragePosition::isStorageArticle($article) && $quantityEnter) {
            $this->_quantity = $barcode;
            $this->_saveProperties();
            $this->_status  = 'success';
            $this->_message ='Вы действительно желаете оприходовать артикул'. $article .', в количестве ' . $this->_quantity . ' штук!';
            return true;
        }

        return false;
    }

    private function isAlreadyInStorage($items)
    {
        /**
         * @var $items ErpNaryad[]
         */
        $item = $items[0];

        if ( ($upn = $item->upn) && ($state = $upn->storageState) ) {
            if (!$upn->reserved_id && !$upn->pid && $state->storage_id == 11 && preg_match('/TT-T/', $state->literal) === 1)
            {
                $objArticle = new Article($upn->article);

                //Кусок проверки на габариты артикула
                $oversized = false;
                if ($car = $objArticle->car) {
                    switch ($objArticle->windowEn) {
                        case 'FV':
                            $length = $car->json('_fv')['dlina']??0;
                            $width = $car->json('_fv')['visota']??0;
                            break;
                        case 'FD':
                            $length = $car->json('_fd')['dlina']??0;
                            $width = $car->json('_fd')['visota']??0;
                            break;
                        case 'RD':
                            $length = $car->json('_rd')['dlina']??0;
                            $width = $car->json('_rd')['visota']??0;
                            break;
                        case 'RV':
                            $length = $car->json('_rv')['dlina']??0;
                            $width = $car->json('_rv')['visota']??0;
                            break;

                        case 'BW':
                            $length = $car->json('_bw')['dlina']??0;
                            $width = $car->json('_bw')['visota']??0;
                            break;
                        default:
                            $length = 0;
                            $width = 0;
                            break;
                    }
                    if ( min([$length,$width]) > 680 ) {
                        $oversized = true;
                    }
                }

                $storageType = Storage::TYPE_REVERSE;

                $productId = $objArticle->product ? $objArticle->product->id : null;
                if ($productId) {
                    $storage = Storage::findStorageForProduct($productId,$storageType);
                }
                if (!$storage) {
                    return false;
                }

                $literal = $storage->getActualLiteral($productId, $oversized);

                $state->literal = $literal;
                $state->storage_id = $storage->id;
                if ($state->save()) {
                    return $state->literal;
                } else {
                    return false;
                }
            }

            return $state->literal;
        }

        return false;
    }




}