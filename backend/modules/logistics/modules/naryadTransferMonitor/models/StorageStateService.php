<?php

namespace backend\modules\logistics\modules\naryadTransferMonitor\models;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use core\logic\UpnGroup;
use core\models\article\Article;
use Yii;
use backend\modules\logistics\models\Naryad as LogisticsNaryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use yii\db\Expression;
use \yii\helpers\ArrayHelper;

class StorageStateService
{
    public function getStorageStates()
    {
        $storagesArray = Storage::find()->where(['in','type',[Storage::TYPE_REVERSE, Storage::TYPE_TIME,Storage::TYPE_TRANSIT]])->all();
        $ids = ArrayHelper::map($storagesArray,'id','id');
        $storageStates = StorageState::find()->where(['in','storage_id',$ids])->limit(100)->orderBy(['created_at' => SORT_DESC])->all();

        return $storageStates;
    }

    public function getStorageStateById($storageStateId)
    {
        return StorageState::findOne($storageStateId);
    }

    public function getId($storageState)
    {
        return $storageState->id;
    }

    public function getIds($storageStates)
    {
        return ArrayHelper::map($storageStates,'id','id');
    }

    public function getUpnId($storageState)
    {
        return $storageState->upn_id;
    }

    public function getArticle($storageState)
    {
        return $storageState->upn->article ?? null;
    }

    public function getLiteral($storageState)
    {
        return $storageState->literal;
    }

    public function getCommonLiteral($storageStates)
    {
        foreach ($storageStates as $storageState) {
            return $storageState->literal;
        }
    }

    public function createStorageStateByNaryad($naryad, $userId = null)
    {
        $objArticle = new Article($naryad->article);

        //Кусок проверки на габариты артикула
        $oversized = false;
        if ($car = $objArticle->car) {
            switch ($objArticle->windowEn) {
                case 'FV':
                    $length = $car->json('_fv')['dlina']??0;
                    $width = $car->json('_fv')['visota']??0;
                    break;
                case 'FD':
                    $length = $car->json('_fd')['dlina']??0;
                    $width = $car->json('_fd')['visota']??0;
                    break;
                case 'RD':
                    $length = $car->json('_rd')['dlina']??0;
                    $width = $car->json('_rd')['visota']??0;
                    break;
                case 'RV':
                    $length = $car->json('_rv')['dlina']??0;
                    $width = $car->json('_rv')['visota']??0;
                    break;

                case 'BW':
                    $length = $car->json('_bw')['dlina']??0;
                    $width = $car->json('_bw')['visota']??0;
                    break;
                default:
                    $length = 0;
                    $width = 0;
                    break;
            }
            if ( min([$length,$width]) > 680 ) {
                $oversized = true;
            }
        }

        //Если приходуем транзитный UPN, то есть UPN, котоорый сразу поедет клиенту.
        if ($naryad->upn && $naryad->upn->reserved_id) {

            /**
             * Вставка для оприходования комплектов
             */
            if ($naryad->upn && ($state = UpnGroup::getStorageStateOfGroup($naryad->upn)))
            {
                $storage = $state->storage;
                $literal = $state->literal;
            } else {
                $storage = null;
                //1. Определяем, под какой заказ зарезервирован данный UPN
                $order = $naryad->upn->orderEntry->order;
                //2. Ищем всех детей и наличие на складе.

                $entries = OrderEntry::find()->select('id')->where(['and',['order_id' => $order->id],['is_deleted' => null]])->column();
                //2. Ищем всех детей и наличие на складе.
                $upnIds = LogisticsNaryad::find()->select('id')->where(['in','reserved_id',$entries])->column();

                //3. Проверяем наличие хотябы одного upn с этого заказа, который уже лежит на транзитном складе
                $storageIds =  Storage::find()->select('id')->where(['type'=> Storage::TYPE_TRANSIT])->column();
//
                $literalCount = 4;
//
//                if ($order->export_username == 'Склад') {
//                    /**
//                     * Костыль, чтобы все ЗПС на пополнение в Гамбург падали в отдельную ячейку с конвертом
//                     */
//                    $notFullLiteral = null;
//                } else {
                if (!$oversized) {
                    $startLiteral = 1;
                    $endLiteral = 126;
                    $literalCount = 4;

                } else {
                    $startLiteral = 127;
                    $endLiteral = 182;
                    $literalCount = 6;
                }

                $orderLiterals = StorageState::find()->where(['and',['in','storage_id',$storageIds],['in','upn_id',$upnIds]])
                    ->andWhere(['not like','literal','::%',false])
                    ->andWhere("CAST(REPLACE(`literal`,'TT-T','') AS UNSIGNED) >= $startLiteral")
                    ->andWhere("CAST(REPLACE(`literal`,'TT-T','') AS UNSIGNED) <= $endLiteral")
                    ->select(['literal'])
                    ->groupBy(['literal','storage_id'])
                    ->having(new Expression('COUNT(id) < :realCount',[':realCount' => $literalCount]))
                    ->column();


                $notFullLiteral = StorageState::find()->where(['and',['in','storage_id',$storageIds],['in','literal',$orderLiterals]])
                    ->select(['literal','storage_id','count' => 'COUNT(id)'])
                    ->groupBy(['literal','storage_id'])
                    ->having(new Expression('COUNT(id) < :realCount',[':realCount' => $literalCount]))
                    ->one();
//                }
//
                if ($notFullLiteral) {
                    $storage = $notFullLiteral->storage;
                    $literal = $notFullLiteral->literal;
                } else {
                    /**
                     * @var Storage $storage
                     */
                    $storage = Storage::find()->where(['type'=> Storage::TYPE_TRANSIT])->one();
                    if (!$storage) {
                        throw new \DomainException('Заведите транзитный склад в программе !!!');
                    }
                    // Если шторка негабаритная, тогда сначала ищется литера негабаритная, а затем обычная
                    if ($oversized) {
                        $literal = $storage->getTempLiteral(true);
                        if (!$literal) $literal = $storage->getTempLiteral(false);
                    // Если шторка Габаритная, тогда сначала ищется литера обычная, а затем негабаритная
                    } else {
                        $literal = $storage->getTempLiteral(false);
                        if (!$literal) $literal = $storage->getTempLiteral(true);
                    }
                    if (!$literal) {
                        throw new \DomainException('Транзитный склад заполнен полностью. Пока больше не стоит приходовать. Пакуйте заказы!');
                    }
                }
            }

            $transaction = Yii::$app->db->beginTransaction();
            //Содание UPN для наряда
            if (!$naryad->logist_id) {
                $logist = new LogisticsNaryad();
                $logist->article = $naryad->article;
                $logist->source_id = 2;
                $logist->team_id = Yii::$app->team->getId();
                if ($userId) {
                    $logist->comment = $logist->comment ? $logist->comment . "[ Оприходовал: $userId]" : "[ Оприходовал: $userId]";
                }
                if (!$logist->save()) {
                    $transaction->rollback();
                    return false;
                }
                $naryad->logist_id = $logist->id;
                if (!$naryad->save()) {
                    $transaction->rollback();
                    return false;
                }
            } else {
                $logist = $naryad->upn;
                if ($userId) {
                    $logist->comment = $logist->comment ? $logist->comment . "[ Оприходовал: $userId]" : "[ Оприходовал: $userId]";
                    if (!$logist->save()) {
                        $transaction->rollback();
                        return false;
                    }
                }
            }

            //Ставим, что данный наряд распределен с диспечер-склад
            $row = new StorageState();
            $row->upn_id = $naryad->logist_id;
            $row->storage_id = $storage->id;
            $row->literal = $literal;
            if (!$row->save()) {
                $transaction->rollback();
                return false;
            }

            $naryad->distributed = 1;
            if (!$naryad->save()) {
                $transaction->rollback();
                return false;
            }
            $transaction->commit();

            return $row;
        }

        $storage = null;
        $prod = null;

        //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
        $storageType = Storage::TYPE_REVERSE;

        $productId = $objArticle->product ? $objArticle->product->id : null;
        if ($productId) {
            $storage = Storage::findStorageForProduct($productId,$storageType);
        }
        if (!$storage) {
            return false;
        }

        //По артикулу определяем вид продукта
        if ($naryad->upn && ($state = UpnGroup::getStorageStateOfGroup($naryad->upn)))
        {
            $storage = $state->storage;
            $literal = $state->literal;
        }
        else {
            $literal = $storage->getActualLiteral($productId, $oversized);
            if (!$literal) {
                throw new \DomainException('Cклад заполнен полностью. Необходимо создать новые литеры или разрядить склад!');
            }
        }

        $transaction = Yii::$app->db->beginTransaction();
        //Содание UPN для наряда
        if (!$naryad->logist_id) {
            $logist = new LogisticsNaryad();
            $logist->article = $naryad->article;
            $logist->source_id = 2;
            $logist->team_id = Yii::$app->team->getId();
            if ($userId) {
                $logist->comment = $logist->comment ? $logist->comment . "[ Оприходовал: $userId]" : "[ Оприходовал: $userId]";
            }
            if (!$logist->save()) {
                $transaction->rollback();
                return false;
            }
            $naryad->logist_id = $logist->id;
            if (!$naryad->save()) {
                $transaction->rollback();
                return false;
            }
        } else {
            $logist = $naryad->upn;
            if ($userId) {
                $logist->comment = $logist->comment ? $logist->comment . "[ Оприходовал: $userId]" : "[ Оприходовал: $userId]";
                if (!$logist->save()) {
                    $transaction->rollback();
                    return false;
                }
            }
        }

        //Ставим, что данный наряд распределен с диспечер-склад
        $row = new StorageState();
        $row->upn_id = $naryad->logist_id;
        $row->storage_id = $storage->id;
        $row->literal = $literal;
        if (!$row->save()) {
            $transaction->rollback();
            return false;
        }

        $naryad->distributed = 1;
        if (!$naryad->save()) {
            $transaction->rollback();
            return false;
        }
        $transaction->commit();

        $logistNaryad = $naryad->upn;
        //При оприходовании органайзеров, пытаемся их зарезервировать вместо позиций, которые еще производятся или непонятно где
        if (preg_match('/OT-(1189)/', $naryad->article) === 1 && $logistNaryad->reserved_id === null) {

            //Нам надо получить заявки, которые еще не упакованы
            $shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
            $shipments = $shipmentManager->findWhereColumn('id',['or',['logisticsStatus' => 'shipped'],['logisticsStatus' => 'packed'],['logisticsStatus' => 'handled']]);

            //Сначала находим все неотгруженные позиции органайзеров
            $orderEntryIds = OrderEntry::find()
                ->select('id')
                ->where('order_id is not NULL')
                ->andWhere('is_deleted is NULL')
                ->andWhere(['>','order_id',30000])
                ->andWhere(['article' => $naryad->article])
                ->andWhere(['or','shipment_id not IN (' .implode(',',$shipments) .')','shipment_id is NULL'] )
                ->distinct()
                ->column();

            //Далее из этого списка выбираем позиции, которые уже лежат на складе
            $naryadIds = Naryad::find()
                ->select('id')
                ->where(['reserved_id' => $orderEntryIds])
                ->column();

            $storageIds = StorageState::find()
                ->select('upn_id')
                ->where(['upn_id' => $naryadIds])
                ->column();

            $orderEntryIdsNotInStorage = Naryad::find()
                ->select('reserved_id')
                ->where(['id' => $naryadIds])
                ->andWhere(['not in','id',$storageIds])
                ->column();

            //Находим все заказы, которым принадлежат данные позиции
            $orderIds = OrderEntry::find()
                ->select('order_id')
                ->where(['id' => $orderEntryIdsNotInStorage])
                ->distinct()
                ->column();

            //Находим позицию заказа, для которого зарезервируем текущий органайзер
            $orderEntryForReserve = null;

            $team =  \Yii::$app->team->id;

            //Сначала ищем среди заказов физиков
            $individualOrder = Order::find()
                ->where(['team_id' => $team])
                ->andWhere(['id' => $orderIds])
                ->andWhere(['category' => 'Физик'])
                ->orderBy('id')
                ->one();

            if ($individualOrder) {
                $orderEntryForReserve = OrderEntry::find()
                    ->where('is_deleted is NULL')
                    ->andWhere(['order_id' => $individualOrder->id])
                    ->andWhere(['article' => $naryad->article])
                    ->andWhere(['id' => $orderEntryIdsNotInStorage])
                    ->one();
            } else {
                //Если не нашли у физиков, ищем у оптовиков
                $optOrder = Order::find()
                    ->where(['team_id' => $team])
                    ->andWhere(['id' => $orderIds])
                    ->andWhere(['!=','category','Физик'])
                    ->orderBy('id')
                    ->one();
                if ($optOrder) {
                    $orderEntryForReserve = OrderEntry::find()
                        ->where('is_deleted is NULL')
                        ->andWhere(['order_id' => $optOrder->id])
                        ->andWhere(['article' => $naryad->article])
                        ->andWhere(['id' => $orderEntryIdsNotInStorage])
                        ->one();
                }
            }

            //Если была найдена позиция, перерезервируем
            if ($orderEntryForReserve) {
                $currentNaryadId = @$orderEntryForReserve->upn->id;
                if ($currentNaryadId)
                    Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['order_id' => null, 'sort' => 7, 'autoSpeed' => 0],['logist_id' => $currentNaryadId])->execute();

                Yii::$app->db->createCommand()->update(Naryad::tableName(),['reserved_id' => null],['reserved_id' => $orderEntryForReserve->id])->execute();
                Yii::$app->db->createCommand()->update(Naryad::tableName(),['reserved_id' => $orderEntryForReserve->id],['id' => $naryad->logist_id])->execute();
            }
        }

        return $row;
    }

    public function createStorageStateByNaryadAct($naryads, $userId = null)
    {
        $articles = [];
        $order = null;
        foreach ($naryads as $naryad) {
            $articles[$naryad->article][] = $naryad;
            if (!$order)
                $order = @$naryad->upn->orderEntry->order;
        }
        if (!$order) {
            throw new \DomainException('Не указан заказ, для которого сдается акт');
        }
        /**
         * @var Order $order
         */
        $storage = null;
        //1. Определяем, под какой заказ зарезервирован данный UPN
        //2. Ищем всех детей и наличие на складе.

        $entries = OrderEntry::find()->select('id')->where(['and',['order_id' => $order->id],['is_deleted' => null]])->column();
        //2. Ищем всех детей и наличие на складе.
        $upnIds = LogisticsNaryad::find()->select('id')->where(['in','reserved_id',$entries])->column();



        //3. Проверяем наличие хотябы одного upn с этого заказа, который уже лежит на транзитном складе
        $storageIds = $storageIds = Storage::find()->select('id')->where(['type'=> Storage::TYPE_TRANSIT])->column();
        $storageState = StorageState::find()->where(['and',['in','storage_id',$storageIds],['in','upn_id',$upnIds]])->one();
        $storageStateReserveCount = StorageState::find()->where(['and',['not in','storage_id',$storageIds],['in','upn_id',$upnIds]])->count();

        if ($storageState) {
            $storage = $storageState->storage;
            $literal = $storageState->literal;
        } else {
            /**
             * @var Storage $storage
             */
            $storage = Storage::find()->where(['type'=> Storage::TYPE_TRANSIT])->one();
            if (!$storage) {
                throw new \DomainException('Заведите транзитный склад в программе !!!');
            }
            $literal = $storage->getNotUseLiteral((count($upnIds) - $storageStateReserveCount));
        }

        foreach ($articles as $key => $naryads) {

            $transaction = Yii::$app->db->beginTransaction();
            foreach ($naryads as $naryad) {
                //Содание UPN для наряда
                if (!$naryad->logist_id) {
                    $logist = new LogisticsNaryad();
                    $logist->article = $naryad->article;
                    $logist->source_id = 2;
                    $logist->team_id = Yii::$app->team->getId();
                    if ($userId) {
                        $logist->comment = $logist->comment ? $logist->comment . "[ Оприходовал: $userId]" : "[ Оприходовал: $userId]";
                    }
                    if (!$logist->save()) {
                        $transaction->rollback();
                        return false;
                    }
                    $naryad->logist_id = $logist->id;
                    if (!$naryad->save()) {
                        $transaction->rollback();
                        return false;
                    }
                } else {
                    $logist = $naryad->upn;
                    if ($userId) {
                        $logist->comment = $logist->comment ? $logist->comment . "[ Оприходовал: $userId]" : "[ Оприходовал: $userId]";
                        if (!$logist->save()) {
                            $transaction->rollback();
                            return false;
                        }
                    }
                }

                //Ставим, что данный наряд распределен с диспечер-склад
                $row = new StorageState();
                $row->upn_id = $naryad->logist_id;
                $row->storage_id = $storage->id;
                $row->literal = $literal;
                if (!$row->save()) {
                    $transaction->rollback();
                    return false;
                }

                $naryad->distributed = 1;
                if (!$naryad->save()) {
                    $transaction->rollback();
                    return false;
                }
            }
            $transaction->commit();
        }

        return $literal;
    }

    public function createStorageStateByArticle($article)
    {
        $storage = null;
        $prod = null;

        //Статситику по артикулу
        $objArticle = new Article($article);

        //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
        $storageType = Storage::TYPE_REVERSE;

        $productId = $objArticle->product ? $objArticle->product->id : null;

        if ($productId) {
            $storage = Storage::findStorageForProduct($productId,$storageType);
        }

        if (!$storage) {
            return false;
        }

        //Внутри склада определяем литеру
        $literal = $storage->getActualLiteral($productId);

        $transaction = Yii::$app->db->beginTransaction();

        //Содание UPN для наряда
        $logist = new LogisticsNaryad();
        $logist->article = $article;
        $logist->team_id = Yii::$app->team->getId();
        $logist->source_id = 2;
        if (!$logist->save()) {
            $transaction->rollback();
            return false;
        }

        //Ставим, что данный наряд распределен с диспечер-склад
        $row = new StorageState();
        $row->upn_id = $logist->id;
        $row->storage_id = $storage->id;
        $row->literal = $literal;
        if (!$row->save()) {
            $transaction->rollback();
            return false;
        }
        $transaction->commit();

        return $row;
    }
}