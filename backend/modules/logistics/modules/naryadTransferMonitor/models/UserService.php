<?php

namespace backend\modules\logistics\modules\naryadTransferMonitor\models;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpUser;

class UserService
{
    public function getId($user)
    {
        return $user->id;
    }

    public function getUserById($userId)
    {
        return ErpUser::findOne($userId);
    }

    public function getUserByBarcode($barcode)
    {
        return ErpUser::find()->where(['barcode' => BarcodeHelper::toLatin($barcode)])->one();

    }
}