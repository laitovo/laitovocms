<?php

namespace backend\modules\logistics\modules\naryadTransferMonitor\models;

use yii\data\ArrayDataProvider;

class StorageStatesDtoProvider
{
    private $_storageStateService;

    public function __construct()
    {
        $this->_storageStateService = new StorageStateService();
    }

    public function getStorageStatesAsDataProvider()
    {
        return $this->_asProvider($this->getStorageStates());
    }

    public function getStorageStates()
    {
        $storageStates = $this->_findStorageStates();
        $dtoStorageStates = $this->_prepareStorageStates($storageStates);

        return $dtoStorageStates;
    }

    public function getStorageStateById($storageStateId)
    {
        $storageState = $this->_findStorageStateById($storageStateId);
        $dtoStorageState = $this->_createDTO($storageState);

        return $dtoStorageState;
    }

    private function _findStorageStates()
    {
        return $this->_storageStateService->getStorageStates();
    }

    private function _findStorageStateById($storageStateId)
    {
        return $this->_storageStateService->getStorageStateById($storageStateId);
    }

    private function _prepareStorageStates($storageStates)
    {
        $dtoStorageStates = [];
        foreach ($storageStates as $storageState) {
            $dtoStorageStates[] = $this->_createDTO($storageState);
        }

        return $dtoStorageStates;
    }

    private function _createDTO($storageState)
    {
        if (!$storageState) {
            return null;
        }
        $row['id'] = $this->_storageStateService->getId($storageState);
        $row['upnId'] = $this->_storageStateService->getUpnId($storageState);
        $row['naryadNumber'] = $row['upnId'];
        $row['article'] = $this->_storageStateService->getArticle($storageState);
        $row['literal'] = $this->_storageStateService->getLiteral($storageState);

        return (object)$row;
    }

    /**
     * Функция оборачивает в дата провайдер некий массив
     *
     * @param $data
     * @param array $sort
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    private function _asProvider($data, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}