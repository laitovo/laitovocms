<?php

namespace backend\modules\logistics\modules\naryadTransferMonitor\models;

use yii\data\ArrayDataProvider;

class NaryadsOnDispatcherDtoProvider
{
    private $_naryadService;

    public function __construct()
    {
        $this->_naryadService = new NaryadService();
    }

    public function getNaryadsOnDispatcherAsDataProvider()
    {
        return $this->_asProvider($this->getNaryadsOnDispatcher());
    }

    public function getNaryadsOnDispatcher()
    {
        $naryads = $this->_findNaryads();
        $sortedNaryads = $this->_sortByReadyDate($naryads, 'DESC');
        $dtoNaryads = $this->_prepareNaryads($sortedNaryads);

        return $dtoNaryads;
    }

    private function _findNaryads()
    {
        return $this->_naryadService->getNaryadsOnDispatcher();
    }

    private function _prepareNaryads($naryads)
    {
        $dtoNaryads = [];
        foreach ($naryads as $naryad) {
            $dtoNaryads[] = $this->_createDTO($naryad);
        }

        return $dtoNaryads;
    }

    private function _createDTO($naryad)
    {
        if (!$naryad) {
            return null;
        }
        $row['id'] = $this->_naryadService->getId($naryad);
        $row['naryadNumber'] = $this->_naryadService->getNumber($naryad);
        $row['article'] = $this->_naryadService->getArticle($naryad);
        $row['readyDate'] = $this->_naryadService->getReadyDate($naryad);

        return (object)$row;
    }

    private function _sortByReadyDate($naryads, $ascOrDesc = 'ASC')
    {
        $ascOrDesc = strtoupper($ascOrDesc);
        if (!in_array($ascOrDesc, ['ASC', 'DESC'])) {
            $ascOrDesc = 'ASC';
        }
        uasort($naryads, function ($naryad1, $naryad2) use ($ascOrDesc) {
            $readyDate1 = $this->_naryadService->getReadyDate($naryad1);
            $readyDate2 = $this->_naryadService->getReadyDate($naryad2);
            switch (true) {
                case $readyDate1 > $readyDate2:
                    return $ascOrDesc == 'ASC' ? 1 : -1;
                case $readyDate1 < $readyDate2:
                    return $ascOrDesc == 'ASC' ? -1 : 1;
                default:
                    return 0;
            }
        });

        return $naryads;
    }

    /**
     * Функция оборачивает в дата провайдер некий массив
     *
     * @param $data
     * @param array $sort
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    private function _asProvider($data, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}