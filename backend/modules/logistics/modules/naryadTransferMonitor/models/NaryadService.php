<?php

namespace backend\modules\logistics\modules\naryadTransferMonitor\models;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpTerminal;
use backend\modules\logistics\models\Naryad;
use Yii;
use backend\modules\laitovo\models\ErpNaryad;
use yii\helpers\ArrayHelper;

class NaryadService
{
    public function getNaryadsOnDispatcher()
    {
        return ErpNaryad::find()
            ->where(['status' => ErpNaryad::STATUS_READY, 'distributed' => false])
            ->andWhere(['NOT', ['ready_date' => null]])
            ->andWhere(['NOT', ['logist_id' => null]])
            ->limit(100)
            ->all();
    }

    public function getUpnByBarcode($barcode)
    {
        return Naryad::find()->where(['barcode' => BarcodeHelper::toLatin($barcode)])->one();
    }

    public function getNaryadByUpnId($logistId)
    {
        return ErpNaryad::find()->where(['logist_id' => $logistId])->one();
    }

    public function getId($naryad)
    {
        return $naryad->id;
    }

    public function getIds($naryads)
    {
        return ArrayHelper::map($naryads,'id','id');
    }

    public function getNumber($naryad)
    {
        return $naryad->name;
    }

    public function getArticle($naryad)
    {
        return $naryad->article;
    }

    public function getReadyDate($naryad)
    {
        return $naryad->ready_date;
    }

    public function getNaryadInfo($naryad)
    {
        return $naryad->name;
    }

    public function moveNaryadToDispatcher($naryads,$userId)
    {

        $model = new ErpTerminal();
        $model->user_id = $userId;
        $model->location_id = Yii::$app->params['erp_otk'];

        /**
         * Костыль на комбинезоны
         */
        if ($model->user && $model->user->location_id == 18) {
            $model->location_id = 18;
        }

        /**
         * Костыль для автофильтра
         */
        if ($model->user && $model->user->location_id == Yii::$app->params['erp_shveika']) {
            $model->location_id = Yii::$app->params['erp_shveika'];
        }

        //Конец костыля
        if (!$model->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        foreach ($naryads as $naryad) {
            if (!$naryad->endPlace($userId)) {
                $transaction->rollBack();
                return false;
            }
        }
        $transaction->commit();
        return true;
    }

    public function isNaryadOnUser($naryads, $userId)
    {
        foreach ($naryads as $naryad) {
            if (!($naryad->status == ErpNaryad::STATUS_IN_WORK
                && $naryad->user_id == $userId
                && ($naryad->location_id == Yii::$app->params['erp_otk'] || $naryad->location_id == 18
                    || ($naryad->location_id == Yii::$app->params['erp_shveika'] && preg_match('/OT-2079/', $naryad->article) === 1 )  ) ))
                return false;
        }
        return true;
    }

    public function isNaryadOnInstanceUser($naryads, $userId)
    {
        foreach ($naryads as $naryad) {
            if (!($naryad->status == ErpNaryad::STATUS_IN_WORK
                && $naryad->user_id == $userId
                && ($naryad->location_id == Yii::$app->params['erp_otk'] || $naryad->location_id == 18
                    || ($naryad->location_id == Yii::$app->params['erp_shveika'] && preg_match('/OT-2079/', $naryad->article) === 1 )  ) ))
                return false;
        }
        return true;
    }

    public function isNaryadOnDispatcher($naryads)
    {
        foreach ($naryads as $naryad) {
            if (!($naryad->status == ErpNaryad::STATUS_READY
                && $naryad->ready_date
                && !$naryad->distributed))
                return false;
        }
        return true;

    }
}