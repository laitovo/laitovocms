<?php

namespace backend\modules\logistics\modules\logisticsMonitor\models;

use common\models\logistics\Order;
use yii\data\ArrayDataProvider;
use Yii;

class Index
{
    private $_orderService;
    private $_orderEntryService;
    private $_upnService;
    private $_shipmentManager;

    private $_countNotHandled = 0;
    private $_countHandled = 0;

    public function __construct()
    {
        $this->_orderService      = new OrderService();
        $this->_orderEntryService = new OrderEntryService();
        $this->_upnService        = new UpnService();
        $this->_shipmentManager   = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function getProvider($params)
    {
        $storage = $params['storage']??0;
        $dateTo = $storage == 2 ? $params['dateTo']??null : null;
        if ($dateTo) {
            $dateTo = strtotime($dateTo);
            $dateTo = mktime( 23,59,59, date("n",$dateTo),date('j', $dateTo), date("Y",$dateTo));
        }

        $models = $this->_findOrders($storage,$dateTo);
        $models = $this->_prepareOrders($models);
//        $models = $this->_sortOrders($models);
        $models = $this->_groupOrders($models);
        $models = $this->_sortShipments($models);
        if ($search = $params['search']) {
            $models = $this->_search($search, $models);
        }

        return $this->_asProvider($models);
    }

    public function getCountNotHandled()
    {
        return $this->_countNotHandled;
    }

    public function getCountHandled()
    {
        return $this->_countHandled;
    }

    private function _findOrders($storage,$dateTo)
    {
        return $this->_orderService->findOrdersForLogist($storage,$dateTo);
    }

    private function _prepareOrders($orders)
    {
        $data = [];
        foreach ($orders as $order) {
            $data[] = $this->_prepareOrder($order);
        }

        return $data;
    }

    private function _prepareOrderEntries($orderEntries)
    {
        $data = [];
        foreach ($orderEntries as $orderEntry) {
            $data[] = $this->_prepareOrderEntry($orderEntry);
        }

        return $data;
    }

    /**
     * @param Order $order
     * @return array
     */
    private function _prepareOrder($order)
    {
        $row = [];

        $orderId                    = $this->_orderService->getId($order);
        $manager                    = $this->_orderService->getManager($order);
        $delivery                   = $this->_orderService->getDelivery($order);
        $address                    = $this->_orderService->getAddress($order);
        $createdAt                  = $this->_orderService->getCreatedAt($order);
        $clientName                 = $this->_orderService->getClientName($order);
        $clientPhone                = $this->_orderService->getClientPhone($order);
        $clientEmail                = $this->_orderService->getClientEmail($order);
        $clientCategory             = $this->_orderService->getClientCategory($order);
        $orderSourceId              = $this->_orderService->getSourceNumber($order);
        $isNeedTtn                  = $this->_orderService->isNeedTtn($order);
        $isCOD                      = $this->_orderService->isCOD($order);
        $shipment                   = $this->_orderService->getShipment($order);
        $elements                   = $this->_prepareOrderEntries($this->_orderService->getOrderEntriesNotDeleted($order));
        $areAllPositionsReadyToPack = $this->_areAllPositionsReadyToPack($elements);

        $exportClientName           = $this->_orderService->getExportClientName($order);
        $exportCategory             = $this->_orderService->getExportClientCategory($order);

        $row['orderId']                    = $orderId;
        $row['manager']                    = $manager;
        $row['delivery']                   = $delivery;
        $row['address']                    = $address;
        $row['createdAt']                  = $createdAt;
        $row['shipmentDate']              = $order->shipment_date;
        $row['client']                     = $clientName;
        $row['phone']                      = $clientPhone;
        $row['email']                      = $clientEmail;
        $row['category']                   = $clientCategory;
        $row['sourceId']                   = $orderSourceId;
        $row['isNeedTtn']                  = $isNeedTtn;
        $row['isCOD']                      = $isCOD;
        $row['shipment']                   = $shipment;
        $row['elements']                   = $elements;
        $row['areAllPositionsReadyToPack'] = $areAllPositionsReadyToPack;
        $row['exportClientName']           = $exportClientName;
        $row['exportCategory']             = $exportCategory;

        return $row;
    }

    private function _prepareOrderEntry($orderEntry)
    {
        $row = [];

        $article     = $this->_orderEntryService->getArticle($orderEntry);
        $title       = $this->_orderEntryService->getTitle($orderEntry);
        $car         = $this->_orderEntryService->getCar($orderEntry);
        $shipmentId  = $this->_orderEntryService->getShipmentId($orderEntry);
        $upn         = $this->_orderEntryService->getReservedUpn($orderEntry);
        $upnId       = !$upn ? null : $this->_upnService->getId($upn);
        $location    = !$upn ? null : $this->_upnService->getLocation($upn);
        $readyToPack = !$upn ? null : $this->_upnService->isReadyToPackage($upn);

        $row['article']      = $article;
        $row['title']        = $title;
        $row['car']          = $car;
        $row['shipmentId']   = $shipmentId;
        $row['upn']          = $upnId;
        $row['location']     = $location;
        $row['readyToPack']  = $readyToPack;

        return $row;
    }

    /**
     * Сортировка заказов.
     * Cначала спускаем вниз обработанные.
     * Затем сортируем по степени готовности к упаковке на складе.
     *
     * @param array $orders
     * @return array
     */
    private function _sortOrders($orders)
    {
        uasort($orders, function ($a, $b) {
            $shipment1 = $a['shipment'];
            $shipment2 = $b['shipment'];
            $isAfterHandling1 = $shipment1 && !$this->_shipmentManager->isBeforeHandling($shipment1);
            $isAfterHandling2 = $shipment2 && !$this->_shipmentManager->isBeforeHandling($shipment2);
            $count1 = $this->_getReadyToPackElementsCount($a);
            $count2 = $this->_getReadyToPackElementsCount($b);
            $count1no = count($a['elements']) - $count1;
            $count2no = count($b['elements']) - $count2;
            switch (true) {
                case !$isAfterHandling1 && $isAfterHandling2:
                    return -1;
                case $isAfterHandling1 && !$isAfterHandling2:
                    return 1;
                case !$count1 && !$count2:
                    return 0;
                case $count1 && !$count2:
                    return -1;
                case !$count1 && $count2:
                    return 1;
                case $count1no < $count2no:
                    return -1;
                case $count1no > $count2no:
                    return 1;
                default:
                    return 0;
            }
        });

        return $orders;
    }


    /**
     * Сортировка заказов.
     * Cначала спускаем вниз обработанные.
     * Затем сортируем по степени готовности к упаковке на складе.
     *
     * @param array $shipments
     * @return array
     */
    private function _sortShipments($shipments)
    {
        ksort($shipments);
//        uasort($shipments, function ($a, $b) {
//            $isAfterHandling1 = $a['isAfterHandling'];
//            $isAfterHandling2 = $b['isAfterHandling'];
//            $count1 = $this->_getReadyToPackElementsCountForShipment($a['orders']);
//            $count2 = $this->_getReadyToPackElementsCountForShipment($b['orders']);
//            $count1no = $a['elementsCount'] - $count1;
//            $count2no = $b['elementsCount'] - $count2;
//            switch (true) {
//                case !$isAfterHandling1 && $isAfterHandling2:
//                    return -1;
//                case $isAfterHandling1 && !$isAfterHandling2:
//                    return 1;
//                case !$count1 && !$count2:
//                    return 0;
//                case $count1 && !$count2:
//                    return -1;
//                case !$count1 && $count2:
//                    return 1;
//                case $count1no < $count2no:
//                    return -1;
//                case $count1no > $count2no:
//                    return 1;
//                default:
//                    return 0;
//            }
//        });

        return $shipments;
    }

    /**
     * Группируем приказы по отгрузкам
     *
     * @param array $orders
     * @return array
     */
    private function _groupOrders($orders)
    {
        $result = [];
        $key = 0;
        foreach ($orders as $order) {
            $shipmentId = !$order['shipment'] ? null : $this->_shipmentManager->getId($order['shipment']);
            if (!$shipmentId) {
                $key++;
                $result['empty' . $key] = [
                    'shipmentId' => null,
                    'shipmentDate' => null,
                    'minOrderCreatedAt' => $order['createdAt'],
                    'shipmentComment' => null,
                    'elementsCount' => count($order['elements']),
                    'status'     => null,
                    'isAfterHandling'  => null,
                    'areAllPositionsReadyToPack'  => $order['areAllPositionsReadyToPack'],
                    'orders'     => [$order]
                ];
                continue;
            }
            if (!isset($result[$shipmentId])) {
                $shipment = $order['shipment'];
                $result[$shipmentId]['shipmentId'] = $shipmentId;
                $result[$shipmentId]['shipmentDate'] = $this->_shipmentManager->getShipmentDate($shipment);
                $result[$shipmentId]['shipmentComment'] = $this->_shipmentManager->getComment($shipment);
                $result[$shipmentId]['status'] = $this->_shipmentManager->getLogisticsStatusLabel($shipment);
                $result[$shipmentId]['isAfterHandling'] = !$this->_shipmentManager->isBeforeHandling($shipment);
                $result[$shipmentId]['areAllPositionsReadyToPack'] = true;
            }
            if (!$order['areAllPositionsReadyToPack']) {
                $result[$shipmentId]['areAllPositionsReadyToPack'] = false;
            }
            $result[$shipmentId]['orders'][] = $order;
            $result[$shipmentId]['elementsCount'] = ($result[$shipmentId]['elementsCount']??0) + count($order['elements']);
            $result[$shipmentId]['minOrderCreatedAt'] = min(($result[$shipmentId]['minOrderCreatedAt']??$order['createdAt']), $order['createdAt']);
        }

        $newResult = [];
        foreach ($result as $key => $item) {
            $sortKey = [];
            //Если обработанная заявка, она идет в низ
            if ($item['isAfterHandling']) {
                $sortKey[] = 2;
                $sortKey[] = $item['shipmentDate'];
                $sortKey[] = $item['minOrderCreatedAt'];
            } else {
                $sortKey[] = 1;
                $sortKey[] = $item['areAllPositionsReadyToPack'] ? 1 : 2;
                $sortKey[] = $item['minOrderCreatedAt'];
                $sortKey[] = 1 - ($item['elementsCount'] / 1000000);
            }
            $sortKey[] = $key;

            $newResult[implode('_', $sortKey)] = $item;

            if ($item['isAfterHandling']) {
                $this->_countHandled++;
                continue;
            }
            if ($item['areAllPositionsReadyToPack']) $this->_countNotHandled++;
        }

        return $newResult;
    }

    /**
     * @param string $search
     * @param array $models
     * @return array
     */
    private function _search($search, $models)
    {
        $searchPattern = '/' . preg_quote(mb_strtolower($search)) . '/';
        $result = [];
        foreach ($models as $shipmentId => $shipment) {
            $parts   = [];
            $parts[] = $shipment['shipmentId'] ?: '(не задано)';
            $parts[] = $shipment['status'] ?: '(не задано)';
            foreach ($shipment['orders'] as $order) {
                $parts[] = $order['manager'];
                $parts[] = $order['delivery'];
                $parts[] = $order['address'];
                $parts[] = date('d.m.Y', $order['createdAt']);
                $parts[] = $order['client'];
                $parts[] = $order['phone'];
                $parts[] = $order['email'];
                $parts[] = $order['category'];
                $parts[] = $order['sourceId'];
                $parts[] = $order['exportClientName'];
                $parts[] = $order['exportCategory'];
                if ($order['isNeedTtn']) {
                    $parts[] = '(С ТТН)';
                }
                if ($order['isCOD']) {
                    $parts[] = '(наложка)';
                }
            }
            if (!preg_match($searchPattern, mb_strtolower(implode('|', $parts)))) {
                continue;
            }
            $result[$shipmentId] = $shipment;
        }

        return $result;
    }

    private function _getReadyToPackElementsCount($orders)
    {
        $count = 0;
        foreach ($orders['elements'] as $element) {
            if ($element['readyToPack']) {
                $count++;
            }
        }

        return $count;
    }

    private function _getReadyToPackElementsCountForShipment($orders)
    {
        $count = 0;
        foreach ($orders as $order) {
            foreach ($order['elements'] as $element) {
                if ($element['readyToPack']) {
                    $count++;
                }
            }
        }

        return $count;
    }

    private function _areAllPositionsReadyToPack($elements)
    {
        foreach ($elements as $element) {
            if (!$element['readyToPack']) {
                return false;
            }
        }

        return true;
    }

    private function _asProvider($models, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $models,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}


