<?php

namespace backend\modules\logistics\modules\logisticsMonitor\models;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use core\services\SBox;
use Yii;
use yii\data\ArrayDataProvider;
use Exception;

class Box
{
    private $_session;
    private $_shipmentManager;
    private $_cancelShipment;
    private $_orderService;
    private $_idsYes = [];
    private $_idsNo = [];
    private $_savingError;

    public function __construct()
    {
        $this->_session         = Yii::$app->session;
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_cancelShipment  = Yii::$container->get('core\logic\CancelShipment');
        $this->_orderService    = new OrderService();
    }

    public function findShipment($shipmentId)
    {
        return $this->_shipmentManager->findById($shipmentId);
    }

    public function getBoxes($shipmentId)
    {
        $boxes = [];
        $result = [];
        $shipment = $this->findShipment($shipmentId);
        $items = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        foreach ($items as $item) {
            $key = $item->boxId ?? 0;
            $boxes[$key][] = $item;
        }
        ksort($boxes);
        $boxesIds = array_keys($boxes);
        $statuses = \core\models\box\Box::find()->select('status')->indexBy('id')->column();
        foreach ($boxes as $key => $items) {
            $result[$key]['items'] = $this->_getOrdersProviderByOrderEntriesNew($items);
            $result[$key]['status'] = isset($statuses[$key]) ? ($statuses[$key]== \core\models\box\Box::STATUS_NOT_HANDLED ? false : true) : false;
        }
        return $result;
    }

    public function getBoxList($shipmentId)
    {
        $boxes = [];
        $shipment = $this->findShipment($shipmentId);
        $items = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        foreach ($items as $item) {
            $key = $item->boxId ?? 0;
            $boxes[] = $key;
        }
        ksort($boxes);
        return array_unique($boxes);
    }


    /**
     * @param $shipmentId
     * @throws \yii\db\Exception
     */
    public function boxed($shipmentId)
    {
        $shipment = $this->findShipment($shipmentId);
        $elements = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        if (!SBox::boxed($elements)) {
            throw new \DomainException('Не удалось распределить по коробкам заказ согласно алгоритму. Что то пошло не так.');
        };
    }


    /**
     * @param $itemsIds
     * @param $box
     * @throws \yii\db\Exception
     */
    public function changeBox($itemsIds,$box)
    {
        if (!is_array($itemsIds))
            throw new \DomainException('Необходимо передать массив значений.');

        $positions = OrderEntry::find()->where(['in','id',$itemsIds])->all();

        if (!$positions)
            throw new \DomainException('Не найдено ни одной позиции, с которой необходимо произвести действие');

        $transaction = Yii::$app->db->beginTransaction();

        foreach ($positions as $position) {
            /**
             * @var $position OrderEntry
             */
            $position->boxId = $box != 0 ? $box : null;
            if (!$position->save()) {
                $transaction->rollBack();
                throw new \DomainException('Не удалось изменить коробку у позиции заказа');
            };
        }

        $transaction->commit();

        SBox::pack($box);
    }

    /**
     * @param $boxId
     * @throws \yii\db\Exception
     */
    public function setBoxReady($boxId)
    {
        if(!SBox::setReadyToPack([$boxId]))
            throw new \DomainException('Не удалось сделать коробку готовой к упаковке');
    }

    /**
     * Объединяем в коробку
     *
     * @param $itemsIds
     * @throws \yii\db\Exception
     */
    public function addToBox($itemsIds)
    {
        if (!is_array($itemsIds))
            throw new \DomainException('Необходимо передать массив значений.');

        $positions = OrderEntry::find()->where(['in','id',$itemsIds])->all();

        if (!$positions)
            throw new \DomainException('Не найдено ни одной позиции, с которой необходимо произвести действие');

        if (!SBox::mergeInBox($positions))
            throw new \DomainException('Не возможно объединить в коробку позиции.');
    }

    public function getProviderYes($shipmentId)
    {
        $this->_readBindings($shipmentId);
        $orderEntries = OrderEntry::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['or',
                ['and',
                    ['shipment_id' => $shipmentId],
                    ['not', ['shipment_id' => null]]
                ],
                ['in', 'id', $this->_idsYes]
            ],
            ['not in', 'id', $this->_idsNo]
        ])->all();

        return $this->_getOrdersProviderByOrderEntries($orderEntries);
    }

    public function getProviderNo($shipmentId, $search = null)
    {
        $this->_readBindings($shipmentId);
        $orderIds = $this->_findOrderIdsCanBeInShipment($shipmentId);
        $shipmentIds = $this->_findShipmentIdsCanBeMerged($shipmentId);
        $orderEntries = OrderEntry::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['in', 'order_id', $orderIds],
            [
                'or',
                ['shipment_id' => null],
                ['in', 'shipment_id', $shipmentIds],
                ['in', 'id', $this->_idsNo]
            ],
            ['not in', 'id', $this->_idsYes]
        ])->all();

        return $this->_getOrdersProviderByOrderEntries($orderEntries, $search);
    }

    public function getProviderNoNew($shipmentId)
    {
        $this->_readBindings($shipmentId);
        $orderEntries = OrderEntry::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['in', 'id', $this->_idsNo]
        ])->all();

        return $this->_getOrdersProviderByOrderEntries($orderEntries);
    }

    public function addBindings($params)
    {
        $params = $this->_prepareParams($params);
        $shipmentId = $params['shipmentId'];
        if (empty($params['idsYesNew'])) {
            $this->_clearBindings($shipmentId);
        }
        $this->_readBindings($shipmentId);
        $this->_updateBindings($params['idsYesNew'], $params['idsNoNew']);
        $this->_writeBindings($shipmentId);
    }

    public function removeBindings($params)
    {
        $params = $this->_prepareParams($params);
        $shipmentId = $params['shipmentId'];
        if (empty($params['idsNoNew'])) {
            $this->_clearBindings($shipmentId);
        }
        $this->_readBindings($shipmentId);
        $this->_updateBindings($params['idsYesNew'], $params['idsNoNew']);
        $this->_writeBindings($shipmentId);
    }

    public function saveBindings($params)
    {
        $params = $this->_prepareParams($params);
        $shipmentId = $params['shipmentId'];
        $this->_readBindings($shipmentId);
        if (!$this->_canSaveBindings($shipmentId, $this->_idsYes, $this->_idsNo)) {
            $this->_savingError = Yii::t('app', 'Данные некорректны. Попробуйте обновить страницу и повторить попытку.');
            return false;
        }
        $ids = $this->_getOrderEntryIds($shipmentId);
        $ids = $this->_updateOrderEntryIds($ids, $this->_idsYes, $this->_idsNo);
        if (!count($ids)) {
            $this->_savingError = Yii::t('app', 'Приказ на отгрузку должен содержать хотя бы одну позицию');
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->_cancelMergedShipments($this->_idsYes)) {
            $transaction->rollBack();
            $this->_savingError = Yii::t('app', 'Изменения не сохранены. Возникла ошибка при отмене отгрузки.');
            return false;
        }
        if (!$this->_saveOrderEntryIds($shipmentId, $ids)) {
            $transaction->rollBack();
            $this->_savingError = Yii::t('app', 'Изменения не сохранены. Возникла ошибка сохранении позиций отгрузки.');
            return false;
        }
        $transaction->commit();

        return true;
    }

    public function getSavingError()
    {
        return $this->_savingError;
    }

    private function _canSaveBindings($shipmentId, $idsYes, $idsNo)
    {
        $shipment = $this->findShipment($shipmentId);
        if (!$shipment || !$this->_shipmentManager->isBeforeHandling($shipment)) {
            return false;
        }
        $shipmentIds = $this->_findShipmentIdsCanBeMerged($shipmentId);
        foreach ($idsYes as $idYes) {
            $orderEntry = OrderEntry::findOne($idYes);
            if ($orderEntry->is_deleted) {
                return false;
            }
            if ($orderEntry->shipment_id && !in_array($orderEntry->shipment_id, $shipmentIds)) {
                return false;
            }
        }
        foreach ($idsNo as $idNo) {
            $orderEntry = OrderEntry::findOne($idNo);
            if ($orderEntry->is_deleted) {
                return false;
            }
            if ($orderEntry->shipment_id != $shipmentId) {
                return false;
            }
        }

        return true;
    }

    private function _cancelMergedShipments($orderEntryIds)
    {
        foreach ($orderEntryIds as $orderEntryId) {
            $orderEntry = OrderEntry::findOne($orderEntryId);
            $shipment = $this->findShipment($orderEntry->shipment_id);
            if (!$shipment) {
                continue;
            }
            if (!$this->_cancelShipment->cancel($shipment)) {
                return false;
            }
        }

        return true;
    }

    private function _readBindings($shipmentId)
    {
        $this->_idsYes = $this->_session->get('logisticsMonitor_bind_' . $shipmentId . '_idsYes', []);
        $this->_idsNo  = $this->_session->get('logisticsMonitor_bind_' . $shipmentId . '_idsNo', []);
    }

    private function _clearBindings($shipmentId)
    {
        $this->_session->remove('logisticsMonitor_bind_' . $shipmentId . '_idsYes');
        $this->_session->remove('logisticsMonitor_bind_' . $shipmentId . '_idsNo');
        $this->_idsYes = [];
        $this->_idsNo  = [];
    }

    private function _updateBindings($idsYesNew, $idsNoNew)
    {
        $this->_idsYes = array_diff(array_merge($this->_idsYes, $idsYesNew), $idsNoNew);
        $this->_idsNo  = array_diff(array_merge($this->_idsNo, $idsNoNew), $idsYesNew);
    }

    private function _writeBindings($shipmentId)
    {
        $this->_session->set('logisticsMonitor_bind_' . $shipmentId . '_idsYes', $this->_idsYes);
        $this->_session->set('logisticsMonitor_bind_' . $shipmentId . '_idsNo', $this->_idsNo);
    }
    
    /**
     * Поиск заказов, которые могут быть включены в отгрузку.
     * Заказ должен соответствовать отгрузке по обязательности товарно-транспортной накладной
     * (либо и там, и там без ТТН, либо и там, и там с ТТН).
     * Заказы, по которым все позиции отгружены, не могут быть включены в отгрузку.
     *
     * @param int $shipmentId
     * @return array
     */
    private function _findOrderIdsCanBeInShipment($shipmentId)
    {
        $isNeedTtn = $this->_isNeedTtn($shipmentId);
//        $orders = Order::find()->where(['and',
//            ['processed' => true],
//            ['shipped' => null],
//            ['is_need_ttn' => $isNeedTtn]
//        ])->all();
        $orders = $this->_orderService->findOrdersForBind($isNeedTtn);
        $result = [];
        foreach ($orders as $order) {
            foreach ($order->notDeletedOrderEntries as $orderEntry) {
                $shipment = $this->_shipmentManager->findById($orderEntry->shipment_id);
                if (!$shipment || !$this->_shipmentManager->getAccountedIn1C($shipment)) {
                    $result[] = $order->id;
                    continue 2;
                }
            }
        }

        return $result;
    }

    /**
     * Поиск отгрузок, которые могут быть объединены с текущей.
     * Объединены могут быть все необработанные отгрузки.
     *
     * @param int $shipmentId
     * @return array
     */
    private function _findShipmentIdsCanBeMerged($shipmentId)
    {
        $shipments = $this->_shipmentManager->findShipmentsNotHandled();
        $shipmentIds = [];
        foreach ($shipments as $shipment) {
            $shipmentIds[] = $this->_shipmentManager->getId($shipment);
        }

        return $shipmentIds;
    }

    /**
     * Определяем isNeedTtn для отгрузки по первой попавшейся позиции
     *
     * @param $shipmentId
     * @return mixed
     */
    private function _isNeedTtn($shipmentId)
    {
        $orderEntry = OrderEntry::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['shipment_id' => $shipmentId]
        ])->one();

        return $orderEntry->order->is_need_ttn ?? false;
    }

    private function _getOrderEntryIds($shipmentId)
    {
        $orderEntryIds = [];
        if ($shipmentId) {
            $orderEntries = OrderEntry::find()->where(['and',
                ['or',
                    ['is_deleted' => false],
                    ['is_deleted' => null]
                ],
                ['shipment_id' => $shipmentId]
            ])->all();
            foreach ($orderEntries as $orderEntry) {
                $orderEntryIds[] = $orderEntry->id;
            }
        }

        return $orderEntryIds;
    }

    private function _updateOrderEntryIds($orderEntryIds, $idsYes, $idsNo)
    {
        $orderEntryIds = array_merge($orderEntryIds, $idsYes);
        $orderEntryIds = array_diff($orderEntryIds, $idsNo);

        return array_unique($orderEntryIds);
    }

    private function _saveOrderEntryIds($shipmentId, $orderEntryIds)
    {
        $orderEntries = OrderEntry::find()->where(['and',
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ],
            ['shipment_id' => $shipmentId],
            ['not in', 'id', $orderEntryIds],
        ])->all();
        foreach ($orderEntries as $orderEntry) {
            $orderEntry->shipment_id = null;
            if (!$orderEntry->save()) {
                return false;
            }
        }
        foreach ($orderEntryIds as $orderEntryId) {
            $orderEntry = OrderEntry::findOne($orderEntryId);
            $orderEntry->shipment_id = $shipmentId;
            if (!$orderEntry->save()) {
                return false;
            }
        }

        return true;
    }

    private function _prepareParams($params)
    {
        if (empty($params['idsYes'])) {
            $params['idsYes'] = [];
        }
        if (empty($params['idsNo'])) {
            $params['idsNo'] = [];
        }
        if (empty($params['idsYesOld'])) {
            $params['idsYesOld'] = [];
        }
        if (empty($params['idsNoOld'])) {
            $params['idsNoOld'] = [];
        }
        if (empty($params['idsYesNew'])) {
            $params['idsYesNew'] = [];
        }
        if (empty($params['idsNoNew'])) {
            $params['idsNoNew'] = [];
        }
        if (empty($params['shipmentId'])) {
            $params['shipmentId'] = null;
        }

        return $params;
    }

    private function _getOrdersProviderByOrderEntries($orderEntries, $search = null)
    {
        $orders = [];
        foreach ($orderEntries as $orderEntry) {
            if (!isset($orders[$orderEntry->order_id])) {
                $order = $orderEntry->order;
                $orders[$orderEntry->order_id]['id']           = $order->id;
                $orders[$orderEntry->order_id]['sourceId']     = $order->source_innumber;
                $orders[$orderEntry->order_id]['manager']      = $order->manager;
                $orders[$orderEntry->order_id]['delivery']     = $order->delivery;
                $orders[$orderEntry->order_id]['address']      = $order->address;
                $orders[$orderEntry->order_id]['client']       = $order->username;
                $orders[$orderEntry->order_id]['phone']        = $order->userphone;
                $orders[$orderEntry->order_id]['email']        = $order->useremail;
                $orders[$orderEntry->order_id]['category']     = $order->category;
                $orders[$orderEntry->order_id]['createdAt']    = $order->created_at;
                $orders[$orderEntry->order_id]['isNeedTtn']    = $order->is_need_ttn;
                $orders[$orderEntry->order_id]['isCOD']        = $order->isCOD;
                $orders[$orderEntry->order_id]['exportClientName'] = $order->export_username;
                $orders[$orderEntry->order_id]['exportCategory']   = $order->export_type;
                $orders[$orderEntry->order_id]['orderEntries'] = [];
            }
            $orders[$orderEntry->order_id]['orderEntries'][] = $orderEntry;
        }
        if ($search) {
            $orders = $this->_search($search, $orders);
        }

        return $this->_asProvider($orders, ['sourceId']);
    }

    private function _getOrdersProviderByOrderEntriesNew($orderEntries, $search = null)
    {
        $orders = [];
        foreach ($orderEntries as $orderEntry) {
            if (!isset($orders[$orderEntry->order_id])) {
                $order = $orderEntry->order;
                $orders[$orderEntry->order_id]['id']           = $order->id;
                $orders[$orderEntry->order_id]['sourceId']     = $order->source_innumber;
                $orders[$orderEntry->order_id]['manager']      = $order->manager;
                $orders[$orderEntry->order_id]['delivery']     = $order->delivery;
                $orders[$orderEntry->order_id]['address']      = $order->address;
                $orders[$orderEntry->order_id]['client']       = $order->username;
                $orders[$orderEntry->order_id]['phone']        = $order->userphone;
                $orders[$orderEntry->order_id]['email']        = $order->useremail;
                $orders[$orderEntry->order_id]['category']     = $order->category;
                $orders[$orderEntry->order_id]['createdAt']    = $order->created_at;
                $orders[$orderEntry->order_id]['isNeedTtn']    = $order->is_need_ttn;
                $orders[$orderEntry->order_id]['isCOD']        = $order->isCOD;
                $orders[$orderEntry->order_id]['exportClientName'] = $order->export_username;
                $orders[$orderEntry->order_id]['exportCategory']   = $order->export_type;
                $orders[$orderEntry->order_id]['orderEntries'] = [];
            }
            $orders[$orderEntry->order_id]['orderEntries'][$orderEntry->setIndex ?: 0][] = $orderEntry;
        }

        return $this->_asProvider($orders, ['sourceId']);
    }

    /**
     * @param string $search
     * @param array $models
     * @return array
     */
    private function _search($search, $models)
    {
        $searchPattern = '/' . preg_quote(mb_strtolower($search)) . '/';
        $result = [];
        foreach ($models as $orderId => $order) {
            $parts   = [];
            $parts[] = $order['manager'];
            $parts[] = $order['delivery'];
            $parts[] = $order['address'];
            $parts[] = date('d.m.Y', $order['createdAt']);
            $parts[] = $order['client'];
            $parts[] = $order['phone'];
            $parts[] = $order['email'];
            $parts[] = $order['category'];
            $parts[] = $order['sourceId'];
            if ($order['isNeedTtn']) {
                $parts[] = '(С ТТН)';
            }
            if ($order['isCOD']) {
                $parts[] = '(наложка)';
            }
            if (!preg_match($searchPattern, mb_strtolower(implode('|', $parts)))) {
                continue;
            }
            $result[$orderId] = $order;
        }

        return $result;
    }

    private function _asProvider($models, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $models,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}


