<?php

namespace backend\modules\logistics\modules\logisticsMonitor\models;

use yii\data\ArrayDataProvider;
use Yii;

class ViewOrder
{
    private $_orderService;
    private $_orderEntryService;

    public function __construct()
    {
        $this->_orderService      = new OrderService();
        $this->_orderEntryService = new OrderEntryService();
    }

    public function findOrder($orderId)
    {
        $order = $this->_findOrder($orderId);
        $order = $this->_prepareOrder($order);

        return $order;
    }

    public function getOrderEntriesProvider($orderId)
    {
        $order        = $this->_findOrder($orderId);
        $orderEntries = $this->_findOrderEntriesByOrder($order);
        $orderEntries = $this->_prepareOrderEntries($orderEntries);

        return $this->_asProvider($orderEntries, $this->getOrderEntriesAttributes());
    }

    public function getOrderEntriesAttributes()
    {
        return [
            'article',
            'name',
            'size',
            'price',
        ];
    }

    private function _findOrder($orderId)
    {
        return $this->_orderService->findById($orderId);
    }

    private function _prepareOrder($order)
    {
        $row['id']                     = $this->_orderService->getId($order);
        $row['number']                 = $this->_orderService->getSourceNumber($order);
        $row['isNeedTtn']              = $this->_orderService->isNeedTtn($order);
        $row['isCOD']                  = $this->_orderService->isCOD($order);
        $row['transport_company_name'] = $this->_orderService->getTransportCompanyName($order);
        $row['client_name']            = $this->_orderService->getClientName($order);
        $row['client_phone']           = $this->_orderService->getClientPhone($order);
        $row['client_address']         = $this->_orderService->getAddress($order);
        $row['inner_comment']          = $this->_orderService->getInnerComment($order);
        $row['payment_status']         = $this->_orderService->getPaymentStatus($order);
        $row['delivery_type']          = $this->_orderService->getDeliveryType($order);
        $row['delivery_status']        = $this->_orderService->getDeliveryStatus($order);
        $row['items_price']            = $this->_formatPrice($this->_orderService->getItemsPrice($order));
        $row['delivery_price']         = $this->_formatPrice($this->_orderService->getDeliveryPrice($order));
        $row['cod_total_price']        = $this->_formatPrice($this->_orderService->getCodTotalPrice($order));
        $row['cod_items_price']        = $this->_formatPrice($this->_orderService->getCodItemsPrice($order));
        $row['cod_delivery_price']     = $this->_formatPrice($this->_orderService->getCodDeliveryPrice($order));

        return $row;
    }

    private function _findOrderEntriesByOrder($order)
    {
        return $this->_orderService->getOrderEntries($order);
    }

    private function _prepareOrderEntries($orderEntries)
    {
        $data = [];
        foreach ($orderEntries as $orderEntry) {
            $data[] = $this->_prepareOrderEntry($orderEntry);
        }

        return $data;
    }

    private function _prepareOrderEntry($orderEntry)
    {
        $row['article']      = $this->_orderEntryService->getArticle($orderEntry);
        $row['name']         = $this->_orderEntryService->getName($orderEntry);
        $row['size']         = '50X50X50'; //test
        $row['price']        = $this->_formatPrice($this->_orderEntryService->getPrice($orderEntry));
        $row['isDeleted']    = $this->_orderEntryService->getIsDeleted($orderEntry);
        $row['deletionInfo'] = !$row['isDeleted'] ? null : $this->_getDeletionInfo($orderEntry);

        return $row;
    }

    private function _getDeletionInfo($orderEntry)
    {
        $deletedAt    = $this->_orderEntryService->getDeletedAt($orderEntry);
        $destroyer    = $this->_orderEntryService->getDestroyer($orderEntry);
        $deletionInfo = Yii::t('yii', 'Позиция удалена (' . date('d.m.Y', $deletedAt)) . ', ' . ($destroyer->name ?? null) . ')';

        return $deletionInfo;
    }

    private function _formatPrice($price)
    {
        return $price ? $price . ' руб' : 0;
    }

    private function _asProvider($data, $sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $data,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}


