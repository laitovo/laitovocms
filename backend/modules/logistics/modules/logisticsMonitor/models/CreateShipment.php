<?php

namespace backend\modules\logistics\modules\logisticsMonitor\models;

use backend\modules\logistics\models\Order;
use Yii;

class CreateShipment
{
    private $_orderService;
    private $_orderEntryService;
    private $_shipmentManager;
    private $_createShipmentError;

    public function __construct()
    {
        $this->_orderService      = new OrderService();
        $this->_orderEntryService = new OrderEntryService();
        $this->_shipmentManager   = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    /**
     * @param int $orderId
     * @return Order|null
     */
    public function findOrder($orderId)
    {
        return $this->_orderService->findById($orderId);
    }

    public function getShipmentId($shipment)
    {
        return $this->_shipmentManager->getId($shipment);
    }

    /**
     * Отгрузка для заказа определяется как первая попавшаяся отгрузка, содержащая позиции из данного заказа
     *
     * @param Order $order
     * @return object|null
     */
    public function findShipmentByOrder($order)
    {
        foreach ($this->_orderService->getOrderEntriesNotDeleted($order) as $orderEntry) {
            if ($shipment = $this->_orderEntryService->getShipment($orderEntry)) {
                return $shipment;
            }
        }

        return null;
    }

    /**
     * Создаём отгрузку, заполняем её позициями из заказа
     *
     * @param Order $order
     * @return object|false
     */
    public function createShipmentByOrder($order)
    {
        $this->_createShipmentError = null;
        $transaction = Yii::$app->db->beginTransaction();
        if (!($shipment = $this->_createShipment())) {
            $this->_createShipmentError = 'Не удалось создать приказ на отгрузку';
            return false;
        }
        $shipmentId = $this->_shipmentManager->getId($shipment);
        foreach ($this->_orderService->getOrderEntriesNotDeleted($order) as $orderEntry) {
            if (!$this->_orderEntryService->saveShipmentId($orderEntry, $shipmentId)) {
                $transaction->rollBack();
                $this->_createShipmentError = 'Не удалось добавить к приказу позицию №' . $this->_orderEntryService->getId($orderEntry);
                return false;
            }
        }
        $transaction->commit();

        return $shipment;
    }

    /**
     * @return string|null
     */
    public function getCreateShipmentError()
    {
        return $this->_createShipmentError;
    }

    private function _createShipment()
    {
        $shipment = $this->_shipmentManager->create();
        if (!$this->_shipmentManager->save($shipment)) {
            return false;
        }

        return $shipment;
    }
}


