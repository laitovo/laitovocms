<?php

namespace backend\modules\logistics\modules\logisticsMonitor\controllers;

use backend\modules\logistics\modules\logisticsMonitor\models\Bind;
use backend\modules\logistics\modules\logisticsMonitor\models\Box;
use backend\modules\logistics\modules\logisticsMonitor\models\CreateShipment;
use backend\modules\logistics\modules\logisticsMonitor\models\Index;
use backend\modules\logistics\modules\logisticsMonitor\models\ViewOrder;
use yii\web\Controller;
use yii\base\Module;
use yii\web\NotFoundHttpException;
use Yii;

class DefaultController extends Controller
{
    private $_index;
    private $_createShipment;
    private $_bind;
    private $_box;
    private $_viewOrder;

    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->_index          = new Index();
        $this->_createShipment = new CreateShipment();
        $this->_bind           = new Bind();
        $this->_box            = new Box();
        $this->_viewOrder      = new ViewOrder();

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        ini_set('memory_limit', '256M');

        $storageList[] = (object)[
            'name' => 'Клиентские заказы',
            'value' => 0
        ];

        $storageList[] = (object)[
            'name' => 'Склад Москва',
            'value' => 1
        ];

        $storageList[] = (object)[
            'name' => 'Hamburg',
            'value' => 2
        ];
        
        $storageList[] = (object)[
            'name' => 'Интернет-решения (OZON)',
            'value' => 250
        ];

        $dateTo      = $this->_updateSession('logisticsMonitor_index_dateTo', 'update_logisticsMonitor_index_dateTo');
        $search      = $this->_updateSession('logisticsMonitor_index_search', 'update_logisticsMonitor_index_search');
        $autoRefresh = $this->_updateSession('logisticsMonitor_index_autoRefresh', 'update_logisticsMonitor_index_autoRefresh', true);
        $currentStorage = $this->_updateSession('childMonitor_index_storage', 'update_childMonitor_storage',0);
        $provider    = $this->_index->getProvider([
            'search' => $search,
            'storage' => $currentStorage,
            'dateTo' => $dateTo,
        ]);

        return $this->render('index', [
            'provider'    => $provider,
            'countHandled'=> $this->_index->getCountHandled(),
            'countNotHandled'=> $this->_index->getCountNotHandled(),
            'search'      => $search,
            'autoRefresh' => $autoRefresh,
            'storageList' => $storageList,
            'currentStorage' => $currentStorage,
            'dateTo' => $dateTo ?: time(),
        ]);
    }

    public function actionViewOrder($orderId)
    {
        if (!($order = $this->_viewOrder->findOrder($orderId))) {
            throw new NotFoundHttpException('Заказ не найден');
        }

        return $this->render('view-order', [
            'order' => $order,
            'orderItemsProvider' => $this->_viewOrder->getOrderEntriesProvider($orderId),
        ]);
    }

    public function actionCreateShipment($orderId, $nextPage)
    {
        switch ($nextPage) {
            case 'fill':
                $next = 'fill-shipment';
                break;
            case 'box':
                $next = 'box-shipment';
                break;
            default:
                $next = 'view-shipment';
                break;
        }
        if (!($order = $this->_createShipment->findOrder($orderId))) {
            throw new NotFoundHttpException('Заказ не найден');
        }
        if ($shipment = $this->_createShipment->findShipmentByOrder($order)) {
            return $this->redirect([$next, 'shipmentId' => $this->_createShipment->getShipmentId($shipment)]);
        }
        if (!($shipment = $this->_createShipment->createShipmentByOrder($order))) {
            Yii::$app->session->setFlash('error', $this->_createShipment->getCreateShipmentError());
            return $this->redirect(['index']);
        } else {
            return $this->redirect([$next, 'shipmentId' => $this->_createShipment->getShipmentId($shipment)]);
        }
    }

    public function actionViewShipment($shipmentId)
    {
        return $this->redirect(['/logistics/shipment-monitor/default/view', 'id' => $shipmentId]);
    }

    public function actionFillShipment($shipmentId)
    {
        ini_set('memory_limit', '256M');
        if (!($shipment = $this->_bind->findShipment($shipmentId))) {
            throw new NotFoundHttpException('Отгрузка не найдена');
        }
        $dateTo = $this->_updateSession('logisticsMonitor_index_dateTo', 'update_logisticsMonitor_index_dateTo');
        $search = $this->_updateSession('logisticsMonitor_index_search', 'update_logisticsMonitor_index_search');
        $currentStorage = $this->_updateSession('childMonitor_index_storage', 'update_childMonitor_storage',0);
        if (!Yii::$app->request->isAjax) {
            $this->_bind->addBindings([
                'shipmentId' => $shipmentId,
                'idsYesNew'  => Yii::$app->request->post('idsYesNew'),
            ]);
        }

        return $this->render('fill-shipment/index', [
            'shipmentId'  => $shipmentId,
            'providerYes' => $this->_bind->getProviderYes($shipmentId),
            'providerNo'  => $this->_bind->getProviderNo($shipmentId, $currentStorage, $search,$dateTo),
            'search'      => $search
        ]);
    }

    /**
     * Действие, которое позволяет раскидать элементы по коробкам согласно алгоритму
     *
     * @param $shipmentId
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionBoxed($shipmentId)
    {
        if (!($shipment = $this->_box->findShipment($shipmentId))) {
            throw new NotFoundHttpException('Отгрузка не найдена');
        }
        try{
            $this->_box->boxed($shipmentId);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Распределены по коробке'));
            return $this->redirect(['/logistics/logistics-monitor/default/box-shipment', 'shipmentId' => $shipmentId]);
        }catch (\Exception $exception){
            Yii::$app->session->setFlash('danger', Yii::t('app', $exception->getMessage()));
            return $this->redirect(['/logistics/logistics-monitor/default/box-shipment', 'shipmentId' => $shipmentId]);
        }
    }

    public function actionBoxShipment($shipmentId)
    {
        if (!($shipment = $this->_box->findShipment($shipmentId))) {
            throw new NotFoundHttpException('Отгрузка не найдена');
        }
        $search = $this->_updateSession('logisticsMonitor_fillShipment_search', 'update_logisticsMonitor_fillShipment_search');
        if (!Yii::$app->request->isAjax) {
//            $ar = Yii::$app->request->post('idsYesNew');
            $delete = Yii::$app->request->post('deleteFromBox');
            if(!empty($delete)){
                Yii::$app->session->setFlash('success', Yii::t('app', 'DELETE: ' . implode(',',$delete)));
            }
            $ready = Yii::$app->request->post('readyBox');
            if(!empty($ready)) {
                try{
                    $this->_box->setBoxReady($ready);
                    Yii::$app->session->setFlash('success', Yii::t('app', 'READY: ' . $ready));
                }catch (\Exception $exception){
                    Yii::$app->session->setFlash('danger', Yii::t('app', $exception->getMessage()));
                }
                Yii::$app->session->setFlash('success', Yii::t('app', 'READY: ' . $ready));
            }
            $change = Yii::$app->request->post('changeBox');
            if(!empty($change)){
                $items = $change['items'] ?? [];
                $box   = $change['box']   ?? 0;
                try{
                    $this->_box->changeBox($items,$box);
                    Yii::$app->session->setFlash('success', Yii::t('app', 'CHANGE: ' . implode(',',($items) ) . ' BOX: ' . ($box) ) );
                }catch (\Exception $exception){
                    Yii::$app->session->setFlash('danger', Yii::t('app', $exception->getMessage()));
                }

            }
            $add = Yii::$app->request->post('addToBox');
            if(!empty($add)){
                try{
                    $this->_box->addToBox($add);
                    Yii::$app->session->setFlash('success', Yii::t('app', 'CHANGE: ' . implode(',',($items) ) . ' BOX: ' . ($box) ) );
                }catch (\Exception $exception){
                    Yii::$app->session->setFlash('success', Yii::t('app', $exception->getMessage()));
                }

            }
//            if (!empty($delete) || !empty($change) || !empty($add)) {
//                var_dump($delete,$change,$add);die;
//            }
//            $this->_box->addBindings([
//                'shipmentId' => $shipmentId,
//                'idsYesNew'  => Yii::$app->request->post('idsYesNew'),
//            ]);
        }

        return $this->render('box-shipment/index', [
            'shipmentId'  => $shipmentId,
            'boxes'       => $this->_box->getBoxes($shipmentId),
            'boxList'     => $this->_box->getBoxList($shipmentId),
            'search'      => $search
        ]);
    }



    public function actionReduceShipment($shipmentId)
    {
        if (!($shipment = $this->_bind->findShipment($shipmentId))) {
            throw new NotFoundHttpException('Отгрузка не найдена');
        }
        if (!Yii::$app->request->isAjax) {
            $this->_bind->removeBindings([
                'shipmentId'    => $shipmentId,
                'idsNoNew'      => Yii::$app->request->post('idsNoNew'),
            ]);
        }

        return $this->render('reduce-shipment/index', [
            'shipmentId'  => $shipmentId,
            'providerYes' => $this->_bind->getProviderYes($shipmentId),
            'providerNo'  => $this->_bind->getProviderNoNew($shipmentId),
        ]);
    }

    public function actionSaveBindings($shipmentId)
    {
        if (!($shipment = $this->_bind->findShipment($shipmentId))) {
            throw new NotFoundHttpException('Отгрузка не найдена');
        }
        if (!$this->_bind->saveBindings([
            'shipmentId' => $shipmentId,
        ])) {
            Yii::$app->session->setFlash('error', $this->_bind->getSavingError());
            return $this->redirect(['index']);
        }

        return $this->redirect(['/logistics/shipment-monitor/default/view', 'id' => $shipmentId]);
    }

    private function _updateSession($name, $pageFlag, $defaultValue = false)
    {
        $session      = Yii::$app->session;
        $remember     = Yii::$app->request->get($pageFlag) ?: Yii::$app->request->post($pageFlag);
        $sessionValue = !$remember ? $session->get($name, $defaultValue) : (Yii::$app->request->get($name, false) ?: Yii::$app->request->post($name, false));
        $session->set($name, $sessionValue);

        return $sessionValue;
    }
}