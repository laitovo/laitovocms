<?php

use yii\jui\DatePicker;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $provider yii\data\ArrayDataProvider */
/* @var $search string */
/* @var $autoRefresh bool */
/* @var $countNotHandled integer */
/* @var $countHandled integer */

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Монитор логистики');

$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    .checkbox-custom label::before {
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
</style>

<?php Pjax::begin(['id' => 'pjax-shipments-list','timeout' => 5000, 'enablePushState' => false]); ?>

<?php ActiveForm::begin([
    'id'      => 'search-form',
    'action'  => [''],
    'method'  => 'get',
    'options' => [
        'data-pjax' => true
    ]
]); ?>

<div class="row">
    <div class="form-group col-md-2">
        <?= Html::tag('div',
            Html::checkbox("logisticsMonitor_index_autoRefresh", $autoRefresh, ['id' => 'checkbox-auto-refresh'])
            .Html::tag('label','Автообновление', ['for' => 'checkbox-auto-refresh']),
            [
                'class' => 'checkbox-custom checkbox-primary text-left',
                'onchange' => '$("#search-form").submit();'
            ]
        ) ?>
        <?= Html::hiddenInput('update_logisticsMonitor_index_autoRefresh',true); ?>
        <?= Html::button(Yii::t('app', 'Обновить'), [
            'id' => 'button-refresh',
            'class' => 'btn btn-outline btn-round btn-primary',
            'onclick' => '$.pjax.reload({container : "#pjax-shipments-list"});'
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::input($type = 'text','logisticsMonitor_index_search',$value = $search, $options = ['class' => 'form-control', 'id' => 'search-textbox', 'placeholder' => 'Поиск']); ?>
        <?php if ($currentStorage == 2) :?>
            <br>
        <?= Html::button('Опт', ['class' => 'btn btn-xs btn-default', 'onclick' => '
            $("#search-textbox").val("Опт");
            $("#search-form").submit();
        ']) ?>
        <?= Html::button('Физик', ['class' => 'btn btn-xs btn-default', 'onclick' => '
            $("#search-textbox").val("Физик");
            $("#search-form").submit();
        ']) ?>
        <?= Html::button('Склад', ['class' => 'btn btn-xs btn-default', 'onclick' => '
            $("#search-textbox").val("Склад");
            $("#search-form").submit();
        ']) ?>
        <?php endif; ?>
        <?= Html::hiddenInput('update_logisticsMonitor_index_search',true); ?>
    </div>
    <div class="col-md-2">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::button('Очистить', ['class' => 'btn btn-sm btn-default', 'onclick' => '
            $("#search-textbox").val("");
            $("#search-form").submit();
        ']) ?>
    </div>
    <div class="col-md-6">
        <?= Html::hiddenInput('update_childMonitor_storage',true); ?>
        <?= Html::hiddenInput('childMonitor_index_storage',$currentStorage,['id' => 'storage-id']); ?>
        <?foreach ($storageList as $storage):?>
            <?= Html::button($storage->name, ['class' => 'btn btn-sm ' . ($storage->value == $currentStorage ? 'btn-success' : 'btn-default'), 'onclick' => '
                $("#storage-id").val(' . $storage->value .');
                $("#search-form").submit();
            ']) ?>
        <?endforeach;?>
    </div>
</div>
<?php if ($currentStorage == 2) :?>
<div class="row">
    <div class="col-md-2">
        <?=
            DatePicker::widget([
                'name' => 'logisticsMonitor_index_dateTo',
                'value' => $dateTo,
                'options' => ['class' => 'form-control property-field shipment-date','id' => 'hamburgDateTo','onchange' => '$("#search-form").submit();'],
                'dateFormat' => 'dd.MM.yyyy',
            ]);
        ?>
        <?= Html::hiddenInput('update_logisticsMonitor_index_dateTo',true); ?>
    </div>
    <div class="col-md-3">
        <?= Html::button('Сейчас', ['class' => 'btn btn-sm btn-info', 'onclick' => '
                $("#hamburgDateTo").val("");
                $("#search-form").submit();
            ']) ?>
    </div>
</div>
<?php endif; ?>

<?php ActiveForm::end(); ?>

<?php if (Yii::$app->user->getId() == 21) {

    echo "<hr />";
    echo "<h5>Готово и не обработано: $countNotHandled</h5>";
    echo "<h5>Обработано: $countHandled</h5>";
    echo "<hr />";
    }
?>

<?= ListView::widget([
    'dataProvider' => $provider,
    'itemView' => '_shipment',
]) ?>

<?php Pjax::end(); ?>

<?php Yii::$app->view->registerJs('
    setInterval(function() {
        if ($("#checkbox-auto-refresh").prop("checked")) {
            $.pjax.reload({container : "#pjax-shipments-list"});
        }
    }, 360*1000);

    $("#pjax-shipments-list").on("pjax:end", function() {
        $("#pjax-shipments-list [data-toggle=tooltip]").tooltip();
    })
', \yii\web\View::POS_END);
?>