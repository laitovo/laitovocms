<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var yii\web\View $this */
/* @var int|null $shipmentId */
/* @var $providerYes yii\data\ArrayDataProvider */
/* @var $providerNo yii\data\ArrayDataProvider */
/* @var string|null $search */

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Объединить');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Монитор логистики'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    .checkbox-custom label::before {
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
</style>

<?= Html::a('<i class="icon wb-check"></i> ' . Yii::t('app', 'Сохранить'), ['save-bindings', 'shipmentId' => $shipmentId], ['class' => 'btn btn-primary', 'id' => 'save-bindings-button']) ?>

<?php $form = ActiveForm::begin(['id' => 'bind-form']); ?>
    <div id="bind-form-content-block" class="hide"></div>
<?php ActiveForm::end(); ?>

<hr>
<div class="row">
    <div class="col-md-6">
        <h4>Позиции в приказе на отгрузку</h4>
        <? Pjax::begin(['id' => 'pjax-yes', 'enablePushState' => false]); ?>
            <?= ListView::widget([
                'dataProvider' => $providerYes,
                'itemView' => '_order-yes',
                'id' => 'table-yes',
                'options' => [
                    'style' => 'margin-top:109px;'
                ]
            ]) ?>
        <? Pjax::end(); ?>
    </div>
    <div class="col-md-6">
        <?= Html::button ('Добавить выбранные', [
            'class' => 'btn btn-success pull-right',
            'id' => 'add-bindings-button',
            'disabled' => true
        ] ) ?>
        <h4>Позиции, готовые к отгрузке</h4>
        <?php Pjax::begin(['id' => 'pjax-no', 'enablePushState' => false]); ?>
                <?php ActiveForm::begin([
                    'id'      => 'search-form',
                    'method'  => 'get',
                    'options' => [
                        'data-pjax' => true
                    ]
                ]); ?>

            <div class="row" style="margin-top:16px;margin-bottom:16px;">
                <div class="col-md-4">
                    <?= Html::input($type = 'text','logisticsMonitor_index_search',$value = $search, $options = ['class' => 'form-control', 'id' => 'search-textbox', 'placeholder' => 'Поиск']); ?>
                    <?= Html::hiddenInput('update_logisticsMonitor_index_search',true); ?>
                </div>
                <div class="col-md-5">
                    <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
                    <?= Html::button('Очистить', ['class' => 'btn btn-sm btn-default', 'onclick' => '
                        $("#search-textbox").val("");
                        $("#search-form").submit();
                    ']) ?>
                </div>
            </div>

            <?= Html::tag('div',
                Html::checkbox('selectAll', false, ['id' => 'checkbox-select-all-no']) .
                Html::tag('label', Yii::t('app', 'Выбрать все'), ['for' => 'checkbox-select-all-no']),
                ['class' => 'checkbox-custom checkbox-primary']
            );?>

            <?php ActiveForm::end(); ?>

            <?= ListView::widget([
                'dataProvider' => $providerNo,
                'itemView' => '_order-no',
                'id' => 'table-no',
            ]) ?>
        <? Pjax::end(); ?>
    </div>
</div>

<?php Yii::$app->view->registerJs('
    $("#add-bindings-button").click(function() {
        fillBindForm();
        $("#bind-form").submit();
    });
    $("#pjax-yes").on("pjax:end", function() {
        $("#pjax-yes [data-toggle=tooltip]").tooltip();
    });
    $("#pjax-no").on("pjax:end", function() {
        $("#pjax-no [data-toggle=tooltip]").tooltip();
        showBindButton();
    });
    $("body").on("change", "#table-no .checkbox-select-order",function(e){
        showBindButton();
    });
    $("body").on("change", "#checkbox-select-all-no",function(e){
       $("#table-no .checkbox-select-order").prop("checked", $(this).prop("checked")).change();
       showBindButton();
    });
    function showBindButton() {
        if ($("#table-no .checkbox-select-order:checked").length > 0) {
            $("#add-bindings-button").prop("disabled", false);
        } else {
            $("#add-bindings-button").prop("disabled", true);
        }
    }
    function fillBindForm() {
        $("#bind-form-content-block").html("");
        $("#table-no .checkbox-select-order-entry:checked").each(function(){
            $("#bind-form-content-block").append($(this).clone());
        });
    }
', \yii\web\View::POS_END);
?>
