<?php
use yii\helpers\Html;
/* var $model array */
?>

<hr>
<div class="col-md-3 col-sm-4 text-center">
    <table class="table">
        <thead>
        <th class="text-center">№ Заказа</th>
        </thead>
        <tbody>
        <tr>
            <td>
                <div>
                    <?= $model['sourceId'] ? str_replace((mb_substr($model['sourceId'], 4, mb_strlen($model['sourceId']))),
                        ('<b style="font-size: 1.4em;font-weight: bold; color: red;">' . mb_substr($model['sourceId'], 4, mb_strlen($model['sourceId'])) . '</b>'), $model['sourceId']) : $model['sourceId'] ?>
                    <br>
                    <span class="text-nowrap">от <?= date('d.m.Y',$model['createdAt']) ?></span>
                    <?php if ($model['isNeedTtn']): ?>
                        <strong class="text-nowrap" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Обязательно приложить ТТН к заказу')?>"><?=Yii::t('app', '(с ТТН)')?></strong>
                    <?php endif;?>
                    <?php if ($model['isCOD']): ?>
                        <strong class="text-nowrap text-danger" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Оплата заказа производится наложенным платежем')?>"><?=Yii::t('app', '(Наложка)')?></strong>
                    <?php endif;?>
                </div>
                <?= Html::a('<i class="fa fa-eye"></i>', ['view-order', 'orderId' => $model['id']], [
                    'target' => '_blank',
                    'class' => 'btn btn-sm btn-icon btn-outline btn-round btn-primary',
                    'style' => 'margin-top:3px;',
                    'data-pjax' => '',
                    'data-toggle' => 'tooltip',
                    'data-original-title' => Yii::t('app', 'Просмотр заказа')]); ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="col-md-3 col-sm-4 text-center">
    <table class="table">
        <thead>
        <th class="text-center">Клиент</th>
        </thead>
        <tbody>
        <tr>
            <td style=""><?= $model['client']?> <?= $model['exportClientName'] ? '(' . $model['exportClientName'] .  ')' : ''?></td>
        </tr>
        <tr>
            <td><?= $model['category']?> <?= $model['exportCategory'] ? '(' . $model['exportCategory'] .  ')' : ''?></td>
        </tr>
        </tbody>
    </table>
</div>
<div class="col-md-3 col-sm-4 text-center">
    <table class="table">
        <thead>
        <th class="text-center">Доставка</th>
        </thead>
        <tbody>
        <tr>
            <td><?= $model['delivery']?></td>
        </tr>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>

