<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var yii\web\View $this */
/* @var int|null $shipmentId */
/* @var $providerYes yii\data\ArrayDataProvider */
/* @var $providerNo yii\data\ArrayDataProvider */
/* @var string|null $search */
/* @var array $boxes */
/* @var array $boxList */

$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Объединить');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Монитор логистики'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= Html::a('<i class="icon wb-check"></i> ' . Yii::t('app', 'Распределить по коробкам'), ['boxed', 'shipmentId' => $shipmentId], ['class' => 'btn btn-info btn-sm', 'id' => 'save-bindings-button']) ?>


<style>
    .checkbox-custom label::before {
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
</style>

<?php
    foreach ($boxes as $key => $box){
        $boxListItmems = &$boxList;
        $del = array_search($key,$boxListItmems);
        unset($boxListItmems[$del]);
        echo $this->render('_box',['box' => $box,'id' => $key,'boxList' => $boxListItmems]);
    }
?>

<?php $form = ActiveForm::begin(['id' => 'delete-form']); ?>
    <div id="delete-form-content-block" class="hide"></div>
<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin(['id' => 'ready-form']); ?>
    <div id="ready-form-content-block" class="hide"></div>
<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin(['id' => 'change-form']); ?>
    <div id="change-form-content-block" class="hide"></div>
<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin(['id' => 'add-form']); ?>
    <div id="add-form-content-block" class="hide"></div>
<?php ActiveForm::end(); ?>

<?php Yii::$app->view->registerJs('
    $(".delete-bindings-button").click(function() {
        fillDeleteForm();
        $("#delete-form").submit();
    });
    $(".change-bindings-button").click(function() {
        let val = $(this).val();
        fillChangeForm(val);
        $("#change-form").submit();
    });
    $(".ready-button").click(function() {
        let val = $(this).attr("data-box-id");
        fillReadyForm(val);
        $("#ready-form").submit();
    });
    $(".add-bindings-button").click(function() {
        fillAddForm();
        $("#add-form").submit();
    });
    
    $("#pjax-yes").on("pjax:end", function() {
        $("#pjax-yes [data-toggle=tooltip]").tooltip();
    });
    $("#pjax-no").on("pjax:end", function() {
        $("#pjax-no [data-toggle=tooltip]").tooltip();
        showBindButton();
    });
    $("body").on("change", "#table-no .checkbox-select-order",function(e){
        showBindButton();
    });
    $("body").on("change", "#checkbox-select-all-no",function(e){
       $("#table-no .checkbox-select-order").prop("checked", $(this).prop("checked")).change();
       showBindButton();
    });
    function showBindButton() {
        if ($("#table-no .checkbox-select-order:checked").length > 0) {
            $("#add-bindings-button").prop("disabled", false);
        } else {
            $("#add-bindings-button").prop("disabled", true);
        }
    }
    function fillDeleteForm() {
        $("#delete-form-content-block").html("");
        $("#table-no .checkbox-select-order-entry:checked").each(function(){
            $("#delete-form-content-block").append($(this).clone().attr("name","deleteFromBox[]"));
        });
    }
    function fillReadyForm(box) {
        $("#ready-form-content-block").html("");
        $("<input>", {
            type: "hidden",
            id: "foo",
            name: "readyBox",
            value: box
        }).appendTo("form");
    }
    function fillChangeForm(box) {
        $("#change-form-content-block").html("");
        $("#table-no .checkbox-select-order-entry:checked").each(function(){
            $("#change-form-content-block").append($(this).clone().attr("name","changeBox[items][]"));
        });
        $("<input>", {
            type: "hidden",
            id: "foo",
            name: "changeBox[box]",
            value: box
        }).appendTo("form");
        
    }
    function fillAddForm() {
        $("#add-form-content-block").html("");
        $("#table-no .checkbox-select-order-entry:checked").each(function(){
            $("#add-form-content-block").append($(this).clone().attr("name","addToBox[]"));
        });
    }
', \yii\web\View::POS_END);
?>
