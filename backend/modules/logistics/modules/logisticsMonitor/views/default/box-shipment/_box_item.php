<?php
use yii\helpers\Html;
/* @var $model array */
/* @var $id integer  */
?>

<hr>
<div class="col-md-12 <?= $status? 'hidden' : '' ?>">
    <button class="change-bindings-button" name="changeBox[box]" value="0">Убрать из коробки</button>
    <?foreach ($boxList as $box):?>
        <button class="change-bindings-button" name="changeBox[box]" value="<?= $box?>">Поместить в коробку № <?= $box?> </button>
    <?endforeach;?>
    <button class="add-bindings-button">Определить в новую коробку</button>
    <button class="ready-button" data-box-id="<?= $id?>">Пометить "готовой к упаковке"</button>
</div>
<div class="col-md-12 <?= $status? '' : 'hidden' ?>">
    <button class="change-bindings-button" name="changeBox[box]" value="0">Убрать из коробки</button>
</div>
<div style="font-size: 0.9em">
<div class="col-md-1 text-center">
    <table class="table">
        <thead>
            <tr>
                <th class="text-center">Выбрать</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?= Html::tag('div',
                    Html::checkbox(null, false, [
                        'class' => 'checkbox-select-order',
                        'onchange' => '
                            $("#table-no .ordern' . $model['id'] . $id . '").prop("checked", $(this).is(":checked"));
                        ',
                    ]) .
                    Html::tag('label',
                        null
                    ),
                    [
                        'class' => 'checkbox-custom checkbox-primary text-center'  . ($status ?' hidden':''),
                    ]
                );?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="col-md-1 text-center">
    <table class="table">
        <thead>
        <th class="text-center">№ Заказа</th>
        </thead>
        <tbody>
        <tr>
            <td>
                <div>
                    <?= @$model['sourceId'] ? str_replace((mb_substr(@$model['sourceId'], 4, mb_strlen(@$model['sourceId']))),
                        ('<b style="font-size: 1.4em;font-weight: bold; color: red;">' . mb_substr(@$model['sourceId'], 4, mb_strlen(@$model['sourceId'])) . '</b>'), @$model['sourceId']) : @@$model['sourceId'] ?>
                    <br>
                    <span class="text-nowrap">от <?= date('d.m.Y',$model['createdAt']) ?></span>
                    <?php if (@$model['isNeedTtn']): ?>
                        <strong class="text-nowrap" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Обязательно приложить ТТН к заказу')?>"><?=Yii::t('app', '(с ТТН)')?></strong>
                    <?php endif;?>
                    <?php if (@$model['isCOD']): ?>
                        <strong class="text-nowrap text-danger" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Оплата заказа производится наложенным платежем')?>"><?=Yii::t('app', '(Наложка)')?></strong>
                    <?php endif;?>
                </div>
                <?= Html::a('<i class="fa fa-eye"></i>', ['view-order', 'orderId' => $model['id']], [
                    'target' => '_blank',
                    'class' => 'btn btn-sm btn-icon btn-outline btn-round btn-primary',
                    'style' => 'margin-top:3px;',
                    'data-pjax' => '',
                    'data-toggle' => 'tooltip',
                    'data-original-title' => Yii::t('app', 'Просмотр заказа')]); ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="col-md-1 text-center">
    <table class="table">
        <thead>
        <th class="text-center">Клиент</th>
        </thead>
        <tbody>
        <tr>
            <td style=""><?= $model['client']?> <?= $model['exportClientName'] ? '(' . $model['exportClientName'] .  ')' : ''?></td>
        </tr>
        <tr>
            <td><?= $model['category']?> <?= $model['exportCategory'] ? '(' . $model['exportCategory'] .  ')' : ''?></td>
        </tr>
        </tbody>
    </table>
</div>
<div class="col-md-9">
    <table class="table table-no">
        <thead>
        <th class="text-center">Выбрать</th>
        <th class="text-center">Автомобиль</th>
        <th class="text-center">Наименование</th>
        <th class="text-center">ID</th>
        </thead>

        <tbody>
        <?foreach ($model['orderEntries'] as $key => $orderEntries):?>
            <tr>
                <td style="" class="text-center" colspan="4"><?= $key ? 'Комплект' : 'Отедельные позиции'?>&nbsp;</td>
            </tr>
            <?foreach ($orderEntries as $orderEntry):?>
                <tr class="<?= @$orderEntry->upn->storageState ? 'success' : ''?>">
                    <td style="">
                        <?= Html::tag('div',
                            Html::checkbox(null, false, ['value' => $orderEntry->id, 'class' => 'checkbox-select-order-entry ordern' . $orderEntry->order_id . $id]) .
                            Html::tag('label',
                                null
                            ),
                            [
                                'class' => 'checkbox-custom checkbox-primary text-center' . (@$orderEntry->upn->packed?' hidden':''),
                            ]
                        );?>
                    </td>
                    <td style="">
                        <?= $orderEntry->car; ?>
                    </td>
                    <td style="">
                        <?= $orderEntry->name; ?>
                    </td>
                    <td style="">
                        <?= $orderEntry->id; ?>
                    </td>
                </tr>
            <?endforeach;?>
        <?endforeach;?>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
</div>
