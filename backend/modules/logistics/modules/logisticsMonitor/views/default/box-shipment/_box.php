<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 1/4/19
 * Time: 9:00 AM
 *
 * @var \yii\data\ArrayDataProvider $box
 * @var integer $id
 */
?>


<div class="" style="padding: 10px;margin: 10px;border-radius: 5px;<?=$box['status'] ? 'background-color:rgba(0, 120, 201, 0.1);': ''?>">
<h5>Коробка № <?=$id?></h5><?=$box['status'] ? '<span style="color: black;border: 1px solid black;display: inline-block;padding: 5px">Коробка уже отправлена на упаковку</span>': ''?>
<?= \yii\widgets\ListView::widget([
    'dataProvider' => $box['items'],
    'itemView' => '_box_item',
    'id' => 'table-no',
    'viewParams' => ['id' => $id,'boxList' => $boxList,'status' => $box['status']],
]) ?>
</div>
