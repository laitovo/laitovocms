var mySave = function(validateUrl) {
    $.post(validateUrl, $("#my-form").serialize())
        .done(function(data) {
            if (data.status == "success") {
                console.log($("#my-form").serialize());
                let arr = [];
                $('[name="shipments[]"]').each(function () {
                    if (this.value) { arr.push(this.value); }
                });
                console.log(JSON.stringify(arr));
                let jsonArr = JSON.stringify(arr);
                $("#europeinvoice-basedon").val(jsonArr);
                $("#europeinvoice-rate").val($('[name="course"]').val());
                $("#submit-form").submit();

            } else if (data.status == "error") {
                notie.alert(3,data.message,15);
            } else {
                notie.alert(3,"Неизвестная ошибка. Обратитесь в IT отдел",15);
            }
        });
};