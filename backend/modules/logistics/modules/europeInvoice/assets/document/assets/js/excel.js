var excel = function(validateUrl,excelUrl,name) {
    $.post(validateUrl, $("#my-form").serialize())
.done(function(data) {
        if (data.status == "success") {

            var csrfParam = $("meta[name=csrf-param]").attr("content");
            var csrfValue = $("meta[name=csrf-token]").attr("content");
            var body = $("#my-form").serialize();
            var request = new XMLHttpRequest();
            request.open("POST",excelUrl, true);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            request.setRequestHeader("X-CSRF-Token", csrfValue);
            request.responseType = "blob";

            request.onload = function(e) {
                if (this.status === 200) {
                    var blob = this.response;
                    if(window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveBlob(blob, name + ".xlsx");
                    } else {
                        var downloadLink = window.document.createElement("a");
                        var contentTypeHeader = request.getResponseHeader("Content-Type");
                        downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: contentTypeHeader }));
                        downloadLink.download = name + ".xlsx";
                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                        document.body.removeChild(downloadLink);
                    }
                }
            };

            request.send(body);

        } else if (data.status == "error") {
            notie.alert(3,data.message,15);
        } else {
            notie.alert(3,"Неизвествная ошибка. Обратитесь в IT отдел",15);
        }
    });
};