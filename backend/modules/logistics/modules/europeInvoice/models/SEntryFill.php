<?php


namespace backend\modules\logistics\modules\europeInvoice\models;

use backend\helpers\ArticleHelper;
use backend\modules\logistics\models\OrderEntry;
use core\models\europeInvoice\EuropeInvoice;
use core\models\europeInvoice\EuropeInvoiceEntry;
use obmen\models\laitovo\Cars;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\di\NotInstantiableException;

/**
 * Сервис для заполнения табличной части инвойса
 *
 * Class SEntryFill
 * @package backend\modules\logistics\modules\europeInvoice\models
 */
class SEntryFill extends Model
{
    /**
     * @param $invoice EuropeInvoice
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public static function fill($invoice)
    {
        foreach ($invoice->europeInvoiceEntries as $entry) {
            $entry->delete();
        }
        $shipments = json_decode($invoice->basedOn);
        $shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        foreach ($shipments as $shipmentID) {
            $shipment = $shipmentManager->findById($shipmentID);
            if (!$shipment) {
                throw new \DomainException('Отгрузка № ' . $shipmentID . ' не найдена в системе!');
            }

            $orderEntries = $shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);

            foreach ($orderEntries as $orderEntry) {
                /**
                 * @var $orderEntry OrderEntry
                 */
                $article = $orderEntry->article;
                /**
                 * @var $car Cars
                 */
                $car = ArticleHelper::getCarModel($orderEntry->article);
                if (!$car) {
                    $title = ArticleHelper::getInvoiceRusTitle($orderEntry->article) . ' / ' . ArticleHelper::getInvoiceTitle($orderEntry->article);
                }else {
                    $title = "Защитные экраны для автомобильных окон / Protective screens for car windows<br>{$car->fullEnName}<br>({$article})";
                }
                $price   = round($orderEntry->price / ($invoice->rate ? : 60),2);

                $element = new EuropeInvoiceEntry();
                $element->article = $article;
                $element->invoiceId = $invoice->id;
                $element->upnId = $orderEntry->upn->id;
                $element->quantity = 1;
                $element->price = $price;
                $element->title = $title;
                $element->save();
            }
        }
    }
}