<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel core\models\europeInvoice\EuropeInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Инвойсы');
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');
?>
<div class="europe-invoice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать новый инвойс'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'basedOn',
                'value' => function ($data) {
                    $arr = json_decode($data->basedOn);
                    return implode(', ',$arr);
                },
            ],
            'rate',
            [
                'attribute' => 'totalPrice',
                'format' => ['decimal', 2]
            ],
            'totalCount',
            'createdAt:datetime',
            [
                'attribute' => 'isLaitovoManufactory',
                'value' => function ($data) {
                    return $data->isLaitovoManufactory ? 'Да' : 'Нет';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
