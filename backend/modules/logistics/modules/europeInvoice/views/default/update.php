<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\logistics\modules\europeInvoice\assets\document\DocumentAsset;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model core\models\europeInvoice\EuropeInvoice */
/* @var $form yii\widgets\ActiveForm */
/* @var $shipments array */

DocumentAsset::register($this);

/* @var $this yii\web\View */
/* @var $model core\models\europeInvoice\EuropeInvoice */

$this->title = Yii::t('app', 'Изменить инвойс №' . $model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список инвойсов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/logistics/views/menu');
?>
<div class="europe-invoice-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?= \yii\helpers\Html::button('<span class="fa fa-plus-square"></span> Добавить отгрузку',[
            'class' => 'btn btn-sm btn-primary btn-margin',
            'data-original-title' => 'Добавить отгрузку',
            'data-toggle' => 'tooltip',
            'onclick' => '
                var elem = $(".copy").clone(true).removeClass("copy").removeClass("hidden");
                $(".shipments").append(elem);'
        ]);?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-sm btn-info btn-margin']) ?>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6 hidden box copy">
        <div class="input-group">
            <?= \yii\helpers\Html::input('text','shipments[]',null,['class' => 'form-control','placeholder' => 'отгрузка №']);?>
            <span class="input-group-addon btn btn-danger btn-sm" onclick="$(this).closest('.box').remove();" title="Удалить позицию"><span class="fa fa-minus-square"></span></span>
        </div>
    </div>
    <?= \yii\helpers\Html::beginForm([''],'post',['id' => 'my-form']);?>
    <!--Курс валюты-->
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-6 box">
            <div class="input-group">
                <?= \yii\helpers\Html::input('number','course',$model->rate,['class' => 'form-control','placeholder' => 'Введите курс ..']);?>
                <span class="input-group-addon" title="Удалить позицию" >Курс валюты</span>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <!--Оотгрузки-->
    <div class="row shipments">
        <?foreach ($shipments as $shipment):?>
        <div class="col-md-3 col-sm-4 col-xs-6 box">
            <div class="input-group">
                <?= \yii\helpers\Html::input('text','shipments[]',$shipment,['class' => 'form-control','placeholder' => 'отгрузка №']);?>
                <span class="input-group-addon btn btn-danger btn-sm" onclick="$(this).closest('.box').remove();" title="Удалить позицию" ><span class="fa fa-minus-square"></span></span>
            </div>
        </div>
        <?endforeach;?>
    </div>

    <?= \yii\helpers\Html::endForm();?>
    <br>
    <?= \yii\helpers\Html::button('Предпросмотр',['class' => 'btn btn-default btn-margin','onclick' => 'myView("'. Url::to("validate") .'","'. Url::to("print") .'")']);?>
    <hr>

    <div class="europe-invoice-form">

        <?php $form = ActiveForm::begin(['id' => 'submit-form']); ?>

        <?= $form->field($model, 'basedOn')->textarea(['rows' => 6])->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'rate')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'isLaitovoManufactory')->checkbox() ?>

<!--        --><?//= $form->field($model, 'createdAt')->textInput() ?>

<!--        --><?//= $form->field($model, 'authorId')->textInput() ?>

        <div class="form-group">
            <?= \yii\helpers\Html::button( Yii::t('app', 'Изменить'),['class' => 'btn btn-success btn-margin','onclick' => 'mySave("'. Url::to("validate") .'")']);?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
