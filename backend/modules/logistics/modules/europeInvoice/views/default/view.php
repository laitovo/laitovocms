<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model core\models\europeInvoice\EuropeInvoice */
/* @var $shipments array */

$this->title = 'Инвойс № ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список инвойсов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\backend\modules\logistics\modules\printDocuments\assets\document\DocumentAsset::register($this);

$this->render('@backendViews/logistics/views/menu');
?>
<div class="europe-invoice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-sm btn-info btn-margin']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'label' => 'Номер',

            ],
            [
                'attribute' => 'basedOn',
                'value' => function ($data) {
                    $arr = json_decode($data->basedOn);
                    return implode(', ',$arr);
                },
            ],
            'rate',
            [
                'attribute' => 'totalPrice',
                'format' => ['decimal', 2]
            ],
            'totalCount',
            'createdAt:datetime',
            [
                'attribute' => 'isLaitovoManufactory',
                'value' => $model->isLaitovoManufactory ? 'Да' : 'Нет',
            ],
        ],
    ]) ?>

</div>

<?= \yii\helpers\Html::beginForm([''],'post',['id' => 'my-form']);?>

    <?foreach ($shipments as $shipment): ?>
        <?= \yii\helpers\Html::hiddenInput('shipments[]',$shipment);?>
    <?endforeach;?>

    <?= \yii\helpers\Html::hiddenInput('course',$model->rate);?>

<?= \yii\helpers\Html::endForm();?>

<div class="form-group">
        <?= \yii\helpers\Html::button('Печать',['class' => 'btn btn-default btn-margin','onclick' => 'myPrint("'. Url::to("validate") .'","'. Url::to("print") .'")']);?>
        <?= \yii\helpers\Html::button('Excel',['class' => 'btn btn-default btn-margin','onclick' => 'excel("'. Url::to("validate") .'","'. Url::to("excel") .'","invoice # '.$model->id.'")']);?>
</div>
