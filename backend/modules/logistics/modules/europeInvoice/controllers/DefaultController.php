<?php

namespace backend\modules\logistics\modules\europeInvoice\controllers;

use backend\modules\logistics\modules\europeInvoice\models\PrintInvoice;
use Yii;
use core\models\europeInvoice\EuropeInvoice;
use core\models\europeInvoice\EuropeInvoiceSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DefaultController implements the CRUD actions for EuropeInvoice model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EuropeInvoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EuropeInvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort([
            'defaultOrder' => ['id'=>SORT_DESC]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EuropeInvoice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'shipments' => json_decode($model->basedOn)
        ]);
    }

    /**
     * Creates a new EuropeInvoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EuropeInvoice();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EuropeInvoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'shipments' => json_decode($model->basedOn)
            ]);
        }
    }

    /**
     * @param $id
     * @return Response
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        Yii::$app->session->setFlash('warning', 'Вы не можете удалить инвойс');
        return $this->redirect(['index']);

        $this->findModel($id)->delete();

    }

    /**
     * Finds the EuropeInvoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EuropeInvoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EuropeInvoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Действие проверяет, возможно ли сформировать документ из заданных данных (Данные проходят вализацию).
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionValidate()
    {
        if (!\Yii::$app->request->isAjax) {
            throw new NotFoundHttpException(\Yii::t('yii', 'Page not found.'));
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        $response['status'] = 'success';
        $response['message'] = 'Документ успешно сформирован!';

        try{
            $model = new PrintInvoice();
            $model->setAttributes(\Yii::$app->request->post());
            if (!$model->validate()) {

                $result = [];
                foreach ($model->getErrors() as $attribute => $error) {
                    foreach ($error as $message) {
                        $result[] = $message;
                    }
                }
                $response['status'] = 'error';
                $response['message'] = implode(' ', $result);
            }
        }catch (\DomainException $exception){
            $response['status'] = 'error';
            $response['message'] = $exception->getMessage();
        }

        return $response;
    }

    /**
     * Действие отдает печатную форму для документа
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionPrint()
    {
        if (!\Yii::$app->request->isAjax) {
            throw new NotFoundHttpException(\Yii::t('yii', 'Page not found.'));
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [
            'status'  => 'success',
            'message' => 'Документ распечатан',
            'html'    => false
        ];

        $model = new PrintInvoice();
        $model->setAttributes(\Yii::$app->request->post());
        if (!$model->validate()) {
            $result = [];
            foreach ($model->getErrors () as $attribute => $error)
            {
                foreach ($error as $message)
                {
                    $result[] = $message;
                }
            }
            $response['status'] = 'error';
            $response['message'] = implode(' ',$result);
        }else{
            $response['html'] = $this->renderPartial('_invoice_print',[
                'elements' => $model->getPrintList(),
                'fullAmount' => $model->fullAmount,
                'fullCount' => $model->fullCount,

            ]);
        }

        return $response;
    }

    /**
     * Действие отдает сформированный документ в формате excel
     *
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionExcel()
    {
        $model = new PrintInvoice();

        $model->setAttributes(\Yii::$app->request->post());

        $pExcel = new \PHPExcel();
        $pExcel->setActiveSheetIndex(0);
        $aSheet = $pExcel->getActiveSheet();
        // Ориентация страницы и  размер листа
        $aSheet->getPageSetup()
            ->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $aSheet->getPageSetup()
            ->SetPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        // Поля документа
        $aSheet->getPageMargins()->setTop(1);
        $aSheet->getPageMargins()->setRight(0.75);
        $aSheet->getPageMargins()->setLeft(0.75);
        $aSheet->getPageMargins()->setBottom(1);
        // Название листа
        $aSheet->setTitle('Инвойс');
        // Настройки шрифта
        $pExcel->getDefaultStyle()->getFont()->setName('Arial');
        $pExcel->getDefaultStyle()->getFont()->setSize(10);
        //Указываем ширину колонки
        $aSheet->getColumnDimension('A')->setWidth(3);
        $aSheet->getColumnDimension('B')->setWidth(100);
        $aSheet->getColumnDimension('C')->setWidth(10);
        $aSheet->getColumnDimension('D')->setWidth(10);
        $aSheet->getColumnDimension('E')->setWidth(10);
        $aSheet->getColumnDimension('F')->setWidth(10);

        $borderArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $line = 1;
        $aSheet->getRowDimension($line)->setRowHeight(35);
        $aSheet->getStyle('A' .$line.':F'.$line)->getAlignment()->setWrapText(true);
        $aSheet->getStyle('A' .$line.':F'.$line)->getFont()->setBold( true );
        $aSheet->getStyle('A' .$line.':F'.$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $aSheet->getStyle('A' .$line.':F'.$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $aSheet->setCellValue('A'.$line, '№'  );
        $aSheet->setCellValue('B'.$line, 'Наименование / Title');
        $aSheet->setCellValue('C'.$line, 'Кол-во / Quantity' );
        $aSheet->setCellValue('D'.$line, 'Ед / Pcs'    );
        $aSheet->setCellValue('E'.$line, 'Цена (EURO) / Price'   );
        $aSheet->setCellValue('F'.$line, 'Сумма (EURO) / Amount'  );

        foreach ($model->getPrintList() as $element) {
            $line++;
            $aSheet->getRowDimension($line)->setRowHeight(30);
            $aSheet->getStyle('A' .$line.':F'.$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $aSheet->getStyle('A' .$line.':F'.$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $aSheet->getStyle('E' .$line.':F'.$line)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

            $aSheet->getStyle('B'.$line)->getAlignment()->setWrapText(true);

            $aSheet->setCellValue('A'.$line, $element['number']   );
            $aSheet->setCellValue('B'.$line, str_ireplace('<br>',"\r\n",$element['title']));
            $aSheet->setCellValue('C'.$line, $element['quantity'] );
            $aSheet->setCellValue('D'.$line, $element['pcs']      );
            $aSheet->setCellValue('E'.$line, number_format($element['price'],2) );
            $aSheet->setCellValue('F'.$line, number_format($element['amount'],2));
        }

        $line++;
        $aSheet->mergeCells('A' .$line.':B'.$line);
        $aSheet->setCellValue('A'.$line, 'Количество / Quantity:');
        $aSheet->getStyle('A' .$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $aSheet->getStyle('A' .$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $aSheet->setCellValue('C'.$line, number_format($model->fullCount,2));
        $aSheet->getStyle('C' .$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $aSheet->getStyle('C' .$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $aSheet->getStyle('C' .$line)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        $aSheet->mergeCells('D' .$line.':E'.$line);
        $aSheet->setCellValue('D'.$line, 'Итого / Total:');
        $aSheet->getStyle('D' .$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $aSheet->getStyle('D' .$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $aSheet->setCellValue('F'.$line, number_format($model->fullAmount,2));
        $aSheet->getStyle('F' .$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $aSheet->getStyle('F' .$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $aSheet->getStyle('F' .$line)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

        $aSheet->getStyle('A' .$line.':F'.$line)->getFont()->setBold( true );

        $aSheet->getStyle('A1:F'.$line)->applyFromArray($borderArray);
        unset($styleArray);

        header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition:attachment;filename="simple.xlsx"');
        $objWriter = new \PHPExcel_Writer_Excel2007($pExcel);
        ob_end_clean();
        $objWriter->save('php://output');
        exit();
    }
}
