<?php

namespace backend\modules\logistics\modules\europeInvoice;

/**
 * europeInvoice module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\logistics\modules\europeInvoice\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
