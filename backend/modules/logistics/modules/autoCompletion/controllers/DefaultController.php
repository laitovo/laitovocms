<?php

namespace backend\modules\logistics\modules\autoCompletion\controllers;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryadFactory;
use backend\modules\laitovo\models\MainProductInfo;
use backend\modules\logistics\models\Naryad;
use core\logic\CheckStoragePosition;
use core\models\autoCompletionBlocks\AutoCompletionBlocks;
use core\models\autoCompletionItems\AutoCompletionItems;
use core\models\autoCompletionItems\AutoCompletionItemsSearch;
use core\services\SAutoCompletionBlocks;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{

    /**
     * [ Показывает общий список позиций ]
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AutoCompletionItemsSearch();
        $dataProvider = $searchModel->searchNotHandled(\Yii::$app->request->post());
//        $dataProvider->setSort(false);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * [ Отвечает за удаление позиции из списка ]
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionRemove($id)
    {
        $model = AutoCompletionItems::findOne($id);
        if (!$model) {
            Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
            return $this->redirect('index');
        }

        $model->status = 1;
        if (!$model->save()) {
            Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
        }else{
            Yii::$app->session->setFlash('success', "Позиция {$model->article} успешно удалена из списка");
        }
        return $this->redirect('index');
    }

    /**
     * @param $ids
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionRemoveMany()
    {
        $articles = Yii::$app->request->post('articles');
        $articles = json_decode($articles,true);
        $models = AutoCompletionItems::find()->where(['in', 'article', $articles])->andWhere(['status' => 0])->all();
        if (empty($models)) {
            Yii::$app->session->setFlash('error', 'Вы не указали, что необходимо исколючить из списка, отметте необходимые позиции галочкой!');
            return $this->redirect('index');
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($models as $model) {
                $model->status = 1;
                if (!$model->save()) {
                    Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
                    $transaction->rollBack();

                }
            }
            $transaction->commit();
            Yii::$app->session->setFlash('success', "Позиции " . implode(', ', $articles) . " успешно удалена из списка");
            return $this->redirect('index');


        } catch (\Exception $exception) {
            Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
            $transaction->rollBack();
            return $this->redirect('index');
        }
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionBlockArticle($id)
    {
        $model = AutoCompletionItems::findOne($id);
        if (!$model) {
            Yii::$app->session->setFlash('error', 'Не удалось заблокировать артикул');
            return $this->redirect('index');
        }

        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (!SAutoCompletionBlocks::blockArticle($model->article)) {
                Yii::$app->session->setFlash('error', 'Не удалось заблокрировать указзанный артикул');
                $transaction->rollBack();
                return $this->redirect('index');
            };

            $model->status = 1;
            if (!$model->save()) {
                Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
                $transaction->rollBack();
                return $this->redirect('index');
            }

            $rows = AutoCompletionItems::find()->notHandledByArticle($model->article)->all();
            if ($rows) {
                foreach ($rows as $row) {
                    $row->status = 1;
                    if (!$row->save()) {
                        Yii::$app->session->setFlash('error', 'Не удалось исключить позиции из списка');
                        $transaction->rollBack();
                        return $this->redirect('index');
                    }
                }
            }

            Yii::$app->session->setFlash('success', "Позиция {$model->article} успешно заблокирована и удалена из списка");
            $transaction->commit();

        }catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Не удалось заблокировать артикул, возникли програмные ошибки!');
        }

        return $this->redirect('index');
    }



    /**
     * @param $ids
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionBlockArticleMany()
    {
        $articles = Yii::$app->request->post('articles');
        $articles = json_decode($articles,true);
        $models = AutoCompletionItems::find()->where(['in', 'article', $articles])->andWhere(['status' => 0])->all();
        if (empty($models)) {
            Yii::$app->session->setFlash('error', 'Вы не указали, что необходимо исколючить из списка, отметте необходимые позиции галочкой!');
            return $this->redirect('index');
        }

        $transaction = Yii::$app->db->beginTransaction();
        try{
            foreach ($models as $model) {
                if (!SAutoCompletionBlocks::blockArticle($model->article)) {
                    Yii::$app->session->setFlash('error', 'Не удалось заблокрировать указзанный артикул');
                    $transaction->rollBack();
                    return $this->redirect('index');
                };

                $model->status = 1;
                if (!$model->save()) {
                    Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
                    $transaction->rollBack();
                    return $this->redirect('index');
                }

                $rows = AutoCompletionItems::find()->notHandledByArticle($model->article)->all();
                if ($rows) {
                    foreach ($rows as $row) {
                        $row->status = 1;
                        if (!$row->save()) {
                            Yii::$app->session->setFlash('error', 'Не удалось исключить позиции из списка');
                            $transaction->rollBack();
                            return $this->redirect('index');
                        }
                    }
                }

            }

            Yii::$app->session->setFlash('success', "Позиции "  . implode(', ', $articles) . " успешно заблокированы и удалены из списка");
            $transaction->commit();

        }catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Не удалось заблокировать артикулы, возникли програмные ошибки!');
        }

        return $this->redirect('index');
    }


    /**
     * [ Действие - блокировка автомобилей ]
     *
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionBlockCar($id)
    {
        $model = AutoCompletionItems::findOne($id);
        if (!$model) {
            Yii::$app->session->setFlash('error', 'Не удалось заблокировать автомобиль');
            return $this->redirect('index');
        }

        $transaction = Yii::$app->db->beginTransaction();
        try{
            $block = SAutoCompletionBlocks::blockCar($model->article);
            if (!$block) {
                Yii::$app->session->setFlash('error', 'Не удалось заблокрировать указзанный автомобиль');
                $transaction->rollBack();
                return $this->redirect('index');
            };

            $model->status = 1;
            if (!$model->save()) {
                Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
                $transaction->rollBack();
                return $this->redirect('index');
            }

            $rows = AutoCompletionItems::find()->notHandledLikeMask($block->catalogMask)->all();
            if ($rows) {
                foreach ($rows as $row) {
                    $row->status = 1;
                    if (!$row->save()) {
                        Yii::$app->session->setFlash('error', 'Не удалось исключить позиции из списка');
                        $transaction->rollBack();
                        return $this->redirect('index');
                    }
                }
            }

            Yii::$app->session->setFlash('success', "Позиция {$model->article} успешно заблокирована и удалена из списка");
            $transaction->commit();

        }catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Не удалось заблокировать артикул, возникли програмные ошибки!');
        }

        return $this->redirect('index');
    }

    /**
     * @param $ids
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionBlockCarMany()
    {
        $articles = Yii::$app->request->post('articles');
        $articles = json_decode($articles,true);
        $models = AutoCompletionItems::find()->where(['in', 'article', $articles])->andWhere(['status' => 0])->all();
        if (empty($models)) {
            Yii::$app->session->setFlash('error', 'Вы не указали, что необходимо исколючить из списка, отметте необходимые позиции галочкой!');
            return $this->redirect('index');
        }

        $transaction = Yii::$app->db->beginTransaction();
        try{
            foreach ($models as $model) {
                $block = SAutoCompletionBlocks::blockCar($model->article);
                if (!$block) {
                    Yii::$app->session->setFlash('error', 'Не удалось заблокрировать указзанный автомобиль');
                    $transaction->rollBack();
                    return $this->redirect('index');
                };

                $model->status = 1;
                if (!$model->save()) {
                    Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
                    $transaction->rollBack();
                    return $this->redirect('index');
                }

                $rows = AutoCompletionItems::find()->notHandledLikeMask($block->catalogMask)->all();
                if ($rows) {
                    foreach ($rows as $row) {
                        $row->status = 1;
                        if (!$row->save()) {
                            Yii::$app->session->setFlash('error', 'Не удалось исключить позиции из списка');
                            $transaction->rollBack();
                            return $this->redirect('index');
                        }
                    }
                }
            }

            Yii::$app->session->setFlash('success', "Позиция "  . implode(', ', $articles) . " успешно заблокирована и удалена из списка");
            $transaction->commit();

        }catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Не удалось заблокировать артикулы, возникли програмные ошибки!');
        }

        return $this->redirect('index');
    }

    /**
     * Отправка заявки на производство
     *
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionOrder()
    {
        $items = AutoCompletionItems::find()->notHandled()->all();
        $connection = \Yii::$app->db;

        $transaction = $connection->beginTransaction();
        try {
            foreach ($items as $item) {
                for ($i = 1; $i <= $item->requested; $i++) {
                    $naryad = new Naryad();
                    $naryad->article = $item->article;
                    $naryad->team_id = \Yii::$app->team->id;
                    $naryad->comment = "Автопополнение склада";
                    $naryad->source_id = 1;
                    $naryad->save();

                    $title = CheckStoragePosition::getStorageTitle($item->article);
                    if ($title === '') {
                        $title = MainProductInfo::getTitleNew($item->article);
                    }
                    $factory = new ErpNaryadFactory();
                    $workOrderBarcode = BarcodeHelper::upnToWorkOrderBarcode($naryad->barcode);
                    $factory->create($item->article,$title,$naryad->id,$workOrderBarcode,$naryad->comment,'','',7);
                }

                $item->status = true;
                if (!$item->save()) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
                    return $this->redirect('index');
                }
            }
            $transaction->commit();
            Yii::$app->session->setFlash('success', 'Позиции отправлены в пополнение склада');
        }catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Не удалось исключить позицию из списка');
        }
        return $this->redirect('index');
    }
}