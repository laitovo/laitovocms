<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$js = <<<JS
    function parseitemsjson (callback) {
        let articles =[];
        $('.articles.item').each(function( index, value ) {
            itemjson = $(this).find("input").serializeArray();
            console.log(itemjson);
            $.each(itemjson, function( index1, value1 ) {
                articles.push(value1.value);
            });
        });
        $('#ids-data').val(JSON.stringify(articles));
    }

    let sendForm = function(href) {
        let  form = $('#my-form');
        form.attr('action',href);
        parseitemsjson();
        form.submit();
    }
JS;

$this->registerJs($js,\yii\web\View::POS_HEAD);

$this->render('@backendViews/logistics/views/menu');
?>
<div class="material-production-scheme-index">

    <h1>Модерация автопополнения</h1>

    <p>
        <?= Html::a('<i class="icon wb-plus"></i> Отправить заказ на производство', ['order'],
            [
                'class' => 'btn btn-icon btn-outline btn-round btn-primary',
                'data' => [
                    'confirm' => 'Вы уверенны, что корректо промодерировали заявку на производство и хотите послать ее в таком виде ?',
                    'method' => 'post',
                ],
                'data-toggle' => "tooltip",
                'data-original-title' => Yii::t('app', 'Отправить заказ на производство')
            ]) ?>

    </p>

    <hr>
        <p>
            <h5>Логика автопополнения:</h5>
            <ul>
                <li> Заказ физика попадает в автопополнение в момент попадания заказа в Biz-zone. </li>
                <li>
                    Заказ оптовика попадает в автопополнение в момент отгрузки заказа со склада и только те позиции, которые были отгружены со склада.
                    Позиции из заказа оптовика, созданные на производстве в автопополнение не попадают.
                </li>
                <li>
                    И заказ физика и заказ оптовика проверяется на наличие заблокированных продуктов, машин, конкретных артикулов.
                    Если блок срабатывает, в автопополнение позиция не попадает.
                </li>
            </ul>
            <h5>Пояснение к колонкам:</h5>
            <ul>
                <li> <span style="color: black">Статистика</span> - количесто продаж в 6 предыдуших месяцах </li>
                <li> <span style="color: black">Остатки</span> - заказ, на основе которого была предложена данная позиция уже в резерве, а остаток указан за минусом резерва</li>
                <li> <span style="color: black">На производстве</span> - показывает позиции только автопополнения, позиции под заказ не показывает</li>
            </ul>
            <h5>Легенда цветов:</h5>
            <span class="btn btn-warning" style="display: inline-block;width: 15px"></span> - позиции, на которые стоит обратить внимание. Остаток в 2 раза больше, чем статистика
        </p>
    <hr>

    <?=
        Html::button('<span class="glyphicon glyphicon-remove"></span> Исключить из текущей заявки',
        [
            'title' => Yii::t('app', 'Убрать из списка'),
            'class' => 'btn btn-primary',
            'onclick' => "sendForm('remove-many')",
//            'data' => [
//                'confirm' => 'Вы уверены, что хотите удалить данное рабочее место ?',
//                'method' => 'post',
//            ],
            'data-toggle' => "tooltip",
            'data-original-title' => Yii::t('app', 'Исключить из текущего заказа на автопополнение')
        ]);
    ?>

    <?=
        Html::button('<span class="glyphicon glyphicon-ban-circle"></span> Заблокировать Артикулы',
        [
            'title' => Yii::t('app', 'Заблокировать артикул и убрать из списка'),
            'class' => 'btn btn-warning',
            'onclick' => "sendForm('block-article-many')",
//            'data' => [
//                'confirm' => 'Вы уверены что хотите заблокировать артикул и убрать его из списка?',
//                'method' => 'post',
//            ],
            'data-toggle' => "tooltip",
        ]);
    ?>
<!---->
    <?=
        Html::button('<span class="fa fa-car"></span> Заблокировать автомобиль',
        [
            'title' => Yii::t('app', 'Заблокировать автомобиль и убрать из списка все позиции, связанные с ним'),
            'class' => 'btn btn-danger',
            'onclick' => "sendForm('block-car-many')",

            'data-toggle' => "tooltip",
        ]);
    ?>

    <?php $form = ActiveForm::begin(['id' => 'my-form']); ?>

        <?= Html::hiddenInput('articles',null,['id' => 'ids-data']) ?>

    <?php ActiveForm::end(); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover'],
        'rowOptions' => function ($data) {
            $options = [];
            if ($data['statistics']*2 < $data['balance']) {
                $options['class'] = 'warning';
            }
            $options['onclick'] = '$(this).find(".articles.item input[type=checkbox]").prop("checked",!$(this).find(".articles.item input[type=checkbox]").prop("checked"));';
            return $options;
        },
        'show' => [
//            'orderNumber',
//            'logId',
            'article',
            'requested',
            'statistics',
            'balance',
            'balanceProdFree',
            'createdAt',
            'checkbox',
//            'updatedAt',
//            'status',
//            'indexGroup',
        ],
        'columns' => [
//            'orderNumber',
//            'logId',
//            'carName',
            [
                'attribute' => 'carName',
                'label' => 'Автомобиль'
            ],
            [
                'attribute' => 'title',
                'label' => 'Продукт'
            ],
            [
                'attribute' => 'article',
                'label' => 'Артикул'
            ],
            [
                'attribute' => 'requested',
                'label' => 'Количество'
            ],
            [
                'attribute' => 'statistics',
                'label' => 'Статистика'
            ],
            [
                'attribute' => 'balance',
                'label' => 'Остаток'
            ],
            [
                'attribute' => 'balanceProdFree',
                'label' => 'На производстве'
            ],
            [
                'attribute' => 'createdAt',
                'format' => 'datetime',
                'label' => 'Дата создания'
            ],
//            'title',
//            'article',
//            'requested',
//            'statistics',
//            'balance',
//            'balanceProdFree',
//            'createdAt:datetime',
            [
                'attribute' => 'checkbox',
                'label' => 'Отметить',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::tag('div',
                        Html::checkbox("articles[]",
                            $checked = false,
                            ['value' => $data['article'],'style'=> 'z-index: 0;'])
                        .Html::tag('label',false),
                        ['class' => 'checkbox-custom checkbox-primary text-left articles item']
                    );
                },
            ],
//            'updatedAt',
//            'status',
//            'indexGroup',
        ],
    ]); ?>

</div>