<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 8/14/18
 * Time: 1:53 PM
 */
namespace backend\modules\logistics\modules\printDocuments\models;

use backend\helpers\ArticleHelper;
use backend\modules\logistics\models\Order;
use core\entities\logisticsShipment\ShipmentManager;
use Yii;
use yii\base\Model;

/**
 * Модель для генерации документа торг 12
 *
 * Class PrintTorg12
 * @package backend\modules\logistics\modules\printDocuments\models
 */
class PrintTorg12 extends Model
{
    /**
     * @var string  Реквизиты
     */
    public $requisites = 'Индивидуальный предприниматель Григорьев Дмитрий Львович, ИНН 440106801328, свидетельство 44 №631861 от 22.08.2011, 156005, Костромская обл, Костромской р-н, Кострома г, Войкова ул, дом № 40, кв.112, р/с 40802810800000001452, в банке ООО "КОСТРОМАСЕЛЬКОМБАНК", БИК 043469720, к/с 30101810200000000720';

    /**
     * @var array $shipment Номер отгрузки
     */
    public $shipments = [];

    /**
     * @var array $shipment Номер отгрузки
     */
    public $orders = [];

    /**
     * @var array $numbers Номер отгрузки
     */
    public $numbers = [];

    /**
     * @var float $course Курс валюты для перевода рублей в евро. По умолчанию - 60
     */
    public $course;

    /**
     * @var int $fullAmount Суммарная стоимость продукции
     */
    public $fullAmount = 0;

    /**
     * @var string $allowedPerson Отпуск разрешил
     */
    public $allowedPerson = 'ИП Григорьев Д.Л.';

    /**
     * @var string $allowedPerson Отгрузил
     */
    public $shippedPerson = 'ИП Григорьев Д.Л.';

    /**
     * @var string $bookkeeper Бухгалтер
     */
    public $bookkeeper = 'ИП Григорьев Д.Л.';

    /**
     * @var string $fullAmount Суммарная стоимость продукции
     */
    public $date;

    /**
     * @var string $consignee Графа - Грузополучатель
     */
    public $consignee;

    /**
     * @var string $payer Графа - Плательщик
     */
    public $payer;

    /**
     * @var string $number Номер документа по порядку
     */
    public $number;

    /**
     * @var string $shipmentHref Ссылка на отгрузку
     */
    public $shipmentHref = '';

    /**
     * @var int $countList Количество листов при распечатке товарной накладной
     */
    public $countList;

    /**
     * @var int $fullCount Сумма позиций по количеству
     */
    public $fullCount = 0;
    /**
     * @var ShipmentManager $_shipmentManager Менеджер для работы с отгрузками
     */
    private $_shipmentManager;

    /**
     * PrintTorg12 constructor.
     * @param array $config
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function __construct(array $config = [])
    {
        $this->date = \Yii::$app->formatter->asDate(time(),'dd.MM.yyyy');
        $this->_shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'shipment' => \Yii::t('app', '№ Отгрузки'),
            'box'      => \Yii::t('app', '№ Коробки')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipments'], 'each', 'rule' => ['integer']],
            [['orders'], 'each', 'rule' => ['integer']],
            [['numbers'], 'each', 'rule' => ['integer']],
            [['countList','payer','number','date','number','consignee','bookkeeper','shippedPerson','allowedPerson'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (empty($this->shipments) && empty($this->orders) && empty($this->numbers)){
            $this->addError('shipments','Накладная создается либо по отгрузке, либо по заказу, либо по номеру накладной');
            return false;
        }

        $count = 0;
        if (!empty($this->shipments)) $count++;
        if (!empty($this->orders))    $count++;
        if (!empty($this->numbers))   $count++;
        if ($count > 1){
            $this->addError('shipments','Данный документ можно генерировать либо по отгрузкам, либо по заказам, либо номеру накладной');
            $this->addError('orders','Данный документ можно генерировать либо по отгрузкам, либо по заказам, либо номеру накладной');
            $this->addError('numbers','Данный документ можно генерировать либо по отгрузкам, либо по заказам, либо номеру накладной');
            return false;
        }
        return parent::beforeValidate();
    }

    /**
     * Функция генерации данных под упаковочный лист
     *
     * @return array
     */
    private function getPrintShipmentList()
    {
        $result = [];
        if (!$this->validate() || empty($this->shipments)) return $result;

        $box = 0;
        $count = 0;

        foreach ($this->shipments as $shipmentID) {
            ++$box;
            $shipment = $this->_shipmentManager->findById($shipmentID);
            if (!$shipment) {
                throw new \DomainException('Отгрузка № ' . $shipmentID . ' не найдена в системе!');
            }

            $this->number = $this->_shipmentManager->getOfficialNumber($shipment);

            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);

            if ($this->_shipmentManager->getHandledAt($shipment)) {
                $this->date = \Yii::$app->formatter->asDate($this->_shipmentManager->getHandledAt($shipment),'dd.MM.yyyy');
            }

            //TODO Создать группировку по артикулам и ценам, чтобы отображение было уже сгруппированных позаций

            foreach ($orderEntries as $orderEntry) {
                $item['number']   = ++$count;
                $item['title']    = ArticleHelper::getInvoiceRusTitle($orderEntry->article);
                $item['article']  = $orderEntry->article;
                $item['packing']  = '';
                $item['quantity'] = 1;
                $item['pcs']      = 'Комп. / Set';
                $item['price']    = round($orderEntry->price,2);
                $item['amount']   = $item['price'] * $item['quantity'];
                $this->fullAmount += $item['amount'];
                $this->fullCount  += $item['quantity'];
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * Функция генерации данных под упаковочный лист
     *
     * @return array
     */
    private function getPrintOrdersList()
    {
        $result = [];
        if (!$this->validate() || empty($this->orders)) return $result;

        $box = 0;
        $count = 0;

        foreach ($this->orders as $orderId) {
            ++$box;
            $order = Order::find()->where(['team_id' => \Yii::$app->team->id])->andWhere(['source_innumber' => $orderId])->one();
            if (!$order) {
                throw new \DomainException('Заказ № ' . $orderId . ' не найден в системе!');
            }

            $orderEntries = $order->notDeletedOrderEntries;

            //TODO Создать группировку по артикулам и ценам, чтобы отображение было уже сгруппированных позаций

            foreach ($orderEntries as $orderEntry) {
                $item['number']   = ++$count;
                $item['title']    = ArticleHelper::getInvoiceRusTitle($orderEntry->article);
                $item['article']  = $orderEntry->article;
                $item['packing']  = '';
                $item['quantity'] = 1;
                $item['pcs']      = 'Комп. / Set';
                $item['price']    = round($orderEntry->price,2);
                $item['amount']   = $item['price'] * $item['quantity'];
                $this->fullAmount += $item['amount'];
                $this->fullCount  += $item['quantity'];
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * Функция генерации данных под упаковочный лист
     *
     * @return array
     */
    private function getPrintNumbersList()
    {
        $result = [];
        if (!$this->validate() || empty($this->numbers)) return $result;

        $box = 0;
        $count = 0;

        foreach ($this->numbers as $number) {
            $date = date("Y-m-d");
            //increment 1 month
            $mod_date = strtotime($date);
            $mod_date_before = strtotime('-1 year',$mod_date);
            $year =  date("Y",$mod_date) . "\n";
            $yearBefore =  date("Y",$mod_date_before) . "\n";

            ++$box;
            $shipment = $this->_shipmentManager->findByOfficialNumber($number,$year);
            if (!$shipment) {
                $shipment = $this->_shipmentManager->findByOfficialNumber($number,$yearBefore);
            }
            if (!$shipment) {
                throw new \DomainException('Отгрузка  по номеру ' . $number . ' не найдена в системе!');
            }

            $shipment = $shipment[0];

            $this->date = \Yii::$app->formatter->asDate($this->_shipmentManager->getHandledAt($shipment),'dd.MM.yyyy');

            $this->number = $this->_shipmentManager->getOfficialNumber($shipment);

            $this->shipmentHref = \Yii::$app->urlManager->createUrl(['/logistics/shipment-monitor/default/view','id' => $shipment->id]);

            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);

            //TODO Создать группировку по артикулам и ценам, чтобы отображение было уже сгруппированных позаций

            foreach ($orderEntries as $orderEntry) {
                $item['number']   = ++$count;
                $item['title']    = ArticleHelper::getInvoiceRusTitle($orderEntry->article);
                $item['article']  = $orderEntry->article;
                $item['packing']  = '';
                $item['quantity'] = 1;
                $item['pcs']      = 'Комп. / Set';
                $item['price']    = round($orderEntry->price,2);
                $item['amount']   = $item['price'] * $item['quantity'];
                $this->fullAmount += $item['amount'];
                $this->fullCount  += $item['quantity'];
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * Функция возвращает нужную функцию
     *
     * @return array
     */
    public function getPrintList()
    {
        if (!empty($this->shipments))
        {
            return $this->getPrintShipmentList();
        }
        if (!empty($this->orders))
        {
            return $this->getPrintOrdersList();
        }
        if (!empty($this->numbers))
        {
            return $this->getPrintNumbersList();
        }
        return [];
    }
}