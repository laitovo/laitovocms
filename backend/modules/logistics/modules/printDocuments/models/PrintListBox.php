<?php
namespace backend\modules\logistics\modules\printDocuments\models;

use backend\helpers\ArticleHelper;
use core\entities\logisticsShipment\ShipmentManager;
use yii\base\Model;

/**
 * Класс, который работает с данными печати упаковочного листа
 *
 * Class PrintListBox
 * @package backend\modules\logistics\modules\printDocuments\models
 */
class PrintListBox extends Model
{
    /**
     * @var integer $shipment Номер отгрузки
     */
    public $shipment;

    /**
     * @var integer $shipment Номер отгрузки
     */
    public $date;

    /**
     * @var integer $box Номер коробки
     */
    public $box;

    /**
     * @var ShipmentManager $_shipmentManager Менеджер для работы с отгрузками
     */
    private $_shipmentManager;

    /**
     * PrintListBox constructor.
     * @param array $config
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function __construct(array $config = [])
    {
        $this->_shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'shipment' => \Yii::t('app', '№ Отгрузки'),
            'box'      => \Yii::t('app', '№ Коробки')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipment','box'], 'integer'],
            [['shipment','box'], 'required'],
        ];
    }

    /**
     * Функция генерации данных под упаковочный лист
     *
     * @return array
     */
    public function getPrintList()
    {
        $result = [];
        if (!$this->validate()) return $result;

        $shipment = $this->_shipmentManager->findById($this->shipment);
        if (!$shipment) return $result;

        $date       = $this->_shipmentManager->getShipmentDate($shipment);
        $this->date = date('y-m-d',$date);

        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        $count = 0;

        foreach ($orderEntries as $orderEntry) {
//            $item['number']   = ++$count;
//            $item['title']    = ArticleHelper::getInvoiceTitle($orderEntry->article);
//            $item['article']  = $orderEntry->article;
//            $item['quantity'] = 1;
//            $item['box']      = $this->box;
//            $result[] = $item;

            $article = $orderEntry->article;
            $key     = $article;
            if (isset($result[$key])) {
                $result[$key]['quantity']++;
            } else {
                $result[$key] = [
                    'number'   => ++$count,
                    'title'    => ArticleHelper::getInvoiceTitle($article),
                    'article'  => $article,
                    'quantity' => 1,
                    'box'      => $this->box,
                ];
            }
        }

        return $result;
    }
}