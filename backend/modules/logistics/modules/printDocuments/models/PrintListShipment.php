<?php
namespace backend\modules\logistics\modules\printDocuments\models;

use backend\helpers\ArticleHelper;
use core\entities\logisticsShipment\ShipmentManager;
use yii\base\Model;

/**
 * Класс, который работает с данными печати упаковочного листа
 *
 * Class PrintListBox
 * @package backend\modules\logistics\modules\printDocuments\models
 */
class PrintListShipment extends Model
{
    /**
     * @var array $shipment Номер отгрузки
     */
    public $shipments = [];

    /**
     * @var ShipmentManager $_shipmentManager Менеджер для работы с отгрузками
     */
    private $_shipmentManager;

    /**
     * PrintListBox constructor.
     * @param array $config
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function __construct(array $config = [])
    {
        $this->_shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'shipment' => \Yii::t('app', '№ Отгрузки'),
            'box'      => \Yii::t('app', '№ Коробки')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipments'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * Функция генерации данных под упаковочный лист
     *
     * @return array
     */
    public function getPrintList()
    {
        $result = [];
        if (!$this->validate()) return $result;

        $box = 0;
        $count = 0;

        foreach ($this->shipments as $shipmentID) {
            ++$box;
            $shipment = $this->_shipmentManager->findById($shipmentID);
            if (!$shipment) { var_dump($this->shipments); die;}

            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);

            foreach ($orderEntries as $orderEntry) {
//                $item['number']   = ++$count;
//                $item['title']    = ArticleHelper::getInvoiceTitle($orderEntry->article);
//                $item['article']  = $orderEntry->article;
//                $item['quantity'] = 1;
//                $item['box']      = $box;
//                $result[] = $item;

                $article = $orderEntry->article;
                $key     = $article . '_' . $box;
                if (isset($result[$key])) {
                    $result[$key]['quantity']++;
                } else {
                    $result[$key] = [
                        'number'   => ++$count,
                        'title'    => ArticleHelper::getInvoiceTitle($article),
                        'article'  => $article,
                        'quantity' => 1,
                        'box'      => $box,
                    ];
                }
            }
        }

        return $result;
    }
}