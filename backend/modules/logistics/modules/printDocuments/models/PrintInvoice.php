<?php
namespace backend\modules\logistics\modules\printDocuments\models;

use backend\helpers\ArticleHelper;
use backend\modules\logistics\models\OrderEntry;
use core\entities\logisticsShipment\ShipmentManager;
use obmen\models\laitovo\Cars;
use yii\base\Model;

/**
 * Класс, который работает с данными печати инвойса
 *
 * Class PrintInvoice
 * @package backend\modules\logistics\modules\printDocuments\models
 */
class PrintInvoice extends Model
{
    /**
     * @var array $shipment Номер отгрузки
     */
    public $shipments = [];

    /**
     * @var float $course Курс валюты для перевода рублей в евро. По умолчанию - 60
     */
    public $course;

    /**
     * @var int $fullAmount Суммарная стоимость продукции
     */
    public $fullAmount = 0;
    /**
     * @var ShipmentManager $_shipmentManager Менеджер для работы с отгрузками
     */
    private $_shipmentManager;

    /**
     * PrintInvoice constructor.
     * @param array $config
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function __construct(array $config = [])
    {
        $this->_shipmentManager = \Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'shipment' => \Yii::t('app', '№ Отгрузки'),
            'box'      => \Yii::t('app', '№ Коробки')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipments'], 'each', 'rule' => ['integer']],
            [['course'], 'double'],
        ];
    }

    /**
     * Функция генерации данных под упаковочный лист
     *
     * @return array
     */
    public function getPrintList()
    {
        $result = [];
        if (!$this->validate()) return $result;

        $count = 0;

        $groups = [];
        $solo = [];

        foreach ($this->shipments as $shipmentID) {
            $shipment = $this->_shipmentManager->findById($shipmentID);
            if (!$shipment) {
                throw new \DomainException('Отгрузка № ' . $shipmentID . ' не найдена в системе!');
            }

            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);

            foreach ($orderEntries as $orderEntry) {
                /**
                 * @var $orderEntry OrderEntry
                 */
                $index = $orderEntry->setIndex;
                if ($index && ($order = $orderEntry->order) && $order->remoteStorage) {
                    $orderId = $orderEntry->order_id ?:0;
                    $key = $orderId . '_' .$index;
                    $groups[$key][] = $orderEntry;
                }else{
                    $solo[] = $orderEntry;
                }
            }
        }

        foreach ($solo as $orderEntry) {
            $article = $orderEntry->article;
            /**
             * @var $car Cars
             */
            $car = ArticleHelper::getCarModel($orderEntry->article);
            if (!$car) {
                $title = ArticleHelper::getInvoiceRusTitle($orderEntry->article) . ' / ' . ArticleHelper::getInvoiceTitle($orderEntry->article);
            }else {
                $title = "Защитные экраны для автомобильных окон / Protective screens for car windows<br>{$car->fullEnName}<br>({$article})";
            }
            $price   = round($orderEntry->price / ($this->course ? : 60),2);
            $key     = $article . '_' . $price;
            if (isset($result[$key])) {
                $result[$key]['quantity']++;
                $result[$key]['amount'] += $price;
            } else {
                $result[$key] = [
                    'number'   => ++$count,
                    'title'    => $title,
                    'packing'  => '',
                    'quantity' => 1,
                    'pcs'      => 'Комп. / Set',
                    'price'    => $price,
                    'amount'   => $price,
                ];
            }
            $this->fullAmount += $price;
        }

        foreach ($groups as $group) {
            $price = 0;
            $car   = null;
            $articles = [];
            foreach ($group as $orderEntry) {
                /**
                 * @var $car Cars
                 */
                if (!$car)
                    $car = ArticleHelper::getCarModel($orderEntry->article);
                $singlePrice = round($orderEntry->price / ($this->course ? : 60),2);
                $price += $singlePrice;
                $articles[] = $orderEntry->article;
            }
            $key  = implode('_',$articles) . '_' . $price;
            $title = "Защитные экраны для автомобильных окон / Protective screens for car windows<br>{$car->fullEnName}<br>(SET: " . implode(', ',$articles).")";

            if (isset($result[$key])) {
                $result[$key]['quantity']++;
                $result[$key]['amount'] += $price;
            } else {
                $result[$key] = [
                    'number'   => ++$count,
                    'title'    => $title,
                    'packing'  => 'Картонная коробка / Carton',
                    'quantity' => 1,
                    'pcs'      => 'Комп. / Set',
                    'price'    => $price,
                    'amount'   => $price,
                ];
            }
            $this->fullAmount += $price;
        }

        return $result;
    }
}