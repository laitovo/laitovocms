<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 8/14/18
 * Time: 9:46 AM
 */

namespace backend\modules\logistics\modules\printDocuments\controllers;

use backend\modules\logistics\modules\printDocuments\models\PrintInvoice;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Контроллер для работы с документом "Инвойс на отправку в европу"
 *
 * Class PrintInvoiceController
 * @package backend\modules\logistics\modules\printDocuments\controllers
 */
class PrintInvoiceController extends Controller
{
    /**
     * Дейсвие генерирует интерфейс для генерации документа
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('_invoice');
    }

    /**
     * Действие проверяет, возможно ли сформировать документ из заданных данных (Данные проходят вализацию).
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionValidate()
    {
        if (!\Yii::$app->request->isAjax) {
            throw new NotFoundHttpException(\Yii::t('yii', 'Page not found.'));
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        $response['status'] = 'success';
        $response['message'] = 'Документ успешно сформирован!';

        try{
            $model = new PrintInvoice();
            $model->setAttributes(\Yii::$app->request->post());
            if (!$model->validate()) {

                $result = [];
                foreach ($model->getErrors() as $attribute => $error) {
                    foreach ($error as $message) {
                        $result[] = $message;
                    }
                }
                $response['status'] = 'error';
                $response['message'] = implode(' ', $result);
            }
        }catch (\DomainException $exception){
            $response['status'] = 'error';
            $response['message'] = $exception->getMessage();
        }

        return $response;
    }

    /**
     * Действие отдает печатную форму для документа
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionPrint()
    {
        if (!\Yii::$app->request->isAjax) {
            throw new NotFoundHttpException(\Yii::t('yii', 'Page not found.'));
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [
            'status'  => 'success',
            'message' => 'Документ распечатан',
            'html'    => false
        ];

        $model = new PrintInvoice();
        $model->setAttributes(\Yii::$app->request->post());
        if (!$model->validate()) {
            $result = [];
            foreach ($model->getErrors () as $attribute => $error)
            {
                foreach ($error as $message)
                {
                    $result[] = $message;
                }
            }
            $response['status'] = 'error';
            $response['message'] = implode(' ',$result);
        }else{
            $response['html'] = $this->renderPartial('_invoice_print',['elements' => $model->getPrintList(),'fullAmount' => $model->fullAmount]);
        }

        return $response;
    }

    /**
     * Действие отдает сформированный документ в формате excel
     *
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionExcel()
    {
        $model = new PrintInvoice();

        $model->setAttributes(\Yii::$app->request->post());

        $pExcel = new \PHPExcel();
        $pExcel->setActiveSheetIndex(0);
        $aSheet = $pExcel->getActiveSheet();
        // Ориентация страницы и  размер листа
        $aSheet->getPageSetup()
            ->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $aSheet->getPageSetup()
            ->SetPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        // Поля документа
        $aSheet->getPageMargins()->setTop(1);
        $aSheet->getPageMargins()->setRight(0.75);
        $aSheet->getPageMargins()->setLeft(0.75);
        $aSheet->getPageMargins()->setBottom(1);
        // Название листа
        $aSheet->setTitle('Инвойс');
        // Настройки шрифта
        $pExcel->getDefaultStyle()->getFont()->setName('Arial');
        $pExcel->getDefaultStyle()->getFont()->setSize(10);
        //Указываем ширину колонки
        $aSheet->getColumnDimension('A')->setWidth(3);
        $aSheet->getColumnDimension('B')->setWidth(100);
        $aSheet->getColumnDimension('C')->setWidth(10);
        $aSheet->getColumnDimension('D')->setWidth(10);
        $aSheet->getColumnDimension('E')->setWidth(10);
        $aSheet->getColumnDimension('F')->setWidth(10);
        $aSheet->getColumnDimension('G')->setWidth(10);

        $borderArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $line = 1;
        $aSheet->getRowDimension($line)->setRowHeight(35);
        $aSheet->getStyle('A' .$line.':G'.$line)->getAlignment()->setWrapText(true);
        $aSheet->getStyle('A' .$line.':G'.$line)->getFont()->setBold( true );
        $aSheet->getStyle('A' .$line.':G'.$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $aSheet->getStyle('A' .$line.':G'.$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $aSheet->setCellValue('A'.$line, '№'  );
        $aSheet->setCellValue('B'.$line, 'Наименование / Title');
        $aSheet->setCellValue('C'.$line, 'Упаковка / Packing'  );
        $aSheet->setCellValue('D'.$line, 'Кол-во / Quantity' );
        $aSheet->setCellValue('E'.$line, 'Ед / Pcs'    );
        $aSheet->setCellValue('F'.$line, 'Цена (EURO) / Price'   );
        $aSheet->setCellValue('G'.$line, 'Сумма (EURO) / Amount'  );

        foreach ($model->getPrintList() as $element) {
            $line++;
            $aSheet->getRowDimension($line)->setRowHeight(30);
            $aSheet->getStyle('A' .$line.':G'.$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $aSheet->getStyle('A' .$line.':G'.$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $aSheet->getStyle('F' .$line.':G'.$line)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

            $aSheet->getStyle('B'.$line)->getAlignment()->setWrapText(true);

            $aSheet->setCellValue('A'.$line, $element['number']   );
            $aSheet->setCellValue('B'.$line, str_ireplace('<br>',"\r\n",$element['title']));
            $aSheet->setCellValue('C'.$line, $element['packing']  );
            $aSheet->setCellValue('D'.$line, $element['quantity'] );
            $aSheet->setCellValue('E'.$line, $element['pcs']      );
            $aSheet->setCellValue('F'.$line, number_format($element['price'],2) );
            $aSheet->setCellValue('G'.$line, number_format($element['amount'],2));
        }

        $line++;
        $aSheet->mergeCells('A' .$line.':F'.$line);
        $aSheet->setCellValue('A'.$line, 'Итого / Total:');
        $aSheet->getStyle('A' .$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $aSheet->getStyle('A' .$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $aSheet->setCellValue('G'.$line, number_format($model->fullAmount,2));
        $aSheet->getStyle('G' .$line)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $aSheet->getStyle('G' .$line)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $aSheet->getStyle('G' .$line)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

        $aSheet->getStyle('A' .$line.':G'.$line)->getFont()->setBold( true );

        $aSheet->getStyle('A1:G'.$line)->applyFromArray($borderArray);
        unset($styleArray);

        header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition:attachment;filename="simple.xlsx"');
        $objWriter = new \PHPExcel_Writer_Excel2007($pExcel);
        $objWriter->save('php://output');
    }
}