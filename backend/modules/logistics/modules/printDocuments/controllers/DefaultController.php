<?php

namespace backend\modules\logistics\modules\printDocuments\controllers;

use yii\web\Controller;

/**
 * Class DefaultController
 * @package backend\modules\logistics\modules\printDocuments\controllers
 */
class DefaultController extends Controller
{
    public function actionDefault()
    {
        return $this->render('default');
    }
}