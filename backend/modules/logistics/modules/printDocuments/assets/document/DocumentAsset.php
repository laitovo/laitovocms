<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\modules\logistics\modules\printDocuments\assets\document;

use yii\web\AssetBundle;
use yii\web\View;

class DocumentAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/logistics/modules/printDocuments/assets/document/assets';

    public $js = [
        'js/print.js',
        'js/excel.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public $jsOptions = ['position' => View::POS_HEAD];

//    public $publishOptions = [
//        'forceCopy'=>true,
//    ];
}
