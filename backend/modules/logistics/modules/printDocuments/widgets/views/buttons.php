<?php
use yii\helpers\Url;

\backend\modules\logistics\modules\printDocuments\assets\document\DocumentAsset::register($this);

?>
<div class="form-group">
    <?php if ($print):?>
    <?= \yii\helpers\Html::button('Печать',['class' => 'btn btn-default btn-margin','onclick' => 'myPrint("'. Url::to("validate") .'","'. Url::to("print") .'")']);?>
    <?php endif; ?>
    <?php if ($excel):?>
    <?= \yii\helpers\Html::button('Excel',['class' => 'btn btn-default btn-margin','onclick' => 'excel("'. Url::to("validate") .'","'. Url::to("excel") .'","'. $name .'")']);?>
    <?php endif; ?>
</div>
