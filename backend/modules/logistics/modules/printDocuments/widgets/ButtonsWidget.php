<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 8/14/18
 * Time: 10:52 AM
 */

namespace backend\modules\logistics\modules\printDocuments\widgets;

use yii\base\Widget;

class ButtonsWidget extends Widget
{
    /**
     * @var string $documentName Наименование документа при генерации
     */
    public $documentName;

    /**
     * @var boolean $excel Возможность распечатать
     */
    public $excel = true;

    /**
     * @var boolean $print Возможность распечатать
     */
    public $print = true;

    public function init()
    {
        parent::init();
        if ($this->documentName === null) {
            $this->documentName = 'document';
        }
    }

    public function run()
    {
        return $this->render('buttons',['name' => $this->documentName,'excel' => $this->excel,'print' => $this->print]);
    }
}