<?php

/**
 * @var $fullCount int
 * @var $fullAmount float
 * @var $model \backend\modules\logistics\modules\printDocuments\models\PrintTorg12
 * @var $group_by_print bool
 */

?>


<style>
    .bold {
        font-weight: bold;
    }

    * {
        font-size: 95% !important;
    }

    .center{
        text-align: center;
    }

    .b, .bordered td {
        border: 1px solid silver;
    }

    td.nb {
        border: 0px;
    }

    .bordered td {
        text-align: center;
    }

    .r {
        text-align: right !important;
    }

    .l {
        text-align: left;
    }

    td {
        padding: 5pt;
    }
    .sm {
        font-size: 50pt;
    }

    .tb {
        border-top: 1px solid silver;
    }

    .bb {
        border-bottom: 1px solid silver;
    }

    td {
        vertical-align: top;
    }
</style>

<p align="right">
    Унифицированная форма № ТОРГ- 12 Утверждена постановлением Госкомстата России от 25.12.98 № 132
</p>

<table cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td valign="top">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td colspan="2" align="center">
                        <?= $model->requisites ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="sm tb">
                        организация – грузоотправитель, адрес, номер телефона, факса, банковские реквизиты
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="2" align="center"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="sm tb">
                        структурное подразделение
                    </td>
                </tr>

                <tr>
                    <td width="120">
                        Грузополучатель:
                    </td>
                    <td>
                        <?= $model->consignee ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="sm tb">
                        наименование организации, адрес, номер телефона, банковские реквизиты
                    </td>
                </tr>

                <tr>
                    <td valign="top">
                        Поставщик:
                    </td>
                    <td>
                        <?= $model->requisites ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="sm tb">
                        наименование организации, адрес, номер телефона, банковские реквизиты
                    </td>
                </tr>

                <tr>
                    <td>
                        Плательщик:
                    </td>
                    <td><?= $model->payer ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="sm tb">
                        наименование организации, адрес, номер телефона, банковские реквизиты
                    </td>
                </tr>
                <tr>
                    <td>
                        Основание:
                    </td>
                    <td>
                        Основной договор
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="sm tb">
                        наименование документа (договор, контракт, заказ-наряд)
                    </td>
                </tr>


            </table>
        </td>
        <td valign="top" width="200">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2"></td>
                    <td align="center">Код</td>
                </tr>
                <tr>
                    <td colspan="2" align="right">Форма по ОКУД</td>
                    <td class="b" width="200">
                        0330212
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">по ОКПО</td>
                    <td class="b" width="200">
                        0178851876
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">Вид деятельности по ОКДП</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">по ОКПО</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">по ОКПО</td>
                    <td class="b" width="200">
                        0178851876
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">по ОКПО</td>
                    <td class="b" width="200"></td>
                </tr>

                <tr>
                    <td colspan="2" class="b" align="right">номер</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" class="b" align="right">дата</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" class="b" align="right">номер</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td align="right">Транспортная накладная</td>
                    <td class="b" align="right">дата</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">Вид операции</td>
                    <td class="b" width="200"></td>
                </tr>

            </table>
        </td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="middle">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td></td>
                    <td class="b">
                        Номер документа
                    </td>
                    <td class="b">
                        Дата составления
                    </td>
                </tr>
                <tr>
                    <td>ТОВАРНАЯ НАКЛАДНАЯ</td>
                    <td class="b center">
                        <?= $model->number?>
                    </td>
                    <td class="b center">
                        <?= $model->date?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0"  width="100%" class="bordered">
    <tr>
        <td rowspan="2" style="width: 5%">Но-мер по по-рядку</td>
        <td colspan="2" style="width: 40%">Товар</td>
        <td colspan="2" style="width: 5%">Единица измерения</td>
        <td rowspan="2" style="width: 5%">Вид упаков ки</td>
        <td colspan="2" style="width: 10%">Количество (масса)</td>
        <td rowspan="2" style="width: 5%">Масса брутто</td>
        <td rowspan="2" style="width: 5%">Коли-чество (масса нетто)</td>
        <td rowspan="2" style="width: 10%">Цена руб. коп.</td>
        <td rowspan="2" style="width: 10%">Сумма без учета НДС, руб. коп</td>
        <td colspan="2" style="width: 15%">НДС</td>
        <td rowspan="2" style="width: 10%">Сумма с учетом НДС, руб. коп</td>
    </tr>
    <tr>
        <td>наименование, характеристика, сорт, артикул товара</td>
        <td>код</td>
        <td >наи-мено-вание</td>
        <td>Код по ОКЕИ</td>
        <td>в одном месте</td>
        <td>мест, штук</td>
        <td>ставка,%</td>
        <td>сумма, руб. коп</td>
    </tr>

    <?php $i = 1 ?>
    <?php foreach ($elements as $element):?>
    <tr>
        <td><?= $i ?></td>
        <td align="left"><?= $element['title']?></td>
        <td ><?= $element['article']?></td>
        <td>шт.</td>
        <td>796</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><?= number_format($element['quantity'],3)?></td>
        <td><?= number_format($element['price'],2) ?></td>
        <td><?= !$group_by_print ? number_format($element['amount'],2, '.', ' ') : number_format($element['amount'] * $element['quantity'],2, '.', ' ') ?></td>
        <td>Без НДС</td>
        <td></td>
        <td><?= !$group_by_print ? number_format($element['amount'],2, '.', ' ') : number_format($element['amount'] * $element['quantity'],2, '.', ' ') ?></td>
    </tr>
    <?php $i++ ?>
    <?endforeach;?>
    <tr>
        <td colspan="7" class="r nb" align="right">
            Всего по накладной:
        </td>
        <td></td>
        <td></td>
        <td><?= number_format($fullCount,3)?></td>
        <td><?= number_format($fullAmount,2) ?></td>
        <td><?= number_format($fullAmount,2) ?></td>
        <td></td>
        <td></td>
        <td><?= number_format($fullAmount,2) ?></td>
    </tr>
</table>


<table cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td width="200">
            Товарная накладная имеет приложение на
        </td>
        <td><?= $model->countList?></td>
        <td width="300">Листах</td>
    </tr>
    <tr>
        <td></td>
        <td align="center" class="sm tb">
        </td>
    </tr>
    <tr>
        <td>
            и содержит
        </td>
        <td><?= --$i ?></td>
        <td>порядковых номеров записей</td>
    </tr>
    <tr>
        <td></td>
        <td align="center" class="sm tb">
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
        <td width="150"></td>
        <td width="150">
            Масса груза (нетто)
        </td>
        <td></td>
        <td width="20"></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td align="center" class="sm tb">
            прописью
        </td>
        <td width="20"></td>
        <td class="b" width="300">

        </td>
    </tr>
    <tr>
        <td></td>
        <td width="200">
            Масса груза (брутто)
        </td>
        <td></td>
        <td width="20"></td>
        <td class="b" width="300">

        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td align="center" class="sm tb">
            прописью
        </td>
        <td width="20"></td>
        <td width="20"></td>
    </tr>
</table>

<table width="100%">
    <tr>
        <td width="60%">
            <table cellspacing="0" cellpadding="10" border="0" width="100%">
                <tr>
                    <td width="350">
                        Приложение (паспорта, сертификаты, и т.п.) на
                    </td>
                    <td></td>
                    <td width="50">листах</td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td class="tb sm" align="center">прописью</td>
                    <td></td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        Всего отпущено на сумму
                    </td>
                    <td class="bold"><?= \backend\modules\logistics\modules\printDocuments\helpers\WriteSum::num2str($fullAmount)?></td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td class="tb sm" align="center" colspan="2">прописью</td>
                </tr>
            </table>


            <table width="100%" cellspacing="20">
                <tr>
                    <td width="20%">
                        Отпуск разрешил
                    </td>
                    <td width="20%"></td>
                    <td></td>
                    <td align="center"><?= $model->allowedPerson?></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center">должность</td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
                <tr>
                    <td width="20%">
                        Главный (старший) бухгалтер
                    </td>
                    <td></td>
                    <td></td>
                    <td align="center"><?= $model->bookkeeper?></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td></td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
                <tr>
                    <td width="20%">
                        Отпуск груза произвел
                    </td>
                    <td></td>
                    <td></td>
                    <td align="center"><?= $model->shippedPerson?></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center">должность</td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
            </table>


            <table width="100%" cellspacing="20">
                <tr>
                    <td width="20%" align="center">
                        М.П.
                    </td>
                    <td width="30%"></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center"></td>
                </tr>
            </table>

        </td>
        <td>
            <table width="100%">
                <tr>
                    <td width="20%">
                        По доверенности №
                    </td>
                    <td width="30%" class="bb"></td>
                    <td>
                        от
                    </td>
                    <td class="bb" width="40%"></td>
                </tr>
            </table>

            <table width="100%">
                <tr>
                    <td width="20%">
                        выданной
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center" colspan="2"></td>
                </tr>
                <tr>
                    <td width="20%">

                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center" colspan="2">кем, кому (организация, должность, фамилия, и.,
                        о.)
                    </td>
                </tr>
            </table>

            <table width="100%" cellspacing="20">
                <tr>
                    <td width="20%">
                        Груз принял
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center">должность</td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
                <tr>
                    <td width="20%">
                        Груз получил грузополучатель
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center">должность</td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
            </table>
        </td>
    </tr>
</table>