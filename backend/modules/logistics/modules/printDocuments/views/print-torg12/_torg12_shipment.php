<?php
$this->render('@backendViews/logistics/views/menu');

//custom css rules
$css = <<<CSS
    .btn-margin {margin-right:5px}
CSS;
$this->registerCss($css);
//custom css rules
?>
<?= \yii\helpers\Html::a('Вернутся назад',[''], ['class' => 'btn btn-default btn-margin']);?>
<hr>
<?= \yii\helpers\Html::beginForm([''],'post',['id' => 'my-form']);?>
<!--Оотгрузки-->
<div class="row shipments">
    <div class="col-md-3 col-sm-4 col-xs-6 box">
        <div class="input-group">
            <?= \yii\helpers\Html::input('text','shipments[]',null,['class' => 'form-control','placeholder' => 'отгрузка №']);?>
            <span class="input-group-addon btn btn-danger btn-sm" onclick="$(this).closest('.box').remove();" title="Удалить позицию" ><span class="fa fa-minus-square"></span></span>
        </div>
    </div>
</div>
<hr>

<?= \yii\helpers\Html::submitButton('Предварительный просмотр',['class' => 'btn btn-default btn-margin']);?>

<?= \yii\helpers\Html::endForm();?>
