<?php
$this->render('@backendViews/logistics/views/menu');

//custom css rules
$css = <<<CSS
    .btn-margin {margin-right:5px}
CSS;
$this->registerCss($css);
//custom css rules
?>
<h3>Распечатка накладной Торг 12</h3>
    <hr>
    <?= \yii\helpers\Html::a('Из заказа',['','type' => 'order'], ['class' => 'btn btn-default btn-margin']);?>
    <?= \yii\helpers\Html::a('Из отгрузки',['','type' => 'shipment'], ['class' => 'btn btn-default btn-margin']);?>
    <?= \yii\helpers\Html::a('По номеру',['','type' => 'number'], ['class' => 'btn btn-default btn-margin']);?>