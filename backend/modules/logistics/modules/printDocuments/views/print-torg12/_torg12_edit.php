<?php

/**
 * @var $fullCount int
 * @var $fullAmount float
 * @var $shipmentHref string
 */

use yii\helpers\Html;

$this->render('@backendViews/logistics/views/menu');
?>
<?= \backend\modules\logistics\modules\printDocuments\widgets\ButtonsWidget::widget(['documentName' => 'torg12','excel' => false])?>
<hr>
<?if ($shipmentHref !== '') :?>
    <a href="<?=$shipmentHref?>" target="_blank">Ссылка на отгрузку >>></a>
    <hr>
<?endif;?>
<?= Html::a('Вернутся назад',[''], ['class' => 'btn btn-default btn-margin']);?>

<?= Html::beginForm([''],'post',['id' => 'my-form']);?>

<div class="form-group" style="margin-top: 20px;">
    <label for="group_by_print_id">
        <?= Html::checkbox('group_by_print', true, ['id' => 'group_by_print_id']) ?>
        Группировать при печати
    </label>
</div>

<?foreach ($model->shipments as $shipment):?>
    <?= \yii\helpers\Html::hiddenInput('shipments[]',$shipment);?>
<?endforeach;?>
<?foreach ($model->orders as $order):?>
    <?= \yii\helpers\Html::hiddenInput('orders[]',$order);?>
<?endforeach;?>
<?foreach ($model->numbers as $number1):?>
    <?= \yii\helpers\Html::hiddenInput('numbers[]',$number1);?>
<?endforeach;?>
<style>
    .bold {
        font-weight: bold;
    }

    .edit-table {
        font-size: 90% !important;
    }

    .b, .bordered td {
        border: 1px solid silver;
    }

    td.nb {
        border: 0px;
    }

    .bordered td {
        text-align: center;
    }

    .r {
        text-align: right !important;
    }

    .l {
        text-align: left;
    }

    td {
        padding: 5pt;
    }
    .sm {
        font-size: 5pt;
    }

    .tb {
        border-top: 1px solid silver;
    }

    .bb {
        border-bottom: 1px solid silver;
    }

    td {
        vertical-align: top;
    }
</style>
<div class="edit-table">

<p align="right">
    Унифицированная форма № ТОРГ- 12 Утверждена постановлением Госкомстата России от 25.12.98 № 132
</p>

<table cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td valign="top">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td colspan="2" align="center">
                        <?= $model->requisites ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="sm tb">
                        организация – грузоотправитель, адрес, номер телефона, факса, банковские реквизиты
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="2" align="center"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="sm tb">
                        структурное подразделение
                    </td>
                </tr>

                <tr>
                    <td width="120">
                        Грузополучатель:
                    </td>
                    <td>
                        <textarea name="consignee" style="width: 100%" placeholder="Грузополучатель"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="sm tb">
                        наименование организации, адрес, номер телефона, банковские реквизиты
                    </td>
                </tr>

                <tr>
                    <td valign="top">
                        Поставщик:
                    </td>
                    <td>
                        <?= $model->requisites ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="sm tb">
                        наименование организации, адрес, номер телефона, банковские реквизиты
                    </td>
                </tr>

                <tr>
                    <td>
                        Плательщик:
                    </td>
                    <td>
                        <textarea name="payer" style="width: 100%" placeholder="Плательщик"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="sm tb">
                        наименование организации, адрес, номер телефона, банковские реквизиты
                    </td>
                </tr>
                <tr>
                    <td>
                        Основание:
                    </td>
                    <td>
                        Основной договор
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="sm tb">
                        наименование документа (договор, контракт, заказ-наряд)
                    </td>
                </tr>


            </table>
        </td>
        <td valign="top" width="250">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2"></td>
                    <td align="center">Код</td>
                </tr>
                <tr>
                    <td colspan="2" align="right">Форма по ОКУД</td>
                    <td class="b" width="200">
                        0330212
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">по ОКПО</td>
                    <td class="b" width="200">
                        0178851876
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">Вид деятельности по ОКДП</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">по ОКПО</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">по ОКПО</td>
                    <td class="b" width="200">
                        0178851876
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">по ОКПО</td>
                    <td class="b" width="200"></td>
                </tr>

                <tr>
                    <td colspan="2" class="b" align="right">номер</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" class="b" align="right">дата</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" class="b" align="right">номер</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td align="right">Транспортная накладная</td>
                    <td class="b" align="right">дата</td>
                    <td class="b" width="200"></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">Вид операции</td>
                    <td class="b" width="200"></td>
                </tr>

            </table>
        </td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="middle">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td></td>
                    <td class="b">
                        Номер документа
                    </td>
                    <td class="b">
                        Дата составления
                    </td>
                </tr>
                <tr>
                    <td>ТОВАРНАЯ НАКЛАДНАЯ</td>
                    <td class="b">
                        <input type="text" name="number" <?= $number? ' value="'.$number.'" disabled="disabled"' :''?> width="100%">
                    </td>
                    <td class="b">
                        <input type="text" name="date"  value="<?= $date?>" disabled="disabled" width="100%">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0"  width="100%" class="bordered">
    <tr>
        <td rowspan="2" style="width: 5%">Но-мер по по-рядку</td>
        <td colspan="2" style="width: 40%">Товар</td>
        <td colspan="2" style="width: 5%">Единица измерения</td>
        <td rowspan="2" style="width: 5%">Вид упаков ки</td>
        <td colspan="2" style="width: 10%">Количество (масса)</td>
        <td rowspan="2" style="width: 5%">Масса брутто</td>
        <td rowspan="2" style="width: 5%">Коли-чество (масса нетто)</td>
        <td rowspan="2" style="width: 10%">Цена руб. коп.</td>
        <td rowspan="2" style="width: 10%">Сумма без учета НДС, руб. коп</td>
        <td colspan="2" style="width: 15%">НДС</td>
        <td rowspan="2" style="width: 10%">Сумма с учетом НДС, руб. коп</td>
    </tr>
    <tr>
        <td>наименование, характеристика, сорт, артикул товара</td>
        <td>код</td>
        <td >наи-мено-вание</td>
        <td>Код по ОКЕИ</td>
        <td>в одном месте</td>
        <td>мест, штук</td>
        <td>ставка,%</td>
        <td>сумма, руб. коп</td>
    </tr>

    <tr>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
        <td>9</td>
        <td>10</td>
        <td>11</td>
        <td>12</td>
        <td>13</td>
        <td>14</td>
        <td>15</td>
    </tr>

    <?php foreach ($elements as $element):?>
        <tr>
            <td><?= $element['number']?></td>
            <td align="left"><?= $element['title']?></td>
            <td ><?= $element['article']?></td>
            <td>шт.</td>
            <td>796</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td></td>
            <td><?= number_format($element['quantity'],3)?></td></td>
            <td><?= number_format($element['price'],2) ?></td>
            <td><?= number_format($element['amount'],2) ?></td>
            <td></td>
            <td></td>
            <td><?= number_format($element['amount'],2) ?></td>
        </tr>
    <?endforeach;?>
    <tr>
        <td colspan="7" class="r nb" align="right">
            Всего по накладной:
        </td>
        <td></td>
        <td></td>
        <td class="bold"><?= number_format($fullCount,3)?></td>
        <td class="bold"><?= number_format($fullAmount,2) ?></td>
        <td class="bold"><?= number_format($fullAmount,2) ?></td>
        <td></td>
        <td></td>
        <td class="bold"><?= number_format($fullAmount,2) ?></td>
    </tr>
</table>


<table cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td width="200">
            Товарная накладная имеет приложение на
        </td>
        <td>
            <input type="text" name="countList" width="100%">
        </td>
        <td width="300">Листах</td>
    </tr>
    <tr>
        <td></td>
        <td align="center" class="sm tb">
        </td>
    </tr>
    <tr>
        <td>
            и содержит
        </td>
        <td>
            <?= $fullCount?>
        </td>
        <td>порядковых номеров записей</td>
    </tr>
    <tr>
        <td></td>
        <td align="center" class="sm tb">
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
        <td width="150"></td>
        <td width="150">
            Масса груза (нетто)
        </td>
        <td></td>
        <td width="20"></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td align="center" class="sm tb">
            прописью
        </td>
        <td width="20"></td>
        <td class="b" width="300">

        </td>
    </tr>
    <tr>
        <td></td>
        <td width="200">
            Масса груза (брутто)
        </td>
        <td></td>
        <td width="20"></td>
        <td class="b" width="300">

        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td align="center" class="sm tb">
            прописью
        </td>
        <td width="20"></td>
        <td width="20"></td>
    </tr>
</table>

<table width="100%">
    <tr>
        <td width="60%">
            <table cellspacing="0" cellpadding="10" border="0" width="100%">
                <tr>
                    <td width="350">
                        Приложение (паспорта, сертификаты, и т.п.) на
                    </td>
                    <td></td>
                    <td width="50">листах</td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td class="tb sm" align="center">прописью</td>
                    <td></td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        Всего отпущено на сумму
                    </td>
                    <td class="bold"><?= \backend\modules\logistics\modules\printDocuments\helpers\WriteSum::num2str($fullAmount)?></td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td class="tb sm" align="center" colspan="2">прописью</td>
                </tr>
            </table>


            <table width="100%" cellspacing="20">
                <tr>
                    <td width="20%">
                        Отпуск разрешил
                    </td>
                    <td width="20%"></td>
                    <td></td>
                    <td align="center">ИП Григорьев Д.Л.</td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center">должность</td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
                <tr>
                    <td width="20%">
                        Главный (старший) бухгалтер
                    </td>
                    <td></td>
                    <td></td>
                    <td align="center">ИП Григорьев Д.Л.</td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td></td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
                <tr>
                    <td width="20%">
                        Отпуск груза произвел
                    </td>
                    <td></td>
                    <td></td>
                    <td align="center">ИП Григорьев Д.Л.</td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center">должность</td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
            </table>


            <table width="100%" cellspacing="20">
                <tr>
                    <td width="20%" align="center">
                        М.П.
                    </td>
                    <td width="30%"></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center"></td>
                </tr>
            </table>

        </td>
        <td>
            <table width="100%">
                <tr>
                    <td width="20%">
                        По доверенности №
                    </td>
                    <td width="30%" class="bb"></td>
                    <td>
                        от
                    </td>
                    <td class="bb" width="40%"></td>
                </tr>
            </table>

            <table width="100%">
                <tr>
                    <td width="20%">
                        выданной
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center" colspan="2"></td>
                </tr>
                <tr>
                    <td width="20%">

                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center" colspan="2">кем, кому (организация, должность, фамилия, и.,
                        о.)
                    </td>
                </tr>
            </table>

            <table width="100%" cellspacing="20">
                <tr>
                    <td width="20%">
                        Груз принял
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center">должность</td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
                <tr>
                    <td width="20%">
                        Груз получил грузополучатель
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="tb sm" align="center">должность</td>
                    <td class="tb sm" align="center">подпись</td>
                    <td class="tb sm" align="center">расшифровка подписи</td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<?= \yii\helpers\Html::endForm();?>
</div>

