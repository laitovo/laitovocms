<?php
$this->render('@backendViews/logistics/views/menu');

//custom css rules
$css = <<<CSS
    .btn-margin {margin-right:5px}
CSS;
$this->registerCss($css);
//custom css rules
?>
    <div class="form-group">
        <?= \yii\helpers\Html::button('<span class="fa fa-plus-square"></span> Добавить отгрузку',[
            'class' => 'btn btn-sm btn-primary btn-margin',
            'data-original-title' => 'Добавить отгрузку',
            'data-toggle' => 'tooltip',
            'onclick' => '
                var elem = $(".copy").clone(true).removeClass("copy").removeClass("hidden");
                $(".shipments").append(elem);'
        ]);?>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6 hidden box copy">
        <div class="input-group">
            <?= \yii\helpers\Html::input('text','shipments[]',null,['class' => 'form-control','placeholder' => 'отгрузка №']);?>
            <span class="input-group-addon btn btn-danger btn-sm" onclick="$(this).closest('.box').remove();" title="Удалить позицию"><span class="fa fa-minus-square"></span></span>
        </div>
    </div>
<?= \yii\helpers\Html::beginForm([''],'post',['id' => 'my-form']);?>
    <!--Оотгрузки-->
    <div class="row shipments">
        <div class="col-md-3 col-sm-4 col-xs-6 box">
            <div class="input-group">
                <?= \yii\helpers\Html::input('text','shipments[]',null,['class' => 'form-control','placeholder' => 'отгрузка №']);?>
                <span class="input-group-addon btn btn-danger btn-sm" onclick="$(this).closest('.box').remove();" title="Удалить позицию" ><span class="fa fa-minus-square"></span></span>
            </div>
        </div>
    </div>
    <hr>

<?= \yii\helpers\Html::submitButton('Предварительный просмотр',['class' => 'btn btn-default btn-margin']);?>

<?= \yii\helpers\Html::endForm();?>
