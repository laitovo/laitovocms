<?php
/**
 * @var integer $shipment Номер отгрузки
 */
?>

<style>
    .center {border:1px solid black;text-align: center;}
    .left {border:1px solid black;text-align: left;}
    .right {border:1px solid black;text-align: right;}
    .less {font-size: 0.7em}
    .less9 {font-size: 0.8em}
    .bold {font-weight: bold}
</style>

<table style="border:1px solid black;border-collapse: collapse;" cellpadding="4">
    <tr>
        <th style = "width:3%;"  class="center less">№</th>
        <th style = "width:65%;" class="center less">Наименование / Title</th>
        <th style = "width:7%;"  class="center less">Упаковка / Packing</th>
        <th style = "width:5%;" class="center less">Кол-во / Quantity</th>
        <th style = "width:5%;" class="center less">Ед / Psc</th>
        <th style = "width:10%;" class="center less">Цена (EURO) / Price</th>
        <th style = "width:10%;" class="center less">Сумма (EURO) / Amount</th>
    </tr>
    <?php foreach ($elements as $element):?>
        <tr style="border:1px solid black">
            <td class="center less9">     <?= $element['number']   ?></td>
            <td class="left less">       <?= $element['title']    ?></td>
            <td class="center less9">     <?= $element['packing']  ?></td>
            <td class="center less9">     <?= $element['quantity'] ?></td>
            <td class="center less">     <?= $element['pcs']      ?></td>
            <td class="center less9">     <?= number_format($element['price'],2)   ?></td>
            <td class="center less9">     <?= number_format($element['amount'],2)  ?></td>
        </tr>
    <?endforeach;?>
    <tr style="border:1px solid black">
        <td class="right less9 bold" colspan="6">Сумма / Total :</td>
        <td class="center less9 bold"><?= number_format($fullAmount,2)?></td>
    </tr>
</table>
