<?php
/**
 * @var integer $shipment Номер отгрузки
 */
?>

<style>
    .center {border:1px solid black;text-align: center;white-space: nowrap}
    .left {border:1px solid black;text-align: left;}
    .less {font-size: 0.9em}
</style>

<h2>Shipment number № <?= $shipment?> from <?= $date?></h2>
<table style="border:1px solid black;border-collapse: collapse;" cellpadding="4">
    <tr>
        <th class="center">№</th>
        <th class="center">Title</th>
        <th class="center">Article</th>
        <th class="center">Quantity</th>
        <th class="center">Box</th>
    </tr>
    <?php foreach ($elements as $element):?>
    <tr style="border:1px solid black">
        <td class="center"><?= $element['number']?></td>
        <td class="left"><?= $element['title']?></td>
        <td class="center less"><?= $element['article']?></td>
        <td class="center"><?= $element['quantity']?></td>
        <td class="center"><?= $element['box']?></td>
    </tr>
    <?endforeach;?>
</table>