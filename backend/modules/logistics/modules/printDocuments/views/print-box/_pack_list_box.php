<?php
$this->render('@backendViews/logistics/views/menu');

//custom css rules
$css = <<<CSS
    .btn-margin {margin-right:5px}
CSS;
$this->registerCss($css);
//custom css rules

?>

<?= \yii\helpers\Html::beginForm([''],'post',['id' => 'my-form']);?>
<div class="row">
<div class="form-group col-md-6">
    <?= \yii\helpers\Html::label('№ Отгрузки','shipment');?>
    <?= \yii\helpers\Html::input('text','shipment',null,['class' => 'form-control']);?>
</div>
<div class="form-group col-md-6">
    <?= \yii\helpers\Html::label('№ Коробки','box');?>
    <?= \yii\helpers\Html::input('text','box',null,['class' => 'form-control']);?>
</div>
</div>
<?= \backend\modules\logistics\modules\printDocuments\widgets\ButtonsWidget::widget(['documentName' => 'box'])?>
<?= \yii\helpers\Html::endForm();?>