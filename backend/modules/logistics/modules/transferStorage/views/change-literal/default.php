<?php

use backend\widgets\GridView;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $userName string*/
/* @var $provider ActiveDataProvider*/
/* @var $boxes array*/

$this->title = Yii::t('app', 'Терминал переезда склада');

$this->render('@backendViews/logistics/views/menu');


FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    $("#erp-terminal-form").on("ajaxComplete", function (event, messages) {
        document.location.reload();
    });

    setInterval(function(){ $(".reloadterminal").click();}, 60*1000);

    var barcode=$(\'#erpterminal-barcode\');

    function searchterminal(e)
    {
        $.ajaxSetup({timeout:10000})
        $.get("' . Url::to(['search']) . '",{
            barcode: barcode.val()
        },function(data){

            $(".reloadterminal").click();
            if (data.status == "success") {
                notie.alert(1,data.message,30);
            } else if (data.status == "error") {
                notie.alert(3,data.message,30);
            } else {
                notie.alert(3,"Неизвествная ошибка. Обратитесь в IT отдел",30);
            }
            if (data.html) {
                myWindow = window.open();
                myWindow.document.write(data.html);
                myWindow.print();
                myWindow.close();
            }
        },"json");

        e.preventDefault();
    }
    
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erpterminal-barcode" && e.which == 13){
                searchterminal(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchterminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });

    ', \yii\web\View::POS_END);

?>
<div class="col-xs-4">
    <input type="text" id="erpterminal-barcode" placeholder="Поиск..." class="form-control">
</div>
<div class="clearfix"></div>
<?php Pjax::begin(['id' => 'pjax-main','timeout'=>5000]); ?>

<?= Html::a("Обновить", ['index'], ['class' => 'hidden reloadterminal']) ?>

<hr>
<div class="row">
    <div class="col-md-6">
        <h3>Вы : <?= $userName ?></h3>
    </div>
</div>


<hr>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $provider,
    // 'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'article',
        [
            'attribute' => 'source.title',
            'label' => 'Источник',
        ],
        'responsible_person',
        // 'comment:ntext',
        'created_at:datetime',
        // 'author_id',
        // 'updated_at',
        // 'updater_id',

        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['data-pjax' => '1'],
        ],
    ],
]); ?>

<?php Pjax::end(); ?>
