<?php

namespace backend\modules\logistics\modules\transferStorage\models;

use backend\modules\laitovo\models\ErpUser;
use backend\modules\logistics\models\Naryad;
use Yii;
use yii\base\Model;
use yii\web\Cookie;

/**
 * Class Terminal
 * @package backend\modules\laitovo\models
 *
 * @property ErpUser $user [ работник ]
 * @property ErpUser $user_id [ работник ]
 * @property ErpUser $upns [ Наряды ]
 */
class Terminal extends Model
{
    ############################################# Properties ###########################################################
    #########

    /**
     * @var integer|null $user_id [ уникальный идентификатор работника ( ErpUser->id либо null ) ]
     */
    public $user_id;

    /**
     * @var array $upns [ список идентификаторов нарядов, с которыми осуществляются действия ( ErpNaryad->id[] либл [] ) ]
     */
    public $upns = [];

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'Сотрудник'),
            'upns' => Yii::t('app', 'Юпны'),
        ];
    }

    #########
    ############################################# Properties ###########################################################


    ############################################# Relations ############################################################
    #########

    /**
     * Возвращает пользователя по идентификатору
     * @return ErpUser|static
     */
    public function getUser()
    {
        return $this->user_id ? ErpUser::findOne($this->user_id) : new ErpUser;
    }

    #########
    ############################################# Relations ############################################################


    ############################################# Validation ###########################################################
    ##########
    /**
     * Список правил валидации для модели
     * - workOrders  :
     *      обязательный параметр, работа модели происходит только при выбранных нарядах,
     *      каждый из списка нарядов - целое число,
     *      должна существовать модель наряда с таким уникальным идентификатором,
     *      наряды должно быть либо со статусом null, пустая строка, в работе, старт должен быть равен текущей локации,
     * - user_id     :
     *      тип - целое число, в базе данных должен существовать работник с таким уникальным идентификатором,
     *      проверить - есть ли на текущем пользователе несданные наряды
     * - location_id : тип - целое число, в базе данных должен существовать участок с таким уникальным идентификатором
     * - group       : тип - булево значение
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upns'], 'required', 'message'=>'{attribute} не выбраны'],
            [['upns'], 'each','rule' => ['integer']],
            [['upns'], 'each','rule' => [
                'exist',
                'skipOnError' => true,
                'targetClass' => Naryad::className(),
//                'filter' => ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK],['start' => $this->location_id]],
                'targetAttribute' => 'id'
            ]],

            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => 'id'],
        ];
    }

    ##########
    ############################################# Validation ###########################################################


    ############################################# Functions ############################################################
    ##########

    /**
     * Инициализируем список нарядов для обработки из сохраненных cookies
     *
     * ErpTerminal constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->upns = Yii::$app->request->cookies->getValue('Terminal_Upns')?:[];
        $this->user_id = Yii::$app->request->cookies->getValue('Terminal_UserId')?:null;
        parent::__construct($config);
    }

    /**
     * Сохраняем список нарядов в cookies на 10 секунд
     */
    public function saveProperties()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Terminal_Upns',
            'value' => $this->upns,
            'expire' => time() + 20*60,
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Terminal_UserId',
            'value' => $this->user_id,
            'expire' => time() + 20*60,
        ]));
    }

    /**
     * Сохраняем список нарядов в cookies на 10 секунд
     */
    public function clearProperties()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Terminal_Upns',
            'value' => [],
        ]));

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Terminal_UserId',
            'value' => null,
        ]));
    }

    ##########
    ############################################# Functions ############################################################

}
