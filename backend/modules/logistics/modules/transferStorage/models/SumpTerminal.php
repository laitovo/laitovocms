<?php

namespace backend\modules\logistics\modules\transferStorage\models;

use backend\modules\laitovo\models\ErpUser;
use Yii;
use yii\base\Model;
use yii\web\Cookie;

/**
 * Class Terminal
 * @package backend\modules\laitovo\models
 *
 * @property ErpUser $user [ работник ]
 * @property ErpUser $user_id [ работник ]
 */
class SumpTerminal extends Model
{
    /**
     * @var integer|null $user_id [ уникальный идентификатор работника ( ErpUser->id либо null ) ]
     */
    public $user_id;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'Сотрудник'),
        ];
    }

    /**
     * Возвращает пользователя по идентификатору
     * @return ErpUser|static
     */
    public function getUser()
    {
        return $this->user_id ? ErpUser::findOne($this->user_id) : new ErpUser;
    }

    /**
     * @return array|array[]
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * Инициализируем список нарядов для обработки из сохраненных cookies
     *
     * ErpTerminal constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->user_id = Yii::$app->request->cookies->getValue('Sump2020_Terminal_UserId') ?:null;
        parent::__construct($config);
    }

    /**
     * Сохраняем список нарядов в cookies на 10 секунд
     */
    public function saveProperties()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Sump2020_Terminal_UserId',
            'value' => $this->user_id,
            'expire' => time() + 20*60,
        ]));
    }

    /**
     * Сохраняем список нарядов в cookies на 10 секунд
     */
    public function clearProperties()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'Sump2020_Terminal_UserId',
            'value' => null,
        ]));
    }
}
