<?php

namespace backend\modules\logistics\modules\transferStorage\controllers;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\modules\transferStorage\models\MainStorageTerminal;
use Yii;
use yii\db\Query;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class IncomingController
 * @package backend\modules\logistics\modules\transferStorage\controllers
 */
class MainStorageController extends Controller
{
    /**
     * @return string Основное терминальное окно
     */
    public function actionIndex()
    {
        $model = new MainStorageTerminal();

        /**
         * Укажем работника
         */
        $userName = $model->user_id ? $model->user->name : "Работник не указан";

        return $this->render('default', [
            'userName' => $userName,
        ]);
    }

    /**
     * @param $barcode
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status'] ='error';
        $response['message']='Не верный штрих код!!!';
        $response['html']   ='';

        $barcode = BarcodeHelper::toLatin($barcode);
        $user = ErpUser::find()->where(['barcode'=>$barcode])->one();
        $upn = Naryad::find()->where(['barcode'=>$barcode])->one();

        if ($user) {
            $model = new MainStorageTerminal();
            $model->user_id = $user->id;
            $model->saveProperties();
            $response['status'] ='success';
            $response['message']= $model->user->name;
            return $response;
        } elseif ($upn) {
            $model = new MainStorageTerminal();

            if (!$model->user_id) {
                $response['status'] = 'error';
                $response['message'] = 'Внимание, зарегестрируйтесь в терминале как работник !!! Пикните своим бейджиком !!!';
                return $response;
            }

            if ($upn->isParent) {
                $response['status'] = 'error';
                $response['message'] = 'Это комлпект, распакуйте комплект и оприходуйте все позиции по отдельности';
                return $response;
            } else {
                if (!$upn->storageState) {
                    $response['status'] = 'error';
                    $response['message'] = "Данный комплект НЕ ЧИСЛИТСЯ НА СКЛАДЕ - Отдайте Алексею";
                    return $response;
                }

                if ( !(
                    (new Query())
                    ->from('tmp_relevance_report_main_storage')
                    ->where(['upn' => $upn->id])
                    ->andWhere(['soldArticle' => 0])
//                    ->andWhere(['soldWindow' => 0])
                    ->andWhere(['<=', 'soldCars', 2])
                    ->exists()
                    ||
                    (
                        (new Query())
                        ->from('tmp_relevance_report_main_storage')
                        ->where(['upn' => $upn->id])
                        ->exists()
                        && preg_match('/^\w+-\w-\d+-1-(2|4|5)$/', $upn->article) !== 1
                    )
                )) {

                    $response['status'] = 'error';
                    $response['message'] = "Положите данный комплект ОБРАТНО В ЛИТЕРУ {$upn->storageState->literal}";
                    return $response;

                } else {
                    if ($upn->reserved_id || $upn->packed || $upn->pid) {
                        $response['status'] = 'error';
                        $response['message'] = 'С данным комплектом надо разбиратся, отложи его!!';
                        return $response;
                    } else {
                        /**
                         * @var $state StorageState
                         */
                        $state = $upn->storageState;
                        if ($state->delete()) {
                            $response['status'] = 'success';
                            $response['message'] = "UPN {$upn->id} успешно списан со склада!!! Утилизируйте его!!!";
                            return $response;
                        }
                    }
                };
            }
        } else {
            return $response;
        }
    }
}