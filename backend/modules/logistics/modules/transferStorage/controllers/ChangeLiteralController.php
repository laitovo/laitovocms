<?php

namespace backend\modules\logistics\modules\transferStorage\controllers;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Naryad as LogisticsNaryad;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\modules\transferStorage\models\Terminal;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class ChangeLiteralController
 * @package backend\modules\logistics\modules\transferStorage\controllers
 */
class ChangeLiteralController extends Controller
{
    public function actionIndex()
    {
        $model = new Terminal();

        /**
         * Укажем работника
         */
        $userName = $model->user_id ? $model->user->name : "Работник не указан";

        /**
         * Найдем все наряды, которые перемещаем
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Naryad::find()->where(['in','id',$model->upns]),
        ]);

        /**
         * Найдем все номера коробок, которые необходимо распечатать
         */
        $boxes = Naryad::find()->select('transferLiteral')->distinct()->where('transferLiteral is not NULL')->orderBy('transferLiteral')->column();


        return $this->render('default', [
            'userName' => $userName,
            'provider' => $dataProvider,
            'boxes' => $boxes,
        ]);
    }

    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        ini_set('memory_limit', '2048M');
        ini_set("max_execution_time", "0");

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status'] ='error';
        $response['message']='Ошибка!!!';
        $response['html']   ='';

        $barcode = BarcodeHelper::toLatin($barcode);
        $user = ErpUser::find()->where(['barcode'=>$barcode])->one();
        $upn = Naryad::find()->where(['barcode'=>$barcode])->one();

        if ($user) {
            $model = new Terminal();
            $model->user_id = $user->id;
            $model->saveProperties();
            $response['status'] ='success';
            $response['message']= $model->user->name;
        } elseif ($upn) {
            $model = new Terminal();

            if (!$model->user_id) {
                $response['status'] ='error';
                $response['message']= 'Внимание, зарегестрируйтесь в терминале как работник !!! Пикните своим бейджиком !!!';
                return $response;
            }

            if ($upn->isParent) {
                $literal = null;
                foreach ($upn->children as $child) {
                    if (!$child->reserved_id) {
                        $response['status'] ='error';
                        $response['message']= 'Данный артикул не находиться в резерве !!!!';
                        return $response;
                    }

                    if (!$child->storageState) {
                        $response['status'] ='error';
                        $response['message']= 'Данный артикул не числится на складе !!!!';
                        return $response;
                    }

                    if ($child->packed) {
                        $response['status'] ='error';
                        $response['message']= 'Отложи данный комлпект в отдельную коробку. Это ошибка, оприходуем позже. Пометь как "оприходовать позже" !!!';
                        return $response;
                    }

                    $storage = null;
                    //1. Определяем, под какой заказ зарезервирован данный UPN
                    $order = $child->orderEntry->order;

                    if (!$order) {
                        $response['status'] ='error';
                        $response['message']= 'ВИМАНИЕ. ОШИБКА. Заказа НЕ СУЩЕСТВУЕТ !!!!';
                        return $response;
                    }

                    $entries = OrderEntry::find()->select('id')->where(['and',['order_id' => $order->id],['is_deleted' => null]])->column();
                    //2. Ищем всех детей и наличие на складе.
                    $upnIds = LogisticsNaryad::find()->select('id')->where(['in','reserved_id',$entries])->column();

                    //3. Проверяем наличие хотябы одного upn с этого заказа, который уже лежит на транзитном складе
                    $storageIds =  Storage::find()->select('id')->where(['type'=> Storage::TYPE_TRANSIT])->column();
                    $storageState = StorageState::find()->where(['and',['in','storage_id',$storageIds],['in','upn_id',$upnIds]])->one();

                    if ($storageState) {
                        $storage = $storageState->storage;
                        $literal = $storageState->literal;
                    } else {
                        /**
                         * @var Storage $storage
                         */
                        $storage = Storage::find()->where(['type'=> Storage::TYPE_TRANSIT])->one();
                        if (!$storage) {
                            throw new \DomainException('Заведите транзитный склад в программе !!!');
                        }
                        $literal = $storage->getNotUseLiteral(count($entries));
                    }

                    $transaction = Yii::$app->db->beginTransaction();
                    //Содание UPN для наряда

                    $row = $child->storageState;
                    $row->storage_id = $storage->id;
                    $row->literal = $literal;
                    if (!$row->save()) {
                        $transaction->rollback();
                        return false;
                    }

                    $transaction->commit();
                }

                $model->saveProperties();
                $response['status'] ='success';
                $response['message']= 'Положите комплект в ' . $upn->id . 'в литеру ' . $literal;

                return $response;
            } else {
                /**
                 * Защита от того, чтобы upn числился в нескольких коробках
                 */
                if (!$upn->reserved_id) {
                    $response['status'] ='error';
                    $response['message']= 'Данный артикул не находиться в резерве !!!!';
                    return $response;
                }

                if (!$upn->storageState) {
                    $response['status'] ='error';
                    $response['message']= 'Данный артикул не числится на складе !!!!';
                    return $response;
                }

                if ($upn->pid) {
                    $response['status'] ='error';
                    $response['message']= 'Данный UPN должен приходовать в составе комлпекта !!!!';
                    return $response;
                }

                if ($upn->packed) {
                    $response['status'] ='error';
                    $response['message']= 'Отложи данный комлпект в отдельную коробку. Это ошибка, оприходуем позже. Пометь как "оприходовать позже" !!!';
                    return $response;
                }

                $storage = null;
                //1. Определяем, под какой заказ зарезервирован данный UPN
                $order = $upn->orderEntry->order;

                if (!$order) {
                    $response['status'] ='error';
                    $response['message']= 'ВИМАНИЕ. ОШИБКА. Заказа НЕ СУЩЕСТВУЕТ !!!!';
                    return $response;
                }
                //2. Ищем всех детей и наличие на складе.
                $entries = OrderEntry::find()->select('id')->where(['and',['order_id' => $order->id],['is_deleted' => null]])->column();
                //2. Ищем всех детей и наличие на складе.
                $upnIds = LogisticsNaryad::find()->select('id')->where(['in','reserved_id',$entries])->column();


                //3. Проверяем наличие хотябы одного upn с этого заказа, который уже лежит на транзитном складе
                $storageIds =  Storage::find()->select('id')->where(['type'=> Storage::TYPE_TRANSIT])->column();
                $storageState = StorageState::find()->where(['and',['in','storage_id',$storageIds],['in','upn_id',$upnIds]])->one();

                if ($storageState) {
                    $storage = $storageState->storage;
                    $literal = $storageState->literal;
                } else {
                    /**
                     * @var Storage $storage
                     */
                    $storage = Storage::find()->where(['type'=> Storage::TYPE_TRANSIT])->one();
                    if (!$storage) {
                        throw new \DomainException('Заведите транзитный склад в программе !!!');
                    }
                    $literal = $storage->getNotUseLiteral(count($entries));
                }

                $transaction = Yii::$app->db->beginTransaction();
                //Содание UPN для наряда

                $row = $upn->storageState;
                $row->storage_id = $storage->id;
                $row->literal = $literal;
                if (!$row->save()) {
                    $transaction->rollback();
                    return false;
                }

                $transaction->commit();

                $model->saveProperties();
                $response['status'] ='success';
                $response['message']= 'Положите в литеру ' . $row->literal;
            }
        }

        return $response;
    }

    public function actionSave()
    {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "0");
        
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status']  = 'success';
        $response['message'] = 'Вы успешно создали литеру';
        $response['html']    = '';

        $model = new Terminal();
        $upns = Naryad::find()->where(['in','id',$model->upns])->all();
        $literal = Naryad::find()->max('transferLiteral');
        $literal = $literal ? $literal + 1 : 1;
        $parents = [];
        foreach ($upns as $upn) {
            $state = $upn->storageState;
            $upn->transferLiteral = $literal;
            $upn->transferUserIn = $model->user_id;
            $upn->lastStorage = $state->storage_id;
            $upn->lastLiteral = $state->literal;
            $upn->save();
            $state->delete();

            if ($upn->pid) {
                $parents[] = $upn->parent;
            }
        }
        if ($parents) {
            foreach ($parents as $parent) {
                $parent->transferLiteral = $literal;
                $parent->transferUserIn = $model->user_id;

                $parent->save();
                $parent->storageState->delete();
            }
        }

        $model->clearProperties();
        $response['html'] = "<h1 style='font-size: 15em'>$literal</h1>";

        $generator = new BarcodeGeneratorHTML();
        $response['html'] .= "<br>" . $generator->getBarcode("LIT" . $literal, $generator::TYPE_CODE_128);

        return $response;
    }

    public function actionPrintList($id)
    {
        $content = "<h1 style='font-size: 15em'>$id</h1>";

        $generator = new BarcodeGeneratorHTML();
        $content .= "<br>" . $generator->getBarcode("LIT" . $id, $generator::TYPE_CODE_128);

        echo $content;
    }
}