<?php

namespace backend\modules\logistics\modules\transferStorage\controllers;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\modules\transferStorage\models\Terminal;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package backend\modules\logistics\modules\transferStorage\controllers
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        $model = new Terminal();

        /**
         * Укажем работника
         */
        $userName = $model->user_id ? $model->user->name : "Работник не указан";

        /**
         * Найдем все наряды, которые перемещаем
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Naryad::find()->where(['in','id',$model->upns]),
        ]);

        /**
         * Найдем все номера коробок, которые необходимо распечатать
         */
        $boxes = Naryad::find()->select('transferLiteral')->distinct()->where('transferLiteral is not NULL')->orderBy('transferLiteral')->column();


        return $this->render('default', [
            'userName' => $userName,
            'provider' => $dataProvider,
            'boxes' => $boxes,
        ]);
    }

    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status'] ='error';
        $response['message']='Ошибка!!!';
        $response['html']   ='';

        $barcode = BarcodeHelper::toLatin($barcode);
        $user = ErpUser::find()->where(['barcode'=>$barcode])->one();
        $upn = Naryad::find()->where(['barcode'=>$barcode])->one();

        if ($user) {
            $model = new Terminal();
            $model->user_id = $user->id;
            $model->saveProperties();
            $response['status'] ='success';
            $response['message']= $model->user->name;
        } elseif ($upn) {
            $model = new Terminal();

            if (!$model->user_id) {
                $response['status'] ='error';
                $response['message']= 'Внимание, зарегестрируйтесь в терминале как работник !!! Пикните своим бейджиком !!!';
                return $response;
            }

            if ($upn->isParent) {
                foreach ($upn->children as $child) {
                    if ($child->transferLiteral) {
                        $response['status'] ='error';
                        $response['message']= 'Данный артикул уже должен лежать в другой коробке. Его коробка №' . $child->transferLiteral . ' !!!';
                        return $response;
                    }

                    if (!$child->storageState) {
                        $response['status'] ='error';
                        $response['message']= 'Данный артикул не числится на складе !!!!';
                        return $response;
                    }

                    if ($child->reserved_id && $child->storageState->storage_id !== 3) {
                        $response['status'] ='error';
                        $response['message']= 'Данный артикул зарезервирован под заказ!!!';
                        return $response;
                    }

                    $model->upns[] = $child->id;
                }
                $model->saveProperties();
                $response['status'] ='success';
                $response['message']= $upn->id;
                return $response;
            } else {
                /**
                 * Защита от того, чтобы upn числился в нескольких коробках
                 */
                if ($upn->transferLiteral) {
                    $response['status'] ='error';
                    $response['message']= 'Данный артикул уже должен лежать в другой коробке. Его коробка №' . $upn->transferLiteral . ' !!!';
                    return $response;
                }

                if (!$upn->storageState) {
                    $response['status'] ='error';
                    $response['message']= 'Данный артикул не числится на складе !!!!';
                    return $response;
                }

                if ($upn->reserved_id && $upn->storageState->storage_id !== 3) {
                    $response['status'] ='error';
                    $response['message']= 'Данный артикул зарезервирован под заказ!!!';
                    return $response;
                }

                $model->upns[] = $upn->id;
                $model->saveProperties();
                $response['status'] ='success';
                $response['message']= $upn->id;
            }
        }

        return $response;
    }

    public function actionSave()
    {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "0");
        
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status']  = 'success';
        $response['message'] = 'Вы успешно создали литеру';
        $response['html']    = '';

        $model = new Terminal();
        $upns = Naryad::find()->where(['in','id',$model->upns])->all();
        $literal = Naryad::find()->max('transferLiteral');
        $literal = $literal ? $literal + 1 : 1;
        $parents = [];
        foreach ($upns as $upn) {
            $state = $upn->storageState;
            $upn->transferLiteral = $literal;
            $upn->transferUserIn = $model->user_id;
            $upn->lastStorage = $state->storage_id;
            $upn->lastLiteral = $state->literal;
            $upn->save();
            $state->delete();

            if ($upn->pid) {
                $parents[] = $upn->parent;
            }
        }
        if ($parents) {
            foreach ($parents as $parent) {
                $parent->transferLiteral = $literal;
                $parent->transferUserIn = $model->user_id;

                $parent->save();
                $parent->storageState->delete();
            }
        }

        $model->clearProperties();
        $response['html'] = "<h1 style='font-size: 15em'>$literal</h1>";

        $generator = new BarcodeGeneratorHTML();
        $response['html'] .= "<br>" . $generator->getBarcode("LIT" . $literal, $generator::TYPE_CODE_128);

        return $response;
    }

    public function actionPrintList($id)
    {
        $content = "<h1 style='font-size: 15em'>$id</h1>";

        $generator = new BarcodeGeneratorHTML();
        $content .= "<br>" . $generator->getBarcode("LIT" . $id, $generator::TYPE_CODE_128);

        echo $content;
    }
}