<?php

namespace backend\modules\logistics\modules\transferStorage\controllers;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\modules\transferStorage\models\SumpTerminal;
use core\models\article\Article;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class IncomingController
 * @package backend\modules\logistics\modules\transferStorage\controllers
 */
class SumpController extends Controller
{
    /**
     * @return string Основное терминальное окно
     */
    public function actionIndex()
    {
        $model = new SumpTerminal();

        /**
         * Укажем работника
         */
        $userName = $model->user_id ? $model->user->name : "Работник не указан";

        /**
         * Найдем все наряды, которые перемещаем
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Naryad::find()->where(['is not','transferLiteral',null]),
        ]);

        return $this->render('default', [
            'userName' => $userName,
            'provider' => $dataProvider,
        ]);
    }

    /**
     * @param $barcode
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        ini_set('memory_limit', '2048M');
        ini_set("max_execution_time", "0");

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status'] ='error';
        $response['message']='Ошибка!!!';
        $response['html']   ='';

        $barcode = BarcodeHelper::toLatin($barcode);
        $barcode = str_ireplace('RL','',$barcode);
        $barcode = str_ireplace('L','',$barcode);
        $user = ErpUser::find()->where(['barcode'=>$barcode])->one();
        $upn = Naryad::find()->where(['id'=>$barcode])->one();

        if ($user) {
            $model = new SumpTerminal();
            $model->user_id = $user->id;
            $model->saveProperties();
            $response['status'] ='success';
            $response['message']= $model->user->name;
        } elseif ($upn) {
            $model = new SumpTerminal();

            if (!$model->user_id) {
                $response['status'] ='error';
                $response['message']= 'Внимание, зарегестрируйтесь в терминале как работник !!! Пикните своим бейджиком !!!';
                return $response;
            }

            if ($upn->isParent) {
                $response['status'] ='error';
                $response['message']= 'Это комлпект, распакуйте комплект и оприходуйте все позиции по отдельности';
                return $response;
            } else {
                /**
                 * Различные проверки на ошибки
                 */
                if ($upn->storageState) {
                    $response['status'] ='error';
                    $response['message']= ' ' . $upn->storageState->literal;
                    $response['message']= "Данный артикул числится на складе !!!! Литера:<span style='font-size: 6em'>{$upn->storageState->literal}</span>";
                    return $response;
                }
                /**
                 * Процесс оприходования комплекта.
                 */
                $storage = null;
                $prod = null;

                //Статситику по артикулу
                $objArticle = new Article($upn->article);

                if ((new Query())
                    ->from('tmp_relevance_report')
                    ->where(['upn' => $upn->id])
                    ->andWhere(['soldArticle' => 0])
                    ->andWhere(['<=','soldWindow',2])
                    ->exists()) {
                    $response['status'] ='error';
                    $response['message']= "<span style='font-size: 6em'>Утилизировать</span>";
                    return $response;
                };

                //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
                $storageType = Storage::TYPE_SUMP;

                $productId = $objArticle->product ? $objArticle->product->id : null;
                if ($productId) {
                    $storage = Storage::findStorageForProduct($productId,$storageType);
                }
                if (!$storage) {
                    return $response;
                }
                $literal = $storage->getActualLiteral($productId);

                if ((new Query())
                    ->from('tmp_relevance_report')
                    ->where(['upn' => $upn->id])
                    ->andWhere(['soldArticle' => 0])
                    ->andWhere(['>','soldWindow',2])
                    ->exists()) {
                    $literal = 'UA-A1000';
                };

                $transaction = Yii::$app->db->beginTransaction();

                if ($upn->reserved_id || $upn->packed || $upn->pid) {
                    $response['status'] ='error';
                    $response['message']= 'С данным комплектом надо разбиратся, отложи его!!';
                    return $response;
                }

                //Ставим, что данный наряд распределен с диспечер-склад
                $row = new StorageState();
                $row->upn_id = $upn->id;
                $row->storage_id = $storage->id;
                $row->literal = $literal;
                if (!$row->save()) {
                    $transaction->rollback();
                    return $response;
                }
                $upn->transferLiteral = null;
                $upn->transferUserOut = $model->user_id;
                if (!$upn->save()) {
                    $transaction->rollback();
                    return $response;
                }
                $transaction->commit();

                $model->saveProperties();
                $response['upnId']      = $upn->id;
                $response['status'] ='success';
                $response['message']= "Положи В <span style='font-size: 6em'>{$row->literal}</span>";
            }
        }

        return $response;
    }


    public function actionRemove($id)
    {
        $upn = Naryad::findOne($id);
        if (!$upn) {
            Yii::$app->session->setFlash('error', 'Удаление не получилось. Не правильный идентификатор UPN');
            return $this->redirect(['index']);
        }

        $model = new SumpTerminal();
        if (!$model->user_id) {
            Yii::$app->session->setFlash('error', 'Удаление не получилось. Не указан пользователь!!');
            return $this->redirect(['index']);
        }

        $objArticle = new Article($upn->article);

        $articlesWithAnalogs = $objArticle->articleWithAnalogs;
        if (!(new Query())
            ->from('tmp_relevance_report')
            ->where(['upn' => $upn->id])
            ->andWhere(['soldArticle' => 0])
            ->andWhere(['<=','soldWindow',2])
            ->exists()) {
            Yii::$app->session->setFlash('error', 'Данный UPN нужен на складе - отстойнике. Его нельзя удалить');
            return $this->redirect(['index']);
        }

        $upn->transferLiteral = null;
        $upn->transferUserOut = $model->user_id;
        if (!$upn->save()) {
            Yii::$app->session->setFlash('error', 'Удаление не получилось. Не сохранить UPN !!!');
            return $this->redirect(['index']);
        }

        $model->saveProperties();
        Yii::$app->session->setFlash('success', "Вы успешно удалили не нужный UPN. Данный комплект {$upn->id} утилизируйте!!!");
        return $this->redirect(['index']);
    }
}