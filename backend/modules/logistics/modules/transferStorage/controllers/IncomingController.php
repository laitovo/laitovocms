<?php

namespace backend\modules\logistics\modules\transferStorage\controllers;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\modules\transferStorage\models\Terminal;
use core\models\article\Article;
use Picqer\Barcode\BarcodeGeneratorHTML;
use returnModule\core\models\ReturnJournal;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class IncomingController
 * @package backend\modules\logistics\modules\transferStorage\controllers
 */
class IncomingController extends Controller
{
    public function actionIndex()
    {
        $model = new Terminal();

        /**
         * Укажем работника
         */
        $userName = $model->user_id ? $model->user->name : "Работник не указан";

        /**
         * Найдем все наряды, которые перемещаем
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Naryad::find()->where(['in','id',$model->upns]),
        ]);

        /**
         * Найдем все номера коробок, которые необходимо распечатать
         */
        $boxes = Naryad::find()->select('transferLiteral')->distinct()->where('transferLiteral is not NULL')->orderBy('transferLiteral')->column();


        return $this->render('default', [
            'userName' => $userName,
            'provider' => $dataProvider,
            'boxes' => $boxes,
        ]);
    }

    /**
     * @param $barcode
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSearch($barcode)
    {
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        ini_set('memory_limit', '2048M');
        ini_set("max_execution_time", "0");

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status'] ='error';
        $response['message']='Ошибка!!!';
        $response['html']   ='';

        $barcode = BarcodeHelper::toLatin($barcode);
        $user = ErpUser::find()->where(['barcode'=>$barcode])->one();
        $upn = Naryad::find()->where(['barcode'=>$barcode])->one();

        if ($user) {
            $model = new Terminal();
            $model->user_id = $user->id;
            $model->saveProperties();
            $response['status'] ='success';
            $response['message']= $model->user->name;
        } elseif ($upn) {
            $model = new Terminal();

            if (!$model->user_id) {
                $response['status'] ='error';
                $response['message']= 'Внимание, зарегестрируйтесь в терминале как работник !!! Пикните своим бейджиком !!!';
                return $response;
            }

            if ($upn->isParent) {
                $response['status'] ='error';
                $response['message']= 'Это комлпект, распакуйте комплект и оприходуйте все позиции по отдельности';
                return $response;

//                $literal = null;
//                $storage = null;
//                foreach ($upn->children as $child) {
//                    if ($child->reserved_id) {
//                        $response['status'] ='error';
//                        $response['message']= 'Данный артикул зарезервирован. Это ошибка, оприходуем позже. Пометь как "оприходовать позже" !!!';
//                        return $response;
//                    }
//
//                    if ($child->packed) {
//                        $response['status'] ='error';
//                        $response['message']= 'Данный артикул почему то УПАКОВАН. Это ошибка, оприходуем позже. Пометь как "оприходовать позже" !!!';
//                        return $response;
//                    }
//
//                    if ($child->storageState) {
//                        $response['status'] ='error';
//                        $response['message']= 'Данный артикул числится на складе !!!! Это ошибка, оприходуем позже. Пометь как "оприходовать позже" !!';
//                        return $response;
//                    }
//
//                    $prod = null;
//
//                    //Статситику по артикулу
//                    $objArticle = new Article($child->article);
//
//                    //Оборотный
//                    $storageType = Storage::TYPE_REVERSE;
//
//                    if (!$storage) {
//                        $productId = $objArticle->product ? $objArticle->product->id : null;
//                        if ($productId) {
//                            $storage = Storage::findStorageForProduct($productId,$storageType);
//                        }
//                    }
//
//                    if (!$storage) {
//                        return false;
//                    }
//                    if (!$literal) {
//                        $literal = $storage->getActualLiteral($productId);
//                    }
//
//                    $transaction = Yii::$app->db->beginTransaction();
//
//                    //Ставим, что данный наряд распределен с диспечер-склад
//                    $row = new StorageState();
//                    $row->upn_id = $child->id;
//                    $row->storage_id = $storage->id;
//                    $row->literal = $literal;
//                    if (!$row->save()) {
//                        $transaction->rollback();
//                        return false;
//                    }
//                    $transaction->commit();
//                }
//
//                $model->saveProperties();
//                $response['status'] ='success';
//                $response['message']= 'Положите комплект в ' . $upn->id . 'в литеру ' . $literal;
//
//                return $response;
            } else {
                /**
                 * Различные проверки на ошибки
                 */

//                if ($upn->reserved_id) {
//                    $response['status'] ='error';
//                    $response['message']= 'Данный артикул зарезервирован. Это ошибка, оприходуем позже. Пометь как "оприходовать позже" !!!';
//                    return $response;
//                }
//
//                if ($upn->packed) {
//                    $response['status'] ='error';
//                    $response['message']= 'Данный артикул почему то УПАКОВАН. Это ошибка, оприходуем позже. Пометь как "оприходовать позже" !!!';
//                    return $response;
//                }
//
                if ($upn->storageState) {
                    $response['status'] ='error';
                    $response['message']= 'Данный артикул числится на складе !!!! Это ошибка, отложи, с ним разберемся позже!!';
                    return $response;
                }
//
//                if ($upn->pid) {
//                    $response['status'] ='error';
//                    $response['message']= 'Данный UPN должен приходовать в составе комлпекта !!!!';
//                    return $response;
//                }

                /**
                 * Процесс оприходования комплекта.
                 */
                $storage = null;
                $prod = null;

                //Статситику по артикулу
                $objArticle = new Article($upn->article);

                //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
                $storageType = $upn->lastStorage != 3 && $upn->lastStorage ? Storage::TYPE_REVERSE : Storage::TYPE_SUMP;

                $productId = $objArticle->product ? $objArticle->product->id : null;
                if ($productId) {
                    $storage = Storage::findStorageForProduct($productId,$storageType);
                }
                if (!$storage) {
                    return false;
                }
                $literal = $storage->getActualLiteral($productId);

                $transaction = Yii::$app->db->beginTransaction();

                $logist = null;
                if ($upn->reserved_id || $upn->packed || $upn->pid) {
                    $logist = new Naryad();
                    $logist->article = $upn->article;
                    $logist->comment = 'Замена: ' . $upn->id;
                    $logist->team_id = Yii::$app->team->getId();
                    $logist->source_id = 2;
                    if (!$logist->save()) {
                        $transaction->rollback();
                        return false;
                    }
                    $upn = $logist;
                }

                //Ставим, что данный наряд распределен с диспечер-склад
                $row = new StorageState();
                $row->upn_id = $upn->id;
                $row->storage_id = $storage->id;
                $row->literal = $literal;
                if (!$row->save()) {
                    $transaction->rollback();
                    return false;
                }
                $transaction->commit();

                $workOrder = ErpNaryad::find()->where(['barcode' => $upn->barcode])->one();
                $exist = ErpNaryad::find()->where(['and',['logist_id' => $upn->id],['like','barcode','NL%',false]])->exists();
                $return = ReturnJournal::find()->where(['result_upn_id' => $upn->id])->exists();
                if (!$logist && ($workOrder || !$exist) && !$return ) {
                    $upn->barcode = 'R' . $upn->barcode;
                    $upn->save();
                    $response['printLabel'] = true;
                    $response['upnId']      = $upn->id;
                } elseif ($logist) {
                    $response['printLabel'] = true;
                    $response['upnId']      = $upn->id;
                }

                $model->saveProperties();
                $response['status'] ='success';
                $response['message']= 'Положите в литеру ' . $row->literal;
            }
        }

        return $response;
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionSave()
    {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "0");
        
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        //Возвращаем данные в формате JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Устанавливаем ответ по умолчанию
        $response['status']  = 'success';
        $response['message'] = 'Вы успешно создали литеру';
        $response['html']    = '';

        $model = new Terminal();
        $upns = Naryad::find()->where(['in','id',$model->upns])->all();
        $literal = Naryad::find()->max('transferLiteral');
        $literal = $literal ? $literal + 1 : 1;
        $parents = [];
        foreach ($upns as $upn) {
            $state = $upn->storageState;
            $upn->transferLiteral = $literal;
            $upn->transferUserIn = $model->user_id;
            $upn->lastStorage = $state->storage_id;
            $upn->lastLiteral = $state->literal;
            $upn->save();
            $state->delete();

            if ($upn->pid) {
                $parents[] = $upn->parent;
            }
        }
        if ($parents) {
            foreach ($parents as $parent) {
                $parent->transferLiteral = $literal;
                $parent->transferUserIn = $model->user_id;

                $parent->save();
                $parent->storageState->delete();
            }
        }

        $model->clearProperties();
        $response['html'] = "<h1 style='font-size: 15em'>$literal</h1>";

        $generator = new BarcodeGeneratorHTML();
        $response['html'] .= "<br>" . $generator->getBarcode("LIT" . $literal, $generator::TYPE_CODE_128);

        return $response;
    }

    public function actionPrintList($id)
    {
        $content = "<h1 style='font-size: 15em'>$id</h1>";

        $generator = new BarcodeGeneratorHTML();
        $content .= "<br>" . $generator->getBarcode("LIT" . $id, $generator::TYPE_CODE_128);

        echo $content;
    }
}