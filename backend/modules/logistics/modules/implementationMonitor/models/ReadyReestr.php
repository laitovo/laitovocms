<?php
namespace backend\modules\logistics\modules\implementationMonitor\models;

use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository;
use yii\data\ArrayDataProvider;

class ReadyReestr
{
    private $_ordersRepository;
    private $_orderElementsRepository;
    private $_orderService;
    private $_orderElementService;

    public function __construct(
        IOrdersRepository $ordersRepository,
        IOrderService $orderService,
        IOrderElementsRepository $orderElementsRepository,
        IOrderElementService $orderElementService
    )
    {
        $this->_ordersRepository = $ordersRepository;
        $this->_orderService = $orderService;
        $this->_orderElementsRepository = $orderElementsRepository;
        $this->_orderElementService = $orderElementService;
    }

    public function getData()
    {
        $data = $this->_addRows();
        $data = $this->_asProvider($data);

        return $data;
    }

    public function getPrintData($ids = [])
    {
        $data = $this->_addRowsForIds($ids);
//        $data = $this->_asProvider($data);

        return $data;
    }


    /**
     * Сгенерировать массив элементов заказаs
     * @param $order
     * @return mixed
     */
    private function _addRows()
    {
        $rows = [];

        $orders = $this->_ordersRepository->getPackageReestrOrders();
        if ($orders) {
            foreach ($orders as $order) {
                $rows[] = $this->_addRow($order);
            }

        }

        return $rows;
    }

    /**
     * Сгенерировать массив элементов заказаs
     * @param $order
     * @return mixed
     */
    private function _addRowsForIds($ids)
    {
        $rows = [];
        foreach ($ids as $id) {
            $order = $this->_ordersRepository->getOrderById($id);
            if ($order) {
                $rows[] = $this->_addRow($order);
            }
        }
        return $rows;
    }

    /**
     * Функция - которая генеррует строку элемента заказа
     * @param $element
     * @return array
     */
    private function _addRow($order)
    {
        $row = [];
        $ordernum  = $this->_orderService->getNumber($order);
        $row['order']  = $ordernum;
        $row['barcodes'] = $this->_addBarcodes($order);

        return $row;
    }


    /**
     * Функция возвращает список штрихкодов для заявки
     *
     * @param $order
     * @return array
     */
    private function _addBarcodes($order)
    {
        $rows = [];
        $elements  = $this->_orderElementsRepository->getElementsForOrder($order);
        if ($elements) {
            foreach ($elements as $element) {
                $rows[] = $this->_addBarcode($element);
            }
        }

        return $rows;
    }

    /**
     * Функция возвращает значение штрихкода для элемента
     *
     * @param $element
     * @return null
     */
    private function _addBarcode($element)
    {
        $barcode = null;
        $barcode  = $this->_orderElementService->getSourceBarcode($element);

        return $barcode;
    }


    /**
     * Функция оборачивает в дата провайдер некий массив
     * @param $data
     * @param array $sort
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    private function _asProvider($data,$sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}