<?php

namespace backend\modules\logistics\modules\implementationMonitor\models;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\StorageState;
use core\models\shipment\Shipment;
use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\logistics\models\Order;
use yii\helpers\ArrayHelper;

/**
 * OrderSearch represents the model behind the search form about `backend\modules\logistics\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * Creates data provider instance with search query applied
     *
     * @param $searchStr
     * @param $onlyActual
     * @return ActiveDataProvider
     */
    public function search($searchStr, $onlyActual): ActiveDataProvider
    {
        $query = Order::find()->where(['team_id' => Yii::$app->team->id]);

        if ($onlyActual && !is_numeric($searchStr)) {
            $shipments = Shipment::find()
                ->select('id')
                ->where(['or',['logisticsStatus' => 'shipped'],['logisticsStatus' => 'packed']])
                ->column();
            $maxOrderEntryId = OrderEntry::find()->max('id');
            $orders = OrderEntry::find()
                ->select('order_id')
                ->where(['>','id', $maxOrderEntryId - 10000])
                ->andWhere('order_id is not NULL')
                ->andWhere(['is_deleted' => null])
                ->andWhere(['or','shipment_id not IN (' .implode(',',$shipments) .')','shipment_id is NULL'] )
                ->groupBy('order_id')->column();
            $query->andWhere('id IN (' .implode(',',$orders) .')');
            $query->andWhere(['and',['processed' => true],['shipped' => null]]);
        }

        if ($searchStr)
            $query->andFilterWhere(['or',
                ['like', 'source_innumber', $searchStr],
                ['like', 'username', $searchStr],
                ['like', 'delivery', $searchStr],
                ['like', 'category', $searchStr],
            ]);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }


    /**
     * @param $searchStr
     * @param $onlyActual
     * @return array
     */
    public function searchDangerous($searchStr, $onlyActual): array
    {
        $query = Order::find()
            ->where(['team_id' => Yii::$app->team->id])
            ->select('source_innumber');
        //Найдем  все отгруженные заказы
        $shipments = Shipment::find()
            ->select('id')
            ->where(['logisticsStatus' => 'not_handled'])
            ->column();
        $errorOrderEntries = [];
        $maxOrderEntryId = OrderEntry::find()->max('id');
        $orderEntries = OrderEntry::find()
            ->select('id')
            ->where(['>','id', $maxOrderEntryId - 3000])
            ->andWhere(['is_deleted' => NULL])
            ->andWhere(['or',['shipment_id' => $shipments],['shipment_id' => NULL]])
            ->column();
        $logisticsNaryads = Naryad::find()
            ->where(['reserved_id' => $orderEntries])
            ->select('id')
            ->indexBy('reserved_id')
            ->column();
        //Сначала находим ошибки, по которым не привязалось ни одного логистического наряда
        $flipLogisticsNaryads = array_flip($logisticsNaryads);
        $errorOrderEntries = ArrayHelper::merge($errorOrderEntries, array_diff($orderEntries, $flipLogisticsNaryads));
        $erpNaryads = ErpNaryad::find()->where(['logist_id' => $logisticsNaryads])->andWhere(['distributed' => false])->select('logist_id')->column();
        $storageState = StorageState::find()->where(['upn_id' => $logisticsNaryads])->select('upn_id')->column();
        $errorLogisticsNaryads = array_diff($logisticsNaryads, $erpNaryads, $storageState);
        $flipErrorLogisticsNaryads = array_flip($errorLogisticsNaryads);
        $errorOrderEntries = ArrayHelper::merge($errorOrderEntries, $flipErrorLogisticsNaryads);
        $orders = OrderEntry::find()
            ->select('order_id')
            ->where(['id' => $errorOrderEntries])
            ->distinct()
            ->column();
        if (empty($orders))
            return [];
        $query->andWhere('id IN (' .implode(',',$orders) .')');
        $query->andWhere(['and',['processed' => true],['shipped' => null]]);
        $query->orderBy(['created_at' => SORT_DESC]);

        return $query->column();
    }
}
