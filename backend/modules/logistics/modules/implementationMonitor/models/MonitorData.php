<?php
namespace backend\modules\logistics\modules\implementationMonitor\models;

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\modules\implementationMonitor\interfaces\ICommandService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IElementService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IElementsRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\ILocationService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnService;
use core\entities\logisticsShipment\models\Shipment;
use core\models\box\Box;
use core\services\SBox;
use obmen\models\laitovo\Cars;
use yii\data\ArrayDataProvider;
use Yii;
use yii\helpers\ArrayHelper;

class MonitorData
{
    private $_ordersRepository;
    private $_elementsRepository;
    private $_orderService;
    private $_elementService;
    private $_upnService;
    private $_shipmentManager;
    private $_deleteOrderEntry;
    private $_packShipment;
    private $_getStorageBalance;

    public function __construct(
        IOrdersRepository $ordersRepository,
        IOrderElementsRepository $elementsRepository,
        IOrderService $orderService,
        IOrderElementService $elementService,
        IUpnService $upnService
    )
    {
        $this->_ordersRepository   =$ordersRepository;
        $this->_elementsRepository =$elementsRepository;
        $this->_orderService       =$orderService;
        $this->_elementService     =$elementService;
        $this->_upnService         =$upnService;
        $this->_shipmentManager    = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_deleteOrderEntry   = Yii::$container->get('core\logic\DeleteOrderEntry');
        $this->_packShipment       = Yii::$container->get('core\logic\PackShipment');
        $this->_getStorageBalance  = Yii::$container->get('core\logic\GetStorageBalance');
    }

    public function getData()
    {
        //Получить заявки для монитора
        $orders = $this->_ordersRepository->getOrders();
        $data = $this->_generateData($orders);
        $data = $this->_asProvider($data);

        return $data;
    }

    public function getDataShipment()
    {
//        return count($this->_shipmentManager->findShipmentsPacked());
        return count($this->_shipmentManager->findShipmentsMustBeShipped(time()));
    }

    public function getDataNextShipment()
    {
//        return count($this->_shipmentManager->findShipmentsPacked());
        return count($this->_shipmentManager->findShipmentsMustBeShipped(time(),true));
    }

    public function getDataPackedForSheep()
    {
//        return count($this->_shipmentManager->findShipmentsPacked());
        return count($this->_shipmentManager->findShipmentsPackedForShip(time()));
    }

    public function getDataCarriedOver()
    {
//        return count($this->_shipmentManager->findShipmentsPacked());
        return count($this->_shipmentManager->findShipmentsCarriedOver(time()));
    }

    public function getDataJournal($searchParams)
    {
        //Получить заказы для монитора, которые не могут быть выполнены.
        $dangerOrders   = $this->_ordersRepository->getOrdersJournalDangerous();

        //Получить заявки для монитора
        $orders   = $this->_ordersRepository->getOrdersJournal($searchParams);
        $tempData = $this->_generateData($orders);
        $data     = $tempData;
        $search     = $searchParams['search'] ?? null;
        $onlyActual = $searchParams['onlyActual'] ?? null;
//        $onlyDangerous = $searchParams['onlyDangerous'] ?? null;

//        $hasDangerousOrders = false;
        foreach ($tempData as $orderKey => $order) {
//            $allElementsAccountedIn1C = true;
            $allElementsShipped = true;
            $isDangerous = false;
            foreach ($order['elements'] as $elementKey => $element) {
                //Помечаем позиции, по которым наряд находится в статусе "Готов":
                $data[$orderKey]['elements'][$elementKey]['naryadIsReady'] = false;
                if (!empty($element['upn'])) {
                    $naryad = ErpNaryad::find()->where(['logist_id' => $element['upn']])->one();
                    if ($naryad && $naryad->status == ErpNaryad::STATUS_READY) {
                        $data[$orderKey]['elements'][$elementKey]['naryadIsReady'] = true;
                    }
                }
                $shipment = $this->_shipmentManager->findById($element['shipmentId']);
//                if ($shipment && $this->_shipmentManager->getAccountedIn1C($shipment)) {
//                    $data[$orderKey]['elements'][$elementKey]['accountedIn1C'] = true;
//                } else {
//                    $data[$orderKey]['elements'][$elementKey]['accountedIn1C'] = false;
//                    $allElementsAccountedIn1C = false;
//                }

                if ($shipment && $this->_shipmentManager->isShipped($shipment)) {
                    $data[$orderKey]['elements'][$elementKey]['shipped'] = true;
                } else {
                    $data[$orderKey]['elements'][$elementKey]['shipped'] = false;
                    if (!$element['isDeleted']) {
                        $allElementsShipped = false;
                    }
                }

                if ($shipment && $this->_shipmentManager->isHandled($shipment)) {
                    $data[$orderKey]['elements'][$elementKey]['isHandled'] = true;
                } else {
                    $data[$orderKey]['elements'][$elementKey]['isHandled'] = false;
                }

                if ($shipment && $this->_shipmentManager->isPacked($shipment)) {
                    $data[$orderKey]['elements'][$elementKey]['isPacked'] = true;
                } else {
                    $data[$orderKey]['elements'][$elementKey]['isPacked'] = false;
                }

                if ($element['upn'] && !$element['workOrderExists'] && !$element['storageExists']) {
                    $data[$orderKey]['elements'][$elementKey]['isDangerous'] = true;
                    $isDangerous = true;
                } else {
                    $data[$orderKey]['elements'][$elementKey]['isDangerous'] = false;
                }
            }
            $isActual = !$allElementsShipped;         //Актуальными считаем заказы, у которых есть неотгруженные позиции
            $isDangerous = $isDangerous && $isActual; //Опасными считаем актуальные заказы, у которых есть позиции с UPN,
                                                      //который не на складе и не на производстве
            $data[$orderKey]['isDangerous'] = $isDangerous;

            if (($search == null) && $onlyActual && !$isActual) {
                unset($data[$orderKey]);
            }
//            if (($search == null) && $onlyDangerous && !$isDangerous) {
//                unset($data[$orderKey]);
//            }
//            if ($isDangerous) {
//                $hasDangerousOrders = true;
//            }
        }
        if ($onlyActual) {
            $data = $this->_sortOrdersByReadyToPack($data); //Для списка актуальных заказов применяем особую сортировку
        }
        $data = $this->_asProvider($data);

        return [
            'provider' => $data,
//            'hasDangerousOrders' => $hasDangerousOrders
            'dangerOrders' => $dangerOrders
        ];
    }

//    public function getDataStorage()
//    {
//        //Получить заявки для монитора
//        $orders = $this->_ordersRepository->getOrdersStorage();
//        $data = $this->_generateData($orders);
//        $data = $this->_asProvider($data);
//
//        return $data;
//    }

    public function getDataStorage($params)
    {
        $data = [];
        //Вкраплен алгоритм с коробками
        $boxes = Box::find()->readyToPack();
        $boxesIds = ArrayHelper::map($boxes,'id','id');
        $shipmentIds = SBox::getShipmentIdsForBoxes($boxesIds);


        $shipments = $this->_findStorageShipments($params);
        foreach ($shipments as $shipment) {
            if (!$this->_shipmentManager->isHandled($shipment) && !$this->_shipmentManager->isOnStorageRequest($shipment) && !$this->_shipmentManager->isPacked($shipment) && !in_array($this->_shipmentManager->getId($shipment),$shipmentIds)) {
                continue;
            }
            $shipmentId = $this->_shipmentManager->getId($shipment);
            if (!isset($data[$shipmentId])) {
                $data[$shipmentId] = $this->_addShipmentRow($shipment);
            }
            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
            foreach ($orderEntries as $orderEntry) {
                $orderId = $this->_elementService->getOrderId($orderEntry);
                $boxId = $orderEntry->boxId ?? 0;
                $boxStatus = $this->_getBoxStatus($orderEntry);
                if (!isset($data[$shipmentId]['boxes'][$boxId])) {
                    $data[$shipmentId]['boxes'][$boxId]['status'] = $boxStatus;
                    $data[$shipmentId]['boxes'][$boxId]['id'] = $boxId;
                }
                if (!isset($data[$shipmentId]['boxes'][$boxId]['orders'][$orderId])){
//                if (!isset($data[$shipmentId]['orders'][$orderId])) {
                    $order = $this->_ordersRepository->getOrderById($orderId);
//                    $data[$shipmentId]['orders'][$orderId] = $this->_addOrderRow($order);
//                    $data[$shipmentId]['orders'][$orderId]['elements'] = [];
                    $data[$shipmentId]['boxes'][$boxId]['orders'][$orderId] = $this->_addStorageOrderRow($order);
                }
//                $data[$shipmentId]['orders'][$orderId]['elements'][] = $this->_addOrderElementRow($orderEntry);
                $element = $this->_addStorageOrderElementRow($orderEntry);
                $element['factBalance'] = $this->_getStorageBalance->getFactBalanceByArticle($element['article']);
                $element['hasBalanceControl'] = $this->_getStorageBalance->isArticleOnBalanceControl($element['article']);
                $element['minBalance'] = $this->_getStorageBalance->getMinBalanceByArticle($element['article']);
                $parentIndex = $element['parentIndex'] ?: 0;
                $data[$shipmentId]['boxes'][$boxId]['orders'][$orderId]['elements'][$parentIndex][] = $element;
            }
        }
        if (($search = $params['search']) && !$params['filter']) {
            $data = $this->_searchStorageShipments($search, $data);
        }
        $data = $this->_sortStorageShipments($data);

//        var_dump($data);
//        die;
        $data = $this->_asProvider($data);

        return $data;
    }

//    public function getDataStorage($params)
//    {
//        $data = [];
//        $shipments = $this->_findStorageShipments($params);
//        foreach ($shipments as $shipment) {
//            if (!$this->_shipmentManager->isHandled($shipment) && !$this->_shipmentManager->isOnStorageRequest($shipment) && !$this->_shipmentManager->isPacked($shipment)) {
//                continue;
//            }
//            $shipmentId = $this->_shipmentManager->getId($shipment);
//            if (!isset($data[$shipmentId])) {
//                $data[$shipmentId] = $this->_addShipmentRow($shipment);
//            }
//            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
//            foreach ($orderEntries as $orderEntry) {
//                $orderId = $this->_elementService->getOrderId($orderEntry);
//                if (!isset($data[$shipmentId]['orders'][$orderId])) {
//                    $order = $this->_ordersRepository->getOrderById($orderId);
////                    $data[$shipmentId]['orders'][$orderId] = $this->_addOrderRow($order);
////                    $data[$shipmentId]['orders'][$orderId]['elements'] = [];
//                    $data[$shipmentId]['orders'][$orderId] = $this->_addStorageOrderRow($order);
//                }
////                $data[$shipmentId]['orders'][$orderId]['elements'][] = $this->_addOrderElementRow($orderEntry);
//                $element = $this->_addStorageOrderElementRow($orderEntry);
//                $element['factBalance'] = $this->_getStorageBalance->getFactBalanceByArticle($element['article']);
//                $element['hasBalanceControl'] = $this->_getStorageBalance->isArticleOnBalanceControl($element['article']);
//                $element['minBalance'] = $this->_getStorageBalance->getMinBalanceByArticle($element['article']);
//                $data[$shipmentId]['orders'][$orderId]['elements'][] = $element;
//            }
//        }
//        if (($search = $params['search']) && !$params['filter']) {
//            $data = $this->_searchStorageShipments($search, $data);
//        }
//        $data = $this->_sortStorageShipments($data);
//        $data = $this->_asProvider($data);
//
//        return $data;
//    }

//    public function getDataForOne($id)
//    {
//        //Получить заявки для монитора
//        $order = $this->_ordersRepository->getOrderById($id);
//        $array = [$order];
//        $data = $this->_generateData($array);
//        $data = $this->_asProvider($data);
//
//        return $data;
//    }

    public function getDataForOne($shipmentId)
    {
        $data = [];
        $shipment = $this->_shipmentManager->findById($shipmentId);
        $shipments = [$shipment];
        foreach ($shipments as $shipment) {
            $shipmentId = $this->_shipmentManager->getId($shipment);
            if (!isset($data[$shipmentId])) {
                $data[$shipmentId] = $this->_addShipmentRow($shipment);
            }
            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
            foreach ($orderEntries as $orderEntry) {
                $orderId = $this->_elementService->getOrderId($orderEntry);
                if (!isset($data[$shipmentId]['orders'][$orderId])) {
                    $order = $this->_ordersRepository->getOrderById($orderId);
                    $data[$shipmentId]['orders'][$orderId] = $this->_addOrderRow($order);
                    $data[$shipmentId]['orders'][$orderId]['elements'] = [];
                }
//                $data[$shipmentId]['orders'][$orderId]['elements'][] = $this->_addOrderElementRow($orderEntry);
                $element = $this->_addOrderElementRow($orderEntry);
                $parentIndex = $element['parentIndex'] ?: 0;
                $element['factBalance'] = $this->_getStorageBalance->getFactBalanceByArticle($element['article']);
                $element['hasBalanceControl'] = $this->_getStorageBalance->isArticleOnBalanceControl($element['article']);
                $element['minBalance'] = $this->_getStorageBalance->getMinBalanceByArticle($element['article']);
                $data[$shipmentId]['orders'][$orderId]['elements'][$parentIndex][] = $element;
            }
        }
        $data = $this->_asProvider($data);

        return $data;
    }

    public function getDataForOneBox($boxId)
    {
        $data = [];
        $box = Box::findOne($boxId);
        $orderEntries = $box->orderEntries;
        foreach ($orderEntries as $orderEntry) {
            $orderId = $orderEntry->order_id;
            if (!isset($data[$boxId]['orders'][$orderId])) {
                $order = $this->_ordersRepository->getOrderById($orderId);
                $data[$boxId]['orders'][$orderId] = $this->_addOrderRow($order);
                $data[$boxId]['orders'][$orderId]['elements'] = [];
            }
            $element = $this->_addStorageOrderElementRow($orderEntry);
            $parentIndex = $element['parentIndex'] ?: 0;
            $element['factBalance'] = $this->_getStorageBalance->getFactBalanceByArticle($element['article']);
            $element['hasBalanceControl'] = $this->_getStorageBalance->isArticleOnBalanceControl($element['article']);
            $element['minBalance'] = $this->_getStorageBalance->getMinBalanceByArticle($element['article']);
            $data[$boxId]['orders'][$orderId]['elements'][$parentIndex][] = $element;
        }

        $data = $this->_asProvider($data);

        return $data;
    }

    public function getDataForOneBoxNew($boxId)
    {
        $data = [];
        $box = Box::findOne($boxId);
        $orderEntries = $box->orderEntries;
        foreach ($orderEntries as $orderEntry) {
            $orderId = $orderEntry->order_id;
            if (!isset($data[$boxId]['orders'][$orderId])) {
                $order = $this->_ordersRepository->getOrderById($orderId);
                $data[$boxId]['orders'][$orderId] = $this->_addOrderRowForPackNew($order);
                $data[$boxId]['orders'][$orderId]['elements'] = [];
            }
            $element = $this->_addStorageOrderElementRowNew($orderEntry);
            $parentIndex = $element['parentIndex'] ?: 0;
            $data[$boxId]['orders'][$orderId]['elements'][$parentIndex][] = $element;
        }

        $data = $this->_asProvider($data);

        return $data;
    }

    public function getDataByShipmentId($shipmentId)
    {
        $shipment     = $this->_shipmentManager->findById($shipmentId);
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        $orders = [];
        foreach ($orderEntries as $orderEntry) {
            $orderId = $this->_elementService->getOrderId($orderEntry);
            if (!isset($orders[$orderId])) {
                $order                        = $this->_ordersRepository->getOrderById($orderId);
                $orders[$orderId]             = $this->_addOrderRow($order);
                $orders[$orderId]['elements'] = [];
            }
            $elementRow = $this->_addStorageOrderElementRow($orderEntry);
            $parentIndex = $elementRow['parentIndex'] ?: 0;
            $orders[$orderId]['elements'][$parentIndex][] = $this->_addStorageOrderElementRow($orderEntry);
        }

        return [
            'shipmentId' => $shipmentId,
            'orders'     => $orders,
        ];
    }

    public function getDataByBoxId($boxId)
    {
        $shipmentId = null;
        $shipment   = null;
        $box = Box::findOne($boxId);
        $orderEntries = $box->orderEntries;

        usort($orderEntries, function ($a, $b) {
            $first = 0;
            $second = 0;
            if (($upn = ($a->upn)) && ($state = $upn->storageState)) {
                preg_match_all("/\d+/", $state->literal, $matches);
                $first = (int)$matches[0][0];
            }
            if (($upn = ($b->upn)) && ($state = $upn->storageState)) {
                preg_match_all("/\d+/", $state->literal, $matches);
                $second = (int)$matches[0][0];
            }
            switch (true) {
                case intval($first) > intval($second):
                    return 1;
                default:
                    return 0;
            }
        });

        $orders = [];
        foreach ($orderEntries as $orderEntry) {
            /**
             * @var $orderEntry OrderEntry
             */
            $orderId = $this->_elementService->getOrderId($orderEntry);
            if (!$shipmentId) $shipmentId = $orderEntry->shipment_id;
            if (!isset($orders[$orderId])) {
                $order                        = $this->_ordersRepository->getOrderById($orderId);
                $orders[$orderId]             = $this->_addOrderRow($order);
                $orders[$orderId]['elements'] = [];
            }
            $shipment    = $this->_shipmentManager->findById($shipmentId);
            $elementRow  = $this->_addStorageOrderElementRow($orderEntry);
            $parentIndex = $elementRow['parentIndex'] ?: 0;
            $orders[$orderId]['elements'][$parentIndex][] = $this->_addStorageOrderElementRow($orderEntry);
        }

        return [
            'boxId' => $box->id,
            'shipmentId' => $shipmentId,
            'shipmentComment' => $shipment ? $shipment->comment : '',
            'remoteBarcode' => $box->remoteStorageBarcode,
            'remoteLiteral' => $box->remoteStorageLiteral,
            'orders'=> $orders,
        ];
    }

    public function getBoxedDataByShipmentId($shipmentId)
    {
        $shipment     = \core\models\shipment\Shipment::findOne($shipmentId);
        $orderEntries = $shipment->notDeletedOrderEntries;
        $boxes = [];

        usort($orderEntries, function ($a, $b) {
            $first = 0;
            $second = 0;
            if (($upn = ($a->upn)) && ($state = $upn->storageState)) {
                preg_match_all("/\d+/", $state->literal, $matches);
                $first = (int)$matches[0][0];
            }
            if (($upn = ($b->upn)) && ($state = $upn->storageState)) {
                preg_match_all("/\d+/", $state->literal, $matches);
                $second = (int)$matches[0][0];
            }
            switch (true) {
                case intval($first) > intval($second):
                    return 1;
                default:
                    return 0;
            }
        });

        foreach ($orderEntries as $orderEntry) {
            /**
             * @var $orderEntry OrderEntry
             */
            $box = $orderEntry->boxId;
            $orderId = $this->_elementService->getOrderId($orderEntry);
            if (!isset($boxes[$box])) {
                $boxes[$box]['id'] = $box;
            }
            if (!isset($boxes[$box]['orders'][$orderId])) {
                $order                        = $this->_ordersRepository->getOrderById($orderId);
                $boxes[$box]['orders'][$orderId]             = $this->_addOrderRow($order);
                $boxes[$box]['orders'][$orderId]['elements'] = [];
            }
            $elementRow = $this->_addStorageOrderElementRow($orderEntry);
            $parentIndex = $elementRow['parentIndex'] ?: 0;
            $boxes[$box]['orders'][$orderId]['elements'][$parentIndex][] = $elementRow;
        }

        return [
            'shipmentId' => $shipmentId,
            'boxes'      => $boxes,
            'comment'    => $shipment->comment,
        ];
    }

    public function getPackageDataByShipmentId($shipmentId)
    {
        $shipment     = $this->_shipmentManager->findById($shipmentId);
        $shipmentDate = $this->_shipmentManager->getShipmentDate($shipment);
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        $orders = [];
        foreach ($orderEntries as $orderEntry) {
            $orderId = $this->_elementService->getOrderId($orderEntry);
            if (!isset($orders[$orderId])) {
                $order                        = $this->_ordersRepository->getOrderById($orderId);
                $orders[$orderId]             = $this->_addOrderRow($order);
                $orders[$orderId]['elements'] = [];
            }
            $orders[$orderId]['elements'][] = $this->_addPackageOrderElementRow($orderEntry);
        }

        return [
            'shipmentId' => $shipmentId,
            'shipmentDate' => $shipmentDate ? date('d.m.y',$shipmentDate) : '',
            'orders'     => $orders,
        ];
    }

    public function getPackageDataByBoxId($boxId)
    {
        $shipmentId = null;
        $box = Box::findOne($boxId);
        $orderEntries = $box->orderEntries;
        $orders = [];
        foreach ($orderEntries as $orderEntry) {
            /**
             * @var $orderEntry OrderEntry
             */
            $orderId = $this->_elementService->getOrderId($orderEntry);
            if (!$shipmentId) $shipmentId = $orderEntry->shipment_id;
            if (!isset($orders[$orderId])) {
                $order                        = $this->_ordersRepository->getOrderById($orderId);
                $orders[$orderId]             = $this->_addOrderRow($order);
                $orders[$orderId]['elements'] = [];
            }
            $orders[$orderId]['elements'][] = $this->_addPackageOrderElementRow($orderEntry);
        }
        $shipment     = $this->_shipmentManager->findById($shipmentId);
        $shipmentDate = $this->_shipmentManager->getShipmentDate($shipment);

        return [
            'boxId' => $box->id,
            'shipmentId' => $shipmentId,
            'shipmentDate' => $shipmentDate ? date('d.m.y',$shipmentDate) : '',
            'orders'     => $orders,
        ];
    }

    public function getPackageDataByBoxIdNew($boxId)
    {
        $shipmentId = null;
        $box = Box::findOne($boxId);
        $orderEntries = $box->orderEntries;

        $groups = [];
        $solo = [];
        $result = [];

        $count = 0;

        foreach ($orderEntries as $orderEntry) {
            /**
             * @var $orderEntry OrderEntry
             */
            $index = $orderEntry->setIndex;
//            if ($index && ($order = $orderEntry->order) && $order->remoteStorage) {
            if ($index && ($order = $orderEntry->order) && $order->username != 'J. Auto lnternational Company Limited') {
                $orderId = $orderEntry->order_id ?:0;
                $key = $orderId . '_' .$index;
                $groups[$key][] = $orderEntry;
            }else{
                $solo[] = $orderEntry;
            }
            if (!$shipmentId) $shipmentId = $orderEntry->shipment_id;
        }

        foreach ($solo as $orderEntry) {
            $article = $orderEntry->article;
            /**
             * @var $car Cars
             */
            $car = ArticleHelper::getCarModel($orderEntry->article);
            if (!$car) {
                $title = ArticleHelper::getInvoiceRusTitle($orderEntry->article) . ' / ' . ArticleHelper::getInvoiceTitle($orderEntry->article);
            }else {
                $title = "Защитные экраны для автомобильных окон / Protective screens for car windows<br>{$car->fullEnName}<br>({$article})";
            }
            $key = $article;
            if (isset($result[$key])) {
                $result[$key]['quantity']++;
            } else {
                $result[$key] = [
                    'number'   => ++$count,
                    'title'    => $title,
                    'quantity' => 1,
                    'pcs'      => 'Комп. / Set',
                ];
            }
        }

        foreach ($groups as $group) {
            $car   = null;
            $articles = [];
            foreach ($group as $orderEntry) {
                /**
                 * @var $car Cars
                 */
                if (!$car)
                    $car = ArticleHelper::getCarModel($orderEntry->article);
                $articles[] = $orderEntry->article;
            }
            $key  = implode('_',$articles);
            $title = "Защитные экраны для автомобильных окон / Protective screens for car windows<br>{$car->fullEnName}<br>(SET :" . implode(', ',$articles).")";

            if (isset($result[$key])) {
                $result[$key]['quantity']++;
            } else {
                $result[$key] = [
                    'number'   => ++$count,
                    'title'    => $title,
                    'quantity' => 1,
                    'pcs'      => 'Комп. / Set',
                ];
            }
        }

        $shipment     = $this->_shipmentManager->findById($shipmentId);
        $shipmentDate = $this->_shipmentManager->getShipmentDate($shipment);

        return [
            'boxId' => $box->id,
            'shipmentId' => $shipmentId,
            'shipmentDate' => $shipmentDate ? date('d.m.y',$shipmentDate) : '',
            'result' => $result,
            'remoteBarcode' => $box->remoteStorageBarcode,
            'remoteLiteral' => $box->remoteStorageLiteral,
        ];
    }

    /**
     * Создает дату для предоставления клиенту
     * @param $orders
     * @return array
     */
    private function _generateData($orders)
    {
        $data = [];
        foreach ($orders as $order) {
            $data[] = $this->_addOrderRow($order);
        }
        return $data;
    }

    /**
     * Функция для генерации структурного элемента данных
     * @param $order
     * @return array
     */
    private function _addOrderRow($order)
    {
        $row = [];

        //Общая информация о заявке
        $orderId      = $this->_orderService->getNumber($order);
        $manager      = $this->_orderService->getManager($order);
        $delivery     = $this->_orderService->getDelivery($order);
        $address      = $this->_orderService->getAddress($order);
        $innerComment = $this->_orderService->getInnerComment($order);
        $createdAt    = $this->_orderService->getCreatedAt($order);
        $isNeedTtn    = $this->_orderService->isNeedTtn($order);
        $isCOD        = $this->_orderService->isCOD($order);

        //Информация о клиента
        $clientName     = $this->_orderService->getClientName($order);
        $clientPhone    = $this->_orderService->getClientPhone($order);
        $clientEmail    = $this->_orderService->getClientEmail($order);
        $clientCategory = $this->_orderService->getClientCategory($order);

        $exportClientName = $this->_orderService->getExportClientName($order);
        $exportCategory   = $this->_orderService->getExportClientCategory($order);


        $orderSourceId = $this->_orderService->getSourceNumber($order);
        $command       = $this->_orderService->getCommand($order);
        $planDate      = $this->_orderService->getPlanShipmentDate($order);
        $packDate      = $this->_orderService->getPackageDate($order);
        $factDate      = $this->_orderService->getFactShipmentDate($order);
        $elements      = $this->_addElements($order);
        $isHandled     = $this->_orderService->isHandled($order);
        $isOnWeighing  = $this->_orderService->isOnWeighing($order);

        $row['orderId']       = $orderId;
        $row['manager']       = $manager;
        $row['delivery']      = $delivery;
        $row['address']       = $address;
        $row['innerComment']  = $innerComment;
        $row['comment']       = $order->comment;
        $row['createdAt']     = $createdAt;
        $row['isNeedTtn']     = $isNeedTtn;
        $row['isCOD']         = $isCOD;

        $row['client']        = $clientName;
        $row['phone']         = $clientPhone;
        $row['email']         = $clientEmail;
        $row['category']      = $clientCategory;

        $row['exportClientName'] = $exportClientName;
        $row['exportCategory']   = $exportCategory;

        $row['sourceId']      = $orderSourceId;
        $row['command']       = $command;
        $row['planDate']      = $planDate;
        $row['packDate']      = $packDate;
        $row['factDate']      = $factDate;
        $row['elements']      = $elements;
        $row['isHandled']     = $isHandled;
        $row['isOnWeighing']  = $isOnWeighing;

        return $row;
    }

    /**
     * Функция для генерации структурного элемента данных
     * @param $order
     * @return array
     */
    private function _addOrderRowForPackNew($order)
    {
        $row = [];

        $innerComment = $this->_orderService->getInnerComment($order);
        $orderSourceId = $this->_orderService->getSourceNumber($order);

        $row['innerComment']  = $innerComment;
        $row['sourceId']      = $orderSourceId;

        return $row;
    }

    /**
     * Функция для генерации структурного элемента данных
     * @param $order
     * @return array
     */
    private function _addStorageOrderRow($order)
    {
        $row = [];

        //Общая информация о заявке
        $orderId      = $this->_orderService->getNumber($order);
        $manager      = $this->_orderService->getManager($order);
        $delivery     = $this->_orderService->getDelivery($order);
        $address      = $this->_orderService->getAddress($order);
        $innerComment = $this->_orderService->getInnerComment($order);
        $createdAt    = $this->_orderService->getCreatedAt($order);
        $isNeedTtn    = $this->_orderService->isNeedTtn($order);
        $isCOD        = $this->_orderService->isCOD($order);

        //Информация о клиента
        $clientName     = $this->_orderService->getClientName($order);
        $clientPhone    = $this->_orderService->getClientPhone($order);
        $clientEmail    = $this->_orderService->getClientEmail($order);
        $clientCategory = $this->_orderService->getClientCategory($order);

        $exportClientName = $this->_orderService->getExportClientName($order);
        $exportCategory   = $this->_orderService->getExportClientCategory($order);


        $orderSourceId = $this->_orderService->getSourceNumber($order);
        $command       = $this->_orderService->getCommand($order);
        $planDate      = $this->_orderService->getPlanShipmentDate($order);
        $packDate      = $this->_orderService->getPackageDate($order);
        $factDate      = $this->_orderService->getFactShipmentDate($order);
//        $elements      = $this->_addElements($order);
        $isHandled     = $this->_orderService->isHandled($order);
        $isOnWeighing  = $this->_orderService->isOnWeighing($order);

        $row['orderId']       = $orderId;
        $row['manager']       = $manager;
        $row['delivery']      = $delivery;
        $row['address']       = $address;
        $row['innerComment']  = $innerComment;
        $row['createdAt']     = $createdAt;
        $row['isNeedTtn']     = $isNeedTtn;
        $row['isCOD']         = $isCOD;

        $row['client']        = $clientName;
        $row['phone']         = $clientPhone;
        $row['email']         = $clientEmail;
        $row['category']      = $clientCategory;

        $row['exportClientName'] = $exportClientName;
        $row['exportCategory']   = $exportCategory;

        $row['sourceId']      = $orderSourceId;
        $row['command']       = $command;
        $row['planDate']      = $planDate;
        $row['packDate']      = $packDate;
        $row['factDate']      = $factDate;
//        $row['elements']      = $elements;
        $row['isHandled']     = $isHandled;
        $row['isOnWeighing']  = $isOnWeighing;

        return $row;
    }

    /**
     * Сгенерировать массив элементов заказа
     * @param $order
     * @return mixed
     */
    private function _addElements($order)
    {
        $rows = [];

        $elements = $this->_elementsRepository->getElementsForOrder($order);
        foreach ($elements as $element) {
            $rows[] = $this->_addOrderElementRow($element);
        }

        return $rows;
    }

    /**
     * Функция - которая генеррует строку элемента заказа
     * @param $element
     * @return array
     */
    private function _addOrderElementRow($element)
    {
        $row = [];

        $article         = $this->_elementService->getArticle($element);
        $title           = $this->_elementService->getTitle($element);
        $upn             = $this->_elementService->getReservedUpn($element);
        $workOrderExists = !$upn ? false: ($upn->erpNaryad ? true: false);
        $storageExists   = !$upn ? false: ($upn->storageState ? true: false);
        $location        = $this->_upnService->getLocation($upn);
        $storage         = $this->_elementService->isStoragePosition($element);
        $isCustom        = $this->_elementService->isCustom($element);
        $car             = $this->_elementService->getCar($element);
        $barcode         = $this->_elementService->getSourceBarcode($element);
        $orderRow        = $this->_elementService->getOrderRow($element);
        $destroyer       = $this->_elementService->getDestroyer($element);
        $deletedBy       = $destroyer->name ?? null;
        $shipment        = $this->_shipmentManager->findById($element->shipment_id);
        $trackCode       = !$shipment ? null : $this->_shipmentManager->getTrackCode($shipment);
        $shipmentDate    = !$shipment ? null : $this->_shipmentManager->getShipmentDate($shipment);

        $row['article']       = $article;
        $row['title']         = $title;
        $row['upn']           = $this->_upnService->getId($upn);
        $row['parentIndex']   = $this->_upnService->getParent($upn);
        $row['storageExists'] = $storageExists;
        $row['workOrderExists'] = $workOrderExists;
        $row['location']      = $location;
        $row['pack']          = $this->_upnService->isPacked($upn);
        $row['readyToPack']   = !$upn ? null : $this->_upnService->isReadyToPackage($upn);
        $row['storage']       = $storage;
        $row['isCustom']      = $isCustom;
        $row['car']           = $car;
        $row['barcode']       = $barcode;
        $row['row']           = $orderRow;
        $row['shipmentId']    = $element->shipment_id;
        $row['id']            = $element->id;
        $row['isDeleted']     = $element->is_deleted;
        $row['deletedAt']     = $element->deleted_at;
        $row['deletedBy']     = $deletedBy;
        $row['canBeDeleted']  = $this->_deleteOrderEntry->canBeDeleted($element);
        $row['canBeRestored'] = $this->_deleteOrderEntry->canBeRestored($element);
        $row['trackCode']     = $trackCode;
        $row['shipmentDate']  = $shipmentDate;

        return $row;
    }

    private function _addStorageOrderElementRow($element)
    {
        $row = [];

        $article   = $this->_elementService->getArticle($element);
        $title     = $this->_elementService->getTitle($element);
        $itemtext  = $this->_elementService->getItemText($element);
        $upn       = $this->_elementService->getReservedUpn($element);
        $storageExists = !$upn ? false: ($upn->storageState ? true: false);
        $workOrderExists = !$upn ? false: ($upn->erpNaryad ? true: false);
        $location  = $this->_upnService->getLocationStorage($upn);
        $storage   = $this->_elementService->isStoragePosition($element);
        $car       = $this->_elementService->getCar($element);
        $barcode   = $this->_elementService->getSourceBarcode($element);
        $orderRow  = $this->_elementService->getOrderRow($element);
        $destroyer = $this->_elementService->getDestroyer($element);
        $deletedBy = $destroyer->name ?? null;
        $shipment  = $this->_shipmentManager->findById($element->shipment_id);
        $trackCode = !$shipment ? null : $this->_shipmentManager->getTrackCode($shipment);

        $row['article']       = $article;
        $row['title']         = $title;
        $row['itemtext']      = $itemtext;
        $row['upn']           = $this->_upnService->getId($upn);
        $row['parentIndex']   = $this->_upnService->getParent($upn);;
        $row['storageExists'] = $storageExists;
        $row['workOrderExists'] = $workOrderExists;
        $row['location']      = $location;
        $row['pack']          = $this->_upnService->isPacked($upn);
        $row['readyToPack']   = !$upn ? null : $this->_upnService->isReadyToPackage($upn);
        $row['storage']       = $storage;
        $row['car']           = $car;
        $row['barcode']       = $barcode;
        $row['row']           = $orderRow;
        $row['shipmentId']    = $element->shipment_id;
        $row['id']            = $element->id;
        $row['isDeleted']     = $element->is_deleted;
        $row['deletedAt']     = $element->deleted_at;
        $row['deletedBy']     = $deletedBy;
        $row['canBeDeleted']  = $this->_deleteOrderEntry->canBeDeleted($element);
        $row['canBeRestored'] = $this->_deleteOrderEntry->canBeRestored($element);
        $row['trackCode']     = $trackCode;

        return $row;
    }

    private function _addStorageOrderElementRowNew($element)
    {
        $row = [];

        $article   = $this->_elementService->getArticle($element);
        $title     = $this->_elementService->getTitle($element);
        $upn       = $this->_elementService->getReservedUpn($element);
        $location  = $this->_upnService->getLocationStorage($upn);

        $row['article']       = $article;
        $row['title']         = $title;
        $row['upn']           = $this->_upnService->getId($upn);
        $row['parentIndex']   = $this->_upnService->getParent($upn);;
        $row['location']      = $location;
        $row['pack']          = $this->_upnService->isPacked($upn);

        return $row;
    }

    private function _addPackageOrderElementRow($element)
    {
        $row = [];

        $article   = $this->_elementService->getArticle($element);
        $title     = $this->_elementService->getInvoiceTitle($element);

        $row['article']       = $article;
        $row['title']         = $title;

        return $row;
    }

    private function _addShipmentRow($shipment)
    {
        $row = [];

        $shipmentId         = $this->_shipmentManager->getId($shipment);
        $shipmentDate       = $this->_shipmentManager->getShipmentDate($shipment);
        $hasWeighing        = $this->_shipmentManager->getHasWeighing($shipment);
        $hasSizing          = $this->_shipmentManager->getHasSizing($shipment);
        $weight             = $this->_shipmentManager->getWeight($shipment);
        $size               = $this->_shipmentManager->getSize($shipment);
        $isOnStorageRequest = $this->_shipmentManager->isOnStorageRequest($shipment);
        $isHandled          = $this->_shipmentManager->isHandled($shipment);
        $isPacked           = $this->_shipmentManager->isPacked($shipment);
        $hasUpns            = $this->_isHasUpns($shipment);

        $row['shipmentId']         = $shipmentId;
        $row['shipmentDate']       = $shipmentDate;
        $row['hasWeighing']       = $hasWeighing;
        $row['hasSizing']         = $hasSizing;
        $row['weight']             = $weight;
        $row['size']               = $size;
        $row['isHandled']          = $isHandled;
        $row['isOnStorageRequest'] = $isOnStorageRequest;
        $row['isPacked']           = $isPacked;
        $row['hasUpns']            = $hasUpns;
        $row['boxes']              = [];

        return $row;
    }

    private function _isHasUpns($shipment)
    {
        foreach ($this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment) as $orderEntry) {
            if ($orderEntry->upn) {
                return true;
            }
        }

        return false;
    }

    /**
     * Сортировка заказов по степени готовности к упаковке на складе
     *
     * @param array $ordersData
     * @return array
     */
    private function _sortOrdersByReadyToPack($ordersData)
    {
        uasort($ordersData, function ($a, $b) {
            $readyCount1 = $this->_getReadyToPackElementsCount($a);
            $readyCount2 = $this->_getReadyToPackElementsCount($b);
            $deletedCount1 = $this->_getDeletedElementsCount($a);
            $deletedCount2 = $this->_getDeletedElementsCount($b);
            $notReadyCount1 = count($a['elements']) - $readyCount1 - $deletedCount1;
            $notReadyCount2 = count($b['elements']) - $readyCount2 - $deletedCount2;
            switch (true) {
                case $a['isDangerous'] && !$b['isDangerous']:
                    return -1;
                case !$a['isDangerous'] && $b['isDangerous']:
                    return 1;
                case !$readyCount1 && !$readyCount2:
                    return 0;
                case $readyCount1 && !$readyCount2:
                    return -1;
                case !$readyCount1 && $readyCount2:
                    return 1;
                case $notReadyCount1 < $notReadyCount2:
                    return -1;
                case $notReadyCount1 > $notReadyCount2:
                    return 1;
                default:
                    return 0;
            }
        });

        return $ordersData;
    }

    /**
     * Сортировка отгрузок по важности для кладовщика
     *
     * @param array $shipmentsData
     * @return array
     */
    private function _sortStorageShipments($shipmentsData)
    {
        uasort($shipmentsData, function ($a, $b) {
            switch (true) {
                case $a['isOnStorageRequest'] && !$b['isOnStorageRequest']:
                    return -1;
                case !$a['isOnStorageRequest'] && $b['isOnStorageRequest']:
                    return 1;
                case $a['isHandled'] && !$b['isHandled']:
                    return -1;
                case !$a['isHandled'] && $b['isHandled']:
                    return 1;
                case $a['isPacked'] && !$b['isPacked']:
                    return -1;
                case !$a['isPacked'] && $b['isPacked']:
                    return 1;
                case $a['shipmentDate'] < $b['shipmentDate']:
                    return -1;
                case $a['shipmentDate'] > $b['shipmentDate']:
                    return 1;
                default:
                    return 0;
            }
        });

        return $shipmentsData;
    }

    /**
     * @param $search
     * @param $models
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private function _searchStorageShipments($search, $models)
    {
        $searchPattern = '/' . preg_quote(mb_strtolower($search)) . '/';
        $result = [];
        foreach ($models as $shipmentId => $shipment) {
            $parts   = [];
            $parts[] = $shipment['shipmentId'];
            $parts[] = $shipment['shipmentDate'] ? \Yii::$app->formatter->asDate($shipment['shipmentDate'], 'dd.MM.yyyy'):$shipment['shipmentDate'];
            foreach ($shipment['boxes'] as $box) {
                foreach ($box['orders'] as $order) {
                    $parts[] = $order['manager'];
                    $parts[] = $order['delivery'];
                    $parts[] = $order['address'];
                    $parts[] = date('d.m.Y', $order['createdAt']);
                    $parts[] = $order['client'];
                    $parts[] = $order['phone'];
                    $parts[] = $order['email'];
                    $parts[] = $order['category'];
                    $parts[] = $order['sourceId'];
                    if ($order['isNeedTtn']) {
                        $parts[] = '(С ТТН)';
                    }
                    if ($order['isCOD']) {
                        $parts[] = '(наложка)';
                    }
                }
            }
            if (!preg_match($searchPattern, mb_strtolower(implode('|', $parts)))) {
                continue;
            }
            $result[$shipmentId] = $shipment;
        }

        return $result;
    }

    /**
     * @param array $params
     */
    private function _findStorageShipments($params)
    {
        switch ($params['filter']) {
            case 'ship':
                return $this->_shipmentManager->findShipmentsMustBeShipped(time());
            case 'carried':
                return $this->_shipmentManager->findShipmentsCarriedOver(time());
            case 'packedForShip':
                return $this->_shipmentManager->findShipmentsPackedForShip(time());
            case 'nextShip':
                return $this->_shipmentManager->findShipmentsMustBeShipped(time(),true);
            default:
                return $this->_shipmentManager->findShipmentsBeforeShipping();
        }
    }

    private function _getReadyToPackElementsCount($orderData)
    {
        $count = 0;
        foreach ($orderData['elements'] as $element) {
            if ($element['readyToPack']) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Возвращает статус для коробки.
     *
     * @param $orderEntry
     * @return string
     */
    private function _getBoxStatus($orderEntry)
    {
        if (!$orderEntry->boxId || !$orderEntry->box)
            return $status = 'disabled';
        switch ($orderEntry->box->status) {
            case Box::STATUS_NOT_HANDLED:
                return $status = 'disabled';
            case Box::STATUS_READY_TO_PACK:
                return $status = 'enabled';
            case Box::STATUS_PACKED:
                return $status = 'done';
            default:
                return $status = 'disabled';
        }
    }

    private function _getDeletedElementsCount($orderData)
    {
        $count = 0;
        foreach ($orderData['elements'] as $element) {
            if ($element['isDeleted']) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Функция оборачивает в дата провайдер некий массив
     * @param $data
     * @param array $sort
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    private function _asProvider($data,$sort = [], $pageSize = 10)
    {
        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => $sort,
            ],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }
}