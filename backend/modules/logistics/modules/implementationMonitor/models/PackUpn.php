<?php
namespace backend\modules\logistics\modules\implementationMonitor\models;
use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IBarcodeHelper;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnService;
use core\logic\CheckStoragePosition;
use core\models\box\Box;
use core\services\SBox;
use Yii;

/**
 * Класс отвечает за упаковку некого upn
 * Class PackUpn
 * @package backend\modules\logistics\modules\implementationMonitor\models
 */
class PackUpn
{
    private $_orderElementsRepository;
    private $_ordersRepository;
    private $_orderService;
    private $_upnRepository;
    private $_barcodeHelper;
    private $_upnService;
    private $_shipmentManager;
    private $_packShipment;

    public function __construct(
        IOrderElementsRepository $orderElementsRepository,
        IOrdersRepository $ordersRepository,
        IOrderService $orderService,
        IUpnRepository $upnRepository,
        IBarcodeHelper $barcodeHelper,
        IUpnService $upnService)
    {
        $this->_orderElementsRepository = $orderElementsRepository;
        $this->_ordersRepository        = $ordersRepository;
        $this->_orderService            = $orderService;
        $this->_upnRepository           = $upnRepository;
        $this->_barcodeHelper           = $barcodeHelper;
        $this->_upnService              = $upnService;
        $this->_shipmentManager         = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_packShipment            = Yii::$container->get('core\logic\PackShipment');
    }

    /**
     * @param $shipmentId
     * @param $barcode
     * @return array|mixed
     * @throws \Exception
     */
    public function packForShipment($shipmentId, $barcode)
    {
        $response = [
            'status'  => '',
            'message' => '',
            'shipmentStatus' => '',
        ];
        $shipment = $this->_shipmentManager->findById($shipmentId);
        $elements  = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        $upns      = $this->getUpns($elements);
        if ($this->_isStorageBarcode($barcode)) {
            $article = CheckStoragePosition::getStorageArticleByBarcode($barcode);
            foreach ($upns as $row) {
                if ($row->article == $article && $row->packed == 0) {
                    $upn = $row;
                    break;
                }
            }
        }else{
            $upn = $this->getUpnByBarcode($barcode);
        }
        if (!$this->isIn($upn, $upns) || !$this->pack($upn)) {
            $response = $this->addStatus($response, false);
            $response = $this->addMessage($response, false);
        } else {
            $this->_packShipment->pack($shipment);
            $response = $this->addStatus($response, true);
            $response = $this->addMessage($response, true);
        }
        $response = $this->addShipmentStatus($response, $this->_shipmentManager->isPacked($shipment));

        return $response;
    }

    /**
     * @param $shipmentId
     * @param $barcode
     * @return array|mixed
     * @throws \Exception
     */
    public function packForBox($shipmentId,$boxId, $barcode)
    {
        $response = [
            'status'  => '',
            'message' => '',
            'shipmentStatus' => '',
            'boxStatus' => '',
            'printLabel' => false,
            'upnId' => '',
            'printParentLabel' => false,
            'parentId' => '',
        ];
        $isPackedBox = false;

        $barcode = BarcodeHelper::toLatin($barcode);
        $shipment = $this->_shipmentManager->findById($shipmentId);
        $box = Box::findOne($boxId);
        $act = ErpNaryad::findAct($barcode);
        $elements  = $box->orderEntries;
        $upns      = $this->getUpns($elements);
        if ($this->_isStorageBarcode($barcode)) {
            $upn = null;
            $article = CheckStoragePosition::getStorageArticleByBarcode($barcode);
            if (preg_match('/OT-1189-(47|60|90)/', $article) === 1 || preg_match('/OT-1190-(47|60)/', $article) === 1) {
                $response = $this->addStatus($response, false);
                $response = $this->addMessage($response, false,'Пикнули НЕВЕРНЫЙ штрихкод!');
                return $response;
            }
            foreach ($upns as $row) {
                if ($row->article == $article && $row->packed == 0) {
                    $upn = $row;
                    break;
                }
            }
            if ($upn && CheckStoragePosition::canBeProduced($article)) {
                $workOrder = ErpNaryad::find()->where(['barcode' => $upn->barcode])->one();
                if ($workOrder) {
                    $upn->barcode = 'R' . $upn->barcode;
                    $upn->save();
                }
                $response = $this->addStatus($response, false);
                $response = $this->addMessage($response, false);
                $response = $this->addLabel($response, $upn->id);

                return $response;
            }

            if (!$upn) {
                $response = $this->addStatus($response, false);
                $response['message'] = 'Не найдено соответствий штрих-кода и позиции для упаковки!';
                return $response;
            }
        }elseif ($act) {

        }elseif (preg_match('/OT-1214-47-3/', $barcode) === 1) { //Костыль для жесткого дна
            foreach ($upns as $row) {
                if ($row->article == $barcode && $row->packed == 0) {
                    $upn = $row;
                    break;
                }
            }
        }else{
            $upn = $this->getUpnByBarcode($barcode);
            $workOrder = ErpNaryad::find()->where(['barcode' => $barcode])->one();
            if ($upn && $workOrder) {
                $upn->barcode = 'R' . $upn->barcode;
                $upn->save();
                $response = $this->addStatus($response, false);
                $response = $this->addMessage($response, false);
                $response = $this->addLabel($response, $upn->id);

                return $response;
            }

            if (!$upn && $workOrder && (preg_match('/OT-1189-(47|60|90)/', $workOrder->article) === 1 || preg_match('/OT-1190-(47|60)/', $workOrder->article) === 1)) {
                $upn = $workOrder->upn;
            }

            if (!$upn) {
                $response = $this->addStatus($response, false);
                $response['message'] = 'Это не штрихкод UPN !!! UPN не найден!!!';
                return $response;
            }
        }

        if ($act) {
            $workOrders = ErpNaryad::find()->where(['actNumber' => $act])->all();
            if (empty($workOrders)) {
                $response = $this->addStatus($response, false);
                $response = $this->addMessage($response, false);
                return $response;
            }
            foreach ($workOrders as $workOrder) {
                if (!$this->isIn($workOrder->upn, $upns) || !$this->pack($workOrder->upn)) {
                    $response = $this->addStatus($response, false);
                    $response = $this->addMessage($response, false);
                    return $response;
                }
                $isPackedBox = SBox::pack($boxId);
                $this->_packShipment->pack($shipment);
            }
        } else {
            if (!$upn->storageState  && ErpNaryad::find()->where(['logist_id' => $upn->id])->andWhere(['status' => ErpNaryad::STATUS_READY])->exists()) {
                $response = $this->addStatus($response, false);
                $response['message'] = 'Оприходуйте данный UPN на склад, а потом пропикайте!!!';
            } elseif (!$this->isIn($upn,$upns) && !$upn->packed && (preg_match('/OT-1189-(47|60|90)/', $upn->article) === 1 || preg_match('/OT-1190-(47|60)/', $upn->article) === 1 || preg_match('/OT-2079/', $upn->article) === 1 || preg_match('/OT-2061/', $upn->article) === 1 || preg_match('/OT-1214-47-3/', $upn->article) === 1 || preg_match('/OT-1211-0-0/', $upn->article) === 1)) {
                if (!$upn->storageState) {
                    $response = $this->addStatus($response, false);
                    $response = $this->addMessage($response, false,'Данный upn не числится на складе!!!');
                } else {
                    $changedUpn = null;
                    foreach ($upns as $boxItem) {
                        if ($boxItem->article == $upn->article && $boxItem->packed == false) {
                            $changedUpn = $boxItem;
                            break;
                        }
                    }
                    if (!$changedUpn) {
                        $response = $this->addStatus($response, false);
                        $response = $this->addMessage($response, false,'Данная номенклатура не нужна в текущюю коробку!!!');
                    } else {
                        /**
                         * @var $changedUpn Naryad
                         * @var $upn Naryad
                         */
                        //Здесь происходит подмена одного upn другим:
                        $oldReservedId =  $changedUpn->reserved_id;
                        $oldReservedAt =  $changedUpn->reserved_at;

                        $newReservedId =  $upn->reserved_id;
                        $newReservedAt =  $upn->reserved_at;

                        $changedUpn->reserved_id = null;
                        $changedUpn->reserved_at = null;

                        $changedUpn->save();

                        $upn->reserved_id = $oldReservedId;
                        $upn->reserved_at = $oldReservedAt;

                        $upn->save();

                        $changedUpn->reserved_id = $newReservedId;
                        $changedUpn->reserved_at = $newReservedAt;

                        $changedUpn->save();

                        $response = $this->addStatus($response, true, true);
                        $response = $this->addMessage($response, false,'Вы успешно поменяли UPN. Пикните его еще раз, чтобы пометить как упакованный!');

                    }
                }
            } elseif (!$this->isIn($upn, $upns) || !$this->pack($upn)) {
                $response = $this->addStatus($response, false);
                $response = $this->addMessage($response, false);
            } else {
                if ($upn->isParent) {
                    /**
                     * @var $child Naryad
                     */
                    foreach ($upn->children as $child) {
                        //Если заказ от icargo тогда делаем наклейку на упаковку
                        if (($entry = $child->orderEntry) && ($order = $entry->order) && $order->remoteStorage == 2 && $order->export_username == 'Склад') {
                            //прописать условие при котором добавляеться лейбла для комлпекта
                            $response = $this->addParentLabel($response, $upn->id);
                        };
                        break;
                    }
                }
                $isPackedBox = SBox::pack($boxId);
                $this->_packShipment->pack($shipment);
                $response = $this->addStatus($response, true);
                $response = $this->addMessage($response, true,'Вы упаковали UPN');
                //Распечатываем UPN если это PARENT UPN и это заказы оптовика

            }
        }

        $response = $this->addShipmentStatus($response, $this->_shipmentManager->isPacked($shipment));
        $response = $this->addBoxStatus($response, $isPackedBox);

        return $response;
    }

    public function packShipmentWithoutUpns($shipmentId)
    {
        $shipment = $this->_shipmentManager->findById($shipmentId);

        return $this->_packShipment->pack($shipment);
    }

    private function getUpns($elements)
    {
        $upns = [];
        $parentIds = [];
        foreach ($elements as $element)
        {
            $upn = $this->_upnRepository->getUpnForElement($element);
            if ($upn) {
                if ($upn->pid && !in_array($upn->pid,$parentIds)) {
                    $upns[] = $upn->parent;
                    $parentIds[] = $upn->pid;
                }elseif(!$upn->pid) {
                    $upns[] = $upn;
                }
            }
        }
        return $upns;
    }

    private function getUpnByBarcode($barcode)
    {
        $barcode = $this->_barcodeHelper->toLatin($barcode);
        return $this->_upnRepository->getUpnByBarcode($barcode);
    }

    private function _isStorageBarcode($barcode)
    {
        $barcode = $this->_barcodeHelper->toLatin($barcode);
        $barcode = mb_strtoupper($barcode);
        return CheckStoragePosition::isStorageBarcode($barcode);
    }

    ##########################################################################################
    ###
    ##
    #

    private function isIn($upn,$upns)
    {
        foreach ($upns as $cUpn)
        {
            if ($this->compare($upn,$cUpn)) return true;
        }

        return false;
    }

    private function compare($upn,$cUpn)
    {
        return $this->_upnService->getId($upn) === $this->_upnService->getId($cUpn);
    }

    #
    ##
    ###
    #########################################################################################

    private function pack($upn)
    {
        return $this->_upnService->pack($upn);
    }

    private function addStatus($response,$bool,$warning = false)
    {
        switch ($bool) {
            case true:
                $response['status'] = 'success';
                break;
            case false:
                $response['status'] = 'error';
                break;
            default:
                $response['status'] = 'error';
                break;
        }

        if ($warning)
            $response['status'] = 'warning';

        return $response;
    }

    private function addLabel($response,$id)
    {
        $response['printLabel'] = true;
        $response['upnId']      = $id;

        return $response;
    }

    private function addParentLabel($response,$id)
    {
        $response['printParentLabel'] = true;
        $response['parentId']         = $id;

        return $response;
    }

    private function addMessage($response,$bool, $message = null)
    {
        switch ($bool) {
            case true:
                $response['message'] = 'Все прошло удачно';
                break;
            case false:
                $response['message'] = 'Все прошло НЕудачно';
                break;
            default:
                $response['message'] = 'Все прошло НЕудачно';
                break;
        }
        if ($message)
            $response['message'] = $message;

        return $response;
    }

    private function addOrderStatus($response, $flag)
    {
        switch ($flag) {
            case true:
                $response['orderStatus'] = true;
                break;
            case false:
                $response['orderStatus'] = false;
                break;
            default:
                $response['orderStatus'] = false;
                break;
        }

        return $response;
    }

    private function addRegistryStatus($response, $registry)
    {
        switch ($registry) {
            case true:
                $response['registryStatus'] = true;
                break;
            case false:
                $response['registryStatus'] = false;
                break;
            default:
                $response['registryStatus'] = false;
                break;
        }

        return $response;
    }

    private function addShipmentStatus($response, $flag)
    {
        switch ($flag) {
            case true:
                $response['shipmentStatus'] = true;
                break;
            case false:
                $response['shipmentStatus'] = false;
                break;
            default:
                $response['shipmentStatus'] = false;
                break;
        }

        return $response;
    }
    private function addBoxStatus($response, $flag)
    {
        switch ($flag) {
            case true:
                $response['boxStatus'] = true;
                break;
            case false:
                $response['boxStatus'] = false;
                break;
            default:
                $response['boxStatus'] = false;
                break;
        }

        return $response;
    }
}