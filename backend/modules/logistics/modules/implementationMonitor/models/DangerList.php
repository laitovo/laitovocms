<?php

namespace backend\modules\logistics\modules\implementationMonitor\models;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use yii\data\ArrayDataProvider;
use Yii;

/**
 * Класс отвечает за обработку логистических позиций, которые не могут быть исполнены из за отсутсвия остатков на складе
 *
 * Class DangerList
 * @package backend\modules\logistics\modules\implementationMonitor\models
 */
class DangerList
{
    /**
     * @var object Сервис для работы с отгрузками
     */
    private $_shipmentManager;
    /**
     * @var object Сервис для оприходования продукции на склад
     */
    private $_oprihodovanie;
    /**
     * @var object Сервис для резервирования UPN-ов
     */
    private $_reserveUpn;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_oprihodovanie   = Yii::$container->get('core\logic\Oprihodovanie');
        $this->_reserveUpn      = Yii::$container->get('core\logic\ReserveUpn');
    }

    /**
     * Список логистических позиций, которые не могут быть исполнены из за отсутсвия остатков на складе
     *
     * @return ArrayDataProvider
     */
    public function getProvider()
    {
        $models = $this->_findModels();

        return $this->_asProvider($models);
    }

    /**
     * Количество логистических позиций, которые не могут быть исполнены из за отсутсвия остатков на складе
     *
     * @return int
     */
    public function getCount()
    {
        return count($this->_findModels());
    }

    /**
     * Наличие логистических позиций, которые не могут быть исполнены из за отсутсвия остатков на складе
     *
     * @return bool
     */
    public function exists()
    {
        return count($this->_findModels()) > 0;
    }

    /**
     * Оприходование списка логистических позиций.
     * Для каждой позиции будет выполнено следующее:
     *   1). Оприходование артикула (создание UPN и StorageState)
     *   2). Отвязка старого UPN-а от логистической позиции
     *   3). Резервирование нового UPN-а, который создался при оприходовании
     *
     * @param int[] $orderEntryIds
     * @return array
     */
    public function oprihod($orderEntryIds)
    {
        $orderEntries = OrderEntry::find()->where(['in', 'id', $orderEntryIds])->all();

        $transaction = Yii::$app->db->beginTransaction();

        foreach ($orderEntries as $orderEntry) {
            $result = $this->_oprihodovanie->oprihodArticle($orderEntry->article);
            if (!$result['status']) {
                $transaction->rollBack();
                return [
                    'status'  => false,
                    'message' => Yii::t('app', 'Возникла ошибка при оприходовании (orderEntryId = ' . $orderEntry->id . '). ')
                        . $result['message'],
                ];
            }

            $result = $this->_reserveUpn->dereserve($orderEntry);
            if (!$result['status']) {
                $transaction->rollBack();
                return [
                    'status'  => false,
                    'message' => Yii::t('app', 'Не удалось отвязать UPN (orderEntryId = ' . $orderEntry->id . '). ')
                        . $result['message'],
                ];
            }

            $result = $this->_reserveUpn->reserve($orderEntry);
            if (!$result['status']) {
                $transaction->rollBack();
                return [
                    'status'  => false,
                    'message' => Yii::t('app', 'Возникла ошибка при резервировании UPN (orderEntryId = ' . $orderEntry->id . '). ')
                        . $result['message'],
                ];
            }
        }

        $transaction->commit();

        return [
            'status'  => true,
            'message' => Yii::t('app', 'Успешно выполнено'),
        ];
    }

    private function _findModels()
    {
        $dangerEntries = [];

        $shipments = $this->_shipmentManager->findShipmentsBeforeShipping();
        foreach ($shipments as $shipment) {
            if (!$this->_shipmentManager->isHandled($shipment) && !$this->_shipmentManager->isOnStorageRequest($shipment) && !$this->_shipmentManager->isPacked($shipment)) {
                continue;
            }
            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
            foreach ($orderEntries as $orderEntry) {
                $upn = $orderEntry->upn;
                if ($upn && !$upn->storageState && !$upn->erpNaryad) {
                    $dangerEntries[] = $orderEntry;
                }
            }
        }

        $orderEntriesWithoutShipment = OrderEntry::find()->joinWith('order as order')->where(['and',
            ['shipment_id' => null],
            ['order.processed' => 1]
        ])->all();

        foreach ($orderEntriesWithoutShipment as $orderEntry) {
            $upn = $orderEntry->upn;
            if ($upn && !$upn->storageState && !$upn->erpNaryad) {
                $dangerEntries[] = $orderEntry;
            }
        }

        return $dangerEntries;
    }

    private function _asProvider($data, $sort = [])
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $data,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => false
        ]);

        return $provider;
    }
}


