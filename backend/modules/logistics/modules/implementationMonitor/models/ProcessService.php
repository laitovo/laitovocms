<?php
namespace backend\modules\logistics\modules\implementationMonitor\models;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderElementsRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrderService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IOrdersRepository;
use backend\modules\logistics\modules\implementationMonitor\interfaces\ITransaction;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IUpnService;
use common\models\laitovo\ErpNaryad;
use core\logic\Equipment;
use core\logic\UpnGroup;
use core\services\SAutoCompletionLogs;
use core\services\SRemoteStorage;
use Yii;
use yii\helpers\ArrayHelper;

class ProcessService
{
    private $_ordersRepository;
    private $_orderElementsReository;
    private $_orderService;
    private $_transaction;
    private $_upnService;
    private $_orderElementService;
    private $_orderElementsRepository;
    private $_shipmentManager;
    private $_realizationLogManager;
    private $_reserveUpn;

    public function __construct(
        IOrdersRepository $ordersRepository,
        IOrderElementsRepository $orderElementsRepository,
        IOrderService $orderService,
        ITransaction $transaction,
        IUpnService $upnService,
        IOrderElementService $orderElementService
    )
    {
        $this->_ordersRepository       = $ordersRepository;
        $this->_orderElementsReository = $orderElementsRepository;
        $this->_orderService           = $orderService;
        $this->_transaction            = $transaction;
        $this->_upnService             = $upnService;
        $this->_orderElementService    = $orderElementService;
        $this->_shipmentManager        = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_realizationLogManager  = Yii::$container->get('core\entities\logisticsRealizationLog\RealizationLogManager');
        $this->_reserveUpn             = Yii::$container->get('core\logic\ReserveUpn');
    }

    public function handleOrder()
    {
        //Найти заявку.
        $order = $this->_ordersRepository->getLastOrder();

        if (!$order) {
            return true;
        }

        //Найти все элементы.
        $elements = $this->_orderElementsReository->getElementsForOrderNotDeleted($order);

        //В транзакции делаем попытку создать или зарезервировать элементы заказа и заказ
        $this->_transaction->begin();

        try {
            foreach ($elements as $element)
            {
                $this->_reserveUpn->reserve($element);
            }

            $this->_orderService->checkAsProcessed($order);

            $this->_transaction->commit();

            return true;
        } catch (\Exception $e) {
            $this->_transaction->rollBack();
            return false;
        } catch (\Throwable $e) {
            $this->_transaction->rollBack();
            return false;
        }
    }

    public function handleOrderById($id)
    {
        //Найти заявку.
        $order = $this->_ordersRepository->getOrderById($id);

        if (!$order) {
            return true;
        }

        //Найти все элементы.
        $elements = $this->_orderElementsReository->getElementsForOrderNotDeleted($order);

        //В транзакции делаем попытку создать или зарезервировать элементы заказа и заказ
        $this->_transaction->begin();

        try {
            foreach ($elements as $element)
            {
                $this->_reserveUpn->reserve($element);
            }

            $this->_orderService->checkAsProcessed($order);

            $this->_transaction->commit();

            return true;
        } catch (\Exception $e) {
            $this->_transaction->rollBack();
            return false;
        } catch (\Throwable $e) {
            $this->_transaction->rollBack();
            return false;
        }
    }

    public function handleOrderBySourceInnumber($sourceInnumber)
    {
        //Найти заявку.
        /**
         * @var $order Order
         */
        $order = Order::find()->where(['source_innumber' => $sourceInnumber])->one();

        if (!$order) {
            return true;
        }

        //Найти все элементы.
        $elements = $this->_orderElementsReository->getElementsForOrderNotDeleted($order);

        //В транзакции делаем попытку создать или зарезервировать элементы заказа и заказ
        $this->_transaction->begin();

        try {
            foreach ($elements as $element)
            {
                $this->_reserveUpn->reserve($element);
            }

            /**
             * [ Записываем в логи автопополнения позиции заказа ]
             */
            SAutoCompletionLogs::logIndividual($order);

            $this->_orderService->checkAsProcessed($order);

            //Добавлена работа с комлпектацией.
            $equipment = Equipment::groupOrderBySetIndex($order);
            foreach ($equipment as $eq) {
                UpnGroup::groupOrderEntries($eq);
            }
            //Добавлена работа с комлпектацией.

            $this->_transaction->commit();
        } catch (\Exception $e) {
            $this->_transaction->rollBack();
            return false;
        } catch (\Throwable $e) {
            $this->_transaction->rollBack();
            return false;
        }

        /**
         * Проверка на позиции со склада
         * Логика ускорения поизций на производстве, если есть позиции со склада - автошторки
         */
        $this->_transaction->begin();

        try {
            if ($order->category !== 'Физик')
                return true;
            $elements = $this->_orderElementsReository->getElementsForOrderNotDeleted($order);
            $needToSpeed = false;
            foreach ($elements as $element)
            {
                $trueArticle = in_array(mb_strtoupper(mb_substr($element->article,0,2)),['FV','FD','RD','RV','BW']);
                if ($trueArticle && $element->upn && $element->upn->storageState) {
                    $needToSpeed = true;
                    break;
                }
            }
            if ($needToSpeed) {
                $workOrders = ErpNaryad::find()->where(['order_id' => $sourceInnumber])->all();
                foreach ($workOrders as $workOrder) {
                    if ($workOrder->sort > 4) { $workOrder->sort = 4; $workOrder->save(); };
                }
            }

            $this->_transaction->commit();
            return true;
        }catch (\Exception $exception) {
            $this->_transaction->rollBack();
            echo "Ошибка при ускорении" . PHP_EOL;
            return true;
        }
    }

    public function shipReestr($barcode)
    {
        //Находим все заявки с определенным номером реестра
        $orders = $this->_ordersRepository->getOrdersByReestrId($barcode);
        if (!$orders) return false;
        //Для каждой заявки ищем элементы
        foreach ($orders as $order) {
            $elements = $this->_orderElementsReository->getElementsForOrderNotDeleted($order);
            foreach ($elements as $element) {
                $upn = $this->_orderElementService->getReservedUpn($element);
                $literal = @$upn->storageState->literal;
                $this->_upnService->shipUpn($upn);
                if ($literal) {
                    $this->_log($upn->id, $element->id, $literal);
                }
            }
            $this->_orderService->shipOrder($order);
        }
        return true;
    }

    public function shipShipment($barcode)
    {
        $shipment = $this->_shipmentManager->findByBarcode($barcode);
        if (!$shipment) {
            return false;
        }
        $elements = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        /**
         * @var $needTTN bool Указывает, нужно ли выписывать оф. накладную
         */
        $needTTN = false;
        /**
         * @var $checkTTN bool Указывает, проходила ли поверка на выписку оф. накладной
         */
        $checkTTN = false;

        $this->_transaction->begin();

        try {
            $parentIds = [];
            $remoteData = [];
            $remote = null;

            /**
             * Кусок кода для записи логов автопополнения на склад оптовиков.
             */
            $orderIds = ArrayHelper::map($elements,'order_id','order_id');
            $orders = Order::find()->where(['in','id',$orderIds])->all();
            foreach ($orders as $rowOrder) {
                /**
                 * @var $rowOrder Order
                 */
                SAutoCompletionLogs::logWholesaler($rowOrder);
            }


            foreach ($elements as $element) {
                if (!$checkTTN && ($order = $element->order) && $order->is_need_ttn) {
                    $checkTTN = true;
                    $needTTN = true;
                }
                if ($remote === null)
                    $remote = ($order = $element->order) && ($remoteStorage = $order->remoteStorage) ? $remoteStorage : 0;

                $upn = $this->_orderElementService->getReservedUpn($element);
                $literal = @$upn->storageState->literal;
                $this->_upnService->shipUpn($upn);
                $remoteItem = [
                    'article' => $element->article,
                    'upn' => $upn->id,
                    'upn_barcode' => $upn->barcode,
                    'order_number' => $element->order->source_innumber,
                    'literal_barcode' => $element->box->remoteStorageBarcode,
                    'parent_upn' => null,
                ];
                if (($parent = $upn->parent)) {
                    $remoteItem['parent_upn'] = $parent->id;
                    if (!in_array($parent->id,$parentIds)) {
                        $remoteData[] = [
                            'article' => null,
                            'upn' => $parent->id,
                            'upn_barcode' => $parent->barcode,
                            'order_number' => $element->order->source_innumber,
                            'literal_barcode' => $element->box->remoteStorageBarcode,
                            'parent_upn' => null,
                        ];
                        $parentIds[] = $parent->id;
                    }
                }
                if ($literal) {
                    $this->_log($upn->id, $element->id, $literal);
                }
                $remoteData[] = $remoteItem;
            }

            if ($remote) {
                $remoteService = new SRemoteStorage($remote);
                if (!$remoteService->postLiteral($remoteData)) {
                    $this->_transaction->rollBack();
                    return false;
                }
            }

            if (!$this->_shipmentManager->ship($shipment,$needTTN)) {
                $this->_transaction->rollBack();
                return false;
            };

            $this->_transaction->commit();
            return true;
        } catch (\Exception $e) {
            $this->_transaction->rollBack();
            return false;
        } catch (\Throwable $e) {
            $this->_transaction->rollBack();
            return false;
        }
    }

    private function _log($upnId, $orderEntryId, $literal)
    {
        $realizationLog = $this->_realizationLogManager->create();
        $this->_realizationLogManager->setUpnId($realizationLog, $upnId);
        $this->_realizationLogManager->setOrderEntryId($realizationLog, $orderEntryId);
        $this->_realizationLogManager->setLiteral($realizationLog, $literal);
        $this->_realizationLogManager->save($realizationLog);
    }

    public function checkShipmentBarcode($shipmentId, $barcode)
    {
        $shipment = $this->_shipmentManager->findByBarcode($barcode);
        if (!$shipment) {
            return false;
        }

        return $shipmentId == $this->_shipmentManager->getId($shipment);
    }
}