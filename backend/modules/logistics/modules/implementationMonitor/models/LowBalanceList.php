<?php

namespace backend\modules\logistics\modules\implementationMonitor\models;

use yii\data\ArrayDataProvider;
use Yii;

class LowBalanceList
{
    private $_getStorageBalance;

    public function __construct()
    {
        $this->_getStorageBalance = Yii::$container->get('core\logic\GetStorageBalance');
    }

    public function getProvider()
    {
        $models = $this->_findModels();

        return $this->_asProvider($models);
    }

    public function exists()
    {
        return count($this->_findModels()) > 0;
    }

    public function getAttributes()
    {
        return [
            'title',
            'article',
            'minBalance',
            'factBalance',
        ];
    }

    private function _findModels()
    {
        return $this->_getStorageBalance->getLowBalanceList();
    }

    private function _asProvider($data, $sort = [])
    {
        $provider = new ArrayDataProvider([
            'allModels'  => $data,
            'sort'       => [
                'attributes' => $sort,
            ],
            'pagination' => false
        ]);

        return $provider;
    }
}


