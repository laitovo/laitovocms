<?php
/**
 * @var  $provider \yii\data\ArrayDataProvider
 */

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->render('@backendViews/logistics/views/menu');

?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover', ],
    'dataProvider' => $provider,
    'columns' => [
        [
            'label'=>'Отметить',
            'format'=>'raw',
            'value' => function($data) {
                return Html::tag('div',
                    Html::checkbox("orders[]",
                        $checked = false,
                        ['value' => $data['order']])
                    .Html::tag('label',''),
                    ['class' => 'checkbox-custom checkbox-primary text-left']
                );
            }

        ],
        [
            'attribute' => 'order',
            'label'=>'Заявка',
        ],
    ],
]); ?>

