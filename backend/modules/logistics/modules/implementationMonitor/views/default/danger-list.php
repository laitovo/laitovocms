<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $provider \yii\data\ArrayDataProvider */
?>

<?php Yii::$app->view->registerCss('
    .checkbox-custom label::before { 
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
'); ?>

<div class="danger-list">

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'summary' => false,
        'dataProvider' => $provider,
        'columns' => [
            [
                'attribute' => 'order.source_innumber',
                'label'     => Yii::t('app', 'Заказ №'),
            ],
            [
                'attribute' => 'article',
                'label'     => Yii::t('app', 'Артикул'),
            ],
            [
                'attribute' => 'name',
                'label'     => Yii::t('app', 'Наименование'),
            ],
            [
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::tag('div',
                        Html::checkbox("orderEntryIds[]", false, ['class' => 'checkbox-danger-entry', 'value' => $data->id])
                        .Html::tag('label','', ['for' => '']),
                        [
                            'class' => 'checkbox-custom checkbox-primary text-left',
                            'onchange' => '
                                var container = $(this).closest(".danger-list");
                                var enableButtons = container.find(".checkbox-danger-entry:checked").length > 0;
                                container.find(".buttons button").prop("disabled", !enableButtons);
                            '
                        ]
                    );
                },
            ],
        ]
    ]) ?>

    <div class="text-center buttons">

        <?= Html::button(Yii::t('app', 'Оприходовать'), [
            'class' => 'btn btn-sm btn-success',
            'disabled' => true,
            'onclick' => '
                $(this).prop("disabled", true).attr("title", "' . Yii::t('app', 'Подождите...') . '").html(\'<i class="fa fa-spinner fa-spin" style="font-size:20px;"></i>\');
                var orderEntryIds = [];
                $(".checkbox-danger-entry:checked").each(function(){
                    orderEntryIds.push($(this).val());
                });
                $.ajax({
                    url: "' . Url::to(['ajax-oprihod-danger-list']) . '",
                    method: "post",
                    dataType: "json",
                    data: {
                        "orderEntryIds": orderEntryIds,
                    },
                    success: function (data) {
                        updateDangerList();
                        if (!data.status) {
                            notie.alert(3, data.message ? data.message : "' . Yii::t('app', 'Не удалось выполнить команду') . '", 3);
                            return;
                        }
                        notie.alert(1, data.message ? data.message : "' . Yii::t('app', 'Успешно выполнено') . '", 1);
                        if (!data.dangerListCount) {
                            $("#modal-danger").modal("hide");
                        }
                    },
                    error: function() {
                        updateDangerList();
                        notie.alert(3, "' . Yii::t('app', 'Неизвестная ошибка. Обратитесь к администратору.') . '", 3);
                    }
                });
            '
        ]) ?>

        <?= Html::button(Yii::t('app', 'Распечатать'), [
            'class' => 'btn btn-sm btn-info',
            'disabled' => true,
            'onclick' => '                
                var params = $(this).closest(".danger-list").find(".checkbox-danger-entry:checked").serialize();
                var url = "' . Url::to(['print-danger-list']) . '?" + params;
                printUrl(url);
            '
        ]) ?>

    </div>

</div>
