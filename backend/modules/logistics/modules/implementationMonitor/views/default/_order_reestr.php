<?php
/**
 * @var  $provider \yii\data\ArrayDataProvider
 */

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->render('@backendViews/logistics/views/menu');

?>

<?=
ListView::widget([
    'dataProvider' => $provider,
    'itemView' => '_order_reestr_detail',
])
?>

