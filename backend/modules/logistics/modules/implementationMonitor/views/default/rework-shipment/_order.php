<?php
use yii\helpers\Html;

$this->registerCss(".table > thead > tr > th{ font-weight: 600 }");
?>
<?php foreach ($model['orders'] as $order): ?>
    <div style="font-weight:bold;">
        № Заказа
        <div><?=$order['sourceId']?></div>
    </div>
    <?php if ($order['innerComment']):?>
        <div class="alert" style="background:white;color:#001F3F;font-size: 1.2em;border-radius: 10px; border: 1px solid #001F3F;opacity: 0.7;">
            <strong style="font-weight:bold;">Комментарий:</strong> <?=$order['innerComment']?>
        </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead class="some-bold">
        <tr>
            <th>Артикул</th>
            <th>Наименование</th>
            <th>Upn</th>
            <th>МестоНахождение</th>
            <th>Выбрать</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($order['elements'] as $key => $element): ?>
                <tr class="<?= @$element['pack'] || @$element['storage'] ? 'success' : ''?>">
                    <td>
                        <?= @$element['article'] ?>
                        <?php
                        if ($element['storage']) {
                            $attention = $element['hasBalanceControl'] && $element['minBalance'] > $element['factBalance'];
                            echo Html::tag('span', '(' . $element['factBalance'] . ')', [
                                'class' => !$attention ? '' : 'text-danger',
                                'style' => !$attention ? '' : 'font-size:1.3em;font-weight:bold;',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title'  => Yii::t('app', !$attention ? 'Фактический остаток' : 'Фактический остаток (Внимание! Меньше минимального!)')
                            ]);
                        }
                        ?>
                    </td>
                    <td "><?= @$element['title'] ?></td>
                    <?if (!@$element['storage']) :?>
                    <td ">
                        <?= @$element['upn'] ? str_replace((mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn']))),
                        ('<b style="font-size: 1.4em">' . mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn'])) . '</b>'), @$element['upn']) : @$element['upn'] ?>
                    </td>
                    <td "><?= @$element['location'] ?></td>
                    <?else :?>
                        <td rowspan="2">Комплектуется со склада</td>
                    <?endif;?>
                    <td>
                        <?= Html::tag('div',
                            Html::checkbox("", false, [
                                'class' => 'checkbox-select-rework-position',
                                'value' => $element['id'],
                                'onchange' => '
                                    if ($(".checkbox-select-rework-position:checked").length > 0) {
                                        $("#rework-button").prop("disabled", false);
                                    } else {
                                        $("#rework-button").prop("disabled", true);
                                    }
                                '
                            ])
                            .Html::tag('label',''),
                            [
                                'class' => 'checkbox-custom checkbox-primary text-left',
                            ]
                        ) ?>
                    </td>
                </tr>
        <?php endforeach;?>
        </tbody>
    </table>
<?php endforeach;?>
