<?php
/* @var $provider \yii\data\ArrayDataProvider */

use yii\widgets\ListView;
use yii\helpers\Html;

?>

<?php Yii::$app->view->registerCss('
    .checkbox-custom label::before { 
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
'); ?>

<?=
ListView::widget([
    'dataProvider' => $provider,
    'itemView' => '_order',
    'summary' => false
])
?>

<?= Html::button('Отправить на доработку', [
    'id' => 'rework-button',
    'class' => 'btn btn-primary',
    'data-shipment-id' => $shipmentId,
    'disabled' => true,
    'onclick' => '
        $(this).prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:20px;\"></i>");
        var button = $(this);
        var shipmentId = $(this).data("shipment-id");
        var orderEntryIds = $(".checkbox-select-rework-position:checked").map(function() {return this.value;}).get();
        $.ajax({
            url: "/logistics/implementation-monitor/default/ajax-rework-shipment?shipmentId=" + shipmentId,
            method: "post",
            dataType: "json",
            data: {
                "orderEntryIds[]": orderEntryIds
            },
            success: function (data) {
                if (!data.status) {
                    notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                    $("#modal-rework-shipment").modal("hide");
                    return;
                }
                notie.alert(1, data.message ? data.message : "Успешно выполнено", 3);
                $("#modal-rework-shipment").modal("hide");
            },
            error: function() {
                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                $("#modal-rework-shipment").modal("hide");
            }
        });
    '
]);?>
