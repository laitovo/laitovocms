<?php
/* @var $models backend\modules\logistics\models\OrderEntry[] */
?>

<table style="border-collapse: collapse;font-size: 1em" cellpadding="2">
    <thead>
    <th>Заказ №</th>
    <th>Артикул</th>
    <th>Наименование</th>
    </thead>
    <tbody>
    <?foreach ($models as $model):?>
        <tr>
            <td style="font-size: 0.8em;border:1px solid;white-space:nowrap;;text-align: center"><?=$model->order->source_innumber ?? null?></td>
            <td style="font-size: 0.8em;border:1px solid;white-space:nowrap;;text-align: center"><?=$model->article?></td>
            <td style="font-size: 0.8em;border:1px solid"><?=$model->name?></td>
        </tr>
    <?endforeach;?>
    </tbody>
</table>