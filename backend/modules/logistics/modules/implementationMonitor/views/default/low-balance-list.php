<?php
/* @var $provider \yii\data\ArrayDataProvider */

use yii\grid\GridView;

?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'summary' => false,
    'dataProvider' => $provider,
    'columns' => [
        [
            'attribute' => 'title',
            'label'     => 'Наименование',
        ],
        [
            'attribute' => 'article',
            'label'     => 'Артикул',
        ],
        [
            'attribute' => 'minBalance',
            'label'     => 'Минимальный остаток',
        ],
        [
            'attribute' => 'factBalance',
            'label'     => 'Фактический остаток',
        ],
    ]
]) ?>

