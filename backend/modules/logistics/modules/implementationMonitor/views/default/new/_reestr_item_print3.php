<?php
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
?>
<div class="col-md-1 text-center">
    <table class="table">
        <thead>
            <th style="text-align: left">№ Коробки</th>
            <th style="text-align: left">№ Отгрузки</th>
        </thead>
        <tbody>
        <tr>
            <td><div style="font-size: 2em;font-weight: bold"><?= $model['boxId']?></div></td>
            <td><div style="font-size: 2em;font-weight: bold"><?= $model['shipmentId']?></div></td>
        </tr>
        <?php if ($model['shipmentComment']):?>
            <tr>
                <td colspan="2"><b>Комментарий к отгрузке:</b><br>
                    <?= $model['shipmentComment']?></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<?foreach ($model['orders'] as $order):?>
    <div style="font-weight:bold;">
        № Заказа
        <div style="font-size:1.6em;"><?=$order['sourceId']?> <?=$order['client']?></div>
    </div>
    <?php if ($order['comment']):?>
        <span style="font-weight:bold;">Комментарий к заказу: </span><?=$order['comment']?>
    <?php endif; ?>
    <table style="border-collapse: collapse;font-size: 1em" cellpadding="4">
        <thead>
        <th>Артикул</th>
        <th>Наименование</th>
        <th>UPN</th>
        <th>Место</th>
        </thead>
        <tbody>
        <?foreach ($order['elements'] as $parentIndex => $set):?>
            <?if ($parentIndex):?>
                <tr>
                    <td colspan="4"><b style="font-size: 1.5em">Комплект № <?=$parentIndex?></b></td>
                </tr>
            <?endif;?>
            <?foreach ($set as $element):?>

            <tr>
                <td style="font-size: 0.8em;border:1px solid;white-space:nowrap;"><?=$element['article']?></td>
                <td style="font-size: 0.8em;border:1px solid"><?=$element['title'] . ' <span style="font-weight: bold"> ' . $element['car'] . '</span>'?></td>
                <td style="font-size: 0.8em;border:1px solid"><?=$element['upn']?></td>
                <td style="font-size: 0.8em;border:1px solid"><?=$element['location']?></td>
            </tr>
            <?endforeach;?>
            <?if ($parentIndex):?>
                <tr>
                    <td colspan="4" style="padding: 0px;" class="bg-info">&nbsp</td>
                </tr>
            <?endif;?>
        <?endforeach;?>
        </tbody>
    </table>
<?endforeach;?>
