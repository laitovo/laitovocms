<?php
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
?>
<? $count = 0;?>
<table class="table" style="font-size: 2em;width: 100%">
    <thead>
    <th class="text-center" width="50%">№ Отгрузки</th>
    <th class="text-center" width="50%">№ Коробки</th>
    </thead>
    <tbody>
    <tr>
        <td width="50%"><div style="font-size: 1.5em;font-weight: bold;text-align: center"><?= $model['shipmentId']?></div></td>
        <td width="50%"><div style="font-size: 1.5em;font-weight: bold;text-align: center"><?= $model['boxId']?></div></td>
    </tr>
    </tbody>
</table>
<?if ($model['remoteLiteral']):?>
<table class="table" style="font-size: 2em;width: 100%">
    <thead>
    <th class="text-center" width="50%">Литера</th>
    <th class="text-center" width="50%">Штрихкод</th>
    </thead>
    <tbody>
    <tr>
        <td width="50%"><div style="font-size: 1.5em;font-weight: bold;text-align: center"><?= $model['remoteLiteral']?></div></td>
        <td width="50%">
            <div style="text-align: center;padding-left: 80px">
                <?= $generator->getBarcode($model['remoteBarcode'], $generator::TYPE_CODE_128, 2) ?>
            </div>
        </td>
    </tr>
    </tbody>
</table>
<? endif;?>
<table style="border-collapse: collapse;font-size: 1em" cellpadding="2">
    <thead>
    <th>№</th>
    <th>Title</th>
    <th>Quantity</th>
    </thead>
    <tbody>
    <?foreach ($model['result'] as $element):?>
        <? $count++;?>
        <tr>
            <td style="font-size: 0.8em;border:1px solid;text-align: center"><div style="padding-left:5px;padding-right: 20px;"><?= $count ?></div></td>
            <td style="font-size: 0.8em;border:1px solid"><?=$element['title']?></td>
            <td style="font-size: 0.8em;border:1px solid;text-align: center"><?= $element['quantity'] ?></td>
        </tr>
    <?endforeach;?>
    </tbody>
</table>