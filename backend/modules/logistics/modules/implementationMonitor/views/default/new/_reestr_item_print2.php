<?php

/**
 * @var $shipment \core\entities\logisticsShipment\models\Shipment Отгрузка
 */
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>
<div class="col-md-1 text-center">
    <table class="table">
        <thead>
            <th colspan="2" style="text-align:left">№ Отрузки</th>
        </thead>
        <tbody>
        <tr>
            <td width="20%"><div style="font-size: 2em;font-weight: bold"><?= $model['shipmentId']?></div></td>
            <td><div style="font-size: 2em;font-weight: bold"><?= $generator->getBarcode('SHIP'.$model['shipmentId'], $generator::TYPE_CODE_128, 1) ?></div></td>
        </tr>
        <?php //if ($model['comment']):?>
        <!--    <tr>-->
        <!--        <td colspan="2"><b>Комментарий к отгрузке:</b><br>-->
        <!--            --><?//= $model['comment']?><!--</td>-->
        <!--    </tr>-->
        <?php //endif; ?>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<?foreach ($model['boxes'] as $box) :?>
<div style="border: 1px solid black;padding: 5px;">
    <div style="font-weight:bold;">
        № Коробки
        <div style="font-size:1.6em;"><?=$box['id']?></div>
    </div>
    <?foreach ($box['orders'] as $order):?>
        <div style="font-weight:bold;">
            № Заказа
            <div style="font-size:1.6em;"><?=$order['sourceId']?> <?=$order['client']?></div>
        </div>
        <?php //if ($order['comment']):?>
        <!--    <span style="font-weight:bold;">Комментарий к заказу: </span>--><?//=$order['comment']?>
        <?php //endif; ?>
        <table style="border-collapse: collapse;font-size: 1em" cellpadding="4">
            <thead>
            <th>Артикул</th>
            <th>Наименование</th>
            <th>UPN</th>
            <th>Место</th>
            </thead>
            <tbody>
            <?foreach ($order['elements'] as $parentIndex => $set):?>
                <?if ($parentIndex):?>
                    <tr>
                        <td colspan="4"><b style="font-size: 1.5em">Комплект № <?=$parentIndex?></b></td>
                    </tr>
                <?endif;?>
                <?foreach ($set as $element):?>
                    <tr>
                        <td style="font-size: 0.8em;border:1px solid;white-space:nowrap;"><?=$element['article']?></td>
                        <td style="font-size: 0.8em;border:1px solid"><?= $element['title'] . ' <span style="font-weight: bold"> ' . ($element['article'] == 'OT-678-0-0' ? "({$element['itemtext']})" : $element['car']) . '</span>'?></td>
                        <td style="font-size: 0.8em;border:1px solid"><?=$element['upn']?></td>
                        <td style="font-size: 0.8em;border:1px solid"><?=$element['location']?></td>
                    </tr>
                <?endforeach;?>
                <?if ($parentIndex):?>
                    <tr>
                        <td colspan="4" style="padding: 0px;" class="bg-info">&nbsp</td>
                    </tr>
                <?endif;?>
            <?endforeach;?>
            </tbody>
        </table>
    <?endforeach;?>
</div>
<?endforeach;?>
