<?php
/* @var $provider \yii\data\ArrayDataProvider */
/* @var $shipcount int */
/* @var $search string */
/* @var $autoRefresh bool */
/* @var $hasPositionsWithLowBalance bool */
/* @var $hasDangerousOrders bool */

use yii\widgets\ListView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//Вынести в ассеты модуля
$script = <<<SCRIPT
    setInterval(function() {
        if ($("#checkbox-auto-refresh").prop("checked")) {
            $.pjax.reload({container : "#myPjax", timeout: false});
        }
    }, 360*1000);
    
    $("#myPjax").on("pjax:end", function() {
      $("#myPjax [data-toggle=tooltip]").tooltip();
    })

    $('#myModal').on('shown.bs.modal', function () {
      $('#barcode-scaner').focus();
    });
    
    $('#myModal').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#myPjax', timeout: false});
    });
    
    $('#myModal3').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#myPjax', timeout: false});
    });
    
    $('#myModal3').on('shown.bs.modal', function () {
      $('#barcode-scaner-2').focus();
    });
    
    $('#modal-ship-one').on('shown.bs.modal', function () {
      $('#modal-ship-one .barcode-scaner').focus();
    });
    
    $('#modal-ship-one').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#myPjax', timeout: false});
    });
    
    $('#modal-update-weight').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#myPjax', timeout: false});
    });
    
    $('#modal-update-size').on('hidden.bs.modal', function () {
      $.pjax.reload({container : '#myPjax', timeout: false});
    });
    
    $("#modal-rework-shipment").on("pjax:end", function() {
      $.pjax.reload({container : '#myPjax', timeout: false});
    })
    
    $("#modal-danger").on('hidden.bs.modal', function() {
      $.pjax.reload({container : '#myPjax', timeout: false});
    })
SCRIPT;
$this->registerJs($script);

$this->render('@backendViews/logistics/views/menu');
$this->title = Yii::t('app', 'Заказы к отгрузке');
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    .checkbox-custom label::before {
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
</style>

<?php Pjax::begin(['id' => 'myPjax','timeout' => 5000, 'enablePushState' => false]); ?>

    <?php if ($hasDangerousOrders): ?>
        <div class="row">
            <div class="col-md-9 col-sm-8">

                <div style="display:inline-block;margin-bottom:20px;padding:15px;border-radius:10px;background:#270101;">

                    <?= Html::tag('span', Yii::t('app', 'Внимание!<br>Заказы не могут быть исполнены!<br>Отсутствуют остатки на складе!'), [
                        'class' => 'text-danger',
                        'style' => 'font-size:2em;font-weight:bold;',
                    ]) ?>

                    <br>

                    <?=Html::a(Yii::t('app', 'Посмотреть'), Url::to(['danger-list']), [
                        'id' => 'link-show-danger-list',
                        'class' => 'btn btn-outline btn-round btn-md btn-danger',
                        'style' => 'margin-top:5px;',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-danger',
                        'onclick' => 'updateDangerList();'
                    ])?>

                </div>

            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-2 col-sm-4 col-xs-6">
            <?= Html::button(Yii::t('app',  'Провести реализацию'), [
                'class' => 'btn btn-icon btn-round ' . ($shipcount ? 'btn-info' : 'btn-default btn-inverse'),
                'onclick' => '
                    $("#myModal3").modal("show");
                ',
            ]) ?>
        </div>
    </div>
    <hr>

    <?php ActiveForm::begin([
        'id'      => 'search-form',
        'action'  => [''],
        'method'  => 'get',
        'options' => [
            'data-pjax' => true
        ]
    ]); ?>

    <div class="row">
        <div class="col-md-12">
            <?= Html::button(
                Html::tag('span', $shipcount, [
                    'class' => 'badge',
                    'style' => 'font-size:1em;' . ($shipcount ? 'background:#3aa99e;' : 'background:#fff;color:#76838f;'),
                ]) . Yii::t('app', ' К отгрузке'),
                [
                    'class'               => 'btn btn-default button-filter ' . ($filter == 'ship' ? 'active' : ''),
                    'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                    'data-toggle'         => 'tooltip',
                    'data-original-title' => Yii::t('app', 'Должно быть отгружено сегодня'),
                    'data-filter'         => 'ship',
                ]
            ); ?>
            <?= Html::button(
                Html::tag('span', $carriedCount, [
                    'class' => 'badge',
                    'style' => 'font-size:1em;' . ($carriedCount ? 'background:#f2a654;' : 'background:#fff;color:#76838f;'),
                ]) . Yii::t('app', ' Не отгружено'),
                [
                    'class'               => 'btn btn-default button-filter ' . ($filter == 'carried' ? 'active' : ''),
                    'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                    'data-toggle'         => 'tooltip',
                    'data-original-title' => Yii::t('app', 'Было запланировано к отгрузке, но не было отгружено в рамках планируемой даты'),
                    'data-filter'         => 'carried',
                ]
            ); ?>
            <?= Html::button(
                Html::tag('span', $packedForShip, [
                    'class' => 'badge',
                    'style' => 'font-size:1em;' . ($packedForShip ? 'background:#46be8a;' : 'background:#fff;color:#76838f;'),
                ]) . Yii::t('app', ' Упаковано'),
                [
                    'class'               => 'btn btn-default button-filter ' . ($filter == 'packedForShip' ? 'active' : ''),
                    'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                    'data-toggle'         => 'tooltip',
                    'data-original-title' => Yii::t('app', 'Упаковано отгрузок из тех, что должны уехать сегодня'),
                    'data-filter'         => 'packedForShip',
                ]
            ); ?>
            <?= Html::button(
                Html::tag('span', $nextShipcount, [
                    'class' => 'badge',
                    'style' => 'font-size:1em;' . ($nextShipcount ? 'background:#77d6e1;' : 'background:#fff;color:#76838f;'),
                ]) . Yii::t('app', ' Отгрузки на следующие дни'),
                [
                    'class'               => 'btn btn-default button-filter ' . ($filter == 'nextShip' ? 'active' : ''),
                    'style'               => 'font-size:1.5em;line-height:1em;margin-right:10px;',
                    'data-toggle'         => 'tooltip',
                    'data-original-title' => Yii::t('app', 'Должно быть отгружено в следующие дни'),
                    'data-filter'         => 'nextShip',
                ]
            ); ?>
            <?= Html::hiddenInput('filter', null, ['id' => 'hidden-filter']) ?>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="form-group col-md-2">
            <?= Html::tag('div',
                Html::checkbox("implementationMonitor_default_autoRefresh", $autoRefresh, ['id' => 'checkbox-auto-refresh'])
                .Html::tag('label','Автообновление', ['for' => 'checkbox-auto-refresh']),
                [
                    'class' => 'checkbox-custom checkbox-primary text-left',
                    'onchange' => '$("#search-form").submit();'
                ]
            ) ?>
            <?= Html::hiddenInput('update_implementationMonitor_default_autoRefresh',true); ?>
            <?= Html::button(Yii::t('app', 'Обновить'), [
                'id' => 'button-refresh',
                'class' => 'btn btn-outline btn-round btn-primary',
                'onclick' => '
                    $.pjax.reload({container : "#myPjax", timeout: false});
                '
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::input($type = 'text','implementationMonitor_default_search',$value = $search, $options = ['class' => 'form-control', 'id' => 'search-textbox', 'placeholder' => 'Поиск']); ?>
            <?= Html::hiddenInput('update_implementationMonitor_default_search',true); ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::button('Очистить', ['class' => 'btn btn-sm btn-default', 'onclick' => '
                $("#search-textbox").val("");
                $("#search-form").submit();
            ']) ?>
        </div>

        <?if($hasPositionsWithLowBalance):?>
            <div class="col-md-3">
                <span class="text-warning" style="font-size:1.2em;font-weight:bold;"><?=Yii::t('app', 'Присутствуют позиции с низким уровнем остатков!')?></span>
                <br>
                <?=Html::a(Yii::t('app', 'Посмотреть'), Url::to(['low-balance-list']), [
                    'class' => 'btn btn-outline btn-round btn-sm btn-warning',
                    'style' => 'margin-top:5px;',
                    'data-toggle' => 'modal',
                    'data-target' => '#modal-low-balance',
                    'onclick' => '
                        $("#block-low-balance-content").load($(this).attr("href"));
                    '
                ])?>
            </div>
        <?endif;?>

    </div>

    <?php ActiveForm::end(); ?>

    <?= ListView::widget([
        'dataProvider' => $provider,
        'itemView' => '_order2',
    ]) ?>
<?php Pjax::end(); ?>

<?php
Modal::begin([
    'header' => '<h2>Отгрузка реестров</h2>',
    'options' => [
        'id' => 'myModal3',
    ],
    'size' => 'modal-lg'
]);

//Подгрузить представление для получения заявок
echo Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control','id' => 'barcode-scaner-2',
    'onchange' => '
        $.get( "'. Url::to(['search-reestr']) .'", {barcode: $(this).val()}, function( data ) {
             if (data.status == true) {
                 notie.alert(1,"Реестр успешно отгружен",2);
             }else if (data.status == false) {
                  notie.alert(3,"Реестр НЕ отгружен. Что то пошло не так",2);
             }else {
                  notie.alert(2,"Непонятная ошибка, обратитесь к IT-службе",2);
             }
        });
        setTimeout(function(){
            $("#barcode-scaner-2").focus(); 
            $("#barcode-scaner-2").val("");
        }, 1000);

    ']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Отгрузка приказа № <span class="shipment-number"></span></h2>',
    'options' => [
        'id' => 'modal-ship-one',
    ],
    'size' => 'modal-sm'
]);

//Подгрузить представление для получения заявок
echo Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control barcode-scaner',
    'placeholder' => 'Штрихкод приказа',
    'onchange' => '
        $.get( "'. Url::to(['search-ship-one']) .'", {shipmentId: $(this).data("shipment-id"), barcode: $(this).val()}, function( data ) {
             if (data.status == true) {
                 notie.alert(1,"Приказ успешно отгружен",2);
                 $("#modal-ship-one").modal("hide");
             }else if (data.status == false) {
                  notie.alert(3, data.error ? data.error : "Приказ НЕ отгружен. Что то пошло не так",2);
             }else {
                  notie.alert(2,"Непонятная ошибка, обратитесь к IT-службе",2);
             }
        });
        $(this).val("");
    ']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Монитор упаковки</h2>',
    'options' => [
        'id' => 'myModal',
    ],
    'size' => 'modal-lg'
]);

echo Html::button(Yii::t('app',  'Упаковать'), [
    'id' => 'pack-button',
    'class' => 'btn btn-primary',
    'style' => 'margin-bottom:10px;',
    'onclick' => '
        $.get( "'. Url::to(['pack-shipment-without-upns']) .'", {shipmentId: $(this).attr("data-shipment-id")}, function(data) {
            if (!data.status) {
                notie.alert(3,"Не удалось выполнить команду",3);
                return;
            }
            notie.alert(1,"Успешное выполнение",2);
            $.get("'. Url::to(['registry-shipment-print']) . '?id=" + $("#hidden-href").attr("data-shipment-id"), function(data) {
                myWindow = window.open();
                myWindow.document.write(data);
                myWindow.print();
                myWindow.close();
            });
            $("#myModal").modal("hide");
        });
    '
]);

//echo Html::button(Yii::t('app',  'Не могу найти UPN'), [
//    'id' => 'button-open-rework-form',
//    'class' => 'btn btn-warning',
//    'style' => 'margin-bottom:10px;',
//    'onclick' => '
//        var shipmentId = $(this).data("shipment-id");
//        $("#block-rework-list").load("/logistics/implementation-monitor/default/rework-shipment-form?shipmentId=" + shipmentId);
//        $("#myModal").modal("hide");
//        $("#modal-rework-shipment").modal("show");
//    '
//]);

//Подгрузить представление для получения заявок
echo Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control','id' => 'barcode-scaner',
   'onchange' => '
        $.get( "'. Url::to(['search-box']) .'", {shipmentId: $("#hidden-href").attr("data-shipment-id"), boxId: $("#hidden-href").attr("data-box-id"), barcode: $(this).val()}, function(data) {
            if (data.status == "success") {
                notie.alert(1,"Успешное выполнение",2);
            } else if (data.status == "error" && data.printLabel == true) {
                notie.alert(1,"Распечатана этикетка. Наклейте ее на продукт и пропикайте ее",2);
            } else if (data.status == "error") {
                notie.alert(3,"Не верный ввод данных, повторите попытку",2);
            } else {
                notie.alert(2,"Непонятная ошибка, обратитесь к IT-службе",2);
            }
            $("#order-content").load( $("#hidden-href").attr("href") , function() {
                $("#barcode-scaner").focus();
                $("#barcode-scaner").val("");
            });
            if (data.boxStatus == true) {
                $.get("'. Url::to(['literal-box-print']) . '?id=" + $("#hidden-href").attr("data-box-id"), function(data) {
                    myWindow = window.open();
                    myWindow.document.write(data);
                    myWindow.print();
                    myWindow.close();
                });
                $("#myModal").modal("hide");
            }
            if (data.printLabel == true) {
                $.get("'. Url::to(['/logistics/naryad/print-single-label']) . '?id=" + data.upnId, function(data) {
                    myWindow = window.open();
                    myWindow.document.write(data);
                    myWindow.print();
                    myWindow.close();
                });
            }
            if (data.printParentLabel == true) {
                $.get("'. Url::to(['/logistics/naryad/print-full-parent-label']) . '?id=" + data.parentId, function(data) {
                    myWindow = window.open();
                    myWindow.document.write(data);
                    myWindow.print();
                    myWindow.close();
                });
            }
        });
   ']);
echo Html::a('', $url = '', $options = ['class' => 'hidden' , 'id' => 'hidden-href']);
echo Html::tag('div','',['id' => 'order-content']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Документы</h2>',
    'options' => [
        'id' => 'modal-print-documents',
    ],
    'size' => 'modal-sm'
]);

echo '<div class="documents"></div>';

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Установить вес</h2>',
    'options' => [
        'id' => 'modal-update-weight',
    ],
    'size' => 'modal-lg'
]);

echo '<div style="display:inline-block;width:200px;margin-right:10px;">';
echo '<input class="form-control weight-textbox" type="number" min="0" />';
echo '</div>';
echo Html::button(Yii::t('app',  'Сохранить'), ['class' => 'btn btn-primary weight-save-button']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Установить габариты</h2>',
    'options' => [
        'id' => 'modal-update-size',
    ],
    'size' => 'modal-lg'
]);

echo '<div style="display:inline-block;width:200px;margin-right:10px;">';
echo '<input class="form-control size-textbox" type="text"/>';
echo '</div>';
echo Html::button(Yii::t('app',  'Сохранить'), ['class' => 'btn btn-primary size-save-button']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Позиции с низким уровнем остатков</h2>',
    'options' => [
        'id' => 'modal-low-balance',
    ],
    'size' => 'modal-lg'
]);

echo Html::tag('div','',['id' => 'block-low-balance-content']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Заказы, которые не могут быть исполнены из за отсутствия остатков на складе</h2>',
    'options' => [
        'id' => 'modal-danger',
    ],
    'size' => 'modal-lg'
]);

echo Html::tag('div','',['id' => 'block-danger-content']);

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h2>Отправка отгрузки на доработку</h2>',
    'options' => [
        'id' => 'modal-rework-shipment',
    ],
    'size' => 'modal-lg'
]);

echo Html::tag('div','',['id' => 'block-rework-list']);

Modal::end();
?>

<?php Yii::$app->view->registerJs('
    $("body").on("click", ".button-filter",function(e){
        $(".button-filter").prop("disabled", true);
        var html = \'<i class="fa fa-spinner fa-spin" style="font-size:20px;"></i>\'
        $(this).attr("title", "' . Yii::t('app', 'Подождите...') . '").find(".badge").html(html);
        var value = $(this).hasClass("active") ? "" : $(this).data("filter");
        $("#hidden-filter").val(value);
        $("#search-form").submit();
    });
        
    //Данная функция отвечает за сохранение веса
    $(".weight-save-button").click(function() {
        var textbox = $("#modal-update-weight .weight-textbox");
        var shipmentId = textbox.data("shipment-id");
        var weight = textbox.val();
        if (!(weight > 0)) {
            return;
        }
        $.ajax({
            url: "/logistics/shipment-monitor/logist/save-weight",
            dataType: "json",
            data: {
                "shipmentId": shipmentId,
                "weight": weight
            },
            success: function (data) {
                if (!data.status) {
                    notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                    return;
                }
                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                $("#modal-update-weight").modal("hide");
            },
            error: function() {
                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
            }
        });
    });

    //Данная функция отвечает за сохранение габаритов
    $(".size-save-button").click(function() {
        var textbox = $("#modal-update-size .size-textbox");
        var shipmentId = textbox.data("shipment-id");
        var size = textbox.val();
        if (!$.trim(size)) {
            return;
        }
        $.ajax({
            url: "/logistics/shipment-monitor/logist/save-size",
            dataType: "json",
            data: {
                "shipmentId": shipmentId,
                "size": size
            },
            success: function (data) {
                if (!data.status) {
                    notie.alert(3, data.message ? data.message : "Не удалось сохранить", 3);
                    return;
                }
                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                $("#modal-update-size").modal("hide");
            },
            error: function() {
                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
            }
        });
    });
    
    //Функция обновляет содержимое модального окна "Заказы, которые не могут быть исполнены из за отсутствия остатков"
    function updateDangerList() {
        var html = "";
        html += \'<div class="text-center">\';
        html += \'<i class="fa fa-spinner fa-spin" style="font-size:20px;" title="' . Yii::t('app', 'Подождите...') . '"></i>\';
        html += \'</div>\';
        $("#block-danger-content").html(html).load($("#link-show-danger-list").attr("href"));
    }
    
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>
