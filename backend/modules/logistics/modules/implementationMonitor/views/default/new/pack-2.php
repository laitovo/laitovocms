<?php
/**
 * @var  $provider \yii\data\ArrayDataProvider
 */

use yii\widgets\ListView;

$this->render('@backendViews/logistics/views/menu');

?>

<?=
ListView::widget([
    'dataProvider' => $provider,
    'itemView' => '_order_sm_2',
    'summary' => false
])
?>

