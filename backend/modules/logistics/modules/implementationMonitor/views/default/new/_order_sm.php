<?php
use yii\helpers\Html;

$this->registerCss(".table > thead > tr > th{ font-weight: 600 }");
?>
<?php foreach ($model['orders'] as $order): ?>
    <div style="font-weight:bold;">
        № Заказа
        <div><?=$order['sourceId']?></div>
    </div>
    <?php if ($order['innerComment']):?>
        <div class="alert" style="background:white;color:#001F3F;font-size: 1.2em;border-radius: 10px; border: 1px solid #001F3F;opacity: 0.7;">
            <strong style="font-weight:bold;">Комментарий:</strong> <?=$order['innerComment']?>
        </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead class="some-bold">
        <tr>
<!--            <th class="text-nowrap">Заказ №</th>-->
            <th>Артикул</th>
            <th>Наименование</th>
            <th>Upn</th>
            <th>МестоНахождение</th>
        </tr>
        </thead>
        <tbody>
<!--        <tr class="--><?//= $order['elements'][0]['pack'] || $order['elements'][0]['storage'] ? 'success' : ''?><!--">-->
<!--            <td rowspan="--><?//=count($order['elements'])?><!--" >--><?php //echo $order['orderId']?><!--</td>-->
<!--            <td ">--><?//= @$order['elements'][0]['article'] ?><!--</td>-->
<!--            <td ">--><?//= @$order['elements'][0]['title'] ?><!--</td>-->
<!--            --><?//if (!$order['elements'][0]['storage']) :?>
<!--            <td ">-->
<!--                --><?//= @$order['elements'][0]['upn'] ? str_replace((mb_substr(@$order['elements'][0]['upn'], 4, mb_strlen(@$order['elements'][0]['upn']))),
//                ('<b style="font-size: 1.4em">' . mb_substr(@$order['elements'][0]['upn'], 4, mb_strlen(@$order['elements'][0]['upn'])) . '</b>'), @$order['elements'][0]['upn']) : @$order['elements'][0]['upn'] ?>
<!--            </td>-->
<!--            <td ">--><?//= @$order['elements'][0]['location'] ?><!--</td>-->
<!--            --><?//else :?>
<!--            <td rowspan="2">Комплектуется со склада</td>-->
<!--            --><?//endif;?>
<!--    -->
<!--        </tr>-->
            <?foreach ($order['elements'] as $parentIndex => $elements):?>
                <?if ($parentIndex):?>
                    <tr>
                        <td colspan="4"><b style="font-size: 1.5em">Комлпект № <?=$parentIndex?></b></td>
                    </tr>
                <?endif;?>
                <?foreach ($elements as $element):?>
<!--            --><?php //if ($key !== 0): ?>
                <tr class="<?= @$element['pack'] ? 'success' : ''?>">
                    <td>
                        <?= @$element['article'] ?>
                        <?php
                        if ($element['storage']) {
                            $attention = $element['hasBalanceControl'] && $element['minBalance'] > $element['factBalance'];
                            echo Html::tag('span', '(' . $element['factBalance'] . ')', [
                                'class' => !$attention ? '' : 'text-danger',
                                'style' => !$attention ? '' : 'font-size:1.3em;font-weight:bold;',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title'  => Yii::t('app', !$attention ? 'Фактический остаток' : 'Фактический остаток (Внимание! Меньше минимального!)')
                            ]);
                        }
                        ?>
                    </td>
                    <td "><?= @$element['title'] ?></td>
<!--                    --><?//if (!@$element['storage']) :?>
                    <td ">
                        <?= @$element['upn'] ? str_replace((mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn']))),
                        ('<b style="font-size: 1.4em">' . mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn'])) . '</b>'), @$element['upn']) : @$element['upn'] ?>
                    </td>
                    <td style="color:black;font-size: larger"><?= @$element['location'] ?></td>
<!--                    --><?//else :?>
<!--                        <td colspan="2">Комплектуется со склада</td>-->
<!--                    --><?//endif;?>
                </tr>
<!--            --><?php //endif;?>
            <?php endforeach;?>
                <?if ($parentIndex):?>
                    <tr>
                        <td colspan="4" style="padding: 0px;" class="bg-info">&nbsp</td>
                    </tr>
                <?endif;?>
        <?php endforeach;?>
        </tbody>
    </table>
<?php endforeach;?>
