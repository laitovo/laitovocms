<?php
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
?>
<div class="col-md-1 text-center">
    <table class="table">
        <thead>
        <th class="text-center">№ Приказа</th>
        </thead>
        <tbody>
        <tr>
            <td><div style="font-size: 2em;font-weight: bold"><?= $model['shipmentId']?></div></td>
            <td><div style="font-size: 2em;font-weight: bold"><?= $generator->getBarcode('SHIP'.$model['shipmentId'], $generator::TYPE_CODE_128, 1) ?></div></td>
        </tr>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<?foreach ($model['orders'] as $order):?>
    <div style="font-weight:bold;">
        № Заказа
        <div style="font-size:1.6em;"><?=$order['sourceId']?> <?=$order['client']?></div>
    </div>
    <?php if ($order['innerComment']):?>
        <span style="font-weight:bold;">Комментарий: </span><?=$order['innerComment']?>
    <?php endif; ?>
    <table style="border-collapse: collapse;font-size: 1em" cellpadding="4">
        <thead>
        <th>Штрихкод 1С</th>
        <th>Артикул</th>
        <th>Наименование</th>
        <th>UPN</th>
        <th>Место</th>
        </thead>
        <tbody>
        <?foreach ($order['elements'] as $parentIndex => $set):?>
            <?if ($parentIndex):?>
                <tr>
                    <td colspan="4"><b style="font-size: 1.5em">Комплект № <?=$parentIndex?></b></td>
                </tr>
            <?endif;?>
            <?foreach ($set as $element):?>
                <tr>
                    <td style="white-space:nowrap;"><div style="padding-left:5px;padding-right: 20px;"><?= $generator->getBarcode($element['barcode'], $generator::TYPE_CODE_128, 1) ?></div></td>
                    <td style="font-size: 0.8em;border:1px solid;white-space:nowrap;"><?=$element['article']?></td>
                    <td style="font-size: 0.8em;border:1px solid"><?=$element['title'] . ' <span style="font-weight: bold"> ' . $element['car'] . '</span>'?></td>
                    <td style="font-size: 0.8em;border:1px solid"><?=$element['upn']?></td>
                    <td style="font-size: 0.8em;border:1px solid"><?=$element['location']?></td>
                </tr>
            <?endforeach;?>
            <?if ($parentIndex):?>
                <tr>
                    <td colspan="4" style="padding: 0px;" class="bg-info">&nbsp</td>
                </tr>
            <?endif;?>
        <?endforeach;?>
        </tbody>
    </table>
<?endforeach;?>
