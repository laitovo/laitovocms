<?php
use yii\helpers\Url;
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
?>
<div class="col-md-1 text-center">
    <table class="table">
        <thead>
        <th class="text-center">№ Приказа</th>
        </thead>
        <tbody>
        <tr>
            <td><div style="font-size: 2em;font-weight: bold"><?= $model['shipmentId']?></div></td>
            <td><div style="font-size: 2em;font-weight: bold"><?= $generator->getBarcode($model['shipmentId'], $generator::TYPE_CODE_128, 1) ?></div></td>
        </tr>
        </tbody>
    </table>
</div>
<div>

    <?foreach ($model['orders'] as $order):?>
        <div>
            <table border="1px solid" style="border-collapse: collapse">
                <thead>
                <th colspan="2">Общая информация</th>
                </thead>
                <tbody>
                <tr>
                    <td>Заказ №</td>
                    <td><?= $order['sourceId']?></td>
                </tr>
                <tr>
                    <td>Менеджер&nbsp;:</td>
                    <td><?= $order['manager']?></td>
                </tr>
                <tr>
                    <td>Способ доставки&nbsp;:</td>
                    <td><?= $order['delivery']?></td>
                </tr>
                <tr>
                    <td>Адрес доставки&nbsp;:</td>
                    <td><?= $order['address']?></td>
                </tr>
                </tbody>
                <thead>
                <th colspan="2">Информация о клиенте</th>
                </thead>
                <tbody>
                <tr>
                    <td>ФИО&nbsp;:</td>
                    <td><?= $order['client']?></td>
                </tr>
                <tr>
                    <td>Телефон&nbsp;:</td>
                    <td><?= $order['phone']?></td>
                </tr>
                <tr>
                    <td>Email&nbsp;:</td>
                    <td><?= $order['email']?></td>
                </tr>
                <tr>
                    <td>Категория&nbsp;:</td>
                    <td><?= $order['category']?></td>
                </tr>
                </tbody>
            </table>
        </div>
        &nbsp;
        <div>
            <table style="border-collapse: collapse;font-size: 1em" cellpadding="4">
                <thead>
                <th>Штрихкод</th>
                <th>Артикул</th>
                <th>Наименование</th>
                <th>UPN</th>
                <th>Место</th>
                </thead>
                <tbody>
                <?foreach ($order['elements'] as $element):?>
                    <tr>
                        <td><div style = "margin:0 40px 0 40px"><?= $generator->getBarcode($element['barcode'], $generator::TYPE_CODE_128, 1) ?></div></td>
                        <td style="border:1px solid"><?=$element['article']?></td>
                        <td style="border:1px solid"><?=$element['title']?></td>
                        <td style="border:1px solid"><?=$element['upn']?></td>
                        <td style="border:1px solid"><?=$element['location']?></td>
                    </tr>
                    <tr style="width:300px">
                        <td colspan = "6">&nbsp;<br>&nbsp;</td>
                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>
    <?endforeach;?>
</div>
<div class="clearfix"></div>

