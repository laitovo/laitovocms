<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<hr>
<div style="font-size: 0.84em">
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
                <tr>
                    <th class="text-center text-nowrap">№ Приказа</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $model['shipmentId'] ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-7 text-center">
        <?foreach ($model['orders'] as $order):?>
            <div class="col-md-2 text-center">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center text-nowrap">№ Заказа</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td onclick = '
                                    $("#shipment-<?=@$model['shipmentId']?>-order-<?=@$order['orderId']?>").slideToggle( "slow", function() {});
                                    $(".shipment-<?=@$model['shipmentId']?>-order-<?=@$order['orderId']?>-name-hide").fadeToggle( "slow", function() {});
                            '>
                                <?= @$order['sourceId'] ? str_replace((mb_substr(@$order['sourceId'], 4, mb_strlen(@$order['sourceId']))),
                                    ('<b style="font-size: 1.4em;font-weight: bold; color: red;">' . mb_substr(@$order['sourceId'], 4, mb_strlen(@$order['sourceId'])) . '</b>'), @$order['sourceId']) : @@$order['sourceId'] ?>
                                <?php if ($order['isNeedTtn']): ?>
                                    <strong class="text-nowrap text-warning" style="font-size:2em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Обязательно приложить ТТН к заказу')?>"><?=Yii::t('app', '(с ТТН)')?></strong>
                                <?php endif;?>
                                <?php if ($order['isCOD']): ?>
                                    <strong class="text-nowrap text-danger" style="font-size:2em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Оплата заказа производится наложенным платежем')?>"><?=Yii::t('app', '(Наложка)')?></strong>
                                <?php endif;?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 text-center">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">Клиент</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $order['client']?></td>
                        </tr>
                        <tr>
                            <td><?= $order['category']?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-8 text-center">
                <div class="row" style="display:none;" id="shipment-<?=@$model['shipmentId']?>-order-<?=@$order['orderId']?>">
                    <div class="col-md-6 text-center">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="2">Общая информация</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Заказ №</td>
                                    <td><?= $order['sourceId']?></td>
                                </tr>
                                <tr>
                                    <td>Менеджер&nbsp;:</td>
                                    <td><?= $order['manager']?></td>
                                </tr>
                                <tr>
                                    <td>Способ доставки&nbsp;:</td>
                                    <td><?= $order['delivery']?></td>
                                </tr>
                                <tr>
                                    <td>Адрес доставки&nbsp;:</td>
                                    <td><?= $order['address']?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6 text-center">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="2">Информация о клиенте</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>ФИО&nbsp;:</td>
                                    <td><?= $order['client']?></td>
                                </tr>
                                <tr>
                                    <td>Телефон&nbsp;:</td>
                                    <td><?= $order['phone']?></td>
                                </tr>
                                <tr>
                                    <td>Email&nbsp;:</td>
                                    <td><?= $order['email']?></td>
                                </tr>
                                <tr>
                                    <td>Категория&nbsp;:</td>
                                    <td><?= $order['category']?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <table class="table table-hover table-default">
                            <thead>
                            <th>Автомобиль</th>
                            <th>Артикул</th>
                            <th class="shipment-<?=@$model['shipmentId']?>-order-<?=@$order['orderId']?>-name-hide" style="display:none;">Наименование</th>
                            <th>UPN</th>
                            <th>Местонахождение</th>
                            </thead>
                            <tbody>
                            <?foreach ($order['elements'] as $element):?>
                                <?php $flag = @$element['readyToPack'] && @$element['pack']; ?>
                                <tr class="<?= $flag ? 'success' : ''?>">
                                    <td><?=$element['car']?></td>
                                    <td style="white-space:nowrap">
                                        <?=$element['article']?>
                                        <?php
                                            if ($element['storage']) {
                                                $attention = $element['hasBalanceControl'] && $element['minBalance'] > $element['factBalance'];
                                                echo Html::tag('span', '(' . $element['factBalance'] . ')', [
                                                    'class' => !$attention ? '' : 'text-danger',
                                                    'style' => !$attention ? '' : 'font-size:1.3em;font-weight:bold;',
                                                    'data-toggle' => 'tooltip',
                                                    'data-title'  => Yii::t('app', !$attention ? 'Фактический остаток' : 'Фактический остаток (Внимание! Меньше минимального!)')
                                                ]);
                                            }
                                        ?>
                                    </td>
                                    <td class="shipment-<?=@$model['shipmentId']?>-order-<?=@$order['orderId']?>-name-hide" style="display: none"><?=$element['title']?></td>
                                    <td>
                                        <?= @$element['upn'] ? str_replace((mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn']))),
                                            ('<b style="font-size: 1.4em;font-weight: 500; color: green;">' . mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn'])) . '</span>'), @$element['upn']) : @$element['upn'] ?>
                                    </td>
                                    <td style="font-size: 1.4em;font-weight: 500; color: black;"><?=$element['location']?></td>
                                </tr>
                            <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        <?endforeach;?>
    </div>
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center">Документы</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <?= Html::button('<span class="fa fa-file" style="font-size:2em;" title="' . Yii::t('app', 'Печать документов логистики') . '" data-toggle="tooltip" data-placement="top"></span>', [
                        'class' => 'btn btn-sm btn-inverse',
                        'data' => [
                            'shipment-id' => $model['shipmentId'],
                        ],
                        'onclick' => '
                            var shipmentId = $(this).data("shipment-id");
                            $.ajax({
                                url: "/logistics/implementation-monitor/default/ajax-get-shipment-documents",
                                dataType: "json",
                                data: {
                                    "shipmentId": shipmentId
                                },
                                success: function (data) {
                                    fillDocumentsTable(data.documents);
                                    $("#modal-print-documents").modal("show");
                                },
                                error: function() {
                                    notie.alert(3, "Не удалось загрузить документы", 3);
                                }
                            });
                        ',
                    ]);?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center">Реестр</th>
            </thead>
            <tbody>
            <tr class="lead">
                <td class="text-center">
                    <iframe id="iframe-reestr-<?= $model['shipmentId']?>" src="<?= Url::to(['registry-shipment-print']) . '?id=' . $model['shipmentId'] ?>" style="display:none;"></iframe>
                    <button class="btn btn-sm btn-inverse" title="<?= Yii::t('app', 'Распечатать реестр') ?>" data-toggle="tooltip" data-placement="top" onclick="printIframeContent('iframe-reestr-<?= $model['shipmentId']?>');">
                        <span style="font-size:2em;color:blueviolet;" class="glyphicon glyphicon-print"></span>
                    </button>
                </td>
            </tr>
            <tr class="lead">
                <td class="text-center">
                    <iframe id="iframe-package-reestr-<?= $model['shipmentId']?>" src="<?= Url::to(['registry-shipment-package-print']) . '?id=' . $model['shipmentId'] ?>" style="display:none;"></iframe>
                    <button class="btn btn-sm btn-inverse" title="<?= Yii::t('app', 'Распечатать упаковочный лист') ?>" data-toggle="tooltip" data-placement="top" onclick="printIframeContent('iframe-package-reestr-<?= $model['shipmentId']?>');">
                        <span style="font-size:2em;color:green;" class="glyphicon glyphicon-print"></span>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center">Обработка</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <?php if($model['isHandled']): ?>
                        <?= Html::a('<span style="font-size: 3.2em" class="glyphicon glyphicon-ok text-success"></span>',Url::to(['pack','shipmentId' => $model['shipmentId'] ]),$options = [
                            'class'            => 'load-order',
                            'data-toggle'      => 'modal',
                            'data-target'      => '#myModal',
                            'data-shipment-id' => $model['shipmentId'],
                            'data-has-upns'    => $model['hasUpns'] ? 1 : '',
                            'onclick'          => '
                                $("#order-content").load($(this).attr("href"));
                                $("#hidden-href").attr("href", $(this).attr("href"));
                                $("#hidden-href").attr("data-shipment-id", $(this).attr("data-shipment-id"));
                                
                                $("#pack-button").attr("data-shipment-id", $(this).attr("data-shipment-id"));
                                if ($(this).attr("data-has-upns")) {
                                    $("#pack-button").addClass("hide");
                                } else {
                                    $("#pack-button").removeClass("hide");
                                }
                                
                                $("#button-open-rework-form").attr("data-shipment-id", $(this).attr("data-shipment-id"));
                            ',
                        ]) ?>
                    <?php elseif($model['isOnStorageRequest']): ?>
                        <?php if($model['hasWeighing'] && !$model['weight']): ?>
                            <?= Html::tag('i', '', [
                                'class' => 'fa fa-balance-scale weight-edit-button',
                                'style' => 'font-size:2em;cursor:pointer;',
                                'data' => [
                                    'shipment-id' => $model['shipmentId'],
                                    'toggle' => 'tooltip',
                                    'title' => Yii::t('app', 'Установить вес')
                                ],
                                'onclick' => '
                                    var weightTextbox = $("#modal-update-weight .weight-textbox");
                                    if (weightTextbox.data("shipment-id") != $(this).data("shipment-id")) {
                                        weightTextbox.val("");
                                    }
                                    weightTextbox.data("shipment-id", $(this).data("shipment-id"));
                                    $("#modal-update-weight").modal("show");
                                ',
                            ]);?>
                        <?php endif; ?>
                        <?php if($model['hasSizing'] && !$model['size']): ?>
                            <?= Html::img('/img/ruler.png', [
                                'class' => 'size-edit-button',
                                'style' => 'position:relative;top:-5px;width:24px;height:24px;opacity:0.7;cursor:pointer;',
                                'data' => [
                                    'shipment-id' => $model['shipmentId'],
                                    'toggle' => 'tooltip',
                                    'title' => Yii::t('app', 'Установить габариты')
                                ],
                                'onclick' => '
                                    var sizeTextbox = $("#modal-update-size .size-textbox");
                                    if (sizeTextbox.data("shipment-id") != $(this).data("shipment-id")) {
                                        sizeTextbox.val("");
                                    }
                                    sizeTextbox.data("shipment-id", $(this).data("shipment-id"));
                                    $("#modal-update-size").modal("show");
                                ',
                            ]);?>
                        <?php endif; ?>
                    <?php elseif($model['isPacked']): ?>
                        <?= Html::button('<span class="fa fa-truck" style="font-size:2em;" title="' . Yii::t('app', 'Отгрузить приказ') . '" data-toggle="tooltip" data-placement="top"></span>', [
                            'class' => 'btn btn-sm btn-inverse ship-button',
                            'data' => [
                                'shipment-id' => $model['shipmentId'],
                            ],
                            'onclick' => '
                                $("#modal-ship-one .barcode-scaner").data("shipment-id", $(this).data("shipment-id"));
                                $("#modal-ship-one .shipment-number").html($(this).data("shipment-id"));
                                $("#modal-ship-one").modal("show");
                            ',
                        ]);?>
                    <?php endif; ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center">Отгрузка</th>
            </thead>
            <tbody>
            <tr>
                <td><?= $model['shipmentDate'] ? Yii::$app->formatter->asDate($model['shipmentDate'], 'dd.MM.yyyy') : null ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
</div>

<?php Yii::$app->view->registerJs('
    //Функция заполняет таблицу документов в модальном окне печати
    function fillDocumentsTable(documents) {
        html = "<table>";
        documents.forEach(function(doc) {
            html += "<tr>";
            html += "<td style=\"min-width:200px;\">" + doc.name + "</td>";
            html += "<td>";

            var iframeId = "iframe-" + doc.id;
            html += "<iframe id=\'" + iframeId + "\' src=\'" + doc.url + "\' style=\'display:none;\'></iframe>";
            html += "<button class=\'btn btn-sm btn-inverse print-document-button\' title=\'Распечатать документ\' data-toggle=\'tooltip\' data-placement=\'top\' onclick=\'printIframeContent(\"" + iframeId + "\");\'>";
            html += "<span style=\'font-size:2em;color:blueviolet;\' class=\'glyphicon glyphicon-print\'></span>";
            html += "</button>";
            
//            //Альтернативный вариант печати через printJs: 
//            html += "<button class=\'btn btn-sm btn-inverse print-document-button\' title=\'Распечатать документ\' data-toggle=\'tooltip\' data-placement=\'top\' onclick=\'printJS(\"" + doc.url + "\")\'>";
//            html += "<span style=\'font-size:2em;color:blueviolet;\' class=\'glyphicon glyphicon-print\'></span>";
//            html += "</button>";
            
            html += "</td>";
            html += "</tr>";
        });
        html += "</table>";
        $("#modal-print-documents .documents").html(documents.length ? html : "Документы не найдены");
    }
    
    //Вывод на печать содержимого iframe
    function printIframeContent(iframeId) {
        var iframe = document.getElementById(iframeId);
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>