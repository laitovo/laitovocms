<?php
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
?>
<? $count = 0;?>
<h3>Shipment from <?= $model['shipmentDate']; ?> № <?=$model['shipmentId']?> Box № <?= $model['boxId']?></h3>
<table style="border-collapse: collapse;font-size: 1em" cellpadding="2">
    <thead>
    <th>№</th>
    <th>Title</th>
    <th>Quantity</th>
    </thead>
    <tbody>
<?foreach ($model['result'] as $element):?>
        <? $count++;?>
            <tr>
                <td style="font-size: 0.8em;border:1px solid;text-align: center"><div style="padding-left:5px;padding-right: 20px;"><?= $count ?></div></td>
                <td style="font-size: 0.8em;border:1px solid"><?=$element['title']?></td>
                <td style="font-size: 0.8em;border:1px solid;text-align: center"><?= $element['quantity'] ?></td>
            </tr>
        <?endforeach;?>
    </tbody>
</table>