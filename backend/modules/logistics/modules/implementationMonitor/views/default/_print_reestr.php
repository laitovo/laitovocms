<?php
use yii\helpers\Html;
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>
<style type="text/css">
    table th, table td {
    border:4px solid #000;
    padding;0.5em;
    }
</style>


<table border="4" class="invoice_items" width="100%" cellpadding="2" cellspacing="50"
       style="font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td style="border-collapse: collapse;padding: 0;font-size: 2.7em;">
            <i>РЕЕСТР №:</i> <?= $reestr ?>
        </td>
        <td  style="padding: 0;" >
            <div style="display: inline-block; margin-left: auto;margin-right: auto;">
                <?= $generator->getBarcode(@$reestr, $generator::TYPE_CODE_128, 1) ?>
            </div>
        </td>
    </tr>
    <?php foreach ($orders as $order):?>
        <tr>
            <td style="font-size: 4em; font-weight: bold;"><?= $order['order']?></td>
            <td>
        <?php foreach ($order['barcodes'] as $barcode):?>
                <div style="display: inline-block; margin-left: auto;margin-right: auto;">
                    <?= $generator->getBarcode($barcode, $generator::TYPE_CODE_128, 1) ?>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
        <?endforeach;?>
            </td>
        </tr>
    <?endforeach;?>
    </tbody>
</table>
