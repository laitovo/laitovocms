<?php
use yii\helpers\Url;
use yii\helpers\Html;
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
?>
<hr>
<div style="font-size: 0.84em">
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center">№ Заказа</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div onclick = '$("#order-<?=@$model['orderId']?>").slideToggle( "slow", function() {});'>
                        <?= @$model['sourceId'] ? str_replace((mb_substr(@$model['sourceId'], 4, mb_strlen(@$model['sourceId']))),
                            ('<b style="font-size: 1.4em;font-weight: bold; color: red;">' . mb_substr(@$model['sourceId'], 4, mb_strlen(@$model['sourceId'])) . '</b>'), @$model['sourceId']) : @@$model['sourceId'] ?>
                        <br>
                        <span class="text-nowrap">от <?= date('d.m.Y',$model['createdAt']) ?></span>
                        <?php if ($model['isNeedTtn']): ?>
                            <strong class="text-nowrap" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Обязательно приложить ТТН к заказу')?>"><?=Yii::t('app', '(с ТТН)')?></strong>
                        <?php endif;?>
                        <?php if ($model['isCOD']): ?>
                            <strong class="text-nowrap text-danger" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Оплата заказа производится наложенным платежем')?>"><?=Yii::t('app', '(Наложка)')?></strong>
                        <?php endif;?>
                    </div>
                    <?= Html::a('<i class="fa fa-eye"></i>', ['/logistics/logistics-monitor/default/view-order', 'orderId' => $model['orderId']], [
                        'target' => '_blank',
                        'class' => 'btn btn-xs btn-icon btn-outline btn-round btn-primary',
                        'style' => 'margin-top:3px;',
                        'data-pjax' => '',
                        'data-toggle' => 'tooltip',
                        'data-original-title' => Yii::t('app', 'Просмотр заказа')]); ?>
                    <?php if(Yii::$app->user->getId() == 21):?>
                    <hr>
                    <?= Html::a('<i class="fa fa-times-circle"></i>', ['/logistics/implementation-monitor/default/delete-order', 'id' => $model['orderId']], [
                        'target' => '_blank',
                        'class' => 'btn btn-xs btn-icon btn-outline btn-round btn-danger',
                        'style' => 'margin-top:3px;',
                        'data-pjax' => '',
                        'data-toggle' => 'tooltip',
                        'data-original-title' => Yii::t('app', 'Отменить и удалить заказ из системы'),
                        'data' => [
                            'confirm' => Yii::t('yii', 'Вы действительно хотите УДАЛИТЬ заказ (Наряды на производство при этом только ОТВЯЗЫВАЮТСЯ от заказа, НО НЕ УДАЛЯЮТСЯ!!!) ?'),
                            'method' => 'post',
                        ],
                    ]); ?>
                     |
                    <?php endif;?>
                    <?php if(in_array(Yii::$app->user->getId(), [21,36])):?>
                    <?= Html::a('<i class="fa fa-times-circle"></i>', ['/logistics/implementation-monitor/default/cancel-order', 'id' => $model['orderId']], [
                        'target' => '_blank',
                        'class' => 'btn btn-xs btn-icon btn-outline btn-round btn-warning',
                        'style' => 'margin-top:3px;',
                        'data-pjax' => '',
                        'data-toggle' => 'tooltip',
                        'data-original-title' => Yii::t('app', 'Отменить без удаления'),
                        'data' => [
                            'confirm' => Yii::t('yii', 'Вы действительно хотите ОТМЕНИТЬ заказ ?'),
                            'method' => 'post',
                        ],
                    ]); ?>
                    <?php endif;?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center">Клиент</th>
            </thead>
            <tbody>
                <tr>
                    <td><?= $model['client']?> <?= $model['exportClientName'] ? '(' . $model['exportClientName'] .  ')' : ''?> </td>
                </tr>
                <tr>
                    <td><?= $model['category']?> <?= $model['exportCategory'] ? '(' . $model['exportCategory'] .  ')' : ''?> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="col-md-10">
<!--            <div class="col-md-12" id="order-sm---><?//=@$model['orderId']?><!--">-->
<!--                <table class="table">-->
<!--                    <thead>-->
<!--                    <th colspan="2">Информация о клиенте</th>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    <tr >-->
<!--                        <td>ФИО&nbsp;:</td>-->
<!--                        <td>--><?//= $model['client']?><!--</td>-->
<!--                        <td>Категория&nbsp;:</td>-->
<!--                        <td>--><?//= $model['category']?><!--</td>-->
<!--                    </tr>-->
<!--                    </tbody>-->
<!--                </table>-->
<!--            </div>-->
            <div class="col-md-12" style="display: none" id="order-<?=@$model['orderId']?>">
                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <thead>
                                <th colspan="2">Общая информация</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Заказ №</td>
                                    <td><?= $model['sourceId']?></td>
                                </tr>
                                <tr>
                                    <td>Менеджер&nbsp;:</td>
                                    <td><?= $model['manager']?></td>
                                </tr>
                                <tr>
                                    <td>Способ доставки&nbsp;:</td>
                                    <td><?= $model['delivery']?></td>
                                </tr>
                                <tr>
                                    <td>Адрес доставки&nbsp;:</td>
                                    <td><?= $model['address']?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <thead>
                                <th colspan="2">Информация о клиенте</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td>ФИО&nbsp;:</td>
                                <td><?= $model['client']?></td>
                            </tr>
                            <tr>
                                <td>Телефон&nbsp;:</td>
                                <td><?= $model['phone']?></td>
                            </tr>
                            <tr>
                                <td>Email&nbsp;:</td>
                                <td><?= $model['email']?></td>
                            </tr>
                            <tr>
                                <td>Категория&nbsp;:</td>
                                <td><?= $model['category']?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <table class="table table-hover table-default">
                    <thead>
    <!--                    <th>Строка заказа</th>-->
    <!--                    <th>Штрихкод</th>-->
                        <th>Автомобиль</th>
                        <th>Артикул</th>
                        <th>Наименование</th>
                        <th>UPN</th>
                        <th>Местонахождение</th>
                        <th class="text-nowrap">Отгрузка</th>
                        <?php if (in_array(Yii::$app->user->getId(), [21,48,36])) :?>
                            <th>Действия</th>
                        <?php endif; ?>
                    </thead>
                    <tbody>
                    <?foreach ($model['elements'] as $element):?>
                        <?php if ($element['isDeleted']) :?>
                            <tr><td colspan="6"><div style="position:relative;top:30px;height:3px;background:red;" data-toggle="tooltip" data-title="<?=Yii::t('yii', 'Позиция удалена (' . date('d.m.Y', $element['deletedAt'])) . ', ' . $element['deletedBy'] . ')'?>"></div></td></tr>
                        <?php endif; ?>
                        <? if (@$element['shipped']) {
                              $style = 'background-color: #33CCFF;color:white;';
//                            $class = 'info';
                        }elseif(@$element['isPacked']) {
                              $style = 'background-color: #660066;color:white;';
//                            $class = 'info';
                        }elseif(@$element['isHandled']) {
                              $style = 'background-color: #6666FF;color:white;';
//                            $class = 'info';
//                        }elseif(@$element['upn'] && !@$element['workOrderExists'] && !@$element['storageExists']){
//                              $style = 'background-color: #FF9933;color:white;';
////                            $class = 'warning';
                        }elseif(@$element['readyToPack']){
                              $style = 'background-color: rgb(70, 190, 138);color:white;';
//                            $class = 'success';
                        }elseif(@$element['isDangerous']) {
                            $style = 'background-color:#f96868;color:white;';
                        }else{
                            $style = '';
                        }
                        ?>
                        <tr class="" style="<?= $style?>">
    <!--                        <td>--><?//=$element['row']?><!--</td>-->
    <!--                        <td>--><?//= $generator->getBarcode($element['barcode'], $generator::TYPE_CODE_128, 1) ?><!--</td>-->
                            <td><?=$element['car']?></td>
                            <td style="white-space:nowrap">
                                <?=$element['article']?>
                                <?php if ($element['isCustom']):?>
                                    <br>
                                    <?= Html::tag('i', null, [
                                        'class'       => 'fa fa-exclamation-circle',
                                        'data-toggle' => 'tooltip',
                                        'data-title'  => Yii::t('yii', 'Индивидуальный заказ'),
                                    ]) ?>
                                <?php endif;?>
                            </td>
                            <td><?=$element['title']?></td>
                            <td class="text-nowrap">
                                <?=$element['upn']?>
                                <?php if (!empty($element['naryadIsReady'])) :?>
                                    <i class="icon wb-check" title="<?=Yii::t('yii', 'Наряд готов')?>" data-toggle="tooltip"></i>
                                <?php elseif (in_array(Yii::$app->user->getId(),[21,48])): ?>
                                    <?= Html::a('<i class="icon wb-check"></i>', ['make-upn-ready', 'upn' => $element['upn']], [
                                        'class'          => 'btn btn-primary btn-xs',
                                        'title'          => Yii::t('yii', 'Назначить наряду статус "Готов"'),
                                        'data-toggle'    => 'tooltip',
                                        'data-placement' => 'top',
                                    ]) ?>
                                <?php endif; ?>
                            </td>
                            <td><?=$element['location']?></td>
                            <td>
                                <?php if ($element['trackCode']): ?>
                                    <?= Yii::t('yii', 'Трек-код: ') . $element['trackCode'] ?><br>
                                <?php endif; ?>
                                <?php if ($element['shipmentDate']): ?>
                                    <?= Yii::t('yii', 'Дата: ') . date('d.m.Y', $element['shipmentDate']) ?>
                                <?php endif; ?>
                            </td>
                                <td>
                                    <?php if (in_array(Yii::$app->user->getId(), [21,48,120,36])) :?>
                                        <?php if (!$element['isDeleted']) :?>
                                            <?php if (in_array(Yii::$app->user->getId(), [21,48,120,36])) :?>
                                            <?= Html::button('<i class="fa fa-minus-circle" data-toggle="tooltip" data-title="' . ($element['canBeDeleted'] ? Yii::t('yii', 'Удалить позицию') : Yii::t('yii', 'Данная позиция не может быть удалена')) . '"></i>', [
                                                'class' => 'btn btn-xs btn-outline btn-round btn-default',
                                                'data-id'        => $element['id'],
                                                'disabled' => !$element['canBeDeleted'],
                                                'onclick'        => '
                                                    var button = $(this);
                                                    var id = $(this).data("id");
                                                    var handle_function = function () {
                                                        button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:12px;\"></i>");
                                                        $.ajax({
                                                            url: "/logistics/implementation-monitor/default/ajax-delete-order-entry",
                                                            dataType: "json",
                                                            data: {
                                                                "id": id
                                                            },
                                                            success: function (data) {
                                                                $.pjax.reload({container : "#myPjax",  timeout: false});
                                                                if (!data.status) {
                                                                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                                    return;
                                                                }
                                                                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                            },
                                                            error: function() {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                                            }
                                                        });
                                                    };
                                                    notie.confirm("' . Yii::t('yii', 'Удалить позицию?') . '", "' . Yii::t('yii', 'Да') . '", "' . Yii::t('yii', 'Нет') . '", handle_function);
                                                ',
                                            ]) ?>
                                            <?php endif;?>
                                        <?php else: ?>
                                            <?php if (in_array(Yii::$app->user->getId(), [48,21,36])) :?>
                                                <?= Html::button('<i class="fa fa-undo" data-toggle="tooltip" data-title="' . ($element['canBeRestored'] ? Yii::t('yii', 'Восстановить позицию') : Yii::t('yii', 'Данная позиция не может быть восстановлена')) . '"></i>', [
                                                'class' => 'btn btn-xs btn-outline btn-round btn-default',
                                                'data-id'        => $element['id'],
                                                'disabled' => !$element['canBeRestored'],
                                                'onclick'        => '
                                                    var button = $(this);
                                                    var id = $(this).data("id");
                                                    var handle_function = function () {
                                                        button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:12px;\"></i>");
                                                        $.ajax({
                                                            url: "/logistics/implementation-monitor/default/ajax-restore-order-entry",
                                                            dataType: "json",
                                                            data: {
                                                                "id": id
                                                            },
                                                            success: function (data) {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                if (!data.status) {
                                                                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                                    return;
                                                                }
                                                                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                            },
                                                            error: function() {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                                            }
                                                        });
                                                    };
                                                    notie.confirm("' . Yii::t('yii', 'Восстановить позицию?') . '", "' . Yii::t('yii', 'Да') . '", "' . Yii::t('yii', 'Нет') . '", handle_function);
                                                ',

                                            ]) ?>
                                            <?php endif;?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if (in_array(Yii::$app->user->getId(), [21,48])) :?>
                                        <?php if (!$element['readyToPack']) :?>
                                            <?= Html::button('<i class="fas fa-asterisk" data-toggle="tooltip" data-title="' . ($element['canBeDeleted'] ? Yii::t('yii', 'Зарезервировать') : Yii::t('yii', 'Данная позиция не может быть удалена')) . '"></i>', [
                                                'class' => 'btn btn-xs btn-outline btn-round btn-default',
                                                'data-id'        => $element['id'],
                                                'disabled' => !$element['canBeDeleted'],
                                                'onclick'        => '
                                                    var button = $(this);
                                                    var id = $(this).data("id");
                                                    var handle_function = function () {
                                                        button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:12px;\"></i>");
                                                        $.ajax({
                                                            url: "/logistics/implementation-monitor/default/ajax-reserve-order-entry",
                                                            dataType: "json",
                                                            data: {
                                                                "id": id
                                                            },
                                                            success: function (data) {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                if (!data.status) {
                                                                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                                    return;
                                                                }else if (data.status == "success") {
                                                                    notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                                }else if (data.status == "error") {
                                                                    notie.alert(3, data.message ? data.message : "Выполнено безуспешно", 1);
                                                                };
                                                                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                            },
                                                            error: function() {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                                            }
                                                        });
                                                    };
                                                    notie.confirm("' . Yii::t('yii', 'Вы уверены что хотите зарезервировать данную позицию?') . '", "' . Yii::t('yii', 'Да') . '", "' . Yii::t('yii', 'Нет') . '", handle_function);
                                                ',
                                            ]) ?>
                                        <?php else: ?>
                                            <?= Html::button('<i class="fa fa-eraser" data-toggle="tooltip" data-title="' . ($element['canBeRestored'] ? Yii::t('yii', 'Убрать с резерва') : Yii::t('yii', 'Данная позиция не может быть восстановлена')) . '"></i>', [
                                                'class' => 'btn btn-xs btn-outline btn-round btn-default',
                                                'data-id'        => $element['id'],
                                                'disabled' => !$element['canBeRestored'],
                                                'onclick'        => '
                                                    var button = $(this);
                                                    var id = $(this).data("id");
                                                    var handle_function = function () {
                                                        button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:12px;\"></i>");
                                                        $.ajax({
                                                            url: "/logistics/implementation-monitor/default/ajax-un-reserve-order-entry",
                                                            dataType: "json",
                                                            data: {
                                                                "id": id
                                                            },
                                                            success: function (data) {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                if (!data.status) {
                                                                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                                    return;
                                                                }else if (data.status == "success") {
                                                                    notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                                }else if (data.status == "error") {
                                                                    notie.alert(3, data.message ? data.message : "Выполнено безуспешно", 1);
                                                                };
                                                                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                            },
                                                            error: function() {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                                            }
                                                        });
                                                    };
                                                    notie.confirm("' . Yii::t('yii', 'Вы уверены что хотите снять с резерва данную позицию?') . '", "' . Yii::t('yii', 'Да') . '", "' . Yii::t('yii', 'Нет') . '", handle_function);
                                                ',

                                            ]) ?>
                                        <?php endif; ?>
                                        <?php if (!$element['upn']) :?>
                                                <?= Html::button('<i class="fa fa-plus-square" data-toggle="tooltip" data-title="' . ($element['canBeDeleted'] ? Yii::t('yii', 'Создать заказ и upn') : Yii::t('yii', 'Данная позиция не может быть удалена')) . '"></i>', [
                                                'class' => 'btn btn-xs btn-outline btn-round btn-default',
                                                'data-id'        => $element['id'],
                                                'disabled' => !$element['canBeDeleted'],
                                                'onclick'        => '
                                                    var button = $(this);
                                                    var id = $(this).data("id");
                                                    var handle_function = function () {
                                                        button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:12px;\"></i>");
                                                        $.ajax({
                                                            url: "/logistics/implementation-monitor/default/ajax-create-work-order",
                                                            dataType: "json",
                                                            data: {
                                                                "id": id
                                                            },
                                                            success: function (data) {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                if (!data.status) {
                                                                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                                    return;
                                                                }else if (data.status == "success") {
                                                                    notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                                }else if (data.status == "error") {
                                                                    notie.alert(3, data.message ? data.message : "Выполнено безуспешно", 1);
                                                                };
                                                                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                            },
                                                            error: function() {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                                            }
                                                        });
                                                    };
                                                    notie.confirm("' . Yii::t('yii', 'Вы уверены что хотите создать наряд и UPN для данной позиции?') . '", "' . Yii::t('yii', 'Да') . '", "' . Yii::t('yii', 'Нет') . '", handle_function);
                                                ',
                                            ]) ?>
                                        <?php else: ?>
                                            <?= Html::button('<i class="fa fa-trash" data-toggle="tooltip" data-title="' . ($element['canBeRestored'] ? Yii::t('yii', 'Удалить upn и заказ') : Yii::t('yii', 'Данная позиция не может быть восстановлена')) . '"></i>', [
                                                'class' => 'btn btn-xs btn-outline btn-round btn-default',
                                                'data-id'        => $element['id'],
                                                'disabled' => !$element['canBeRestored'],
                                                'onclick'        => '
                                                    var button = $(this);
                                                    var id = $(this).data("id");
                                                    var handle_function = function () {
                                                        button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:12px;\"></i>");
                                                        $.ajax({
                                                            url: "/logistics/implementation-monitor/default/ajax-cancel-work-order",
                                                            dataType: "json",
                                                            data: {
                                                                "id": id
                                                            },
                                                            success: function (data) {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                if (!data.status) {
                                                                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                                    return;
                                                                }else if (data.status == "success") {
                                                                    notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                                }else if (data.status == "error") {
                                                                    notie.alert(3, data.message ? data.message : "Выполнено безуспешно", 1);
                                                                };
                                                                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                            },
                                                            error: function() {
                                                                $.pjax.reload({container : "#myPjax", timeout: false});
                                                                notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                                            }
                                                        });
                                                    };
                                                    notie.confirm("' . Yii::t('yii', 'Вы уверены что хотите отменить наряд и upn для данной позиции?') . '", "' . Yii::t('yii', 'Да') . '", "' . Yii::t('yii', 'Нет') . '", handle_function);
                                                ',

                                            ]) ?>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                </td>
                        </tr>
                    <?endforeach;?>
                    </tbody>
                </table>
            </div>
    </div>
    <div class="clearfix"></div>
</div>
