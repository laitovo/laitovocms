<?php
use yii\helpers\Url;

$this->registerCss(".table > thead > tr > th{ font-weight: 600 }");
?>
<table class="table table-striped">
    <thead class="some-bold">
    <tr>
        <th>Заявка №</th>
        <th>Артикул</th>
        <th>Наименование</th>
        <th>Upn</th>
        <th>МестоНахождение</th>
        <th>Упакован</th>
        <th>Отгружен</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td rowspan="<?=count($model['elements'])?>" ><?php echo "<span class='lead' style='color:red'> " . $model['sourceId'] . "</span><br>Вн. ном.: " . $model['orderId']?></td>
        <td "><span data-toggle="tooltip" data-placement="top" title="<?= $model['elements'][0]['car']?>"><?= @$model['elements'][0]['article'] ?></span></td>
        <td "><?= @$model['elements'][0]['title'] ?></td>
        <?if (!$model['elements'][0]['storage']) :?>
            <td class="<?= $model['elements'][0]['readyToPack'] ? 'success lead' : ''?>">
                <?= @$model['elements'][0]['upn'] ? str_replace((mb_substr(@$model['elements'][0]['upn'], 4, mb_strlen(@$model['elements'][0]['upn']))),
                    ('<b style="font-size: 1.4em">' . mb_substr(@$model['elements'][0]['upn'], 4, mb_strlen(@$model['elements'][0]['upn'])) . '</b>'), @$model['elements'][0]['upn']) : @$model['elements'][0]['upn'] ?>
            </td>
            <td class="<?= $model['elements'][0]['readyToPack'] ? 'success lead' : ''?>"><?= @$model['elements'][0]['location'] ?></td>
        <?else :?>
            <td colspan="2">Комплектуется со склада</td>
        <?endif;?>

        <td rowspan="3""><?=  Yii::$app->formatter->asDatetime($model['packDate'])?></td>
        <td rowspan="3""><?=  Yii::$app->formatter->asDatetime($model['factDate'])?></td>
    </tr>
    <?php foreach ($model['elements'] as $key => $element): ?>
        <?php if ($key !== 0): ?>
            <tr>
                <td "><span data-toggle="tooltip" data-placement="top" title="<?= $element['car']?>"><?= @$element['article'] ?></span></td>
                <td "><?= @$element['title'] ?></td>
                <?if (!@$element['storage']) :?>
                    <td class="<?= @$element['readyToPack'] ? 'success lead' : ''?>">
                        <?= @$element['upn'] ? str_replace((mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn']))),
                            ('<b style="font-size: 1.4em">' . mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn'])) . '</b>'), @$element['upn']) : @$element['upn'] ?>
                    </td>
                    <td class="<?= @$element['readyToPack'] ? 'success lead' : ''?>"><?= @$element['location'] ?></td>
                <?else :?>
                    <td colspan="2"><span class="text-danger">Комплектуется со склада</span></td>
                <?endif;?>
            </tr>
        <?php endif;?>
    <?php endforeach;?>
    </tbody>
</table>