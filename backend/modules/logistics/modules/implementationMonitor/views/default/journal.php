<?php
/* @var $provider \yii\data\ArrayDataProvider */
/* @var $onlyActual bool */
/* @var $autoRefresh bool */
/* @var $search string */

use backend\widgets\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');
//Вынести в ассеты модуля
$script = <<<SCRIPT
setInterval(function() {
    if ($("#checkbox-auto-refresh").prop("checked")) {
        $.pjax.reload({container : "#myPjax"});
    }
}, 360*1000);

$("#myPjax").on("pjax:end", function() {
  $("#myPjax [data-toggle=tooltip]").tooltip();
})
    
$('#myModal').on('shown.bs.modal', function () {
  $('#barcode-scaner').focus();
});

$('#myModal').on('hidden.bs.modal', function () {
  $.pjax.reload({container : '#myPjax'});
});

$('#myModal2').on('shown.bs.modal', function () {
  $('#ship-content').load('/logistics/implementation-monitor/default/ship-ready-orders');
});

$('#myModal2').on('hidden.bs.modal', function () {
  $('#ship-content').empty();
});

$('#myModal3').on('shown.bs.modal', function () {
  $('#barcode-scaner-2').focus();
});

$(document).on('click', '.load-order', function(){
        $('#order-content').load($(this).attr('href'));
        console.log($(this).attr('href'));
        $('#hidden-href').attr('href', $(this).attr('href') );
        $('#hidden-href').attr('data-order', $(this).attr('data-order') );
});
SCRIPT;
$this->registerJs($script);


$this->render('@backendViews/logistics/views/menu');

$this->title = Yii::t('app', 'Журнал заказов');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .checkbox-custom label::before {
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
</style>

<?php if (in_array(Yii::$app->user->getId(), [21, 48])):?>

<!--<div class="row">-->
    <!--    <div class="col-md-3">-->
    <!--        --><?//= \yii\helpers\Html::button('Обработать одну поступившую заявку', [
    //            'class' => 'btn btn-info',
    //            'onclick' => '
    //                $.get( "'. Url::to(['handle']).'",function( data ) {
    //                  $( ".result" ).html( data );
    //                  alert( "Load was performed." );
    //                });
    //            '
    //        ]); ?>
    <!--    </div>-->
<!--</div> -->

<?php ActiveForm::begin([
    'id'      => 'handle-form',
    'action'  => ['handle-by-source-innumber'],
    'method'  => 'get',
    'options' => [
        'data-pjax' => true
    ]
]); ?>

<div class="row">
    <div class="col-md-3">
        <?= Html::input($type = 'text','sourceInnumber',$value = null, $options = ['class' => 'form-control', 'placeholder' => 'Внутренний номер источника заявки']); ?>
    </div>
    <div class="col-md-3">
        <?= Html::submitButton('Обработать', ['class' => 'btn btn-sm btn-info']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<hr>
<?php endif;?>

<?php Pjax::begin(['id' => 'myPjax','timeout' => 5000, 'enablePushState' => false]); ?>

<?php ActiveForm::begin([
    'id' => 'search-form',
    'action' => [''],
    'method' => 'get',
    'options' => [
        'data-pjax' => true
    ]
]); ?>

<div class="row">
    <div class="form-group col-md-2">
        <?= Html::tag('div',
            Html::checkbox("implementationMonitor_journal_autoRefresh", $autoRefresh, ['id' => 'checkbox-auto-refresh'])
            .Html::tag('label', Yii::t('app', 'Автообновление'), ['for' => 'checkbox-auto-refresh']),
            [
                'class' => 'checkbox-custom checkbox-primary text-left',
                'onchange' => '$("#search-form").submit();'
            ]
        ) ?>
        <?= Html::hiddenInput('update_implementationMonitor_journal_autoRefresh',true); ?>
    </div>
    <div class="col-md-3">
        <?= Html::tag('div',
            Html::checkbox("implementationMonitor_journal_onlyActual", $onlyActual, ['id' => 'checkbox-only-actual'])
            .Html::tag('label', Yii::t('app', 'Только актуальные заказы'), ['for' => 'checkbox-only-actual']),
            [
                'class' => 'checkbox-custom checkbox-primary text-left',
                'onchange' => '$("#search-form").submit();'
            ]
        ) ?>
        <?= Html::hiddenInput('update_implementationMonitor_journal_onlyActual',true); ?>
    </div>
<!--    --><?php //if ($hasDangerousOrders): ?>
<!--        <div class="col-md-2">-->
<!--            --><?//= Html::tag('div',
//                Html::checkbox("onlyDangerous", $onlyDangerous, ['id' => 'checkbox-only-dangerous'])
//                .Html::tag('label', Yii::t('app', 'Внимание,отсутствуют остатки на складе!'), [
//                    'class' => 'text-danger text-nowrap',
//                    'style' => 'font-size:1.3em;font-weight:bold;',
//                    'for' => 'checkbox-only-dangerous',
//                    'data-toggle' => 'tooltip',
//                    'data-title' => Yii::t('app', 'Заказы, на которые стоит обратить внимание, так как они не могут быть исполнены из за отсутствия остатков на складе')
//                ]),
//                [
//                    'class' => 'checkbox-custom checkbox-primary text-left',
//                    'onchange' => '$("#search-form").submit();'
//                ]
//            ) ?>
<!--        </div>-->
<!--    --><?php //endif; ?>
    <?php if (!empty($dangerOrders) && Yii::$app->user->getId() == 21): ?>
        <div class="col-md-2">
            Внимание! Данные заказы с ошибками : <?= implode(', ',$dangerOrders);?>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-md-2">
        <?= Html::button(Yii::t('app', 'Обновить'), [
            'id' => 'button-refresh',
            'class' => 'btn btn-outline btn-round btn-primary',
            'onclick' => '$.pjax.reload({container : "#myPjax"});'
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::input($type = 'text','implementationMonitor_journal_search',$value = $search, $options = ['class' => 'form-control', 'id' => 'search-textbox', 'placeholder' => 'Поиск']); ?>
        <?= Html::hiddenInput('update_implementationMonitor_journal_search',true); ?>
    </div>
    <div class="col-md-3">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::button('Очистить', ['class' => 'btn btn-sm btn-default', 'onclick' => '
            $("#search-textbox").val("");
            $("#search-form").submit();
        ']) ?>
    </div>
    <div class="col-md-3">
        Обозначения:
        <div style="display:inline-block;width:20px;height:20px;background-color: rgb(70, 190, 138)" title="Находится на складе" data-toggle="tooltip"></div>
        <div style="display:inline-block;width:20px;height:20px;background-color: #6666FF" title="Ожидается упаковка" data-toggle="tooltip"></div>
        <div style="display:inline-block;width:20px;height:20px;background-color: #660066" title="Упакован, ожидается отгрузка" data-toggle="tooltip"></div>
        <div style="display:inline-block;width:20px;height:20px;background-color: #33CCFF" title="Отгружен" data-toggle="tooltip"></div>
        <div style="display:inline-block;width:20px;height:20px;background-color:#f96868;" title="Заказ не может быть выполнен из-за отсутствия остатков на складе" data-toggle="tooltip"></div>
    </div>
</div>
<br>

<?php ActiveForm::end(); ?>

<?php //\yii\widgets\Pjax::begin(['id' => 'myPjax','timeout' => 5000]); ?>

<?=
ListView::widget([
    'dataProvider' => $provider,
    'itemView' => '_journal_order',
])
?>

<?php Pjax::end(); ?>


<?
Modal::begin([
    'header' => '<h2>Hello world</h2>',
    'options' => [
        'id' => 'myModal',
    ],
    'size' => 'modal-lg'
]);

//Подгрузить представление для получения заявок

echo Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control','id' => 'barcode-scaner',
    'onchange' => '
        $.get( "'. Url::to(['search']) .'", {order: $(\'#hidden-href\').attr(\'data-order\'), barcode: $(this).val()}, function( data ) {
          alert( "Load was performed." );
          $(\'#order-content\').load( $(\'#hidden-href\').attr(\'href\'));
        });
    ']);
echo Html::a('', $url = '', $options = ['class' => 'hidden' , 'id' => 'hidden-href']);

echo Html::tag('div','',['id' => 'order-content']);

Modal::end();?>
