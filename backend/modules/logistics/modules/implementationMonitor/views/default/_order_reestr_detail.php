<?php
use yii\helpers\Url;

$this->registerCss("
    .table > thead > tr > th{ font-weight: 600 }
");
?>

<hr>
<h3>Реестр заявки</h3>
<hr>
<?php foreach ($model['elements'] as $key => $element): ?>
    <?= @$element['article'] ?>
    <br>
     <?if (!@$element['storage']) :?>
    <?= @$element['upn'] ? str_replace((mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn']))),
        ('<b style="font-size: 1.4em">' . mb_substr(@$element['upn'], 4, mb_strlen(@$element['upn'])) . '</b>'), @$element['upn']) : @$element['upn'] ?>
         <br>
    <?= @$element['location'] ?>
         <br>
     <?else :?>
         <td rowspan="2">Комплектуется со склада</td>
     <?endif;?>
    <hr>
</div>

<?php endforeach;?>
