<?php

use backend\modules\logistics\models\Order;
use common\models\laitovo\ErpNaryad;
use common\models\logistics\Storage;
use core\models\shipment\Shipment;
use core\services\SUpn;
use yii\helpers\Html;

/** @var $model Order */

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>
<hr>
<div style="font-size: 0.84em">
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center">№ Заказа</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div onclick = '$("#order-<?= $model->id ?>").slideToggle( "slow", function() {});'>
                        <?= $model->source_innumber ? str_replace((mb_substr($model->source_innumber, 4, mb_strlen($model->source_innumber))),
                            ('<b style="font-size: 1.4em;font-weight: bold; color: red;">' . mb_substr($model->source_innumber, 4, mb_strlen($model->source_innumber)) . '</b>'), $model->source_innumber) : $model->source_innumber ?>
                        <br>
                        <span class="text-nowrap">от <?= date('d.m.Y',$model->created_at) ?></span>
                        <?php if ($model->is_need_ttn): ?>
                            <strong class="text-nowrap" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Обязательно приложить ТТН к заказу')?>"><?=Yii::t('app', '(с ТТН)')?></strong>
                        <?php endif;?>
                        <?php if ($model->isCOD): ?>
                            <strong class="text-nowrap text-danger" style="font-size:1.5em;font-weight:bold;" data-toggle="tooltip" title="<?=Yii::t('app', 'Оплата заказа производится наложенным платежем')?>"><?=Yii::t('app', '(Наложка)')?></strong>
                        <?php endif;?>
                    </div>
                    <?= Html::a('<i class="fa fa-eye"></i>', ['/logistics/logistics-monitor/default/view-order', 'orderId' => $model->id], [
                        'target' => '_blank',
                        'class' => 'btn btn-xs btn-icon btn-outline btn-round btn-primary',
                        'style' => 'margin-top:3px;',
                        'data-pjax' => '',
                        'data-toggle' => 'tooltip',
                        'data-original-title' => Yii::t('app', 'Просмотр заказа')]); ?>
                    <?php if(Yii::$app->user->getId() == 21):?>
                    <hr>
                    <?= Html::a('<i class="fa fa-times-circle"></i>', ['/logistics/implementation-monitor/default/delete-order', 'id' => $model->id], [
                        'target' => '_blank',
                        'class' => 'btn btn-xs btn-icon btn-outline btn-round btn-danger',
                        'style' => 'margin-top:3px;',
                        'data-pjax' => '',
                        'data-toggle' => 'tooltip',
                        'data-original-title' => Yii::t('app', 'Отменить и удалить заказ из системы'),
                        'data' => [
                            'confirm' => Yii::t('yii', 'Вы действительно хотите УДАЛИТЬ заказ (Наряды на производство при этом только ОТВЯЗЫВАЮТСЯ от заказа, НО НЕ УДАЛЯЮТСЯ!!!) ?'),
                            'method' => 'post',
                        ],
                    ]); ?>
                     |
                    <?php endif;?>
                    <?php if(in_array(Yii::$app->user->getId(), [21,36])):?>
                    <?= Html::a('<i class="fa fa-times-circle"></i>', ['/logistics/implementation-monitor/default/cancel-order', 'id' => $model->id], [
                        'target' => '_blank',
                        'class' => 'btn btn-xs btn-icon btn-outline btn-round btn-warning',
                        'style' => 'margin-top:3px;',
                        'data-pjax' => '',
                        'data-toggle' => 'tooltip',
                        'data-original-title' => Yii::t('app', 'Отменить без удаления'),
                        'data' => [
                            'confirm' => Yii::t('yii', 'Вы действительно хотите ОТМЕНИТЬ заказ ?'),
                            'method' => 'post',
                        ],
                    ]); ?>
                    <?php endif;?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-1 text-center">
        <table class="table">
            <thead>
            <th class="text-center">Клиент</th>
            </thead>
            <tbody>
                <tr>
                    <td onclick = '$("#search-textbox").val( "<?= $model->username?>");$("#search-form").submit();' style="cursor: pointer"><?= $model->username?> <?= $model->export_username ? '(' . $model->export_username .  ')' : ''?> </td>
                </tr>
                <tr>
                    <td onclick = '$("#search-textbox").val( "<?= $model->category?>");$("#search-form").submit();' style="cursor: pointer"><?= $model->category?> <?= $model->export_type ? '(' . $model->export_type .  ')' : ''?> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="col-md-10">
            <div class="col-md-12" style="display: none" id="order-<?=@$model->id?>">
                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <thead>
                                <th colspan="2">Общая информация</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Заказ №</td>
                                    <td><?= $model->source_innumber?></td>
                                </tr>
                                <tr>
                                    <td>Менеджер&nbsp;:</td>
                                    <td><?= $model->manager?></td>
                                </tr>
                                <tr>
                                    <td>Способ доставки&nbsp;:</td>
                                    <td><?= $model->delivery?></td>
                                </tr>
                                <tr>
                                    <td>Адрес доставки&nbsp;:</td>
                                    <td><?= $model->address?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <thead>
                                <th colspan="2">Информация о клиенте</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td>ФИО&nbsp:</td>
                                <td><?= $model->username?></td>
                            </tr>
                            <tr>
                                <td>Телефон&nbsp:</td>
                                <td><?= $model->userphone?></td>
                            </tr>
                            <tr>
                                <td>Email&nbsp:</td>
                                <td><?= $model->useremail?></td>
                            </tr>
                            <tr>
                                <td>Категория&nbsp:</td>
                                <td><?= $model->category?> <?= $model->export_type ? '(' . $model->export_type .  ')' : ''?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <table class="table table-hover table-default">
                    <thead>
                        <th>Автомобиль</th>
                        <th>Артикул</th>
                        <th>Наименование</th>
                        <th>UPN</th>
                        <th>Местонахождение</th>
                        <th class="text-nowrap">Отгрузка</th>
                        <?php if (in_array(Yii::$app->user->getId(), [21,48,36])) :?>
                            <th>Действия</th>
                        <?php endif; ?>
                    </thead>
                    <tbody>
                    <?php foreach ($model->orderEntries as $element):?>
                        <?php
                            $shipment = $element->shipment;
                            $upn = $element->upn;
                            $erpNaryad = $upn ? $upn->erpNaryad : null;
                            $state = $upn ? $upn->storageState : null;
                            $storage = $state ? $state->storage : null;

                            $readyToPack = $storage && $storage->type != Storage::TYPE_SUMP;
                            $isDangerous = !$element->is_deleted && (!$upn || (!$erpNaryad && !$state));
                            $naryadIsReady = $erpNaryad && $erpNaryad->status == ErpNaryad::STATUS_READY;
                            $location = SUpn::getLocation($upn, $erpNaryad, $state, $storage, $shipment);
                            $trackCode = $shipment ? $shipment->trackCode : null;
                            $shipmentDate = $shipment ? $shipment->shipmentDate : null;
                            $canBeDeleted =  !$element->is_deleted && (!$shipment || $shipment->isBeforeHandling());
                            $canBeRestored =  $element->is_deleted && (!$shipment || $shipment->isBeforeHandling());
                        ?>


                        <?php if ($element->is_deleted) :?>
                            <tr><td colspan="6"><div style="position:relative;top:30px;height:3px;background:red;" data-toggle="tooltip" data-title="<?=Yii::t('yii', 'Позиция удалена (' . date('d.m.Y', $element->deleted_at)) . ', ' . $element->deleted_by . ')'?>"></div></td></tr>
                        <?php endif; ?>
                        <? if ($shipment && $shipment->logisticsStatus == Shipment::LOGISTICS_STATUS_SHIPPED) {
                              $style = 'background-color: #33CCFF;color:white;';
                        }elseif($shipment && $shipment->logisticsStatus == Shipment::LOGISTICS_STATUS_PACKED) {
                              $style = 'background-color: #660066;color:white;';
                        }elseif($shipment && $shipment->logisticsStatus == Shipment::LOGISTICS_STATUS_HANDLED) {
                              $style = 'background-color: #6666FF;color:white;';
                        }elseif($readyToPack){
                              $style = 'background-color: rgb(70, 190, 138);color:white;';
                        }elseif($isDangerous) {
                            $style = 'background-color:#f96868;color:white;';
                        }else{
                            $style = '';
                        }
                        ?>
                        <tr class="" style="<?= $style?>">
    <!--                        <td>--><?//=$element['row']?><!--</td>-->
    <!--                        <td>--><?//= $generator->getBarcode($element['barcode'], $generator::TYPE_CODE_128, 1) ?><!--</td>-->
                            <td><?=$element->car?></td>
                            <td style="white-space:nowrap">
                                <?=$element->article?>
                                <?php if ($element->is_custom):?>
                                    <br>
                                    <?= Html::tag('i', null, [
                                        'class'       => 'fa fa-exclamation-circle',
                                        'data-toggle' => 'tooltip',
                                        'data-title'  => Yii::t('yii', 'Индивидуальный заказ'),
                                    ]) ?>
                                <?php endif;?>
                            </td>
                            <td><?= $element->name?></td>
                            <td class="text-nowrap">
                                <?= ($upn ? $upn->id : '') ?>
                                <?php if ($naryadIsReady) :?>
                                    <i class="icon wb-check" title="<?=Yii::t('yii', 'Наряд готов')?>" data-toggle="tooltip"></i>
                                <?php endif; ?>
                            </td>
                            <td><?= $location ?></td>
                            <td>
                                <?php if ($trackCode): ?>
                                    <?= Yii::t('yii', 'Трек-код: ') . $trackCode ?><br>
                                <?php endif; ?>
                                <?php if ($shipmentDate): ?>
                                    <?= Yii::t('yii', 'Дата: ') . date('d.m.Y', $shipmentDate) ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (in_array(Yii::$app->user->getId(), [21,48,120,36])) :?>
                                    <?php if (!$element->is_deleted) :?>
                                        <?php if (in_array(Yii::$app->user->getId(), [21,48,120,36])) :?>
                                        <?= Html::button('<i class="fa fa-minus-circle" data-toggle="tooltip" data-title="' . ($canBeDeleted ? Yii::t('yii', 'Удалить позицию') : Yii::t('yii', 'Данная позиция не может быть удалена')) . '"></i>', [
                                            'class' => 'btn btn-xs btn-outline btn-round btn-default',
                                            'data-id'        => $element->id,
                                            'disabled' => !$canBeDeleted,
                                            'onclick'        => '
                                                var button = $(this);
                                                var id = $(this).data("id");
                                                var handle_function = function () {
                                                    button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:12px;\"></i>");
                                                    $.ajax({
                                                        url: "/logistics/implementation-monitor/default/ajax-delete-order-entry",
                                                        dataType: "json",
                                                        data: {
                                                            "id": id
                                                        },
                                                        success: function (data) {
                                                            $.pjax.reload({container : "#myPjax",  timeout: false});
                                                            if (!data.status) {
                                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                                return;
                                                            }
                                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                        },
                                                        error: function() {
                                                            $.pjax.reload({container : "#myPjax", timeout: false});
                                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                                        }
                                                    });
                                                };
                                                notie.confirm("' . Yii::t('yii', 'Удалить позицию?') . '", "' . Yii::t('yii', 'Да') . '", "' . Yii::t('yii', 'Нет') . '", handle_function);
                                            ',
                                        ]) ?>
                                        <?php endif;?>
                                    <?php else: ?>
                                        <?php if (in_array(Yii::$app->user->getId(), [48,21,36])) :?>
                                            <?= Html::button('<i class="fa fa-undo" data-toggle="tooltip" data-title="' . ($canBeRestored ? Yii::t('yii', 'Восстановить позицию') : Yii::t('yii', 'Данная позиция не может быть восстановлена')) . '"></i>', [
                                            'class' => 'btn btn-xs btn-outline btn-round btn-default',
                                            'data-id'  => $element->id,
                                            'disabled' => !$canBeRestored,
                                            'onclick'        => '
                                                var button = $(this);
                                                var id = $(this).data("id");
                                                var handle_function = function () {
                                                    button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:12px;\"></i>");
                                                    $.ajax({
                                                        url: "/logistics/implementation-monitor/default/ajax-restore-order-entry",
                                                        dataType: "json",
                                                        data: {
                                                            "id": id
                                                        },
                                                        success: function (data) {
                                                            $.pjax.reload({container : "#myPjax", timeout: false});
                                                            if (!data.status) {
                                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                                return;
                                                            }
                                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                                        },
                                                        error: function() {
                                                            $.pjax.reload({container : "#myPjax", timeout: false});
                                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                                        }
                                                    });
                                                };
                                                notie.confirm("' . Yii::t('yii', 'Восстановить позицию?') . '", "' . Yii::t('yii', 'Да') . '", "' . Yii::t('yii', 'Нет') . '", handle_function);
                                            ',

                                        ]) ?>
                                        <?php endif;?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
    </div>
    <div class="clearfix"></div>
</div>
