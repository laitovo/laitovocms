<?php


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $onlyActual bool */
/* @var $autoRefresh bool */
/* @var $search string */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->render('@backendViews/logistics/views/menu');

$script = <<<SCRIPT
setInterval(function() {
    if ($("#checkbox-auto-refresh").prop("checked")) {
        $.pjax.reload({container : "#myPjax", timeout: 10000});
    }
}, 300*1000);

$("#myPjax").on("pjax:end", function() {
  $("#myPjax [data-toggle=tooltip]").tooltip();
})
SCRIPT;
$this->registerJs($script);

?>
<div class="journal-index">

    <?php Pjax::begin(['id' => 'myPjax', 'timeout' => 10000, 'enablePushState' => true]); ?>

    <h1><?= Html::encode('Логистические заказы') ?></h1>

    <?php ActiveForm::begin([
        'id' => 'search-form',
        'action' => array_merge([''],Yii::$app->request->get()),
        'method' => 'post',
        'options' => [
            'data-pjax' => true
        ]
    ]); ?>

    <div class="row">
        <div class="form-group col-md-2">
            <?= Html::tag('div',
                Html::checkbox("implementationMonitor_new_journal_autoRefresh", $autoRefresh, ['id' => 'checkbox-auto-refresh'])
                .Html::tag('label', Yii::t('app', 'Автообновление'), ['for' => 'checkbox-auto-refresh']),
                [
                    'class' => 'checkbox-custom checkbox-primary text-left',
                    'onchange' => '$("#search-form").submit();'
                ]
            ) ?>
            <?= Html::hiddenInput('update_implementationMonitor_new_journal_autoRefresh',true); ?>
        </div>
        <div class="col-md-3">
            <?= Html::tag('div',
                Html::checkbox("implementationMonitor_new_journal_onlyActual", $onlyActual, ['id' => 'checkbox-only-actual'])
                .Html::tag('label', Yii::t('app', 'Только актуальные заказы'), ['for' => 'checkbox-only-actual']),
                [
                    'class' => 'checkbox-custom checkbox-primary text-left',
                    'onchange' => '$("#search-form").submit();'
                ]
            ) ?>
            <?= Html::hiddenInput('update_implementationMonitor_new_journal_onlyActual',true); ?>
        </div>
        <!--    --><?php //if ($hasDangerousOrders): ?>
        <!--        <div class="col-md-2">-->
        <!--            --><?//= Html::tag('div',
        //                Html::checkbox("onlyDangerous", $onlyDangerous, ['id' => 'checkbox-only-dangerous'])
        //                .Html::tag('label', Yii::t('app', 'Внимание,отсутствуют остатки на складе!'), [
        //                    'class' => 'text-danger text-nowrap',
        //                    'style' => 'font-size:1.3em;font-weight:bold;',
        //                    'for' => 'checkbox-only-dangerous',
        //                    'data-toggle' => 'tooltip',
        //                    'data-title' => Yii::t('app', 'Заказы, на которые стоит обратить внимание, так как они не могут быть исполнены из за отсутствия остатков на складе')
        //                ]),
        //                [
        //                    'class' => 'checkbox-custom checkbox-primary text-left',
        //                    'onchange' => '$("#search-form").submit();'
        //                ]
        //            ) ?>
        <!--        </div>-->
        <!--    --><?php //endif; ?>
        <?php if (!empty($dangerOrders)): ?>
            <div class="col-md-2">
                Внимание! Данные заказы с ошибками : <?= implode(', ',$dangerOrders);?>
            </div>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= Html::button(Yii::t('app', 'Обновить'), [
                'id' => 'button-refresh',
                'class' => 'btn btn-outline btn-round btn-primary',
                'onclick' => '$.pjax.reload({container : "#myPjax"});'
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::input($type = 'text','implementationMonitor_new_journal_search',$value = $search, $options = ['class' => 'form-control', 'id' => 'search-textbox', 'placeholder' => 'Поиск']); ?>
            <?= Html::hiddenInput('update_implementationMonitor_new_journal_search',true); ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::button('Очистить', ['class' => 'btn btn-sm btn-default', 'onclick' => '
            $("#search-textbox").val("");
            $("#search-form").submit();
        ']) ?>
        </div>
        <div class="col-md-3">
            Обозначения:
            <div style="display:inline-block;width:20px;height:20px;background-color: rgb(70, 190, 138)" title="Находится на складе" data-toggle="tooltip"></div>
            <div style="display:inline-block;width:20px;height:20px;background-color: #6666FF" title="Ожидается упаковка" data-toggle="tooltip"></div>
            <div style="display:inline-block;width:20px;height:20px;background-color: #660066" title="Упакован, ожидается отгрузка" data-toggle="tooltip"></div>
            <div style="display:inline-block;width:20px;height:20px;background-color: #33CCFF" title="Отгружен" data-toggle="tooltip"></div>
            <div style="display:inline-block;width:20px;height:20px;background-color:#f96868;" title="Заказ не может быть выполнен из-за отсутствия остатков на складе" data-toggle="tooltip"></div>
        </div>
    </div>
    <br>

    <?php ActiveForm::end(); ?>

        <?php
            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_journal_order',
                'layout' => "{summary}\n{pager}\n{items}\n{pager}",
            ]);
        ?>
    <?php Pjax::end(); ?>
</div>
