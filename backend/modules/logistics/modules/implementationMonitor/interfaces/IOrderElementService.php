<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IOrderElementService
{
    /**
     * Принимает в качестве значения объект элемента заказа и возвращает артикула для данного элемента
     * @param $element
     * @return mixed
     */
    public function getArticle($element);

    /**
     * Принимает в качестве значения объект элемента заказа и возвращает наименоване для данного элемента
     * @param $element
     * @return mixed
     */
    public function getTitle($element);

    /**
     * Принимает в качестве значения объект элемента заказа и возвращает наименоване для данного элемента
     * @param $element
     * @return mixed
     */
    public function getItemText($element);


    /**
     * Принимает в качестве значения объект элемента заказа и возвращает наименоване для данного элемента
     * @param $element
     * @return mixed
     */
    public function getInvoiceTitle($element);

    /**
     * Получить зарезервированный upn
     * @param $element
     * @return mixed
     */
    public function getReservedUpn($element);

    /**
     * Получить информацию - складская ли это позиция или нет
     * @param $element
     * @return mixed
     */
    public function isStoragePosition($element);


    /**
     * Фукнция должна вернуть значени
     *
     * @param $element
     * @return mixed
     */
    public function getCar($element);


    /**
     * Функция, которая возвращает штрихкод для элемента
     *
     * @param $element
     * @return mixed
     */
    public function getSourceBarcode($element);

    /**
     * Функция, которая возвращает номер строки заказа
     *
     * @param $element
     * @return mixed
     */
    public function getOrderRow($element);
}