<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IUpnRepository
{
    /**
     * Должен возвращать обект зарезервированного наряда для элемента заказа
     * @param $element
     * @return mixed
     */
    public function getUpnForElement($element);


    /**
     * Фукцния должна возвращать объект наряда по переданному штрихкоду
     * @param $barcode
     * @return mixed
     */
    public function getUpnByBarcode($barcode);
}