<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IOrdersRepository
{
    /**
     * Вернуть массив входящих заявок
     * @return mixed
     */
    public function getIncomingOrders();

    /**
     * Вернуть массив упакованных заявок
     * @return mixed
     */
    public function getPackageOrders();

    /**
     * Вернуть массив упакованных заявок
     * @return mixed
     */
    public function getPackageReestrOrders();

    /**
     * Вернуть массив готовых к отгрузке заявок
     * @return mixed
     */
    public function getReadyToShipmentOrders();

    /**
     * Найти и вернуть заявку, которую необходимо обработать
     *
     * @param $id int - Уникальный идентификатор заявки
     * @return mixed object|null $order
     */
    public function getLastOrder();

    /**
     * Функция для получения заказа по уникальному идентификатору
     * @param $id int
     * @return mixed
     */
    public function getOrderById($id);

    /**
     * Возвращает все заявки которые нобходмо отслеживать в мониторе
     * @return mixed
     */
    public function getOrders();

    /**
     * Возвращает все заявки которые нобходмо отслеживать в мониторе
     * @param $searchParams array
     * @return mixed
     */
    public function getOrdersJournal($searchParams);

    public function getOrdersJournalDangerous();

    /**
     * Возвращает все заявки которые нобходмо отслеживать в мониторе
     * @return mixed
     */
    public function getOrdersStorage();

    /**
     * Возвращает все заявки которые нобходмо отслеживать в мониторе
     * @return mixed
     */
    public function getOrdersShipmentCount();

    /**
     * Функция должа вернуть массив заявок, которые с таким то номером реестра
     * @param $id
     * @return mixed
     */
    public function getOrdersByReestrId($id);


    /**
     * Функция для получаения заявок, у которых есть реестр
     * @return mixed
     */
    public function getOrderWithReestr();




}