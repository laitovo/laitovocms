<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IOrderService
{
    /**
     * Вернуть номер заявки
     * @param $order
     * @return mixed
     */
    public function getNumber($order);

    /**
     * Вернуть номер заявки из источника
     * @param $order
     * @return mixed
     */
    public function getSourceNumber($order);

    /**
     * Вернуть дату планируемой отгрузки для заявки
     * @param $order
     * @return int (timestamp)
     */
    public function getPlanShipmentDate($order);

    /**
     * Вернуть дату полной упаковки заявки
     * @param $order
     * @return int (timestamp)
     */
    public function getPackageDate($order);

    /**
     * Вернуть дату фактической отгрузки заявки
     * @param $order
     * @return int (timestamp)
     */
    public function getFactShipmentDate($order);


    /**
     * Отметить заявку как обработанную
     * @param $order
     * @return mixed
     */
    public function checkAsProcessed($order);


    /**
     * Что делать с заявкой
     * @param $order
     * @return mixed
     */
    public function getCommand($order);

    /**
     * Заявка должна стать отгруженной
     * @param $order
     * @return mixed
     */
    public function shipOrder($order);

    /**
     * Вернуть идентификатор реестра отгрузки для данной заявки
     * @param $order
     * @return mixed
     */
    public function getRegistryId($order);

    /**
     * Вернуть менеджера, который обрабатывал заказ
     * @param $order
     * @return mixed
     */
    public function getManager($order);

    /**
     * Вернуть адрес доставки
     * @param $order
     * @return mixed
     */
    public function getAddress($order);

    /**
     * Вернуть ФИО клиента
     * @param $order
     * @return mixed
     */
    public function getClientName($order);

    /**
     * Вернуть ФИО клиента для экспорта
     * @param $order
     * @return mixed
     */
    public function getExportClientName($order);

    /**
     * Вернуть телефон клиента
     * @param $order
     * @return mixed
     */
    public function getClientPhone($order);

    /**
     * Вернуть email клиента
     * @param $order
     * @return mixed
     */
    public function getClientEmail($order);

    /**
     * Верунть категорию (ОПТ/ФИЗИК)
     * @param $order
     * @return mixed
     */
    public function getClientCategory($order);

    /**
     * Верунть категорию (ОПТ/ФИЗИК) для экспорта
     * @param $order
     * @return mixed
     */
    public function getExportClientCategory($order);

    /**
     * Вернуть способ доставки (ТП)
     * @param $order
     * @return mixed
     */
    public function getDelivery($order);

    /**
     * Узнать, упакована ли заявка
     * @param $order
     * @return bool
     */
    public function isOrderPacked($order);

    /**
     * Обработан ли заказ логистом
     *
     * @param $order
     * @return bool
     */
    public function isHandled($order);

    /**
     * Ожидает ли заказ взвешивания
     *
     * @param $order
     * @return bool
     */
    public function isOnWeighing($order);
}