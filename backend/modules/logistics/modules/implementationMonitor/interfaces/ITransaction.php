<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface ITransaction
{
    /**
     * Функция открывает транзакцию
     * @return mixed
     */
    public function begin();
    /**
     * Функция для сохранения транзакции
     * @return mixed
     */
    public function commit();

    /**
     * Функция которая должна откатить транзакцию
     * @return mixed
     */
    public function rollBack();
}