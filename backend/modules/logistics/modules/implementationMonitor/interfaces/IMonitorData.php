<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IMonitorData
{
    /**
     * Получить ArrayDataProvider со входящими заявками
     * @return mixed
     */
    public function getData();

    public function getDataStorage($params);

    public function getDataJournal($searchParams);

    public function getDataShipment();

    public function getDataNextShipment();

    public function getDataPackedForSheep();

    public function getDataCarriedOver();

}