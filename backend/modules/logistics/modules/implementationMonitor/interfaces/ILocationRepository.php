<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface ILocationRepository
{
    /**
     * Возвращает локацию, где находится upn
     * @return mixed
     */
    public function getLocationWhereUpn($upn);
}