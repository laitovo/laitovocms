<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IUpnService
{
    /**
     * Функция принимает в качестве параметра object UPN и возврщает номер для данного объекта
     * @param $upn
     * @return mixed
     */
    public function getNumber($upn);

    /**
     * Функция, которая возвращает уникальный идентификатор UPN в системе
     * @param object $upn
     * @return int
     */
    public function getId($upn);


    /**
     * Принимает в качестве значения объект элемента заказа и возвращает папу-UPN для данного элемента
     * @param $element
     * @return mixed
     */
    public function getParent($element);

    /**
     * Функция возвращает ответ на вопрос "Готов upn к упаковке."
     * @param $upn
     * @return bool
     */
    public function isReadyToPackage($upn);


    /**
     * Функция должна упаковать переданный upn и верунть статус - либо упакован либо нет.
     * @param object $upn
     * @return bool
     */
    public function pack($upn);


    public function isPacked($upn);


    public function getLocation($upn);

    public function getLocationStorage($upn);

    /**
     * Функция должна отгрузить со склада данный upn
     * @param $upn
     * @return mixed
     */
    public function shipUpn($upn);




}