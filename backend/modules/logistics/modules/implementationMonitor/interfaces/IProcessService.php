<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IProcessService
{
    /**
     * Функция должна обработать некую заявку и вернуть об успешном или неуспешном выполенении обработки заявки
     *
     * @param $id int
     * @return bool
     */
    public function handleOrder();

    /**
     * Функция должна обрабатывать конкретную заявку
     * @param $id
     * @return mixed
     */
    public function handleOrderById($id);


    public function shipReestr($barcode);
}