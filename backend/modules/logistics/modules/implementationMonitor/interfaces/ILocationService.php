<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface ILocationService
{
    /**
     * Принимает в качестве параметра объект локации и возвращает ее наименование
     * @param $location
     * @return mixed
     */
    public function getTitle($location);
}