<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IPackUpnService
{
    public function packForShipment($shipmentId, $barcode);
    public function packForBox($shipmentId, $boxId, $barcode);
}