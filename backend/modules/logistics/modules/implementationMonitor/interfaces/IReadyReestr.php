<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IReadyReestr
{
    //Фукцния должна возвращать провайдер данных со списком готовых к отгрузке заявок
    public function getData();

    public function getPrintData($ids);
}