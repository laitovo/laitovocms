<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IOrderElementsRepository
{
    /**
     * Вернуть коллекцию элементов заказа для конкретного закзаза
     * @param $order
     * @return mixed
     */
    public function getElementsForOrder($order);
}