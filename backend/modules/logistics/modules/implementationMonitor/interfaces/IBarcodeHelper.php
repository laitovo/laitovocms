<?php
namespace backend\modules\logistics\modules\implementationMonitor\interfaces;

interface IBarcodeHelper
{
    /**
     * Функция должна возвращать строку, преобразованную в латиницу согласно клавиатурного ввода.
     * @param string $barcode
     * @return string
     */
    public function toLatin(string $barcode) : string;
}