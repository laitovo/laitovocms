<?php

namespace backend\modules\logistics\modules\implementationMonitor\controllers;

use Yii;
use yii\web\Controller;
use backend\modules\logistics\modules\implementationMonitor\models\OrderSearch;

/**
 * Default controller for the `laitovo` module
 */
class JournalController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $search        = $this->_updateSession('implementationMonitor_new_journal_search', 'update_implementationMonitor_new_journal_search');
        $autoRefresh   = $this->_updateSession('implementationMonitor_new_journal_autoRefresh', 'update_implementationMonitor_new_journal_autoRefresh', true);
        $onlyActual    = $this->_updateSession('implementationMonitor_new_journal_onlyActual', 'update_implementationMonitor_new_journal_onlyActual', true);

        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search($search, $onlyActual);
        $sort = $dataProvider->getSort();
        $sort->defaultOrder = ['created_at' => ($onlyActual && !is_numeric($search) ? SORT_ASC : SORT_DESC)];
        $dataProvider->setSort($sort);
        $dataProvider->pagination->setPageSize(10);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
            'search'       => $search,
            'autoRefresh'  => $autoRefresh,
            'onlyActual'   => $onlyActual,
            'dangerOrders'=>  Yii::$app->user->getId() == 21 ? $searchModel->searchDangerous($search, $onlyActual) : null,
        ]);
    }

    /**
     * @param $name
     * @param $pageFlag
     * @param $defaultValue
     * @return array|mixed
     */
    private function _updateSession($name, $pageFlag, $defaultValue = false)
    {
        $session      = Yii::$app->session;
        $remember     = Yii::$app->request->get($pageFlag) ?: Yii::$app->request->post($pageFlag);
        $sessionValue = !$remember ? $session->get($name, $defaultValue) : (Yii::$app->request->get($name, false) ?: Yii::$app->request->post($name, false));
        $session->set($name, $sessionValue);

        return $sessionValue;
    }
}