<?php

namespace backend\modules\logistics\modules\implementationMonitor\controllers;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpOrder;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IMonitorData;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IPackUpnService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IProcessService;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IReadyReestr;
use backend\modules\logistics\modules\implementationMonitor\models\DangerList;
use backend\modules\logistics\modules\implementationMonitor\models\LowBalanceList;
use common\models\laitovo\ErpOrder as ErpOrderAlias;
use core\logic\OrderEntryReservation;
use core\logic\OrderEntryWorkOrder;
use core\models\box\Box;
use core\services\SRemoteStorage;
use yii\base\Module;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `laitovo` module
 */
class DefaultController extends Controller
{
    /** @var IMonitorData  */
    private $_monitorData;
    /** @var IProcessService  */
    private $_processService;
    /** @var IPackUpnService  */
    private $_packUpn;
    /** @var IReadyReestr  */
    private $_readyReestr;
    /** @var object  */
    private $_deleteOrderEntry;
    /** @var LowBalanceList  */
    private $_lowBalanceList;
    /** @var DangerList  */
    private $_dangerList;

    public function __construct(
        string $id,
        Module $module,
        IProcessService $processService,
        IPackUpnService $packUpnService,
        IMonitorData $monitorData,
        IReadyReestr $readyReestr,
        array $config = [])
    {
        $this->_processService = $processService;
        $this->_packUpn = $packUpnService;
        $this->_monitorData = $monitorData;
        $this->_readyReestr = $readyReestr;
        $this->_deleteOrderEntry = Yii::$container->get('core\logic\DeleteOrderEntry');
        $this->_lowBalanceList = new LowBalanceList();
        $this->_dangerList = new DangerList();

        parent::__construct($id, $module, $config);
    }

    public function actionDefault()
    {
        $filter       = Yii::$app->request->get('filter');
        $search       = $this->_updateSession('implementationMonitor_default_search', 'update_implementationMonitor_default_search');
        $autoRefresh  = $this->_updateSession('implementationMonitor_default_autoRefresh', 'update_implementationMonitor_default_autoRefresh', true);
        $provider     = $this->_monitorData->getDataStorage([
            'search'       => $search,
            'filter' => $filter,
        ]);
        $shipcount = $this->_monitorData->getDataShipment();
        $nextShipcount = $this->_monitorData->getDataNextShipment();
        $carriedCount = $this->_monitorData->getDataCarriedOver();
        $packedForShip = $this->_monitorData->getDataPackedForSheep();

        return $this->render('new/default', [
            'provider'                   => $provider,
            'hasPositionsWithLowBalance' => $this->_lowBalanceList->exists(),
            'hasDangerousOrders'         => null, //$this->_dangerList->exists(),
            'shipcount'                  => $shipcount,
            'carriedCount'               => $carriedCount,
            'packedForShip'              => $packedForShip,
            'nextShipcount'              => $nextShipcount,
            'search'                     => $search,
            'filter'                     => $filter,
            'autoRefresh'                => $autoRefresh
        ]);
    }

    public function actionDefault2()
    {
        ini_set('memory_limit', '256M');

        $filter       = Yii::$app->request->get('filter');
        $search       = $this->_updateSession('implementationMonitor_default_search', 'update_implementationMonitor_default_search');
        $autoRefresh  = $this->_updateSession('implementationMonitor_default_autoRefresh', 'update_implementationMonitor_default_autoRefresh', true);
        $provider     = $this->_monitorData->getDataStorage([
            'search'       => $search,
            'filter' => $filter,
        ]);
        $shipcount = $this->_monitorData->getDataShipment();
        $nextShipcount = $this->_monitorData->getDataNextShipment();
        $carriedCount = $this->_monitorData->getDataCarriedOver();
        $packedForShip = $this->_monitorData->getDataPackedForSheep();

        return $this->render('new/default2', [
            'provider'                   => $provider,
            'hasPositionsWithLowBalance' => $this->_lowBalanceList->exists(),
            'hasDangerousOrders'         => null, //$this->_dangerList->exists(),
            'shipcount'                  => $shipcount,
            'carriedCount'               => $carriedCount,
            'packedForShip'              => $packedForShip,
            'nextShipcount'              => $nextShipcount,
            'search'                     => $search,
            'filter'                     => $filter,
            'autoRefresh'                => $autoRefresh
        ]);
    }


    /**
     * Главная страница для контроля заказов менеджером.
     * На ней, по идее, минимум действий - в основном поиск и контроль (цвета, статусы, ...).
     *
     * @return Response
     */
    public function actionJournal(): Response
    {
        return $this->redirect('/logistics/implementation-monitor/journal/index');
//        $search        = $this->_updateSession('implementationMonitor_journal_search', 'update_implementationMonitor_journal_search');
//        $autoRefresh   = $this->_updateSession('implementationMonitor_journal_autoRefresh', 'update_implementationMonitor_journal_autoRefresh', true);
//        $onlyActual    = $this->_updateSession('implementationMonitor_journal_onlyActual', 'update_implementationMonitor_journal_onlyActual', true);
//        $onlyDangerous = Yii::$app->request->get('onlyDangerous');
//        $data          = $this->_monitorData->getDataJournal([
//            'search'        => $search,
//            'onlyActual'    => $onlyActual,
//            'onlyDangerous' => $onlyDangerous
//        ]);
//
//        return $this->render('journal', [
//            'provider'              => $data['provider'],
////            'hasDangerousOrders'    => $data['hasDangerousOrders'],
//            'dangerOrders'          => $data['dangerOrders'],
//            'search'                => $search,
//            'autoRefresh'           => $autoRefresh,
//            'onlyActual'            => $onlyActual,
//            'onlyDangerous'         => $onlyDangerous,
//        ]);
    }

    public function actionPack($shipmentId)
    {
        $this->_throwIfShipmentNotExists($shipmentId);
        $provider = $this->_monitorData->getDataForOne($shipmentId);
        return $this->renderPartial('new/pack', [
            'provider' => $provider,
        ]);
    }

    public function actionPackBox($id)
    {
        $this->_throwIfBoxNotExists($id);
        $provider = $this->_monitorData->getDataForOneBox($id);
        return $this->renderPartial('new/pack', [
            'provider' => $provider,
        ]);
    }

    public function actionPackBoxNew($id)
    {
        $this->_throwIfBoxNotExists($id);
        $provider = $this->_monitorData->getDataForOneBoxNew($id);
        return $this->renderPartial('new/pack-2', [
            'provider' => $provider,
        ]);
    }

    public function actionPackShipmentWithoutUpns($shipmentId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->_throwIfShipmentNotExists($shipmentId);

        return [
            'status'  => $this->_packUpn->packShipmentWithoutUpns($shipmentId)
        ];
    }

    /**
     * Действие для обработки заявки - суть данного экшна - это найти юпны на складе для заявки и если нет таких то создать новый наряд для экшна
     */
    public function actionHandle()
    {
        $bool = $this->_processService->handleOrder();
        return $bool;
    }

    public function actionHandleById($id)
    {
        $bool = $this->_processService->handleOrderById($id);
        return $bool;
    }

    public function actionHandleBySourceInnumber($sourceInnumber)
    {
        $result = $this->_processService->handleOrderBySourceInnumber($sourceInnumber);
        if ($result) {
            Yii::$app->session->setFlash('success', 'Обработка прошла успешно');
        } else {
            Yii::$app->session->setFlash('error', 'Обработка прошла не успешно');
        }

        return $this->redirect(['journal']);
    }

    public function actionMakeUpnReady($upn)
    {
        if (!$upn) {
            Yii::$app->session->setFlash('error', 'UPN пуст');
            return $this->redirect(['journal']);
        }
        if (!($naryad = ErpNaryad::find()->where(['logist_id' => $upn])->one())) {
            Yii::$app->session->setFlash('error', 'Не найден наряд по данному UPN');
            return $this->redirect(['journal']);
        }
        $naryad->status       = ErpNaryad::STATUS_READY;
        $naryad->start        = null;
        $naryad->location_id  = null;
        if (!$naryad->save()) {
            Yii::$app->session->setFlash('error', 'Не удалось сохранить наряд');
            return $this->redirect(['journal']);
        }
        Yii::$app->session->setFlash('success', 'Наряд успешно отредактирован');

        return $this->redirect(['journal']);
    }

    public function actionSearch($shipmentId, $barcode)
    {
        $this->_throwIfShipmentNotExists($shipmentId);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = $this->_packUpn->packForShipment($shipmentId, $barcode);

        return $response;
    }

    public function actionSearchBox($shipmentId, $boxId, $barcode)
    {
        $this->_throwIfBoxNotExists($boxId);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = $this->_packUpn->packForBox($shipmentId, $boxId, $barcode);

        return $response;
    }

//    public function actionSearchReestr($barcode)
//    {
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//        $bool = $this->_processService->shipReestr($barcode);
//        //Функция должна удалить с остатков со склада все upn, которые разеревированы
//        $response = [];
//        $response['status'] = $bool;
//        return $response;
//        //определяем является ли это штрихкодом
//        //если это является штрихкодом, тогда должны найти наряд по данному штрихокоду
//        //для данного наряд upn упаковать upn
//        //ернуть true или false.
//        //response.message = 'Наряд успешно отмечен как упакованный'
//        //response.message = 'Введен штрихкод неверного upn.'
//
//    }

    public function actionSearchReestr($barcode)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $barcode = BarcodeHelper::toLatin($barcode);
        $barcode = mb_strtoupper($barcode);
        $bool = $this->_processService->shipShipment($barcode);
        $response = [];
        $response['status'] = $bool;
        return $response;
    }

    public function actionSearchShipOne($shipmentId, $barcode)
    {
        $barcode = BarcodeHelper::toLatin($barcode);
        $barcode = mb_strtoupper($barcode);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!$this->_processService->checkShipmentBarcode($shipmentId, $barcode)) {
            return [
                'status' => false,
                'error' => 'Штрихкод не соответствует приказу'
            ];
        }
        return [
            'status' => $this->_processService->shipShipment($barcode)
        ];
    }

    public function actionShipReadyOrders()
    {
        $provider = $this->_readyReestr->getData();
        return $this->renderPartial('_ship_reestr', [
            'provider' => $provider,
        ]);
    }

    public function actionReestrOrders()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $some = \Yii::$app->request->post('orders') ? \Yii::$app->request->post('orders') : [];
        $orders = [];

        if (count($some)) {
            //Эти ордеры надо объединить одним реестром.
            //Далее эти ордеры счтать отгруженными, после того как пропикан реестр и остатки уходят со склада
            $topnum = Order::find()->where(['is not','reestrId',null])->orderBy(['reestrId' => SORT_DESC])->one();
            $reestr = $topnum ? $topnum->reestrId + 1 : 1;

            foreach ($some as $one) {
                $order = Order::findOne($one);
                if ($order) {
                    $order->reestrId = $reestr;
                    $order->save();
                    $orders[] = $order->id;
                }
            }
        }
        $response['status'] = 'success';
        $response['orders']  = $orders;
        $response['reestr']  = $reestr;
        return $response;
    }

    public function actionRegistryShipmentPrint($id)
    {
        $this->_throwIfShipmentNotExists($id);
        return $this->renderPartial('new/_reestr_item_print', [
            'model' => $this->_monitorData->getDataByShipmentId($id),
        ]);
    }

    public function actionRegistryShipmentPrintByBox($id)
    {
        $this->_throwIfShipmentNotExists($id);
        return $this->renderPartial('new/_reestr_item_print2', [
            'model' => $this->_monitorData->getBoxedDataByShipmentId($id),
        ]);
    }

    public function actionRegistryBoxPrint($id)
    {
        return $this->renderPartial('new/_reestr_item_print3', [
            'model' => $this->_monitorData->getDataByBoxId($id),
        ]);
    }

    public function actionLiteralBoxPrint($id)
    {
        $model = $this->_monitorData->getPackageDataByBoxIdNew($id);
        if (!$model['remoteLiteral'])
            return $this->renderPartial('new/_reestr_item_package_print', [
                'model' => $model,
            ]);

        return $this->renderPartial('new/_reestr_item_print4', [
            'model' => $model,
        ]);
    }

    public function actionRegistryShipmentPackagePrint($id)
    {
        $this->_throwIfShipmentNotExists($id);
        return $this->renderPartial('new/_reestr_item_package_print', [
            'model' => $this->_monitorData->getPackageDataByShipmentId($id),
        ]);
    }

    public function actionRegistryBoxPackagePrint($id)
    {
        $model = $this->_monitorData->getPackageDataByBoxIdNew($id);
        return $this->renderPartial('new/_reestr_item_package_print', [
            'model' => $model,
        ]);
    }

    public function actionPrintReestr($id,$reestr)
    {
        $id=explode(',', $id);
        $orders = $this->_readyReestr->getPrintData($id);
        $content = '';
        $content .= $this->renderPartial('_print_reestr',[
            'orders' => $orders,
            'reestr' => $reestr,
        ]);

        echo $content;
    }

    public function actionPrintOrderReestr($id)
    {
        $provider = $this->_monitorData->getDataForOne($id);
        return $this->renderPartial('_order_reestr', [
            'provider' => $provider,
        ]);
    }

    /**
     * Список артикулов с низким уровнем остатков
     *
     * @return string
     */
    public function actionLowBalanceList()
    {
        return $this->renderPartial('low-balance-list', [
            'provider' => $this->_lowBalanceList->getProvider(),
        ]);
    }

    /**
     * Список логистических позиций, которые не могут быть исполнены из за отсутсвия остатков на складе
     *
     * @return string
     */
    public function actionDangerList()
    {
        return $this->renderPartial('danger-list', [
            'provider' => $this->_dangerList->getProvider(),
        ]);
    }

    /**
     * Печать выбранных элементов из DangerList
     *
     * @param int[] $orderEntryIds
     * @return string
     */
    public function actionPrintDangerList(array $orderEntryIds)
    {
        return $this->renderPartial('danger-list-print', [
            'models' => OrderEntry::find()->where(['in', 'id', $orderEntryIds])->all(),
        ]);
    }

    /**
     * Оприходование выбранных элементов из DangerList
     *
     * @return array
     */
    public function actionAjaxOprihodDangerList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = $this->_dangerList->oprihod(Yii::$app->request->post('orderEntryIds'));

        return [
            'status'          => $result['status'],
            'message'         => $result['message'],
            'dangerListCount' => $this->_dangerList->getCount(),
        ];
    }

    /**
     * Форма для отправки отгрузки на доработку
     *
     * @param $shipmentId
     * @return string
     */
    public function actionReworkShipmentForm($shipmentId)
    {
        $this->_throwIfShipmentNotExists($shipmentId);
        $provider = $this->_monitorData->getDataForOne($shipmentId);

        return $this->renderPartial('rework-shipment/form', [
            'shipmentId' => $shipmentId,
            'provider' => $provider,
        ]);
    }

    /**
     * Отправка отгрузки на доработку
     * (ajax-функция)
     *
     * @param int $shipmentId
     * @return array
     */
    public function actionAjaxReworkShipment($shipmentId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

         return [
             'status' => false,
             'message' => 'Данный сервис пока недоступен'
         ];
    }

    /**
     * Получение списка документов по id заказа
     * (ajax-функция)
     *
     * @param int $orderId
     * @return array
     */
    public function actionAjaxGetDocuments($orderId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $repository = new \backend\modules\logistics\models\orderDocument\OrderDocumentRepository();
        $getSet = new \backend\modules\logistics\models\orderDocument\OrderDocumentService();
        $documents = $repository->getDocumentsByOrderId($orderId);
        $rows = [];
        foreach ($documents as $document) {
            $row['id'] = $getSet->getId($document);
            $row['url'] = $getSet->getFileUrlForView($document);
            $row['name'] = $getSet->getFileOriginalName($document);
            $rows[] = $row;
        }

        return [
            'documents' => $rows
        ];
    }

    /**
     * Получение списка документов по id отгрузки
     * (ajax-функция)
     *
     * @param int $shipmentId
     * @return array
     */
    public function actionAjaxGetShipmentDocuments($shipmentId)
    {
        $this->_throwIfShipmentNotExists($shipmentId);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $shipmentDocumentManager    = Yii::$container->get('core\entities\logisticsShipmentDocument\ShipmentDocumentManager');
        $fileManager                = Yii::$app->fileManager;
        $documents                  = $shipmentDocumentManager->findBy(['shipmentId' => $shipmentId]);
        $rows                       = [];
        foreach ($documents as $document) {
            $fileKey     = $shipmentDocumentManager->getFileKey($document);
            $row['id']   = $shipmentDocumentManager->getId($document);
            $row['url']  = $fileManager->getFileUrlForView($fileKey);
            $row['name'] = $fileManager->getFileOriginalName($fileKey);
            $rows[]      = $row;
        }

        return [
            'documents' => $rows
        ];
    }

    /**
     * Удаление позиции в заказе
     * (ajax-функция)
     *
     * @param int $id
     * @return array
     */
    public function actionAjaxDeleteOrderEntry($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $orderEntry = $this->_findOrderEntry($id);
        $result = $this->_deleteOrderEntry->delete($orderEntry);

        return [
            'status'  => $result['status'],
            'message' => $result['message']
        ];
    }

    /**
     * Восстановление позиции в заказе
     * (ajax-функция)
     *
     * @param int $id
     * @return array
     */
    public function actionAjaxRestoreOrderEntry($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $orderEntry = $this->_findOrderEntry($id);
        $result = $this->_deleteOrderEntry->restore($orderEntry);

        return [
            'status'  => $result['status'],
            'message' => $result['message']
        ];
    }

    /**
     * Постановка на резер позиции заказа
     *
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionAjaxReserveOrderEntry($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        try {
            $orderEntry = $this->_findOrderEntry($id);
            $message = OrderEntryReservation::reserve($orderEntry);
            $response['status'] = 'success';
            $response['message'] = $message;
        }catch (\DomainException $e){
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * Снятие с резерва позиции заказа
     *
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionAjaxUnReserveOrderEntry($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        try {
            $orderEntry = $this->_findOrderEntry($id);
            $message = OrderEntryReservation::uNreserve($orderEntry);
            $response['status'] = 'success';
            $response['message'] = $message;
        }catch (\DomainException $e){
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }


    /**
     * Постановка на резер позиции заказа
     *
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionAjaxCreateWorkOrder($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        try {
            $orderEntry = $this->_findOrderEntry($id);
            $message = OrderEntryWorkOrder::createWorkOrderWithUpn($orderEntry);
            $response['status'] = 'success';
            $response['message'] = $message;
        }catch (\DomainException $e){
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * Постановка на резер позиции заказа
     *
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionAjaxCancelWorkOrder($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        try {
            $orderEntry = $this->_findOrderEntry($id);
            $message = OrderEntryWorkOrder::cancelWorkOrder($orderEntry);
            $response['status'] = 'success';
            $response['message'] = $message;
        }catch (\DomainException $e){
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    private function _throwIfShipmentNotExists($shipmentId)
    {
        $shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');

        if (!$shipmentManager->exists($shipmentId)) {
            throw new NotFoundHttpException('Приказ на отгрузку не найден');
        }
    }

    private function _throwIfBoxNotExists($boxId)
    {
        if (!Box::findOne($boxId)) {
            throw new NotFoundHttpException('Коробка не найдена');
        }
    }

    private function _findOrderEntry($orderEntryId)
    {
        if (!($orderEntry = \backend\modules\logistics\models\OrderEntry::findOne($orderEntryId))) {
            throw new NotFoundHttpException('Позиция не найдена');
        }

        return $orderEntry;
    }

    private function _updateSession($name, $pageFlag, $defaultValue = false)
    {
        $session      = Yii::$app->session;
        $remember     = Yii::$app->request->get($pageFlag) ?: Yii::$app->request->post($pageFlag);
        $sessionValue = !$remember ? $session->get($name, $defaultValue) : (Yii::$app->request->get($name, false) ?: Yii::$app->request->post($name, false));
        $session->set($name, $sessionValue);

        return $sessionValue;
    }

    /**
     * @throws StaleObjectException
     */
    public function actionDeleteOrder($id): Response
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $logisticsOrder = Order::findOne($id);
            if (!$logisticsOrder) {
                $transaction->rollBack();
                throw new \DomainException('Не найден логистический заказ с идентифкатором "' . $id . '"!');
            }
            $prodOrder = ErpOrder::findOne($logisticsOrder->source_innumber);
            if (!$prodOrder) {
                $transaction->rollBack();
                throw new \DomainException('Не найден производственный заказ с идентифкатором "' . $logisticsOrder->source_innumber . '"!');
            }
            $workOrders = $prodOrder->naryads;
            foreach ($workOrders as $workOrder) {
                $workOrder->order_id = null;
                if (!$workOrder->save()) {
                    $transaction->rollBack();
                    throw new \DomainException('Не удалось отвязать производственные наряды от заказа "' . $logisticsOrder->source_innumber . '"!');
                }
            }

            if (!$prodOrder->delete()) {
                $transaction->rollBack();
                throw new \DomainException('Не удалось удалить производственный заказ "' . $logisticsOrder->source_innumber . '"!');
            }

            $orderEntries = $logisticsOrder->orderEntries;
            foreach ($orderEntries as $orderEntry) {
                if (!$orderEntry->delete()) {
                    $transaction->rollBack();
                    throw new \DomainException('Не удалось удалить позиции логистического заказа "' . $id. '"!');
                }
            }

            if (!$logisticsOrder->delete()) {
                $transaction->rollBack();
                throw new \DomainException('Не удалось удалить логистический заказ "' . $id . '"!');
            }
            $transaction->commit();
            return $this->redirect('journal');
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw new \DomainException($exception->getMessage());
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw new \DomainException($exception->getMessage());
        }
    }

    /**
     * @throws StaleObjectException
     */
    public function actionCancelOrder($id): Response
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $logisticsOrder = Order::findOne($id);
            if (!$logisticsOrder) {
                $transaction->rollBack();
                throw new \DomainException('Не найден логистический заказ с идентифкатором "' . $id . '"!');
            }
            $prodOrder = ErpOrder::findOne($logisticsOrder->source_innumber);
            if (!$prodOrder) {
                $transaction->rollBack();
                throw new \DomainException('Не найден производственный заказ с идентифкатором "' . $logisticsOrder->source_innumber . '"!');
            }
            $workOrders = $prodOrder->naryads;
            foreach ($workOrders as $workOrder) {
                if ($workOrder->location_id == null && in_array($workOrder->start, [2,4,1])) {
                    if (!$workOrder->cancel()) {
                        $transaction->rollBack();
                        throw new \DomainException('Не удалось отменить производственные наряды в заказе "' . $logisticsOrder->source_innumber . '"!');
                    }
                } else {
                    $workOrder->order_id = null;
                    if (!$workOrder->save()) {
                        $transaction->rollBack();
                        throw new \DomainException('Не удалось отвязать производственные наряды от заказа "' . $logisticsOrder->source_innumber . '"!');
                    }
                }
            }

            $prodOrder->status = ErpOrderAlias::STATUS_CANCEL;
            if (!$prodOrder->save()) {
                $transaction->rollBack();
                throw new \DomainException('Не удалось отменить производственный заказ "' . $logisticsOrder->source_innumber . '"!');
            }

            $orderEntries = $logisticsOrder->orderEntries;
            foreach ($orderEntries as $orderEntry) {
                if (!$this->_deleteOrderEntry->delete($orderEntry)['status']) {
                    $transaction->rollBack();
                    throw new \DomainException('Не удалось удалить позиции логистического заказа "' . $id. '"!');
                }
            }

            $transaction->commit();
            return $this->redirect('journal');
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw new \DomainException($exception->getMessage());
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw new \DomainException($exception->getMessage());
        }
    }
}