<?php

namespace backend\modules\logistics;

/**
 * logistics module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\logistics\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        \Yii::$app->layout = '2columns';
        parent::init();

        // custom initialization code goes here
        $this->modules = [
            'implementation-monitor' =>
                ['class' => 'backend\modules\logistics\modules\implementationMonitor\Module'],
            'logistics-monitor' =>
                ['class' => 'backend\modules\logistics\modules\logisticsMonitor\Module'],
            'child-monitor' =>
                ['class' => 'backend\modules\logistics\modules\childMonitor\Module'],
            'replenishment-monitor' =>
                ['class' => 'backend\modules\logistics\modules\replenishmentMonitor\Module'],
            'warehouse-monitor' =>
                ['class' => 'backend\modules\logistics\modules\warehouseMonitor\Module'],
            'storage-analytics' =>
                ['class' => 'backend\modules\logistics\modules\storageAnalytics\Module'],
            'shipment-monitor' =>
                ['class' => 'backend\modules\logistics\modules\shipmentMonitor\Module'],
            'naryad-transfer-monitor' =>
                ['class' => 'backend\modules\logistics\modules\naryadTransferMonitor\Module'],
            'storage-balance-monitor' =>
                ['class' => 'backend\modules\logistics\modules\storageBalanceMonitor\Module'],
            'transport-company' =>
                ['class' => 'backend\modules\logistics\modules\transportCompany\Module'],
            'print-documents' =>
                ['class' => 'backend\modules\logistics\modules\printDocuments\Module'],
            'print-labels' =>
                ['class' => 'backend\modules\logistics\modules\printLabels\Module'],
            'auto-completion' =>
                ['class' => 'backend\modules\logistics\modules\autoCompletion\Module'],
            'ean' =>
                ['class' => 'backend\modules\logistics\modules\ean\Module'],
            'transfer-storage' =>
                ['class' => 'backend\modules\logistics\modules\transferStorage\Module'],
            'europe-invoice' => [
                'class' => 'backend\modules\logistics\modules\europeInvoice\Module'
            ],
        ];
    }
}
