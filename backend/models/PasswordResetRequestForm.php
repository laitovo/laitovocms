<?php
namespace backend\models;

use Yii;
use common\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE, 'website_id' => Yii::$app->params['website_id']],
                'message' => Yii::t('app', 'Пользователь не найден.')
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'E-mail'),
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @param $continue string
     * @return boolean whether the email was send
     */
    public function sendEmail($continue = null)
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'website_id' => Yii::$app->params['website_id'],
            'email' => $this->email,
        ]);

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                Yii::$app->mailer->setTransport(Yii::$app->params['mailer.transport']['supportEmail']);
                return Yii::$app->mailer->compose('passwordResetToken', ['user' => $user, 'continue' => $continue])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($this->email)
                    ->setSubject(Yii::t('app', 'Восстановление пароля'))
                    ->send();

            }
        }

        return false;
    }
}
