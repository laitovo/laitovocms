<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\team\Team;
use yii\helpers\Json;

/**
 * Приглашение в команду
 */
class InviteForm extends Model
{
    public $emails;

    private $pattern = '/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/';

    private $_team=null;


    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['emails'], 'required'],
            ['emails', 'filter', 'filter' => 'trim'],
            ['emails', 'validateEmails'],
        ];
    }

    /**
     * @ignore
     */
    public function attributeLabels()
    {
        return [
            'emails' => Yii::t('app', 'Адреса электронной почты через запятую'),
        ];
    }

    /**
     * Validates the emails.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateEmails($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $emails=explode(',', $this->emails);
            $_emails=[];
            foreach ($emails as $row) {
                $row=trim($row);
                if (!(preg_match($this->pattern, $row))){
                    $this->addError($attribute, Yii::t('yii', '{attribute} is not a valid email address.',['attribute'=>$row]));
                }
                if ( ($team=Team::find()->joinWith('users')->where(['team.id'=>$this->_team->id,'user.email'=>$row])->one())!==null )
                    $this->addError($attribute, Yii::t('app', '"{attribute}" уже зарегистрирован.',['attribute'=>$row]));
             
                $_emails[]=$row;
            }

            $this->emails=implode(',', $_emails);
        }
    }

    public function __construct($team_id, $config = [])
    {
        if ( ($this->_team=Team::find()->joinWith('users')->where(['team.id'=>$team_id,'user.id'=>Yii::$app->user->id])->one())===null ) {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function sendInvite()
    {
        if ($this->validate()){
            $messages = [];
            $from=Yii::$app->user->identity->name.' <'.Yii::$app->user->identity->email.'>';
            $json=Json::decode($this->_team->json ? $this->_team->json : '{}');
            Yii::$app->mailer->setTransport(Yii::$app->params['mailer.transport']['supportEmail']);
            $emails=explode(',', $this->emails);
            foreach ($emails as $row) {
                $json['Invite'][$row]=isset($json['Invite'][$row]) ? $json['Invite'][$row] : Yii::$app->security->generateRandomString();
                $messages[] = Yii::$app->mailer->compose('sendInvite', ['user' => Yii::$app->user->identity,'team'=>$this->_team,'email'=>$row,'invite'=>$json['Invite'][$row]])
                    ->setFrom([Yii::$app->params['supportEmail'] => $from])
                    ->setTo($row)
                    ->setSubject( Yii::t('app', 'Приглашение в команду') );
            }
            $this->_team->json=Json::encode($json);
            if ($this->_team->save())
                return Yii::$app->mailer->sendMultiple($messages);
        } else {
            return false;
        }
    }

}
