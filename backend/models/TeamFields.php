<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\team\Team;
use common\models\team\Organization;
use common\models\team\UserTeam;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\helpers\Json;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;

/**
 * Team form
 */
class TeamFields extends TeamForm
{

    public $target='[]';
    public $render;

    private $_json;
    private $_module;
    private $_table;

    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['target','render'], 'string'],
            [['target'], 'default', 'value' => '[]'],
        ];
    }

    public function __construct($team_id, $module, $table, $_fields, $config = [])
    {
        parent::__construct($team_id, $config);

        if (!(isset($_fields[$module]) && isset($_fields[$module][$table])))
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
            
        $this->_module=$module;
        $this->_table=$table;
        $this->_json=Json::decode($this->team->json ? $this->team->json : '{}');
        $this->target=Json::encode((isset($this->_json['formbuilder']) && isset($this->_json['formbuilder'][$module]) && isset($this->_json['formbuilder'][$module][$table])) ? $this->_json['formbuilder'][$module][$table]['target'] : []);
    }

    public function save()
    {
        if ($this->validate()) {
            $team=$this->team;
            $json=$this->_json;
            $json['formbuilder'][$this->_module][$this->_table]['target']=Json::decode($this->target);
            $json['formbuilder'][$this->_module][$this->_table]['render']=$this->render;
            $team->json=Json::encode($json);
            return $team->save();
        } else {
            return false;
        }
    }

}
