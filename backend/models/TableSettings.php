<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use yii\helpers\Json;
use Yii;

/**
 * Настройки таблицы
 * 
 * @author basyan <basyan@yandex.ru>
 */
class TableSettings extends Model
{
    public $show = [];
    public $hideall = [];
    public $hidephone = [];
    public $hidetablet = [];
    public $salt;
    public $pageSize = 20;
    public $open = false;

    private $_user;


    public function __construct($get, $_salt, $_show=[], $_hideall=[], $_hidephone=[], $_hidetablet=[], $_pageSize=20, $_open=false, $config = [])
    {
        $user=$this->getUser();
        $table=md5(Yii::$app->id.Yii::$app->controller->module->id.Yii::$app->controller->id.Yii::$app->controller->action->id.Yii::$app->user->id.$_salt);

        $json=Json::decode(isset($user->json) && $user->json ? $user->json : '{}');

        if ($user && $this->load($get) && $this->validate() && $this->salt==$_salt) {
            $json['TableSettings'][$table]=$this;
            $_json=Json::encode($json);

            if ($_json!=$user->json){
                $user->json=$_json;
                $user->save();
            }
        } else {
            $_json['TableSettings']=isset($json['TableSettings'][$table]) ? $json['TableSettings'][$table] : ['show'=>$_show,'hideall'=>$_hideall,'hidephone'=>$_hidephone,'hidetablet'=>$_hidetablet,'pageSize'=>$_pageSize,'open'=>$_open];
            $_json['TableSettings']['salt']=$_salt;

            $this->load($_json);
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['salt', 'string'],
            ['show', 'required'],
            [['show','hideall','hidephone','hidetablet'], 'each', 'rule' => ['string']],
            ['pageSize', 'integer', 'min'=>1, 'max'=>200],
            ['open', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'show' => Yii::t('app', 'Колонки таблицы'),
            'hideall' => Yii::t('app', 'Сворачивать всегда'),
            'hidephone' => Yii::t('app', 'Сворачивать на телефоне'),
            'hidetablet' => Yii::t('app', 'Сворачивать на планшете'),
            'pageSize' => Yii::t('app', 'Элементов на странице'),
            'open' => Yii::t('app', 'Разворачивать все поля'),
        ];
    }


    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Yii::$app->user->identity;
        }

        return $this->_user;
    }

}
