<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Security form
 */
class EditPasswordForm extends Model
{
    public $oldpassword;
    public $password;
    public $passwordr;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oldpassword', 'password', 'passwordr'], 'required'],
            ['password', 'string', 'min' => 6],
            ['passwordr','compare','compareAttribute'=>'password'],
            ['oldpassword', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oldpassword' => Yii::t('app', 'Текущий пароль'),
            'password' => Yii::t('app', 'Новый пароль'),
            'passwordr' => Yii::t('app', 'Повторить пароль'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->oldpassword)) {
                $this->addError($attribute, Yii::t('app', 'Неверный пароль.'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function save()
    {
        if ($this->validate()) {
            $user=$this->getUser();
            $user->setPassword($this->password);
            return $user->save();
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Yii::$app->user->identity;
        }

        return $this->_user;
    }
}
