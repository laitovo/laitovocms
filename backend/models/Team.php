<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace backend\models;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Текущая команда
 *
 * @author basyan <basyan@yandex.ru>
 */
class Team extends Component
{

    private $_team=null;

    public function __construct($config = [])
    {
        $this->_team=\common\models\team\Team::find()->joinWith('users')->joinWith('teamModules')->where(['team.id'=>Yii::$app->session['team_id'],'user.id'=>Yii::$app->user->id])->one();

        if ($this->_team===null){
            $this->_team=\common\models\team\Team::find()->joinWith('users')->joinWith('teamModules')->where(['user.id'=>Yii::$app->user->id])->one();
        }

        parent::__construct($config);
    }

    public function getTeam()
    {
        if ($this->_team===null){
            $this->_team=new \common\models\team\Team;
        }
        return $this->_team;
    }

    public function getId()
    {
        return $this->getTeam()->id;
    }

    public function setTeam($id)
    {
        Yii::$app->session['team_id']=$id;
        static::__construct();
    }

    public function can($mod, $ver=0)
    {
        $can=ArrayHelper::map($this->getTeam()->teamModules,'module_name','version');
        return (isset($can[$mod]) && $can[$mod]>=$ver);
    }
}
