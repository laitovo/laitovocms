<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Settings form
 */
class SettingsForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $password;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['password', 'required', 'when' => function($model) {
                return $model->email != $this->getUser()->email;
            },'whenClient' => "function (attribute, value) {
                return $('input[name=\"SettingsForm[email]\"]').val()!='".$this->getUser()->email."';
            }"],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 
                'targetClass' => '\common\models\User',
                'filter' => ['and', ['website_id' => Yii::$app->params['website_id']],['not', ['id' => Yii::$app->user->id]]],
                'message' => Yii::t('app', 'Данный адрес электронной почты уже зарегистрирован.')
            ],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'string'],

            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'Текущий пароль'),
            'name' => Yii::t('app', 'Ваше имя'),
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Телефон'),
        ];
    }

    public function __construct($config = [])
    {
        $user=$this->getUser();
        $this->name=$user->name;
        $this->email=$user->email;
        $this->phone=$user->phone;

        parent::__construct($config);
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('app', 'Неверный пароль.'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function save()
    {
        if ($this->validate()) {
            $user=$this->getUser();
            $user->name=$this->name;
            $user->email=$this->email;
            $user->phone=$this->phone;
            return $user->save();
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Yii::$app->user->identity;
        }

        return $this->_user;
    }
}
