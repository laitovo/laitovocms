<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\team\Team;
use common\models\team\Organization;
use common\models\team\UserTeam;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\helpers\Json;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;

/**
 * Team form
 */
class TeamForm extends Model
{
    public $name;
    public $organization='[]';

    public $team=null;

    private $_user=null;


    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'max' => 255],
            [['organization'], 'string'],
            ['organization', 'default', 'value' => '[]'],
            [['organization'], 'validateOrg'],
        ];
    }

    public function validateOrg($attribute, $params)
    {
        $number = new NumberValidator();
        $string = new StringValidator();
        $required = new RequiredValidator();

        if (!$this->hasErrors()) {
            if ($this->organization){
                try {
                    $organization=Json::decode($this->organization);
                    if ($organization){
                        foreach ($organization as $row) {
                            if (($number->validate($row['id'], $error) || $row['id']==null)
                                && $required->validate($row['name'], $error) 
                                && $string->validate($row['name'], $error) ) {
                            } else {
                                $this->addError($attribute, Yii::t('yii', 'Invalid data received for parameter "{param}".', ['param'=>$this->getAttributeLabel($attribute)]));
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $this->addError($attribute, Yii::t('yii', 'Invalid data received for parameter "{param}".', ['param'=>$this->getAttributeLabel($attribute)]));
                }
            }
        }
    }

    /**
     * @ignore
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название команды'),
            'organization' => Yii::t('app', 'Реквизиты организации'),
        ];
    }

    public function __construct($team_id=null, $config = [])
    {
        if ($team_id===null){
            $this->team=new Team;
        } elseif ( ($this->team=Team::find()->joinWith('users')->where(['team.id'=>$team_id,'user.id'=>Yii::$app->user->id])->one())!==null ) {
            $this->name=$this->team->name;

            $organization=[];
            if ($this->team->organizations){
                foreach ($this->team->organizations as $row) {
                    $organization[]=[
                        'id'=>$row->id,
                        'name'=>$row->name,
                    ];
                }
            }
            $this->organization=Json::encode($organization);



        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    /**
     * Сохранение команды.
     *
     * @return boolean true|false
     */
    public function save()
    {
        if ($this->validate()) {
            $team=$this->team;
            $team->name=$this->name;
            if ($team->save()) {

                if ($this->organization){
                    $organization=Json::decode($this->organization);
                    foreach ($organization as $row) {
                        if (!($row['id'] && ($org=Organization::findOne($row['id']))!==null && $org->team_id==$team->id))
                            $org=new Organization;
                        $org->team_id=$team->id;
                        $org->name=$row['name'];
                        if ($org->save())
                            $itemsid[$org->id]=$org->id;
                    }
                    if ($team->organizations){
                        foreach ($team->organizations as $row) {
                            if (!isset($itemsid[$row->id])){
                                $row->delete();
                            }
                        }
                    }
                }
                return true;

            }
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Yii::$app->user->identity;
        }

        return $this->_user;
    }

    public function delete()
    {
        if ($this->team->author_id!=Yii::$app->user->id)
            $this->team->unlink('users', $this->getUser(), true);
        else
            $this->team->delete();
        return;
    }

    public function unlink($user_id)
    {
        if (($user=User::findOne(['id'=>$user_id]))!==null && $this->team->author_id==Yii::$app->user->id)
            $this->team->unlink('users', $user, true);
        else
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        return;
    }
}
