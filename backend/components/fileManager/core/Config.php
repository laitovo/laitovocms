<?php

namespace backend\components\fileManager\core;

use Yii;

/**
 * Класс предоставляет доступ к конфигурации приложения.
 * По идее все конструкции вида "Yii::$app->...", "Yii::getAlias..." должны кастомизироваться и храниться здесь.
 *
 * Class Config
 * @package backend\components\fileManager\core
 */
class Config
{
    /**
     * Папка для загрузки файлов
     *
     * @return bool|string
     */
    public function getUploadsDir()
    {
        return Yii::getAlias('@backendUploads');
    }

    /**
     * id текущего пользователя
     *
     * @return int|string
     */
    public function getUserId()
    {
        return Yii::$app->user->id;
    }

    /**
     * id команды, к которой относится текущий пользователь
     *
     * @return mixed
     */
    public function getTeamId()
    {
        return Yii::$app->team->id;
    }
}