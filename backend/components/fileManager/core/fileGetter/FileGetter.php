<?php

namespace backend\components\fileManager\core\fileGetter;

use backend\components\fileManager\core\FileAccessService;
use backend\components\fileManager\core\FileUrlService;
use backend\components\fileManager\core\models\uploadedFile\UploadedFile;
use backend\components\fileManager\core\models\uploadedFile\UploadedFileService;

/**
 * Класс, предоставляющий информацию о загруженных файлах
 *
 * Class FileGetter
 * @package backend\components\fileManager\core\fileGetter
 */
class FileGetter
{
    /**
     * @var UploadedFileService Сервис получения свойств объектов "Загруженный файл"
     */
    private $_uploadedFileService;
    /**
     * @var FileAccessService Сервис для получения файлов по ключу
     */
    private $_fileAccessService;
    /**
     * @var FileUrlService Сервис для получения ссылок на файлы
     */
    private $_fileUrlService;

    public function __construct()
    {
        $this->_uploadedFileService = new UploadedFileService();
        $this->_fileAccessService = new FileAccessService();
        $this->_fileUrlService = new FileUrlService();
    }

    /**
     * Проверка существования файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return bool
     */
    public function fileExists($fileKey)
    {
        return $this->_fileAccessService->fileExists($fileKey);
    }

    /**
     * Проверка что файл доступен для текущего пользователя
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return bool
     */
    public function fileAccessible($fileKey)
    {
        return $this->_fileAccessService->fileAccessible($fileKey);
    }

    /**
     * Размер файла в байтах
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return int|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFileSize($fileKey)
    {
        if (!($file = $this->_getFileByKey($fileKey))) {
            return false;
        }

        return $this->_uploadedFileService->getSize($file);
    }

    /**
     * Оригинальное имя, с которым загружался файл
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFileOriginalName($fileKey)
    {
        if (!($file = $this->_getFileByKey($fileKey))) {
            return false;
        }

        return $this->_uploadedFileService->getOriginalName($file);
    }

    /**
     * Абсолютный путь к файлу
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFilePath($fileKey)
    {
        if (!($file = $this->_getFileByKey($fileKey))) {
            return false;
        }

        return $this->_uploadedFileService->getPath($file);
    }

    /**
     * Ссылка для просмотра файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFileUrlForView($fileKey)
    {
        return $this->_fileUrlService->getFileUrlForView($fileKey);
    }

    /**
     * Ссылка для скачивания файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFileUrlForDownload($fileKey)
    {
        return $this->_fileUrlService->getFileUrlForDownload($fileKey);
    }

    /**
     * Функция возвращает файл по ключу
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return UploadedFile|false Объект "Загруженный файл" (или false, если файл недоступен)
     */
    private function _getFileByKey($fileKey)
    {
        return $this->_fileAccessService->getFileByKey($fileKey);
    }
}