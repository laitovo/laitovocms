<?php

namespace backend\components\fileManager\core;

use backend\components\fileManager\core\models\uploadedFile\UploadedFile;
use backend\components\fileManager\core\models\uploadedFile\UploadedFileAccessService;
use backend\components\fileManager\core\models\uploadedFile\UploadedFileRepository;
use backend\components\fileManager\core\models\uploadedFile\UploadedFileService;

/**
 * Класс, отвечающий за предоставление доступа к файлам
 *
 * Class FileAccessService
 * @package backend\components\fileManager\core
 */
class FileAccessService
{
    /**
     * @var UploadedFileRepository Сервис для получения объектов "Загруженный файл"
     */
    private $_uploadedFileRepository;
    /**
     * @var UploadedFileService Сервис получения свойств объектов "Загруженный файл"
     */
    private $_uploadedFileService;
    /**
     * @var UploadedFileAccessService Сервис для проверки доступа к файлу
     */
    private $_uploadedFileAccessService;

    public function __construct()
    {
        $this->_uploadedFileRepository = new UploadedFileRepository();
        $this->_uploadedFileService = new UploadedFileService();
        $this->_uploadedFileAccessService = new UploadedFileAccessService();
    }

    /**
     * Функция выдает ключ для доступа к файлу
     *
     * @param UploadedFile $file Объект "Загруженный файл"
     * @return string
     */
    public function getKeyForFile($file)
    {
        return $this->_uploadedFileService->getId($file);
    }

    /**
     * Функция возвращает файл по ключу
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return UploadedFile|false Объект "Загруженный файл" (или false, если файл недоступен)
     */
    public function getFileByKey($fileKey)
    {
        if (!($file = $this->_findFileByKey($fileKey))) {
            return false;
        }
        if (!$this->_checkFileAccess($file)) {
            return false;
        }

        return $file;
    }

    /**
     * Проверка существования файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return bool
     */
    public function fileExists($fileKey)
    {
        return $this->_findFileByKey($fileKey) != null;
    }

    /**
     * Проверка что файл доступен для текущего пользователя
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return bool
     */
    public function fileAccessible($fileKey)
    {
        if (!($file = $this->_findFileByKey($fileKey))) {
            return false;
        }
        if (!$this->_checkFileAccess($file)) {
            return false;
        }

        return true;
    }

    /**
     * Данная функция отвечает за логику поиска файла по ключу
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return UploadedFile|null Объект "Загруженный файл"
     */
    private function _findFileByKey($fileKey)
    {
        return $this->_uploadedFileRepository->getUploadedFileById($fileKey);
    }

    /**
     * Проверка доступен ли файл
     *
     * @param UploadedFile $file Объект "Загруженный файл"
     * @return bool Результат проверки
     */
    private function _checkFileAccess($file)
    {
        return $this->_uploadedFileAccessService->checkAccess($file);
    }
}