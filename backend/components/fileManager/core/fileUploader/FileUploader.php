<?php

namespace backend\components\fileManager\core\fileUploader;

use backend\components\fileManager\core\Config;
use backend\components\fileManager\core\FileAccessService;
use backend\components\fileManager\core\models\uploadedFile\UploadedFileFactory;
use backend\components\fileManager\core\models\uploadedFile\UploadedFileRepository;
use backend\components\fileManager\core\models\uploadedFile\UploadedFileService;

/**
 * Класс для загрузки файлов на сервер
 *
 * Class FileUploader
 * @package backend\components\fileManager\core\fileUploader
 */
class FileUploader
{
    /**
     * @var Config Сервис для доступа к конфигурации приложения
     */
    private $_config;
    /**
     * @var UploadedFileFactory Сервис для создания объектов "Загруженный файл"
     */
    private $_uploadedFileFactory;
    /**
     * @var UploadedFileService Сервис для доступа к свойствам объектов "Загруженный файл"
     */
    private $_uploadedFileService;
    /**
     * @var UploadedFileRepository Сервис для сохранения объектов "Загруженный файл"
     */
    private $_uploadedFileRepository;
    /**
     * @var FileFactory Сервис для создания объектов "Загружаемый файл" (не путать с моделью "Загруженный файл")
     */
    private $_fileFactory;
    /**
     * @var FileAccessService Сервис для предоставления доступа к файлам
     */
    private $_fileAccessService;
    /**
     * @var string Устанавливаемые права для загружаемых файлов
     */
    private $_fileAccess;
    /**
     * @var \yii\web\UploadedFile Текущий загружаемый файл
     */
    private $_file;
    /**
     * @var string Оригинальное пользовательское имя текущего файла
     */
    private $_fileOriginalName;
    /**
     * @var int Размер текущего файла в байтах
     */
    private $_fileSize;
    /**
     * @var string Папка для текущего файла
     */
    private $_fileDir;
    /**
     * @var string Путь относительно папки uploads для текущего файла
     */
    private $_fileRelativePath;
    /**
     * @var string Абсолютный путь для текущего файла
     */
    private $_filePath;
    /**
     * @var string Ключ для доступа к текущему файлу после его сохранения
     */
    private $_fileKey;

    public function __construct()
    {
        $this->_config = new Config();
        $this->_uploadedFileFactory = new UploadedFileFactory();
        $this->_uploadedFileService = new UploadedFileService();
        $this->_uploadedFileRepository = new UploadedFileRepository();
        $this->_fileFactory = new FileFactory();
        $this->_fileAccessService = new FileAccessService();
    }

    /**
     * Загрузка на сервер файлов с публичными правами.
     *
     * На выходе массив ключей для доступа к файлам.
     * Если какой-то файл не удалось загрузить, значение ключа для него будет false.
     * Пример выходного массива:
     * [
     *     'test_doc.pdf' => '123',
     *     'test_img.jpg' => '4567',
     *     'test_video.mp4' => false,
     * ].
     *
     * @param string $fieldName Поле формы, из которого берём файлы
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return array|bool Массив ключей для доступа к загруженным файлам (или false, если что-то пошло не так)
     */
    public function uploadPublicFiles($fieldName, $relativeDir)
    {
        $this->_applyPublicAccess();
        $files = $this->_fileFactory->createFiles($fieldName);

        return $this->_uploadFiles($files, $relativeDir);
    }

    /**
     * Загрузка на сервер файлов с командными правами.
     *
     * На выходе массив ключей для доступа к файлам.
     * Если какой-то файл не удалось загрузить, значение ключа для него будет false.
     * Пример выходного массива:
     * [
     *     'test_doc.pdf' => '123',
     *     'test_img.jpg' => '4567',
     *     'test_video.mp4' => false,
     * ].
     *
     * @param string $fieldName Поле формы, из которого берём файлы
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return array|bool Массив ключей для доступа к загруженным файлам (или false, если что-то пошло не так)
     */
    public function uploadTeamFiles($fieldName, $relativeDir)
    {
        $this->_applyTeamAccess();
        $files = $this->_fileFactory->createFiles($fieldName);

        return $this->_uploadFiles($files, $relativeDir);
    }

    /**
     * Загрузка на сервер файлов с индивидуальными правами.
     *
     * На выходе массив ключей для доступа к файлам.
     * Если какой-то файл не удалось загрузить, значение ключа для него будет false.
     * Пример выходного массива:
     * [
     *     'test_doc.pdf' => '123',
     *     'test_img.jpg' => '4567',
     *     'test_video.mp4' => false,
     * ].
     *
     * @param string $fieldName Поле формы, из которого берём файлы
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return array|bool Массив ключей для доступа к загруженным файлам (или false, если что-то пошло не так)
     */
    public function uploadUserFiles($fieldName, $relativeDir)
    {
        $this->_applyUserAccess();
        $files = $this->_fileFactory->createFiles($fieldName);

        return $this->_uploadFiles($files, $relativeDir);
    }

    /**
     * Загрузка на сервер файла с публичными правами
     *
     * @param string $fieldName Поле формы, из которого берём файл
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return string|bool Ключ для доступа к загруженному файлу (или false, если что-то пошло не так)
     */
    public function uploadPublicFile($fieldName, $relativeDir)
    {
        $this->_applyPublicAccess();
        $file = $this->_fileFactory->createFile($fieldName);

        return $this->_uploadFile($file, $relativeDir);
    }

    /**
     * Загрузка на сервер файла с командными правами
     *
     * @param string $fieldName Поле формы, из которого берём файл
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return string|bool Ключ для доступа к загруженному файлу (или false, если что-то пошло не так)
     */
    public function uploadTeamFile($fieldName, $relativeDir)
    {
        $this->_applyTeamAccess();
        $file = $this->_fileFactory->createFile($fieldName);

        return $this->_uploadFile($file, $relativeDir);
    }

    /**
     * Загрузка на сервер файла с индивидуальными правами
     *
     * @param string $fieldName Поле формы, из которого берём файл
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return string|bool Ключ для доступа к загруженному файлу (или false, если что-то пошло не так)
     */
    public function uploadUserFile($fieldName, $relativeDir)
    {
        $this->_applyUserAccess();
        $file = $this->_fileFactory->createFile($fieldName);

        return $this->_uploadFile($file, $relativeDir);
    }

    /**
     * Помечаем, что загружаются файлы с публичными правами
     */
    private function _applyPublicAccess()
    {
        $this->_fileAccess = $this->_uploadedFileService->getPublicAccessValue();
    }

    /**
     * Помечаем, что загружаются файлы с командными правами
     */
    private function _applyTeamAccess()
    {
        $this->_fileAccess = $this->_uploadedFileService->getTeamAccessValue();
    }

    /**
     * Помечаем, что загружаются файлы с индивидуальными правами
     */
    private function _applyUserAccess()
    {
        $this->_fileAccess = $this->_uploadedFileService->getUserAccessValue();
    }

    /**
     * Основная функция, используемая для загрузки нескольких файлов независимо от устанавливаемых прав
     *
     * @param \yii\web\UploadedFile[] $files
     * @param string|null $relativeDir
     * @return array|bool
     */
    private function _uploadFiles($files, $relativeDir)
    {
        if (!count($files)) {
            return false;
        }
        $uploadedFilesInfo = [];
        foreach ($files as $file) {
            $uploadedFilesInfo[$file->name] = $this->_uploadFile($file, $relativeDir);
        }

        return $uploadedFilesInfo;
    }

    /**
     * Основная функция, используемая для загрузки любого файла независимо от кол-ва загружаемых файлов и устанавливаемых прав
     *
     * @param \yii\web\UploadedFile|null $file
     * @param string|null $relativeDir
     * @return string|bool
     */
    private function _uploadFile($file, $relativeDir)
    {
        if (!$file) {
            return false;
        }
        $this->_prepareFileToSave($file, $relativeDir);
        if (!$this->_saveFilePhysically()) {
            return false;
        }
        if (!$this->_saveFileToDb()) {
            return false;
        }

        return $this->_getUploadedFileInfo();
    }

    /**
     * Подготовка к сохранению файла (генерация имени, пути, url)
     *
     * @param \yii\web\UploadedFile $file
     * @param string|null $relativeDir
     */
    private function _prepareFileToSave($file, $relativeDir)
    {
        $uploadsDir = $this->_config->getUploadsDir();
        $relativeDir = $this->_removeSideSlashes($relativeDir);
        $fileName = $this->_generateFileName() . '.' . $file->extension;
        $this->_file = $file;
        $this->_fileOriginalName = $file->name;
        $this->_fileSize = $file->size;
        $this->_fileDir = ($relativeDir || $relativeDir === '0') ? ($uploadsDir . DIRECTORY_SEPARATOR . $relativeDir) : $uploadsDir;
        $this->_fileRelativePath = ($relativeDir || $relativeDir === '0') ? ($relativeDir . DIRECTORY_SEPARATOR . $fileName) : $fileName;
        $this->_filePath = $uploadsDir . DIRECTORY_SEPARATOR . $this->_fileRelativePath;
    }

    /**
     * Функция обрубает начальный и конечный слэши
     *
     * @param string|null $path
     * @return string
     */
    private function _removeSideSlashes($path)
    {
        return trim($path, '/\\');
    }

    /**
     * Функция генерирует рандомное имя для файла
     *
     * @return string
     */
    private function _generateFileName()
    {
        return md5(microtime() . rand(0, 9999));
    }

    /**
     * Непосредственная загрузка файла на сервер
     *
     * @return bool Результат операции (успешно или нет)
     */
    private function _saveFilePhysically()
    {
        $this->_createDirIfNotExists($this->_fileDir);

        return $this->_file->saveAs($this->_filePath);
    }

    /**
     * Рекурсивно создаём нужную папку если таковой ещё нет
     *
     * @param string $dir
     */
    private function _createDirIfNotExists($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    /**
     * Сохраняем в бд информацию о загруженном файле
     *
     * @return bool Результат операции (успешно или нет)
     */
    private function _saveFileToDb()
    {
        $uploadedFile = $this->_uploadedFileFactory->create();
        $this->_uploadedFileService->setRelativePath($uploadedFile, $this->_fileRelativePath);
        $this->_uploadedFileService->setAccess($uploadedFile, $this->_fileAccess);
        $this->_uploadedFileService->setOriginalName($uploadedFile, $this->_fileOriginalName);
        $this->_uploadedFileService->setSize($uploadedFile, $this->_fileSize);
        if (!$this->_uploadedFileRepository->save($uploadedFile)) {
            return false;
        }
        $this->_fileKey = $this->_fileAccessService->getKeyForFile($uploadedFile);

        return true;
    }

    /**
     * Функция возвращает выходную информацию о загруженном файле
     *
     * @return string
     */
    private function _getUploadedFileInfo()
    {
        return (string)$this->_fileKey;
    }
}
