<?php

namespace backend\components\fileManager\core\fileUploader;

use \yii\web\UploadedFile;

/**
 * Фабрика для создания объектов "Загружаемый файл" (не путать с моделью "Загруженный файл")
 *
 * Class FileFactory
 * @package backend\components\fileManager\core\fileUploader
 */
class FileFactory
{
    /**
     * Функция преобразует данные из формы в массив объектов "Загружаемый файл".
     *
     * @param string $fieldName Поле формы, из которого берём файлы
     * @return UploadedFile[]
     */
    public function createFiles($fieldName) {
        return UploadedFile::getInstancesByName($fieldName);
    }

    /**
     * Функция преобразует данные из формы в объект "Загружаемый файл".
     *
     * @param string $fieldName Поле формы, из которого берём файл
     * @return UploadedFile
     */
    public function createFile($fieldName) {
        return UploadedFile::getInstanceByName($fieldName);
    }
}