<?php

namespace backend\components\fileManager\core\fileRemover;

use backend\components\fileManager\core\Config;
use backend\components\fileManager\core\FileAccessService;
use backend\components\fileManager\core\models\uploadedFile\UploadedFile;
use backend\components\fileManager\core\models\uploadedFile\UploadedFileRepository;

/**
 * Класс для удаления файлов
 *
 * Class FileRemover
 * @package backend\components\fileManager\core\fileRemover
 */
class FileRemover
{
    /**
     * @var Config Сервис для доступа к конфигурации приложения
     */
    private $_config;
    /**
     * @var UploadedFileRepository Сервис для удаления объектов "Загруженный файл"
     */
    private $_uploadedFileRepository;
    /**
     * @var FileAccessService Сервис для получения файлов по ключу
     */
    private $_fileAccessService;

    public function __construct()
    {
        $this->_config = new Config();
        $this->_uploadedFileRepository = new UploadedFileRepository();
        $this->_fileAccessService = new FileAccessService();
    }

    /**
     * Удаление файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return bool Результат операции (успешно или нет)
     */
    public function deleteFile($fileKey)
    {
        if (!($file = $this->_getFileByKey($fileKey))) {
            return false;
        }
        if (!$this->_deleteFilePhysically($file)) {
            return false;
        }
        if (!$this->_deleteFileInfoFromDb($file)) {
            return false;
        }

        return true;
    }

    /**
     * Функция возвращает файл по ключу
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return UploadedFile|false Объект "Загруженный файл" (или false, если файл недоступен)
     */
    private function _getFileByKey($fileKey)
    {
        return $this->_fileAccessService->getFileByKey($fileKey);
    }

    /**
     * Непосредственное удаление файла с сервера
     *
     * @param UploadedFile $file Объект "Загруженный файл"
     * @return bool Результат операции (успешно или нет)
     */
    private function _deleteFilePhysically($file)
    {
        $filePath = $this->_config->getUploadsDir() . DIRECTORY_SEPARATOR . $file->relative_path;
        if (!file_exists($filePath) || !is_writable($filePath)) {
            return false;
        }

        return unlink($filePath);
    }

    /**
     * Удаление из бд информации о файле
     *
     * @param UploadedFile $file Объект "Загруженный файл"
     * @return bool Результат операции (успешно или нет)
     */
    private function _deleteFileInfoFromDb($file)
    {
        return $this->_uploadedFileRepository->delete($file);
    }
}