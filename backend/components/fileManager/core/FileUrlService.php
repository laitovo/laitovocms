<?php

namespace backend\components\fileManager\core;

use backend\components\fileManager\core\models\uploadedFile\UploadedFile;

/**
 * Класс отвечает за выдачу ссылок на файлы
 *
 * Class FileUrlService
 * @package backend\components\fileManager\core
 */
class FileUrlService
{
    /**
     * @var string Префикс для ссылок на просмотр файла
     */
    private $_urlPrefixForView = '/files/view/';
    /**
     * @var string Префикс для ссылок на скачивание файла
     */
    private $_urlPrefixForDownload = '/files/download/';
    /**
     * @var FileAccessService Сервис для получения файлов по ключу
     */
    private $_fileAccessService;

    public function __construct()
    {
        $this->_fileAccessService = new FileAccessService();
    }

    /**
     * Ссылка для просмотра файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемая ссылка (или false, если файл недоступен)
     */
    public function getFileUrlForView($fileKey)
    {
        if (!($file = $this->_getFileByKey($fileKey))) {
            return false;
        }

        return $this->_urlPrefixForView . $fileKey;
    }

    /**
     * Ссылка для скачивания файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемая ссылка (или false, если файл недоступен)
     */
    public function getFileUrlForDownload($fileKey)
    {
        if (!($file = $this->_getFileByKey($fileKey))) {
            return false;
        }

        return $this->_urlPrefixForDownload . $fileKey;
    }

    /**
     * Функция возвращает файл по ключу
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return UploadedFile|false Объект "Загруженный файл" (или false, если файл недоступен)
     */
    private function _getFileByKey($fileKey)
    {
        return $this->_fileAccessService->getFileByKey($fileKey);
    }
}