<?php

namespace backend\components\fileManager\core\models\uploadedFile;

use backend\components\fileManager\core\Config;

/**
 * Класс для работы с правами на загруженные файлы
 *
 * Class UploadedFileAccessService
 * @package backend\components\fileManager\core
 */
class UploadedFileAccessService
{
    /**
     * @var Config Сервис для доступа к конфигурации приложения
     */
    private $_config;

    public function __construct()
    {
        $this->_config = new Config();
    }

    /**
     * Проверка доступен ли файл
     *
     * @param mixed $uploadedFile
     * @return bool Результат проверки
     */
    public function checkAccess($uploadedFile) {
        $this->_throwIfInvalidType($uploadedFile);
        switch ($uploadedFile->access) {
            case UploadedFile::ACCESS_PUBLIC:
                return true;
            case UploadedFile::ACCESS_TEAM:
                return $this->_checkTeam($uploadedFile) || $this->_checkUser($uploadedFile);
            case UploadedFile::ACCESS_USER:
                return $this->_checkUser($uploadedFile);
            default:
                return false;
        }
    }

    /**
     * Проверка что команда файла совпадает с командой текущего пользователя
     *
     * @param mixed $uploadedFile
     * @return bool Результат проверки
     */
    private function _checkTeam($uploadedFile) {
        return $uploadedFile->team_id == $this->_config->getTeamId();
    }

    /**
     * Проверка что файл загружен текущим пользователем
     *
     * @param mixed $uploadedFile
     * @return bool Результат проверки
     */
    private function _checkUser($uploadedFile) {
        return $uploadedFile->user_id == $this->_config->getUserId();
    }

    /**
     * Проверка на тип переданного файла
     *
     * @param mixed $uploadedFile Объект "Загруженный файл"
     * @throws \Exception
     */
    private function _throwIfInvalidType($uploadedFile) {
        if (!($uploadedFile instanceof UploadedFile)) {
            throw new \Exception('В сервис для работы с правами передан неверный тип файла');
        }
    }
}