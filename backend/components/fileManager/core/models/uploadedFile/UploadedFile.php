<?php

namespace backend\components\fileManager\core\models\uploadedFile;

use common\models\team\Team;
use common\models\LogActiveRecord;
use common\models\user\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Модель "Загруженный файл" (храним в бд информацию обо всех загруженных файлах)
 *
 * Class UploadedFile
 * @package backend\components\fileManager\core
 */
class UploadedFile extends LogActiveRecord
{
    #Права доступа
    const ACCESS_PUBLIC = 'public';
    const ACCESS_TEAM = 'team';
    const ACCESS_USER = 'user';

    /**
     * Наименование таблицы в базе данных
     *
     * @return string
     */
    public static function tableName()
    {
        return 'uploaded_file';
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id'], 'message' => 'Сотрудник не найден'],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id'], 'message' => 'Команда не найдена'],
            [['user_id', 'team_id'], 'integer', 'message' => 'Это поле должно быть целым числом'],
        ];
    }

    /**
     * Поведения (примеси)
     * Автоматически заполняем поле uploaded_at
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['uploaded_at'],
                ],
            ],
        ];
    }
}