<?php

namespace backend\components\fileManager\core\models\uploadedFile;

/**
 * Репозиторий загруженных файлов. Отвечает за работу с БД.
 *
 * Class UploadedFilesRepository
 * @package backend\components\fileManager\core
 */
class UploadedFileRepository
{
    /**
     * Поиск загруженного файла по относительному пути
     *
     * @param string $fileRelativePath Путь для загрузки файла (относительно папки uploads без начального и конечного слэшей)
     * @return UploadedFile|null Объект "Загруженный файл"
     */
    public function getUploadedFileByRelativePath($fileRelativePath) {
        return UploadedFile::find()->where(['relative_path' => $fileRelativePath])->one();
    }

    /**
     * Поиск загруженного файла по идентификатору
     *
     * @param int $id Идентификатор файла
     * @return UploadedFile|null Объект "Загруженный файл"
     */
    public function getUploadedFileById($id) {
        return UploadedFile::findOne($id);
    }

    /**
     * Сохранение загруженного файла в бд
     *
     * @param mixed $uploadedFile
     * @return bool Результат операции (успешно или нет)
     */
    public function save($uploadedFile) {
        $this->_throwIfInvalidType($uploadedFile);

        return $uploadedFile->save();
    }

    /**
     * Удаление загруженного файла из бд
     *
     * @param mixed $uploadedFile
     * @return bool Результат операции (успешно или нет)
     */
    public function delete($uploadedFile) {
        $this->_throwIfInvalidType($uploadedFile);

        return $uploadedFile->delete();
    }

    /**
     * Проверка на тип переданного файла
     *
     * @param mixed $uploadedFile Объект "Загруженный файл"
     * @throws \Exception
     */
    private function _throwIfInvalidType($uploadedFile) {
        if (!($uploadedFile instanceof UploadedFile)) {
            throw new \Exception('В репозиторий передан неверный тип файла');
        }
    }
}