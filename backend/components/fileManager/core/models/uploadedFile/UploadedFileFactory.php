<?php

namespace backend\components\fileManager\core\models\uploadedFile;

use backend\components\fileManager\core\Config;

/**
 * Фабрика для создания объектов "Загруженный файл"
 *
 * Class UploadedFileFactory
 * @package backend\components\fileManager\core
 */
class UploadedFileFactory
{
    /**
     * @var Config Сервис для доступа к конфигурации приложения
     */
    private $_config;

    public function __construct()
    {
        $this->_config = new Config();
    }

    /**
     * Базовое создание объекта "Загруженный файл".
     * По-умолчанию проставляем текущего пользователя и команду.
     *
     * @return UploadedFile
     */
    public function create() {
        $uploadedFile = new UploadedFile();
        $uploadedFile->user_id = $this->_config->getUserId();
        $uploadedFile->team_id = $this->_config->getTeamId();

        return $uploadedFile;
    }
}