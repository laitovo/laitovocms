<?php

namespace backend\components\fileManager\core\models\uploadedFile;

use backend\components\fileManager\core\Config;

/**
 * Класс для доступа к свойствам объекта "Загруженный файл"
 *
 * Class UploadedFileService
 * @package backend\components\fileManager\core
 */
class UploadedFileService
{
    /**
     * @var Config Сервис для доступа к конфигурации приложения
     */
    private $_config;

    public function __construct()
    {
        $this->_config = new Config();
    }

    /**
     * Значение поля access для файла с публичными правами
     *
     * @return string
     */
    public function getPublicAccessValue()
    {
        return UploadedFile::ACCESS_PUBLIC;
    }

    /**
     * Значение поля access для файла с командными правами
     *
     * @return string
     */
    public function getTeamAccessValue()
    {
        return UploadedFile::ACCESS_TEAM;
    }

    /**
     * Значение поля access для файла с индивидуальными правами
     *
     * @return string
     */
    public function getUserAccessValue()
    {
        return UploadedFile::ACCESS_USER;
    }

    /**
     * Идентификатор файла
     *
     * @return int
     */
    public function getId($uploadedFile)
    {
        $this->_throwIfInvalidType($uploadedFile);

        return $uploadedFile->id;
    }

    /**
     * Оригинальное имя, с которым загружался файл
     *
     * @return string
     */
    public function getOriginalName($uploadedFile)
    {
        $this->_throwIfInvalidType($uploadedFile);

        return $uploadedFile->original_name;
    }

    /**
     * Размер файла в байтах
     *
     * @return int
     */
    public function getSize($uploadedFile)
    {
        $this->_throwIfInvalidType($uploadedFile);

        return $uploadedFile->size;
    }

    /**
     * Абсолютный путь к файлу
     *
     * @param mixed $uploadedFile
     * @return string
     */
    public function getPath($uploadedFile) {
        $this->_throwIfInvalidType($uploadedFile);

        return $this->_config->getUploadsDir() . DIRECTORY_SEPARATOR . $uploadedFile->relative_path;
    }

    /**
     * Задаём путь к файлу относительно папки uploads
     *
     * @param mixed $uploadedFile
     * @param string $relativePath
     */
    public function setRelativePath($uploadedFile, $relativePath) {
        $this->_throwIfInvalidType($uploadedFile);

        $uploadedFile->relative_path = $relativePath;
    }

    /**
     * Задаём права доступа на файл
     *
     * @param mixed $uploadedFile
     * @param string $access
     */
    public function setAccess($uploadedFile, $access) {
        $this->_throwIfInvalidType($uploadedFile);

        $uploadedFile->access = $access;
    }

    /**
     * Задаем оригинальное имя, с которым загружался файл
     *
     * @param mixed $uploadedFile
     * @param string $originalName
     */
    public function setOriginalName($uploadedFile, $originalName) {
        $this->_throwIfInvalidType($uploadedFile);

        $uploadedFile->original_name = $originalName;
    }

    /**
     * Задаем размер файла в байтах
     *
     * @param mixed $uploadedFile
     * @param int $size
     */
    public function setSize($uploadedFile, $size) {
        $this->_throwIfInvalidType($uploadedFile);

        $uploadedFile->size = $size;
    }

    /**
     * Проверка на тип переданного файла
     *
     * @param mixed $uploadedFile Объект "Загруженный файл"
     * @throws \Exception
     */
    private function _throwIfInvalidType($uploadedFile) {
        if (!($uploadedFile instanceof UploadedFile)) {
            throw new \Exception('В сервис для работы со свойствами передан неверный тип файла');
        }
    }
}
