<?php

namespace backend\components\fileManager\core;

use yii\base\Module;
use yii\web\Controller;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Контроллер для выдачи файлов
 *
 * Class FileAccessController
 * @package backend\components\fileManager\core
 */
class FileAccessController extends Controller
{
    /**
     * @var mixed Сервис для работы с файлами
     */
    private $_fileManager;

    public function __construct($id, Module $module, array $config = [])
    {
        $this->_fileManager = Yii::$app->fileManager;
        parent::__construct($id, $module, $config);
    }

    /**
     * Выдача файла для просмотра
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return mixed
     */
    public function actionView($fileKey)
    {
        $this->_throwIfFileNotAvailable($fileKey);

        return Yii::$app->response->sendFile($this->_fileManager->getFilePath($fileKey), $this->_fileManager->getFileOriginalName($fileKey), ['inline'=>true]);
    }

    /**
     * Выдача файла для скачивания
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return mixed
     */
    public function actionDownload($fileKey)
    {
        $this->_throwIfFileNotAvailable($fileKey);

        return Yii::$app->response->sendFile($this->_fileManager->getFilePath($fileKey), $this->_fileManager->getFileOriginalName($fileKey), ['inline'=>false]);
    }

    /**
     * Функция бросает исключение если файл недоступен
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    private function _throwIfFileNotAvailable($fileKey)
    {
        if (!$this->_fileManager->fileExists($fileKey)) {
            throw new NotFoundHttpException('Файл не найден');
        }
        if (!$this->_fileManager->fileAccessible($fileKey)) {
            throw new ForbiddenHttpException('Доступ запрещён');
        }
    }
}
