<?php

namespace backend\components\fileManager;

use backend\components\fileManager\core\fileGetter\FileGetter;
use backend\components\fileManager\core\fileUploader\FileUploader;
use backend\components\fileManager\core\fileRemover\FileRemover;

/**
 * Класс для работы с файлами (загрузка, удаление, предоставление свойств)
 *
 * Class FileManager
 * @package backend\components\fileManager
 */
class FileManager
{
    /**
     * @var FileUploader Сервис для загрузки файлов на сервер
     */
    private $_fileUploader;
    /**
     * @var FileRemover Сервис для удаления файлов
     */
    private $_fileRemover;
    /**
     * @var FileGetter Сервис для получения информации о файлах
     */
    private $_fileGetter;

    public function __construct()
    {
        $this->_fileUploader = new FileUploader();
        $this->_fileRemover = new FileRemover();
        $this->_fileGetter = new FileGetter();
    }

    /**
     * Загрузка на сервер файлов с публичными правами.
     *
     * На выходе массив ключей для доступа к файлам.
     * Если какой-то файл не удалось загрузить, значение ключа для него будет false.
     * Пример выходного массива:
     * [
     *     'test_doc.pdf' => '123',
     *     'test_img.jpg' => '4567',
     *     'test_video.mp4' => false,
     * ].
     *
     * @param string $fieldName Поле формы, из которого берём файлы
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return array|bool Массив ключей для доступа к загруженным файлам (или false, если что-то пошло не так)
     */
    public function uploadPublicFiles($fieldName, $relativeDir)
    {
        return $this->_fileUploader->uploadPublicFiles($fieldName, $relativeDir);
    }

    /**
     * Загрузка на сервер файлов с командными правами.
     *
     * На выходе массив ключей для доступа к файлам.
     * Если какой-то файл не удалось загрузить, значение ключа для него будет false.
     * Пример выходного массива:
     * [
     *     'test_doc.pdf' => '123',
     *     'test_img.jpg' => '4567',
     *     'test_video.mp4' => false,
     * ].
     *
     * @param string $fieldName Поле формы, из которого берём файлы
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return array|bool Массив ключей для доступа к загруженным файлам (или false, если что-то пошло не так)
     */
    public function uploadTeamFiles($fieldName, $relativeDir)
    {
        return $this->_fileUploader->uploadTeamFiles($fieldName, $relativeDir);
    }

    /**
     * Загрузка на сервер файлов с индивидуальными правами.
     *
     * На выходе массив ключей для доступа к файлам.
     * Если какой-то файл не удалось загрузить, значение ключа для него будет false.
     * Пример выходного массива:
     * [
     *     'test_doc.pdf' => '123',
     *     'test_img.jpg' => '4567',
     *     'test_video.mp4' => false,
     * ].
     *
     * @param string $fieldName Поле формы, из которого берём файлы
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return array|bool Массив ключей для доступа к загруженным файлам (или false, если что-то пошло не так)
     */
    public function uploadUserFiles($fieldName, $relativeDir)
    {
        return $this->_fileUploader->uploadUserFiles($fieldName, $relativeDir);
    }

    /**
     * Загрузка на сервер файла с публичными правами
     *
     * @param string $fieldName Поле формы, из которого берём файл
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return string|bool Ключ для доступа к загруженному файлу (или false, если что-то пошло не так)
     */
    public function uploadPublicFile($fieldName, $relativeDir)
    {
        return $this->_fileUploader->uploadPublicFile($fieldName, $relativeDir);
    }

    /**
     * Загрузка на сервер файла с командными правами
     *
     * @param string $fieldName Поле формы, из которого берём файл
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return string|bool Ключ для доступа к загруженному файлу (или false, если что-то пошло не так)
     */
    public function uploadTeamFile($fieldName, $relativeDir)
    {
        return $this->_fileUploader->uploadTeamFile($fieldName, $relativeDir);
    }

    /**
     * Загрузка на сервер файла с индивидуальными правами
     *
     * @param string $fieldName Поле формы, из которого берём файл
     * @param string|null $relativeDir Путь для загрузки относительно папки uploads (например, vasya/docs)
     * @return string|bool Ключ для доступа к загруженному файлу (или false, если что-то пошло не так)
     */
    public function uploadUserFile($fieldName, $relativeDir)
    {
        return $this->_fileUploader->uploadUserFile($fieldName, $relativeDir);
    }

    /**
     * Удаление файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return bool Результат операции (успешно или нет)
     */
    public function deleteFile($fileKey)
    {
        return $this->_fileRemover->deleteFile($fileKey);
    }

    /**
     * Размер файла в байтах
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return int|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFileSize($fileKey)
    {
        return $this->_fileGetter->getFileSize($fileKey);
    }

    /**
     * Оригинальное имя, с которым загружался файл
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFileOriginalName($fileKey)
    {
        return $this->_fileGetter->getFileOriginalName($fileKey);
    }

    /**
     * Ссылка для просмотра файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFileUrlForView($fileKey)
    {
        return $this->_fileGetter->getFileUrlForView($fileKey);
    }

    /**
     * Ссылка для скачивания файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFileUrlForDownload($fileKey)
    {
        return $this->_fileGetter->getFileUrlForDownload($fileKey);
    }

    /**
     * Абсолютный путь к файлу
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return string|false Требуемое свойство (или false, если файл недоступен)
     */
    public function getFilePath($fileKey)
    {
        return $this->_fileGetter->getFilePath($fileKey);
    }

    /**
     * Проверка существования файла
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return bool
     */
    public function fileExists($fileKey)
    {
        return $this->_fileGetter->fileExists($fileKey);
    }

    /**
     * Проверка что файл доступен для текущего пользователя
     *
     * @param string $fileKey Ключ для доступа к файлу
     * @return bool
     */
    public function fileAccessible($fileKey)
    {
        return $this->_fileGetter->fileAccessible($fileKey);
    }
}