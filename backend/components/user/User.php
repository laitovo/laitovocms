<?php

namespace backend\components\user;

/**
 * @inheritdoc
 *
 * @property \common\models\User|\yii\web\IdentityInterface|null $identity The identity _object associated with the currently logged-in user. Null is returned if the user is not logged in (not authenticated).
 */
class User extends \yii\web\User
{

}
