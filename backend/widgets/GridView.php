<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace backend\widgets;

use yii;
use backend\assets\footable\FooTableAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\models\TableSettings;

/**
 * @author basyan <basyan@yandex.ru>
 */
class GridView extends \yii\grid\GridView
{
    public $layout = "<div class='table-responsive'>{items}</div>\n{summary}\n{pager}";
    /**
     * @var string соль для идентификации таблицы
     */
    public $salt = '';
    /**
     * @var array атрибуты отображаемые по умолчанию
     */
    public $show = [];

    /**
     * [$customHead Для кстомного header]
     * @var html
     */
    public $customHead = '';

    /**
     * [$showCustomHead Показывать или нет кастомный header]
     * @var boolean
     */
    public $showCustomHead = false;
    /**
     * @var \backend\models\TableSettings настройки
     */
    private $_model;
    /**
     * @var array колонки для настроек
     */
    private $_columns = [];


    public function init()
    {
        parent::init();

        $this->_model=new TableSettings(Yii::$app->request->queryParams,$this->salt,$this->show);
        
        $view = $this->getView();
        FooTableAsset::register($view);
        $view->registerCss('a.asc:after,a.desc:after{position:relative;top:1px;display:inline-block;line-height:1;padding-left:5px;display:inline-block;font:normal normal normal 14px/1 \'FontAwesome\';font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}a.asc:after{content:"\f176"}a.desc:after{content:"\f175"}');

        $this->tableOptions['class'].=' toggle-arrow-tiny footable'.($this->_model->open ? '-open' : '');

        $this->dataProvider->pagination->pageSize=$this->_model->pageSize;
        $this->dataProvider->pagination->pageParam.=$this->salt;
        $this->dataProvider->pagination->pageSizeParam.=$this->salt;

        foreach ($this->columns as $i => $column) {
            if (isset($column->attribute)){
            	$this->_columns[$column->attribute]=strip_tags($column->renderHeaderCell());

            	if (!in_array($column->attribute, (array)$this->_model->show)){
	                unset($this->columns[$i]);
            	} else {
	            	if (in_array($column->attribute, (array)$this->_model->hideall))
		                $this->columns[$i]->headerOptions['data-hide']='all';
	            	elseif (in_array($column->attribute, (array)$this->_model->hidephone))
		                $this->columns[$i]->headerOptions['data-hide']='phone';
            	}
            }
        }
    }

    public function run()
    {
        echo Html::beginTag('div', ['class'=>'pull-right']) . "\n";

        Modal::begin([
        	'size'=>Modal::SIZE_LARGE,
            'header' => Html::tag('h4',Yii::t('app', 'Настройки таблицы'),['class'=>'modal-title']),
            'toggleButton' => ['class'=>'btn btn-icon btn-round btn-default btn-outline','label' => '<i class="fa fa-wrench"></i>'],
        ]);

        $form = ActiveForm::begin(['method'=>'get','action'=>ArrayHelper::merge([Yii::$app->controller->action->id],Yii::$app->request->get()) ]);

        echo $form->field($this->_model, 'show',['options'=>['class'=>'form-group col-sm-4']])->checkboxList($this->_columns);
        echo $form->field($this->_model, 'hideall',['options'=>['class'=>'form-group col-sm-4']])->checkboxList($this->_columns);
        echo $form->field($this->_model, 'hidephone',['options'=>['class'=>'form-group col-sm-4']])->checkboxList($this->_columns);
        // echo $form->field($this->_model, 'hidetablet',['options'=>['class'=>'form-group col-sm-3']])->checkboxList($this->_columns);
        echo $form->field($this->_model, 'pageSize')->textInput();
        echo $form->field($this->_model, 'open')->checkbox();
        echo $form->field($this->_model, 'salt')->hiddenInput()->label(false);
        
        echo Html::submitButton(Yii::t('app', 'Применить'), ['class' => 'pull-right btn btn-primary btn-round btn-outline']);

        ActiveForm::end();
        echo Html::tag('div', '', ['class'=>'clearfix']);

        Modal::end();

        echo "\n" . Html::endTag('div');

        echo Html::tag('p', '', ['class'=>'clearfix']);

        if ($this->showCustomHead) {
            echo $this->customHead;
        }

        parent::run();
    }
}
