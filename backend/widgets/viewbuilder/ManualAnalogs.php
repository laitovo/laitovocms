<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\widgets\viewbuilder;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Cars;
use common\models\laitovo\CarsAnalog;

class ManualAnalogs extends Widget
{
    public  $car_id;
    public  $window_type;

    private $_array_cars = [];

    public function init()
    {
        parent::init();
        
        //находим аналоги данного автомобиля
        $cars = CarsAnalog::find()
            ->where(['and',
                ['car_id' => $this->car_id],
                ['like','json','%'.$this->window_type.'%',false]
            ])->all();

        // var_dump($cars);
        // die();
        //Составляем массив идентификаторов
        $array = ArrayHelper::map($cars,'analog_id','analog_id');

        //для каждого аналога находим
        $cars = Cars::find()->where(['in','id',$array])->all();

        foreach ($cars as $car) {
            $this->_array_cars[] = $car->fullEnName;
        }

    }

    public function run()
    {
        if (count($this->_array_cars)) {
            return $this->render('manual-analogs',['cars' => $this->_array_cars]);
        }

        return '';
    }
}
