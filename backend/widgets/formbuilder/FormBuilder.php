<?php

namespace backend\widgets\formbuilder;

use yii\helpers\Html;
use yii\helpers\Url;
use Yii;

class FormBuilder extends \yii\bootstrap\Widget
{

    /**
     * @var form
     */
    public $form;
    /**
     * @var model
     */
    public $model;
    /**
     * @var attribute
     */
    public $attribute;
    /**
     * @var render
     */
    public $render;

    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        parent::init();

        $view = $this->getView();
        if (@$this->model[$this->attribute]){
            foreach ($this->model[$this->attribute] as $key => $value) {
                $this->render=str_replace('id="'.$key.'"', 'id="'.Html::getInputId($this->model, $this->attribute).'-'.$key.'"', $this->render);
                $this->render=str_replace('name="'.$key.'"', 'name="'.Html::getInputName($this->model, $this->attribute).'['.$key.']"', $this->render);
                $this->render=str_replace('{'.$key.'}', $value, $this->render);
                $this->render=str_replace('data-select="'.$key.'">'.$value.'<', 'data-select="'.$key.'" selected>'.$value.'<', $this->render);
                $this->render=str_replace('data-checkbox="'.$key.'" value="'.$value.'"', 'data-checkbox="'.$key.'" checked value="'.$value.'"', $this->render);
            }
        }
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo $this->render;
        
        parent::run();
    }
}
