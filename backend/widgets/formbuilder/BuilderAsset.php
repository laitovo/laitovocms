<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\widgets\formbuilder;

use yii\web\AssetBundle;

/**
 * @author basyan <basyan@yandex.ru>
 */
class BuilderAsset extends AssetBundle
{
    public $sourcePath = '@backend/widgets/formbuilder/assets';
    public $css = [
        'css/custom.css?v=2',
    ];
    public $js = [
    ];
    public $depends = [
        'backend\themes\remark\assets\AppAsset',
    ];

    /**
     * @inheritdoc
     */
    public function registerAssetFiles($view)
    {
        $this->js[] = ['js/lib/require.js?v=3','data-main'=>\Yii::$app->assetManager->getPublishedUrl('@backend/widgets/formbuilder/assets').'/js/main-built.js'];
        $this->js[] = \Yii::$app->assetManager->getPublishedUrl('@bower/bootstrap/dist').'/js/bootstrap.min.js';

        parent::registerAssetFiles($view);
    }
}
