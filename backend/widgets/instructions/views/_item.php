<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 4/12/18
 * Time: 11:56 AM
 */

use yii\helpers\Html;
use yii\helpers\Url;

$style= '
.switch {
  position: relative;
  display: inline-block;
  width: 50px;
  height: 24px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 20px;
  width: 20px;
  left: 2px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
 ';
 $this->registerCss($style);

$script = <<<SCRIPT
$('.switch').click(function(){

});

SCRIPT;
$this->registerJs($script);
?>

<div class="col-md-1" style="width:135px;padding:5px;">
<!--    --><?//= \yii\helpers\Html::beginForm('instruction','post',['enctype' => 'multipart/form-data']);?>
<!--    <form action="" method="post" enctype="multipart/form-data">-->
    <div class="text-center" style="height:170px;border:1px solid black;border-radius:3px;padding-top:5px">
        <div style="height:90px;">
            <h6 class="text-center"><?=$model['brandName']?></h6>
            <h6 class="text-center"><?=$model['fixationType']?></h6>
            <h6 class="text-center"><?=$model['executionName']?></h6>
        </div>
        <!--        <div class="col-md-6" style="font-size: 2em"><i class="glyphicon glyphicon-eye-open"></i></div>-->
<!--        <div class="col-md-6" style="font-size: 2em">-->
<!--            <input type="file" class="file-input" name="myfile" style ="display:none"-->
<!--                onchange="$(this).closest('form').submit();"-->
<!--            >-->
<!--            <i class="glyphicon glyphicon-cloud-upload"-->
<!--                onclick=" $(this).siblings('.file-input').click();"-->
<!--            ></i>-->

<!--            <input type="hidden" name="instruction[id]"               value="--><?//=$model['id']?><!--">-->
<!--            <input type="hidden" name="instruction[brand]"            value="--><?//=$model['brand']?><!--">-->
<!--            <input type="hidden" name="instruction[performance]"      value="--><?//=$model['performance']?><!--">-->
<!--            <input type="hidden" name="instruction[window]"           value="--><?//=$model['window']?><!--">-->
<!---->
<!--            <input type="hidden" name="instruction[windowAlias]"      value="--><?//=$model['windowAlias']?><!--">-->
<!--            <input type="hidden" name="instruction[brandAlias]"       value="--><?//=$model['brandAlias']?><!--">-->
<!--            <input type="hidden" name="instruction[performanceAlias]" value="--><?//=$model['performanceAlias']?><!--">-->
<!--            <input type="hidden" name="instruction[fixAlias]"         value="--><?//=$model['fixAlias']?><!--">-->
<!---->
<!--        </div>-->
<!--        <div class="clearfix"></div>-->
<!--        <div class="col-md-12 text-center">-->

<!--        <i class="glyphicon glyphicon-eye-open" style="font-size:2em;cursor:pointer;"></i>-->
        <?php if ($model['id']): ?>
            <?= Html::a('<i class="fa fa-pencil-alt"></i>', ['/laitovo/instruction/crud/cms', 'carId' => $model['carId'], 'instructionTypeId' => $model['instructionTypeId']], ['class' => 'btn btn-sm btn-icon btn-outline btn-round btn-primary', 'style' => 'margin-bottom:5px;', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Редактировать индивидуальную инструкцию')]); ?>
        <?php else: ?>
            <?= Html::a('<i class="icon wb-plus"></i>', ['/laitovo/instruction/crud/cms', 'carId' => $model['carId'], 'instructionTypeId' => $model['instructionTypeId']], ['class' => 'btn btn-sm btn-icon btn-outline btn-round btn-primary', 'style' => 'margin-bottom:5px;', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Cоздать индивидуальную инструкцию')]); ?>
        <?php endif; ?>
        <?= Html::button('<i class="glyphicon glyphicon-file"></i>' , ['class' => 'btn btn-sm btn-icon btn-outline btn-round btn-info', 'style' => 'margin-bottom:5px;', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Распечатать инструкцию'), 'onclick' => '
                    printUrl("' . Url::to(['/laitovo/instruction/printing/print-any', 'carId' => $model['carId'], 'instructionTypeId' => $model['instructionTypeId']]) . '");
                ',
        ]) ?>
        <?= Html::button('<i class="glyphicon glyphicon-file"></i>' , ['class' => 'btn btn-sm btn-icon btn-outline btn-round btn-info', 'style' => 'margin-bottom:5px;', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Распечатать инструкцию EU'), 'onclick' => '
                    printUrl("' . Url::to(['/laitovo/instruction/printing/print-any-eu', 'carId' => $model['carId'], 'instructionTypeId' => $model['instructionTypeId']]) . '");
                ',
        ]) ?>
<!--        --><?//= Html::button('<i class="glyphicon glyphicon-file"></i>' , ['class' => 'btn btn-sm btn-icon btn-outline btn-round btn-danger', 'style' => 'margin-bottom:5px;', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Распечатать инструкцию английскую'), 'onclick' => '
//                    printUrl("' . Url::to(['/laitovo/instruction/printing/print-any-eu', 'carId' => $model['carId'], 'instructionTypeId' => $model['instructionTypeId']]) . '");
//                ',
//        ]) ?>
        <br>
        <label class="switch" data-toggle="tooltip" data-original-title="<?=Yii::t('app', 'Выкл. / Вкл.')?>">
            <input type="checkbox" data-id="<?=$model['id']?>" <?php if ($model['isUsed']): ?>checked<?php endif; ?> <?php if (!$model['id']): ?>disabled<?php endif; ?>>
            <span class="slider round"></span>
        </label>

<!--        </div>-->
<!--        <div class="clearfix"></div>-->
    </div>
<!--    </form>-->
<!--    --><?//=  \yii\helpers\Html::endForm();?>
</div>

