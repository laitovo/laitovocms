<?php
use yii\widgets\ListView;
use yii\helpers\Url;
?>
<div class="col-md-12">
    <h4>Индивидуальные инструкции</h4>
<!--    --><?php //foreach ($providers as $key => $provider):?>
<!--    <div class="col-md-1">-->
<!--        <h3>--><?//=$key?><!--</h3>-->
<!--    </div>-->
<!--    <div class="col-md-11">-->
<!--        --><?//=
//        ListView::widget([
//            'dataProvider' => $provider,
//            'itemView' => '_item',
//        ]);
//        ?>
<!--    </div>-->
<!--    <div class="clearfix"></div>-->
<!--    --><?//endforeach;?>

    <?php foreach ($providers as $windowName => $provider):?>
        <div class="col-md-1">
            <h3><?=$windowName?></h3>
        </div>
        <div class="col-md-11">
            <?=
            ListView::widget([
                'id' => 'custom-instructions-grid',
                'dataProvider' => $provider,
                'itemView' => '_item',
                'summary' => ''
            ]);
            ?>
            <div class="clearfix"></div>
        </div>
    <?endforeach;?>
    <? if (empty($providers)): ?>
        Ничего не найдено
    <? endif; ?>
</div>
<div class="clearfix"></div>

<?php Yii::$app->view->registerJs('    
    $("#custom-instructions-grid .switch input").change(function() {
        var checkbox = $(this);
        var id = $(this).data("id");
        var isUsed = $(this).prop("checked") ? 1 : "";
        $.ajax({
            url: "' . Url::to(['/laitovo/instruction/ajax/save-is-used']) . '",
            dataType: "json",
            data: {
                "id": id,
                "isUsed": isUsed,
            },
            success: function (data) {
                if (!data.status) {
                    notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                    checkbox.prop("checked", !checkbox.prop("checked"));
                    return;
                }
                notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
            },
            error: function() {
                notie.alert(3, "Возникла ошибка при выполнении команды", 3);
                checkbox.prop("checked", !checkbox.prop("checked"));
            }
        });
    });
    
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>
