<?php

namespace backend\widgets\instructions;

use yii\base\Widget;
use yii\data\ArrayDataProvider;
use Yii;
use backend\modules\laitovo\models\CarsForm;

class Instructions extends Widget
{
    public $car;
    private $_array;
//    private $_array =
//        [
//            'ПБ' => [
//                [
//                    'id'               => 1,
//                    'window'           => 'FD',
//                    'brand'            => 'Laitovo',
//                    'performance'      => 'Стандарт',
//
//                    'windowAlias'      => 'ПБ',
//                    'brandAlias'       => 'Laitovo',
//                    'performanceAlias' => 'Стандарт',
//                    'fixAlias'         => 'Простые',
//
//                    'status'           => true,
//                    'url'              => '/test-url',
//                ],
//            ],
//            'ЗБ' => [
//                [
//                    'id'               => 2,
//                    'window'           => 'RD',
//                    'brand'            => 'Chiko',
//                    'performance'      => 'Стандарт',
//
//                    'windowAlias'      => 'ПБ',
//                    'brandAlias'       => 'Chiko',
//                    'performanceAlias' => 'Стандарт',
//                    'fixAlias'         => 'Простые',
//
//                    'status'           => true,
//                    'url'              => '/test-url',
//                ],
//            ]
//        ];
    private $_instructionTypeManager;
    private $_windowOpeningTypeManager;
    private $_brandManager;
    private $_executionTypeManager;
    private $_customCarInstructionManager;

    public function __construct(array $config = [])
    {
//        $this->_windowManager      = new WindowManager();
//        $this->_brandManager       = new BrandManager();
//        $this->_performanceManager = new PerformanceManager();
//
//        $this->_typeManager = new InstructionTypeManager();
//        $this->_customManager = new CustomCarInstructionManager();

        $this->_instructionTypeManager      = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_windowOpeningTypeManager    = Yii::$container->get('core\entities\propWindowOpeningType\WindowOpeningTypeManager');
        $this->_brandManager                = Yii::$container->get('core\entities\propBrand\BrandManager');
        $this->_executionTypeManager        = Yii::$container->get('core\entities\propExecutionType\ExecutionTypeManager');
        $this->_customCarInstructionManager = Yii::$container->get('core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager');

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

//        //hekstodo По умолчанию виджет должен принимать идентификатор автомобиля

//
//        $array = [];
//        //hekstodo Получаем все наборы свойств для инструкций
//        $types = $this->_typeManager->getAllTypes();
//
//        foreach ($types as $type) {
//            $id = $this->_typeManager->getId($type);
//
//            $windowID    = $this->_typeManager->getWindowID($type);
//            $window      = $this->_windowManager->getById($windowID);
//            $windowAlias = $this->_windowManager->getAlias($window);
//            $windowName = $this->_windowManager->getName($window);
//
//            $brandID    = $this->_typeManager->getBrandID($type);
//            $brand      = $this->_brandManager->getById($brandID);
//            $brandAlias = $this->_brandManager->getAlias($brand);
//            $brandName = $this->_brandManager->getName($brand);
//
//            $performanceID  = $this->_typeManager->getPerformanceID($type);
//            $performance  = $this->_performanceManager->getById($performanceID);
//            $performanceAlias = $this->_performanceManager->getAlias($performance);
//            $performanceName = $this->_performanceManager->getName($performance);
//
//            $fixAlias  = $this->_typeManager->getFixture($type);
//
//            $array[$id] = [
//                'brandAlias'      => $brandAlias,
//                'windowAlias'     => $windowAlias,
//                'typeAlias'       => $performanceAlias,
//                'type_clips'      => $fixAlias,
//                'brand'      => $brandName,
//                'window'     => $windowName,
//                'type'       => $performanceName,
//            ];
//        }
//
//        //Здесь мы сформировали данные для виджета
//
//        //hekstodo По принятому идентификатору из таблицы со списком файлов он должен дернуть
//        $custom = $this->_customManager->getAllInstructionsForCar($car);
//        //Дозаполняем ранее созднный массив для создания
//
//        foreach ($custom as $row) {
//            $id = $this->_customManager->getInstuctionsTypeId($row);
//
//            $fileID = $this->_customManager->getFileId($row);
//            $status = $this->_customManager->getStatus($row);
//
//            $url  = $this->_fileManager->getUrlForFile($fileID);
//
//            $array[$id]['status'] = $status;
//            $array[$id]['url']    = $url;
//        }
//
//
//        $this->_array = $array;
        //hekstodo Необходимо сформировать данные данные свойств по группам

        $this->_initGridData();
    }

    public function run()
    {
        $providers = [];
        foreach ($this->_array as $key => $item) {
            $provider = new ArrayDataProvider([
                'allModels' => $item,
            ]);
            $providers[$key] = $provider;
        }

            return $this->render('instructions',['providers' => $providers]);
//        }

        return '';
    }

    private function _initGridData()
    {
        $instructionTypes = $this->_instructionTypeManager->findAll();
        $tempArray = [];
        foreach ($instructionTypes as $instructionType) {
            if (!$this->_checkCarsFormStatus($instructionType)) {
                continue;
            }
            $window     = $this->_instructionTypeManager->getRelatedWindowOpeningType($instructionType);
            $windowName = $this->_windowOpeningTypeManager->getName($window);
            $brandId  = $this->_instructionTypeManager->getBrandId($instructionType);
            $fixType  = $this->_instructionTypeManager->getFixationType($instructionType);
            $tempArray[$windowName][$brandId][$fixType][] = $this->_prepareModel($instructionType);
        }
        $this->_array = [];
        foreach ($tempArray as $windowName => $windowData) {
            foreach ($windowData as $brandId => $brandData) {
                foreach ($brandData as $fixType => $fixTypeData) {
                    $this->_array[$windowName] = array_merge($this->_array[$windowName] ?? [], $fixTypeData);
                }
            }
        }
    }

    private function _prepareModel($instructionType)
    {
        $window               = $this->_instructionTypeManager->getRelatedWindowOpeningType($instructionType);
        $brand                = $this->_instructionTypeManager->getRelatedBrand($instructionType);
        $execution            = $this->_instructionTypeManager->getRelatedExecutionType($instructionType);
        $windowName           = $this->_windowOpeningTypeManager->getName($window);
        $brandName            = $this->_brandManager->getName($brand);
        $executionName        = $this->_executionTypeManager->getName($execution);
        $fixationType         = $this->_instructionTypeManager->getFixationTypeLabel($instructionType);
        $instructionTypeId    = $this->_instructionTypeManager->getId($instructionType);
        $carId                = $this->car;
        $customCarInstruction = $this->_customCarInstructionManager->findWhere(['and',
            ['carId' => $carId],
            ['instructionTypeId' => $instructionTypeId]
        ])[0] ?? null;
        $id = !$customCarInstruction ? null : $this->_customCarInstructionManager->getId($customCarInstruction);
        $isUsed = $customCarInstruction && $this->_customCarInstructionManager->getIsUsed($customCarInstruction);

        return [
            'id'                => $id,
            'instructionTypeId' => $instructionTypeId,
            'carId'             => $carId,
            'windowName'        => $windowName,
            'brandName'         => $brandName,
            'executionName'     => $executionName,
            'fixationType'      => $fixationType,
            'isUsed'            => $isUsed,
        ];
    }

    /**
     * Проверка, можем ли мы производить продукты для данного вида инструкций.
     * На основании этой проверки выводятся карточки в данном виджете.
     *
     * @return bool
     */
    private function _checkCarsFormStatus($instructionType)
    {
        $model =  new CarsFORM($this->car);
        $carsFormStatus = $this->_instructionTypeManager->getCarsFormStatus($instructionType);

        return property_exists($model, $carsFormStatus) && $model->$carsFormStatus;
    }
}