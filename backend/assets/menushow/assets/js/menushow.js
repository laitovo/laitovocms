$(document).ready(function() {
    //Данный код отвечает за маркеры слева и за шевроны справа в пунктах меню
    $(".list-group .list-group, .list-group-item").each(function() {
        var depth = $(this).parents(".list-group").length;
        var title_element = $(this).hasClass("list-group") ? $(this).siblings(".page-aside-title") : $(this);
        var marked_title = "<i class='fa fa-circle depth-marker'></i>".repeat(depth) + " " + title_element.html();
        if (!title_element.is("a")) { //Если не ссылка, добавляем шеврон
            var chevron_class = $(this).find(".active").length ? "fa-chevron-up" : "fa-chevron-down";
            marked_title += "<i class='fa " + chevron_class + " chevron'></i>";
        }
        title_element.html(marked_title);
    });
    //Данный код отвечает за начальное раскрытие блоков меню при загрузке страницы
    $(".list-group .list-group").hide();
    $(".list-group .active").parents(".list-group").not(":last").show().siblings(".page-aside-title").addClass("green");
    //Данный код отвечает за раскрытие блоков меню по клику на соответствующие пункты
    $(".list-group .page-aside-title").click(function() {
        $(this).next().slideToggle("slow");
        var chevron = $(this).find(".chevron");
        if (chevron.hasClass("fa-chevron-down")) {
            chevron.removeClass("fa-chevron-down").addClass("fa-chevron-up");
        } else {
            chevron.removeClass("fa-chevron-up").addClass("fa-chevron-down");
        }
    });
});