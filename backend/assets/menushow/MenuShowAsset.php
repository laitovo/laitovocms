<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets\menushow;

use yii\web\AssetBundle;

class MenuShowAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/menushow/assets';

    public $css = [
        'css/menushow.css',
    ];
    public $js = [
        'js/menushow.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
//    public $publishOptions = [
//        'forceCopy'=>true,
//    ];
}
