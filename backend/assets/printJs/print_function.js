function print_by_url(url) {
    $.get(url, function (data) {
        $('body').append(`<div id='print-block'></div>`);
        let printBlock = $('#print-block');
        printBlock.html(data);
        printJS({
            printable: 'print-block',
            type: 'html'
        });
        printBlock.remove();
    }, 'html');
}