<?php

namespace backend\assets\printJs;

use yii\web\AssetBundle;

class PrintJs  extends AssetBundle
{
    public $sourcePath = __DIR__ . '/';
    public $js = ['print.min.js', 'print_function.js'];
}