<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets\footable;

use yii\web\AssetBundle;

/**
 * @author basyan <basyan@yandex.ru>
 */
class FooTableAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/footable/assets';
    public $css = [
        'footable-2.0.3/css/footable.core.min.css',
    ];
    public $js = [
        'footable-2.0.3/footable.min.js',
        'footable.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
}
