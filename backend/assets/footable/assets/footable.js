$(function () {
    $('.footable').footable({
        breakpoints: {
            phone: 768,
            tablet: 1024
        }
    });

    $('.footable-open').footable({
        breakpoints: {
            phone: 768,
            tablet: 1024
        }
    }).trigger('footable_expand_all');
});
