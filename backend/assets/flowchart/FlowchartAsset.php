<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets\flowchart;

use yii\web\AssetBundle;

/**
 * @author basyan <basyan@yandex.ru>
 */
class FlowchartAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/flowchart/assets';

    public $js = [
        'release/go.js',
        'flowchart.js',
    ];
}
