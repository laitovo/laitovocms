<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

?>
<?php $this->beginContent('@backend/themes/remark/layouts/main.php'); ?>
<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?=Html::encode($this->title)?></h1>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <div class="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?=Html::encode($this->title)?></h3>
            </div>
            <div class="panel-body">
                <?=$content?>
            </div>
        </div>
    </div>
</div>
<!-- End Page -->
<?php $this->endContent(); ?>
