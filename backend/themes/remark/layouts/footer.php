<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;

?>
<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">© <?=date('Y')?></div>
    <div class="site-footer-right">
    </div>
</footer>
