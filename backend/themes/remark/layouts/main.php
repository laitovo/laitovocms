<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\themes\remark\assets\AppAsset;
use yii\helpers\Html;
use common\widgets\AlertNotie;

AppAsset::register($this)->skin='teal';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?php $this->beginContent('@backend/themes/remark/layouts/head.php'); ?>
        <?php $this->endContent(); ?>
    </head>
    <body class="site-menubar-fold site-menubar-keep site-navbar-small">
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <?php $this->beginContent('@backend/themes/remark/layouts/navbar.php'); ?>
        <?php $this->endContent(); ?>
        
        <?php $this->beginContent('@backend/themes/remark/layouts/menubar.php'); ?>
        <?php $this->endContent(); ?>

        <?= AlertNotie::widget() ?>
        <?=$content?>
        
        <?php $this->beginContent('@backend/themes/remark/layouts/footer.php'); ?>
        <?php $this->endContent(); ?>

    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
