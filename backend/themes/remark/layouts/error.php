<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\themes\remark\assets\ErrorAsset;
use yii\helpers\Html;

ErrorAsset::register($this)->skin='teal';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?php $this->beginContent('@backend/themes/remark/layouts/head.php'); ?>
        <?php $this->endContent(); ?>
    </head>
    <body class="page-error page-error-404 layout-full">
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="page-content vertical-align-middle">

                <?=$content?>

                <footer class="page-copyright">
                    <p><?=Yii::$app->name?></p>
                    <p>© <?=date('Y')?>. All RIGHT RESERVED.</p>
                </footer>

            </div>
        </div>

        
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
