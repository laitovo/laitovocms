<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Menu;

?>
<div class="site-menubar">
    <?
        if (Yii::$app->team->can('wage') && in_array(Yii::$app->user->getId(),[121,122,123,124,138])) {

            $mainMenuItems[] = [
                'label' => Yii::t('app', 'Заработная плата'), 'url' => ['/wage/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-money-bill-alt" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];

        } else {

            if (Yii::$app->user->can('admin')) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Администрирование'), 'url' => ['/admin/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-cogs" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
            if (Yii::$app->team->can('wage') && in_array(Yii::$app->user->getId(), [21])) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Заработная плата'), 'url' => ['/wage/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-money-bill-alt" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
            if (Yii::$app->team->can('order') && !in_array(Yii::$app->user->getId(), [40, 41])) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Торговля'), 'url' => ['/order/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-shopping-cart" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
            if (Yii::$app->team->can('user')) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Контрагенты'), 'url' => ['/user/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-users" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
            if (Yii::$app->team->can('website') && !in_array(Yii::$app->user->getId(), [40, 41])) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['/website/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-shopping-basket" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
            if (Yii::$app->team->can('laitovo') && !in_array(Yii::$app->user->getId(), [40, 41])) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Лайтово'), 'url' => ['/laitovo/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-car" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
            if (Yii::$app->team->can('logistics')) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-truck" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
            if (Yii::$app->team->can('return')) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Возвраты'), 'url' => ['/return/journal/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-archive" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];

            if (in_array(Yii::$app->user->getId(), [2, 29]) || Yii::$app->user->identity->isRoot()) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Отчеты для руководителя'), 'url' => ['/leader-module/default/index'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-pencil-alt" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];

            if (in_array(Yii::$app->user->getId(), [2, 11, 21, 29, 34, 45, 51, 58])) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Учет Россия'), 'url' => 'https://clients.biz-zone.biz',
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-book" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];

            if (Yii::$app->team->can('laitovo') && Yii::$app->user->getId() == 40) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Лайтово'), 'url' => ['/laitovo/erp/terminal-sdacha'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-car" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
            if (Yii::$app->team->can('laitovo') && Yii::$app->user->getId() == 41) $mainMenuItems[] = [
                'label' => Yii::t('app', 'Лайтово'), 'url' => ['/laitovo/erp/terminal-vidacha'],
                'template' => '<a href="{url}"><i class="site-menu-icon fa fa-car" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
            ];
        }

        echo Menu::widget([
            'labelTemplate' => '<a href="javascript:void(0)"><span class="site-menu-title">{label}</span><span class="site-menu-arrow"></span></a>',
            'options' => ['class'=>'site-menu'],
            'itemOptions' =>['class'=>'site-menu-item'],
            'items' => isset($mainMenuItems) ? $mainMenuItems : [],
            'submenuTemplate' => "\n<ul class='site-menu-sub'>\n{items}\n</ul>\n",
            'linkTemplate' => '<a href="{url}"><span class="site-menu-title">{label}</span></a>',
            // 'encodeLabels' => false,
            'activateParents' => true,
        ]);
    ?>

</div>
