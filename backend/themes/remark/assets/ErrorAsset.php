<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\themes\remark\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ErrorAsset extends AssetBundle
{
    public $skin;
    
    public $sourcePath = '@backend/themes/remark/assets/app';
    public $css = [
        'css/errors.css',
    ];
    public $depends = [
        'backend\themes\remark\assets\AppAsset',
    ];

    /**
     * @inheritdoc
     */
    public function registerAssetFiles($view)
    {
        if ($this->skin) $this->css[] = 'css/skins/' . $this->skin . '.css';
        parent::registerAssetFiles($view);
    }

}
