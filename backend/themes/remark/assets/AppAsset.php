<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\themes\remark\assets;

use Yii;
use yii\web\AssetBundle;
use yii\helpers\Url;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $skin;

    public $sourcePath = '@backend/themes/remark/assets/app';
    public $css = [
        '//fonts.googleapis.com/css?family=Roboto:400,300,500,300italic&subset=latin,cyrillic',
        'css/bootstrap-extend.css',
        'css/site.css',
        'animsition-4.0.1/css/animsition.min.css',
        'css/asScrollable.css',
        'css/slidePanel.css',
        'css/jquery-mmenu.css',
        'switchery-0.8.1/switchery.min.css',
        'web-icons/css/web-icons.css',
        'flag-icon-css-1.1.0/css/flag-icon.min.css',
    ];
    public $js = [
        'animsition-4.0.1/js/animsition.min.js',
        'jquery-asScroll/jquery-asScroll.min.js',
        'jquery-mousewheel-3.1.13/jquery.mousewheel.min.js',
        'jquery-asScrollable/jquery.asScrollable.all.min.js',

        'jQuery.mmenu-5.5.3/core/js/jquery.mmenu.min.all.js',
        'switchery-0.8.1/switchery.min.js',
        'screenfull.js-3.0.0/screenfull.min.js',
        'jquery-slidePanel/jquery-slidePanel.min.js',
        'jquery-placeholder-2.3.1/jquery.placeholder.min.js',

        'js/core.js',
        'js/site.js',

        'js/menubar.js',
        'js/switchery.js',
        'js/sidebar.js',

        'js/asscrollable.js',
        'js/slidepanel.js',
        'js/jquery-placeholder.js',
        'js/material.js',
        'js/animsition.js',
        'js/run.js',
    ];
    public $depends = [
        'backend\themes\remark\assets\BreakpointsAsset',
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\CdnFreeAssetBundle',
        'common\assets\notie\NotieAsset',
    ];

    /**
     * @inheritdoc
     */
    public function registerAssetFiles($view)
    {
        if ($this->skin) $this->css[] = 'css/skins/' . $this->skin . '.css';

        $view->registerJs("
            $('body').on('click','.deleteconfirm[data-method=\"post\"]',function(){
                var url=$(this).attr('href');
                notie.confirm($(this).attr('data-confirm'), '".Yii::t('yii', 'Yes')."', '".Yii::t('yii', 'No')."', function() {
                    $.post(url);
                });
                return false;
            });

            $(document).on('click.pageAsideScroll', '.page-aside-switch', function() {
                $.get('".Url::to(['/settings/close-sidebar-menu'])."');
            });


        ");

        parent::registerAssetFiles($view);
    }

}
