<?php

namespace backend\themes\remark\assets;

use yii\web\AssetBundle;
use Yii;

class FormAsset extends AssetBundle
{
    public $sourcePath = '@backend/themes/remark/assets/app';

    public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css',
        'bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker.min.css',
        '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css',
    ];
    public $js = [
        '//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js',
        'bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js',
        'bootstrap-datepicker-1.6.4-dist/locales/bootstrap-datepicker.ru.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
    ];
    public $depends = [
        'backend\themes\remark\assets\AppAsset',
    ];

    public function registerAssetFiles($view)
    {
        $view->registerJs("

            $('select.select2').select2({
                width: '100%'
            });

			$('.datepicker').datepicker({
                autoclose: true,
                todayHighlight: true,
				language: 'ru'
			});

        ");

        parent::registerAssetFiles($view);
    }

}
