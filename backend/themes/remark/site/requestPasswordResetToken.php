<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
/* @var $continue string|null */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->layout = 'login';
?>

<?=Html::tag('p',Yii::t('app', 'Восстановление пароля'))?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email',[
        'template' => "{input}\n{label}\n{hint}\n{error}",
        'options'=>['class'=>'form-group form-material floating'],
        'labelOptions'=>['class'=>'floating-label'],
    ])->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Восстановить'), ['class' => 'btn btn-block btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?=Html::tag('p',Html::a(Yii::t('app', 'Войти'),['login', 'continue' => $continue]))?>
