<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

Yii::$app->layout = 'error';
$this->title = $name;
?>
<header>
    <h1 class="animation-slide-top"><?=$exception->statusCode?></h1>
    <p><?= Html::encode($this->title) ?></p>
</header>
<p class="error-advise">
    <?= nl2br(Html::encode($message)) ?>
</p>
<a class="btn btn-primary btn-round" href="/"><?=Yii::t('app', 'Вернуться на главную страницу')?></a>
