<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
/* @var $continue string|null */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->layout = 'login';
?>

<?=Html::tag('p',Yii::t('app', 'Введите свой e-mail и пароль для входа'))?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username',[
        'template' => "{input}\n{label}\n{hint}\n{error}",
        'options'=>['class'=>'form-group form-material floating'],
        'labelOptions'=>['class'=>'floating-label'],
    ])->textInput() ?>

    <?= $form->field($model, 'password',[
        'template' => "{input}\n{label}\n{hint}\n{error}",
        'options'=>['class'=>'form-group form-material floating'],
        'labelOptions'=>['class'=>'floating-label'],
	])->passwordInput() ?>

    <?= $form->field($model, 'rememberMe',[
        'template' => "<div class=\"checkbox-custom checkbox-inline checkbox-primary pull-left\">\n{input}\n{beginLabel}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>",
    ])->checkbox([], false) ?>
    
	<?=Html::a(Yii::t('app', 'Забыли пароль?'),['request-password-reset', 'continue' => $continue],['class'=>'pull-right'])?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Войти'), ['class' => 'btn btn-block btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?=Html::tag('p',Html::a(Yii::t('app', 'Зарегистрироваться'),['signup', 'continue' => $continue]))?>
