<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
/* @var $continue string|null */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->layout = 'login';
?>

<?=Html::tag('p',Yii::t('app', 'Регистрация в системе'))?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username',[
        'template' => "{input}\n{label}\n{hint}\n{error}",
        'options'=>['class'=>'form-group form-material floating'],
        'labelOptions'=>['class'=>'floating-label'],
    ])->textInput() ?>

    <?= Yii::$app->session['invite_team_id'] ? '' : $form->field($model, 'team',[
        'template' => "{input}\n{label}\n{hint}\n{error}",
        'options'=>['class'=>'form-group form-material floating'],
        'labelOptions'=>['class'=>'floating-label'],
    ])->textInput() ?>

    <?= $form->field($model, 'email',[
        'template' => "{input}\n{label}\n{hint}\n{error}",
        'options'=>['class'=>'form-group form-material floating'],
        'labelOptions'=>['class'=>'floating-label'],
    ])->textInput() ?>

    <?= $form->field($model, 'password',[
        'template' => "{input}\n{label}\n{hint}\n{error}",
        'options'=>['class'=>'form-group form-material floating'],
        'labelOptions'=>['class'=>'floating-label'],
    ])->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Зарегистрироваться'), ['class' => 'btn btn-block btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?=Html::tag('p',Html::a(Yii::t('app', 'Войти'),['login', 'continue' => $continue]))?>
