<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Контрагенты');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');

$fields = [];
foreach ($model->team->fields as $key => $value) {
    $fields[] = [
        'attribute' => $value['fields']['id']['value'],
        'label' => $value['fields']['label']['value'],
        'format' => 'text',
        'value' => function ($data) use ($value) {
            return $data->json($value['fields']['id']['value']);
        },
    ];
}
?>

<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name'],
    'columns' => ArrayHelper::merge([
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'name',
            'label' => $model->getAttributeLabel('name'),
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['update', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute' => 'discount',
            'label' => $model->getAttributeLabel('discount'),
        ],

    ], $fields, [

        'created_at:datetime',
        'updated_at:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ]),
]); ?>
