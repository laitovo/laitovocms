<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\team\Team */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Контрагенты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
