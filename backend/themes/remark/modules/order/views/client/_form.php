<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Region;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Json;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\team\Team */
/* @var $form yii\widgets\ActiveForm */

$js1 = '';
$js2 = '';
foreach ($model->organization_fields as $key => $value) {
    $js1 .= '$(".organizationmodalform .organization-' . $value['fields']['id']['value'] . '").val($(this).parent().find("input[name=\'' . $value['fields']['id']['value'] . '\']").val());';
    $js2 .= '$(".organizationitems .newitem input[name=\'' . $value['fields']['id']['value'] . '\']").val($(".organizationmodalform .organization-' . $value['fields']['id']['value'] . '").val());';
}

Yii::$app->view->registerJs('

    function parseitemsjson () {
        var organization =[];
        $(\'.organizationitems .item\').each(function( index, value ) {
            itemjson=$(this).find("input").serializeArray();
            var obj ={};
            $.each(itemjson, function( index1, value1 ) {
                obj[value1.name]=value1.value;
            });
            organization.push(obj);
        });
        $(\'#clientform-organization\').val(JSON.stringify(organization));
    }

    var edititem = null;

    $("body").on("click",".neworganization",function(e){
        window.edititem = null;
        $(".organizationmodalform input").val("");
    });

    $("body").on("click",".editorganization",function(e){
        window.edititem = $(this).parent();
        $(".organizationmodalform input").val("");
        ' . $js1 . '
        $(".organizationmodalform .organization-id").val($(this).parent().find("input[name=\'id\']").val());
        $(".organizationmodalform .organization-name").val($(this).parent().find("input[name=\'name\']").val());
        $(".organizationmodalform").modal("show") 
        e.preventDefault();
    });

    $("body").on("submit",".saveorganization",function(e){
        ' . $js2 . '
        $(".organizationitems .newitem input[name=\'id\']").val($(".organizationmodalform .organization-id").val());
        $(".organizationitems .newitem input[name=\'name\']").val($(".organizationmodalform .organization-name").val());
        $(".organizationitems .newitem input[name=\'name\']").prev().text($(".organizationmodalform .organization-name").val());

        if (window.edititem){
            $(".organizationitems .newitem").clone().insertAfter(window.edititem).removeClass("hidden newitem").addClass("item");
            window.edititem.remove();
        } else {
            $(".organizationitems .newitem").clone().appendTo(".organizationitems").removeClass("hidden newitem").addClass("item");
        }

        parseitemsjson();
        $(".organizationmodalform").modal("hide") 
        e.preventDefault();
    });

    $("body").on("click",".organizationitems .deletethisitem",function(e){
        var item = $(this).parent();
        notie.confirm("' . Yii::t('app', 'Удалить организацию?') . '", "' . Yii::t('yii', 'Yes') . '", "' . Yii::t('yii', 'No') . '", function() {
            item.remove();
            parseitemsjson();
        });
        e.preventDefault();
    });
    ', \yii\web\View::POS_END);
?>

<?php $form = ActiveForm::begin(['id' => 'clientform-form']); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'country')->dropDownList(
            ArrayHelper::merge(['' => ''], ArrayHelper::map(Region::find()->where(['parent_id' => null])->asArray()->all(), 'id', 'name')),
            ['onchange' => '$.get("' . Url::to(['/ajax/regions']) . '",{"region":$(this).val()},function(data){
                $("#clientform-region").html("<option></option>");
                $("#clientform-city").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#clientform-region").append($("<option></option>").attr("value",key).text(value));
                });
            })']
        ) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'region')->dropDownList(
            ArrayHelper::merge(['' => ''], ArrayHelper::map(Region::find()->where(['and', ['parent_id' => $model->country], ['not', ['parent_id' => null]]])->asArray()->all(), 'id', 'name')),
            ['onchange' => '$.get("' . Url::to(['/ajax/regions']) . '",{"region":$(this).val()},function(data){
                $("#clientform-city").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#clientform-city").append($("<option></option>").attr("value",key).text(value));
                });
            })']
        ) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'city')->dropDownList(
            ArrayHelper::merge(['' => ''], ArrayHelper::map(Region::find()->where(['and', ['parent_id' => $model->region], ['not', ['parent_id' => null]]])->asArray()->all(), 'id', 'name'))
        ) ?>
    </div>
</div>

<?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

<?= FormBuilder::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'fields',
    'render' => $model->team->fieldsrender,
]); ?>

<h4><?= Yii::t('app', 'Реквизиты') ?></h4>
<?= $form->field($model, 'organization')->hiddenInput()->label(false) ?>


<div class="organizationitems">
    <div class="newitem hidden">
        <a href="#" class="editorganization"></a>
        <input type="hidden" name="name">
        <input type="hidden" name="id">
        <? foreach ($model->organization_fields as $key => $value): ?>
            <input type="hidden" name="<?= $value['fields']['id']['value'] ?>">
        <? endforeach ?>
        <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
    </div>
    <? foreach ($model->team->organizations as $org):
        $orginfo = Json::decode($org->json ? $org->json : '{}');
        ?>
        <div class="item">
            <a href="#" class="editorganization"><?= Html::encode($org->name) ?></a>
            <input type="hidden" name="name" value="<?= Html::encode($org->name); ?>">
            <input type="hidden" name="id" value="<?= $org->id ?>">
            <? foreach ($model->organization_fields as $key => $value):?>
                <input type="hidden" name="<?= $value['fields']['id']['value'] ?>"
                       value="<?= isset($orginfo[$value['fields']['id']['value']]) ? $orginfo[$value['fields']['id']['value']] : null ?>">
            <? endforeach ?>
            <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
        </div>
    <? endforeach ?>
</div>

<button type="button" class="neworganization btn btn-icon btn-round btn-default btn-outline" data-toggle="modal"
        data-target="#organization-form"><i class="icon wb-plus"></i></button>

<p>&nbsp;</p>
<div class="form-group">
    <?= Html::submitButton($model->team->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    <?= Html::submitButton(($model->team->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить')) . Yii::t('app', ' и закрыть'), ['class' => 'btn btn-outline btn-round btn-primary', 'name' => 'redirect', 'value' => 'index']) ?>
    <?= Html::a(Yii::t('app', 'Отмена'), ['index'], ['class' => 'btn btn-outline btn-round  btn-default ']) ?>
    <?= $model->team->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->team->id], [
        'class' => 'btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>

</div>

<?php ActiveForm::end(); ?>

<?php Modal::begin([
    'id' => 'organization-form',
    // 'clientOptions'=>['show'=>$invite->hasErrors() ? true : false],
    'options' => ['class' => 'organizationmodalform fade modal'],
    'header' => Html::tag('h4', Yii::t('app', 'Реквизиты организации'), ['class' => 'modal-title']),
    // 'toggleButton' => ['class'=>'neworganization btn btn-icon btn-round btn-default btn-outline','label' => '<i class="icon wb-plus"></i>'],
]); ?>
<form class="saveorganization">

    <div class="form-group">
        <label class="control-label" for="organization-name"><?= Yii::t('app', 'Наименование') ?></label>
        <input type="text" required="required" value="" class="required form-control organization-name"
               name="organization-name">
        <input type="hidden" class="organization-id" name="organization-id">
    </div>

    <? foreach ($model->organization_fields as $key => $value): ?>
        <div class="form-group">
            <label class="control-label"
                   for="organization-<?= $value['fields']['id']['value'] ?>"><?= $value['fields']['label']['value'] ?></label>
            <input type="text" value=""
                   class="required form-control organization-<?= $value['fields']['id']['value'] ?>"
                   name="organization-<?= $value['fields']['id']['value'] ?>">
        </div>
    <? endforeach ?>

    <?= Html::submitButton(Yii::t('app', 'Готово'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>

</form>

<?php Modal::end(); ?>


