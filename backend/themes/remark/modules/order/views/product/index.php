<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use backend\widgets\GridView;
use backend\assets\tree\TreeAsset;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

TreeAsset::register($this);
$this->title = Yii::t('app', 'Товары');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->params['searchmodelaction'] = ['index', 'categories' => Yii::$app->request->get('categories')];
$this->render('../menu');

$fields = [];
foreach ($model->product->fields as $key => $value) {
    $fields[] = [
        'attribute' => $value['fields']['id']['value'],
        'label' => $value['fields']['label']['value'],
        'format' => 'text',
        'value' => function ($data) use ($value) {
            return $data->json($value['fields']['id']['value']);
        },
    ];
}

$checked = explode(',', Yii::$app->request->get('categories'));
$data = [];
foreach (Yii::$app->team->getTeam()->getCategories()->andWhere(['parent_id' => null])->orderBy('sort')->all() as $row) {
    $data[] = [
        'text' => Html::encode($row->name),
        'selectable' => false,
        'href' => $row->id,
        'state' => [
            'checked' => in_array($row->id, $checked) ? true : false,
        ],
        'nodes' => subcat($row, $checked),
    ];
}
function subcat($cat, $checked)
{
    $data = null;
    foreach ($cat->getCategories()->orderBy('sort')->all() as $row) {
        $data[] = [
            'text' => Html::encode($row->name),
            'selectable' => false,
            'href' => $row->id,
            'state' => [
                'checked' => in_array($row->id, $checked) ? true : false,
            ],
            'nodes' => subcat($row, $checked),
        ];
    }
    return $data;
}

Yii::$app->view->registerJs("
    var treeview = $('#tree').treeview({
        data: " . Json::encode($data) . ",
        showCheckbox: true,
        onNodeChecked: function(event, node) {
            var categories=JSON.parse('['+$('#treevalue').val()+']');
            categories.push(node.href);
            $('#treevalue').val(JSON.stringify(categories).replace('[','').replace(']',''));
            $('#treevalueform').submit();
        },
        onNodeUnchecked: function (event, node) {
            var categories=JSON.parse('['+$('#treevalue').val()+']');
            for (var key in categories) {
                if (categories[key] == node.href) {
                    categories.splice(key, 1);
                }
            }
            $('#treevalue').val(JSON.stringify(categories).replace('[','').replace(']',''));
            $('#treevalueform').submit();
        }
    });
");
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>
<!-- &nbsp;
<a class="btn btn-icon btn-outline btn-round btn-default" data-toggle="collapse" href="#exampleCollapseExample" aria-expanded="false" aria-controls="exampleCollapseExample">
<i class="fa fa-search"></i>
</a>

<div class="collapse" id="exampleCollapseExample">
</div>
 -->
<div class="row">
    <div class="col-sm-3">
        <div id="tree"></div>
    </div>
    <div class="col-sm-9">

        <?php Pjax::begin(['timeout' => 5000]); ?>

        <?php $form = ActiveForm::begin([
            'options' => ['data-pjax' => ''],
            'action' => ['index', 'search' => Yii::$app->request->get('search')],
            'method' => 'get',
            'id' => 'treevalueform',
        ]); ?>
        <input type="hidden" id="treevalue" name="categories" value='<?= Yii::$app->request->get('categories') ?>'>

        <?php ActiveForm::end(); ?>


        <?= GridView::widget([
            'id' => 'product-grid',
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $dataProvider,
            'show' => ['name', 'article'],
            'columns' => ArrayHelper::merge([
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                    'label' => $model->getAttributeLabel('name'),
                    'format' => 'html',
                    'value' => function ($data) {
                        return $data->name ? Html::a(Html::encode($data->name), ['update', 'id' => $data->id]) : null;
                    },
                ],
                [
                    'attribute' => 'article',
                    'label' => $model->getAttributeLabel('article'),
                ],
                [
                    'attribute' => 'unit',
                    'label' => $model->getAttributeLabel('unit'),
                ],
                [
                    'attribute' => 'quantity',
                    'format' => 'decimal',
                    'label' => $model->getAttributeLabel('quantity'),
                ],
                [
                    'attribute' => 'prices.price',
                    'label' => $model->getAttributeLabel('price'),
                    'format' => 'currency',
                    'value' => function ($data) {
                        return $data->prices ? $data->prices[0]->price() : null;
                    },
                ],

            ], $fields, [

                'created_at:datetime',
                'updated_at:datetime',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttonOptions' => ['class' => 'deleteconfirm'],
                ],
            ]),
        ]); ?>

        <?php Pjax::end(); ?>
    </div>
</div>