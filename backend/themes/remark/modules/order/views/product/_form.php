<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\assets\tree\TreeAsset;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model backend\modules\order\models\ProductForm */
/* @var $form yii\widgets\ActiveForm */

TreeAsset::register($this);
Yii::$app->view->registerJs('
    $("#productform-unit").autocomplete({
        source: ' . Json::encode($model->units) . ',
        minLength: 0
    });
    $("#productform-unit").bind("focus", function(){ $(this).autocomplete("search"); } );
');

$checked = Json::decode($model->categories);
$data = [];
foreach (Yii::$app->team->getTeam()->getCategories()->andWhere(['parent_id' => null])->orderBy('sort')->all() as $row) {
    $data[] = [
        'text' => Html::encode($row->name),
        'selectable' => false,
        'href' => $row->id,
        'state' => [
            'checked' => in_array($row->id, $checked) ? true : false,
        ],
        'nodes' => subcat($row, $checked),
    ];
}
function subcat($cat, $checked)
{
    $data = null;
    foreach ($cat->getCategories()->orderBy('sort')->all() as $row) {
        $data[] = [
            'text' => Html::encode($row->name),
            'selectable' => false,
            'href' => $row->id,
            'state' => [
                'checked' => in_array($row->id, $checked) ? true : false,
            ],
            'nodes' => subcat($row, $checked),
        ];
    }
    return $data;
}

Yii::$app->view->registerJs("
    var treeview = $('#tree').treeview({
        data: " . Json::encode($data) . ",
        showCheckbox: true,
        levels: 1,
        onNodeChecked: function(event, node) {
            var categories=JSON.parse($('#productform-categories').val());
            categories.push(node.href);
            $('#productform-categories').val(JSON.stringify(categories));
        },
        onNodeUnchecked: function (event, node) {
            var categories=JSON.parse($('#productform-categories').val());
            for (var key in categories) {
                if (categories[key] == node.href) {
                    categories.splice(key, 1);
                }
            }
            $('#productform-categories').val(JSON.stringify(categories));
        }
    });
");
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'oldprice')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'currency')->dropDownList($model->cur) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'nds')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'nds_in')->checkbox() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'quantity')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'maxdiscount')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<?= $form->field($model, 'categories')->hiddenInput() ?>

<div id="tree"></div>

<?= FormBuilder::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'fields',
    'render' => $model->product->fieldsrender,
]); ?>

<div class="form-group">
    <?= Html::submitButton($model->product->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    <?= Html::submitButton(($model->product->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить')) . Yii::t('app', ' и закрыть'), ['class' => 'btn btn-outline btn-round btn-primary', 'name' => 'redirect', 'value' => 'index']) ?>
    <?= Html::a(Yii::t('app', 'Отмена'), ['index'], ['class' => 'btn btn-outline btn-round  btn-default ']) ?>
    <?= $model->product->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->product->id], [
        'class' => 'btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>

</div>

<?php ActiveForm::end(); ?>
