<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use backend\widgets\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Заказы');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');

$fields = [];
foreach ($model->order->fields as $key => $value) {
    $fields[] = [
        'attribute' => $value['fields']['id']['value'],
        'label' => $value['fields']['label']['value'],
        'format' => 'text',
        'value' => function ($data) use ($value) {
            return $data->json($value['fields']['id']['value']);
        },
    ];
}

Yii::$app->view->registerJs('
    $("#loadorder-file").change(function(){
        $("#loadorderfileform").submit();
    });
');
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'pull-left btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?php $form = ActiveForm::begin(['id' => 'loadorderfileform', 'enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($load, 'file', ['template' => "<div class='input-group input-group-file pull-left'>&nbsp;<span class='btn btn-file btn-icon btn-outline btn-round btn-primary' data-toggle='tooltip' data-original-title='" . Yii::t('app', 'Загрузить из файла') . " (*.csv)'><i class='icon wb-upload'></i> Загрузить {input}</span></div>", 'options' => ['class' => '']])->fileInput()->label(false) ?>

<?php ActiveForm::end() ?>



<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['created_at', 'number', 'sum'],
    'columns' => ArrayHelper::merge([
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'created_at',
            'format' => 'datetime',
            'label' => $model->getAttributeLabel('created_at'),
        ],
        [
            'attribute' => 'number',
            'label' => $model->getAttributeLabel('number'),
            'format' => 'html',
            'value' => function ($data) {
                return $data->number ? Html::a(Yii::t('app', 'Заказ №{number}', ['number' => $data->number]), ['update', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute' => 'quantity',
            'format' => 'decimal',
            'label' => $model->getAttributeLabel('quantity'),
        ],
        [
            'attribute' => 'sum',
            'format' => 'raw',
            'label' => $model->getAttributeLabel('sum'),
            'value' => function ($data) {
                return Yii::$app->formatter->asCurrency($data->sum ?: 0, @Json::decode($data->json)['currency']);
            },
        ],
        [
            'attribute' => 'sum1',
            'format' => 'raw',
            'label' => $model->getAttributeLabel('sum1'),
            'value' => function ($data) {
                return Yii::$app->formatter->asCurrency($data->getSum(true) ?: 0, @Json::decode($data->json)['currency']);
            },
        ],
        [
            'attribute' => 'user_id',
            'label' => $model->getAttributeLabel('user'),
            'value' => function ($data) {
                return $data->user ? $data->user->name : null;
            },
        ],
        [
            'attribute' => 'organization_id',
            'label' => $model->getAttributeLabel('organization'),
            'value' => function ($data) {
                return $data->organization ? $data->organization->name : null;
            },
        ],
        [
            'attribute' => 'client_team_id',
            'label' => $model->getAttributeLabel('clientteam'),
            'value' => function ($data) {
                return $data->clientTeam ? $data->clientTeam->name : null;
            },
        ],
        [
            'attribute' => 'client_organization_id',
            'label' => $model->getAttributeLabel('clientorganization'),
            'value' => function ($data) {
                return $data->clientOrganization ? $data->clientOrganization->name : null;
            },
        ],
        [
            'attribute' => 'manager_id',
            'label' => $model->getAttributeLabel('manager'),
            'value' => function ($data) {
                return $data->manager ? $data->manager->name : null;
            },
        ],
        [
            'attribute' => 'discount',
            'label' => $model->getAttributeLabel('discount'),
            'format' => 'percent',
            'value' => function ($data) {
                return $data->discount ? $data->discount / 100 : null;
            },
        ],
        [
            'attribute' => 'nds',
            'label' => $model->getAttributeLabel('nds'),
            'format' => 'decimal',
            'value' => function ($data) {
                return @Json::decode($data->json)['nds'];
            },
        ],
        [
            'attribute' => 'currency',
            'label' => $model->getAttributeLabel('currency'),
            'value' => function ($data) {
                return @Json::decode($data->json)['currency'];
            },
        ],

    ], $fields, [

        'updated_at:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ]),
]); ?>
