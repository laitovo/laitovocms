<!doctype html>
<html>
<body>

<table width="100%">
    <tr>
        <td colspan="2" class="text-left">ООО "Автофильтр"</td>
    </tr>
    <tr>
        <td colspan="2" class="text-left">Адрес: 156007, Костромская обл, Кострома г, Пушкина ул, д. 43/102, <br>
            тел.: (4942) 55-0991, 55-0821
        </td>
    </tr>
    <tr>
        <td colspan="2" class="text-center">
            <div style="text-align:center;  font-weight:bold;">
                Образец заполнения платежного поручения
            </div>
        </td>
    </tr>
</table>


<table width="100%" cellpadding="2" cellspacing="2" class="invoice_bank_rekv">
    <tr>
        <td colspan="2" rowspan="2" style="min-height:13mm; width: 105mm;border: 1px solid;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 13mm;">
                <tr>
                    <td valign="top">
                        <div>
                            КОСТРОМСКОЕ ОТДЕЛЕНИЕ N 8640 ПАО СБЕРБАНК Г.КОСТРОМА
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" style="height: 3mm;">
                        <div style="font-size:10pt;">Банк получателя</div>
                    </td>
                </tr>
            </table>
        </td>
        <td style="min-height:7mm;height:auto; width: 25mm;border: 1px solid;">
            <div>БИK</div>
        </td>
        <td rowspan="2" style="vertical-align: top; width: 60mm;border: 1px solid;">
            <div style=" height: 7mm; line-height: 7mm; vertical-align: middle;">043469623</div>
            <div>30101810200000000623</div>
        </td>
    </tr>
    <tr>
        <td style="width: 25mm;border: 1px solid;">
            <div>Сч. №</div>
        </td>
    </tr>
    <tr>
        <td style="min-height:6mm; height:auto; width: 50mm;border: 1px solid;">
            <div>ИНН 4442015755</div>
        </td>
        <td style="min-height:6mm; height:auto; width: 55mm;border: 1px solid;">
            <div>КПП 440101001</div>
        </td>
        <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 25mm;border: 1px solid;">
            <div>Сч. №</div>
        </td>
        <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 60mm;border: 1px solid;">
            <div>40702810029010113361</div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="min-height:13mm; height:auto;border: 1px solid;">

            <table border="0" cellpadding="0" cellspacing="0" style="height: 13mm; width: 105mm;">
                <tr>
                    <td valign="top">
                        <div>ООО Костромское предприятие "Автофильтр"</div>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" style="height: 3mm;">
                        <div style="font-size: 10pt;">Получатель</div>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<br/>

<div style="font-weight: bold; font-size: 16pt; padding-left:5px;">
    Счет № <?= $model->order->number ?> от <?= Yii::$app->formatter->asDate($model->order->created_at) ?></div>
<br/>

<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>

<table width="100%">
    <tr>
        <td style="width: 30mm;">
            <div style=" padding-left:2px;">Поставщик:</div>
        </td>
        <td>
            <div style="font-weight:bold;  padding-left:2px;">
                <?= @$model->order->organization ? @$model->order->organization->name : @$model->order->team->name ?>
            </div>
        </td>
    </tr>
    <tr>
        <td style="width: 30mm;">
            <div style=" padding-left:2px;">Покупатель:</div>
        </td>
        <td>
            <div style="font-weight:bold;  padding-left:2px;">
                <?= @$model->order->clientOrganization ? @$model->order->clientOrganization->name : @$model->order->clientTeam->name ?>
            </div>
        </td>
    </tr>
</table>


<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <thead>
    <tr>
        <th style="width:13mm;">№</th>
        <th style="width:20mm;">Код</th>
        <th>Товар</th>
        <th style="width:20mm;">Кол-во</th>
        <th style="width:17mm;">Ед.</th>
        <th style="width:27mm;">Цена</th>
        <th style="width:27mm;">Сумма</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($model->order->items as $key => $item): ?>
        <tr>
            <td align="center"><?= $key + 1 ?></td>
            <td align="left"></td>
            <td align="left"><?= $item->name ?></td>
            <td align="right"><?= Yii::$app->formatter->asDecimal($item->quantity) ?></td>
            <td align="left"><?= $item->product->unit ?></td>
            <td align="right"><?= Yii::$app->formatter->asCurrency($item->price(false)) ?></td>
            <td align="right">
                <?= Yii::$app->formatter->asCurrency($item->price(false) * $item->quantity) ?>
                <? $sum += round($item->price(false) * $item->quantity, 2) ?>
                <? $nds += round(($item->price(true) - $item->price(false)) * $item->quantity, 2) ?>
            </td>
        </tr>
    <? endforeach ?>
    </tbody>
</table>

<table border="0" width="100%" cellpadding="1" cellspacing="1">
    <tr>
        <td></td>
        <td style="width:27mm; font-weight:bold;  text-align:right;">Итого:</td>
        <td style="width:27mm; font-weight:bold;  text-align:right;"><?= Yii::$app->formatter->asCurrency($sum) ?></td>
    </tr>
    <? if ($nds): ?>
        <tr>
            <td colspan="2" style="width:27mm; font-weight:bold;  text-align:right;">НДС:</td>
            <td style="width:27mm; font-weight:bold;  text-align:right;"><?= Yii::$app->formatter->asCurrency($nds) ?></td>
        </tr>
        <tr>
            <td colspan="2" style="width:27mm; font-weight:bold;  text-align:right;">Всего:</td>
            <td style="width:27mm; font-weight:bold;  text-align:right;"><?= Yii::$app->formatter->asCurrency($sum + $nds) ?></td>
        </tr>
    <? endif ?>
</table>

<br/>
<div>
    Скидка <?= Yii::$app->formatter->asPercent($model->order->discount / 100) ?><br/>
    Всего наименований <?= count($model->order->items) ?> на сумму <?= Yii::$app->formatter->asCurrency($sum + $nds) ?>
    <br/>
    <span><?= num2str($sum + $nds) ?></span>
</div>
<br/><br/>
<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>
<br/>

<div>Руководитель ______________________ (Поляков С.С.)</div>
<br/>

<div>Главный бухгалтер ______________________ (Екимова Н.М.)</div>
<br/>

<div style="width: 85mm;text-align:center;">М.П.</div>
<br/>


</body>
</html>

<?
/**
 * Возвращает сумму прописью
 * @author runcore
 * @uses morph(...)
 */
function num2str($num)
{
    $nul = 'ноль';
    $ten = array(
        array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
    );
    $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
    $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
    $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
    $unit = array( // Units
        array('копейка', 'копейки', 'копеек', 1),
        array('рубль', 'рубля', 'рублей', 0),
        array('тысяча', 'тысячи', 'тысяч', 1),
        array('миллион', 'миллиона', 'миллионов', 0),
        array('миллиард', 'милиарда', 'миллиардов', 0),
    );
    //
    list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub) > 0) {
        foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit) - $uk - 1; // unit key
            $gender = $unit[$uk][3];
            list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
            else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk > 1) $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
        } //foreach
    } else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
    $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5)
{
    $n = abs(intval($n)) % 100;
    if ($n > 10 && $n < 20) return $f5;
    $n = $n % 10;
    if ($n > 1 && $n < 5) return $f2;
    if ($n == 1) return $f1;
    return $f5;
}

?>
