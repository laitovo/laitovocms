<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <thead>
    <tr>
        <th style="width:13mm;">№</th>
        <th>Товар</th>
        <th style="width:20mm;">Кол-во</th>
        <th style="width:17mm;">Ед.</th>
        <th style="width:27mm;">Цена без НДС</th>
        <th style="width:27mm;">Сумма</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($model->order->items as $key => $item): ?>
        <tr>
            <td align="center"><?= $key + 1 ?></td>
            <? if ($nds): ?>
                <td align="left"><?= $item->product ? ($item->product->json('fullname') ?: $item->name) : $item->name ?></td>
            <? else: ?>
                <td align="left"><?= $item->product ? ($item->product->json('nameexport') ?: $item->name) : $item->name ?></td>
            <? endif ?>
            <td align="right"><?= Yii::$app->formatter->asDecimal($item->quantity) ?></td>
            <td align="left"><?= @$item->product->unit ?></td>
            <td align="right"><?= Yii::$app->formatter->asCurrency($item->price(false)) ?></td>
            <td align="right">
                <?= Yii::$app->formatter->asCurrency($item->price(false) * $item->quantity) ?>
                <? $sum += round($item->price(false) * $item->quantity, 2) ?>
                <? $nds += round(($item->price(true) - $item->price(false)) * $item->quantity, 2) ?>
            </td>
        </tr>
    <? endforeach ?>
    </tbody>
</table>

<table border="0" width="100%" cellpadding="1" cellspacing="1">
    <tr>
        <td></td>
        <td style="width:27mm; font-weight:bold;  text-align:right;">Итого:</td>
        <td style="width:27mm; font-weight:bold;  text-align:right;"><?= Yii::$app->formatter->asCurrency($sum) ?></td>
    </tr>
    <tr>
        <td colspan="2" style="width:27mm; font-weight:bold;  text-align:right;">
            НДС <?= Yii::$app->formatter->asPercent($model->nds / 100) ?>:
        </td>
        <td style="width:27mm; font-weight:bold;  text-align:right;"><?= Yii::$app->formatter->asCurrency($nds) ?></td>
    </tr>
    <tr>
        <td colspan="2" style="width:27mm; font-weight:bold;  text-align:right;">Всего к оплате:</td>
        <td style="width:27mm; font-weight:bold;  text-align:right;"><?= Yii::$app->formatter->asCurrency($sum + $nds) ?></td>
    </tr>
</table>

<br/>
<div>
    Скидка <?= Yii::$app->formatter->asPercent($model->order->discount / 100) ?><br/>
    Всего наименований <?= count($model->order->items) ?> на сумму <?= Yii::$app->formatter->asCurrency($sum + $nds) ?>
    <br/>
    <span><?= num2str($sum + $nds) ?></span>
</div>
<br/><br/>
<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>
<br/>

<div>Руководитель ______________________ (Поляков С.С.)</div>
<br/>

<div>Главный бухгалтер ______________________ (Екимова Н.М.)</div>
<br/>

<div style="width: 85mm;text-align:center;">М.П.</div>
<br/>

