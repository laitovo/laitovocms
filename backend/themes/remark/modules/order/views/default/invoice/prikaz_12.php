<div style="font-weight: bold; font-size: 12pt; padding-left:5px;">
    Приказ на отпуск товаров к накладной № <?= $model->order->number ?>
    от <?= Yii::$app->formatter->asDate($model->order->created_at) ?></div>
<br/>

<table width="100%">
    <tr>
        <td style="">
            <div style=" padding-left:2px;">Железная дорога:</div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">Станция назначения:</div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">Код станции:</div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">
                Получатель: <?= @$model->order->clientOrganization ? @$model->order->clientOrganization->name : @$model->order->clientTeam->name ?></div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">Код получателя:</div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">№ контейнера:</div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">ОКПО:</div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">Почтовый
                адрес: <?= $model->order->clientTeam ? $model->order->clientTeam->json('postadres') : '' ?></div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">Менеджер: <?= @$model->order->manager->name ?></div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style=" padding-left:2px;">Склад:</div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div style="font-weight: bold; padding-left:2px;">Прочее: <?= @$model->order->json('coment') ?></div>
        </td>
    </tr>
</table>
<br>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <thead>
    <tr>
        <th style="width:13mm;">№</th>
        <th>Наименование</th>
        <th style="width:20mm;">Всего (шт.)</th>
        <th style="width:17mm;">Коробок</th>
        <th style="width:17mm;">В коробке</th>
        <th style="width:17mm;">Россыпью</th>
        <th style="width:27mm;">Нетто</th>
        <th style="width:27mm;">Объем</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($model->order->items as $key => $item): ?>
        <?
        $total += $item->quantity;

        $vkorob = $item->product ? (float)$item->product->json('vkorobke') : null;

        $korob = $item->product && $vkorob ? (int)($item->quantity / $vkorob) : null;
        $korobok += $korob;

        $rossip = $item->quantity - $korob * $vkorob ?: 0;

        $mass = $item->product ? (float)$item->product->json('mass') * $item->quantity : null;
        $summass += $mass;

        $objem = $item->product && $vkorob ? (float)$item->product->json('obemkor') / $vkorob * $item->quantity : null;
        $sumobjem += $objem;
        ?>
        <tr>
            <td align="center"><?= $key + 1 ?></td>
            <td align="left"><?= $item->name ?></td>
            <td align="right"><?= Yii::$app->formatter->asDecimal($item->quantity) ?></td>
            <td align="right"><?= Yii::$app->formatter->asDecimal($korob) ?></td>
            <td align="right"><?= Yii::$app->formatter->asDecimal($vkorob) ?></td>
            <td align="right"><?= $rossip ? Yii::$app->formatter->asDecimal($rossip) : '' ?></td>
            <td align="right"><?= Yii::$app->formatter->asDecimal($mass) ?></td>
            <td align="right"><?= Yii::$app->formatter->asDecimal($objem) ?></td>
        </tr>
    <? endforeach ?>
    </tbody>
    <tr>
        <th align="left" colspan="2">Итого</th>
        <th align="right"><?= Yii::$app->formatter->asDecimal($total) ?></th>
        <th align="right"><?= Yii::$app->formatter->asDecimal($korobok) ?></th>
        <th align="right"></th>
        <th align="right"></th>
        <th align="right"><?= Yii::$app->formatter->asDecimal($summass) ?></th>
        <th align="right"><?= Yii::$app->formatter->asDecimal($sumobjem) ?></th>
    </tr>
</table>


<br/>
<br/>

<div>Отгрузку разрешил ______________________ (А.Г. Митров)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Отгрузку
    произвел ______________________
</div>
<br/>

<div>Груз принял Водитель ___________________________________ Мест _____________</div>
<br/>

<div style="width: 85mm;text-align:center;">М.П.</div>
<br/>

