<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\widgets\ActiveForm;
use common\assets\twitter_cldr\TwitterCldrAsset;
use backend\themes\remark\assets\FormAsset;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\assets\tree\TreeAsset;
use common\assets\toastr\ToastrAsset;
use common\models\team\Team;
use common\models\order\Product;
use common\models\User;
use common\models\Region;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model backend\modules\order\models\OrderForm */
/* @var $form yii\widgets\ActiveForm */

$items = Json::decode($model->items);

TwitterCldrAsset::register($this);
FormAsset::register($this);
TreeAsset::register($this);
ToastrAsset::register($this);

$checked = explode(',', Yii::$app->request->get('categories'));
$data = [];
foreach (Yii::$app->team->getTeam()->getCategories()->andWhere(['parent_id' => null])->orderBy('sort')->all() as $row) {
    $data[] = [
        'text' => Html::encode($row->name),
        'selectable' => false,
        'href' => $row->id,
        'state' => [
            'checked' => in_array($row->id, $checked) ? true : false,
        ],
        'nodes' => subcat($row, $checked),
    ];
}
function subcat($cat, $checked)
{
    $data = null;
    foreach ($cat->getCategories()->orderBy('sort')->all() as $row) {
        $data[] = [
            'text' => Html::encode($row->name),
            'selectable' => false,
            'href' => $row->id,
            'state' => [
                'checked' => in_array($row->id, $checked) ? true : false,
            ],
            'nodes' => subcat($row, $checked),
        ];
    }
    return $data;
}

Yii::$app->view->registerJs("
    var treeview = $('#tree').treeview({
        data: " . Json::encode($data) . ",
        showCheckbox: true,
        onNodeChecked: function(event, node) {
            var categories=JSON.parse('['+$('#treevalue').val()+']');
            categories.push(node.href);
            $('#treevalue').val(JSON.stringify(categories).replace('[','').replace(']',''));
            $('#treevalueform').submit();
        },
        onNodeUnchecked: function (event, node) {
            var categories=JSON.parse('['+$('#treevalue').val()+']');
            for (var key in categories) {
                if (categories[key] == node.href) {
                    categories.splice(key, 1);
                }
            }
            $('#treevalue').val(JSON.stringify(categories).replace('[','').replace(']',''));
            $('#treevalueform').submit();
        }
    });
");

Yii::$app->view->registerJs('
    var fmt = new TwitterCldr.CurrencyFormatter();
    $(document).ready(function() {
        $(\'.editable-inline\').editable({
            mode: "inline",
            inputclass: "form-control",
            emptytext:"Не задано",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
            }
        });
        $(\'.editable-popup\').editable({
            mode: "popup",
            emptytext:"Не задано",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
            }
        });

        $(\'#tbody-items .name-editable\').editable({
            emptytext:"Укажите название товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .price-editable\').editable({
            emptytext:"Укажите цену",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .quantity-editable\').editable({
            emptytext:"Укажите кол-во",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
    });

    function parseitemsjson () {
        var items =[];
        var totalcount =0;
        var totalsum =0;
        var positions =0;
        $(\'#tbody-items .items-order\').each(function( index, value ) {
            positions++;
            $(this).find(\'td:first\').text(positions);
            itemjson=$(this).find(\'.itemjson\').serializeArray();
            var obj ={};
            $.each(itemjson, function( index1, value1 ) {
                obj[value1.name]=value1.value;
            });
            sum=parseFloat(parseFloat(obj.price)*parseFloat(obj.quantity));
            totalsum=parseFloat(totalsum+sum);
            totalcount=parseFloat(totalcount+parseFloat(obj.quantity));
            items.push(obj);
            $(this).find(\'.item-sum\').text(fmt.format(sum, {currency: $("#orderform-currency").val() }));
        });
        $(\'#orderform-items\').val(JSON.stringify(items));
        $(\'#orderform-totalcount\').text(totalcount);
        $(\'#orderform-totalsum\').text(fmt.format(totalsum, {currency: $("#orderform-currency").val() }));
    }
    $("body").on("change","#tbody-items .items-order .itemjson[name=\'lock\']",function(e){
        parseitemsjson();
        e.preventDefault();
    });
    $("body").on("click","#tbody-items .items-order .delete-orderitem",function(e){
        $(this).parent().parent().remove();
        parseitemsjson();
        e.preventDefault();
    });
    $("body").on("click","#add-orderitem",function(e){
        $("#add-item-modal").modal("show");
        e.preventDefault();
    });
    $("body").on("click",".add-orderproduct",function(e){
        $(".add-item-tr .itemjson[name=\'product_id\']").val($(this).attr("data-id"));
        $(".add-item-tr .itemjson[name=\'name\']").val($(this).attr("data-name"));
        $(".add-item-tr .name-editable").html($(this).attr("data-name"));
        $(".add-item-tr .itemjson[name=\'quantity\']").val($(this).parent().parent().find("input[name=\'countadd\']").val());
        $(".add-item-tr .quantity-editable").html($(this).parent().parent().find("input[name=\'countadd\']").val());

        $(".add-item-tr").clone().appendTo("#tbody-items");

        $(\'#tbody-items .add-item-tr .name-editable\').editable({
            emptytext:"Укажите название товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .price-editable\').editable({
            emptytext:"Укажите цену",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .quantity-editable\').editable({
            emptytext:"Укажите кол-во",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });

        $("#tbody-items .add-item-tr").removeClass("hidden add-item-tr");
        calc();
        toastr.success("Товар добавлен.");
        e.preventDefault();
    });
    function calc(){
        var country=$("select[name=\'OrderForm[country]\']").val();
        var region=$("select[name=\'OrderForm[region]\']").val();
        var city=$("select[name=\'OrderForm[city]\']").val();
        var team=$("select[name=\'OrderForm[clientteam]\']").val();
        var user=$("select[name=\'OrderForm[user]\']").val();
        var discount=$("input[name=\'OrderForm[discount]\']").val();

        $( "#tbody-items .items-order" ).each(function( index ) {
            var product=$( this ).find(".itemjson[name=\'product_id\']").val();
            var lock=$( this ).find(".itemjson[name=\'lock\']").is(":checked") ? 1 : 0;
            var item=$( this );
            if (product>0 && !lock){
                $.get("' . Url::to(['/ajax/price']) . '",{
                    "country":country ? country : 1,
                    "region":region,
                    "city":city,
                    "team":team,
                    "user":user,
                    "discount":discount,
                    "currency":$("#orderform-currency").val(),
                    "product":product
                },function(data){
                    item.find(".itemjson[name=\'price\']").val(data.price);
                    item.find(".itemjson[name=\'price\']").prev().editable("setValue",data.price);
                    parseitemsjson();
                })
            }
        });
    }
    ', \yii\web\View::POS_END);

?>

<?php $form = ActiveForm::begin(); ?>
<?= $form->errorSummary($model) ?>
<?php ActiveForm::end(); ?>


<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Товар</th>
            <th>Цена</th>
            <th>Кол-во</th>
            <th>кг</th>
            <th>м<sup>3</sup></th>
            <th>Сумма</th>
            <th>Фикс.</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="tbody-items">
        <?
        $summass = 0;
        $sumobjem = 0;
        $positions = 0;
        if ($items) foreach ($items as $key => $row):
            $product_id = isset($row['product_id']) ? $row['product_id'] : null;
            $product = Product::findOne($product_id);

            $price = isset($row['price']) ? $row['price'] : null;
            $quantity = isset($row['quantity']) ? (int)$row['quantity'] : null;
            $name = isset($row['name']) ? $row['name'] : '';
            $total = isset($total) ? $quantity + $total : $quantity;
            $sum = isset($sum) ? $quantity * $price + $sum : $quantity * $price;

            $mass = $product ? (float)$product->json('mass') * $quantity : null;
            $summass += $mass;

            $objem = $product && (float)$product->json('vkorobke') ? (float)$product->json('obemkor') / (float)$product->json('vkorobke') * $quantity : null;
            $sumobjem += $objem;
            $positions++;
            ?>
            <tr class="items-order">
                <td><?= $positions ?></td>
                <td>
                    <?= $product_id ? Html::a('<i class="wb wb-eye"></i>', ['product/update', 'id' => $product_id]) : '' ?>
                    <a href="#" class="name-editable" data-type="text"><?= $name ?></a>
                    <input class="itemjson" type="hidden" name="name" value='<?= $name ?>'>
                    <input class="itemjson" type="hidden" name="product_id" value='<?= $product_id ?>'>
                </td>
                <td>
                    <a href="#" class="price-editable" data-type="text"><?= $price ?></a>
                    <input class="itemjson" type="hidden" name="price" value='<?= $price ?>'>
                </td>
                <td>
                    <a href="#" class="quantity-editable" data-type="text"><?= $quantity ?></a>
                    <input class="itemjson" type="hidden" name="quantity" value='<?= $quantity ?>'>
                </td>
                <td>
                    <?= Yii::$app->formatter->asDecimal($mass) ?>
                </td>
                <td>
                    <?= Yii::$app->formatter->asDecimal($objem) ?>
                </td>
                <td class="item-sum"><?= Yii::$app->formatter->asCurrency($quantity * $price, $model->currency) ?></td>
                <td><?= Html::checkbox('lock', isset($row['lock']) ? $row['lock'] : 0, ['class' => 'itemjson']) ?></td>
                <td>
                    <a href="#" class="text-danger delete-orderitem"><i class="wb wb-close"></i></a>
                    <input class="itemjson" type="hidden" name="id"
                           value='<?= isset($row['id']) ? $row['id'] : null ?>'>
                    <input class="itemjson" type="hidden" name="retail"
                           value='<?= isset($row['retail']) ? $row['retail'] : null ?>'>
                    <input class="itemjson" type="hidden" name="properties"
                           value='<?= isset($row['properties']) ? $row['properties'] : '[]' ?>'>
                </td>
            </tr>
        <? endforeach ?>
        </tbody>
        <tr class="items-order hidden add-item-tr">
            <td></td>
            <td>
                <a href="#" class="name-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" name="name" value=''>
                <input class="itemjson" type="hidden" name="product_id" value='<?= null ?>'>
            </td>
            <td>
                <a href="#" class="price-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" name="price" value='0'>
            </td>
            <td>
                <a href="#" class="quantity-editable" data-type="text">1</a>
                <input class="itemjson" type="hidden" name="quantity" value='1'>
            </td>
            <td></td>
            <td></td>
            <td class="item-sum"><?= Yii::$app->formatter->asCurrency(0, $model->currency) ?></td>
            <td><?= Html::checkbox('lock', false, ['class' => 'itemjson']) ?></td>
            <td>
                <a href="#" class="text-danger delete-orderitem"><i class="wb wb-close"></i></a>
                <input class="itemjson" type="hidden" name="id" value='<?= null ?>'>
                <input class="itemjson" type="hidden" name="retail" value='<?= null ?>'>
                <input class="itemjson" type="hidden" name="properties" value='[]'>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="text-right lead"><?= Yii::t('app', 'Итого:') ?></td>
            <td class="lead" id="orderform-totalcount"><?= isset($total) ? $total : 0 ?></td>
            <td><?= Yii::$app->formatter->asDecimal($summass) ?></td>
            <td><?= Yii::$app->formatter->asDecimal($sumobjem) ?></td>
            <td class="" colspan="3">
                <div class="lead"
                     id="orderform-totalsum"><?= Yii::$app->formatter->asCurrency(isset($sum) ? $sum : 0, $model->currency) ?></div>
                <div>
                    Ндс: <?= Yii::$app->formatter->asCurrency($model->order->getSum(true) - $model->order->getSum(false) ?: 0, $model->currency) ?></div>
                <div>
                    Всего: <?= Yii::$app->formatter->asCurrency($model->order->getSum(true) ?: 0, $model->currency) ?></div>
            </td>
        </tr>
    </table>
</div>

<?= Html::tag('span', '<span class="icon wb-refresh"></span>', ['onclick' => 'calc();', 'class' => 'btn btn-icon btn-outline btn-round btn-default pull-right', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('yii', 'Пересчитать')]) ?>

<?php Modal::begin([
    'size' => Modal::SIZE_LARGE,
    'id' => 'add-item-modal',
    'header' => Html::tag('h4', Yii::t('app', 'Добавить товар'), ['class' => 'modal-title']),
    'toggleButton' => ['class' => 'btn btn-icon btn-round btn-default btn-outline', 'label' => '<i class="icon wb-plus"></i>'],
]); ?>

<div class="row">
    <div class="col-xs-4">
        <div id="tree"></div>
    </div>
    <div class="col-xs-8">

        <?php Pjax::begin(['timeout' => 5000]); ?>

        <?php $form = ActiveForm::begin([
            'options' => ['data-pjax' => ''],
            'action' => [Yii::$app->controller->action->id, 'id' => $model->order->id, 'categories' => Yii::$app->request->get('categories')],
            'method' => 'get',
        ]); ?>

        <div class="input-search input-search-dark pull-right"><i class="input-search-icon wb-search"
                                                                  aria-hidden="true"></i><input type="text"
                                                                                                class="form-control"
                                                                                                value="<?= Yii::$app->request->get('search') ?>"
                                                                                                name="search"
                                                                                                autofocus="autofocus"
                                                                                                placeholder="<?= Yii::t('app', 'Поиск') ?>">
        </div>

        <?php ActiveForm::end(); ?>

        <?php $form = ActiveForm::begin([
            'options' => ['data-pjax' => ''],
            'action' => [Yii::$app->controller->action->id, 'id' => $model->order->id, 'search' => Yii::$app->request->get('search')],
            'method' => 'get',
            'id' => 'treevalueform',
        ]); ?>
        <input type="hidden" id="treevalue" name="categories" value='<?= Yii::$app->request->get('categories') ?>'>

        <?php ActiveForm::end(); ?>

        <?= GridView::widget([
            'id' => 'product-grid',
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'countadd',
                    'label' => $model->getAttributeLabel('quantity'),
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::input('text', 'countadd', 1, ['size' => 5, 'onkeyup' => 'if(window.event.keyCode==13) $(this).parent().parent().find(".add-orderproduct").click();']) . ' (' . $data->json('vkorobke') . ')';
                    },
                ],
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data->name ? Html::a(Html::encode($data->name), '#', ['class' => 'add-orderproduct', 'data' => ['name' => $data->name, 'id' => $data->id]]) : null;
                    },
                ],
                [
                    'label' => 'Не выбирать',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data->json('nevibirat') ?: null;
                    },
                ],
                'article',

            ],
        ]); ?>

        <?php Pjax::end(); ?>
    </div>
</div>
<div class="clearfix"></div>

<?php Modal::end(); ?>

<hr>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'items', ['template' => "{input}\n{error}"])->hiddenInput() ?>

<? $form->field($model, 'user')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'))) ?>

<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'organization')->dropDownList(ArrayHelper::map($model->order->team->organizations, 'id', 'name')) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'clientteam')->dropDownList(
            ArrayHelper::merge(['' => ''], ArrayHelper::map($model->order->team->getClients()->orderBy('name')->all(), 'id', 'name')),
            ['class' => 'select2', 'onchange' => '$.get("' . Url::to(['/ajax/organizations']) . '",{"team":$(this).val()},function(data){
                $("#orderform-clientorganization").html("");
                if (Object.keys(data).length!=1)
                    $("#orderform-clientorganization").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#orderform-clientorganization").append($("<option></option>").attr("value",key).text(value));
                });
            });var team=$(this).val();$.get("' . Url::to(['/ajax/team']) . '",{"team":team},function(data){
                $("#orderform-region").html("<option></option>");
                $("#orderform-city").html("<option></option>");
                $.each(data.regions, function(key, value) {
                    $("#orderform-region").append($("<option></option>").attr("value",key).text(value));
                });
                $.each(data.citys, function(key, value) {
                    $("#orderform-city").append($("<option></option>").attr("value",key).text(value));
                });
                $("#orderform-country").val(data.country);
                $("#orderform-region").val(data.region);
                $("#orderform-city").val(data.city);
                $("#orderform-discount").val(data.discount);
                calc();
            });']
        ) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'clientorganization')->dropDownList(ArrayHelper::merge(['' => ''], @ArrayHelper::map($model->order->clientTeam->organizations, 'id', 'name'))) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'discount')->textInput(['onkeyup' => 'calc();']) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'manager')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map($model->order->team->users, 'id', 'name'))) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'currency')->dropDownList($model->cur,
            ['onchange' => 'calc();']
        ) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'nds')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'nds_in')->checkbox() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'country')->dropDownList(
            ArrayHelper::merge(['' => ''], ArrayHelper::map(Region::find()->where(['parent_id' => null])->asArray()->all(), 'id', 'name')),
            ['onchange' => '$.get("' . Url::to(['/ajax/regions']) . '",{"region":$(this).val()},function(data){
                $("#orderform-region").html("<option></option>");
                $("#orderform-city").html("<option></option>");
                calc();
                $.each(data, function(key, value) {
                    $("#orderform-region").append($("<option></option>").attr("value",key).text(value));
                });
            })']
        ) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'region')->dropDownList(
            ArrayHelper::merge(['' => ''], ArrayHelper::map(Region::find()->where(['and', ['parent_id' => $model->country], ['not', ['parent_id' => null]]])->asArray()->all(), 'id', 'name')),
            ['onchange' => '$.get("' . Url::to(['/ajax/regions']) . '",{"region":$(this).val()},function(data){
                $("#orderform-city").html("<option></option>");
                calc();
                $.each(data, function(key, value) {
                    $("#orderform-city").append($("<option></option>").attr("value",key).text(value));
                });
            })']
        ) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'city')->dropDownList(
            ArrayHelper::merge(['' => ''], ArrayHelper::map(Region::find()->where(['and', ['parent_id' => $model->region], ['not', ['parent_id' => null]]])->asArray()->all(), 'id', 'name')),
            ['onchange' => 'calc();']
        ) ?>
    </div>
</div>

<?= FormBuilder::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'fields',
    'render' => $model->order->fieldsrender,
]); ?>

<div class="form-group">
    <?= Html::submitButton($model->order->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    <?= Html::submitButton(($model->order->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить')) . Yii::t('app', ' и закрыть'), ['class' => 'btn btn-outline btn-round btn-primary', 'name' => 'redirect', 'value' => 'index']) ?>
    <?= Html::a(Yii::t('app', 'Отмена'), ['index'], ['class' => 'btn btn-outline btn-round  btn-default ']) ?>

    <? if (!$model->order->isNewRecord): ?>
        <div class="btn-group">
            <button type="button" class="btn btn-outline btn-info btn-round dropdown-toggle" data-toggle="dropdown">
                Печать<span class="caret"></span></button>
            <ul class="dropdown-menu" role="menu">
                <li><?= Html::a(Yii::t('app', 'Счет'), ['invoice', 'id' => $model->order->id], ['target' => '_blank']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Приказ'), ['invoice', 'id' => $model->order->id, 'forma' => 'prikaz'], ['target' => '_blank']) ?></li>
            </ul>
        </div>
    <? endif ?>

    <?= $model->order->isNewRecord ? '' : Html::a('<i class="icon wb-copy"></i>', ['create', 'copy' => $model->order->id], [
        'class' => 'btn btn-icon btn-outline btn-round btn-warning ',
        'target' => '_blank',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Копировать'),
    ]) ?>
    <?= $model->order->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->order->id], [
        'class' => 'btn btn-icon btn-outline btn-round btn-danger deleteconfirm ',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>

</div>

<?php ActiveForm::end(); ?>
