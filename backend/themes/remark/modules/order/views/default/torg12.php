<!doctype html>
<html>
<head>
    <style type="text/css">body {
            width: 297mm;
            margin-left: auto;
            margin-right: auto;
            border: 1px #efefef solid;
            font-size: 10pt;
        }

        table.invoice_bank_rekv {
            border-collapse: collapse;
            border: 1px solid;
        }

        table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td {
            border: 1px solid;
        }

        table.invoice_com_items {
            border: 1px solid;
            border-collapse: collapse;
            text-align: center;
        }

        table.invoice_com_items td, table.invoice_com_items th {
            border: 1px solid;
        }</style>
</head>
<body>


<table border="0" style="width:100%;border:0px;">
    <tr>
        <td width="770" style="z-index:100;">
            <div id="gruzootpavitel" style="width:100%;font-size:0.73em;">
                ООО "Костромское предприятие "Автофильтр", ИНН:4442015755\440101001, 156007, Костромская обл, Кострома
                г, Пушкина ул, д. 43/102, тел. : (4942) 55-0991, 55-0821, р/с 40702810029010113361 в КОСТРОМСКОЕ
                ОТДЕЛЕНИЕ N 8640 ПАО СБЕРБАНК, БИК 043469623, корр/с 30
            </div>
            <div id="go_name" style="font-size:0.6em;border-top:1px solid;text-align:center;width:100%;height:15px;">
                грузоотправитель, адрес, номер телефона, банковские реквизиты
            </div>
            <div id="gruzootpavitel" style="width:100%;font-size:0.73em;">
                Склад готовой продукции
            </div>
            <div id="go_name" style="font-size:0.6em;border-top:1px solid;text-align:center;width:100%;height:15px;">
                структурное подразделение
            </div>
            <table width="100%">
                <tr>
                    <td width="125" align="right">
                        <div id="gruzopoluchatel_name" style="width:100%;font-size:0.9em;">Грузополучатель</div>
                    </td>
                    <td style="border-bottom:1px solid;">
                        <div id="gruzopoluchatel"
                             style="width:100%;font-size:0.73em;"><?= $model->order->clientOrganization->name ?>
                            , <?= $model->order->clientOrganization->json('inn') ?>
                            , <?= $model->order->clientOrganization->json('uradres') ?> </div>
                    </td>
                </tr>
                <tr>
                    <td width="125" align="right">
                        <div id="postavshik_name" style="width:100%;font-size:0.9em;">Поставщик</div>
                    </td>
                    <td style="border-bottom:1px solid;">
                        <div id="postavshik" style="width:100%;font-size:0.73em;">ООО "Костромское предприятие
                            "Автофильтр", ИНН:4442015755\440101001, 156007, Костромская обл, Кострома г, Пушкина ул, д.
                            43/102, тел. : (4942) 55-0991, 55-0821, р/с 40702810029010113361 в КОСТРОМСКОЕ ОТДЕЛЕНИЕ N
                            8640 ПАО СБЕРБАНК, БИК 043469623, корр/с 30
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="125" align="right">
                        <div id="platelshik_name" style="width:100%;font-size:0.9em;">Плательщик</div>
                    </td>
                    <td style="border-bottom:1px solid;">
                        <div id="platelshik"
                             style="width:100%;font-size:0.73em;"><?= $model->order->clientOrganization->name ?>
                            , <?= $model->order->clientOrganization->json('inn') ?>
                            , <?= $model->order->clientOrganization->json('uradres') ?></div>
                    </td>
                </tr>
                <tr>
                    <td width="125" align="right">
                        <div id="osnovanie_name" style="width:100%;font-size:0.9em;">Основание</div>
                    </td>
                    <td style="border-bottom:1px solid;">
                        <div id="osnovanie" style="width:100%;font-size:0.73em;">Договор</div>
                    </td>
                </tr>
                <tr>
                    <td width="125" align="right"></td>
                    <td valign="top">
                        <div id="naryad_name" style="width:100%;font-size:0.6em;text-align:center;">договор,
                            заказ-наряд
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <table width="100%">
                <tr>
                    <td align="right" width="50%">
                        <div id="nakladnaya_name" style="font-size:0.9em;font-weight:bold;">ТОВАРНАЯ НАКЛАДНАЯ</div>
                    </td>
                    <td align="left">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="100" style="border:1px solid;" align="center">
                                    <div id="nomerdoc_name" style="font-size:0.6em;">Номер документа</div>
                                </td>
                                <td width="100" style="border:1px solid;border-left:0px;" align="center">
                                    <div id="datesoz_name" style="font-size:0.6em;">Дата составления</div>
                                </td>
                            </tr>
                            <tr>
                                <td style="border:2px solid;" align="center">
                                    <div id="nomerdoc"
                                         style="font-size:0.9em;font-weight:bold;"><?= $model->order->number ?></div>
                                </td>
                                <td align="center" style="border:2px solid;border-left:0px;">
                                    <div id="date_soz"
                                         style="font-size:0.9em;font-weight:bold;"><?= Yii::$app->formatter->asDate($model->order->created_at) ?></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


        </td>
        <td align="right" valign="top">
            <div style="position:relative;z-index:0; margin-left: -50px;">
                <div style="right:0px;width:310px;z-index:2;">
                    <table border="0" cellpadding="0" cellspacing="0" width="310"
                           style="background-color: transparent;">
                        <tr>
                            <td colspan="2"></td>
                            <td width="70" align="center" style="border:1px solid;">
                                <div id="kod_okud_name" style="width:100%;font-size:0.6em;text-align:center;">Код</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" style="padding-right:5px;">
                                <div id="okud_name" style="width:100%;font-size:0.9em;">Форма по ОКУД</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-left:2px solid;border-right:2px solid;">
                                <div id="okud" style="width:100%;font-size:0.9em;font-weight:bold;">0330212</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" style="padding-right:5px;">
                                <div id="okpo_name" style="width:100%;font-size:0.9em;">по ОКПО</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="okpo" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" style="padding-right:5px;">&nbsp;</td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" style="padding-right:5px;">
                                <div id="okpd_name" style="width:100%;font-size:0.9em;">Вид деятельности по ОКДП</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="okpd" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" style="padding-right:5px;height:25px;vertical-align:middle;">
                                <div id="okpo2_name" style="width:100%;font-size:0.9em;">по ОКПО</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="okpo2" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" style="padding-right:5px;height:25px;vertical-align:middle;">
                                <div id="okpo3_name" style="width:100%;font-size:0.9em;">по ОКПО</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="okpo3" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" style="padding-right:5px;height:25px;vertical-align:middle;">
                                <div id="okpo4_name" style="width:100%;font-size:0.9em;">по ОКПО</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="okpo4" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td width="70" align="right" style="padding-right:5px;border:1px solid;border-right:0px;">
                                <div id="nomer1_name" style="width:100%;font-size:0.9em;">номер</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="nomer1" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td width="70" align="right"
                                style="padding-right:5px;border:1px solid;border-right:0px;border-top:0px;">
                                <div id="data1_name" style="width:100%;font-size:0.9em;">дата</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="data1" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding-right:0px;">
                                <div id="transp_nakl_name" style="width:100%;font-size:0.9em;">Транспортная накладная
                                </div>
                            </td>
                            <td width="70" align="right"
                                style="padding-right:5px;border:1px solid;border-right:0px;border-top:0px;">
                                <div id="nomer2_name" style="width:100%;font-size:0.9em;">номер</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="nomer2" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td width="70" align="right"
                                style="padding-right:5px;border:1px solid;border-right:0px;border-top:0px;">
                                <div id="data2_name" style="width:100%;font-size:0.9em;">дата</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;">
                                <div id="data2" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" style="padding-right:5px;">
                                <div id="vido_name" style="width:100%;font-size:0.9em;">Вид операции</div>
                            </td>
                            <td width="70" align="center"
                                style="border:1px solid;border-top:0px;border-left:2px solid;border-right:2px solid;border-bottom:2px solid;">
                                <div id="vido" style="width:100%;font-size:0.9em;font-weight:bold;">&nbsp;</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>


<table id="items" class="invoice_com_items" border="0" width="100%" cellpadding="0" cellspacing="0">
    <thead>
    <tr height="10">
        <td rowspan="2" height="12" width="36">
            <div id="nomerpp_name">Но-<br/>мер<br/>по по-<br/>рядку</div>
        </td>
        <td colspan="2" height="12">
            <div id="tovar_name" style="">Товар</div>
        </td>
        <td colspan="2" height="12">
            <div id="ed_name" style="">Единица измерения</div>
        </td>
        <td rowspan="2" height="12" width="42">
            <div id="vidupak_name" style="">Вид<br/>упа-<br/>ковки</div>
        </td>
        <td colspan="2" height="12">
            <div id="kolvo_name">Количество</div>
        </td>
        <td rowspan="2" height="12" width="53">
            <div id="mbrutto_name" style="">Масса<br/>брутто</div>
        </td>
        <td rowspan="2" height="12" width="61">
            <div id="mnetto_name">Количе-<br/>ство<br/>(масса<br/>нетто)</div>
        </td>
        <td rowspan="2" height="12" width="67">
            <div id="price_name">Цена,<br/>руб. коп.</div>
        </td>
        <td rowspan="2" height="12" width="60">
            <div id="summwo_nds_name">Сумма без<br/>учета НДС,<br/>руб. коп.</div>
        </td>
        <td colspan="2" height="12">
            <div id="nds_name">НДС</div>
        </td>
        <td rowspan="2" height="12" width="67">
            <div id="summw_nds_name">Сумма с<br/>учетом<br/>НДС,<br/>руб. коп.</div>
        </td>
    </tr>
    <tr>
        <td width="200">
            <div id="naim_name" style="font-size:0.9em;">наименование, характеристика, сорт,<br/>артикул товара</div>
        </td>
        <td width="55">
            <div id="kod_name" style="font-size:0.9em;">код</div>
        </td>
        <td width="54">
            <div id="ednaim_name" style="font-size:0.9em;">наиме-<br/>нование</div>
        </td>
        <td width="54">
            <div id="okei_name" style="font-size:0.9em;">код по<br/>ОКЕИ</div>
        </td>
        <td width="42">
            <div id="inplace_name" style="font-size:0.9em;">в<br/>одном<br/>месте</div>
        </td>
        <td width="55">
            <div id="mest_name" style="font-size:0.9em;">мест,<br/>штук</div>
        </td>
        <td width="60">
            <div id="stavka_name" style="font-size:0.9em;">ставка, %</div>
        </td>
        <td width="67">
            <div id="total_summ_name" style="font-size:0.9em;">сумма,<br/>руб. коп.</div>
        </td>
    </tr>
    <tr>
        <td style="">
            <div id="1_id">1</div>
        </td>
        <td style="">
            <div id="2_id">2</div>
        </td>
        <td style="">
            <div id="3_id">3</div>
        </td>
        <td style="">
            <div id="4_id">4</div>
        </td>
        <td style="">
            <div id="5_id">5</div>
        </td>
        <td style="">
            <div id="6_id">6</div>
        </td>
        <td style="">
            <div id="7_id">7</div>
        </td>
        <td style="">
            <div id="8_id">8</div>
        </td>
        <td style="">
            <div id="9_id">9</div>
        </td>
        <td style="">
            <div id="10_id">10</div>
        </td>
        <td style="">
            <div id="11_id">11</div>
        </td>
        <td style="">
            <div id="12_id">12</div>
        </td>
        <td style="">
            <div id="13_id">13</div>
        </td>
        <td style="">
            <div id="14_id">14</div>
        </td>
        <td style="">
            <div id="15_id">15</div>
        </td>
    </tr>
    </thead>
    <tbody>

    <? foreach ($model->order->items as $key => $item): ?>
        <tr>
            <td align="center"><?= $key + 1 ?></td>
            <td align="left"><?= $item->name ?></td>
            <td></td>
            <td><?= $item->product->unit ?></td>
            <td>796</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <?= Yii::$app->formatter->asDecimal($item->quantity) ?>
                <? $count += $item->quantity ?>
            </td>
            <td><?= Yii::$app->formatter->asCurrency($item->price(false)) ?></td>
            <td>
                <?= Yii::$app->formatter->asCurrency($item->price(false) * $item->quantity) ?>
                <? $sum += $item->price(false) * $item->quantity ?>
            </td>
            <td><?= Yii::$app->formatter->asPercent($item->nds / 100) ?></td>
            <td>
                <?= Yii::$app->formatter->asCurrency(($item->price(true) - $item->price(false)) * $item->quantity) ?>
                <? $nds += ($item->price(true) - $item->price(false)) * $item->quantity ?>
            </td>
            <td>
                <?= Yii::$app->formatter->asCurrency($item->price(true) * $item->quantity) ?>
                <? $total += $item->price(true) * $item->quantity ?>
            </td>
        </tr>
    <? endforeach ?>

    </tbody>
    <tr id="last_item">
        <td colspan="7" style="text-align:right;border-bottom:1px solid #fff;border-left:1px solid #fff;">
            Итого&nbsp;
        </td>
        <td>X</td>
        <td>X</td>
        <td>
            <?= Yii::$app->formatter->asDecimal($count) ?>
        </td>
        <td>X</td>
        <td>
            <?= Yii::$app->formatter->asCurrency($sum) ?>
        </td>
        <td>X</td>
        <td>
            <?= Yii::$app->formatter->asCurrency($nds) ?>
        </td>
        <td>
            <?= Yii::$app->formatter->asCurrency($total) ?>
        </td>
    </tr>
    <tr id="last_item">
        <td colspan="7" style="text-align:right;border-bottom:1px solid #fff;border-left:1px solid #fff;">
            Всего по накладной&nbsp;
        </td>
        <td>X</td>
        <td>X</td>
        <td>
            <?= Yii::$app->formatter->asDecimal($count) ?>
        </td>
        <td>X</td>
        <td>
            <?= Yii::$app->formatter->asCurrency($sum) ?>
        </td>
        <td>X</td>
        <td>
            <?= Yii::$app->formatter->asCurrency($nds) ?>
        </td>
        <td>
            <?= Yii::$app->formatter->asCurrency($total) ?>
        </td>
    </tr>
</table>


<table width="100%">
    <tr>
        <td align="center">
            <div style="width:700px;text-align:left;font-size:0.73em;">
                Товарная накладная имеет приложение на листах<br/>и содержит
                _______________________________________________________________________________ порядковых номеров
                записей
                <div id="propis1_name" style="width:87%;font-size:0.6em;text-align:center;">прописью</div>
            </div>
        </td>
    </tr>
</table>


<table width="100%">
    <tr>
        <td align="right" valign="bottom" width="365">
            <div id="vsego_mest" style="font-size:0.73em;display:inline;">Всего мест</div>
            <div style="display:inline;">___________________________</div>
            <div id="propis2_name" style="width:180px;font-size:0.6em;text-align:center;">прописью</div>
        </td>
        <td valign="top" style="padding-left:10px;" align="right">
            <div id="massa_netto" style="display:inline;text-align:left;">Масса груза (нетто)</div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                    style="border:2px solid;width:100px;margin-bottom:2px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <div id="propis3_name" style="width:450px;font-size:0.6em;text-align:center;border-top:1px solid;">
                прописью
            </div>
            <div id="massa_brutto" style="display:inline;">Масса груза (брутто)</div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                    style="border:2px solid;width:100px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <div id="propis4_name" style="width:450px;font-size:0.6em;text-align:center;border-top:1px solid;">
                прописью
            </div>
        </td>
    </tr>
</table>


<table width="100%">
    <tr>
        <td style="border-right:1px solid;width:50%;">
            <div id="pril_pasport" style="display:inline;">Приложение (паспорта, сертификаты и т.п) на</div>
            <div style="display:inline;"> _________________</div>
            <div id="pril_list" style="display:inline;">листах</div>
            <div id="propis5_name" style="font-size:0.6em;padding-left:270px;">прописью</div>
            <div style="font-weight:bold;">Всего отпущено наименований <?= $key + 1 ?><br/>на
                сумму <?= num2str($total) ?></div>
            <br/>
            <table width="100%" cellspacing="1">
                <tr>
                    <td align="left">
                        <div id="otpus_raz_name">Отпуск разрешил</div>
                    </td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="text-align:center;padding:2px;">
                        <div id="otpus_raz_mile"><!-- /Фамилия И.О./ --></div>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-size:0.6em;">&nbsp;</td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="dolznost1_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            должность
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="podpis1_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">подпись
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="rashifr1_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            расшифровка подписи
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <div id="buhgalter_name">Главный (старший бухгалтер)</div>
                    </td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="text-align:center;padding:2px;">
                        <div id="buhgalter_mile">
                            <!-- /Фамилия И.О./ -->
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" style="font-size:0.6em;">&nbsp;</td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="podpis2_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">подпись
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="rashifr2_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            расшифровка подписи
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <div id="otpus_pro_name">Отпуск груза произвел</div>
                    </td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="text-align:center;padding:2px;">
                        <div id="otpus_pro_mile"><!-- /Фамилия И.О./ --></div>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-size:0.6em;">&nbsp;</td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="dolznost3_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            должность
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="podpis3_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">подпись
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="rashifr3_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            расшифровка подписи
                        </div>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td width="50%" align="center">
                        <div id="mp1_name" style="font-size:0.73em;text-align:center;">М.П.</div>
                    </td>
                    <td>
                        <div id="date1_name" style="font-size:0.73em;text-align:center;">''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;______________
                            20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;года
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width:50%;padding-left:30px;vertical-align:top;">
            <div id="po_dover">По доверенности №&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                от
            </div>
            <br>
            <div id="dover_vid" style="display:inline;">выданной</div>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <div style="display:inline;width:300px;border-bottom:1px solid;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
            <div id="kem_name" style="font-size:0.6em;padding-left:150px;">кем, кому (организация, должность, фамилия,
                и.о.)
            </div>
            <br>
            <div style="font-size:0.6em;">&nbsp;</div>
            <div style="border-bottom:1px solid;width:100%;">&nbsp;</div>
            <br>
            <table width="100%" cellspacing="1">
                <tr>
                    <td align="left">
                        <div id="gruz_prin_name">Груз принял</div>
                    </td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="text-align:center;padding:2px;">
                        <div id="gruz_prin_mile"></div>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-size:0.6em;">&nbsp;</td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="dolznost4_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            должность
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="podpis4_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">подпись
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="rashifr4_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            расшифровка подписи
                        </div>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" align="left" valign="top">
                        <div id="gruz_pol_name">Груз получил<br/>грузополучатель</div>
                    </td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="padding:2px;">&nbsp;</td>
                    <td width="100" style="text-align:center;padding:2px;">
                        <div id="gruz_pol_mile"></div>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="dolznost5_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            должность
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="podpis5_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">подпись
                        </div>
                    </td>
                    <td style="padding:0px;padding-left:3px;padding-right:3px;">
                        <div id="rashifr5_name" style="border-top:1px solid;font-size:0.6em;text-align:center;">
                            расшифровка подписи
                        </div>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td width="50%" align="center">
                        <div id="mp2_name" style="font-size:0.73em;text-align:center;">М.П.</div>
                    </td>
                    <td>
                        <div id="date2_name" style="font-size:0.73em;text-align:center;">''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;______________
                            20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;года
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


</body>
</html>

<?
/**
 * Возвращает сумму прописью
 * @author runcore
 * @uses morph(...)
 */
function num2str($num)
{
    $nul = 'ноль';
    $ten = array(
        array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
    );
    $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
    $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
    $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
    $unit = array( // Units
        array('копейка', 'копейки', 'копеек', 1),
        array('рубль', 'рубля', 'рублей', 0),
        array('тысяча', 'тысячи', 'тысяч', 1),
        array('миллион', 'миллиона', 'миллионов', 0),
        array('миллиард', 'милиарда', 'миллиардов', 0),
    );
    //
    list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub) > 0) {
        foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit) - $uk - 1; // unit key
            $gender = $unit[$uk][3];
            list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
            else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk > 1) $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
        } //foreach
    } else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
    $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5)
{
    $n = abs(intval($n)) % 100;
    if ($n > 10 && $n < 20) return $f5;
    $n = $n % 10;
    if ($n > 1 && $n < 5) return $f2;
    if ($n == 1) return $f1;
    return $f5;
}

?>
