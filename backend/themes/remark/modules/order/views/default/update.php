<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\order\models\OrderForm */

$this->title = Yii::t('app', 'Заказ №{number}', ['number' => $model->order->number]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заказы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<p class="pull-right" style="margin-top:-40px;"><?= Yii::$app->formatter->asDatetime($model->order->created_at) ?></p>
<?
try {
    echo $this->render('_form/_form_' . $model->order->team->id, [
        'model' => $model,
        'dataProvider' => $dataProvider,
    ]);
} catch (Exception $e) {
    echo $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
    ]);
}
?>

