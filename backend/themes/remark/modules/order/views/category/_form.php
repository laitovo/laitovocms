<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\order\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(ArrayHelper::merge([['name' => Yii::t('app', 'Нет')]], $model->items), 'id', 'name')) ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

<?= FormBuilder::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'fields',
    'render' => $model->category->fieldsrender,
]); ?>

<div class="form-group">
    <?= Html::submitButton($model->category->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    <?= $model->category->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->category->id], [
        'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>

</div>

<?php ActiveForm::end(); ?>
