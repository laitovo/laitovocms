<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use backend\widgets\GridView;
use backend\assets\tree\TreeAsset;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
TreeAsset::register($this);

$this->title = Yii::t('app', 'Категории');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>
<p>&nbsp;</p>
<div id="tree"></div>

<?
//  GridView::widget([
//     'tableOptions'=>['class'=>'table table-hover'],
//     'dataProvider' => $dataProvider,
//     'show'=>['name'],
//     'columns' => [
//         ['class' => 'yii\grid\SerialColumn'],

//         'parent_id',
//         'name',
//         // 'sort',
//         // 'status',
//         // 'created_at',
//         // 'updated_at',
//         // 'author_id',
//         // 'updater_id',
//         // 'json:ntext',

//         [
//             'class' => 'yii\grid\ActionColumn',
//             'template' => '{update} {delete}',
//             'buttonOptions' => ['class'=>'deleteconfirm'],
//         ],
//     ],
// ]); ?>

<?
$data = [];
foreach ($query->andWhere(Yii::$app->request->get('search') ? [] : ['parent_id' => null])->orderBy('sort')->all() as $row) {
    $data[] = [
        'text' => Html::encode($row->name),
        'href' => Url::to(['update', 'id' => $row->id]),
        'selectable' => false,
        'nodes' => Yii::$app->request->get('search') ? null : subcat($row),
    ];
}
function subcat($cat)
{
    $data = null;
    foreach ($cat->getCategories()->orderBy('sort')->all() as $row) {
        $data[] = [
            'text' => Html::encode($row->name),
            'href' => Url::to(['update', 'id' => $row->id]),
            'selectable' => false,
            'nodes' => subcat($row),
        ];
    }
    return $data;
}

Yii::$app->view->registerJs("
    $('#tree').treeview({data: " . Json::encode($data) . ",enableLinks: true});
");
?>
