<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\order\Price */

$this->title = $model->_price->product->name . ' (' . $model->_price->region->name . ')';

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Регионы продаж'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->_price->region->name, 'url' => ['view', 'id' => $model->_price->region_id]];
$this->params['breadcrumbs'][] = $model->_price->product->name;

$this->params['searchmodel'] = true;
$this->params['searchmodelaction'] = ['view', 'id' => $model->_price->region->id];
$this->render('../menu');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
