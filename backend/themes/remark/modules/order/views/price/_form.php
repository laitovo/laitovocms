<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\order\Price */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>


<?= $form->field($model, 'oldprice')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'currency')->dropDownList($model->cur) ?>

<?= $form->field($model, 'nds')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'nds_in')->checkbox() ?>

<?= $form->field($model, 'maxdiscount')->textInput(['maxlength' => true]) ?>

<div class="form-group">
    <?= Html::submitButton($model->_price->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    <?= $model->_price->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'region_id' => $model->_price->region_id, 'product_id' => $model->_price->product_id], [
        'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
