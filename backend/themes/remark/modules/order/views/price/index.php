<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Регионы продаж');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<p>&nbsp;</p>
<div id="tree"></div>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'parent.name:text:' . Yii::t('app', 'Родитель'),
        'name',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
        ],
    ],
]); ?>
