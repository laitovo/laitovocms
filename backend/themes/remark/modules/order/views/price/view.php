<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\order\Price */

$this->title = $model->name;
$region = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Торговля'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Регионы продаж'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->params['searchmodelaction'] = ['view', 'id' => $model->id];
$this->render('../menu');
?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name', 'prices.price'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'name',
        'article',
        [
            'attribute' => 'prices.price',
            'format' => 'currency',
            'value' => function ($data) use ($region) {
                foreach ($data->prices as $row) if ($row->region_id == $region) {
                    return $row->price();
                }
                return null;
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' =>
                [
                    'update' => function ($url, $model, $key) use ($region) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'region_id' => $region, 'product_id' => $model->id], [
                            'title' => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax' => '0',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) use ($region) {
                        foreach ($model->prices as $row) if ($row->region_id == $region) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'region_id' => $region, 'product_id' => $model->id], [
                                'title' => Yii::t('yii', 'Delete'),
                                'class' => 'deleteconfirm',
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        }
                    },
                ],

        ],

    ],
]); ?>
