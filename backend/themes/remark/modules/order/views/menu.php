<?php

/* @var $this yii\web\View */

$this->params['menuItems'] = [
    [
        'items' => [
            ['label' => Yii::t('app', 'Заказы'), 'url' => ['default/index']],
            ['label' => Yii::t('app', 'Товары'), 'url' => ['product/index']],
            ['label' => Yii::t('app', 'Контрагенты'), 'url' => ['client/index']],
            ['label' => Yii::t('app', 'Категории'), 'url' => ['category/index']],
            ['label' => Yii::t('app', 'Регионы продаж'), 'url' => ['price/index']],
            ['label' => Yii::t('app', 'Акции'), 'url' => ['promo/index']],
            ['label' => Yii::t('app', 'Отчеты'), 'url' => ['report/index']],
        ],
    ],
];