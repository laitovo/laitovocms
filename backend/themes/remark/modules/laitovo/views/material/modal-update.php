<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Material;
use common\models\laitovo\UnitMaterials;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;



/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\MaterialForm */

$this->params['breadcrumbs'][] = ['label' => 'Материалы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');
?>
<div class="material-form-create">

    <h1>Изменить материал <?=@$model->material->name?> - <?= @$model->parameter?></h1>


<div class="material-parameter-form">
    <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>
   
    <?= $form->field($model, 'material_id')->dropDownList(ArrayHelper::map(Material::find()->all(), 'id', 'name')) ?>
     
    <?= $form->field($model, 'parameter')->textInput(['maxlength' => true]) ?>
   
    <?= $form->field($model, 'accounting_unit')->dropDownList(ArrayHelper::map(UnitMaterials::find()->all(), 'id', 'name')) ?>
   
    <?= $form->field($model, 'accounting_unit_price')->textInput() ?>
   
    <?= $form->field($model, 'unit_settlement')->dropDownList(ArrayHelper::map(UnitMaterials::find()->all(), 'id', 'name'))?>
 
    <?= $form->field($model, 'coefficient_settlement')->textInput() ?>

   

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success',
            'onclick'=>'$("#modalView.in").modal("hide");']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>




</div>

