<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use common\models\laitovo\UnitAccounting;
use common\models\laitovo\UnitSettlement;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('../menu');
?>
<div class="material-form-index">

    <h1><?= Html::encode('Журнал изменений материалов') ?></h1>

    <p>
         <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить материал')]) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'show' => [ 'material.name','parameter', 'accounting_unit','accounting_unit_price','unit_settlement','coefficient_settlement','price_settlement'],
        'columns' => [
           
            [
               'attribute'=>'materials',
               'label'=>'Материал',
                'format' => 'html',
               'value'=> function($data){
                    return  $data->material->name  ?  Html::a(Html::encode($data->material->name .' '. @$data->parameter), ['view', 'id' => $data->id]): null; 
               }
            ],
           
            [
                'attribute'=>'unit_accounting',
                'label'=>'Учетная единица',
                'format'=>'raw',
                'value' => function($data){
                        $unit_accounting = UnitAccounting::find()->where(['material_parameter_id'=>$data->id])->orderBy('created_at DESC')->all();
                        $return = '';
                         if ($unit_accounting) {
                        foreach ($unit_accounting as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->accountingUnit->name )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'accounting_unit_price',
                'label'=>'Цена учетной единицы',
                'format'=>'raw',
                'value' => function($data){
                        $unit_accounting = UnitAccounting::find()->where(['material_parameter_id'=>$data->id])->orderBy('created_at DESC')->all();
                        $return = '';
                        if ($unit_accounting) {
                        foreach ($unit_accounting as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->accounting_unit_price )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'unit_settlement',
                'label'=>'Расчетная единица',
                'format'=>'raw',
                'value' => function($data){
                        $unit_accounting = UnitSettlement::find()->where(['material_parameter_id'=>$data->id])->orderBy('created_at DESC')->all();
                        $return = '';
                        if ($unit_accounting) {
                        foreach ($unit_accounting as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->unitSettlement->name  )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'coefficient_settlement',
                'label'=>'Коэффициент для расчета(взвешивание)',
                'format'=>'raw',
                'value' => function($data){
                        $unit_accounting = UnitSettlement::find()->where(['material_parameter_id'=>$data->id])->orderBy('created_at DESC')->all();
                        $return = '';
                        if ($unit_accounting) {
                        foreach ($unit_accounting as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->coefficient_settlement)
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            
            [
                'attribute'=>'price_settlement',
                'label'=>'Стоимость расчетной единицы',
                'format'=>'raw',
                'value' => function($data){
                        $unit_accounting = UnitSettlement::find()->where(['material_parameter_id'=>$data->id])->orderBy('created_at DESC')->all();
                        $return = '';
                        if ($unit_accounting) {
                        foreach ($unit_accounting as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->materialParameter->lastAccounting->accounting_unit_price*$row->coefficient_settlement)
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
                           [
                'attribute'=>'autor_id',
                'label'=>'Автор записи',
                'format'=>'raw',
                'value' => function($data){
                        $unit_accounting = UnitSettlement::find()->where(['material_parameter_id'=>$data->id])->orderBy('created_at DESC')->all();
                        $return = '';
                        if ($unit_accounting) {
                        foreach ($unit_accounting as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->autor->name)
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            
            [
                'attribute'=>'created_at',
                'label'=>'Дата записи',
                'format'=>'raw',
                'value' => function($data){
                        $unit_accounting = UnitSettlement::find()->where(['material_parameter_id'=>$data->id])->orderBy('created_at DESC')->all();
                        $return = '';
                        if ($unit_accounting) {
                        foreach ($unit_accounting as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    (Yii::$app->formatter->asDateTime($row->created_at))
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
        ],
    ]); ?>
</div>
