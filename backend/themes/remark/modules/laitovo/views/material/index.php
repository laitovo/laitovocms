<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use common\models\laitovo\MaterialParameters;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('../menu');
?>
<div class="material-form-index">

    <h1><?= Html::encode('Материалы на участках') ?></h1>
   
  
         <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить материал')]) ?>
  
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'show' => [ 'material.name','parameter', 'accounting_unit','accounting_unit_price','unit_settlement','coefficient_settlement','price_settlement'],
        'columns' => [
           
            'name',                   
            [
                'attribute'=>'materials',
                'label'=>'Материал',
                'format'=>'raw',
                'value' => function($data){
                        $material_list = MaterialParameters::find()->joinWith('materialLocations')->andWhere(['laitovo_erp_location_id'=>$data->id])->all();
                        $return = '';
                         if ($material_list) {
                        foreach ($material_list as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->material->name ? Html::a(Html::encode($row->material->name), ['view', 'id' => $row->id]) . ' - ' . @$row->parameter  : null)
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'accountingUnit',
                'label'=>'Учетная единица',
                'format'=>'raw',
                'value' => function($data){
                        $material_list = MaterialParameters::find()->joinWith('materialLocations')->andWhere(['laitovo_erp_location_id'=>$data->id])->all();
                        $return = '';
                         if ($material_list) {
                        foreach ($material_list as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->lastAccounting->accountingUnit->name  )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'accounting_unit_price',
                'label'=>'Цена учетной единицы',
                'format'=>'raw',
                'value' => function($data){
                        $material_list = MaterialParameters::find()->joinWith('materialLocations')->andWhere(['laitovo_erp_location_id'=>$data->id])->all();
                        $return = '';
                         if ($material_list) {
                        foreach ($material_list as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->lastAccounting->accounting_unit_price )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'unitSettlement',
                'label'=>'Рчетная единица',
                'format'=>'raw',
                'value' => function($data){
                        $material_list = MaterialParameters::find()->joinWith('materialLocations')->andWhere(['laitovo_erp_location_id'=>$data->id])->all();
                        $return = '';
                         if ($material_list) {
                        foreach ($material_list as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->lastSettlement->unitSettlement->name )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'coefficient_settlement',
                'label'=>'Коэффициент для расчета(взвешивание)',
                'format'=>'raw',
                'value' => function($data){
                        $material_list = MaterialParameters::find()->joinWith('materialLocations')->andWhere(['laitovo_erp_location_id'=>$data->id])->all();
                        $return = '';
                         if ($material_list) {
                        foreach ($material_list as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->lastSettlement->coefficient_settlement )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'price_settlement',
                'label'=>'Стоимость расчетной единицы',
                'format'=>'raw',
                'value' => function($data){
                        $material_list = MaterialParameters::find()->joinWith('materialLocations')->andWhere(['laitovo_erp_location_id'=>$data->id])->all();
                        $return = '';
                         if ($material_list) {
                        foreach ($material_list as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->lastSettlement->coefficient_settlement*$row->lastAccounting->accounting_unit_price )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'autor_id',
                'label'=>'Автор последних изменений',
                'format'=>'raw',
                'value' => function($data){
                        $material_list = MaterialParameters::find()->joinWith('materialLocations')->andWhere(['laitovo_erp_location_id'=>$data->id])->all();
                        $return = '';
                         if ($material_list) {
                        foreach ($material_list as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($row->lastSettlement->autor->name )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],
            [
                'attribute'=>'lastSettlement',
                'label'=>'Дата последних изменений',
                'format'=>'raw',
                'value' => function($data){
                        $material_list = MaterialParameters::find()->joinWith('materialLocations')->andWhere(['laitovo_erp_location_id'=>$data->id])->all();
                        $return = '';
                         if ($material_list) {
                        foreach ($material_list as $row) {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    (Yii::$app->formatter->asDateTime($row->lastSettlement->created_at) )
                                ));
                        }
                    }
                    return $return;
                   },    
            ],       
        ],
    ]); ?>
</div>
