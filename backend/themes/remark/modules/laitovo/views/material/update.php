<?php

use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\MaterialForm */

$this->params['breadcrumbs'][] = ['label' => 'Материалы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');
?>
<div class="material-form-create">

    <h1>Изменить материал <?=@$model->material->name?> - <?= @$model->parameter?></h1>

    <?= $this->render('_form', [
        'model' => $model,    
    ]) ?>

</div>
