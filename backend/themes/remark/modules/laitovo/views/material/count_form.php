<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>
    
    <?= $form->field($model, 'material_count')->textInput() ?> 
   
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success',
            'onclick'=>'
                 $("#modalView.in").modal("hide");
            ',
            ]) ?>
    </div>

<?php ActiveForm::end(); ?>
    
