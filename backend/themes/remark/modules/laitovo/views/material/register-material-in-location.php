<?php 
 
use yii\helpers\Html; 
use yii\helpers\ArrayHelper; 
use yii\widgets\ActiveForm; 
use backend\modules\laitovo\models\ErpLocation; 
use backend\modules\laitovo\models\ErpJobType; 
use common\models\laitovo\ProductionScheme; 
use yii\grid\GridView; 
use yii\widgets\Pjax; 
use common\models\laitovo\MaterialProductionScheme; 
use yii\helpers\Url; 
use yii\bootstrap\Modal; 
 
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']]; 
if(isset( ProductionScheme::findOne($production_scheme_id)->title)){
    $this->params['breadcrumbs'][] = ['label' =>  ProductionScheme::findOne($production_scheme_id)->title , 'url' => ['production/view', 'id'=>$production_scheme_id]];
}
$this->params['searchmodel'] = true; 
$this->render('../menu');

$customScript = <<< SCRIPT
      $(document).on("click","[data-remote]",function(e) {
            e.preventDefault();
            $("#p0").append("<div id=material-div></div>");
            $("#modalView #material-div").load($(this).data('remote'));
        });
        $('#modalView').on('hidden.bs.modal', function(){
            $('#reload_submit_pjax').click();
            $("#p0 > div").remove();
            $("#p0 > div").remove();
        }); 
SCRIPT;

$this->registerJs($customScript, \yii\web\View::POS_READY); 
//вывод модального окна 
Modal::begin(['id'=>'modalView', 'size'=>'modal-md']); 
Pjax::begin(['enablePushState' => FALSE]);
Pjax::end(); 
Modal::end(); 
//вывод модального окна 
?> 
<?php if (isset(ProductionScheme::findOne($production_scheme_id)->title) && isset(ErpLocation::findOne($location_id)->name)): ?>  
<h2><?php echo 'Редактирование материала на производственной схеме "'. ProductionScheme::findOne($production_scheme_id)->title .'" участка ' . ErpLocation::findOne($location_id)->name ; ?></h2> 
<?php endif;?>    
 
<?php Pjax::begin(); ?> 
 <div class="col-md-6"> 
    <h4 id='tables'>Виды материалов на участке </h4> 
    <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]);?> 
    <?= GridView::widget([ 
        'tableOptions' => ['class' => 'table table-hover', ], 
        'rowOptions'=> ['class'=>'info'],
        'dataProvider' => $materialSchemeProvider, 
        'columns' => [ 
                ['class' => 'yii\grid\ActionColumn', 
                    'buttons'=>[
                        'view'=>function($url, $data){
                            return Html::a(@$data->materialParameter->material->name .' '. @$data->materialParameter->parameter ,
                                    '#modalView', [
                                        'title' => 'Назначить',
                                        'data-toggle'=>'modal',
                                        'data-backdrop'=>false,
                                        'data-remote'=>Url::to(['modal-update','id'=>$data->materialParameter->id])
                                    ]);
                            },
                    ],
                    'template'=>'{view}'
                ],
                
               
                [                 
                    'label'=>'Расч. ед', 
                    'value'=> function($data){ 
                        return $data->materialParameter->lastSettlement ? $data->materialParameter->lastSettlement->unitSettlement->name : '';
                    } 
                ], 
                [                 
                    'label'=>'Количество материалов', 
                    'format' => 'raw', 
                     
                    'value'=> function($data) use ($form){ 
                        return   Html::tag('div',
                                     Html::input('numeric','material_count['.$data->materialParameter->id.']', $data->material_count, ['class' => 'form-control'])
                                ); 
                    }  
                ], 
                         
               [ 
                'class' => 'yii\grid\ActionColumn', 
                'template' => '{today_action}', 
                'buttons' => [ 
                                'today_action' => function ($url, $data) use ($location_id, $production_scheme_id) { 
                                return Html::a('<span class="glyphicon glyphicon-remove" style = "color:red"></span>', 
                                        [ 
                                            'register-material-in-location', 
                                            'location_id' => $location_id, 
                                            'production_scheme_id' => $production_scheme_id, 
                                        ], 
                                        [ 
                                            'data-pjax'=>true, 
                                                'data' => [ 
                                                    'method' => 'post', 
                                                    'params' =>  
                                                    [ 
                                                        'unregister'=>true, 
                                                        'material_id'=>$data->material_parameter_id 
                                                    ],
                                                    'confirm' => Yii::t('yii', 'Убрать из группы?'),
                                                ], 
                                        ],  
                                [ 
                                    'title' => Yii::t('app', 'Убрать из группы'), 
                                ]); 
                            } 
                    ], 
                ], 
        ], 
    ]); ?> 
    
   <?php if($flag): ?>
    <?= Html::submitButton(Yii::t('app', 'Вернуться к карточке продукта'),['id'=>'reload_submit', 'class'=>'btn  btn-round btn-warning',
         'data'=>['confirm' => Yii::t('yii', 'Сохранить изменения?'),'method' => 'post',]])?> 
    <?php else: ?>
     <?= Html::a(Yii::t('app', 'Вернуться к карточке продукта'), ['/laitovo/production/view', 'id' => $production_scheme_id], ['class' => ' btn btn-round btn-warning', 'target' => '_blank']) ?>
    <?php endif; ?>    
     <?= Html::a(Yii::t('app', 'Добавить новый материал'), ['/laitovo/material/create'], ['class' => ' btn btn-outline btn-round btn-primary', 'target' => '_blank']) ?>
    <?php ActiveForm::end();?> 
    </div> 
 
    <div class="col-md-6"> 
          <h4>Виды материалов для добавления</h4> 
    <?= GridView::widget([ 
        'tableOptions' => ['class' => 'table table-hover'], 
        'rowOptions'=> ['class'=>'success'],
        'dataProvider' => $materialProvider, 
        'columns' => [ 
            [ 
                'attribute'=>'material_id',
                'label'=>'Материал', 
                'format'=>'text',
                'value'=>function($data){ 
                        return $data->getMaterialName() ; 
                    } 
            ], 
            [
                'format' => 'raw',
                'value' => function ($data)use($location_id, $production_scheme_id) {
                    return Html::tag('div',
                        Html::checkbox($data->id, false, ['value'=>$data->id]) .Html::tag('label', false ),
                        [
                            'class' => 'checkbox-custom checkbox-primary text-left',
                            'onclick'=>'
                                 $.get("'.Url::to("/ajax/register-material").'", {"location_id": "'.$location_id.'", "production_scheme_id":"'.$production_scheme_id.'", "material_id":"'.$data->id.'"});   
                            '
                        ]);
                },
            ],
        ], 
    ]); ?> 
    </div> 
<?php $form = ActiveForm::begin(['id'=>'material_id', 'options' => ['data-pjax'=>'']]);?>
 <?= Html::submitButton(Yii::t('app', 'Добавить на участок'),['class' => ' pull-right btn  btn-round btn-danger', 'data'=>['confirm' => Yii::t('yii', 'Добавить материалы на участок?'),'method' => 'post',]]) ?> 
 <?php ActiveForm::end();?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?> 
 <?= Html::submitButton('Перезагрузка',['id'=>'reload_submit_pjax', 'hidden'=>true])?> 
<?php ActiveForm::end(); ?> 

<?php Pjax::end(); ?> 
 
<div class = "clearfix"></div> 
<div class = form-group> 
 

</div>

