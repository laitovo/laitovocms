<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="unit-create" id="unit_create">

    <h1>Добавить единицу</h1>
     <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>
         <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
         <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 
            'onclick'=>'
                 $("#modalMaterials.in").modal("hide");
            ',
            ]) ?>
    </div>
    <?php ActiveForm::end(); ?>