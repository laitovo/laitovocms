<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use common\models\laitovo\UnitAccounting;
use common\models\laitovo\UnitSettlement;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use backend\modules\laitovo\models\ErpProductType;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpUser;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('../menu');
?>


<div class="material-form-index">


    <h1><?= Html::encode('Журнал расхода  материалов') ?></h1>
    <?php $form = ActiveForm::begin(['method'=>'get']); ?>

        <div class="col-xs-4">

            <?= $form->field($model, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'options' => [
                    'class' => 'form-control',
                ],
                'dateFormat' => 'dd.MM.yyyy',
            ]) ?>


        </div>
        <div class="col-xs-4">

            <?= $form->field($model, 'dateTo')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'options' => [
                    'class' => 'form-control',
                ],
                'dateFormat' => 'dd.MM.yyyy',
            ]) ?>

        </div>

        <div class="col-xs-4">
            <?= $form->field($model, 'product_id')->widget(Select2::classname(), [
                'data' =>ArrayHelper::merge(['' => ''], ErpProductType::getProductTypeList()),
                'language' => 'ru',

                //'initValueText' => common\models\laitovo\ErpUser::findOne($made_user_id)->name,
                'options' => [
                    'placeholder' => 'Введите название ...',
                    'class'=>'form-control',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>


<!--     <div class="clearfix"></div>-->
<div class="form-group">
         <div   class="col-xs-2 ">

             <?= $form->field($model, 'unit')->dropDownList(array(0=>'Уч. ед.',1=>'Рубли' ))?>
         </div>
    <div   class="col-xs-2 ">

        <?= $form->field($model, 'location')->widget(Select2::classname(), [
            'data' => ErpLocation::locationList(),
            'language' => 'ru',

            //'initValueText' => common\models\laitovo\ErpUser::findOne($made_user_id)->name,
            'options' => [
                'placeholder' => 'Введите название ...',
                'class'=>'form-control',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
    </div>


<!--    <div   class="col-xs-4 ">-->
<!---->
<!--        --><?//= $form->field($model, 'user')->widget(Select2::classname(), [
//            'data' => ErpUser::allUserNameList(),
//            'language' => 'ru',
//
//            //'initValueText' => common\models\laitovo\ErpUser::findOne($made_user_id)->name,
//            'options' => [
//                'placeholder' => 'Введите имя ...',
//                'class'=>'form-control',
//            ],
//            'pluginOptions' => [
//                'allowClear' => true
//            ],
//        ]);?>
<!--    </div>-->

</div>

    <div class="col-xs-4">
                <br>
             <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => ' btn  btn-outline btn-round btn-primary']) ?>
         </div>

<div class="clearfix">
</div >

    <div>
        <div class="input-search input-search-dark pull-left"><i class="input-search-icon wb-search" aria-hidden="true"></i><input type="text" class="form-control" value="<?=Yii::$app->request->get('search')?>" name="search" placeholder="<?=Yii::t('app', 'Поиск')?>"></div>

<!--        <a href="--><?//=Url::to('report-material-cost')?><!--" class="pull-left btn btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="--><?//=Yii::t('app', 'Сбросить')?><!--"><i class="icon wb-refresh" aria-hidden="true"></i></a>-->
    </div>
    <div class="col-xs-4 ">

        <?= $form->field($model, 'status')->checkbox([])?>

    </div>
</div>

<?php ActiveForm::end(); ?>
<div class="clearfix">
</div >
    <?php if($total_sum): ?>
    <div  style = "font-size:20px; padding-top: 20px; color:green">
        <h> Общая стоимость  <?= Yii::$app->formatter->asDecimal($total_sum)?> руб <h>
    </div>
    <?php endif; ?>




<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'show' => [ 'materialName','unit', 'count'],
        'columns' => [
                [
                    'attribute'=>'materialName',
                    'label'=>'Название материала',
                    'format'=>'html',
                    'value'=>function($data){
                        return Html::a($data['materialName'], ['view', 'id'=>$data['id']]);
                    }
                ],
                [
                    'attribute'=>'unit',
                    'label'=>'Уч. ед',
                    'value'=>function($data){
                        return $data['unit'];
                    }
                ],
                [
                    'attribute'=>'count',
                    'label'=>'Количество',
                    'value'=>function($data) use ($model){

                        return $model->unit ==1 ? Yii::$app->formatter->asDecimal($data['count']) : $data['count'];
                    }
                ],

        ],
    ]); ?>


</div>




