<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use common\models\laitovo\UnitAccounting;
use common\models\laitovo\UnitSettlement;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('../menu');
?>


<div class="material-form-index">

    <h1><?= Html::encode('Журнал  материалов') ?></h1>
    <?php  ActiveForm::begin([
        'method' => 'get',
    ]); ?>

   <a href="<?=Url::to('materials')?>" class="pull-right btn btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="<?=Yii::t('app', 'Сбросить')?>"><i class="icon wb-refresh" aria-hidden="true"></i></a> 
    <div class="input-search input-search-dark pull-right"><i class="input-search-icon wb-search" aria-hidden="true"></i><input type="text" class="form-control" value="<?=Yii::$app->request->get('search')?>" name="search" placeholder="<?=Yii::t('app', 'Поиск')?>"></div>

<?php ActiveForm::end(); ?>

    <p>
         <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить материал')]) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'show' => [ 'materials', 'created_at'],
        'columns' => [
           
            [
               'attribute'=>'materials',
               'label'=>'Материал',
                'format' => 'html',
               'value'=> function($data){
                    return  $data->material->name  ?  Html::a(Html::encode($data->material->name .' '. @$data->parameter), ['view', 'id' => $data->id]): ''; 
               }
            ],
           [
                'attribute'=>'price_settlement',
                'label'=>'Стоимость расч. ед.',
                'format'=>'raw',
                'value' => function($data){
                    return $data->lastSettlement->coefficient_settlement && $data->lastAccounting->accounting_unit_price? @$data->lastSettlement->coefficient_settlement*@$data->lastAccounting->accounting_unit_price : '';
                   },    
            ],
            [
                'attribute' => 'locations',
                'label'=>'Материал на участках',
                'format'=>'html',
                'value'=>function($data){
                    if(isset($data->locations))
                    {
                        $return = '';
                        foreach($data->locations as $location)
                        {
                            $return .= Html::tag('div',

                                Html::tag('span',
                                    ($location->name)
                                ));
                        }
                        return isset($return) ? $return:'';
                    }
                    return '';
                    
                }
            ],        
            [
                'attribute'=>'created_at',
                'label'=>'Дата записи',
                'format'=>'raw',
                'value' => function($data){
                        return $data->created_at ? Yii::$app->formatter->asDateTime($data->created_at) :'';
                   },    
            ],
        ],
    ]); ?>
</div>


