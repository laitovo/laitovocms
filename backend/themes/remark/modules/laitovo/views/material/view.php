<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\laitovo\ErpLocation;
use yii\helpers\Url;
use common\models\laitovo\MaterialLocation;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\MaterialForm */

$this->params['breadcrumbs'][] = ['label' => 'Журнал материалов', 'url' => ['materials']];

Yii::$app->view->registerJs('
      
        $(document).ready(function(){
         $("#w1 > div:nth-child(4) > button").prop("disabled", true);
        });

        $("#materialparameters-locationid").change(function(){
            $("#w1 > div:nth-child(4) > button").prop("disabled", false);
        });
        


        ', \yii\web\View::POS_END);
$this->render('../menu');
?>
<div class="material-form-view">
    
    <h1><?= Html::encode($model->material->name  ? $model->material->name .' '. $model->parameter  : '' ) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'material.name',           
            [
                'attribute' => 'parameter',
                'value' => $model->parameter ? $model->parameter : null
            ], 
            [
              'attribute'=> 'accounting_unit',
              'value'=> $model->lastAccounting->accountingUnit->name  
            ],
            [
              'attribute'=> 'accounting_unit_price',
              'value'=> $model->lastAccounting->accounting_unit_price  
            ],
            [
              'attribute'=> 'unit_settlement',
              'value'=> $model->lastSettlement->unitSettlement->name  
            ],
            [
              'attribute'=> 'coefficient_settlement',
              'value'=> $model->lastSettlement->coefficient_settlement  
            ],
            [
                'attribute'=>'price_settlement',
                'value' => $model->lastAccounting->accounting_unit_price*$model->lastSettlement->coefficient_settlement
            ],
            [
                'attribute'=>'autor_id',
                'value' => $model->lastAccounting->autor->name
            ],
            [
                'attribute'=>'created_at',
                'value' => Yii::$app->formatter->asDateTime($model->lastAccounting->created_at)
            ]
        ],
    ]) ?>

</div>
<div class="location-checkbox">
    
    <?php $form = ActiveForm::begin(['method' => 'get']); ?>
        <h1 style='font-size:25px; color:#A52A2A'>Материал на участке</h1>
         <?= $form->field($model, 'locationId')->checkboxList(ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'), [
             'multiple'=>true,
             
             ])->label(false) ?>
        <div class="form-group">
    
        <?= Html::button( 'Обновить' , ['class' => 'btn btn-warning btn-round',
            'onclick' => '
                     $.post("' . Url::to(['/ajax/save-location-material']) . '",{"location[]":$("input:checkbox:checked").map(function() {return this.value;}).get(), "id": "'.$model->id.'"},function(data){ 
                              if(data){
                              
                                    $("#w1 > div:nth-child(4) > button").prop("disabled", true);
                                    notie.alert(1,"Материалы отправлены на: "+data,4);
                                    
                                };
                         });
             ',
            ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
<div>
 <p>
       <?= Html::a(Yii::t('app', 'Перейти к журналу материалов на участках'), ['index'], ['class' => 'btn btn-outline btn-round btn-primary']) ?>
       <?= Html::a(Yii::t('app', 'Изменить материал'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>
       <?= Html::a(Yii::t('app', 'Добавить новый материал'), ['create'], ['class' => 'btn btn-outline btn-round btn-warning']) ?>
  
       
       <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id],[
        'class' => 'deleteconfirm pull-right btn btn-outline btn-round btn-danger',
        'style'=>'margin-right:5px',
        'data' => [
            'confirm' => Yii::t('yii', 'Отменить этот акт?'),
            'method' => 'post',
        ]]) ?>
         
            
              
  </p>