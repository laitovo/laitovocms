<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Material;
use common\models\laitovo\UnitMaterials;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

$customScript = <<< SCRIPT
$('#modalMaterials').on('hidden.bs.modal', function() { 
         
        $('#reload_submit').click();
         setTimeout(function(){
             $('#newMaterials').remove();
             $('#unitMaterials').remove();
             $('#settlementMaterials').remove();
             $('#material_form_create').remove();
             $('#unit_create').remove();
             $('#w0').remove(); 
                 }, 300);
        
      }); 

SCRIPT;

$this->registerJs($customScript, \yii\web\View::POS_READY); 
$this->render('../menu');

//вывод модального окна 
Modal::begin(['id'=>'modalMaterials', 'size'=>'modal-md']); 
Pjax::begin(['enablePushState' => FALSE]); 
 
Pjax::end(); 
Modal::end(); 
//вывод модального окна 
?>

<div class="material-parameter-form">
    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>
 <?php if($model->isNewRecord): ?>   
    <?= Html::button('<span class="glyphicon glyphicon-ok"></span>', [ 
                        'class' => 'btn btn-warning btn-xs custom_button', 
                        'onclick'=>' 
                                $("#p0").append("<div id=newMaterials></div>"); 
                                 $("#modalMaterials").modal("show").find("#newMaterials").load("'.Url::to(['create-material-name']).'"); 

                            ', 
                        ]); 
                        ?> Добавить новый вид материала*
<?php endif; ?>
    <?= $form->field($model, 'material_id')->dropDownList(ArrayHelper::map(Material::find()->all(), 'id', 'name')) ?>
     
    <?= $form->field($model, 'parameter')->textInput(['maxlength' => true]) ?>
<?php if($model->isNewRecord): ?>  
   <?= Html::button('<span class="glyphicon glyphicon-ok"></span>', [ 
                        'class' => 'btn btn-warning btn-xs custom_button', 
                        'onclick'=>' 
                                $("#p0").append("<div id=unitMaterials></div>"); 
                                 $("#modalMaterials").modal("show").find("#unitMaterials").load("'.Url::to(['create-material-unit']).'"); 

                            ', 
                        ]); 
                        ?> Добавить новый вид учетной единицы*
<?php endif; ?>                        
    <?= $form->field($model, 'accounting_unit')->dropDownList(ArrayHelper::map(UnitMaterials::find()->all(), 'id', 'name')) ?>
 
   
    <?= $form->field($model, 'accounting_unit_price')->textInput() ?>
   
<?php if($model->isNewRecord): ?>  
   <?= Html::button('<span class="glyphicon glyphicon-ok"></span>', [ 
                        'class' => 'btn btn-warning btn-xs custom_button', 
                        'onclick'=>' 
                                $("#p0").append("<div id=settlementMaterials></div>"); 
                                 $("#modalMaterials").modal("show").find("#settlementMaterials").load("'.Url::to(['create-material-unit']).'"); 

                            ', 
                        ]); 
                        ?> Добавить новый вид расчетной единицы*
  <?php endif; ?> 
  <?= $form->field($model, 'unit_settlement')->dropDownList(ArrayHelper::map(UnitMaterials::find()->all(), 'id', 'name'))?>
 
    <?= $form->field($model, 'coefficient_settlement')->textInput() ?>

   

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
   <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?> 
        <?= Html::submitButton('Перезагрузка',['id'=>'reload_submit', 'hidden'=>true])?> 
   <?php ActiveForm::end(); ?> 
   <?php Pjax::end(); ?>

</div>


