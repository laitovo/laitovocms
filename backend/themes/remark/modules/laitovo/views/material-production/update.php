<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\MaterialProductionScheme */

$this->title = 'Update Material Production Scheme: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Material Production Schemes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="material-production-scheme-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
