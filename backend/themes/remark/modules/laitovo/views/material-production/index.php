<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Material Production Schemes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-production-scheme-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Material Production Scheme', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'production_scheme_id',
            'location_id',
            'material_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
