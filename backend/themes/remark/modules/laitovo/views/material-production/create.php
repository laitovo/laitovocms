<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\laitovo\MaterialProductionScheme */

$this->title = 'Create Material Production Scheme';
$this->params['breadcrumbs'][] = ['label' => 'Material Production Schemes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-production-scheme-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
