<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\ErpLocation;
use kartik\select2\Select2;
use backend\modules\laitovo\models\ErpUser;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */

$this->title = Yii::t('app', 'Уволить сотрудника сотрудника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Штатное расписание'), 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'dismissed_id')->widget(Select2::classname(), [
    'data' => ErpUser::userInWork(),
    'theme' => Select2::THEME_BOOTSTRAP,
    'language' => 'ru',
    'options' => [
        'placeholder' => 'Введите имя ...',

    ],
    'pluginOptions' => [
        'allowClear' => true
    ],
])->label('ФИО');?>

<?= $form->field($model, 'date_dismissed')->widget(\yii\jui\DatePicker::classname(), [
    'language' => 'ru',
    'options' => [
        'class' => 'form-control',
    ],
    'dateFormat' => 'yyyy.MM.dd',
]) ?>

<div class="form-group">
    <?= Html::submitButton( Yii::t('app', 'Уволить'), ['class' => 'btn btn-outline btn-round btn-danger']) ?>

</div>

<?php ActiveForm::end(); ?>
