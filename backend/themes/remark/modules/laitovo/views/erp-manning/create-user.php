<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\ErpLocation;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */

$this->title = Yii::t('app', 'Добавить сотрудника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];

$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>

<?= $form->field($model, 'status')->dropDownList($model->statuses) ?>

<?= $form->field($model, 'type')->dropDownList($model->types) ?>

<?= $form->field($model, 'factor')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'date_work')->widget(\yii\jui\DatePicker::classname(), [
    'language' => 'ru',
    'options' => [
        'class' => 'form-control',
    ],
    'dateFormat' => 'yyyy.MM.dd',
]) ?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>

</div>

<?php ActiveForm::end(); ?>

