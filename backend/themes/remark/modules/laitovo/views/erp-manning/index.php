<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use \yii\web\View;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->render('../menu');


$customScript = <<< SCRIPT
      $(document).on("click","[data-remote]",function(e) {
            e.preventDefault();
            $("#p0 > div").remove();
            $("#p0").append("<div id=manning-div></div>");
            $("#modalView #manning-div").load($(this).data('remote'));
        });
        $('#modalView').on('hidden.bs.modal', function(){
            $('#reload_submit_pjax').click();
        }); 
SCRIPT;

$this->registerJs($customScript, View::POS_READY);

//вывод модального окна
Modal::begin(['id'=>'modalView', 'size'=>'modal-md']);
Pjax::begin(['enablePushState' => FALSE]);
Pjax::end();
Modal::end();
//вывод модального окна
?>
<div class="erp-manning-index">
    <?
    $result=[
        0=>'salary_one',
        1=>'salary_two',
        2=>'car_compensation',
        3=>'surcharge',
        4=>'qualification',
        5=>'premium',
        6=>'kpi_one',
        7=>'kpi_two',
        8=>'kpi_three',
    ];
    ?>

    <h1>Штатное расписание</h1>

    <?php Pjax::begin(); ?>


    <?php try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'panel' => ['heading' => false],
            'showPageSummary' => true,
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-user"></i>',
                            ['create-user'],
                            [
                                'title' => 'Добавить нового сотрудника',
                                'class' => 'btn btn-success',
                                'id' => 'create-user',
                            ]
                        )
                        . Html::a('<i class="glyphicon glyphicon-user"></i>',
                            ['dismissed-user'],
                            [
                                'title' => 'Уволить сотрудника',
                                'class' => 'btn btn-danger ',
                                'id' => 'dismissed-user'
                            ]
                        )
                ],
                '{export}',
                '{summary}'
            ],
            'pjax' => true,
            'columns' => [
                [
                    'attribute' => 'division_id',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data->division_id ? $data->division->title . ($data->division->article ? '<br>' . $data->division->article : '') . '  ' . Html::a('<span class="glyphicon glyphicon-wrench" style = "color:green"></span>', '#modalView', [
                                'title' => 'Редактировать', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['update-division-modal', 'division_id' => $data->division_id])]) : '';
                    },
                    //                    'pageSummary'=>false,
                    'group' => true,  // enable grouping
                    'header' => 'Отдел ' . Html::a('<span class="glyphicon glyphicon-plus" style = "color:green"></span>', '#modalView', [
                            'title' => 'Создать новый отдел', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['create-division-modal'])])
                        . ' '
                        . Html::a('<span class="glyphicon glyphicon-minus" style = "color:red"></span>', '#modalView', [
                            'title' => 'Удалить отдел', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['delete-division-modal'])])
                ],
                [
                    'attribute' => 'title',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data->title ? $data->title . ($data->article ? ' - ' . $data->article : '') : '';
                    },
                    //                    'pageSummary'=>false,
                    'header' => 'Должность ' . Html::a('<span class="glyphicon glyphicon-plus" style = "color:green"></span>', '#modalView', [
                            'title' => 'Добавить должность', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['create-position-modal'])])
                        . ' '
                        . Html::a('<span class="glyphicon glyphicon-minus" style = "color:red"></span>', '#modalView', [
                            'title' => 'Удалить должность', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['delete-position-modal'])])
                ],
                [
                    'attribute' => 'user_id',
                    'value' => function ($data) {
                        return $data->user ? $data->user->name : '';
                    },
                    //
                    'header' => 'Сотрудник ' . Html::a('<span class="glyphicon glyphicon-plus" style = "color:green"></span>', '#modalView', [
                            'title' => 'Добавить в штатное расписанеи', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['user-position-modal'])])
                        . ' '
                        . Html::a('<span class="glyphicon glyphicon-minus" style = "color:red"></span>', '#modalView', [
                            'title' => 'Убрать из штатного расписания', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['delete-user-position-modal'])]),
                ],
                [
                    'label' => 'Оклад-1',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->salary_one ? $data->salary_one : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'Оклад-2',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->salary_two ? $data->salary_two : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'Компенсация а/м',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->car_compensation ? $data->car_compensation : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'Сезонная доплата',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->surcharge ? $data->surcharge : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'Квалификация',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->qualification ? $data->qualification : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'Премия',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->premium ? $data->premium : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'KPI-1',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->kpi_one ? $data->kpi_one : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'KPI-2',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->kpi_two ? $data->kpi_two : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'KPI-2',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) {
                        return $data->kpi_three ? $data->kpi_three : 0;
                    },
                    'pageSummary' => true,
                ],
                [
                    'label' => 'Итого',
                    //                    'format'=>['decimal', 2],
                    'value' => function ($data) use ($result) {
                        $sum = 0;

                        foreach ($data as $key => $value) {
                            if (in_array($key, $result))
                                $sum += (int)$value;
                        }
                        return $sum;
                    },
                    'pageSummary' => true,
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{today_action}',
                    'buttons' => [
                        'today_action' => function ($url, $data) {
                            return Html::a('<span class="glyphicon glyphicon-wrench" style = "color:green"></span>', '#modalView', [
                                'title' => 'Редактировать', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['update-modal', 'id' => $data->id])]);
                        }
                    ],
                    'pageSummary' => false,
                ],
            ]
        ]);
    } catch (Exception $e) {
        echo "Невозможно отобразить таблицу, обратитесь в IT Отдел";
    } ?>


    <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>
    <?= Html::submitButton('Перезагрузка',['id'=>'reload_submit_pjax', 'hidden'=>true])?>
    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>
</div>


