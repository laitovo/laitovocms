<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpPosition;
use common\models\laitovo\ErpDivision;
use yii\helpers\Url;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpManning */


$this->params['breadcrumbs'][] = ['label' => 'Штатное расписание', 'url' => ['index']];

?>
<div class="erp-user-create">
    <h1>Назначить сотрудника в штатное расписание</h1>

    <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>


        <?= $form->field($user_position, 'division_id')->dropDownList(ErpDivision::division_list(),[
            'onchange'=>'
                 $.get("'. Url::to('/ajax/position-list') .'", {"division_id":$(this).val()},function(data){
                                $("#erpuserposition-position_id").html("<option></option>");
                                $.each(data, function(key, value) {
                                    $("#erpuserposition-position_id").append($("<option></option>").attr("value",key).text(value));
                                });
                           });
            '
        ])->label('Отдел') ?>

        <?= $form->field($user_position, 'position_id')->dropDownList(ErpPosition::position_list_not_user()) ?>

        <?= $form->field($user_position, 'user_id')->dropDownList(ErpUser::userListWorkNotManning()); ?>



        <div class="form-group">
            <?= Html::submitButton('Назначить', ['class' =>'btn btn-outline btn-round btn-success',
                'onclick'=>'$("#modalView.in").modal("hide");']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>