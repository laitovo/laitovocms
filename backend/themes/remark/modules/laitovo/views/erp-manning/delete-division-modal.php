<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\laitovo\ErpDivision;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpManning */

?>
<div class="erp-user-create">
    <h1>Удалить отдел</h1>

    <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>

    <?= $form->field($model, 'id')->dropDownList(ErpDivision::delete_list())->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Удалить', ['class' =>'btn btn-outline btn-round btn-danger',
            'onclick'=>'$("#modalView.in").modal("hide");'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>