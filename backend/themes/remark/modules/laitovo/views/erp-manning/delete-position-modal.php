<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\laitovo\ErpDivision;
use common\models\laitovo\ErpPosition;
use yii\helpers\Url;
?>

<div class="erp-manning-form">
    <h1>Удалить должность</h1>

    <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>

    <?= $form->field($model, 'division_id')->dropDownList(ErpDivision::division_list(),[
            'onchange'=>'
                 $.get("'. Url::to('/ajax/position-list') .'", {"division_id":$(this).val()},function(data){
                                $("#erpposition-id").html("<option></option>");
                                $.each(data, function(key, value) {
                                    $("#erpposition-id").append($("<option></option>").attr("value",key).text(value));
                                });
                           });
            '
    ])?>

    <?= $form->field($model, 'id')->dropDownList(ErpPosition::position_list()) ?>

    <div class="form-group">
        <?= Html::submitButton('Удалить', ['class' =>'btn btn-outline btn-round btn-danger',
            'onclick'=>'$("#modalView.in").modal("hide");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>