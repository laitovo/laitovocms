<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpPosition;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpManning */
/* @var $form yii\widgets\ActiveForm */
$customScript = <<< SCRIPT
        $('#modalView').on('hidden.bs.modal', function(){
            $('#reload_submit_pjax').click();
        }); 
SCRIPT;

$this->registerJs($customScript, \yii\web\View::POS_READY);

//вывод модального окна
Modal::begin(['id'=>'modalView', 'size'=>'modal-md']);
Pjax::begin(['enablePushState' => FALSE]);
Pjax::end();
Modal::end();
//вывод модального окна
?>

<div class="erp-manning-form">
    <?php Pjax::begin(); ?>

        <?php $form = ActiveForm::begin(); ?>



<!--            --><?//= Html::button('<span class="glyphicon glyphicon-plus"></span>', [
//                'class' => 'btn btn-success btn-xs custom_button',
//                'onclick'=>'
//                                        $("#p0 > div").remove();
//                                        $("#p0").append("<div id=new_user></div>");
//                                        $("#modalView").modal("show").find("#new_user").load("'.Url::to(['create-user-modal']).'");
//
//                                    ',
//            ]);
//            ?><!-- *добавить нового сотрудника-->

<!--            --><?//= $form->field($model, 'user_id')->dropDownList(ErpUser::userListWork()) ?>

<!--            --><?//= Html::button('<span class="glyphicon glyphicon-plus"></span>', [
//                'class' => 'btn btn-success btn-xs custom_button',
//                'onclick'=>'
//                                                $("#p0 > div").remove();
//                                                $("#p0").append("<div id=new_position></div>");
//                                                $("#modalView").modal("show").find("#new_position").load("'.Url::to(['create-position-modal']).'");
//
//                                            ',
//            ]);
//            ?><!-- *добавить новую должность-->

            <?= $form->field($model, 'position_id')->dropDownList(ErpPosition::positionList()) ?>

            <?= $form->field($model, 'salary_one')->textInput() ?>

            <?= $form->field($model, 'salary_two')->textInput() ?>

            <?= $form->field($model, 'car_compensation')->textInput() ?>

            <?= $form->field($model, 'surcharge')->textInput() ?>

            <?= $form->field($model, 'qualification')->textInput() ?>

            <?= $form->field($model, 'premium')->textInput() ?>

            <?= $form->field($model, 'kpi_one')->textInput() ?>

            <?= $form->field($model, 'kpi_two')->textInput() ?>

            <?= $form->field($model, 'kpi_three')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-outline btn-round btn-success' : 'btn btn-outline btn-round btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>

        <?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>
            <?= Html::submitButton('Перезагрузка',['id'=>'reload_submit_pjax', 'hidden'=>true])?>
        <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>


