<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpManning */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Штатное расписание', 'url' => ['index']];
$this->render('../menu');
?>
<div class="erp-manning-view">

    <h1><?=$model->user->name ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'location_id',
            'user_id',
            'position_id',
            'salary_one',
            'salary_two',
            'car_compensation',
            'surcharge',
            'qualification',
            'premium',
            'kpi_one',
            'kpi_two',
            'kpi_three',
        ],
    ]) ?>

</div>
