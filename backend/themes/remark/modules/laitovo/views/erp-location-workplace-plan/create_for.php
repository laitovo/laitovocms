<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\grid\GridView;
use backend\modules\laitovo\models\ErpLocation;
use yii\widgets\ActiveForm;



/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplacePlan */

$this->title = 'Планирование. '.$model->workplace->location_number.' на участке '.$model->workplace->location->name;
$this->params['breadcrumbs'][] = ['label' => 'Монитор планирования', 'url' => ['erp-location-workplace/monitor-plan']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-plan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Pjax::begin(); ?>

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
            <?= $form->field($searchModel, 'curDate')->widget(\yii\jui\DatePicker::classname(), [
                //'language' => 'ru',
                'options' => [
                    'class' => 'form-control',
                    'onchange' => '$("#search").click();',
                ],
                'dateFormat' => 'dd.MM.yyyy',
            ]) ?>

        <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary hidden','id' => 'search']) ?>
        </div>
    <?php ActiveForm::end(); ?>


    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'workplace_id',
            'user.name',
            [
                'attribute' => 'time_from',
                'label' => 'Начало работы',
                'format' => 'datetime',
            ],
            [
                'attribute' => 'time_to',
                'label' => 'Окончание работы',
                'format' => 'datetime',
            ],
            // 'created_at',
            // 'author_id',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{delete}'
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
