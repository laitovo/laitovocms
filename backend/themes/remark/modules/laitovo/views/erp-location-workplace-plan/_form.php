<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\ErpUser;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplacePlan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-location-workplace-plan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'workplace_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'theme' => Select2::THEME_BOOTSTRAP,
        'data' => ArrayHelper::map(ErpUser::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Выберите сотрудника ...'],
        'language' => 'ru',
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'viewTimeFrom')->widget(DateTimePicker::className(), [
        'language' => 'ru',
        'size' => 'ms',
    ]);?>

    <?= $form->field($model, 'viewTimeTo')->widget(DateTimePicker::className(), [
        'language' => 'ru',
        'size' => 'ms',
    ]);?>

    <!-- <?= $form->field($model, 'created_at')->textInput() ?> -->

    <!-- <?= $form->field($model, 'author_id')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
