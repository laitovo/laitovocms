<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpJobScheme */
/* @var $scheme backend\modules\laitovo\models\ErpJobType */

$this->title = Yii::t('app', 'Добавить вид работ для маршрута');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Маршрутизация'), 'url' => ['erp-job-scheme/index']];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $scheme->name), 'url' => ['erp-job-scheme/view_job_schemes', 'scheme_id' => $scheme->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
