<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpJobType;
use common\models\laitovo\ProductionScheme;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal; 



$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
if(isset( ProductionScheme::findOne($production_scheme_id)->title)){
    $this->params['breadcrumbs'][] = ['label' =>  ProductionScheme::findOne($production_scheme_id)->title, 'url' => ['production/view', 'id'=>$production_scheme_id]];
}
$this->params['searchmodel'] = true;
$this->render('../menu');

$customScript = <<< SCRIPT
      $(document).on("click","[data-remote]",function(e) {
            e.preventDefault();
            $("#p0").append("<div id=job-div></div>");
            $("#modalView #job-div").load($(this).data('remote'));
        });
        $('#modalView').on('hidden.bs.modal', function(){
            $('#reload_submit').click();
           $("#p0 > div").remove();
           $("#p0 > div").remove();
            $("#w0").remove();
        }); 
SCRIPT;

$this->registerJs($customScript, \yii\web\View::POS_READY); 
//вывод модального окна 
Modal::begin(['id'=>'modalView', 'size'=>'modal-md']); 
Pjax::begin(['enablePushState' => FALSE]);
Pjax::end(); 
Modal::end(); 
//вывод модального окна 


?>
<?php if (isset(ProductionScheme::findOne($production_scheme_id)->title) && isset(ErpLocation::findOne($location_id)->name)): ?> 
<h2><?php echo 'Редактирование видов работ на производственном цикле "'. ProductionScheme::findOne($production_scheme_id)->title .'" участка ' . ErpLocation::findOne($location_id)->name ; ?></h2>
<?php endif;?> 

<?php Pjax::begin(); ?>

 <div class="col-md-6">
    <h4 id='tables'>Виды работ на участке </h4>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $jobTypeSchemeProvider,
        'rowOptions'=> ['class'=>'info'],
        // 'filterModel' => $searchModel,
       // 'show' => ['id','title','location_id','author_id','created_at'],
        'columns' => [
            
                ['class' => 'yii\grid\ActionColumn', 
                    'buttons'=>[
                        'view'=>function($url, $data){
                                return Html::a(@$data->type->title , '#modalView', [
                                        'title' => 'Открыть форму редактирования',
                                    'data-toggle'=>'modal',
                                    'data-backdrop'=>false,
                                    'data-remote'=>Url::to(['modal-update','id'=>@$data->type->id])
                                    ]);
                            },
                    ],
                    'template'=>'{view}'
                ],
                [
                    'label'=>'Ед. измерения',
                    'value'=>function($data){
                        return $data->type_id? $data->type->unit : '';
                    }
                ],                    
                [
                    'label'=>'Количество работ на участке',
                    'value'=> function($data){
                        return $data->job_count? $data->job_count : '';
                    }
                ],
                
               [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{today_action}',
                    'buttons' => [
                                    'today_action' => function ($url, $data) use ($location_id, $production_scheme_id) {
                                    return Html::a('<span class="glyphicon glyphicon-plus" style="color:green"></span>',
                                            [
                                                'register-job-type-in-location',                                                
                                                'location_id' => $location_id,
                                                'production_scheme_id' => $production_scheme_id,
                                                
                                            ],
                                            [ 
                                                'data-pjax'=>true,
                                                'data' => [
                                                    'method' => 'post',
                                                   
                                                    'params' => [
                                                        'upregister'=>true,
                                                        'type_id'=>$data->type_id,
                                                        'count'=>$data->job_count,
                                                       
                                                      ]
                                                   ],
                                            ],
                                    [
                                        'title' => Yii::t('app', 'Убрать из группы'),
                                    ]);
                                }
                        ],
                ],         
               [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{today_action}',
                    'buttons' => [
                                    'today_action' => function ($url, $data) use ($location_id, $production_scheme_id) {
                                    return $data->job_count==1? Html::a('<span class="glyphicon glyphicon-minus" style="color:red"></span>',
                                            [
                                                'register-job-type-in-location',
                                                'production_scheme_id' => $production_scheme_id,
                                                'location_id' => $location_id,
                                            ],
                                            [ 
                                                'data-pjax'=>true,
                                                'data' => [
                                                    'method' => 'post',
                                                    'params' => [
                                                        'unregister'=>true,
                                                        'type_id'=>$data->type_id,
                                                        'count'=>$data->job_count,
                                                      ],
                                                     'confirm' => Yii::t('yii', 'Удалить из группы?'),
                                                ],
                                            ]
                                            
                                    ):
                                            Html::a('<span class="glyphicon glyphicon-minus" style="color:red"></span>',
                                                [
                                                    'register-job-type-in-location',
                                                    'production_scheme_id' => $production_scheme_id,
                                                    'location_id' => $location_id,
                                                ],
                                                [ 
                                                    'data-pjax'=>true,
                                                    'data' => [
                                                        'method' => 'post',
                                                        'params' => [
                                                            'unregister'=>true,
                                                            'type_id'=>$data->type_id,
                                                            'count'=>$data->job_count,
                                                          ],
                                                       
                                                    ],
                                                ]
                                              );
                                }
                        ],
                ],         
        ],
    ]); ?>
</div>
<?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]);?>
<div class="col-md-6">
          <h4>Виды работ для добавления</h4>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $jobTypeProvider,
        'rowOptions'=> ['class'=>'success'],
        'columns' => [
            
            [
                'label'=>'Название',
                'value'=> function($data){
                     return $data->title? $data->title : '';
                  }  
            ],
            [
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::tag('div',
                        Html::checkbox('job_type['.$data->id.']', false, ['value'=>$data->id]) .Html::tag('label', false ),
                        [
                            'class' => 'checkbox-custom checkbox-primary text-left',
                        ]);
                },
            ],
        ],
    ]); ?>
   <?= Html::submitButton(Yii::t('app', 'Добавить на участок'),['class' => ' pull-right btn  btn-round btn-danger', 'data'=>['confirm' => Yii::t('yii', 'Добавить материалы на участок?'),'method' => 'post',]]) ?> 
</div >
 <?php ActiveForm::end();?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?> 
 <?= Html::submitButton('Перезагрузка',['id'=>'reload_submit', 'hidden'=>true])?> 
<?php ActiveForm::end(); ?> 

<?php Pjax::end(); ?>

<div class = "clearfix"></div>

<div class = form-group>
<?= Html::a(Yii::t('app', 'Вернуться карточке продукта'), ['/laitovo/production/view', 'id' => $production_scheme_id], ['class' => 'btn  btn-round btn-warning']) ?>
<?= Html::a(Yii::t('app', 'Добавить вид работ'), ['/laitovo/erp-job-types/create'], ['class' => ' btn btn-outline btn-round btn-primary', 'target' => '_blank']) ?>
</div>

