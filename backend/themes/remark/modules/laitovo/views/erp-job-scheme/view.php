<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpScheme */
/* @var $type backend\modules\laitovo\models\ErpJobType */

$this->title = 'Просмотр вида работ для маршрута';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Виды работ'), 'url' => ['erp-job-types/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $type->title), 'url' => ['erp-job-types/view', 'id' => $type->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => $model->location ? Html::a(Html::encode($model->location->name), ['erp-location/view', 'id' => $model->location->id]) : null,
        ],
        [
            'attribute' => 'type_id',
            'format' => 'html',
            'value' => $model->type ? Html::a(Html::encode($model->type->title), ['erp-job-types/view', 'id' => $model->type->id]) : null,
        ],
        [
            'attribute' => 'production_scheme_id',
            'format' => 'html',
            'value' => $model->productionScheme->title ? Html::a(Html::encode($model->productionScheme->title), ['erp-scheme/view', 'id' => $model->productionScheme->id]) : null,
        ],
        'rule',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => $model->author ? $model->author->name : null,
        ],
    ],
]) ?>

<?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>


<?= $model->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
    'data-toggle' => "tooltip",
    'data-original-title' => Yii::t('yii', 'Delete'),
    'data' => [
        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
        'method' => 'post',
    ],
]) ?>