<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpJobType;
use common\models\laitovo\ProductionScheme;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'production_scheme_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ProductionScheme::find()->all(), 'id', 'title'))) ?>
<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>
<?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpJobType::find()->all(), 'id', 'title'))) ?>

<?= $form->field($model, 'job_count')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'rule')->textInput(['maxlength' => true]) ?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>

</div>

<?php ActiveForm::end(); ?>
