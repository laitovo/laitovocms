<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $scheme common\models\laitovo\ErpScheme */

$this->title = $scheme->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Маршрутизация видов работ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create', 'scheme_id' => $scheme->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['location_id', 'type_id'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view', 'id' => $data->location->id]) : null;
            },
        ],
        [
            'attribute' => 'type_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->type ? Html::a(Html::encode($data->type->title), ['erp-job-type/view', 'id' => $data->type_id]) : null;
            },
        ],
        'rule:ntext',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>

