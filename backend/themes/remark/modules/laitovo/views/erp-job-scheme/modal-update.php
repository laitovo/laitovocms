<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpJobType;
use common\models\laitovo\ProductionScheme;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Маршрутизация'), 'url' => ['erp-job-scheme/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?php $form = ActiveForm::begin(['options' => ['data-pjax'=>'']]); ?>
<h1>Изменить вид работ <?=@$model->title?></h1>
<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>

<?= $form->field($model, 'production_scheme_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ProductionScheme::find()->all(), 'id', 'title'))) ?>

<?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'rate')->textInput(['maxlength' => true]) ?>

 <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success',
            'onclick'=>'$("#modalView.in").modal("hide");']) ?>
    </div>

<?php ActiveForm::end(); ?>


