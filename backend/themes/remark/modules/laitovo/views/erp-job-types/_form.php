<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use common\models\laitovo\ProductionScheme;
use backend\modules\laitovo\models\ErpJobType;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>

<?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'rate')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'status')->dropDownList(ErpJobType::$statuses) ?>


<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>

</div>

<?php ActiveForm::end(); ?>
