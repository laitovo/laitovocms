<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpJobType;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Виды работ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['title', 'location_id', 'rate', 'unitPrice'],
    'rowOptions' => function ($data) {
        if($data->status == ErpJobType::INACTIVE){
            return ['class' => 'danger'];
        }
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($data) {
                return $data->title ? Html::a(Html::encode($data->title), ['view', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view', 'id' => $data->location->id]) : null;
            },
        ],
        'unit',
        'rate',
        'jobRate',
        [
            'attribute'=>'unitPrice',
            'label'=>'Цена за единицу',
            'value'=>function($data){
                    return $data->unitPrice ? Yii::$app->formatter->asDecimal($data->unitPrice) : '';
            }
        ],
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
            'attribute' => 'status',
            'value' => function ($data) {
                return $data->getStatus();
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
