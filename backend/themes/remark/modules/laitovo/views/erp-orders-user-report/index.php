<?php

use yii\bootstrap\ActiveForm;
use backend\modules\laitovo\models\ErpOrdersUserReport;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];

$this->render('../menu');
?>

<h1>Отчет по заказам диллеров</h1>

<?php $form = ActiveForm::begin(['method' => 'get']); ?>

<div class="col-xs-3">
    <span style="font-size: 20px">Начало периода</span>
</div>
<div class="col-xs-3">
    <span style="font-size: 20px">Конец периода</span>
</div>
<div class="clearfix"></div>
<div class="col-xs-3">
    <?= $form->field($model, 'year_from')->dropDownList(ErpOrdersUserReport::$years)->label('год',
        ['style' => ['font-size' => '12px']]) ?>
    <?= $form->field($model, 'month_from')->dropDownList(ErpOrdersUserReport::$months)->label('месяц',
        ['style' => ['font-size' => '12px']]) ?>
</div>

<div class="col-xs-3">

    <?= $form->field($model, 'year_to')->dropDownList(ErpOrdersUserReport::$years)->label('год',
        ['style' => ['font-size' => '12px']]) ?>
    <?= $form->field($model, 'month_to')->dropDownList(ErpOrdersUserReport::$months)->label('месяц',
        ['style' => ['font-size' => '12px']]) ?>

</div>

<div>
    <div class="col-xs-3 ">
        <?= $form->field($model, 'country')->dropDownList(ErpOrdersUserReport::$countrys) ?>
    </div>
    <div class="col-xs-3">
        <?= $form->field($model, 'client_name')->textInput() ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-3 ">
        <br>
        <?= $form->field($model, 'dealer')->checkbox([]) ?>

    </div>
    <div class="col-xs-3 ">
        <br>
        <?= $form->field($model, 'compare')->checkbox([]) ?>
    </div>
</div>
<div>

    <div class="col-xs-4 ">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Сформировать'),
            ['class' => ' pull-right btn  btn-outline btn-round btn-primary']) ?>
    </div>

</div>
<div class="clearfix"></div>
<?php if ($model->compare ) : ?>
    <div class="col-xs-4 ">
        <br>
        <?= $form->field($model, 'last_year')->checkbox([]) ?>

    </div>
<?php endif; ?>
<div class="clearfix"></div>

<div>
    <span style="color: green"> * Если не выбраны даты то отчет за текущий год  </span><br>
    <span style="color: red"> * Данные для сравнения за тот же период за прошлый год  </span><br>
</div>
<div class="clearfix"></div>

<?php ActiveForm::end(); ?>

<?php
$fields = [];
foreach ($model->getDateList() as $date) {
    $fields[] = [
        'attribute' => $date,
        'format' => 'html',
        'encodeLabel' => false,
        'label' => ErpOrdersUserReport::date_lable($date),
        'value' => function ($data, $key) use ($date) {

            return (isset($data[$date]) ? number_format($data[$date], 2, ',', ' ') : '')
                . '<br>' . '<span style="color: red">'
                . (isset($data[ErpOrdersUserReport::lastDate($date)]) ? number_format($data[ErpOrdersUserReport::lastDate($date)], 2, ',', ' ') : '') . '</span>' ;
        }
    ];
}

?>

<?= GridView::widget([
    'tableOptions' => [
        'class' => 'table table-hover',
        'style' => [
            'font-size' => '11px',
            'font-weight' => 800
        ]
    ],
    'dataProvider' => $dataProvider,
    'columns' => ArrayHelper::merge(
        [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'client_name',
                'label' => 'ФИО',
            ],
            [
                'attribute' => 'region_name',
                'label' => 'Откуда',
                'value' => function ($data) {
                    return $data['region_name'] ? $data['region_name'] : '';
                }
            ],
            [
                'attribute' => 'total',
                'label' => 'Итого за период',
                'format' => 'raw',
                'value' => function ($data) {

                    return (isset($data['total']) ? number_format($data['total'], 2, ',', ' ') : '')
                        . '<br>' . '<span style="color: red">'
                        . (isset($data['last_total']) ? number_format($data['last_total'], 2, ',', ' ') : '') . '</span>' ;
                }
            ],
        ],
        $fields
    )
])

?>
