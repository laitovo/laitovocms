<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpNaryad;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $show string */

$this->title = Yii::t('app', 'Все наряды');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<div style="position: relative">
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], [
    'class'               => 'btn btn-icon btn-outline btn-round btn-primary',
    'style'               => 'margin-bottom:16px;',
    'data-toggle'         => "tooltip",
    'data-original-title' => Yii::t('app', 'Добавить'),
]) ?>

<div class="page-header-actions">
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>
<div>
    <div class="text-center">Поиск по номерам заказа</div>
<?php if (Yii::$app->request->get('search-by-order')):?>
    <a href="<?=Url::to(['index'])?>" class="pull-right btn btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="<?=Yii::t('app', 'Сбросить')?>"><i class="icon wb-refresh" aria-hidden="true"></i></a>
<?php endif?>
    <div class="input-search input-search-dark pull-right">
        <input type="text" class="form-control" value="<?=Yii::$app->request->get('search-by-order')?>" name="search-by-order" placeholder="<?=Yii::t('app', 'Поиск')?>">
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
</div>
<hr>
<?php Pjax::begin(['id' => 'pjax-naryad-list']); ?>

<?php ActiveForm::begin([
    'id' => 'search-form',
    'action' => [''],
    'method' => 'post',
    'options' => ['data-pjax' => 1]
]); ?>

    <div class="btn-group" id="rb-group-show" data-toggle="buttons">
        <label class="btn btn-default <?php if(!$show || $show == 'all'):?>active<?php endif;?>">
            <input onchange="$('.page-header-actions input[name=search]').val('');$('#search-form').submit();" type="radio" name="erpNaryad_index_show" value="all" <?php if(!$show || $show == 'all'):?>checked<?php endif;?>> Все (<?=$countAll?>)
        </label>
        <label class="btn btn-default <?php if($show == 'onProduction'):?>active<?php endif;?>">
            <input onchange="$('.page-header-actions input[name=search]').val('');$('#search-form').submit();" type="radio" name="erpNaryad_index_show" value="onProduction" <?php if($show == 'onProduction'):?>checked<?php endif;?>> Только на производстве (<?=$countOnProduction?>)
        </label>
        <label class="btn btn-default <?php if($show == 'onPause'):?>active<?php endif;?>">
            <input onchange="$('.page-header-actions input[name=search]').val('');$('#search-form').submit();" type="radio" name="erpNaryad_index_show" value="onPause" <?php if($show == 'onPause'):?>checked<?php endif;?>> Только на паузе (<?=$countOnPause?>)
        </label>
    </div>
<?= Html::hiddenInput('update_erpNaryad_index_show',true); ?>

<?php ActiveForm::end(); ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'show' => ['id', 'article', 'order_id', 'location_id', 'status'],
    'columns' => array_filter([
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['view', 'id' => $data->id]) : null;
            },
        ],
        'article',
        [
        'attribute'=>'windowType',
        'label'=>'Название продукта',
        'filter'=> ErpNaryad::filterTypeStatic(),
        'value'=>function ($data) {
            return ErpNaryad::windowTypeStatic($data->article);
        },
    ],
        [
            'attribute' => 'window',
            'label'     => 'Оконный проем',
            'filter'    => array("FW" => "ПШ", "FV" => "ПФ", "FD" => "ПБ", "RD" => "ЗБ", "RV" => "ЗФ", "BW" => "ЗШ"),
            'value'     => function ($data) {
                return ErpNaryad::windowStatic($data->article);
            },
        ],
        [
        'attribute'=>'tkan',
        'label'=>'Тип ткани',
        'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
        'value'=>function ($data) {
            return ErpNaryad::tkanStatic($data->article);
        },
    ],
        [
            'attribute' => 'order_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view', 'id' => $data->order->id]) : null;
            },
        ],
        [
            'attribute'=>'location_id',
            'format'=>'raw',
            'value'=>function ($data) {
                return $data->locationstart 
                ? Html::a(Html::encode($data->locationstart->name), ['erp-location/view','id'=>$data->locationstart->id]).' <small>'.($data->location ? $data->location->name : 'Облако').'</small>'
                : ($data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view','id'=>$data->location->id]) : null);
            },
        ],
        [
            'attribute'=>'valueLiteral',
            'label'=>'Литера',
            'value'=>function ($data) {
                return $data->currentProductionLiteral;
            },
        ],
        [
            'attribute' => 'scheme_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view', 'id' => $data->scheme->id]) : null;
            },
        ],
        'status',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
            'attribute' => 'sort',
            'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
            'filter' => ErpNaryad :: prioritets(),
        ],
        [
            'class'  => 'yii\grid\ActionColumn',
            'header' => 'Пауза',
            'template' => '{pause}',
            'buttons' => [
                'pause' => function ($url, $data) {
                    if ($data->isPaused()) {
                        return Html::button('<i class="fa fa-play"></i>', [
                            'class' => 'btn btn-outline btn-round btn-info',
                            'data'  => [
                                'id' => $data->id,
                                'toggle'  => 'tooltip',
                                'title'   => Yii::t('app', 'Возобновить')
                            ],
                            'onclick' => '
                                var id = $(this).data("id");
                                var handle_function = function () {
                                    $.ajax({
                                        url: "' . Url::to(['ajax-unpause']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите снять наряд с паузы?", "Да", "Нет", handle_function);
                            '
                        ]);
                    } else {
                        $canBePaused = $data->canBePaused();
                        return Html::button('<i class="fa fa-pause" data-toggle="tooltip" data-title="' . ($canBePaused ? '' : Yii::t('app', 'Данный наряд не может быть остановлен')) . '"></i>', [
                            'class' => 'btn btn-outline btn-round btn-info',
                            'data'  => [
                                'id' => $data->id,
                                'toggle'  => 'tooltip',
                                'title'   => Yii::t('app', 'Остановить')
                            ],
                            'disabled' => !$canBePaused,
                            'onclick' => '
                                var id = $(this).data("id");
                                var handle_function = function () {
                                    $.ajax({
                                        url: "' . Url::to(['ajax-pause']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите поставить наряд на паузу?", "Да", "Нет", handle_function);
                            '
                        ]);
                    }
                },
            ],
        ],
        !in_array(Yii::$app->user->getId(), [21, 26, 43, 46, 36, 55, 56, 45]) ? null : [
            'class'  => 'yii\grid\ActionColumn',
            'header' => 'Срочность',
            'template' => '{priority}',
            'buttons' => [
                'priority' => function ($url, $data) {
                    $canUpdatePriority = $data->canUpdatePriority();
                    return implode(' ', [
                        Html::button('<i class="fa fa-backward text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'Низкий') : Yii::t('app', 'Данный наряд не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'margin-right:10px;padding:3px;background-color:transparent;' . ($data->sort == 7 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 7
                            ],
                            'disabled' => !$canUpdatePriority || $data->sort == 7,
                            'onclick' => '
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность наряда?", "Да", "Нет", handle_function);
                            '
                        ]),
                        Html::button('<i class="fa fa-play text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'Обычный') : Yii::t('app', 'Данный наряд не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'margin-right:10px;padding:3px;background-color:transparent;' . ($data->sort == 6 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 6
                            ],
                            'disabled' => !$canUpdatePriority || $data->sort == 6,
                            'onclick' => '
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность наряда?", "Да", "Нет", handle_function);
                            '
                        ]),
                        Html::button('<i class="fa fa-forward text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'Срочный') : Yii::t('app', 'Данный наряд не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'margin-right:10px;padding:3px;background-color:transparent;' . ($data->sort == 4 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 4
                            ],
                            'disabled' => !$canUpdatePriority || $data->sort == 4,
                            'onclick' => '
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность наряда?", "Да", "Нет", handle_function);
                            '
                        ]),
                        Html::button('<i class="fa fa-step-forward text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'Большой заказ') : Yii::t('app', 'Данный наряд не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'margin-right:10px;padding:3px;background-color:transparent;' . ($data->sort == 3 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 3
                            ],
                            'disabled' => !$canUpdatePriority || $data->sort == 3,
                            'onclick' => '
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность наряда?", "Да", "Нет", handle_function);
                            '
                        ]),
                        Html::button('<i class="fa fa-fast-forward text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'TopSpeed') : Yii::t('app', 'Данный наряд не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'padding:3px;background-color:transparent;' . ($data->sort == 2 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 2
                            ],
                            'disabled' => !$canUpdatePriority || $data->sort == 2,
                            'onclick' => '
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-naryad-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность наряда?", "Да", "Нет", handle_function);
                            '
                        ]),
                    ]);
                },
            ],
        ],
        // 'created_at',
        // 'updated_at',
        // 'author_id',
        // 'updater_id',
        // 'json:ntext',
        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ]
    ])
]); ?>

<?php Pjax::end(); ?>

<?php Yii::$app->view->registerJs('
    setInterval(function() { 
        $.pjax.reload({container : "#pjax-naryad-list"});
    }, 360*1000);

    $("#pjax-naryad-list").on("pjax:end", function() {
      $("#pjax-naryad-list [data-toggle=tooltip]").tooltip();
    })
', \yii\web\View::POS_END);
?>