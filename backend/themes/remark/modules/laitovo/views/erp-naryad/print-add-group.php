<?php
/**
 * @var $article string
 * @var $title string
 * @var $barcode string
 * @var $number integer
 * @var $workOrders ErpNaryad[]
 */

use backend\modules\laitovo\models\ErpNaryad;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>
<p class="page-break-<?= $barcode ?>"></p>

<table width="500" cellpadding="9" cellspacing="0" style="border: 2px solid; border-bottom: 1px dashed;border-collapse: collapse;">
    <tr>
        <td style="text-align: center;" width="40%">
            <div>
                Реестр № <?= $number ?>
            </div>
            <div style="text-align:center; padding-left: 80px; padding-top: 5px">
                <?= $generator->getBarcode($barcode, $generator::TYPE_CODE_128, 1,30) ?>
            </div>
        </td>
        <td width="60%" style="text-align: center; padding-left: 20px; font-family: Verdana;font-size: 22pt;line-height: 0.7em; border-left: 2px solid; border-bottom: 1px solid;">
            <b style="font-size: 14px"><?= $article ?> [ <?= count($workOrders) ?> шт ]</b><br>
            <hr>
            <b style="font-size: 12px"><?= $title ?> </b><br>
        </td>
    </tr>
</table>
<table width="500" cellpadding="9" cellspacing="0" style="border: 2px solid; border-bottom: 1px dashed;border-collapse: collapse;">
    <tr>
        <td style="text-align: center; height: 150px" width="40%">

        </td>
    </tr>
</table>
<table width="500" cellpadding="9" cellspacing="0" style="border: 2px solid; border-bottom: 1px dashed;border-collapse: collapse;">
    <?php
        /** Количество столбцов в распечатке реестра. Для одеял - 3, для остальных - 2 **/
        $chunkLength = !empty($workOrders) & preg_match('/OT-(2061)/', $workOrders[0]->article) ? 3 : 2;
        $width = $chunkLength == 3 ? '33%' : '50%';
        $padding = $chunkLength == 3 ? '30' : '60';
    ?>
    <?php $chunks = array_chunk($workOrders,$chunkLength); ?>
    <?php foreach ($chunks as $chunk): ?>
    <tr>
        <td style="padding: 10px 20px 10px <?=$padding?>px;width: <?= $width?>;border: 1px solid black" >
            <?php if (isset($chunk[0])): ?>
                <?= $chunk[0]->name ?>
                <?= $generator->getBarcode($chunk[0]->upn->barcode, $generator::TYPE_CODE_128, 1,20) ?>
            <?php endif; ?>
        </td>
        <td style="padding: 10px 20px 10px <?=$padding?>px;width: <?= $width?>;border: 1px solid black">
            <?php if (isset($chunk[1])): ?>
                <?= $chunk[1]->name ?>
                <?= $generator->getBarcode($chunk[1]->upn->barcode, $generator::TYPE_CODE_128, 1,20) ?>
            <?php endif; ?>
        </td>
        <?php if ($chunkLength == 3) : ?>
        <td style="padding: 10px 20px 10px <?=$padding?>px;width: <?= $width?>;border: 1px solid black">
            <?php if (isset($chunk[2])): ?>
                <?= $chunk[2]->name ?>
                <?= $generator->getBarcode($chunk[2]->upn->barcode, $generator::TYPE_CODE_128, 1,20) ?>
            <?php endif; ?>
        </td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>

</table>
