<?php

use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;
use backend\widgets\viewbuilder\ManualAnalogs;

?>
<p class="page-break-<?= $model->id ?>"></p>


<? if (@$model->json('items')[0]['car']): ?>
    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td colspan="2" style="text-align: center">
                <b style="font-size: 17px">ИНСТРУКЦИЯ ПО УСТАНОВКЕ И ЭКСПЛУАТАЦИИ<br>
                    АВТОМОБИЛЬНОГО СОЛНЦЕЗАЩИТНОГО КОЗЫРЬКА LAITOVO НА МАГНИТНЫХ ДЕРЖАТЕЛЯХ</b><br>
                <a style="padding-left: 270px"><?= @$model->car->name?></a>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px">
                <?= ManualAnalogs::widget([
                    'car_id'=> @$model->car->id, 
                    'window_type' => $model->window
                ]) ?>
            </td>
        </tr>
        <tr>
            <td>
                <b style="font-size: 13px">НАЗНАЧЕНИЕ</b><br>
            </td>
            <td rowspan="4" style="text-align:center">
                <img width="230 px" src="/img/manual/laitovo-lable.png"><br>
                <img width="230 px" src="/img/manual/vizor.jpg">
                
            </td>

        </tr>
        <tr>
            <td>
                <a style="font-size: 13px">Солнцезащитные козырьки для передних автомобильных окон предназначены для защиты водителя или 
                    пассажира автомобиля от ярких слепящих солнечных лучей.</a><br>
            </td>
        </tr>
       
        <tr>
            <td width="60%">
                <b style="font-size: 13px">ОСОБЕННОСТИ ЭКСПЛУАТАЦИИ</b>
                <ul style="font-size: 13px">
                    <li>
                        запрещается стряхивать пепел от сигарет с установленными защитными экранами, это может привести к повреждению тканевого покрытия.
                    </li>
                    <br>
                    <li>
                        запрещается оказывать физическое воздействие (ущемление, прокалывание и т. д.) тканевого покрытия, это может привести к его повреждению.
                    </li>
                    <br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                 <b style="font-size: 13px">УХОД</b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                 <a style="font-size: 13px">При загрязнении поверхности экрана рекомендуется промыть под струей воды
                     (возможно промывание струей высокого давления), просушить. При незначительном загрязнении тканевого
                     покрытия рекомендуется протереть влажной салфеткой или губкой, возможно применение щадящих моющих
                     средств или средств по уходу за тканевыми поверхностями.</a>
            </td>
        </tr>
    </table>

    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td >
                <b style="font-size: 13px">КОМПЛЕКТАЦИЯ</b>
            </td>
            <td style="text-align: right; text-align: center ">
               <!-- <b style="font-size: 11px; ">ОСОБЕННОСТИ УСТАНОВКИ</b>-->
            </td>
        </tr>
        <tr>
            <td style="font-size: 13px">
                <ul>
                    <li>
                        Автомобильный солнцезащитный козырек, шт..............2

                    </li>
                    <li>
                        Магнитные держатели, компл.  ....................................1
                    </li>
                    <li>
                        Инструкция по эксплуатации, экз. ...............................1
                    </li>

                </ul>
          
        </tr>
         <tr>
            <td >
                 <b style="font-size: 13px">ХРАНЕНИЕ</b>
            </td>
        </tr>
        <tr>
            <td>
                 <a style="font-size: 13px">Солнцезащитные козырьки LAITOVO вне эксплуатации не требуют особых условий хранения и могут храниться в
                     любом удобном месте. Не следует хранить вблизи открытого пламени, острых, режущих предметов и в помещении с повышенной влажностью.  </a>
            </td>
        </tr>
        
         <tr>
            <td >
                 <b style="font-size: 13px">УСТАНОВКА</b>
            </td>
        </tr>
         <tr >
            <td colspan="3" style=" text-align: left; font-size: 15px;border: 2px solid;
             border-radius: 15px;padding-left: 5px;
            background-color: rgba(112, 109, 112, 0.46) ">
                <b style="">Внимание</b><br>
                <a style="font-size: 13px">
                    Установка производится при температуре выше +10 °С!
                    Перед установкой обязательно очистите поверхность оконного проема в местах установки креплений от
                    пыли или других загрязнений,
                    обезжирьте и просушите!
                    Для надежной фиксации, сильно прижмите крепление к обшивке на 5-10 секунд.
                </a>
            </td>
         </tr>
    </table>


    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td>
                 <img width="150 px" src="/img/manual/vizor-in-magnet-1.png">
            </td>
            <td width="60%" valign="top" style="text-align:left">
                <a>Откройте переднюю дверь автомобиля. </a>
            </td>
        </tr>
        <tr>
            <td>
                 <img width="150 px" src="/img/manual/vizor-in-magnet-2.jpg">
            </td>
            <td width="60%" valign="top" style="text-align:left">
                <a>Не снимая защитную пленку с клейкой ленты, разместите крепления на козырьке в соответствии со схемой установки.   </a>
            </td>
        </tr>
        <tr>
            <td>
                 <img width="150 px" src="/img/manual/vizor-in-magnet-3.png">
            </td>
            <td width="60%" valign="top" style="text-align:left">
                <a>Установите козырек с креплениями в оконный проем. Определите места установки креплений по обшивке.  </a>
            </td>
        </tr>
        <tr>
            <td>
                 <img width="300 px" src="/img/manual/vizor-in-magnet-4.png">
            </td>
            <td width="60%" valign="top" style="text-align:left">
                <a>Поочередно установите крепления на вертикальной стойке и верхней части оконного проема.  </a>
            </td>
        </tr>
        <tr>
            <td>
                 <img width="150 px" src="/img/manual/vizor-in-magnet-5.png">
            </td>
            <td width="60%" valign="top" style="text-align:left">
                <a>Установите козырек на магнитные держатели.  </a>
            </td>
        </tr>
         <tr>
             <td    colspan="2" >
                <b style="font-size: 13px">ГАРАНТИЙНЫЕ ОБЯЗАТЕЛЬСТВА</b><br>
                 <a style="font-size: 13px">При надлежащем соблюдении покупателем правил эксплуатации, продавец устанавливает срок гарантии на изделие – 1 год с даты получения покупателем товара.</a>
            </td>
        </tr>
       
    </table>
   
    <table width="820" cellspacing="5" cellpadding="5" style="font-family: Arial">
        <tr>
            <td width="30%">
                <b style="font-size: 14px">КОНТАКТЫ:</b><br>
                <a style="font-size: 14px">+7 499 703 06-85<br>
                    E-mail: info@laitovo.ru<br>
                www.laitovo.ru</a>
            </td>
           
            <td style="text-align: center">
                <img width="150 px" src="/img/manual/laitovo-lable.png">
            </td>
            <td style="text-align: center">
                <img width="50 px" src="/img/manual/round.jpg"><br>
                <a style="font-size: 13px"> ОТК </a>
            </td>
            <td width="30%" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: center; ">
                    
                </li>
                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    _________________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                    <?=date('d-m-Y'); ?>
                </li>
            </td>
        </tr>
    </table>
<? endif; ?>

