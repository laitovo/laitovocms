<?php
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;
use backend\widgets\viewbuilder\ManualAnalogs;


$scheme = new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

?>
<p class="page-break-<?= $model->id ?>"></p>


<? if (@$model->json('items')[0]['car']): ?>
    <table width="820">
        <tr>
            <td colspan="2">
                <b style=" text-align:center;font-size: 19px">Инструкция по установке и эксплуатации москитных сеток Laitovo для автомобильных окон</b><br>
                <a style="padding-left: 270px"><?= @$model->car->name?></a>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px">
                <?= ManualAnalogs::widget([
                    'car_id'=> @$model->car->id, 
                    'window_type' => $model->window
                ]) ?>
            </td>
        </tr>
        <tr>
            <td>
                <b style="font-size: 11px">НАЗНАЧЕНИЕ И ЭКСПЛУАТАЦИЯ</b><br>
            </td>
            <td rowspan="5">
                <? if (@$model->json('items')[0]['car']): ?>
                    <img src="<?= $scheme->file() ?>" width="360px">
                <? endif ?>
            </td>

        </tr>
        <tr>
            <td>
                <a style="font-size: 11px">Москитные сетки для автомобильных окон предназначены для защиты салона
                    транспортного средства, в том числе находящихся в нем пассажиров от
                    насекомых, осколков стекла и защиты частной жизни в транспортном средстве.</a><br>
            </td>
        </tr>
        <tr>
            <td>
                <b style="font-size: 11px">УХОД ЗА ЗАЩИТНЫМИ ЭКРАНАМИ</b><br>
            </td>
        </tr>
        <tr>
            <td>
                <a style="font-size: 11px">При загрязнении тканевого покрытия рекомендуется протереть влажной салфеткой или губкой.<br>
                        При сильных загрязнениях возможно применение щадящих моющих средств или средств по уходу за тканевыми поверхностями.</a>
            </td>

        </tr>
        <tr>
            <td width="60%">
                <b style="font-size: 11px">ОСОБЕННОСТИ ЭКСПЛУАТАЦИИ</b>
                <ul style="font-size: 11px">
                    <li>
                        не рекомендуется эксплуатация передних боковых москитных сеток во время
                        движения автомобиля.
                    </li>
                    <br>
                    <li>
                        не рекомендуется стряхивать пепел от сигарет, выбрасывать мусор в окно с
                        установленными москитными сетками. Это может привести к повреждению
                        тканевого покрытия.
                    </li>
                    <br>
                    <li>
                        не рекомендуется оказывать физическое воздействие (ущемление,
                        прокалывание и т. д.) тканевого покрытия москитной сетки, это может привести к
                        её повреждению.
                    </li>
                  
            </td>
        </tr>
        <tr>
            <td>
                <ul style="font-size: 11px">
                    <li>
                        не рекомендуется использовать москитную сетку зимой (в холодную погоду), т.к.
                        ткань может провисать (особенность материала). Этот дефект исчезает как
                        только вы прогреете автомобиль.
                    </li>
                </ul>
            </td>

            <td style=" text-align: center">
                <a style="font-size: 13px;">Схема расстановки зажимов - держателей</a>
            </td>
        </tr>

    </table>

    <table width="820">
        <tr>
            <td colspan="2">
                <b style="font-size: 11px">КОМПЛЕКТАЦИЯ</b>
            </td>
            <td style="text-align: right; text-align: center ">
               <!-- <b style="font-size: 11px; ">ОСОБЕННОСТИ УСТАНОВКИ</b>-->
            </td>
        </tr>
        <tr>
            <td style="font-size: 11px">
                <ul>
                    <li>
                        Защитный экран, <?= $model->getCountWindow()?> шт
                    </li>
                    <li>
                        Инструкция, 1 шт
                    </li>


                </ul>
            </td>
            <td style="font-size: 11px">
                <ul>
                    <? foreach ($model->getClips() as $key => $value) : ?>
                        <? if ($key == 'M') :?>
                            <li>
                                Комплект магнитных держателей, 10 шт.
                            </li>
                        <?else :?>
                            <li>
                                Зажим № <?= $key ?>, <?= $value ?> шт
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>

                </ul>
            </td>
            <td valign="top" style="text-align: center">
               <!-- <a style="font-size: 11px">Особенности установки зажимов держателей для автомобиля из 1с</a>-->
            </td>
        </tr>

    </table>


    <table width="820">
        <tr>
            <td colspan="2">
                <b style="font-size: 11px">УСТАНОВКА ЗАЖИМОВ-ДЕРЖАТЕЛЕЙ</b><br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <a style="font-size: 11px">Настоятельно рекомендуется перед установкой зажимов-держателей и москитных сеток убедиться в том, что
                    Вы приобрели комплект соответствующий Вашей марке автомобиля, предварительно примерьте москитную сетку к окнам автомобиля. На 
                    бирке указан тип крепления зажимов держателей. Существует 3 основных типа крепления.
                </a><br>
            </td>
        </tr>
        <tr>
            <td rowspan="2">
                <img width="70 px" src="/img/manual/01.png">
            </td>
            <td style="font-size: 11px" valign="top">
                <a><b>1.</b> При установке откройте дверь и приведите стекло двери в нижнее положение см. Рис .[1]; <b>2.</b>
                    Отогните резиновый уплотнитель, расположенный в раме
                    окна двери в 1-м (м1) месте установки зажима-держателя [1], так чтоб оголился буртик оконной рамы;
                    <b>3.</b> За буртик зоной зажима закрепите зажим-
                    держатель [2]; <b>4.</b> Резиновый уплотнитель верните в первоначальное положение рис. [3];<b>5.</b>
                    Повторите п. 2-4 во всех остальных местах установки
                    зажимов - держателей, согласно схеме установки
                    зажимов-держателей.</a>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <img width="380 px" src="/img/manual/1.png">
            </td>
        </tr>
        <tr>
            <td rowspan="2">
                <img width="70 px" src="/img/manual/02.png">
            </td>
            <td style="font-size: 11px" valign="top">
                <a><b>1.</b> При установке откройте дверь и приведите стекло транспортного средства в нижнее положение
                    [4]; <b>2.</b> Выньте резиновый уплотнитель, расположенный
                    в раме окна транспортного средства в 1-м (м1) месте
                    установки зажима-держателя [4]; <b>3.</b> Установите зажим-держатель зоной зажима в рамку оконной
                    рамы [5]; <b>4.</b> Прижмите установленный зажим -
                    держатель резиновым уплотнителем [6]; <b>5.</b> Повторите п. 2-4 во всех остальных местах установки
                    зажимов - держателей, согласно схеме установки
                    зажимов-держателей.</a>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <img width="380 px" src="/img/manual/2.png">
            </td>
        </tr>
        <tr>
            <td rowspan="2">
                <img width="70 px" src="/img/manual/03.png">
            </td>
            <td style="font-size: 11px" valign="top">
                <a><b>1.</b> При установке откройте дверь и приведите стекло транспортного средства в нижнее положение
                    [7]; <b>2.</b> В месте установки со стороны салона
                    зацепите специальным крючком (входит в комплект) за край пластиковой обшивки; <b>3.</b> В отогнутый
                    край пластиковой обшивки заведите зажим -
                    держатель [7]; <b>4.</b> Подведите загиб зажима - держателя к краю пластиковой обшивки и защелкните
                    зажим - держатель [8], [9]; <b>5.</b> Возьмите крючок,
                    отогните край пластиковой обшивки, зацепите вторым креплением зажим - держатель, выньте крючок. <b>6.</b>
                    Повторите п. 2-4 во всех остальных местах
                    установки зажима-держателя, согласно схеме установки зажимов-держателей.</a>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <img width="380 px" src="/img/manual/3.png">
            </td>
        </tr>
    </table>
    <table width="820">
        <tr>
            <td>
                <b style="font-size: 11px">УСТАНОВКА МОСКИТНОЙ СЕТКИ В ЗАЖИМЫ ДЕРЖАТЕЛИ</b>
                <ol type="1" style="font-size: 11px">
                    <li>
                        Поднимите стекло.
                    </li>
                    <li>
                        Вставьте каркас москитной сетки в зажимы-держатели. Делать это лучше поочерёдно: сначала
                        боковое ребро экрана, затем верхнее. Нижнее ребро москитной сетки должно лежать на обшивке
                        без фиксации в зажимы-держатели.
                    </li>
                </ol>
            </td>
            <td rowspan="2">
                <img width="180 px" src="/img/manual/4.png">
            </td>
        </tr>
        <tr>
            <td>
                <b style="font-size: 11px">ДЕМОНТАЖ МОСКИТНОЙ СЕТКИ</b><br>
                <a style="font-size: 11px">Для москитных сеток, устанавливаемых в зажимы-держатели:</a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <a style="font-size: 12px">1. Ухватиться за петлю расположенную в верхней части сетки. 2. Слегка оттянуть край сетки в сторону центра,
                    так, чтобы можно было его вынуть из зажима-держателя. 3. Вынуть москитную сетку из зоны держателя зажима-держателя. 4. Вынуть москитную 
                    сетку из остальных зажимов-держателей.</a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <a style="font-size: 12px">Для москитных сеток, устанавливаемых в распорку:<br>
                   1. Ухватиться за петлю, расположенную в верхней части сетки 2. Слегка оттянуть край сетки в сторону центра. 3. Вынуть верхнюю часть москитной
                    сетки. 4. Вынуть москитную сетку полностью.</a>
            </td>
        </tr>
    </table>

    <table width="820">
        <tr>
            <td width="30%">
                <b style="font-size: 12px">КОНТАКТЫ:</b><br>
                <a style="font-size: 12px">+7 499 703 06-85<br>
                    E-mail: info@laitovo.ru<br>
                www.laitovo.ru</a>
            </td>
            <td style="text-align: right">
                <img width="50 px" src="/img/manual/round.jpg"><br>
                <a style="padding-right: 11px; font-size: 13px"> ОТК </a>
            </td>
            <td width="30%" style="padding-right: 20px; text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: center; ">
                   
                </li>
                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    _________________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                 <?=date('d-m-Y'); ?> 
                </li>
            </td>
        </tr>
    </table>
<? endif; ?>


