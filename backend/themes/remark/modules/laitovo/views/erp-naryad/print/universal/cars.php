<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 9:37 AM
 */
use Picqer\Barcode\BarcodeGeneratorHTML as BarcodeGenerator;
use backend\helpers\ArticleBuilder;

/**
 * @var $article string
 */

$generator = new BarcodeGenerator();

$list = ArticleBuilder::buildList($article);

?>
<?php foreach ($list as $row):?>
<div>
    <div style="width: 20%;float: left">
        <div style="padding-left:5%">
            <?= $generator->getBarcode($row->barcode, $generator::TYPE_CODE_128, 1)?>
            <div style="text-align: left;font-size: 12px"><?=$row->article?></div>
        </div>
    </div>
    <div style="width: 80%;float: left">
        <span style="font-size: 24px;font-weight: bold"><?=$row->carTitle?></span>
    </div>
    <div style="display: table; clear:both"></div>
</div>
<?php endforeach;?>


