<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 9:18 AM
 */

use backend\helpers\ArticleHelper;

/**
 * @var $article string
 */

$enType = ArticleHelper::getTypeTitleEn($article);
$ruType = ArticleHelper::getTypeTitleRu($article);
$enWindow = ArticleHelper::getWindowTitleEn($article);
$ruWindow = ArticleHelper::getWindowTitleRu($article);
?>
<div style="font-size:9.9px;text-align: center">
    INSTRUCTION FOR INSTALLATION AND SERVICING OF THE SET OF PROTECTIVE SCREENS FOR CAR WINDOWS LAITOVO <b>·</b>
    ANLEITUNG FÜR MONTAGE UND BETRIEB VOM SONNENSHUTZ-SET FÜR AUTOFENSTER LAITOVO <b>·</b>
    ИНСТРУКЦИЯ ПО УСТАНОВКЕ И ЭКСПЛУАТАЦИИ ЗАЩИТНЫХ ЭКРАНОВ ДЛЯ АВТОМОБИЛЬНЫХ ОКОН LAITOVO
</div>
<div style="font-size:18px;text-align: center">
     <?= $enType?> / <?= $enWindow?>  <b>·</b> <?= $ruType?> / <?= $ruWindow?>
</div>
