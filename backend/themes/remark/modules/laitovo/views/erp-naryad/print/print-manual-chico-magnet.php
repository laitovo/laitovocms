<?php
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;
use backend\widgets\viewbuilder\ManualAnalogs;


$scheme = new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

?>
    <p class="page-break-<?= $model->id ?>"></p>


<? if (@$model->json('items')[0]['car']): ?>


    <table width="820">
        <tr>
            <td colspan="2" style=" text-align: center">
                <li style="list-style-type: none">
                    <b style="font-size: 13px;">ИНСТРУКЦИЯ ПО УСТАНОВКЕ И ЭКСПЛУАТАЦИИ </b>
                </li>
                <li style="list-style-type: none">
                    <b style="font-size: 13px;" <b>КОМПЛЕКТА ЗАЩИТНЫХ ЭКРАНОВ ДЛЯ АВТОМОБИЛЬНЫХ ОКОН “CHIKO MAGNET” </b><br>
                    <a style="padding-left: 270px"><?= @$model->car->name?></a>
                </li>
            </td>
            <tr>
                <td colspan="2" style="font-size: 11px">
                    <?= ManualAnalogs::widget([
                        'car_id'=> @$model->car->id, 
                        'window_type' => $model->window
                    ]) ?>
                </td>
            </tr>
            <td width="" style="padding-left: 15px">
                <img width="170" src="/img/manual-chico-magnet/10.jpg">
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left">
                <a style="font-size: 13px;">Защитные экраны для автомобильных окон предназначены для защиты салона
                    транспортного средства, в том числе находящихся в нем пассажиров от солнца, насекомых, осколков
                    стекла и защиты частной жизни в транспортном средстве.
                </a>
            </td>
            <td width="%" style="padding-left: 15px">
                <img width="170" src="/img/manual-chico-magnet/2.png">
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: left; ">
                    <b style="">Особенности эксплуатации</b>
                </li>
                <li style="font-size: 13px; padding-left: 20px ">
                    запрещается стряхивать пепел от сигарет, выбрасывать мусор в окно с установленными защитными
                    экранами, это может привести к повреждению тканевого покрытия.
                </li>
                <li style="font-size: 13px; padding-left: 20px ">
                    запрещается оказывать физическое воздействие (ущемление, прокалывание и т. д.) тканевого покрытия,
                    это может привести к его повреждению.
                </li>
                <li style="font-size: 13px; padding-left: 20px ">
                    не рекомендуется эксплуатация передних боковых защитных экранов во время движения автомобиля.
                </li>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Уход</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a style="font-size: 13px">При загрязнении тканевого покрытия рекомендуется протереть влажной салфеткой
                    или губкой. При сильных загрязнениях возможно применение щадящих моющих средств или средств по уходу
                    за тканевыми поверхностями.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Комплектность</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: left; ">
                    <a style="font-size: 13px">В комплект поставки входит:</a>
                </li>
                <li style=" list-style-type: none;font-size: 13px;">
                    Защитные экраны, шт .......................................................2
                </li>
                <li style="list-style-type: none;font-size: 13px; ">
                    Инструкция по эксплуатации, экз .................................1
                </li>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Хранение</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a style="font-size: 13px">
                    Комплект защитных экранов CHIKO вне эксплуатации не требует особых условий хранения и осуществляется
                    в любом удобном месте.
                    Рекомендуется хранить в специальной фирменной сумке. Не следует хранить вблизи открытого пламени,
                    острых, режущих предметов и
                    в помещении с повышенной влажностью.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a style="">Защитные экраны <b>Chiko Magnet</b> состоит из облегченного стального каркаса, обшитого крупноячеистой
                    тканью. По всему периметру экрана вшиты неодимовые магниты, позволяющие фиксировать его в оконном проеме
                    без применения дополнительных крепежных средств.</a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a >
                    Защитные экраны <b>Chiko Magnet</b> используются только на автомобилях с металлической обшивкой оконного проема двери.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px;">
                <b style="">Установка</b>
            </td>
        </tr>
    </table>

    <table width="820">
        <tr>
            <td style=" ">
                <img width="250px" src="/img/manual-chico-magnet/12.jpg">
            </td>
            <td style=" text-align: left; padding-left: 30px ">
                <a style="font-size: 18px">
                    <b>1</b>. Откройте дверь.
                </a>
            </td>

        </tr>
        <tr>
            <td width="15%" style=" ">
                <img width="250px" src="/img/manual-chico-magnet/7.jpg">
            </td>
            <td  style=" text-align: left; padding-left: 30px">
                <a style="font-size: 18px">
                    <b>2</b>. Установите экран в оконный проем, прикрепив его к металлической
                    рамке двери. Убедитесь, что защитный экран  надежно зафиксирован в оконном проеме.
                </a>
            </td>
        </tr>
        <tr>
        </tr>
            <td colspan="2" style=" text-align: left; font-size: 15px;">
                <b style="">Гарантийные обязательства</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px">
                <a style="font-size: 13px">
                    При надлежащем соблюдении покупателем правил эксплуатации, продавец устанавливает срок гарантии на
                    изделие
                    – 1 год с даты получения покупателем товара.
                </a>
            </td>
        </tr>
    </table>

    <table width="820" style="padding-top: 130px">
        <tr>
            <td width="20%" style=" text-align: left; font-size: 14px;">
                <b style="">Контакты:
                    +7 499 703 06-85
                    E-mail: info@laitovo.ru<br>
                www.laitovo.ru </b>
            </td>
            <td width="60%" style="text-align: center ">
                <img width="60px" src="/img/manual-chico-magnet/8.jpg">
                <li style=" list-style-type: none;font-size: 11px; text-align: center">
                    <b>www.laitovo.ru</b>
                </li>

            </td>
            <td width="20%" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: center; ">
                    <a style="font-size: 13px"> ОТК </a>
                </li>
                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    _________________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                    <?=date('d-m-Y'); ?>
                </li>
            </td>
        </tr>
    </table>


<? endif ?>