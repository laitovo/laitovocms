<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/17/18
 * Time: 10:49 AM
 */
?>
<hr>
<div style="padding: 5px 20px">
    <div style="text-align: center">
        <div style="display: inline-block;color: white;background-color: black;font-weight: bold;padding: 1px">DE</div>
    </div>
    <div>
        <div style="width: 100%">
            <section style="font-size: 12px">
                <div style="width: 50%;float: left">
                    <h4>PACKUNGSINHALT</h4>
                    <!--Комплектация-->
                    <div >
                        <div style="width: 50%;float: left">
                            <ul>
                                <li>
                                    Schutzschirme ............ <?= $equipment['countWindows']??5 ?>
                                </li>
                                <li>
                                    Anleitung  ..................... 1
                                </li>
                                <?if ($needHook):?>
                                <li>
                                    Installationshaken ....... 1
                                </li>
                                <?endif;?>
                            </ul>
                        </div>
                        <div style="width: 50%;float: left">
                            <ul>
                                <? if (isset($equipment['clips']) && count( ($clips = $equipment['clips']) )): foreach ($clips as $key => $value) : ?>
                                    <? if ($key == 'M') : ?>
                                        <li>
                                            Magnethalter ... 10
                                        </li>
                                    <? else : ?>
                                        <li>
                                            Halteklammer № <?= $key ?>..... <?= $value ?>
                                        </li>
                                    <? endif; ?>
                                <? endforeach; ?>
                                <? endif; ?>
                            </ul>
                        </div>
                        <div style="display: table; clear:both"></div>
                    </div>
                </div>
                <div style="width: 50%;float: left">
                    <h4>ZWECKBESTIMMUNG UND BETRIEB</h4>
                    <p>
                        E Schutzschirme für Autofenster dienen dem Schutz des Kfz-Innenraums, einschließlich der darin
                        befindlichen Fahrgäste, vor Sonne, Insekten, Glassplittern; auch Privatleben im Auto wird geschützt
                    </p>
                </div>
                <div style="display: table; clear:both"></div>
            </section>
            <section style="font-size: 12px">

                <h4>BETRIEBSBESONDERHEITEN</h4>
                <p>
                <ul style="padding: 0px 20px">
                    <li>
                        Beim Autofahren ist der Betrieb der Seitenschutzschirme vorn nicht empfehlenswert.
                    </li>
                    <li>
                        Es ist nicht empfehlenswert, Zigarettenasche abzuschütteln, Müll zum Autofenster herauszuwerfen,
                        an dem Schutzschirme befestigt sind, sofern sie keine Spezialausschnitte haben. Denn dadurch
                        kann das Gewebe beschädigt werden.
                    </li>
                    <li>
                        Winters (bei kaltem Wetter) kann es passieren, dass im unbeheizten Innenraum das
                        Schutzschirmmaterial durchhängt (Materialbesonderheit); dieser Defekt verschwindet
                        von selbst, sobald das Auto beheizt wird.
                    </li>
                    <li>
                        Es ist nicht empfehlenswert, das Schutzschirmgewebe einer physischen Einwirkung
                        (der Einklemmung, dem Durchstechen) zu unterziehen. Denn dadurch kann es beschädigt werden.
                    </li>
                </ul>
                </p>
                <h4>PFLEGE VON SCHUTZSCHIRMEN</h4>
                <p>
                    Wenn die Gewebeoberﬂäche verschmutzt ist, empﬁehlt es sich, sie mit einem feuchten Tuch
                    oder Schwamm zu reinigen. Bei starker Verschmutzung können schonende Reinigungsmittel
                    oder Mittel zur Pﬂege von Gewebeoberﬂächen verwendet werden
                </p>
                <h4>INSTALLATION</h4>
                <p>
                    Stellen Sie vor der Installation sicher, dass Sie ein Kit erworben haben, das Ihrer
                    Automarke und der in der Anleitung angegebenen Fensteröﬀnung entspricht, und
                    versuchen Sie zuerst, einen Schutzschirm gegen das Fenster des Autos anzubringen.
                    Überprüfen Sie die Konformität der in der Anleitung angegebenen Ausrüstung mit der
                    tatsächlichen Verfügbarkeit.
                </p>
                <ul style="list-style-type: decimal;padding: 0px 20px">
                    <?php if ($bwSklad):?>
                        <li>
                            Wenn Sie einen Klappschutzschirm gekauft haben, müssen Sie ihn vor der
                            Installation zusammenbauen. Fassen Sie an der Stelle der Biegung des
                            Schutzschirms das Verbindungsrohr und stecken Sie es in die gegenüberliegende
                            Kante des Rahmens ein, wie in Abb.<?=isset($pictures['bwSklad'])?$pictures['bwSklad']->num:'';?> gezeigt ist.
                        </li>
                    <?php endif;?>
                    <?php if (isset($newClips['V'])):?>
                        <li>
                            Nehmen Sie den Schutzschirm an der Stelle, wo das Klettband ist, und kleben
                            Sie es an die Fensteröffnung an die angegebene Stelle (Abb. 1) "V"-förmig, wie in Abb.
                            <?=isset($pictures['V'])?$pictures['V']->num:'';?> gezeigt ist.
                        </li>
                        <?php $velkro = true;unset($newClips['V']);?>
                    <?php endif;?>
                    <?php if (in_array($window,['FD','RD'])):?>
                        <li>
                            Installieren Sie den Schutzschirm in die Fensteröﬀnung, die in der
                            Anleitung angegeben ist.
                        </li>
                    <?endif;?>
                    <?if ($needHook):?>
                    <li>
                        Wenn im Lieferumfang Ihres Bausatzes ein Installationshaken enthalten ist, verwenden
                        Sie ihn zum Biegen der Fensterverkleidung
                    </li>
                    <?endif;?>
                    <?if (empty($newClips) || isset($velkro) || in_array($window,['BW']) ):?>
                        <li>
                            Installieren Sie den Schutzschirm in der Fensteröffnung, die in der Anleitung angegeben ist.<?= (!isset($velkro)) && !empty($pictures) && empty($newClips) ? ' Abb.' . @$pictures[0]->num : '' ?>
                        </li>
                    <?endif;?>
                    <?php foreach ($newClips as $key => $value) {?>
                        <li>
                            Nehmen Sie den Beutel mit den Halteklammern Nr. <?=$key;?> . Stellen, die im
                            Diagramm (Abb. 1) unter Nr. <?=$key;?> angegeben sind, wie in Abb. <?=isset($pictures[$value])?$pictures[$value]->num:'';?> zur zusätzlichen
                            ﬁxierung des bildschirms
                        </li>
                    <?php } ?>
                    <?php if (in_array($window,['FD','RD'])):?>
                        <li>
                            Machen Sie das Fenster zu
                        </li>
                    <?endif;?>
                    <?php if (!isset($velkro) && !empty($newClips) && !in_array($window,['BW'])):?>
                    <li>
                        Setzen Sie den Schirmrahmen in die Halterklammern ein. Es ist besser, dies
                        nacheinander zu tun: zuerst die Seitenkante des Bildschirms, dann die Oberseite.
                        Die untere Kante des Schutzschirms sollte auf dem Gehäuse liegen, ohne in den
                        Cliphaltern zu befestigen.
                    </li>
                    <?php endif;?>
                </ul>
                <div>
                    <div style="float:left;width: 70%">
                        <h4>ENTFERNEN DES SCHUTZSCHIRMS.</h4>
                        <ul style="list-style-type: decimal;padding: 0px 20px">
                            <li>
                                Fassen Sie die Schleife oben auf dem Schutzschirm.
                            </li>
                            <li>
                                Ziehen Sie den Schutzschirmrand leicht zur Mitte.
                            </li>
                            <li>
                                Nehmen Sie den oberen Teil vom Schutzschirm raus.
                            </li>
                            <li>
                                Nehmen Sie den Schutzschirm komplett raus.
                            </li>
                        </ul>
                        <h4>CONTACTS</h4>
                        <p style="float:left">
                            Bei Fragen melden Sie sich bitte unter:<br>
                            Tel.: +49 (0)40-87407233 (DE)<br>
                            <span style="text-decoration: underline">E-mail: info@laitovo.de</span>
                        </p>
                    </div>
                    <p style="float:right">
                        <img style="width: 200px" src="/img/instructions/qren.png" alt="">
                    </p>
                    <div style="display: table; clear:both"></div>
                </div>
            </section>
        </div>
        <div style="width: 50%"></div>
    </div>
</div>
