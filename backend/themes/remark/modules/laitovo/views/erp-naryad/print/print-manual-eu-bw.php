<?php
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;
use backend\widgets\viewbuilder\ManualAnalogs;


$scheme = new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

?>
<p class="page-break-<?= $model->id ?>"></p>


<? if (@$model->json('items')[0]['car']): ?>
    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial" >
        <tr>
            <td colspan="4" style="background-color: rgba(71, 63, 63, 0.36); text-align: center">
                <b style="font-size:12px">INSTRUCTION FOR INSTALLATION AND SERVICING OF THE SET OF PROTECTIVE SCREENS FOR CAR
                  WINDOWS  /<br> ANLEITUNG FÜR MONTAGE UND BETRIEB SET SONNENSCHUTZ FÜR AUTOFENSTER<br>
                    <a style="text-align: center"><?= @$model->car->fullEnName?></a>
                 
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px">
                <?= ManualAnalogs::widget([
                    'car_id'=> @$model->car->id, 
                    'window_type' => $model->window
                ]) ?>
            </td>
        </tr>
        <tr>
            <td  colspan="2">
                <b style="font-size: 9px">FUNCTION AND SERVICING/ZWECKBESTIMMUNG UND BETRIEB</b><br>
            </td>
            <td rowspan="2"  colspan="2" style=" padding-left: 40px; padding-top: 10px ">
                <b style="font-size: 11px">BEFESTIGUNGPLAN FÜR HALTEKLAMMERN /<br>
                    SCHEME OF INSTALLATION OF CLIP-HOLDERS</b><br>
            </td>

        </tr>
        <tr>
            <td colspan="2">
                <a style="background-color: #0a0a0a; color: white; font-size: 11px">EN</a>
                <a style="font-size: 10px">Protective screens for car windows are designed for car saloon protection,
                    also protection of the
                    passengers inside from the sun, insects, glass fragments and protection of a private life in a
                    vehicle.
                    <br>
                    <a style="background-color: #0a0a0a; color: white; font-size: 11px">DE</a>
                    <a style="font-size: 10px">
                        Schutzschirme für Autofenster dienen dem Schutz des Kfz-Innenraums, einschließlich der darin
                        befindlichen
                        Fahrgäste, vor Sonne, Insekten, Glassplittern; auch Privatleben im Auto wird geschützt.
                    </a>
            </td>


        </tr>
        <tr>
            <td colspan="2" style="padding-top: 5px">
                <b style="font-size: 9px">WARNINGS /BETRIEBSBESONDERHEITEN</b><br>
            </td>
            <td rowspan="3" valign="bottom">
                <? if (@$model->json('items')[0]['car']): ?>
                    <img src="<?= $scheme->file() ?>" width="320px">
                <? endif ?>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px;">EN</a>
            </td>
            <td>
                <ul style="font-size: 10px; padding-left: 10px ">
                    <li style="list-style-type: none; ">
                        <a style="font-size: 10px">It is not recommended to use the front side protective screens
                            on-the-ride.</a>
                    </li>
                    <li>
                        It is not recommended to flick cigarette ash, dump the trash out of the windows with protective
                        screens
                        nstalled, if they are not equipped with special cutaways. It may cause textile cover damage.
                    </li>
                    <li>
                        In winter (during cold weather), in a not warmed-up saloon, the material of protective screens
                        may hang
                        down (textile peculiarity). This defect vanishes after you warmup the vehicle.
                    </li>
                    <li>
                        It is not recommended to make a physical impact on textile cover of protective screens (jamming,
                        transfixing etc.) as it may cause its damage.
                    </li>
                </ul>
            </td>

        </tr>
        <tr>
            <td valign="top">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px;">DE</a>
            </td>
            <td>
                <ul style="font-size: 10px; padding-left: 10px ">
                    <li>
                        <a style="font-size: 10px">Beim Autofahren ist der Betrieb der Seitenschutzschirme vorn nicht
                            empfehlenswert.</a>
                    </li>
                    <li>
                        Es ist nicht empfehlenswert, Zigarettenasche abzuschütteln, Müll zum Autofenster herauszuwerfen,
                        an dem
                        Schutzschirme befestigt sind, sofern sie keine Spezialausschnitte haben. Denn dadurch kann das
                        Gewebe
                        beschädigt werden.
                    </li>
                    <li>
                        Winters (bei kaltem Wetter) kann es passieren, dass im unbeheizten Innenraum das
                        Schutzschirmmaterial
                        durchhängt (Materialbesonderheit); dieser Defekt verschwindet von selbst, sobald das Auto
                        beheizt wird.
                    </li>
                    <li>
                        Es ist nicht empfehlenswert, das Schutzschirmgewebe einer physischen Einwirkung (der
                        Einklemmung, dem
                        Durchstechen) zu unterziehen. Denn dadurch kann es beschädigt werden.
                    </li>
                </ul>

            </td>

        </tr>

    </table>

    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td colspan="4">
                <b style="font-size: 9px">PACKAGE CONTENTS/PACKUNGSINHALT</b>
            </td>

        </tr>
        <tr>
            <td valign="top" style="text-align: right; ">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
            </td>
            <td style="font-size: 10px">
                <ul>
                    <li>
                        Protective screens .... <?= $model->getCountWindow() ?>
                    </li>
                    <li>
                        Instruction................. 1
                    </li>


                </ul>
            </td>
            <td style="font-size: 10px">
                <ul>
                    <? foreach ($model->getClips() as $key => $value) : ?>
                        
                            <li>
                                Clip-holder № <?= $key ?>..... <?= $value ?>
                            </li>
                        
                    <? endforeach; ?>

                </ul>
            </td>
            <td valign="top" style="text-align: right">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
            </td>
            <td style="font-size: 10px">
                <ul>
                    <li>
                        Schutzschirme .... <?= $model->getCountWindow() ?>
                    </li>
                    <li>
                        Anleitung............. 1
                    </li>


                </ul>
            </td>
            <td style="font-size: 10px">
                <ul>
                    <? foreach ($model->getClips() as $key => $value) : ?>
                        <? if ($key == 'M') : ?>
                            <li>
                                Set of magnetic holders 10
                            </li>
                        <? else : ?>
                            <li>
                                Klammerhalter № <?= $key ?>..... <?= $value ?>
                            </li>
                        <? endif; ?>
                    <? endforeach; ?>

                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="50%">
                <b style="font-size: 9px">CLIP-HOLDERS INSTALLATION/ANBRINGUNG VON KLAMMERHALTERN</b><br>
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
                <a style="font-size: 10px">Protective screens for car windows are designed for car saloon protection,
                    also protection of the
                    passengers inside from the sun, insects, glass fragments and protection of a private life in a
                    vehicle.
            </td>
            <td colspan="3" width="50%">

                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
                <a style="font-size: 10px">Es wird dringend empfohlen, vor Anbringung der Klammerhalter und der
                    Schutzschirme sich
                    davon zu überzeugen, dass Sie komplette Schutzschirme erworben haben, die Ihrer Kfz-Marke
                    entsprechen. Den Schutzschirm sollte man vorher den Autofenstern anprobieren. Auf dem
                    Etikett ist der Befestigungstyp der Klammerhalter angegeben. Es gibt drei
                    Hauptbefestigungstype.
            </td>
        </tr>

    </table>

    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td rowspan="2">
                <img width="72 px" src="/img/manual/01n.png">
            </td>
            <td style="font-size: 10px; padding-left: 15px">

                        <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
                    <a>
                        1. While installing open the door and put down the glass in the lower position [1];<br>
                        2. Unbend a rubber weatherstrip located in the door window frame in the 1st<br>
                        position of a clip-holder [1] to bare a window-frame collar*;<br>
                        3. Fasten the clip-holder to the clip-zone [2];<br>
                        4. Return a rubber weatherstrip into the original position [3];<br>
                        5. Repeat steps 2-4 in the rest of positions* of clip-holders installing, according to the
                        clip-holders installation diagram.
                    </a>

            </td>
            <td style="font-size: 10px; padding-left: 15px">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
                <a>
                    1.Beim Einbau machen Sie die Tür auf und versetzen Sie das Türfenster in die untere Lage
                    s.Abb.(1);<br>
                    2.Biegen Sie den Gummiverdichter im Türfensterrahmen am ersten Anbringungsort (M1) * des
                    Klammerhalters (1) so, dass der Fensterrahmenbördel freigelegt wird;<br>
                    3.Hinter dem Bördelklammerbereich befestigen Sie den Klammerhalter (Abb.2);<br>
                    4.Den Gummiverdichter versetzen Sie in die ursprüngliche Lage (Abb. 3);<br>
                    5. Gehen Sie Punkte 2 bis 4 an allen anderen Anbringungsstellen* der Klammerhalter entsprechend
                    dem Befestigungsplan durch.<br>
                </a>
            </td>
        </tr>

        <tr>
            <td colspan="2" style="text-align: center">
                <img width="360 px" src="/img/manual/1eu.png">
            </td>
        </tr>

         <td rowspan="2">
                <img  width="72 px" src="/img/manual/02n.png">
            </td>
            <td style="font-size: 10px; padding-left: 15px">

                <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
                <a>
                    <b>1.</b> Open the door and put down the glass of a vehicle in the lower position [4]; DE<br>
                    <b>2.</b> Take out a rubber weatherstrip located in the vehicle window frame in the 1st
                    position* of the clip holder installation [4];<br>
                    <b>3.</b> Hook the clip-holder to the window-frame collar [5];<br>
                    4. Install a rubber weatherstrip into the original position [6];<br>
                    5. Repeat steps 2-4 in the rest of positions* of clip-holders installing, according to the
                    clip-holders installation diagram.
                </a>

            </td>
            <td style="font-size: 10px; padding-left: 15px">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
                <a>
                    1.Machen Sie die Tür auf und versetzen Sie das Türfenster in die untere Lage s.
                    Abb. (4);<br>
                    2.Nehmen Sie den Gummiverdichter im Türfensterrahmen am ersten
                    Anbringungsort (M1) * des Klammerhalters (Abb.4) heraus;<br>
                    3.Haken Sie den Klammerhalter mit dem Klemmbereich am Fensterrahmenbördel ein (Abb.5);<br>
                    4.Den Gummiverdichter versetzen Sie in die ursprüngliche Lage (Abb. 6);<br>
                    5.Gehen Sie Punkte 2 bis 4 an allen anderen Anbringungsstellen* des Klammerhalters
                    entsprechend dem Befestigungsplan durch.
                </a>
            </td>


        </tr>

        <tr>
            <td colspan="2" style="text-align: center">
                <img width="360px" src="/img/manual/2eu.png">
            </td>
        </tr>


        <tr>
            <td rowspan="2">
                <img width="72 px" src="/img/manual/03n.png">
            </td>
            <td style="font-size: 10px; padding-left: 15px">

                <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
                <a>
                    1.While installing, open the door and put down the glass of a vehicle into a lower DE
                    position [7];<br>
                    2.In the installation position* from the saloon side hook the edge of
                    plastic cover and click the clip-holder [8],[9];<br>
                    3.Put a clip-holder into the bent edge of plastic cover[7]<br>
                    4.Put the bend of a clip-holder to the edge of plastic cover and click the clip-holder [8],[9];<br>
                    5.Take the hook, bend the
                    edge of plastic cover, hook the clip-holder by the second bracing, take out the hook.<br>
                    6.Repeat steps 2-4 in the rest of positions* of clip-holders installing, according to the
                    clip-holders installation diagram.
                </a>

            </td>
            <td style="font-size: 10px; padding-left: 15px">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
                <a>
                    1.Beim Einbau machen Sie die Tür auf und versetzen Sie das Türfenster in die
                    untere Lage s. Abb. (7);<br>
                    2.Am Anbringungsort * (von der Seite des Kfz-Innenraums her) haken Sie mit einem Spezialhaken
                    (gehört zum Set) den Plastikbeschlagrand ein;<br>
                    3.An den abgebogenen Plastikbeschlagrand führen Sie den Klammerhalter ein (Abb.7);<br>
                    4.Führen Sie die Klammerhalterbiegung an den Plastikbeschlagrand heran und schnappen Sie den
                    Klammerhalter ein (Abb. 8,9);<br>
                    5.Nehmen Sie den Haken, biegen Sie den Plastikbeschlagrand ab, haken Sie mit der zweiten
                    Befestigung den Klammerhalter ein und nehmen Sie den Haken heraus.<br>
                    6.Gehen Sie Punkte 2 bis 4 an allen anderen Anbringungsstellen* des Klammerhalters
                    entsprechend dem Befestigungsplan durch
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <img width="360 px" src="/img/manual/3eu.jpg">
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: left">
                <b style="font-size: 9px">PROTECTIVE SCREEN INSTALLATION INTO CLIP-HOLDERS/SCHUTZCHIRMANBRINGUNG AN
                    KLAMMERHALTERN</b>
            </td>
        </tr>
    </table>

    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td style="font-size: 10px;">

                <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
                <a>
                    1.Uplift the glass.<br>
                    2.Insert the frame of the protective screen into the clip-holders.
                    It is better to be done one at the time: first the side edge of the
                    screen, then the upper edge. The lower edge of the protective
                    screen must be located on the cover.
                </a>

            </td>
            <td style="font-size: 10px; padding-left: 15px" valign="top">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
                <a>
                    1.Die Glasscheibe wird hochgehoben.<br>
                    2.Setzen Sie der Schutzschirmrahmen in die Klammerhalter
                    ein. Dies sollte man am besten der Reihe nach ausführen.
                    Zuerst die untere Schutzschirmseitenkante, danach die
                    obere Schutzschirmkante. Die untere Schutzschirmkante
                    soll auf dem Beschlag.
                </a>
            </td>
            <td rowspan="2">
                <img width="180 px" src="/img/manual/4.png">
            </td>
        </tr>
    </table>
    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td colspan="2">
                <b style="font-size: 9px">PROTECTIVE SCREEN DISMANTLING/SCHUTZSCHIRMDEMONTAGE</b><br>
            </td>
        </tr>
        <tr>
            <td>
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
                <a style="font-size: 10px"> <b>1.</b> Grasp the loop in the upper part of a screen.<b>2.</b> Slightly
                    pull the edge of the screen
                    toward the center so that it can be taken out of the clip-holder. <b>3.</b> Take off the protective
                    screen out of the holder-zone of a clip-holder. <b>4.</b> Take off the protective screen out the
                    rest
                    of clip-holders.</a>
            </td>
            <td>
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
                <a style="font-size: 10px"> <b>1.</b> Schleife im oberen Schutzschirmteil ergreifen. <b>2.</b> Den
                    Schutzschirmrand ein klein wenig
                    in Richtung Mitte so ziehen, dass man ihn aus dem Klammerhalter herausziehen kann. <b>3.</b> Den
                    Schutzschirm aus dem Klammerhalterbereich herausziehen. <b>4.</b> Den Schutzschirm aus anderen
                    Klammerhaltern herausziehen.</a>
            </td>
        </tr>

    </table>

    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td>
                <b style="font-size: 10px">CONTACTS/KONTAKT</b>
            </td>
        </tr>
        <tr>
            <td width="20%">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
                <a style="font-size: 10px">Should you have any questions
                    feel free to contact us under:<br>
                    Tel: +44 20 3695 1976 (UK)<br>
                    Email: info@laitovo.eu</a>
            </td>
            <td width="20%">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
                <a style="font-size: 10px">Bei Fragen stehen wir Ihnen gerne
                    über Email oder telefonisch unter:<br>
                    Tel: +49 (0)40-95063310 (DE)<br>
                    Email: info@laitovo.de</a>
            </td>

            <td width="20%" style="text-align: center">
                
            </td>
            <td style="text-align: center">
                <img width="50 px" src="/img/manual/round.jpg"><br>
                <a style="font-size: 9px; ">QALITY CONTROL</a>
            </td>
            <td style=" text-align: left;">
                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    ____________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                    <?=date('d-m-Y'); ?>
                </li>
            </td>
        </tr>
    </table>
<? endif; ?>