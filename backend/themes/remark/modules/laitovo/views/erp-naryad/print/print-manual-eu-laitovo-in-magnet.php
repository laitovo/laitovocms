<?php
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;
use backend\widgets\viewbuilder\ManualAnalogs;


$scheme = new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

?>
<p class="page-break-<?= $model->id ?>"></p>


<? if (@$model->json('items')[0]['car']): ?>
    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial" >
        <tr>
            <td colspan="4" style="background-color: rgba(71, 63, 63, 0.36); text-align: center">
                <b style="font-size:11px">INSTRUCTION FOR INSTALLATION AND SERVICING OF THE SET OF PROTECTIVE
                   SCREENS FOR CAR WINDOWS LAITOVO ON MAGNET HOLDERS/<br />
                    ANLEITUNG FÜR MONTAGE UND BETRIEB SET SONNENSCHUTZ FÜR AUTOFENSTER LAITOVO MIT MAGNETHALTERN<br>
                    <a style="text-align: center"><?= @$model->car->fullEnName?></a>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px">
                <?= ManualAnalogs::widget([
                    'car_id'=> @$model->car->id, 
                    'window_type' => $model->window
                ]) ?>
            </td>
        </tr>
        <tr>
            <td  colspan="2">
                <b style="font-size: 10px">FUNCTION AND SERVICING/ZWECKBESTIMMUNG UND BETRIEB</b><br>
            </td>
            <td rowspan="2"  colspan="2" style="text-align:center; padding-left: 20px; padding-top: 10px ">
                <b style="font-size: 10px">SCHEME OF INSTALLATION OF MAGNETIC HOLDERS/<br>
                    BEFESTIGUNGPLAN FÜR MAGNETHALTERN</b><br>
            </td>

        </tr>
        <tr>
            <td colspan="2">
                <a style="background-color: #0a0a0a; color: white; font-size: 11px">EN</a>
                <a style="font-size: 11px">Protective screens for car windows are designed for car saloon protection,
                    also protection of the
                    passengers inside from the sun, insects, glass fragments and protection of a private life in a
                    vehicle.
                    <br>
                    <a style="background-color: #0a0a0a; color: white; font-size: 11px">DE</a>
                    <a style="font-size: 11px">
                        Schutzschirme für Autofenster dienen dem Schutz des Kfz-Innenraums, einschließlich der darin
                        befindlichen
                        Fahrgäste, vor Sonne, Insekten, Glassplittern; auch Privatleben im Auto wird geschützt.
                    </a>
            </td>


        </tr>
        <tr>
            <td colspan="2" style="padding-top: 5px">
                <b style="font-size: 10px">WARNINGS /BETRIEBSBESONDERHEITEN</b><br>
            </td>
            <td rowspan="3" valign="center">
              <img width="260 px" src="/img/manual/chiko-in-magnet-big.jpg">
            </td>
        </tr>
        <tr>
            <td valign="top">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px;">EN</a>
            </td>
            <td>
                <ul style="font-size: 11px; padding-left: 10px ">
                    <li style="list-style-type: none; ">
                        <a style="font-size: 11px">It is not recommended to use the front side protective screens on-the-ride.</a>
                    </li>
                    <li>
                      It is not recommended to flick cigarette ash, dump the trash out of the windows with
                       protective screensinstalled, if they are not equipped with special cutaways. It may
                        cause textile cover damage.
                    </li>
                    <li>
                        In winter (during cold weather), in a not warmed-up saloon, the material of protective screens
                        may hang
                        down (textile peculiarity). This defect vanishes after you warmup the vehicle.
                    </li>
                    <li>
                        It is not recommended to make a physical impact on textile cover of protective screens (jamming,
                        transfixing etc.) as it may cause its damage.
                    </li>
                </ul>
            </td>

        </tr>
        <tr>
            <td valign="top">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px;">DE</a>
            </td>
            <td>
                <ul style="font-size: 11px; padding-left: 10px ">
                    <li>
                        <a style="font-size: 11px">Beim Autofahren ist der Betrieb der Seitenschutzschirme vorn nicht
                            empfehlenswert.</a>
                    </li>
                    <li>
                        Es ist nicht empfehlenswert, Zigarettenasche abzuschütteln, Müll zum Autofenster herauszuwerfen,
                        an dem
                        Schutzschirme befestigt sind, sofern sie keine Spezialausschnitte haben. Denn dadurch kann das
                        Gewebe
                        beschädigt werden.
                    </li>
                    <li>
                        Winters (bei kaltem Wetter) kann es passieren, dass im unbeheizten Innenraum das
                        Schutzschirmmaterial
                        durchhängt (Materialbesonderheit); dieser Defekt verschwindet von selbst, sobald das Auto
                        beheizt wird.
                    </li>
                    <li>
                        Es ist nicht empfehlenswert, das Schutzschirmgewebe einer physischen Einwirkung (der
                        Einklemmung, dem
                        Durchstechen) zu unterziehen. Denn dadurch kann es beschädigt werden.
                    </li>
                </ul>
            </td>
        </tr>
    </table>

    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
      <tr>
        <td colspan="4">
            <b style="font-size: 10px">LAITOVO PROTECTIVE SCREENS MAINTENANCE/PFLEGE DER SCHUTZSCHIRME LAITOVO</b>
        </td>
      </tr>
      <tr>
        <td colspan="2" width="50%">
          <a style="background-color: #0a0a0a; color: white; font-size: 11px">EN</a>
          <a style="font-size: 11px">In case of impurity of protective screens surface (material 2) it is
             recommended to wash it in the stream of water (high pressure stream possible), then dry it. In
             case of impurity of protective screen textile surface (other materials) it is recommended to wipe
              it with a wet tissue or sponge. In case of substantial impurity the usage of mild detergents or soaps.
          </a>
        </td>
        <td colspan="2" width="50%">
          <a style="background-color: #0a0a0a; color: white; font-size: 11px">DE</a>
          <a style="font-size: 11px">Wird die Schutzschirmoberfläche (Material Nr. 2) verschmutzt, kann single
             mit einem Wasserstrahl (eventuell unter Hochdruck) gewaschen werden. Wird die aus anderen
              Materialien produzierte Schutzschirmoberfläche verschmutzt, kann sie mit einer feuchten
               Serviette oder einem Schwamm sauber gemacht werden. Bei starken Verschmutzungen
               ist die Anwendung milder Waschmittel oder Gewebepflegemittel möglich.
          </a>
        </td>
      </tr>

        <tr>
            <td colspan="4">
                <b style="font-size: 10px">PACKAGE CONTENTS/PACKUNGSINHALT</b>
            </td>
        </tr>
        <tr>
            <td valign="top"  style="text-align: right; ">
                <a style="background-color: #0a0a0a; color: white; font-size: 11px">EN</a>
            </td>
            <td valign="left" style="font-size: 11px">
                <ul>
                    <li>
                        Protective screens - <?= $model->getCountWindow() ?>
                    </li>
                    <li>
                        Instruction - 1
                    </li>
                    <li>
                        Set of magnet holders - 1
                    </li>


                </ul>
            </td>

            <td valign="top" style="text-align: right">
                <a style="background-color: #0a0a0a; color: white; font-size: 11px">DE</a>
            </td>
            <td valign="left" style="font-size: 11px">
                <ul>
                    <li>
                        Schutzschirme - <?= $model->getCountWindow() ?>
                    </li>
                    <li>
                        Anweisung - 1
                    </li>
                    <li>
                        Satz von Magnethaltern - 1
                    </li>


                </ul>
            </td>
        </tr>

        <tr>
          <td colspan="4" style="border: 2px solid;
           border-radius: 15px; padding-left: 8px;
          background-color: rgba(112, 109, 112, 0.46)">
              <b style="font-size: 10px">ATTENTION/ACHTUNG</b><br />
            <a style="background-color: #0a0a0a; color: white; font-size: 11px">EN</a>
            <a style="font-size: 11px">Installation is carried out at a temperature above + 10 ° C! Before
               installation, is required to clean the surface of the window frame in fasteners mounting places
                from dust or other contaminants, degrease and dry! For a secure fit, press fastening strong to
                trim for 5 - 10 seconds!
            </a><br />
            <a style="background-color: #0a0a0a; color: white; font-size: 11px">DE</a>
            <a style="font-size: 11px">Installation erfolgt bei einer Temperatur von über +10 ° C ! Vor der
               Installation reinigen Sie die Montageplätze vor Staub oder anderen Verschmutzungen, entfetten
                und trocken! Für einen sicheren Halt, drücken Sie die Befestigung stark zum Verkleidung für 5-10 Sekunden!
            </a>
          </td>
        </tr>
    </table>

    <table width="820" cellspacing="0" cellpadding="0"  style="font-family: Arial; font-size:15px">
        <tr>
            <td >
                <img width="160px" src="/img/manual/chiko-in-magnet-1.png">
            </td>
            <td colspan = "2" valign="top" style=" padding-left: 15px">

                        <a style="background-color: #0a0a0a; color: white; font-size:12px ">EN</a>
                    <a>
                      Open the door and put down the glass.
                    </a>

            </td>
            <td  valign="top"width = "30%" style=" padding-left: 15px">
                <a style="background-color: #0a0a0a; color: white;font-size:12px ">DE</a>
                <a>
                  Öffnen Sie die Tür und versetzen Sie das Fensterin die untere Lage.
                </a>
            </td>
        </tr>

        <tr>
            <td >
                <img width="160px" src="/img/manual/chiko-in-magnet.jpg">
            </td>
            <td valign="top" colspan = "2" style=" padding-left: 15px">
                    <a>
                      Do not remove the protective film from the sticky tape, place holders on the
                       screen according to the scheme of installation.
                    </a>

            </td>
            <td valign="top" width = "30%" style=" padding-left: 15px">
                <a>
                  Ohne die Schutzfolie vom Klebeband zuentfernen, platzieren Sie die Halter auf dem
                  Schirm nach dem Schema der Installation.
                </a>
            </td>
        </tr>

        <tr>
            <td >
                <img width="160px" src="/img/manual/chiko-in-magnet-3.png">
            </td>
            <td valign="top" colspan = "2" style=" padding-left: 15px">
                    <a>
                      Set the screen with holders into the window frame. Define holders mounting
                      locations on the casing.
                    </a>

            </td>
            <td valign="top" width = "30%" style=" padding-left: 15px">
                <a>
                  Stellen Sie den Schirm mit den Haltern in die Fensteröffnung. BestimmenSiedie
                  Befestigungsstellen der Halter an der Türverkleidung.
                </a>
            </td>
        </tr>

        <tr>
            <td colspan = "2" >
                <img width="290px" src="/img/manual/chiko-in-magnet-4.jpg">
            </td>
            <td  valign="top" style=" padding-left: 15px">
                    <a>
                      Set the screen with holders into the window frame. Define holders mounting
                      locations on the casing.
                    </a>

            </td>
            <td valign="top" width = "30%" style=" padding-left: 15px">
                <a>
                  Stellen Sie den Schirm mit den Haltern in die Fensteröffnung. Bestimmen Sie die
                  Befestigungsstellen der Halter an der Türverkleidung.
                </a>
            </td>
        </tr>

        <tr>
            <td >
                <img width="160px" src="/img/manual/chiko-in-magnet-5.jpg">
            </td>
            <td  valign="top" colspan = "2" style=" padding-left: 15px">
                    <a>
                      Install the screen on a magnetic holders.
                    </a>

            </td>
            <td valign="top" width = "30%" style=" padding-left: 15px">
                <a>
                  Installieren Sie den Schirm an die Magnethaltern.
                </a>
            </td>
       </tr>

  </table>

    <table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial">
        <tr>
            <td>
                <b style="font-size: 10px">CONTACTS/KONTAKT</b>
            </td>
        </tr>
        <tr>
            <td width="20%">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">EN</a>
                <a style="font-size: 10px">Should you have any questions
                    feel free to contact us under:<br>
                    Tel: +44 20 3695 1976 (UK)<br>
                    Email: info@laitovo.eu</a>
            </td>
            <td width="20%">
                <a style="background-color: #0a0a0a; color: white; font-size: 10px">DE</a>
                <a style="font-size: 10px">Bei Fragen stehen wir Ihnen gerne
                    über Email oder telefonisch unter:<br>
                    Tel: +49 (0)40-95063310 (DE)<br>
                    Email: info@laitovo.de</a>
            </td>
            <td style="text-align: center">
                <img width="60 px" src="/img/manual/8eu.jpg"><br>
            </td>

            <td style="text-align: center">
                <img width="170 px" src="/img/manual/laitovo-lable.png"><br>
                <a>www.laitovo.eu</a>
            </td>
            <td style="text-align: center">
                <img width="50 px" src="/img/manual/round.jpg"><br>
                <a style="font-size: 9px; ">QALITY CONTROL</a>
            </td>
            <td style=" text-align: left;">
                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    ____________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                    <?=date('d-m-Y'); ?>
                </li>
            </td>
        </tr>
    </table>
<? endif; ?>
