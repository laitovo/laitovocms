<?php
use backend\widgets\viewbuilder\ManualAnalogs;


?>
    <p class="page-break-<?= $model->id ?>"></p>


    <table width="820" cellspacing="2" cellpadding="2">
        <tr>
            <td width="75%"  style=" text-align: left;font-size: 14px">
               
                <b>ИНСТРУКЦИЯ ПО УСТАНОВКЕ И ЭКСПЛУАТАЦИИ КОМПЛЕКТА ЗАЩИТНЫХ<br>
                    ЭКРАНОВ ДЛЯ АВТОМОБИЛЬНЫХ ОКОН CHIKO MAGNET  </b><br>
                <a style="padding-left: 270px"><?= @$model->car->name?></a>
               
            </td>
             <td width="25%" style="text-align: right" >
                <img width="150" src="/img/manual-chico-magnet/10.jpg">
             </td>
           
        </tr>
        
        <tr>
            <td colspan="2" style="font-size: 11px">
                <?= ManualAnalogs::widget([
                    'car_id'=> @$model->car->id, 
                    'window_type' => $model->window
                ]) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left">
                <a style="font-size: 13px;">Защитные экраны для автомобильных окон предназначены для защиты салона транспортного средства,
                    в том числе находящихся в нем пассажиров от солнца, насекомых, осколков стекла и защиты частной жизни в транспортном средстве.
                </a>
            </td>
           
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 13px">
                <li style="list-style-type: none; text-align: left; ">
                    <b style="">Особенности эксплуатации</b>
                </li>
                <li style=" padding-left: 20px ">
                    запрещается стряхивать пепел от сигарет, выбрасывать мусор в окно с установленными защитными экранами, это может привести
                    к повреждению тканевого покрытия.
                </li>
                <li style=" padding-left: 20px ">
                    запрещается оказывать физическое воздействие (ущемление, прокалывание и т. д.) тканевого покрытия, это может привести к его повреждению.
                </li>
                <li style=" padding-left: 20px ">
                    не рекомендуется эксплуатация передних боковых защитных экранов во время движения автомобиля.
                </li>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px">
                <b style="">Уход</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 13px">
                <a>При загрязнении поверхности экрана рекомендуется протереть влажной салфеткой или губкой, возможно применение щадящих моющих 
                    средств или средств по уходу за тканевыми поверхностями.
                </a>
            </td>
        </tr>
        <tr>
            <td style=" text-align: left; font-size: 15px">
                <b style="">Комплектность</b>
            </td>
        </tr>
        <tr>
            <td colspan="2"  style=" text-align: left; font-size: 13px">
                
                    <a>В комплект поставки входит:</a><br>
               
                    Защитные экраны, шт ...................................................2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;      Металлические держатели, комплектов …………… 1<br> 
               
                    Инструкция по эксплуатации, экз ..............................1
               
                
            </td>
           
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px">
                <b style="">Хранение</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 13px">
                <a>
                   Комплект защитных экранов Chiko Magnet вне эксплуатации не требует особых условий хранения и осуществляется в любом удобном месте.
                   Рекомендуется хранить в специальной фирменной сумке. Не следует хранить вблизи открытого пламени, острых, режущих предметов и в 
                   помещении с повышенной влажностью.
                </a>
            </td>
        </tr>
         <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px;">
                <b style="">Установка</b>
            </td>
        </tr>
         <tr>
            <td colspan="2" style=" text-align: left; font-size: 12px;">
                <a style="">Если оконные проемы вашего автомобиля полностью металлические и не имеют обшивки, то вам подойдет способ установки
                    защитного экрана без креплений, в противном случае – используйте дополнительные крепления для установки защитных экранов
                    Chiko Magnet.</a>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px;border: 2px solid;
             border-radius: 15px;padding-left: 5px;
            background-color:yellow ">
                    <b style="">ВНИМАНИЕ!              
                   Дополнительные металлические крепления не предназначены для установки на резиновые
                        или прорезиненные элементы обшивки!
                   </b>
            </td>
        </tr>
       
    </table>

    <table width="820">
        <tr>
            <td  width="15%" style=" ">
                <img width="170px" src="/img/manual/laitovo-magnet/laitovo-magnet-1.png">
            </td>
            <td colspan="4"  valign="top" style=" text-align: left; padding-left: 30px">
                <a style="font-size: 12px">
                    Установите экран в оконный проем, прикрепив его к металлической рамке двери. Убедитесь,
                        что защитный экран надежно зафиксирован в оконном проеме.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="5" >
                 <a style="font-size: 12px">
                     Если оконный проем двери вашего автомобиля имеет неметаллическую обшивку, используйте металлические держатели
                     (идут в комплекте поставки) для крепления защитного экрана.
                </a>
            </td>
        </tr>
         <tr>
            <td  width="15%" >
                <img width="170px" src="/img/manual/laitovo-magnet/laitovo-magnet-2.png">
            </td>
            <td  valign="top" style=" text-align: left; ">
                <a style="font-size: 12px">
                     Откройте дверь и опустите стекло.
                </a>
            </td>
            <td>
                <img width="170px" src="/img/manual/laitovo-magnet/laitovo-magnet-3.png">
            </td>
            <td colspan="2" valign="top"  style=" text-align: left; position:relative; left:-60px">
                <a style="font-size: 12px">
                     Найдите места расположения вшитых магнитов на защитном экране. Не снимая защитную пленку с клейкой ленты,
                     равномерно распределите дополнительные металлические крепления на экране, зафиксировав их вшитыми в экран магнитами.
                </a>
            </td>
          
        </tr>
         <tr>
            <td >
                <img width="170px" src="/img/manual/laitovo-magnet/laitovo-magnet-4.png">
            </td>
            <td  valign="top" style=" text-align: left; ">
                <a style="font-size: 12px">
                    Приложите защитный экран к оконному проему, чтобы определить места установки креплений на обшивке двери 
                    (на неметаллических частях обшивки оконного проема)
                </a>
            </td>
            <td colspan="2"  width="15%" style=" ">
                <img width="250px" src="/img/manual/laitovo-magnet/laitovo-magnet-5.png">
            </td>
            <td  valign="top" style=" text-align: left;">
                <a style="font-size: 12px">
                     Поочередно установите крепления, зафиксировав их на обшивке клейкой лентой.
                </a>
            </td>
        </tr>
         <tr>
            <td  width="15%" style=" ">
                <img width="170px" src="/img/manual/laitovo-magnet/laitovo-magnet-6.png">
            </td>
            <td colspan="4"  valign="top" style=" text-align: left; ">
                <a style="font-size: 12px">
                     Установите экран в оконный проем, убедитесь что магниты надежно фиксируют его на металлических держателях. 
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px;">
                <b style="">Гарантийные обязательства</b>
            </td>
        </tr>
        <tr>
            <td colspan="5" style=" text-align: left; font-size: 13px">
                <a>
                    При надлежащем соблюдении покупателем правил эксплуатации, продавец устанавливает срок гарантии на изделие
                    – 1 год с даты получения покупателем товара.
                </a>
            </td>
        </tr>
    </table>

    <table width="820" >
        <tr>
            <td width="20%" style=" text-align: left; font-size: 14px;">
                <b style="">Контакты:
                    +7 499 703 06-85
                    E-mail: info@laitovo.ru<br>
                www.laitovo.ru </b>
            </td>
            
             <td width="" style="text-align: center">
                <img width="200" src="/img/manual-chico-magnet/10.jpg">
             </td>
            
            <td style="text-align: center">
                <img width="50 px" src="/img/manual/round.jpg"><br>
                <a style="font-size: 13px"> ОТК </a>
            </td>
            <td width="20%" style=" text-align: left; font-size: 15px">
                
                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    _________________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                    <?=date('d-m-Y'); ?>
                </li>
            </td>
        </tr>
    </table>







