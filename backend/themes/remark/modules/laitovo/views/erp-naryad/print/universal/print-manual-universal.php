<?php
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsSchemeNew;
use common\models\laitovo\Cars;
use backend\widgets\viewbuilder\ManualAnalogs;


$scheme = new CarsSchemeNew();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

?>
<!--Вставляем разделитель станиц-->
<p class="page-break-<?= $model->id ?>"></p>

<!--Вся инструкция будет ввиде таблицы-->
<table width="820" cellspacing="0" cellpadding="0" style="font-family: Arial" >
    <!--Блок 1-ый. Вводная верхня надпись для инструкции-->
    <tr>
        <td colspan="4" style="background-color:; text-align: left">
            <?= $this->render('header',['article' => $model->article]);?>
        </td>
    </tr>
    <!--Блок 2-ой. Артикулы и автомобили-->
    <tr>
        <td colspan="4" style="text-align: left">
            <hr size="4" color="black">
            <?= $this->render('cars',['article' => $model->article]);?>
            <hr size="4" color="black">
        </td>
    </tr>
    <!--Блок 3-ой. Блок схем креплений-->
    <tr>
        <td colspan="2" style="text-align: left">
            <?= $this->render('clips',['file' => $scheme->file()]);?>
        </td>
        <td colspan="2" style="text-align: left">
            <?= $this->render('magnets',['article' => $model->article]);?>
        </td>
    </tr>
    <!--Блок 4-ый. Блок изображений установки-->
    <tr>
        <td colspan="4" style="text-align: left">
            <?= $this->render('pictures',['article' => $model->article]);?>
        </td>
    </tr>
    <!--Блок 5-ый. Блок текст установки-->
    <tr>
        <td colspan="4" style="text-align: left">
            <?= $this->render('text/main',[
                    'equipment' =>
                        [
                            'countWindows' => $model->getCountWindow(),
                            'clips' => $model->getClips(),
                        ],
                    'article' => $model->article
            ]);?>
        </td>
    </tr>
</table>






