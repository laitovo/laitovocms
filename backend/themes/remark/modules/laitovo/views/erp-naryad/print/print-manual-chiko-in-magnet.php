<?php
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;
use backend\widgets\viewbuilder\ManualAnalogs;


$scheme = new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

?>
    <p class="page-break-<?= $model->id ?>"></p>


<? if (@$model->json('items')[0]['car']): ?>


    <table width="820" cellspacing="0" cellpadding="0" style = "font-family: Arial">
        <tr>
            <td colspan="3" style=" text-align: center">
                <li style="list-style-type: none">
                    <b style="font-size: 13px;">ИНСТРУКЦИЯ ПО УСТАНОВКЕ И ЭКСПЛУАТАЦИИ<br>
                      КОМПЛЕКТА ЗАЩИТНЫХ ЭКРАНОВ ДЛЯ АВТОМОБИЛЬНЫХ ОКОН CHIKO НА МАГНИТАХ</br>
                    </b>
                    <a style="text-align: center"><?= @$model->car->name?></a>
                </li>

            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px">
                <?= ManualAnalogs::widget([
                    'car_id'=> @$model->car->id, 
                    'window_type' => $model->window
                ]) ?>
            </td>
        </tr>
        <tr>
          <td valign="bottom" colspan="2" style="font-size:10px">
            <b>НАЗНАЧЕНИЕ</b>
          </td>
          <td style="font-size:10px; text-align: center">
            <b>СХЕМА УСТАНОВКИ МАГНИТНЫХ ДЕРЖАТЕЛЕЙ</b>
          </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left">
                <a style="font-size: 13px;">Защитные экраны для автомобильных окон предназначены для защиты салона
                    транспортного средства, в том числе находящихся в нем пассажиров от солнца, насекомых, осколков
                    стекла и защиты частной жизни в транспортном средстве.
                </a>
            </td>
            <td rowspan="2" width="33%" >
                <img width="250 px" src="/img/manual/chiko-in-magnet-big.jpg">
            </td>
        </tr>
        <tr>
            <td  colspan="2" style=" text-align: left; font-size: 10px">
              <b>ОСОБЕННОСТИ ЭКСПЛУАТАЦИИ</b>
              <ul style="font-size: 13px;  ">
                <li>
                    запрещается стряхивать пепел от сигарет, выбрасывать мусор в окно с установленными защитными экранами,
                     это может привести к повреждению тканевого покрытия.
                </li>
                <li>
                    запрещается оказывать физическое воздействие (ущемление, прокалывание и т. д.) тканевого покрытия,
                    это может привести к его повреждению.
                </li>
                <li>
                    не рекомендуется эксплуатация передних боковых защитных экранов во время движения автомобиля.
                </li>
            </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 10px">
                <b style="">УХОД</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left;">
                <a style="font-size: 13px">При загрязнении тканевого покрытия рекомендуется протереть влажной салфеткой или губкой.
                   При сильных загрязнениях возможно применение щадящих моющих средств или средств по уходу за тканевыми поверхностями.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 10px">
                <b style="">КОМПЛЕКТНОСТЬ</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 13px">

                <a style="font-size: 13px">В комплект поставки входит:</a>
                <ul>
                    <li>
                        Защитный экран, <?= $model->getCountWindow()?> шт
                    </li>
                    <li>
                        Инструкция, 1 шт
                    </li>
                    <li>
                      Магнитные держатели, компл., 1шт
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 10px">
                <b style="">ХРАНЕНИЕ</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 13px">
                <a style="font-size: 13px">
                    Комплект защитных экранов CHIKO рекомендуется хранить в специальной фирменной сумке.
                     Не следует хранить вблизи открытого пламени,
                     острых, режущих предметов и в помещении с повышенной влажностью.
                </a>
            </td>
        </tr>
        <tr >
            <td colspan="3" style=" text-align: left; font-size: 15px;border: 2px solid;
             border-radius: 15px;padding-left: 5px;
            background-color: rgba(112, 109, 112, 0.46) ">
                <b style="">Внимание</b><br>
                <a style="font-size: 13px">
                    Установка производится при температуре выше +10 °С!
                    Перед установкой обязательно очистите поверхность оконного проема в местах установки креплений от
                    пыли или других загрязнений,
                    обезжирьте и просушите!
                    Для надежной фиксации, сильно прижмите крепление к обшивке на 5-10 секунд.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 10px;">
                <b style="">УСТАНОВКА</b>
            </td>
        </tr>
    </table>

    <table width="820">
        <tr>
            <td style=" ">
                <img width="150px" src="/img/manual/chiko-in-magnet-1.png">
            </td>
            <td width="70%" >
              <a style="font-size: 13px">
                   Откройте дверь и опустите стекло.
              </a>
            </td>
        </tr>
        <tr>
          <td style=" ">
              <img width="150px" src="/img/manual/chiko-in-magnet.jpg">
          </td>
            <td  width="70%"  style=" text-align: left; font-size: 15px; ">
                <a style="font-size: 13px">
                   Не снимая защитную пленку с клейкой ленты, разместите крепления на экране в соответствии
                    со схемой установки.
                </a>
            </td>
        </tr>
        <tr>
            <td width="15%" style=" ">
                <img width="150px" src="/img/manual/chiko-in-magnet-3.png">
            </td>
            <td width="70%" style=" ">
              <a style="font-size: 13px">
                Определите места установки креплений по
               обшивке, установив экран с креплениями в оконный проем. 
              </a>
            </td>

        </tr>
        <tr>
            <td valign="top" style=" text-align: left; ">
                  <img width="270px" src="/img/manual/chiko-in-magnet-4.jpg">
            </td>
            <td width="70%" style=" text-align: left; ">
                <a style="font-size: 13px">
                     Поочередно установите крепления на вертикальной стойке, верхней части оконного проема и
                    у бокового зеркала, если это предусмотрено схемой установки.
                </a>
            </td>
        </tr>
        <tr>
            <td width="15%" style=" ">
                <img width="150px" src="/img/manual/chiko-in-magnet-5.jpg">
            </td>
            <td  width="70%" style=" text-align: left; font-size: 15px; ">
                <a style="font-size: 13px">
                     Установите экран на магнитные держатели.
                </a>
            </td>
        </tr>

        <tr>
            <td colspan="2" style=" text-align: left; font-size: 10px;">
                <b style="">ГАРАНТИЙНЫЕ ОБЯЗАТЕЛЬСТВА</b>
            </td>
        </tr>
        <tr>
            <td colspan="2"  style=" text-align: left; font-size: 15px">
                <a style="font-size: 13px">
                    При надлежащем соблюдении покупателем правил эксплуатации, продавец устанавливает срок гарантии на
                    изделие
                    – 1 год с даты получения покупателем товара.
                </a>
            </td>
        </tr>
    </table>

    <table width="820" style="padding-top: 10px">
        <tr>
            <td width="25%"  style=" text-align: left; font-size: 14px;">
                <b style="">Контакты:
                    +7 499 703 06-85<br>
                    E-mail: info@laitovo.ru </br>
                    www.laitovo.ru
                </b>
            </td>
            <td  style="text-align: center ">
                <img width="180 px" src="/img/manual/chiko.jpg" >
            </td>
            <td style="text-align: center">
                <img width="50 px" src="/img/manual/round.jpg"><br>
                <a style="font-size: 13px; ">ОТК</a>
            </td>
            <td width="20%" style=" text-align: left; font-size: 15px">

                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    _________________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                    <?=date('d-m-Y'); ?>
                </li>
            </td>
        </tr>
    </table>


<? endif ?>
