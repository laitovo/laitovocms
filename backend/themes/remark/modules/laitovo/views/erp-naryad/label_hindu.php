<?php
use yii\helpers\Html;
use common\models\laitovo\ErpNaryad;

$type = '';
$product = $model->searchProduct();
if ($product) {
    $type = $product->title_en ? $product->title_en : '';
}
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>

<p class="page-break-<?= $model->id ?>"></p>
<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"
       style="border: none;font-size: 0.72em;">
    <tbody>
    <tr>
        <td style="padding: 3px;" colspan="2">
            <b> Imported & Marked By :</b> HOT TRACKS</b> <br>
            <div style="text-align: center"><b>Address :</b> No. 17, Victoria Road, Bangalore - 560047</b>  </div>

            <? if (@$model->json('items')[0]['car']): ?>
            <b>Part No. :</b> <?= @$model->car->fullEnName . ' ' . mb_substr($model->article, 0, 2, 'utf-8') . ', ' .  $type?> <br>
            <b>Description :</b> Sun shades. <br>
            </b>
            <? else: ?>
            <?php echo $type?> <br>
            <? endif ?>
            <b>Date of import :</b> June 2018 </b><br>
            <b>MRP :</b> RS 19,990/-(Inclusive of all taxes) </b><br>
            <b>Supplied By :</b> <br> Individual entreprener Grigorev Dmitri Lvovich <br>
            <div style="text-align: center">156005 Kostroma Russia, Voykova street. 40/112</div>
            <b>Customer Care :</b> Ph: 080-25361442
            <hr>
            <b>Legal metrology packing licence no :</b> KAR-155/11-12
        </td>
    </tr>
    </tbody>
</table>
