<?php

/*  @var $type string */
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

switch ($type) {
    case 11:
        $color = 'хаки';
        $article = 'OT-1189-60-7';
        $code = '4260628163051';
        $size = '40х30х25';
        break;
    case 12:
        $color = 'синий';
        $article = 'OT-1189-60-9';
        $code = '4260628163068';
        $size = '40х30х25';
        break;
    case 13:
        $color = 'черный';
        $article = 'OT-1189-60-3';
        $code = '4260628163044';
        $size = '40х30х25';
        break;
    case 1:
        $color = 'хаки';
        $article = 'OT-1189-47-7';
        $code = '4260628162252';
        $size = '47х28х25';
        break;
    case 2:
        $color = 'синий';
        $article = 'OT-1189-47-9';
        $code = '4260628164447';
        $size = '47х28х25';
        break;
    default:
        $color = 'черный';
        $article = 'OT-1189-47-3';
        $code = '4260628162269';
        $size = '47х28х25';
}
?>

<style>
    .bold {
        font-size: 12px!important;
        font-weight: bold;
    }

    .container {
        max-width: 7cm;
        font-size: 11px;
    }

    .container p {
        margin: 2px 0;
    }
</style>


<div class="container">

    <p class="bold">Сумка-органайзер в багажник автомобиля складная, LaitBag <?=$color?>, <?=$size?>.</p>
    <p class="bold"> Артикул: <?=$article?></p>

    <p>Предназначен для размещения и хранения различных автомобильных
        принадлежностей в багажнике автомобиля.</p>
    <p>Срок годности не ограничен.</p>

    <p>
        Изготовитель:
        ИП Григорьев Д.Л.
        156005, Россия, Костромская область,
        Кострома, Войкова, 40, 112
        тел.:<br> +7 499 703 06-85
        e-mail: info@laitovo.ru
        www.laitovo.ru
    </p>
    <p>
        <?= $generator->getBarcode($code, $generator::TYPE_CODE_128, 1) ?>
        <span><?= $code?></span><br>
    </p>
</div>


