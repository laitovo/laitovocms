<?php

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;
use core\logic\UpnGroup;

/**
 * @var $model \backend\modules\laitovo\models\ErpNaryad
 */


$scheme = new CarsScheme();
$carsForm = null;
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = ($carsForm = new CarsForm($car->id));
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

$itemtext = isset($model->json('items')[0]['itemtext']) ? $model->json('items')[0]['itemtext'] : '';
$settext = @$model->json('items')[0]['settext'];
$tehComment = @$model->json('items')[0]['comment'];
?>
<p class="page-break-<?= $model->id ?>"></p>

<table width="500" cellpadding="9" cellspacing="0" style="border: 2px solid; border-bottom: 1px dashed;border-collapse: collapse;">
    <tr>
        <td style="text-align: center;" width="40%" rowspan="2">
            <div style="text-align:center; padding-left: 60px; padding-top: 5px">
                <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 1,120) ?>
            </div>
        </td>
        <td width="60%" style="text-align: center; padding-left: 20px; font-family: Verdana;font-size: 22pt;line-height: 0.7em; border-left: 2px solid; border-bottom: 1px solid;" colspan="2">
            <? $number = trim(str_replace('Наряд', '', $model->name)) ?>
            <?= str_replace((mb_substr($number, 5, mb_strlen($number))),
                ('<b style="font-size: 35pt">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number); ?>
        </td>
    </tr>
    <tr>
        <td width="60%" colspan="2" style="text-align: center; border-left: 2px solid; text-transform: uppercase; width: 40%; <?= $model->order && @$model->order->json('category') == 'Экспорт' ? 'background: #000;color: #fff' : '' ?>">
            <h2 style="padding: 0;margin: 0"><?= $model->order ? (@$model->order->json('category') == 'Экспорт' ? (preg_match('/Neil\sParker/',@$model->order->json('comment')) === 1 ? 'Англия' : @$model->order->json('country')) : @$model->order->json('category')) : ($model->is_new_scheme ? 'НОВЫЙ АЛГОРИТМ' : '') ?></h2>
        </td>
    </tr>
</table>

<table width="500" cellpadding="9" cellspacing="0" style="border: 2px solid; border-top: 1px dashed; solidborder-collapse: collapse;">
    <? if (@$model->json('items')[0]['car']): ?>
    <tr>
        <td style="text-align: center; border-bottom: 1px solid black" width="25%">
            <div style="text-align:center;">
                <span style="font-size:1.3em;font-weight: bold;"> Лекало:</span><br>
            </div>
        </td>
        <td width="35%" style="text-align: center; border-bottom: 1px solid black; font-family: Verdana;font-size: 22pt;line-height: 0.7em; border-left: 2px solid;" colspan="2">
            <span style="font-weight: bold"><?= @$model->json('items')[0]['lekalo'] ?></span>
        </td>
        <td style="padding: 2px; text-align: center; border: 1px solid; border-top: none; border-right: none;" rowspan="2">
            <img src="<?= ArticleHelper::isOnMagnets($model->article) ? $scheme->file() : $scheme->clearFile() ?>" width="170">
        </td>
    </tr>
    <tr>
        <td style="text-align: center; border-bottom: 1px solid black" width="25%">
            <div style="text-align:center; padding-top: 5px;">
                    <span style="font-size:1.3em;font-weight: bold">Артикул:</span><br>
            </div>
        </td>
        <td width="35%" style="text-align: center; padding-left: 20px; font-family: Verdana;font-size: 22pt;line-height: 0.7em; border-left: 2px solid; border-bottom: 1px solid;" colspan="2">
            <span style="font-weight: bold"><?= @$model->carArticle ?></span>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;" width="25%">
            <span style="font-size:1.1em;font-weight: bold"><?= @$model->windowName?></span><br>
        </td>
        <td width="35%" style="text-align: center; font-family: Verdana;font-size: 12pt;line-height: 0.7em; border-left: 2px solid;" colspan="2">
                <?= $model->window ?>
                 <?= $model->windowType ?> <?= $model->withoutLabels ? ', БЕЗ ЛЕЙБЫ' : ''?>
        </td>
        <td style="text-align: center; border-left: 1px solid black;" width="40%" >
            <div style="text-align:center; padding-left: 30px;padding-top: 5px">
                <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 1) ?>
            </div>
            <div style="text-align:center;padding-top: 5px">
                <? $number = trim(str_replace('Наряд', '', $model->name)) ?>
                <?= str_replace((mb_substr($number, 5, mb_strlen($number))),
                    ('<b style="font-size: 20pt">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number); ?>
            </div>
        </td>
    </tr>
    <? else: ?>
    <tr>
        <td width="50%" style="text-align: center; font-family: Verdana;font-size: 12pt; border-left: 2px solid;" colspan="2">
            <?= @$model->json('items')[0]['name'] ?>
        </td>
        <td style="text-align: center; border-left: 1px solid black;" width="50%" >
            <div style="text-align:center; padding-left: 30px;padding-top: 5px">
                <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 1) ?>
            </div>
            <div style="text-align:center;padding-top: 5px">
                <? $number = trim(str_replace('Наряд', '', $model->name)) ?>
                <?= str_replace((mb_substr($number, 5, mb_strlen($number))),
                    ('<b style="font-size: 20pt">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number); ?>
            </div>
        </td>
    </tr>
    <? endif; ?>
</table>

<table width="500" cellpadding="0" cellspacing="0" style="border: 1px solid;border-bottom:none">
    <tr style="height: 30px">
        <td style="text-align: center; width: 16.6%; border:1px solid ">Ткань</td>
        <td style="text-align: center; width: 16.6%; border:1px solid ">Высота для оклейки</td>
        <td style="text-align: center; width: 16.6%; border:1px solid ">Хлястик</td>
        <td style="text-align: center; width: 16.6%; border:1px solid ">Сумма за наряд</td>
    </tr>
    <tr style="height: 30px">
        <td style="text-align: center; width: 16.6%; border:1px solid "><b style="font-size: 3em;font-family:'Courier New'"><?= str_ireplace('№','',$model->tkan) ?></b></td>
        <td style="text-align: center; width: 16.6%; border:1px solid "><?= @$model->json('items')[0]['visota'] ?></td>
        <td style="text-align: center; width: 16.6%; border:1px solid "><?= @$model->json('items')[0]['hlyastik'] ?></td>
        <td style="text-align: center; width: 16.6%; border:1px solid "><?php if($model->location_id == Yii::$app->params['erp_izgib'] || $model->location_id == Yii::$app->params['erp_clipsi'] || $model->location_id == Yii::$app->params['erp_shveika']): ?>
            <?=$model->jobPrice() ?>
            <? endif; ?>
        </td>
    </tr>
</table>

<table width="500" cellpadding="0" cellspacing="0" style="border: 1px solid; border-top:none;">
    <tr>
        <td height="12 mm"
            style="text-align:left; vertical-align: text-top; border-left:2px solid; padding-left: 5px; text-align: center;">
            <?php
            $array = UpnGroup::getAllFromGroup($model->upn);
            if (!empty($array) && count($array) > 1){ ?>
            <span style="display: inline-block;border: 1px solid black;padding: 2px">
                    Комплектация: <?= @$model->json('items')[0]['car'] ? trim(@$model->json('items')[0]['settext'], ';') : '' ?>
                    <?php
                    $groups = \yii\helpers\ArrayHelper::map($array,'id','id');
                    echo '' . implode(' , ',$groups) . '</span>';
                    }
                    ?>
                <?php if(@$model->getDontLook()) : ?>
                    Швейное лекало : <?= $model->getShLekalo() ?>
                <?php endif; ?>
                <?php if ($model->location_id == Yii::$app->params['erp_izgib'] && @$model->user->trainee) : ?>
                    СТАЖЕР!!! _______________
                <?php endif; ?>
        </td>
    </tr>
</table>



<table width="500" cellpadding="0" cellspacing="0" style="border: 1px solid; border-top:none;">
    <?if ($model->location_id == Yii::$app->params['erp_shveika'] && $model->isNeedTemplate) :?>
    <tr>
        <td colspan="2" style="text-align: center;border:1px solid; padding: 5px; background-color: black; color: white">НАРЯД НА ИЗГОТОВЛЕНИЕ ШВЕЙНОГО ЛЕКАЛА!!! </td>
    </tr>
    <?endif;?>
    <?if (($semiFinishedArticle = $model->semiFinishedArticle)) :?>
    <tr>
        <td colspan="2" style="text-align: center;border:1px solid; padding: 5px; background-color: black; color: white">ВОЗМИТЕ РАМКИ СО СКЛАДА ПОЛУФАБРИКАТОВ. ЛИТЕРА - <?= $semiFinishedArticle->literal ?>; АРТИКУЛ АВТОМОБИЛЯ - <?= $semiFinishedArticle->carArticle ?> </td>
    </tr>
    <?endif;?>
    <?if (($model->location_id == Yii::$app->params['erp_otk'] || $model->start == Yii::$app->params['erp_otk']) && $model->isFromSump && !$model->rework) :?>
    <tr>
        <td colspan="2" style="text-align: center;border:1px solid; padding: 5px; background-color: black; color: white">ВЗЯТЬ НА СКЛАДЕ-ОТСТОЙНИКЕ !!! ЛИТЕРА : "<?= $model->sumpLiteral?>"</td>
    </tr>
    <?endif;?>
</table>

<table width="500" cellpadding="0" cellspacing="0" style="border: 1px solid; border-top:none">
    <tbody>
    <tr style="min-height: 10px; ">
        <td style="text-align: left;border:1px solid; padding: 5px">
            <h4 style="margin-bottom: 0px;margin-top: 0px">Комментарий производства:</h4>
        </td>
    </tr>
    </tbody>
</table>

<table width="500" cellpadding="0" cellspacing="0" style="border: 1px solid; border-top:none;  min-height: 50px;">
    <tbody>
        <tr style="min-height: 10px;">
            <td style="text-align: left;border:1px solid;border-bottom: none; padding: 5px;">
                    <?= $model->withoutLabels ? '<b style="display:inline-block;background-color: black; color:white;padding: 5px">БЕЗ ЛЕЙБЫ</b>' : ''?>

                    <?if ($model->onMagnets()): ?>
                        <span style="display: inline-block; padding: 3px; background-color: black;color:white;font-weight: bolder;letter-spacing: 0.1em">
                            <?php if ($model->articleTkan != 2) :?>
                                [! Изготовить из 3-ей проволоки (сечением 3мм) !]<br>
                            <?php endif;?>
                            Изготавливать по новой схеме крепления магнитнов!!! <br><b>Магниты крепятся к рамке</b> согласно инструкции
                            <?if ($carsForm && ($model->window == 'ПБ' && $carsForm->fd_install_direction == 'Снаружи' || $model->window == 'ЗБ' && $carsForm->rd_install_direction == 'Снаружи')):?>
                                <b>Магниты клеятся со стороны петельки</b>
                            <? endif; ?>
                        </span>
                    <? endif; ?>
                    <? if (($upn = $model->upn) && $upn->withTape) : ?>
                        <span style="display: inline-block; padding: 3px; background-color: black;color:white;font-weight: bolder;letter-spacing: 0.1em">
                            Внимание!!! <b>КЛИПСЫ</b> - не изготавливать !!! На <b>ОТК</b> - вкладывать новые крепления <b>НА СКОТЧЕ</b>
                        </span>
                    <? elseif (isset($car) && $car && (($car->json('kryuchokpb') && $model->window == 'ПБ') || ($car->json('kryuchokzb') && $model->window == 'ЗБ') || ($car->json('kryuchokzps') && $model->window == 'ЗШ')) && ArticleHelper::isStandardScreen($model->article)) :?>
                        <span style="display: inline-block; padding: 3px; background-color: black;color:white;font-weight: bolder;letter-spacing: 0.1em">
                            На <b>ОТК</b> - вложить крючок !!!
                        </span>
                    <? endif;?>
                    <? if (($upn = $model->upn) && $upn->laitovoSimple) : ?>
                        <span style="display: inline-block; padding: 3px; background-color: black;color:white;font-weight: bolder;letter-spacing: 0.1em">
                            <b>Швейка</b> - лейблы "Laitovo Simple" !!!
                            <b>ОТК</b> - лейблы "Laitovo Simple" !!!
                        </span>
                    <? endif;?>
                </span>
            </td>
        </tr>
        <tr style="min-height: 10px;">
            <td style="text-align: left;border:1px solid;border-bottom: none;border-top: none; padding: 5px;">
                <?php if ($itemtext):?>
                    <span style="display: inline-block; padding: 3px; background-color: black;color:white;font-weight: bolder;letter-spacing: 0.1em"><?=$itemtext?></span>
                <?endif;?>
            </td>
        </tr>
        <tr style="min-height: 10px;">
            <td style="text-align: left;border:1px solid; border-top: none; padding: 5px; width: 50%">
                <?php if ($tehComment):?>
                    <span style="display: inline-block; padding: 3px; background-color: black;color:white;font-weight: bolder;letter-spacing: 0.1em"><?=$tehComment?></span>
                <?endif;?>
            </td>
        </tr>
    </tbody>
</table>

<table width="500" cellpadding="0" cellspacing="0" style="border: 1px solid; min-height: 50px;">
    <tr style="min-height: 10px">
        <td style="text-align: left; vertical-align: text-top; border:1px solid; max-height: 1px;">
            <div style="padding: 5px; margin: 0;"><b>Комментарии общий:</b></div>
            <hr style="background-color: black; height: 2px;margin: 0px;">
            <div style="padding: 5px; margin: 0;">
                <span style="display: inline-block; padding: 3px; background-color: black;color:white;font-weight: bolder">
                    <?=$model->order ? @$model->order->json('innercomment') : ''?>
                </span>
            </div>
        </td>
    </tr>
</table>

<table width="500" cellpadding="9" cellspacing="0" style="border: 2px solid; border-bottom: 1px dashed;border-collapse: collapse;">
    <tr>
        <td style="text-align: left;padding-left:5px; text-align: center; border-bottom: 2px solid black">
            <b><?= $model->article ?></b><br>
        </td>
    </tr>
</table>




