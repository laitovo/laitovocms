<?php

/*  @var $type string */
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

switch ($type) {
    case 13:
        $color = 'черный';
        $article = 'KF0901';
        $code = '4610007941747';
        $size = '40х30х25';
        break;

    default:
        $color = 'черный';
        $article = 'KF0901';
        $code = '4610007941747';
        $size = '40х30х25';
}
?>

<style>
    .bold {
        font-size: 12px!important;
        font-weight: bold;
    }

    .container {
        max-width: 7cm;
        font-size: 11px;
    }

    .container p {
        margin: 2px 0;
    }
</style>


<div class="container">

    <p class="bold">Сумка органайзер автомобильный <?=$size?></p>
    <p class="bold"> Цвет: <?=$color?></p>
    <p class="bold"> Артикул: <?=$article?></p>

    <p>Предназначен для хранения вещей в автомобиле.</p>
    <p>Срок годности не ограничен.</p>

    <p>
        Изготовлено по заказу ООО «Костромское предприятие «Автофильтр»
        156007, область Костромская, <br>
        город Кострома, улица Пушкина, дом 43/102 <br>
        т.: (4942) 55-0991, 55-0821  sales@afilter.ru
    </p>
    <p>
        ИП Григорьев Д.Л. 156005, Россия, Кострома, Войкова, 40, 112
    </p>
    <p>
        <?= $generator->getBarcode($code, $generator::TYPE_CODE_128, 1) ?>
        <span><?= $code?></span><br>
    </p>
</div>


