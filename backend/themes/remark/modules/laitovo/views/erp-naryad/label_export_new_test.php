<?php
$generator = new Picqer\Barcode\BarcodeGeneratorSVG();
?>

<p class="page-break-<?= $model->id ?>"></p>
<table class="invoice_items" width="270" cellpadding="2" cellspacing="7"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td colspan="2" style="font-family: 'Roboto', sans-serif;font-size: 24pt;line-height: 0.7em">
            <? $number = $model->upn->id; ?>
            <?= str_replace((mb_substr($number, 4, mb_strlen($number))),
                ('<b style="font-size: 37pt">' . mb_substr($number, 4, mb_strlen($number)) . '</b>'), $number); ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-collapse: collapse;border-bottom: none;border: none;">
            <?if ($model->upn):?>
                <?= $generator->getBarcode($model->upn->barcode, $generator::TYPE_CODE_128, 1) ?>
            <?endif;?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;font-family: 'Roboto', sans-serif;font-size: 24pt;line-height: 0.7em">
            <b><?= $model->upn ? $model->upn->article : $model->article ?></b>
        </td>
    </tr>
    </tbody>

</table>
<!--<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"-->
<!--       style="font-size: 11px;text-align: center;">-->
<!--    <tbody>-->
<!--        <tr>-->
<!--            <td style="border: none;font-size: 11px">Tel. +49(0)4095063310</td>-->
<!--            <td style="border: none;font-size: 11px">info@laitovo.de</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td colspan="2" style="border: none;font-size: 11px;">-->
<!--                Made by request of LAITOVO Manufactory T. & S. GMBH-->
<!--                Ferdinandstrasse 25-27 D-20095 Hamburg Germany-->
<!--            </td>-->
<!--        </tr>-->
<!--    </tbody>-->
<!--</table>-->
