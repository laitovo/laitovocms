<?php

/*  @var $type string */
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

$color = 'Черный';
$article ='OT-1211-0-0';
$size = '60х53';
$code = 'OT1211000'

?>

<style>
    .bold {
        font-size: 12px!important;
        font-weight: bolder;
    }

    .container {
        max-width: 7cm;
        font-size: 11px;
    }

    .container p {
        margin: 2px 0;
    }
</style>


<div class="container">

    <p class="bold">Защитная накидка для спинки автомобильного
        сиденья Laitovo с карманами</p>
    <p class="bold"> Артикул: <?=$article?>, размер: <?=$size?>, цвет: <?=$color?></p>

    <p>Предназначена для защиты спинки сиденья от загрязнений
        ножками ребенка.</p>

    <p>Срок годности не ограничен.</p>

    <p>
        Изготовитель:
        ИП Григорьев Д.Л.
        156005, Россия, Костромская область,
        Кострома, Войкова, 40, 112
        тел.:<br> +7 499 703 06-85
        e-mail: info@laitovo.ru
        www.laitovo.ru
    </p>
    <p>
        <?= $generator->getBarcode($code, $generator::TYPE_CODE_128, 1) ?>
        <span><?= $code?></span><br>
    </p>
</div>


