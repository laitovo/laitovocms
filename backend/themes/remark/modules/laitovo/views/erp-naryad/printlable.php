<?php
use yii\helpers\Html;
use common\models\laitovo\ErpNaryad;

$generator = new Picqer\Barcode\BarcodeGeneratorPNG();

?>

<p class="page-break-<?= $model->id ?>"></p>
<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2" style="border-bottom: 1px dashed">
    <tbody>
    <tr>
        <td style="font-size: 12px;"><b>
                <? if (@$model->json('items')[0]['car']): ?>
                    <b><?= @$model->json('items')[0]['car'] ?></b>
                <? else: ?>
                    <?= @$model->json('items')[0]['name'] ?>
                <? endif ?>
            </b></td>
    </tr>
    <tr>
        <td style="font-size: 12px;">
            <b><?= $model->article ?></b>
        </td>
    </tr>
    <tr>
        <? if (@$model->json('items')[0]['car']): ?>
            <td style="font-size: 12px;height: 25px"><b>
                    <?
                    $n = $model->window;
                    mb_internal_encoding("UTF-8");
                    $artcl = mb_substr($model->article, 2);
                    echo $n . $artcl;
                    ?>
                </b></td>
        <? else: ?>
            <td>;">&nbsp;</td>
        <? endif ?>
    </tr>
    <tr>
        <td style="padding: 10px">
            <img width="100%" height="70px"
                 src="data:image/png;base64,<?= base64_encode($generator->getBarcode($model->barcode, $generator::TYPE_CODE_128)) ?>">
        </td>
    </tr>
    </tbody>
</table>
