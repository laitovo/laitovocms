<?php
use yii\helpers\Html;
use common\models\laitovo\ErpNaryad;
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

$scheme= new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car=Cars::find()->where(['article'=>$model->carArticle])->one())!=null){
	$scheme->model=new CarsForm($car->id);
}
$log = $model->json('log');
if ($model->rework && isset($log) && count($model->json('log')))
{
	foreach ($log as $row) {
		if ($row['location_from'] == ErpLocation::findOne(Yii::$app->params['erp_clipsi'])->name){
			$isClipsi = true;
			break;
		}
	}
}

$scheme->brand=$model->brand;
$scheme->type=$model->type;
$scheme->type_clips=$model->type_clips;
$scheme->window=$model->window;

?>
<?if ( in_array( $model->location_id, [ Yii::$app->params['erp_clipsi'] ] )):?>
<p class="page-break-<?=$model->id?>"></p> 
<table class="invoice_items" width="559" cellpadding="0" cellspacing="0" style="border: 1px solid; border-bottom: 1px dashed;border-collapse: collapse;">
	<caption style='padding-bottom: 5px;padding-top:5px'>
        <?php if ($model->prodGroup): ?>
            <span style="display: inline-block; background-color: black; color: white; padding: 5px;font-weight: bold;"><?= $model->prodGroup?></span>
        <?php endif;?>
		<b> <? $number = trim(str_replace('Наряд', '', $model->name)) ?>
            <?= str_replace((mb_substr($number, 5, mb_strlen($number))),
                ('<b style="font-size: x-large">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number); ?></b>

		<!-- <b><?=$model->order_id ? @$model->order->json('username') : '' ?></b> -->
		Время: <?= Yii::$app->formatter->asTime(time())?>
		<?= 'Следующий участок: <span style="text-transform:uppercase;font-weight:bold">'.((isset($next) && $next) ? ErpLocation::findOne($next)->name : 'Cклад')."</span>"?>
		<?if (isset($isClipsi)) echo "<br><span style='font-weight:bolder;font-style:italic' class='lead'>В ПЕРЕДЕЛКЕ КЛИПСЫ УЖЕ ЕСТЬ !!!! </span>"?>
		<? if ($model->location_id == Yii::$app->params['erp_okleika'] && $model->isAnotherPlaced) {
		    echo "<br><span style='font-weight:bolder;font-style:italic' class='lead'>ИЩИ НА НОВОЙ ВЕШАЛКЕ - ИЗГИБ !!!!</span>";
        }?>
    <!-- Ставим указание что предыдущую работу делал стажер -->
<!--        --><?php //if($model->lastExecutor()) : ?>
<!--        --><?php //endif; ?>
<!--        <pre>-->
<!--            --><?//= print_r($model->json('log')) ?>
<!--        </pre>-->
	</caption>
	<tbody>
		<tr style='height:18px;'>
			<td rowspan="3"  style="text-align: center;border: 1px solid;font-size: 5em;padding: 5px;">
				<span> <?= $model->productionLiteral?></span>
			</td>
			<td  colspan="2" style="text-align: center; font-size: 14px">
					Исполнитель:<?=@$model->user->name?><br>

                <?if (@$model->json('items')[0]['car']):?>
                    Тип: <?=$model->window?> - <?=$model->windowType == 'Сдвижной' ? "<b>{$model->windowType}</b>" : $model->windowType?>,
                <?endif?>
					Артикул: <?=$model->article?>,
				<?if (@$model->json('items')[0]['car']):?>
					<b><?=@$model->json('items')[0]['car']?></b>
				<?else:?>
					<b><?=@$model->json('items')[0]['name']?></b>
					<small><?=trim(@$model->json('items')[0]['settext'],';')?></small>
				<?endif?>
                <?if(@$model->detailClisp):?>
                       <br> <b style="font-size: 12px"><?=@$model->detailClisp?></b>
                <? endif ?>
			</td>
			<td rowspan="3" style="text-align: center;" >
				<?if (@$model->json('items')[0]['car']):?>
					<img src="<?=$scheme->file()?>" width="140px">
				<?endif?>
			</td>
		</tr>
                <tr>
			<?if ($model->jobPrice()):?>
				<td colspan="" style="text-align: center;border: 1px solid; border-right: none;"> <?=$model->jobPrice()?></td>
			<?endif?>
				<td style="text-align: center;border: 1px solid; border-right: none;">
	                Лекало: <span style="font-weight: bold"><?= @$model->json('items')[0]['lekalo'] ?></span>
	                Артикул: <span style="font-weight: bold"><?= @$model->carArticle ?></span>
				</td>

                        
		</tr>
	</tbody>
</table>
<?elseif ( in_array( $model->location_id, [ Yii::$app->params['erp_otk'], Yii::$app->params['erp_izgib'] ] )):?>
<p class="page-break-<?=$model->id?>"></p> 
<table class="invoice_items" width="559" cellpadding="0" cellspacing="0" style="border: 1px solid; border-bottom: 1px dashed;border-collapse: collapse;">
	<caption style='font-size: 1.1em'>
        <?php if ($model->prodGroup): ?>
            <span style="display: inline-block; background-color: black; color: white; padding: 5px;font-weight: bold;"><?= $model->prodGroup?></span>
        <?php endif;?>
		<b> <? $number = trim(str_replace('Наряд', '', $model->name)) ?>
            <?= str_replace((mb_substr($number, 5, mb_strlen($number))),
                ('<b style="font-size: x-large">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number); ?></b>
		<i><?=@$model->user->name?></i><br>
		Время: <?= Yii::$app->formatter->asTime(time())?>
                <small><?= 'Следующий участок: <span style="text-transform:uppercase;font-weight:bold">'.((isset($next) && $next) ? ErpLocation::findOne($next)->name : 'Cклад')."</span>"?></small>
        <?php if($model->lastExecutorTrainee()) : ?>
            <small><span style="display:inline-block; border:1px solid; background:greenyellow">Работал СТАЖЕР!!!</span></small>
        <?php endif; ?>
                
	</caption>
	<tbody>
		<tr>
	     	 <td rowspan="2"  style="text-align: center;border: 1px solid;font-size: 2em;padding: 10px;">
        		<span> <?= $model->productionLiteral?></span>
                 <? if ($model->location_id == Yii::$app->params['erp_otk'] && $model->upn && ($groupOtk = $model->upn->group)) {
                     echo "<br><span style='font-size:0.5em;font-style:italic'>$groupOtk</span>";
                 }?>
      		</td>
			<?if (@$model->json('items')[0]['car']):?>
				<td colspan="" style="text-align: center;border: 1px solid; border-right: none;"><span style="font-size:0.9em;font-weight: bold"><?= @$model->windowName?></span> | <small><?=$model->window?></small> <small><?=$model->windowType?></small></td>
			<?endif?>

		</tr>
		<tr>
			<td colspan="" rowspan="" style="text-align: center;border: none;">
				<?if (@$model->json('items')[0]['car']):?>
	                Лекало: <span style="font-weight: bold"><?= @$model->json('items')[0]['lekalo'] ?></span>
	                Артикул: <span style="font-weight: bold"><?= @$model->carArticle ?></span>
				<?else:?>
					<b><?=@$model->json('items')[0]['name']?></b>
					<small><?=trim(@$model->json('items')[0]['settext'],';')?></small>
				<?endif?>
			</td>
		</tr>
                 
			
	</tbody>
</table><? if($model->location_id == Yii::$app->params['erp_okleika']): ?>
                <small> <?=$model->user->activeWorkplaces?> </small>
                <? endif ?>
<?if ($model->jobPrice()):?>
      <?= $model->jobPrice()?>
<?endif?>
<?if (isset($isClipsi)) echo "<span style='font-weight:bolder;font-style:italic; font-size:15px; position:absolute; left:250px' class='lead'>В ПЕРЕДЕЛКЕ КЛИПСЫ УЖЕ ЕСТЬ !!!! </span>"?>
<br>/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<?else:?>
<p class="page-break-<?=$model->id?>"></p> 
<table class="invoice_items" width="559" cellpadding="0" cellspacing="0" style="border: 1px solid; border-bottom: 1px dashed;border-collapse: collapse;">
	<caption style='font-size: 1.1em'>
        <?php if ($model->prodGroup): ?>
            <span style="display: inline-block; background-color: black; color: white; padding: 5px;font-weight: bold;"><?= $model->prodGroup?></span>
        <?php endif;?>
        <b> <? $number = trim(str_replace('Наряд', '', $model->name)) ?>
            <?= str_replace((mb_substr($number, 5, mb_strlen($number))),
                ('<b style="font-size: x-large">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number); ?></b>
		<i><?=@$model->user->name?></i><br>
		Время: <?= Yii::$app->formatter->asTime(time())?>
                <small><?= 'Следующий участок: <span style="text-transform:uppercase;font-weight:bold">'.((isset($next) && $next) ? ErpLocation::findOne($next)->name : 'Cклад')."</span>"?></small>
        <?php if($model->lastExecutorTrainee()) : ?>
            <small><span style="display:inline-block; border:1px solid; background:greenyellow">Работал СТАЖЕР!!!</span></small>
        <?php endif; ?>

	</caption>
	<tbody>
		<tr>
	     	 <td rowspan="2"  style="text-align: center;border: 1px solid;font-size: 2em;padding: 10px;"> 
        		<span> <?= $model->productionLiteral?></span>
      		</td> 
			<?if (@$model->json('items')[0]['car']):?>
				<td colspan="" style="text-align: center;border: 1px solid; border-right: none;"><span style="font-size:0.9em;font-weight: bold"><?= @$model->windowName?></span> | <small><?=$model->window?></small> <small><?=$model->windowType?></small></td>
			<?endif?>

		</tr>
		<tr>
			<td colspan="" rowspan="" style="text-align: center;border: none;">
				<?if (@$model->json('items')[0]['car']):?>
					Автомобиль:
					<b><?=@$model->json('items')[0]['car']?></b>
				<?else:?>
					<b><?=@$model->json('items')[0]['name']?></b>
					<small><?=trim(@$model->json('items')[0]['settext'],';')?></small>
				<?endif?>
			</td>
		</tr>
                 
			
	</tbody>
</table><? if($model->location_id == Yii::$app->params['erp_okleika']): ?>
                <small> <?=$model->user->activeWorkplaces?> </small>
                <? endif ?>
<?if ($model->jobPrice()):?>
      <?= $model->jobPrice()?>
<?endif?>
<?if (isset($isClipsi)) echo "<span style='font-weight:bolder;font-style:italic; font-size:15px; position:absolute; left:250px' class='lead'>В ПЕРЕДЕЛКЕ КЛИПСЫ УЖЕ ЕСТЬ !!!! </span>"?>
<br>/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<?endif;?>
