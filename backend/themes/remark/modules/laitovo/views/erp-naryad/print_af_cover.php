<?php

use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use backend\modules\laitovo\models\ErpNaryad;
use common\models\laitovo\Cars;
use core\services\SAutoFilterItems;

/**
 * @var $model ErpNaryad
 */


$scheme = new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

$itemtext = isset($model->json('items')[0]['itemtext']) ? $model->json('items')[0]['itemtext'] : '';
$settext = @$model->json('items')[0]['settext'];
?>
<p class="page-break-<?= $model->id ?>"></p>

<table width="500" cellpadding="9" cellspacing="0" style="border: 2px solid; border-bottom: 1px dashed;border-collapse: collapse;">
    <tr>
        <td style="text-align: center; font-family: Verdana;font-size: 22pt" height="14mm" width="40%">
            <div style="text-align:center; padding-left: 70px; padding-top: 5px">
                <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 1) ?>
            </div>
            <? $number = trim(str_replace('Наряд', '', $model->name)) ?>
            <?= str_replace((mb_substr($number, 5, mb_strlen($number))),
                ('<b style="font-size: 26pt">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number); ?>
        </td>
        <td height="14mm" width="60%" colspan="2"
            style="text-align: center; border-left: 2px solid; border-bottom: 1px solid; text-transform: uppercase; width: 40%; <?= $model->order && @$model->order->json('category') == 'Экспорт' ? 'background: #000;color: #fff' : '' ?>">
            <div style="text-align:center; padding-left:65px; padding-bottom: 5px">
                <?= $generator->getBarcode(@$model->upn->barcode, $generator::TYPE_CODE_128, 1) ?>
            </div>
            <b><?= @$model->json('items')[0]['name'] ?></b>
            <small><?= $itemtext ? $itemtext : trim($settext, ';') ?></small>
        </td>
    </tr>
</table>



<style>
    .table-label {
        text-align: center;
        border: 20px solid gray;
    }

    .table-label td {
        border: none;
        padding-bottom: 10px;
        font-size: 1.6em;
    }

    .table-label td.bodered-cell {
        border: 1px solid black;
    }

    .table-width {
        width: 500px;
    }
</style>

<?php if (preg_match('/КЗА/', @$model->json('items')[0]['name']) === 1) :?>

<table class="table-width" style="text-align: center; ">
    <tbody>
        <tr>
            <td style="padding: 10px;font-size: 2em"><b>КЗА</b></td>
        </tr>
        <tr>
            <td style="padding: 10px;font-size: 3em"><b><u>БЕЗ ЛЕЙБ</u>, ПАКЕТ, НАКЛЕЙКА</b></td>
        </tr>
        <td class="bodered-cell" style="width: 40%">
            <div style="padding-left: 40%;padding-top:20px;padding-bottom:10px;">
                <?= $generator->getBarcode(@$model->upn->barcode, $generator::TYPE_CODE_128, 1,40) ?>
            </div>
            <b style="font-size: 3em"><?= $model->logist_id ?></b>
        </td>

    </tbody>
</table>
<?php else:?>
    <table class="table-width table-label">
        <tbody>
        <tr style="padding-top: 10px">
            <td colspan="2"><img src="/img/filterlogo.jpg" alt="автофильтр логотип" height="100px" width="auto"></td>
        </tr>
        <tr>
            <td class="bodered-cell" style="width: 60%">
                <?php
                $str2 = preg_match('/\[(.+)\]/',@$model->json('items')[0]['name'],$matches);
                $str1 = str_replace($matches[0],'',@$model->json('items')[0]['name']);
                ?>
                <b style="font-size: larger"> <?= SAutoFilterItems::getTitle($model->article)?></b>
                <!--            <strong>--><?//= $matches[0]?><!--</strong>-->
            </td>
            <td class="bodered-cell" style="width: 40%">
                <?php if ($afBarcode = SAutoFilterItems::getBarcode($model->article)): ?>
                <div style="padding-left: 17%;padding-top:20px;padding-bottom:10px;">
                    <?= $generator->getBarcode($afBarcode, $generator::TYPE_CODE_128, 1,40) ?>
                </div>
                <b style=""><?= $afBarcode ?></b>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td class="bodered-cell" style="font-size: 20px">
                Дата изготовления:
                <br>
                <br>
                <br>
                Срок хранения 2 года
            </td>
            <td>
                <b>Количество: <br><?= $matches[1] ?> шт.</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="bodered-cell">
                <p>
                    <b style="font-size: 20px">
                        ООО &laquo;Костромское предприятие &laquo;Автофильтр&raquo; <br>
                        156007, г.Кострома, ул. Пушкина, 43/102 <br>
                        Тел./факс: (4942) 55-0991, 55-0821 <br>
                        www.afilter.ru

                    </b>
                </p>
            </td>
        </tr>
        </tbody>
    </table>
<?php endif;?>




