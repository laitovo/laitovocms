<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpScheme;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpNaryad */
/* @var $form yii\widgets\ActiveForm */

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

Yii::$app->view->registerJs('
    $(document).ready(function() {
        $(\'#tbody-items .name-editable\').editable({
            emptytext:"Укажите название товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .article-editable\').editable({
            emptytext:"Укажите артикул товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .quantity-editable\').editable({
            emptytext:"Укажите кол-во",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .comment-editable\').editable({
            emptytext:"Комментарий",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).parent().next().val(newValue);
                parseitemsjson();
            }
        });
    });

    function parseitemsjson () {
        var items =[];
        var totalcount =0;
        $(\'#tbody-items .items-order\').each(function( index, value ) {
            itemjson=$(this).find(\'.itemjson\').serializeArray();
            var obj ={};
            $.each(itemjson, function( index1, value1 ) {
                obj[value1.name]=value1.value;
            });
            totalcount=parseFloat(totalcount+parseFloat(obj.quantity));
            items.push(obj);
        });
        $(\'#erpnaryad-items\').val(JSON.stringify(items));
        $(\'#erpnaryad-totalcount\').text(totalcount);
    }

    $("body").on("click","#tbody-items .items-order .delete-orderitem",function(e){
        $(this).parent().parent().remove();
        parseitemsjson();
        e.preventDefault();
    });

    $("body").on("click","#add-orderitem",function(e){
        $(".add-item-tr").clone().appendTo("#tbody-items");

        $(\'#tbody-items .add-item-tr .name-editable\').editable({
            emptytext:"Укажите название товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .article-editable\').editable({
            emptytext:"Укажите артикул товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .quantity-editable\').editable({
            emptytext:"Укажите кол-во",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .comment-editable\').editable({
            emptytext:"Комментарий",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).parent().next().val(newValue);
                parseitemsjson();
            }
        });

        $("#tbody-items .add-item-tr").removeClass("hidden add-item-tr");
        parseitemsjson();
        e.preventDefault();
    });


    ', \yii\web\View::POS_END);


?>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th colspan="2">Товар</th>
            <th>Кол-во</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="tbody-items">
        <? if ($model->json('items')) foreach ($model->json('items') as $key => $row):
            $name = isset($row['name']) ? $row['name'] : '';
            $article = isset($row['article']) ? $row['article'] : null;
            $quantity = isset($row['quantity']) ? $row['quantity'] : null;
            $comment = isset($row['comment']) ? $row['comment'] : null;
            $total = isset($total) ? $quantity + $total : $quantity;
            ?>
            <tr class="items-order">
                <td>
                    <a href="#" class="name-editable" data-type="text"><?= $name ?></a>
                    <input class="itemjson" type="hidden" name="name" value='<?= $name ?>'><br>
                    <small><a href="#" class="comment-editable" data-type="text"><?= $comment ?></a></small>
                    <input class="itemjson" type="hidden" name="comment" value='<?= $comment ?>'>
                </td>
                <td>
                    <a href="#" class="article-editable" data-type="text"><?= $article ?></a>
                    <input class="itemjson" type="hidden" name="article" value='<?= $article ?>'>
                </td>
                <td>
                    <a href="#" class="quantity-editable" data-type="text"><?= $quantity ?></a>
                    <input class="itemjson" type="hidden" name="quantity" value='<?= $quantity ?>'>
                </td>
                <td>
                    <a href="#" class="text-danger delete-orderitem"><i class="wb wb-close"></i></a>
                </td>
            </tr>
        <? endforeach ?>
        </tbody>
        <tr class="items-order hidden add-item-tr">
            <td>
                <a href="#" class="name-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" name="name" value=''><br>
                <small><a href="#" class="comment-editable" data-type="text"></a></small>
                <input class="itemjson" type="hidden" name="comment" value=''>
            </td>
            <td>
                <a href="#" class="article-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" name="article" value=''>
            </td>
            <td>
                <a href="#" class="quantity-editable" data-type="text">1</a>
                <input class="itemjson" type="hidden" name="quantity" value='1'>
            </td>
            <td>
                <a href="#" class="text-danger delete-orderitem"><i class="wb wb-close"></i></a>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="text-right lead"><?= Yii::t('app', 'Итого:') ?></td>
            <td class="lead" id="erpnaryad-totalcount"><?= isset($total) ? $total : 0 ?></td>
            <td></td>
        </tr>
    </table>
</div>


<?= Html::a('<span class="icon wb-plus"></span>', '#', ['class' => 'pull-left btn btn-icon btn-outline btn-round btn-default', 'id' => 'add-orderitem', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('yii', 'Добавить товар')]) ?>

<div class="clearfix"></div>
<p></p>

<?php $form = ActiveForm::begin(); ?>

<? //= $form->field($model, 'items')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'order_id')->textInput() ?>

<?= $form->field($model, 'scheme_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpScheme::find()->all(), 'id', 'name'))) ?>

<?= $form->field($model, 'start')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::getAllProduction(), 'id', 'name'))) ?>

<?//if (Yii::$app->user->getId() == 43 || Yii::$app->user->getId() == 44 || Yii::$app->user->getId() == 3 || Yii::$app->user->getId() == 31 || Yii::$app->user->getId() == 21 || Yii::$app->user->getId() == 14): ?>
<?if (Yii::$app->user->getId() == 3 || Yii::$app->user->getId() == 21 || Yii::$app->user->getId() == 48 || Yii::$app->user->getId() == 53): ?>
    <?if (!in_array($model->sort,[1,2])):?>
        <?= $form->field($model, 'sort')->dropDownList($model->prioritetsForView) ?>
    <?endif;?>
<? endif;?>



<!-- <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpUser::find()->all(), 'id', 'name'))) ?> -->

<?= $form->field($model, 'user_id')->widget(Select2::classname(), [
    'theme' => Select2::THEME_BOOTSTRAP,
    'data' => ArrayHelper::map(ErpUser::find()->all(), 'id', 'name'),
    'options' => ['placeholder' => 'Выберите сотрудника ...'],
    'language' => 'ru',
    'pluginOptions' => [
        'allowClear' => true,
    ],
]); ?>



<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>

<?= $form->field($model, 'status')->dropDownList($model->statuses) ?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
