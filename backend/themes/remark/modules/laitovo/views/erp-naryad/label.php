<?php
use yii\helpers\Html;
use common\models\laitovo\ErpNaryad;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>

<p class="page-break-<?= $model->id ?>"></p>
<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"
       style="border: none;font-size: 12px;text-align: center;">
    <tbody>
    <tr>
        <td style="border: none;"><b>
                <? if (@$model->json('items')[0]['car']): ?>
                    <b><?= @$model->json('items')[0]['car'] ?></b>
                <? else: ?>
                    <?= @$model->json('items')[0]['name'] ?>
                <? endif ?>
            </b></td>
    </tr>
    <tr>
        <td style="border-left: none;border-right: none;border-bottom: 1px solid;border-top: 1px solid">
            <b><?= $model->article ?></b>
        </td>
    </tr>
    <tr>
        <? if (@$model->json('items')[0]['car']): ?>
            <td style="height: 25px;border-left: none;border-right: none;border-bottom: 1px solid"><b>
                    <?
                    $n = $model->window;
                    mb_internal_encoding("UTF-8");
                    $artcl = mb_substr($model->article, 2);
                    echo $n . $artcl;
                    ?>
                </b></td>
        <? else: ?>
            <td style="border-left: none;border-right: none;">&nbsp;</td>
        <? endif ?>
    </tr>
    <tr>
        <td style="padding: 10px; border: none;text-align: center;">
            <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 1.8) ?>
        </td>
    </tr>
    </tbody>
</table>
