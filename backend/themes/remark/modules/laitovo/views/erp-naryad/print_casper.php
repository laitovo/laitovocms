<?php

use backend\modules\laitovo\models\ErpNaryad;

/**
 * @var $model ErpNaryad
 */

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>

<style>
    table, th, td {
        border: 1px solid black;
    }
    table {
        border-spacing: 0px;
        border-collapse: collapse;
    }

    .table-label td {
        border: none;
        padding-top: 30px;
        padding-bottom: 30px;
        font-size: 1.8em;

    }

    .table-label {
        text-align: center;
    }
    .second-row {
        width: 78%;
    }
    .table-width {
        min-width: 500px;
    }
</style>

<table class="table-width">
    <tbody>
        <tr>
            <td>Фио</td>
            <td class="second-row"><?= $model->user ? $model->user->name : ''?></td>
        </tr>
        <tr>
            <td>Дата</td>
            <td class="second-row"><?= Yii::$app->formatter->asDateTime(time())?></td>
        </tr>
        <tr>
            <td>Кол-во</td>
            <td class="second-row">1</td>
        </tr>
    </tbody>
</table>
<br>
<table class="table-width">
    <tbody >
        <tr><td>I</td><td class="second-row">Сшиваем плечи</td><td rowspan="17" style="text-align: center">46</td></tr>
        <tr><td>II</td><td class="second-row">Сшиваем капюшон</td></tr>
        <tr><td>III</td><td class="second-row">Резинка в капюшон</td></tr>
        <tr><td>IV</td><td class="second-row">Капюшон в горловину с лэйбой</td></tr>
        <tr><td>V</td><td class="second-row">Сшить передние полочки</td></tr>
        <tr><td>VII</td><td class="second-row">Закрепка молнии</td></tr>
        <tr><td>VIII</td><td class="second-row">Вшиваем молнию закрепка</td></tr>
        <tr><td>IX</td><td class="second-row">Резинка в рукав правая</td></tr>
        <tr><td>X</td><td class="second-row">Втачивание правого рукава</td></tr>
        <tr><td>XI</td><td class="second-row">Резинка в рукав левая</td></tr>
        <tr><td>XII</td><td class="second-row">Втачивание левого рукава</td></tr>
        <tr><td>XIII</td><td class="second-row">Сшиваем задние полочки</td></tr>
        <tr><td>XIV</td><td class="second-row">Спинку к задним полочкам</td></tr>
        <tr><td>XV</td><td class="second-row">Резинка в талию</td></tr>
        <tr><td>XVI</td><td class="second-row">Боковые швы. Закрепка рукава</td></tr>
        <tr><td>XVII</td><td class="second-row">Резинка в брюки</td></tr>
        <tr><td>XVIII</td><td class="second-row">Стачивание шагового шва. Закрепка брюк</td></tr>
        <tr><td></td><td class="second-row">ОТК упаковка</td><td style="text-align: center">4</td></tr>
    </tbody>
</table>
<br>

<table class="table-width table-label">
    <tbody>
    <tr>
        <td ><?= $model->name ?></td>
    </tr>
    <tr>
        <td style="padding-left: 30%"><?= $generator->getBarcode(@$model->upn->barcode, $generator::TYPE_CODE_128, 2,60) ?></td>
    </tr>
    <tr>
        <td>Защитный комбинезон каспер 40 шт.</td>
    </tr>
    </tbody>
</table>


