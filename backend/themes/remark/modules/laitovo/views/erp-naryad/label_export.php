<?php
use yii\helpers\Html;
use common\models\laitovo\ErpNaryad;

$type = '';
$product = $model->searchProduct();
if ($product) {
    $type = $product->title_en ? $product->title_en : '';
}
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

?>

<p class="page-break-<?= $model->id ?>"></p>
<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td style="padding: 3px;border:1px solid" colspan="2">
            <b>
                <? if (@$model->json('items')[0]['car']): ?>
                    Protective screens for car windows<br>
                    <?= @$model->car->fullEnName . '<br>' . $type?>
                <? else: ?>
                    <?php echo $type?>

                    <?/*switch (@$model->dopArticle) {
                        case 564:
                            echo "Set of clip holders";
                            break;
                        case 1189:
                            echo "Trunk Organizer";
                            break;
                        case 1394:
                            echo "Set of magnetic holders";
                            break;
                        case 1220:
                            echo "Car air freshener (Bubble gum)";
                            break;
                        case 1221:
                            echo "Car air freshener (New car)";
                            break;
                        case 1222:
                            echo "Car air freshener (Citrus)";
                            break;
                        case 1223:
                            echo "Car air freshener (Vanilla)";
                            break;
                        case 1224:
                            echo "Car air freshener (Anti tobacco)";
                            break;

                        default:
                            echo @$model->json('items')[0]['name'];
                            break;
                    }
                    */?>
                <? endif ?>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;border-bottom: 1px solid;font-family: 'Roboto', sans-serif;font-size: 24pt;line-height: 0.7em">
            <b><?= $model->article ?></b>
        </td>
    </tr>
    <tr>
        <td style="border-collapse: collapse;padding: 0;border-bottom: none;border: none;">
            <span style="display: inline-block;border: 1px solid black;border-radius: 1px;width: 10px"> ! </span> UPN: <?= $model->logist_id ? $model->logist_id : 'undefined' ?>
        </td>
        <td rowspan="2" style="border-collapse: collapse;padding: 0;border-bottom: none;border: none;">
            <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 1) ?>
        </td>
    </tr>
    <tr>
        <td style="border-left: none;border-right: none; font-size: 10px;border-bottom: 1px solid">
            <? if (@$model->json('items')[0]['car']): ?>
                Type of fixing: <?= $model->car ? $model->car->json('typekrep') : '' ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <td style="border: none;font-size: 11px">Tel. +49(0)4095063310</td>
        <td style="border: none;font-size: 11px">info@laitovo.de</td>
    </tr>
    <tr>
        <td colspan="2" style="border: none;font-size: 11px;">
            Made by request of LAITOVO Manufactory T. & S. GMBH
            Ferdinandstrasse 25-27 D-20095 Hamburg Germany
        </td>
    </tr>
    </tbody>
</table>
