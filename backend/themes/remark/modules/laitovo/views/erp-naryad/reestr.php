<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\laitovo\ErpNaryad;

$generator = new Picqer\Barcode\BarcodeGeneratorPNG();


?>

<p class="reestr-break-<?= $model->id ?>"></p>


<div class="text-center" style="width: 100%;position: relative;margin: auto;border-bottom: 1px dashed;padding: 5px">
    <div style="width: 30%;float: left;">
        <img width="100%" height="100px"
             src="data:image/png;base64,<?= base64_encode($generator->getBarcode($model->reestr, $generator::TYPE_CODE_128)) ?>">
    </div>
    <div style="width: 70%;float: left;vertical-align: middle;">
        <h4>Реестр нарядов: <br><?= implode(', ', ArrayHelper::map($group, 'id', 'id')) ?></h4>
    </div>
</div>
