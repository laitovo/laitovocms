<?php
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;


$scheme = new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

?>


<p class="page-break-<?= $model->id ?>"></p>


<? if (@$model->json('items')[0]['car']): ?>


    <table height="559" width="559" style="transform: rotate(-90deg);">
        <tr>
            <td colspan="3">
                <div style="font-size: 12px;text-transform: uppercase;background: #cecece;">
                    KELEPÇELERİN KURULUM ŞEMASI / Schéma d'installation des serre-joints<br>
                    ESQUEMA DE INSTALAÇÃO DE FIXADORES / СХЕМА УСТАНОВКИ ЗАЖИМОВ-ДЕРЖАТЕЛЕЙ<br>
                    BEFESTIGUNGPLAN FÜR HALTEKLAMMERN / SCHEME OF INSTALLATION OF CLIP-HOLDERS
                </div>


            </td>
        </tr>

        <tr>
            <td style="width: 33.33%;vertical-align: top;text-align: justify;font-size: 9px">
                <p style="text-align: center;"><span style="background: #000;color: #fff;float: left;">EN</span><b>Dear
                        customer!</b></p>
                <p>Before installation, be sure to read the "Instructions for installation and operation of a set of
                    protective screens “Laitovo”.</p>
                <p><b>Installation:</b> Fix clip-holders with letter designations, shown in the diagram, into the places
                    indicated by the arrows. Letter designations of clips are indicated on the individual packaging,
                    supplied in the kit. If the designations in the diagram below are missing, install protective
                    screens in thrust (without clips).</p>
                <p style="text-align: center;"><span style="background: #000;color: #fff;float: left;">FR</span><b>Cher
                        client!</b></p>
                <p>Avant l'installation lisez obligatoirement "Instruction d'installation et d'exploitation de
                    l'assortiment d'écrans de protection Laitovo pour les voitures.</p>
                <p><b>Installation:</b> Fixez les serre-joints selon le numéro désigné sur le schéma aux endroits
                    indiqués par les flèches. Le numéro du serre-joint est indiqué sur l'emballage individuel fourni
                    dans l'assortiment.S'il n'a pas d'indications sur le schéma ci-dessous, l'installation des écrans de
                    protection se fait à la poussée (sans serre-joints).</p>
                <br>
                <? if (@$model->json('items')[0]['car']): ?>
                    <img src="<?= $scheme->file() ?>" width="190px">
                <? endif ?>

            </td>
            <td style="width: 33.33%;vertical-align: top;text-align: justify;font-size: 9px">
                <p style="text-align: center;"><span style="background: #000;color: #fff;float: left;">RU</span><b>Уважаемый
                        покупатель!</b></p>
                <p>Перед началом установки обязательно ознакомьтесь с «Инструкцией по установке и эксплуатации комплекта
                    автомобильных защитных экранов «Laitovo».</p>
                <p><b>Установка:</b> Закрепите в места, указанные стрелками, зажимы-держатели с номером, обозначенном на
                    схеме. Номер зажима-держателя указан на индивидуальном пакетике, входящем в Ваш комплект. Если номер
                    в схеме отсутствует - крепление устанавливать не нужно.</p>
                <p style="text-align: center;"><span style="background: #000;color: #fff;float: left;">PT</span><b>Caro
                        cliente!</b></p>
                <p>Antes da instalação, certifique-se de ler as "Instruções de instalação e operação de um conjunto de
                    cortinas Laitovo"</p>
                <p><b>Instalação:</b> Fixe as molas de clipe com designações por letra, mostrado no diagrama, nos locais
                    indicados pelas setas. As designações da letra dos clipes estão indicados na embalagem individual,
                    fornecido no kit. Se não existirem designações no diagrama abaixo , instale as cortinas com impulso
                    (sem fixadores).</p>

                <div style="font-size: 8px">
                    <p style="text-align: center;"><b>НАЗНАЧЕНИЕ И ЭКСПЛУАТАЦИЯ</b></p>
                    <p>Защитные экраны для автомобильных окон предназначены для защиты салона транспортного средства, в
                        том числе находящихся в нем пассажиров от солнца, насекомых, осколков стекла и защиты частной
                        жизни в транспортном средстве.  </p>
                    <p style="text-align: center;"><b>ХРАНЕНИЕ</b></p>
                    <p>Комплект защитных экранов Laitovo вне эксплуатации не требует особых условий хранения и
                        осуществляется в удобном для хранения месте.</p>
                    <p>Рекомендуется хранить защитные экраны в специальной сумке для хранения защитных экранов Laitovo.
                        Защитные экраны и комплектующие не следует хранить вблизи открытого пламени, острых, режущих
                        предметов и в помещении с повышенной влажностью.</p>
                </div>

            </td>
            <td style="width: 33.33%;vertical-align: top;text-align: justify;font-size: 9px">
                <p style="text-align: center;"><img src="/img/laitovo_logo.png"><br><b>www.laitovo.ru www.laitovo.eu<br>www.laitovo.kz</b>
                </p>
                <p></p>
                <p style="text-align: center;"><span style="background: #000;color: #fff;float: left;">DE</span><b>Sehr
                        geehrter Kunde!</b></p>
                <p>Zu Beginn der Befestigung machen Sie sich mit der „Befestigungs- und Betriebsanleitung für Kfz-Sicht-
                    und Sonnenschutz „Laitovo“ vertraut.</p>
                <p><b>Befestigung:</b> Befestigen Sie an den mit Pfeilern markierten Stellen Halteklammern mit der im
                    Plan bezeichneten Nummer. Die Halteklammernummer ist auf der Individualtüte angegeben, die zu Ihrem
                    Set gehört.</p>
                <p style="text-align: center;"><span style="background: #000;color: #fff;float: left;">TR</span><b>Sayın
                        müşterimiz!</b></p>
                <p>Montaja başlamadan önce mutlaka “Laitovo” oto perdeleri montaj ve kullanma kılavuzunu okuyun.</p>
                <p><b>Ön/arka yan perde montajı:</b> Okların gösterdiği yerlere numaralı kelepçeleri takın. Kelepçelerin
                    numaraları özel ambalajları üzerinde bellirtilmektedir. Aşağıdaki şemasinda özel işaretler yok ise,
                    kuruyucu perde kepçesiz monte edilir.</p>

                <table>
                    <tr>
                        <td style="width: 1px;">
                            <span style="position: absolute;font-size: 50px;margin: -30px -5px">!</span>
                        </td>
                        <td style="font-size: 6px;padding-left: 5px;">
                            <p><b>Перед установкой зажимов-держателей и защитных экранов убедитесь в том, что Вы
                                    приобрели комплект соответствующий Вашей марке автомобиля, предварительно примерьте
                                    защитный кран к окнам автомобиля.</b></p>
                        </td>
                    </tr>
                </table>

                <div style="font-size: 6px">
                    <p style="text-align: center;"><b>ОСОБЕННОСТИ ЭКСПЛУАТАЦИИ</b></p>
                    <p>Не рекомендуется эксплуатация передних боковых защитных экранов во время движения автомобиля. Не
                        рекомендуется стряхивать пепел от сигарет, выбрасывать мусор в окно с установленными защитными
                        экранами, если они не имеют специальных вырезов. Это может привести к повреждению тканевого
                        покрытия. Зимой (в холодную погоду), в непрогретом салоне, материал защитных экранов может
                        провисать (особенность материала). Этот дефект исчезает, как только вы прогреете автомобиль. Не
                        рекомендуется оказывать физическое воздействие (ущемление, прокалывание и т. д.) тканевого
                        покрытия защитного экрана, это - может привести к повреждению.</p>
                </div>

            </td>
        </tr>

    </table>

<? endif ?>
