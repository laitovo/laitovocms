<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Json;
use backend\modules\laitovo\models\ErpLocation;
use common\models\laitovo\ErpUser;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpNaryad */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Все наряды'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => ArrayHelper::merge([
        'name:text:ID',
        'barcode',
        'article',
        [
            'attribute' => 'order_id',
            'format' => 'html',
            'value' => $model->order ? Html::a(Html::encode($model->order->name), ['erp-order/view', 'id' => $model->order->id]) : ($model->upn ? Html::a('№ '. Html::encode(@$model->upn->orderEntry->order->source_innumber), ['erp-order/view', 'id' => @$model->upn->orderEntry->order->source_innumber]) : null),
        ],
        [
            'attribute' => 'user_id',
            'format' => 'html',
            'value' => $model->user ? Html::a(Html::encode($model->user->getName()), ['erp-user/view', 'id' => $model->user->id]) : null,
        ],
        [
            'attribute'=>'location_id',
            'format'=>'html',
            'value'=>$model->locationstart 
            ? Html::a(Html::encode($model->locationstart->name), ['erp-location/view','id'=>$model->locationstart->id]).' <small>'.($model->location ? $model->location->name : 'Облако').'</small>'
            : ($model->location ? Html::a(Html::encode($model->location->name), ['erp-location/view','id'=>$model->location->id]) : null),

        ],
        [
            'attribute' => 'scheme_id',
            'format' => 'html',
            'value' => $model->scheme ? Html::a(Html::encode($model->scheme->name), ['erp-scheme/view', 'id' => $model->scheme->id]) : null,
        ],
        [
            'attribute' => 'sort',
            'value' => @$model->prioritets[$model->sort],
        ],
        'status',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => $model->author ? $model->author->name : null,
        ],
        [
            'attribute' => 'prodLiteral',
            'label' => 'Литера, в которой лежит',
            'value' => $model->currentProductionLiteral,
        ],
        [
            'attribute' => 'prodGroup',
            'label' => 'Группа нарядов',
            'value' => $model->prodGroup,
        ],
    ], $model->reworkActs ? [
        [
            'attribute' => 'Акты переделок',
            'format' => 'raw',
            'value' =>
                function($data) {
                    $content = '';
                    foreach ($data->reworkActs as $act) {
                        $content .= Html::a(Html::encode($act->name), ['/laitovo/erp-rework-act/view', 'id' => $act->id]) . '<br>';
                    }
                    return $content;
                }
        ]]
    : [])
]) ?>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Автомобиль</th>
            <th>Товар</th>
            <th>Артикул</th>
            <th>Кол-во</th>
        </tr>
        </thead>
        <tbody id="tbody-items">
        <? foreach ($model->json('items') as $key => $row):
            $name = isset($row['name']) ? $row['name'] : '';
            $car = isset($row['car']) ? $row['car'] : null;
            $article = isset($row['article']) ? $row['article'] : null;
            $quantity = isset($row['quantity']) ? $row['quantity'] : null;
            $comment = isset($row['comment']) ? $row['comment'] : null;
            $settext = isset($row['settext']) ? $row['settext'] : null;
            $total = isset($total) ? $quantity + $total : $quantity;
            ?>
            <tr class="items-order">
                <td><?= $car ?><br>
                    <small><i><?= trim($settext, ';') ?></i></small>
                </td>
                <td><?= $name ?><br>
                    <small><i><?= $comment ?></i></small>
                </td>
                <td><?= $article ?></td>
                <td><?= $quantity ?></td>
            </tr>
        <? endforeach ?>
        </tbody>
        <!-- <tr></tr>
            <tr>
                <td colspan="2" class="text-right lead"><?= Yii::t('app', 'Итого:') ?></td>
                <td class="lead" id="erporder-totalcount"><?= isset($total) ? $total : 0 ?></td>
            </tr> -->
    </table>
</div>

<h4>Выдача на участок</h4>

<div class="table-responsive">  
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Участок</th>
            </tr>
        </thead>
        <tbody id="tbody-items">
            <tr class="items-order">
                <td><?= ErpLocation::findOne($model->start) ? ErpLocation::findOne($model->start)->name : ''?></td>
            </tr>
        </tbody>
    </table>
</div>

<h4>Движение</h4>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Дата</th>
            <th>Передано с участка</th>
            <th>Передано на участок</th>
            <th>Ответственный</th>
        </tr>
        </thead>
        <tbody id="tbody-items">
        <? if ($model->json('log')) foreach ($model->json('log') as $key => $row):
            ?>
            <tr class="items-order">
                <td><?= @Yii::$app->formatter->asDateTime($row['date']) ?></td>
                <td><?= $row['location_from'] ?></td>
                <td><?= $row['location_to'] ?></td>
                <td><?= ErpUser::getNameByName($row['user']) ?></td>
            </tr>
        <? endforeach ?>
        </tbody>
    </table>
</div>

<? if (in_array(Yii::$app->user->getId(),[21,3,43,46,36,173,12])) : ?>
    <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>
<?endif;?>

<?= Html::a(Yii::t('app', 'Распечатать'), ['print', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-info', 'target' => '_blank']) ?>

<?=Html::a(Yii::t('app', 'Распечатать этикетку'), ['print-label', 'id' => $model->id],['class' => 'btn btn-outline btn-round btn-info','target' => '_blank'])?>
<!--Кнопка для остановки и возобновления работы наряда-->
<? if($model->status!=$model::STATUS_IN_PAUSE) : /* если не находиться на паузе */ ?>
<?= Html::a(Yii::t('app', 'Остановить'), ['pause', 'id' => $model->id],[
        'class' => 'btn btn-outline btn-round btn-info deleteconfirm',
        'style'=>'margin-right:5px',
        'data' => [
            'confirm' => Yii::t('yii', 'Хотите извлечь наряд из производства?'),
            'method' => 'post',
        ]])
?>
<? else : /* если же он находиться на пааузе*/?>
<?= Html::a(Yii::t('app', 'Возобновить'), ['unpause', 'id' => $model->id],[
        'class' => 'btn btn-outline btn-round btn-info deleteconfirm',
        'style'=>'margin-right:5px',
        'data' => [
            'confirm' => Yii::t('yii', 'Хотите вернуть наряд в производство? ВНИМАНИЕ!!! ЕСЛИ НАРЯД НА РУКАХ ЧЕЛОВЕКА, ТО ОН АВТОМАТИЧЕСКИ СДАСТСЯ С УЧАСТКА!!!'),
            'method' => 'post',
        ]])
?>
<? endif; ?>
<?= Html::a(Yii::t('app', 'Распечатать инструкцию'), ['print-manual-new', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-info', 'target' => '_blank']) ?>

<? if (Yii::$app->user->getId() == 21): ?>
<?= $model->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
    'data-toggle' => "tooltip",
    'data-original-title' => Yii::t('yii', 'Delete'),
    'data' => [
        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
        'method' => 'post',
    ],
]) ?>
<? endif ?>

<? if (Yii::$app->user->getId() == 21 || Yii::$app->user->getId() == 24 || Yii::$app->user->getId() == 36): ?>
<?= $model->status != $model::STATUS_CANCEL ? Html::a(Yii::t('app', 'Отменить'), ['cancel', 'id' => $model->id], [
    'class' => 'deleteconfirm pull-right btn btn-outline btn-round btn-danger',
    'style' => 'margin-right:5px',
    'data' => [
        'confirm' => Yii::t('yii', 'Отменить этот наряд?'),
        'method' => 'post',
    ]]) : '' ?>
<? endif; ?>
