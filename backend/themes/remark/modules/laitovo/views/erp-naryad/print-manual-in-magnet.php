<?php
use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;


$scheme = new CarsScheme();
if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
    $scheme->model = new CarsForm($car->id);
}
$scheme->brand = $model->brand;
$scheme->type = $model->type;
$scheme->type_clips = $model->type_clips;
$scheme->window = $model->window;

?>
    <p class="page-break-<?= $model->id ?>"></p>


<? if (@$model->json('items')[0]['car']): ?>


    <table width="820">
        <tr>
            <td colspan="2" style=" text-align: center">
                <li style="list-style-type: none">
                    <b style="font-size: 13px;">ИНСТРУКЦИЯ ПО УСТАНОВКЕ И ЭКСПЛУАТАЦИИ </b>
                </li>
                <li style="list-style-type: none">
                    <b style="font-size: 13px;" <b>КОМПЛЕКТА ЗАЩИТНЫХ ЭКРАНОВ ДЛЯ АВТОМОБИЛЬНЫХ ОКОН “LAITOVO” НА
                        МАГНИТАХ</b>
                </li>
            </td>
            <td width="" style="padding-left: 15px">
                <img width="100" src="/img/manual-chico-magnet/logo-LAITOVO-small.gif">
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left">
                <a style="font-size: 13px;">Защитные экраны для автомобильных окон предназначены для защиты салона
                    транспортного средства, в том числе находящихся в нем пассажиров от солнца, насекомых, осколков
                    стекла и защиты частной жизни в транспортном средстве.
                </a>
            </td>
            <td width="%" style="padding-left: 15px">
                <img width="100" src="/img/manual-chico-magnet/2.png">
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: left; ">
                    <b style="">Особенности эксплуатации</b>
                </li>
                <li style="font-size: 13px; padding-left: 20px ">
                    запрещается стряхивать пепел от сигарет, выбрасывать мусор в окно с установленными защитными
                    экранами, это может привести к повреждению тканевого покрытия.
                </li>
                <li style="font-size: 13px; padding-left: 20px ">
                    запрещается оказывать физическое воздействие (ущемление, прокалывание и т. д.) тканевого покрытия,
                    это может привести к его повреждению.
                </li>
                <li style="font-size: 13px; padding-left: 20px ">
                    не рекомендуется эксплуатация передних боковых защитных экранов во время движения автомобиля.
                </li>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Уход</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a style="font-size: 13px">При загрязнении тканевого покрытия рекомендуется протереть влажной салфеткой
                    или губкой. При сильных загрязнениях возможно применение щадящих моющих средств или средств по уходу
                    за тканевыми поверхностями.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Комплектность</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: left; ">
                    <a style="font-size: 13px">В комплект поставки входит:</a>
                </li>
                <li style=" list-style-type: none;font-size: 13px;">
                    Защитные экраны, шт .......................................................2
                </li>
                <li style="list-style-type: none;font-size: 13px;  ">
                    Магнитные держатели, компл. .......................................1
                </li>
                <li style="list-style-type: none;font-size: 13px; ">
                    Инструкция по эксплуатации, экз .................................1
                </li>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Хранение</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a style="font-size: 13px">
                    Комплект защитных экранов CHIKO вне эксплуатации не требует особых условий хранения и осуществляется
                    в любом удобном месте.
                    Рекомендуется хранить в специальной фирменной сумке. Не следует хранить вблизи открытого пламени,
                    острых, режущих предметов и
                    в помещении с повышенной влажностью.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <b style="">Внимание</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px">
                <a style="font-size: 13px">
                    Установка производится при температуре выше +10 °С!
                    Перед установкой обязательно очистите поверхность оконного проема в местах установки креплений от
                    пыли или других загрязнений,
                    обезжирьте и просушите!
                    Для надежной фиксации, сильно прижмите крепление к обшивке на 5-10 секунд.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3" style=" text-align: left; font-size: 15px;">
                <b style="">Установка</b>
            </td>
        </tr>
    </table>

    <table width="820">
        <tr>
            <td style=" ">
                <img width="190px" src="/img/manual-chico-magnet/3.jpg">
            </td>
            <td style=" ">
                <? if (@$model->json('items')[0]['car']): ?>
                    <img src="<?= $scheme->file() ?>" width="190px">
                <? endif ?>
            </td>
        </tr>
        <tr>
            <td valign="top" style=" text-align: left; font-size: 15px; ">
                <a style="font-size: 13px">
                    <b>1</b>. Откройте дверь и опустите стекло.
                </a>
            </td>
            <td valign="top" style=" text-align: left; font-size: 15px; ">
                <a style="font-size: 13px">
                    <b>2</b>. Не снимая защитную пленку с клейкой ленты, разместите крепления на экране в соответствии
                    со схемой установки.
                </a>
            </td>
        </tr>
        <tr>
            <td width="15%" style=" ">
                <img width="190px" src="/img/manual-chico-magnet/5.jpg">
            </td>
            <td width="15%" style=" ">
                <img width="300px" src="/img/manual-chico-magnet/11.jpg">
            </td>

        </tr>
        <tr>
            <td valign="top" style=" text-align: left; font-size: 15px; ">
                <a style="font-size: 13px">
                    <b>3</b>. Установите экран с креплениями в оконный проем. Определите места установки креплений по
                    обшивке.
                </a>
            </td>
            <td valign="top" style=" text-align: left; font-size: 15px; ">
                <a style="font-size: 13px">
                    <b>4</b>. Поочередно установите крепления на вертикальной стойке, верхней части оконного проема и
                    у бокового зеркала, если это предусмотрено схемой установки.
                </a>
            </td>
        </tr>
        <tr>
            <td width="15%" style=" ">
                <img width="190px" src="/img/manual-chico-magnet/7.jpg">
            </td>
        </tr>
        <tr>
            <td valign="top" style=" text-align: left; font-size: 15px; ">
                <a style="font-size: 13px">
                    <b>5</b>. Установите экран на магнитные держатели.
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px;">
                <b style="">Гарантийные обязательства</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" text-align: left; font-size: 15px">
                <a style="font-size: 13px">
                    При надлежащем соблюдении покупателем правил эксплуатации, продавец устанавливает срок гарантии на
                    изделие
                    – 1 год с даты получения покупателем товара.
                </a>
            </td>
        </tr>
    </table>

    <table width="820" style="padding-top: 10px">
        <tr>
            <td width="20%"  style=" text-align: left; font-size: 14px;">
                <b style="">Контакты:
                    +7 499 703 06-85
                    E-mail: info@laitovo.ru </b>
            </td>
            <td width="60%" style="text-align: center ">
                <img width="50px" src="/img/manual-chico-magnet/8.jpg">
                <li style=" list-style-type: none;font-size: 11px; text-align: center">
                    <b>www.laitovo.ru</b>
                </li>

            </td>
            <td width="20%" style=" text-align: left; font-size: 15px">
                <li style="list-style-type: none; text-align: center; ">
                    <a style="font-size: 13px"> ОТК </a>
                </li>
                <li style=" list-style-type: none;font-size: 10px; text-align: center">
                    _________________________

                </li>
                <li style="list-style-type: none;font-size: 13px; text-align: center  ">
                    дата проверки
                </li>
            </td>
        </tr>
    </table>


<? endif ?>