<?php

/*  @var $type string */
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

switch ($type) {
    case 'WS':
        $color = 'белый';
        $colorLabel = 'White';
        $article = 'OT-2061-12-1';
        $code = 'OT2061121';
        $size = 'S 120х80 см';
        break;
    case 'WM':
        $color = 'белый';
        $colorLabel = 'White';
        $article = 'OT-2061-12-2';
        $code = 'OT2061122';
        $size = 'M 140х90 см';
        break;
    case 'WL':
        $color = 'белый';
        $colorLabel = 'White';
        $article = 'OT-2061-12-3';
        $code = 'OT2061123';
        $size = 'L 160х90см';
        break;
    case 'S':
        $color = 'черный';
        $colorLabel = 'Black';
        $article = 'OT-2061-11-1';
        $code = 'OT2061111';
        $size = 'S 120х80 см';
        break;
    case 'M':
        $color = 'черный';
        $colorLabel = 'Black';
        $article = 'OT-2061-11-2';
        $code = 'OT2061112';
        $size = 'M 140х90 см';
        break;
    default:
        $color = 'черный';
        $colorLabel = 'Black';
        $article = 'OT-2061-11-3';
        $code = 'OT2061113';
        $size = 'L 160х90см';
}
?>

<style>
    .bold {
        font-size: 12px!important;
    }

    .container {
        max-width: 7cm;
        font-size: 11px;
    }

    .container p {
        margin: 2px 0;
    }
</style>


<div class="container">

    <p class="bold">Утеплитель двигателя Laitovo <?=$colorLabel?>, размер <?=$size?>, <?=$color?> .</p>
    <p class="bold"> Артикул: <?=$article?></p>

    <p>Предназначен для теплоизоляции автомобильного двигателя в холодное время года. Срок годности не ограничен.</p>

    <p>
        Изготовитель:
        ИП Григорьев Д.Л.
        156005, Россия, Костромская область,
        Кострома, Войкова, 40, 112
        тел.:<br> +7 (499) 703-06-85
        e-mail: info@laitovo.ru
        www.laitovo.ru
    </p>
    <p>
        <?= $generator->getBarcode($code, $generator::TYPE_CODE_128, 1) ?>
        <span><?= $code?></span><br>
    </p>
</div>


