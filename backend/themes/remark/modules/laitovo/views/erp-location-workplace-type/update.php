<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplaceType */

$this->title = 'Update Erp Location Workplace Type: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Erp Location Workplace Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$this->render('../menu');

?>
<div class="erp-location-workplace-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
