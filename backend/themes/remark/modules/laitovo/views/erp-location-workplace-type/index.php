<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\models\ErpLocationWorkplaceTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Erp Location Workplace Types';
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Erp Location Workplace Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'location.name',
            'created_at:datetime',
            'updated_at:datetime',
            'author_id',
            // 'updater_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
