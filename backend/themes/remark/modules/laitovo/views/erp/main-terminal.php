<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use backend\modules\laitovo\models\ErpLocation;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\laitovo\models\ErpNaryad;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Выдача нарядов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('

    setInterval(function(){ $(".reloadterminal").click(); }, 600*1000);

', \yii\web\View::POS_END);

if (($print = Yii::$app->session->getFlash('ERP_START_NARYADS')) != null) {
    Yii::$app->view->registerJs('
        printJS({printable:"' . Url::to(['print', 'id' => implode(',', $print)]) . '", type:"pdf", showModal:true, modalMessage: "Печать..."});
    ');
}

?>
 <?php  ActiveForm::begin([
        'method' => 'get',
    ]); ?>

   <a href="<?=Url::to('main-terminal')?>" class="pull-right btn btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="<?=Yii::t('app', 'Сбросить')?>"><i class="icon wb-refresh" aria-hidden="true"></i></a> 
    <div class="input-search input-search-dark pull-right"><i class="input-search-icon wb-search" aria-hidden="true"></i><input type="text" class="form-control" value="<?=Yii::$app->request->get('search')?>" name="search" placeholder="<?=Yii::t('app', 'Поиск')?>"></div>

<?php ActiveForm::end(); ?>

    
    <?php $form = ActiveForm::begin(); ?>

<?php Pjax::begin(['timeout' => 3000000]); ?>

<h4><?= $model->user->name ?></h4>
<?= Html::a("Обновить", ['main-terminal'], ['class' => 'hidden reloadterminal']) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $naryads,
    // 'show'=>['id','order_id','location_id','status'],
    'rowOptions' => function ($data) {
        if ($data->sort == 1) {
            return ['class' => 'danger'];
        } elseif ($data->sort == 2) {
            return ['class' => 'warning'];
        }
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'order_id',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::tag('div',
                    Html::checkbox(null, false, [
                        'onchange' => "$('.ordern" . $data->order_id . "').prop('checked', $(this).is(':checked'));",
                    ]) .
                    Html::tag('label',
                        ($data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view', 'id' => $data->order->id]) : null)
                    ),
                    [
                        'class' => 'checkbox-custom checkbox-primary text-left',
                    ]);
            },
        ],

        [
            'attribute'=>'id',
            'format'=>'raw',
            'value'=>function ($data) {
                $naryads=ErpNaryad::find()->where(['and',
                    ['order_id'=>$data->order_id],
                    ['or',['location_id'=>null],['location_id' => 7]],
                    ['or',['status'=>null],['status'=>''],['status' => ErpNaryad::STATUS_IN_WORK]]
                ])->all();
                $return = '';
                if ($naryads) {
                    foreach ($naryads as $row) {
                        $return .= Html::tag('div',
                            Html::checkbox('ErpMainTerminal[workOrders][]', $checked = false, ['value' => $row->id, 'class' => 'ordern' . $data->order_id]) .
                            Html::tag('label',
                                ($row->name ? Html::a(Html::encode($row->name), ['erp-naryad/view', 'id' => $row->id]) . ' - ' . @$row->locationstart->name . ' - ' . $row->article : null)
                            ) . Html::tag('span',$row->getTypeClipsStr()),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                    }
                }

                return $return;
            },
        ],
        // [
        //     'attribute'=>'location_id',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view','id'=>$data->location->id]) : null;
        //     },
        // ],
        // [
        //     'attribute'=>'scheme_id',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
        //     },
        // ],
        // 'status',
        'created_at:datetime',
        // [
        //     'attribute'=>'author_id',
        //     'value'=>function ($data) {
        //         return $data->author ? $data->author->name : null;
        //     },
        // ],
    ],
]); ?>
<?= Html::submitButton(Yii::t('app', 'Выдать'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
<?php Pjax::end(); ?>

<?php ActiveForm::end(); ?>
