<?php
/**
 * @var $carName string
 * @var $window string
 */
?>

<table class="invoice_items" width="270" cellpadding="2" cellspacing="7"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td height="60">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="border-left: none;border-right: none; font-size: 14px;font-family: 'Roboto', sans-serif;font-size: 12pt;line-height: 1em">
            <b><?= $window ?> | <?= $carName?></b>
        </td>
    </tr>
    </tbody>

</table>

