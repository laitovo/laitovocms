<?php
use backend\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpStorageInfo;

/** @var $prodLiteralStat array **/

$this->title = Yii::t('app', 'Производство');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

Yii::$app->view->registerJs('
    
        $(function() {
            /*ищю чекбокс. Для каждого который я нашел*/
            setInterval(function(){ $(".reloadpage").click(); }, 60000);

            /*если в хранилище есть свойство как имя найденного чекбокса и оно равно истина*/
            if (localStorage.getItem($("input[name=\'reload-checkbox\']").attr("name")) == "true") {
                $("input[name=\'reload-checkbox\']").attr("checked", true);
                $(".btn-outline").addClass("reloadpage", $(this).attr("checked") );
            }else{
                $("input[name=\'reload-checkbox\']").attr("checked", false);
                $(".btn-outline").removeClass("reloadpage", $(this).attr("checked"));
            }
          
            $("input[name=\'reload-checkbox\']").on("click", function() {
                var isChecked = ($(this).is(":checked")) ? true : false;
                localStorage.setItem($(this).attr("name"), isChecked);
                if (isChecked){
                    $(".btn-outline").addClass("reloadpage", $(this).attr("checked") );
                }else{
                    $(".btn-outline").removeClass("reloadpage", $(this).attr("checked"));
                }
            });
        });

    
', \yii\web\View::POS_END);

//Права доступа
$access = true; //in_array(Yii::$app->user->getId(),[2,14,29,21]);

?>
<?php Pjax::begin(['timeout' => 5000]); ?>

<?php $form = ActiveForm::begin(); ?>
<div class="clearfix">
    <div class="col-xs-4">

        <?= $form->field($model, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
            //'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'dd.MM.yyyy',
        ]) ?>


    </div>
    <div class="col-xs-4">

        <?= $form->field($model, 'dateTo')->widget(\yii\jui\DatePicker::classname(), [
            //'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'dd.MM.yyyy',
        ]) ?>

    </div>

    <div class="form-group col-xs-4">
        <?= Html::checkbox('reload-checkbox', $checked = true, ['class' => 'reload-checkbox ', 'label' => 'Автообновление']) ?>
        <br>
        <?= Html::submitButton(Yii::t('app', 'Обновить'), ['class' => 'btn reloadpage btn-outline btn-round btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?
$dFrom = Yii::$app->formatter->asTimestamp($model->dateFrom);
$dTo = Yii::$app->formatter->asTimestamp($model->dateTo);
?>
    <? if (isset($arNaryadsTimeout) && count($arNaryadsTimeout) > 0) {
        $classLog = 'danger';
    } else {
        $classLog = 'info';
    } ?>
    <h3>Количество нарядов в логистике</h3>
    <div style="overflow: overlay">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Участок</th>
                <th>Поступили в логистику</th>
                <th>Ушли в производство</th>
                <th>Будут отгружены со склада</th>
                <th>Процент производства*</th>
                <th>Процент склада**</th>
            </tr>
            </thead>
            <tbody>
            <tr class="<?= $classLog ?>" data-key="1">
                <td>#</td>
                <td><a href="/laitovo/erp-location/view?id=1"><?= $logisticsData->title ?></a></td>
                <td><?= $logisticsData->all ?></td>
                <td><?= $logisticsData->prod ?></td>
                <td><?= $logisticsData->storage ?></td>
                <td><?= $logisticsData->percentProd ?>%</td>
                <td><?= $logisticsData->percentStorage ?>%</td>
            </tbody>
        </table>
    </div>
    <p>* в экране логистики графа <span style="font-weight: bold;font-size: 1.2em">Процент производства</span> высчитывается отношение производимой продукции ко всей</p>
    <p>** в экране логистики графа <span style="font-weight: bold;font-size: 1.2em">Процент склада</span> высчитывается отношение складской продукции ко всей</p>
<?// endif ?>

<!-- <?if (ErpNaryad::queryNoScheme()->count()):?>
    <h3>ЕСТЬ НАРЯДЫ БЕЗ СХЕМЫ ПРОИЗВОДСТВА</h3>
    <div>
    <?foreach(ErpNaryad::queryNoScheme()->all() as $naryad): ?>
        <a href="<?=$naryad->updateUrl?>" target="_blank"><?$naryad->name?></a> 
    <?endforeach;?> 
    </div>
<?endif;?> -->

<h3>Количество нарядов на участках</h3>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $plantcontrol,
    'showFooter'=>TRUE,
    'summary' => false,
    'show' => ['Участки','На участке нарядов','На участке людей','Не выдано','Полученные на участок','Просрочены*','№1*','№2*','№3*','Всего'], 
     'rowOptions' => function ($data) {
        $itogo = $data->naryadTimeout;
        if ($itogo) {
            return ['class' => 'danger'];
        } else {
            return ['class' => 'success'];
        }
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [

            'attribute' => 'Участки',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-location/view', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute'=>'На участке нарядов',
            'format'=>'raw',
            'label' => 'Нарядов',
            'value'=>function ($data) use ($access){
                /**
                 * @var $data ErpLocation
                 */
                $wholeWork = $data->getNaryadsWholeWork()->count();
                $forStorage = $data->getNaryadsForStorage()->count();
                $inPause = $data->getNaryadsInPause()->count();
                $autoSpeed = $data->getNaryadsWithAutoSpeed()->count();
                $fromSump = $data->getNaryadsFromSump()->count();
                $moveAct = $data->getNaryadsMoveAct()->count();
                $oneDay = $data->getCountNaryadsMoreOneDayInWork();
                $completionFlag = $data->getNaryadsWithCompletionFlag()->count();

                $allPriorities = $data->getNaryadsByPriority();
                $rework = $allPriorities[1] ?? 0;
                $express = $allPriorities[2] ?? 0;
                $bigOrder = $allPriorities[3] ?? 0;
                $main = $allPriorities[4] ?? 0;
                $completion = $allPriorities[5] ?? 0;
                $standard = $allPriorities[6] ?? 0;
                $low = $allPriorities[7] ?? 0;

                $autoFilter = $data->getAutoFilterWorkOrders()->count();
                $laitBag = $data->getLBWorkOrders()->count();
                ###########################################################
                $result = '';
                if ($wholeWork) $result .= Html::a($wholeWork, ['erp-location/view-on-location','id'=>$data->id],['target' => '_blank','data-pjax' => 0, 'title' => 'Всего нарядов']);
                if ($autoSpeed) $result .= Html::a("[ ".$autoSpeed." ]", ['erp-location/view-auto-speed','id'=>$data->id],['style' => 'color:white;background-color: green','target' => '_blank','data-pjax' => 0, 'title' => 'Автоматически ускоренные']);
                if ($forStorage) $result .= Html::a("[ ".$forStorage." ]", ['erp-location/view-for-storage','id'=>$data->id],['style' => "color:white;background-color: black",'target' => '_blank','data-pjax' => 0, 'title' => 'Пополнение склада']);
                if ($inPause) $result .= Html::a("[ ".$inPause." ]", ['erp-location/view-in-pause','id'=>$data->id],['style' => 'color:yellow','target' => '_blank','data-pjax' => 0, 'title' => 'На паузе']);
                if ($completionFlag) $result .= Html::a("[ ".$completionFlag." ]", ['erp-location/view-completion-flag','id'=>$data->id],['style' => "color:white;background-color: #0b96e5",'target' => '_blank','data-pjax' => 0, 'title' => 'Комплектация']);
                if ($fromSump) $result .= Html::a("[ ".$fromSump." ]", ['erp-location/view-from-sump','id'=>$data->id],['style' => "color:aqua;background-color: brown",'target' => '_blank','data-pjax' => 0, 'title' => 'С отстойника']);
                if ($moveAct) $result .= Html::a("[ ".$moveAct." ]", ['erp-location/view-move-act','id'=>$data->id],['style' => 'color:red;background-color:yellow','target' => '_blank','data-pjax' => 0, 'title' => 'По акту приема-передачи']);
                if ($oneDay) $result .= Html::a("[ ".$oneDay." ]", ['erp-location/view-one-day','id'=>$data->id],['style' => "color:white;background-color: blue",'target' => '_blank','data-pjax' => 0, 'title' => 'Более одного дня']);
                $result .= ' ';
                if ($rework) $result .= Html::a("[ ".$rework." ]", ['erp-location/view-rework','id'=>$data->id],['style' => "color:white;background-color: blueviolet",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Переделка']);
                if ($express && $access) $result .= Html::a("[ ".$express." ]", ['erp-location/view-express-priority','id'=>$data->id],['style' => 'color:green','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - TopSpeed']);
                if ($bigOrder) $result .= Html::a("[ ".$bigOrder." ]", ['erp-location/view-big-order-priority','id'=>$data->id],['style' => "color:blue;background-color: #EAE6CA" ,'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Большой заказ']);
                if ($main) $result .= Html::a("[ ".$main." ]", ['erp-location/view-main-priority','id'=>$data->id],['style' => 'color:blue','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Срочный']);
                if ($completion) $result .= Html::a("[ ".$completion." ]", ['erp-location/view-completion-priority','id'=>$data->id],['style' => "color:white;background-color: #0b96e5",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Комплектация']);
                if ($standard) $result .= Html::a("[ ".$standard." ]", ['erp-location/view-standard-priority','id'=>$data->id],['style' => "color:white",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Обычный']);
                if ($low) $result .= Html::a("[ ".$low." ]", ['erp-location/view-low-priority','id'=>$data->id],['style' => 'color:black','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Низкий']);
                $result .= ' ';
                if ($autoFilter) $result .= Html::a(" [ ".$autoFilter." АФ ]", ['erp-location/view-auto-filter','id'=>$data->id],['style' => 'color:white','target' => '_blank','data-pjax' => 0, 'title' => 'Наряды для АФ']);
                if ($laitBag) $result .= Html::a(" [ ".$laitBag." LB ]", ['erp-location/view-laitbag','id'=>$data->id],['style' => 'color:white','target' => '_blank','data-pjax' => 0, 'title' => 'Наряды LAITBAG']);
                return $result ? $result : 0;
            },
            'footer' => ErpLocation::getNaryadsWholeWorkSum([1,2,3,4,6]),
        ],
        [
            'attribute'=>'На участке людей',
            'format'=>'raw',
            'label' => 'Людей',
            'value'=>function ($data) {
                $result = $data->recieveActiveUsersCount();
                return $result ? Html::a($result, ['erp-location-workplace/monitor','location_id'=>$data->id],['target' => '_blank','data-pjax' => 0]) : 0;
            },
            'footer' => ErpLocation::recieveActiveUsersCountSum([1,2,3,4,6]),
        ],
        [
            'attribute'=>'Полученные на участок',
            'format'=>'html',
            'label' => 'Пришло из облака',
            'value'=>function ($data) use ($model) {
                $result = $data->recieveCurentStartCount($model->dateFrom,$model->dateTo);
                return $result ? $result : 0;
            },
            'headerOptions' => [
                'style'=>'white-space:normal;'
            ],
        ],
        [
            'attribute'=>'Не выдано',
            'label'=>'Не выдано**',
            'format'=>'raw',
            'value'=>function ($data) use ($access) {
                //Получаем общее количество нарядов, которые должны пройти через участок
                $wholeWork = $data->getNaryadsToStartWholeWork()->count(); 
                $fromCloud = $data->getNaryadsToStartFromCloud()->count();
                $forStorage = $data->getNaryadsToStartForStorage()->count();
                $inDelay = $data->getNaryadsToStartInDelay()->count();
                $inPause = $data->getNaryadsToStartInPause()->count();
                $autoSpeed = $data->getNaryadsToStartWithAutoSpeed()->count();
                $fromSump = $data->getNaryadsToStartFromSump()->count();
                $moveAct = $data->getNaryadsToStartMoveAct()->count();
                $oneDay = $data->getCountNaryadsMoreOneDayToStart();
                $completionFlag = $data->getNaryadsToStartWithCompletionFlag()->count();


                $allPriorities = $data->getNaryadsToStartByPriority();
                $rework = $allPriorities[1] ?? 0;
                $express = $allPriorities[2] ?? 0;
                $bigOrder = $allPriorities[3] ?? 0;
                $main = $allPriorities[4] ?? 0;
                $completion = $allPriorities[5] ?? 0;
                $standard = $allPriorities[6] ?? 0;
                $low = $allPriorities[7] ?? 0;

                $autoFilter = $data->getAutoFilterWorkOrdersToStart()->count();
                $laitBag = $data->getLBWorkOrdersToStart()->count();
                #############################################################
                $result = '';
                if ($wholeWork) $result .= Html::a($wholeWork, ['erp-location/view-not-given','id'=>$data->id],['target' => '_blank','data-pjax' => 0, 'title' => 'Всего нарядов']);
                if ($fromCloud) $result .= Html::a("( ".$fromCloud." )", ['erp-location/view-from-cloud','id'=>$data->id],['target' => '_blank','data-pjax' => 0, 'title' => 'Из облака']);
                if ($autoSpeed) $result .= Html::a("[ ".$autoSpeed." ]", ['erp-location/view-to-start-auto-speed','id'=>$data->id],['style' => 'color:white;background-color: green','target' => '_blank','data-pjax' => 0, 'title' => 'Автоматически ускоренные']);
                if ($forStorage) $result .= Html::a("[ ".$forStorage." ]", ['erp-location/view-to-start-for-storage','id'=>$data->id],['style' => "color:white;background-color: black",'target' => '_blank','data-pjax' => 0, 'title' => 'Пополнение склада']);
                if ($inDelay) $result .= Html::a("[ ".$inDelay." ]", ['erp-location/view-in-delay','id'=>$data->id],['style' => 'color:yellow;background-color:black','target' => '_blank','data-pjax' => 0, 'title' => 'Не могут быть выданы']);
                if ($inPause) $result .= Html::a("[ ".$inPause." ]", ['erp-location/view-to-start-in-pause','id'=>$data->id],['style' => 'color:yellow','target' => '_blank','data-pjax' => 0, 'title' => 'На паузе']);
                if ($completionFlag) $result .= Html::a("[ ".$completionFlag." ]", ['erp-location/view-to-start-completion-flag','id'=>$data->id],['style' => "color:white;background-color: #0b96e5",'target' => '_blank','data-pjax' => 0, 'title' => 'Комплектация']);
                if ($fromSump) $result .= Html::a("[ ".$fromSump." ]", ['erp-location/view-to-start-from-sump','id'=>$data->id],['style' => "color:aqua;background-color: brown",'target' => '_blank','data-pjax' => 0, 'title' => 'С отстойника']);
                if ($moveAct) $result .= Html::a("[ ".$moveAct." ]", ['erp-location/view-to-start-move-act','id'=>$data->id],['style' => 'color:red;background-color:yellow','target' => '_blank','data-pjax' => 0, 'title' => 'По акту приема-передачи']);
                if ($oneDay) $result .= Html::a("[ ".$oneDay." ]", ['erp-location/view-to-start-one-day','id'=>$data->id],['style' => "color:white;background-color: blue",'target' => '_blank','data-pjax' => 0, 'title' => 'Более одного дня']);
                $result .= ' ';
                if ($rework) $result .= Html::a("[ ".$rework." ]", ['erp-location/view-to-start-rework','id'=>$data->id],['style' => "color:white;background-color: blueviolet",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Переделка']);
                if ($express && $access) $result .= Html::a("[ ".$express." ]", ['erp-location/view-to-start-express-priority','id'=>$data->id],['style' => 'color:green','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - TopSpeed']);
                if ($bigOrder) $result .= Html::a("[ ".$bigOrder." ]", ['erp-location/view-to-start-big-order-priority','id'=>$data->id],['style' => "color:blue;background-color: #EAE6CA" ,'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Большой заказ']);
                if ($main) $result .= Html::a("[ ".$main." ]", ['erp-location/view-to-start-main-priority','id'=>$data->id],['style' => 'color:blue','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Срочный']);
                if ($completion) $result .= Html::a("[ ".$completion." ]", ['erp-location/view-to-start-completion-priority','id'=>$data->id],['style' => "color:white;background-color: #0b96e5",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Комплектация']);
                if ($standard) $result .= Html::a("[ ".$standard." ]", ['erp-location/view-to-start-standard-priority','id'=>$data->id],['style' => "color:white",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Обычный']);
                if ($low) $result .= Html::a("[ ".$low." ]", ['erp-location/view-to-start-low-priority','id'=>$data->id],['style' => 'color:black','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Низкий']);
                $result .= ' ';
                if ($autoFilter) $result .= Html::a(" [ ".$autoFilter." АФ ]", ['erp-location/view-auto-filter-to-start','id'=>$data->id],['style' => 'color:white','target' => '_blank','data-pjax' => 0, 'title' => 'Наряды для АФ']);
                if ($laitBag) $result .= Html::a(" [ ".$laitBag." LB ]", ['erp-location/view-laitbag-to-start','id'=>$data->id],['style' => 'color:white','target' => '_blank','data-pjax' => 0, 'title' => 'Наряды LAITBAG']);
                return $result ? $result : 0;
            },
            'headerOptions' => [
                'style'=>'white-space:normal;'
            ],
        ],
//        [
//            'attribute' => 'Просрочены*',
//            'format' => 'html',
//            'value' => function ($data) {
//                if ($intCount = count($data->naryadTimeout)) {
//                    return Html::a($intCount, ['erp-location/view-overdue', 'id' => $data->id]);
//                } else {
//                    return $intCount;
//                }
//            },
//        ],
//        [
//            'attribute' => '№1*',
//            'format' => 'raw',
//            'value' => function ($data) use ($access) {
//                if (isset($data->naryadTimeout[0])) {
//                    $naryad = $data->naryadTimeout[0];
//
//                    $arDelay = [];
//                    $flag = false;
//                    $inDelay = $data->getNaryadsToStartInDelay()->all();
//                    foreach ($inDelay as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad['id'])
//                        $flag = true;
//                    }
//                    $inPause = $data->getNaryadsInPause()->all();
//                    foreach ($inPause as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad->id)
//                        $flag = true;
//                    }
//                    $link = '';
//                    if ($flag)
//                        $link .='<span style="color:yellow">';
//
//                    $link .= $naryad->number . '<br>' . '<span class="small">' . Yii::$app->formatter->asDateTime($naryad->created_at, 'short') . '</span></br>';
//                    //Только для руководителя
//                    if ($access) {
//                        $link .= '<span class="small">' . (isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at, 'short') : Yii::$app->formatter->asDateTime($naryad->created_at, 'short')) . '</span>';
//                    }
//                    if ($flag)
//                        $link .='</span>';
//                    return Html::a($link, ['erp-naryad/view', 'id' => $naryad->id], ['target' => '_blank', 'data-pjax' => 0]);
//                } else {
//                    return "";
//                }
//
//            },
//        ],
//        [
//            'attribute' => '№2*',
//            'format' => 'raw',
//            'value' => function ($data) use ($access) {
//                if (isset($data->naryadTimeout[1])) {
//                    $naryad = $data->naryadTimeout[1];
//
//                    $arDelay = [];
//                    $flag = false;
//                    $inDelay = $data->getNaryadsToStartInDelay()->all();
//                    foreach ($inDelay as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad['id'])
//                        $flag = true;
//                    }
//                    $inPause = $data->getNaryadsInPause()->all();
//                    foreach ($inPause as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad->id)
//                        $flag = true;
//                    }
//                    $link = '';
//                    if ($flag)
//                        $link .='<span style="color:yellow">';
//
//                    $link .= $naryad->number . '<br>' . '<span class="small">' . Yii::$app->formatter->asDateTime($naryad->created_at, 'short') . '</span></br>';
//                    //Только для руководителя
//                    if ($access) {
//                        $link .= '<span class="small">' . (isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at, 'short') : Yii::$app->formatter->asDateTime($naryad->created_at, 'short')) . '</span>';
//                    }
//                    if ($flag)
//                        $link .='</span>';
//                    return Html::a($link, ['erp-naryad/view', 'id' => $naryad->id], ['target' => '_blank', 'data-pjax' => 0]);
//                } else {
//                    return "";
//                }
//            },
//        ],
//        [
//            'attribute' => '№3*',
//            'format' => 'raw',
//            'value' => function ($data) use ($access){
//                if (isset($data->naryadTimeout[2])) {
//                    $naryad = $data->naryadTimeout[2];
//
//                    $arDelay = [];
//                    $flag = false;
//                    $inDelay = $data->getNaryadsToStartInDelay()->all();
//                    foreach ($inDelay as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad['id'])
//                        $flag = true;
//                    }
//                    $inPause = $data->getNaryadsInPause()->all();
//                    foreach ($inPause as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad->id)
//                        $flag = true;
//                    }
//                    $link = '';
//                    if ($flag)
//                        $link .='<span style="color:yellow">';
//
//                    $link .= $naryad->number . '<br>' . '<span class="small">' . Yii::$app->formatter->asDateTime($naryad->created_at, 'short') . '</span></br>';
//                    //Только для руководителя
//                    if ($access) {
//                        $link .= '<span class="small">' . (isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at, 'short') : Yii::$app->formatter->asDateTime($naryad->created_at, 'short')) . '</span>';
//                    }
//                    if ($flag)
//                        $link .='</span>';
//                    return Html::a($link, ['erp-naryad/view', 'id' => $naryad->id], ['target' => '_blank', 'data-pjax' => 0]);
//                } else {
//                    return "";
//                }
//            },
//        ],
        // [
        //     'attribute' => '№4*',
        //     'format' => 'raw',
        //     'value' => function ($data) {
        //         if (isset($data->naryadTimeout[3])) {
        //             $naryad = $data->naryadTimeout[3];

        //             $arDelay = [];
        //             $flag = false;
        //             $inDelay = $data->getNaryadsToStartInDelay()->all();
        //             foreach ($inDelay as $delayNaryad) {
        //                if ($naryad->id == $delayNaryad['id'])
        //                 $flag = true;
        //             }
        //             $inPause = $data->getNaryadsInPause()->all();
        //             foreach ($inPause as $delayNaryad) {
        //                if ($naryad->id == $delayNaryad->id)
        //                 $flag = true;
        //             }
        //             $link = '';
        //             if ($flag)
        //                 $link .='<span style="color:yellow">';

        //             $link .= $naryad->number . '<br>' . '<span class="small">' . Yii::$app->formatter->asDateTime($naryad->created_at, 'short') . '</span></br>';
        //             //Только для руководителя или технгического директора
        //             if (Yii::$app->user->getId() == 2 || Yii::$app->user->getId() == 14 || Yii::$app->user->getId() == 29) {
        //                 $link .= '<span class="small">' . (isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at, 'short') : Yii::$app->formatter->asDateTime($naryad->created_at, 'short')) . '</span>';
        //             }
        //             if ($flag)
        //                 $link .='</span>';
        //             return Html::a($link, ['erp-naryad/view', 'id' => $naryad->id], ['target' => '_blank', 'data-pjax' => 0]);
        //         } else {
        //             return "";
        //         }
        //     },
        // ],

        /*[
            'attribute'=>'№5*       ',
            'format'=>'raw',
            'value'=>function ($data) {
                if (isset($data->naryadTimeout[4])) {
                    $naryad=$data->naryadTimeout[4];

                    $arDelay = [];
                    $flag = false;
                    $inDelay = $data->getNaryadsToStartInDelay()->all();
                    foreach ($inDelay as $delayNaryad) {
                       if ($naryad->id == $delayNaryad['id'])
                        $flag = true;
                    }
                    $inPause = $data->getNaryadsInPause()->all();
                    foreach ($inPause as $delayNaryad) {
                       if ($naryad->id == $delayNaryad->id)
                        $flag = true;
                    }
                    $link = '';
                    if ($flag)
                        $link .='<span style="color:yellow">';

                    $link .= $naryad->number.'<br>'.'<span class="small">'.Yii::$app->formatter->asDateTime($naryad->created_at,'short').'</span></br>';
                    //Только для руководителя
                    if (Yii::$app->user->getId() == 2 || Yii::$app->user->getId() == 14) {
                    $link .= '<span class="small">'.(isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at,'short') :Yii::$app->formatter->asDateTime($naryad->created_at,'short')).'</span>';  
                    }
                    if ($flag)
                        $link .='</span>';
                    return Html::a($link, ['erp-naryad/view','id'=>$naryad->id],['target'=>'_blank','data-pjax'=>0]);
                } else {
                    return "";
                }
            },
        ],*/
//        [
//            'attribute'=>'Готовы к получению',
//            'format'=>'html',
//            'label' => 'Пришло на участок',
//            'value'=>function ($data) use ($model) {
//                $result = $data->workOrdersWasStart($model->dateFrom,$model->dateTo);
//                return $result ? $result : 0;
//            },
//            'headerOptions' => [
//                'style'=>'white-space:normal;'
//            ],
//        ],
        [
            'attribute' => 'Всего',
            'label' => 'Сданы',
            'format' => 'raw',
            'value' => function ($data) use ($dFrom, $dTo) {
                $result = '';
                if ($intCount = $data->getNaryadsCreated($dFrom, $dTo)) {
                    $result .= Html::a($intCount, ['erp-location/view-throw-location-by-date',
                        'location' => $data->id,
                        'dFrom' => $dFrom,
                        'dTo' => $dTo],['target' => '_blank','data-pjax' => 0]);
                } else {
                    $result .= $intCount;
                }

                $result .= ' [';

                if ($intCount = $data->getNaryadsCreatedAF($dFrom, $dTo)) {
                    $result .= Html::a($intCount . 'АФ', ['erp-location/view-throw-location-by-date',
                        'location' => $data->id,
                        'dFrom' => $dFrom,
                        'dTo' => $dTo,
                        'af' => true,
                    ],['target' => '_blank','data-pjax' => 0]);
                } else {
                    $result .= $intCount . 'АФ';
                }

                $result .= ']';

                return $result;
            },
        ],

    ]

]) ?>

<? $classProd = $prodLiteralStat['status']; ?>
<h3>Информация о производственных литерах</h3>
<div style="overflow: overlay">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Всего литер</th>
            <th>Заполненныx</th>
            <th>Пустыx</th>
            <th>Вместимость нарядов</th>
            <th>Занято нарядами в производстве</th>
            <th>Свободных мест</th>
            <th>Коэффициент заполненности*</th>
        </tr>
        </thead>
        <tbody>
        <tr class="<?= $classProd ?>" data-key="1">
            <td>#</td>
            <td><?= $prodLiteralStat['countLiterals']?></td>
            <td><?= $prodLiteralStat['countBusyLiterals']?></td>
            <td><?= $prodLiteralStat['countEmptyLiterals']?></td>
            <td><?= $prodLiteralStat['countPlaceForWorkOrders']?></td>
            <td><?= $prodLiteralStat['countWorkOrdersNeededInLiterals']?></td>
            <td><?= $prodLiteralStat['freeWorkOrdersPlace']?></td>
            <td><?= $prodLiteralStat['occupancyRate']?>%</td>

        </tbody>
    </table>
</div>
<p>* <span style="font-weight: bold;font-size: 1.2em">Коэффициент заполненности</span> высчитывается как отношение нарядов в производстве к вместимости нарядов</p>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $plantcontrol2,
    'showFooter'=>TRUE,
    'summary' => false,
    'show' => ['Участки','На участке нарядов','На участке людей','Не выдано','Полученные на участок','Просрочены*','№1*','№2*','№3*','Всего'],
    'rowOptions' => function ($data) {
        $itogo = $data->naryadTimeout;
        if ($itogo) {
            return ['class' => 'danger'];
        } else {
            return ['class' => 'success'];
        }
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [

            'attribute' => 'Участки',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-location/view', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute'=>'На участке нарядов',
            'format'=>'raw',
            'label' => 'Нарядов',
            'value'=>function ($data) use ($access){
                /**
                 * @var $data ErpLocation
                 */
                $wholeWork = $data->getNaryadsWholeWork()->count();
                $forStorage = $data->getNaryadsForStorage()->count();
                $inPause = $data->getNaryadsInPause()->count();
                $autoSpeed = $data->getNaryadsWithAutoSpeed()->count();
                $fromSump = $data->getNaryadsFromSump()->count();
                $moveAct = $data->getNaryadsMoveAct()->count();
                $oneDay = $data->getCountNaryadsMoreOneDayInWork();
                $completionFlag = $data->getNaryadsWithCompletionFlag()->count();

                $allPriorities = $data->getNaryadsByPriority();
                $rework = $allPriorities[1] ?? 0;
                $express = $allPriorities[2] ?? 0;
                $bigOrder = $allPriorities[3] ?? 0;
                $main = $allPriorities[4] ?? 0;
                $completion = $allPriorities[5] ?? 0;
                $standard = $allPriorities[6] ?? 0;
                $low = $allPriorities[7] ?? 0;

                $autoFilter = $data->getAutoFilterWorkOrders()->count();
                $laitBag = $data->getLBWorkOrders()->count();
                ###########################################################
                $result = '';
                if ($wholeWork) $result .= Html::a($wholeWork, ['erp-location/view-on-location','id'=>$data->id],['target' => '_blank','data-pjax' => 0, 'title' => 'Всего нарядов']);
                if ($autoSpeed) $result .= Html::a("[ ".$autoSpeed." ]", ['erp-location/view-auto-speed','id'=>$data->id],['style' => 'color:white;background-color: green','target' => '_blank','data-pjax' => 0, 'title' => 'Автоматически ускоренные']);
                if ($forStorage) $result .= Html::a("[ ".$forStorage." ]", ['erp-location/view-for-storage','id'=>$data->id],['style' => "color:white;background-color: black",'target' => '_blank','data-pjax' => 0, 'title' => 'Пополнение склада']);
                if ($inPause) $result .= Html::a("[ ".$inPause." ]", ['erp-location/view-in-pause','id'=>$data->id],['style' => 'color:yellow','target' => '_blank','data-pjax' => 0, 'title' => 'На паузе']);
                if ($completionFlag) $result .= Html::a("[ ".$completionFlag." ]", ['erp-location/view-completion-flag','id'=>$data->id],['style' => "color:white;background-color: #0b96e5",'target' => '_blank','data-pjax' => 0, 'title' => 'Комплектация']);
                if ($fromSump) $result .= Html::a("[ ".$fromSump." ]", ['erp-location/view-from-sump','id'=>$data->id],['style' => "color:aqua;background-color: brown",'target' => '_blank','data-pjax' => 0, 'title' => 'С отстойника']);
                if ($moveAct) $result .= Html::a("[ ".$moveAct." ]", ['erp-location/view-move-act','id'=>$data->id],['style' => 'color:red;background-color:yellow','target' => '_blank','data-pjax' => 0, 'title' => 'По акту приема-передачи']);
                if ($oneDay) $result .= Html::a("[ ".$oneDay." ]", ['erp-location/view-one-day','id'=>$data->id],['style' => "color:white;background-color: blue",'target' => '_blank','data-pjax' => 0, 'title' => 'Более одного дня']);
                $result .= ' ';
                if ($rework) $result .= Html::a("[ ".$rework." ]", ['erp-location/view-rework','id'=>$data->id],['style' => "color:white;background-color: blueviolet",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Переделка']);
                if ($express && $access) $result .= Html::a("[ ".$express." ]", ['erp-location/view-express-priority','id'=>$data->id],['style' => 'color:green','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - TopSpeed']);
                if ($bigOrder) $result .= Html::a("[ ".$bigOrder." ]", ['erp-location/view-big-order-priority','id'=>$data->id],['style' => "color:blue;background-color: #EAE6CA" ,'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Большой заказ']);
                if ($main) $result .= Html::a("[ ".$main." ]", ['erp-location/view-main-priority','id'=>$data->id],['style' => 'color:blue','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Срочный']);
                if ($completion) $result .= Html::a("[ ".$completion." ]", ['erp-location/view-completion-priority','id'=>$data->id],['style' => "color:white;background-color: #0b96e5",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Комплектация']);
                if ($standard) $result .= Html::a("[ ".$standard." ]", ['erp-location/view-standard-priority','id'=>$data->id],['style' => "color:white",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Обычный']);
                if ($low) $result .= Html::a("[ ".$low." ]", ['erp-location/view-low-priority','id'=>$data->id],['style' => 'color:black','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Низкий']);
                $result .= ' ';
                if ($autoFilter) $result .= Html::a(" [ ".$autoFilter." АФ ]", ['erp-location/view-auto-filter','id'=>$data->id],['style' => 'color:white','target' => '_blank','data-pjax' => 0, 'title' => 'Наряды для АФ']);
                if ($laitBag) $result .= Html::a(" [ ".$laitBag." LB ]", ['erp-location/view-laitbag','id'=>$data->id],['style' => 'color:white','target' => '_blank','data-pjax' => 0, 'title' => 'Наряды LAITBAG']);
                return $result ? $result : 0;
            },
            'footer' => ErpLocation::getNaryadsWholeWorkSum([18,19]),
        ],
        [
            'attribute'=>'На участке людей',
            'format'=>'raw',
            'label' => 'Людей',
            'value'=>function ($data) {
                $result = $data->recieveActiveUsersCount();
                return $result ? Html::a($result, ['erp-location-workplace/monitor','location_id'=>$data->id],['target' => '_blank','data-pjax' => 0]) : 0;
            },
            'footer' => ErpLocation::recieveActiveUsersCountSum([18,19]),
        ],
        [
            'attribute'=>'Полученные на участок',
            'format'=>'html',
            'label' => 'Пришло из облака',
            'value'=>function ($data) use ($model) {
                $result = $data->recieveCurentStartCount($model->dateFrom,$model->dateTo);
                return $result ? $result : 0;
            },
            'headerOptions' => [
                'style'=>'white-space:normal;'
            ],
        ],
        [
            'attribute'=>'Не выдано',
            'label'=>'Не выдано**',
            'format'=>'raw',
            'value'=>function ($data) use ($access) {
                //Получаем общее количество нарядов, которые должны пройти через участок
                $wholeWork = $data->getNaryadsToStartWholeWork()->count();
                $fromCloud = $data->getNaryadsToStartFromCloud()->count();
                $forStorage = $data->getNaryadsToStartForStorage()->count();
                $inDelay = $data->getNaryadsToStartInDelay()->count();
                $inPause = $data->getNaryadsToStartInPause()->count();
                $autoSpeed = $data->getNaryadsToStartWithAutoSpeed()->count();
                $fromSump = $data->getNaryadsToStartFromSump()->count();
                $moveAct = $data->getNaryadsToStartMoveAct()->count();
                $oneDay = $data->getCountNaryadsMoreOneDayToStart();
                $completionFlag = $data->getNaryadsToStartWithCompletionFlag()->count();


                $allPriorities = $data->getNaryadsToStartByPriority();
                $rework = $allPriorities[1] ?? 0;
                $express = $allPriorities[2] ?? 0;
                $bigOrder = $allPriorities[3] ?? 0;
                $main = $allPriorities[4] ?? 0;
                $completion = $allPriorities[5] ?? 0;
                $standard = $allPriorities[6] ?? 0;
                $low = $allPriorities[7] ?? 0;

                $autoFilter = $data->getAutoFilterWorkOrdersToStart()->count();
                $laitBag = $data->getLBWorkOrdersToStart()->count();
                #############################################################
                $result = '';
                if ($wholeWork) $result .= Html::a($wholeWork, ['erp-location/view-not-given','id'=>$data->id],['target' => '_blank','data-pjax' => 0, 'title' => 'Всего нарядов']);
                if ($fromCloud) $result .= Html::a("( ".$fromCloud." )", ['erp-location/view-from-cloud','id'=>$data->id],['target' => '_blank','data-pjax' => 0, 'title' => 'Из облака']);
                if ($autoSpeed) $result .= Html::a("[ ".$autoSpeed." ]", ['erp-location/view-to-start-auto-speed','id'=>$data->id],['style' => 'color:white;background-color: green','target' => '_blank','data-pjax' => 0, 'title' => 'Автоматически ускоренные']);
                if ($forStorage) $result .= Html::a("[ ".$forStorage." ]", ['erp-location/view-to-start-for-storage','id'=>$data->id],['style' => "color:white;background-color: black",'target' => '_blank','data-pjax' => 0, 'title' => 'Пополнение склада']);
                if ($inDelay) $result .= Html::a("[ ".$inDelay." ]", ['erp-location/view-in-delay','id'=>$data->id],['style' => 'color:yellow;background-color:black','target' => '_blank','data-pjax' => 0, 'title' => 'Не могут быть выданы']);
                if ($inPause) $result .= Html::a("[ ".$inPause." ]", ['erp-location/view-to-start-in-pause','id'=>$data->id],['style' => 'color:yellow','target' => '_blank','data-pjax' => 0, 'title' => 'На паузе']);
                if ($completionFlag) $result .= Html::a("[ ".$completionFlag." ]", ['erp-location/view-to-start-completion-flag','id'=>$data->id],['style' => "color:white;background-color: #0b96e5",'target' => '_blank','data-pjax' => 0, 'title' => 'Комплектация']);
                if ($fromSump) $result .= Html::a("[ ".$fromSump." ]", ['erp-location/view-to-start-from-sump','id'=>$data->id],['style' => "color:aqua;background-color: brown",'target' => '_blank','data-pjax' => 0, 'title' => 'С отстойника']);
                if ($moveAct) $result .= Html::a("[ ".$moveAct." ]", ['erp-location/view-to-start-move-act','id'=>$data->id],['style' => 'color:red;background-color:yellow','target' => '_blank','data-pjax' => 0, 'title' => 'По акту приема-передачи']);
                if ($oneDay) $result .= Html::a("[ ".$oneDay." ]", ['erp-location/view-to-start-one-day','id'=>$data->id],['style' => "color:white;background-color: blue",'target' => '_blank','data-pjax' => 0, 'title' => 'Более одного дня']);
                $result .= ' ';
                if ($rework) $result .= Html::a("[ ".$rework." ]", ['erp-location/view-to-start-rework','id'=>$data->id],['style' => "color:white;background-color: blueviolet",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Переделка']);
                if ($express && $access) $result .= Html::a("[ ".$express." ]", ['erp-location/view-to-start-express-priority','id'=>$data->id],['style' => 'color:green','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - TopSpeed']);
                if ($bigOrder) $result .= Html::a("[ ".$bigOrder." ]", ['erp-location/view-to-start-big-order-priority','id'=>$data->id],['style' => "color:blue;background-color: #EAE6CA" ,'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Большой заказ']);
                if ($main) $result .= Html::a("[ ".$main." ]", ['erp-location/view-to-start-main-priority','id'=>$data->id],['style' => 'color:blue','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Срочный']);
                if ($completion) $result .= Html::a("[ ".$completion." ]", ['erp-location/view-to-start-completion-priority','id'=>$data->id],['style' => "color:white;background-color: #0b96e5",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Комплектация']);
                if ($standard) $result .= Html::a("[ ".$standard." ]", ['erp-location/view-to-start-standard-priority','id'=>$data->id],['style' => "color:white",'target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Обычный']);
                if ($low) $result .= Html::a("[ ".$low." ]", ['erp-location/view-to-start-low-priority','id'=>$data->id],['style' => 'color:black','target' => '_blank','data-pjax' => 0, 'title' => 'Приоритет - Низкий']);
                $result .= ' ';
                if ($autoFilter) $result .= Html::a(" [ ".$autoFilter." АФ ]", ['erp-location/view-auto-filter-to-start','id'=>$data->id],['style' => 'color:white','target' => '_blank','data-pjax' => 0, 'title' => 'Наряды для АФ']);
                if ($laitBag) $result .= Html::a(" [ ".$laitBag." LB ]", ['erp-location/view-laitbag-to-start','id'=>$data->id],['style' => 'color:white','target' => '_blank','data-pjax' => 0, 'title' => 'Наряды LAITBAG']);
                return $result ? $result : 0;
            },
            'headerOptions' => [
                'style'=>'white-space:normal;'
            ],
        ],
//        [
//            'attribute' => 'Просрочены*',
//            'format' => 'html',
//            'value' => function ($data) {
//                if ($intCount = count($data->naryadTimeout)) {
//                    return Html::a($intCount, ['erp-location/view-overdue', 'id' => $data->id]);
//                } else {
//                    return $intCount;
//                }
//            },
//        ],
//        [
//            'attribute' => '№1*',
//            'format' => 'raw',
//            'value' => function ($data) use ($access) {
//                if (isset($data->naryadTimeout[0])) {
//                    $naryad = $data->naryadTimeout[0];
//
//                    $arDelay = [];
//                    $flag = false;
//                    $inDelay = $data->getNaryadsToStartInDelay()->all();
//                    foreach ($inDelay as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad['id'])
//                        $flag = true;
//                    }
//                    $inPause = $data->getNaryadsInPause()->all();
//                    foreach ($inPause as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad->id)
//                        $flag = true;
//                    }
//                    $link = '';
//                    if ($flag)
//                        $link .='<span style="color:yellow">';
//
//                    $link .= $naryad->number . '<br>' . '<span class="small">' . Yii::$app->formatter->asDateTime($naryad->created_at, 'short') . '</span></br>';
//                    //Только для руководителя
//                    if ($access) {
//                        $link .= '<span class="small">' . (isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at, 'short') : Yii::$app->formatter->asDateTime($naryad->created_at, 'short')) . '</span>';
//                    }
//                    if ($flag)
//                        $link .='</span>';
//                    return Html::a($link, ['erp-naryad/view', 'id' => $naryad->id], ['target' => '_blank', 'data-pjax' => 0]);
//                } else {
//                    return "";
//                }
//
//            },
//        ],
//        [
//            'attribute' => '№2*',
//            'format' => 'raw',
//            'value' => function ($data) use ($access) {
//                if (isset($data->naryadTimeout[1])) {
//                    $naryad = $data->naryadTimeout[1];
//
//                    $arDelay = [];
//                    $flag = false;
//                    $inDelay = $data->getNaryadsToStartInDelay()->all();
//                    foreach ($inDelay as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad['id'])
//                        $flag = true;
//                    }
//                    $inPause = $data->getNaryadsInPause()->all();
//                    foreach ($inPause as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad->id)
//                        $flag = true;
//                    }
//                    $link = '';
//                    if ($flag)
//                        $link .='<span style="color:yellow">';
//
//                    $link .= $naryad->number . '<br>' . '<span class="small">' . Yii::$app->formatter->asDateTime($naryad->created_at, 'short') . '</span></br>';
//                    //Только для руководителя
//                    if ($access) {
//                        $link .= '<span class="small">' . (isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at, 'short') : Yii::$app->formatter->asDateTime($naryad->created_at, 'short')) . '</span>';
//                    }
//                    if ($flag)
//                        $link .='</span>';
//                    return Html::a($link, ['erp-naryad/view', 'id' => $naryad->id], ['target' => '_blank', 'data-pjax' => 0]);
//                } else {
//                    return "";
//                }
//            },
//        ],
//        [
//            'attribute' => '№3*',
//            'format' => 'raw',
//            'value' => function ($data) use ($access){
//                if (isset($data->naryadTimeout[2])) {
//                    $naryad = $data->naryadTimeout[2];
//
//                    $arDelay = [];
//                    $flag = false;
//                    $inDelay = $data->getNaryadsToStartInDelay()->all();
//                    foreach ($inDelay as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad['id'])
//                        $flag = true;
//                    }
//                    $inPause = $data->getNaryadsInPause()->all();
//                    foreach ($inPause as $delayNaryad) {
//                       if ($naryad->id == $delayNaryad->id)
//                        $flag = true;
//                    }
//                    $link = '';
//                    if ($flag)
//                        $link .='<span style="color:yellow">';
//
//                    $link .= $naryad->number . '<br>' . '<span class="small">' . Yii::$app->formatter->asDateTime($naryad->created_at, 'short') . '</span></br>';
//                    //Только для руководителя
//                    if ($access) {
//                        $link .= '<span class="small">' . (isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at, 'short') : Yii::$app->formatter->asDateTime($naryad->created_at, 'short')) . '</span>';
//                    }
//                    if ($flag)
//                        $link .='</span>';
//                    return Html::a($link, ['erp-naryad/view', 'id' => $naryad->id], ['target' => '_blank', 'data-pjax' => 0]);
//                } else {
//                    return "";
//                }
//            },
//        ],
        // [
        //     'attribute' => '№4*',
        //     'format' => 'raw',
        //     'value' => function ($data) {
        //         if (isset($data->naryadTimeout[3])) {
        //             $naryad = $data->naryadTimeout[3];

        //             $arDelay = [];
        //             $flag = false;
        //             $inDelay = $data->getNaryadsToStartInDelay()->all();
        //             foreach ($inDelay as $delayNaryad) {
        //                if ($naryad->id == $delayNaryad['id'])
        //                 $flag = true;
        //             }
        //             $inPause = $data->getNaryadsInPause()->all();
        //             foreach ($inPause as $delayNaryad) {
        //                if ($naryad->id == $delayNaryad->id)
        //                 $flag = true;
        //             }
        //             $link = '';
        //             if ($flag)
        //                 $link .='<span style="color:yellow">';

        //             $link .= $naryad->number . '<br>' . '<span class="small">' . Yii::$app->formatter->asDateTime($naryad->created_at, 'short') . '</span></br>';
        //             //Только для руководителя или технгического директора
        //             if (Yii::$app->user->getId() == 2 || Yii::$app->user->getId() == 14 || Yii::$app->user->getId() == 29) {
        //                 $link .= '<span class="small">' . (isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at, 'short') : Yii::$app->formatter->asDateTime($naryad->created_at, 'short')) . '</span>';
        //             }
        //             if ($flag)
        //                 $link .='</span>';
        //             return Html::a($link, ['erp-naryad/view', 'id' => $naryad->id], ['target' => '_blank', 'data-pjax' => 0]);
        //         } else {
        //             return "";
        //         }
        //     },
        // ],

        /*[
            'attribute'=>'№5*       ',
            'format'=>'raw',
            'value'=>function ($data) {
                if (isset($data->naryadTimeout[4])) {
                    $naryad=$data->naryadTimeout[4];

                    $arDelay = [];
                    $flag = false;
                    $inDelay = $data->getNaryadsToStartInDelay()->all();
                    foreach ($inDelay as $delayNaryad) {
                       if ($naryad->id == $delayNaryad['id'])
                        $flag = true;
                    }
                    $inPause = $data->getNaryadsInPause()->all();
                    foreach ($inPause as $delayNaryad) {
                       if ($naryad->id == $delayNaryad->id)
                        $flag = true;
                    }
                    $link = '';
                    if ($flag)
                        $link .='<span style="color:yellow">';

                    $link .= $naryad->number.'<br>'.'<span class="small">'.Yii::$app->formatter->asDateTime($naryad->created_at,'short').'</span></br>';
                    //Только для руководителя
                    if (Yii::$app->user->getId() == 2 || Yii::$app->user->getId() == 14) {
                    $link .= '<span class="small">'.(isset($naryad->updated_at) ? Yii::$app->formatter->asDateTime($naryad->updated_at,'short') :Yii::$app->formatter->asDateTime($naryad->created_at,'short')).'</span>';
                    }
                    if ($flag)
                        $link .='</span>';
                    return Html::a($link, ['erp-naryad/view','id'=>$naryad->id],['target'=>'_blank','data-pjax'=>0]);
                } else {
                    return "";
                }
            },
        ],*/
//        [
//            'attribute'=>'Готовы к получению',
//            'format'=>'html',
//            'label' => 'Пришло на участок',
//            'value'=>function ($data) use ($model) {
//                $result = $data->workOrdersWasStart($model->dateFrom,$model->dateTo);
//                return $result ? $result : 0;
//            },
//            'headerOptions' => [
//                'style'=>'white-space:normal;'
//            ],
//        ],
        [
            'attribute' => 'Всего',
            'label' => 'Сданы',
            'format' => 'raw',
            'value' => function ($data) use ($dFrom, $dTo) {
                $result = '';
                if ($intCount = $data->getNaryadsCreated($dFrom, $dTo)) {
                    $result .= Html::a($intCount, ['erp-location/view-throw-location-by-date',
                        'location' => $data->id,
                        'dFrom' => $dFrom,
                        'dTo' => $dTo],['target' => '_blank','data-pjax' => 0]);
                } else {
                    $result .= $intCount;
                }

                $result .= ' [';

                if ($intCount = $data->getNaryadsCreatedAF($dFrom, $dTo)) {
                    $result .= Html::a($intCount . 'АФ', ['erp-location/view-throw-location-by-date',
                        'location' => $data->id,
                        'dFrom' => $dFrom,
                        'dTo' => $dTo,
                        'af' => true,
                    ],['target' => '_blank','data-pjax' => 0]);
                } else {
                    $result .= $intCount . 'АФ';
                }

                $result .= ']';

                return $result;
            },
        ],

    ]

]) ?>



<h3>Количество в диспетчер склад</h3>
<div style="overflow: overlay">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Участок</th>
            <th>Не сданы</th>
            <th>На участке</th>
            <th>Просрочены*</th>
            <th>№1*</th>
            <th>№2*</th>
            <th>№3*</th>
            <th>№4*</th>
            <th>Всего</th>
        </tr>
        </thead>
        <tbody>
        <tr class="<?= ErpStorageInfo::getCountUpnOverdue() ? 'danger' : 'success' ?>" data-key="1">
            <td>#</td>
            <td><a href="#">Склад</a></td>
            <td><?= ErpStorageInfo::getCountUpnInProduction(); ?></td>
            <td>
                <?= Html::a(ErpStorageInfo::getCountUpnToPosting(), ['erp-location/view-on-storage'], ['target' => '_blank', 'data-pjax' => 0]) ?>
                <? if ($af = ErpStorageInfo::getCountUpnToPostingAF()):?>
                    <?= "[ $af АФ]" ?>
                <? endif; ?>
            </td>
            <td><?= ErpStorageInfo::getCountUpnOverdue(); ?></td>
            <?php $overdueByReadyDate = ErpStorageInfo::getUpnOverdueByReadyDate(0);?>
            <td>
                <? if ($item = $overdueByReadyDate[0] ?? null) { ?>
                    <?=
                    $item->number
                    . '<br>' .
                    Yii::$app->formatter->asDateTime($item->ready_date, 'short')
                    ?>
                <? } ?>
            </td>
            <td>
                <? if ($item = $overdueByReadyDate[1] ?? null) { ?>
                    <?=
                    $item->number
                    . '<br>' .
                    Yii::$app->formatter->asDateTime($item->ready_date, 'short')
                    ?>
                <? } ?>
            </td>
            <td>
                <? if ($item = $overdueByReadyDate[2] ?? null) { ?>
                    <?=
                    $item->number
                    . '<br>' .
                    Yii::$app->formatter->asDateTime($item->ready_date, 'short')
                    ?>
                <? } ?>
            </td>
            <td>
                <? if ($item = $overdueByReadyDate[3] ?? null) { ?>
                    <?=
                    $item->number
                    . '<br>' .
                    Yii::$app->formatter->asDateTime($item->ready_date, 'short')
                    ?>
                <? } ?>
            </td>
            <td>
                <?=ErpStorageInfo::getSumCountUpn();?>
            </td>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-md-6">
        <p>
        <h4>Информация по продукции автофильтра: </h4>
        <ul>
            <li>
                Количество на складе: <?= ErpStorageInfo::getCountAutoFilterOnStorage()?> ( текущий момент)
            </li>
            <li>
                Прибыло на склад: <?= ErpStorageInfo::getCountAutoFilterComingToStorage($dFrom,$dTo)?> ( в выбранную дату )
            </li>
            <li>
                Реализовано: <?= ErpStorageInfo::getCountAutoFilterRealizated()?> ( за весь период )
            </li>
        </ul>
        </p>
        <h5>* указана дата передачи в производство</h5>
        <h5>** формат вида: <span class="bg-danger" style="padding: 3px"><span style="color:white">всего</span> <span >(облако)</span> <span  style="color:yellow">[на паузе]</span></span>. "Всего" - содержит "Облако" и "На паузе"</h5>
        <h5>****** <span class="bg-danger" style="padding: 3px"><span  style="color:yellow;background-color: black;display: inline-block;padding: 5px;">[на паузе/задержка из за логики выдачи]</span></span></h5>

        <?if ($access):?>
            <h5>****** <span class="" style="padding: 3px"> <span  style="display:inline-block;padding: 5px;color:white;background-color: blue">[Наряды, созданные более одного дня назад]</span> - <?= ErpLocation::getCountNaryadsMoreOneDay() ?></span></h5>
            <h5>****** <span class="" style="padding: 3px"> <span  style="display:inline-block;padding: 5px;color:aqua;background-color: brown">[Наряды cо склада отстойника]</span> - <?= ErpLocation::getCountNaryadsFromSump() ?></span></h5>
            <h5>****** <span class="" style="padding: 3px"> <span  style="display:inline-block;padding: 5px;color:white;background-color: black">[Наряды для пополнения склада]</span> - <?= ErpLocation::getCountNaryadsForStorage() ?></span></h5>
            <h5>****** <span class="" style="padding: 3px"> <span  style="display:inline-block;padding: 5px;color:white;background-color: green">[Наряды - автоматически ускоренные, просрочка ]</span> - <?= ErpLocation::getCountNaryadsWithAutoSpeed()?></span></h5>
            <h5>****** <span class="" style="padding: 3px"> <span  style="display:inline-block;padding: 5px;color:red;background-color:yellow">[Наряды - по акту приема-передачи ]</span> - <?= ErpLocation::getCountNaryadsMoveAct()?></span></h5>
            <h5>****** <span style="padding: 3px"> <span  style="display:inline-block;padding: 5px;color:white" class="bg-danger">___[Наряды - для автофильтра АФ]</span> - <?= ErpLocation::getCountAutoFilterWorkOrders()?></span></h5>
            <!--<h5>******* <span class="" style="padding: 3px"> <span  style="background:white;color:red;">[По новой схеме] --><?//= ErpLocation::getCountNaryadsWithNewScheme() ?><!--</span></span></h5>-->
        <?endif;?>
    </div>
    <div class="col-md-6">
        <p>
            <h4>Таблица приоритетов:</h4>
            <h5>1 <span class="" style="padding: 3px"> <span style="display: inline-block; min-width: 125px"><span  style="display:inline-block;padding:5px;color:white;background-color: blueviolet">[ Переделки ]</span></span> - <?= ErpLocation::getCountNaryadsRework() ?></span></h5>
            <h5>2 <span class="" style="padding: 3px"> <span style="display: inline-block; min-width: 125px"><span  style="color:green">[ TopSpeed ]</span></span> - <?= ErpLocation::getCountNaryadsWithExpressPriority() ?></span></h5>
            <h5>3 <span class="" style="padding: 3px"> <span style="display: inline-block; min-width: 125px"><span  style="display:inline-block;padding:5px;color:blue;background-color: #EAE6CA"> [ Большой заказ ]</span></span> - <?= ErpLocation::getCountNaryadsWithBigOrderPriority() ?> </h5>
            <h5>4 <span class="" style="padding: 3px"> <span style="display: inline-block; min-width: 125px"><span  style="color:blue">[ Срочный ]</span></span> - <?= ErpLocation::getCountNaryadsWithMainPriority()  ?></h5>
            <h5>5 <span class="" style="padding: 3px"> <span style="display: inline-block; min-width: 125px"><span  style="display:inline-block;padding:5px;color:white;background-color: #0b96e5"> [ Комплектация ]</span></span> - <?= ErpLocation::getCountNaryadsWithCompletionPriority() ?> </h5>
            <h5>6 <span class="" style="padding: 3px"> <span style="display: inline-block; min-width: 125px"><span  style="display:inline-block;padding:5px;color:white;background-color:#f96868"> [ Обычный ]</span></span> - <?= ErpLocation::getCountNaryadsWithStandardPriority() ?> </h5>
            <h5>7 <span class="" style="padding: 3px"> <span style="display: inline-block; min-width: 125px"><span  style="color:black">[ Низкий ]</span></span> - <?= ErpLocation::getCountNaryadsWithLowPriority()  ?> </h5>
        </p>
    </div>
</div>

<? $dangerNaryadas = ErpNaryad::getDangerUserTimeoutNaryads();?>
<? if ($dangerNaryadas):?>
<div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>
  ВНИМАНИЕ. Наряды без движения более суток: <?=$dangerNaryadas?> шт. 
  <?=Html::a('>>>',['erp-leader-report/danger-timeout-report','hard' => 1])?>
  [
  <? $dangerNaryadasPause = ErpNaryad::getDangerUserTimeoutNaryadsPause();?>
    <? if ($dangerNaryadasPause):?>
          Пауза: <?=$dangerNaryadasPause?> шт. 
          <?=Html::a('>>>',['erp-leader-report/danger-pause-timeout-report','hard' => 1])?>
    <? endif;?>,
  <? $dangerNaryadasUser = ErpNaryad::getDangerUserTimeoutNaryadsUser();?>
    <? if ($dangerNaryadasPause):?>
          На человеке: <?=$dangerNaryadasUser?> шт. 
          <?=Html::a('>>>',['erp-leader-report/danger-user-timeout-report','hard' => 1])?>
    <? endif;?>
  ]
<? endif;?>
</div>
<div>
    <?php if($addProduct->getTemplate()) : ?>
        <div class="pull-right">
            Лекало: <?= $addProduct->getTemplate()?>&nbsp;
        </div>
    <?php endif; ?>

    <?php if($addProduct->getDontlook()) : ?>
        <div class="pull-right">
            Dont look: <?= $addProduct->getDontlook()?>&nbsp;
        </div>
    <?php endif; ?>

    <?php if($addProduct->getLaitBag()) : ?>
        <div class="pull-right">
            Lait Bag: <?= $addProduct->getLaitBag()?>&nbsp;
        </div>
    <?php endif; ?>

    <?php if($addProduct->getWeelCover()) : ?>
        <div class="pull-right">
            Чехлы для колес: <?= $addProduct->getWeelCover()?>&nbsp;
        </div>
    <?php endif; ?>
    <?php if($addProduct->getSeatCover()) : ?>
        <div class="pull-right">
            Чехол на спинку сидения: <?= $addProduct->getSeatCover()?>&nbsp;
        </div>
    <?php endif; ?>
</div>


<?php Pjax::end(); ?>


