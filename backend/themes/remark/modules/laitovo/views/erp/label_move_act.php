<?php

use backend\helpers\ArticleHelper;
use backend\modules\laitovo\models\ErpNaryad;
/**
 * @var $barcode string
 * @var $number integer
 * @var $elements ErpNaryad
 * @var $client string
 * @var $orderNumber string
 * @var $car \common\models\laitovo\Cars
 * @var $carArticle integer
 * @var $template string
 * @var $workerName string
 */

$generator = new Picqer\Barcode\BarcodeGeneratorSVG();

?>
<style type="text/css">
    TABLE {
        border-collapse: collapse; /* Убираем двойные линии между ячейками */
    }
    TD, TH {
        padding: 3px; /* Поля вокруг содержимого таблицы */
        border: 1px solid black; /* Параметры рамки */
    }
    TH {
        background: #b0e0e6; /* Цвет фона */
    }
</style>

<table style="width: 800px">
    <thead>
        <tr>
            <th colspan="2">Акт приёма - передачи № <?= $number?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Штрихкод :</td>
            <td style="padding-left: 28%"><?= $generator->getBarcode($barcode, $generator::TYPE_CODE_128, 2)?></td>
        </tr>
        <tr>
            <td>Клиент :</td>
            <td><?= $client?></td>
        </tr>
        <tr>
            <td>Заказ :</td>
            <td><?= $orderNumber?></td>
        </tr>
        <tr>
            <td>Автомобиль :</td>
            <td><?= $car->fullName?></td>
        </tr>
        <tr>
            <td>Артикул а/м :</td>
            <td><?= $car->article?></td>
        </tr>
        <tr>
            <td>Лекало :</td>
            <td><?= $template?></td>
        </tr>
    </tbody>
</table>

<br>
<table style="width: 800px">
    <thead>
        <tr>
            <th colspan="4">Продукция</th>
        </tr>
        <tr>
            <th>Наименование:</th>
            <th>Количество:</th>
            <th>Ткань:</th>
            <th>Комментарий производства:</th>
        </tr>
    </thead>
    <tbody>
        <?foreach ($elements as $element):?>
        <?$itemtext = isset($element->json('items')[0]['itemtext']) ? $element->json('items')[0]['itemtext'] : '';?>
        <tr>
            <td><?= $element->window . ' ' . $element->windowType?></td>
            <td><?= $count[$element->article]?></td>
            <td><?= $element->tkan?></td>
            <td><h4 style="margin-bottom: 0px;margin-top: 0px">Комментарий производства:</h4>
                <u><i>Примечание:</i>
                    <span>
                        <? if (($upn = $element->upn) && $upn->withTape) : ?>
                            Внимание!!! <b>КЛИПСЫ</b> - не изготавливать !!! На <b>ОТК</b> - вкладывать новые крепления <b>НА СКОТЧЕ</b><br>
                        <? elseif (isset($car) && $car && (($car->json('kryuchokpb') && $element->window == 'ПБ') || ($car->json('kryuchokzb') && $element->window == 'ЗБ') || ($car->json('kryuchokzps') && $element->window == 'ЗШ')) && ArticleHelper::isStandardScreen($element->article)) :?>
                            На <b>ОТК</b> - вложить крючок !!!<br>
                        <? endif;?>
                        <? if (($upn = $element->upn) && $upn->laitovoSimple) : ?>
                            <b>Швейка</b> - лейблы "Laitovo Simple" !!!<br>
                            <b>ОТК</b> - лейблы "Laitovo Simple" !!!<br>
                        <? endif;?>
                    </span>
                </u>


                <i>От менеджера:</i><span> <?= $itemtext ?></span>
                <br>
                <i>От тех.службы:</i><span> <?= @$element->json('items')[0]['comment'] ?></span><br></td>
        </tr>
        <?endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td>Общий комментарий</td>
            <td colspan="3"><?=$element->order ? @$element->order->json('innercomment') : ''?></td>
        </tr>
    </tfoot>
</table>

<br>
<table style="width: 800px">
    <thead>
        <tr>
            <th rowspan="2">Участок производства</th>
            <th colspan="<?= count($elements)?>">Количество</th>
            <th rowspan="2">ФИО Принял</th>
            <th rowspan="2">Подпись</th>
        </tr>
        <tr>
            <?foreach ($elements as $element):?>
                <th><?= $element->window . ' ' . $element->windowType?></th>
            <?endforeach;?>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Изгиб</td>
            <?foreach ($elements as $element):?>
                <td><?= $count[$element->article]?></td>
            <?endforeach;?>
            <td><?= $workerName?></td>
            <td></td>
        </tr>
        <tr>
            <td>Оклейка</td>
            <?foreach ($elements as $element):?>
                <td><?= $count[$element->article]?></td>
            <?endforeach;?>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Швейка</td>
            <?foreach ($elements as $element):?>
                <td><?= $count[$element->article]?></td>
            <?endforeach;?>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Клипсы</td>
            <?foreach ($elements as $element):?>
                <td><?= $count[$element->article]?></td>
            <?endforeach;?>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>ОТК</td>
            <?foreach ($elements as $element):?>
                <td><?= $count[$element->article]?></td>
            <?endforeach;?>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Склад</td>
            <?foreach ($elements as $element):?>
                <td><?= $count[$element->article]?></td>
            <?endforeach;?>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>