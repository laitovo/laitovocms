<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Терминал');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    $("#erp-terminal-form").on("ajaxComplete", function (event, messages) {
        document.location.reload();
    });

    setInterval(function(){ $(".reloadterminal").click();}, 60*1000);

    var barcode=$(\'#erpterminal-barcode\');

    function searchterminal(e)
    {
        $.get("' . Url::to(['search']) . '",{
            barcode: barcode.val()
        },function(data){

            $(".reloadterminal").click();
            if (data.status=="success"){
                notie.alert(1,data.message,15);
                if (data.vidacha && data.vidacha.length &&  data.reestr && data.reestr.length){
                    printJS({printable:"'.Url::to(['print-both']).'?id="+Object.values(data.vidacha).join(",")+"&id2="+Object.values(data.reestr).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }else if (data.reestr && data.reestr.length){
                    printJS({printable:"'.Url::to(['print-reestr']).'?id="+Object.values(data.reestr).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }else if (data.vidacha && data.vidacha.length){
                    printJS({printable:"'.Url::to(['print']).'?id="+Object.values(data.vidacha).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }
                if (data.etiketka){
                    printJS("' . Url::to(['erp-naryad/print-label']) . '?id="+data.etiketka);
                }
            } else {
                notie.alert(3,data.message,15);
            }

        },"json");

        e.preventDefault();
    }
    
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erpterminal-barcode" && e.which == 13){
                searchterminal(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchterminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });

    ', \yii\web\View::POS_END);


?>
<div class="col-xs-4">
    <input type="text" id="erpterminal-barcode" placeholder="Поиск..." class="form-control">

<!--
<?php $form = ActiveForm::begin([
    'id' => 'erp-terminal-form',
    'enableAjaxValidation' => true,
]); ?>

<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>

<?php ActiveForm::end(); ?>
-->
</div>
<?php Pjax::begin(['timeout'=>5000]); ?>

<div class="clearfix"></div>
<!-- поиск наряда -->
<!-- <? if ($poiskNaryad):?>
<div class="col-xs-8 pull-right">
    <h3 class="text-center">Следующий наряд для поиска №</h3>
    <div style="font-size:5em;color:red;" class="text-center"><?= $poiskNaryad?></div>
</div>
<? endif;?> -->
<h3><?= $model->location ? 'Участок : '.$model->location->name:'' ?></h3>
<h4><?=$model->user->name?><?=$model->user->location ? ' - '.$model->user->location->name : ''?></h4>
<? if ($model->user_id):?>
<div>
    <? if($model->location_id !== Yii::$app->params['erp_okleika'] && $model->location_id !== Yii::$app->params['erp_label']): ?>
        <h4>Наряды на сотруднике : <?= count($naryadsOnUser->models) ?>
            <? $ids = []; ?>
            <? foreach ($naryadsOnUser->models as $naryad) {
                $ids[] = $naryad->id ;   
            }
            ?>
            <? if (count($ids)): ?>
                <?= Html::a("<span class='glyphicon glyphicon-print'></span>", ['print-reestr','id' => implode(",", $ids)],['target' => '_blank','data-pjax' => 0] ) ?>
            <? endif; ?>
        </h4>
    <? endif; ?>
    <h4>Наряды к выдаче : <?= count($naryadsToVidacha->models) ?></h4>
</div>
<? endif; ?>

<?= Html::a("Обновить", ['terminal'], ['class' => 'hidden reloadterminal']) ?>

<?= !(($model->location_id && !$model->user_id) || ($model->user_id && ($model->location_id == Yii::$app->params['erp_okleika']) || ($model->location_id == Yii::$app->params['erp_label']))) ? '' : '<h4>Наряды на участке</h4>'.GridView::widget([
    'tableOptions'=>['class'=>'table table-hover'],
    'rowOptions' => function ($data) use ($model,$vidano) {
        if(in_array($data->id, $model->workOrders) || in_array($data->id, $vidano) ) {
            return ['class' => 'success'];
        } else {
            return ['class' => ''];
        }
    },
    'dataProvider' => $naryads,
    'show' => ['id', 'article', 'order_id', 'location_id', 'user_id', 'status'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-naryad/view', 'id' => $data->id]) : null;
            },
        ],
        'article',
        [
            'attribute' => 'order_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view', 'id' => $data->order->id]) : null;
            },
        ],
        [
            'attribute'=>'location_id',
            'format'=>'raw',
            'value'=>function ($data) {
                return $data->locationstart
                ? Html::a(Html::encode($data->locationstart->name), ['erp-location/view','id'=>$data->locationstart->id]).' <small>'.($data->location ? $data->location->name : 'Облако').'</small>'
                : ($data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view','id'=>$data->location->id]) : null);
            },
        ],
        [
            'attribute'=>'user_id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->user ? Html::a(Html::encode($data->user->name), ['erp-user/view','id'=>$data->user->id]) : null;
            },
        ],
        [
            'attribute'=>'scheme_id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
            },
        ],
        'status',
        'created_at:datetime',
        [
            'attribute'=>'author_id',
            'value'=>function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
    ],
]); ?>

<!--Показать наряды на пользователе-->
<?= (!($naryadsOnUser->models && $model->user_id) && (!(count($vidano) && $model->user_id))) ? '' : GridView::widget([
    'tableOptions'=>['class'=>'table table-hover'],
    'rowOptions' => function ($data) use ($model,$vidano) {
        if(in_array($data->id, $model->workOrders) || in_array($data->id, $vidano) ) {
            return ['class' => 'success'];
        } else {
            return ['class' => ''];
        }
    },
    'dataProvider' => $naryadsOnUser,
    'show'=>['id','article','order_id','location_id','user_id','status'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute'=>'id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->name ? /*Html::a(Html::encode($data->name), ['erp-naryad/view','id'=>$data->id])*/ Html::encode($data->name) : null;
            },
        ],
        'article',
        [
            'attribute'=>'order_id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view','id'=>$data->order->id]) : null;
            },
        ],
        [
            'attribute'=>'location_id',
            'format'=>'raw',
            'value'=>function ($data) {
                return $data->locationstart
                    ? Html::a(Html::encode($data->locationstart->name), ['erp-location/view','id'=>$data->locationstart->id]).' <small>'.($data->location ? $data->location->name : 'Облако').'</small>'
                    : ($data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view','id'=>$data->location->id]) : null);
            },
        ],
        [
            'attribute'=>'user_id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->user ? Html::a(Html::encode($data->user->name), ['erp-user/view','id'=>$data->user->id]) : null;
            },
        ],
        [
            'attribute' => 'scheme_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view', 'id' => $data->scheme->id]) : null;
            },
        ],
        'status',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
    ],
]); ?>

<!--Показать наряды к выдаче-->
<?=!($model->user_id && count($naryadsToVidacha->models)) ? '' : GridView::widget([
        'tableOptions'=>['class'=>'table table-hover'],
        'rowOptions' => function ($data,$key,$index) use ($model,$vidano) {
            if((!$data->location_id)) {
                return ['class' => 'warning'];
            }
            elseif ($data->location_id)
            {
                return ['class' => 'info'];
            }
            else
            {
                return ['class' => ''];
            }
        },
        'dataProvider' => $naryadsToVidacha,
        'show'=>['id','article','order_id','location_id','user_id','status'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->name ? /*Html::a(Html::encode($data->name), ['erp-naryad/view','id'=>$data->id])*/ Html::encode($data->name) : null;
                },
            ],
            'article',
            [
                'attribute'=>'order_id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view','id'=>$data->order->id]) : null;
                },
            ],
            [
                'attribute'=>'location_id',
                'format'=>'raw',
                'value'=>function ($data) {
                    return $data->locationstart
                        ? Html::a(Html::encode($data->locationstart->name), ['erp-location/view','id'=>$data->locationstart->id]).' <small>'.($data->location ? 'Найти' : 'Распечатать').'</small>'
                        : ($data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view','id'=>$data->location->id]) : null);
                },
            ],
            [
                'attribute'=>'user_id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->user ? Html::a(Html::encode($data->user->name), ['erp-user/view','id'=>$data->user->id]) : null;
                },
            ],
            [
                'attribute'=>'scheme_id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
                },
            ],
            'status',
            'created_at:datetime',
            [
                'attribute'=>'author_id',
                'value'=>function ($data) {
                    return $data->author ? $data->author->name : null;
                },
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
