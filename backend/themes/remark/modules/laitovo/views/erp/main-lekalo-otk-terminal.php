<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use backend\modules\laitovo\models\ErpLocation;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\laitovo\models\ErpNaryad;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $admin boolean*/

$this->title = Yii::t('app', 'Выдача нарядов на работника по лекалу ОТК!!!');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('

    setInterval(function(){ $(".reloadterminal").click(); }, 600*1000);

', \yii\web\View::POS_END);

if (($print = Yii::$app->session->getFlash('ERP_START_NARYADS')) != null) {
    Yii::$app->view->registerJs('
//        printJS({printable:"' . Url::to(['print', 'id' => implode(',', $print)]) . '", type:"pdf", showModal:true, modalMessage: "Печать..."});

//        $.get("'. Url::to(['print', 'id' => implode(',', $print)]) . '", function(data) {
//            myWindow = window.open();
//            myWindow.document.write(data);
//            myWindow.print();
//            myWindow.close();
//        });

        printUrl("'. Url::to(['print', 'id' => implode(',', $print)]) . '");
    ');
}
?>

<?php
/**
 *  Сортировка по скорости
 */

$speed = isset($_GET['sort']) && $_GET['sort'] == 'speed';

?>

<?= Html::a($speed ? 'Выключить сортировку по срочности' : 'Сортировать по срочности',$speed ? '?' : '?sort=speed',['class' => ($speed ? 'btn btn-primary' : 'btn btn-default')]) ?>


<?php  ActiveForm::begin([
    'id' => 'search-form',
    'method' => 'post',
]); ?>

<?= Html::tag('div',
    Html::checkbox("erp_controller_otk_terminal_show_stop_templates", $showStopTemplates, ['id' => 'checkbox-show-stop'])
    .Html::tag('label','Скрывать заблокированные лекала', ['for' => 'checkbox-show-stop']),
    [
        'class' => 'checkbox-custom checkbox-primary text-left',
        'onchange' => '$("#search-form").submit();'
    ]
) ?>
<?= Html::hiddenInput('update_erp_controller_otk_terminal_show_stop_templates',true); ?>

<?php ActiveForm::end(); ?>

<?php  ActiveForm::begin([
    'method' => 'get',
]); ?>

<a href="<?=Url::to('main-lekalo-otk-terminal')?>" class="pull-right btn btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="<?=Yii::t('app', 'Сбросить')?>"><i class="icon wb-refresh" aria-hidden="true"></i></a>
<div class="input-search input-search-dark pull-right"><i class="input-search-icon wb-search" aria-hidden="true"></i><input type="text" class="form-control" value="<?=Yii::$app->request->get('search')?>" name="search" placeholder="<?=Yii::t('app', 'Поиск')?>"></div>

<?php ActiveForm::end(); ?>

<div class="clearfix"></div>
<?php $form = ActiveForm::begin(['id' => 'main-submit']); ?>

<?= $form->field($model, 'user_id')->widget(Select2::classname(), [
    'theme' => Select2::THEME_BOOTSTRAP,
    'data' => ArrayHelper::merge([null => ''],ArrayHelper::map(\backend\modules\laitovo\models\ErpUser::getAllWorkersLekalo(Yii::$app->params['erp_otk']),'id','nameWithLocation')),
    'options' => ['placeholder' => 'Выберите сотрудника ...'],
    'language' => 'ru',
    'pluginOptions' => [
        'allowClear' => true,
    ],
]); ?>

<?php Pjax::begin(['timeout' => 3000000]); ?>

<h4><?= $model->user->name ?></h4>
<?= Html::a("Обновить", ['main-lekalo-otk-terminal'], ['class' => 'hidden reloadterminal']) ?>
<?= Html::submitButton(Yii::t('app', 'Выдать'), ['class' => 'btn btn-outline btn-round btn-primary','onclick' => '$(this).prop("disabled","disabled");$("#main-submit").submit();']) ?>



<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $naryads,
    // 'show'=>['id','order_id','location_id','status'],
//    'rowOptions' => function ($data) {
//        if ($data->sort == 1) {
//            return ['class' => 'danger'];
//        } elseif ($data->sort == 2) {
//            return ['class' => 'warning'];
//        }
//    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'lekalo',
            'format' => 'raw',
            'label' => 'Номер лекала',
            'value' => function ($data) {
                return Html::tag('div',
                    Html::checkbox(null, false, [
                        'onchange' => "
                        var obj = $(this);
                        $(\".changer\").not(this).each(function(){
                            $(this).prop('disabled', obj.is(':checked'));
                            $(this).closest('tr').find('[class^=\'ordern\']').each(function(){
                                $(this).prop('disabled', obj.is(':checked'));
                            });
                        });
                        $('.ordern" . $data['lekalo'] . "').prop('checked', $(this).is(':checked'));",
                        'class' => 'changer'
                    ]) .
                    Html::tag('label',
                        ($data['lekalo'])
                    ),
                    [
                        'class' => 'checkbox-custom checkbox-primary text-left',
                    ]);
            },
        ],

        [
            'attribute'=>'id',
            'label'=>'Списко нарядов',
            'format'=>'raw',
            'value'=>function ($data) use ($admin){
                $naryads=$data['naryads'];
                $return = '';
                if ($naryads) {
                    foreach ($naryads as $row) {
                        $return .= Html::tag('div',
                            Html::checkbox('ErpMainTerminal[workOrders][]', $checked = false, ['value' => $row->id, 'class' => 'ordern'. $data['lekalo'],'onclick' => ($admin ?'':'return false;')]) .
                            Html::tag('label',
                                ($row->name ? Html::a(Html::encode($row->name), ['erp-naryad/view', 'id' => $row->id]) . ' - ' . @$row->locationstart->name . ' - ' . @$row->car->fullName : null)
                            ,['style' => "color : " .($row->order_id  ?'inherit' : 'black') ]),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                    }
                }

                return $return;
            },
        ],
        [
            'attribute'=>'count',
            'label'=>'Количество нарядов',
        ],
        // [
        //     'attribute'=>'location_id',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view','id'=>$data->location->id]) : null;
        //     },
        // ],
        // [
        //     'attribute'=>'scheme_id',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
        //     },
        // ],
        // 'status',
//        'created_at:datetime',
        // [
        //     'attribute'=>'author_id',
        //     'value'=>function ($data) {
        //         return $data->author ? $data->author->name : null;
        //     },
        // ],
    ],
]); ?>
<?php Pjax::end(); ?>

<?php ActiveForm::end(); ?>

<?php Yii::$app->view->registerJs('
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>