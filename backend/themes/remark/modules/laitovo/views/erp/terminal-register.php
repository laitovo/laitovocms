<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Терминал регистрации сотрудников');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    $("#erp-terminal-form").on("ajaxComplete", function (event, messages) {
        document.location.reload();
    });

    setInterval(function(){ $(".reloadterminal").click();}, 60*1000);

    var barcode=$(\'#erpterminal-barcode\');

    function searchterminal(e)
    {
        $.get("' . Url::to(['search-register']) . '",{
            barcode: barcode.val()
        },function(data){

            $(".reloadterminal").click();
            if (data.status=="success"){
                notie.alert(1,data.message,15);
                if (data.vidacha && data.vidacha.length &&  data.reestr && data.reestr.length){
                    printJS({printable:"'.Url::to(['print-both']).'?id="+Object.values(data.vidacha).join(",")+"&id2="+Object.values(data.reestr).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }else if (data.reestr && data.reestr.length){
                    printJS({printable:"'.Url::to(['print-reestr']).'?id="+Object.values(data.reestr).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }else if (data.vidacha && data.vidacha.length){
                    printJS({printable:"'.Url::to(['print']).'?id="+Object.values(data.vidacha).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }
                if (data.etiketka){
                    printJS("' . Url::to(['erp-naryad/print-label']) . '?id="+data.etiketka);
                }
            } else {
                notie.alert(3,data.message,15);
            }

        },"json");

        e.preventDefault();
    }
    
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erpterminal-barcode" && e.which == 13){
                searchterminal(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchterminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });

    ', \yii\web\View::POS_END);


?>
<div class="col-xs-4">
    <input type="text" id="erpterminal-barcode" placeholder="Поиск..." class="form-control">

<!--
<?php $form = ActiveForm::begin([
    'id' => 'erp-terminal-form',
    'enableAjaxValidation' => true,
]); ?>

<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>

<?php ActiveForm::end(); ?>
-->
</div>
<?php Pjax::begin(['timeout'=>5000]); ?>


<?php Pjax::end(); ?>
