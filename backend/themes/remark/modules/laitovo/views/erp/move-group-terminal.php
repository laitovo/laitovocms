<?php

/**
 * @var yii\web\View $this
 * @var boolean $admin
 * @var yii\data\ArrayDataProvider $naryads
 * @var array|null $already_exist
 * @var string $prefix_barcode
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use yii\grid\GridView;
use kartik\select2\Select2;
use backend\assets\printJs\PrintJs;

$this->title = Yii::t('app', 'Выдача нарядов на работника по лекалу');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);
PrintJs::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

$this->registerJs('setInterval(function(){ $(".reloadterminal").click(); }, 600*1000);', \yii\web\View::POS_END);

if (($print = Yii::$app->session->getFlash('PROD_GROUP_START_NARYADS')) != null && ($groupBarcode = Yii::$app->session->getFlash('PROD_GROUP_BARCODE'))) {
    $url = Url::to(['/laitovo/erp/move-group-print', 'groupBarcode' => $groupBarcode]);
    // Функция print_by_url() объявлена в backend\assets\printJs\PrintJs
    $js = 'printJS("'. Url::to(['/laitovo/erp/move-group-print', 'groupBarcode' => $groupBarcode]) .'");
    ';
    $this->registerJs($js, \yii\web\View::POS_END);
}

/**
 *  Сортировка по скорости
 */
$speed = isset($_GET['sort']) && $_GET['sort'] == 'speed';
$js = "
        function hiddenBtnSubmit() {
            let btnSubmit = $('#main-submit-button');
            let num = $('.order-n:checked').length;
            let worker = $('#worker-name').val();
            if (num && worker) {
                btnSubmit.removeClass('hidden');
            } else {
                btnSubmit.addClass('hidden');
            }
        }
        $(function () {
            $('#worker-name').change(function () {
                hiddenBtnSubmit();
            });

            $(document).on('submit', '#main-submit', function () {
                let num = $('.order-n:checked').length;
                let worker = $('#worker-name option:selected').text();
                let str = 'По акту будет выдано ' + num + ' наряд(а-ов). На ' + worker + ' Продолжить?';
                if (!confirm(str)) {
                    return false;
                }
                $(this).prop('disabled','disabled');
                return true;
            });
            $('.print-already-act').click(function (e) {
                e.preventDefault();
                printJS($(this).attr('href'));
            });
        });
";
$this->registerJs($js, \yii\web\View::POS_END);
?>

<?= Html::a($speed ? 'Выключить сортировку по срочности' : 'Сортировать по срочности', $speed ? '?' : '?sort=speed',
    ['class' => ($speed ? 'btn btn-primary' : 'btn btn-default')]) ?>

<?php ActiveForm::begin([
    'method' => 'get',
]); ?>

    <a href="<?= Url::to('move-group-terminal') ?>" class="pull-right btn btn-icon btn-default btn-outline btn-round"
       data-toggle="tooltip" data-original-title="<?= Yii::t('app', 'Сбросить') ?>"><i class="icon wb-refresh"
                                                                                       aria-hidden="true"></i></a>
    <div class="input-search input-search-dark pull-right"><i class="input-search-icon wb-search"
                                                              aria-hidden="true"></i><input type="text"
                                                                                            class="form-control"
                                                                                            value="<?= Yii::$app->request->get('search') ?>"
                                                                                            name="search"
                                                                                            placeholder="<?= Yii::t('app',
                                                                                                'Поиск') ?>"></div>

<?php ActiveForm::end(); ?>

    <div class="clearfix"></div>
<?php $form = ActiveForm::begin(['id' => 'main-submit']); ?>

<?= $form->field($model, 'user_id')->widget(Select2::classname(), [
    'theme' => Select2::THEME_BOOTSTRAP,
    'data' => ArrayHelper::merge([null => ''],
        ArrayHelper::map(\backend\modules\laitovo\models\ErpUser::getAllWorkersLekaloFull(), 'id', 'nameWithLocation')),
    'options' => ['placeholder' => 'Выберите сотрудника ...', 'id' => 'worker-name'],
    'language' => 'ru',
    'pluginOptions' => [
        'allowClear' => true,
    ],
]); ?>


    <h4><?= $model->user->name ?></h4>
<?= Html::a("Обновить", ['move-group-terminal'], ['class' => 'hidden reloadterminal']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Выдать'),
            ['class' => 'btn btn-outline btn-round btn-primary hidden', 'id' => 'main-submit-button']) ?>
    </div>

<?php if ($already_exist) : ?>
    <div class="form-group">
        <h5>Выданные акты</h5>
        <?php foreach ($already_exist as $item) : ?>
            <?php $groupBarcode = $prefix_barcode . $item['prodGroup'] ?>
            <p>
                <?= Html::a('Группа #' . $item['prodGroup'],
                    ['/laitovo/erp/move-group-print', 'groupBarcode' => $groupBarcode], ['class' => 'print-already-act']) ?>
            </p>
        <?php endforeach ?>
    </div>
<?php endif ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $naryads,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'lekalo',
            'format' => 'raw',
            'label' => 'Номер лекала',
            'value' => function ($data) {
                return Html::tag('div',
                    Html::checkbox(null, false, [
                        'onchange' => "
                        let obj = $(this);
                        $('.ordern" . $data['lekalo'] . "').prop('checked', $(this).is(':checked'));",
                        'class' => 'changer'
                    ]) .
                    Html::tag('label',
                        ($data['lekalo'])
                    ),
                    [
                        'class' => 'checkbox-custom checkbox-primary text-left',
                    ]);
            },
        ],
        [
            'attribute' => 'id',
            'label' => 'Список нарядов',
            'format' => 'raw',
            'value' => function ($data) use ($admin) {
                $naryads = $data['naryads'];
                $return = '';
                if ($naryads) {
                    foreach ($naryads as $groupId => $row) {
                        $return .= Html::tag('div',
                            Html::checkbox(null, false, [
                                'onchange' => "
                        let obj = $(this);
                        $('.ordern" . $groupId . "').prop('checked', $(this).is(':checked'));hiddenBtnSubmit();",
                                'class' => 'changer parent-changer'
                            ]) .
                            Html::tag('label',
                                ($groupId)
                            ),
                            [
                                'class' => 'checkbox-custom checkbox-primary text-left',
                                'data-group-id' => $groupId
                            ]);
                        foreach ($row as $element) {
                            $return .= Html::tag('div',
                                Html::checkbox('ProdGroupTerminal[workOrders][]', $checked = false, [
                                    'value' => $element->id,
                                    'class' => 'order-n ordern' . $groupId,
                                    'onclick' => ($admin ? '' : 'return false;')
                                ]) .
                                Html::tag('label',
                                    ($element->name ? Html::a(Html::encode($element->name), [
                                            'erp-naryad/view',
                                            'id' => $element->id
                                        ]) . ' - ' . @$element->locationstart->name . ' - ' . @$element->car->fullName : null)
                                    , ['style' => "color : " . ($element->order_id ? 'inherit' : 'black')]),
                                ['class' => 'checkbox-custom checkbox-primary text-left']);
                        }
                    }
                }

                return $return;
            },
        ],
        [
            'attribute' => 'count',
            'label' => 'Количество нарядов',
        ],
    ],
]); ?>

<?php ActiveForm::end(); ?>