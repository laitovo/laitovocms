<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Терминал-выдача');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
//$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    $("#erp-terminal-form").on("ajaxComplete", function (event, messages) {
        document.location.reload();
    });
    
    $("#p0").on("pjax:end", function() {
      $("#p0 [data-toggle=tooltip]").tooltip();
    })
    
    setInterval(function(){ $(".reloadterminal").click();}, 60*1000);

    var barcode=$(\'#erpterminal-barcode\');

    function searchterminal(e)
    {
        $.get("' . Url::to(['search-vidacha']) . '",{
            barcode: barcode.val()
        },function(data){

            $(".reloadterminal").click();
            if (data.status=="success"){
                notie.alert(1,data.message,15);
                if (data.vidacha && data.vidacha.length &&  data.reestr && data.reestr.length){
//                    printJS({printable:"'.Url::to(['print-both']).'?id="+Object.values(data.vidacha).join(",")+"&id2="+Object.values(data.reestr).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                    printUrl("' . Url::to(['print-both']) . '?id="+Object.values(data.vidacha).join(",")+"&id2="+Object.values(data.reestr).join(","));
                }else if (data.reestr && data.reestr.length){
//                    printJS({printable:"'.Url::to(['print-reestr']).'?id="+Object.values(data.reestr).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                    printUrl("' . Url::to(['print-reestr']) . '?id="+Object.values(data.reestr).join(","));
                }else if (data.vidacha && data.vidacha.length){
//                    printJS({printable:"'.Url::to(['print']).'?id="+Object.values(data.vidacha).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                    printUrl("' . Url::to(['print']) . '?id="+Object.values(data.vidacha).join(","));
                }else if (data.addGroup && data.addGroup.length){
//                    printJS({printable:"'.Url::to(['print-add-group']).'?id="+Object.values(data.addGroup).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                    printUrl("' . Url::to(['print-add-group']) . '?id="+Object.values(data.addGroup).join(","));
                }
                if (data.etiketka){
//                    printJS("' . Url::to(['erp-naryad/print-label']) . '?id="+data.etiketka);
                    printUrl("' . Url::to(['erp-naryad/print-label']) . '?id="+data.etiketka);
                }
            } else {
                notie.alert(3,data.message,15);
            }

        },"json");

        e.preventDefault();
    }
    
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erpterminal-barcode" && e.which == 13){
                searchterminal(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchterminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });
    
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
    ', \yii\web\View::POS_END);


?>
<div class="col-xs-4">
    <input type="text" id="erpterminal-barcode" placeholder="Поиск..." class="form-control">

<!--
<?php $form = ActiveForm::begin([
    'id' => 'erp-terminal-form',
    'enableAjaxValidation' => true,
]); ?>

<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>

<?php ActiveForm::end(); ?>
-->
</div>
<?php Pjax::begin(['timeout'=>50000]); ?>

<?= Yii::$app->view->registerJs( '
    $(".set-cloth").on("click", function (event, messages) {
        $.get("' . Url::to(['set-vidacha-cloth']) . '",{
            cloth: $(this).val()
        },function(data){
            $(".reloadterminal").click();
            if (data.status=="success") {
                notie.alert(1,data.message,15);
            }else {
                notie.alert(3,data.message,15);
            }
        },"json");
    });
    $(".set-window").on("click", function (event, messages) {
        $.get("' . Url::to(['set-vidacha-window']) . '",{
            window: $(this).val()
        },function(data){
            $(".reloadterminal").click();
            if (data.status=="success") {
                notie.alert(1,data.message,60);
            }else {
                notie.alert(3,data.message,60);
            }
        },"json");
    });
', \yii\web\View::POS_END);?>
<div class="col-xs-8">
    <?if (!empty($model->clothValues)) : ?>
    Выберите ткнань :
    <? foreach ($model->clothValues as $val): ?>

    <button class="btn <?= $val == $model->cloth ? 'btn-primary' : 'btn-default'?> btn-sm set-cloth" value="<?=$val?>"><?=$val?></button>
    <?endforeach;?>
    <?endif;?>
</div>
<div class="col-xs-8">
    <?if (!empty($model->cloth)) : ?>
    Выберите оконный проём :
    <? foreach ($model->windowValuesExisting as $key1 => $val1): ?>

    <button class="btn <?= $key1 == $model->window ? 'btn-primary' : 'btn-default'?> btn-sm set-window" value="<?=$key1?>"><?=$val1?></button>
    <?endforeach;?>
    <?endif;?>
</div>

<div class="clearfix"></div>
<!-- поиск наряда -->
<!-- <? if ($poiskNaryad):?>
<div class="col-xs-8 pull-right">
    <h3 class="text-center">Следующий наряд для поиска №</h3>
    <div style="font-size:5em;color:red;" class="text-center"><?= $poiskNaryad?></div>
</div>
<? endif;?> -->
<? if ($nextNaryad && $nextNaryad->rework && $model->location_id == Yii::$app->params['erp_okleika']):?>
<div class="col-xs-8 pull-right">
    <h3 class="text-center">Возми переделку:</h3>
    <div style="font-size:3em;color:red;" class="text-center"><?= mb_substr(trim(str_replace('Наряд','',$nextNaryad->name)), 5, mb_strlen(trim(str_replace('Наряд','',$nextNaryad->name))))?> Ткань: <?=$nextNaryad->tkan?> </div>
</div>
<? endif;?>

<h3><?= $model->location ? 'Участок : '.$model->location->name . ' ' . $model->location->recieveActiveWorkplacesProperties($model->user_id):'' ?></h3>
<h4><?=$model->user->name?><?=$model->user->location ? ' - '.$model->user->location->name . ' ' . $model->user->location->recieveActiveWorkplacesProperties($model->user_id): ''?></h4>
<? if ($model->user_id):?>
<div>
    <hr>
    <?php
        $today = round($model->user->getNaryadSumToday(), 2);
        $month = round($model->user->getNaryadSumMonth(), 2);
        $fineMonth = round($model->user->getFineSumMonth(), 2);
        $reworkMonth = round($model->user->getReworkSumMonth(), 2);
        $addWorkOrder = round($model->user->getAddSumMonth(), 2);
        $resultSum = $month - $fineMonth - $reworkMonth;
    ?>
    <h4>За сегодня: <?= $today ?> | Дисциплина за месяц: <?= $fineMonth ?> | Брак переделки за месяц: <?= $reworkMonth ?></h4>
    <h4>Доп-наряды за месяц: <?= $addWorkOrder ?></h4>
    <h4>Всего за месяц, с учетом доп-нарядов, за минусом дисциплины и переделок (чистыми): <span class="<?= ($resultSum < 0 ? 'text-danger' : '')?>"><?= $resultSum ?></span></h4>
    <hr>
    <h4>Сделано нарядов: <?= $model->user->getNaryadCountToday() ?></h4>
    <? if($model->location_id !== Yii::$app->params['erp_label']): ?>
        <h4>Наряды на сотруднике : <?= count($naryadsOnUser->models) ?>
            <? $ids = []; $addGroup=[] ?>
            <? foreach ($naryadsOnUser->models as $naryad) {
                $ids[] = $naryad->id ;
                if ($naryad->addGroup) $addGroup[$naryad->addGroup] = ErpNaryad::ADD_GROUP_BARCODE_PREFIX . $naryad->addGroup;
            }
            ?>
            <? if (count($ids)): ?>
                <?= Html::a("<span class='glyphicon glyphicon-print'></span>", ['print-reestr','id' => implode(",", $ids)],[
                    'target' => '_blank',
                    'data-pjax' => 0,
                    'data-original-title' => "Распечатать как РЕЕСТР",
                    'data-toggle' => "tooltip",
                    'data-placement' => "top",
                    'data-html' => "true"] ) ?>
            &nbsp;
                <?= Html::a("<i class='glyphicon glyphicon-file'></i>", ['print','id' => implode(",", $ids)],[
                        'target' => '_blank',
                        'data-pjax' => 0,
                        'data-original-title' => "Распечатать КАК НАРЯДЫ",
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'data-html' => "true"] ) ?>
            &nbsp;
                <?php if (count($addGroup)) :?>
                <?= Html::a("<i class='glyphicon glyphicon-list-alt'></i>", ['print-add-group','id' => implode(",", $addGroup)],[
                        'target' => '_blank',
                        'data-pjax' => 0,
                        'data-original-title' => "Распечатать КАК Реестр доп. продукции",
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'data-html' => "true"] ) ?>
                <?php endif; ?>
            <? endif; ?>
        </h4>
    <? endif; ?>
    <h4>Наряды к выдаче : <?= count($naryadsToVidacha->models) ?></h4>
</div>
<? endif; ?>

<?// if (count($naryadsOnUserDelay->models) && $model->location_id == Yii::$app->params['erp_okleika']):?>
<!--    <div class="row">-->
<!--        <h3 class="text-center" style="font-size:2em;">Список нарядов, которые у Вас более 1,5 часов !!!<br>-->
<!--            За наряды, которые у Вас более 3 часов <span class="text-danger">ШТРАФ</span> в размере 50 рублей за каждый час просрочки!!!-->
<!--        </h3>-->
<!--        --><?php //=GridView::widget([
//            'tableOptions'=>['class'=>'table table-hover'],
//            'rowOptions' => function ($data)  {
//                if (in_array($data->sort,[1,2,3])) {
//                    return ['class' => 'danger','style' => 'font-size:1.2em'];
//                }
//                return ['class' => 'warning','style' => 'font-size:1.2em'];
//            },
//            'salt' => 'dsadsadsad123',
//            'dataProvider' => $naryadsOnUserDelay,
//            'show' => ['id', 'article', 'order_id', 'leftTime'],
//            'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
//
//                [
//                    'attribute' => 'id',
//                    'format' => 'html',
//                    'value' => function ($data) {
//                        $number = trim(str_replace('Наряд', '', $data->name));
//                        return str_replace((mb_substr($number, 5, mb_strlen($number))),
//                            ('<b style="font-size: 1.5em; color: #0a0a0a">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number);
//                    },
//                ],
//                'article',
//                [
//                    'attribute' => 'order_id',
//                    'format' => 'html',
//                    'value' => function ($data) {
//                        return $data->order ? Html::encode($data->order->name) : null;
//                    },
//                ],
//                [
//                    'attribute'=>'location_id',
//                    'format'=>'raw',
//                    'value'=>function ($data) {
//                        return $data->locationstart
//                            ? Html::encode($data->locationstart->name) .' <small>'.($data->location ? $data->location->name : 'Облако').'</small>'
//                            : ($data->location ? Html::encode($data->location->name) : null);
//                    },
//                ],
//                [
//                    'attribute'=>'user_id',
//                    'format'=>'html',
//                    'value'=>function ($data) {
//                        return $data->user ? Html::encode($data->user->name) : null;
//                    },
//                ],
//                [
//                    'attribute'=>'scheme_id',
//                    'format'=>'html',
//                    'value'=>function ($data) {
//                        return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
//                    },
//                ],
//                'status',
//                'created_at:datetime',
//                [
//                    'attribute'=>'author_id',
//                    'value'=>function ($data) {
//                        return $data->author ? $data->author->name : null;
//                    },
//                ],
//                [
//                    'attribute' => 'lekalo',
//                    'label' => 'Лекало',
//                    'value' => function ($data) {
//                        return $data->lekalo ? (int)$data->lekalo: null;
//                    },
//                ],
//                [
//                    'attribute' => 'leftTime',
//                    'label' => 'Находиться на участке',
//                    'value' => function ($data) {
//                        $diff = time() - $data->updated_at;
//                        return date("H:i:s", mktime(0, 0, $diff));
//                    },
//                ],
//
//            ],
//        ]);?>
<!--    </div>-->
<?// endif;?>


<? if ($model->user_id && $naryadsInTopSpeed && count($naryadsInTopSpeed->models) && $model->location_id == Yii::$app->params['erp_okleika']):?>
    <div class="row">
        <h3 class="text-center" style="font-size:2em;">Наряды, которые нужно выполнить в первую очередь !!!
        </h3>
        <?=GridView::widget([
            'tableOptions'=>['class'=>'table table-hover'],
            'rowOptions' => function ($data)  {
                if (in_array($data->sort,[1,2,3])) {
                    return ['class' => 'danger','style' => 'font-size:1.2em'];
                }
                return ['class' => 'warning','style' => 'font-size:1.2em'];
            },
            'salt' => 'dsadsadsad12345',
            'dataProvider' => $naryadsInTopSpeed,
            'show' => ['id', 'productionLiteral', 'sort', 'article', 'order_id'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'format' => 'html',
                    'value' => function ($data) {
                        $number = trim(str_replace('Наряд', '', $data->name));
                        return str_replace((mb_substr($number, 5, mb_strlen($number))),
                            ('<b style="font-size: 1.5em; color: #0a0a0a">' . mb_substr($number, 5, mb_strlen($number)) . '</b>'), $number);
                    },
                ],
                [
                    'attribute'=>'productionLiteral',
                    'label'=>'Литера',
                    'format'=>'html',
                    'value'=>function ($data) {
                        return $data->currentProductionLiteral;
                    },
                ],
                [
                    'attribute' => 'sort',
                    'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
                    'filter' => ErpNaryad :: prioritets(),
                ],
                'article',
                [
                    'attribute' => 'order_id',
                    'format' => 'html',
                    'value' => function ($data) {
                        return $data->order ? Html::encode($data->order->name) : null;
                    },
                ],
            ],
        ]);?>
    </div>
<? endif;?>

<?= Html::a("Обновить", ['terminal-vidacha'], ['class' => 'hidden reloadterminal']) ?>

<!--Показать наряды на пользователе-->
<?= (!($naryadsOnUser->models && $model->user_id) && (!(count($vidano) && $model->user_id))) ? '' : GridView::widget([
    'tableOptions'=>['class'=>'table table-hover'],
    'rowOptions' => function ($data) use ($model,$vidano) {
        if(in_array($data->id, $model->workOrders) || in_array($data->id, $vidano) ) {
            return ['class' => 'success'];
        } else {
            return ['class' => ''];
        }
    },
    'dataProvider' => $naryadsOnUser,
    'show' => ['id','productionLiteral','sort','article','order_id','location_id','user_id','status'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute'=>'id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->name ? /*Html::a(Html::encode($data->name), ['erp-naryad/view','id'=>$data->id])*/ Html::encode($data->name) : null;
            },
        ],
        [
            'attribute'=>'productionLiteral',
            'label'=>'Литера',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->productionLiteral;
            },
        ],
        [
            'attribute' => 'sort',
            'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
            'filter' => ErpNaryad :: prioritets(),
        ],
        'article',
        [
            'attribute'=>'order_id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->order ? Html::encode($data->order->name) : null;
            },
        ],
        [
            'attribute'=>'location_id',
            'format'=>'raw',
            'value'=>function ($data) {
                return $data->locationstart
                    ? Html::encode($data->locationstart->name) .' <small>'.($data->location ? $data->location->name : 'Облако').'</small>'
                    : ($data->location ? Html::encode($data->location->name) : null);
            },
        ],
        [
            'attribute'=>'user_id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->user ? Html::encode($data->user->name) : null;
            },
        ],
        [
            'attribute' => 'scheme_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view', 'id' => $data->scheme->id]) : null;
            },
        ],
        'status',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
            'attribute' => 'lekalo',
            'label' => 'Лекало',
            'value' => function ($data) {
                return $data->lekalo ? (int)$data->lekalo: null;
            },
        ],

    ],
]); ?>

<!--Показать наряды к выдаче-->
<?=!($model->user_id && count($naryadsToVidacha->models)) ? '' : GridView::widget([
        'tableOptions'=>['class'=>'table table-hover'],
        'rowOptions' => function ($data,$key,$index) use ($model,$vidano) {
            if((!$data->location_id)) {
                return ['class' => 'warning'];
            }
            elseif ($data->location_id)
            {
                return ['class' => 'info'];
            }
            else
            {
                return ['class' => ''];
            }
        },
        'dataProvider' => $naryadsToVidacha,
        'show'=>['id','productionLiteral','sort','article','order_id','location_id','user_id','status'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->name ? /*Html::a(Html::encode($data->name), ['erp-naryad/view','id'=>$data->id])*/ Html::encode($data->name) : null;
                },
            ],
            [
                'attribute'=>'productionLiteral',
                'label'=>'Литера',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->currentProductionLiteral;
                },
            ],
            [
                'attribute' => 'sort',
                'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
                'filter' => ErpNaryad :: prioritets(),
            ],
            'article',
            [
                'attribute'=>'order_id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view','id'=>$data->order->id]) : null;
                },
            ],
            [
                'attribute'=>'location_id',
                'format'=>'raw',
                'value'=>function ($data) {
                    return $data->locationstart
                        ? Html::a(Html::encode($data->locationstart->name), ['erp-location/view','id'=>$data->locationstart->id]).' <small>'.($data->location ? 'Найти' : 'Распечатать').'</small>'
                        : ($data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view','id'=>$data->location->id]) : null);
                },
            ],
            [
                'attribute'=>'user_id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->user ? Html::a(Html::encode($data->user->name), ['erp-user/view','id'=>$data->user->id]) : null;
                },
            ],
            [
                'attribute'=>'scheme_id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
                },
            ],
            'status',
            'created_at:datetime',
            [
                'attribute'=>'author_id',
                'value'=>function ($data) {
                    return $data->author ? $data->author->name : null;
                },
            ],
            [
                'attribute' => 'lekalo',
                'label' => 'Лекало',
                'value' => function ($data) {
                    return $data->lekalo ? (int)$data->lekalo: null;
                },
            ],

        ],
    ]); ?>

<?= !(($model->location_id && !$model->user_id) || ($model->user_id && ($model->location_id == Yii::$app->params['erp_okleika']) || ($model->location_id == Yii::$app->params['erp_label']))) ? '' : '<h4>Наряды на участке</h4>'.GridView::widget([
        'tableOptions'=>['class'=>'table table-hover'],
        'rowOptions' => function ($data) use ($model,$vidano) {
            if(in_array($data->id, $model->workOrders) || in_array($data->id, $vidano) ) {
                return ['class' => 'success'];
            } else {
                return ['class' => ''];
            }
        },
        'salt' => 'dsadsadsad1233333',
        'dataProvider' => $naryads,
        'show' => ['id', 'article', 'order_id', 'location_id', 'user_id', 'status'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->name ? /*Html::a(Html::encode($data->name), ['erp-naryad/view','id'=>$data->id])*/ Html::encode($data->name) : null;
                },
            ],
            'article',
            [
                'attribute' => 'order_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->order ? Html::encode($data->order->name) : null;
                },
            ],
            [
                'attribute'=>'location_id',
                'format'=>'raw',
                'value'=>function ($data) {
                    return $data->locationstart
                        ? Html::encode($data->locationstart->name) .' <small>'.($data->location ? $data->location->name : 'Облако').'</small>'
                        : ($data->location ? Html::encode($data->location->name) : null);
                },
            ],
            [
                'attribute'=>'user_id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->user ? Html::encode($data->user->name) : null;
                },
            ],
            [
                'attribute'=>'scheme_id',
                'format'=>'html',
                'value'=>function ($data) {
                    return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
                },
            ],
            'status',
            'created_at:datetime',
            [
                'attribute'=>'author_id',
                'value'=>function ($data) {
                    return $data->author ? $data->author->name : null;
                },
            ],
            [
                'attribute' => 'lekalo',
                'label' => 'Лекало',
                'value' => function ($data) {
                    return $data->lekalo ? (int)$data->lekalo: null;
                },
            ],

        ],
    ]); ?>
<?php Pjax::end(); ?>
