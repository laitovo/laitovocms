<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpNaryad;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpOrder */
/* @var $orderEntries array */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заказы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

$naryadscount = [];
$naryadsid = [];
foreach (ArrayHelper::map($model->naryads, 'id', 'article') as $key => $value) {
    $naryadsid[$value][] = $key;
}

if (($print = Yii::$app->session->getFlash('ERP_FROMSKLAD_NARYADS')) != null) {
    Yii::$app->view->registerJs('
//        printJS({printable:"' . Url::to(['erp/print', 'id' => implode(',', $print)]) . '", type:"pdf", showModal:true, modalMessage: "Печать..."});
       printUrl("' . Url::to(['erp/print', 'id' => implode(',', $print)]) . '");
    ');
}

?>

<style>
    .bg-shipped * {
        background-color: rgb(58, 169, 158);
        color: white;
    }
    .bg-deleted * {
        background-color: #001f3f;
    }
</style>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => $model->order ? Html::a(Html::encode($model->name), ['/order/default/update', 'id' => $model->order->id]) : $model->name,
        ],
        [
            'attribute' => 'laitovo_href',
            'label' => 'Ссылка на заказ на сайте Laitovo.ru',
            'format' => 'html',
            'value' => $model->id ? Html::a(Html::encode($model->name), 'https://laitovo.eu/ru_RU/admin/orders/orders/update/'.$model->id) : $model->name,
        ],

        // 'order_id',
        [
            'attribute' => 'sort',
            'value' => @$model->prioritets[$model->sort],
        ],
        'status',
        'created_at:datetime',
        // [
        //     'attribute'=>'author_id',
        //     'value'=>$model->author ? $model->author->name : null,
        // ],
        [
            'attribute' => 'Категория',
            'value' => $model->json('category'),
        ],
        [
            'attribute' => 'Покупатель',
            'value' => $model->json('username'),
        ],
        [
            'attribute' => 'Телефон',
            'value' => $model->json('userphone'),
        ],
        [
            'attribute' => 'E-mail',
            'value' => $model->json('useremail'),
        ],
        [
            'attribute' => 'ИНН',
            'value' => $model->json('userinn'),
        ],
        [
            'attribute' => 'Доставка',
            'value' => $model->json('delivery'),
        ],
        [
            'attribute' => 'Страна',
            'value' => $model->json('country'),
        ],
        [
            'attribute' => 'Адрес',
            'value' => $model->json('address'),
        ],
        [
            'attribute' => 'Менеджер',
            'value' => $model->json('manager'),
        ],
        [
            'attribute' => 'Комментарий',
            'value' => $model->json('comment'),
        ],
        [
            'attribute' => 'Общий комментарий к заказу',
            'value' => $model->json('innercomment'),
        ],
    ],
]) ?>

<!--<form action="--><?//= Url::to(['to-naryad','id' => $model->id]) ?><!--" method="POST">-->
<?= Html::beginForm(Url::to(['to-naryad','id' => $model->id]),'POST');?>
    <h4>Товары</h4>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Автомобиль</th>
                <th>Товар</th>
                <th>Артикул</th>
                <? if ($model->status): ?>
                    <th>Участок</th>
                    <th>Статус</th>
                <? else: ?>
                    <th>Остаток(1C)</th>
                    <th>Остаток(NEW)</th>
                    <th class="text-center">Со склада
                        <div class="checkbox-custom checkbox-primary">
                            <input type="checkbox"
                                   onchange="$('.productionthis').prop('checked',$(this).is(':checked'))">
                            <label></label>
                        </div>
                    </th>
                <? endif ?>
                <th width='250'>Аналоги</th>
            </tr>
            </thead>
            <tbody id="tbody-items">
    <?php
        $indexes = [];
        foreach ($model->json('items') as $key => $row):
            $name = isset($row['name']) ? $row['name'] : '';
            $car = isset($row['car']) ? $row['car'] : null;
            $article = isset($row['article']) ? $row['article'] : null;
            $barcode = isset($row['barcode']) ? $row['barcode'] : null;
            $quantity = isset($row['quantity']) ? $row['quantity'] : null;
            $comment = isset($row['comment']) ? $row['comment'] : null;
            $settext = isset($row['settext']) ? $row['settext'] : null;
            $itemtext = isset($row['itemtext']) ? $row['itemtext'] : null;
            $total = isset($total) ? $quantity + $total : $quantity;

            if (!isset($indexes[$article])) {
                $index = $indexes[$article] = 0;
            } else {
                $index = $indexes[$article]++;
            }
            $isShipped   = $orderEntries[$article][$index]['isShipped'] ?? false; //закладываемся на то, что OrderEntries идут в таком же количестве и порядке, как и $model->json('items')
            $isDeleted   = $orderEntries[$article][$index]['isDeleted'] ?? false;
            $person      = $orderEntries[$article][$index]['person'] ?? null;
            $dateStorage = $orderEntries[$article][$index]['date'] ?? null;
            $upnStorage  = $orderEntries[$article][$index]['upn']  ?? null;

            if (isset($naryadsid[$article]) && (@$naryadscount[$article]+=1) && isset($naryadsid[$article][$naryadscount[$article]-1])){
                $naryad = ErpNaryad::findOne( $naryadsid[$article][$naryadscount[$article]-1] );
                $clas='warning';
                $status=Html::a($naryad->status?:'Через производство', ['erp-naryad/view','id'=>$naryad->id]);
            } else {
                $clas='';
                $status='';
                $naryad = new ErpNaryad;
            }
            if (in_array($naryad->status,[$naryad::STATUS_READY, $naryad::STATUS_FINISH, $naryad::STATUS_CHECKED])) {
                $clas='success';
            }
            if (in_array($naryad->status,[$naryad::STATUS_CANCEL])) {
                $clas='danger';
            }
            if (in_array($naryad->status,[$naryad::STATUS_FROM_SKLAD])) {
                $clas='info';
            }
            if ($model->is_new_scheme && !isset($naryadsid[$article])) {
                $clas='info';
                $status = $naryad::STATUS_FROM_SKLAD;
            }
            if ($model->is_new_scheme && $isShipped) {
                $clas='bg-shipped';
                $status = 'Отгружен';
            }
            if ($model->is_new_scheme && $isDeleted) {
                $clas='bg-deleted';
                $status = 'Позиция отменена';
            }
            ?>
            <tr class="items-order <?=$model->status?$clas:''?>">
                <td><?=$car?><br><small><i><?=trim($settext, ';')?></i></small></td>
                <td><?=$name?><br><small><i><?=$comment?></i></small></td>
                <td><?=$article?></td>
                <? if ($model->status):?>
                    <td>
                    <?=$naryad->locationstart
                    ? Html::a(Html::encode($naryad->locationstart->name), ['erp-location/view','id'=>$naryad->locationstart->id]).' <small>'.($naryad->location ? $naryad->location->name : 'Облако').'</small>'
                    : ($naryad->location ? Html::a(Html::encode($naryad->location->name), ['erp-location/view','id'=>$naryad->location->id]) : null);
                    ?>
                    </td>
                    <td><?=$status?> <br> <?= $naryad->id ? Html::a(Html::encode($naryad->logist_id), ['erp-naryad/view','id'=>$naryad->id],['target' => '_blank']) : 'UPN № ' . $upnStorage . ' был создан на складе. <br> ' . $person . ' <br>  ' . $dateStorage?></td>
                <?else:?>
                    <td><?=$model->reserve($barcode)?></td>
                    <td class="text-center"><?=$model->reserveNew($article)?></td>
                    <td class="text-center">
                        <div class="checkbox-custom checkbox-primary">
                            <input type="hidden" name="items[<?=$key?>]" value="1">
                            <input class="productionthis" type="checkbox" name="items[<?=$key?>]" value="0">
                            <label></label>
                        </div>
                    </td>
                <? endif; ?>
                    <td>
                        <?foreach ($model->recieveAnalogs($article) as $analog):?>
                            <? $json = Json::decode($analog->json);
                            $var = implode(",", $json['elements'])?>
                            <p class = "small"><?= $analog->analog->fullName."[ ".$var." ]"?></p>
                        <?endforeach?>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
            <tr></tr>
            <tr>
                <td colspan="5"
                    class="text-right lead"><?= Yii::t('app', 'Итого:') ?> <?= isset($total) ? $total : 0 ?></td>
            </tr>
        </table>
    </div>
<!--    --><?//= $model->status ? '' : Html::submitButton(Yii::t('app', 'Обработать'), ['class' => 'btn btn-outline btn-round btn-danger pull-right']) ?>
<?= Html::endForm();?>

<?= $model->status ? Html::a(Yii::t('app', 'Следующий не обработанный заказ'), ['next'], ['class' => 'btn btn-outline btn-round btn-success pull-right', 'style' => 'margin-right:5px;']) : '' ?>

<?= Html::a(Yii::t('app', 'Назад к списку'), ['index'], ['class' => 'btn btn-outline btn-round btn-default pull-right', 'style' => 'margin-right:5px;']) ?>

<?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>


<? $model->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => ' btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
    'data-toggle' => "tooltip",
    'data-original-title' => Yii::t('yii', 'Delete'),
    'data' => [
        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
        'method' => 'post',
    ],
]) ?>

<?php Yii::$app->view->registerJs('
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>

