<?php

use backend\modules\laitovo\models\ErpNaryad;
use yii\helpers\Html;
use backend\widgets\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use backend\modules\laitovo\models\ErpOrder;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $show string */
/* @var $countAll int */
/* @var $countOnProduction int */
/* @var $countOnPause int */
/* @var $pauseOrder object */
/* @var $accelerateOrder object */

$this->title = Yii::t('app', 'Заказы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');


Yii::$app->view->registerJs('
    $(\'#myModal\').on(\'shown.bs.modal\', function () {
      $(\'#barcode-scaner\').focus();
    });
    
    $(\'#myModal-2\').on(\'shown.bs.modal\', function () {
      $(\'#barcode-scaner-2\').focus();
    });

    setInterval(function(){ $(".reloadterminal").click(); }, 100000);
', \yii\web\View::POS_END);


$fields = [];
foreach ($model->fields as $key => $value) {
    $fields[] = [
        'attribute' => $value['fields']['id']['value'],
        'label' => $value['fields']['label']['value'],
        'format' => 'text',
        'value' => function ($data) use ($value) {
            return $data->json($value['fields']['id']['value']);
        },
    ];
}

?>
<div>
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>
&nbsp;
<?= Html::a(Yii::t('app', 'Последний не обработанный заказ'), ['next'], ['class' => 'btn btn-outline btn-round btn-success']) ?>

<?php if ( in_array(Yii::$app->user->getId(),[21,120,48]) ):?>
<?
Modal::begin([
    'header' => '<h2>Поиск остатков на складе по артикулу</h2><br>(Пример: FD-R-123-1-2)',
    'options' => [
        'id' => 'myModal-2',
    ],
    'size' => 'modal-lg',
    'toggleButton' => ['class' => 'btn btn-outline btn-round btn-info', 'label' => 'Проверить остатки на складе'],
]);

//Подгрузить представление для получения заявок

echo \yii\helpers\Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control','id' => 'barcode-scaner-2',
    'onchange' => '
        $.get( "'. Url::to(['check-state']) .'", {article: $(this).val()}, function( data ) {
         if (data.status == "info") {
             notie.alert(4,data.message,10);
         }else {
              notie.alert(2,"Непонятная ошибка, обратитесь к IT-службе",15);
         }
         
         $(\'#barcode-scaner-2\').focus();
         $(\'#barcode-scaner-2\').val("");
        });
    ']);

Modal::end();?>

<?//
//Modal::begin([
//    'header' => '<h2>Списание остатков со склада по штрихкоду UPN</h2>',
//    'options' => [
//        'id' => 'myModal',
//    ],
//    'size' => 'modal-lg',
//    'toggleButton' => ['class' => 'btn btn-outline btn-round btn-warning pull-right', 'label' => 'Списать со склада'],
//]);
//
////Подгрузить представление для получения заявок
//
//echo \yii\helpers\Html::input($type = 'text','Сканер штрихкода',$value = null, $options = ['class' => 'form-control','id' => 'barcode-scaner',
//    'onchange' => '
//        $.get( "'. Url::to(['write-off']) .'", {barcode: $(this).val()}, function( data ) {
//         if (data.status == "success") {
//             notie.alert(1,data.message,10);
//         }else if (data.status == "error") {
//              notie.alert(3,data.message,10);
//         }else {
//              notie.alert(2,"Непонятная ошибка, обратитесь к IT-службе",10);
//         }
//
//         $(\'#barcode-scaner\').focus();
//         $(\'#barcode-scaner\').val("");
//       });
//
//    ']);
//
//Modal::end();?>


</div>
<hr>
<div class="clearfix"></div>

<?php endif;?>

<?php Pjax::begin(['id' => 'pjax-order-list', 'timeout' => 5000]); ?>
<?= Html::a("Обновить", ['index'], ['class' => 'hidden reloadterminal']) ?>

<?php ActiveForm::begin([
    'id' => 'search-form',
    'action' => [''],
    'method' => 'post',
    'options' => ['data-pjax' => 1]
]); ?>

<div class="btn-group" id="rb-group-show" data-toggle="buttons">
    <label class="btn btn-default <?php if(!$show || $show == 'all'):?>active<?php endif;?>">
        <input onchange="$('.page-header-actions input[name=search]').val('');$('#search-form').submit();" type="radio" name="erpOrder_index_show" value="all" <?php if(!$show || $show == 'all'):?>checked<?php endif;?>> Все (<?=$countAll?>)
    </label>
    <label class="btn btn-default <?php if($show == 'onProduction'):?>active<?php endif;?>">
        <input onchange="$('.page-header-actions input[name=search]').val('');$('#search-form').submit();" type="radio" name="erpOrder_index_show" value="onProduction" <?php if($show == 'onProduction'):?>checked<?php endif;?>> Только на производстве (<?=$countOnProduction?>)
    </label>
    <label class="btn btn-default <?php if($show == 'onPause'):?>active<?php endif;?>">
        <input onchange="$('.page-header-actions input[name=search]').val('');$('#search-form').submit();" type="radio" name="erpOrder_index_show" value="onPause" <?php if($show == 'onPause'):?>checked<?php endif;?>> Только на паузе (<?=$countOnPause?>)
    </label>
</div>
<?= Html::hiddenInput('update_erpOrder_index_show',true); ?>

<?php ActiveForm::end(); ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['order_id', 'status', 'created_at'],
    'columns' => ArrayHelper::merge(array_filter([
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        [
            'attribute' => 'order_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['view', 'id' => $data->id]) : null;
            },
        ],
        'status',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
           'attribute' => 'sort',
           'value' => function($data){return ErpOrder :: prioritetName($data->sort);},
           'filter' => ErpOrder :: prioritets(),
        ],
        [
           'attribute' => 'countWorkOrders',
           'label' => 'Кол-во нарядов',
           'value' => function($data) {
                return ErpNaryad::find()->where(['order_id' => $data->id])->count();
            },
        ],
        !in_array(Yii::$app->user->getId(), [2, 27, 45, 21, 43, 46, 36, 173, 12]) ? null : [
            'class'  => 'yii\grid\ActionColumn',
            'header' => 'Выдача по акту',
            'template' => '{act}',
            'buttons' => [
                'act' => function ($url, $data) use ($pauseOrder) {
                    return Html::a(Yii::t('app', 'Выдать по акту'), ['erp/move-act-terminal','order' => $data->id], ['class' => 'btn btn-outline btn-round btn-success','target' => '_blank','data-pjax' => 0]);
                },
            ],
        ],
        !in_array(Yii::$app->user->getId(), [2, 27, 45, 21, 43, 46, 36, 173, 12]) ? null : [
            'class'  => 'yii\grid\ActionColumn',
            'header' => 'Выдача группой',
            'template' => '{act}',
            'buttons' => [
                'act' => function ($url, $data) use ($pauseOrder) {
                    return Html::a(Yii::t('app', 'Выдать группой'), ['erp/move-group-terminal','order' => $data->id], ['class' => 'btn btn-outline btn-round btn-success','target' => '_blank','data-pjax' => 0]);
                },
            ],
        ],
        [
            'class'  => 'yii\grid\ActionColumn',
            'header' => 'Пауза',
            'template' => '{pause}',
            'buttons' => [
                'pause' => function ($url, $data) use ($pauseOrder) {
                    if ($pauseOrder->isPaused($data)) {
                        return Html::button('<i class="fa fa-play"></i>', [
                            'class' => 'btn btn-outline btn-round btn-info',
                            'data'  => [
                                'id' => $data->id,
                                'toggle'  => 'tooltip',
                                'title'   => Yii::t('app', 'Возобновить')
                            ],
                            'onclick' => '
                                var button = $(this);
                                var id = $(this).data("id");
                                var handle_function = function () {
                                    button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:20px;\"></i>");
                                    $.ajax({
                                        url: "' . Url::to(['ajax-unpause']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите снять заказ с паузы?", "Да", "Нет", handle_function);
                            '
                        ]);
                    } else {
                        $canBePaused = $pauseOrder->canBePaused($data);
                        return Html::button('<i class="fa fa-pause" data-toggle="tooltip" data-title="' . ($canBePaused ? '' : Yii::t('app', 'Данный заказ не может быть остановлен')) . '"></i>', [
                            'class' => 'btn btn-outline btn-round btn-info',
                            'data'  => [
                                'id' => $data->id,
                                'toggle'  => 'tooltip',
                                'title'   => Yii::t('app', 'Остановить')
                            ],
                            'disabled' => !$canBePaused,
                            'onclick' => '
                                var button = $(this);
                                var id = $(this).data("id");
                                var handle_function = function () {
                                    button.prop("disabled", true).attr("title",  "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:20px;\"></i>");
                                    $.ajax({
                                        url: "' . Url::to(['ajax-pause']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите поставить заказ на паузу?", "Да", "Нет", handle_function);
                            '
                        ]);
                    }
                },
            ],
        ],
        !in_array(Yii::$app->user->getId(), [2, 21, 26, 27, 43, 46, 36, 55, 56, 45]) ? null : [
            'class'  => 'yii\grid\ActionColumn',
            'header' => 'Срочность',
            'template' => '{priority}',
            'buttons' => [
                'priority' => function ($url, $data) use ($accelerateOrder) {
                    $sort = $accelerateOrder->getNaryadsSort($data);
                    $canUpdatePriority = $accelerateOrder->canUpdatePriority($data);
                    return implode(' ', [
                        Html::button('<i class="fa fa-backward text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'Низкий') : Yii::t('app', 'Данный заказ не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'margin-right:10px;padding:3px;background-color:transparent;' . ($sort == 7 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 7
                            ],
                            'disabled' => !$canUpdatePriority || $sort == 7,
                            'onclick' => '
                                var button = $(this);
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    button.closest("td").data("toggle", "tooltip").attr("title",  "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"margin-left:30px;font-size:20px;\"></i>");
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность заказа?", "Да", "Нет", handle_function);
                            '
                        ]),
                        Html::button('<i class="fa fa-play text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'Обычный') : Yii::t('app', 'Данный заказ не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'margin-right:10px;padding:3px;background-color:transparent;' . ($sort == 6 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 6
                            ],
                            'disabled' => !$canUpdatePriority || $sort == 6,
                            'onclick' => '
                                var button = $(this);
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    button.closest("td").data("toggle", "tooltip").attr("title",  "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"margin-left:30px;font-size:20px;\"></i>");
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность заказа?", "Да", "Нет", handle_function);
                            '
                        ]),
                        Html::button('<i class="fa fa-forward text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'Срочный') : Yii::t('app', 'Данный заказ не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'margin-right:10px;padding:3px;background-color:transparent;' . ($sort == 4 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 4
                            ],
                            'disabled' => !$canUpdatePriority || $sort == 4,
                            'onclick' => '
                                var button = $(this);
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    button.closest("td").data("toggle", "tooltip").attr("title",  "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"margin-left:30px;font-size:20px;\"></i>");
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность заказа?", "Да", "Нет", handle_function);
                            '
                        ]),
                        Html::button('<i class="fa fa-step-forward text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'Большой заказ') : Yii::t('app', 'Данный заказ не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'margin-right:10px;padding:3px;background-color:transparent;' . ($sort == 3 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 3
                            ],
                            'disabled' => !$canUpdatePriority || $sort == 3,
                            'onclick' => '
                                var button = $(this);
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    button.closest("td").data("toggle", "tooltip").attr("title",  "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"margin-left:30px;font-size:20px;\"></i>");
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность заказа?", "Да", "Нет", handle_function);
                            '
                        ]),
                        Html::button('<i class="fa fa-fast-forward text-info" data-toggle="tooltip" data-title="' . ($canUpdatePriority ? Yii::t('app', 'TopSpeed') : Yii::t('app', 'Данный заказ не может менять срочность')) . '"></i>', [
                            'class'    => 'btn btn-sm',
                            'style'    => 'padding:3px;background-color:transparent;' . ($sort == 2 ? 'font-size:20px;' : ''),
                            'data'     => [
                                'id'   => $data->id,
                                'sort' => 2
                            ],
                            'disabled' => !$canUpdatePriority || $sort == 2,
                            'onclick' => '
                                var button = $(this);
                                var id = $(this).data("id");
                                var sort = $(this).data("sort");
                                var handle_function = function () {
                                    button.closest("td").data("toggle", "tooltip").attr("title",  "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"margin-left:30px;font-size:20px;\"></i>");
                                    $.ajax({
                                        url: "' . Url::to(['ajax-update-priority']) . '",
                                        dataType: "json",
                                        data: {
                                            "id": id,
                                            "sort": sort,
                                        },
                                        success: function (data) {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            if (!data.status) {
                                                notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                                                return;
                                            }
                                            notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                                        },
                                        error: function() {
                                            $.pjax.reload({container : "#pjax-order-list"});
                                            notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                                        }
                                    });
                                };
                                notie.confirm("Хотите изменить срочность заказа?", "Да", "Нет", handle_function);
                            '
                        ]),
                        !($canUpdatePriority && !$sort) ? '' : '<i class="fa fa-comment" style="margin-left:10px;color:#ccd5db;" data-toggle="tooltip" data-title="' . Yii::t('app', 'В заказе присутствуют наряды с разной срочностью') . '"></i>'
                    ]);
                },
            ],
        ],
        // 'author_id',
        // 'updater_id',
        // 'json:ntext',
    ]), $fields, [
        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ]),
]); ?>
<?php Pjax::end(); ?>

<?php Yii::$app->view->registerJs('
    setInterval(function() { 
        $.pjax.reload({container : "#pjax-order-list"});
    }, 360*1000);

    $("#pjax-order-list").on("pjax:end", function() {
      $("#pjax-order-list [data-toggle=tooltip]").tooltip();
    })
', \yii\web\View::POS_END);
?>

