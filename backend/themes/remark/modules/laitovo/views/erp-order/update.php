<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpOrder */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заказы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('yii', 'Update');

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
