<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpOrder */
/* @var $form yii\widgets\ActiveForm */

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

Yii::$app->view->registerJs('
    $(document).ready(function() {
        $(\'#tbody-items .name-editable\').editable({
            emptytext:"Укажите название товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .article-editable\').editable({
            emptytext:"Укажите артикул товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .quantity-editable\').editable({
            emptytext:"Укажите кол-во",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .comment-editable\').editable({
            emptytext:"Комментарий",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).parent().next().val(newValue);
                parseitemsjson();
            }
        });
    });

    function parseitemsjson () {
        var items =[];
        var totalcount =0;
        $(\'#tbody-items .items-order\').each(function( index, value ) {
            itemjson=$(this).find(\'.itemjson\').serializeArray();
            var obj ={};
            $.each(itemjson, function( index1, value1 ) {
                obj[value1.name]=value1.value;
            });
            totalcount=parseFloat(totalcount+parseFloat(obj.quantity));
            items.push(obj);
        });
        $(\'#erporder-items\').val(JSON.stringify(items));
        $(\'#erporder-totalcount\').text(totalcount);
    }

    $("body").on("click","#tbody-items .items-order .delete-orderitem",function(e){
        $(this).parent().parent().remove();
        parseitemsjson();
        e.preventDefault();
    });

    $("body").on("click","#add-orderitem",function(e){
        $(".add-item-tr").clone().appendTo("#tbody-items");

        $(\'#tbody-items .add-item-tr .name-editable\').editable({
            emptytext:"Укажите название товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .article-editable\').editable({
            emptytext:"Укажите артикул товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .quantity-editable\').editable({
            emptytext:"Укажите кол-во",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .comment-editable\').editable({
            emptytext:"Комментарий",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).parent().next().val(newValue);
                parseitemsjson();
            }
        });

        $("#tbody-items .add-item-tr").removeClass("hidden add-item-tr");
        parseitemsjson();
        e.preventDefault();
    });

    var barcode=$(\'#erporder-barcode\');

    function searchproduct(e)
    {
        $.get("' . Url::to(['search']) . '",{
            barcode: barcode.val()
        },function(data){

            if (data.status=="success"){
                $(".add-item-tr").clone().appendTo("#tbody-items");

                $(\'#tbody-items .add-item-tr .name-editable\').text(data.name);
                $(\'#tbody-items .add-item-tr .itemjson[name="name"]\').val(data.name)
                $(\'#tbody-items .add-item-tr .name-editable\').editable({
                    emptytext:"Укажите название товара",
                    showbuttons: false,
                    onblur: "submit",
                    success: function(response, newValue) {
                        $(this).next().next().val(newValue);
                        parseitemsjson();
                    }
                });
                $(\'#tbody-items .add-item-tr .car-editable\').text(data.car)
                $(\'#tbody-items .add-item-tr .itemjson[name="car"]\').val(data.car)
                $(\'#tbody-items .add-item-tr .itemjson[name="lekalo"]\').val(data.lekalo)
                $(\'#tbody-items .add-item-tr .itemjson[name="natyag"]\').val(data.natyag)
                $(\'#tbody-items .add-item-tr .itemjson[name="visota"]\').val(data.visota)
                $(\'#tbody-items .add-item-tr .itemjson[name="magnit"]\').val(data.magnit)
                $(\'#tbody-items .add-item-tr .itemjson[name="hlyastik"]\').val(data.hlyastik)

                $(\'#tbody-items .add-item-tr .article-editable\').text(data.article)
                $(\'#tbody-items .add-item-tr .itemjson[name="article"]\').val(data.article)
                $(\'#tbody-items .add-item-tr .itemjson[name="barcode"]\').val(data.barcode)
                $(\'#tbody-items .add-item-tr .article-editable\').editable({
                    emptytext:"Укажите артикул товара",
                    showbuttons: false,
                    onblur: "submit",
                    success: function(response, newValue) {
                        $(this).next().next().val(newValue);
                        parseitemsjson();
                    }
                });
                $(\'#tbody-items .add-item-tr .quantity-editable\').editable({
                    emptytext:"Укажите кол-во",
                    showbuttons: false,
                    onblur: "submit",
                    success: function(response, newValue) {
                        $(this).next().next().val(newValue);
                        parseitemsjson();
                    }
                });
                $(\'#tbody-items .add-item-tr .comment-editable\').text(data.comment);
                $(\'#tbody-items .add-item-tr .itemjson[name="comment"]\').val(data.comment)
                $(\'#tbody-items .add-item-tr .comment-editable\').editable({
                    emptytext:"Комментарий",
                    showbuttons: false,
                    onblur: "submit",
                    success: function(response, newValue) {
                        $(this).parent().next().val(newValue);
                        parseitemsjson();
                    }
                });

                $("#tbody-items .add-item-tr").removeClass("hidden add-item-tr");
                parseitemsjson();

                toastr.success(data.message);
            } else {
                notie.alert(3,data.message);
            }

        },"json");

        e.preventDefault();
    }
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Z0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erporder-barcode" && e.which == 13){
                searchproduct(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchproduct(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });

    ', \yii\web\View::POS_END);


?>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Автомобиль</th>
            <th>Товар</th>
            <th>Артикул</th>
            <th>Кол-во</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="tbody-items">
        <? if ($model->json('items')) foreach ($model->json('items') as $key => $row):
            $name = isset($row['name']) ? $row['name'] : '';
            $car = isset($row['car']) ? $row['car'] : null;
            $lekalo = isset($row['lekalo']) ? $row['lekalo'] : null;
            $natyag = isset($row['natyag']) ? $row['natyag'] : null;
            $visota = isset($row['visota']) ? $row['visota'] : null;
            $magnit = isset($row['magnit']) ? $row['magnit'] : null;
            $hlyastik = isset($row['hlyastik']) ? $row['hlyastik'] : null;
            $settext = isset($row['settext']) ? $row['settext'] : null;
            $article = isset($row['article']) ? $row['article'] : null;
            $barcode = isset($row['barcode']) ? $row['barcode'] : null;
            $quantity = isset($row['quantity']) ? $row['quantity'] : null;
            $comment = isset($row['comment']) ? $row['comment'] : null;
            $total = isset($total) ? $quantity + $total : $quantity;
            ?>
            <tr class="items-order">
                <td>
                    <a href="#" class="car-editable" data-type="text"><?= $car ?></a>
                    <input class="itemjson" type="hidden" name="car" value='<?= $car ?>'>
                    <input class="itemjson" type="hidden" name="lekalo" value='<?= $lekalo ?>'>
                    <input class="itemjson" type="hidden" name="natyag" value='<?= $natyag ?>'>
                    <input class="itemjson" type="hidden" name="visota" value='<?= $visota ?>'>
                    <input class="itemjson" type="hidden" name="magnit" value='<?= $magnit ?>'>
                    <input class="itemjson" type="hidden" name="hlyastik" value='<?= $hlyastik ?>'>
                    <br>
                    <small><a href="#" class="settext-editable" data-type="text"><?= trim($settext, ';') ?></a></small>
                    <input class="itemjson" type="hidden" name="settext" value='<?= $settext ?>'>
                </td>
                <td>
                    <a href="#" class="name-editable" data-type="text"><?= $name ?></a>
                    <input class="itemjson" type="hidden" name="name" value='<?= $name ?>'><br>
                    <small><a href="#" class="comment-editable" data-type="text"><?= $comment ?></a></small>
                    <input class="itemjson" type="hidden" name="comment" value='<?= $comment ?>'>
                </td>
                <td>
                    <a href="#" class="article-editable" data-type="text"><?= $article ?></a>
                    <input class="itemjson" type="hidden" name="article" value='<?= $article ?>'>
                    <input class="itemjson" type="hidden" name="barcode" value='<?= $barcode ?>'>
                </td>
                <td>
                    <a href="#" class="quantity-editable" data-type="text"><?= $quantity ?></a>
                    <input class="itemjson" type="hidden" name="quantity" value='<?= $quantity ?>'>
                </td>
                <td>
                    <a href="#" class="text-danger delete-orderitem"><i class="wb wb-close"></i></a>
                </td>
            </tr>
        <? endforeach ?>
        </tbody>
        <tr class="items-order hidden add-item-tr">
            <td>
                <a href="#" class="car-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" name="car" value=''>
                <input class="itemjson" type="hidden" name="lekalo" value=''>
                <input class="itemjson" type="hidden" name="natyag" value=''>
                <input class="itemjson" type="hidden" name="visota" value=''>
                <input class="itemjson" type="hidden" name="magnit" value=''>
                <input class="itemjson" type="hidden" name="hlyastik" value=''>
                <br>
                <small><a href="#" class="settext-editable" data-type="text"></a></small>
                <input class="itemjson" type="hidden" name="settext" value=''>
            </td>
            <td>
                <a href="#" class="name-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" name="name" value=''><br>
                <small><a href="#" class="comment-editable" data-type="text"></a></small>
                <input class="itemjson" type="hidden" name="comment" value=''>
            </td>
            <td>
                <a href="#" class="article-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" name="article" value=''>
                <input class="itemjson" type="hidden" name="barcode" value=''>
            </td>
            <td>
                <a href="#" class="quantity-editable" data-type="text">1</a>
                <input class="itemjson" type="hidden" name="quantity" value='1'>
            </td>
            <td>
                <a href="#" class="text-danger delete-orderitem"><i class="wb wb-close"></i></a>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="text-right lead"><?= Yii::t('app', 'Итого:') ?></td>
            <td class="lead" id="erporder-totalcount"><?= isset($total) ? $total : 0 ?></td>
            <td></td>
        </tr>
    </table>
</div>


<?= Html::a('<span class="icon wb-plus"></span>', '#', ['class' => 'pull-left btn btn-icon btn-outline btn-round btn-default', 'id' => 'add-orderitem', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('yii', 'Добавить товар')]) ?>
<div class="pull-left col-md-3">
    <input type="text" id="erporder-barcode" placeholder="Поиск..." class="form-control">
</div>

<div class="clearfix"></div>
<p></p>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'items')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'order_id')->textInput() ?>

<?= FormBuilder::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'inputfields',
    'render' => $model->fieldsrender,
]); ?>

<?if (Yii::$app->user->getId() == 43 || Yii::$app->user->getId() == 44): ?> 
    <?= $form->field($model, 'sort')->dropDownList($model->prioritets) ?>
<? endif;?> 

<?= $form->field($model, 'status')->dropDownList($model->statuses) ?>


<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>


<?php ActiveForm::end(); ?>
