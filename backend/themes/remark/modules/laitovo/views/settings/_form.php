<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Config */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?= FormBuilder::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'fields',
    'render' => $model->config->getFieldsrender($model->config->id),
]); ?>

<div class="form-group">
    <?= Html::submitButton($model->config->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>


<?php ActiveForm::end(); ?>
