<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use backend\modules\laitovo\models\ErpLocation;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Терминал');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);


Yii::$app->view->registerJs('
    $("#erp-terminal-form").on("ajaxComplete", function (event, messages) {
        document.location.reload();
    });

    setInterval(function(){ $(".reloadterminal").click();}, 60*1000);

    var barcode=$(\'#erpterminal-barcode\');

    function searchterminal(e)
    {
        $.get("' . Url::to(['search']) . '",{
            barcode: barcode.val()
        },function(data){

            $(".reloadterminal").click();
            if (data.status=="success"){
                notie.alert(1,data.message,15);
                if (data.file) {
                    printUrl(data.file);
                } else if (data.manual) {
                    printUrl("' . Url::to(['erp-naryad/print-manual-new']) . '?id="+data.manual);
                }
            } else {
                notie.alert(3,data.message,15);
            }

        },"json");

        e.preventDefault();
    }
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erpterminal-barcode" && e.which == 13){
                searchterminal(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchterminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });

    ', \yii\web\View::POS_END);


?>
<div class="pull-left">
    <input type="text" id="erpterminal-barcode" placeholder="Поиск..." class="form-control">
</div>

<div class="clearfix"></div>
<p></p>

<!--
<?php $form = ActiveForm::begin([
    'id' => 'erp-terminal-form',
    'enableAjaxValidation' => true,
]); ?>


<?php ActiveForm::end(); ?>
-->

<?php Pjax::begin(['timeout' => 5000]); ?>


<?= Html::a("Обновить", ['terminal'], ['class' => 'hidden reloadterminal']) ?>


<?php Pjax::end(); ?>


