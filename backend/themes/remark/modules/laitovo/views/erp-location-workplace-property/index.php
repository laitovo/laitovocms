<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\models\ErpLocationWorkplacePropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Erp Location Workplace Properties';
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-property-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Erp Location Workplace Property', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type_id',
            'title',
            'created_at',
            'updated_at',
            // 'author_id',
            // 'updater_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
