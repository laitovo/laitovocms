<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */
/* @var $model backend\modules\laitovo\models\ErpJobType */

$this->title = Yii::t('app', 'Добавить коэффициент по участку');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'КПД работников по участкам'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
