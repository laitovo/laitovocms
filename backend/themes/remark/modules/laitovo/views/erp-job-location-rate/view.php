<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpJobLocationRate */
/* @var $location backend\modules\laitovo\models\ErpLocation */

$this->title = $model->user->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'КПД работников по участкам'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => $model->location ? Html::a(Html::encode($model->location->name), ['erp-location/view', 'id' => $model->location_id]) : null,
        ],
        [
            'attribute' => 'user_id',
            'format' => 'html',
            'value' => $model->user ? Html::a(Html::encode($model->user->name), ['erp-user/view', 'id' => $model->user_id]) : null,
        ],
        'rate',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => $model->author ? $model->author->name : null,
        ],
    ],
]) ?>

<?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>


<?= $model->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
    'data-toggle' => "tooltip",
    'data-original-title' => Yii::t('yii', 'Delete'),
    'data' => [
        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
        'method' => 'post',
    ],
]) ?>