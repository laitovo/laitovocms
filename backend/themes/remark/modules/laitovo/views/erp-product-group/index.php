<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\models\ErpProductGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы продуктов';
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>
<div class="erp-product-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить группу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'show' => ['id','title','location_id','author_id','created_at'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'location_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->location_id ? $data->location->name : null;
                },
            ],
            'title',
            [
                'attribute' => 'Количество продуктов',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->getProductsCount();
                },
            ],
            'created_at:datetime',
            [
                'attribute' => 'author_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->author_id ? $data->author->name : null;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updater_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->updater_id ? $data->updater->name : null;
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
</div>
