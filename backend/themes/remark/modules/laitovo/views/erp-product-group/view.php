<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\GridView;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpProductGroup */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Группы продуктов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>
<div class="erp-product-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'title',
            'location.name',
            'created_at:datetime',
            'author.name',
            'updated_at:datetime',
            'updater.name',
        ],
    ]) ?>
    <div class="col-md-6">
    <?= Html::a('Убрать все', ['erp-product-group/un-link-all','id' => $model->id], 
                                [
                                    'title' => Yii::t('app', 'Убрать из группы'),
                                    'class' => 'btn btn-info ',
                                    'data' => [
                                        'confirm' => 'Вы уверены чт хотите убрать все продукты из группы? Вернуть данное состояние будет невозможно!',
                                        'method' => 'post',
                                    ],
                                ]);?>
    <h4 id='tables'>Продукты в группе</h4>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'show' => ['id','title','location_id','author_id','created_at'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'created_at:datetime',
            [
                'attribute' => 'author_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->author_id ? $data->author->name : null;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updater_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->updater_id ? $data->updater->name : null;
                },
            ],

           [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{today_action}',
                'buttons' => [
                                'today_action' => function ($url, $data) use ($model) {
                                return Html::a('<span class="glyphicon glyphicon-check"></span>', ['erp-product-group/un-link','id' => $model->id,'product_id' => $data->id ], 
                                [
                                    'title' => Yii::t('app', 'Убрать из группы'),
                                ]);
                            }
                        ],
            ],
        ],
    ]); ?>
    </div>
    <div class="col-md-6">
    <?= Html::a('Добавить все', ['erp-product-group/link-all','id' => $model->id], 
            [
                'title' => Yii::t('app', 'Добавить все в группы'),
                'class' => 'btn btn-info ',
                'data' => [
                    'confirm' => 'Вы уверены чт хотите добавить все продукты в группу? Вернуть данное состояние будет невозможно!',
                    'method' => 'post',
                ],
            ]);?>
    <h4>Продукты для добавления</h4>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider2,
        // 'filterModel' => $searchModel,
        'show' => ['id','title','location_id','author_id','created_at'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'created_at:datetime',
            [
                'attribute' => 'author_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->author_id ? $data->author->name : null;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updater_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->updater_id ? $data->updater->name : null;
                },
            ],
           [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{today_action}',
                'buttons' => [
                                'today_action' => function ($url, $data) use ($model) {
                                return Html::a('<span class="glyphicon glyphicon-unchecked"></span>', ['erp-product-group/link','id' => $model->id,'product_id' => $data->id ], 
                                [
                                    'title' => Yii::t('app', 'Добавить в группу'),
                                ]);
                            }
                        ],
            ],
        ],
    ]); ?>
    </div>
  
</div>
