<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\modules\laitovo\models\ErpLocation;
/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpProductGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-product-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location_id')->widget(Select2::classname(), [
        'theme' => Select2::THEME_BOOTSTRAP,
        'data' => ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Выберите участок ...'],
        'language' => 'ru',
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]) ?>

    <!-- <?= $form->field($model, 'created_at')->textInput() ?> -->

    <!-- <?= $form->field($model, 'author_id')->textInput() ?> -->

    <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

    <!-- <?= $form->field($model, 'updater_id')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
