<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpNaryad;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocation */
/* @var $naryadsProvider yii\data\ActiveDataProvider */
/* @var $usersProvider yii\data\ActiveDataProvider */
/* @var $usersRateProvider yii\data\ActiveDataProvider */

$this->title = 'Диспетчер - склад';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Участки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>

<? if (Yii::$app->request->get('user')) { ?>
    <?= Html::a(Html::decode($user = Yii::$app->request->get('user')),
        ['erp-user/view', 'id' => (ErpUser::find()->where(['name' => $user])->one()->id)], ['class' => 'lead']); ?>
<? } else { ?>
    <h4>Наряды</h4>
<? } ?>
<? if (isset($literals) && $literals):
    foreach ($literals as $key => $value):?>
        <span><?= "<span style = 'color:black'>".$value['literal']." = <span style = 'color:red'>".$value['count']."</span>" ?>..../</span>
    <? endforeach;?>
<? endif;?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $naryadsProvider,
    'show' => ['id', 'article', 'order_id', 'status', 'windowType', 'window', 'tkan'],
    'salt' => 'NSOvcg',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-naryad/view', 'id' => $data->id]) : null;
            },
        ],
        'article',
        [
          'attribute'=>'windowType',
          'label'=>'Название продукта',
          'filter'=> ErpNaryad::filterTypeStatic(),
          'value'=>function ($data) {
           return ErpNaryad::windowTypeStatic($data->article);
            },
        ],
        [
          'attribute'=>'window',
          'label'=>'Оконный проем',
          'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
          'value'=>function ($data) {
            return ErpNaryad::windowStatic($data->article);
            },
        ],
        [
          'attribute'=>'tkan',
          'label'=>'Тип ткани',
          'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
          'value'=>function ($data) {
           return ErpNaryad::tkanStatic($data->article);
            },
        ],
        [
            'attribute' => 'order_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view', 'id' => $data->order->id]) : null;
            },
        ],
        [
            'attribute' => 'scheme_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view', 'id' => $data->scheme->id]) : null;
            },
        ],
        [
            'attribute' => 'user_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->user ? Html::a(Html::encode($data->user->name), ['erp-user/view', 'id' => $data->user->id]) : null;
            },
        ],
        [
            'attribute'=>'status',
            'format'=> 'html',
            'filter' => false,
        ],  
        [
            'attribute'=>'created_at',
            'label' => 'Создание наряда',
            'value'=> function ($data) {
                return $data->updated_at ? Yii::$app->formatter->asDatetime($data->created_at): null;
            },
        ],
        [
            'attribute'=>'updated_at',
            'label' => 'Приход на участок',
            'value'=> function ($data) {
                return $data->updated_at ? Yii::$app->formatter->asDatetime($data->updated_at): null;
            },
        ],
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
            'attribute'=>'literal',
            'label'=>'Литера',
            'value'=>function ($data) {
                return $data->literal ? $data->getValueLiteral() : null;
            },
        ],
         [
            'attribute' => 'sort',
            'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
            'filter' => ErpNaryad :: prioritets(),
          ],
        // ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
