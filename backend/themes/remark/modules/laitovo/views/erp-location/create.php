<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\laitovo\ErpLocation */


$this->title = Yii::t('app', 'Добавить участок');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Участки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

