<?php

use backend\modules\laitovo\models\ErpNaryad;
use common\models\laitovo\ErpNaryadLog;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use common\models\laitovo\ErpUser;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocation */
/* @var $naryadsProvider yii\data\ActiveDataProvider */
/* @var $usersProvider yii\data\ActiveDataProvider */
/* @var $usersRateProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Участки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>

<!-- <?= print_r($naryadsProvider) ?> -->
<h4>Наряды</h4>

<?php
$content = '';
$content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: black; color: white;" title="Всего">Всего сдано</span>';
$content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: green;" title="Стандартные шторки">Стандартных шторок</span>';
$content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: black;" title="Don\'t look и бескаркасники">Don\'t look и бескаркасники</span>';
$content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: blue;" title="Органайзеры">Органайзеры</span>';
$content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: red;" title="Прочее">Прочее</span>';
echo $content;
?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $naryadsProvider,
    'show' => ['user', 'count', 'ready'],
    'salt' => 'NSOvcg',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'user',
            'format' => 'html',
            'label' => 'Работник',
            'value' => function ($data) {
                return ErpUser::getNameByName($data['user']);
            },
        ],
        // 'article',
        [
            'attribute' => 'count',
            'format' => 'html',
            'label' => 'Количество выполненых нарядов',
            'value' => function ($data) {
                if ($intCount = $data['count']) {
                    return Html::a($intCount, ['erp-location/view-throw-location-by-date-user-list',
                        'location' => Yii::$app->request->get('location'),
                        'user' => $data['user'],
                        'dFrom' => Yii::$app->request->get('dFrom'),
                        'dTo' => Yii::$app->request->get('dTo'),
                        'af' => Yii::$app->request->get('af')]);
                } else {
                    return $intCount;
                }
            },
        ],
        [
            'attribute' => 'ready',
            'label' => 'Сдано нарядов',
            'format' => 'raw',
            'value' => function ($data) {

                $locationId = Yii::$app->request->get('location');
                $dateFrom = Yii::$app->request->get('dFrom');
                $dateTo = Yii::$app->request->get('dTo') + 60*60*24 - 1;
                $userId = $data['workerId'];
                if (!$userId) return '';
                if (Yii::$app->request->get('af')) return $data['count'];

                $naryads =  ErpNaryadLog::find()
                    ->select('workOrderId')
                    ->where(['locationFromId' => $locationId])
                    ->andWhere(['workerId' => $userId])
                    ->andWhere(['>=','date',$dateFrom])
                    ->andWhere(['<=','date',$dateTo])
                    ->column();

                $standard = ErpNaryad::find()
                    ->select('id')
                    ->where(['id' => $naryads])
                    ->andWhere(['or',
                        ['like','article','FD-%',false],
                        ['like','article','BW-%',false],
                        ['like','article','RD-%',false],
                        ['like','article','RV-%',false],
                        ['like','article','FV-%',false],
                    ])
                    ->andWhere(['and',
                        ['not like','article','FD-%-%-5-%',false],
                        ['not like','article','FD-%-%-4-%',false],
                        ['not like','article','RD-%-%-5-%',false],
                        ['not like','article','RD-%-%-4-%',false],
                        ['not like','article','RV-%-%-5-%',false],
                        ['not like','article','RV-%-%-4-%',false],
                        ['not like','article','FV-%-%-5-%',false],
                        ['not like','article','FV-%-%-4-%',false],
                        ['not like','article','BW-%-%-5-%',false],
                        ['not like','article','BW-%-%-4-%',false],
                    ])
                    ->column();

                $notStandard = ErpNaryad::find()
                    ->select('id')
                    ->where(['id' => $naryads])
                    ->andWhere(['or',
                        ['like','article','FD-%-%-5-%',false],
                        ['like','article','FD-%-%-4-%',false],
                        ['like','article','RD-%-%-5-%',false],
                        ['like','article','RD-%-%-4-%',false],
                        ['like','article','RV-%-%-5-%',false],
                        ['like','article','RV-%-%-4-%',false],
                        ['like','article','FV-%-%-5-%',false],
                        ['like','article','FV-%-%-4-%',false],
                        ['like','article','BW-%-%-5-%',false],
                        ['like','article','BW-%-%-4-%',false],
                        ['like','article','FW-%',false],
                    ])
                    ->column();

                $organaizer = ErpNaryad::find()
                    ->select('id')
                    ->where(['id' => $naryads])
                    ->andWhere(['or',
                        ['like','article','OT-1189-%',false],
                        ['like','article','OT-1190-%',false],
                    ])
                    ->column();

                $other = ErpNaryad::find()
                    ->select('id')
                    ->where(['id' => $naryads])
                    ->andWhere(['like','article','OT-%',false])
                    ->andWhere(['and',
                        ['not like','article','OT-1189-%',false],
                        ['not like','article','OT-1190-%',false],
                    ])
                    ->column();

                $content = '';
                $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: black; color: white;" title="Всего">' . count($naryads) .'</span>';
                $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: green;" title="Стандартные шторки">' . count($standard) .'</span>';
                $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: black;" title="Don\'t look и бескаркасники">' . count($notStandard) .'</span>';
                $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: blue;" title="Органайзеры">' . count($organaizer) .'</span>';
                $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: red;" title="Прочее">' . count($other) .'</span>';
                return $content;
            },
        ],

        // [
        //     'attribute'=>'scheme_id',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
        //     },
        // ],
        // [
        //     'attribute'=>'user_id',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return $data->user ? Html::a(Html::encode($data->user->name), ['erp-user/view','id'=>$data->user->id]) : null;
        //     },
        // ],        'status',
        // 'created_at:datetime',
        // [
        //     'attribute'=>'author_id',
        //     'value'=>function ($data) {
        //         return $data->author ? $data->author->name : null;
        //     },
        // ],

        // ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
