<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocation */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'type')->dropDownList($model->types)?>
<?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    <?= $model->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
        'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>

</div>

<?php ActiveForm::end(); ?>

    