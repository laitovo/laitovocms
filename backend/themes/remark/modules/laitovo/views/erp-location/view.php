<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpNaryad;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocation */
/* @var $naryadsProvider yii\data\ActiveDataProvider */
/* @var $usersProvider yii\data\ActiveDataProvider */
/* @var $usersRateProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Участки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        // 'id',
        [
            'attribute' => 'barcode',
            'format' => 'raw',
            'value' => Html::a($generator->getBarcode($model->barcode, $generator::TYPE_CODE_128), ['print', 'id' => $model->id], ['target' => '_blank']),
        ],
        'name',
        'type',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => $model->author ? $model->author->name : null,
        ],
        // 'updated_at',
        // 'updater_id',
        // 'json:ntext',
    ],
]) ?>

<!-- <h4>Работники</h4>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $usersProvider,
    'show' => ['name'],
    'salt' => '3KWLtQ',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'barcode',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a(Html::img("http://barcode.tec-it.com/barcode.ashx?data=" . $data->barcode), ['print', 'id' => $data->id], ['target' => '_blank']);
            },
        ],
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-user/view', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute'=>'status',
            'format'=> 'html',
            'filter' => false,
        ],  
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
                    
        // 'updated_at',
        // 'updater_id',
        // 'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'erp-user',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
 -->
<h4>Рабочие места на участке</h4>
<?= Html::a('<i class="icon wb-plus"></i>', [Yii::$app->urlManager->createUrl('laitovo/erp-location-workplace/create'),'from' => 'location','id' => $model->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover','id' => 'workplaces'],
    'dataProvider' => $workplaceProvider,
    'show' => ['type','location_number','user_id'],
    'salt' => '3KWLtQdsad',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        // 'id',
        'location_number',
        // 'type',
        [
            'attribute' => 'user_id',
            'label' => 'Сотрудник',
            'format' => 'html',
            'value' => function ($data) {
                return $data->user ? $data->user->name : null;
            },
        ],
        // 'updated_at',
        // 'updater_id',
        // 'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {time}',
            'buttons' => [
                'delete' => function ($url, $data) use ($model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
                        Yii::$app->urlManager->createUrl(['/laitovo/erp-location-workplace/delete-from-location','id' => $data->id,'from' => $model->id]), 
                    [
                        'title' => Yii::t('app', 'Изменить расписание рабочего места'),
                        'class' => 'deleteconfirm',
                        'data' => [
                            'confirm' => 'Вы уверены, что хотите удалить данное рабочее место ?',
                            'method' => 'post',
                        ],
                    ]);
                },

                'time' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-time"></span>', 
                        Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace-plan/create-for-workplace','workplace' => $model->id]), 
                    [
                        'title' => Yii::t('app', 'Изменить расписание рабочего места'),
                    ]);
                }
            ],
            'controller' => 'erp-location-workplace',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>

<h4>Наряды на участке</h4>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $naryadsProvider,
    'filterModel' => $model,
    'show' => ['id', 'article', 'order_id', 'status', 'window', 'windowType', 'tkan'],
    'salt' => 'NSOvcg',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-naryad/view', 'id' => $data->id]) : null;
            },
        ],
        'article',
        [
        'attribute'=>'window',
        'label'=>'Оконный проем',
       'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
        'value'=>function ($data) {
            return ErpNaryad::windowStatic($data->article);
        },
    ],
         [
        'attribute'=>'windowType',
        'label'=>'Название продукта',
        'filter'=> ErpNaryad::filterTypeStatic(),
        'value'=>function ($data) {
            return ErpNaryad::windowTypeStatic($data->article);
        },
    ],
                 [
        'attribute'=>'tkan',
        'label'=>'Тип ткани',
        'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
        'value'=>function ($data) {
            return ErpNaryad::tkanStatic($data->article);
        },
    ],
        [
            'attribute' => 'order_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view', 'id' => $data->order->id]) : null;
            },
        ],
        [
            'attribute' => 'scheme_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view', 'id' => $data->scheme->id]) : null;
            },
        ],
        [
            'attribute' => 'user_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->user ? Html::a(Html::encode($data->user->name), ['erp-user/view', 'id' => $data->user->id]) : null;
            },
        ],
        [
            'attribute'=>'status',
            'format'=> 'html',
            'filter' => false,
        ],  
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
          [
            'attribute' => 'sort',
            'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
            'filter' => ErpNaryad :: prioritets(),
          ],

        // ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>

<h4>В очереди на выдачу</h4>
<?= GridView::widget([
    'tableOptions'=>['class'=>'table table-hover'],
    'dataProvider' => $startnaryadsProvider,
    'filterModel' => $model,
    'show'=>['id','article','order_id','status', 'window', 'windowType', 'tkan'],
    'salt'=>'GEISwG',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute'=>'id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-naryad/view','id'=>$data->id]) : null;
            },
        ],
        'article',
                    
                     [
        'attribute'=>'window',
        'label'=>'Оконный проем',
       'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
        'value'=>function ($data) {
            return ErpNaryad::windowStatic($data->article);
        },
    ],
         [
        'attribute'=>'windowType',
        'label'=>'Название продукта',
        'filter'=> ErpNaryad::filterTypeStatic(),
        'value'=>function ($data) {
            return ErpNaryad::windowTypeStatic($data->article);
        },
    ],
                 [
        'attribute'=>'tkan',
        'label'=>'Тип ткани',
        'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
        'value'=>function ($data) {
            return ErpNaryad::tkanStatic($data->article);
        },
    ],
        [
            'attribute'=>'order_id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view','id'=>$data->order->id]) : null;
            },
        ],
        [
            'attribute'=>'scheme_id',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view','id'=>$data->scheme->id]) : null;
            },
        ],
        [
            'attribute'=>'status',
            'format'=> 'html',
            'filter' => false,
        ],            
     
        'created_at:datetime',
        [
            'attribute'=>'author_id',
            'value'=>function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
                    [
            'attribute' => 'sort',
            'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
            'filter' => ErpNaryad :: prioritets(),
          ], 

        // ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
