<?php

use backend\modules\laitovo\models\ToolFile;
use yii\widgets\ActiveForm;

/**
 * @var $model ToolFile
 * @var $result string
 */

$this->render('../menu');

$this->title = Yii::t('app', 'Файл для станка');
$this->params['breadcrumbs'][] = $this->title;
$url = \yii\helpers\Url::toRoute('/laitovo/tool/ajax-file');
$js = <<<JS
    $( "#target" ).submit(function( event ) {
        event.preventDefault();
        if (window.FormData === undefined) {
            alert('В вашем браузере FormData не поддерживается');
        } else {
            var formData = new FormData();
            formData.append('ToolFile[imageFile]', $("#toolfile-imagefile")[0].files[0]);
            formData.append('ToolFile[degreeFactor]', $("#toolfile-degreefactor").val());
            formData.append('ToolFile[lineFactor]', $("#toolfile-linefactor").val());
        
            $.ajax({
                type: "POST",
                url:  '$url',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                dataType : 'json',
                success: function(msg){
                    if (msg.error == '') {
                        $("#result-container").show();
                        $('#result-html').html(msg.success);
                    } else {
                        $('#result').html(msg.error);
                    }
                }
            });
        }
    });
JS;

$this->registerJs($js, \yii\web\View::POS_END);

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'target']]) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>
    <?= $form->field($model, 'degreeFactor')->textInput(['type' => 'number', 'step' => '0.001', 'min' => '0.7', 'max' => '1.3']) ?>
    <?= $form->field($model, 'lineFactor')->textInput(['type' => 'number', 'step' => '0.1', 'min' => '-10', 'max' => '10']) ?>

    <button>Пересчитать углы и отрезки</button>

<?php ActiveForm::end() ?>

<div id="result-container" style="display: none">
    <h1>Результат пересчета углов</h1>
    <p>
        <pre id="result-html"></pre>
    </p>
</div>


