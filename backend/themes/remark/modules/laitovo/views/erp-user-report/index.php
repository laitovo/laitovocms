<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отчет по работникам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['user_id', 'location_id', 'job_count', 'job_rate'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'user_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->user ? Html::a(Html::encode($data->user->getName()), ['view', 'id' => $data->user_id]) : null;
            },
        ],
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view', 'id' => $data->location_id]) : null;
            },
        ],
        'job_count',
        'job_rate',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
