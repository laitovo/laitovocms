<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpUsersReport;

$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUsersReport */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->user->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Отчет по работникам'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'user_id',
            'format' => 'html',
            'value' => $model->user ? Html::a(Html::encode($model->user->name), ['erp-user/view', 'id' => $model->user_id]) : null,
        ],
        [
            'attribute' => 'job_count',
            'format' => 'html',
            'value' => ErpUsersReport::find()->andWhere(['user_id' => $model->user_id])->sum('job_count'),
        ],
        [
            'attribute' => 'job_rate',
            'format' => 'html',
            'value' => ErpUsersReport::find()->andWhere(['user_id' => $model->user_id])->sum('job_rate'),
        ],
    ],
]) ?>


<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['location_id', 'naryad_id', 'job_type_id', 'job_count', 'job_rate'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view', 'id' => $data->location_id]) : null;
            },
        ],
        [
            'attribute' => 'naryad_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->naryad ? Html::a(Html::encode($data->naryad->name), ['erp-naryad/view', 'id' => $data->naryad_id]) : null;
            },
        ],
        [
            'attribute' => 'job_type_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->jobType ? Html::a(Html::encode($data->jobType->title), ['erp-job-types/view', 'id' => $data->job_type_id]) : null;
            },
        ],
        'job_count',
        'job_rate',
    ],
]); ?>