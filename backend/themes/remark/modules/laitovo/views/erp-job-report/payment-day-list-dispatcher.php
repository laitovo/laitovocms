<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\bootstrap\ActiveForm;



/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рапорта выполненных работ'), 'url' => ['user-list']];


//$this->params['searchmodel'] = false;
$this->render('../menu');
?>

    <h1><?= @$user->getName()?></h1>

<?php $form = ActiveForm::begin(['method'=>'get']); ?>

    <div class="col-xs-4">

        <?= $form->field($model, 'dateFrom1')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'dd.MM.yyyy',
        ]) ?>

    </div>
    <div class="col-xs-4">

        <?= $form->field($model, 'dateTo1')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'dd.MM.yyyy',
        ]) ?>

    </div>

    <div class="col-xs-4 ">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => '  btn  btn-outline btn-round btn-primary']) ?>
    </div>


<?php ActiveForm::end(); ?>


    <br>
    <br><br>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'rowOptions'=> function($data){
        if(!$data['created_at']){
            return ['class' => 'warning'];
        }
    },
    'show' => ['created_at', 'naryad_sum', 'deduction', 'fine', 'total_sum'],
    'columns' => [

        [
            'attribute' => 'created_at',
            'label'=>'Дата',
            'format' => 'html',
            'value' => function ($data)use($user) {
                return $data['created_at'] ? Html::a($data['created_at'],['report-day-dispatcher', 'user_id'=>$user->id, 'date'=>$data['created_at']]) : '';
            },
        ],
        [
            'attribute' => 'naryad_sum',
            'label'=>'',
            'format' => 'html',
            'value' => function ($data) {
                return Html::tag('span', Yii::$app->formatter->asDecimal($data['naryad_sum']), [
                    'style'=>[
                        'color'=>'blue',
                    ]]);
            },
        ],
        [
            'attribute' => 'deduction',
            'label'=>'',
            'format' => 'html',
            'value' => function ($data) {
                return Html::tag('span', Yii::$app->formatter->asDecimal($data['deduction']), [
                        'style'=>[
                                'color'=>'red',
                        ]]);
            },
        ],
        [
            'attribute' => 'fine',
            'label'=>'',
            'format' => 'html',
            'value' => function ($data) {
                return Html::tag('span', Yii::$app->formatter->asDecimal($data['fine']), [
                        'style'=>[
                                'color'=>'red',
                        ]]);
            },
        ],
        [
            'attribute' => 'total_sum',
            'label'=>'Итого',
            'format' => 'html',
            'value' => function ($data) {
                return Html::tag('span', Yii::$app->formatter->asDecimal($data['total_sum']), [
                    'style'=>[
                        'color'=>'green',
                    ]]);
            },
        ],
    ],
]); ?>