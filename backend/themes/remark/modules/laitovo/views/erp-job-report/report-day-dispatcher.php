<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\bootstrap\ActiveForm;
use backend\modules\laitovo\models\ErpNaryad;



/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рапорта выполненных работ'), 'url' => ['user-list']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Детализация по датам'), 'url' => ['payment-day-list-dispatcher', 'user_id'=>$user->id]];

//$this->params['searchmodel'] = false;
$this->render('../menu');
?>

<h1><?= @$user->getName()?></h1>

<div class="clearfix">

</div>
<div class="pull-right" style="padding-top: 30px">
    <h1 style="color: green; font-size: large">Итого <?= Yii::$app->formatter->asDecimal(@$sum_total)?></h1>
</div>

<div class="pull-right" style="padding-right: 80px; padding-top: 30px">
    <h1 style="color: orangered; font-size: large"> <?= Yii::$app->formatter->asDecimal(@$sum_deduction)?></h1>
</div>
<div class="pull-right" style="padding-right: 80px; padding-top: 30px">
    <h1 style="color: blue; font-size: large"> <?= Yii::$app->formatter->asDecimal(@$sum_naryad)?></h1>
</div>

<br>
<br><br>
<h1 style="color: green; font-size: x-large">Выполненные наряды</h1>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProviderNaryad,
    'filterModel'=>$model,
    'show' => ['naryad_id', 'article', 'naryad_sum', 'deduction', 'created_at', 'location_id'],
    'salt' =>1,
    'columns' => [


        [
            'attribute' => 'naryad_id',
            'label'=>'Номер наряда',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->naryad_id ? Html::a($data->naryad->name,['erp-naryad/view', 'id'=>$data->naryad_id])
                    : ($data->additional_naryad_id ? Html::a(" Доп. наряд №".Html::encode($data->additional_naryad_id), ['erp-additional-work/view', 'id' => $data->additional_naryad_id]): '');
            },
        ],
        [
            'attribute' => 'location_id',
            'label'=>'Участок',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->location_id ? @$data->location->name : '';
            },
        ],
        [
            'attribute' => 'article',
            'label'=>'Артикул',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->naryad_id ? @$data->naryad->article : '';
            },
        ],
        [
            'attribute'=>'window',
            'label'=>'Оконный проем',
            'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
            'value'=>function ($data) {
                return $data->naryad_id ? ErpNaryad::windowStatic($data->naryad->article): '';
            },
        ],
        [
            'attribute'=>'tkan',
            'label'=>'Тип ткани',
            'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
            'value'=>function ($data) {
                return $data->naryad_id ? ErpNaryad::tkanStatic($data->naryad->article) : '';
            },
        ],
        [
            'attribute' => 'product_type',
            'label'=>'Название продукта',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->naryad_id ?$data->naryad->productType->title: '';
            },
        ],
        [
            'attribute' => 'naryad_sum',
            'label'=>'',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data) {
                return $data->naryadSum ?  Html::tag('span', Yii::$app->formatter->asDecimal($data->naryadSum), [
                    'style'=>[
                        'color'=>'green',
                    ]]) : '';
            },
        ],

        [
            'attribute'=>'created_at',
            'label'=>'Дата',
            'filter'=>false,
            'value'=>function($data){
                return $data->created_at ? Yii::$app->formatter->asDatetime(@$data->created_at) : '';
            }
        ]
    ],
]); ?>


<h1 style="color: orangered; font-size: x-large">Наряды с несоответствиями</h1>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProviderRework,
    'filterModel'=>$model,
    'show' => ['rework_naryad_id', 'rework_article', 'rework_deduction', 'rework_created_at', 'rework_location_id'],
    'salt' =>2,
    'columns' => [
        [
            'attribute' => 'rework_naryad_id',
            'label'=>'Акта',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->rework_number ? Html::a('Акт №'.$data->rework_number,['erp-rework-act/view', 'id'=>$data->id]) : '';
            },
        ],
        [
            'attribute' => 'rework_location_id',
            'label'=>'Участок',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->location_defect ? @$data->locationDefect->name : '';
            },
        ],
        [
            'attribute' => 'rework_article',
            'label'=>'Артикул',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->naryad_id ? @$data->naryad->article : '';
            },
        ],
        [
            'attribute' => 'rework_product_type',
            'label'=>'Название продукта',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->naryad_id ?$data->naryad->productType->title: '';
            },
        ],
        [
            'attribute'=>'rework_window',
            'label'=>'Оконный проем',
            'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
            'value'=>function ($data) {
                return $data->naryad_id ? ErpNaryad::windowStatic($data->naryad->article): '';
            },
        ],
        [
            'attribute'=>'rework_tkan',
            'label'=>'Тип ткани',
            'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
            'value'=>function ($data) {
                return $data->naryad_id ? ErpNaryad::tkanStatic($data->naryad->article) : '';
            },
        ],
        [
            'attribute' => 'rework_deduction',
            'label'=>'Удержание',
            'format' => 'html',
            'filter'=>false,
            'value' => function ($data){
                return $data->deduction ? Html::tag('span', Yii::$app->formatter->asDecimal($data->deduction), [
                    'style'=>[
                        'color'=>'red',
                    ]]): '';
            },
        ],
        [
            'attribute'=>'rework_created_at',
            'label'=>'Дата',
            'filter'=>false,
            'value'=>function($data){
                return $data->created_at ? Yii::$app->formatter->asDatetime(@$data->created_at) : '';
            }
        ]
    ],
]); ?>


<h1 style="color: orangered; font-size: x-large">Провинности</h1>
<?= GridView::widget([
    'dataProvider' => $dataProviderFine,
    'show'=>['id', 'description', 'amount', 'created_at'],
    'columns' => [
        [
            'attribute'=>'id',
            'value'=>function($data){
                return '№'.$data->id;
            }
        ],
        [
            'attribute'=>'user_id',
            'value'=>function($data){
                return $data->user_id ? $data->user->name : '';
            }
        ],
        [
            'attribute'=>'description',
            'label'=>'Причина'
        ],
        [
            'attribute'=>'amount',
            'label'=> 'Размер',
            'value'=>function($data){
                return $data->amount ? Yii::$app->formatter->asDecimal($data->amount): 0;
            }
        ],
        'created_at:datetime',

    ],
]); ?>
