<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpLocation;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отчет по остаткам на складе');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = false;
$this->render('../menu');

Yii::$app->view->registerJs('
    var searchArticle = function() { 
        $.get("' . Url::to(['/ajax/automatic-article-search']) . '",{"article":$("#check-article").val()},function(data){ 
            if (data.status == "error") { 
                notie.alert(3,data.message,3);              
                $(".auto-search").html(""); 
                $(".group-search").html(""); 
                $(".article-search").html(""); 
                $(".analogs-search").html(""); 
//                $(".rpp-search").html(""); 
//                $(".rp-search").html(""); 
//                $(".pnp-search").html(""); 
//                $(".pr-search").html(""); 
//                $(".ksp-search").html(""); 
//                $(".kk-search").html(""); 
//                $(".chr-search").html(""); 
//                $(".op-search").html(""); 
                $(".of-search").html("");
            } else {
                notie.alert(1,"Успешно найдена статистика по артикулу",3);
                $(".auto-search").html(data.message.auto); 
                $(".group-search").html(data.message.group); 
                $(".article-search").html(data.message.article); 
                $(".analogs-search").html(data.message.names); 
//                $(".rpp-search").html(data.message.rpp); 
//                $(".rp-search").html(data.message.rp); 
//                $(".pnp-search").html(data.message.pnp); 
//                $(".pr-search").html(data.message.pr); 
//                $(".ksp-search").html(data.message.ksp); 
//                $(".kk-search").html(data.message.kk); 
//                $(".chr-search").html(data.message.chr); 
//                $(".op-search").html(data.message.op); 
                $(".of-search").html(data.message.of); 
            }
        }); 
    } 
', \yii\web\View::POS_END);

?>

<div class="row">
    <div class="col-sm-4">
        <div class="input-group input-group-sm">
            <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
            <?= Html::input($type='text', $name = null, $value = null, ['class' => 'form-control','id' => 'check-article','placeholder' => 'Введите артикул, например ... FD-V-530-1-2']); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <?= Html::a($text='Искать', $url = null, [
            'class' => 'btn btn-default btn-sm',
            'onclick' => 'if ($(\'#check-article\').val()) {searchArticle(),$(\'.check-article-info\').slideDown()} else {$(\'.check-article-info\').slideUp()};
                '
        ]); ?>
        <?= Html::button($content='Очистить', [
            'class' => 'btn btn-default btn-sm',
            'onclick' => '($(\'#check-article\').val(\'\')); $(\'.check-article-info\').slideUp();
                '
        ]); ?>
    </div>
</div>
<div class="clearfix"></div>

<hr>
<div class="row check-article-info" style="display: none;">
    <!-- Область вывода статиски -->
    <table class="table table-hover">
        <thead>
        <tr>
            <th>(Аналоги) Автомобиль</th>
            <th>Наименование группы</th>
            <th>Артикул</th>
            <th>Аналоги</th>
<!--            <th>РПП</th>-->
<!--            <th>РП</th>-->
<!--            <th>ПНП</th>-->
<!--            <th>ПР</th>-->
<!--            <th>КСП</th>-->
<!--            <th>КК</th>-->
<!--            <th>ЧР</th>-->
<!--            <th>ОП</th>-->
            <th>Фактические остатки</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="auto-search"></td>
            <td class="group-search"></td>
            <td class="article-search"></td>
            <td class="analogs-search"></td>
<!--            <td class="rpp-search"></td>-->
<!--            <td class="rp-search"></td>-->
<!--            <td class="pnp-search"></td>-->
<!--            <td class="pr-search"></td>-->
<!--            <td class="ksp-search"></td>-->
<!--            <td class="kk-search"></td>-->
<!--            <td class="chr-search"></td>-->
<!--            <td class="op-search"></td>-->
            <td class="of-search"></td>
        </tr>
        </tbody>
    </table>
</div>
