<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpOrder;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отчет о заказах');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = false;
$this->render('../menu');

?>
<h3><?= ($flag == 1 ? 'Выполненные заказы' : 'Текущие заказы')?></h3>
<?= GridView::widget([
    'tableOptions'=>['class'=>'table table-hover'],
    'dataProvider' => $ArrayDataProvider,
    'show'=>['id','status','created_at','updated_at'],
    'columns' => ArrayHelper::merge([
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'id',
            'format'=>'html',
            'label'=>'Номер заказа',
            'value'=>function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-order/view','id'=>$data->id]) : null;
            },
        ],
        'status',
        [
            'attribute'=>'created_at',
            'label' => 'Дата поступления',
            'value'=>function ($data) {
                return $data->created_at ? Yii::$app->formatter->asDatetime($data->created_at) : null;
            },
        ],
        [
            'attribute'=>'updated_at',
            'label' => ($flag == 1 ? 'Дата завершения' : 'Дата изменения'),
            'value'=>function ($data) {
                return $data->updated_at ? Yii::$app->formatter->asDatetime($data->updated_at) : null;
            },
        ],
        [
            'attribute'=>'author_id',
            'value'=>function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
           'attribute' => 'sort',
           'value' => function($data){return ErpOrder :: prioritetName($data->sort);},
           'filter' => ErpOrder :: prioritets(),
        ], 
        // 'author_id',
        // 'updater_id',
        // 'json:ntext',
    ],[
        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class'=>'deleteconfirm'],
        ],
    ]),
]); ?>




