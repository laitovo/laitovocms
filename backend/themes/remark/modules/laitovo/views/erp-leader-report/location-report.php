<?php
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpNaryad;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Отчет по участкам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');
?>


<?php $form = ActiveForm::begin([
         'method' => 'get',
    ]); ?>
<div class="col-xs-3">

    <?= $form->field($searchModel, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>

    <b style="font-size: 11px">
      *Выборка по дате "Готов к отгрузке"
    </b>
</div>

<div class="col-xs-3">

    <?= $form->field($searchModel, 'dateTo')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
         ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>
</div>
    <div class="col-xs-3">

        <?= $form->field($searchModel, 'location')->dropDownList([
          'null'=>'',
          '"Клипсы"'=> 'Клипсы',
          '"Изгиб"'=> 'Изгиб',
          '"Оклейка"'=> 'Оклейка',
          '"Швейка"'=> 'Швейка',
          '"Лейбл"'=> 'Лейбл',
          '"Отк"'=> 'Отк',
          ]) ?>


</div>

<div class="form-group col-xs-3">
    <div class=spacer>&nbsp;</div> <!-- Символ неразрывного пробела для того, чтобы встала кнопка -->
    <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


<div style="font-size: 20px; padding-top: 20px; clear:both  " >
    <b>
        Отчет по основной продукции
    </b>
</div>

    <span class="lead">Итого: <?= $totalSumm ?></span>


<?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader' => 'Отчет по основным продуктам',
        'show' => ['article', 'window', 'windowType', 'tkan', 'count', 'status'],
        'salt' =>1,
        'columns' => [
          [
              'attribute' => 'id',
              'label' => '',
              'format' => 'html',
              'value' => function ($data) {
                  return $data->name ? Html::a(Html::encode($data->name), ['erp-naryad/view', 'id' => $data->id]) : null;
              },
          ],
            [
            'attribute'=>'article',
            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            ],
            [
            'attribute'=>'window',
            'label'=>'Оконный проем',
           'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
            'value'=>function ($data) {
                return ErpNaryad::windowStatic($data->article);
            },
        ],
             [
            'attribute'=>'windowType',
            'label'=>'Название продукта',
            'filter'=>ErpNaryad::filterTypeStatic(),
            'value'=>function ($data) {
                return ErpNaryad::windowTypeStatic($data->article);
            },
        ],
                     [
            'attribute'=>'tkan',
            'label'=>'Тип ткани',
            'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
            'value'=>function ($data) {
                return ErpNaryad::tkanStatic($data->article);
            },
          ],
          [
            'attribute'=>'status',
            'label'=>'Производство/Со склада',
            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'filter'=> array(ErpNaryad::STATUS_READY=>'Производство',ErpNaryad::STATUS_FROM_SKLAD=>ErpNaryad::STATUS_FROM_SKLAD),
            'value'=>function($data){
              if($data->status == ErpNaryad::STATUS_READY) return 'Производство';
              return 'Со склада';
            }
           ],
           [
            'attribute' => 'sort',
            'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
            'filter' => ErpNaryad :: prioritets(),
           ],
       ]
   ]);?>
