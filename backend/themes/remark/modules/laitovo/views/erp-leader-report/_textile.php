<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpLocation;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отчет по ткани');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = false;
$this->render('../menu');

?>
<?php $form = ActiveForm::begin(); ?>
<div class="col-xs-3">

    <?= $form->field($model, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>


</div>
<div class="col-xs-3">

    <?= $form->field($model, 'dateTo')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>

</div>
<div class="col-xs-3">
    <?= $form->field($model, 'location')
        ->dropDownList(
            ArrayHelper::merge(
                ['' => ''],
                ArrayHelper::map(
                    ErpLocation::find()->all(),
                    'id',
                    'name'
                )
            )
        );
    ?>
</div>
<div class="form-group col-xs-3">
    <div class=spacer>&nbsp;</div> <!-- Символ неразрывного пробела для того, чтобы встала кнопка -->
    <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>


<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => 'Не требуется ткань'],
    'dataProvider' => $ArrayDataProvider,
    'show' => ['Номер ткани', 'Количество нарядов'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'Номер ткани',
            'format' => 'html',
            'value' => function ($data) {
                return $data['type'] ? $data['type'] : null;
            },
        ],
        [
            'attribute' => 'Количество нарядов',
            'value' => function ($data) {
                return $data['count'] ? $data['count'] : null;
            },
        ],
        // 'updated_at',
        // 'author_id',
        // 'updater_id',
        // 'json:ntext',

        /* [
             'class' => 'yii\grid\ActionColumn',
             'buttonOptions' => ['class'=>'deleteconfirm'],
         ],*/
    ],
]);
?>
<? if (isset($count)) { ?>
    <div class="col-xs-offset-9 col-xs-3">
        <div class="alert alert-info text-center"><span class="lead">Всего: <?= $count ?></span></div>
    </div>
<? } ?>

