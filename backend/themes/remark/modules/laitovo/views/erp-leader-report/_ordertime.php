<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpLocation;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отчет о заказах');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = false;
$this->render('../menu');

?>
<?php $form = ActiveForm::begin(); ?>
<div class="col-xs-3">

    <?= $form->field($model, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>



</div>
<div class="col-xs-3">

    <?= $form->field($model, 'dateTo')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>

</div>
<div class="form-group col-xs-3">
    <div class=spacer>&nbsp;</div> <!-- Символ неразрывного пробела для того, чтобы встала кнопка -->
    <?= Html::submitButton( Yii::t('app', 'Сформировать') ,['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
<?= GridView::widget([
    'tableOptions'=>['class'=>'table table-hover'],
    'dataProvider' => $ArrayDataProvider,
    'show'=>['title','count','countCur'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'title',
            'format'=>'html',
            'label' => 'Продолжительность',
            'value'=>function ($data) {
                return $data['title'] ? $data['title'] : null;
            },
        ],
        [
            'attribute'=>'count',
            'label' => 'Количество заказов',
            'format'=>'html',
            'value'=> function ($data) use ($model) {

                return $data['count'] ? (Html::a($data['count'],
                    ['erp-leader-report/order-time-report-view',
                    'dateFrom'=>$model->dateFrom,
                    'dateTo' => $model->dateTo,
                    'index' => $data['index']])) : 0;
            },
        ],
        [
            'attribute'=>'countCur',
            'label' => 'Текущие заказы',
            'format'=>'html',
            'value'=> function ($data) use ($model) {

                return $data['countCur'] ? (Html::a($data['countCur'],
                    ['erp-leader-report/order-time-report-view-cur',
                        'dateFrom'=>$model->dateFrom,
                        'dateTo' => $model->dateTo,
                        'index' => $data['index']])) : 0;
            },
        ],
    ],
]);
?>
<div class="alert alert-info" role="alert">
    <span class="lead">Средний срок выполнения заказа, дней:
        <?= ($objReport['intMedium'] ? number_format($objReport['intMedium'],1) : 0).' ('.
            ($objReport['intMediumInHours'] ? number_format($objReport['intMediumInHours'],0) : 0).' часов)'?>
    </span>
</div>
<div class="alert alert-danger" role="alert">
    <span class="lead">Самый долгий срок выполнения заказа, дней:
    <?= $objReport['intLongest'] ? $objReport['intLongest'].' '.Html::a('>>>',['erp-order/view','id' => $objReport['intLongestID']]) : 0; ?>
</div>



