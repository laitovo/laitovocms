<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отчет наряда на людях');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = false;
$this->render('../menu');

?>
<?= Html::a('Hard режим', ['','hard' => true],['data-pjax' => '','class' => 'btn btn-success']) ?>


<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'show' => ['userName','count'],
    'salt' => 'zxxf',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute'=>'userName',
            'format'=>'raw',
            'label'=>'Работник',
            'value'=>function ($data) {
                return $data->user->name;
            },
        ],
        [
            'attribute'=>'count',
            'format'=>'raw',
            'label'=>'Кол-во нарядов',
            'value'=>function ($data) {
                return $data->user->recieveCountActiveNaryads();
            },
        ],

       
    ],
]); ?>




