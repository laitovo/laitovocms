<?php

/**
 * @var $provider ArrayDataProvider
 */

use backend\widgets\GridView;
use yii\data\ArrayDataProvider;

$this->title = Yii::t('app', 'Отчет об испольовании лекал за последний год');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');
?>


<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $provider,
    'show' => ['templateNumber','usingCount1','usingCount2','usingCount3','usingCount4','usingCount5','cars'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'templateNumber',
                'label' => 'Номер лекала'
            ],
            [
                    'attribute' => 'cars',
                    'format' => 'raw',
                    'label' => 'Список автомобилей',
                    'value' => function($data) {
                        return str_replace(';','<br>',$data['cars']);
                    },
            ],
            [
                'attribute' => 'usingCount1',
                'label' => 'Использовалось за 1 год'
            ],
            [
                'attribute' => 'usingCount2',
                'label' => 'Использовалось за 2 года'
            ],
            [
                'attribute' => 'usingCount3',
                'label' => 'Использовалось за 3 года'
            ],
            [
                'attribute' => 'usingCount4',
                'label' => 'Использовалось за 4 года'
            ],
            [
                'attribute' => 'usingCount5',
                'label' => 'Использовалось за 5 лет'
            ],
    ]
]) ?>