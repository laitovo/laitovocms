<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отчет наряда на людях');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = false;
$this->render('../menu');

?>
<?= Html::a('Lite режим', ['','hard' => false],['data-pjax' => '','class' => 'btn btn-success']) ?>


<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'show' => ['fullname','article','status','ordername','locationName','userName'],
    'salt' => 'das',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute'=>'userName',
            'format'=>'raw',
            'label'=>'Работник',
            'value'=>function ($data) {
                return $data->user->name;
            },
        ],
        [
            'attribute'=>'locationName',
            'format'=>'raw',
            'label' => 'Участок',
            'value'=>function ($data) {
                return $data->locationstart 
                ? Html::a(Html::encode($data->locationstart->name), ['erp-location/view','id'=>$data->locationstart->id]).' <small>'.($data->location ? $data->location->name : 'Облако').'</small>'
                : ($data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view','id'=>$data->location->id]) : null);
            },
        ],
        [
            'attribute'=>'ordererName',
            'format'=>'raw',
            'label' => 'Заказчик',
            'value'=>function ($data) {
                return $data->order
                ? $data->order->json('username') : null;
            },
        ],
        [
            'attribute' => 'fullname',
            'format' => 'html',
            'label' => 'Номер наряда',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-naryad/view', 'id' => $data->id]) : null;
            },
        ],
        'article',
        'status',
        [
            'attribute' => 'ordername',
            'format' => 'html',
            'label' => 'Заказ',
            'value' => function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view', 'id' => $data->order->id]) : null;
            },
        ],


        // [
        //     'attribute' => 'scheme_id',
        //     'format' => 'html',
        //     'value' => function ($data) {
        //         return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view', 'id' => $data->scheme->id]) : null;
        //     },
        // ],
        'created_at:datetime',
        // [
        //     'attribute' => 'author_id',
        //     'value' => function ($data) {
        //         return $data->author ? $data->author->name : null;
        //     },
        // ],
        [
            'attribute' => 'sort',
            'value' => function($data){return ErpNaryad :: prioritetName($data->sort);},
        ],
        // 'created_at',
        // 'updated_at',
        // 'author_id',
        // 'updater_id',
        // 'json:ntext',
    ],
]); ?>




