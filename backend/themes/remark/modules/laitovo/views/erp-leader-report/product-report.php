<?php
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpNaryad;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Отчёт по произведённой продукции');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');
?>


<?php $form = ActiveForm::begin([
         'method' => 'get',
    ]); ?>
<div class="col-xs-3">

    <?= $form->field($searchModel, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>

    <b style="font-size: 11px">
      *Выборка по дате "Готов к отгрузке"
    </b>
</div>

<div class="col-xs-3">

    <?= $form->field($searchModel, 'dateTo')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
         ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>


</div>

<div class="col-xs-2">
    <?= $form->field($searchModel, 'country')->dropDownList([0=>'', 1=>'Россия', 2=>'Гамбург']) ?>
</div>

<div class="form-group col-xs-3">
    <div class=spacer>&nbsp;</div> <!-- Символ неразрывного пробела для того, чтобы встала кнопка -->
    <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


<div style="font-size: 20px; padding-top: 20px; clear:both  " >
    <b>
        Отчет по основной продукции
    </b>
</div>

    <span class="lead">Итого: <?= $totalGeneralSumm ?></span>


<?=GridView::widget([
        'dataProvider' => $dataGeneralProvider,
        'filterModel' => $searchModel,
        'showHeader' => 'Отчет по основным продуктам',
        'show' => ['article', 'window', 'windowType', 'tkan', 'count', 'status', 'car'],
        'salt' =>1,
        'columns' => [

            [
            'attribute'=>'article',
            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            ],
            [
            'attribute'=>'window',
            'label'=>'Оконный проем',
           'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
            'value'=>function ($data) {
                return ErpNaryad::windowStatic($data->article);
            },
        ],
             [
            'attribute'=>'windowType',
            'label'=>'Название продукта',
            'filter'=> ErpNaryad::filterTypeStatic(1),
            'value'=>function ($data) {
                return ErpNaryad::windowTypeStatic($data->article);
            },
        ],
                     [
            'attribute'=>'tkan',
            'label'=>'Тип ткани',
            'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
            'value'=>function ($data) {
                return ErpNaryad::tkanStatic($data->article);
            },
        ],
           [
            'attribute'=>'count',
            'label'=>'Количество',
           ],
            [
                'attribute'=>'car',
                'label'=>'Автомобиль',
                'value'=>function ($data) {
                    return $data->car->name ?? null;
                },
            ],
                [
            'attribute'=>'status',
            'label'=>'Производство/Со склада',
            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'filter'=> array(ErpNaryad::STATUS_READY=>'Производство',ErpNaryad::STATUS_FROM_SKLAD=>ErpNaryad::STATUS_FROM_SKLAD),
            'value'=>function($data){
              if($data->status == ErpNaryad::STATUS_READY) return 'Производство';
              return 'Со склада';
            }
            ],

            [
             'class' => 'yii\grid\ActionColumn',
             'headerOptions' => ['width' => '80'],
             'template' => '{view}{link}',
           'urlCreator' => function ($action, $model, $key, $index) use($searchModel) {
                if ($action === 'view') {
                    $url = Url::to(['show-naryads','article' => $model->article,'dateFrom' => $searchModel->dateFrom, 'dateTo'=>$searchModel->dateTo, 'status' => $model->status]); // your own url generation logic
                    return $url;
                }
            }
         ],
        ],
    ]); ?>

<div style="font-size: 20px; padding-top: 15px" >
    <b>
        Отчет по дополнительной продукции
    </b>
</div>

<span class="lead"> Итого: <?= $totalAdditionalSumm ?></span>

<?= GridView::widget([
        'dataProvider' => $dataAdditionalProvider,
        'filterModel' => $searchModel,
        'show' => ['article1', 'window1', 'windowType1', 'tkan1', 'count1', 'status1', 'car'],
    'salt' =>3,
        'columns' => [

            [
            'attribute'=>'article1',
            'label'=>'Номенклатура',
            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
           'value'=>function ($data) {
                return $data->article ;
            },
            ],
            [
            'attribute'=>'window1',
            'label'=>'Оконный проем',
            'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
            'value'=>function ($data) {
                return ErpNaryad::windowStatic($data->article);
            },
        ],
             [
            'attribute'=>'windowType1',
            'label'=>'Название продукта',
            'filter'=> ErpNaryad::filterTypeStatic(2),
            'value'=>function ($data) {
                return ErpNaryad::windowTypeStatic($data->article);
            },
        ],
                     [
            'attribute'=>'tkan1',
            'label'=>'Тип ткани',
            'filter'=> array("№1.5"=>"№1.5","№1.25"=>"№1.25", "№1"=>"№1","№2"=>"№2","№3"=>"№3","№5"=>"№5"),
            'value'=>function ($data) {
                return ErpNaryad::tkanStatic($data->article);
            },
        ],
           [
            'attribute'=>'count1',
            'label'=>'Количество',
            'value'=>function ($data) {
                return $data->count ;
            },
           ],
            [
                'attribute'=>'car',
                'label'=>'Автомобиль',
                'value'=>function ($data) {
                    return $data->car->name ?? null;
                },
            ],
                [
            'attribute'=>'status1',
            'label'=>'Производство/Со склада',
            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'filter'=> array(ErpNaryad::STATUS_READY=>'Производство',ErpNaryad::STATUS_FROM_SKLAD=>ErpNaryad::STATUS_FROM_SKLAD),
            'value'=>function($data){
              if($data->status == ErpNaryad::STATUS_READY) return 'Производство';
              return 'Со склада';
            }
            ],
      


            [
             'class' => 'yii\grid\ActionColumn',
             'headerOptions' => ['width' => '80'],
             'template' => '{view}{link}',
             'urlCreator' => function ($action, $model, $key, $index) use($searchModel) {
                  if ($action === 'view') {
                      $url = Url::to(['show-naryads','article' => $model->article,'dateFrom' => $searchModel->dateFrom, 'dateTo'=>$searchModel->dateTo, 'status' => $model->status]); // your own url generation logic
                      return $url;
                  }
              }
         ],
        ],
    ]); ?>

<div style="font-size: 20px; padding-top: 15px" >
    <b>
        Отчет о прочей продукции
    </b>
</div>

<span class="lead">Итого: <?= $totalOtherSumm ?></span>

<?= GridView::widget([
        'dataProvider' => $dataOtherProvider,
        'filterModel' => $searchModel,
        'show' => ['article2', 'window2', 'windowType2', 'tkan2', 'count2', 'status2'],
    'salt' =>2,
        'columns' => [

            [
            'attribute'=>'article2',
            'label'=>'Номенклатура',
            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
           'value'=>function ($data) {
                return $data->article ;
            },
            ],
        [
          'attribute'=>'windowType2',
          'label'=>'Название продукта',

          'value'=>function ($data) {
              return ErpNaryad::windowTypeStatic($data->article);
          },
      ],


           [
              'attribute'=>'count2',
              'label'=>'Количество',
              'value'=>function ($data) {
                return $data->count ;
              },
           ],
                [
            'attribute'=>'status2',
            'label'=>'Производство/Со склада',
            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'filter'=> array(ErpNaryad::STATUS_READY=>'Производство',ErpNaryad::STATUS_FROM_SKLAD=>ErpNaryad::STATUS_FROM_SKLAD),
            'value'=>function($data){
              if($data->status == ErpNaryad::STATUS_READY) return 'Производство';
              return 'Со склада';
            }
            ],
       
            [
             'class' => 'yii\grid\ActionColumn',
             'headerOptions' => ['width' => '80'],
             'template' => '{view}{link}',
             'urlCreator' => function ($action, $model, $key, $index) use($searchModel) {
                  if ($action === 'view') {
                      $url = Url::to(['show-naryads','article' => $model->article,'dateFrom' => $searchModel->dateFrom, 'dateTo'=>$searchModel->dateTo,'status' => $model->status]); // your own url generation logic
                      return $url;
                  }
              }
         ],
        ],
    ]); ?>
