<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplaceRegister */

$this->title = 'Добавить запись в журнал регистрации';
$this->params['breadcrumbs'][] = ['label' => 'Erp Location Workplace Registers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-register-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
