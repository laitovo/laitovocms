<?php

use yii\helpers\Html;
use backend\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\modules\laitovo\models\ErpUser;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplaceRegister */


$this->params['breadcrumbs'][] = ['label' => 'Журнал регистрации', 'url' => ['index']];

$this->render('../menu');

?>
<div class="erp-location-workplace-register-create">

    <h1>Регистрация сотрудников на рабочем месте</h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => ErpUser::userInWorkSupport(),
        'language' => 'ru',

        //'initValueText' => common\models\laitovo\ErpUser::findOne($made_user_id)->name,
        'options' => [
            'placeholder' => 'Введите название ...',
            'class'=>'form-control',
            'onchange'=>'
                $.get("' . Url::to(['/ajax/render-workplace']) . '",{"user_id":$(this).val()},function(data){ 
                              if(data.status){
                                    $("#status-true").text(data.message);
                                    $("#status-red").text(data.workplaceName);
                              }
                              if(data.status==2){
                                    $("#start-day").prop("class", "btn btn-success btn-round btn-lg");
                                    $("#start-day").text("Начать рабочий день");
                                    $("#div-button").prop("hidden", false);
                              }
                              if(data.status==1){
                                     $("#start-day").prop("class", "btn btn-danger btn-round btn-lg");
                                     $("#start-day").text("Закончить рабочий день");
                                     $("#div-button").prop("hidden", false);
                              }
                              if(!data.status){
                                    notie.alert(3,data.message,4);
                              }
                         });
            '
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <h1 id="status-true" style="color: #00aa00"><h1 id="status-red" style="color:red"></h1></h1>
<div id="div-button" hidden="true">
    <?= Html::button('', ['class'=> 'btn btn-success btn-round btn-lg', 'id'=>'start-day',
        'onclick'=>'
              $.get("' . Url::to(['/ajax/register-workplace']) . '",{"user_id":$("#erplocationworkplaceregister-user_id").val()},function(data){   
                    $.get("' . Url::to(['/ajax/render-workplace']) . '",{"user_id":$("#erplocationworkplaceregister-user_id").val()},function(data){ 
                              if(data.status){
                                    $("#status-true").text(data.message);
                                    $("#status-red").text(data.workplaceName);
                              }
                              if(data.status==2){
                                    $("#start-day").prop("class", "btn btn-success btn-round btn-lg");
                                    $("#start-day").text("Начать рабочий день");
                                    $("#div-button").prop("hidden", false);
                              }
                              if(data.status==1){
                                     $("#start-day").prop("class", "btn btn-danger btn-round btn-lg");
                                     $("#start-day").text("Закончить рабочий день");
                                     $("#div-button").prop("hidden", false);
                              }
                         });
                    if(data.flag==1){
                         $("#start-day").prop("class", "btn btn-danger btn-round btn-lg");
                         $("#start-day").text("Закончить рабочий день");
                         $("#div-button").prop("hidden", false);
                         notie.alert(1,"Вы начали рабочий день "+data.date,4);
                    }
                    
                    if(data.flag==2){
                         $("#start-day").prop("class", "btn btn-success btn-round btn-lg");
                         $("#start-day").text("Начать рабочий день");
                         $("#div-button").prop("hidden", false);
                         notie.alert(3,"Вы закончили рабочий день "+data.date,4);
                    }
                    
                    if(data.flag==3){
                         $("#div-button").prop("hidden", true);
                         notie.alert(3,data.message,4);
                    }
              
              });
        '
        ]) ?>
</div>
    <?php ActiveForm::end(); ?>

</div>
