<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplaceRegister */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-location-workplace-register-form">

    <h3 style="color: red;"><?= @$model->user->name?></h3>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'workplace_id')->hiddenInput()->label(false) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'register_at')->textInput() ?>
        </div>

        <div class="col-md-6">
            <?= Html::label('Человеческая дата') ?>
            <?= Html::input($type = 'text',
                'datetime',
                $value = $model->register_at ? Yii::$app->formatter->asDatetime($model->register_at,'php:d.m.Y H:i:s') : '',
                [
                    'class' => 'form-control',
                    'placeholder' => '25.12.2017 08:48:05',
                    'onchange' => '
                $.get("' . Url::to(['/ajax/timestamp']) . '",{"date":$(this).val()},function(data){
                    $("#erplocationworkplaceregister-register_at").val(data.date);
                });
            '
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'action_type')->dropDownList(\yii\helpers\ArrayHelper::merge(['' => ''],$model->actiontypes)) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Вернуться в пользователя',['erp-user/view','id' => $model->user_id],['class' => 'btn btn-info'])?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
