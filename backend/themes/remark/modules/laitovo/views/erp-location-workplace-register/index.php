<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\models\ErpLocationWorkplaceRegisterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Журнал регистрации пользователей';
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-register-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--     <p>
        <?= Html::a('Create Erp Location Workplace Register', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'show' => ['user.name','workplace.location_number','register_at','action_type','workplace.location.name'],
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user.name',
            'workplace.location.name',
            'workplace.location_number',
            'register_at:datetime',
            'action_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
