<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\MainProductInfo;

$this->title = Yii::t('app', 'Печать наряда');

$this->render('../menu');

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');
Yii::$app->view->registerJs('
$( document ).ready(function() {
//    printJS({printable:"'.Url::to(['erp-naryad/print-with-manual']).'?id=' . $id . '", type:"pdf", showModal:true, modalMessage: "Печать..."});
       printUrl("' . Url::to(['erp-naryad/print-with-manual', 'id' => $id]) . '");
});

    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>

<div class="alert alert-success">Вы успешно создали наряд и теперь он числиться на Вас.</div>

<div class="row">
    <div class="erp-fine-form">
            <div class="form-group">
                <?= Html::a('Создать еще наряд', ['index'], ['class' => 'btn btn-success']); ?>
                <?= Html::a('Вернуться к указанию сотрудника', ['back-user'], ['class' => 'btn btn-info']); ?>
            </div>
    </div>
</div>





