<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\MainProductInfo;

Yii::$app->view->registerJs('
$( document ).ready(function() {
    $.get("' . Url::to(['/ajax/product-types']) . '",{"type":$("#otkcreate-window").val(),"cloth":$("#otkcreate-tkan").val()},function(data){
                $("#otkcreate-goodid").html("");
                if (Object.keys(data).length!=1)
                    $("#otkcreate-goodid").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#otkcreate-goodid").append($("<option></option>").attr("value",key).text(value));
                });
            });
});
function checkNeed() {
    $.get("' . Url::to(['/ajax/check-need']) . '",{"goodid":$("#otkcreate-goodid").val(),"cararticle":$("#otkcreate-cararticle").val()},function(data){
                $("#article-info").html("");
                    if (data && data.status == "success") {
                        $("#article-info").html("<div class=\'alert alert-success\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>" + data.message + "</div>"
                        );
                        $("#submitbtn").removeAttr("disabled");
                    }else if (data && data.status == "error"){
                        $("#article-info").html("<div class=\'alert alert-danger\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>" + data.message + "</div>"
                        );
                        $("#submitbtn").attr("disabled","true");
                    }else{
                        $("#article-info").html("<div class=\'alert alert-danger\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>Нет ответа по данному артикулу.</div>"
                        );
                        $("#submitbtn").attr("disabled","true");
                    };
            });   
}



', \yii\web\View::POS_END);
?>


<div class="row">
    <div class="erp-fine-form">
        <p id="article-info"></p>


        <?php $form = ActiveForm::begin(/*['options' => ['target' => '_blank']]*/); ?>
            <h1><?=$model2->user->name?></h1>

            <?= $form->field($model2, 'user_id')->hiddenInput()->label(false) ?>

            <?= $form->field($model2, 'carArticle')->textInput([
                'onchange' => '$.get("' . Url::to(['/ajax/car-name']) . '",{"article":$(this).val()},function(data){
                    $("#car-name").html("");
                    if (data && data.status == "success") {
                        $("#car-name").html("<div class=\'alert alert-success\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>" + data.message + "</div>"
                        );
                        $("#submitbtn").removeAttr("disabled");
                    }else if (data && data.status == "error"){
                        $("#car-name").html("<div class=\'alert alert-danger\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>" + data.message + "</div>"
                        );
                        $("#submitbtn").attr("disabled","true");
                    }else{
                        $("#car-name").html("<div class=\'alert alert-danger\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>Нет ответа по данному артикулу.</div>"
                        );
                        $("#submitbtn").attr("disabled","true");
                    };
                });checkNeed();
            ']) ?>

            <p id="car-name"></p>

            <?= $form->field($model2, 'window')->dropDownList(ArrayHelper::merge([''=>''],MainProductInfo::getGoodsPrefixList()),
            ['onchange' => '$.get("' . Url::to(['/ajax/product-types']) . '",{"type":$(this).val(),"cloth":$("#otkcreate-tkan").val()},function(data){
                $("#otkcreate-goodid").html("");
                if (Object.keys(data).length!=1)
                    $("#otkcreate-goodid").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#otkcreate-goodid").append($("<option></option>").attr("value",key).text(value));
                });
            });checkNeed();
            ']); ?>

            <?= $form->field($model2, 'tkan')->dropDownList(ArrayHelper::merge([''=>''],MainProductInfo::getGoodsClothList()),
            ['onchange' => '$.get("' . Url::to(['/ajax/product-types']) . '",{"cloth":$(this).val(),"type":$("#otkcreate-window").val()},function(data){
                $("#otkcreate-goodid").html("");
                $("#otkcreate-goodid").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#otkcreate-goodid").append($("<option></option>").attr("value",key).text(value));
                });
            });checkNeed();
            ']); ?>





            <?= $form->field($model2, 'goodid')->dropDownList(['' => ''],['onchange' => 'checkNeed();']) ?>

            <div class="form-group">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-success','id'=>'submitbtn']) ?>
                <?= Html::a('Вернуться к указанию сотрудника', ['back-user'], ['class' => 'btn btn-info']); ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>





