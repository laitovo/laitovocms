<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpNaryad;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpScheme */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Маршрутизация нарядов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

backend\assets\flowchart\FlowchartAsset::register($this);
$this->registerJs("
    flowchartinit({
        palette:'myPaletteDiv',
        diagram:'myDiagramDiv',
        model:'erpscheme-map'
    });
    flowchartmakeSVG('SVGArea');
");

?>
<div class="row" style="display: none;">
    <div class="col-sm-1" id="myPaletteDiv"></div>
    <div class="col-sm-10" id="myDiagramDiv"></div>
</div>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'map')->hiddenInput()->label(false) ?>
<?php ActiveForm::end(); ?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'name',
            'value' => $model->name,
        ],
        'created_at:datetime',
        // 'updated_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => $model->author ? $model->author->name : null,
        ],
        'rule:ntext',
        // 'updater_id',
        // 'json:ntext',
    ],
]) ?>
<div class="text-center">
    <div id="SVGArea"></div>
</div>

<h4>Наряды</h4>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $naryadsProvider,
    'show' => ['id', 'article', 'order_id', 'location_id', 'status', 'window', 'windowType', 'tkan'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-naryad/view', 'id' => $data->id]) : null;
            },
        ],
        'article',

        [
        'attribute'=>'window',
        'label'=>'Оконный проем',
       'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
        'value'=>function ($data) {
            return ErpNaryad::windowStatic($data->article);
        },
      ],
         [
        'attribute'=>'windowType',
        'label'=>'Название продукта',
        'filter'=> array(1=>'Стандарт',45=>"ChikoMagnet, Стандарт",
            48=>"ChikoMagnet, Укороченный",67=>"Chiko на магнитах, Стандарт",
            2=>"Вырез курил.",3=>"Вырез зерк.", 6=>"Укороченный"),
        'value'=>function ($data) {
            return ErpNaryad::windowTypeStatic($data->article);
        },
      ],
      [
        'attribute'=>'tkan',
        'label'=>'Тип ткани',
        'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
        'value'=>function ($data) {
        return ErpNaryad::tkanStatic($data->article);
        },
      ],
        [
            'attribute' => 'order_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view', 'id' => $data->order->id]) : null;
            },
        ],
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view', 'id' => $data->location->id]) : null;
            },
        ],
        'status',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        // 'created_at',
        // 'updated_at',
        // 'author_id',
        // 'updater_id',
        // 'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'erp-naryad',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>


<?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>


<?= $model->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
    'data-toggle' => "tooltip",
    'data-original-title' => Yii::t('yii', 'Delete'),
    'data' => [
        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
        'method' => 'post',
    ],
]) ?>
