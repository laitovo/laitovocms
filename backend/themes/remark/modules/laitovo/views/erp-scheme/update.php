<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpScheme */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Маршрутизация нарядов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('yii', 'Update');

$this->render('../menu');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
