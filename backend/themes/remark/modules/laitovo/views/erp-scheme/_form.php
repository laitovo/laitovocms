<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use common\models\laitovo\ErpLocation;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpScheme */
/* @var $form yii\widgets\ActiveForm */

backend\assets\flowchart\FlowchartAsset::register($this);
$this->registerJs("
  $('#form-erp-scheme').on('beforeSubmit', function (e) {
    flowchartsave('erpscheme-map');
    return true;
  });
");
$location[] = [
    'category' => 'Start',
    'text' => 'Старт',
];
foreach (ErpLocation::find()->orderBy('sort')->all() as $loc) {
    $location[] = [
        'location_id' => $loc->id,
        'text' => $loc->name,
    ];
}
$location[] = [
    'category' => 'End',
    'text' => 'Финиш',
];
$palettecat = Json::encode($location);
$this->registerJs("
	flowchartinit({
		palette:'myPaletteDiv',
		diagram:'myDiagramDiv',
		model:'erpscheme-map',
		palettecat:$palettecat
	});
");
$height = 150 + ((count($location) - 2) * 41);
?>

<div class="row">
    <div class="col-sm-1" id="myPaletteDiv"
         style="height: <?= $height ?>px;border: 1px dashed #ddd;margin-left: 25px;"></div>
    <div class="col-sm-10" id="myDiagramDiv"
         style="height: <?= $height ?>px;border: 1px dashed #ddd;margin-left: 35px;"></div>
</div>

<div class="clearfix"></div>

<?php $form = ActiveForm::begin(['id' => 'form-erp-scheme']); ?>

<?= $form->field($model, 'map')->hiddenInput()->label(false) ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'rule')->textArea() ?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
