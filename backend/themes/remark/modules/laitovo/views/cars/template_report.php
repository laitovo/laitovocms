<?php

use backend\widgets\GridView;
use yii\data\ArrayDataProvider;
/**
 * @var ArrayDataProvider $provider
 */

$this->title = 'Топ лекал';

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;


$this->render('../menu');
?>

<?= GridView::widget([
    'dataProvider' => $provider,
    'tableOptions' => ['class' => 'table table-hover'],
    'show' => [
        'pos',
        'carName',
        'lekalo',
    ],
    'columns' => [
        [
            'attribute' => 'pos',
            'label' => 'Позиция в топе'
        ],
        [
            'attribute' => 'carName',
            'label' => 'Автомобиль',
            'format' => 'raw',
            'value' => function($data) {
                return implode('<br>',$data['carName']);
            }
        ],

        [
            'attribute' => 'lekalo',
            'label' => 'Лекало'
        ],
    ],
]); ?>