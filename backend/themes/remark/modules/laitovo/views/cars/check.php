<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Cars;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Автомобили');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');

$fields = [];
foreach ($model->auto->fields as $key => $value) {
    $fields[] = [
        'attribute' => $value['fields']['id']['value'],
        'label' => $value['fields']['label']['value'],
        'format' => 'text',
        'value' => function ($data) use ($value) {
            return $data->json($value['fields']['id']['value']);
        },
    ];
}
?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name'],
    'columns' => ArrayHelper::merge([
        ['class' => 'yii\grid\SerialColumn'],

        'article',
        [
            'attribute' => 'name',
            'label' => $model->getAttributeLabel('name'),
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a($data->name, ['view', 'id' => $data->id]) : null;
            },
        ],
        'mark',
        'model',
        'generation',
        'carcass',
        'doors',
        'firstyear',
        'lastyear',
        'modification',
        'country',
        'category',
        // 'mounting:ntext',
        // 'fw:ntext',
        // 'fv:ntext',
        // 'fd:ntext',
        // 'rd:ntext',
        // 'rv:ntext',
        // 'bw:ntext',
        // 'json:ntext',
    ], $fields, [
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}', //{delete},
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ]),
]); ?>
