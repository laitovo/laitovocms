<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'bw_date:date',
        'bw_dlina',
        'bw_visota',
        'bw_magnit',
        'bw_obshivka',
        'bw_openwindow',
        'bw_openwindowtrue',
        'bw_chastei',
        'bw_hlyastik',
        [
            'attribute' => 'bw_simmetr',
            'format' => 'html',
            'value' => $model->bw_simmetr === null ? null : Html::tag('i', '', ['class' => $model->bw_simmetr ? 'wb-check text-success' : 'wb-close text-danger']),
        ],
        [
            'attribute' => 'bw_gabarit',
            'format' => 'html',
            'value' => $model->bw_gabarit === null ? null : Html::tag('i', '', ['class' => $model->bw_gabarit ? 'wb-check text-success' : 'wb-close text-danger']),
        ],
    ],
]) ?>

<div class="form-group">
    <!-- Nav tabs -->
    <h5>Основная продукция</h5>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#chiko_view_7" aria-controls="chiko_view_7" role="tab"
                                                  data-toggle="tab">Chiko</a></li>
        <li role="presentation"><a href="#laitovo_view_7" aria-controls="laitovo_view_7" role="tab" data-toggle="tab">Laitovo</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <br>
        <h5>Зависимая продукция</h5>
        <div role="tabpanel" class="tab-pane fade" id="laitovo_view_7">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_standart_view_7"
                                                              aria-controls="laitovo_standart_view_7" role="tab"
                                                              data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Стандарт</a></li>
                    <li role="presentation"><a href="#laitovo_dontlooks_view_7" aria-controls="laitovo_dontlooks_view_7"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->bw_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look сдвижной</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_standart_view_7">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'bw_laitovo_standart_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsbwscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Простые',
                                    'window' => 'ЗШ',
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsbwscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ЗШ',
                                ]) ?>
                            </div>
                        </div>
                        <h4>2 части</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsbw2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт 2 части',
                                    'type_clips' => 'Простые',
                                    'window' => 'ЗШ',
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsbw2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт 2 части',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ЗШ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlooks_view_7">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'bw_laitovo_dontlooks_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->bw_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade in active" id="chiko_view_7">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_standart_view_7"
                                                              aria-controls="chiko_standart_view_7" role="tab"
                                                              data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->bw_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Стандарт</a></li>
                    <li role="presentation"><a href="#chiko_dontlookb_view_7" aria-controls="chiko_dontlookb_view_7"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->bw_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_standart_view_7">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'bw_chiko_standart_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->bw_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                                'bw_chiko_standart_natyag',
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsbwscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Простые',
                                    'window' => 'ЗШ',
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsbwscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ЗШ',
                                ]) ?>
                            </div>
                        </div>
                        <h4>2 части</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsbw2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт 2 части',
                                    'type_clips' => 'Простые',
                                    'window' => 'ЗШ',
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsbw2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт 2 части',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ЗШ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlookb_view_7">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'bw_chiko_dontlookb_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->bw_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>