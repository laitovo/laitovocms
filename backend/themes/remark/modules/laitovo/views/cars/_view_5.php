<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'rd_date:date',
        'rd_dlina',
        'rd_visota',
        'rd_magnit',
        'rd_obshivka',
        'rd_hlyastik',
        'rd_install_direction',
        'rd_with_recess',
        [
            'attribute' => 'rd_use_tape',
            'format' => 'html',
            'value' => Html::tag('i', '', ['class' => $model->rd_use_tape ? 'wb-check-circle text-success' : 'wb-minus-circle text-danger']),
        ],
    ],
]) ?>

<div class="form-group">
    <!-- Nav tabs -->
    <h5>Основная продукция</h5>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#laitovo_view_5" aria-controls="laitovo_view_5" role="tab"
                                                  data-toggle="tab">Laitovo</a></li>
        <li role="presentation"><a href="#chiko_view_5" aria-controls="chiko_view_5" role="tab"
                                   data-toggle="tab">Chiko</a></li>
        <li role="presentation"><a href="#chikomagnet_view_5" aria-controls="chikomagnet_view_5" role="tab"
                                   data-toggle="tab">Chiko Magnet</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <br>
        <h5>Зависимая продукция</h5>
        <div role="tabpanel" class="tab-pane fade in active" id="laitovo_view_5">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_standart_view_5"
                                                              aria-controls="laitovo_standart_view_5" role="tab"
                                                              data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->rd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Стандарт</a></li>
                    <li role="presentation"><a href="#laitovo_dontlooks_view_5" aria-controls="laitovo_dontlooks_view_5"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->rd_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look сдвижной</a></li>
                    <li role="presentation"><a href="#laitovo_dontlookb_view_5" aria-controls="laitovo_dontlookb_view_5"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->rd_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_standart_view_5">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'rd_laitovo_standart_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->rd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsrdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Простые',
                                    'window' => 'ЗБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsrdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ЗБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsrdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ЗБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlooks_view_5">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'rd_laitovo_dontlooks_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->rd_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlookb_view_5">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'rd_laitovo_dontlookb_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->rd_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chiko_view_5">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_standart_view_5"
                                                              aria-controls="chiko_standart_view_5" role="tab"
                                                              data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->rd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Стандарт</a></li>
                    <li role="presentation"><a href="#chiko_dontlooks_view_5" aria-controls="chiko_dontlooks_view_5"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->rd_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look сдвижной</a></li>
                    <li role="presentation"><a href="#chiko_dontlookb_view_5" aria-controls="chiko_dontlookb_view_5"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->rd_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_standart_view_5">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'rd_chiko_standart_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->rd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                                'rd_chiko_standart_natyag',
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsrdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Простые',
                                    'window' => 'ЗБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsrdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ЗБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsrdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ЗБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlooks_view_5">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'rd_chiko_dontlooks_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->rd_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlookb_view_5">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'rd_chiko_dontlookb_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->rd_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chikomagnet_view_5">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'rd_chikomagnet_status',
                        'format' => 'html',
                        'value' => Html::tag('i', '', ['class' => $model->rd_chikomagnet_status ? 'wb-check text-success' : 'wb-close text-danger']),
                    ],
                    'rd_chikomagnet_magnitov',
                ],
            ]) ?>
        </div>
    </div>

</div>