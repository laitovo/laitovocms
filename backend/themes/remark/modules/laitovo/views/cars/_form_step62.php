<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */

$clips = Json::decode($model->clips);
$scheme1 = Json::decode($model->rv_laitovo_triangul_scheme);
$scheme2 = Json::decode($model->rv_laitovo_triangul_schemem);
$scheme3 = Json::decode($model->rv_chiko_triangul_scheme);
$scheme4 = Json::decode($model->rv_chiko_triangul_schemem);

Yii::$app->view->registerJs('

    function parseitemsjson1 () {
        var scheme =[];
        $(\'.schemeitems1 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carsform-rv_laitovo_triangul_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems1 select",function(e){
        parseitemsjson1();
        e.preventDefault();
    });


    function parseitemsjson2 () {
        var scheme =[];
        $(\'.schemeitems2 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carsform-rv_laitovo_triangul_schemem\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems2 select",function(e){
        parseitemsjson2();
        e.preventDefault();
    });


    function parseitemsjson3 () {
        var scheme =[];
        $(\'.schemeitems3 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carsform-rv_chiko_triangul_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems3 select",function(e){
        parseitemsjson3();
        e.preventDefault();
    });


    function parseitemsjson4 () {
        var scheme =[];
        $(\'.schemeitems4 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carsform-rv_chiko_triangul_schemem\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems4 select",function(e){
        parseitemsjson4();
        e.preventDefault();
    });

    ', \yii\web\View::POS_END);


?>
<h3>ЗФ: Треугольная</h3>
<div class="form-group tabcontent">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#laitovo" aria-controls="laitovo" role="tab" data-toggle="tab">Laitovo</a>
        </li>
        <li role="presentation"><a href="#chiko" aria-controls="chiko" role="tab" data-toggle="tab">Chiko</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="laitovo">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_triangul" aria-controls="laitovo_triangul"
                                                              role="tab" data-toggle="tab">Простые</a></li>
                    <li role="presentation"><a href="#laitovo_magnit" aria-controls="laitovo_magnit" role="tab"
                                               data-toggle="tab">Магнитные</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_triangul">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems1">

                                <img src="/img/rv_t_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme1[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 13%;top: 43%;", 'id' => 'rv_laitovo_triangul_scheme_0']) ?>
                                <?= Html::dropDownList(null, @$scheme1[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 28%;", 'id' => 'rv_laitovo_triangul_scheme_1']) ?>
                                <?= Html::dropDownList(null, @$scheme1[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 18%;", 'id' => 'rv_laitovo_triangul_scheme_2']) ?>
                                <?= Html::dropDownList(null, @$scheme1[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 44%;top: 26%;", 'id' => 'rv_laitovo_triangul_scheme_3']) ?>
                                <?= Html::dropDownList(null, @$scheme1[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 41%;top: 37%;", 'id' => 'rv_laitovo_triangul_scheme_4']) ?>
                                <?= Html::dropDownList(null, @$scheme1[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 38%;top: 47%;", 'id' => 'rv_laitovo_triangul_scheme_5']) ?>
                                <?= Html::dropDownList(null, @$scheme1[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 25%;top: 56%;", 'id' => 'rv_laitovo_triangul_scheme_6']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_magnit">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems2">

                                <img src="/img/rv_t_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme2[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 13%;top: 43%;", 'id' => 'rv_laitovo_triangul_schemem_0']) ?>
                                <?= Html::dropDownList(null, @$scheme2[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 28%;", 'id' => 'rv_laitovo_triangul_schemem_1']) ?>
                                <?= Html::dropDownList(null, @$scheme2[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 18%;", 'id' => 'rv_laitovo_triangul_schemem_2']) ?>
                                <?= Html::dropDownList(null, @$scheme2[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 44%;top: 26%;", 'id' => 'rv_laitovo_triangul_schemem_3']) ?>
                                <?= Html::dropDownList(null, @$scheme2[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 41%;top: 37%;", 'id' => 'rv_laitovo_triangul_schemem_4']) ?>
                                <?= Html::dropDownList(null, @$scheme2[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 38%;top: 47%;", 'id' => 'rv_laitovo_triangul_schemem_5']) ?>
                                <?= Html::dropDownList(null, @$scheme2[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 25%;top: 56%;", 'id' => 'rv_laitovo_triangul_schemem_6']) ?>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chiko">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_triangul" aria-controls="chiko_triangul"
                                                              role="tab" data-toggle="tab">Простые</a></li>
                    <li role="presentation"><a href="#chiko_magnit" aria-controls="chiko_magnit" role="tab"
                                               data-toggle="tab">Магнитные</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_triangul">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems3">

                                <img src="/img/rv_t_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme3[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 13%;top: 43%;", 'id' => 'rv_chiko_triangul_scheme_0']) ?>
                                <?= Html::dropDownList(null, @$scheme3[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 28%;", 'id' => 'rv_chiko_triangul_scheme_1']) ?>
                                <?= Html::dropDownList(null, @$scheme3[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 18%;", 'id' => 'rv_chiko_triangul_scheme_2']) ?>
                                <?= Html::dropDownList(null, @$scheme3[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 44%;top: 26%;", 'id' => 'rv_chiko_triangul_scheme_3']) ?>
                                <?= Html::dropDownList(null, @$scheme3[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 41%;top: 37%;", 'id' => 'rv_chiko_triangul_scheme_4']) ?>
                                <?= Html::dropDownList(null, @$scheme3[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 38%;top: 47%;", 'id' => 'rv_chiko_triangul_scheme_5']) ?>
                                <?= Html::dropDownList(null, @$scheme3[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 25%;top: 56%;", 'id' => 'rv_chiko_triangul_scheme_6']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_magnit">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems4">

                                <img src="/img/rv_t_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme4[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 13%;top: 43%;", 'id' => 'rv_chiko_triangul_schemem_0']) ?>
                                <?= Html::dropDownList(null, @$scheme4[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 28%;", 'id' => 'rv_chiko_triangul_schemem_1']) ?>
                                <?= Html::dropDownList(null, @$scheme4[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 18%;", 'id' => 'rv_chiko_triangul_schemem_2']) ?>
                                <?= Html::dropDownList(null, @$scheme4[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 44%;top: 26%;", 'id' => 'rv_chiko_triangul_schemem_3']) ?>
                                <?= Html::dropDownList(null, @$scheme4[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 41%;top: 37%;", 'id' => 'rv_chiko_triangul_schemem_4']) ?>
                                <?= Html::dropDownList(null, @$scheme4[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 38%;top: 47%;", 'id' => 'rv_chiko_triangul_schemem_5']) ?>
                                <?= Html::dropDownList(null, @$scheme4[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 25%;top: 56%;", 'id' => 'rv_chiko_triangul_schemem_6']) ?>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
</div>
