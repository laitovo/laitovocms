<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'fd_date:date',
        'fd_dlina',
        'fd_visota',
        'fd_magnit',
        'fd_obshivka',
        'fd_hlyastik',
        'fd_install_direction',
        'fd_with_recess',
        [
            'attribute' => 'fd_use_tape',
            'format' => 'html',
            'value' => Html::tag('i', '', ['class' => $model->fd_use_tape ? 'wb-check-circle text-success' : 'wb-minus-circle text-danger']),
        ],
    ],
]) ?>

<div class="form-group">
    <!-- Nav tabs -->
    <h5>Основная продукция</h5>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#laitovo_view_4" aria-controls="laitovo_view_4" role="tab"
                                                  data-toggle="tab">Laitovo</a></li>
        <li role="presentation"><a href="#chiko_view_4" aria-controls="chiko_view_4" role="tab"
                                   data-toggle="tab">Chiko</a></li>
        <li role="presentation"><a href="#chikomagnet_view_4" aria-controls="chikomagnet_view_4" role="tab"
                                   data-toggle="tab">Chiko Magnet</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <br>
        <h5>Зависимая продукция</h5>
        <div role="tabpanel" class="tab-pane fade in active" id="laitovo_view_4">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_standart_view_4"
                                                              aria-controls="laitovo_standart_view_4" role="tab"
                                                              data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Стандарт</a></li>
                    <li role="presentation"><a href="#laitovo_short_view_4" aria-controls="laitovo_short_view_4"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_laitovo_short_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Укороченный</a></li>
                    <li role="presentation"><a href="#laitovo_smoke_view_4" aria-controls="laitovo_smoke_view_4"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Вырез для курящих</a></li>
                    <li role="presentation"><a href="#laitovo_mirror_view_4" aria-controls="laitovo_mirror_view_4"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_laitovo_mirror_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Вырез для зеркал</a></li>
                    <li role="presentation"><a href="#laitovo_dontlooks_view_4" aria-controls="laitovo_dontlooks_view_4"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look сдвижной</a></li>
                    <li role="presentation"><a href="#laitovo_dontlookb_view_4" aria-controls="laitovo_dontlookb_view_4"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_standart_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_laitovo_standart_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsfdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_short_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_laitovo_short_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_short_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfd2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Укороченный',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfd2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Укороченный',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsfd2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Укороченный',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_smoke_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_laitovo_smoke_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfd3scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'ВырезДляКурящих',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfd3scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'ВырезДляКурящих',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsfd3scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'ВырезДляКурящих',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_mirror_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_laitovo_mirror_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_mirror_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                                'fd_laitovo_mirror_maxdlina',
                                'fd_laitovo_mirror_maxvisota',
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfd4scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'ВырезДляЗеркала',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfd4scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'ВырезДляЗеркала',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsfd4scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'ВырезДляЗеркала',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlooks_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_laitovo_dontlooks_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlookb_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_laitovo_dontlookb_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chiko_view_4">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_standart_view_4"
                                                              aria-controls="chiko_standart_view_4" role="tab"
                                                              data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Стандарт</a></li>
                    <li role="presentation"><a href="#chiko_short_view_4" aria-controls="chiko_short_view_4" role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_chiko_short_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Укороченный</a></li>
                    <li role="presentation"><a href="#chiko_smoke_view_4" aria-controls="chiko_smoke_view_4" role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Вырез для курящих</a></li>
                    <li role="presentation"><a href="#chiko_mirror_view_4" aria-controls="chiko_mirror_view_4"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_chiko_mirror_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Вырез для зеркал</a></li>
                    <li role="presentation"><a href="#chiko_dontlooks_view_4" aria-controls="chiko_dontlooks_view_4"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look сдвижной</a></li>
                    <li role="presentation"><a href="#chiko_dontlookb_view_4" aria-controls="chiko_dontlookb_view_4"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fd_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_standart_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_chiko_standart_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                                'fd_chiko_standart_natyag',
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsfdscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_short_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_chiko_short_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_short_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                                'fd_chiko_short_natyag',
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfd2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Укороченный',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfd2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Укороченный',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsfd2scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Укороченный',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_smoke_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_chiko_smoke_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                                'fd_chiko_smoke_natyag',
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfd3scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'ВырезДляКурящих',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfd3scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'ВырезДляКурящих',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsfd3scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'ВырезДляКурящих',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_mirror_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_chiko_mirror_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_mirror_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                                'fd_chiko_mirror_maxdlina',
                                'fd_chiko_mirror_maxvisota',
                                'fd_chiko_mirror_natyag',
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfd4scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'ВырезДляЗеркала',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfd4scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'ВырезДляЗеркала',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Скотч</h5>
                                <p><a href="#inputCheckedstepsfd4scheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'ВырезДляЗеркала',
                                    'type_clips' => 'Скотч',
                                    'window' => 'ПБ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlooks_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_chiko_dontlooks_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlookb_view_4">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fd_chiko_dontlookb_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chikomagnet_view_4">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'fd_chikomagnet_status',
                        'format' => 'html',
                        'value' => Html::tag('i', '', ['class' => $model->fd_chikomagnet_status ? 'wb-check text-success' : 'wb-close text-danger']),
                    ],
                    'fd_chikomagnet_magnitov',
                ],
            ]) ?>
        </div>
    </div>

</div>