<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */
?>
<h3>Дополнительные сведения</h3>

<?= FormBuilder::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'fields',
    'render' => $model->auto->fieldsrender,
]); ?>
