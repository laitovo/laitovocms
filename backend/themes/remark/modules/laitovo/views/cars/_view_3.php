<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'fv_date:date',
        'fv_dlina',
        'fv_visota',
        'fv_magnit',
    ],
]) ?>

<div class="form-group">
    <!-- Nav tabs -->
    <h5>Основная продукция</h5>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#laitovo_view_3" aria-controls="laitovo_view_3" role="tab"
                                                  data-toggle="tab">Laitovo</a></li>
        <li role="presentation"><a href="#chiko_view_3" aria-controls="chiko_view_3" role="tab"
                                   data-toggle="tab">Chiko</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <br>
        <h5>Зависимая продукция</h5>
        <div role="tabpanel" class="tab-pane fade in active" id="laitovo_view_3">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_standart_view_3"
                                                              aria-controls="laitovo_standart_view_3" role="tab"
                                                              data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Стандарт</a></li>
                    <li role="presentation"><a href="#laitovo_dontlookb_view_3" aria-controls="laitovo_dontlookb_view_3"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fv_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_standart_view_3">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fv_laitovo_standart_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfvscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПФ',
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfvscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Laitovo',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПФ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlookb_view_3">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fv_laitovo_dontlookb_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fv_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chiko_view_3">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_standart_view_3"
                                                              aria-controls="chiko_standart_view_3" role="tab"
                                                              data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Стандарт</a></li>
                    <li role="presentation"><a href="#chiko_dontlookb_view_3" aria-controls="chiko_dontlookb_view_3"
                                               role="tab"
                                               data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->fv_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                            Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_standart_view_3">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fv_chiko_standart_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                                'fv_chiko_standart_natyag',
                            ],
                        ]) ?>
                        <h4>Схема креплений</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Простые</h5>
                                <p><a href="#inputCheckedstepsfvscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Простые',
                                    'window' => 'ПФ',
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Магнитные</h5>
                                <p><a href="#inputCheckedstepsfvscheme" class="link-dashed" data-toggle="modal"
                                      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                                <?= $this->render('_scheme', [
                                    'model' => $model,
                                    'brand' => 'Chiko',
                                    'type' => 'Стандарт',
                                    'type_clips' => 'Магнитные',
                                    'window' => 'ПФ',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlookb_view_3">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'fv_chiko_dontlookb_status',
                                    'format' => 'html',
                                    'value' => Html::tag('i', '', ['class' => $model->fv_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger']),
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>