<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use backend\widgets\GridView;
use common\models\laitovo\CarsAct;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */

$this->title = $model->auto->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Автомобили'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');

Yii::$app->view->registerJs('
    $(".productions ul.nav-tabs li.visible:first a").click();
    $("body").on("click","a[data-target=\"#edit-car-modal\"]",function(e){
        $("#edit-car-modal input[type=\"checkbox\"]").prop("checked",false);
        $("#edit-car-modal "+$(this).attr("href")).prop("checked",true);
        e.preventDefault();
    });
    $("#edit-car-modal").on("submit","form",function(e){
        if(!$("#edit-car-modal form input[type=\"checkbox\"]:checked").length)
            e.preventDefault();
    });
');


Yii::$app->view->registerJs('
    function checkBoxing(cur,id,element){
        if ($(cur).prop("checked") == true)
        {
            var str = $("#modal [name=\'elem\']").val();
            var arr = str.split(",");
            if (arr.indexOf(element)<0) arr.push(element);
            $("#modal [name=\'elem\']").val(arr.join(","));
        }
        else
        {
            var result = false;
            if ($(id).prop("checked") == true) {result = true};
            $(id).parent(".checkbox-custom").next("ul").find("[type=\'checkbox\']").each(function(){
                console.log(this);
                if ($(this).prop("checked") == true){
                    result = true;
                };
            });
            if (result == false){
                var str = $("#modal [name=\'elem\']").val();
                var arr = str.split(",");
                arr.splice(arr.indexOf(element), 1);
                $("#modal [name=\'elem\']").val(arr.join(","));
            } 
        };            
    };
',\yii\web\View::POS_HEAD);


$fields = [];
foreach ($model->auto->fields as $key => $value) {
    if (!in_array($value['fields']['id']['value'], ['fw', 'fv', 'fd', 'rd', 'rv', 'bw', 'info'])) {
        $fields[] = [
            'attribute' => $value['fields']['id']['value'],
            'label' => $value['fields']['label']['value'],
            'format' => $value['title'] == 'Check Box' ? 'raw' : 'text',
            'value' => $value['title'] == 'Check Box' ? Html::tag('i', '', ['class' => $model->auto->json($value['fields']['id']['value']) ? 'wb-check text-success' : 'wb-close text-danger']) : $model->auto->json($value['fields']['id']['value']),
        ];  
    }
}
?>
<? if ($model->auto->json('reworkmsg')): ?>
    <div class="alert dark alert-icon alert-warning alert-dismissible alert-alt dark" role="alert">
        <i class="icon wb-warning" aria-hidden="true"
           style="font-size: 30px;margin: -8px;"></i> <?= $model->auto->json('reworkmsg') ?>&nbsp;
    </div>
<? endif ?>
<div class="form-group text-right">
    <?php if (!$model->checked): ?>
    <!-- ЕСЛИ АВТОМОБИЛЬ НЕ ПРОВЕРЕН -->
        <?if (in_array(Yii::$app->user->getId(),[21,2,12,50])):?>
        <!-- ДЛЯ ЛЮДЕЙ КОТОРЫЕ МОГУТ ИЗМЕНЯТЬ ПОКАЖЕМ КНОПКУ ДОБАВЛЕНИЯ В КАТАЛОГ -->
        <?= Html::a('Добавить в каталог', ['change-check', 'id' => $model->auto->id], [
                'class' => ' btn btn-icon btn-outline btn-round  btn-info deleteconfirm',
                'data-original-title' => Yii::t('yii', 'Add'),
                'data' => [
                    'confirm' => Yii::t('yii', 'Вы уверены что хотите добавить в каталог это значение?'),
                    'method' => 'post',
                ],
        ]) ?>
        <?else:?>
        <!-- А КОТОРЫЕ НЕ ИМЕЮТ ПРАВ - ПОКАЖЕМ СООБЩЕНИЕ -->
        <div class="alert alert-info text-left" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Данный автомобиль еще не прошел проверку уполномоченным лицом!
        </div>
        <?endif;?>
    <?endif;?>

    <?php if ($model->checked && $model->checked == 2 ): ?>
    <!-- ЕСЛИ АВТОМОБИЛЬ БЫЛ ПРОВЕРЕН И ОТПРАВЛЕН НА ДОРАБОТКУ -->
    <?php Modal::begin([
        // 'size'=>Modal::SIZE_SMALL,
        'id' => 'edit-car-modal3',
        'options' => ['class' => 'text-left'],
        'header' => Html::tag('h4', $model->mastered ? Yii::t('app', 'Акт внесеня изменений') : Yii::t('app', 'Акт освоения'), ['class' => 'modal-title']),
        'toggleButton' => ['style' => 'margin-right:5px;', 'class' => ' btn btn-round btn-info btn-outline', 'label' => Yii::t('app', 'Доработать')],
    ]); ?>

    <form action="<?= Url::to(['update']) ?>">
        <h5>Сведения</h5>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedsteps1" name="steps[]"
                   value="1" <?= $model->mastered ? '' : 'disabled checked' ?> />
            <label for="inputCheckedsteps1">Основные</label>
        </div>
        <br>
        
        <input type="hidden" name="id" value="<?= $model->auto->id ?>">
        <input type="hidden" name="step" value="1">

        <div class="pull-right text-right form-group">
            <div class="">Ответственный:</div>
            <h4 class=""> <?= $this->renderDynamic('return Yii::$app->user->identity->name;') ?></h4>
        </div>
        <br>
        <button type="submit" class="btn btn-outline btn-round btn-primary">Далее</button>
    </form>

    <?php Modal::end(); ?>

    <?endif;?>

    <?php if ($model->checked && $model->checked == 1): ?>
    <!-- Если Автомобиль добавлен в каталог -->
    <div class="col-xs-4 text-left">
        <ul class="list-inline" role="">
            <li role="presentation" class="btn btn-sm
                <?= $model->auto->json('fw') ? 'btn-primary' : ($model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_FW) ? 'btn-info': 'btn-default')  ?>
                <?= $model->auto->json('fw1') ? 'visible' : 'hidden'?>
            ">
                    ПШ</li>
            <li role="presentation" class="btn btn-sm
                <?= $model->auto->json('fv') ? 'btn-primary' : ($model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_FV) ? 'btn-info': 'btn-default')  ?>
                <?= $model->auto->json('fv1') ? 'visible' : 'hidden'?>
            ">
                    ПФ</li>
            <li role="presentation" class="btn btn-sm
                <?= $model->auto->json('fd') ? 'btn-primary' : ($model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_FD) ? 'btn-info': 'btn-default')  ?>
                <?= $model->auto->json('fd1') ? 'visible' : 'hidden'?>
            ">
                    ПБ</li>
            <li role="presentation" class="btn btn-sm
                <?= $model->auto->json('rd') ? 'btn-primary' : ($model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_RD) ? 'btn-info': 'btn-default')  ?>
                <?= $model->auto->json('rd1') ? 'visible' : 'hidden'?>
            ">
                    ЗБ</li>
            <li role="presentation" class="btn btn-sm
                <?= $model->auto->json('rv') ? 'btn-primary' : ($model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_RV) ? 'btn-info': 'btn-default')  ?>
                <?= $model->auto->json('rv1') ? 'visible' : 'hidden'?>
            ">
                    ЗФ</li>
            <li role="presentation" class="btn btn-sm
                <?= $model->auto->json('bw') ? 'btn-primary' : ($model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_BW) ? 'btn-info': 'btn-default')  ?>
                <?= $model->auto->json('bw1') ? 'visible' : 'hidden'?>
            ">
                    ЗШ</li>
        </ul>
    </div>

    <?if (!$model->auto->isFullMastered()):?>
    <?php Modal::begin([
        // 'size'=>Modal::SIZE_SMALL,
        'id' => 'edit-car-modal2',
        'options' => ['class' => 'text-left'],
        'header' => Html::tag('h4', Yii::t('app', 'Акт освоения'), ['class' => 'modal-title']),
        'toggleButton' => ['style' => 'margin-right:5px;', 'class' => ' btn btn-round btn-info btn-outline', 'label' => Yii::t('app', 'Освоить')],
    ]); ?>

    <form action="<?= Url::to(['update']) ?>" id ="modal-2">
        <!-- ОБЩИЕ КУСКИ СЦЕНАРИЕВ ОСВОЕНИЯ -->
        <div class="checkbox-custom checkbox-primary ">
            <input type="checkbox" id="inputCheckedstepsclips2" name="steps[]" value="9" disabled="" />
            <label for="inputCheckedstepsclips2">Крепления</label>
        </div>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedsteps8" name="steps[]" disabled=""
                   value="8" />
            <label for="inputCheckedsteps8">Дополнительные</label>
        </div>
        <!-- ОТДЕЛЬНЫЕ КУСКИ СЦЕНАРИЕВ ОСВОЕНИЯ -->
        <h5>Элементы</h5>
        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ПШ ##### START #####-->
        <div class="col-md-2">
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="inputCheckedstepsfw2"
                       name="steps[]" <?= $model->auto->json('fw1') ? '' : 'disabled' ?> value="2"
                       <?= ((!$model->auto->json('fw') && !$model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_FW))) ? '' : 'disabled' ?>
                       onchange = "
                            if ($(this).prop('checked')){
                                $(this).prop('checked',true);
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.push('<?= CarsAct::ELEMENT_FW?>');
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));
                                $('#elemFw2').prop('checked',true);
                                $('#inputCheckedstepsfwscheme2').prop('checked',true);

                                $('#inputCheckedstepsclips2').prop('disabled',false);
                                $('#inputCheckedsteps8').prop('disabled',false);
                            }
                            else{
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.splice(arr.indexOf('<?= CarsAct::ELEMENT_FW?>'), 1);
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));
                                $('#elemFw2').prop('checked',false);
                                $('#inputCheckedstepsfwscheme2').prop('checked',false);
                            };
                       "
                       />
                <label for="inputCheckedstepsfw2">ПШ</label>
            </div>
            <ul class="hidden">
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsfwscheme2"
                               name="steps[]" <?= $model->auto->json('fw1') ? '' : 'disabled' ?> value="21"/>
                        <label for="inputCheckedstepsfwscheme2">Схема замеров</label>
                    </div>
                </li>
            </ul>
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="elemFw2"
                       name="<?= CarsAct::ELEMENT_FW?>" value="<?= CarsAct::ELEMENT_FW?>"/>
            </div>
        </div>
        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ПШ ##### END #####-->

        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ПФ ##### START #####-->
        <div class="col-md-2">
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="inputCheckedstepsfv2"
                       name="steps[]"<?= $model->auto->json('fv1') ? '' : 'disabled' ?> value="3"
                       <?= ((!$model->auto->json('fv') && !$model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_FV))) ? '' : 'disabled' ?>
                       onchange = "
                            if ($(this).prop('checked')){
                                // $('.checkbox-custom > input').prop('checked',false);
                                $(this).prop('checked',true);

                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.push('<?= CarsAct::ELEMENT_FV?>');
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));

                                $('#inputCheckedstepsfvscheme2').prop('checked',true);
                                // $('#inputCheckedstepsclips2').prop('checked',true);
                                $('#inputCheckedstepsclips2').prop('disabled',false);
                                $('#inputCheckedsteps8').prop('disabled',false);
                            }
                            else{
                                // $('.checkbox-custom > input').prop('checked',false);
                                $('#inputCheckedstepsfvscheme2').prop('checked',false);
                                // $('#inputCheckedstepsclips2').prop('checked',false);
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.splice(arr.indexOf('<?= CarsAct::ELEMENT_FV?>'), 1);
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));

                            };
                       "
                       />
                <label for="inputCheckedstepsfv2">ПФ</label>
            </div>
            <ul class="hidden">
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsfvscheme2"
                               name="steps[]"<?= $model->auto->json('fv1') ? '' : 'disabled' ?> value="31"/>
                        <label for="inputCheckedstepsfvscheme2">Схема креплений</label>
                    </div>
                </li>
            </ul>
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="elemFv2"
                       name="<?= CarsAct::ELEMENT_FV?>" value="<?= CarsAct::ELEMENT_FV?>"/>
            </div>
        </div>
        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ПФ ##### END #####-->

        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ПБ ##### START #####-->
        <div class="col-md-2">
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="inputCheckedstepsfd2"
                       name="steps[]" <?= $model->auto->json('fd1') ? '' : 'disabled' ?> value="4"
                       <?= ((!$model->auto->json('fd') && !$model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_FD))) ? '' : 'disabled' ?>
                       onchange = "
                            if ($(this).prop('checked')){
                                // $('.checkbox-custom > input').prop('checked',false);
                                $(this).prop('checked',true);

                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.push('<?= CarsAct::ELEMENT_FD?>');
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));

                                $('#inputCheckedstepsfdscheme2').prop('checked',true);
                                $('#inputCheckedstepsfd2scheme2').prop('checked',true);
                                $('#inputCheckedstepsfd3scheme2').prop('checked',true);
                                $('#inputCheckedstepsfd4scheme2').prop('checked',true);
                                // $('#inputCheckedstepsclips2').prop('checked',true);
                                $('#inputCheckedstepsclips2').prop('disabled',false);
                                $('#inputCheckedsteps8').prop('disabled',false);
                            }
                            else{
                                $('#inputCheckedstepsfdscheme2').prop('checked',false);
                                $('#inputCheckedstepsfd2scheme2').prop('checked',false);
                                $('#inputCheckedstepsfd3scheme2').prop('checked',false);
                                $('#inputCheckedstepsfd4scheme2').prop('checked',false);
                                // $('#inputCheckedstepsclips2').prop('checked',false);
                                
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.splice(arr.indexOf('<?= CarsAct::ELEMENT_FD?>'), 1);
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));
                            };
                       "
                       />
                <label for="inputCheckedstepsfd2">ПБ</label>
            </div>
            <ul class="hidden">
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsfdscheme2"
                               name="steps[]" <?= $model->auto->json('fd1') ? '' : 'disabled' ?> value="41"/>
                        <label for="inputCheckedstepsfdscheme2">Схема креплений</label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsfd2scheme2"
                               name="steps[]" <?= $model->auto->json('fd1') ? '' : 'disabled' ?> value="42"/>
                        <label for="inputCheckedstepsfd2scheme2">Схема креплений (Укороченный)</label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsfd3scheme2"
                               name="steps[]" <?= $model->auto->json('fd1') ? '' : 'disabled' ?> value="43"/>
                        <label for="inputCheckedstepsfd3scheme2">Схема креплений (Вырез для курящих)</label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsfd4scheme2"
                               name="steps[]" <?= $model->auto->json('fd1') ? '' : 'disabled' ?> value="44"/>
                        <label for="inputCheckedstepsfd4scheme2">Схема креплений (Вырез для зеркала)</label>
                    </div>
                </li>
            </ul>
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="elemFd2"
                       name="<?= CarsAct::ELEMENT_FD?>" value="<?= CarsAct::ELEMENT_FD?>"/>
            </div>
        </div>
        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ПБ ##### END #####-->

        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ЗБ ##### START #####-->
        <div class="col-md-2">
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="inputCheckedstepsrd2"
                       name="steps[]" <?= $model->auto->json('rd1') ? '' : 'disabled' ?> value="5"
                       <?= ((!$model->auto->json('rd') && !$model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_RD))) ? '' : 'disabled' ?>
                       onchange = "
                            if ($(this).prop('checked')){
                                // $('.checkbox-custom > input').prop('checked',false);
                                $(this).prop('checked',true);
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.push('<?= CarsAct::ELEMENT_RD?>');
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));;
                                $('#inputCheckedstepsrdscheme2').prop('checked',true);
                                // $('#inputCheckedstepsclips2').prop('checked',true);
                                $('#inputCheckedstepsclips2').prop('disabled',false);
                                $('#inputCheckedsteps8').prop('disabled',false);
                            }
                            else{
                                // $('.checkbox-custom > input').prop('checked',false);
                                $('#inputCheckedstepsrdscheme2').prop('checked',false);
                                // $('#inputCheckedstepsclips2').prop('checked',false);
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.splice(arr.indexOf('<?= CarsAct::ELEMENT_RD?>'), 1);
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));
                            };
                       "
                       />
                <label for="inputCheckedstepsrd2">ЗБ</label>
            </div>
            <ul class="hidden">
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsrdscheme2"
                               name="steps[]" <?= $model->auto->json('rd1') ? '' : 'disabled' ?> value="51"/>
                        <label for="inputCheckedstepsrdscheme2">Схема креплений</label>
                    </div>
                </li>
            </ul>
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="elemRd2"
                       name="<?= CarsAct::ELEMENT_RD?>" value="<?= CarsAct::ELEMENT_RD?>"/>
            </div>
        </div>
        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ЗБ ##### END #####-->

        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ЗФ ##### START #####-->
        <div class="col-md-2">
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="inputCheckedstepsrv2"
                       <?= ((!$model->auto->json('rv') && !$model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_RV))) ? '' : 'disabled' ?>
                       name="steps[]" <?= $model->auto->json('rv1') ? '' : 'disabled' ?> value="6"
                       onchange = "
                            if ($(this).prop('checked')){
                                // $('.checkbox-custom > input').prop('checked',false);
                                $(this).prop('checked',true);
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.push('<?= CarsAct::ELEMENT_RV?>');
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));
                                $('#inputCheckedstepsrvscheme2').prop('checked',true);
                                $('#inputCheckedstepsrv2scheme2').prop('checked',true);
                                // $('#inputCheckedstepsclips2').prop('checked',true);
                                $('#inputCheckedstepsclips2').prop('disabled',false);
                                $('#inputCheckedsteps8').prop('disabled',false);
                            }
                            else{
                                // $('.checkbox-custom > input').prop('checked',false);
                                $('#inputCheckedstepsrvscheme2').prop('checked',false);
                                $('#inputCheckedstepsrv2scheme2').prop('checked',false);
                                // $('#inputCheckedstepsclips2').prop('checked',false);
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.splice(arr.indexOf('<?= CarsAct::ELEMENT_RV?>'), 1);
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));
                            };
                       "
                       />
                <label for="inputCheckedstepsrv2">ЗФ</label>
            </div>
            <ul class="hidden">
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsrvscheme2"
                               name="steps[]" <?= $model->auto->json('rv1') ? '' : 'disabled' ?> value="61"/>
                        <label for="inputCheckedstepsrvscheme2">Схема креплений</label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsrv2scheme2"
                               name="steps[]" <?= $model->auto->json('rv1') ? '' : 'disabled' ?> value="62"/>
                        <label for="inputCheckedstepsrv2scheme2">Схема креплений (Треугольная)</label>
                    </div>
                </li>
            </ul>
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="elemRv2"
                       name="<?= CarsAct::ELEMENT_RV?>" value="<?= CarsAct::ELEMENT_RV?>"/>
            </div>
        </div>
        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ЗФ ##### END #####-->

        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ЗШ ##### START #####-->
        <div class="col-md-2">
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="inputCheckedstepsbw2"
                       name="steps[]" <?= $model->auto->json('bw1') ? '' : 'disabled' ?> value="7"
                       <?= ((!$model->auto->json('bw') && !$model->auto->receiveMasteringActByElement(CarsAct::ELEMENT_BW))) ? '' : 'disabled' ?>
                       onchange = "
                            if ($(this).prop('checked')){
                                // $('.checkbox-custom > input').prop('checked',false);
                                $(this).prop('checked',true);
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.push('<?= CarsAct::ELEMENT_BW?>');
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));
                                $('#inputCheckedstepsbwscheme2').prop('checked',true);
                                $('#inputCheckedstepsbw2scheme2').prop('checked',true);
                                // $('#inputCheckedstepsclips2').prop('checked',true);
                                $('#inputCheckedstepsclips2').prop('disabled',false);
                                $('#inputCheckedsteps8').prop('disabled',false);
                            }
                            else{
                                // $('.checkbox-custom > input').prop('checked',false);
                                $('#inputCheckedstepsbwscheme2').prop('checked',false);
                                $('#inputCheckedstepsbw2scheme2').prop('checked',false);
                                // $('#inputCheckedstepsclips2').prop('checked',false);
                                var str = $('#modal-2 [name=\'elem\']').val();
                                var arr = str.split(',');
                                arr.splice(arr.indexOf('<?= CarsAct::ELEMENT_BW?>'), 1);
                                $('#modal-2 [name=\'elem\']').val(arr.join(','));
                            };
                       "
                       />
                <label for="inputCheckedstepsbw2">ЗШ</label>
            </div>
            <ul class="hidden">
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsbwscheme2"
                               name="steps[]" <?= $model->auto->json('bw1') ? '' : 'disabled' ?> value="71"/>
                        <label for="inputCheckedstepsbwscheme2">Схема креплений</label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputCheckedstepsbw2scheme2"
                               name="steps[]" <?= $model->auto->json('bw1') ? '' : 'disabled' ?> value="72"/>
                        <label for="inputCheckedstepsbw2scheme2">Схема креплений (2 части)</label>
                    </div>
                </li>
            </ul>
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="elemBw2"
                       name="<?= CarsAct::ELEMENT_BW?>" value="<?= CarsAct::ELEMENT_BW?>"/>
            </div>
        </div>
        <!-- СЦЕНАРИЙ ОСВОЕНИЯ ЗШ ##### END #####-->


        <input type="hidden" name="id" value="<?= $model->auto->id ?>">
        <input type="hidden" name="step" value="1">
        <input type="hidden" name="elem" value="">

        <div class="pull-right text-right form-group">
            <div class="">Ответственный:</div>
            <h4 class=""> <?= $this->renderDynamic('return Yii::$app->user->identity->name;') ?></h4>
        </div>
        <br>
        <button type="submit" class="btn btn-outline btn-round btn-primary">Далее</button>
    </form>

    <?php Modal::end(); ?>
    <?endif;?>
    <!-- КНОПКА ВНЕСЕНИЯ ИЗМЕНЕНИЙ, ЕСЛИ ЕСТЬ ОСВОЕННЫЕ ЭЛЕМЕНТЫ -->
    <?php Modal::begin([
        // 'size'=>Modal::SIZE_SMALL,
        'id' => 'edit-car-modal',
        'options' => ['class' => 'text-left'],
        'header' => Html::tag('h4', $model->mastered ? Yii::t('app', 'Акт внесеня изменений') : Yii::t('app', 'Акт освоения'), ['class' => 'modal-title']),
        'toggleButton' => ['style' => 'margin-right:5px;', 'class' => ' btn btn-round btn-info btn-outline', 'label' => Yii::t('app', 'Внести изменения')],
    ]); ?>

    <form action="<?= Url::to(['update']) ?>" id="modal">
        <h5>Сведения</h5>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedsteps1" name="steps[]"
                   value="1" />
            <label for="inputCheckedsteps1">Основные</label>
        </div>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedsteps8" name="steps[]"
                   value="8" />
            <label for="inputCheckedsteps8">Дополнительные</label>
        </div>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsclips" name="steps[]" value="9"/>
            <label for="inputCheckedstepsclips">Крепления</label>
        </div>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsanalog" name="steps[]" value="10"/>
            <label for="inputCheckedstepsanalog">Аналоги</label>
        </div>
        <h5>Продукция</h5>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsfw"
                   name="steps[]" <?= $model->auto->json('fw') ? '' : 'disabled' ?> value="2" 
                   onchange = "checkBoxing(this,'#inputCheckedstepsfw','<?= CarsAct::ELEMENT_FW?>')"/>
            <label for="inputCheckedstepsfw">ПШ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfwscheme"
                           name="steps[]" <?= $model->auto->json('fw1') && $model->auto->json('fw') ? '' : 'disabled' ?> value="21"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfw','<?= CarsAct::ELEMENT_FW?>')" />
                    <label for="inputCheckedstepsfwscheme">Схема замеров</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsfv"
                   name="steps[]"<?= $model->auto->json('fv1') && $model->auto->json('fv') ? '' : 'disabled' ?> value="3"
                   onchange = "checkBoxing(this,'#inputCheckedstepsfv','<?= CarsAct::ELEMENT_FV?>')" />
            <label for="inputCheckedstepsfv">ПФ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfvscheme"
                           name="steps[]"<?= $model->auto->json('fv1') && $model->auto->json('fv')? '' : 'disabled' ?> value="31"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfv','<?= CarsAct::ELEMENT_FV?>')" 
                           />
                    <label for="inputCheckedstepsfvscheme">Схема креплений</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsfd"
                   name="steps[]" <?= $model->auto->json('fd1') && $model->auto->json('fd')? '' : 'disabled' ?> value="4"
                   onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')"
                   />
            <label for="inputCheckedstepsfd">ПБ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfdscheme"
                           name="steps[]" <?= $model->auto->json('fd1') && $model->auto->json('fd')? '' : 'disabled' ?> value="41"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')" 
                           />
                    <label for="inputCheckedstepsfdscheme">Схема креплений</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfd2scheme"
                           name="steps[]" <?= $model->auto->json('fd1') && $model->auto->json('fd')? '' : 'disabled' ?> value="42"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')"
                           />
                    <label for="inputCheckedstepsfd2scheme">Схема креплений (Укороченный)</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfd3scheme"
                           name="steps[]" <?= $model->auto->json('fd1') && $model->auto->json('fd')? '' : 'disabled' ?> value="43"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')"
                           />
                    <label for="inputCheckedstepsfd3scheme">Схема креплений (Вырез для курящих)</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfd4scheme"
                           name="steps[]" <?= $model->auto->json('fd1') && $model->auto->json('fd')? '' : 'disabled' ?> value="44"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')"
                           />
                    <label for="inputCheckedstepsfd4scheme">Схема креплений (Вырез для зеркала)</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsrd"
                   name="steps[]" <?= $model->auto->json('rd1') && $model->auto->json('rd')? '' : 'disabled' ?> value="5"
                   onchange = "checkBoxing(this,'#inputCheckedstepsrd','<?= CarsAct::ELEMENT_RD?>')"
                   />
            <label for="inputCheckedstepsrd">ЗБ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsrdscheme"
                           name="steps[]" <?= $model->auto->json('rd1') && $model->auto->json('rd')? '' : 'disabled' ?> value="51"
                           onchange = "checkBoxing(this,'#inputCheckedstepsrd','<?= CarsAct::ELEMENT_RD?>')"
                           />
                    <label for="inputCheckedstepsrdscheme">Схема креплений</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsrv"
                   name="steps[]" <?= $model->auto->json('rv1') && $model->auto->json('rv')? '' : 'disabled' ?> value="6"
                   onchange = "checkBoxing(this,'#inputCheckedstepsrv','<?= CarsAct::ELEMENT_RV?>')"
                   />
            <label for="inputCheckedstepsrv">ЗФ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsrvscheme"
                           name="steps[]" <?= $model->auto->json('rv1') && $model->auto->json('rv')? '' : 'disabled' ?> value="61"
                           onchange = "checkBoxing(this,'#inputCheckedstepsrv','<?= CarsAct::ELEMENT_RV?>')"
                           />
                    <label for="inputCheckedstepsrvscheme">Схема креплений</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsrv2scheme"
                           name="steps[]" <?= $model->auto->json('rv1') && $model->auto->json('rv')? '' : 'disabled' ?> value="62"
                           onchange = "checkBoxing(this,'#inputCheckedstepsrv','<?= CarsAct::ELEMENT_RV?>')"
                           />
                    <label for="inputCheckedstepsrv2scheme">Схема креплений (Треугольная)</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsbw"
                   name="steps[]" <?= $model->auto->json('bw1') && $model->auto->json('bw')? '' : 'disabled' ?> value="7"
                   onchange = "checkBoxing(this,'#inputCheckedstepsbw','<?= CarsAct::ELEMENT_BW?>')"
                   />
            <label for="inputCheckedstepsbw">ЗШ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsbwscheme"
                           name="steps[]" <?= $model->auto->json('bw1') && $model->auto->json('bw')? '' : 'disabled' ?> value="71"
                           onchange = "checkBoxing(this,'#inputCheckedstepsbw','<?= CarsAct::ELEMENT_BW?>')"
                           />
                    <label for="inputCheckedstepsbwscheme">Схема креплений</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsbw2scheme"
                           name="steps[]" <?= $model->auto->json('bw1') && $model->auto->json('bw')? '' : 'disabled' ?> value="72"
                           onchange = "checkBoxing(this,'#inputCheckedstepsbw','<?= CarsAct::ELEMENT_BW?>')"
                           />
                    <label for="inputCheckedstepsbw2scheme">Схема креплений (2 части)</label>
                </div>
            </li>
        </ul>

        <input type="hidden" name="id" value="<?= $model->auto->id ?>">
        <input type="hidden" name="step" value="1">
        <input type="hidden" name="elem" value="">

        <div class="pull-right text-right form-group">
            <div class="">Ответственный:</div>
            <h4 class=""> <?= $this->renderDynamic('return Yii::$app->user->identity->name;') ?></h4>
        </div>
        <br>
        <button type="submit" class="btn btn-outline btn-round btn-primary">Далее</button>
    </form>

    <?php Modal::end(); ?>        

    <?= ($actos = $model->auto->getActs()->andWhere(['parent_id' => null])->one()) !== null ? Html::a(Yii::t('app', 'Акты <span class="glyphicon glyphicon-share-alt"></span>'), ['carsact/view', 'id' => $model->auto->id], ['style' => 'margin-right:5px;', 'class' => ' btn btn-outline btn-round  btn-default']) : '' ?>

    <!--     <?= $model->auto->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->auto->id], [
        'class' => ' btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?> -->
    <?endif;?>

    <?php if (!$model->checked || !$model->mastered): ?>
        <div class="alert alert-primary text-left" role="alert" style="margin-top:5px">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Создал: <?= @$model->auto->author->name.' '.Yii::$app->formatter->asDateTime(@$model->auto->created_at)?> <br>
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Проверил: <?= @$model->auto->checkedAuthor->name.' '.Yii::$app->formatter->asDateTime(@$model->auto->checked_date)?> 
        </div>
    <?endif;?>

</div>

<div class="clearfix"></div>

<div class="row">
    <?= Html::beginForm(['change-check-rework', 'id' => $model->auto->id], 'post', ['data-pjax' => '', 'id' => 'checkeds']); ?>
    <div class="col-md-5">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('article'),
                    'value' => ($model->checked ? $model->article: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('article')])
                           .Html::tag('label',($model->article)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),            
                ],  
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('mark'),
                    'value' => ($model->checked ? $model->mark: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('mark')])
                           .Html::tag('label',($model->mark)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),            
                ],              
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('model'),
                    'value' => ($model->checked ? $model->model: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('model')])
                           .Html::tag('label',($model->model)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('generation'),
                    'value' => ($model->checked ? $model->generation: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('generation')])
                           .Html::tag('label',($model->generation)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('carcass'),
                    'value' => ($model->checked ? $model->carcass: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('carcass')])
                           .Html::tag('label',($model->carcass)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('doors'),
                    'value' => ($model->checked ? $model->doors: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('doors')])
                           .Html::tag('label',($model->doors)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('firstyear'),
                    'value' => ($model->checked ? $model->firstyear: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('firstyear')])
                           .Html::tag('label',($model->firstyear)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('lastyear'),
                    'value' => ($model->checked ? $model->lastyear: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('lastyear')])
                           .Html::tag('label',($model->lastyear)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('modification'),
                    'value' => ($model->checked ? $model->modification: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('modification')])
                           .Html::tag('label',($model->modification)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('country'),
                    'value' => ($model->checked ? $model->country: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('country')])
                           .Html::tag('label',($model->country)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('category'),
                    'value' => ($model->checked ? $model->category: 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('category')])
                           .Html::tag('label',($model->category)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('blanket_default'),
                    'value' => ($model->checked ? $model->blanket_default:
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false,
                                ['value' => $model->getAttributeLabel('blanket_default')])
                           .Html::tag('label',($model->blanket_default)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('blanket_custom'),
                    'value' => ($model->checked ? $model->blanket_custom:
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false,
                                ['value' => $model->getAttributeLabel('blanket_custom')])
                           .Html::tag('label',($model->blanket_custom)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    )),
                ],
            ],
        ]) ?>

        <a href="#inputCheckedsteps1" class="link-dashed <?= $model->mastered ? '' : 'hidden' ?>" data-toggle="modal"
           data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a>
    </div>
    <?php if (!$model->checked): ?>
    <?= Html::submitButton('Отправить на доработку',[
        'class' => ' btn btn-icon btn-outline btn-round  btn-danger ',
        'data-original-title' => Yii::t('yii', 'Add'),

        ]) ?>
    <?endif;?>
    <?= Html::endForm() ?>
    <div class="col-md-7">
        <div class="col-md-12">
            <img width="100%" src="/img/car.png" style="position: relative;">
            <?= $model->auto->json('fw1') ? '<img width="100%" src="/img/fw.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= $model->auto->json('fv1') ? '<img width="100%" src="/img/fv.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= $model->auto->json('fd1') ? '<img width="100%" src="/img/fd.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= $model->auto->json('rd1') ? '<img width="100%" src="/img/rd.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= $model->auto->json('rv1') ? '<img width="100%" src="/img/rv.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= $model->auto->json('bw1') ? '<img width="100%" src="/img/bw.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>

            <a href="#inputCheckedstepsfw" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= $model->auto->json('fw1') ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->auto->json('fw') ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 26%;top: 62%;"></i></a>
            <a href="#inputCheckedstepsfv" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= $model->auto->json('fv1') ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->auto->json('fv') ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 32.5%;top: 17%;"></i></a>
            <a href="#inputCheckedstepsfd" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= $model->auto->json('fd1') ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->auto->json('fd') ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 45%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsrd" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= $model->auto->json('rd1') ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->auto->json('rd') ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 60%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsrv" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= $model->auto->json('rv1') ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->auto->json('rv') ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 70%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsbw" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= $model->auto->json('bw1') ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->auto->json('bw') ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 71%;top: 60%;"></i></a>
        </div>
        <div class="clearfix"></div>

        <? if ($model->auto->json('info')): ?>
            <div class="alert dark alert-icon alert-warning alert-dismissible alert-alt dark" role="alert">
                <i class="icon wb-warning" aria-hidden="true"
                   style="font-size: 30px;margin: -8px;"></i> <?= $model->auto->json('info') ?>&nbsp;
            </div>
        <? endif ?>

    </div>
    <div class="clearfix"></div>

    <? if ($model->mastered): ?>
        <hr>
        <div class="col-md-7">
            <h4>Дополнительные сведения</h4>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => 
                    ArrayHelper::merge([
                        // 'mark_en',
                        // 'model_en',
                        // 'modification_en',
                        // 'country_en'
                    ],$fields),
            ]) ?>
            <a href="#inputCheckedsteps8" class="link-dashed" data-toggle="modal"
               data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a>

        </div>
        <div class="col-md-5">
            <h4>Крепления</h4>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $clipsProvider,
                'show' => ['name', 'type'],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    'type',
                ],
            ]); ?>
            <br>
            <a href="#inputCheckedstepsclips" class="link-dashed" data-toggle="modal"
               data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a>

        </div>
        <div class="clearfix"></div>

        <hr>
        <div class="col-md-12">
            <h4>Аналоги</h4>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $analogsProvider,
                'show' => ['analog.name', 'elements'],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'analog.name',
                        'format' => 'html',
                        'value' => function ($data) {
                            return Html::a($data->analog->name, ['update', 'id' => $data->analog->id]);
                        },
                    ],
                    [
                        'attribute' => 'elements',
                        'label' => Yii::t('app', 'Элементы'),
                        'format' => 'html',
                        'value' => function ($data) {
                            return implode(', ', $data->json('elements'));
                        },
                    ],
                ],
            ]); ?>
            <br>
            <a href="#inputCheckedstepsanalog" class="link-dashed" data-toggle="modal"
               data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a>

        </div>

        <div class="clearfix"></div>
        <hr>

        <?= \backend\widgets\instructions\Instructions::widget(['car' => $model->auto->id])?>

        <hr>
        <div class="col-md-12 productions">
            <h4>Продукция</h4>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="<?= $model->auto->json('fw1') ? 'visible' : 'hidden' ?>"><a
                            href="#_view_2" aria-controls="_view_2" role="tab"
                            data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->auto->json('fw') ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                        ПШ</a></li>
                <li role="presentation" class="<?= $model->auto->json('fv1') ? 'visible' : 'hidden' ?>"><a
                            href="#_view_3" aria-controls="_view_3" role="tab"
                            data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->auto->json('fv') ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                        ПФ</a></li>
                <li role="presentation" class="<?= $model->auto->json('fd1') ? 'visible' : 'hidden' ?>"><a
                            href="#_view_4" aria-controls="_view_4" role="tab"
                            data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->auto->json('fd') ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                        ПБ</a></li>
                <li role="presentation" class="<?= $model->auto->json('rd1') ? 'visible' : 'hidden' ?>"><a
                            href="#_view_5" aria-controls="_view_5" role="tab"
                            data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->auto->json('rd') ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                        ЗБ</a></li>
                <li role="presentation" class="<?= $model->auto->json('rv1') ? 'visible' : 'hidden' ?>"><a
                            href="#_view_6" aria-controls="_view_6" role="tab"
                            data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->auto->json('rv') ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                        ЗФ</a></li>
                <li role="presentation" class="<?= $model->auto->json('bw1') ? 'visible' : 'hidden' ?>"><a
                            href="#_view_7" aria-controls="_view_7" role="tab"
                            data-toggle="tab"><?= Html::tag('i', '', ['class' => $model->auto->json('bw') ? 'wb-check text-success' : 'wb-close text-danger']) ?>
                        ЗШ</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="_view_2">
                    <br>
                    <?if ($model->auto->json('fw')): ?> 
                    <p><a href="#inputCheckedstepsfw" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                    <?= $this->render('_view_2', [
                        'model' => $model,
                    ]) ?>
                    <?else:?>
                        <p><a href="" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal2"><?= Yii::t('app', 'Освоить') ?></a></p>
                    <?endif;?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="_view_3">
                    <br>
                    <?if ($model->auto->json('fv')): ?> 
                    <p><a href="#inputCheckedstepsfv" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                    <?= $this->render('_view_3', [
                        'model' => $model,
                    ]) ?>
                    <?else:?>
                        <p><a href="" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal2"><?= Yii::t('app', 'Освоить') ?></a></p>
                    <?endif;?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="_view_4">
                    <br>
                    <?if ($model->auto->json('fd')): ?> 
                    <p><a href="#inputCheckedstepsfd" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                    <?= $this->render('_view_4', [
                        'model' => $model,
                    ]) ?>
                    <?else:?>
                        <p><a href="" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal2"><?= Yii::t('app', 'Освоить') ?></a></p>
                    <?endif;?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="_view_5">
                    <br>
                    <?if ($model->auto->json('rd')): ?> 
                    <p><a href="#inputCheckedstepsrd" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                    <?= $this->render('_view_5', [
                        'model' => $model,
                    ]) ?>
                    <?else:?>
                        <p><a href="" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal2"><?= Yii::t('app', 'Освоить') ?></a></p>
                    <?endif;?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="_view_6">
                    <br>
                    <?if ($model->auto->json('rv')): ?> 
                    <p><a href="#inputCheckedstepsrv" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                    <?= $this->render('_view_6', [
                        'model' => $model,
                    ]) ?>
                    <?else:?>
                        <p><a href="" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal2"><?= Yii::t('app', 'Освоить') ?></a></p>
                    <?endif;?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="_view_7">
                    <br>
                    <?if ($model->auto->json('bw')): ?> 
                    <p><a href="#inputCheckedstepsbw" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>
                    <?= $this->render('_view_7', [
                        'model' => $model,
                    ]) ?>
                    <?else:?>
                        <p><a href="" class="link-dashed" data-toggle="modal"
                          data-target="#edit-car-modal2"><?= Yii::t('app', 'Освоить') ?></a></p>
                    <?endif;?>
                </div>
            </div>

        </div>
    <? endif ?>
</div>


