<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */

Yii::$app->view->registerJs('

    function parseitemsjson () {
        var analogs =[];
        $(\'.analogsitems .item\').each(function( index, value ) {
            itemjson=$(this).find("input").serializeArray();
            var obj ={};
            var elements =[];
            $.each(itemjson, function( index1, value1 ) {
                if (value1.name=="elements"){
                    elements.push(value1.value);
                } else {
                    obj[value1.name]=value1.value;
                }
            });
            obj[\'elements\']=elements;
            analogs.push(obj);
        });
        $(\'#carsform-analogs\').val(JSON.stringify(analogs));
    }

    var edititem = null;

    $("body").on("click",".newanalogs",function(e){
        window.edititem = null;
        $(".analogsmodalform input").val("");
        $(".analogsmodalform select").val("");
        $(".analogsmodalform").modal("show") 
    });

    $("body").on("click",".editanalogs",function(e){
        window.edititem = $(this).parent();
        $(".analogsmodalform input").val("");
        $(".analogsmodalform select").val("");
        $(".analogsmodalform .analogs-car").val($(this).parent().find("input[name=\'car\']").val());
        $(".analogsmodalform").modal("show") 
        e.preventDefault();
    });

    $("body").on("submit",".saveanalogs",function(e){
        $(".analogsitems .newitem input[name=\'car\']").val($(".analogsmodalform .analogs-car").val());
        $(".analogsitems .newitem input[name=\'name\']").val($(".analogsmodalform .analogs-car option:selected").text());
        $(".analogsitems .newitem input[name=\'name\']").prev().text($(".analogsmodalform .analogs-car option:selected").text());

        if (window.edititem){
            $(".analogsitems .newitem").clone().insertAfter(window.edititem).removeClass("hidden newitem").addClass("item");
            window.edititem.remove();
        } else {
            $(".analogsitems .newitem").clone().appendTo(".analogsitems").removeClass("hidden newitem").addClass("item");
        }

        parseitemsjson();
        $(".analogsmodalform").modal("hide") 
        e.preventDefault();
    });

    $("body").on("click",".analogsitems .deletethisitem",function(e){
        var item = $(this).parent();
        notie.confirm("' . Yii::t('app', 'Удалить аналог?') . '", "' . Yii::t('yii', 'Yes') . '", "' . Yii::t('yii', 'No') . '", function() {
            item.remove();
            parseitemsjson();
        });
        e.preventDefault();
    });

    $("body").on("change",".analogsitems .analogelements",function(e){
        parseitemsjson();
        e.preventDefault();
    });
    ', \yii\web\View::POS_END);


?>
<h3>Аналоги</h3>

<div class="analogsitems">
    <div class="newitem hidden">
        <a href="#" class="editanalogs"></a>
        <input type="hidden" name="name">
        <input type="hidden" name="car">
        <label>&nbsp;ПШ&nbsp;<?= Html::checkbox('elements', false, ['class' => 'analogelements', 'value' => 'ПШ']); ?>
            &nbsp;</label>
        <label>&nbsp;ПФ&nbsp;<?= Html::checkbox('elements', false, ['class' => 'analogelements', 'value' => 'ПФ']); ?>
            &nbsp;</label>
        <label>&nbsp;ПБ&nbsp;<?= Html::checkbox('elements', false, ['class' => 'analogelements', 'value' => 'ПБ']); ?>
            &nbsp;</label>
        <label>&nbsp;ЗБ&nbsp;<?= Html::checkbox('elements', false, ['class' => 'analogelements', 'value' => 'ЗБ']); ?>
            &nbsp;</label>
        <label>&nbsp;ЗФ&nbsp;<?= Html::checkbox('elements', false, ['class' => 'analogelements', 'value' => 'ЗФ']); ?>
            &nbsp;</label>
        <label>&nbsp;ЗШ&nbsp;<?= Html::checkbox('elements', false, ['class' => 'analogelements', 'value' => 'ЗШ']); ?>
            &nbsp;</label>
        <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
    </div>
    <?
    $analogs = Json::decode($model->analogs);
    if ($analogs)
        foreach ($analogs as $krep):
            $elements = [];
            if (isset($krep['elements']) && $krep['elements'])
                foreach ($krep['elements'] as $key => $value) {
                    $elements[$value] = $value;
                }
            ?>
            <div class="item">
                <a href="#" class="editanalogs"><?= Html::encode($krep['name']) ?></a>
                <input type="hidden" name="name" value="<?= Html::encode($krep['name']); ?>">
                <input type="hidden" name="car" value="<?= Html::encode($krep['car']); ?>">
                <label>&nbsp;ПШ&nbsp;<?= Html::checkbox('elements', $checked = isset($elements['ПШ']) ? true : false, ['class' => 'analogelements', 'value' => 'ПШ']); ?>
                    &nbsp;</label>
                <label>&nbsp;ПФ&nbsp;<?= Html::checkbox('elements', $checked = isset($elements['ПФ']) ? true : false, ['class' => 'analogelements', 'value' => 'ПФ']); ?>
                    &nbsp;</label>
                <label>&nbsp;ПБ&nbsp;<?= Html::checkbox('elements', $checked = isset($elements['ПБ']) ? true : false, ['class' => 'analogelements', 'value' => 'ПБ']); ?>
                    &nbsp;</label>
                <label>&nbsp;ЗБ&nbsp;<?= Html::checkbox('elements', $checked = isset($elements['ЗБ']) ? true : false, ['class' => 'analogelements', 'value' => 'ЗБ']); ?>
                    &nbsp;</label>
                <label>&nbsp;ЗФ&nbsp;<?= Html::checkbox('elements', $checked = isset($elements['ЗФ']) ? true : false, ['class' => 'analogelements', 'value' => 'ЗФ']); ?>
                    &nbsp;</label>
                <label>&nbsp;ЗШ&nbsp;<?= Html::checkbox('elements', $checked = isset($elements['ЗШ']) ? true : false, ['class' => 'analogelements', 'value' => 'ЗШ']); ?>
                    &nbsp;</label>
                <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
            </div>
        <? endforeach ?>
</div>

<?= Html::a('<i class="icon wb-plus"></i>', '#', ['class' => 'newanalogs btn btn-icon btn-round btn-default btn-outline']) ?>

<p>&nbsp;</p>
