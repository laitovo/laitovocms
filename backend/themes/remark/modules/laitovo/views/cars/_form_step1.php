<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */

Yii::$app->view->registerJs("
    $('body').on('change','.windowcheckbox', function (e) {
    	img=$('.carscheme img.'+$(this).attr('id').split('-')[1]);
    	if ($(this).is(':checked')) {
    		img.show();
    	} else {
    		img.hide();
    	}
        e.preventDefault();
    });
    $('body').on('click','.carscheme a', function (e) {
		btn=$('#carsform-'+$(this).attr('href').split('#')[1]+'.windowcheckbox');
		btn.click();
        e.preventDefault();
    });
");

?>
<h3>Основные сведения</h3>
<?= $form->field($model, 'mark')->textInput([
    'maxlength' => true,
    'onchange' => '
        $(\'#carsform-mark_en\').val($(this).val());
    ',
]) ?>
<?= $form->field($model, 'mark_en')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'model')->textInput([
    'maxlength' => true,
    'onchange' => '
        $(\'#carsform-model_en\').val($(this).val());
    ',
]) ?>
<?= $form->field($model, 'model_en')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'modification')->textInput([
    'maxlength' => true,
    'onchange' => '
        $(\'#carsform-modification_en\').val($(this).val());
    ',
]) ?>
<?= $form->field($model, 'modification_en')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'firstyear')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'lastyear')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'generation')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'carcass')->dropDownList($model->_carcass) ?>
<?= $form->field($model, 'doors')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
<?//= $form->field($model, 'country_en')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'category')->dropDownList($model->_category) ?>

<div class="row">
    <div class="col-md-5">
        <h5>Оконные проемы автомобиля</h5>
        <?= $form->field($model, 'fw1')->checkbox(['class' => 'windowcheckbox']) ?>
        <?= $form->field($model, 'fv1')->checkbox(['class' => 'windowcheckbox']) ?>
        <?= $form->field($model, 'fd1')->checkbox(['class' => 'windowcheckbox']) ?>
        <?= $form->field($model, 'rd1')->checkbox(['class' => 'windowcheckbox']) ?>
        <?= $form->field($model, 'rv1')->checkbox(['class' => 'windowcheckbox']) ?>
        <?= $form->field($model, 'bw1')->checkbox(['class' => 'windowcheckbox']) ?>

        <div class="alert dark alert-icon alert-info alert-dismissible alert-alt dark" role="alert">
            <i class="icon wb-info" aria-hidden="true"></i> Укажите оконные проемы автомобиля
        </div>

    </div>

    <div class="col-md-7">
        <div class="col-md-12 carscheme">
            <img width="100%" src="/img/car.png" style="position: relative;">
            <img width="100%" class="fw1" src="/img/fw.png"
                 style="<?= $model->fw1 ? '' : 'display: none;' ?> position: absolute;left: 0;top: -15px;padding: 15px">
            <img width="100%" class="fv1" src="/img/fv.png"
                 style="<?= $model->fv1 ? '' : 'display: none;' ?> position: absolute;left: 0;top: -15px;padding: 15px">
            <img width="100%" class="fd1" src="/img/fd.png"
                 style="<?= $model->fd1 ? '' : 'display: none;' ?> position: absolute;left: 0;top: -15px;padding: 15px">
            <img width="100%" class="rd1" src="/img/rd.png"
                 style="<?= $model->rd1 ? '' : 'display: none;' ?> position: absolute;left: 0;top: -15px;padding: 15px">
            <img width="100%" class="rv1" src="/img/rv.png"
                 style="<?= $model->rv1 ? '' : 'display: none;' ?> position: absolute;left: 0;top: -15px;padding: 15px">
            <img width="100%" class="bw1" src="/img/bw.png"
                 style="<?= $model->bw1 ? '' : 'display: none;' ?> position: absolute;left: 0;top: -15px;padding: 15px">

            <a href="#fw1"><i style="position: absolute;left: 14%;top: 55%;height: 69px;width: 127px;"></i></a>
            <a href="#fv1"><i style="position: absolute;left: 29.5%;top: 12%;width: 39px;height: 38px;"></i></a>
            <a href="#fd1"><i style="position: absolute;left: 37%;top: 5%;width: 94px;height: 70px;"></i></a>
            <a href="#rd1"><i style="position: absolute;left: 55%;top: 6%;width: 73px;height: 57px;"></i></a>
            <a href="#rv1"><i style="position: absolute;left: 68%;top: 8%;width: 50px;height: 45px;"></i></a>
            <a href="#bw1"><i style="position: absolute;left: 61%;top: 54%;width: 127px;height: 50px;"></i></a>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
