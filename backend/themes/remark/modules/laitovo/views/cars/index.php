<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Cars;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Автомобили');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');

$fields = [];
foreach ($model->auto->fields as $key => $value) {
    $fields[] = [
        'attribute' => $value['fields']['id']['value'],
        'label' => $value['fields']['label']['value'],
        'format' => 'text',
        'value' => function ($data) use ($value) {
            return isset($data[$value['fields']['id']['value']]) ? $data[$value['fields']['id']['value']] : '';
        },
    ];
}

?>

<?= Html::beginForm([''], 'post',['id' => 'hide-form']); ?>

<div class="col-md-3">
<?=
Html::tag('div',
        Html::checkbox("hide[value]",
            $checked = $checked
            )
       .Html::tag('label','Скрывать нулевые лекала'),
        [
            'class' => 'checkbox-custom checkbox-primary text-left',
            'onchange' => '$("#hide-form").submit();'
        ]
);
?>
</div>
<?= Html::hiddenInput("hide[check]", $value = true); ?>

<div class="col-md-offset-3 col-md-6">
    <div class="pull-right">
        <div class="col-md-5">
            <?= Html::input($type = 'text', 'articlesearch', $value = '', ['class' => 'form-control','placeholder' => 'поиск по артикулу...']); ?>
        </div>
        <div class="col-md-5">        <?= Html::input($type = 'text', 'lekalosearch', $value = '', ['class' => 'form-control','placeholder' => 'поиск по лекалу...']); ?>
        </div>
        <div class="col-md-2">
           <div class="pull-right"> <?= Html::submitButton($content="Найти", ['class' => 'btn btn-success btn-sm']); ?></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<?= Html::endForm() ?>
<div class="clearfix"></div>

<hr>
<div class="clearfix"></div>

<?= Html::a('Добавить автомобиль', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>
    <span> </span>

<?if (Cars::receiveNotCheckedCarsCount()):?>
<?= Html::a(Yii::t('app', 'Проверить <span class="badge">'.Cars::receiveNotCheckedCarsCount().'</span>'), ['check'], ['class' => 'btn  btn-round  btn-primary']) ?>
<?endif;?>

<?if (Cars::receiveNotCheckedReworkCarsCount()):?>
<?= Html::a(Yii::t('app', 'На доработку <span class="badge">'.Cars::receiveNotCheckedReworkCarsCount().'</span>'), ['check-rework'], ['class' => 'btn  btn-round  btn-warning']) ?>
<?endif;?>


<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name'],
    'columns' => ArrayHelper::merge([
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'article',
            'label' => $model->getAttributeLabel('article'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['name'] ? $data['article'] : '';
            },
        ],
        [
            'attribute' => 'name',
            'label' => $model->getAttributeLabel('name'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['name'] ? Html::a($data['name'], ['view', 'id' => $data['id']]) : '';
            },
        ],
        [
            'attribute' => 'mark',
            'label' => $model->getAttributeLabel('mark'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['mark'] ? $data['mark'] : '';
            },
        ],
        [
            'attribute' => 'model',
            'label' => $model->getAttributeLabel('model'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['model'] ? $data['model'] : '';
            },
        ],
        [
            'attribute' => 'generation',
            'label' => $model->getAttributeLabel('generation'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['generation'] ? $data['generation'] :'';
            },
        ],
        [
            'attribute' => 'carcass',
            'label' => $model->getAttributeLabel('carcass'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['carcass'] ? $data['carcass'] : '';
            },
        ],
        [
            'attribute' => 'doors',
            'label' => $model->getAttributeLabel('doors'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['doors'] ? $data['doors'] : '';
            },
        ],
        [
            'attribute' => 'firstyear',
            'label' => $model->getAttributeLabel('firstyear'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['firstyear'] ? $data['firstyear'] : '';
            },
        ],
        [
            'attribute' => 'lastyear',
            'label' => $model->getAttributeLabel('lastyear'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['lastyear'] ? $data['lastyear'] : '';
            },
        ],
        [
            'attribute' => 'modification',
            'label' => $model->getAttributeLabel('modification'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['modification'] ? $data['modification'] : '';
            },
        ],
        [
            'attribute' => 'country',
            'label' => $model->getAttributeLabel('country'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['country'] ? $data['country'] : '';
            },
        ],
        [
            'attribute' => 'category',
            'label' => $model->getAttributeLabel('category'),
            'format' => 'html',
            'value' => function ($data) {
                return $data['category'] ? $data['category'] : '';
            },
        ],
        [
            'attribute' => 'printclips',
            'label' => 'Печать "клипсы"',
            'format' => 'raw',
            'value' => function ($data) {
                return $data ['mastered'] ? Html::a('<span class="glyphicon glyphicon-file"></span>', ['carsact/actos-last','id' => $data['id'],'p' => 2], [
                        'title' => Yii::t('app', 'Просмотреть акты'),
                        'target' => '_blank'
                ]) : 'Не освноена';
            },
        ],
        [
            'attribute' => 'printotk',
            'label' => 'Печать "Отк"',
            'format' => 'raw',
            'value' => function ($data) {
                return $data ['mastered'] ? Html::a('<span class="glyphicon glyphicon-file"></span>', ['carsact/actos-last','id' => $data['id'],'p' => 7], [
                        'title' => Yii::t('app', 'Просмотреть акты'),
                        'target' => '_blank'
                ]) : 'Не освноена';
            },
        ],

        // 'mounting:ntext',
        // 'fw:ntext',
        // 'fv:ntext',
        // 'fd:ntext',
        // 'rd:ntext',
        // 'rv:ntext',
        // 'bw:ntext',
        // 'json:ntext',
    ], $fields, [
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {acts}',
            'buttonOptions' => ['class' => 'deleteconfirm'],
            'buttons'=>[
                'acts' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-file"></span>', ['carsact/view','id' => $model['id']], [
                            'title' => Yii::t('app', 'Просмотреть акты'),
                    ]);
                },
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['cars/view','id' => $model['id']], [
                            'title' => Yii::t('app', 'Просмотреть автомобиль'),
                    ]);
                }
            ],
        ],
    ]),
]); ?>

