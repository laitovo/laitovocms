<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;



/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\modules\laitovo\models\CarsFormSearch */
/* @var $dataProviderYes \yii\data\ActiveDataProvider */
/* @var $dataProviderNo \yii\data\ActiveDataProvider */
/* @var $search string */

$this->title = Yii::t('app', 'Автомобили');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');

?>

<div class="naryad-search">

    <?php $form = ActiveForm::begin([
        'id' => 'my-search-form',
        'action' => ['blanket-edit'],
        'method' => 'get',
        'options' => ['data-pjax' => '1'],
    ]); ?>

    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control" value="<?=Yii::$app->request->get('search')?>" name="search" id="cars-name" placeholder="<?=Yii::t('app', 'Поиск')?>">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo Html::tag('div',
                    Html::checkbox("emptyBlanket",
                        $checked = Yii::$app->request->get('emptyBlanket'),
                        ['value' => 'emptyBlanket'])
                    .Html::tag('label', 'Только те, где не проставлены одеяла'),
                    ['class' => 'checkbox-custom checkbox-primary text-left']
                );
            ?>
        </div>
    </div>
    <br>

    <div class="form-group">
        <?= Html::submitButton('<i class="icon wb-search"></i> ' . Yii::t('app','Искать'), ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('<i class="icon wb-refresh"></i> ' . Yii::t('app','Очистить'), [
            'class' => 'btn btn-default',
            'onclick' => 'console.log($(\'form\'));console.log($(\'[id ^= "cars-name"]\').val(\'\'));$(\'#my-search-form\').submit();'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<div class="row">
    <div class="col-md-12">
        <?php ActiveForm::begin(['action' => ['save-blanket'], 'id' => 'save-bindings-form']); ?>
        <?php foreach($idsYes as $idYes): ?>
            <input type="hidden" name="idsYes[]" value="<?= $idYes ?>">
        <?php endforeach; ?>

        <label class="control-label" for="record-status">Номер одеяла</label>
        <select id="record-status" class="form-control" name="Blanket">
            <option value="">Выберите номер одеяла ...</option>
            <option value="№ 2">Одеяло № 2</option>
            <option value="№ 4">Одеяло № 4</option>
            <option value="№ 6">Одеяло № 6</option>
            <option value="№ 8">Одеяло № 8</option>
            <option value="№ 10">Одеяло № 10</option>
            <option value="№ 12">Одеяло № 12</option>
            <option value="№ 14">Одеяло № 14</option>
            <option value="№ 16">Одеяло № 16</option>
            <option value="№ 18">Одеяло № 18</option>
            <option value="№ 20">Одеяло № 20</option>
            <option value="№ 22">Одеяло № 22</option>
            <option value="№ 24">Одеяло № 24</option>
            <option value="№ 28">Одеяло № 28</option>
            <option value="№ 30">Одеяло № 30</option>
            <option value="№ 40">Одеяло № 40</option>
            <option value="№ 42">Одеяло № 42</option>
            <option value="№ 48">Одеяло № 48</option>
            <option value="№ 50">Одеяло № 50</option>
            <option value="№ 52">Одеяло № 52</option>
            <option value="№ 60">Одеяло № 60</option>
            <option value="№ 62">Одеяло № 62</option>
            <option value="№ 72">Одеяло № 72</option>
            <option value="№ 74">Одеяло № 74</option>
            <option value="№ 76">Одеяло № 76</option>
            <option value="№ 202">Одеяло № 202</option>
        </select>
        <?php ActiveForm::end(); ?>

        <br>
        <?= Html::button ('Указать для всех позиций из правого столбца номер одеяла', ['class' => 'btn btn-success', 'id' => 'save-bindings-button'] ) ?>
        <hr>

    </div>
</div>

<div class="row">
    <?php $form = ActiveForm::begin(['id' => 'bind-form', 'options' => ['data-pjax'=>'']]); ?>
    <input id="hidden-only-add" type="hidden" name="onlyAdd" value="">
    <input id="hidden-only-remove" type="hidden" name="onlyRemove" value="">
    <?php foreach($idsYes as $idYes): ?>
        <input type="hidden" name="idsYes[]" value="<?= $idYes ?>">
    <?php endforeach; ?>
    <div class="col-md-6">
<?= Html::button ('Добавить выбранные', ['class' => 'btn btn-success pull-right', 'id' => 'add-bindings-button'] ) ?>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProviderNo,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'name',
            'label' => $model->getAttributeLabel('name'),
            'format' => 'raw',
            'value' => function($data) {
                $html = '';
                $html .= Html::tag('div',
                    Html::checkbox('idsToAdd[]', $checked = false, ['value' => $data->id, 'class' => 'carId' . $data->id]) .
                    Html::tag('label',
                        $data->name,
                        [
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'data-html' => 'true',
                            'title' => $data->name,
                        ]
                    ),
                    ['class' => 'checkbox-custom checkbox-primary text-left']
                );

                return $html;
            }
        ],
        'blanket_default'
    ]
]); ?>

</div>
    <div class="col-md-6">
        <?= Html::button ('Убрать выбранные', ['class' => 'btn btn-danger pull-right', 'id' => 'remove-bindings-button'] ) ?>
        <?= Html::button ('Убрать все', ['class' => 'btn btn-warning', 'id' => 'remove-all-bindings-button'] ) ?>
        <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $dataProviderYes,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'name',
                    'label' => $model->getAttributeLabel('name'),
                    'format' => 'raw',
                    'value' => function($data) {
                        $html = '';
                        $html .= Html::tag('div',
                            Html::checkbox('idsToRemove[]', $checked = false, ['value' => $data->id, 'class' => 'carId' .$data->id]) .
                            Html::tag('label',
                                $data->name,
                                [
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-html' => 'true',
                                    'title' => $data->name,
                                ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        );

                        return $html;
                    }
                ],
                'blanket_default'
            ]
        ]); ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>


<?php Yii::$app->view->registerJs('
    $("#add-bindings-button").click(function() {
        addBindings();
    });
    $("#remove-bindings-button").click(function() {
        removeBindings();
    });
    $("#remove-all-bindings-button").click(function() {
        removeAllBindings();
    });
    $("#save-bindings-button").click(function() {
        saveBindings();
    });
    function addBindings() {
        $("#hidden-only-add").val(1);
        $("#hidden-only-remove").val("");
        $("#bind-form").submit();
    }
    function removeBindings() {
        $("#hidden-only-add").val("");
        $("#hidden-only-remove").val(1);
        $("#bind-form").submit();
    }
    function removeAllBindings() {
        $(\'[name ^= "idsToRemove"]\').each(
            function() {
                $(this).attr(\'checked\',\'checked\');
            }
        );
        $("#bind-form").submit();
    }
    function saveBindings() {
        $("#save-bindings-form").submit();
    }
', \yii\web\View::POS_END);
?>