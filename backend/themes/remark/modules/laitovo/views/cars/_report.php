<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\widgets\ActiveForm;
use backend\modules\laitovo\models\CarsForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Автомобили');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->params['searchmodelaction'] = 'report';
$this->render('../menu');

$customCss = <<< CSS
    .checkbox-primary {font-size: 1em;}
CSS;
$this->registerCss($customCss);

$customScript = <<< SCRIPT
$(function() {
    $( ":checkbox" ).on('click',function(){
        if ($(this).prop( "checked" ) == false) {
            $(this).val('0');
            $(this).removeAttr( "checked" );
        }else{
            $(this).val('1');
        }
    })
});
SCRIPT;
$this->registerJs($customScript, \yii\web\View::POS_READY);

$fields = [];
foreach ($model->auto->fields as $key => $value) {
    $fields[] = [
        'attribute' => $value['fields']['id']['value'],
        'label' => $value['fields']['label']['value'],
        'format' => 'html',
        'value' => function ($data) use ($value) {
            return $data->json($value['fields']['id']['value']) ? '<span class="glyphicon glyphicon-check"><span>' : '<span class="glyphicon glyphicon-unchecked"><span>';
        },
    ];
}
?>
<div class="erp-location-workplace-search">

    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'options' => ['data-pjax' => true],
    ]); ?>

    <?= $form->field($searchmodel, 'filter')->textInput(['onchange' => '$("#search").click();']) ?>
    <?= $form->field($searchmodel, 'reload')->checkbox(['onchange' => '$("#search").click();']) ?>
    <?= $form->field($searchmodel, 'notclipsed')->checkbox(['onchange' => '$("#search").click();']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary hidden','id' => 'search']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'rowOptions'=>function ($data, $key, $index, $grid){
        if ($data->json('reload'))
            return ['class' => 'success'];
    },
    'columns' => ArrayHelper::merge([
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'naryads',
            'label' => 'Заказано',
            'format' => 'text',
            'value' => function ($data) {
                return $data->naryads ? $data->naryads : 0;
            },
        ],
        'article',
        [
            'attribute' => 'name',
            'label' => $model->getAttributeLabel('name'),
            'format' => 'html',
            'headerOptions' => ['width' => '200'],
            'value' => function ($data) {
                return $data->name ? Html::a($data->name, ['view', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute' => 'Список',
            'label' => 'Список',
            'format' => 'raw',
            'value' => function ($data) {
                $model=new CarsForm($data->id);
                $result = '';

                $result .= Html::beginForm(['cars/report?'.Yii::$app->request->queryString], 'post', ['class' => '']);
                $result .= Html::hiddenInput('id', $data->id);

                $result .=  '<div class="clearfix text-right">';
                $result .=  '<div class="col-md-2">';

                $result .= Html::tag('div',
                            Html::checkbox('fw', $data->json('fw'), ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('ПШ')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);

                $result .=  '</div>';
                $result .=  '<div class="col-md-2">';

                $result .= Html::tag('div',
                            Html::checkbox('fv', $data->json('fv'), ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('ПФ')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);

                $result .=  '</div>';
                $result .=  '<div class="col-md-2">';

                $result .= Html::tag('div',
                            Html::checkbox('fd', $data->json('fd'), ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('ПБ')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_laitovo_standart_status', $model->fd_laitovo_standart_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('L&nbsp;Ст'),['style' => (($model->fd_laitovo_standart_status && !$model->fd_laitovo_standart_scheme)? 'color:red' : '') ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_laitovo_short_status', $model->fd_laitovo_short_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                'L&nbsp;Ук',['style' => (($model->fd_laitovo_short_status && !$model->fd_laitovo_short_scheme)? 'color:red' : '') ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_laitovo_smoke_status', $model->fd_laitovo_smoke_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('L&nbsp;Кур'),['style' => (($model->fd_laitovo_smoke_status && !$model->fd_laitovo_smoke_scheme)? 'color:red' : '') ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_laitovo_mirror_status', $model->fd_laitovo_mirror_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('L&nbsp;Зер'),['style' => (($model->fd_laitovo_mirror_status && !$model->fd_laitovo_mirror_scheme)? 'color:red' : '') ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_laitovo_dontlooks_status', $model->fd_laitovo_dontlooks_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Сдв')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_laitovo_dontlookb_status', $model->fd_laitovo_dontlookb_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Бес')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_chiko_standart_status', $model->fd_chiko_standart_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Ч&nbsp;Ст'),['style' => (($model->fd_chiko_standart_status && !$model->fd_chiko_standart_scheme)? 'color:red' : '') ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_chiko_short_status', $model->fd_chiko_short_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Ч&nbsp;Ук'),['style' => (($model->fd_chiko_short_status && !$model->fd_chiko_short_scheme)? 'color:red' : '') ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_chiko_smoke_status', $model->fd_chiko_smoke_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Ч&nbsp;Кур'),['style' => (($model->fd_chiko_smoke_status && !$model->fd_chiko_smoke_scheme)? 'color:red' : '') ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_chiko_mirror_status', $model->fd_chiko_mirror_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Ч&nbsp;Зер'),['style' => (($model->fd_chiko_mirror_status && !$model->fd_chiko_mirror_scheme)? 'color:red' : '') ]
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_chikomagnet_status', $model->fd_chikomagnet_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Mag')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('fd_laitovowithmagnets_standart_status', $model->fd_laitovowithmagnets_standart_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('На&nbsp;маг'),['class' => 'ordern']
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);

                $result .=  '</div>';
                $result .=  '<div class="col-md-2">';

                $result .= Html::tag('div',
                            Html::checkbox('rd', $data->json('rd'), ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('ЗБ')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('rd_laitovo_standart_status', $model->rd_laitovo_standart_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Ст')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('rd_laitovo_dontlooks_status', $model->rd_laitovo_dontlooks_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Сдв')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('rd_laitovo_dontlookb_status', $model->rd_laitovo_dontlookb_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Бес')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('rd_chikomagnet_status', $model->rd_chikomagnet_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Mag')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('rd_laitovowithmagnets_standart_status', $model->rd_laitovowithmagnets_standart_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('На&nbsp;маг')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);

                $result .=  '</div>';
                $result .=  '<div class="col-md-2">';

                $result .= Html::tag('div',
                            Html::checkbox('rv', $data->json('rv'), ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('ЗФ')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('rv_laitovo_standart_status', $model->rv_laitovo_standart_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Ст')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('rv_laitovo_dontlookb_status', $model->rv_laitovo_dontlookb_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Бес')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('rv_laitovo_dontlooks_status', $model->rv_laitovo_dontlooks_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Сдв')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);

                $result .=  '</div>';
                $result .=  '<div class="col-md-2">';

                $result .= Html::tag('div',
                            Html::checkbox('bw', $data->json('bw'), ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('ЗШ')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('bw_laitovo_standart_status', $model->bw_laitovo_standart_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Ст')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('bw_laitovo_dontlooks_status', $model->bw_laitovo_dontlooks_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Сдв')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);
                $result .= Html::tag('div',
                            Html::checkbox('bw_chiko_dontlookb_status', $model->bw_chiko_dontlookb_status, ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Бес')
                            ),
                            ['class' => 'checkbox-custom checkbox-primary text-left']);

                $result .=  '</div>';
                $result .=  '</div>';

                $result .= Html::tag('div',
                            Html::checkbox('reload', $data->json('reload'), ['class' => 'ordern' . $data->id]) .
                            Html::tag('label',
                                ('Проверена'),['style' => 'color:green;font-size:1.2em']
                            ),
                            ['class' => 'checkbox-custom checkbox-success text-left ']);
                $result .= Html::submitButton('Сохранить в один клик !', ['class' => 'btn btn-primary deleteconfirm', 'name' => 'hash-button']);
                $result .= Html::endForm();



                return $result;
            }
        ],

    ], [

    ]),
]); ?>
