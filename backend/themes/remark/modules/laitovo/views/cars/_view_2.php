<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\helpers\Json;

$scheme = Json::decode($model->fw_scheme);
?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'fw_date:date',
        [
            'attribute' => 'fw_status',
            'format' => 'html',
            'value' => Html::tag('i', '', ['class' => $model->fw_status ? 'wb-check text-success' : 'wb-close text-danger']),
        ],
    ],
]) ?>

<h4>Схема замеров</h4>

<p><a href="#inputCheckedstepsfwscheme" class="link-dashed" data-toggle="modal"
      data-target="#edit-car-modal"><?= Yii::t('app', 'Внести изменения') ?></a></p>

<div class="row">
    <div class="col-md-10">
        <img src="/img/act/image00.jpg" width="100%">

        <span style="position: absolute;left: 30%;top: 86%;"><?= $scheme[0] ?></span>
        <span style="position: absolute;left: 29%;top: 62%;"><?= $scheme[1] ?></span>
        <span style="position: absolute;left: 23%;top: 3%;"><?= $scheme[2] ?></span>
        <span style="position: absolute;left: 45%;top: 11%;"><?= $scheme[3] ?></span>
        <span style="position: absolute;left: 45%;top: 1%;"><?= $scheme[4] ?></span>
        <span style="position: absolute;left: 52%;top: 7%;"><?= $scheme[5] ?></span>
        <span style="position: absolute;left: 49%;top: 58%;"><?= $scheme[6] ?></span>
        <span style="position: absolute;left: 73%;top: 53%;"><?= $scheme[7] ?></span>
        <span style="position: absolute;left: 53%;top: 64%;"><?= $scheme[8] ?></span>
        <span style="position: absolute;left: 83%;top: 3%;"><?= @$scheme[9] ?></span>


    </div>
</div>
