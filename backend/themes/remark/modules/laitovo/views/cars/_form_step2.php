<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */
?>
<h3>Продукция: ПШ</h3>
<?= $form->field($model, 'fw_date')->textInput(['maxlength' => true, 'class' => 'date_picker form-control']) ?>
<?= $form->field($model, 'fw_status')->checkbox() ?>
