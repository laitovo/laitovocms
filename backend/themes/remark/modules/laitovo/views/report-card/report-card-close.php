<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Tabs;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->render('../menu');

//Perpare models
$salary_one_1 = 0;
$salary_two_1 = 0;
$sum_hour_in_support_1 = 0;
$sum_day_in_support_1 = 0;
$sum_price_in_support_1_1 = 0;
$sum_price_in_support_2_1 = 0;
$fine_1 = 0;
$premium_1 = 0;
$total_sum_1 = 0;

foreach ($dataProviderSupport->allModels as $somemodel) {
    $salary_one_1 += $somemodel['salary_one'];
    $salary_two_1 += $somemodel['salary_two'];
    $sum_hour_in_support_1 += $somemodel['sum_hour_in_support'];
    $sum_day_in_support_1 += $somemodel['sum_day_in_support'];
    $sum_price_in_support_1_1 += $somemodel['sum_price_in_support_1'];
    $sum_price_in_support_2_1 += $somemodel['sum_price_in_support_2'];
    $fine_1 += $somemodel['fine'];
    $premium_1 += $somemodel['premium'];
    $total_sum_1 += $somemodel['total_sum'];
}

$sum_price_in_production_2 = 0;
$salary_one_2 = 0;
$salary_two_2 = 0;
$sum_hour_in_support_2 = 0;
$sum_day_in_support_2 = 0;
$sum_price_in_1_2 = 0;
$sum_price_in_2_2 = 0;
$total_sum_2 = 0;

foreach ($dataProviderProduction->allModels as $somemodel) {
    $sum_price_in_production_2 += $somemodel['sum_price_in_production'];
    $salary_one_2 += $somemodel['salary_one'];
    $salary_two_2 += $somemodel['salary_two'];
    $sum_hour_in_support_2 += $somemodel['sum_hour_in_support'];
    $sum_day_in_support_2 += $somemodel['sum_day_in_support'];
    $sum_price_in_1_2 += $somemodel['sum_price_in_1'];
    $sum_price_in_2_2 += $somemodel['sum_price_in_2'];
    $total_sum_2 += $somemodel['total_sum'];
}

$sum_price_in_production_3 = 0;
$salary_one_3 = 0;
$salary_two_3 = 0;
$sum_hour_in_support_3 = 0;
$sum_price_in_support_1_3 = 0;
$fine_2_3 = 0;
$premium_2 = 0;

foreach ($dataProviderOther->allModels as $somemodel) {
    $sum_price_in_production_3 += $somemodel['sum_price_in_production'];
    $salary_one_3 += $somemodel['salary_one'];
    $salary_two_3 += $somemodel['salary_two'];
    $sum_hour_in_support_3 += $somemodel['sum_hour_in_support'];
    $sum_price_in_support_1_3 += $somemodel['sum_price_in_support_1'];
    $fine_2_3 += $somemodel['fine_2'];
    $premium_2 += $somemodel['premium'];
}

?>
<pre>
    <?= print_r($model->getFine(10))?>
</pre>
<?php $form = ActiveForm::begin(['method' =>'get']); ?>

    <div class="col-xs-4">

        <?= $form->field($model, 'date_from')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>
    </div>
    <div class="col-xs-4">
        <?= $form->field($model, 'date_to')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>
    </div>

    <div class="col-xs-4 ">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => '  btn  btn-outline btn-round btn-primary']) ?>
    </div>

    <div class="clearfix"></div>

<?php ActiveForm::end(); ?>

<? $support = GridView::widget([
    'dataProvider' => $dataProviderSupport,
    'tableOptions' => ['class' => 'table table-hover'],
    'showFooter' => true,
    'panel'=>[
        'heading'=>'',
    ],
    'toolbar'=> [
        '{export}',
    ],
    'columns' => [
        [
            'attribute' => 'username',
            'label' => 'Имя',
            'value' => function ($data) {
                return isset($data['username']) ? $data['username'] : '';
            },
        ],
        [
            'attribute' => 'salary_one',
            'label' => 'Оклад-1',
            'value' => function ($data) {
//                return isset($data['salary_one']) ? $data['salary_one'] : '';
                return number_format(isset($data['salary_one']) ? $data['salary_one'] : 0,2,',',' ');
            },
//            'footer' => $salary_one_1,
            'footer' => number_format($salary_one_1,2,',',' '),
        ],
        [
            'attribute' => 'salary_two',
            'label' => 'Оклад-2',
            'value' => function ($data) {
//                return isset($data['salary_two']) ? $data['salary_two'] : '';
                return number_format(isset($data['salary_two']) ? $data['salary_two'] : 0,2,',',' ');
            },
//            'footer' => $salary_two_1,
            'footer' => number_format($salary_two_1,2,',',' '),
        ],
        [
            'attribute' => 'sum_hour_in_support',
            'label' => 'Часы по окладу',
            'value' => function ($data) {
//                return isset($data['sum_hour_in_support']) ? $data['sum_hour_in_support'] : '';
                return number_format(isset($data['sum_hour_in_support']) ? $data['sum_hour_in_support'] : 0,2,',',' ');
            },
//            'footer' => $sum_hour_in_support_1,
            'footer' => number_format($sum_hour_in_support_1,2,',',' '),
        ],
        [
            'attribute' => 'sum_day_in_support',
            'label' => 'Дни по окладу',
            'value' => function ($data) {
//                return isset($data['sum_day_in_support']) ? $data['sum_day_in_support'] : '';
                return number_format(isset($data['sum_day_in_support']) ? $data['sum_day_in_support'] : 0,2,',',' ');
            },
//            'footer' => $sum_day_in_support_1,
            'footer' => number_format($sum_day_in_support_1,2,',',' '),
        ],
        [
            'attribute' => 'sum_price_in_support_1',
            'label' => 'Сумма по окладу-1',
            'value' => function ($data) {
//                return isset($data['sum_price_in_support_1']) ? $data['sum_price_in_support_1'] : '';
                return number_format(isset($data['sum_price_in_support_1']) ? $data['sum_price_in_support_1'] : 0,2,',',' ');
            },
//            'footer' => $sum_price_in_support_1_1,
            'footer' => number_format($sum_price_in_support_1_1,2,',',' '),
        ],
        [
            'attribute' => 'sum_price_in_support_2',
            'label' => 'Сумма по окладу-2',
            'value' => function ($data) {
//                return isset($data['sum_price_in_support_2']) ? $data['sum_price_in_support_2'] : '';
                return number_format(isset($data['sum_price_in_support_2']) ? $data['sum_price_in_support_2'] : 0,2,',',' ');
            },
//            'footer' => $sum_price_in_support_2_1,
            'footer' => number_format($sum_price_in_support_2_1,2,',',' '),
        ],
        [
            'attribute' => 'fine',
            'label' => 'Штрафы',
            'value' => function ($data) {
//                return isset($data['fine']) ? $data['fine'] : 0;
                return number_format(isset($data['fine']) ? $data['fine'] : 0,2,',',' ');
            },
//            'footer' => $fine_1,
            'footer' => number_format($fine_1,2,',',' '),
        ],
        [
            'attribute' => 'premium',
            'label' => 'Премии',
            'value' => function ($data) {
//                return isset($data['premium']) ? $data['premium'] : 0;
                return number_format(isset($data['premium']) ? $data['premium'] : 0,2,',',' ');
            },
//            'footer' => $premium_1,
            'footer' => number_format($premium_1,2,',',' '),
        ],
        [
            'attribute' => 'total_sum',
            'label' => 'Сумма к выдаче',
            'format' => 'html',
            'value' => function ($data) {
//                return isset($data['total_sum']) ? $data['total_sum'] : '';
                return number_format(isset($data['total_sum']) ? $data['total_sum'] : 0,2,',',' ');
            },
//            'footer' => $total_sum_1,
            'footer' => number_format($total_sum_1,2,',',' '),
        ],
    ]
])?>


<? $production = GridView::widget([
    'dataProvider' => $dataProviderProduction,
    'tableOptions' => ['class' => 'table table-hover'],
    'showFooter' => true,
    'panel'=>[
        'heading'=>'',
    ],
    'toolbar'=> [
        '{export}',
    ],
    'columns' => [
        [
            'attribute' => 'username',
            'label' => 'Имя',
            'value' => function ($data) {
                return isset($data['username']) ? $data['username'] : '';
            },
        ],
        [
            'attribute' => 'sum_price_in_production',
            'label' => 'Сумма по нарядам',
            'value' => function ($data) {
//                return isset($data['sum_price_in_production']) ? $data['sum_price_in_production'] : 0;
                return number_format(isset($data['sum_price_in_production']) ? $data['sum_price_in_production'] : 0,2,',',' ');
            },
//            'footer' => $sum_price_in_production_2,
            'footer' => number_format($sum_price_in_production_2,2,',',' '),
        ],
        [
            'attribute' => 'salary_one',
            'label' => 'Оклад-1',
            'value' => function ($data) {
//                return isset($data['salary_one']) ? $data['salary_one'] : 0;
                return number_format(isset($data['salary_one']) ? $data['salary_one'] : 0,2,',',' ');
            },
//            'footer' => $salary_one_2,
            'footer' => number_format($salary_one_2,2,',',' '),
        ],
        [
            'attribute' => 'salary_two',
            'label' => 'Оклад-2',
            'value' => function ($data) {
//                return isset($data['salary_two']) ? $data['salary_two'] : 0;
                return number_format(isset($data['salary_two']) ? $data['salary_two'] : 0,2,',',' ');
            },
//            'footer' => $salary_two_2,
            'footer' => number_format($salary_two_2,2,',',' '),
        ],
        [
            'attribute' => 'sum_hour_in_support',
            'label' => 'Часы по окладу',
            'value' => function ($data) {
//                return isset($data['sum_hour_in_support']) ? $data['sum_hour_in_support'] : 0;
                return number_format(isset($data['sum_hour_in_support']) ? $data['sum_hour_in_support'] : 0,2,',',' ');
            },
//            'footer' => $sum_hour_in_support_2,
            'footer' => number_format($sum_hour_in_support_2,2,',',' '),
        ],
        [
            'attribute' => 'sum_day_in_support',
            'label' => 'Дни по окладу',
            'value' => function ($data) {
//                return isset($data['sum_day_in_support']) ? $data['sum_day_in_support'] : 0;
                return number_format(isset($data['sum_day_in_support']) ? $data['sum_day_in_support'] : 0,2,',',' ');
            },
//            'footer' => $sum_day_in_support_2,
            'footer' => number_format($sum_day_in_support_2,2,',',' '),
        ],
        [
            'attribute' => 'sum_price_in_1',
            'label' => 'Сумма по окладу-1',
            'value' => function ($data) {
//                return isset($data['sum_price_in_1']) ? $data['sum_price_in_1'] : 0;
                return number_format(isset($data['sum_price_in_1']) ? $data['sum_price_in_1'] : 0,2,',',' ');
            },
//            'footer' => $sum_price_in_1_2,
            'footer' => number_format($sum_price_in_1_2,2,',',' '),
        ],
        [
            'attribute' => 'sum_price_in_2',
            'label' => 'Остаток сделки',
            'value' => function ($data) {
//                return isset($data['sum_price_in_2']) ? $data['sum_price_in_2'] : 0;
                return number_format(isset($data['sum_price_in_2']) ? $data['sum_price_in_2'] : 0,2,',',' ');
            },
//            'footer' => $sum_price_in_2_2,
            'footer' => number_format($sum_price_in_2_2,2,',',' '),
        ],
        [
            'attribute' => 'total_sum',
            'label' => 'Сумма к выдаче',
            'value' => function ($data) {
//                return isset($data['total_sum']) ? $data['total_sum'] : 0;
                return number_format(isset($data['total_sum']) ? $data['total_sum'] : 0,2,',',' ');
            },
//            'footer' => $total_sum_2,
            'footer' => number_format($total_sum_2,2,',',' '),
        ],
    ]
])?>

<? $other = GridView::widget([
    'dataProvider' => $dataProviderOther,
    'tableOptions' => ['class' => 'table table-hover'],
    'showFooter' => true,
    'panel'=>[
        'heading'=>'',
    ],
    'toolbar'=> [
        '{export}',
    ],
    'columns' => [
        [
            'attribute' => 'username',
            'label' => 'Имя',
            'value' => function ($data) {
                return isset($data['username']) ? $data['username'] : '';
            },
        ],
        [
            'attribute' => 'sum_price_in_production',
            'label' => 'Сумма по нарядам',
            'value' => function ($data) {
//                return isset($data['sum_price_in_production']) ? $data['sum_price_in_production'] : '';
                return number_format(isset($data['sum_price_in_production']) ? $data['sum_price_in_production'] : 0,2,',',' ');
            },
//            'footer' => $sum_price_in_production_3,
            'footer' => number_format($sum_price_in_production_3,2,',',' '),
        ],
        [
            'attribute' => 'salary_one',
            'label' => 'Оклад-1',
            'value' => function ($data) {
//                return isset($data['sum_price_in_production']) ? $data['sum_price_in_production'] : '';
                return number_format(isset($data['sum_price_in_production']) ? $data['sum_price_in_production'] : 0,2,',',' ');
            },
//            'footer' => $sum_price_in_production_3,
            'footer' => number_format($sum_price_in_production_3,2,',',' '),
        ],
        [
            'attribute' => 'salary_two',
            'label' => 'Оклад-2',
            'value' => function ($data) {
//                return isset($data['salary_two']) ? $data['salary_two'] : '';
                return number_format(isset($data['salary_two']) ? $data['salary_two'] : 0,2,',',' ');
            },
//            'footer' => $salary_two_3,
            'footer' => number_format($salary_two_3,2,',',' '),
        ],
        [
            'attribute' => 'sum_hour_in_support',
            'label' => 'Часы по окладу',
            'value' => function ($data) {
//                return isset($data['sum_hour_in_support']) ? $data['sum_hour_in_support'] : '';
                return number_format(isset($data['sum_hour_in_support']) ? $data['sum_hour_in_support'] : 0,2,',',' ');
            },
//            'footer' => $sum_hour_in_support_3,
            'footer' => number_format($sum_hour_in_support_3,2,',',' '),
        ],
        [
            'attribute' => 'sum_price_in_support_1',
            'label' => 'Сумма по окладу-1',
            'value' => function ($data) {
//                return isset($data['sum_price_in_support_1']) ? $data['sum_price_in_support_1'] : '';
                return number_format(isset($data['sum_price_in_support_1']) ? $data['sum_price_in_support_1'] : 0,2,',',' ');
            },
//            'footer' => $sum_price_in_support_1_3,
            'footer' => number_format($sum_price_in_support_1_3,2,',',' '),
        ],
        [
            'attribute' => 'sum_price_in_2',
            'format' => 'html',
            'label' => 'Сумма по окладу-2',
            'value' => function ($data) {
                return isset($data['username']) ? '<i class ="glyphicon glyphicon-remove" style="color: red"></i>' : '';
            },
        ],
        [
            'attribute' => 'fine_2',
            'format' => 'html',
            'label' => 'Штрафы',
            'value' => function ($data) {
//                return isset($data['fine_2']) ? $data['fine_2'] : 0;
                return number_format(isset($data['fine_2']) ? $data['fine_2'] : 0,2,',',' ');
            },
//            'footer' => $fine_2_3,
            'footer' => number_format($fine_2_3,2,',',' '),
        ],
        [
            'attribute' => 'premium',
            'format' => 'html',
            'label' => 'Премии',
            'value' => function ($data) {
//                return isset($data['premium']) ? $data['premium'] : 0;
                return number_format(isset($data['premium']) ? $data['premium'] : 0,2,',',' ');
            },
//            'footer' => $premium_2,
            'footer' => number_format($premium_2,2,',',' '),
        ],
    ]
])?>

<?php
    $items[0]['label'] = 'Окладники';
    $items[0]['content'] = $support;

    $items[1]['label'] = 'Сдельщики';
    $items[1]['content'] = $production;

    $items[2]['label'] = 'Окладники со сделкой';
    $items[2]['content'] = $other;
?>

<?= Tabs::widget([
    'items' => $items,
    'encodeLabels' => false,
]); ?>