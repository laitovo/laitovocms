<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->render('../menu');
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 13.09.17
 * Time: 16:20
 */
?>
<?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-4">

        <?= $form->field($model, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>
    </div>
    <div class="col-xs-4">
        <?= $form->field($model, 'dateTo')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'options' => [
                'class' => 'form-control',
            ],
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>
    </div>

    <div class="col-xs-4 ">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => '  btn  btn-outline btn-round btn-primary']) ?>
    </div>

    <div class="clearfix"></div>

<?php ActiveForm::end(); ?>

<h1>Сводный отчет заработной платы</h1>

<?= GridView::widget([

    'dataProvider' => $model->getArrayDataProvider(),
    'beforeHeader'=>[
        [
            'columns'=>[
                [
                    'content' => '',
                    'tag'=>'td'
                ],
                [
                    'content' => 'Оклад',
                    'tag'=>'td',
                    'options'=>[
                        'colspan'=>3,
                        'style'=>[
                            'border-left'=>'2px solid',
                            'border-right'=>'2px solid',
                            'border-top'=>'2px solid',
                        ]
                    ]
                ],
                [
                    'content' => 'Сделка',
                    'tag'=>'td',
                    'options'=>[
                        'colspan'=>3,
                        'style'=>[
                            'border-right'=>'2px solid',
                            'border-top'=>'2px solid',
                        ]
                    ]
                ],
                [
                    'content' => 'Итого',
                    'tag'=>'td',
                    'options'=>[
                        'colspan'=>3,
                        'style'=>[
                            'border-right'=>'2px solid',
                            'border-top'=>'2px solid',
                        ]
                    ]
                ],
            ],
            'options'=>[
                'style'=>[
                    'text-align'=>'center',
                    'font-size'=>'20px'
                ]
            ]

        ]
    ] ,
    'panel'=>['heading'=>false],
    'toolbar' => [
        '{export}',
    ],
    'layout'=>"{items}",
    'columns' =>
        [

            [
                'attribute'=>'username',
                'label'=>'Сотрудники',
            ],
            [
                'attribute'=>'sum_hour_in_support',
                'label'=>'Часы',
                'contentOptions'=>[
                    'style'=>[
                        'border-left'=>'2px solid',
                    ]
                ],
                'headerOptions' => [
                    'style'=>[
                        'border-left'=>'2px solid',
                    ]
                ]
            ],
            [
                'attribute'=>'sum_day_in_support',
                'label'=>'Дни',
            ],
            [
                'attribute'=>'sum_price_in_support',
                'label'=>'Сумма',
            ],
            [
                'attribute'=>'sum_hour_in_production',
                'label'=>'Часы',
                'contentOptions'=>[
                    'style'=>[
                        'border-left'=>'2px solid',
                    ]
                ],
                'headerOptions' => [
                    'style'=>[
                        'border-left'=>'2px solid',
                    ]
                ]
            ],
            [
                'attribute'=>'sum_day_in_production',
                'label'=>'Дни',
            ],
            [
                'attribute'=>'sum_price_in_production',
                'label'=>'Сумма',
            ],
            [
                'attribute'=>'total_hour',
                'label'=>'Часы',
                'contentOptions' =>[
                    'style'=>[
                        'border-left'=>'2px solid',
                    ]
                ],
                'headerOptions' => [
                    'style'=>[
                        'border-left'=>'2px solid',
                    ]
                ]
            ],
            [
                'attribute'=>'total_day',
                'label'=>'Дни',
            ],
            [
                'attribute'=>'total_sum',
                'label'=>'Сумма',
                'contentOptions' =>[
                    'style'=>[
                        'border-right'=>'2px solid',
                    ]
                ],
                'headerOptions' => [
                    'style'=>[
                        'border-right'=>'2px solid',
                    ]
                ]
            ],

        ]
])?>

