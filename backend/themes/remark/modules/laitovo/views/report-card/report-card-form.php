<?
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\DateAction;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\laitovo\Weekend;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->render('../menu');

?>
<h1><span style="color: #0b58a2" ><?= @$model->userName?></span></h1><br>
<h1>Форма табеля за <?=@$model->dateName?> <br> </h1>
<p>
    <?= Html::a('Прировнять к плану',['perfect-card-form','date'=>$model->date,'user_id'=>$model->user],['class'=>'btn btn-danger'])?>

    <?= Html::a('Прировнять к плану с учетом документов',['perfect-card-document-form','date'=>$model->date,'user_id'=>$model->user],['class'=>' btn btn-warning'])?>

    <?= Html::a('Сбросить все изменения',['report-card-form','date'=>$model->date,'user_id'=>$model->user],['class'=>' btn btn-primary'])?>
</p>

<?php $form = ActiveForm::begin(); ?>

<?
$fields=[];
$weekends = Weekend::getWeekendList();
foreach ($model->getDateArray() as $day) {
    $fields[] = [
        'attribute' => $day,
        'format'=>'raw',
        'label' => DateAction::littleLabel($day) ,
        'value'=>function($data, $key)use ($model,$day){
            return Html::input('string','fields['.$day.']['.$key.']', $data[$day],['style'=>['width'=>'35px']]);
        },
        'contentOptions' =>function($data, $key, $index, $column) use($weekends, $day){
        return key_exists($day, $weekends) ? ['style'=>['color'=>'red']] : [];
    }
    ];
}
?>


<?=
    GridView::widget([
        'tableOptions' => ['class' => 'table table-hover',
            'style'=>[
                'font-size'=>'11px',
                'font-weight'=>800
            ]
        ],

        'dataProvider' => $dataProvider,
        'layout'=>"{items}",
        'columns' =>ArrayHelper::merge(
            [
                [
                    'attribute'=>'label',
                    'label'=>'',
                ],
            ],
            $fields,
            [
                [
                    'attribute'=>'fact_sum',
                    'label'=>'Факт',
                ],
                [
                    'attribute'=>'month_rate',
                    'label'=>'Мес. норма',
                ]
            ]
        )
    ])
?>
<div class="col-xs-4 ">
    <br>
    <?= Html::submitButton(Yii::t('app', 'Закрыть табель'), ['class' => ' deleteconfirm btn  btn-outline btn-round btn-primary',
        'data' => ['confirm' => 'Вы действительно хотите закрыть табель?
      ВНИМАНИЕ!! ПОСЛЕ ЗАКРЫТИЯ ТАБЕЛЯ ДАЛЬНЕЙШЕЕ РЕДАКТИРОВАНИЕ НЕ ВОЗМОЖНО'],]) ?>
</div>

<?php ActiveForm::end(); ?>
<div class="clearfix"></div>

<h1>Документы</h1>
<p>
    <?= Html::a('Добавить документ',['create-document-cause','date'=>$model->date,'user_id'=>$model->user],['class'=>' btn btn-success'])?>

</p>
<div>
    <?=
        GridView::widget([
            'dataProvider' => $document_provider,
            'layout'=>"{items}",
            'columns' =>[
                'type',
                'title',
                'date_start',
                'date_finish',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{delete}',
                    'buttons' => [
                        'delete' => function ($url, $data)use($model){
                            return Html::a('<span class="glyphicon glyphicon-remove" style = "color:red"></span>',
                                [
                                    'delete-document',
                                    'user_id' => $model->user,
                                    'date' => $model->date,
                                    'document_id' => $data->id,
                                ],
                                ['data'=>[
                                    'confirm' => Yii::t('yii', 'Удалить документ?'),
                                ]]
                            );
                        }
                    ],
                ],
            ],
        ])
    ?>

</div>
