<?php
use yii\helpers\Html;
use backend\widgets\GridView;
use yii\grid\ActionColumn;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->render('../../menu');
?>


<?php if(isset($user_list)): ?>
    <?php $userCount=0?>

    <?php foreach ($user_list as $key =>$value ):?>
        <?php $userCount++;?>
        <?= Html::tag('div',$userCount . '. ' . Html::a($value,['document-cause-index','user_id'=>$key]))  ?>
    <?php endforeach;?>
<?php endif; ?>


<?php if(isset($user) && isset($dataProvider)): ?>
    <h1><?= $user->name?> </h1>

    <?= Html::a('Добавить документ',['create-document-cause','user_id'=>$user->id],['class'=>['btn btn-primary']]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'show' => [ 'title', 'type', 'date_start', 'date_finish'],
        'columns' => [
            'title',
            'type',
            'date_start',
            'date_finish',
            [
             'class' => 'yii\grid\ActionColumn',
             'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $data)use($user){
                        return Html::a('<span class="glyphicon glyphicon-remove" style = "color:red"></span>',
                            [
                                'delete-document',
                                'user_id' => $user->id,
                                'document_id' => $data->id,
                            ],
                            ['data'=>[
                                'confirm' => Yii::t('yii', 'Удалить документ?'),
                            ]]
                        );
                    }
                ]
            ],

        ],
    ]); ?>
<?php endif; ?>
