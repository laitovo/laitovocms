<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Weekend;
use common\models\laitovo\ErpDivision;
use kartik\grid\GridView;
use backend\modules\laitovo\models\ErpDocumentCause;


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->render('../menu');
?>

<? if(empty($year)): ?>
    <? $year_list = $model->year ?>
    <? foreach($year_list as $year):?>
        <?= Html::a($year, ['index-archive','year'=>$year])?>
    <? endforeach ?>

<? elseif(!empty($year) && empty($month)): ?>
    <h1>Архив табелей за <?= $year?> год</h1>
    <div class=">clearfix"></div>
    <? $month_list = $model->month ?>
    <? foreach ($month_list as $key=>$value): ?>
        <?= Html::tag('div',Html::a($value,['index-archive','year'=>$year,'month'=>$key])) ?>
    <? endforeach; ?>


<? elseif(isset($year) && isset($month)): ?>

    <h1>Архив табелей за <?= $model->month[$month] ?> - <?= $year?> годa</h1>
    <div class=">clearfix"></div>
    <? foreach ($model->divisions as $division):  ?>
            <?
            $fields=[];
            $weekends = ArrayHelper::map(Weekend::find()->all(), 'weekend', 'weekend');

            foreach ($model->getDateArray($year, $month) as $day) {
                $fields[] = [
                    'attribute' => $day,
                    'label' => $model->littleLabel($day) ,
                    'contentOptions' =>function($data, $key, $index, $column) use($weekends, $day){
                        if ($data[$day]==ErpDocumentCause::SICK_LEAVE){
                            return ['class' => 'warning'];
                        }elseif ($data[$day]==ErpDocumentCause::BISINESS_TRIP){
                            return ['class' => 'success'];
                        }elseif ($data[$day]==ErpDocumentCause::ANNUAL_VACATION){
                            return ['class'=>'info'];
                        }elseif(key_exists($day, $weekends) || $data[$day]==ErpDocumentCause::ADDITIONAL_LEAVE){
                            return  ['class'=>'danger'];
                        }
                    }
                ];
            }
            ?>
            <?

               $divisionName = ErpDivision::findOne($division)->title;

               $gridView = GridView::widget([
                   'tableOptions' => ['class' => 'table table-hover',
                       'style'=>[
                           'font-size'=>'11px',
                           'font-weight'=>800
                       ]
                   ],
                   'panel'=>['heading'=>false],
                   'toolbar' => [
                       '{export}',
                   ],
                   'rowOptions' => function($data, $key, $index,$grid){
                       if ($key=='final_difference') {
                           return ['class' => 'warning'];
                       }
                   },
                   'dataProvider' => $model->getArrayProvider($division,$year,$month),
                   'layout'=>"{items}",
                   'columns' =>ArrayHelper::merge(
                       [
                           [
                               'attribute'=>'user_name',
                               'label'=>'',
                           ],
                       ],
                       $fields,
                       [
                           [
                               'attribute'=>'fact_sum',
                               'label'=>'Факт'
                           ],
                           [
                               'attribute'=>'month_rate',
                               'label'=>'Мес. норма'
                           ]
                       ]
                   )
               ])
            ?>
            <?= '<h1 style="color: #0b58a2" >' .$divisionName .'</h1>'.$gridView?>

    <? endforeach; ?>

<? endif; ?>




