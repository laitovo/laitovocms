<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Weekend;
use backend\modules\laitovo\models\ErpPosition;
use common\models\laitovo\ErpDivision;


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->render('../menu');
?>

<h1><?=$model->title ?></h1>

<?php $form = ActiveForm::begin(['method'=>'get']); ?>

        <div class="col-xs-4">

            <?= $form->field($model, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'options' => [
                    'class' => 'form-control',
                ],
                'dateFormat' => 'dd.MM.yyyy',
            ]) ?>
        </div>
        <div class="col-xs-4 ">
            <br>
            <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => '  btn  btn-outline btn-round btn-primary']) ?>
        </div>

    <div class="clearfix">

    </div>

<?php ActiveForm::end(); ?>
<?
    $fields=[];
    $weekends = ArrayHelper::map(Weekend::find()->all(), 'weekend', 'weekend');

        foreach ($model->getDateArray() as $day) {
            $fields[] = [
                'attribute' => $day,
                'label' => $model->littleLabel($day) ,
                'contentOptions' =>function($data, $key, $index, $column) use($weekends, $day){
                            return key_exists($day, $weekends) ? ['class'=>'danger'] : [];
                        }
            ];
        }
    ?>

    <?
        $resultSupport = '';
        $divisions =ErpDivision::find()->all();
        foreach ($divisions as $division)
        {
            $result_users = '';
            $positions = ErpPosition::find()->where(['division_id'=>$division->id])->all();
            foreach ($positions as $position)
            {
                if($position->user){
                    $result_users .= '<h1 style="font-size: 20px">'.Html::a($position->user->getName(), ['erp-user/view','id'=> $position->user->id],['target' => '_blank', 'style'=>['color'=>'green']]).'</h1>';
                    if($model->getAccessUpdateCardForm($position->user->id)) {

                        $result_users .= Html::a('Закрыть табель', ['report-card-form', 'date' => date('d.m.Y',strtotime($model->dateFrom)), 'user_id' => $position->user->id], ['class' => 'btn btn-success']);
                    }
                    $result_users.= GridView::widget([
                            'tableOptions' => ['class' => 'table table-hover',
                                'style'=>[
                                    'font-size'=>'11px',
                                    'font-weight'=>800
                                ]
                            ],
                            'rowOptions' => function($data, $key, $index,$grid){
                                if ($key=='final_difference') {
                                    return ['class' => 'warning'];
                                }
                            },
                            'dataProvider' => $model->getArrayDataProvider($position->user->id),
                            'layout'=>"{items}",
                            'columns' =>ArrayHelper::merge(
                                [
                                    [
                                        'attribute'=>'label',
                                        'label'=>'',
                                    ],
                                ],
                                $fields,
                                [
                                    [
                                        'attribute'=>'fact_sum',
                                        'label'=>'Факт'
                                    ],
                                    [
                                        'attribute'=>'month_rate',
                                        'label'=>'Мес. норма'
                                    ]
                                ]
                            )
                        ]).'<br>';
                }

            }
            $resultSupport .= '<h1 style="color: blue">'.$division->title.'</h1>'.$result_users;
        }
    ?>

<?= $resultSupport?>





