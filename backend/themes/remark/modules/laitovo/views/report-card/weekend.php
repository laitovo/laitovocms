<?
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->render('../menu');

?>



<?
    $fields = [];
    foreach($model->week_list as $day)
    {
        $fields[] = [
            'attribute' => $day,
            'label' => $model->label($day),
            'format'=>'raw',
            'value' => function($data) use ($day, $model){
                if($data[$day]){
                    return Html::tag('div',
                        Html::checkbox(false, $model->weekendIn($data[$day])?true:false,[
                                'onchange'=>'
                                    $.get("'. Url::to('/ajax/save-weekend') .'", {"weekend":"'.$data[$day].'"},function(data){
                                            if(data){
//                                                alert("Клик");
                                            }     
                                    });
                                 '
                        ])
                        .Html::tag('label', explode('-',$data[$day])[2]),
                        [
                            'class' => 'checkbox-custom checkbox-primary text-left',
                        ]
                    );
                }
                return '';
            }
        ];
    }
?>

<?php $form = ActiveForm::begin(['method'=>'get']); ?>

    <?
        $calendar='';
        foreach ($model->getFirstDayMonth() as $day)
        {
            $calendar .='<h1>'. $model->getMonthYear($day).'</h1>'.'<div style="width: 100px">'.
                GridView::widget([
                    'tableOptions' => ['class' => 'table ',
                        'style'=>[
                            'font-size'=>'15px',
                            'font-weight'=>800,
//                            'color'=>'blue',
                            'background'=>'#BDB76B',
                            'width'=>100
                        ]
                    ],
                    'dataProvider' => $model->getArrayDataProvider($day),
                    'layout'=>"{items}",
                    'columns' =>$fields
                ]).'</div>';
        }
        echo $calendar;
    ?>
<?php ActiveForm::end(); ?>



