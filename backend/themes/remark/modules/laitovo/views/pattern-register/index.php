<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use common\models\laitovo\PatternRegister;
use backend\widgets\GridView;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];

$this->render('../menu');

?>
<h1>Журнал лекал</h1>

<?php $form = ActiveForm::begin(['method' => 'get']); ?>

    <div class="col-xs-3 ">
        <br>
        <?= $form->field($model, 'status')->checkbox([]) ?>
    </div>
    <div class="col-xs-3 ">
        <?= $form->field($model, 'car')->textInput() ?>
    </div>
    <div class="col-xs-4 ">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Сформировать'),
        ['class' => ' pull-left btn  btn-outline btn-round btn-primary']) ?>
    </div>
<div class="clearfix"></div>

<? if ($statistics !== null):?>
<hr>
    Всего лекал освоено: <?= $statistics?>
<hr>
<? endif;?>

<?php ActiveForm::end(); ?>

<?= GridView::widget([
      'dataProvider' => $dataProvider,
      'show' => ['name', 'article', 'product', 'FW', 'FV', 'FD', 'RD', 'RV', 'BW'],
      'columns' => [
          'name',
          'article',
          [
              'attribute' => 'product',
              'label' => 'Продукт',
              'format' => 'html',
              'value' => function($data) {
                  return Html::tag('span', 'DontLook бескаркасный')
                  .'<br>'
                  .Html::tag('span', 'DontLook сдвижной');
              }
          ],
          [
              'attribute' => 'FW',
              'label' => 'ПШ',
              'format' => 'raw',
              'value' => function($data) {
                  $button_bk = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create','car_id' => $data->id, 'type'=>'bk', 'window' =>'FW']);
                  $button_sd = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create', 'car_id' => $data->id, 'type'=>'sd', 'window' =>'FW']);

                  return (PatternRegister::getFwBk($data->id) ? PatternRegister::getFwBk($data->id) : $button_bk)
                      .'<br>'
                      .(PatternRegister::getFwSd($data->id) ? PatternRegister::getFwSd($data->id) : $button_sd);
              }
          ],
          [
              'attribute' => 'FV',
              'label' => 'ПФ',
              'format' => 'raw',
              'value' => function($data) {
                  $button_bk = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create','car_id' => $data->id, 'type'=>'bk', 'window' =>'FV']);
                  $button_sd = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create', 'car_id' => $data->id, 'type'=>'sd', 'window' =>'FV']);
                  return (PatternRegister::getFvBk($data->id) ? PatternRegister::getFvBk($data->id) : $button_bk)
                      .'<br>'
                      .(PatternRegister::getFvSd($data->id) ? PatternRegister::getFvSd($data->id) : $button_sd);
              }
          ],
          [
              'attribute' => 'FD',
              'label' => 'ПБ',
              'format' => 'raw',
              'value' => function($data) {

                  $button_bk = Html ::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create', 'car_id' => $data->id, 'type'=>'bk', 'window' =>'FD']);
                  $button_sd = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create', 'car_id' => $data->id, 'type'=>'sd', 'window' =>'FD']);
                  return (PatternRegister::getFdBk($data->id) ? PatternRegister::getFdBk($data->id) : $button_bk)
                      .'<br>'
                      .(PatternRegister::getFdSd($data->id) ? PatternRegister::getFdSd($data->id) : $button_sd);
              }
          ],
          [
              'attribute' => 'RD',
              'label' => 'ЗБ',
              'format' => 'raw',
              'value' => function($data) {
                  $button_bk = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create','car_id' => $data->id, 'type'=>'bk', 'window' =>'RD']);
                  $button_sd = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create', 'car_id' => $data->id, 'type'=>'sd', 'window' =>'RD']);

                  return (PatternRegister::getRdBk($data->id) ? PatternRegister::getRdBk($data->id) : $button_bk)
                      .'<br>'
                      .(PatternRegister::getRdSd($data->id) ? PatternRegister::getRdSd($data->id) : $button_sd);
              }
          ],
          [
              'attribute' => 'RV',
              'label' => 'ЗФ',
              'format' => 'raw',
              'value' => function($data) {
                  $button_bk = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create','car_id' => $data->id, 'type'=>'bk', 'window' =>'RV']);
                  $button_sd = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create', 'car_id' => $data->id, 'type'=>'sd', 'window' =>'RV']);

                  return (PatternRegister::getRvBk($data->id) ? PatternRegister::getRvBk($data->id) : $button_bk)
                      .'<br>'
                      .(PatternRegister::getRvSd($data->id) ? PatternRegister::getRvSd($data->id) : $button_sd);
              }
          ],
          [
              'attribute' => 'BW',
              'label' => 'ЗШ',
              'format' => 'raw',
              'value' => function($data) {
                  $button_bk = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create','car_id' => $data->id, 'type'=>'bk', 'window' =>'BW']);
                  $button_sd = Html::a('<i class = "glyphicon glyphicon-minus" style="color: red"></i>',['create', 'car_id' => $data->id, 'type'=>'sd', 'window' =>'BW']);

                  return (PatternRegister::getBwBk($data->id) ? PatternRegister::getBwBk($data->id) : $button_bk)
                      .'<br>'
                      .(PatternRegister::getBwSd($data->id) ? PatternRegister::getBwSd($data->id) : $button_sd);
              }
          ],
      ],
]) ?>



