<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\modules\laitovo\models\PatternRegister;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Журнал лекал'), 'url' => ['index']];

$this->render('../menu');
?>

<h3><?= $car->name . ' ' . $model->product_type . ' ' . $model->window_type  ?></h3>
<?php $form = ActiveForm::begin(); ?>

<div class="col-xs-3 ">
    <br>
    <?= $form->field($model, 'litera')->dropDownList(PatternRegister::$litera_items) ?>
</div>
<div class="col-xs-3 ">
    <br>
    <?php foreach ($model->getLiteralCount() as $key => $value) : ?>
        <?= $key . ' : ' . $value?><br>
    <?php endforeach; ?>
</div>

<div>

    <div class="clearfix"></div>
    <div class="col-xs-4 ">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Сохранить'),
        ['class' => ' btn  btn-outline btn-round btn-primary']) ?>
    </div>

</div>


<?php ActiveForm::end(); ?>
