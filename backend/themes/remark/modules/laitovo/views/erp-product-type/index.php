<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\models\ErpProductTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Виды продукта';
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

?>
<div class="erp-product-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить вид продукта', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'show' => ['id','title','article','author_id','created_at'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'title_en',
            'category',
            'rule',
            [
                'attribute'=>'article',
                'format'=>'html',
                'value'=>function($data){
                    return $data->article ? $data->article : ($data->rule ? $data->articleRule : '');
                },
            ],
            [
                'attribute' => 'author_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->author_id ? $data->author->name : null;
                },
            ],
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
