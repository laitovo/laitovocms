<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpProductType */

$this->params['breadcrumbs'][] = ['label' => 'Виды продукта', 'url' => ['index']];

$this->render('../menu');

?>
<div class="erp-product-type-create">

    <h1>Добавить новый вид продукта</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
