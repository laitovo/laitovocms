<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpProductType */

$this->title = 'Изменить вид продукта: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Виды продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';

$this->render('../menu');

?>
<div class="erp-product-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
