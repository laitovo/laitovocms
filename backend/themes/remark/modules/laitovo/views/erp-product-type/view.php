<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\laitovo\ProductionScheme;
/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpProductType */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Виды продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

?>
<div class="erp-product-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'title_en',
            'category',
            [
                'attribute'=>'article',
                'format'=>'html',
                'value'=>function($data){
                     return $data->article ? $data->article : ($data->rule ? $data->articleRule : '');
                },
            ],
            'rule',
//            [
//                'attribute'=>'production_scheme_id',
//                'value'=> function($data) use ($model){
//                    return $model->production_scheme_id ? ProductionScheme::findOne($model->production_scheme_id)->title : '' ;
//                }
//            ],
            
            [
                'attribute'=>'author_id',
                'value'=> function($data) use ($model){
                    return $model->author->name ? $model->author->name : '' ;
                }
            ],
            
            'created_at:datetime',
        ],
    ]) ?>
    
     <p>
       
  
       <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>
       <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id],[
        'class' => 'deleteconfirm pull-right btn btn-outline btn-round btn-danger',
        'style'=>'margin-right:5px',
        'data' => [
            'confirm' => Yii::t('yii', 'Отменить этот акт?'),
            'method' => 'post',
        ]]) ?>
         
            
              
  </p>

</div>
