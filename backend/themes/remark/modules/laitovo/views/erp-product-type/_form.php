<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\laitovo\ProductionScheme;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpProductType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-product-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->dropDownList($model->categories); ?>

    <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rule')->textInput(['maxlength' => true]) ?>
    
   <!-- <?= $form->field($model, 'production_scheme_id')->dropDownList(ArrayHelper::merge(['' => ''],ArrayHelper::map(ProductionScheme::find()->where(['product_id'=>$model->id])->all(), 'id', 'title'))) ?> -->
    
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-outline btn-round btn-primary' : 'btn btn-outline btn-round btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
