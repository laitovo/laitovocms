<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\helpers\Json;

$scheme = Json::decode($model->fw_scheme);
$scheme_b = Json::decode($model->fw_scheme_b);
?>
<? if (isset($model->act->difference['fw_status'])): ?>
<div class="row">
    <div class="col-md-6">
        <h5>ПШ - Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fw_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fw_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fw_status']) ? 'bg-warning' : ''],
                    'value' => 
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fw_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fw_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?> 
    </div>
    <div class="col-md-6">
        <h5>ПШ - Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fw_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fw_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?> 
    </div>
</div>
<?endif;?>

<? if ($model->fw_scheme !== $model->fw_scheme_b): ?>
<h5>ПШ - Схема замеров (после)</h5>
<?=Html::tag('div',
    Html::checkbox("message[]",
        $checked = false, 
        ['value' => 'ПШ - Схема замеров'])
        .Html::tag('label','Переделать'),
    ['class' => 'checkbox-custom checkbox-primary text-left']
)
?>
<div class="row">
    <div class="col-md-12">
        <img src="/img/act/image00.jpg" width="100%">

        <span style="position: absolute;left: 30%;top: 86%;"><?= $scheme[0] ?></span>
        <span style="position: absolute;left: 29%;top: 62%;"><?= $scheme[1] ?></span>
        <span style="position: absolute;left: 23%;top: 3%;"><?= $scheme[2] ?></span>
        <span style="position: absolute;left: 45%;top: 11%;"><?= $scheme[3] ?></span>
        <span style="position: absolute;left: 45%;top: 1%;"><?= $scheme[4] ?></span>
        <span style="position: absolute;left: 52%;top: 7%;"><?= $scheme[5] ?></span>
        <span style="position: absolute;left: 49%;top: 58%;"><?= $scheme[6] ?></span>
        <span style="position: absolute;left: 73%;top: 53%;"><?= $scheme[7] ?></span>
        <span style="position: absolute;left: 53%;top: 64%;"><?= $scheme[8] ?></span>
        <span style="position: absolute;left: 83%;top: 3%;"><?= @$scheme[9] ?></span>


    </div>
</div>

<h5>ПШ - Схема замеров (до)</h5>

<div class="row">
    <div class="col-md-12">
        <img src="/img/act/image00.jpg" width="100%">

        <span style="position: absolute;left: 30%;top: 86%;"><?= $scheme_b[0] ?></span>
        <span style="position: absolute;left: 29%;top: 62%;"><?= $scheme_b[1] ?></span>
        <span style="position: absolute;left: 23%;top: 3%;"><?= $scheme_b[2] ?></span>
        <span style="position: absolute;left: 45%;top: 11%;"><?= $scheme_b[3] ?></span>
        <span style="position: absolute;left: 45%;top: 1%;"><?= $scheme_b[4] ?></span>
        <span style="position: absolute;left: 52%;top: 7%;"><?= $scheme_b[5] ?></span>
        <span style="position: absolute;left: 49%;top: 58%;"><?= $scheme_b[6] ?></span>
        <span style="position: absolute;left: 73%;top: 53%;"><?= $scheme_b[7] ?></span>
        <span style="position: absolute;left: 53%;top: 64%;"><?= $scheme_b[8] ?></span>
        <span style="position: absolute;left: 83%;top: 3%;"><?= @$scheme_b[9] ?></span>

    </div>
</div>

<?endif;?>