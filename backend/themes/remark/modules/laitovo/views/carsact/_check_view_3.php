<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?if (isset($model->act->difference['fv_dlina']) || isset($model->act->difference['fv_visota']) || isset($model->act->difference['fv_magnit'])):?>
<!-- Общие свойства ПФ -->
<div class="row">
    <div class="col-md-6">
        <h5>ПФ - Свойства (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_dlina',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fv_dlina']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fv_dlina']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fv_dlina')])
                               .Html::tag('label',$model->fv_dlina),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),             
                ],
                [
                    'attribute' => 'fv_visota',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fv_visota']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fv_visota']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fv_visota')])
                               .Html::tag('label',$model->fv_visota),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),              
                ],
                [
                    'attribute' => 'fv_magnit',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fv_magnit']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fv_magnit']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fv_magnit')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fv_magnit ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
        <h5>ПФ - Свойства (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_dlina_b',
                    'format' => 'html',
                    'value' => $model->fv_dlina_b,              
                ],
                [
                    'attribute' => 'fv_visota_b',
                    'format' => 'html',
                    'value' => $model->fv_visota_b,              
                ],
                [
                    'attribute' => 'fv_magnit_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fv_magnit_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<!-- Продукция для ПФ  -->
<div class="form-group">
    <!-- Laitovo Стандарт -->
    <?if  (isset($model->act->difference['fv_laitovo_standart_status'])):?>
    <h5>ПФ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->fv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_laitovo_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fv_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fv_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fv_laitovo_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_laitovo_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fv_laitovo_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fv_laitovo_standart_scheme != $model->fv_laitovo_standart_scheme_b): ?>
    <h5>ПФ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->fv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПФ - Laitovo Стандарт [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fv_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ПФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fv_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ПФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fv_laitovo_standart_schemem != $model->fv_laitovo_standart_schemem_b): ?>
    <h5>ПФ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->fv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПФ - Laitovo Стандарт [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fv_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ПФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fv_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ПФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- Laitovo Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['fv_laitovo_dontlookb_status'])):?>
    <h5>ПФ - Laitovo Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->fv_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_laitovo_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fv_laitovo_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fv_laitovo_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fv_laitovo_dontlookb_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fv_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_laitovo_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fv_laitovo_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- Chiko Стандарт -->
    <?if  (isset($model->act->difference['fv_chiko_standart_status'])):?>
    <h5>ПФ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->fv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_chiko_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fv_chiko_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fv_chiko_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fv_chiko_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_chiko_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fv_chiko_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fv_chiko_standart_scheme != $model->fv_chiko_standart_scheme_b): ?>
    <h5>ПФ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->fv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПФ - Chiko Стандарт [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fv_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ПФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fv_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ПФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif?>

    <? if ($model->fv_chiko_standart_schemem != $model->fv_chiko_standart_schemem_b): ?>
    <h5>ПФ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->fv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПФ - Chiko Стандарт [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fv_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ПФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fv_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ПФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif?>


    <!-- Chiko Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['fv_chiko_dontlookb_status'])):?>
    <h5>ПФ - Chiko Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->fv_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">  
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_chiko_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fv_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fv_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fv_chiko_dontlookb_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fv_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fv_chiko_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fv_chiko_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>
</div>