<?php

use yii\helpers\Json;
use common\models\laitovo\Cars;

?>

<!doctype html>
<html>
<body>

<table class="waffle" width="100%" cellspacing="0" cellpadding="0">
    <thead>
    </thead>
    <tbody>
    <tr style='height:19px;'>
        <td class="s0" colspan="2">
            <small>Версия от 01.10.2016 г.</small>
        </td>
        <td class="s1"></td>
        <td class="s1"></td>
        <td class="s1"></td>
        <td class="s1"></td>
        <td class="s1"></td>
        <td class="s1"></td>
        <td class="s0"></td>
        <td class="s0"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s2">УТВЕРЖДАЮ</td>
        <td class="s2">&nbsp;</td>
        <td class="s2">&nbsp;</td>
        <td class="s2">ДОПУЩЕНО К ПРОИЗВОДСТВУ</td>
        <td class="s2">&nbsp;</td>
        <td class="s2">&nbsp;</td>
        <td class="s2">СОГЛАСОВАНО</td>
        <td class="s2">&nbsp;</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s2">Директор по развитию</td>
        <td class="s2"></td>
        <td class="s2"></td>
        <td class="s2">Технический директор</td>
        <td class="s2"></td>
        <td class="s2"></td>
        <td class="s2">Мастер производства</td>
        <td class="s2"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s2">_______________ Антонов Д.Л.</td>
        <td class="s2"></td>
        <td class="s2"></td>
        <td class="s2">_____________ Алексеев А.В.</td>
        <td class="s2"></td>
        <td class="s2"></td>
        <td class="s2">_____________</td>
        <td class="s2"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s2">&quot;_____&quot;______________ <?= Yii::$app->formatter->asDate($model->created_at, 'yyyy') ?>
            г.
        </td>
        <td class="s2"></td>
        <td class="s2"></td>
        <td class="s2">&quot;_____&quot;______________ <?= Yii::$app->formatter->asDate($model->created_at, 'yyyy') ?>
            г.
        </td>
        <td class="s2"></td>
        <td class="s2"></td>
        <td class="s2">&quot;_____&quot;______________ <?= Yii::$app->formatter->asDate($model->created_at, 'yyyy') ?>
            г.
        </td>
        <td class="s2"></td>
    </tr>
    </tbody>
</table>

<br>
<div class="">
    <div style="float: left;width: 30.33%"><img src="/img/laitovo_logo.png"></div>
    <div style="float: left;width: 36.33%; font-size: 10px"><br><b><?= @$title?></b></div>
    <div style="float: left;width: 33.33%"><br>

        <table class="invoice_items" width="100%" cellpadding="2" cellspacing="2" border="0">
            <tbody>
            <tr style='height:28px;'>
                <td class="s8"><b><?= $after['article'] ?></b></td>
                <td class="s9">&nbsp;</td>
            </tr>
            </tbody>
        </table>

    </div>
</div>
<br>
<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:19px;background: #ddd;'>
        <td class="s11">Артикул</td>
        <td class="s12" colspan="3">МАРКА</td>
        <td class="s12" colspan="3">Модель для // Laitovo.ru</td>
        <td class="s12" colspan="3">Модель для // Laitovo.de</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s13"><b><?= $after['article'] ?></b></td>
        <td class="s13" colspan="3"><b><?= $after['mark'] ?></b></td>
        <td class="s13" colspan="3"><b><?= $after['model'] ?></b></td>
        <td class="s13" colspan="3"><b><?= @$after['fields']['modelde'] ?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s12" colspan="3">Модификация</td>
        <td class="s12" colspan="3">Тип кузова</td>
        <td class="s12" colspan="2">Кол-во дверей</td>
        <td class="s12" colspan="2">Поколение</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s13" colspan="3"><b><?= $after['modification'] ?></b></td>
        <td class="s13" colspan="3"><b><?= $after['carcass'] ?></b></td>
        <td class="s13" colspan="2"><b><?= $after['doors'] ?></b></td>
        <td class="s13" colspan="2"><b><?= $after['generation'] ?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s12" colspan="2">Год начала выпуска</td>
        <td class="s12" colspan="2">Год окон-я выпуска</td>
        <td class="s12" colspan="4">Закрытие г.в. предыдущего поколения</td>
        <td class="s12" colspan="2">Тип крепления</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s13" colspan="2"><b><?= $after['firstyear'] ?></b></td>
        <td class="s13" colspan="2"><b><?= $after['lastyear'] ?></b></td>
        <td class="s13" colspan="4"><b></b></td>
        <td class="s13" colspan="2"><b><?= $after['fields']['typekrep'] ?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s11" colspan="6">Страна производитель
            <small>(указать все страны в которых производится(лась) модель)</small>
        </td>
        <td class="s11" colspan="2">№ ЛЕКАЛА / сторона</td>
        <td class="s12" colspan="2">Тип обшивки</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s13" colspan="6"><b><?= $after['country'] ?></b></td>
        <td class="s13" colspan="2"><b><?= $after['fields']['nomerlekala'] ?></b></td>
        <td class="s13" colspan="2"></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s14" colspan="2">Ценовая категория</td>
        <td class="s12" colspan="8">ОСВАИВАЕМЫЕ ЭЛЕМЕНТЫ АВТО (отметить знаком V)</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s13" colspan="2"><b><?= $after['category'] ?></b></td>
        <td class="s15">ПШ <b><?= $after['fields']['fw'] ? 'V' : '' ?></b></td>
        <td class="s15">ПФ <b><?= $after['fields']['fv'] ? 'V' : '' ?></b></td>
        <td class="s15">ПБ <b><?= $after['fields']['fd'] ? 'V' : '' ?></b></td>
        <td class="s15">ЗБ <b><?= $after['fields']['rd'] ? 'V' : '' ?></b></td>
        <td class="s15">ЗФ <b><?= $after['fields']['rv'] ? 'V' : '' ?></b></td>
        <td class="s15">ЗШ <b><?= $after['fields']['bw'] ? 'V' : '' ?></b></td>
        <td class="s16">ПК <b><?= $after['fields']['pk'] ? 'V' : '' ?></b></td>
        <td class="s15">Капот</td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s12" colspan="10">Возможность изготовления вырезов и дополнительных продуктов</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s17" colspan="10">С - стандарт / К - для курящих / З - для зеркал / У - укороченный</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s18" colspan="2">Laitovo</td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s18"><b><?= $after['fv_laitovo_standart_status'] ? 'С' : '' ?></b></td>
        <td class="s18">
            <b>
                <?= $after['fd_laitovo_standart_status'] ? 'С' : '' ?>
                <?= $after['fd_laitovo_short_status'] ? 'У' : '' ?>
                <?= $after['fd_laitovo_smoke_status'] ? 'К' : '' ?>
                <?= $after['fd_laitovo_mirror_status'] ? 'З' : '' ?>
            </b>
        </td>
        <td class="s18"><b><?= $after['rd_laitovo_standart_status'] ? 'С' : '' ?></b></td>
        <td class="s18"><b><?= $after['rv_laitovo_standart_status'] ? 'С' : '' ?></b></td>
        <td class="s18"><b><?= $after['bw_laitovo_standart_status'] ? 'С' : '' ?></b></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s18" colspan="2">Chiko</td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s18"><b><?= $after['fv_chiko_standart_status'] ? 'С' : '' ?></b></td>
        <td class="s18">
            <b>
                <?= $after['fd_chiko_standart_status'] ? 'С' : '' ?>
                <?= $after['fd_chiko_short_status'] ? 'У' : '' ?>
                <?= $after['fd_chiko_smoke_status'] ? 'К' : '' ?>
                <?= $after['fd_chiko_mirror_status'] ? 'З' : '' ?>
            </b>
        </td>
        <td class="s18"><b><?= $after['rd_chiko_standart_status'] ? 'С' : '' ?></b></td>
        <td class="s18"><b><?= $after['rv_chiko_standart_status'] ? 'С' : '' ?></b></td>
        <td class="s18"><b><?= $after['bw_chiko_standart_status'] ? 'С' : '' ?></b></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s18" colspan="2">Chiko magnet</td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s18"><b><?= $after['fd_chikomagnet_status'] ? 'С' : '' ?></b></td>
        <td class="s18"><b><?= $after['rd_chikomagnet_status'] ? 'С' : '' ?></b></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s18" colspan="2">Don't look
            <small>сдвижной</small>
        </td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s18">
            <b><?= $after['fd_laitovo_dontlooks_status'] || $after['fd_chiko_dontlooks_status'] ? 'С' : '' ?></b></td>
        <td class="s18">
            <b><?= $after['rd_laitovo_dontlooks_status'] || $after['rd_chiko_dontlooks_status'] ? 'С' : '' ?></b></td>
        <td class="s18"><b><?= $after['rv_laitovo_dontlooks_status'] ? 'С' : '' ?></b></td>
        <td class="s18"><b><?= $after['bw_laitovo_dontlooks_status'] ? 'С' : '' ?></b></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s18" colspan="2">Don't look
            <small>бескаркасный</small>
        </td>
        <td class="s18"><b><?= $after['fw_status'] ? 'С' : '' ?></b></td>
        <td class="s18">
            <b><?= $after['fv_laitovo_dontlookb_status'] || $after['fv_chiko_dontlookb_status'] ? 'С' : '' ?></b></td>
        <td class="s18">
            <b><?= $after['fd_laitovo_dontlookb_status'] || $after['fd_chiko_dontlookb_status'] ? 'С' : '' ?></b></td>
        <td class="s18">
            <b><?= $after['rd_laitovo_dontlookb_status'] || $after['rd_chiko_dontlookb_status'] ? 'С' : '' ?></b></td>
        <td class="s18">
            <b><?= $after['rv_laitovo_dontlookb_status'] || $after['rv_chiko_dontlookb_status'] ? 'С' : '' ?></b></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
        <td class="s19" style="background: #ddd;"></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s12" colspan="10">ИНФОРМАЦИЯ ОБ АНАЛОГАХ</td>
    </tr>
    <? if ($analogs = Json::decode($after['analogs'], $asArray = true))
        foreach ($analogs as $analog):?>
            <tr style='height:19px;'>
                <td class="s17"><?= @Cars::findOne($analog['car'])->article ?></td>
                <td class="s17" colspan="9"><?= $analog['name'] ?> <b><?= implode(',', $analog['elements']) ?></b></td>
            </tr>
        <? endforeach ?>
    <tr style='height:19px;background: #ddd;'>
        <td class="s20" colspan="10">ЭТАПЫ ОСВОЕНИЯ</td>
    </tr>
    <tr style='height:65px;'>
        <td class="s21" colspan="2">Снятие контура</td>
        <td class="s21" colspan="2">Изготовление временного лекала и запуск в пр-во опытного образца</td>
        <td class="s21" colspan="2">Примерка опытного образца и снятие фото и видео отчета</td>
        <td class="s22" colspan="2">Изготовление действующего лекала, оцифровка и помещение информации в хранилище</td>
        <td class="s21" colspan="2">Занесение информации в журналы ОТК и КЛИПС</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s17" colspan="2">&nbsp;</td>
        <td class="s17" colspan="2"></td>
        <td class="s17" colspan="2"></td>
        <td class="s17" colspan="2"></td>
        <td class="s17" colspan="2"></td>
    </tr>
    <tr style='height:14px;'>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
    </tr>
    <tr style='height:52px;'>
        <td class="s18">&nbsp;</td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
    </tr>
    <tr style='height:14px;'>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
    </tr>
    <tr style='height:65px;'>
        <td class="s21" colspan="2">Занесение информации в карточку автомобиля</td>
        <td class="s21" colspan="2">Занесение информации в систему управления (менеджер файлов/фото)</td>
        <td class="s21" colspan="2">Размещение видео на канале и отправка ссылки на модерацию</td>
        <td class="s21" colspan="2">Лекало принято участком оклейки</td>
        <td class="s21" colspan="2">Лекало принято участком изгиба</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s17" colspan="2">&nbsp;</td>
        <td class="s17" colspan="2"></td>
        <td class="s17" colspan="2"></td>
        <td class="s17" colspan="2"></td>
        <td class="s17" colspan="2"></td>
    </tr>
    <tr style='height:14px;'>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
        <td class="s23" colspan="2">
            <small>ФИО</small>
        </td>
    </tr>
    <tr style='height:52px;'>
        <td class="s18">&nbsp;</td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
        <td class="s18"></td>
    </tr>
    <tr style='height:14px;'>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
        <td class="s23">
            <small>дата</small>
        </td>
        <td class="s23">
            <small>подпись</small>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
