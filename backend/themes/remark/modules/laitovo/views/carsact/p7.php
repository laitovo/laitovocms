<?php

use yii\helpers\Json;
use common\models\laitovo\Cars;
use common\models\laitovo\CarsAnalog;

//в функцию передаем идентификатор и список идентификаторов
//функция возвращает либо массив с элементами либо FALSE
function search ($car_id,$type,$ids = []) {
    $ids2 = [];
    $ids2[] = $car_id;
    $analogs = CarsAnalog::find()->where(['car_id' => $car_id])->all();
    foreach ($analogs as $analog) {
        $json=Json::decode($analog->json ? $analog->json : '{}');
        $types = isset($json['elements']) ? $json['elements'] : [];
        if (!in_array($analog->analog_id, $ids2) && in_array($type,$types)) {
            $ids2[] = $analog->analog_id;
        }
    }

    //Находим расхождения в массивах
    $diff = array_diff($ids2, $ids);

    // if ($car_id == 638) {
    //     var_dump('IDS-2');
    //     var_dump($ids2);
    //     var_dump('IDS');
    //     var_dump($ids);
    //     var_dump('DIFF');
    //     var_dump($diff);
    //     var_dump(empty($diff));
    //     die();
    // }

    if (count($diff)) {
        foreach ($diff as $key => $value) {
            $values = search($value,$type,array_merge($diff, $ids));
            if ($values) {
                foreach ($values as $value) {
                    if (!in_array($value, $diff)) {
                        $diff[] = $value;
                    }
                }
            }

        }
    }


    //возвращаем либо массив либо ложь
    return empty($diff) ? false : $diff;
}

$FVId = search($model->car_id,'ПФ');
$FDId = search($model->car_id,'ПБ');
$RDId = search($model->car_id,'ЗБ');
$RVId = search($model->car_id,'ЗФ');
$BWId = search($model->car_id,'ЗШ');


//Собрал аналоги по проемам
$elements = array_fill_keys ( ['ПФ','ПБ','ЗБ','ЗФ','ЗШ'] , [@$model->car->article] );

$FVanalogs = Cars::find()->where(['in','id',$FVId])->all();
$FDanalogs = Cars::find()->where(['in','id',$FDId])->all();
$RDanalogs = Cars::find()->where(['in','id',$RDId])->all();
$RVanalogs = Cars::find()->where(['in','id',$RVId])->all();
$BWanalogs = Cars::find()->where(['in','id',$BWId])->all();

if ($FVanalogs) {
    foreach ($FVanalogs as $FVanalog) {
        if (!in_array($FVanalog->article,$elements['ПФ'])) {
            $elements['ПФ'][] = $FVanalog->article;
        }
    }
}

if ($FDanalogs) {
    foreach ($FDanalogs as $FDanalog) {
        if (!in_array($FDanalog->article,$elements['ПБ'])) {
            $elements['ПБ'][] = $FDanalog->article;
        }
    }
}

if ($RDanalogs) {
    foreach ($RDanalogs as $RDanalog) {
        if (!in_array($RDanalog->article,$elements['ЗБ'])) {
            $elements['ЗБ'][] = $RDanalog->article;
        }
    }
}

if ($RVanalogs) {
    foreach ($RVanalogs as $RVanalog) {
        if (!in_array($RVanalog->article,$elements['ЗФ'])) {
            $elements['ЗФ'][] = $RVanalog->article;
        }
    }
}

if ($BWanalogs) {
    foreach ($BWanalogs as $BWanalog) {
        if (!in_array($BWanalog->article,$elements['ЗШ'])) {
            $elements['ЗШ'][] = $BWanalog->article;
        }
    }
}


sort($elements['ПФ']);
sort($elements['ПБ']);
sort($elements['ЗБ']);
sort($elements['ЗФ']);
sort($elements['ЗШ']);

//Ищем в аналогах все записи, где ма
$fv_clip = [];
if ($after['fv_laitovo_standart_scheme']) {
    foreach (Json::decode($after['fv_laitovo_standart_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$fv_clip[$value[0]] += 2;
        }
    }
}
ksort($fv_clip);
//ПБ стандарт
$fd_clip_standart = [];
if ($after['fd_laitovo_standart_scheme']) {
    foreach (Json::decode($after['fd_laitovo_standart_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$fd_clip_standart[$value[0]] += 2;
        }
    }
}
ksort($fd_clip_standart);
//ПБ укороченки
$fd_clip_short = [];
if ($after['fd_laitovo_short_scheme']) {
    foreach (Json::decode($after['fd_laitovo_short_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$fd_clip_short[$value[0]] += 2;
        }
    }
}
ksort($fd_clip_short);
//ПБ с вырезом для зеркала
$fd_clip_mirror = [];
if ($after['fd_laitovo_mirror_scheme']) {
    foreach (Json::decode($after['fd_laitovo_mirror_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$fd_clip_mirror[$value[0]] += 2;
        }
    }
}
ksort($fd_clip_mirror);
//ПБ с вырезом для курящих
$fd_clip_smoke = [];
if ($after['fd_laitovo_smoke_scheme']) {
    foreach (Json::decode($after['fd_laitovo_smoke_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$fd_clip_smoke[$value[0]] += 2;
        }
    }
}
ksort($fd_clip_smoke);

$fd_clip = $fd_clip_standart + $fd_clip_short + $fd_clip_mirror + $fd_clip_smoke;
ksort($fd_clip);

// if ($after['fd_laitovo_standart_schemem']) {
//     foreach (Json::decode($after['fd_laitovo_standart_schemem']) as $key => $value) {
//         $value = explode('/', $value);
//         if ($value[0]) {
//             @$fd_clip[$value[0]] += 2;
//         }
//     }
// }

$rd_clip = [];
if ($after['rd_laitovo_standart_scheme']) {
    foreach (Json::decode($after['rd_laitovo_standart_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$rd_clip[$value[0]] += 2;
        }
    }
}
ksort($rd_clip);
// if ($after['rd_laitovo_standart_schemem']) {
//     foreach (Json::decode($after['rd_laitovo_standart_schemem']) as $key => $value) {
//         $value = explode('/', $value);
//         if ($value[0]) {
//             @$rd_clip[$value[0]] += 2;
//         }
//     }
// }

$rv_clip = [];
if ($after['rv_laitovo_standart_scheme'] && preg_match('#/#',$after['rv_laitovo_standart_scheme']) == 1 ) {
    foreach (Json::decode($after['rv_laitovo_standart_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$rv_clip[$value[0]] += 2;
        }
    }
} elseif ($after['rv_laitovo_triangul_scheme'] && preg_match('#/#',$after['rv_laitovo_triangul_scheme']) == 1 ) {
    foreach (Json::decode($after['rv_laitovo_triangul_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$rv_clip[$value[0]] += 2;
        }
    }
}
ksort($rv_clip);

$bw_clip = [];
if ($after['bw_chiko_standart_scheme'] && preg_match('#/#',$after['bw_chiko_standart_scheme']) == 1 ) {
    foreach (Json::decode($after['bw_chiko_standart_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$bw_clip[$value[0]] += 1;

        }
    }
} elseif ($after['bw_chiko_double_scheme'] && preg_match('#/#',$after['bw_chiko_double_scheme']) == 1 ) {
    foreach (Json::decode($after['bw_chiko_double_scheme']) as $key => $value) {
        $value = explode('/', $value);
        if ($value[0]) {
            @$bw_clip[$value[0]] += 1;
        }
    }
}
ksort($bw_clip);

//Здесь мы имеем все записи клипс, которые могут быть и для них в случае чего выводим документы

$someResult = [];
$someResult['ПФ'] = $fv_clip;
$someResult['ПБ'] = $fd_clip;
$someResult['ЗБ'] = $rd_clip;
$someResult['ЗФ'] = $rv_clip;
$someResult['ЗШ'] = $bw_clip;

?>
<!doctype html>
<html>
<body>

<?
$page = 0;
foreach ($someResult as $key => $value) {
    if (count($value)) {
        $type_clips = [];
        foreach ($value as $key1 => $value1) {
            if (!in_array($key1, $type_clips)) {
                $type_clips[] = $key1;
            }
        }

        sort($type_clips);

        $left = [];
        $right = [];

        $count = count($type_clips);

        for($i = 0; $i < $count; $i +=2)
        {
            $left[] = $type_clips[$i];
        }

        for($i = 1; $i < $count; $i +=2)
        {
            $right[] = $type_clips[$i];
        }

        if ($page > 0) echo '<p style = "page-break-before:always"></p>';

        echo $this->render('p7_inside',[
                'model' => $model,
                'after' => $after,
                'title' => $title,
                'clips' => $value,
                'left' => $left,
                'right' => $right,
                'listcars' => isset($elements[$key]) ? implode(', ', $elements[$key]) : '',
                'element' => $key]
        );
        $page++;
    }
}
// var_dump($type_clips);
// var_dump($left);
// var_dump($right);
// die();
?>


</body>
</html>





