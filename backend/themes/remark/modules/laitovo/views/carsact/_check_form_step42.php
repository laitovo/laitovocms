<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */

$clips = Json::decode($model->clips);
$scheme1 = Json::decode($model->fd_laitovo_short_scheme);
$scheme2 = Json::decode($model->fd_laitovo_short_schemem);
$scheme5 = Json::decode($model->fd_laitovo_short_schemet);
$scheme3 = Json::decode($model->fd_chiko_short_scheme);
$scheme4 = Json::decode($model->fd_chiko_short_schemem);
$scheme6 = Json::decode($model->fd_chiko_short_schemet);

Yii::$app->view->registerJs('

    function parseitemsjson1 () {
        var scheme =[];
        $(\'.schemeitems1 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-fd_laitovo_short_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems1 select",function(e){
        parseitemsjson1();
        e.preventDefault();
    });


    function parseitemsjson2 () {
        var scheme =[];
        $(\'.schemeitems2 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-fd_laitovo_short_schemem\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems2 select",function(e){
        parseitemsjson2();
        e.preventDefault();
    });


    function parseitemsjson5 () {
        var scheme =[];
        $(\'.schemeitems5 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-fd_laitovo_short_schemet\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems5 select",function(e){
        parseitemsjson5();
        e.preventDefault();
    });


    function parseitemsjson3 () {
        var scheme =[];
        $(\'.schemeitems3 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-fd_chiko_short_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems3 select",function(e){
        parseitemsjson3();
        e.preventDefault();
    });


    function parseitemsjson4 () {
        var scheme =[];
        $(\'.schemeitems4 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-fd_chiko_short_schemem\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems4 select",function(e){
        parseitemsjson4();
        e.preventDefault();
    });


    function parseitemsjson6 () {
        var scheme =[];
        $(\'.schemeitems6 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-fd_chiko_short_schemet\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems6 select",function(e){
        parseitemsjson6();
        e.preventDefault();
    });

    ', \yii\web\View::POS_END);


?>
<h3>ПБ: Укороченный</h3>
<div class="form-group tabcontent">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#laitovo" aria-controls="laitovo" role="tab" data-toggle="tab">Laitovo</a>
        </li>
        <li role="presentation"><a href="#chiko" aria-controls="chiko" role="tab" data-toggle="tab">Chiko</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="laitovo">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_short" aria-controls="laitovo_short"
                                                              role="tab" data-toggle="tab">Простые</a></li>
                    <li role="presentation"><a href="#laitovo_magnit" aria-controls="laitovo_magnit" role="tab"
                                               data-toggle="tab">Магнитные</a></li>
                    <li role="presentation"><a href="#laitovo_tape" aria-controls="laitovo_tape" role="tab"
                                               data-toggle="tab">Скотч</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_short">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems1">

                                <img src="/img/fd_u_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme1[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 35%;", 'id' => 'fd_laitovo_short_scheme_0']) ?>
                                <?= Html::dropDownList(null, @$scheme1[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 27%;", 'id' => 'fd_laitovo_short_scheme_1']) ?>
                                <?= Html::dropDownList(null, @$scheme1[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 19%;", 'id' => 'fd_laitovo_short_scheme_2']) ?>
                                <?= Html::dropDownList(null, @$scheme1[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 16%;", 'id' => 'fd_laitovo_short_scheme_3']) ?>
                                <?= Html::dropDownList(null, @$scheme1[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 17%;", 'id' => 'fd_laitovo_short_scheme_4']) ?>
                                <?= Html::dropDownList(null, @$scheme1[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 40%;", 'id' => 'fd_laitovo_short_scheme_5']) ?>
                                <?= Html::dropDownList(null, @$scheme1[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 40%;", 'id' => 'fd_laitovo_short_scheme_6']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_magnit">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems2">

                                <img src="/img/fd_u_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme2[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 35%;", 'id' => 'fd_laitovo_short_schemem_0']) ?>
                                <?= Html::dropDownList(null, @$scheme2[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 27%;", 'id' => 'fd_laitovo_short_schemem_1']) ?>
                                <?= Html::dropDownList(null, @$scheme2[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 19%;", 'id' => 'fd_laitovo_short_schemem_2']) ?>
                                <?= Html::dropDownList(null, @$scheme2[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 16%;", 'id' => 'fd_laitovo_short_schemem_3']) ?>
                                <?= Html::dropDownList(null, @$scheme2[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 17%;", 'id' => 'fd_laitovo_short_schemem_4']) ?>
                                <?= Html::dropDownList(null, @$scheme2[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 40%;", 'id' => 'fd_laitovo_short_schemem_5']) ?>
                                <?= Html::dropDownList(null, @$scheme2[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 40%;", 'id' => 'fd_laitovo_short_schemem_6']) ?>

                            </div>
                        </div>


                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_tape">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems5">

                                <img src="/img/fd_u_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme5[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 35%;", 'id' => 'fd_laitovo_short_schemet_0']) ?>
                                <?= Html::dropDownList(null, @$scheme5[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 27%;", 'id' => 'fd_laitovo_short_schemet_1']) ?>
                                <?= Html::dropDownList(null, @$scheme5[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 19%;", 'id' => 'fd_laitovo_short_schemet_2']) ?>
                                <?= Html::dropDownList(null, @$scheme5[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 16%;", 'id' => 'fd_laitovo_short_schemet_3']) ?>
                                <?= Html::dropDownList(null, @$scheme5[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 17%;", 'id' => 'fd_laitovo_short_schemet_4']) ?>
                                <?= Html::dropDownList(null, @$scheme5[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 40%;", 'id' => 'fd_laitovo_short_schemet_5']) ?>
                                <?= Html::dropDownList(null, @$scheme5[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 40%;", 'id' => 'fd_laitovo_short_schemet_6']) ?>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chiko">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_short" aria-controls="chiko_short" role="tab"
                                                              data-toggle="tab">Простые</a></li>
                    <li role="presentation"><a href="#chiko_magnit" aria-controls="chiko_magnit" role="tab"
                                               data-toggle="tab">Магнитные</a></li>
                    <li role="presentation"><a href="#chiko_tape" aria-controls="chiko_tape" role="tab"
                                               data-toggle="tab">Скотч</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_short">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems3">

                                <img src="/img/fd_u_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme3[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 35%;", 'id' => 'fd_chiko_short_scheme_0']) ?>
                                <?= Html::dropDownList(null, @$scheme3[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 27%;", 'id' => 'fd_chiko_short_scheme_1']) ?>
                                <?= Html::dropDownList(null, @$scheme3[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 19%;", 'id' => 'fd_chiko_short_scheme_2']) ?>
                                <?= Html::dropDownList(null, @$scheme3[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 16%;", 'id' => 'fd_chiko_short_scheme_3']) ?>
                                <?= Html::dropDownList(null, @$scheme3[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 17%;", 'id' => 'fd_chiko_short_scheme_4']) ?>
                                <?= Html::dropDownList(null, @$scheme3[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 40%;", 'id' => 'fd_chiko_short_scheme_5']) ?>
                                <?= Html::dropDownList(null, @$scheme3[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 40%;", 'id' => 'fd_chiko_short_scheme_6']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_magnit">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems4">

                                <img src="/img/fd_u_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme4[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 35%;", 'id' => 'fd_chiko_short_schemem_0']) ?>
                                <?= Html::dropDownList(null, @$scheme4[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 27%;", 'id' => 'fd_chiko_short_schemem_1']) ?>
                                <?= Html::dropDownList(null, @$scheme4[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 19%;", 'id' => 'fd_chiko_short_schemem_2']) ?>
                                <?= Html::dropDownList(null, @$scheme4[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 16%;", 'id' => 'fd_chiko_short_schemem_3']) ?>
                                <?= Html::dropDownList(null, @$scheme4[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 17%;", 'id' => 'fd_chiko_short_schemem_4']) ?>
                                <?= Html::dropDownList(null, @$scheme4[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 40%;", 'id' => 'fd_chiko_short_schemem_5']) ?>
                                <?= Html::dropDownList(null, @$scheme4[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 40%;", 'id' => 'fd_chiko_short_schemem_6']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_tape">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems6">

                                <img src="/img/fd_u_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme6[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 35%;", 'id' => 'fd_chiko_short_schemet_0']) ?>
                                <?= Html::dropDownList(null, @$scheme6[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 27%;", 'id' => 'fd_chiko_short_schemet_1']) ?>
                                <?= Html::dropDownList(null, @$scheme6[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 22%;top: 19%;", 'id' => 'fd_chiko_short_schemet_2']) ?>
                                <?= Html::dropDownList(null, @$scheme6[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 16%;", 'id' => 'fd_chiko_short_schemet_3']) ?>
                                <?= Html::dropDownList(null, @$scheme6[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 17%;", 'id' => 'fd_chiko_short_schemet_4']) ?>
                                <?= Html::dropDownList(null, @$scheme6[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 47%;top: 40%;", 'id' => 'fd_chiko_short_schemet_5']) ?>
                                <?= Html::dropDownList(null, @$scheme6[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 35%;top: 40%;", 'id' => 'fd_chiko_short_schemet_6']) ?>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
</div>
