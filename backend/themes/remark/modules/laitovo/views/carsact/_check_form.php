<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;
use backend\themes\remark\assets\FormAsset;
use yii\bootstrap\Modal;
use common\models\laitovo\Cars;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */
FormAsset::register($this);

Yii::$app->view->registerJs("

    $('.date_picker').datepicker({
        autoclose: true,
        todayHighlight: true,
        language: 'ru'
    }).on('changeDate', function(e) {
        $('.tabcontent').removeClass('hidden');
        changebtnvalue();
    });

    $('a[data-toggle=\"tab\"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr(\"href\");
        if (target=='#chiko'){
            notie.confirm('" . Yii::t('app', 'Скопировать из Laitovo') . "', '" . Yii::t('yii', 'Yes') . "', '" . Yii::t('yii', 'No') . "', function() {
                $( \"#chiko.tab-pane input[type='checkbox']\" ).each(function( index ) {
                    if ($( '#laitovo.tab-pane input[type=\'checkbox\']#'+$( this ).attr('id').replace('_chiko_','_laitovo_') ).length){
                        $( this ).prop( 'checked', $( '#laitovo.tab-pane input[type=\'checkbox\']#'+$( this ).attr('id').replace('_chiko_','_laitovo_') ).prop('checked') );
                    }
                });
                $( \"#chiko.tab-pane input[type='text']\" ).each(function( index ) {
                    if ($( '#laitovo.tab-pane input[type=\'text\']#'+$( this ).attr('id').replace('_chiko_','_laitovo_') ).length){
                        $( this ).val($( '#laitovo.tab-pane input[type=\'text\']#'+$( this ).attr('id').replace('_chiko_','_laitovo_') ).val() );
                    }
                });
                $( \"#chiko.tab-pane select\" ).each(function( index ) {
                    if ($( '#laitovo.tab-pane select#'+$( this ).attr('id').replace('_chiko_','_laitovo_') ).length){
                        $( this ).val($( '#laitovo.tab-pane select#'+$( this ).attr('id').replace('_chiko_','_laitovo_') ).val() ).change();
                    }
                });

                $('div.tabcontent > div.tab-content > .tab-pane.active ul.nav-tabs li:last a').tab('show');
                $('div.tabcontent > ul.nav-tabs > li.active').next().find('a').tab('show');
                changebtnvalue();
            });
        }
    });

    $('div.tabcontent ul.nav-tabs').on('click', function() {
        return false;
    });

    function changebtnvalue()
    {
    }
  
    $('body').on('submit','#laitovocarscheckupdateform', function (e)
    {
        if (!$('div.tabcontent').hasClass('hidden')){
            var li=$('div.tabcontent > div.tab-content > .tab-pane.active ul.nav-tabs li.active');
            var li_1=$('div.tabcontent > ul.nav-tabs > li.active');

            if (li.next().length){
                li.next().find('a').tab('show');
                e.preventDefault();
            } else if (li_1.next().length){
                li_1.next().find('a').tab('show');
                e.preventDefault();
            }
            changebtnvalue();
        }
    });

    $('body').on('click','.btnstepprev', function (e)
    {
        if (!$('div.tabcontent').hasClass('hidden')){
            var li=$('div.tabcontent > div.tab-content > .tab-pane.active ul.nav-tabs li.active');
            var li_1=$('div.tabcontent > ul.nav-tabs > li.active');
            if (li.prev().length){
                li.prev().find('a').tab('show');
                e.preventDefault();
            } else if (li_1.prev().length){
                li_1.prev().find('a').tab('show');
                e.preventDefault();
            }
            changebtnvalue();
        }
    });

");

if ($model->scenario == $model->SCENARIO_STEP_FINISH || $model->scenario == $model::SCENARIO_STEP_CREATE) {
    Yii::$app->view->registerJs("
        function changebtnvalue()
        {
            setTimeout(function(){
                if (!$('div.tabcontent').hasClass('hidden')){
                    var li=$('div.tabcontent > div.tab-content > .tab-pane.active ul.nav-tabs li.active');
                    var li_1=$('div.tabcontent > ul.nav-tabs > li.active');

                    if (li.next().length || li_1.next().length){
                        $('.btnstepnext').text('Далее');
                    } else {
                        $('.btnstepnext').text('Сохранить');
                    }
                }
        }, 200);            
        }
        changebtnvalue();
    ");
}

?>
<style type="text/css">
    .select2-container {
        z-index: 1400;
    }
</style>
<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'id' => 'laitovocarscheckupdateform']); ?>
<?= $form->errorSummary($model) ?>

<div class="hidden">
    <?= $form->field($model, 'mark')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'model')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'generation')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'carcass')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'doors')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'firstyear')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'lastyear')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'modification')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'country')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'category')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fw1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw1')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'clips')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'analogs')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fw_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fw_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fw_scheme')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fv_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_laitovo_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_dontlookb_status')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fv_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_standart_schemem')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_obshivka')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_hlyastik')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_short_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_smoke_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_maxdlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_maxvisota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_maxdlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_maxvisota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chikomagnet_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chikomagnet_magnitov')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_standart_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_laitovo_short_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_short_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_short_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_laitovo_smoke_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_smoke_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_smoke_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_laitovo_mirror_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_install_direction')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_with_recess')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_use_tape')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rd_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_obshivka')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_hlyastik')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chikomagnet_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chikomagnet_magnitov')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rd_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_standart_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rd_install_direction')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_with_recess')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_use_tape')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rv_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_obshivka')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_openwindow')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_openwindowtrue')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_hlyastik')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_standart_forma')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_forma')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_dontlookb_status')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rv_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_schemem')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rv_laitovo_triangul_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_triangul_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_triangul_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_triangul_schemem')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'bw_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_obshivka')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_openwindow')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_openwindowtrue')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chastei')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_hlyastik')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_simmetr')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_gabarit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_laitovo_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_dontlookb_status')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'bw_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_standart_schemem')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'bw_laitovo_double_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_laitovo_double_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_double_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_double_schemem')->hiddenInput()->label(false) ?>

    <?= FormBuilder::widget([
        'form' => $form,
        'model' => $model,
        'attribute' => 'fields',
        'render' => $model->auto->fieldsrender,
    ]); ?>
</div>


<?= $this->render('_check_form_' . $model->scenario, [
    'model' => $model,
    'form' => $form,
]) ?>


<div class="form-group">
    <?= $model->auto->isNewRecord ? '' : Html::a(Yii::t('app', 'Назад'), Yii::$app->request->get('step') > 1 ? ['update-changes', 'id' => $model->auto->id, 'step' => Yii::$app->request->get('step') - 1, 'steps' => Yii::$app->request->get('steps')] : ['update-changes', 'id' => $model->auto->id], [
        'class' => 'btn btn-icon btn-outline btn-round  btn-default btnstepprev',
    ]) ?>
    <?= Html::submitButton(($model->scenario == $model->SCENARIO_STEP_FINISH || $model->scenario == $model::SCENARIO_STEP_CREATE) ? Yii::t('app', 'Сохранить') : Yii::t('app', 'Далее'), ['class' => 'btn btn-outline btn-round btn-primary btnstepnext']) ?>

</div>


<?php ActiveForm::end(); ?>


<?php Modal::begin([
    'options' => ['class' => 'clipsmodalform fade modal'],
    'header' => Html::tag('h4', Yii::t('app', 'Крепления'), ['class' => 'modal-title']),
]); ?>

<form class="saveclips">
    <div class="form-group">
        <label class="control-label" for="clips-name"><?= Yii::t('app', 'Наименование') ?></label>
        <input type="text" required="required" value="" class="required form-control clips-name" name="clips-name">
        <input type="hidden" class="clips-id" name="clips-id">
    </div>

    <div class="form-group">
        <label class="control-label" for="clips-type"><?= Yii::t('app', 'Тип крепления') ?></label>
        <select required="required" class="required form-control clips-type" name="clips-type">
            <option value=""></option>
            <option value="A">A</option>
            <option value="AA">AA</option>
            <option value="B">B</option>
            <option value="C">C</option>
            <option value="CC">CC</option>
            <option value="M">M</option>
            <option value="V">V</option>
            <option value="P">P</option>
            <option value="T">T</option>
        </select>
    </div>

    <?= Html::submitButton(Yii::t('app', 'Готово'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>
</form>

<?php Modal::end(); ?>

<?php Modal::begin([
    'options' => ['class' => 'analogsmodalform fade modal', 'tabindex' => null],
    'header' => Html::tag('h4', Yii::t('app', 'Аналоги'), ['class' => 'modal-title']),
]); ?>

<form class="saveanalogs">

    <div class="form-group">

        <label class="control-label" for="analogs-car"><?= Yii::t('app', 'Выберите автомобиль') ?></label>
        <?
        if ($this->beginCache('modal-analogs-car' . $model->auto->id, ['duration' => 3600])) {
            echo Html::dropDownList('analogs-car', null, ArrayHelper::merge(['' => ''], ArrayHelper::map(Cars::find()->where(['not', ['id' => $model->auto->id]])->all(), 'id', 'name')), [
                'required' => "required", 'class' => "required form-control analogs-car select2"
            ]);
            $this->endCache();
        } ?>

    </div>

    <?= Html::submitButton(Yii::t('app', 'Готово'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>
</form>

<?php Modal::end(); ?>
