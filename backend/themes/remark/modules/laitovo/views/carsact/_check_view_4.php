<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?if (
       isset($model->act->difference['fd_dlina']) 
    || isset($model->act->difference['fd_visota']) 
    || isset($model->act->difference['fd_magnit']) 
    || isset($model->act->difference['fd_obshivka']) 
    || isset($model->act->difference['fd_hlyastik']) 
    || isset($model->act->difference['fd_install_direction'])
    || isset($model->act->difference['fd_with_recess'])
    || isset($model->act->difference['fd_use_tape'])
):?>

<!-- Основные харакетристики для ПБ -->
<div class="row">
    <div class="col-md-6">
        <h5>ПБ - Свойства (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_dlina',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_dlina']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_dlina']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_dlina')])
                               .Html::tag('label',$model->fd_dlina),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_visota',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_visota']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_visota']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_visota')])
                               .Html::tag('label',$model->fd_visota),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_magnit',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_magnit']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_magnit']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_magnit')])
                               .Html::tag('label', $model->fd_magnit),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_obshivka',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_obshivka']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_obshivka']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_obshivka')])
                               .Html::tag('label',$model->fd_obshivka),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_hlyastik',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_hlyastik']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_hlyastik']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_hlyastik')])
                               .Html::tag('label',$model->fd_hlyastik),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_install_direction',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_install_direction']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_install_direction']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                        Html::checkbox("message[]",
                            $checked = false,
                            ['value' => $model->getAttributeLabel('fd_install_direction')])
                        .Html::tag('label',$model->fd_install_direction),
                        ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),
                ],
                [
                    'attribute' => 'fd_with_recess',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_with_recess']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_with_recess']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                        Html::checkbox("message[]",
                            $checked = false,
                            ['value' => $model->getAttributeLabel('fd_with_recess')])
                        .Html::tag('label',$model->fd_with_recess),
                        ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),
                ],
                [
                    'attribute' => 'fd_use_tape',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_use_tape']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_use_tape']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                        Html::checkbox("message[]",
                            $checked = false,
                            ['value' => $model->getAttributeLabel('fd_use_tape')])
                        .Html::tag('label',$model->fd_use_tape),
                        ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),
                ],
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
        <h5>ПБ - Свойства (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_dlina_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_dlina_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_dlina_b']) ? 'bg-warning' : ''],
                    'value' => $model->fd_dlina_b,
                ],
                [
                    'attribute' => 'fd_visota_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_visota_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_visota_b']) ? 'bg-warning' : ''],
                    'value' => $model->fd_visota_b,
                ],
                [
                    'attribute' => 'fd_magnit_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_magnit_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_magnit_b']) ? 'bg-warning' : ''],
                    'value' => $model->fd_magnit_b,
                ],
                [
                    'attribute' => 'fd_obshivka_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_obshivka_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_obshivka_b']) ? 'bg-warning' : ''],
                    'value' => $model->fd_obshivka_b,
                ],
                [
                    'attribute' => 'fd_hlyastik_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_hlyastik_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_hlyastik_b']) ? 'bg-warning' : ''],
                    'value' => Html::tag('i', '', ['class' => $model->fd_hlyastik_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fd_install_direction_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_install_direction_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_install_direction_b']) ? 'bg-warning' : ''],
                    'value' => $model->fd_install_direction_b,
                ],
                [
                    'attribute' => 'fd_with_recess_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_with_recess_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_with_recess_b']) ? 'bg-warning' : ''],
                    'value' => $model->fd_with_recess_b,
                ],
                [
                    'attribute' => 'fd_use_tape_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_use_tape_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_use_tape_b']) ? 'bg-warning' : ''],
                    'value' => $model->fd_use_tape_b,
                ],
            ],
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<!-- Продукция для ПБ  -->
<div class="form-group">
    <!-- Laitovo Стандарт -->
    <?if  (isset($model->act->difference['fd_laitovo_standart_status'])):?>
    <h5>ПБ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->fd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_laitovo_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_standart_scheme != $model->fd_laitovo_standart_scheme_b): ?>
    <h5>ПБ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->fd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Laitovo Стандарт [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_standart_schemem != $model->fd_laitovo_standart_schemem_b): ?>
    <h5>ПБ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->fd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Laitovo Стандарт [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_standart_schemet != $model->fd_laitovo_standart_schemet_b): ?>
    <h5>ПБ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->fd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ПБ - Laitovo Стандарт [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_standart_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_standart_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- Laitovo Укороченный -->
    <?if  (isset($model->act->difference['fd_laitovo_short_status'])):?>
    <h5>ПБ - Laitovo Укороченный <?=Html::tag('i', '', ['class' => $model->fd_laitovo_short_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_laitovo_short_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_laitovo_short_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_laitovo_short_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_laitovo_short_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_short_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_short_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_short_scheme != $model->fd_laitovo_short_scheme_b): ?>
    <h5>ПБ - Laitovo Укороченный <?=Html::tag('i', '', ['class' => $model->fd_laitovo_short_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Laitovo Укороченный [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_short_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Укороченный',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_short_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Укороченный',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_short_schemem != $model->fd_laitovo_short_schemem_b): ?>
    <h5>ПБ - Laitovo Укороченный <?=Html::tag('i', '', ['class' => $model->fd_laitovo_short_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Laitovo Укороченный [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_short_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Укороченный',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_short_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Укороченный',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>   
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_short_schemet != $model->fd_laitovo_short_schemet_b): ?>
    <h5>ПБ - Laitovo Укороченный <?=Html::tag('i', '', ['class' => $model->fd_laitovo_short_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ПБ - Laitovo Укороченный [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_short_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Укороченный',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_short_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Укороченный',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- Laitovo Вырез для курящих -->
    <?if  (isset($model->act->difference['fd_laitovo_smoke_status'])):?>
    <h5>ПБ - Laitovo Вырез для курящих <?=Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_laitovo_smoke_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_laitovo_smoke_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_laitovo_smoke_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_smoke_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_smoke_scheme != $model->fd_laitovo_smoke_scheme_b): ?>
    <h5>ПБ - Laitovo Вырез для курящих <?=Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Laitovo Вырез для курящих [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_smoke_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_smoke_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_smoke_schemem != $model->fd_laitovo_smoke_schemem_b): ?>
    <h5>ПБ - Laitovo Вырез для курящих <?=Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Laitovo Вырез для курящих [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_smoke_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_smoke_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>   
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_smoke_schemet != $model->fd_laitovo_smoke_schemet_b): ?>
    <h5>ПБ - Laitovo Вырез для курящих <?=Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ПБ - Laitovo Вырез для курящих [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_smoke_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_smoke_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Laitovo Вырез для зеркал -->
    <?if  (isset($model->act->difference['fd_laitovo_mirror_status'])):?>
    <h5>ПБ - Laitovo Вырез для зеркал <?=Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Свойства (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_mirror_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_laitovo_mirror_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_laitovo_mirror_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_laitovo_mirror_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_laitovo_mirror_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_laitovo_mirror_maxdlina',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_laitovo_mirror_maxdlina']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_laitovo_mirror_maxdlina']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_laitovo_mirror_maxdlina')])
                               .Html::tag('label',$model->fd_laitovo_mirror_maxdlina),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_laitovo_mirror_maxvisota',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_laitovo_mirror_maxvisota']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_laitovo_mirror_maxvisota']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('$model->fd_laitovo_mirror_maxvisota')])
                               .Html::tag('label',$model->fd_laitovo_mirror_maxvisota),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Свойства (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_mirror_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_mirror_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fd_laitovo_mirror_maxdlina_b',
                    'format' => 'html',
                    'value' => $model->fd_laitovo_mirror_maxdlina_b,
                ],
                [
                    'attribute' => 'fd_laitovo_mirror_maxvisota_b',
                    'format' => 'html',
                    'value' => $model->fd_laitovo_mirror_maxvisota_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_mirror_scheme != $model->fd_laitovo_mirror_scheme_b): ?>
    <h5>ПБ - Laitovo Вырез для зеркал <?=Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Laitovo Вырез для зеркал [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_mirror_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_mirror_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_mirror_schemem != $model->fd_laitovo_mirror_schemem_b): ?>
    <h5>ПБ - Laitovo Вырез для зеркал <?=Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Laitovo Вырез для зеркал [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_mirror_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_mirror_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>   
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_laitovo_mirror_schemet != $model->fd_laitovo_mirror_schemet_b): ?>
    <h5>ПБ - Laitovo Вырез для зеркал <?=Html::tag('i', '', ['class' => $model->fd_laitovo_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ПБ - Laitovo Вырез для зеркал [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_laitovo_mirror_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_laitovo_mirror_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Laitovo Don't Look сдвижной-->
    <?if  (isset($model->act->difference['fd_laitovo_dontlooks_status'])):?>
    <h5>ПБ - Laitovo Don't Look сдвижной <?=Html::tag('i', '', ['class' => $model->fd_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_dontlooks_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_laitovo_dontlooks_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_laitovo_dontlooks_status']) ? 'bg-warning' : ''],
                    'value' =>Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_laitovo_dontlooks_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_dontlooks_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_dontlooks_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Laitovo Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['fd_laitovo_dontlookb_status'])):?>
    <h5>ПБ -Laitovo Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->fd_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_laitovo_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_laitovo_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_laitovo_dontlookb_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_laitovo_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_laitovo_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->


    <!-- Chiko Стандарт -->
    <?if  (isset($model->act->difference['fd_chiko_standart_status'])):?>
    <h5>ПБ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->fd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_chiko_standart_natyag',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_standart_natyag']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_standart_natyag']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_standart_natyag')])
                               .Html::tag('label',$model->fd_chiko_standart_natyag),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fv_chiko_standart_natyag_b',
                    'format' => 'html',
                    'value' => $model->fv_chiko_standart_natyag_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_standart_scheme != $model->fd_chiko_standart_scheme_b): ?>
    <h5>ПБ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->fd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Chiko Стандарт [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_standart_schemem != $model->fd_chiko_standart_schemem_b): ?>
    <h5>ПБ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->fd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Chiko Стандарт [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <? if ($model->fd_chiko_standart_schemet != $model->fd_chiko_standart_schemet_b): ?>
    <h5>ПБ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->fd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ПБ - Chiko Стандарт [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_standart_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_standart_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- Chiko Укороченный -->
    <?if  (isset($model->act->difference['fd_chiko_short_status'])):?>
    <h5>ПБ - Chiko Укороченный <?=Html::tag('i', '', ['class' => $model->fd_chiko_short_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_short_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_short_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_short_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_short_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_chiko_short_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_chiko_short_natyag',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_short_natyag']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_short_natyag']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_short_natyag')])
                               .Html::tag('label',$model->fd_chiko_short_natyag),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_short_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_short_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fd_chiko_short_natyag_b',
                    'format' => 'html',
                    'value' => $model->fd_chiko_short_natyag_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_short_scheme != $model->fd_chiko_short_scheme_b): ?>
    <h5>ПБ - Chiko Укороченный <?=Html::tag('i', '', ['class' => $model->fd_chiko_short_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Chiko Укороченный [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_short_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Укороченный',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_short_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Укороченный',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_short_schemem != $model->fd_chiko_short_schemem_b): ?>
    <h5>ПБ - Chiko Укороченный <?=Html::tag('i', '', ['class' => $model->fd_chiko_short_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Chiko Укороченный [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_short_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Укороченный',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_short_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Укороченный',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>   
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_short_schemet != $model->fd_chiko_short_schemet_b): ?>
    <h5>ПБ - Chiko Укороченный <?=Html::tag('i', '', ['class' => $model->fd_chiko_short_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ПБ - Chiko Укороченный [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_short_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Укороченный',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_short_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Укороченный',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- Chiko Вырез для курящих -->
    <?if  (isset($model->act->difference['fd_chiko_smoke_status'])):?>
    <h5>ПБ - Chiko Вырез для курящих <?=Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_smoke_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_smoke_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_smoke_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_smoke_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_chiko_smoke_natyag',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_smoke_natyag']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_smoke_natyag']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_short_natyag')])
                               .Html::tag('label',$model->fd_chiko_smoke_natyag),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_smoke_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fd_chiko_smoke_natyag_b',
                    'format' => 'html',
                    'value' => $model->fd_chiko_smoke_natyag_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_smoke_scheme != $model->fd_chiko_smoke_scheme_b): ?>
    <h5>ПБ - Chiko Вырез для курящих <?=Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Chiko Вырез для курящих [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_smoke_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_smoke_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_smoke_schemem != $model->fd_chiko_smoke_schemem_b): ?>
    <h5>ПБ - Chiko Вырез для курящих <?=Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Chiko Вырез для курящих [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_smoke_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_smoke_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>   
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_smoke_schemet != $model->fd_chiko_smoke_schemet_b): ?>
    <h5>ПБ - Chiko Вырез для курящих <?=Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ПБ - Chiko Вырез для курящих [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_smoke_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_smoke_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляКурящих',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Chiko Вырез для зеркал -->
    <?if  (isset($model->act->difference['fd_chiko_smoke_status'])):?>
    <h5>ПБ - Chiko Вырез для зеркал <?=Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Свойства (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_mirror_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_mirror_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_mirror_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_mirror_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_chiko_mirror_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_chiko_mirror_maxdlina',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_mirror_maxdlina']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_mirror_maxdlina']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_mirror_maxdlina')])
                               .Html::tag('label',$model->fd_chiko_mirror_maxdlina),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_chiko_mirror_maxvisota',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_mirror_maxvisota']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_mirror_maxvisota']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_mirror_maxvisota')])
                               .Html::tag('label',$model->fd_chiko_mirror_maxvisota),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_chiko_mirror_natyag',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_mirror_natyag']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_mirror_natyag']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_mirror_natyag')])
                               .Html::tag('label',$model->fd_chiko_mirror_natyag),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Свойства (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_mirror_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_mirror_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fd_chiko_mirror_maxdlina_b',
                    'format' => 'html',
                    'value' => $model->fd_chiko_mirror_maxdlina_b,
                ],
                [
                    'attribute' => 'fd_chiko_mirror_maxvisota_b',
                    'format' => 'html',
                    'value' => $model->fd_chiko_mirror_maxvisota_b,
                ],
                [
                    'attribute' => 'fd_chiko_mirror_natyag_b',
                    'format' => 'html',
                    'value' => $model->fd_chiko_mirror_natyag_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_mirror_scheme != $model->fd_chiko_mirror_scheme_b): ?>
    <h5>ПБ - Chiko Вырез для зеркал <?=Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Chiko Вырез для зеркал [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_mirror_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_mirror_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Простые',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_mirror_schemem != $model->fd_chiko_mirror_schemem_b): ?>
    <h5>ПБ - Chiko Вырез для зеркал <?=Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ПБ - Chiko Вырез для зеркал [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_mirror_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_mirror_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Магнитные',
                'window' => 'ПБ',
            ]) ?>   
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->fd_chiko_mirror_schemet != $model->fd_chiko_mirror_schemet_b): ?>
    <h5>ПБ - Chiko Вырез для зеркал <?=Html::tag('i', '', ['class' => $model->fd_chiko_smoke_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ПБ - Chiko Вырез для зеркал [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['fd_chiko_mirror_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['fd_chiko_mirror_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'ВырезДляЗеркала',
                'type_clips' => 'Скотч',
                'window' => 'ПБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Chiko Don't Look сдвижной-->
    <?if  (isset($model->act->difference['fd_chiko_dontlooks_status'])):?>
    <h5>ПБ - Chiko Don't Look сдвижной <?=Html::tag('i', '', ['class' => $model->fd_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_dontlooks_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_dontlooks_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_dontlooks_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_dontlooks_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_dontlooks_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_dontlooks_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Chiko Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['fd_chiko_dontlookb_status'])):?>
    <h5>ПБ - Chiko Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->fd_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chiko_dontlookb_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chiko_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_chiko_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Chiko Magnet-->
    <?if  (isset($model->act->difference['fd_chikomagnet_status'])):?>
    <h5>ПБ - Chiko Magnet <?=Html::tag('i', '', ['class' => $model->fd_chikomagnet_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chikomagnet_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chikomagnet_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chikomagnet_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('fd_chikomagnet_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->fd_chikomagnet_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'fd_chikomagnet_magnitov',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['fd_chikomagnet_magnitov']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['fd_chikomagnet_magnitov']) ? 'bg-warning' : ''],
                    'value' => $model->fd_chikomagnet_magnitov,
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'fd_chikomagnet_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->fd_chikomagnet_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fd_chikomagnet_magnitov_b',
                    'format' => 'html',
                    'value' => $model->fd_chikomagnet_magnitov,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>
</div>

