<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */
?>
<h3>Продукция: ПБ</h3>
<?= $form->field($model, 'fd_date')->textInput(['maxlength' => true, 'class' => 'date_picker form-control']) ?>
<?= $form->field($model, 'fd_dlina')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'fd_visota')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'fd_magnit')->dropDownList($model->_magnit) ?>
<?= $form->field($model, 'fd_obshivka')->dropDownList($model->_obshivka) ?>
<?= $form->field($model, 'fd_hlyastik')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'fd_install_direction')->dropDownList($model->_install_direction) ?>
<?= $form->field($model, 'fd_with_recess')->dropDownList($model->_with_recess) ?>
<?= $form->field($model, 'fd_use_tape')->checkbox() ?>

<div class="form-group tabcontent <?= $model->fd_date ? '' : 'hidden' ?>">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#laitovo" aria-controls="laitovo" role="tab" data-toggle="tab">Laitovo</a>
        </li>
        <li role="presentation"><a href="#chiko" aria-controls="chiko" role="tab" data-toggle="tab">Chiko</a></li>
        <li role="presentation"><a href="#chikomagnet" aria-controls="chikomagnet" role="tab" data-toggle="tab">Chiko
                Magnet</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="laitovo">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_standart" aria-controls="laitovo_standart"
                                                              role="tab" data-toggle="tab">Стандарт</a></li>
                    <li role="presentation"><a href="#laitovo_short" aria-controls="laitovo_short" role="tab"
                                               data-toggle="tab">Укороченный</a></li>
                    <li role="presentation"><a href="#laitovo_smoke" aria-controls="laitovo_smoke" role="tab"
                                               data-toggle="tab">Вырез для курящих</a></li>
                    <li role="presentation"><a href="#laitovo_mirror" aria-controls="laitovo_mirror" role="tab"
                                               data-toggle="tab">Вырез для зеркал</a></li>
                    <li role="presentation"><a href="#laitovo_dontlooks" aria-controls="laitovo_dontlooks" role="tab"
                                               data-toggle="tab">Don't Look сдвижной</a></li>
                    <li role="presentation"><a href="#laitovo_dontlookb" aria-controls="laitovo_dontlookb" role="tab"
                                               data-toggle="tab">Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_standart">
                        <?= $form->field($model, 'fd_laitovo_standart_status')->checkbox() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_short">
                        <?= $form->field($model, 'fd_laitovo_short_status')->checkbox() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_smoke">
                        <?= $form->field($model, 'fd_laitovo_smoke_status')->checkbox() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_mirror">
                        <?= $form->field($model, 'fd_laitovo_mirror_status')->checkbox() ?>
                        <?= $form->field($model, 'fd_laitovo_mirror_maxdlina')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'fd_laitovo_mirror_maxvisota')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlooks">
                        <?= $form->field($model, 'fd_laitovo_dontlooks_status')->checkbox() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlookb">
                        <?= $form->field($model, 'fd_laitovo_dontlookb_status')->checkbox() ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chiko">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_standart" aria-controls="chiko_standart"
                                                              role="tab" data-toggle="tab">Стандарт</a></li>
                    <li role="presentation"><a href="#chiko_short" aria-controls="chiko_short" role="tab"
                                               data-toggle="tab">Укороченный</a></li>
                    <li role="presentation"><a href="#chiko_smoke" aria-controls="chiko_smoke" role="tab"
                                               data-toggle="tab">Вырез для курящих</a></li>
                    <li role="presentation"><a href="#chiko_mirror" aria-controls="chiko_mirror" role="tab"
                                               data-toggle="tab">Вырез для зеркал</a></li>
                    <li role="presentation"><a href="#chiko_dontlooks" aria-controls="chiko_dontlooks" role="tab"
                                               data-toggle="tab">Don't Look сдвижной</a></li>
                    <li role="presentation"><a href="#chiko_dontlookb" aria-controls="chiko_dontlookb" role="tab"
                                               data-toggle="tab">Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_standart">
                        <?= $form->field($model, 'fd_chiko_standart_status')->checkbox() ?>
                        <?= $form->field($model, 'fd_chiko_standart_natyag')->dropDownList($model->_natyag) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_short">
                        <?= $form->field($model, 'fd_chiko_short_status')->checkbox() ?>
                        <?= $form->field($model, 'fd_chiko_short_natyag')->dropDownList($model->_natyag) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_smoke">
                        <?= $form->field($model, 'fd_chiko_smoke_status')->checkbox() ?>
                        <?= $form->field($model, 'fd_chiko_smoke_natyag')->dropDownList($model->_natyag) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_mirror">
                        <?= $form->field($model, 'fd_chiko_mirror_status')->checkbox() ?>
                        <?= $form->field($model, 'fd_chiko_mirror_maxdlina')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'fd_chiko_mirror_maxvisota')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'fd_chiko_mirror_natyag')->dropDownList($model->_natyag) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlooks">
                        <?= $form->field($model, 'fd_chiko_dontlooks_status')->checkbox() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlookb">
                        <?= $form->field($model, 'fd_chiko_dontlookb_status')->checkbox() ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chikomagnet">
            <br>
            <?= $form->field($model, 'fd_chikomagnet_status')->checkbox() ?>
            <?= $form->field($model, 'fd_chikomagnet_magnitov')->dropDownList($model->_magnitov) ?>

        </div>
    </div>

    <br>
</div>