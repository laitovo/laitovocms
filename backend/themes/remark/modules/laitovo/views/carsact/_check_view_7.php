<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?if (
       isset($model->act->difference['bw_dlina']) 
    || isset($model->act->difference['bw_visota']) 
    || isset($model->act->difference['bw_magnit']) 
    || isset($model->act->difference['bw_obshivka']) 
    || isset($model->act->difference['bw_hlyastik']) 
    || isset($model->act->difference['bw_openwindowtrue']) 
    || isset($model->act->difference['bw_openwindow']) 
    || isset($model->act->difference['bw_simmetr'])
    || isset($model->act->difference['bw_gabarit'])
):?>
<!-- Основные харакетристики для ЗШ -->
<div class="row">
    <div class="col-md-6">
        <h5>ЗШ - Свойства (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_dlina',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_dlina']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_dlina']) ? 'bg-warning' : ''],
                    'value' =>  Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_dlina')])
                               .Html::tag('label',$model->bw_dlina),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_visota',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_visota']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_visota']) ? 'bg-warning' : ''],
                    'value' =>  Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_visota')])
                               .Html::tag('label',$model->bw_visota),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_magnit',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_magnit']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_magnit']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_magnit')])
                               .Html::tag('label',$model->bw_magnit),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_obshivka',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_obshivka']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_obshivka']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_obshivka')])
                               .Html::tag('label',$model->bw_obshivka),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_hlyastik',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_hlyastik']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_hlyastik']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_hlyastik')])
                               .Html::tag('label',$model->bw_hlyastik),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_openwindowtrue',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_openwindowtrue']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_openwindowtrue']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_openwindowtrue')])
                               .Html::tag('label',$model->bw_openwindowtrue),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_openwindow',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_openwindow']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_openwindow']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_openwindow')])
                               .Html::tag('label',$model->bw_openwindow),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_chastei',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_chastei']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_chastei']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_chastei')])
                               .Html::tag('label',$model->bw_chastei),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_simmetr',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_simmetr']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_simmetr']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_simmetr')])
                               .Html::tag('label',  Html::tag('i', '', ['class' =>  $model->bw_simmetr ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_gabarit',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_gabarit']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_gabarit']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false,
                                    ['value' => $model->getAttributeLabel('bw_gabarit')])
                               .Html::tag('label',  Html::tag('i', '', ['class' =>  $model->bw_gabarit ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
        <h5>ЗШ - Свойства (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_dlina_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_dlina_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_dlina_b']) ? 'bg-warning' : ''],
                    'value' => $model->bw_dlina_b,
                ],
                [
                    'attribute' => 'bw_visota_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_visota_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_visota_b']) ? 'bg-warning' : ''],
                    'value' => $model->bw_visota_b,
                ],
                [
                    'attribute' => 'bw_magnit_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_magnit_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_magnit_b']) ? 'bg-warning' : ''],
                    'value' => $model->bw_magnit_b,
                ],
                [
                    'attribute' => 'bw_obshivka_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_obshivka_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_obshivka_b']) ? 'bg-warning' : ''],
                    'value' => $model->bw_obshivka_b,
                ],
                [
                    'attribute' => 'bw_hlyastik_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_hlyastik_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_hlyastik_b']) ? 'bg-warning' : ''],
                    'value' => $model->bw_hlyastik_b,
                ],
                [
                    'attribute' => 'bw_openwindowtrue_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_openwindowtrue_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_openwindowtrue_b']) ? 'bg-warning' : ''],
                    'value' => $model->bw_openwindowtrue_b,
                ],
                [
                    'attribute' => 'bw_openwindow_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_openwindow_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_openwindow_b']) ? 'bg-warning' : ''],
                    'value' => $model->bw_openwindow_b,
                ],
                [
                    'attribute' => 'bw_chastei_b',
                    'format' => 'html',
                    'value' => $model->bw_chastei_b,
                ],
                [
                    'attribute' => 'bw_simmetr_b',
                    'format' => 'html',
                    'value' => $model->bw_simmetr_b === null ? null : Html::tag('i', '', ['class' => $model->bw_simmetr_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'bw_gabarit_b',
                    'format' => 'html',
                    'value' => $model->bw_gabarit_b === null ? null : Html::tag('i', '', ['class' => $model->bw_gabarit_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<!-- Продукция для ЗШ  -->
<div class="form-group">
    <!-- Laitovo Стандарт -->
    <?if  (isset($model->act->difference['bw_laitovo_standart_status'])):?>
    <h5>ЗШ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_laitovo_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_laitovo_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_laitovo_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->bw_laitovo_standart_scheme != $model->bw_laitovo_standart_scheme_b): ?>
    <h5>ЗШ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗШ - Laitovo Стандарт [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['bw_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗШ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['bw_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗШ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->bw_laitovo_standart_schemem != $model->bw_laitovo_standart_schemem_b): ?>
    <h5>ЗШ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗШ - Laitovo Стандарт [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['bw_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗШ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['bw_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗШ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <? if ($model->bw_laitovo_double_scheme != $model->bw_laitovo_double_scheme_b): ?>
    <h5>ЗШ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые (2 части)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗШ - Laitovo Стандарт [Схема креплений - простые(2 части)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['bw_chiko_double_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт 2 части',
                'type_clips' => 'Простые',
                'window' => 'ЗШ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['bw_chiko_double_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт 2 части',
                'type_clips' => 'Простые',
                'window' => 'ЗШ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->bw_laitovo_double_schemem != $model->bw_laitovo_double_schemem_b): ?>
    <h5>ЗШ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->bw_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные (2 части)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗШ - Laitovo Стандарт [Схема креплений - магнитные(2 части)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['bw_chiko_double_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт 2 части',
                'type_clips' => 'Магнитные',
                'window' => 'ЗШ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['bw_chiko_double_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт 2 части',
                'type_clips' => 'Магнитные',
                'window' => 'ЗШ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Laitovo Don't Look сдвижной-->
    <?if  (isset($model->act->difference['bw_laitovo_dontlooks_status'])):?>
    <h5>ЗШ - Laitovo Don't Look сдвижной <?=Html::tag('i', '', ['class' => $model->bw_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_laitovo_dontlooks_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_laitovo_dontlooks_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_laitovo_dontlooks_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_laitovo_dontlooks_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->bw_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_laitovo_dontlooks_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->bw_laitovo_dontlooks_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->


    <!-- Chiko Стандарт -->
    <?if  (isset($model->act->difference['bw_chiko_standart_status'])):?>
    <h5>ЗШ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->bw_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_chiko_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_chiko_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_chiko_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_chiko_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->bw_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'bw_chiko_standart_natyag',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_chiko_standart_natyag']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_chiko_standart_natyag']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_chiko_standart_natyag')])
                               .Html::tag('label',$model->bw_chiko_standart_natyag),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_chiko_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->bw_chiko_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fv_chiko_standart_natyag_b',
                    'format' => 'html',
                    'value' => $model->fv_chiko_standart_natyag_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->bw_chiko_standart_scheme != $model->bw_chiko_standart_scheme_b): ?>
    <h5>ЗШ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->bw_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗШ - Chiko Стандарт [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['bw_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗШ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['bw_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗШ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->bw_chiko_standart_schemem != $model->bw_chiko_standart_schemem_b): ?>
    <h5>ЗШ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->bw_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗШ - Chiko Стандарт [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['bw_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗШ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['bw_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗШ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->bw_chiko_double_scheme != $model->bw_chiko_double_scheme_b): ?>
    <h5>ЗШ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->bw_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые (2 части)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗШ - Chiko Стандарт [Схема креплений - простые (2 части)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['bw_chiko_double_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт 2 части',
                'type_clips' => 'Простые',
                'window' => 'ЗШ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['bw_chiko_double_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт 2 части',
                'type_clips' => 'Простые',
                'window' => 'ЗШ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->bw_chiko_double_schemem != $model->bw_chiko_double_schemem_b): ?>
    <h5>ЗШ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->bw_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные (2 части)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗШ - Chiko Стандарт [Схема креплений - магнитные (2 части)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['bw_chiko_double_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт 2 части',
                'type_clips' => 'Магнитные',
                'window' => 'ЗШ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['bw_chiko_double_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт 2 части',
                'type_clips' => 'Магнитные',
                'window' => 'ЗШ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Chiko Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['bw_chiko_dontlookb_status'])):?>
    <h5>ЗШ - Chiko Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->bw_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_chiko_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['bw_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['bw_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('bw_chiko_dontlookb_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->bw_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'bw_chiko_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->bw_chiko_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>
</div>































