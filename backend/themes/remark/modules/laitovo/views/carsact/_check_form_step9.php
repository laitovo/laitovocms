<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */

Yii::$app->view->registerJs('

    function parseitemsjson () {
        var clips =[];
        $(\'.clipsitems .item\').each(function( index, value ) {
            itemjson=$(this).find("input").serializeArray();
            var obj ={};
            $.each(itemjson, function( index1, value1 ) {
                obj[value1.name]=value1.value;
            });
            clips.push(obj);
        });
        $(\'#carscheckupdateform-clips\').val(JSON.stringify(clips));
    }

    var edititem = null;

    $("body").on("click",".newclips",function(e){
        window.edititem = null;
        $(".clipsmodalform input").val("");
        $(".clipsmodalform select").val("");
        $(".clipsmodalform").modal("show") 
    });

    $("body").on("click",".editclips",function(e){
        window.edititem = $(this).parent();
        $(".clipsmodalform input").val("");
        $(".clipsmodalform .clips-id").val($(this).parent().find("input[name=\'id\']").val());
        $(".clipsmodalform .clips-name").val($(this).parent().find("input[name=\'name\']").val());
        $(".clipsmodalform .clips-type").val($(this).parent().find("input[name=\'type\']").val());
        $(".clipsmodalform").modal("show") 
        e.preventDefault();
    });

    $("body").on("submit",".saveclips",function(e){
        $(".clipsitems .newitem input[name=\'id\']").val($(".clipsmodalform .clips-id").val());
        $(".clipsitems .newitem input[name=\'fullname\']").val($(".clipsmodalform .clips-name").val()+"/"+$(".clipsmodalform .clips-type").val());
        $(".clipsitems .newitem input[name=\'type\']").val($(".clipsmodalform .clips-type").val());
        $(".clipsitems .newitem input[name=\'name\']").val($(".clipsmodalform .clips-name").val());
        $(".clipsitems .newitem input[name=\'name\']").prev().text($(".clipsmodalform .clips-name").val()+"/"+$(".clipsmodalform .clips-type").val());

        if (window.edititem){
            $(".clipsitems .newitem").clone().insertAfter(window.edititem).removeClass("hidden newitem").addClass("item");
            window.edititem.remove();
        } else {
            $(".clipsitems .newitem").clone().appendTo(".clipsitems").removeClass("hidden newitem").addClass("item");
        }

        parseitemsjson();
        $(".clipsmodalform").modal("hide") 
        e.preventDefault();
    });

    $("body").on("click",".clipsitems .deletethisitem",function(e){
        var item = $(this).parent();
        notie.confirm("' . Yii::t('app', 'Удалить крепление?') . '", "' . Yii::t('yii', 'Yes') . '", "' . Yii::t('yii', 'No') . '", function() {
            item.remove();
            parseitemsjson();
        });
        e.preventDefault();
    });
    ', \yii\web\View::POS_END);


?>
<h3>Крепления</h3>

<div class="clipsitems">
    <div class="newitem hidden">
        <a href="#" class="editclips"></a>
        <input type="hidden" name="name">
        <input type="hidden" name="type">
        <input type="hidden" name="fullname">
        <input type="hidden" name="id">
        <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
    </div>
    <?
    $clips = Json::decode($model->clips);
    if ($clips)
        foreach ($clips as $krep):?>
            <div class="item">
                <a href="#" class="editclips"><?= Html::encode($krep['fullname']) ?></a>
                <input type="hidden" name="name" value="<?= Html::encode($krep['name']); ?>">
                <input type="hidden" name="type" value="<?= Html::encode($krep['type']); ?>">
                <input type="hidden" name="fullname" value="<?= Html::encode($krep['fullname']); ?>">
                <input type="hidden" name="id" value="<?= $krep['id'] ?>">
                <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
            </div>
        <? endforeach ?>
</div>

<?= Html::a('<i class="icon wb-plus"></i>', '#', ['class' => 'newclips btn btn-icon btn-round btn-default btn-outline']) ?>

<p>&nbsp;</p>
