<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\helpers\Json;

$map = [];
$scheme = $scheme;
if ($scheme) {
    $map = Json::decode($scheme);
}

?>

<div class="row">
    <div class="col-md-12">
        <? if ($window == 'ПФ'): ?>
            <img src="/img/fv_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 15%;top: 41%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 25%;top: 27%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 36%;top: 17%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 45%;top: 25%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 42%;top: 35%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 38%;top: 45%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 26%;top: 55%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ПБ' && $type == 'Стандарт'): ?>
            <img src="/img/fd_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 24%;top: 36%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 23%;top: 29%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 23%;top: 23%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 34%;top: 15%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 51%;top: 17%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 65%;top: 26%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 73%;top: 40%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[7]) ?>"
                   style="position: absolute;left: 59%;top: 44%;"><i
                            class="fa fa-circle <?= $map[7] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[8]) ?>"
                   style="position: absolute;left: 45%;top: 44%;"><i
                            class="fa fa-circle <?= $map[8] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[9]) ?>"
                   style="position: absolute;left: 32%;top: 44%;"><i
                            class="fa fa-circle <?= $map[9] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ПБ' && $type == 'Укороченный'): ?>
            <img src="/img/fd_u_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 25%;top: 35%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 25%;top: 26%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 25%;top: 18%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 36%;top: 15%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 48%;top: 17%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 48%;top: 39%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 36%;top: 39%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ПБ' && $type == 'ВырезДляКурящих'): ?>
            <img src="/img/fd_k_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 25%;top: 35%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 25%;top: 26%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 25%;top: 18%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 36%;top: 15%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 48%;top: 17%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 70%;top: 36%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 59%;top: 39%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[7]) ?>"
                   style="position: absolute;left: 48%;top: 39%;"><i
                            class="fa fa-circle <?= $map[7] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[8]) ?>"
                   style="position: absolute;left: 36%;top: 39%;"><i
                            class="fa fa-circle <?= $map[8] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ПБ' && $type == 'ВырезДляЗеркала'): ?>
            <img src="/img/fd_z_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 25%;top: 35%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 25%;top: 26%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 25%;top: 18%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 36%;top: 15%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 48%;top: 17%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 58%;top: 21%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 48%;top: 39%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[7]) ?>"
                   style="position: absolute;left: 36%;top: 39%;"><i
                            class="fa fa-circle <?= $map[7] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ЗБ'): ?>
            <img src="/img/rd_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 25%;top: 37%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 24.5%;top: 33%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 24%;top: 29%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 35%;top: 18%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 48%;top: 14%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 60%;top: 13%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 71%;top: 21%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[7]) ?>"
                   style="position: absolute;left: 71%;top: 27%;"><i
                            class="fa fa-circle <?= $map[7] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[8]) ?>"
                   style="position: absolute;left: 71%;top: 33%;"><i
                            class="fa fa-circle <?= $map[8] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[9]) ?>"
                   style="position: absolute;left: 63%;top: 40%;"><i
                            class="fa fa-circle <?= $map[9] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[10]) ?>"
                   style="position: absolute;left: 50%;top: 40%;"><i
                            class="fa fa-circle <?= $map[10] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[11]) ?>"
                   style="position: absolute;left: 35%;top: 40%;"><i
                            class="fa fa-circle <?= $map[11] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ЗФ' && $type == 'Стандарт'): ?>
            <img src="/img/rv_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 27%;top: 46%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 30%;top: 38%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 32%;top: 31%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 41%;top: 25%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 54%;top: 24%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 67%;top: 24%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 76%;top: 30%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[7]) ?>"
                   style="position: absolute;left: 74%;top: 39%;"><i
                            class="fa fa-circle <?= $map[7] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[8]) ?>"
                   style="position: absolute;left: 73%;top: 47%;"><i
                            class="fa fa-circle <?= $map[8] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[9]) ?>"
                   style="position: absolute;left: 64%;top: 54%;"><i
                            class="fa fa-circle <?= $map[9] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[10]) ?>"
                   style="position: absolute;left: 51%;top: 54%;"><i
                            class="fa fa-circle <?= $map[10] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[11]) ?>"
                   style="position: absolute;left: 37%;top: 54%;"><i
                            class="fa fa-circle <?= $map[11] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ЗФ' && $type == 'Треугольная'): ?>
            <img src="/img/rv_t_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 15%;top: 41%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 25%;top: 27%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 36%;top: 17%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 45%;top: 25%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 42%;top: 35%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 38%;top: 45%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 26%;top: 55%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ЗШ' && $type == 'Стандарт'): ?>
            <img src="/img/bw_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 19%;top: 34%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 18%;top: 26%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 27%;top: 16%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 37%;top: 15%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 49%;top: 15%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 62%;top: 15%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 72%;top: 16%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[7]) ?>"
                   style="position: absolute;left: 80%;top: 26%;"><i
                            class="fa fa-circle <?= $map[7] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[8]) ?>"
                   style="position: absolute;left: 79%;top: 34%;"><i
                            class="fa fa-circle <?= $map[8] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[9]) ?>"
                   style="position: absolute;left: 72%;top: 38%;"><i
                            class="fa fa-circle <?= $map[9] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[10]) ?>"
                   style="position: absolute;left: 61%;top: 38%;"><i
                            class="fa fa-circle <?= $map[10] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[11]) ?>"
                   style="position: absolute;left: 49%;top: 38%;"><i
                            class="fa fa-circle <?= $map[11] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[12]) ?>"
                   style="position: absolute;left: 37%;top: 38%;"><i
                            class="fa fa-circle <?= $map[12] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[13]) ?>"
                   style="position: absolute;left: 26%;top: 38%;"><i
                            class="fa fa-circle <?= $map[13] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
        <? if ($window == 'ЗШ' && $type == 'Стандарт 2 части'): ?>
            <img src="/img/bw_2_scheme.jpg" width="100%">
            <? if ($scheme): ?>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[0]) ?>"
                   style="position: absolute;left: 18%;top: 56%;"><i
                            class="fa fa-circle <?= $map[0] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[1]) ?>"
                   style="position: absolute;left: 18%;top: 47%;"><i
                            class="fa fa-circle <?= $map[1] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[2]) ?>"
                   style="position: absolute;left: 19%;top: 37%;"><i
                            class="fa fa-circle <?= $map[2] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[3]) ?>"
                   style="position: absolute;left: 25%;top: 32%;"><i
                            class="fa fa-circle <?= $map[3] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[4]) ?>"
                   style="position: absolute;left: 31%;top: 32%;"><i
                            class="fa fa-circle <?= $map[4] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[5]) ?>"
                   style="position: absolute;left: 38%;top: 32%;"><i
                            class="fa fa-circle <?= $map[5] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[6]) ?>"
                   style="position: absolute;left: 43%;top: 37%;"><i
                            class="fa fa-circle <?= $map[6] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[7]) ?>"
                   style="position: absolute;left: 43%;top: 47%;"><i
                            class="fa fa-circle <?= $map[7] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[8]) ?>"
                   style="position: absolute;left: 43%;top: 57%;"><i
                            class="fa fa-circle <?= $map[8] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[9]) ?>"
                   style="position: absolute;left: 38%;top: 64%;"><i
                            class="fa fa-circle <?= $map[9] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[10]) ?>"
                   style="position: absolute;left: 31%;top: 64%;"><i
                            class="fa fa-circle <?= $map[10] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[11]) ?>"
                   style="position: absolute;left: 24%;top: 64%;"><i
                            class="fa fa-circle <?= $map[11] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[12]) ?>"
                   style="position: absolute;left: 56%;top: 57%;"><i
                            class="fa fa-circle <?= $map[12] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[13]) ?>"
                   style="position: absolute;left: 56%;top: 47%;"><i
                            class="fa fa-circle <?= $map[13] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[14]) ?>"
                   style="position: absolute;left: 56%;top: 37%;"><i
                            class="fa fa-circle <?= $map[14] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[15]) ?>"
                   style="position: absolute;left: 61%;top: 32%;"><i
                            class="fa fa-circle <?= $map[15] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[16]) ?>"
                   style="position: absolute;left: 68%;top: 32%;"><i
                            class="fa fa-circle <?= $map[16] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[17]) ?>"
                   style="position: absolute;left: 74%;top: 32%;"><i
                            class="fa fa-circle <?= $map[17] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[18]) ?>"
                   style="position: absolute;left: 80%;top: 37%;"><i
                            class="fa fa-circle <?= $map[18] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[19]) ?>"
                   style="position: absolute;left: 81%;top: 47%;"><i
                            class="fa fa-circle <?= $map[19] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[20]) ?>"
                   style="position: absolute;left: 82%;top: 57%;"><i
                            class="fa fa-circle <?= $map[20] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[21]) ?>"
                   style="position: absolute;left: 75%;top: 64%;"><i
                            class="fa fa-circle <?= $map[21] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[22]) ?>"
                   style="position: absolute;left: 68%;top: 64%;"><i
                            class="fa fa-circle <?= $map[22] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-original-title="<?= $model->clips($map[23]) ?>"
                   style="position: absolute;left: 62%;top: 64%;"><i
                            class="fa fa-circle <?= $map[23] ? 'text-success' : 'text-danger hidden' ?>"
                            aria-hidden="true"></i></a>

            <? endif ?>
        <? endif ?>
    </div>
</div>
