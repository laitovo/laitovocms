<?php

use yii\helpers\Json;
use common\models\laitovo\Cars;
use common\models\laitovo\CarsAnalog;
?>


<div class="">
    <div style="width: 100%;text-align: center;"><b><?= $title ?></b></div>
    <div style="width: 100%;text-align: right;">стр.1</div>
</div>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3">ЛЕКАЛО №</td>
        <td class="s3">ЭЛЕМЕНТ</td>
        <td class="s4" colspan="10">АРТИКУЛЫ</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s5"><b style="font-size: 2.5em"><?= $after['fields']['nomerlekala'] ?></b></td>
        <td class="s5"><b style="font-size: 2.5em"><?= $element?></b></td>
        <td style='height:45px; font-size: 1.5em; font-weight: bold ;' class="s5" colspan="10">
            <?= $listcars ?>
        </td>;
    </tr>
    <!--     <? if ($analogs = Json::decode($after['analogs'], $asArray = true))
        foreach ($analogs as $analog):?>
            <tr style='height:19px;'>
                <td class="s17"><?= @Cars::findOne($analog['car'])->article ?></td>
                <td class="s17" colspan="11"><?= $analog['name'] ?> <b><?= implode(',', $analog['elements']) ?></b></td>
            </tr>
        <? endforeach ?> -->
    <tr style='height:19px;'>
        <td class="s10" colspan="6" style="background: #ddd;">УКОМПЛЕКТОВАТЬ УСТАНОВОЧНЫМ КРЮЧКОМ</td>
        <td class="s11" style="border: none;"></td>
        <td class="s11" style="border: none;"></td>
        <td class="s11" style="border: none;"></td>
        <td class="s12">ПБ <b><?= @$after['fields']['kryuchokpb'] ? 'V' : '' ?></b></td>
        <td class="s12">ЗБ <b><?= @$after['fields']['kryuchokzb'] ? 'V' : '' ?></b></td>
        <td class="s12">ЗПС <b><?= @$after['fields']['kryuchokzps'] ? 'V' : '' ?></b></td>
    </tr>
    </tbody>
</table>
<br>

<div style="width: 100%;">

    <table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
        <tbody>
        <? foreach ($clips as $key => $value): ?>
            <tr style='height:19px;text-align: left'>
                <td style="font-size: 22px;border-bottom:none;width:25%;background-color:#ddd">№</td>
                <td style="font-size: 29px;width:25%;font-weight: bold"><?= $key ?></td>
                <td style="font-size: 22px;border-bottom:none;width:25%;background-color:#ddd">шт.</td>
                <td style="font-size: 29px;width:25%;font-weight: bold"><?= $value ?></td>

            </tr>
        <? endforeach ?>
        </tbody>
    </table>

</div>

<div class="clearfix"></div>

<div style="width: 100%;">

    <table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
        <tbody>

        <?php
        $scheme = new \backend\modules\laitovo\models\CarsScheme();
        //if ($model->carArticle && @$model->json('items')[0]['car'] && ($car = Cars::find()->where(['article' => $model->carArticle])->one()) != null) {
        $scheme->model = new \backend\modules\laitovo\models\CarsForm($model->car_id);
        //}
        ?>

        <?php if ($element == 'ПФ') : ?>
            <tr>
                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'Стандарт';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ПФ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>
            </tr>
        <?php elseif ($element == 'ПБ') : ?>

            <tr>
                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'Стандарт';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ПБ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>

                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'Укороченный';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ПБ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>
            </tr>

            <tr>
                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'ВырезДляКурящих';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ПБ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>

                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'ВырезДляЗеркала';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ПБ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>
            </tr>

        <?php elseif ($element == 'ЗБ') : ?>
            <tr>
                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'Стандарт';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ЗБ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>
            </tr>

        <?php elseif ($element == 'ЗФ') : ?>

            <tr>
                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'Стандарт';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ЗФ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>

                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'Треугольная';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ЗФ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td>';
                ?>
            </tr>
        <?php elseif ($element == 'ЗШ') : ?>

            <tr>
                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'Стандарт';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ЗШ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>

                <?php
                $scheme->brand = 'Laitovo';
                $scheme->type = 'Стандарт 2 части';
                $scheme->type_clips = 'Простые';
                $scheme->window = 'ЗШ';
                echo '<td><img width="50%" src="' . $scheme->file(). '"></td> ';
                ?>
            </tr>
        <?endif;?>

        </tbody>
    </table>

</div>

<div class="clearfix"></div>

<br>
<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>

    <tr style='height:19px;background: #ddd;'>
        <td class="s3" colspan="12">ПОДПИСИ ОТВЕТСТВЕННЫХ</td>
    </tr>
    <tr style='height:15px;'>
        <td class="s7" style="border: none;">&nbsp;</td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
    </tr>
    <tr style='height:25px;'>
        <td class="s31">РАЗРАБОТАЛ</td>
        <td class="s20" style="border: none;"></td>
        <td class="s8" colspan="6" style="width: 50mm"></td>
        <td class="s20" style="border: none;"></td>
        <td class="s8" colspan="2"></td>
        <td class="s32" style="border: none;"></td>
    </tr>
    <tr style='height:16px;'>
        <td class="s32" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s7" colspan="6" style="border: none;">ФИО</td>
        <td class="s9" style="border: none;"></td>
        <td class="s7" colspan="2" style="border: none;">подпись</td>
        <td class="s32" style="border: none;"></td>
    </tr>
    <tr style='height:25px;'>
        <td class="s31">УТВЕРЖДАЮ</td>
        <td class="s20" style="border: none;"></td>
        <td class="s8" colspan="6"></td>
        <td class="s20" style="border: none;"></td>
        <td class="s8" colspan="2"></td>
        <td class="s32" style="border: none;"></td>
    </tr>
    <tr style='height:16px;'>
        <td class="s32" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s7" colspan="6" style="border: none;">ФИО</td>
        <td class="s9" style="border: none;"></td>
        <td class="s7" colspan="2" style="border: none;">подпись</td>
        <td class="s32" style="border: none;"></td>
    </tr>
    </tbody>
</table>