<?php

use yii\helpers\Json;
use common\models\laitovo\Cars;

?>

<!doctype html>
<html>
<body>

<div class="">
    <div style="width: 100%;text-align: center;"><b><?= $title ?></b></div>
    <div style="width: 100%;text-align: center;"><h4>ЧЕК-ЛИСТ ЛЕКАЛА</h4></div>
</div>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:19px;background: #ddd;'>
        <td class="s2">АРТ.</td>
        <td class="s2" colspan="11">НАИМЕНОВАНИЕ АВТО</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s5"><b><?= $after['article'] ?></b></td>
        <td class="s5" colspan="11"><b><?= $after['auto']['name'] ?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s5" colspan="12">ИНФОРМАЦИЯ ОБ АНАЛОГАХ</td>
    </tr>
    <? if ($analogs = Json::decode($after['analogs'], $asArray = true))
        foreach ($analogs as $analog):?>
            <tr style='height:19px;'>
                <td class="s17"><?= @Cars::findOne($analog['car'])->article ?></td>
                <td class="s17" colspan="11"><?= $analog['name'] ?> <b><?= implode(',', $analog['elements']) ?></b></td>
            </tr>
        <? endforeach ?>
    <tr style='height:19px;background: #ddd;'>
        <td class="s7" colspan="2">№ лекала / сторона</td>
        <td class="s7" colspan="5">максимальный размер выреза для зеркала</td>
        <td class="s2" colspan="5">симметричность ЗШ</td>
    </tr>
    <tr style='height:65px;'>
        <td class="s4" colspan="2"><b><?= $after['fields']['nomerlekala'] ?></b></td>
        <td class="s4" colspan="5">
            <b><? if ($after['fd_laitovo_mirror_maxdlina'] && $after['fd_laitovo_mirror_maxvisota']): ?><?= $after['fd_laitovo_mirror_maxdlina'] ?> x <?= $after['fd_laitovo_mirror_maxvisota'] ?><? endif ?></b>
        </td>
        <td class="s4" colspan="5"><b><?= @$after['bw_simmetr'] ? 'V' : '' ?></b></td>
    </tr>
    <tr style='height:54px;background: #ddd;'>
        <td class="s8" colspan="6">обшивка оконного проема (накладная \ пласик \ металл \ резина \ комбинированная)</td>
        <td class="s8" colspan="3">Хлястик</td>
        <td class="s8" colspan="3">возможность установки магнитных держателей да / нет</td>
    </tr>
    <tr style='height:32px;'>
        <td class="s9" colspan="2" style="background: #ddd;">ПБ</td>
        <td class="s4" colspan="4"><b><?= $after['fd_obshivka'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['fd_hlyastik'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['fd_magnit'] ?></b></td>
    </tr>
    <tr style='height:32px;'>
        <td class="s9" colspan="2" style="background: #ddd;">ЗБ</td>
        <td class="s4" colspan="4"><b><?= $after['rd_obshivka'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['rd_hlyastik'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['rd_magnit'] ?></b></td>
    </tr>
    <tr style='height:32px;'>
        <td class="s9" colspan="2" style="background: #ddd;">ЗФ</td>
        <td class="s4" colspan="4"><b><?= $after['rv_obshivka'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['rv_hlyastik'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['rv_magnit'] ?></b></td>
    </tr>
    <tr style='height:32px;'>
        <td class="s9" colspan="2" style="background: #ddd;">ЗШ</td>
        <td class="s4" colspan="4"><b><?= $after['bw_obshivka'] ?></b></td>
        <td class="s4" colspan="3"></td>
        <td class="s4" colspan="3"><b><?= $after['bw_magnit'] ?></b></td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s10" colspan="12">ВЫСОТА (для оклейки)</td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s8" colspan="3">ПБ</td>
        <td class="s8" colspan="3">ЗБ</td>
        <td class="s8" colspan="3">ЗФ</td>
        <td class="s8" colspan="3">ЗШ</td>
    </tr>
    <tr style='height:32px;'>
        <td class="s4" colspan="3"><b><?= $after['fields']['visotapb'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['fields']['visotazb'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['fields']['visotazf'] ?></b></td>
        <td class="s4" colspan="3"><b><?= $after['fields']['visotazs'] ?></b></td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s10" colspan="12">ДЛИНА (проволоки)</td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s11" colspan="2"></td>
        <td class="s8" colspan="2">ПФ</td>
        <td class="s8" colspan="2">ПБ</td>
        <td class="s8" colspan="2">ЗБ</td>
        <td class="s8" colspan="2">ЗФ</td>
        <td class="s8" colspan="2">ЗШ</td>
    </tr>
    <tr style='height:32px;'>
        <td class="s8" colspan="2" style="background: #ddd;">1 часть</td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"><b><?= $after['fields']['dlinapb'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['fields']['dlinazb'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['fields']['dlinazf'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['fields']['dlinazs'] ?></b></td>
    </tr>
    <tr style='height:32px;'>
        <td class="s9" colspan="2" style="background: #ddd;">2 часть</td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"><b><?= $after['fields']['dlinapb1'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['fields']['dlinazb1'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['fields']['dlinazf1'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['fields']['dlinazs1'] ?></b></td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s10" colspan="12">ГАБАРИТЫ (для логистики)</td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s5" colspan="2"></td>
        <td class="s8" colspan="2">ПФ</td>
        <td class="s8" colspan="2">ПБ</td>
        <td class="s8" colspan="2">ЗБ</td>
        <td class="s8" colspan="2">ЗФ</td>
        <td class="s8" colspan="2">ЗШ</td>
    </tr>
    <tr style='height:32px;'>
        <td class="s8" colspan="2" style="background: #ddd;">длина</td>
        <td class="s4" colspan="2"><b><?= $after['fv_dlina'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['fd_dlina'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['rd_dlina'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['rv_dlina'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['bw_dlina'] ?></b></td>
    </tr>
    <tr style='height:32px;'>
        <td class="s9" colspan="2" style="background: #ddd;">высота</td>
        <td class="s4" colspan="2"><b><?= $after['fv_visota'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['fd_visota'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['rd_visota'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['rv_visota'] ?></b></td>
        <td class="s4" colspan="2"><b><?= $after['bw_visota'] ?></b></td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s10" colspan="12">ОЦИФРОВКА ЛЕКАЛА</td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s11" colspan="2"></td>
        <td class="s8" colspan="2">ПФ</td>
        <td class="s8" colspan="2">ПБ</td>
        <td class="s8" colspan="2">ЗБ</td>
        <td class="s8" colspan="2">ЗФ</td>
        <td class="s8" colspan="2">ЗШ</td>
    </tr>
    <tr style='height:32px;'>
        <td class="s4" colspan="2" style="background: #ddd;">1 часть</td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
    </tr>
    <tr style='height:32px;'>
        <td class="s4" colspan="2" style="background: #ddd;">2 часть</td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s10" colspan="12">УДАЛЕНИЕ СТАРОГО КОНТУРА</td>
    </tr>
    <tr style='height:32px;background: #ddd;'>
        <td class="s11" colspan="2"></td>
        <td class="s8" colspan="2">ПФ</td>
        <td class="s8" colspan="2">ПБ</td>
        <td class="s8" colspan="2">ЗБ</td>
        <td class="s8" colspan="2">ЗФ</td>
        <td class="s8" colspan="2">ЗШ</td>
    </tr>
    <tr style='height:32px;'>
        <td class="s4" colspan="2" style="background: #ddd;">1 часть</td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
    </tr>
    <tr style='height:32px;'>
        <td class="s4" colspan="2" style="background: #ddd;">2 часть</td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s12" colspan="12" style="border: none;">&nbsp;</td>
    </tr>
    <tr style='height:32px;'>
        <td class="s14" colspan="2" style="border: none;">Дата заполнения</td>
        <td class="s4"></td>
        <td class="s4" colspan="2"></td>
        <td class="s4" colspan="2"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s15" style="border: none;">число</td>
        <td class="s15" colspan="2" style="border: none;">месяц</td>
        <td class="s15" colspan="2" style="border: none;">год</td>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s12" colspan="12" style="border: none;">&nbsp;</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s12" colspan="12" style="border: none;">&nbsp;</td>
    </tr>
    <tr style='height:32px;'>
        <td class="s14" colspan="2" style="border: none;">ОТВЕТСТВЕННЫЙ</td>
        <td class="s4" colspan="5"></td>
        <td class="s16" style="border: none;"></td>
        <td class="s4" colspan="3"></td>
        <td class="s12" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s12" style="border: none;"></td>
        <td class="s12" style="border: none;"></td>
        <td class="s15" colspan="5" style="border: none;">ФИО</td>
        <td class="s12" style="border: none;"></td>
        <td class="s15" colspan="3" style="border: none;">подпись</td>
        <td class="s12" style="border: none;"></td>
    </tr>
    </tbody>
</table>

</body>
</html>
