<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\CarsAct */

$this->title = Yii::t('app', 'Акт освоения {car}', ['car' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Автомобили'), 'url' => ['cars/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Акты освоения / внесеня изменений'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => $model->id ? Html::a($model->name, ['cars/update', 'id' => $model->id]) : null,
        ],
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => $model->author ? $model->author->name : null,
        ],
    ],
]) ?>
<!-- <div class="btn-group">
    <button type="button" class="btn btn-outline btn-info btn-round dropdown-toggle" data-toggle="dropdown">Печать<span
                class="caret"></span></button>
    <ul class="dropdown-menu" role="menu">
        <li><?= Html::a(Yii::t('app', 'Акт освоения'), ['actos', 'id' => $model->id], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №1'), ['actos', 'id' => $model->id, 'p' => '1'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №2'), ['actos', 'id' => $model->id, 'p' => '2'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №3'), ['actos', 'id' => $model->id, 'p' => '3'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №4'), ['actos', 'id' => $model->id, 'p' => '4'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №5'), ['actos', 'id' => $model->id, 'p' => '5'], ['target' => '_blank']) ?></li>
    </ul>
</div> -->


<h5>Внесение изменений</h5>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'created_at',
            'format' => 'datetime',
            'label' => Yii::t('app', 'Дата'),
        ],

        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return '
                        <div class="btn-group">
                            <button type="button" class="btn btn-outline btn-info btn-round dropdown-toggle" data-toggle="dropdown">Печать<span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li>' . Html::a(Yii::t('app', 'Акт внесеня изменений'), ['act', 'id' => $model->id], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №1'), ['act', 'id' => $model->id, 'p' => '1'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №2'), ['act', 'id' => $model->id, 'p' => '2'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №3'), ['act', 'id' => $model->id, 'p' => '3'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №4'), ['act', 'id' => $model->id, 'p' => '4'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №5'), ['act', 'id' => $model->id, 'p' => '5'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №6'), ['act', 'id' => $model->id, 'p' => '6'], ['target' => '_blank']) . '</li>
                            </ul>
                        </div>';
                },
            ],

        ],
    ],
]); ?>


