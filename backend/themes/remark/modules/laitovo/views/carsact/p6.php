<?php

use yii\helpers\Json;
use common\models\laitovo\Cars;

$elements = ['ПШ', 'ПФ', 'ПБ', 'ЗБ', 'ЗФ', 'ЗШ'];

$kontur = [];
$scheme = [];
if (
    $model->json('korect_fw')
    || $model->json('korect_pk')
) $kontur[] = 'ПШ';
if (
    $model->json('korect_fv')
    || $model->json('korect_pk')
) $kontur[] = 'ПФ';
if (
    $before['fv_laitovo_standart_scheme'] != $after['fv_laitovo_standart_scheme']
    || $before['fv_laitovo_standart_schemem'] != $after['fv_laitovo_standart_schemem']
    || $before['fv_chiko_standart_scheme'] != $after['fv_chiko_standart_scheme']
    || $before['fv_chiko_standart_schemem'] != $after['fv_chiko_standart_schemem']
) $scheme[] = 'ПФ';
if (
    $model->json('korect_fd')
    || $model->json('korect_pk')
) $kontur[] = 'ПБ';
if (
    $before['fd_laitovo_standart_scheme'] != $after['fd_laitovo_standart_scheme']
    || $before['fd_laitovo_standart_schemem'] != $after['fd_laitovo_standart_schemem']
    || $before['fd_chiko_standart_scheme'] != $after['fd_chiko_standart_scheme']
    || $before['fd_chiko_standart_schemem'] != $after['fd_chiko_standart_schemem']
    || $before['fd_laitovo_short_scheme'] != $after['fd_laitovo_short_scheme']
    || $before['fd_laitovo_short_schemem'] != $after['fd_laitovo_short_schemem']
    || $before['fd_chiko_short_scheme'] != $after['fd_chiko_short_scheme']
    || $before['fd_chiko_short_schemem'] != $after['fd_chiko_short_schemem']
    || $before['fd_laitovo_smoke_scheme'] != $after['fd_laitovo_smoke_scheme']
    || $before['fd_laitovo_smoke_schemem'] != $after['fd_laitovo_smoke_schemem']
    || $before['fd_chiko_smoke_scheme'] != $after['fd_chiko_smoke_scheme']
    || $before['fd_chiko_smoke_schemem'] != $after['fd_chiko_smoke_schemem']
    || $before['fd_laitovo_mirror_scheme'] != $after['fd_laitovo_mirror_scheme']
    || $before['fd_laitovo_mirror_schemem'] != $after['fd_laitovo_mirror_schemem']
    || $before['fd_chiko_mirror_scheme'] != $after['fd_chiko_mirror_scheme']
    || $before['fd_chiko_mirror_schemem'] != $after['fd_chiko_mirror_schemem']
) $scheme[] = 'ПБ';
if (
    $model->json('korect_rd')
    || $model->json('korect_pk')
) $kontur[] = 'ЗБ';
if (
    $before['rd_laitovo_standart_scheme'] != $after['rd_laitovo_standart_scheme']
    || $before['rd_laitovo_standart_schemem'] != $after['rd_laitovo_standart_schemem']
    || $before['rd_chiko_standart_scheme'] != $after['rd_chiko_standart_scheme']
    || $before['rd_chiko_standart_schemem'] != $after['rd_chiko_standart_schemem']
) $scheme[] = 'ЗБ';
if (
    $model->json('korect_rv')
    || $model->json('korect_pk')
) $kontur[] = 'ЗФ';
if (
    $before['rv_laitovo_standart_scheme'] != $after['rv_laitovo_standart_scheme']
    || $before['rv_laitovo_standart_schemem'] != $after['rv_laitovo_standart_schemem']
    || $before['rv_chiko_standart_scheme'] != $after['rv_chiko_standart_scheme']
    || $before['rv_chiko_standart_schemem'] != $after['rv_chiko_standart_schemem']
    || $before['rv_laitovo_triangul_scheme'] != $after['rv_laitovo_triangul_scheme']
    || $before['rv_laitovo_triangul_schemem'] != $after['rv_laitovo_triangul_schemem']
    || $before['rv_chiko_triangul_scheme'] != $after['rv_chiko_triangul_scheme']
    || $before['rv_chiko_triangul_schemem'] != $after['rv_chiko_triangul_schemem']
) $scheme[] = 'ЗФ';
if (
    $model->json('korect_bw')
    || $model->json('korect_pk')
) $kontur[] = 'ЗШ';
if (
    $before['bw_laitovo_standart_scheme'] != $after['bw_laitovo_standart_scheme']
    || $before['bw_laitovo_standart_schemem'] != $after['bw_laitovo_standart_schemem']
    || $before['bw_chiko_standart_scheme'] != $after['bw_chiko_standart_scheme']
    || $before['bw_chiko_standart_schemem'] != $after['bw_chiko_standart_schemem']
    || $before['bw_laitovo_double_scheme'] != $after['bw_laitovo_double_scheme']
    || $before['bw_laitovo_double_schemem'] != $after['bw_laitovo_double_schemem']
    || $before['bw_chiko_double_scheme'] != $after['bw_chiko_double_scheme']
    || $before['bw_chiko_double_schemem'] != $after['bw_chiko_double_schemem']
) $scheme[] = 'ЗШ';


?>

<!doctype html>
<html>
<body>

<div class="">
    <div style="width: 100%;text-align: center;"><b><?= $title ?></b></div>
    <div style="width: 100%;text-align: right;">стр.1</div>
    <div style="width: 100%;text-align: center;"><h4>СЛУЖЕБНАЯ ЗАПИСКА</h4></div>
    <div style="width: 100%;text-align: center;"><p>В связи с внесением изменений в действующее лекало начальнику склада
            готовой продукции предоставить на проверку ОТК всю имеющуюся на складах продукцию в соответствии со списком
            ниже:</p></div>
</div>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:44px;background: #ddd;'>
        <td class="s9">арт.</td>
        <td class="s9">наименование авто</td>
        <td class="s9" colspan="2">элемент(ы)</td>
        <td class="s10" colspan="2">вид корректировки</td>
        <td class="s9" colspan="3">решение ТС Россия</td>
        <td class="s9" colspan="3">решение ТС Европа</td>
    </tr>

    <?
    $elem = [];
    foreach ($elements as $key => $value) {
        if (in_array($value, $kontur) && in_array($value, $scheme)) {
            $elem['контур / схема'][$value] = $value;
        } elseif (in_array($value, $kontur)) {
            $elem['контур'][$value] = $value;
        } elseif (in_array($value, $scheme)) {
            $elem['схема'][$value] = $value;
        }
    }
    ?>
    <? foreach ($elem as $key => $value): ?>
        <tr style='height:32px;'>
            <td class="s11"><?= $after['article'] ?></td>
            <td class="s12"><?= $after['auto']['name'] ?></td>

            <td class="s11" colspan="2">
                <?= implode(',', $value) ?>
            </td>
            <td class="s11" colspan="2">
                <?= $key ?>
            </td>
            <td class="s11" colspan="3"></td>
            <td class="s11" colspan="3"></td>
        </tr>
    <? endforeach ?>


    <? if ($analogs = Json::decode($after['analogs'], $asArray = true))
        foreach ($analogs as $analog):

            $elem = [];
            foreach ($analog['elements'] as $key => $value) {
                if (in_array($value, $kontur) && in_array($value, $scheme)) {
                    $elem['контур / схема'][$value] = $value;
                } elseif (in_array($value, $kontur)) {
                    $elem['контур'][$value] = $value;
                } elseif (in_array($value, $scheme)) {
                    $elem['схема'][$value] = $value;
                }
            }

            foreach ($elem as $key => $value):
                ?>

                <tr style='height:19px;'>

                    <td class="s11"><?= @Cars::findOne($analog['car'])->article ?></td>
                    <td class="s12"><?= $analog['name'] ?></td>
                    <td class="s11" colspan="2">
                        <?= implode(',', $value) ?>
                    </td>
                    <td class="s11" colspan="2">
                        <?= $key ?>
                    </td>
                    <td class="s11" colspan="3"></td>
                    <td class="s11" colspan="3"></td>
                </tr>
            <? endforeach ?>
        <? endforeach ?>

    <tr style='height:28px;'>
        <td class="s8" style="border: none;" colspan="12">&nbsp;</td>
    </tr>
    <tr style='height:28px;'>
        <td class="s8" style="border: none;" colspan="12">&nbsp;</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s12" colspan="2">МАСТЕР ТЕХНИЧЕСКОГО ОТДЕЛА</td>
        <td class="s16" style="border: none;"></td>
        <td class="s17" colspan="4">Авсеенко А.В.</td>
        <td class="s16" style="border: none;"></td>
        <td class="s14" colspan="4"></td>
    </tr>
    <tr style='height:17px;'>
        <td class="s14" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s18" colspan="4" style="border: none;">ФИО</td>
        <td class="s15" style="border: none;"></td>
        <td class="s8" colspan="4" style="border: none;">подпись</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s12" colspan="2">НАЧАЛЬНИК СЛУЖБЫ ТБ</td>
        <td class="s16" style="border: none;"></td>
        <td class="s17" colspan="4">Ширшов А.Е.</td>
        <td class="s16" style="border: none;"></td>
        <td class="s14" colspan="4"></td>
    </tr>
    <tr style='height:17px;'>
        <td class="s14" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s18" colspan="4" style="border: none;">ФИО</td>
        <td class="s15" style="border: none;"></td>
        <td class="s8" colspan="4" style="border: none;">подпись</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s12" colspan="2">НАЧАЛЬНИК ОТДЕЛА ЛОГИСТИКИ</td>
        <td class="s16" style="border: none;"></td>
        <td class="s17" colspan="4">Жуковская Е.И.</td>
        <td class="s16" style="border: none;"></td>
        <td class="s14" colspan="4"></td>
    </tr>
    <tr style='height:16px;'>
        <td class="s14" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s18" colspan="4" style="border: none;">ФИО</td>
        <td class="s15" style="border: none;"></td>
        <td class="s8" colspan="4" style="border: none;">подпись</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s12" colspan="2">НАЧАЛЬНИК СКЛАДА ГП</td>
        <td class="s16" style="border: none;"></td>
        <td class="s11" colspan="4">Галафеева Е.</td>
        <td class="s16" style="border: none;"></td>
        <td class="s14" colspan="4"></td>
    </tr>
    <tr style='height:16px;'>
        <td class="s14" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s18" colspan="4" style="border: none;">ФИО</td>
        <td class="s15" style="border: none;"></td>
        <td class="s8" colspan="4" style="border: none;">подпись</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s12" colspan="2">НАЧАЛЬНИК ЕВРОПЕЙСКОГО ОТДЕЛА</td>
        <td class="s16" style="border: none;"></td>
        <td class="s17" colspan="4">Рогалев Р.В.</td>
        <td class="s16" style="border: none;"></td>
        <td class="s14" colspan="4"></td>
    </tr>
    <tr style='height:16px;'>
        <td class="s15" style="border: none;"></td>
        <td class="s15" style="border: none;"></td>
        <td class="s15" style="border: none;"></td>
        <td class="s19" colspan="4" style="border: none;">ФИО</td>
        <td class="s15" style="border: none;"></td>
        <td class="s4" colspan="4" style="border: none;">подпись</td>
    </tr>
    <tr style='height:25px;'>
        <td class="s12" colspan="2">ДИРЕКТОР ПО РАЗВИТИЮ</td>
        <td class="s16" style="border: none;"></td>
        <td class="s17" colspan="4">Антонов Д.Л.</td>
        <td class="s16" style="border: none;"></td>
        <td class="s14" colspan="4"></td>
    </tr>
    <tr style='height:16px;'>
        <td class="s15" style="border: none;"></td>
        <td class="s15" style="border: none;"></td>
        <td class="s15" style="border: none;"></td>
        <td class="s19" colspan="4" style="border: none;">ФИО</td>
        <td class="s15" style="border: none;"></td>
        <td class="s4" colspan="4" style="border: none;">подпись</td>
    </tr>
    </tbody>
</table>
</body>
</html>
