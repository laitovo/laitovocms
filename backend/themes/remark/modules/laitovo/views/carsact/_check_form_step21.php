<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */

$scheme = Json::decode($model->fw_scheme);

Yii::$app->view->registerJs('

    function parseitemsjson1 () {
        var scheme =[];
        $(\'.schemeitems1 input\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-fw_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("keyup",".schemeitems1 input",function(e){
        parseitemsjson1();
        e.preventDefault();
    });


    ', \yii\web\View::POS_END);


?>
<h3>ПШ: Замеры</h3>

<div class="row">
    <div class="col-md-12 schemeitems1">

        <img src="/img/act/image00.jpg" width="100%">

        <div style="position: absolute;left: 29%;top: 86%;"><?= Html::input('text', null, $scheme[0], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 28%;top: 62%;"><?= Html::input('text', null, $scheme[1], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 21%;top: 3%;"><?= Html::input('text', null, $scheme[2], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 44%;top: 11%;"><?= Html::input('text', null, $scheme[3], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 43%;top: 1%;"><?= Html::input('text', null, $scheme[4], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 51%;top: 7%;"><?= Html::input('text', null, $scheme[5], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 47%;top: 58%;"><?= Html::input('text', null, $scheme[6], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 71%;top: 53%;"><?= Html::input('text', null, $scheme[7], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 51%;top: 64%;"><?= Html::input('text', null, $scheme[8], ['style' => 'width:60px']) ?></div>
        <div style="position: absolute;left: 83%;top: 3%;"><?= Html::input('text', null, @$scheme[9], ['style' => 'width:60px']) ?></div>


    </div>
</div>

