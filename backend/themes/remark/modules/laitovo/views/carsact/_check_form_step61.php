<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */

$clips = Json::decode($model->clips);
$scheme1 = Json::decode($model->rv_laitovo_standart_scheme);
$scheme2 = Json::decode($model->rv_laitovo_standart_schemem);
$scheme3 = Json::decode($model->rv_chiko_standart_scheme);
$scheme4 = Json::decode($model->rv_chiko_standart_schemem);

Yii::$app->view->registerJs('

    function parseitemsjson1 () {
        var scheme =[];
        $(\'.schemeitems1 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-rv_laitovo_standart_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems1 select",function(e){
        parseitemsjson1();
        e.preventDefault();
    });


    function parseitemsjson2 () {
        var scheme =[];
        $(\'.schemeitems2 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-rv_laitovo_standart_schemem\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems2 select",function(e){
        parseitemsjson2();
        e.preventDefault();
    });


    function parseitemsjson3 () {
        var scheme =[];
        $(\'.schemeitems3 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-rv_chiko_standart_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems3 select",function(e){
        parseitemsjson3();
        e.preventDefault();
    });


    function parseitemsjson4 () {
        var scheme =[];
        $(\'.schemeitems4 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-rv_chiko_standart_schemem\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems4 select",function(e){
        parseitemsjson4();
        e.preventDefault();
    });

    ', \yii\web\View::POS_END);


?>
<h3>ЗФ: Стандарт</h3>
<div class="form-group tabcontent">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#laitovo" aria-controls="laitovo" role="tab" data-toggle="tab">Laitovo</a>
        </li>
        <li role="presentation"><a href="#chiko" aria-controls="chiko" role="tab" data-toggle="tab">Chiko</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="laitovo">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_standart" aria-controls="laitovo_standart"
                                                              role="tab" data-toggle="tab">Простые</a></li>
                    <li role="presentation"><a href="#laitovo_magnit" aria-controls="laitovo_magnit" role="tab"
                                               data-toggle="tab">Магнитные</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_standart">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems1">

                                <img src="/img/rv_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme1[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 24%;top: 47%;', 'id' => 'rv_laitovo_standart_scheme_0']) ?>
                                <?= Html::dropDownList(null, @$scheme1[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 26%;top: 39%;', 'id' => 'rv_laitovo_standart_scheme_1']) ?>
                                <?= Html::dropDownList(null, @$scheme1[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 29%;top: 32%;', 'id' => 'rv_laitovo_standart_scheme_2']) ?>
                                <?= Html::dropDownList(null, @$scheme1[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 40%;top: 26%;', 'id' => 'rv_laitovo_standart_scheme_3']) ?>
                                <?= Html::dropDownList(null, @$scheme1[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 53%;top: 26%;', 'id' => 'rv_laitovo_standart_scheme_4']) ?>
                                <?= Html::dropDownList(null, @$scheme1[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 67%;top: 26%;', 'id' => 'rv_laitovo_standart_scheme_5']) ?>
                                <?= Html::dropDownList(null, @$scheme1[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 76%;top: 31%;', 'id' => 'rv_laitovo_standart_scheme_6']) ?>
                                <?= Html::dropDownList(null, @$scheme1[7], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 75%;top: 40%;', 'id' => 'rv_laitovo_standart_scheme_7']) ?>
                                <?= Html::dropDownList(null, @$scheme1[8], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 74%;top: 48%;', 'id' => 'rv_laitovo_standart_scheme_8']) ?>
                                <?= Html::dropDownList(null, @$scheme1[9], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 63%;top: 55%;', 'id' => 'rv_laitovo_standart_scheme_9']) ?>
                                <?= Html::dropDownList(null, @$scheme1[10], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 50%;top: 55%;', 'id' => 'rv_laitovo_standart_scheme_10']) ?>
                                <?= Html::dropDownList(null, @$scheme1[11], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 35%;top: 55%;', 'id' => 'rv_laitovo_standart_scheme_11']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_magnit">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems2">

                                <img src="/img/rv_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme2[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 24%;top: 47%;', 'id' => 'rv_laitovo_standart_schemem_0']) ?>
                                <?= Html::dropDownList(null, @$scheme2[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 26%;top: 39%;', 'id' => 'rv_laitovo_standart_schemem_1']) ?>
                                <?= Html::dropDownList(null, @$scheme2[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 29%;top: 32%;', 'id' => 'rv_laitovo_standart_schemem_2']) ?>
                                <?= Html::dropDownList(null, @$scheme2[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 40%;top: 26%;', 'id' => 'rv_laitovo_standart_schemem_3']) ?>
                                <?= Html::dropDownList(null, @$scheme2[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 53%;top: 26%;', 'id' => 'rv_laitovo_standart_schemem_4']) ?>
                                <?= Html::dropDownList(null, @$scheme2[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 67%;top: 26%;', 'id' => 'rv_laitovo_standart_schemem_5']) ?>
                                <?= Html::dropDownList(null, @$scheme2[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 76%;top: 31%;', 'id' => 'rv_laitovo_standart_schemem_6']) ?>
                                <?= Html::dropDownList(null, @$scheme2[7], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 75%;top: 40%;', 'id' => 'rv_laitovo_standart_schemem_7']) ?>
                                <?= Html::dropDownList(null, @$scheme2[8], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 74%;top: 48%;', 'id' => 'rv_laitovo_standart_schemem_8']) ?>
                                <?= Html::dropDownList(null, @$scheme2[9], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 63%;top: 55%;', 'id' => 'rv_laitovo_standart_schemem_9']) ?>
                                <?= Html::dropDownList(null, @$scheme2[10], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 50%;top: 55%;', 'id' => 'rv_laitovo_standart_schemem_10']) ?>
                                <?= Html::dropDownList(null, @$scheme2[11], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 35%;top: 55%;', 'id' => 'rv_laitovo_standart_schemem_11']) ?>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chiko">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_standart" aria-controls="chiko_standart"
                                                              role="tab" data-toggle="tab">Простые</a></li>
                    <li role="presentation"><a href="#chiko_magnit" aria-controls="chiko_magnit" role="tab"
                                               data-toggle="tab">Магнитные</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_standart">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems3">

                                <img src="/img/rv_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme3[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 24%;top: 47%;', 'id' => 'rv_chiko_standart_scheme_0']) ?>
                                <?= Html::dropDownList(null, @$scheme3[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 26%;top: 39%;', 'id' => 'rv_chiko_standart_scheme_1']) ?>
                                <?= Html::dropDownList(null, @$scheme3[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 29%;top: 32%;', 'id' => 'rv_chiko_standart_scheme_2']) ?>
                                <?= Html::dropDownList(null, @$scheme3[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 40%;top: 26%;', 'id' => 'rv_chiko_standart_scheme_3']) ?>
                                <?= Html::dropDownList(null, @$scheme3[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 53%;top: 26%;', 'id' => 'rv_chiko_standart_scheme_4']) ?>
                                <?= Html::dropDownList(null, @$scheme3[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 67%;top: 26%;', 'id' => 'rv_chiko_standart_scheme_5']) ?>
                                <?= Html::dropDownList(null, @$scheme3[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 76%;top: 31%;', 'id' => 'rv_chiko_standart_scheme_6']) ?>
                                <?= Html::dropDownList(null, @$scheme3[7], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 75%;top: 40%;', 'id' => 'rv_chiko_standart_scheme_7']) ?>
                                <?= Html::dropDownList(null, @$scheme3[8], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 74%;top: 48%;', 'id' => 'rv_chiko_standart_scheme_8']) ?>
                                <?= Html::dropDownList(null, @$scheme3[9], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 63%;top: 55%;', 'id' => 'rv_chiko_standart_scheme_9']) ?>
                                <?= Html::dropDownList(null, @$scheme3[10], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 50%;top: 55%;', 'id' => 'rv_chiko_standart_scheme_10']) ?>
                                <?= Html::dropDownList(null, @$scheme3[11], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 35%;top: 55%;', 'id' => 'rv_chiko_standart_scheme_11']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_magnit">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems4">

                                <img src="/img/rv_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme4[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 24%;top: 47%;', 'id' => 'rv_chiko_standart_schemem_0']) ?>
                                <?= Html::dropDownList(null, @$scheme4[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 26%;top: 39%;', 'id' => 'rv_chiko_standart_schemem_1']) ?>
                                <?= Html::dropDownList(null, @$scheme4[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 29%;top: 32%;', 'id' => 'rv_chiko_standart_schemem_2']) ?>
                                <?= Html::dropDownList(null, @$scheme4[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 40%;top: 26%;', 'id' => 'rv_chiko_standart_schemem_3']) ?>
                                <?= Html::dropDownList(null, @$scheme4[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 53%;top: 26%;', 'id' => 'rv_chiko_standart_schemem_4']) ?>
                                <?= Html::dropDownList(null, @$scheme4[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 67%;top: 26%;', 'id' => 'rv_chiko_standart_schemem_5']) ?>
                                <?= Html::dropDownList(null, @$scheme4[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 76%;top: 31%;', 'id' => 'rv_chiko_standart_schemem_6']) ?>
                                <?= Html::dropDownList(null, @$scheme4[7], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 75%;top: 40%;', 'id' => 'rv_chiko_standart_schemem_7']) ?>
                                <?= Html::dropDownList(null, @$scheme4[8], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 74%;top: 48%;', 'id' => 'rv_chiko_standart_schemem_8']) ?>
                                <?= Html::dropDownList(null, @$scheme4[9], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 63%;top: 55%;', 'id' => 'rv_chiko_standart_schemem_9']) ?>
                                <?= Html::dropDownList(null, @$scheme4[10], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 50%;top: 55%;', 'id' => 'rv_chiko_standart_schemem_10']) ?>
                                <?= Html::dropDownList(null, @$scheme4[11], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => 'width:50px;position: absolute;left: 35%;top: 55%;', 'id' => 'rv_chiko_standart_schemem_11']) ?>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
</div>
