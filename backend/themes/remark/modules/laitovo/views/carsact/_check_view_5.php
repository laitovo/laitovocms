<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?if (
       isset($model->act->difference['rd_dlina']) 
    || isset($model->act->difference['rd_visota']) 
    || isset($model->act->difference['rd_magnit']) 
    || isset($model->act->difference['rd_obshivka']) 
    || isset($model->act->difference['rd_hlyastik'])
    || isset($model->act->difference['rd_install_direction'])
    || isset($model->act->difference['rd_with_recess'])
    || isset($model->act->difference['rd_use_tape'])
):?>

<!-- Основные харакетристики для ЗБ -->
<div class="row">
    <div class="col-md-6">
        <h5>ЗБ - Свойства (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_dlina',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_dlina']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_dlina']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rd_dlina')])
                               .Html::tag('label',$model->rd_dlina),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rd_visota',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_visota']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_visota']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rd_visota')])
                               .Html::tag('label',$model->rd_visota),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rd_magnit',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_magnit']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_magnit']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rd_magnit')])
                               .Html::tag('label',$model->rd_magnit),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rd_obshivka',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_obshivka']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_obshivka']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rd_obshivka')])
                               .Html::tag('label',$model->rd_obshivka),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rd_hlyastik',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_hlyastik']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_hlyastik']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rd_hlyastik')])
                               .Html::tag('label',$model->rd_hlyastik),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rd_install_direction',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_install_direction']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_install_direction']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                        Html::checkbox("message[]",
                            $checked = false,
                            ['value' => $model->getAttributeLabel('rd_install_direction')])
                        .Html::tag('label',$model->rd_install_direction),
                        ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),
                ],
                [
                    'attribute' => 'rd_with_recess',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_with_recess']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_with_recess']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                        Html::checkbox("message[]",
                            $checked = false,
                            ['value' => $model->getAttributeLabel('rd_with_recess')])
                        .Html::tag('label',$model->rd_with_recess),
                        ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),
                ],
                [
                    'attribute' => 'rd_use_tape',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_use_tape']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_use_tape']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                        Html::checkbox("message[]",
                            $checked = false,
                            ['value' => $model->getAttributeLabel('rd_use_tape')])
                        .Html::tag('label',$model->rd_use_tape),
                        ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),
                ],
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
        <h5>ЗБ - Свойства (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_dlina_b',
                    'format' => 'html',
                    'value' => $model->rd_dlina_b,
                ],
                [
                    'attribute' => 'rd_visota_b',
                    'format' => 'html',
                    'value' => $model->rd_visota_b,
                ],
                [
                    'attribute' => 'rd_magnit_b',
                    'format' => 'html',
                    'value' =>$model->rd_magnit_b,
                ],
                [
                    'attribute' => 'rd_obshivka_b',
                    'format' => 'html',
                    'value' => $model->rd_obshivka_b,
                ],
                [
                    'attribute' => 'rd_hlyastik_b',
                    'format' => 'html',
                    'value' => $model->rd_hlyastik_b,
                ],
                [
                    'attribute' => 'fd_install_direction_b',
                    'format' => 'html',
                    'value' => $model->fd_install_direction_b,
                ],
                [
                    'attribute' => 'fd_with_recess_b',
                    'format' => 'html',
                    'value' => $model->fd_with_recess_b,
                ],
                [
                    'attribute' => 'fd_use_tape_b',
                    'format' => 'html',
                    'value' => $model->fd_use_tape_b,
                ],
            ],
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<!-- Продукция для ЗБ  -->
<div class="form-group">
    <!-- Laitovo Стандарт -->
    <?if  (isset($model->act->difference['rd_laitovo_standart_status'])):?>
    <h5>ЗБ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_laitovo_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rd_laitovo_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->rd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_laitovo_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rd_laitovo_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rd_laitovo_standart_scheme != $model->rd_laitovo_standart_scheme_b): ?>
    <h5>ЗБ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗБ - Laitovo Стандарт [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rd_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rd_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rd_laitovo_standart_schemem != $model->rd_laitovo_standart_schemem_b): ?>
    <h5>ЗБ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗБ - Laitovo Стандарт [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rd_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rd_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rd_laitovo_standart_schemet != $model->rd_laitovo_standart_schemet_b): ?>
    <h5>ЗБ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rd_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ЗБ - Laitovo Стандарт [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rd_laitovo_standart_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Скотч',
                'window' => 'ЗБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rd_laitovo_standart_schemet'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Скотч',
                'window' => 'ЗБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Laitovo Don't Look сдвижной-->
    <?if  (isset($model->act->difference['rd_laitovo_dontlooks_status'])):?>
    <h5>ЗБ - Laitovo Don't Look сдвижной <?=Html::tag('i', '', ['class' => $model->rd_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_laitovo_dontlooks_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_laitovo_dontlooks_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_laitovo_dontlooks_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rd_laitovo_dontlooks_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->rd_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_laitovo_dontlooks_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rd_laitovo_dontlooks_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Laitovo Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['rd_laitovo_dontlookb_status'])):?>
    <h5>ЗБ - Laitovo Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->rd_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_laitovo_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_laitovo_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_laitovo_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' => 
                        Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('rd_laitovo_dontlookb_status')])
                           .Html::tag('label',Html::tag('i', '', ['class' => $model->rd_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_laitovo_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rd_laitovo_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->


    <!-- Chiko Стандарт -->
    <?if  (isset($model->act->difference['rd_chiko_standart_status'])):?>
    <h5>ЗБ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_chiko_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_chiko_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_chiko_standart_status']) ? 'bg-warning' : ''],
                    'value' =>                         
                        Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('rd_chiko_standart_status')])
                           .Html::tag('label',Html::tag('i', '', ['class' => $model->rd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rd_chiko_standart_natyag',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_chiko_standart_natyag']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_chiko_standart_natyag']) ? 'bg-warning' : ''],
                    'value' => 
                        Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('rd_chiko_standart_status')])
                           .Html::tag('label',$model->rd_chiko_standart_natyag),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_chiko_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rd_chiko_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'fv_chiko_standart_natyag_b',
                    'format' => 'html',
                    'value' => $model->fv_chiko_standart_natyag_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rd_chiko_standart_scheme != $model->rd_chiko_standart_scheme_b): ?>
    <h5>ЗБ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗБ - Chiko Стандарт [Схема креплений - простые]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rd_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rd_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rd_chiko_standart_schemem != $model->rd_chiko_standart_schemem_b): ?>
    <h5>ЗБ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗБ - Chiko Стандарт [Схема креплений - магнитные]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rd_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rd_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <? if ($model->rd_chiko_standart_schemet != $model->rd_chiko_standart_schemet_b): ?>
    <h5>ЗБ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rd_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - скотч</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false,
            ['value' => 'ЗБ - Chiko Стандарт [Схема креплений - скотч]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rd_chiko_standart_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Скотч',
                'window' => 'ЗБ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rd_chiko_standart_schemet'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Скотч',
                'window' => 'ЗБ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- Chiko Don't Look сдвижной-->
    <?if  (isset($model->act->difference['rd_chiko_dontlooks_status'])):?>
    <h5>ЗБ - Chiko Don't Look сдвижной <?=Html::tag('i', '', ['class' => $model->rd_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_chiko_dontlooks_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_chiko_dontlooks_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_chiko_dontlooks_status']) ? 'bg-warning' : ''],
                    'value' => 
                         Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('rd_chiko_dontlooks_status')])
                           .Html::tag('label',Html::tag('i', '', ['class' => $model->rd_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_chiko_dontlooks_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rd_chiko_dontlooks_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Chiko Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['rd_chiko_dontlookb_status'])):?>
    <h5>ЗБ - Chiko Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->rd_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_chiko_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' =>
                        Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('rd_chiko_dontlookb_status')])
                           .Html::tag('label',Html::tag('i', '', ['class' => $model->rd_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_chiko_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rd_chiko_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Chiko Magnet-->
    <?if  (isset($model->act->difference['rd_chikomagnet_status'])):?>
    <h5>ЗБ - Chiko Magnet <?=Html::tag('i', '', ['class' => $model->rd_chikomagnet_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_chikomagnet_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_chikomagnet_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_chikomagnet_status']) ? 'bg-warning' : ''],
                    'value' => 
                        Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('rd_chikomagnet_status')])
                           .Html::tag('label',Html::tag('i', '', ['class' => $model->rd_chikomagnet_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rd_chikomagnet_magnitov',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rd_chikomagnet_magnitov']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rd_chikomagnet_magnitov']) ? 'bg-warning' : ''],
                    'value' => 
                        Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('rd_chikomagnet_magnitov')])
                           .Html::tag('label',$model->rd_chikomagnet_magnitov),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rd_chikomagnet_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rd_chikomagnet_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'rd_chikomagnet_magnitov_b',
                    'format' => 'html',
                    'value' => $model->rd_chikomagnet_magnitov_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>
</div>

