<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */

$clips = Json::decode($model->clips);
$scheme1 = Json::decode($model->bw_laitovo_double_scheme);
$scheme2 = Json::decode($model->bw_laitovo_double_schemem);
$scheme3 = Json::decode($model->bw_chiko_double_scheme);
$scheme4 = Json::decode($model->bw_chiko_double_schemem);

Yii::$app->view->registerJs('

    function parseitemsjson1 () {
        var scheme =[];
        $(\'.schemeitems1 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-bw_laitovo_double_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems1 select",function(e){
        parseitemsjson1();
        e.preventDefault();
    });


    function parseitemsjson2 () {
        var scheme =[];
        $(\'.schemeitems2 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-bw_laitovo_double_schemem\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems2 select",function(e){
        parseitemsjson2();
        e.preventDefault();
    });


    function parseitemsjson3 () {
        var scheme =[];
        $(\'.schemeitems3 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-bw_chiko_double_scheme\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems3 select",function(e){
        parseitemsjson3();
        e.preventDefault();
    });


    function parseitemsjson4 () {
        var scheme =[];
        $(\'.schemeitems4 select\').each(function( index, value ) {
            scheme.push(value.value);
        });
        $(\'#carscheckupdateform-bw_chiko_double_schemem\').val(JSON.stringify(scheme));
    }
    $("body").on("change",".schemeitems4 select",function(e){
        parseitemsjson4();
        e.preventDefault();
    });

    ', \yii\web\View::POS_END);


?>
<h3>ЗШ: 2 части</h3>
<div class="form-group tabcontent">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#chiko" aria-controls="chiko" role="tab"
                                                  data-toggle="tab">Chiko</a></li>
        <li role="presentation"><a href="#laitovo" aria-controls="laitovo" role="tab" data-toggle="tab">Laitovo</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade" id="laitovo">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_double" aria-controls="laitovo_double"
                                                              role="tab" data-toggle="tab">Простые</a></li>
                    <li role="presentation"><a href="#laitovo_magnit" aria-controls="laitovo_magnit" role="tab"
                                               data-toggle="tab">Магнитные</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_double">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems1">

                                <img src="/img/bw_2_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme1[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 14%;top: 58%;", 'id' => 'bw_laitovo_double_scheme_0']) ?>
                                <?= Html::dropDownList(null, @$scheme1[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 14%;top: 49%;", 'id' => 'bw_laitovo_double_scheme_1']) ?>
                                <?= Html::dropDownList(null, @$scheme1[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 15%;top: 39%;", 'id' => 'bw_laitovo_double_scheme_2']) ?>
                                <?= Html::dropDownList(null, @$scheme1[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 34%;", 'id' => 'bw_laitovo_double_scheme_3']) ?>
                                <?= Html::dropDownList(null, @$scheme1[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 29%;top: 34%;", 'id' => 'bw_laitovo_double_scheme_4']) ?>
                                <?= Html::dropDownList(null, @$scheme1[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 36%;top: 34%;", 'id' => 'bw_laitovo_double_scheme_5']) ?>
                                <?= Html::dropDownList(null, @$scheme1[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 39%;", 'id' => 'bw_laitovo_double_scheme_6']) ?>
                                <?= Html::dropDownList(null, @$scheme1[7], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 49%;", 'id' => 'bw_laitovo_double_scheme_7']) ?>
                                <?= Html::dropDownList(null, @$scheme1[8], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 58%;", 'id' => 'bw_laitovo_double_scheme_8']) ?>
                                <?= Html::dropDownList(null, @$scheme1[9], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 37%;top: 65%;", 'id' => 'bw_laitovo_double_scheme_9']) ?>
                                <?= Html::dropDownList(null, @$scheme1[10], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 30%;top: 65%;", 'id' => 'bw_laitovo_double_scheme_10']) ?>
                                <?= Html::dropDownList(null, @$scheme1[11], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 65%;", 'id' => 'bw_laitovo_double_scheme_11']) ?>
                                <?= Html::dropDownList(null, @$scheme1[12], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 58%;", 'id' => 'bw_laitovo_double_scheme_12']) ?>
                                <?= Html::dropDownList(null, @$scheme1[13], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 48%;", 'id' => 'bw_laitovo_double_scheme_13']) ?>
                                <?= Html::dropDownList(null, @$scheme1[14], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 39%;", 'id' => 'bw_laitovo_double_scheme_14']) ?>
                                <?= Html::dropDownList(null, @$scheme1[15], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 60%;top: 34%;", 'id' => 'bw_laitovo_double_scheme_15']) ?>
                                <?= Html::dropDownList(null, @$scheme1[16], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 67%;top: 34%;", 'id' => 'bw_laitovo_double_scheme_16']) ?>
                                <?= Html::dropDownList(null, @$scheme1[17], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 74%;top: 34%;", 'id' => 'bw_laitovo_double_scheme_17']) ?>
                                <?= Html::dropDownList(null, @$scheme1[18], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 81%;top: 38%;", 'id' => 'bw_laitovo_double_scheme_18']) ?>
                                <?= Html::dropDownList(null, @$scheme1[19], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 82%;top: 48%;", 'id' => 'bw_laitovo_double_scheme_19']) ?>
                                <?= Html::dropDownList(null, @$scheme1[20], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 82%;top: 58%;", 'id' => 'bw_laitovo_double_scheme_20']) ?>
                                <?= Html::dropDownList(null, @$scheme1[21], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 75%;top: 65%;", 'id' => 'bw_laitovo_double_scheme_21']) ?>
                                <?= Html::dropDownList(null, @$scheme1[22], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 68%;top: 65%;", 'id' => 'bw_laitovo_double_scheme_22']) ?>
                                <?= Html::dropDownList(null, @$scheme1[23], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 61%;top: 65%;", 'id' => 'bw_laitovo_double_scheme_23']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_magnit">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems2">

                                <img src="/img/bw_2_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme2[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 14%;top: 58%;", 'id' => 'bw_laitovo_double_schemem_0']) ?>
                                <?= Html::dropDownList(null, @$scheme2[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 14%;top: 49%;", 'id' => 'bw_laitovo_double_schemem_1']) ?>
                                <?= Html::dropDownList(null, @$scheme2[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 15%;top: 39%;", 'id' => 'bw_laitovo_double_schemem_2']) ?>
                                <?= Html::dropDownList(null, @$scheme2[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 34%;", 'id' => 'bw_laitovo_double_schemem_3']) ?>
                                <?= Html::dropDownList(null, @$scheme2[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 29%;top: 34%;", 'id' => 'bw_laitovo_double_schemem_4']) ?>
                                <?= Html::dropDownList(null, @$scheme2[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 36%;top: 34%;", 'id' => 'bw_laitovo_double_schemem_5']) ?>
                                <?= Html::dropDownList(null, @$scheme2[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 39%;", 'id' => 'bw_laitovo_double_schemem_6']) ?>
                                <?= Html::dropDownList(null, @$scheme2[7], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 49%;", 'id' => 'bw_laitovo_double_schemem_7']) ?>
                                <?= Html::dropDownList(null, @$scheme2[8], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 58%;", 'id' => 'bw_laitovo_double_schemem_8']) ?>
                                <?= Html::dropDownList(null, @$scheme2[9], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 37%;top: 65%;", 'id' => 'bw_laitovo_double_schemem_9']) ?>
                                <?= Html::dropDownList(null, @$scheme2[10], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 30%;top: 65%;", 'id' => 'bw_laitovo_double_schemem_10']) ?>
                                <?= Html::dropDownList(null, @$scheme2[11], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 65%;", 'id' => 'bw_laitovo_double_schemem_11']) ?>
                                <?= Html::dropDownList(null, @$scheme2[12], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 58%;", 'id' => 'bw_laitovo_double_schemem_12']) ?>
                                <?= Html::dropDownList(null, @$scheme2[13], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 48%;", 'id' => 'bw_laitovo_double_schemem_13']) ?>
                                <?= Html::dropDownList(null, @$scheme2[14], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 39%;", 'id' => 'bw_laitovo_double_schemem_14']) ?>
                                <?= Html::dropDownList(null, @$scheme2[15], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 60%;top: 34%;", 'id' => 'bw_laitovo_double_schemem_15']) ?>
                                <?= Html::dropDownList(null, @$scheme2[16], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 67%;top: 34%;", 'id' => 'bw_laitovo_double_schemem_16']) ?>
                                <?= Html::dropDownList(null, @$scheme2[17], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 74%;top: 34%;", 'id' => 'bw_laitovo_double_schemem_17']) ?>
                                <?= Html::dropDownList(null, @$scheme2[18], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 81%;top: 38%;", 'id' => 'bw_laitovo_double_schemem_18']) ?>
                                <?= Html::dropDownList(null, @$scheme2[19], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 82%;top: 48%;", 'id' => 'bw_laitovo_double_schemem_19']) ?>
                                <?= Html::dropDownList(null, @$scheme2[20], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 82%;top: 58%;", 'id' => 'bw_laitovo_double_schemem_20']) ?>
                                <?= Html::dropDownList(null, @$scheme2[21], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 75%;top: 65%;", 'id' => 'bw_laitovo_double_schemem_21']) ?>
                                <?= Html::dropDownList(null, @$scheme2[22], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 68%;top: 65%;", 'id' => 'bw_laitovo_double_schemem_22']) ?>
                                <?= Html::dropDownList(null, @$scheme2[23], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 61%;top: 65%;", 'id' => 'bw_laitovo_double_schemem_23']) ?>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade in active" id="chiko">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_double" aria-controls="chiko_double"
                                                              role="tab" data-toggle="tab">Простые</a></li>
                    <li role="presentation"><a href="#chiko_magnit" aria-controls="chiko_magnit" role="tab"
                                               data-toggle="tab">Магнитные</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_double">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems3">

                                <img src="/img/bw_2_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme3[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 14%;top: 58%;", 'id' => 'bw_chiko_double_scheme_0']) ?>
                                <?= Html::dropDownList(null, @$scheme3[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 14%;top: 49%;", 'id' => 'bw_chiko_double_scheme_1']) ?>
                                <?= Html::dropDownList(null, @$scheme3[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 15%;top: 39%;", 'id' => 'bw_chiko_double_scheme_2']) ?>
                                <?= Html::dropDownList(null, @$scheme3[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 34%;", 'id' => 'bw_chiko_double_scheme_3']) ?>
                                <?= Html::dropDownList(null, @$scheme3[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 29%;top: 34%;", 'id' => 'bw_chiko_double_scheme_4']) ?>
                                <?= Html::dropDownList(null, @$scheme3[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 36%;top: 34%;", 'id' => 'bw_chiko_double_scheme_5']) ?>
                                <?= Html::dropDownList(null, @$scheme3[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 39%;", 'id' => 'bw_chiko_double_scheme_6']) ?>
                                <?= Html::dropDownList(null, @$scheme3[7], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 49%;", 'id' => 'bw_chiko_double_scheme_7']) ?>
                                <?= Html::dropDownList(null, @$scheme3[8], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 58%;", 'id' => 'bw_chiko_double_scheme_8']) ?>
                                <?= Html::dropDownList(null, @$scheme3[9], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 37%;top: 65%;", 'id' => 'bw_chiko_double_scheme_9']) ?>
                                <?= Html::dropDownList(null, @$scheme3[10], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 30%;top: 65%;", 'id' => 'bw_chiko_double_scheme_10']) ?>
                                <?= Html::dropDownList(null, @$scheme3[11], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 65%;", 'id' => 'bw_chiko_double_scheme_11']) ?>
                                <?= Html::dropDownList(null, @$scheme3[12], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 58%;", 'id' => 'bw_chiko_double_scheme_12']) ?>
                                <?= Html::dropDownList(null, @$scheme3[13], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 48%;", 'id' => 'bw_chiko_double_scheme_13']) ?>
                                <?= Html::dropDownList(null, @$scheme3[14], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 39%;", 'id' => 'bw_chiko_double_scheme_14']) ?>
                                <?= Html::dropDownList(null, @$scheme3[15], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 60%;top: 34%;", 'id' => 'bw_chiko_double_scheme_15']) ?>
                                <?= Html::dropDownList(null, @$scheme3[16], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 67%;top: 34%;", 'id' => 'bw_chiko_double_scheme_16']) ?>
                                <?= Html::dropDownList(null, @$scheme3[17], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 74%;top: 34%;", 'id' => 'bw_chiko_double_scheme_17']) ?>
                                <?= Html::dropDownList(null, @$scheme3[18], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 81%;top: 38%;", 'id' => 'bw_chiko_double_scheme_18']) ?>
                                <?= Html::dropDownList(null, @$scheme3[19], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 82%;top: 48%;", 'id' => 'bw_chiko_double_scheme_19']) ?>
                                <?= Html::dropDownList(null, @$scheme3[20], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 82%;top: 58%;", 'id' => 'bw_chiko_double_scheme_20']) ?>
                                <?= Html::dropDownList(null, @$scheme3[21], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 75%;top: 65%;", 'id' => 'bw_chiko_double_scheme_21']) ?>
                                <?= Html::dropDownList(null, @$scheme3[22], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 68%;top: 65%;", 'id' => 'bw_chiko_double_scheme_22']) ?>
                                <?= Html::dropDownList(null, @$scheme3[23], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 61%;top: 65%;", 'id' => 'bw_chiko_double_scheme_23']) ?>

                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_magnit">
                        <br>
                        <div class="row">
                            <div class="col-md-12 schemeitems4">

                                <img src="/img/bw_2_scheme.jpg" width="100%">

                                <?= Html::dropDownList(null, @$scheme4[0], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 14%;top: 58%;", 'id' => 'bw_chiko_double_schemem_0']) ?>
                                <?= Html::dropDownList(null, @$scheme4[1], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 14%;top: 49%;", 'id' => 'bw_chiko_double_schemem_1']) ?>
                                <?= Html::dropDownList(null, @$scheme4[2], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 15%;top: 39%;", 'id' => 'bw_chiko_double_schemem_2']) ?>
                                <?= Html::dropDownList(null, @$scheme4[3], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 34%;", 'id' => 'bw_chiko_double_schemem_3']) ?>
                                <?= Html::dropDownList(null, @$scheme4[4], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 29%;top: 34%;", 'id' => 'bw_chiko_double_schemem_4']) ?>
                                <?= Html::dropDownList(null, @$scheme4[5], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 36%;top: 34%;", 'id' => 'bw_chiko_double_schemem_5']) ?>
                                <?= Html::dropDownList(null, @$scheme4[6], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 39%;", 'id' => 'bw_chiko_double_schemem_6']) ?>
                                <?= Html::dropDownList(null, @$scheme4[7], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 49%;", 'id' => 'bw_chiko_double_schemem_7']) ?>
                                <?= Html::dropDownList(null, @$scheme4[8], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 43%;top: 58%;", 'id' => 'bw_chiko_double_schemem_8']) ?>
                                <?= Html::dropDownList(null, @$scheme4[9], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 37%;top: 65%;", 'id' => 'bw_chiko_double_schemem_9']) ?>
                                <?= Html::dropDownList(null, @$scheme4[10], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 30%;top: 65%;", 'id' => 'bw_chiko_double_schemem_10']) ?>
                                <?= Html::dropDownList(null, @$scheme4[11], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 23%;top: 65%;", 'id' => 'bw_chiko_double_schemem_11']) ?>
                                <?= Html::dropDownList(null, @$scheme4[12], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 58%;", 'id' => 'bw_chiko_double_schemem_12']) ?>
                                <?= Html::dropDownList(null, @$scheme4[13], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 48%;", 'id' => 'bw_chiko_double_schemem_13']) ?>
                                <?= Html::dropDownList(null, @$scheme4[14], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 53%;top: 39%;", 'id' => 'bw_chiko_double_schemem_14']) ?>
                                <?= Html::dropDownList(null, @$scheme4[15], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 60%;top: 34%;", 'id' => 'bw_chiko_double_schemem_15']) ?>
                                <?= Html::dropDownList(null, @$scheme4[16], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 67%;top: 34%;", 'id' => 'bw_chiko_double_schemem_16']) ?>
                                <?= Html::dropDownList(null, @$scheme4[17], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 74%;top: 34%;", 'id' => 'bw_chiko_double_schemem_17']) ?>
                                <?= Html::dropDownList(null, @$scheme4[18], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 81%;top: 38%;", 'id' => 'bw_chiko_double_schemem_18']) ?>
                                <?= Html::dropDownList(null, @$scheme4[19], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 82%;top: 48%;", 'id' => 'bw_chiko_double_schemem_19']) ?>
                                <?= Html::dropDownList(null, @$scheme4[20], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 82%;top: 58%;", 'id' => 'bw_chiko_double_schemem_20']) ?>
                                <?= Html::dropDownList(null, @$scheme4[21], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 75%;top: 65%;", 'id' => 'bw_chiko_double_schemem_21']) ?>
                                <?= Html::dropDownList(null, @$scheme4[22], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 68%;top: 65%;", 'id' => 'bw_chiko_double_schemem_22']) ?>
                                <?= Html::dropDownList(null, @$scheme4[23], ArrayHelper::merge(['' => ''], ArrayHelper::map($clips, 'fullname', 'fullname')), ['style' => "position: absolute;left: 61%;top: 65%;", 'id' => 'bw_chiko_double_schemem_23']) ?>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
</div>
