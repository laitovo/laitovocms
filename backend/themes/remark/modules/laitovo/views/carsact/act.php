<?php

use yii\helpers\Html;
use yii\helpers\Json;
use common\models\laitovo\Cars;
use backend\widgets\ActiveForm;

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Автомобили'), 'url' => ['cars/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Акты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->car->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>

<!doctype html>
<html>
<body>
<?php ActiveForm::begin(); ?>

<? if (!$edit): ?>

    <table class="waffle" width="100%" cellspacing="0" cellpadding="0">
        <thead>
        </thead>
        <tbody>
        <tr style='height:19px;'>
            <td class="s0" colspan="2">
                <small>Версия от 01.10.2016 г.</small>
            </td>
            <td class="s1"></td>
            <td class="s1"></td>
            <td class="s1"></td>
            <td class="s1"></td>
            <td class="s1"></td>
            <td class="s1"></td>
        </tr>
        <tr style='height:19px;'>
            <td class="s2">УТВЕРЖДАЮ</td>
            <td class="s2">&nbsp;</td>
            <td class="s2">&nbsp;</td>
            <td class="s2">ДОПУЩЕНО К ПРОИЗВОДСТВУ</td>
            <td class="s2">&nbsp;</td>
            <td class="s2">&nbsp;</td>
            <td class="s2">СОГЛАСОВАНО</td>
        </tr>
        <tr style='height:19px;'>
            <td class="s2">Директор по развитию</td>
            <td class="s2"></td>
            <td class="s2"></td>
            <td class="s2">Технический директор</td>
            <td class="s2"></td>
            <td class="s2"></td>
            <td class="s2">Мастер производства</td>
        </tr>
        <tr style='height:19px;'>
            <td class="s2">_______________ Антонов Д.Л.</td>
            <td class="s2"></td>
            <td class="s2"></td>
            <td class="s2">_____________ Алексеев А.В.</td>
            <td class="s2"></td>
            <td class="s2"></td>
            <td class="s2">_____________</td>
        </tr>
        <tr style='height:19px;'>
            <td class="s2">
                &quot;_____&quot;______________ <?= Yii::$app->formatter->asDate($model->created_at, 'yyyy') ?> г.
            </td>
            <td class="s2"></td>
            <td class="s2"></td>
            <td class="s2">
                &quot;_____&quot;______________ <?= Yii::$app->formatter->asDate($model->created_at, 'yyyy') ?> г.
            </td>
            <td class="s2"></td>
            <td class="s2"></td>
            <td class="s2">
                &quot;_____&quot;______________ <?= Yii::$app->formatter->asDate($model->created_at, 'yyyy') ?> г.
            </td>
        </tr>
        </tbody>
    </table>

    <br>
    <div class="">
        <div style="float: left;width: 33.33%"><img src="/img/laitovo_logo.png"></div>
        <div style="float: left;width: 33.33%"><br><b>АКТ ВНЕСЕНИЯ ИЗМЕНЕНИЙ №</b></div>
        <div style="float: left;width: 33.33%"><br>

            <table class="invoice_items" width="100%" cellpadding="2" cellspacing="2" border="0">
                <tbody>
                <tr style='height:28px;'>
                    <td class="s8"><b><?= $after['article'] ?></b></td>
                    <td class="s9"><b><?= $model->json('number') ?></b></td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
    <br>
<? endif ?>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:19px;background: #ddd;'>
        <td class="s11 softmerge">
            <div class="softmerge-inner" style="width: 69px; left: -8px;">Артикул</div>
        </td>
        <td class="s12" colspan="9">НАИМЕНОВАНИЕ АВТО</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s13"><b><?= $after['article'] ?></b></td>
        <td class="s14" colspan="9"><b><?= $after['auto']['name'] ?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s12" colspan="10">ИНФОРМАЦИЯ ОБ АНАЛОГАХ</td>
    </tr>
    <? if ($analogs = Json::decode($after['analogs'], $asArray = true))
        foreach ($analogs as $analog):?>
            <tr style='height:19px;'>
                <td class="s17"><?= @Cars::findOne($analog['car'])->article ?></td>
                <td class="s17" colspan="9"><?= $analog['name'] ?> <b><?= implode(',', $analog['elements']) ?></b></td>
            </tr>
        <? endforeach ?>
    <tr style='height:19px;background: #ddd;'>
        <td class="s16" colspan="8">НОМЕРА КОРРЕКТИРУЕМЫХ ЗАЖИМОВ</td>
        <td class="s16" colspan="2">№ ЛЕКАЛА / сторона</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s14" colspan="8">&nbsp;</td>
        <td class="s14" colspan="2"><b><?= $after['fields']['nomerlekala'] ?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s17" colspan="10">КОРРЕКТИРУЕМЫЕ ЭЛЕМЕНТЫ АВТО (отметить знаком V)</td>
    </tr>


    <? if ($edit): ?>
        <tr style='height:19px;'>
            <td class="s18">ПШ
                <input type="hidden" name="korect_fw" value="0">
                <input type="checkbox" name="korect_fw" value="1" <?= $model->json('korect_fw') ? 'checked' : '' ?> >
            </td>
            <td class="s18">ПФ
                <input type="hidden" name="korect_fv" value="0">
                <input type="checkbox" name="korect_fv" value="1" <?= $model->json('korect_fv') ? 'checked' : '' ?> >
            </td>
            <td class="s18">ПБ
                <input type="hidden" name="korect_fd" value="0">
                <input type="checkbox" name="korect_fd" value="1" <?= $model->json('korect_fd') ? 'checked' : '' ?> >
            </td>
            <td class="s18">ЗБ
                <input type="hidden" name="korect_rd" value="0">
                <input type="checkbox" name="korect_rd" value="1" <?= $model->json('korect_rd') ? 'checked' : '' ?> >
            </td>
            <td class="s18">ЗФ
                <input type="hidden" name="korect_rv" value="0">
                <input type="checkbox" name="korect_rv" value="1" <?= $model->json('korect_rv') ? 'checked' : '' ?> >
            </td>
            <td class="s18">ЗШ
                <input type="hidden" name="korect_bw" value="0">
                <input type="checkbox" name="korect_bw" value="1" <?= $model->json('korect_bw') ? 'checked' : '' ?> >
            </td>
            <td class="s19">ПК
                <input type="hidden" name="korect_pk" value="0">
                <input type="checkbox" name="korect_pk" value="1" <?= $model->json('korect_pk') ? 'checked' : '' ?> >
            </td>
            <td class="s18">Капот
            </td>
            <td class="s19"></td>
            <td class="s18"></td>
        </tr>
    <? else: ?>
        <tr style='height:19px;'>
            <td class="s18">ПШ <b>
                    <?= $model->json('korect_fw') ? 'V' : '' ?>
                </b></td>
            <td class="s18">ПФ <b>
                    <?= $model->json('korect_fv') ? 'V' : '' ?>
                </b></td>
            <td class="s18">ПБ <b>
                    <?= $model->json('korect_fd') ? 'V' : '' ?>
                </b></td>
            <td class="s18">ЗБ <b>
                    <?= $model->json('korect_rd') ? 'V' : '' ?>
                </b></td>
            <td class="s18">ЗФ <b>
                    <?= $model->json('korect_rv') ? 'V' : '' ?>
                </b></td>
            <td class="s18">ЗШ <b>
                    <?= $model->json('korect_bw') ? 'V' : '' ?>
                </b></td>
            <td class="s19">ПК <b>
                    <?= $model->json('korect_pk') ? 'V' : '' ?>
                </b></td>
            <td class="s18">Капот</td>
            <td class="s19"></td>
            <td class="s18"></td>
        </tr>
    <? endif ?>

    <? if (!$edit): ?>
        <tr style='height:19px;background: #ddd;'>
            <td class="s20" colspan="10">ЭТАПЫ КОРРЕКТИРОВКИ</td>
        </tr>
        <tr style='height:65px;'>
            <td class="s21" colspan="2">Снятие контура</td>
            <td class="s21" colspan="2">Изготовление временного лекала и запуск в пр-во опытного образца</td>
            <td class="s21" colspan="2">Примерка опытного образца и снятие фото и видео отчета</td>
            <td class="s22" colspan="2">Изготовление действующего лекала, оцифровка и помещение информации в хранилище
            </td>
            <td class="s21" colspan="2">Занесение информации в журналы ОТК и КЛИПС</td>
        </tr>
        <tr style='height:25px;'>
            <td class="s15" colspan="2">&nbsp;</td>
            <td class="s15" colspan="2"></td>
            <td class="s15" colspan="2"></td>
            <td class="s15" colspan="2"></td>
            <td class="s15" colspan="2"></td>
        </tr>
        <tr style='height:14px;'>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
        </tr>
        <tr style='height:52px;'>
            <td class="s24">&nbsp;</td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
        </tr>
        <tr style='height:14px;'>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
        </tr>
        <tr style='height:65px;'>
            <td class="s21" colspan="2">Занесение информации в карточку автомобиля</td>
            <td class="s21" colspan="2">Занесение информации в систему управления (менеджер файлов/фото)</td>
            <td class="s21" colspan="2">Размещение видео на канале и отправка ссылки на модерацию</td>
            <td class="s21" colspan="2">Лекало принято участком оклейки</td>
            <td class="s21" colspan="2">Лекало принято участком изгиба</td>
        </tr>
        <tr style='height:25px;'>
            <td class="s15" colspan="2">&nbsp;</td>
            <td class="s15" colspan="2"></td>
            <td class="s15" colspan="2"></td>
            <td class="s15" colspan="2"></td>
            <td class="s15" colspan="2"></td>
        </tr>
        <tr style='height:14px;'>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
            <td class="s23" colspan="2">
                <small>ФИО</small>
            </td>
        </tr>
        <tr style='height:52px;'>
            <td class="s24">&nbsp;</td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
            <td class="s24"></td>
        </tr>
        <tr style='height:14px;'>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
            <td class="s23">
                <small>дата</small>
            </td>
            <td class="s23">
                <small>подпись</small>
            </td>
        </tr>
    <? endif ?>

    <tr style='height:19px;background: #ddd;'>
        <td class="s25" colspan="10">ОПИСАНИЕ ВНОСИМЫХ ИЗМЕНЕНИЙ</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s26" colspan="5">БЫЛО</td>
        <td class="s26" colspan="5">СТАЛО</td>
    </tr>
    <? if ($edit): ?>

        <tr style='height:19px;'>
            <td class="s15" colspan="5"><textarea name="beforecomment" class="form-control"
                                                  rows="5"><?= $model->json('beforecomment') ?></textarea></td>
            <td class="s15" colspan="5"><textarea name="aftercomment" class="form-control"
                                                  rows="5"><?= $model->json('aftercomment') ?></textarea></td>
        </tr>
        <tr style='height:19px;'>
            <td class="s15" colspan="10">
                <br>
                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>
            </td>
        </tr>


        </form>
    <? else: ?>
        <tr style='height:19px;'>
            <td class="s15" style="text-align: left;vertical-align: top;"
                colspan="5"><?= $model->json('beforecomment') ?>&nbsp;
            </td>
            <td class="s15" style="text-align: left;vertical-align: top;"
                colspan="5"><?= $model->json('aftercomment') ?></td>
        </tr>
    <? endif ?>
    </tbody>
</table>
<?php ActiveForm::end(); ?>

</body>
</html>
