<?php

use yii\helpers\Json;
use common\models\laitovo\Cars;

?>

<!doctype html>
<html>
<body>


<div class="">
    <div style="width: 100%;text-align: center;"><b><?= $title ?></b></div>
    <div style="width: 100%;text-align: right;">стр.1</div>
</div>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3">АРТ.</td>
        <td class="s3" colspan="11">НАИМЕНОВАНИЕ АВТО</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s4"><b><?= $after['article'] ?></b></td>
        <td class="s4" colspan="11"><b><?= mb_substr(($after['auto']['name']), 0, 80).'...'?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s5" colspan="12">ИНФОРМАЦИЯ ОБ АНАЛОГАХ</td>
    </tr>
    <? if ($analogs = Json::decode($after['analogs'], $asArray = true))
        foreach ($analogs as $analog):?>
            <tr style='height:19px;'>
                <td class="s17"><?= @Cars::findOne($analog['car'])->article ?></td>
                <td class="s17" colspan="11"><?= mb_substr($analog['name'], 0, 65).'...' ?> <b><?= implode(',', $analog['elements']) ?></b></td>
            </tr>
        <? endforeach ?>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3" colspan="12">СХЕМЫ РАСПОЛОЖЕНИЯ СТАНДАРТНЫХ ЗАЖИМОВ</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="3">ПФ</td>
        <td class="s6" colspan="3">ПБ</td>
        <td class="s6" colspan="3">ПБ (вырез для зеркал)</td>
        <td class="s6" colspan="3">ПБ (вырез для курения)</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="3">
            <img width="100" src="/img/act/image06.jpg">
            <div style="position: fixed; left: 5mm; top: <?= 45 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fv_laitovo_standart_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 13mm; top: <?= 39 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fv_laitovo_standart_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 21mm; top: <?= 35 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fv_laitovo_standart_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 29mm; top: <?= 39 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fv_laitovo_standart_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 27mm; top: <?= 43 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fv_laitovo_standart_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 24mm; top: <?= 47 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fv_laitovo_standart_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 15mm; top: <?= 51 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fv_laitovo_standart_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td class="s6" colspan="3">
            <img width="150" src="/img/act/image001.jpg">
            <div style="position: fixed; left: 43mm; top: <?= 46 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 43mm; top: <?= 42 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 43mm; top: <?= 38 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 51mm; top: <?= 34 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 63mm; top: <?= 36 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 75mm; top: <?= 39 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 82mm; top: <?= 47 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 70mm; top: <?= 50 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 60mm; top: <?= 50 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[8] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 50mm; top: <?= 50 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_scheme'])[9] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td class="s6" colspan="3">
            <img width="150" src="/img/act/image05.jpg">
            <div style="position: fixed; left: 87mm; top: <?= 46 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_mirror_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 87mm; top: <?= 42 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_mirror_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 87mm; top: <?= 38 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_mirror_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 97mm; top: <?= 35 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_mirror_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 107mm; top: <?= 36 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_mirror_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 117mm; top: <?= 38 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_mirror_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 108mm; top: <?= 50 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_mirror_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 98mm; top: <?= 50 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_mirror_scheme'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td class="s6" colspan="3">
            <img width="150" src="/img/act/image04.jpg">
            <div style="position: fixed; left: 133mm; top: <?= 46 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 133mm; top: <?= 42 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 133mm; top: <?= 37 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 142mm; top: <?= 34 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 153mm; top: <?= 35 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 173mm; top: <?= 47 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 161mm; top: <?= 50 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 152mm; top: <?= 50 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 142mm; top: <?= 50 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_smoke_scheme'])[8] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr style='height:19px;'>
        <td class="s7" colspan="3">ПБ (укороченный)</td>
        <td class="s7" colspan="3">ЗБ</td>
        <td class="s6" colspan="3">ЗФ</td>
        <td class="s6" colspan="3">ЗФ (трапеция)</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="3">
            <img width="150" src="/img/act/image03.jpg">
            <div style="position: fixed; left: 0mm; top: <?= 74 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_short_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 0mm; top: <?= 68 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_short_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 0mm; top: <?= 62 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_short_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 13mm; top: <?= 59 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_short_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 28mm; top: <?= 60 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_short_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 28mm; top: <?= 78 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_short_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 13mm; top: <?= 78 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_short_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td class="s6" colspan="3">
            <img width="150" src="/img/act/image01.jpg">
            <div style="position: fixed; left: 43mm; top: <?= 75 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 42mm; top: <?= 72 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 42mm; top: <?= 69 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 52mm; top: <?= 63 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 62mm; top: <?= 61 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 72mm; top: <?= 60 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 81mm; top: <?= 65 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 81mm; top: <?= 69 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 81mm; top: <?= 72 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[8] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 74mm; top: <?= 76 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[9] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 63mm; top: <?= 76 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[10] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 51mm; top: <?= 76 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_scheme'])[11] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td class="s6" colspan="3">
            <img width="100" src="/img/act/image061.jpg">
            <div style="position: fixed; left:  95mm; top: <?= 71 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_triangul_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 103mm; top: <?= 65 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_triangul_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 111mm; top: <?= 62 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_triangul_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 118mm; top: <?= 65 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_triangul_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 116mm; top: <?= 69 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_triangul_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 114mm; top: <?= 73 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_triangul_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 104mm; top: <?= 77 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_triangul_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td class="s6" colspan="3">
            <img width="150" src="/img/act/image02.jpg">
            <div style="position: fixed; left: 133mm; top: <?= 73 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 135mm; top: <?= 68 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 138mm; top: <?= 63 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 145mm; top: <?= 59 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 155mm; top: <?= 59 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 165mm; top: <?= 59 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 173mm; top: <?= 63 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 172mm; top: <?= 68 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 171mm; top: <?= 74 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[8] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 162mm; top: <?= 79 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[9] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 152mm; top: <?= 79 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[10] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 142mm; top: <?= 79 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rv_laitovo_standart_scheme'])[11] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="6">ЗШ</td>
        <td class="s6" colspan="6">ЗШ (из 2-х частей)</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="6">
            <img width="200" src="/img/act/image07.jpg">
            <div style="position: fixed; left: 17mm; top: <?= 103 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed;  left: 16mm; top: <?= 98 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed;  left: 24mm; top: <?= 93 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed;  left: 32mm; top: <?= 92 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed;  left: 41mm; top: <?= 92 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed;  left: 51mm; top: <?= 92 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed;  left: 60mm; top: <?= 93 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed;  left: 67mm; top: <?= 98 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 67mm; top: <?= 103 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[8] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 60mm; top: <?= 105 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[9] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 51mm; top: <?= 105 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[10] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 41mm; top: <?= 105 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[11] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 32mm; top: <?= 105 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[12] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 24mm; top: <?= 105 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_standart_scheme'])[13] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td class="s6" colspan="6">
            <img width="250" src="/img/act/image08.jpg">
            <div style="position: fixed; left: 98mm; top: <?= 106 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed;  left: 99mm; top: <?= 98 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 100mm; top: <?= 91 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 107mm; top: <?= 87 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 114mm; top: <?= 87 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 121mm; top: <?= 87 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 128mm; top: <?= 91 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 128mm; top: <?= 98 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 128mm; top: <?= 106 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[8] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 121mm; top: <?= 111 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[9] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 114mm; top: <?= 111 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[10] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 107mm; top: <?= 111 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[11] ?></small>
                        </td>
                    </tr>
                </table>
            </div>

            <div style="position: fixed; left: 135mm; top: <?= 106 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[12] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 135mm; top: <?= 98 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[13] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 135mm; top: <?= 91 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[14] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 141mm; top: <?= 87 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[15] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 148mm; top: <?= 87 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[16] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 156mm; top: <?= 87 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[17] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 163mm; top: <?= 91 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[18] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 163mm; top: <?= 98 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[19] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 164mm; top: <?= 106 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[20] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 156mm; top: <?= 111 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[21] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 148mm; top: <?= 111 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[22] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 141mm; top: <?= 111 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['bw_chiko_double_scheme'])[23] ?></small>
                        </td>
                    </tr>
                </table>
            </div>

        </td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3" colspan="12">СХЕМЫ РАСПОЛОЖЕНИЯ МАГНИТНЫХ ДЕРЖАТЕЛЕЙ</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="6">ПБ</td>
        <td class="s6" colspan="6">ЗБ</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="6">
            <img width="200" src="/img/act/image09.jpg">
            <div style="position: fixed; left: 18mm; top: <?= 139 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 18mm; top: <?= 134 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 18mm; top: <?= 130 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 29mm; top: <?= 125 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 45mm; top: <?= 126 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 58mm; top: <?= 132 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 67mm; top: <?= 141 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 53mm; top: <?= 144 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 39mm; top: <?= 144 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[8] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 26mm; top: <?= 144 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['fd_laitovo_standart_schemem'])[9] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td class="s6" colspan="6">
            <img width="200" src="/img/act/image10.jpg">
            <div style="position: fixed; left: 108mm; top: <?= 143 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[0] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 108mm; top: <?= 139 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[1] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 107mm; top: <?= 135 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[2] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 119mm; top: <?= 128 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[3] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 132mm; top: <?= 125 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[4] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 145mm; top: <?= 124 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[5] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 157mm; top: <?= 130 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[6] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 157mm; top: <?= 134 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[7] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 157mm; top: <?= 138 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[8] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 148mm; top: <?= 144 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[9] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 134mm; top: <?= 144 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[10] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 119mm; top: <?= 144 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td>
                            <small style="background: #fff;"><?= Json::decode($after['rd_laitovo_standart_schemem'])[11] ?></small>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3" colspan="12">ОПИСАНИЕ УСТАНОВКИ</td>
    </tr>
    <tr style='height:20px;'>
        <td class="s8">ПБ</td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s10" style="border: none;"></td>
        <td class="s8">ЗБ</td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s10" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s11" colspan="6" style="border-top: none;text-align: left;"><br><br><br><br><br><br><br><br><br></td>
        <td class="s11" colspan="6" style="border-top: none;text-align: left;"><br><br><br><br><br><br><br><br><br></td>
    </tr>
    <tr style='height:20px;'>
        <td class="s8">ЗФ</td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s10" style="border: none;"></td>
        <td class="s8">ЗШ</td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s10" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s11" colspan="6" style="border-top: none;text-align: left;"><br><br><br><br><br><br><br><br><br></td>
        <td class="s11" colspan="6" style="border-top: none;text-align: left;"><br><br><br><br><br><br><br><br><br></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s7" style="border: none;">&nbsp;</td>
        <td class="s13" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
        <td class="s14" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
        <td class="s13" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s15" colspan="6" style="background: #ddd;">УКОМПЛЕКТОВАТЬ УСТАНОВОЧНЫМ КРЮЧКОМ</td>
        <td class="s16" style="border: none;"></td>
        <td class="s16" style="border: none;"></td>
        <td class="s16" style="border: none;"></td>
        <td class="s12">ПБ <b><?= @$after['fields']['kryuchokpb'] ? 'V' : '' ?></b></td>
        <td class="s12">ЗБ <b><?= @$after['fields']['kryuchokzb'] ? 'V' : '' ?></b></td>
        <td class="s12">ЗПС <b><?= @$after['fields']['kryuchokzps'] ? 'V' : '' ?></b></td>
    </tr>
    </tbody>
</table>


<p style="page-break-after: always;"></p>

<div class="">
    <div style="width: 100%;text-align: right;">стр.2</div>
</div>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:19px;background: #ddd;'>
        <td class="s2" colspan="12">ОТТИСК ЭТАЛОНА ЗАЖИМА</td>
    </tr>
    <tr style='height:20px;'>
        <td class="s3">№1</td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s5" style="border: none;"></td>
        <td class="s3">№2</td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s5" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="6" style=" border-top: none;"><br><br><br><br><br><br><br><br><br><br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td class="s6" colspan="6" style=" border-top: none;"><br><br><br><br><br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr style='height:20px;'>
        <td class="s3">№3</td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s5" style="border: none;"></td>
        <td class="s3">№4</td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s5" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="6" style="border-top: none;"><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
        <td class="s6" colspan="6" style="border-top: none;"></td>
    </tr>
    <tr style='height:20px;'>
        <td class="s3">№5</td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s5" style="border: none;"></td>
        <td class="s3">№6</td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s5" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="6" style="border-top: none;"><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
        <td class="s6" colspan="6" style="border-top: none;"></td>
    </tr>
    <tr style='height:20px;'>
        <td class="s3">№7</td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s5" style="border: none;"></td>
        <td class="s3">№8</td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s4" style="border: none;"></td>
        <td class="s5" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="6" style="border-top: none;"><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
        <td class="s6" colspan="6" style="border-top: none;"></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s2" colspan="12">ПОДПИСИ ОТВЕТСТВЕННЫХ</td>
    </tr>
    <tr style='height:15px;'>
        <td class="s7" style="border: none;">&nbsp;</td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
    </tr>
    <tr style='height:25px;'>
        <td class="s10">РАЗРАБОТЧИК</td>
        <td class="s11" style="border: none;"></td>
        <td class="s8" colspan="6" style="width: 50mm"></td>
        <td class="s11" style="border: none;"></td>
        <td class="s8" colspan="2"></td>
        <td class="s11" style="border: none;"></td>
    </tr>
    <tr style='height:17px;'>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s7" colspan="6" style="border: none;">ФИО</td>
        <td class="s9" style="border: none;"></td>
        <td class="s7" colspan="2" style="border: none;">подпись</td>
        <td class="s8" style="border: none;"></td>
    </tr>
    <tr style='height:25px;'>
        <td class="s10">СТАРШИЙ УЧАСТКА ОТК</td>
        <td class="s11" style="border: none;"></td>
        <td class="s8" colspan="6"></td>
        <td class="s11" style="border: none;"></td>
        <td class="s8" colspan="2"></td>
        <td class="s11" style="border: none;"></td>
    </tr>
    <tr style='height:16px;'>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s7" colspan="6" style="border: none;">ФИО</td>
        <td class="s9" style="border: none;"></td>
        <td class="s7" colspan="2" style="border: none;">подпись</td>
        <td class="s8" style="border: none;"></td>
    </tr>
    <tr style='height:25px;'>
        <td class="s10">СТАРШИЙ УЧАСТКА КЛИПС</td>
        <td class="s11" style="border: none;"></td>
        <td class="s8" colspan="6"></td>
        <td class="s11" style="border: none;"></td>
        <td class="s8" colspan="2"></td>
        <td class="s11" style="border: none;"></td>
    </tr>
    <tr style='height:19px;'>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s12" colspan="6" style="border: none;">ФИО</td>
        <td class="s9" style="border: none;"></td>
        <td class="s12" colspan="2" style="border: none;">подпись</td>
        <td class="s12" style="border: none;"></td>
    </tr>
    </tbody>
</table>

</body>
</html>
