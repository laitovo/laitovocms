<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

?>
<?if (
       isset($model->act->difference['rv_dlina']) 
    || isset($model->act->difference['rv_visota']) 
    || isset($model->act->difference['rv_magnit']) 
    || isset($model->act->difference['rv_obshivka']) 
    || isset($model->act->difference['rv_hlyastik']) 
    || isset($model->act->difference['rv_openwindowtrue']) 
    || isset($model->act->difference['rv_openwindow']) 
):?>
<!-- Основные харакетристики для ЗФ -->
<div class="row">
    <div class="col-md-6">
        <h5>ЗФ - Свойства (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_dlina',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_dlina']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_dlina']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_dlina')])
                               .Html::tag('label',$model->rv_dlina),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_visota',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_visota']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_visota']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_visota')])
                               .Html::tag('label',$model->rv_visota),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_magnit',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_magnit']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_magnit']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_magnit')])
                               .Html::tag('label',$model->rv_magnit),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_obshivka',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_obshivka']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_obshivka']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_obshivka')])
                               .Html::tag('label',$model->rv_obshivka),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_hlyastik',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_hlyastik']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_hlyastik']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_hlyastik')])
                               .Html::tag('label',$model->rv_hlyastik),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_openwindowtrue',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_openwindowtrue']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_openwindowtrue']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_openwindowtrue')])
                               .Html::tag('label',$model->rv_openwindowtrue),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_openwindow',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_openwindow']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_openwindow']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_openwindow')])
                               .Html::tag('label',$model->rv_openwindow),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
        <h5>ЗФ - Свойства (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_dlina_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_dlina_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_dlina_b']) ? 'bg-warning' : ''],
                    'value' => $model->rv_dlina_b,
                ],
                [
                    'attribute' => 'rv_visota',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_visota_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_visota_b']) ? 'bg-warning' : ''],
                    'value' => $model->rv_visota_b,
                ],
                [
                    'attribute' => 'rv_magnit_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_magnit_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_magnit_b']) ? 'bg-warning' : ''],
                    'value' => $model->rv_magnit_b,
                ],
                [
                    'attribute' => 'rv_obshivka_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_obshivka_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_obshivka_b']) ? 'bg-warning' : ''],
                    'value' => $model->rv_obshivka_b,
                ],
                [
                    'attribute' => 'rv_hlyastik_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_hlyastik_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_hlyastik_b']) ? 'bg-warning' : ''],
                    'value' => $model->rv_hlyastik_b,
                ],
                [
                    'attribute' => 'rv_openwindowtrue_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_openwindowtrue_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_openwindowtrue_b']) ? 'bg-warning' : ''],
                    'value' => $model->rv_openwindowtrue_b,
                ],
                [
                    'attribute' => 'rv_openwindow_b',
                    'format' => 'html',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_openwindow_b']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_openwindow_b']) ? 'bg-warning' : ''],
                    'value' => $model->rv_openwindow_b,
                ],
            ],
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<!-- Продукция для ЗФ  -->
<div class="form-group">
    <!-- Laitovo Стандарт -->
    <?if  (isset($model->act->difference['rv_laitovo_standart_status'])):?>
    <h5>ЗФ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_laitovo_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_laitovo_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_laitovo_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->rv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_laitovo_standart_forma',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_laitovo_standart_forma']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_laitovo_standart_forma']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_laitovo_standart_forma')])
                               .Html::tag('label',$model->rv_laitovo_standart_forma),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_laitovo_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rv_laitovo_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'rv_laitovo_standart_forma_b',
                    'format' => 'html',
                    'value' => $model->rv_laitovo_standart_forma_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rv_laitovo_standart_scheme != $model->rv_laitovo_standart_scheme_b): ?>
    <h5>ЗФ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые (квадратная)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗФ - Laitovo Стандарт [Схема креплений - простые (квадратная)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rv_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rv_laitovo_standart_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rv_laitovo_standart_schemem != $model->rv_laitovo_standart_schemem_b): ?>
    <h5>ЗФ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные (квадратная)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗФ - Laitovo Стандарт [Схема креплений - магнитные (квадратная)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rv_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rv_laitovo_standart_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <? if ($model->rv_laitovo_triangul_scheme != $model->rv_laitovo_triangul_scheme_b): ?>
    <h5>ЗФ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые (треугольная)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗФ - Laitovo Стандарт [Схема креплений - простые (треугольная)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rv_laitovo_triangul_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Треугольная',
                'type_clips' => 'Простые',
                'window' => 'ЗФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rv_laitovo_triangul_scheme'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Треугольная',
                'type_clips' => 'Простые',
                'window' => 'ЗФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rv_laitovo_triangul_schemem != $model->rv_laitovo_triangul_schemem_b): ?>
    <h5>ЗФ - Laitovo Стандарт <?=Html::tag('i', '', ['class' => $model->rv_laitovo_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные (треугольная)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗФ - Laitovo Стандарт [Схема креплений - магнитные (треугольная)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rv_laitovo_triangul_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Треугольная',
                'type_clips' => 'Магнитные',
                'window' => 'ЗФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rv_laitovo_triangul_schemem'],
                'model' => $model,
                'brand' => 'Laitovo',
                'type' => 'Треугольная',
                'type_clips' => 'Магнитные',
                'window' => 'ЗФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Laitovo Don't Look сдвижной-->
    <?if  (isset($model->act->difference['rv_laitovo_dontlooks_status'])):?>
    <h5>ЗФ - Laitovo Don't Look сдвижной <?=Html::tag('i', '', ['class' => $model->rv_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_laitovo_dontlooks_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_laitovo_dontlooks_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_laitovo_dontlooks_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_laitovo_dontlooks_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->rv_laitovo_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_laitovo_dontlooks_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rv_laitovo_dontlooks_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Laitovo Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['rv_laitovo_dontlookb_status'])):?>
    <h5>ЗФ - Laitovo Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->rv_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_laitovo_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_laitovo_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_laitovo_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_laitovo_dontlookb_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->rv_laitovo_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_laitovo_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rv_laitovo_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->
    <!-- ############################################################################################################################################ -->


    <!-- Chiko Стандарт -->
    <?if  (isset($model->act->difference['rv_chiko_standart_status'])):?>
    <h5>ЗФ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_chiko_standart_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_chiko_standart_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_chiko_standart_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_chiko_standart_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->rv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_chiko_standart_forma',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_chiko_standart_forma']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_chiko_standart_forma']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_chiko_standart_forma')])
                               .Html::tag('label',$model->rv_chiko_standart_forma),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
                [
                    'attribute' => 'rv_chiko_standart_natyag',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_chiko_standart_natyag']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_chiko_standart_natyag']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_chiko_standart_natyag')])
                               .Html::tag('label',$model->rv_chiko_standart_natyag),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_chiko_standart_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rv_chiko_standart_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
                [
                    'attribute' => 'rv_chiko_standart_forma_b',
                    'format' => 'html',
                    'value' => $model->rv_chiko_standart_forma_b,
                ],
                [
                    'attribute' => 'fv_chiko_standart_natyag_b',
                    'format' => 'html',
                    'value' => $model->fv_chiko_standart_natyag_b,
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rv_chiko_standart_scheme != $model->rv_chiko_standart_scheme_b): ?>
    <h5>ЗФ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗФ - Chiko Стандарт [Схема креплений - простые (квадратная)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rv_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rv_chiko_standart_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Простые',
                'window' => 'ЗФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rv_chiko_standart_schemem != $model->rv_chiko_standart_schemem_b): ?>
    <h5>ЗФ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗФ - Chiko Стандарт [Схема креплений - магнитные (квадратная)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rv_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rv_chiko_standart_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Стандарт',
                'type_clips' => 'Магнитные',
                'window' => 'ЗФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rv_chiko_triangul_scheme != $model->rv_chiko_triangul_scheme_b): ?>
    <h5>ЗФ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - простые (треугольная)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗФ - Chiko Стандарт [Схема креплений - простые (треугольная)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rv_chiko_triangul_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Треугольная',
                'type_clips' => 'Простые',
                'window' => 'ЗФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rv_chiko_triangul_scheme'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Треугольная',
                'type_clips' => 'Простые',
                'window' => 'ЗФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <? if ($model->rv_chiko_triangul_schemem != $model->rv_chiko_triangul_schemem_b): ?>
    <h5>ЗФ - Chiko Стандарт <?=Html::tag('i', '', ['class' => $model->rv_chiko_standart_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <h4>Схема креплений - магнитные (треугольная)</h4>
    <?=Html::tag('div',
        Html::checkbox("message[]",
            $checked = false, 
            ['value' => 'ЗФ - Chiko Стандарт [Схема креплений - магнитные (треугольная)]'])
            .Html::tag('label','Переделать'),
        ['class' => 'checkbox-custom checkbox-primary text-left']
    )
    ?>
    <div class="row">
        <div class="col-md-6">
            <h5>После</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('after')['rv_chiko_triangul_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Треугольная',
                'type_clips' => 'Магнитные',
                'window' => 'ЗФ',
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>До</h5>
            <?= $this->render('_check_scheme', [
                'scheme' => $model->act->json('before')['rv_chiko_triangul_schemem'],
                'model' => $model,
                'brand' => 'Chiko',
                'type' => 'Треугольная',
                'type_clips' => 'Магнитные',
                'window' => 'ЗФ',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>

    <!-- Chiko Don't Look сдвижной-->
    <?if  (isset($model->act->difference['rv_chiko_dontlooks_status'])):?>
    <h5>ЗФ - Chiko Don't Look сдвижной <?=Html::tag('i', '', ['class' => $model->rv_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_chiko_dontlooks_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_chiko_dontlooks_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_chiko_dontlooks_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_chiko_dontlooks_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->rv_chiko_dontlooks_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_chiko_dontlooks_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rv_chiko_dontlooks_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>


    <!-- Chiko Don't Look бескаркасный-->
    <?if  (isset($model->act->difference['rv_chiko_dontlookb_status'])):?>
    <h5>ЗФ - Chiko Don't Look бескаркасный <?=Html::tag('i', '', ['class' => $model->rv_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])?></h5>
    <div class="row">
        <div class="col-md-6">
        <h5>Статус (после)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_chiko_dontlookb_status',
                    'format' => 'raw',
                    'contentOptions' => ['class' => isset($model->act->difference['rv_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'captionOptions' => ['class' => isset($model->act->difference['rv_chiko_dontlookb_status']) ? 'bg-warning' : ''],
                    'value' => Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('rv_chiko_dontlookb_status')])
                               .Html::tag('label',Html::tag('i', '', ['class' => $model->rv_chiko_dontlookb_status ? 'wb-check text-success' : 'wb-close text-danger'])),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">
        <h5>Статус (до)</h5>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'rv_chiko_dontlookb_status_b',
                    'format' => 'html',
                    'value' => Html::tag('i', '', ['class' => $model->rv_chiko_dontlookb_status_b ? 'wb-check text-success' : 'wb-close text-danger']),
                ],
            ],
        ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <?endif;?>
</div>