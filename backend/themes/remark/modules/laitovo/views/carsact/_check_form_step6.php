<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */
/* @var $form yii\widgets\ActiveForm */
?>
<h3>Продукция: ЗФ</h3>
<?= $form->field($model, 'rv_date')->textInput(['maxlength' => true, 'class' => 'date_picker form-control']) ?>
<?= $form->field($model, 'rv_dlina')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'rv_visota')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'rv_magnit')->dropDownList($model->_magnit) ?>
<?= $form->field($model, 'rv_obshivka')->dropDownList($model->_obshivka) ?>
<?= $form->field($model, 'rv_openwindow')->dropDownList($model->_openwindow) ?>
<?= $form->field($model, 'rv_openwindowtrue')->dropDownList($model->_openwindowtrue) ?>
<?= $form->field($model, 'rv_hlyastik')->textInput(['maxlength' => true]) ?>

<div class="form-group tabcontent <?= $model->rv_date ? '' : 'hidden' ?>">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#laitovo" aria-controls="laitovo" role="tab" data-toggle="tab">Laitovo</a>
        </li>
        <li role="presentation"><a href="#chiko" aria-controls="chiko" role="tab" data-toggle="tab">Chiko</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="laitovo">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#laitovo_standart" aria-controls="laitovo_standart"
                                                              role="tab" data-toggle="tab">Стандарт</a></li>
                    <li role="presentation"><a href="#laitovo_dontlooks" aria-controls="laitovo_dontlooks" role="tab"
                                               data-toggle="tab">Don't Look сдвижной</a></li>
                    <li role="presentation"><a href="#laitovo_dontlookb" aria-controls="laitovo_dontlookb" role="tab"
                                               data-toggle="tab">Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="laitovo_standart">
                        <?= $form->field($model, 'rv_laitovo_standart_status')->checkbox() ?>
                        <?= $form->field($model, 'rv_laitovo_standart_forma')->dropDownList($model->_formafv) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlooks">
                        <?= $form->field($model, 'rv_laitovo_dontlooks_status')->checkbox() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="laitovo_dontlookb">
                        <?= $form->field($model, 'rv_laitovo_dontlookb_status')->checkbox() ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chiko">
            <br>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#chiko_standart" aria-controls="chiko_standart"
                                                              role="tab" data-toggle="tab">Стандарт</a></li>
                    <li role="presentation"><a href="#chiko_dontlooks" aria-controls="chiko_dontlooks" role="tab"
                                               data-toggle="tab">Don't Look сдвижной</a></li>
                    <li role="presentation"><a href="#chiko_dontlookb" aria-controls="chiko_dontlookb" role="tab"
                                               data-toggle="tab">Don't Look бескаркасный</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="chiko_standart">
                        <?= $form->field($model, 'rv_chiko_standart_status')->checkbox() ?>
                        <?= $form->field($model, 'rv_chiko_standart_forma')->dropDownList($model->_formafv) ?>
                        <?= $form->field($model, 'rv_chiko_standart_natyag')->dropDownList($model->_natyag) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlooks">
                        <?= $form->field($model, 'rv_chiko_dontlooks_status')->checkbox() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="chiko_dontlookb">
                        <?= $form->field($model, 'rv_chiko_dontlookb_status')->checkbox() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
</div>