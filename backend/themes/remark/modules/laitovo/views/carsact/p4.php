<?php

use yii\helpers\Json;
use common\models\laitovo\Cars;

?>

<!doctype html>
<html>
<body>

<div class="">
    <div style="width: 100%;text-align: center;"><b><?= $title ?></b></div>
    <div style="width: 100%;text-align: right;">стр.1</div>
</div>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3">АРТ.</td>
        <td class="s3" colspan="11">НАИМЕНОВАНИЕ АВТО</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s5"><b><?= $after['article'] ?></b></td>
        <td class="s5" colspan="11"><b><?= $after['auto']['name'] ?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s5" colspan="12">ИНФОРМАЦИЯ ОБ АНАЛОГАХ</td>
    </tr>
    <? if ($analogs = Json::decode($after['analogs'], $asArray = true))
        foreach ($analogs as $analog):?>
            <tr style='height:19px;'>
                <td class="s17"><?= @Cars::findOne($analog['car'])->article ?></td>
                <td class="s17" colspan="11"><?= $analog['name'] ?> <b><?= implode(',', $analog['elements']) ?></b></td>
            </tr>
        <? endforeach ?>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3" colspan="12">ФОТО УСТАНОВКИ ЗАЖИМОВ-ДЕРЖАТЕЛЕЙ</td>
    </tr>
    <tr style='height:20px;'>
        <td class="s7">ПБ</td>
        <td class="s8" colspan="5">Общий план</td>
        <td class="s7">ПБ</td>
        <td class="s8" colspan="5">Зажим №1</td>
    </tr>
    <tr style='height:265px;'>
        <td class="s9" colspan="6">&nbsp;<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
        <td class="s9" colspan="6"></td>
    </tr>
    <tr style='height:20px;'>
        <td class="s7">ЗБ</td>
        <td class="s8" colspan="5">Общий план</td>
        <td class="s7">ЗБ</td>
        <td class="s8" colspan="5">Зажим № 2</td>
    </tr>
    <tr style='height:265px;'>
        <td class="s9" colspan="6">&nbsp;<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
        <td class="s9" colspan="6"></td>
    </tr>
    </tbody>
</table>

</body>
</html>
