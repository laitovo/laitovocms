<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use common\models\laitovo\CarsAct;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Акты освоения / внесеня изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Автомобили'), 'url' => ['cars/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['car_id', 'created_at'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'car_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->car ? Html::a($data->car->name, ['check-view', 'id' => $data->id]) : null;
            },
        ],

        'created_at:date',
        'updated_at:date',

        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',

        ],
    ],
]); ?>
