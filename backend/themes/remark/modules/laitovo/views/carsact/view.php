<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\CarsAct */

$this->title = Yii::t('app', 'Журнал актов для {car}', ['car' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Автомобили'), 'url' => ['cars/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Акты освоения / внесеня изменений'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'car_id',
            'label'=>'Название автомобиля',
            'format' => 'html',
            'value' => $model->id ? Html::a($model->name, ['cars/update', 'id' => $model->id]) : null,
        ],
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => $model->author ? $model->author->name : null,
        ],
    ],
]) ?>
<div class="btn-group">
    <button type="button" class="btn btn-outline btn-info btn-round dropdown-toggle" data-toggle="dropdown">Акт текущего состояния автомобиля<span
                class="caret"></span></button>
    <ul class="dropdown-menu" role="menu">
        <li><?= Html::a(Yii::t('app', 'Акт текущего состояния автомобиля'), ['actos-last', 'id' => $model->id], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №1'), ['actos-last', 'id' => $model->id, 'p' => '1'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №2'), ['actos-last', 'id' => $model->id, 'p' => '2'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №3'), ['actos-last', 'id' => $model->id, 'p' => '3'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №4'), ['actos-last', 'id' => $model->id, 'p' => '4'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №5'), ['actos-last', 'id' => $model->id, 'p' => '5'], ['target' => '_blank']) ?></li>
        <li><?= Html::a(Yii::t('app', 'Приложение №6'), ['actos-last', 'id' => $model->id, 'p' => '7'], ['target' => '_blank']) ?></li>
    </ul>
</div>

<br>
<br>
<h5>Акт освоения нового автомобиля</h5>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $newCarsAct,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'created_at',
            'format' => 'datetime',
            'label' => Yii::t('app', 'Дата'),
        ],

        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return '
                        <div class="btn-group">
                            <button type="button" class="btn btn-outline btn-info btn-round dropdown-toggle" data-toggle="dropdown">Печать<span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li>' . Html::a(Yii::t('app', 'Акт освоения нового автомобиля'), ['act', 'id' => $model->id], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №1'), ['act', 'id' => $model->id, 'p' => '1'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №2'), ['act', 'id' => $model->id, 'p' => '2'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №3'), ['act', 'id' => $model->id, 'p' => '3'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №4'), ['act', 'id' => $model->id, 'p' => '4'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №5'), ['act', 'id' => $model->id, 'p' => '5'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №6'), ['act', 'id' => $model->id, 'p' => '6'], ['target' => '_blank']) . '</li>
                            </ul>
                        </div>';
                },
            ],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{check}',
            'buttons' => [
                'check'=>function($url,$model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['check-view-act','id'=>$model->id],['target' => '_blank']);
                }
            ],
        ],
    ],
]); ?>



<!--<h5>Акты освоения оконного проема</h5>-->
<?//= GridView::widget([
//    'tableOptions' => ['class' => 'table table-hover'],
//    'dataProvider' => $masteringData,
//    'columns' => [
//        ['class' => 'yii\grid\SerialColumn'],
//
//        [
//            'attribute' => 'created_at',
//            'format' => 'datetime',
//            'label' => Yii::t('app', 'Дата'),
//        ],
//
//        [
//            'attribute' => 'author_id',
//            'value' => function ($data) {
//                return $data->author ? $data->author->name : null;
//            },
//        ],
//
//        [
//            'class' => 'yii\grid\ActionColumn',
//            'template' => '{view}',
//            'buttons' => [
//                'view' => function ($url, $model, $key) {
//                    return '
//                        <div class="btn-group">
//                            <button type="button" class="btn btn-outline btn-info btn-round dropdown-toggle" data-toggle="dropdown">Печать<span class="caret"></span></button>
//                            <ul class="dropdown-menu" role="menu">
//                                <li>' . Html::a(Yii::t('app', 'Акты освоения оконного проема'), ['act', 'id' => $model->id], ['target' => '_blank']) . '</li>
//                                <li>' . Html::a(Yii::t('app', 'Приложение №1'), ['act', 'id' => $model->id, 'p' => '1'], ['target' => '_blank']) . '</li>
//                                <li>' . Html::a(Yii::t('app', 'Приложение №2'), ['act', 'id' => $model->id, 'p' => '2'], ['target' => '_blank']) . '</li>
//                                <li>' . Html::a(Yii::t('app', 'Приложение №3'), ['act', 'id' => $model->id, 'p' => '3'], ['target' => '_blank']) . '</li>
//                                <li>' . Html::a(Yii::t('app', 'Приложение №4'), ['act', 'id' => $model->id, 'p' => '4'], ['target' => '_blank']) . '</li>
//                                <li>' . Html::a(Yii::t('app', 'Приложение №5'), ['act', 'id' => $model->id, 'p' => '5'], ['target' => '_blank']) . '</li>
//                                <li>' . Html::a(Yii::t('app', 'Приложение №6'), ['act', 'id' => $model->id, 'p' => '6'], ['target' => '_blank']) . '</li>
//                            </ul>
//                        </div>';
//                },
//            ],
//        ],
//        [
//            'class' => 'yii\grid\ActionColumn',
//            'template' => '{check}',
//            'buttons' => [
//                'check'=>function($url,$model, $key) {
//                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['check-view-act','id'=>$model->id],['target' => '_blank']);
//                }
//            ],
//        ],
//    ],
//]); ?>

<h5>Акты внесения изменений</h5>
<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $changesData,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'created_at',
            'format' => 'datetime',
            'label' => Yii::t('app', 'Дата'),
        ],

        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return '
                        <div class="btn-group">
                            <button type="button" class="btn btn-outline btn-info btn-round dropdown-toggle" data-toggle="dropdown">Печать<span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li>' . Html::a(Yii::t('app', 'Акт внесеня изменений'), ['act', 'id' => $model->id], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №1'), ['act', 'id' => $model->id, 'p' => '1'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №2'), ['act', 'id' => $model->id, 'p' => '2'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №3'), ['act', 'id' => $model->id, 'p' => '3'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №4'), ['act', 'id' => $model->id, 'p' => '4'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №5'), ['act', 'id' => $model->id, 'p' => '5'], ['target' => '_blank']) . '</li>
                                <li>' . Html::a(Yii::t('app', 'Приложение №6'), ['act', 'id' => $model->id, 'p' => '6'], ['target' => '_blank']) . '</li>
                            </ul>
                        </div>';
                },
            ],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{check}',
            'buttons' => [
                'check'=>function($url,$model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['check-view-act','id'=>$model->id],['target' => '_blank']);
                }
            ],
        ],
    ],
]); ?>


