<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use common\models\laitovo\CarsAct;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Акты освоения / внесеня изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Автомобили'), 'url' => ['cars/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?if (CarsAct::receiveChangesActForCheckCount()):?>
    <?= Html::a(Yii::t('app', 'Проверить <span class="badge">'.CarsAct::receiveChangesActForCheckCount().'</span>'), ['check-changes'], ['class' => 'btn  btn-round  btn-primary']) ?>
<?endif;?>

<?if (CarsAct::receiveChangesActForReworkCount()):?>
    <?= Html::a(Yii::t('app', 'На доработку <span class="badge">'.CarsAct::receiveChangesActForReworkCount().'</span>'), ['rework-changes'], ['class' => 'btn  btn-round  btn-warning']) ?>
<?endif;?>

<?if (CarsAct::receiveMasteringActForCheckCount()):?>
    <?= Html::a(Yii::t('app', 'Проверить <span class="badge">'.CarsAct::receiveMasteringActForCheckCount().'</span>'), ['check-mastering'], ['class' => 'btn  btn-round  btn-primary']) ?>
<?endif;?>

<?if (CarsAct::receiveMasteringActForReworkCount()):?>
    <?= Html::a(Yii::t('app', 'На доработку <span class="badge">'.CarsAct::receiveMasteringActForReworkCount().'</span>'), ['rework-mastering'], ['class' => 'btn  btn-round  btn-warning']) ?>
<?endif;?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name', 'created_at'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function ($data) {
                return $data->id ? Html::a($data->name, ['view', 'id' => $data->id]) : null;
            },
        ],

        'created_at:date',
        'updated_at:date',

        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',

        ],
    ],
]); ?>
