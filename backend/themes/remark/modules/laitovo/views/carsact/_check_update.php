<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */

$this->title = $model->auto->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Автомобили'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $model->auto->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактировать');

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= $this->render('_check_form', [
    'model' => $model,
]) ?>