<?php

use yii\helpers\Json;
use common\models\laitovo\Cars;

?>

<!doctype html>
<html>
<body>

<div class="">
    <div style="width: 100%;text-align: center;"><b><?= $title ?></b></div>
    <div style="width: 100%;text-align: right;">стр.1</div>
</div>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <tbody>
    <tr style='height:19px;background: #ddd;'>
        <td >АРТ.</td>
        <td class="s3" colspan="11" style="width:80%">НАИМЕНОВАНИЕ АВТО</td>
    </tr>
    <tr style='height:19px;'>
        <td ><b><?= $after['article'] ?></b></td>
        <td class="s3" colspan="11" style="width:80%"><b><?= mb_substr(($after['auto']['name']), 0, 75).'...' ?></b></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s5" colspan="12">ИНФОРМАЦИЯ ОБ АНАЛОГАХ</td>
    </tr>
    <? if ($analogs = Json::decode($after['analogs'], $asArray = true))
        foreach ($analogs as $analog):?>
            <tr style='height:19px;'>
                <td class="s17" width="150"><?= @Cars::findOne($analog['car'])->article ?></td>
                <td class="s17" colspan="11"><?= mb_substr($analog['name'], 0, 55).'...' ?> <b><?= implode(',', $analog['elements']) ?></b></td>
            </tr>
        <? endforeach ?>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3" colspan="12">СХЕМА ЗАМЕРОВ ЛОБОВОГО СТЕКЛА</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="12">
            <img src="/img/act/image00.jpg">
            <div style="position: fixed; left: 53mm; top: <?= 119 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[0] ?></td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 51mm; top: <?= 93 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[1] ?></td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 39mm; top: <?= 34 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[2] ?></td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 80mm; top: <?= 41 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[3] ?></td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 78mm; top: <?= 32 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[4] ?></td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 93mm; top: <?= 37 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[5] ?></td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 86mm; top: <?= 89 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[6] ?></td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 131mm; top: <?= 85 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[7] ?></td>
                    </tr>
                </table>
            </div>
            <div style="position: fixed; left: 93mm; top: <?= 96 + count($analogs) * 5 ?>mm;">
                <table>
                    <tr>
                        <td><?= Json::decode($after['fw_scheme'])[8] ?></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3" colspan="12">СХЕМА ЗАМЕРОВ КАПОТА</td>
    </tr>
    <tr style='height:19px;'>
        <td class="s6" colspan="12">&nbsp;<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
    </tr>
    <tr style='height:19px;background: #ddd;'>
        <td class="s3" colspan="12">ПОДПИСИ ОТВЕТСТВЕННЫХ</td>
    </tr>
    <tr style='height:15px;'>
        <td class="s7" style="border: none;">&nbsp;</td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
    </tr>
    <tr style='height:25px;'>
        <td class="s10" colspan="2">РАЗРАБОТЧИК</td>
        <td class="s11" style="border: none;"></td>
        <td class="s8" colspan="5"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" colspan="2"></td>
        <td class="s9" style="border: none;"></td>
    </tr>
    <tr style='height:17px;'>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s7" colspan="5" style="border: none;">ФИО</td>
        <td class="s9" style="border: none;"></td>
        <td class="s7" colspan="2" style="border: none;">подпись</td>
        <td class="s9" style="border: none;"></td>
    </tr>
    <tr style='height:25px;'>
        <td class="s10" colspan="2">МАСТЕР ПРОИЗВОДСТВА</td>
        <td class="s11" style="border: none;"></td>
        <td class="s8" colspan="5"></td>
        <td class="s8" style="border: none;"></td>
        <td class="s8" colspan="2"></td>
        <td class="s9" style="border: none;"></td>
    </tr>
    <tr style='height:16px;'>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s9" style="border: none;"></td>
        <td class="s12" colspan="5" style="border: none;">ФИО</td>
        <td class="s9" style="border: none;"></td>
        <td class="s12" colspan="2" style="border: none;">подпись</td>
        <td class="s9" style="border: none;"></td>
    </tr>
    </tbody>
</table>

</body>
</html>
