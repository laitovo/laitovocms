<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use backend\widgets\GridView;
use common\models\laitovo\Cars;
use common\models\laitovo\CarsAct;
use backend\widgets\ActiveForm;
use backend\widgets\formbuilder\FormBuilder;
/* @var $this yii\web\View */
/* @var $model common\models\laitovo\Cars */

$this->title = $model->auto->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Автомобили'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');

Yii::$app->view->registerJs('
    $(".productions ul.nav-tabs li.visible:first a").click();
    $("body").on("click","a[data-target=\"#edit-car-modal\"]",function(e){
        $("#edit-car-modal input[type=\"checkbox\"]").prop("checked",false);
        $("#edit-car-modal "+$(this).attr("href")).prop("checked",true);
        e.preventDefault();
    });
    $("#edit-car-modal").on("submit","form",function(e){
        if(!$("#edit-car-modal form input[type=\"checkbox\"]:checked").length)
            e.preventDefault();
    });
');


$fields = [];
foreach ($model->auto->fields as $key => $value) {
    if (!in_array($value['fields']['id']['value'], ['fw', 'fv', 'fd', 'rd', 'rv', 'bw', 'info'])) {
        $fields[] = [
            'attribute' => $value['fields']['id']['value'],
            'label' => $value['fields']['label']['value'],
            'format' => $value['title'] == 'Check Box' ? 'raw' : 'raw',
            'contentOptions' => ['class' => isset($model->act->difference['fields'][$value['fields']['id']['value']])?'bg-warning':''],
            'captionOptions' => ['class' => isset($model->act->difference['fields'][$value['fields']['id']['value']])?'bg-warning':''],
            'value' =>  Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $value['fields']['label']['value']])
                               .Html::tag('label',
                                    $value['title'] == 'Check Box' ? 
                                    Html::tag('i', '', ['class' => @$model->act->json('after')['fields'][$value['fields']['id']['value']] ? 'wb-check text-success' : 'wb-close text-danger']) 
                                    : @$model->act->json('after')['fields'][$value['fields']['id']['value']]),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
        ];

        $fields_b[] = [
            'attribute' => $value['fields']['id']['value'],
            'label' => $value['fields']['label']['value'],
            'format' => $value['title'] == 'Check Box' ? 'raw' : 'text',
            'value' => $value['title'] == 'Check Box' ? Html::tag('i', '', ['class' => @$model->act->json('before')['fields'][$value['fields']['id']['value']] ? 'wb-check text-success' : 'wb-close text-danger']) : @$model->act->json('before')['fields'][$value['fields']['id']['value']],
        ];
    }
}
?>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'action' => ['','id' => $model->act->id],
    'options' => ['enableClientValidation' => false, 'id' => 'laitovocarsform']
]); ?>
<?= $form->errorSummary($model) ?>

<div class="hidden">
    <?= $form->field($model, 'mark')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'model')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'generation')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'carcass')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'doors')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'firstyear')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'lastyear')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'modification')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'country')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'category')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fw1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw1')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'clips')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'analogs')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fw_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fw_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fw_scheme')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fv_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_laitovo_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_dontlookb_status')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fv_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fv_chiko_standart_schemem')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_obshivka')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_hlyastik')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_short_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_smoke_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_maxdlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_maxvisota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_maxdlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_maxvisota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chikomagnet_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chikomagnet_magnitov')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_standart_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_standart_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_laitovo_short_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_short_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_short_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_short_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_laitovo_smoke_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_smoke_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_smoke_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_smoke_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_laitovo_mirror_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_laitovo_mirror_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_chiko_mirror_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fd_install_direction')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_with_recess')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'fd_use_tape')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rd_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_obshivka')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_hlyastik')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chikomagnet_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chikomagnet_magnitov')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rd_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_laitovo_standart_schemet')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_chiko_standart_schemet')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rd_install_direction')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_with_recess')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rd_use_tape')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rv_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_obshivka')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_openwindow')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_openwindowtrue')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_hlyastik')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_standart_forma')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_dontlookb_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_forma')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_dontlookb_status')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rv_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_standart_schemem')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rv_laitovo_triangul_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_laitovo_triangul_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_triangul_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'rv_chiko_triangul_schemem')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'bw_date')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_dlina')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_visota')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_magnit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_obshivka')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_openwindow')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_openwindowtrue')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chastei')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_hlyastik')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_simmetr')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_gabarit')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_laitovo_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_laitovo_dontlooks_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_standart_status')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_standart_natyag')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_dontlookb_status')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'bw_laitovo_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_laitovo_standart_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_standart_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_standart_schemem')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'bw_laitovo_double_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_laitovo_double_schemem')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_double_scheme')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'bw_chiko_double_schemem')->hiddenInput()->label(false) ?>

    <?= FormBuilder::widget([
        'form' => $form,
        'model' => $model,
        'attribute' => 'fields',
        'render' => $model->auto->fieldsrender,
    ]); ?>
</div>
<?php ActiveForm::end(); ?>
<? if ($model->act->json('reworkmsg')): ?>
    <div class="alert dark alert-icon alert-warning alert-dismissible alert-alt dark" role="alert">
        <i class="icon wb-warning" aria-hidden="true"
           style="font-size: 30px;margin: -8px;"></i> <?= $model->act->json('reworkmsg') ?>&nbsp;
    </div>
<? endif ?>
<div class="form-group">
<?php if (!$model->act->checked && !isset($hiddenButton)): ?>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'hash-button','form' => 'laitovocarsform']) ?>


<?= Html::submitButton('Отправить на доработку',[
    'class' => ' btn btn-icon btn-outline btn-danger ',
    'form' => 'checkeds',
    'data-original-title' => Yii::t('yii', 'Add'),
]) ?>
<?endif;?>

<?php if ($model->act->checked && $model->act->checked == 2 ): ?>
    <!-- ЕСЛИ АВТОМОБИЛЬ БЫЛ ПРОВЕРЕН И ОТПРАВЛЕН НА ДОРАБОТКУ -->
    <?php Modal::begin([
        // 'size'=>Modal::SIZE_SMALL,
        'id' => 'edit-car-modal',
        'options' => ['class' => 'text-left'],
        'header' => Html::tag('h4', $model->mastered ? Yii::t('app', 'Акт внесеня изменений') : Yii::t('app', 'Акт освоения'), ['class' => 'modal-title']),
        'toggleButton' => ['style' => 'margin-right:5px;', 'class' => ' btn btn-round btn-info btn-outline', 'label' => Yii::t('app', 'Внести изменения')],
    ]); ?>

    <form action="<?= Url::to(['update-changes']) ?>" id="modal">
        <h5>Сведения</h5>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedsteps1" name="steps[]"
                   value="1" />
            <label for="inputCheckedsteps1">Основные</label>
        </div>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedsteps8" name="steps[]"
                   value="8" />
            <label for="inputCheckedsteps8">Дополнительные</label>
        </div>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsclips" name="steps[]" value="9"/>
            <label for="inputCheckedstepsclips">Крепления</label>
        </div>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsanalog" name="steps[]" value="10"/>
            <label for="inputCheckedstepsanalog">Аналоги</label>
        </div>
        <h5>Продукция</h5>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsfw"
                   name="steps[]" <?= $model->_json['fw'] ? '' : 'disabled' ?> value="2" 
                   onchange = "checkBoxing(this,'#inputCheckedstepsfw','<?= CarsAct::ELEMENT_FW?>')"/>
            <label for="inputCheckedstepsfw">ПШ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfwscheme"
                           name="steps[]" <?= $model->_json['fw1'] && $model->_json['fw'] ? '' : 'disabled' ?> value="21"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfw','<?= CarsAct::ELEMENT_FW?>')" />
                    <label for="inputCheckedstepsfwscheme">Схема замеров</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsfv"
                   name="steps[]"<?= $model->_json['fv1'] && $model->_json['fv'] ? '' : 'disabled' ?> value="3"
                   onchange = "checkBoxing(this,'#inputCheckedstepsfv','<?= CarsAct::ELEMENT_FV?>')" />
            <label for="inputCheckedstepsfv">ПФ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfvscheme"
                           name="steps[]"<?= $model->_json['fv1'] && $model->_json['fv']? '' : 'disabled' ?> value="31"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfv','<?= CarsAct::ELEMENT_FV?>')" 
                           />
                    <label for="inputCheckedstepsfvscheme">Схема креплений</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsfd"
                   name="steps[]" <?= $model->_json['fd1'] && $model->_json['fd']? '' : 'disabled' ?> value="4"
                   onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')"
                   />
            <label for="inputCheckedstepsfd">ПБ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfdscheme"
                           name="steps[]" <?= $model->_json['fd1'] && $model->_json['fd']? '' : 'disabled' ?> value="41"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')" 
                           />
                    <label for="inputCheckedstepsfdscheme">Схема креплений</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfd2scheme"
                           name="steps[]" <?= $model->_json['fd1'] && $model->_json['fd']? '' : 'disabled' ?> value="42"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')"
                           />
                    <label for="inputCheckedstepsfd2scheme">Схема креплений (Укороченный)</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfd3scheme"
                           name="steps[]" <?= $model->_json['fd1'] && $model->_json['fd']? '' : 'disabled' ?> value="43"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')"
                           />
                    <label for="inputCheckedstepsfd3scheme">Схема креплений (Вырез для курящих)</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsfd4scheme"
                           name="steps[]" <?= $model->_json['fd1'] && $model->_json['fd']? '' : 'disabled' ?> value="44"
                           onchange = "checkBoxing(this,'#inputCheckedstepsfd','<?= CarsAct::ELEMENT_FD?>')"
                           />
                    <label for="inputCheckedstepsfd4scheme">Схема креплений (Вырез для зеркала)</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsrd"
                   name="steps[]" <?= $model->_json['rd1'] && $model->_json['rd']? '' : 'disabled' ?> value="5"
                   onchange = "checkBoxing(this,'#inputCheckedstepsrd','<?= CarsAct::ELEMENT_RD?>')"
                   />
            <label for="inputCheckedstepsrd">ЗБ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsrdscheme"
                           name="steps[]" <?= $model->_json['rd1'] && $model->_json['rd']? '' : 'disabled' ?> value="51"
                           onchange = "checkBoxing(this,'#inputCheckedstepsrd','<?= CarsAct::ELEMENT_RD?>')"
                           />
                    <label for="inputCheckedstepsrdscheme">Схема креплений</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsrv"
                   name="steps[]" <?= $model->_json['rv1'] && $model->_json['rv']? '' : 'disabled' ?> value="6"
                   onchange = "checkBoxing(this,'#inputCheckedstepsrv','<?= CarsAct::ELEMENT_RV?>')"
                   />
            <label for="inputCheckedstepsrv">ЗФ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsrvscheme"
                           name="steps[]" <?= $model->_json['rv1'] && $model->_json['rv']? '' : 'disabled' ?> value="61"
                           onchange = "checkBoxing(this,'#inputCheckedstepsrv','<?= CarsAct::ELEMENT_RV?>')"
                           />
                    <label for="inputCheckedstepsrvscheme">Схема креплений</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsrv2scheme"
                           name="steps[]" <?= $model->_json['rv1'] && $model->_json['rv']? '' : 'disabled' ?> value="62"
                           onchange = "checkBoxing(this,'#inputCheckedstepsrv','<?= CarsAct::ELEMENT_RV?>')"
                           />
                    <label for="inputCheckedstepsrv2scheme">Схема креплений (Треугольная)</label>
                </div>
            </li>
        </ul>
        <div class="checkbox-custom checkbox-primary">
            <input type="checkbox" id="inputCheckedstepsbw"
                   name="steps[]" <?= $model->_json['bw1'] && $model->_json['bw']? '' : 'disabled' ?> value="7"
                   onchange = "checkBoxing(this,'#inputCheckedstepsbw','<?= CarsAct::ELEMENT_BW?>')"
                   />
            <label for="inputCheckedstepsbw">ЗШ</label>
        </div>
        <ul>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsbwscheme"
                           name="steps[]" <?= $model->_json['bw1'] && $model->_json['bw']? '' : 'disabled' ?> value="71"
                           onchange = "checkBoxing(this,'#inputCheckedstepsbw','<?= CarsAct::ELEMENT_BW?>')"
                           />
                    <label for="inputCheckedstepsbwscheme">Схема креплений</label>
                </div>
            </li>
            <li>
                <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputCheckedstepsbw2scheme"
                           name="steps[]" <?= $model->_json['bw1'] && $model->_json['bw']? '' : 'disabled' ?> value="72"
                           onchange = "checkBoxing(this,'#inputCheckedstepsbw','<?= CarsAct::ELEMENT_BW?>')"
                           />
                    <label for="inputCheckedstepsbw2scheme">Схема креплений (2 части)</label>
                </div>
            </li>
        </ul>

        <input type="hidden" name="id" value="<?= $model->act->id ?>">
        <input type="hidden" name="step" value="1">
        <input type="hidden" name="elem" value="">

        <div class="pull-right text-right form-group">
            <div class="">Ответственный:</div>
            <h4 class=""> <?= $this->renderDynamic('return Yii::$app->user->identity->name;') ?></h4>
        </div>
        <br>
        <button type="submit" class="btn btn-outline btn-round btn-primary">Далее</button>
    </form>

    <?php Modal::end(); ?>        

<?endif;?>
</div>

<?= Html::beginForm(['change-check-rework', 'id' => $model->act->id], 'post', ['data-pjax' => '', 'id' => 'checkeds']); ?>


<!-- Блок основных сведений -->
<div class="row">
    <div class="col-md-6">
        <h4>Основные сведения (после)</h4>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('article'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['article'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['article'])?'bg-success':''],
                    'value' => 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('article')])
                           .Html::tag('label',($model->article)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),           
                ],  
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('mark'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['mark'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['mark'])?'bg-success':''],
                    'value' =>

                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('mark')])
                           .Html::tag('label',($model->mark)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),            
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('mark_en'),
                    'contentOptions' => ['class' => $model->mark_en != $model->mark_en_b ? 'bg-success':''],
                    'contentOptions' => ['class' => $model->mark_en != $model->mark_en_b ? 'bg-success':''],
                    'value' => 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('mark_en')])
                           .Html::tag('label',($model->mark_en)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),           
                ],               
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('model'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['model'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['model'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('model')])
                               .Html::tag('label',($model->model)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('model_en'),
                    'contentOptions' => ['class' => $model->model_en != $model->model_en_b ?'bg-success':''],
                    'contentOptions' => ['class' => $model->model_en != $model->model_en_b ?'bg-success':''],
                    'value' => 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('model_en')])
                           .Html::tag('label',($model->model_en)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),           
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('generation'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['generation'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['generation'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('generation')])
                               .Html::tag('label',($model->generation)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('carcass'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['carcass'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['carcass'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('carcass')])
                               .Html::tag('label',($model->carcass)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('doors'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['doors'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['doors'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('doors')])
                               .Html::tag('label',($model->doors)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('firstyear'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['firstyear'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['firstyear'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('firstyear')])
                               .Html::tag('label',($model->firstyear)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('lastyear'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['lastyear'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['lastyear'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('lastyear')])
                               .Html::tag('label',($model->lastyear)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('modification'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['modification'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['modification'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('modification')])
                               .Html::tag('label',($model->modification)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('modification_en'),
                    'contentOptions' => ['class' => $model->modification_en != $model->modification_en_b ? 'bg-success':''],
                    'contentOptions' => ['class' => $model->modification_en != $model->modification_en_b ? 'bg-success':''],
                    'value' => 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('modification_en')])
                           .Html::tag('label',($model->modification_en)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),           
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('country'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['country'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['country'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('country')])
                               .Html::tag('label',($model->country)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('country_en'),
                    'contentOptions' => ['class' => $model->country_en != $model->country_en_b? 'bg-success':''],
                    'contentOptions' => ['class' => $model->country_en != $model->country_en_b? 'bg-success':''],
                    'value' => 
                    Html::tag('div',
                            Html::checkbox("message[]",
                                $checked = false, 
                                ['value' => $model->getAttributeLabel('country_en')])
                           .Html::tag('label',($model->country_en)),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    ),           
                ], 
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('category'),
                    'contentOptions' => ['class' => isset($model->act->difference['auto']['category'])?'bg-success':''],
                    'captionOptions' => ['class' => isset($model->act->difference['auto']['category'])?'bg-success':''],
                    'value' =>
                        Html::tag('div',
                                Html::checkbox("message[]",
                                    $checked = false, 
                                    ['value' => $model->getAttributeLabel('category')])
                               .Html::tag('label',($model->category)),
                                ['class' => 'checkbox-custom checkbox-primary text-left']
                        ),
                ],
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
        <h4>Основные сведения (до)</h4>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('article_b'),
                    'value' => $model->article_b,            
                ],  
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('mark_b'),
                    'value' => $model->mark_b,            
                ],    
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('mark_en_b'),
                    'value' => $model->mark_en_b
                ],           
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('model_b'),
                    'value' => $model->model_b,             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('model_en_b'),
                    'value' => $model->model_en_b
                ], 
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('generation_b'),
                    'value' => $model->generation_b,              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('carcass_b'),
                    'value' => $model->carcass_b,              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('doors_b'),
                    'value' => $model->doors_b,             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('firstyear_b'),
                    'value' => $model->firstyear_b,             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('lastyear_b'),
                    'value' => $model->lastyear_b,              
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('modification_b'),
                    'value' => $model->modification_b,             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('modification_en_b'),
                    'value' => $model->modification_en_b
                ], 
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('country_b'),
                    'value' => $model->country_b,             
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('country_en_b'),
                    'value' => $model->country_en_b 
                ],
                [  // name свойство зависимой      модели owner
                    'format' => 'raw',
                    'label' => $model->getAttributeLabel('category_b'),
                    'value' => $model->category_b,             
                ],
            ],
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>

<?if (
       isset($model->act->difference['fw1']) 
    || isset($model->act->difference['fv1']) 
    || isset($model->act->difference['fd1']) 
    || isset($model->act->difference['rd1']) 
    || isset($model->act->difference['rv1']) 
    || isset($model->act->difference['bw1']) 
):?>
<!-- Блок схемы проемов -->
<div class="row">
    <div class="col-md-6">
        <h4>Схема проемов (после)</h4>
        <div class="col-md-12">
            <img width="100%" src="/img/car.png" style="position: relative;">
            <?= @$model->_json['fw1'] ? '<img width="100%" src="/img/fw.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json['fv1'] ? '<img width="100%" src="/img/fv.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json['fd1'] ? '<img width="100%" src="/img/fd.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json['rd1'] ? '<img width="100%" src="/img/rd.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json['rv1'] ? '<img width="100%" src="/img/rv.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json['bw1'] ? '<img width="100%" src="/img/bw.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>

            <a href="#inputCheckedstepsfw" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json['fw1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json['fw'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 26%;top: 62%;"></i></a>
            <a href="#inputCheckedstepsfv" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json['fv1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json['fv'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 32.5%;top: 17%;"></i></a>
            <a href="#inputCheckedstepsfd" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json['fd1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json['fd'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 45%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsrd" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json['rd1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json['rd'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 60%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsrv" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json['rv1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json['rv'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 70%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsbw" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json['bw1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json['bw'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 71%;top: 60%;"></i></a>
        </div>
        <div class="clearfix"></div>
        <? if ($model->_json['info']): ?>
            <div class="alert dark alert-icon alert-warning alert-dismissible alert-alt dark" role="alert">
                <i class="icon wb-warning" aria-hidden="true"
                   style="font-size: 30px;margin: -8px;"></i> <?= $model->_json['info'] ?>&nbsp;
            </div>
        <? endif ?>
    </div>
    <div class="col-md-6">
        <h4>Схема проемов (до)</h4>
        <div class="col-md-12">
            <img width="100%" src="/img/car.png" style="position: relative;">
            <?= @$model->_json_b['fw1'] ? '<img width="100%" src="/img/fw.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json_b['fv1'] ? '<img width="100%" src="/img/fv.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json_b['fd1'] ? '<img width="100%" src="/img/fd.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json_b['rd1'] ? '<img width="100%" src="/img/rd.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json_b['rv1'] ? '<img width="100%" src="/img/rv.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>
            <?= @$model->_json_b['bw1'] ? '<img width="100%" src="/img/bw.png" style="position: absolute;left: 0;top: -15px;padding: 15px">' : '' ?>

            <a href="#inputCheckedstepsfw" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json_b['fw1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json_b['fw'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 26%;top: 62%;"></i></a>
            <a href="#inputCheckedstepsfv" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json_b['fv1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json_b['fv'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 32.5%;top: 17%;"></i></a>
            <a href="#inputCheckedstepsfd" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json_b['fd1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json_b['fd'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 45%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsrd" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json_b['rd1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json_b['rd'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 60%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsrv" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json_b['rv1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json_b['rv'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 70%;top: 12%;"></i></a>
            <a href="#inputCheckedstepsbw" data-toggle="modal" data-target="#edit-car-modal"
               class="<?= @$model->_json_b['bw1'] ? '' : 'hidden' ?>"><i
                        class="fa fa-circle <?= $model->_json_b['bw'] ? 'text-success' : 'text-danger' ?>"
                        aria-hidden="true" style="position: absolute;left: 71%;top: 60%;"></i></a>
        </div>
        <div class="clearfix"></div>
        <? if ($model->_json_b['info']): ?>
            <div class="alert dark alert-icon alert-warning alert-dismissible alert-alt dark" role="alert">
                <i class="icon wb-warning" aria-hidden="true"
                   style="font-size: 30px;margin: -8px;"></i> <?= $model->_json_b['info'] ?>&nbsp;
            </div>
        <? endif ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<?if (isset($model->act->difference['fields']) && count($model->act->difference['fields'])):?>
<!-- Блок дополнительных сведений -->
<div class="row">
    <div class="col-md-6">
        <h4>Дополнительные сведения (после)</h4>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => 
                ArrayHelper::merge([
 
                ],$fields),
        ]) ?>
    </div>
    <div class="col-md-6">
        <h4>Дополнительные сведения (до)</h4>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => 
                ArrayHelper::merge([

                ],$fields_b),
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<!-- Блок креплений -->
<? if (isset($model->act->difference['clips'])): ?>
<div class="row">
    <div class="col-md-6">
        <h4>Крепления (после)</h4>
        <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $clipsProvider,
            'show' => ['name', 'type'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                'type',
            ],
        ]); ?>
    </div>
    <div class="col-md-6">
        <h4>Крепления (до)</h4>
        <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $clipsProvider_b,
            'show' => ['name', 'type'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                'type',
            ],
        ]); ?>
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<!-- Блок аналогов -->
<? if (isset($model->act->difference['analogs'])): ?>
<div>
    <div class="col-md-12">
        <h4>Аналоги (после)</h4>
        <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $analogsProvider,
            'show' => ['name', 'elements'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                    'format' => 'html',
                    'label' => 'Наименование',
                    'value' => function ($data) {
                    return ($car = Cars::findOne($data['car'])) ? Html::a($car->name, ['cars/update', 'id' => $car->id]):'';
                    },
                ],
                [
                    'attribute' => 'elements',
                    'label' => Yii::t('app', 'Элементы'),
                    'format' => 'html',
                    'value' => function ($data) {
                    return implode(', ', $data['elements']);
                    },
                ],
            ],
        ]); ?>
    </div> 
    <div class="col-md-12">
        <h4>Аналоги (до)</h4>
        <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $analogsProvider_b,
            'show' => ['name', 'elements'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                    'format' => 'html',
                    'label' => 'Наименование',
                    'value' => function ($data) {
                    return ($car = Cars::findOne($data['car'])) ? Html::a($car->name, ['cars/update', 'id' => $car->id]):'';
                    },
                ],
                [
                    'attribute' => 'elements',
                    'label' => Yii::t('app', 'Элементы'),
                    'format' => 'html',
                    'value' => function ($data) {
                    return implode(', ', $data['elements']);
                    },
                ],
            ],
        ]); ?>
    </div> 
</div>
<div class="clearfix"></div>
<hr>
<?endif;?>

<!-- Блок продукции -->
        <div class="col-md-12 productions">
            <?= $this->render('_check_view_2', [
                'model' => $model,
            ]) ?>

            <?= $this->render('_check_view_3', [
                'model' => $model,
            ]) ?>

            <?= $this->render('_check_view_4', [
                'model' => $model,
            ]) ?>

            <?= $this->render('_check_view_5', [
                'model' => $model,
            ]) ?>

            <?= $this->render('_check_view_6', [
                'model' => $model,
            ]) ?>

            <?= $this->render('_check_view_7', [
                'model' => $model,
            ]) ?>

        </div>
<?= Html::endForm() ?>
       <?php if (!$model->act->checked && !isset($hiddenButton)): ?>
<div class="form-group">


<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'hash-button','form' => 'laitovocarsform']) ?>


<?= Html::submitButton('Отправить на доработку',[
    'class' => ' btn btn-icon btn-outline btn-danger ',
    'form' => 'checkeds',
    'data-original-title' => Yii::t('yii', 'Add'),
]) ?>
</div>
<?endif;?> 
</div>





