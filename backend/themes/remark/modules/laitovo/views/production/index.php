<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpScheme;
use backend\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\ErpProductType;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->render('../menu');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="production-scheme-index">

    <h1><?= Html::encode('Журнал карточек продуктов') ?></h1>
     
    <?php  ActiveForm::begin([
        'method' => 'get',
    ]); ?>
        <a href="<?=Url::to('materials')?>" class="pull-right btn btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="<?=Yii::t('app', 'Сбросить')?>"><i class="icon wb-refresh" aria-hidden="true"></i></a> 
        <div class="input-search input-search-dark pull-right"><i class="input-search-icon wb-search" aria-hidden="true"></i><input type="text" class="form-control" value="<?=Yii::$app->request->get('search')?>" name="search" placeholder="<?=Yii::t('app', 'Поиск')?>"></div>
    <?php ActiveForm::end(); ?>
   
    <p>
       <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

    </p>
    
   
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($data) {
            if($data->product_id){
                if ($data->product->production_scheme_id !=null) {
                    return ['class' => 'info'];
                }
            }
        },
        'show'=>['title',  'product_id', 'article' , 'scheme_id', 'author_id', 'created_at','product.category'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
       
            [
               'attribute' => 'title', 
               'label' =>'Название',
               'format' =>'html',
               'value' =>function($data){
                   return $data->title ? Html::a(Html::encode($data->title), ['view', 'id' => $data->id]): ''; 
               }             
            ],
            
            [
               'attribute' => 'product_id', 
               'label' =>'Вид продукта',
               'format' =>'html',
               'filter'=> ArrayHelper::map(ErpProductType::find()->all(), 'id', 'title'),
               'value' =>function($data){
                   return $data->product_id ? Html::encode($data->product->title): ''; 
               }             
            ],
            'product.category',

            [
                'attribute'=>'article',
                'label'=>'Артикул продукта',
                'value'=>function($data){
                    return $data->product_id ? ($data->product->article ? $data->product->article : ($data->product->rule ? $data->product->articleRule : '')) : '';
                }
            ],
            [
                'attribute'=>'total_count',
                'label'=>'Общая стоимость',
                'value'=>function($data){
                    return $data->materialCount() || $data->totalJobCount() ? round(@$data->materialCount()+@$data->totalJobCount(),2) : '';
                }
            ],
            [
                'attribute'=>'material_count',
                'label'=>'Стоимость материалов',
                'value'=>function($data){
                    return $data->materialCount()? round(@$data->materialCount(),2) : '';
                }
            ],
            [
                'attribute'=>'job_count',
                'label'=>'Стоимость работ',
                'value'=>function($data){
                    return $data->totalJobCount()? round(@$data->totalJobCount(),2) : '';
                }
            ],
            [
               'attribute' => 'scheme_id', 
               'label' =>'Название маршрута',
               'format' =>'html',
               'value' =>function($data){
                   return $data->scheme->name ? Html::encode($data->scheme->name): ''; 
               }             
            ],
            [
               'attribute' => 'author_id', 
               'label' =>'Автор записи',
               'format' =>'html',
               'value' =>function($data){
                   return $data->author->name ? Html::encode($data->author->name): ''; 
               }             
            ],
            [
               'attribute' => 'created_at', 
               'label' =>'Дата',
               'format' =>'html',
               'value' =>function($data){
                   return $data->created_at ? Yii::$app->formatter->asDateTime($data->created_at) : ''; 
               }             
            ],
            
            // 'created_at',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
