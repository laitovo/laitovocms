<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\laitovo\models\ErpProductType;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use common\models\laitovo\ErpLocation;
use yii\helpers\Json;
use backend\modules\laitovo\models\ErpScheme;
use common\models\laitovo\ProductionScheme;

?>

<div class="production-scheme-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
   <?=Html::button('<i class="glyphicon glyphicon-plus"></i>', ['id'=>'product_button','class' => 'btn btn-icon btn-outline btn-round btn-warning btn-xs' , 'data-toggle' => "tooltip",
         'data-original-title' => Yii::t('app', 'Добавить новый продукт'),
         'onclick' => '                
                    $("#new_product_type").modal("show");
                  
        ' ]) ?>
   

     <?= $form->field($model, 'product_id')->dropDownList(
        $model->isNewRecord ?  ArrayHelper::map(ErpProductType::find()->where(['production_scheme_id' => null])->all(), 'id', 'title'): 
         ArrayHelper::map(ErpProductType::find()->where(['or',['production_scheme_id' => null], ['production_scheme_id' => $model->id]])->all(), 'id', 'title'))?>
   
     <?= $form->field($model, 'scheme_id')->dropDownList(ArrayHelper::map(ErpScheme::find()->all(), 'id', 'name')) ?>
           
     
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
     <?php Modal::begin([
            'header'=> '<h2>Добавьте новый продукт</h2>',
            'id' => 'new_product_type'
        ])?>
    <div>
        <div id="validate-message" style="font-size: 16px; color: red">
            
        </div>
             <?= Html::label('Название продукта', $model2->title, ['class' => 'label-default']) ?>
        
             <?= Html::input('text', 'erpproducttype-title', $model2->title, ['class' => 'form-control', 'id'=>'erpproducttype-title',  ]) ?>
        
             <?= Html::label('Правило', $model2->title, ['class' => ' label-default']) ?>
              
              <?= Html::input('rule', 'erpproducttype-rule', $model2->title, ['class' => 'form-control', 'id'=>'erpproducttype-rule', ]) ?>
        
    </div>
    <? if($model->isNewRecord): ?>
    <?=Html::button('Сохранить', ['id'=>'scheme_button','class' => 'btn  btn-outline  btn-warning ' ,
        'data-original-title' => Yii::t('app', 'Добавить новый маршрут'),
        'onclick' => '                
                     $.post("' . Url::to(['/ajax/save-new-product-type']) . '",{"title":$("#erpproducttype-title").val(), "rule": $("#erpproducttype-rule").val()},function(data){
                         if(data)
                         {
                            if(data.id)
                            {
                                 $("#new_product_type").modal("hide");
                            }
                            if(data.title)
                             {
                                 notie.alert(1,"Продукт "+data.title+" сохранен",4);
                             }
                             if(data.value)
                             {
                                $("#productionscheme-product_id").html("");
                                $("#productionscheme-product_id").html("<option></option>");    
                                $.each(data.value, function(key, value) {
                                     $("#productionscheme-product_id").append($("<option></option>").attr("value",key).text(value));    
                                });
                                $("#productionscheme-product_id").val(data.id);
                            }
                            if(data.message)
                            {
                                $("#validate-message").text(data.message)
                            }
                        }else{
                            $("#new_product_type").modal("hide");
                            notie.alert(3,"Произошла ошибка",4);
                        }
                            
                                
                            
                     });
          //   alert($("#erpproducttype-title").val()+$("#erpproducttype-rule").val());
                  
        ']) ?>
    <? else: ?>
    <?=Html::button('Сохранить', ['id'=>'scheme_button','class' => 'btn  btn-outline  btn-warning ' ,
        'data-original-title' => Yii::t('app', 'Добавить новый маршрут'),
        'onclick' => '                
                     $.post("' . Url::to(['/ajax/save-new-product-type']) . '",{"title":$("#erpproducttype-title").val(), "rule": $("#erpproducttype-rule").val(), "production_id": "'.$model->id.'"},function(data){
                         if(data)
                         {
                            if(data.id)
                            {
                                 $("#new_product_type").modal("hide");
                            }
                            if(data.title)
                             {
                                 notie.alert(1,"Продукт "+data.title+" сохранен",4);
                             }
                             if(data.value)
                             {
                                $("#productionscheme-product_id").html("");
                                $("#productionscheme-product_id").html("<option></option>");    
                                $.each(data.value, function(key, value) {
                                     $("#productionscheme-product_id").append($("<option></option>").attr("value",key).text(value));    
                                });
                                $("#productionscheme-product_id").val(data.id);
                            }
                            if(data.message)
                            {
                                $("#validate-message").text(data.message)
                            }
                        }else{
                            $("#new_product_type").modal("hide");
                            notie.alert(3,"Произошла ошибка",4);
                        }
                            
                                
                            
                     });
          //   alert($("#erpproducttype-title").val()+$("#erpproducttype-rule").val());
                  
        ']) ?>
    <? endif ?>

    <?php Modal::end()?>
    
</div>
