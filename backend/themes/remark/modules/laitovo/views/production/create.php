<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\laitovo\ProductionScheme */

$this->params['breadcrumbs'][] = ['label' => 'Журнал карточек продуктов', 'url' => ['index']];
$this->render('../menu');
?>
<div class="production-scheme-create">

    <h1><?= Html::encode('Создать новую карту продукта') ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model2' => $model2,
    ]) ?>
</div>
