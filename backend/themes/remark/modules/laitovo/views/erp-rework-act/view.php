<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\laitovo\ErpLocation;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\LaitovoReworkAct */
$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');
$this->params['breadcrumbs'][] = ['label' => 'Журнал актов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name];
$this->render('../menu');

Yii::$app->view->registerJs('
        
        $(window).ready(function(){        
            $("#laitovoreworkact-location_start").val("'. $model->location_defect .'");
        });

    ', \yii\web\View::POS_END);

?>
<div class="laitovo-rework-act-view">
    <div style="font-size:30px"><?php echo $model->name ?></div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'created_at:datetime',
            [
            'attribute' => 'naryad_id',
            'format' => 'html',
            'value' => $model->naryad->name ? Html::a(Html::encode($model->naryad->name), ['erp-naryad/view', 'id' => $model->naryad_id]) : null,
            ],
            'compiledUser.name',
            'foundUser.name',                      
            'madeUser.name',
            'deacription_problem',
            'cause_problem',
            'locationDefect.name',
//            'real_quilty',
            'amount_damage',
            'deduction',
        ],
    ]) ?>
    

<? if (!$model->location_start):?>   
<?php $form = ActiveForm::begin(); ?>
    <div>
        <div >
        <span style="font-size: 25px">
            Вернуть на участок:
        </span> 
                <?= $form->field($model, 'location_start')->dropDownList(ArrayHelper::map(ErpLocation::find()->where(['!=','id',Yii::$app->params['erp_dispatcher']])->all(), 'id', 'name'),
                        [
                        'style'=>[
                        'font-size' => '25px',
                        'height' => '40px',
                        'width'=>'200px',
                        'color'=>'#FF4500',    
                        'position'=>'absolute',    
                        'left'=>'300px',
                        'bottom'=>'85px',
                            
                        ],
                         
                    ])->label(false) ?>
            
        
        </div>   
        <div style="position:relative; left: 500px; bottom: 50px ">
           
        <?= Html::button(  $label = 'Далее', $options = [      
                    'class' => 'btn  btn-lg btn-warning',
                    'style'=>
                    [
                        'font-size'=>'15px',
                        'width'=>'100px',
                        'height'=>'40px',
                        'position'=>'absolute',
                        'left'=>'60px',    
                    ],
                    'onclick'=>'
                        
                        $.get("' . Url::to(['/ajax/reload-naryad']) . '",{"naryad_id":"'. $model->naryad_id .'", "location_start":$("#laitovoreworkact-location_start option:selected").val(), "id": "'.$model->id.'"},function(data){
                               if(data){
                               printJS("' . Url::to(['print-covering-letter']) . '?id="+"' .$model->id . '");
                               
                                    setTimeout(function(){location.reload()},500);
                                  
                            }
                            
                        });
                            
                    '
            
               ] ) ?>
        </div>     
    </div> 
<?php ActiveForm::end(); ?>
<? else: ?>

 <div>
        <div >
        <span style="font-size: 30px">
            Наряд успешно направлен на участок: 
            <span style="color:#FF4500">
                    <?= $model->locationStart ? $model->locationStart->name : '' ?>
            </span>
            <?= Html::a(Yii::t('app', 'Перейти к журналу актов'), ['index'], ['class' => 'btn   btn-warning']) ?>
        </span> 
        </div>
 </div>
<? endif ?>
     <p>
       <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>
       <?= Html::a(Yii::t('app', 'Распечатать акт'), ['print-rework', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-info', 'target' => '_blank']) ?>

<? if ($model->location_start):?>
       <?= Html::a(Yii::t('app', 'Распечатать сопроводительное письмо'), ['print-covering-letter', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-info', 'target' => '_blank']) ?>
<? endif;?>
       <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id],[
        'class' => 'deleteconfirm pull-right btn btn-outline btn-round btn-danger',
        'style'=>'margin-right:5px',
        'data' => [
            'confirm' => Yii::t('yii', 'Отменить этот акт?'),
            'method' => 'post',
        ]]) ?>
         
    </p>

</div>
