<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\LaitovoReworkAct */


$this->params['breadcrumbs'][] = ['label' => 'Laitovo Rework Acts', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');
?>
<div class="laitovo-rework-act-create">
<div style="font-size:30px">Создание Акта Переделки</div>
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
