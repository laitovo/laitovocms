<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpNaryad;
use yii\helpers\ArrayHelper;
use common\models\laitovo\ErpLocation;
use backend\modules\laitovo\models\ErpUser;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Журнал актов';
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');
?>
<p>
    <?= Html::a('<i class="icon wb-plus"></i>', ['terminal'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Оформить переделку')]) ?>

</p>
<!--<div class="laitovo-rework-act-index">-->
<!--   <div style="font-size:30px">Журнал актов</div>-->
<!--    --><?//= GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' =>$searchModel,
//        'rowOptions' => function ($model, $index, $widget, $grid){
//            if ($model->location_start):
//                return ['class' => 'success'];
//            else:
//                return ['class' => 'info'];
//            endif;
//         },
//        'show' => [ 'rework_number','naryad_id', 'found_user_id','made_user_id','amount_damage','location_defect',],
//        'columns' => [
//            [
//                'attribute' =>'created_at',
//                'label' =>'Дата создания акта',
//                'filter'=>false,
//                'value' =>function ($data) {
//                return $data->created_at ? Yii::$app->formatter->asDateTime($data->created_at) : '';
//                },
//            ],
//            [
//                'attribute' =>'rework_number',
//                'format'=>'html',
//                // 'filter'=>false,
//                'value'=>function ($data) {
//                return $data->name ? Html::a(Html::encode($data->name), ['view', 'id' => $data->id]) : null;
//            },
//            ],
//            [
//                'attribute' =>'naryad_id',
//                'format'=>'html',
//                'filter'=>false,
//                'value'=>function ($data) {
//                return $data->naryad->name ? Html::a(Html::encode($data->naryad->name), (Yii::$app->request->get('naryad_id') ? Yii::$app->urlManager->createUrl(['laitovo/erp-naryad/view','id' => Yii::$app->request->get('naryad_id')]) : ['naryad-act', 'naryad_id' => $data->naryad_id])) : null;
//            },
//            ],
//            [
//                'attribute'=>'windowType',
//                'label'=>'Название продукта',
//                'filter'=> ErpNaryad::filterTypeStatic(),
//                'value'=>function ($data) {
//                    return ErpNaryad::windowTypeStatic($data->naryad->article);
//                },
//            ],
//            [
//                'attribute'=>'window',
//                'label'=>'Оконный проем',
//               'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
//                'value'=>function ($data) {
//                    return ErpNaryad::windowStatic($data->naryad->article);
//                },
//            ],
//            [
//                'attribute'=>'tkan',
//                'label'=>'Тип ткани',
//                'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
//                'value'=>function ($data) {
//                    return ErpNaryad::tkanStatic($data->naryad->article);
//                },
//            ],
//
//            [
//              'attribute'=>'location_defect',
//              'value' => 'locationDefect.name',
//              'filter' => ArrayHelper::map(ErpLocation::find()->all(),'id', 'name'),
//            ],
//            [
//              'attribute'=>'location_start',
//              'value' => 'locationStart.name',
//              'filter' => ArrayHelper::map(ErpLocation::find()->all(),'id', 'name'),
//            ],
//            [
//              'attribute'=>'found_user_id',
//              'value' => 'foundUser.name',
//              'filter'=>ErpUser::userInWork(),
//            ],
//            [
//              'attribute'=>'made_user_id',
//              'value' => 'madeUser.name',
//              'filter'=> ErpUser::userInWork(),
//            ],
//            [
//              'attribute'=>'compiled_user',
//              'value' => 'compiledUser.name',
//              'filter'=> ErpUser::userInWork(),
//            ],
//            [
//                'attribute'=>'deacription_problem',
//                'filter'=>false,
//            ],
//            [
//                'attribute'=>'cause_problem',
//                'filter'=>false,
//            ],
//            [
//                'attribute'=>'real_quilty',
//                'filter'=>false,
//            ],
//            [
//                'attribute'=>'amount_damage',
//                'filter'=>false,
//            ],
//            [
//                'attribute'=>'deduction',
//                'filter'=>false,
//            ],
//
//
//        ],
//    ]); ?>
<!--</div>-->
