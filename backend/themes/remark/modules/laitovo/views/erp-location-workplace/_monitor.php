<?php

use backend\modules\laitovo\models\ErpNaryad;
use common\models\laitovo\ErpNaryadLog;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpLocationWorkplace;
use backend\modules\laitovo\models\ErpLocationWorkplacePlan;
use yii\helpers\Url;  
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\ErpLocationWorkplaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рабочие места';
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

$customScript = <<< SCRIPT
    $(function(){
    $('.custom_button').click(function(){
        $('#modalView').modal('show').find('#modalContentView').load($(this).attr('value'));

    });});
SCRIPT;
$this->registerJs($customScript, \yii\web\View::POS_READY);

if (isset($location_id) && $location_id){
    $customScript = "
        $(function(){
            $('.location-".$location_id." a').click();
        });
    ";
    $this->registerJs($customScript, \yii\web\View::POS_READY);    
}

$hasAccess = in_array(Yii::$app->user->getId(), [2, 21, 36]);

//вывод модального окна
Modal::begin(['id'=>'modalView', 'size'=>'modal-md']);
Pjax::begin(['enablePushState' => false]);
echo "<div id='modalContentView'></div>";
Pjax::end();
Modal::end();
//вывод модального окна
?>
<div class="erp-location-workplace-index">
    <? $locations = ErpLocation::find()
    ->where(['type' => ErpLocation::TYPE_PRODUCTION])
    ->orderby('sort')
    ->all();

    $content = '';
    $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: black; color: white;" title="Всего">Всего сдано</span>';
    $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: green;" title="Стандартные шторки">Стандартных шторок</span>';
    $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: black;" title="Don\'t look и бескаркасники">Don\'t look и бескаркасники</span>';
    $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: blue;" title="Органайзеры">Органайзеры</span>';
    $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: red;" title="Прочее">Прочее</span>';


    $content_production = "<div class='bg-success' style='display: inline-block; padding: 10px;'> Легенда цветов $content</div>";
    if ($hasAccess) {
        $content_production .= "<div style='margin-top:10px'>" . Html::a('Снять работников со всех участков разом', ['cancel-users-from-workplaces'], [
            'target' => '_blank',
            'class' => 'btn btn-danger',
            'data-pjax' => '',
            'data-toggle' => 'tooltip',
            'data-original-title' => Yii::t('app', 'Снять работников со всех участков разом'),
            'data' => [
                'confirm' => Yii::t('yii', 'Вы действительно хотите снять работников со всех участков разом?'),
                'method' => 'post',
            ],
        ]) . "</div>";
    }
    foreach ($locations as $location) {
            $content_production .= '<h1>' . $location->name . ' '
                . '<span style="font-size: 30px; color: green">' .$searchModel->locationActiveCount($location->id) .'</span>'
                .  '</h1>'
                . '<h3>Нарядов к работе :'.$location->getAllNaryadsForPlan()
                . '</h3>'
                . ($location->id == Yii::$app->params['erp_clipsi'] ? '<h4>На магнитах : '.$location->getAllNaryadsForPlanOnMagnets().'</h4>' : '')
                . ($location->id == Yii::$app->params['erp_okleika'] ? '<div>'.$location->getAllNaryadsForPlanPropertyArrayOkleika().'</div>' : '')
                . ($location->id == Yii::$app->params['erp_okleika'] ? '<div>'.$location->getAllNaryadsForPlanPropertyArrayOkleika().'</div>' : '')
                . ($hasAccess ? Html::a('Снять всех работников с участка: ' . $location->name, ['cancel-users-from-workplaces', 'location_id' => $location->id], [
                     'target' => '_blank',
                     'class' => 'btn btn-warning',
                     'data-pjax' => '',
                     'data-toggle' => 'tooltip',
                     'data-original-title' => Yii::t('app', 'Снять всех работников с участка: ' . $location->name),
                     'data' => [
                         'confirm' => Yii::t('yii', 'Вы действительно хотите снять всех работников с участка ' . $location->name . ' ?'),
                         'method' => 'post',
                     ],
                 ]) : '')
                . GridView::widget([
                'options' => ['style' => 'overflow:overlay'],
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $dataProvider,
                'rowOptions' => function ($data) use($location) {
                    if($data->location_id !== $location->id){
                        return ['class' => 'hidden'];
                    }
                    if ($data->activity) {
                        return ['class' => 'success'];
                    } else{
                        return ['class' => 'danger'];
                    }

                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'location_number',
                    'type.title',
                    'property',
                    [
                        'attribute' => 'user.name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data->user ? Html::a($data->user->getName(), ['erp-user/view','id'=> $data->user_id],['target' => '_blank']):'';
                        },
                    ],
                    [
                        'attribute' => 'Запланирован',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $plan = ErpLocationWorkplacePlan::find()->select('user_id')->where(['workplace_id' => $data->id])->orderBy('time_to DESC')->limit(1)->one();
                            return ($plan && @$plan->user) ? Html::a(@$plan->user->getName(), ['erp-user/view','id'=>@$plan->user->id],['target' => '_blank']):'';
                        },
                    ],
                    [
                        'attribute' => 'Сдано нарядов',
                        'format' => 'raw',
                        'value' => function ($data) use ($location) {

                            if (!$data->user_id) return '';

                            $dateFrom = strtotime("today");
                            $dateTo   = strtotime("tomorrow") - 1;
                            $naryads =  ErpNaryadLog::find()
                                ->select('workOrderId')
                                ->where(['locationFromId' => $location->id])
                                ->andWhere(['workerId' => $data->user_id])
                                ->andWhere(['>=','date',$dateFrom])
                                ->andWhere(['<=','date',$dateTo])
                                ->column();

                            $standard = ErpNaryad::find()
                                ->select('id')
                                ->where(['id' => $naryads])
                                ->andWhere(['or',
                                        ['like','article','FD-%',false],
                                        ['like','article','BW-%',false],
                                        ['like','article','RD-%',false],
                                        ['like','article','RV-%',false],
                                        ['like','article','FV-%',false],
                                ])
                                ->andWhere(['and',
                                        ['not like','article','FD-%-%-5-%',false],
                                        ['not like','article','FD-%-%-4-%',false],
                                        ['not like','article','RD-%-%-5-%',false],
                                        ['not like','article','RD-%-%-4-%',false],
                                        ['not like','article','RV-%-%-5-%',false],
                                        ['not like','article','RV-%-%-4-%',false],
                                        ['not like','article','FV-%-%-5-%',false],
                                        ['not like','article','FV-%-%-4-%',false],
                                        ['not like','article','BW-%-%-5-%',false],
                                        ['not like','article','BW-%-%-4-%',false],
                                ])
                                ->column();

                            $notStandard = ErpNaryad::find()
                                ->select('id')
                                ->where(['id' => $naryads])
                                ->andWhere(['or',
                                        ['like','article','FD-%-%-5-%',false],
                                        ['like','article','FD-%-%-4-%',false],
                                        ['like','article','RD-%-%-5-%',false],
                                        ['like','article','RD-%-%-4-%',false],
                                        ['like','article','RV-%-%-5-%',false],
                                        ['like','article','RV-%-%-4-%',false],
                                        ['like','article','FV-%-%-5-%',false],
                                        ['like','article','FV-%-%-4-%',false],
                                        ['like','article','BW-%-%-5-%',false],
                                        ['like','article','BW-%-%-4-%',false],
                                        ['like','article','FW-%',false],
                                ])
                                ->column();

                            $organaizer = ErpNaryad::find()
                                ->select('id')
                                ->where(['id' => $naryads])
                                ->andWhere(['or',
                                        ['like','article','OT-1189-%',false],
                                        ['like','article','OT-1190-%',false],
                                ])
                                ->column();

                            $other = ErpNaryad::find()
                                ->select('id')
                                ->where(['id' => $naryads])
                                ->andWhere(['like','article','OT-%',false])
                                ->andWhere(['and',
                                        ['not like','article','OT-1189-%',false],
                                        ['not like','article','OT-1190-%',false],
                                ])
                                ->column();

                            $content = '';
                            $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: black; color: white;" title="Всего">' . count($naryads) .'</span>';
                            $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: green;" title="Стандартные шторки">' . count($standard) .'</span>';
                            $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: black;" title="Don\'t look и бескаркасники">' . count($notStandard) .'</span>';
                            $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: blue;" title="Органайзеры">' . count($organaizer) .'</span>';
                            $content .= '<span style="display: inline-block; padding: 2px 5px; margin-right: 5px; background-color: white; color: red;" title="Прочее">' . count($other) .'</span>';
                            return $content;
                        },
                    ],
                    [
                        'attribute'=>'timeIn',
                        'label'=>'Время прихода',
                        'value'=>function($data){
                            $plan = ErpLocationWorkplacePlan::find()->where(['workplace_id' => $data->id])->orderBy('time_to DESC')->one();
                            if($plan && $plan->timeIn && $plan->user_id)
                                return Yii::$app->formatter->asDatetime($plan->timeIn, 'hh:mm');
                            if(!$plan && $data->user_id && $data->timeIn)
                                return Yii::$app->formatter->asDatetime($data->timeIn, 'hh:mm');
                            return '';
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{send} {cancel} {update}', //{time} {update}
                        'buttons' => [
                            'time' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-time"></span>',
                                    Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace-plan/create-for-workplace', 'workplace' => $model->id ]),
                                    [
                                        'title' => Yii::t('app', 'Изменить расписание рабочего места'),
                                    ]);
                            },
                            'send'=>function ($url, $model ) use ($location) {

                                $t = $model->user_id ? Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user','user'=>$model->user_id]) : Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user', 'workplace'=>$model->id, 'location_type'=>$location->type]);
                                return Html::button('<span class="glyphicon glyphicon-share-alt"></span>', ['value'=>Url::to($t), 'class' => 'btn btn-info btn-xs custom_button']);
                            },
                            'cancel'=>function ($url, $model){

                                $t = Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/cancel-user','workplace_id'=>$model->id, ]);
                                return Html::button('Снять', ['value'=>Url::to($t), 'class' => 'btn btn-warning btn-xs custom_button']);
                            }
                        ],
                        'controller' => 'erp-location-workplace',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]);
        }

    ?>

    <? $locations = ErpLocation::find()
        ->where(['type' => ErpLocation::TYPE_SUPPROT])
        ->orderby('sort')
        ->all();
    $content_support = '';
    foreach ($locations as $location) {
        $content_support .= '<h1>' . $location->name. ' ' . '<span style="font-size: 30px; color: green">' .$searchModel->locationActiveCount($location->id) . '</span>' . '</h2>'.GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $dataProvider,
                'rowOptions' => function ($data) use($location) {
                    if($data->location_id !== $location->id){
                        return ['class' => 'hidden'];
                    }
                    if ($data->activity) {
                        return ['class' => 'success'];
                    } else{
                        return ['class' => 'danger'];
                    }

                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'location_number',
                    'type.title',
                    'property',
                    [
                        'attribute' => 'user.name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data->user ? Html::a($data->user->getName(), ['erp-user/view','id'=> $data->user_id],['target' => '_blank']):'';
                        },
                    ],
                    [
                        'attribute' => 'Запланирован',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $plan = ErpLocationWorkplacePlan::find()->select('user_id')->where(['workplace_id' => $data->id])->orderBy('time_to DESC')->limit(1)->one();
                            return ($plan && $plan->user) ? Html::a(@$plan->user->getName(), ['erp-user/view','id'=>@$plan->user->id],['target' => '_blank']):'';
                        },
                    ],
                    [
                            'attribute'=>'timeIn',
                            'label'=>'Время прихода',
                            'value'=>function($data){
                                $plan = ErpLocationWorkplacePlan::find()->select('user_id')->where(['workplace_id' => $data->id])->orderBy('time_to DESC')->limit(1)->one();
                                if($plan && $plan->timeIn && $plan->user_id)
                                    return Yii::$app->formatter->asDatetime($plan->timeIn, 'hh:mm');
                                if(!$plan && $data->user_id && $data->timeIn)
                                    return Yii::$app->formatter->asDatetime($data->timeIn, 'hh:mm');
                                return '';
                            }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{time}',
                        'buttons' => [
                            'time' => function ($url, $model) {
                                if(Yii::$app->user->getId() ==2 || Yii::$app->user->getId() ==37 || Yii::$app->user->getId() ==21)
                                    return Html::a('<span class="glyphicon glyphicon-time"></span>',
                                        Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace-plan/create-for-workplace','workplace' => $model->id]),
                                        [
                                            'title' => Yii::t('app', 'Изменить расписание рабочего места'),
                                        ]);
                            }
                        ],
                        'controller' => 'erp-location-workplace',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]);
    }
    ?>



    <?php

        $items[0]['label'] = 'Производственные '. $searchModel->activeCountProduction;
        $items[0]['content'] = $content_production;
//        $items[1]['headerOptions'] = ['class' => 'location-' . $location->id];
        $items[1]['label'] = 'Не производственные '. $searchModel->activeCountSupport;
        $items[1]['content'] = $content_support;

  ?>

    <?= Tabs::widget([
         'items' => $items,
         'encodeLabels' => false,
     ]); ?>


</div>
