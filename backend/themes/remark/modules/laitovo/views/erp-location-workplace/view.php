<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use backend\widgets\GridView;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplace */

$this->title = 'Рабочее место №'.$model->id.' на участке '.$model->location->name;
$this->params['breadcrumbs'][] = ['label' => 'Рабочие места', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-view">


    <p>
        <?= Html::a('Распланировать', ['erp-location-workplace-plan/create-for-workplace', 'workplace' => $model->id], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger deleteconfirm', 
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'attribute' => 'location.name',
                'label' => 'Участок'
            ],
            'location_number',
            'property',
            'type.title',
            'user_id',
        ],
    ]) ?>

    <?php Pjax::begin(); ?>

<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
    <?= $form->field($searchModel, 'curDate')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'options' => [
            'class' => 'form-control',
            'onchange' => '$("#search").click();',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>
<div class="form-group">
        <?= Html::submitButton('Сформировать', ['class' => 'btn btn-primary hidden','id' => 'search']) ?>
</div>



<?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user.name',
            'time_from:time',
            'time_to:time',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>



</div>
