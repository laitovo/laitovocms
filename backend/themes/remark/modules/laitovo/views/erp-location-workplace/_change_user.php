<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use backend\modules\laitovo\models\ErpUser;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-group">
<?if (isset($user)):?>
	<?if (!isset($step)):?>
		<h4>Укажите участок:</h4>
		<?= Html::beginForm([''], 'post', ['data-pjax' => '', 'class' => '']); ?>
		<div class="form-group">
		    <!-- <?= Html::label('Участок',null,['class'=> 'form-label'])?> -->
		    <?= Html::dropDownList('location',null,$locations,['class' => 'form-control','required' => true])?>
		</div>
	    <?= Html::hiddenInput('step', 2) ?>
	    <?= Html::hiddenInput('user', $user) ?>
	    <?= Html::submitButton('Далее', ['class' => 'btn btn-primary', 'name' => 'hash-button']) ?>
	    <?= Html::a('Отмена', 'monitor',['data-pjax' => '','class' => 'btn btn-warning'])?>
		<?= Html::endForm() ?>
	<?elseif (isset($step) && $step == 2):?>
		<h3>Участок: <?=$location->name?></h3>
		<h4>Уточните рабочее место</h4>
		<?= Html::beginForm([''], 'post', ['data-pjax' => '', 'class' => '']); ?>
		<div class="form-group">
		    <?= Html::label('Участок')?>
		    <?= Html::dropDownList('workplace_id',null,$workplaces,['class' => 'form-control','required' => true])?>
		</div>
		    <?= Html::hiddenInput('step', 3) ?>
	   		<?= Html::hiddenInput('user', $user) ?>
		    <?= Html::submitButton('Рабочее место', ['class' => 'btn btn-lg btn-primary', 'name' => 'hash-button']) ?>
	    	<?= Html::a('Отмена', 'monitor',['data-pjax' => '','class' => 'btn btn-warning'])?>

		<?= Html::endForm() ?>
	<?elseif (isset($step) && $step == 3):?>
		<h1 class="success">Success</h1>
		<h3 class="success">Сотрудник успешно перенаправлен на участок такой то рабочее место такое то</h3>
		<?= Html::a('Обновить', 'monitor',['data-pjax' => '']) ?>
	<?endif;?>
<?elseif (isset($workplace)):?>
	<?if (!isset($step)):?>
			<h4>Установить для данного рабочего места нового пользователя:</h4>
			<?= Html::beginForm([''], 'post', ['data-pjax' => '', 'class' => '']); ?>
			<div class="form-group">
			    <!-- <?= Html::label('Участок',null,['class'=> 'form-label'])?> -->
			    <?= Html::dropDownList('user_id',null,$users,['class' => 'form-control','required' => true])?>

		   		<?= Html::checkbox('delete',true) ?>
			    <?= Html::label('Убрать работника с других рабочих мест?','delete',['class'=> 'form-label'])?>

			</div>
		    <?= Html::hiddenInput('step', 2) ?>
		    <?= Html::hiddenInput('workplace', $workplace) ?>
		    <?= Html::submitButton('Далее', ['class' => 'btn btn-primary', 'name' => 'hash-button']) ?>
	    	<?= Html::a('Отмена', 'monitor',['data-pjax' => '','class' => 'btn btn-warning'])?>

			<?= Html::endForm() ?>
			<hr>
			<div class="alert alert-danger" role="alert">
			  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			 * при добавлении данного рабочего места на участке Оклейки оклейщику не забудь снять галочку об удалении с других рабочих мест.
			</div>
			<div class="alert alert-info" role="alert">
			  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			 ** при установке работника происходит его перепланирование и завтра его место останется таким же, на которые Вы его сейчас назначаете.
			</div>
			
		<?elseif (isset($step) && $step == 2):?>
			<div class="alert alert-success" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					Вы успешно назначили работника на рабочее место. При следующем получении нарядов он 	перерегестрируется на новые рабочие места.
			</div>
			<?= Html::button('Обновить', ['class' => 'btn btn-primary','onclick' => "document.location.reload()"]) ?>
		<?endif;?>
<?endif;?>
</div>