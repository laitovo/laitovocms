<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\ErpLocationWorkplaceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-location-workplace-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['data-pjax' => true],
    ]); ?>

    <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge([''=>''],ArrayHelper::map(ErpLocation::find()->all(),'id','name')),['onchange' => '$("#search").click();']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary hidden','id' => 'search']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
