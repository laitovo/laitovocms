<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\ErpLocationWorkplaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рабочие места';
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-index">

    <?php \yii\widgets\Pjax::begin(); ?>
    <h1><?= Html::encode($this->title) ?></h1>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'show' => ['location.name','location_number','type.title','user.name'],
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'location.name',
            'location_number',
            'property',
            'type.title',
            'user.name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
