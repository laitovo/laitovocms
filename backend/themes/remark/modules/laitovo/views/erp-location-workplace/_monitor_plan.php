<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpLocationWorkplace;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\laitovo\ErpLocationWorkplaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рабочие места';
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-index">
    <? $locations = ErpLocation::getAllSupport();
        $i = 0;
        foreach ($locations as $location) {
            $items[$i]['label'] = $location->name;
            $items[$i]['content'] = GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($data) use($location) {
                if($data->location_id !== $location->id){
                    return ['class' => 'hidden'];
                }
                if ($data->lastPlan) {
                    return ['class' => 'success'];
                } else{
                    return ['class' => 'danger'];
                }

            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'location_number',
                [
                    'attribute' => 'Сотрудник',
                    'value' => function($data) {
                        return ($data->lastPlan && $data->lastPlan->user) ?  $data->lastPlan->user->name : '';
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{time}',
                    'buttons' => [
                        'time' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-time"></span>',
                                Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace-plan/create-for-workplace','workplace' => $model->id]),
                            [
                                'title' => Yii::t('app', 'Изменить расписание рабочего места'),
                            ]);
                        }
                    ],
                    'controller' => 'erp-location-workplace',
                    'buttonOptions' => ['class' => 'deleteconfirm'],
                ],
            ],
        ]);
            $i++;
        }

    ?>
     <?= Tabs::widget([
         'items' => $items,
         'encodeLabels' => false,
     ]); ?>
</div>
