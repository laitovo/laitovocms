<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpLocationWorkplaceType;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplace */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-location-workplace-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(ErpLocation::find()->all(),'id','name')) ?>

    <?= $form->field($model, 'location_number')->textInput() ?>

    <?= $form->field($model, 'property')->dropDownList($model->properties) ?>

    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::merge([''=>''],ArrayHelper::map(ErpLocationWorkplaceType::find()->all(),'id','title'))) ?>

    <!-- <?= $form->field($model, 'user_id')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
