<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-group">
<?if (isset($status) && $status == 'confirm'):?>
		<?= Html::beginForm([''], 'post', ['data-pjax' => '', 'class' => '']); ?>
		<div class="form-group">
			<h3>Уверены что хотите удалить пользователя с участка?</h3>
		</div>
	    <?= Html::hiddenInput('step', 2) ?>
	    <?= Html::hiddenInput('workplace_id', $workplace_id) ?>
	    <?= Html::submitButton('Уверен', ['class' => 'btn btn-primary', 'name' => 'hash-button']) ?>
	    <?= Html::a('Отмена', 'monitor',['data-pjax' => '','class' => 'btn btn-warning'])?>
		<?= Html::endForm() ?>
<?elseif (isset($status) && $status == 'success'):?>
	<h1 class="success">Success</h1>
	<h3 class="success">Сотрудник успешно снят с участка</h3>
	<?= Html::a('Обновить', 'monitor',['data-pjax' => '','class' => 'btn btn-success']) ?>
<?else:?>
	<h1 class="success">Ошибка</h1>
	<h3 class="success">Возникла нека ошибка</h3>
	<?= Html::a('Обновить', 'monitor',['data-pjax' => '','class' => 'btn btn-success']) ?>
<?endif;?>
</div>