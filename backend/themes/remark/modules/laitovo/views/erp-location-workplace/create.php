<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpLocationWorkplace */

$this->title = 'Создать рабочее место';
$this->params['breadcrumbs'][] = ['label' => 'Рабочие места', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-location-workplace-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
