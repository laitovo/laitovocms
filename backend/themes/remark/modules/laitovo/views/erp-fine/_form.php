<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpUser;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\laitovo\ErpFine */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-fine-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
            'data' => ErpUser::userInWork(),
            'language' => 'ru',
            'options' => [
                'placeholder' => 'Введите имя ...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>

        <?= $form->field($model, 'description')->textarea() ?>

        <?= $form->field($model, 'amount')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>