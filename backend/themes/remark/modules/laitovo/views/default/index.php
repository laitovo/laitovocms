<?php

$this->title = Yii::t('app', 'Laitovo');
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

$image_src = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzBEOEZEQiIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjEwMCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojZmZmO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEzcHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MjAweDIwMDwvdGV4dD48L3N2Zz4=';
?>
<div class="container">
    <div id="room-1" class='col-md-3' >
        <h5 class='text-center'>Швейка</h5>
        <div id="shveika" style="border-right: 3px black solid; border-left: 3px black solid">
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
        </div>
    </div>

    <div id="room-2" class='col-md-3'>
        <h5 class='text-center'>Оклейка-1</h5>
        <div id="okleika-1" style="border-right: 3px black solid; border-left: 3px black solid">
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
        </div>

        <h5 class='text-center'>Диспетчер</h5>
        <div id="dispatcher" style="border-right: 3px black solid; border-left: 3px black solid">
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
        </div>
    </div>

    <div id="room-3" class='col-md-3'>
        <h5 class='text-center'>Изгиб</h5>
        <div id="izgib" style="border-right: 3px black solid; border-left: 3px black solid">
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
        </div>
    </div>

    <div id="room-4" class='col-md-3'>
        <h5 class='text-center'>Оклейка-2</h5>
        <div id="okleika-2" style="border-right: 3px black solid; border-left: 3px black solid">
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
        </div>

        <h5 class='text-center'>Клипсы</h5>
        <div id="okleika-2" style="border-right: 3px black solid; border-left: 3px black solid">
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
        </div>

        <h5 class='text-center'>Отк</h5>
        <div id="okleika-2" style="border-right: 3px black solid; border-left: 3px black solid">
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
            <hr>
            <div class='clearfix'>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
                <div class="col-xs-6 col-sm-6 placeholder">
                      <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="<?=$image_src?>">
                </div>
            </div>
        </div>
    </div>
</div>

