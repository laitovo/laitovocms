<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu');

?>
<div class="erp-additional-work-index">

    <h1>Журнал нарядов на дополнительные работы</h1>

    <p>
         <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Создать наряд на доп. работы')]) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=> 'id',
                'format' =>'html',
                'value' =>function($data){
                   return $data->id ? Html::a("Наряд №".Html::encode($data['id']), ['view', 'id' => $data['id']]): '';
               }      
            ],
            [
                'attribute'=> 'user_id',
                'value' => function($data){
                    return $data->user_id ? $data->user->name : '';
                }
            ],
            [
                'attribute'=> 'location_id',
                'value' => function($data){
                    return $data->location_id ? $data->location->name : '';
                }
            ],
            [
                'attribute'=> 'job_type',
                'value' => function($data){
                    return $data->job_type ? $data->jobType->title : '';
                }
            ],
            [
                'attribute'=> 'job_price',
                'value' => function($data){
                    return $data->job_price;
                }
            ],
        ],
    ]); ?>
</div>
