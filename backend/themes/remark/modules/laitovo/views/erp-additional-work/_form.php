<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpJobType;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpAdditionalWork */
/* @var $form yii\widgets\ActiveForm */
Yii::$app->view->registerJs('
            $("#erpadditionalwork-job_count").keyup(function(){
                 $.get("' . Url::to(['/ajax/job-count']) . '",{"count":$("#erpadditionalwork-job_count").val(), "jobType":$("#erpadditionalwork-job_type").val()},function(data){
                                if(data)
                                {
                                    $("#erpadditionalwork-job_price").val(data);
                                }else{
                                    $("#erpadditionalwork-job_price").val(null)
                                }
                        });
            });
        ', \yii\web\View::POS_END);

?>

<div class="erp-additional-work-form">

    <?php $form = ActiveForm::begin(); ?>

     <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(ErpUser::find()->all(), 'id', 'name'),
        'language' => 'ru',
        //'initValueText' => common\models\laitovo\ErpUser::findOne($made_user_id)->name,
        'options' => [
            'placeholder' => 'Введите имя ...',
//            'onchange' => '  
//                $("#select2-laitovoreworkact-real_quilty-container").text($("option:selected", this).text());
//                $("#laitovoreworkact-real_quilty").val($("option:selected", this).val());
//        ',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?> 

    <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name')),
            ['class' => 'form-control', 'onchange' => '$.get("' . Url::to(['/ajax/job-type']) . '",{"location":$(this).val()},function(data){
                    if (Object.keys(data).length!=1)
                    {
                        $("#erpadditionalwork-job_type").html("<option></option>");
                        $.each(data, function(key, value) {
                            $("#erpadditionalwork-job_type").append($("<option></option>").attr("value",key).text(value));
                        });
                    }
                  
            });']) ?>

    <?= $form->field($model, 'job_type')->widget(Select2::classname(), [
        'data' => ErpJobType::activeList(),
        'language' => 'ru',
        //'initValueText' => common\models\laitovo\ErpUser::findOne($made_user_id)->name,
        'options' => [
            'placeholder' => 'Введите наименование работ ...',
            'onchange' => '
                    $("#erpadditionalwork-job_count").val(1);
                    $.get("' . Url::to(['/ajax/job-count']) . '",{"count":1, "jobType":$(this).val()},function(data){
                            if(data)
                            {
                                $("#erpadditionalwork-job_price").val(data);
                            }
                    });
                   
            '
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <?= $form->field($model, 'job_count')->textInput() ?>

    <?= $form->field($model, 'job_price')->textInput() ?>
    <?= $form->field($model, 'comment')->textarea() ?>
    <?php if(!$model->isNewRecord) :?>
        <?php if($model->status != null) :?>
            <?= $form->field($model, 'status')->dropDownlist([true =>'Готов' ,null => 'Вернуть в работу']) ?>
        <?php endif; ?> 
    <?php endif; ?>    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn  btn-round btn-lg btn-info' : 'btn btn-round btn-lg btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
