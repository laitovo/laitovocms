<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpAdditionalWork */


$this->params['breadcrumbs'][] = ['label' => 'Журнал дополнительных нарядов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

?>
<div class="erp-additional-work-create">

    <h1>Создание наряда на дополнительные работы</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
