<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpAdditionalWork */


$this->params['breadcrumbs'][] = ['label' => ' Журнал нарядов на дополнительные работы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
$this->render('../menu');

?>
<div class="erp-additional-work-update">

    <h1>Редактировани наряда на дополнительные работы № <?= $model->id?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
