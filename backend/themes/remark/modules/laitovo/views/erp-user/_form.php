<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\laitovo\models\ErpLocation;

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge(['' => ''], ArrayHelper::map(ErpLocation::find()->all(), 'id', 'name'))) ?>

<?= $form->field($model, 'status')->dropDownList($model->statuses) ?>

<?= $form->field($model, 'type')->dropDownList($model->types) ?>

<?= $form->field($model, 'factor')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'trainee')->checkbox() ?>

<?= $form->field($model, 'canCreateTemplate')->checkbox() ?>

<?= $form->field($model, 'canCreateDontlook')->checkbox() ?>

<?php if (in_array(Yii::$app->user->getId(),[2,21])) : ?>
<?= $form->field($model, 'otherPricing')->checkbox() ?>
<?php endif; ?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>

</div>

<?php ActiveForm::end(); ?>
