<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$generator = new Picqer\Barcode\BarcodeGeneratorPNG();

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Сотрудники'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<div class="text-center" style="width: 50%;margin-left: 25%">
    <div style="border: 1px dashed #000;border-radius: 10px;padding: 50px">
        <img width="100%" height="100px"
             src="data:image/png;base64,<?= base64_encode($generator->getBarcode($model->barcode, $generator::TYPE_CODE_128)) ?>">
        <h3><?= $model->name ?></h3>
    </div>
</div>
