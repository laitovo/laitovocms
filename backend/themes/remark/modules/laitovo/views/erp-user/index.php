<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Сотрудники');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>
<?php if (in_array(Yii::$app->user->id,[2, 21, 36])): ?>
    <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить сотрудника')]) ?>
<?php endif ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name', 'location_id'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'barcode',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a(Html::img("http://barcode.tec-it.com/barcode.ashx?data=" . $data->barcode), ['print', 'id' => $data->id], ['target' => '_blank']);
            },
        ],
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->getName()), ['view', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->location ? Html::a(Html::encode($data->location->name), ['erp-location/view', 'id' => $data->location->id]) : null;
            },
        ],
        'status',
        'type',
        'created_at:datetime',
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],
        [
            'attribute' => 'trainee',
            'value' => function($data) {
                return $data->trainee ? 'Да' : 'Нет';
            }
        ],
        // 'updated_at',
        // 'updater_id',
        // 'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
