<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use backend\widgets\GridView;
use backend\modules\laitovo\models\ErpNaryad;
use backend\widgets\ActiveForm;
use backend\modules\laitovo\models\ReportCard;
use yii\helpers\ArrayHelper;
use common\models\laitovo\Weekend;
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();

/* @var $this yii\web\View */
/* @var $model backend\modules\laitovo\models\ErpUser */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Производство'), 'url' => ['erp/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Сотрудники'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => ArrayHelper::merge([
        'id',
        [
            'attribute' => 'barcode',
            'format' => 'raw',
            'value' => Html::a($generator->getBarcode($model->barcode, $generator::TYPE_CODE_128), ['print', 'id' => $model->id], ['target' => '_blank']),
        ],
        'name',
        [
            'attribute' => 'location_id',
            'format' => 'html',
            'value' => $model->location ? Html::a(Html::encode($model->location->name), ['erp-location/view', 'id' => $model->location->id]) : null,
        ],
        'status',
        'type',
        'factor',
        'created_at:datetime', 
        [
            'attribute' => 'author_id',
            'value' => $model->author ? $model->author->name : null,
        ],
        [
            'attribute' => 'trainee',
            'value' => $model->trainee ? 'Да' : 'Нет',
        ],
        [
            'attribute' => 'canCreateTemplate',
            'value' => $model->canCreateTemplate ? 'Да' : 'Нет',
        ],
        [
            'attribute' => 'canCreateDontlook',
            'value' => $model->canCreateDontlook ? 'Да' : 'Нет',
        ],
    ], in_array(Yii::$app->user->getId(),[2,21]) ? [[
        'attribute' => 'otherPricing',
        'value' => $model->otherPricing ? 'Да' : 'Нет',
    ]] : [] )
]) ?>

<?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-outline btn-round btn-primary']) ?>




<?= $model->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
    'data-toggle' => "tooltip",
    'data-original-title' => Yii::t('yii', 'Delete'),
    'data' => [
        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
        'method' => 'post',
    ],
]) ?>


<h1>Наряды на сотруднике</h1>

<?
    if ( in_array(Yii::$app->user->getId(), [21, 36]) )
    echo Html::button('<i class="fa fa-play"></i> Снять все наряды с паузы', [
        'class' => 'btn btn-outline btn-round btn-info',
        'data'  => [
            'id' => $model->id,
            'toggle'  => 'tooltip',
            'title'   => Yii::t('app', 'Возобновить')
        ],
        'onclick' => '
            var button = $(this);
            var id = $(this).data("id");
            var handle_function = function () {
                button.prop("disabled", true).attr("title", "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:20px;\"></i>");
                $.ajax({
                    url: "' . Url::to(['ajax-unpause']) . '",
                    dataType: "json",
                    data: {
                        "id": id
                    },
                    success: function (data) {
                        button.prop("disabled", false).html("<i class=\"fa fa-play\" data-toggle=\"tooltip\"></i> Снять все наряды с паузы");
                        if (!data.status) {
                            notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                            return;
                        }
                        notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                    },
                    error: function() {
                        button.prop("disabled", false).html("<i class=\"fa fa-play\" data-toggle=\"tooltip\"></i> Снять все наряды с паузы");
                        notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                    }
                });
            };
            notie.confirm("Хотите снять заказ с паузы?", "Да", "Нет", handle_function);
        '
    ]);

//    $canBePaused = $pauseOrder->canBePaused($data);
    echo Html::button('<i class="fa fa-pause" data-toggle="tooltip"></i> Поставить все наряды на паузу', [
        'class' => 'btn btn-outline btn-round btn-info',
        'data'  => [
            'id' => $model->id,
            'toggle'  => 'tooltip',
            'title'   => Yii::t('app', 'Остановить')
        ],
        'onclick' => '
            var button = $(this);
            var id = $(this).data("id");
            var handle_function = function () {
                button.prop("disabled", true).attr("title",  "Подождите...").html("<i class=\"fa fa-spinner fa-spin\" style=\"font-size:20px;\"></i>");
                $.ajax({
                    url: "' . Url::to(['ajax-pause']) . '",
                    dataType: "json",
                    data: {
                        "id": id
                    },
                    success: function (data) {
                        button.prop("disabled", false).html("<i class=\"fa fa-pause\" data-toggle=\"tooltip\"></i> Поставить все наряды на паузу");
                        if (!data.status) {
                            notie.alert(3, data.message ? data.message : "Не удалось выполнить команду", 3);
                            return;
                        }
                        notie.alert(1, data.message ? data.message : "Успешно выполнено", 1);
                    },
                    error: function() {
                        button.prop("disabled", false).html("<i class=\"fa fa-pause\" data-toggle=\"tooltip\"></i> Поставить все наряды на паузу");
                        notie.alert(3, "Неизвестная ошибка. Обратитесь к администратору.", 3);
                    }
                });
                
            };
            notie.confirm("Хотите поставить заказ на паузу?", "Да", "Нет", handle_function);
        '
    ]);
?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $naryadsProvider,
    'filterModel' => $model,
    'show' => ['id', 'article', 'order_id', 'status', 'window', 'windowType', 'tkan'],
    'salt' => 'NSOvcg1',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->name ? Html::a(Html::encode($data->name), ['erp-naryad/view', 'id' => $data->id]) : null;
            },
        ],
        [
            'attribute'=> 'article',
            'label'=> 'Артикул',
            'value'=>function($data){
                return $data->article ? $data->article : '';
            }
        ],

        [
            'attribute'=>'window',
            'label'=>'Оконный проем',
            'filter'=> array("FW"=>"ПШ","FV"=>"ПФ", "FD"=>"ПБ","RD"=>"ЗБ","RV"=>"ЗФ","BW"=>"ЗШ"),
            'value'=>function ($data) {
                return ErpNaryad::windowStatic($data->article);
            },
        ],
        [
            'attribute'=>'windowType',
            'label'=>'Название продукта',
            'filter'=> ErpNaryad::filterTypeStatic(),
            'value'=>function ($data) {
                return ErpNaryad::windowTypeStatic($data->article);
            },
        ],
        [
            'attribute'=>'tkan',
            'label'=>'Тип ткани',
            'filter'=> array(4=>"№1.5",8=>"№1.25", 1=>"№1",2=>"№2",3=>"№3",5=>"№5"),
            'value'=>function ($data) {
                return ErpNaryad::tkanStatic($data->article);
            },
        ],
        [
            'attribute' => 'order_id',
            'label'=>'Номер заказа',
            'format' => 'html',
            'value' => function ($data) {
                return $data->order ? Html::a(Html::encode($data->order->name), ['erp-order/view', 'id' => $data->order->id]) : null;
            },
        ],
        [
            'attribute' => 'scheme_id',
            'label'=>'Схема',
            'format' => 'html',
            'value' => function ($data) {
                return $data->scheme ? Html::a(Html::encode($data->scheme->name), ['erp-scheme/view', 'id' => $data->scheme->id]) : null;
            },
        ],
        [
            'attribute' => 'status',
            'format' => 'html',
            'filter'=> false,
        ],

        'created_at:datetime',
        [
            'attribute'=>'updated_at',
            'format' => 'html',
            'label'=>'Дата получения наряда работником',
            'value'=> function($data){
                return $data->userLocationDate ? Yii::$app->formatter->asDateTime($data->userLocationDate) : '';
            }
        ],
        [
            'attribute' => 'author_id',
            'value' => function ($data) {
                return $data->author ? $data->author->name : null;
            },
        ],

        // ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
<br>
<br>


<?php $form = ActiveForm::begin([
         'method' => 'get',
    ]); ?>
<div class="col-xs-3">

    <?= $form->field($model, 'dateFrom')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>

    <b style="font-size: 11px">
      *Выборка по дате получения наряда работником
    </b>
</div>

<div class="col-xs-3">

    <?= $form->field($model, 'dateTo')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'options' => [
            'class' => 'form-control',
         ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>
</div>
    
   
<div class="form-group col-xs-3">
    <div class=spacer>&nbsp;</div> <!-- Символ неразрывного пробела для того, чтобы встала кнопка -->
    <?= Html::submitButton(Yii::t('app', 'Сформировать'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
<div class="clearfix"></div>

<div class="pull-left" style="padding-top: 30px">
    <h1 style="font-size: large">Количество нарядов: <?= Html::a(@$model->naryadCount,
            ['erp-job-report/piece-work-payment-day-list', 'user_id'=>$model->id, 'dateFrom'=>date('d.m.Y',$model->getDateFrom()),
                'dateTo'=>date('d.m.Y',$model->getDateTo())],['style'=>['color'=>'darkslategrey']])?></h1>
</div>

<div class="pull-right" style="padding-top: 30px">
    <h1 style="color: green; font-size: large">Итого <?= Html::a(Yii::$app->formatter->asDecimal(@$model->totalSum),
            ['erp-job-report/piece-work-payment-day-list', 'user_id'=>$model->id, 'dateFrom'=>date('d.m.Y',$model->getDateFrom()),
                'dateTo'=>date('d.m.Y',$model->getDateTo())],['style'=>['color'=>'green'], 'data'=>['toggle'=>'tooltip', 'placement'=>'top'], 'title'=>'Итого'])?></h1>
</div>

<div class="pull-right" style="padding-right: 80px; padding-top: 30px">
    <h1 style="font-size: large"> <?= Html::a(Yii::$app->formatter->asDecimal(@$model->reworkSum),
            ['erp-job-report/piece-work-payment-day-list', 'user_id'=>$model->id, 'dateFrom'=>date('d.m.Y',$model->getDateFrom()),
                'dateTo'=>date('d.m.Y',$model->getDateTo())],['style'=>['color'=>'orangered'], 'data'=>['toggle'=>'tooltip', 'placement'=>'top'], 'title'=>'Сумма за переделки'])?></h1>
</div>

<div class="pull-right" style="padding-right: 80px; padding-top: 30px">
    <h1 style="font-size: large"> <?= Html::a(Yii::$app->formatter->asDecimal(@$model->premiumSum),
            ['erp-job-report/piece-work-payment-day-list', 'user_id'=>$model->id, 'dateFrom'=>date('d.m.Y',$model->getDateFrom()),
                                                           'dateTo'=>date('d.m.Y',$model->getDateTo())],['style'=>['color'=>'green'], 'data'=>['toggle'=>'tooltip', 'placement'=>'top'], 'title'=>'Сумма премий'])?></h1>
</div>

<div class="pull-right" style="padding-right: 80px; padding-top: 30px">
    <h1 style="font-size: large"> <?= Html::a(Yii::$app->formatter->asDecimal(@$model->naryadSum),
            ['erp-job-report/piece-work-payment-day-list', 'user_id'=>$model->id, 'dateFrom'=>date('d.m.Y',$model->getDateFrom()),
                'dateTo'=>date('d.m.Y',$model->getDateTo())],['style'=>['color'=>'blue'], 'data'=>['toggle'=>'tooltip', 'placement'=>'top'], 'title'=>'Сумма за наряды'])?></h1>
</div>


<? $fields=[];

    $fields=[];
    $weekends = ArrayHelper::map(Weekend::find()->all(), 'weekend', 'weekend');

    foreach ($report->getDateArray() as $day) {
        $fields[] = [
            'attribute' => $day,
            'label' => $report->littleLabel($day) ,
            'contentOptions' =>function($data, $key, $index, $column) use($weekends, $day){
                return key_exists($day, $weekends) ? ['class'=>'danger'] : [];
            }
        ];
    }

?>
<div class="clearfix"></div>
<h1> <?=$report->title ?> </h1>
<?= \yii\grid\GridView::widget([
    'tableOptions' => ['class' => 'table table-hover',
        'style'=>[
            'font-size'=>'11px',
            'font-weight'=>800
        ]
    ],
    'rowOptions' => function($data, $key, $index,$grid) {
        if ($key=='final_difference') {
            return ['class' => 'warning'];
        }
    },
    'dataProvider' => $report->getArrayDataProvider($model->id),
    'layout'=>"{items}",
    'columns' =>ArrayHelper::merge(
        [
            [
                'attribute'=>'label',
                'label'=>'',
            ],
        ],
        $fields,
        [
            [
                'attribute'=>'fact_sum',
                'label'=>'Факт'
            ],
            [
                'attribute'=>'month_rate',
                'label'=>'Мес. норма'
            ]
        ]
    )
]) ?>
<?php if (Yii::$app->user->getId() == 21): ?>
<hr>
<?= Html::a('<i class="icon wb-plus"></i>', ['erp-location-workplace-register/create-from-user','user' => $model->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $journalProvider,
    'filterModel' => $model,
    'rowOptions'=>function ($data, $key, $index, $grid){
        if ($data->action_type == \common\models\laitovo\ErpLocationWorkplaceRegister::TYPE_OUT) {
            return ['class' => 'success'];
        }elseif ($data->action_type == \common\models\laitovo\ErpLocationWorkplaceRegister::TYPE_IN) {
            return ['class' => 'info'];
        }
    },
    'show' => ['id', 'register_at', 'action_type'],
    'salt' => 'sadsdasdasdas',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'register_at',
            'value' => function($data) {
                return Yii::$app->formatter->asDatetime($data->register_at,'php:d.m.Y H:i:s');
            }
        ],
        'action_type',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'controller' => 'erp-location-workplace-register',
        ],

    ],
]); ?>
<?php endif; ?>
