<?php
use yii\grid\GridView;
use \yii\helpers\Html;

$this->title = Yii::t('app', 'Журнал премий');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laitovo'), 'url' => ['/laitovo/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('@backendViews/laitovo/views/menu');
?>

<?= $this->render('_search', ['model' => $searchModel]); ?>

<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'summary' => false,
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Сотрудник',
            'format' => 'raw',
            'value' => function($data) {
                return $data->user ? Html::a($data->user->name, ['/laitovo/erp-user/view','id'=>$data->user_id], ['target' => '_blank']) : null;
            }
        ],
        [
            'label' => 'Документ основание',
            'format' => 'raw',
            'value' => function($data) {
                return $data->act ? Html::a('Акт №' . $data->act->rework_number, ['/laitovo/erp-rework-act/view','id'=>$data->rework_act_id], ['target' => '_blank']) : null;
            }
        ],
        [
            'attribute' => 'amount',
            'label' => 'Размер премии',
        ],
        [
            'label' => 'Начислено',
            'value' => function($data) {
                return $data->is_paid ? 'Да' : 'Нет';
            }
        ],
        [
            'attribute' => 'paid_at',
            'format'=> ['datetime', 'php:d.m.Y H:i:s'],
            'label' => 'Дата начисления',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]) ?>
