<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'search-form'
    ],
    'action' => ['index'],
    'method' => 'post',
]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'user_name')->label('Сотрудник')->textInput([
                'class' => 'form-control search-text'
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'act_number')->label('Документ основание (номер)')->textInput([
                'class' => 'form-control search-text'
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'is_paid')->label('Начислено')->radioList(
                [
                    '1' => 'Да',
                    '0' => 'Нет',
                    'all' => 'Все',
                ],
                [
                    'class' => 'search-is-paid'
                ]
            ); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'paid_at')->label('Дата начисления')->widget(DatePicker::classname(), [
                'options' => [
                    'class' => 'form-control search-text',
                ],
                'dateFormat' => 'dd.MM.yyyy',
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('<i class="icon wb-search"></i> ' . Yii::t('app', 'Искать'),
            ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('<i class="icon wb-refresh"></i> ' . Yii::t('app', 'Очистить'), [
            'class' => 'btn btn-default reset-form-button',
        ]) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php Yii::$app->view->registerJs('
    $(document).ready(function() {
        if (!$(".search-form .search-is-paid input[type=\'radio\']:checked").length) {
            $(".search-form .search-is-paid input[value=\'all\']").prop("checked", true);
        }
    });
    
    $(".search-form .reset-form-button").click(function() {
        $(".search-form .search-text").val("");
        $(".search-form .search-is-paid input[value=\'all\']").prop("checked", true);
    });
', \yii\web\View::POS_END);
?>