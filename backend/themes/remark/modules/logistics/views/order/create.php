<?php

use yii\helpers\Html;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\Order */

$this->render('../menu');

$this->title = Yii::t('app','Список заявок');
$viewtitle = Yii::t('app','Добавление заявки')
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="order-create">

    <h4><i><?= $viewtitle ?></i></h4>
    <hr>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?Pjax::end();?>
