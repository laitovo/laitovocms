<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->render('../menu');

$this->title = Yii::t('app','Список заявок');
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="order-index">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary pull-left','title' => Yii::t('app','Добавить заявку')]) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        'show' => ['id','source.title','responsible_person','created_at'],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            // 'team_id',
            'source.title',
            'source_innumber',
            'responsible_person',
            'comment:ntext',
            'created_at:datetime',
            // 'author_id',
            // 'updated_at',
            // 'updater_id',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttonOptions' => ['data-pjax' => '1'],
            ],
        ],
    ]); ?>
</div>
<?Pjax::end();?>
