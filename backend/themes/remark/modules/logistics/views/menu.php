<?php

/* @var $this yii\web\View */

$admin   = in_array(Yii::$app->user->getId(),[2,21,48]);
$logist  = in_array(Yii::$app->user->getId(),[45,56,55,38,36,33,26,69,11]);
$storage = in_array(Yii::$app->user->getId(),[120,62,26,3]);
$useEan  = in_array(Yii::$app->user->getId(),[56,55,64,26]);
$dispatcher  = in_array(Yii::$app->user->getId(),[43,44]);



$this->params['menuItems'][0]['label'] = '<span class="lead">Основное меню</span>';

//меню для админов
if ($admin) {
    $this->params['menuItems'][0]['items'][0]['label'] = 'Автозаказ';
//
    $this->params['menuItems'][0]['items'][0]['items'][0]['label'] = Yii::t('app', 'Монитор автозаказа');
    $this->params['menuItems'][0]['items'][0]['items'][0]['url'] = ['/logistics/automatic-order/monitor'];
//
    $this->params['menuItems'][0]['items'][0]['items'][1]['label'] = Yii::t('app', 'Настройки автозаказа');
    $this->params['menuItems'][0]['items'][0]['items'][1]['url'] = ['/logistics/automatic-order/index'];
//
    $this->params['menuItems'][0]['items'][0]['items'][2]['label'] = Yii::t('app', 'Автопополнение');
    $this->params['menuItems'][0]['items'][0]['items'][2]['url'] = ['/logistics/auto-completion/default/index'];

    $this->params['menuItems'][0]['items'][1]['label'] = 'Склады';
//
    $this->params['menuItems'][0]['items'][1]['items'][0]['label'] = Yii::t('app', 'Группы для пополнения');
    $this->params['menuItems'][0]['items'][1]['items'][0]['url'] = ['/logistics/automatic-order/group-spec'];
//
    $this->params['menuItems'][0]['items'][1]['items'][1]['label'] = Yii::t('app', 'Отчет по литерам');
    $this->params['menuItems'][0]['items'][1]['items'][1]['url'] = ['/logistics/report/storage-state-report'];

    $this->params['menuItems'][0]['items'][1]['items'][2]['label'] = Yii::t('app', 'Остатки по автомобилям');
    $this->params['menuItems'][0]['items'][1]['items'][2]['url'] = ['/logistics/storage-report/index'];
//
    $this->params['menuItems'][0]['items'][1]['items'][3]['label'] = Yii::t('app', 'Оприходование на склад');
    $this->params['menuItems'][0]['items'][1]['items'][3]['url'] = ['/logistics/admission-terminal/index'];
//
    $this->params['menuItems'][0]['items'][1]['items'][4]['label'] = Yii::t('app', 'СКЛАД-ОТСТОЙНИК');
    $this->params['menuItems'][0]['items'][1]['items'][4]['url'] = ['/logistics/admission-terminal/sump-index'];
//
    $this->params['menuItems'][0]['items'][1]['items'][5]['label'] = Yii::t('app', 'Контроль остатков');
    $this->params['menuItems'][0]['items'][1]['items'][5]['url'] = ['/logistics/storage-balance-monitor/default/index'];

    $this->params['menuItems'][0]['items'][1]['items'][6]['label'] = Yii::t('app', 'Места хранения');
    $this->params['menuItems'][0]['items'][1]['items'][6]['url'] = ['/logistics/storage/index'];

    $this->params['menuItems'][0]['items'][1]['items'][7]['label'] = Yii::t('app', 'Распечатка штрихкодов');
    $this->params['menuItems'][0]['items'][1]['items'][7]['url'] = ['/logistics/default/print-storage-barcode'];

    $this->params['menuItems'][0]['items'][1]['items'][8]['label'] = Yii::t('app', 'Оприходывание на склад NEW');
    $this->params['menuItems'][0]['items'][1]['items'][8]['url'] = ['/logistics/naryad-transfer-monitor/default/index'];

    $this->params['menuItems'][0]['items'][1]['items'][9]['label'] = Yii::t('app', 'Печать этикеток оптовикам');
    $this->params['menuItems'][0]['items'][1]['items'][9]['url'] = ['/logistics/print-labels/default/index'];

    $this->params['menuItems'][0]['items'][1]['items'][10]['label'] = Yii::t('app', 'Печать этикеток Румыния');
    $this->params['menuItems'][0]['items'][1]['items'][10]['url'] = ['/logistics/print-labels/romania/index'];

    $this->params['menuItems'][0]['items'][1]['items'][11]['label'] = Yii::t('app', 'Печать этикеток Индия');
    $this->params['menuItems'][0]['items'][1]['items'][11]['url'] = ['/logistics/print-labels/india/index'];

    $this->params['menuItems'][0]['items'][1]['items'][12]['label'] = Yii::t('app', 'Печать этикеток Чехия');
    $this->params['menuItems'][0]['items'][1]['items'][12]['url'] = ['/logistics/print-labels/czech/index'];

    $this->params['menuItems'][0]['items'][2]['label'] = 'Ручное пополнение';
    $this->params['menuItems'][0]['items'][2]['url'] = ['/logistics/replenishment-monitor/default/index'];

    $this->params['menuItems'][0]['items'][3]['label'] = 'Контроль отгрузок';

    $this->params['menuItems'][0]['items'][3]['items'][0]['label'] = Yii::t('app', 'Журнал заказов');
    $this->params['menuItems'][0]['items'][3]['items'][0]['url'] = ['/logistics/implementation-monitor/journal/index'];

//    $this->params['menuItems'][0]['items'][3]['items'][1]['label'] = Yii::t('app', 'Журнал заказов');
//    $this->params['menuItems'][0]['items'][3]['items'][1]['url'] = ['/logistics/implementation-monitor/default/journal'];
//
    $this->params['menuItems'][0]['items'][3]['items'][2]['label'] = Yii::t('app', 'Монитор логистики');
    $this->params['menuItems'][0]['items'][3]['items'][2]['url'] = ['/logistics/logistics-monitor/default/index'];

//    $this->params['menuItems'][0]['items'][3]['items'][2]['label'] = Yii::t('app', 'Дочерний склад');
//    $this->params['menuItems'][0]['items'][3]['items'][2]['url'] = ['/logistics/child-monitor/default/index'];
//
//    $this->params['menuItems'][0]['items'][3]['items'][3]['label'] = Yii::t('app', 'Заказы к отгрузке');
//    $this->params['menuItems'][0]['items'][3]['items'][3]['url'] = ['/logistics/implementation-monitor/default/default2'];

    $this->params['menuItems'][0]['items'][3]['items'][4]['label'] = Yii::t('app', 'Заказы к отгрузке NEW');
    $this->params['menuItems'][0]['items'][3]['items'][4]['url'] = ['/logistics/warehouse-monitor/default/index'];

    $this->params['menuItems'][0]['items'][4]['label'] = 'Прочие справочники';

    $this->params['menuItems'][0]['items'][4]['items'][0]['label'] = Yii::t('app', 'Источники заявок');
    $this->params['menuItems'][0]['items'][4]['items'][0]['url'] = ['/logistics/sources/index'];

    $this->params['menuItems'][0]['items'][4]['items'][1]['label'] = Yii::t('app', 'Транспортные компании');
    $this->params['menuItems'][0]['items'][4]['items'][1]['url'] = ['/logistics/transport-company/crud/index'];

    $this->params['menuItems'][0]['items'][4]['items'][2]['label'] = Yii::t('app', 'Список EAN');
    $this->params['menuItems'][0]['items'][4]['items'][2]['url'] = ['/logistics/ean/default/index'];

    $this->params['menuItems'][0]['items'][5]['label'] = 'Печать документов Европа';
//
    $this->params['menuItems'][0]['items'][5]['items'][0]['label'] = Yii::t('app', 'Упак. лист на отгрузку');
    $this->params['menuItems'][0]['items'][5]['items'][0]['url'] = ['/logistics/print-documents/print-box/index'];

    $this->params['menuItems'][0]['items'][5]['items'][1]['label'] = Yii::t('app', 'Упак. лист на отправку');
    $this->params['menuItems'][0]['items'][5]['items'][1]['url'] = ['/logistics/print-documents/print-shipment/index'];

    $this->params['menuItems'][0]['items'][5]['items'][2]['label'] = Yii::t('app', 'Инвойс / Спецификация');
    $this->params['menuItems'][0]['items'][5]['items'][2]['url'] = ['/logistics/europe-invoice/default/index'];

    $this->params['menuItems'][0]['items'][6]['label'] = 'Печать документов Россия';
//
    $this->params['menuItems'][0]['items'][6]['items'][0]['label'] = Yii::t('app', 'Торг 12');
    $this->params['menuItems'][0]['items'][6]['items'][0]['url'] = ['/logistics/print-documents/print-torg12/index'];

    $this->params['menuItems'][0]['items'][7]['label'] = Yii::t('app', 'Перевоз склада');
    $this->params['menuItems'][0]['items'][7]['url'] = ['/logistics/transfer-storage/default/index'];

    $this->params['menuItems'][0]['items'][8]['label'] = Yii::t('app', 'Список незарезервированных');
    $this->params['menuItems'][0]['items'][8]['url'] = ['/logistics/report/storage-state-not-reserved'];

    $this->params['menuItems'][0]['items'][9]['label'] = Yii::t('app', 'Список зарезервированных');
    $this->params['menuItems'][0]['items'][9]['url'] = ['/logistics/report/storage-state-reserved'];

    $this->params['menuItems'][0]['items'][10]['label'] = Yii::t('app', 'Перебор отстойника');
    $this->params['menuItems'][0]['items'][10]['url'] = ['/logistics/transfer-storage/sump/index'];

    $this->params['menuItems'][0]['items'][11]['label'] = Yii::t('app', 'Перебор осн. склада');
    $this->params['menuItems'][0]['items'][11]['url'] = ['/logistics/transfer-storage/main-storage/index'];
}

//меню для логиста
elseif ($logist) {
    $this->params['menuItems'][0]['items'][0]['label'] = Yii::t('app', 'Журнал заказов');
    $this->params['menuItems'][0]['items'][0]['url'] = ['/logistics/implementation-monitor/default/journal'];
//
    $this->params['menuItems'][0]['items'][1]['label'] = Yii::t('app', 'Монитор логистики');
    $this->params['menuItems'][0]['items'][1]['url'] = ['/logistics/logistics-monitor/default/index'];

    $this->params['menuItems'][0]['items'][2]['label'] = Yii::t('app', 'Заказы к отгрузке');
    $this->params['menuItems'][0]['items'][2]['url'] = ['/logistics/warehouse-monitor/default/index'];

    $this->params['menuItems'][0]['items'][3]['label'] = 'Печать документов Европа';
//
    $this->params['menuItems'][0]['items'][3]['items'][0]['label'] = Yii::t('app', 'Упак. лист на отгрузку');
    $this->params['menuItems'][0]['items'][3]['items'][0]['url'] = ['/logistics/print-documents/print-box/index'];

    $this->params['menuItems'][0]['items'][3]['items'][1]['label'] = Yii::t('app', 'Упак. лист на отправку');
    $this->params['menuItems'][0]['items'][3]['items'][1]['url'] = ['/logistics/print-documents/print-shipment/index'];

    $this->params['menuItems'][0]['items'][3]['items'][2]['label'] = Yii::t('app', 'Инвойс / Спецификация');
    $this->params['menuItems'][0]['items'][3]['items'][2]['url'] = ['/logistics/europe-invoice/default/index'];

    $this->params['menuItems'][0]['items'][4]['label'] = 'Печать документов Россия';
//
    $this->params['menuItems'][0]['items'][4]['items'][0]['label'] = Yii::t('app', 'Торг 12');
    $this->params['menuItems'][0]['items'][4]['items'][0]['url'] = ['/logistics/print-documents/print-torg12/index'];

    if (in_array(Yii::$app->user->getId(),[36, 11, 12])) {
        $this->params['menuItems'][0]['items'][5]['label'] = Yii::t('app', 'Контроль остатков');
        $this->params['menuItems'][0]['items'][5]['url'] = ['/logistics/storage-balance-monitor/default/index'];
    }
}

//меню для логиста
elseif ($storage) {
//
    $this->params['menuItems'][0]['items'][0]['label'] = Yii::t('app', 'Отчет по литерам');
    $this->params['menuItems'][0]['items'][0]['url'] = ['/logistics/report/storage-state-report'];

    $this->params['menuItems'][0]['items'][1]['label'] = Yii::t('app', 'Остатки по автомобилям');
    $this->params['menuItems'][0]['items'][1]['url'] = ['/logistics/storage-report/index'];
//
    $this->params['menuItems'][0]['items'][2]['label'] = Yii::t('app', 'Оприходование на склад');
    $this->params['menuItems'][0]['items'][2]['url'] = ['/logistics/admission-terminal/index'];
//
    $this->params['menuItems'][0]['items'][3]['label'] = Yii::t('app', 'Оприходывание на склад NEW');
    $this->params['menuItems'][0]['items'][3]['url'] = ['/logistics/naryad-transfer-monitor/default/index'];

    $this->params['menuItems'][0]['items'][4]['label'] = Yii::t('app', 'СКЛАД-ОТСТОЙНИК');
    $this->params['menuItems'][0]['items'][4]['url'] = ['/logistics/admission-terminal/sump-index'];

    $this->params['menuItems'][0]['items'][5]['label'] = Yii::t('app', 'Журнал заказов');
    $this->params['menuItems'][0]['items'][5]['url'] = ['/logistics/implementation-monitor/default/journal'];

    $this->params['menuItems'][0]['items'][6]['label'] = Yii::t('app', 'Монитор логистики');
    $this->params['menuItems'][0]['items'][6]['url'] = ['/logistics/logistics-monitor/default/index'];

//    $this->params['menuItems'][0]['items'][7]['label'] = Yii::t('app', 'Заказы к отгрузке');
//    $this->params['menuItems'][0]['items'][7]['url'] = ['/logistics/implementation-monitor/default/default2'];

    $this->params['menuItems'][0]['items'][7]['label'] = Yii::t('app', 'Заказы к отгрузке NEW');
    $this->params['menuItems'][0]['items'][7]['url'] = ['/logistics/warehouse-monitor/default/index'];

    $this->params['menuItems'][0]['items'][8]['label'] = Yii::t('app', 'Печать этикеток оптовикам');
    $this->params['menuItems'][0]['items'][8]['url'] = ['/logistics/print-labels/default/index'];

    $this->params['menuItems'][0]['items'][9]['label'] = Yii::t('app', 'Печать этикеток Румыния');
    $this->params['menuItems'][0]['items'][9]['url'] = ['/logistics/print-labels/romania/index'];

    $this->params['menuItems'][0]['items'][10]['label'] = Yii::t('app', 'Печать этикеток Индия');
    $this->params['menuItems'][0]['items'][10]['url'] = ['/logistics/print-labels/india/index'];

    $this->params['menuItems'][0]['items'][11]['label'] = Yii::t('app', 'Печать этикеток Чехия');
    $this->params['menuItems'][0]['items'][11]['url'] = ['/logistics/print-labels/czech/index'];


    $this->params['menuItems'][0]['items'][12]['label'] = 'Печать документов Европа';
//
    $this->params['menuItems'][0]['items'][12]['items'][0]['label'] = Yii::t('app', 'Упак. лист на отгрузку');
    $this->params['menuItems'][0]['items'][12]['items'][0]['url'] = ['/logistics/print-documents/print-box/index'];

    $this->params['menuItems'][0]['items'][12]['items'][1]['label'] = Yii::t('app', 'Упак. лист на отправку');
    $this->params['menuItems'][0]['items'][12]['items'][1]['url'] = ['/logistics/print-documents/print-shipment/index'];

    $this->params['menuItems'][0]['items'][12]['items'][2]['label'] = Yii::t('app', 'Инвойс / Спецификация');
    $this->params['menuItems'][0]['items'][12]['items'][2]['url'] = ['/logistics/europe-invoice/default/index'];

    $this->params['menuItems'][0]['items'][13]['label'] = 'Печать документов Россия';
//
    $this->params['menuItems'][0]['items'][13]['items'][0]['label'] = Yii::t('app', 'Торг 12');
    $this->params['menuItems'][0]['items'][13]['items'][0]['url'] = ['/logistics/print-documents/print-torg12/index'];

    $this->params['menuItems'][0]['items'][14]['label'] = Yii::t('app', 'Перевоз склада');
    $this->params['menuItems'][0]['items'][14]['url'] = ['/logistics/transfer-storage/default/index'];

    $this->params['menuItems'][0]['items'][15]['label'] = Yii::t('app', 'Список незарезервированных');
    $this->params['menuItems'][0]['items'][15]['url'] = ['/logistics/report/storage-state-not-reserved'];

    $this->params['menuItems'][0]['items'][16]['label'] = Yii::t('app', 'Список зарезервированных');
    $this->params['menuItems'][0]['items'][16]['url'] = ['/logistics/report/storage-state-reserved'];

    $this->params['menuItems'][0]['items'][17]['label'] = Yii::t('app', 'Контроль остатков');
    $this->params['menuItems'][0]['items'][17]['url'] = ['/logistics/storage-balance-monitor/default/index'];

    $this->params['menuItems'][0]['items'][18]['label'] = Yii::t('app', 'Перебор осн. склада');
    $this->params['menuItems'][0]['items'][18]['url'] = ['/logistics/transfer-storage/main-storage/index'];
}else{
    $this->params['menuItems'][0]['items'][6]['label'] = Yii::t('app', 'Журнал заказов');
    $this->params['menuItems'][0]['items'][6]['url'] = ['/logistics/implementation-monitor/default/journal'];

    if ($useEan) {
        $this->params['menuItems'][0]['items'][7]['label'] = Yii::t('app', 'Список EAN');
        $this->params['menuItems'][0]['items'][7]['url'] = ['/logistics/ean/default/index'];
    }

    if ($dispatcher) {
        $this->params['menuItems'][0]['items'][7]['label'] = Yii::t('app', 'Контроль остатков');
        $this->params['menuItems'][0]['items'][7]['url'] = ['/logistics/storage-balance-monitor/default/index'];
    }
}
