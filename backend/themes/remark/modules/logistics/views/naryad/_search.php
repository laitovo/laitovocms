<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\logistics\models\Sources;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\NaryadSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="naryad-search">

    <?php $form = ActiveForm::begin([
        'id' => 'my-search-form',
        'action' => ['index'],
        'method' => 'post',
        'options' => ['data-pjax' => '1'],
    ]); ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'team_id') ?>

    <div class="row">
        <div class="col-md-4">  
            <?php  echo $form->field($model, 'article') ?>
        </div>  

        <div class="col-md-4">  
            <?= $form->field($model, 'source_id')->dropDownList(Sources::asList()); ?>
        </div>  

        <div class="col-md-4">  
            <?php  echo $form->field($model, 'responsible_person') ?>
        </div>  
    </div>
    
    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'author_id') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updater_id') ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="icon wb-search"></i> ' . Yii::t('app','Искать'), ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('<i class="icon wb-refresh"></i> ' . Yii::t('app','Очистить'), [
            'class' => 'btn btn-default',
            'onclick' => 'console.log($(\'form\'));console.log($(\'[id ^= "naryadsearch"]\').val(\'\'));$(\'#my-search-form\').submit();'
            ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
