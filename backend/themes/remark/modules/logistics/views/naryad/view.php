<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\Naryad */

$this->render('../menu');

$this->title = Yii::t('app','Список нарядов');
$viewtitle = $model->name;
?>
<?Pjax::begin(['enablePushState' => true]);?>

<div class="naryad-view">

    <h4><?= $viewtitle ?></h4>
    <hr>

    <p>
        <?= Html::a('<i class="icon wb-edit"></i> ' . Yii::t('app','Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="icon wb-trash"></i> ' . Yii::t('app','Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data-pjax' => '1',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'label' => 'Номер наряда'
            ],
            'article',
            [
                'attribute' => 'source.title',
                'label' => 'Источник'
            ],
            'responsible_person',
            'comment:ntext',
            'created_at:datetime',
            [
                'attribute' => 'author.name',
                'label' => 'Автор'
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updater.name',
                'label' => 'Редактор'
            ],
        ],
    ]) ?>

</div>
<?Pjax::end();?>
