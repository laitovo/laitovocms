<?php

/**
 * @var $articles []
 * @var $ean string|null Ean number
 */


$generator = new Picqer\Barcode\BarcodeGeneratorSVG();
?>

<p class="page-break-<?= $model->id ?>"></p>
<table class="invoice_items" width="270" cellpadding="2" cellspacing="7"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td colspan="2" style="font-family: 'Roboto', sans-serif;font-size: 11pt;padding: 5px">
            Protective screens for car windows
        </td>
    </tr>
    <tr>
        <td colspan="2" style="font-family: 'Roboto', sans-serif;font-size: 18pt;line-height: 0.7em">
            <? $number = $model->id; ?>
            <?= str_replace((mb_substr($number, 4, mb_strlen($number))),
                ('<b style="font-size: 31pt">' . mb_substr($number, 4, mb_strlen($number)) . '</b>'), $number); ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-collapse: collapse;border-bottom: none;border: none;">
            <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 2) ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-collapse: collapse;border-bottom: none;border: none;font-family: 'Roboto', sans-serif;font-size: 11pt;line-height: 1em;">
            <?= (count($articles) > 1 ? "SET : " : "") . "<span style=\"white-space:nowrap\">" . implode('</span>, <span style="white-space:nowrap">',$articles) . "</span>"; ?>
        </td>
    </tr>
    <?if ($ean):?>
    <tr>
        <td colspan="2" style="border-collapse: collapse;border-bottom: none;border: none;font-family: 'Roboto', sans-serif;font-size: 11pt;line-height: 1em;">
            <b>EAN 13</b> : <?= $ean?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-collapse: collapse;border-bottom: none;border: none;font-family: 'Roboto', sans-serif;font-size: 11pt;line-height: 1em;">
            <?= $generator->getBarcode($ean, $generator::TYPE_CODE_128, 2) ?>
        </td>
    </tr>
    <?endif;?>
    </tbody>
</table>

