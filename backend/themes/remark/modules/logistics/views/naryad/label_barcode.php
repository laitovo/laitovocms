<?php

/**
 * @var string $barcode
 */
$generator = new Picqer\Barcode\BarcodeGeneratorSVG();
?>

<table class="invoice_items" width="270" cellpadding="2" cellspacing="7"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td colspan="2" style="border-collapse: collapse;border-bottom: none;border: none;">
            <?= $generator->getBarcode($barcode, $generator::TYPE_CODE_128, 2) ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;font-family: 'Roboto', sans-serif;font-size: 24pt;line-height: 0.7em">
            <b><?= $barcode ?></b>
        </td>
    </tr>
    </tbody>

</table>
<!--<table class="invoice_items" width="270" cellpadding="2" cellspacing="2"-->
<!--       style="font-size: 11px;text-align: center;">-->
<!--    <tbody>-->
<!--        <tr>-->
<!--            <td style="border: none;font-size: 11px">Tel. +49(0)4095063310</td>-->
<!--            <td style="border: none;font-size: 11px">info@laitovo.de</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td colspan="2" style="border: none;font-size: 11px;">-->
<!--                Made by request of LAITOVO Manufactory T. & S. GMBH-->
<!--                Ferdinandstrasse 25-27 D-20095 Hamburg Germany-->
<!--            </td>-->
<!--        </tr>-->
<!--    </tbody>-->
<!--</table>-->
