<?php
$generator = new Picqer\Barcode\BarcodeGeneratorSVG();
?>

<p class="page-break-<?= $model->id ?>"></p>
<table class="invoice_items" width="270" cellpadding="2" cellspacing="7"
       style="border: none;font-size: 11px;text-align: center;">
    <tbody>
    <tr>
        <td colspan="2" style="font-family: 'Roboto', sans-serif;font-size: 24pt;line-height: 0.7em">
            <? $number = $model->id; ?>
            <?= str_replace((mb_substr($number, 4, mb_strlen($number))),
                ('<b style="font-size: 37pt">' . mb_substr($number, 4, mb_strlen($number)) . '</b>'), $number); ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-collapse: collapse;border-bottom: none;border: none;">
            <?= $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128, 2) ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="border-left: none;border-right: none; font-size: 14px;font-family: 'Roboto', sans-serif;font-size: 24pt;line-height: 0.7em">
            <b><?= $model->article ?></b>
        </td>
    </tr>
    </tbody>

</table>

