<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\Naryad */

$this->render('../menu');

$this->title = Yii::t('app','Список нарядов');
$viewtitle = Yii::t('app','Изменение наряда');

?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="naryad-update">

    <h4><i><?= $viewtitle ?></i></h4>
    <hr>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?Pjax::end();?>