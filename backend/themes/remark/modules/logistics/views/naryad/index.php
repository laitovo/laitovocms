<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\NaryadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->render('../menu');

$this->title = Yii::t('app','Список нарядов');
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="naryad-index">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary pull-left','title' => Yii::t('app','Добавить наряд')]) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'article',
            [
                'attribute' => 'source.title',
                'label' => 'Источник',
            ],
            'responsible_person',
            // 'comment:ntext',
            'created_at:datetime',
            // 'author_id',
            // 'updated_at',
            // 'updater_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttonOptions' => ['data-pjax' => '1'],
            ],
        ],
    ]); ?>
</div>
<?Pjax::end();?>
