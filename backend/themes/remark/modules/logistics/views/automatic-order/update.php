<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\AutomaticOrder */

$this->title = Yii::t('app','Настройки модуля автозаказа');
$this->params['breadcrumbs'][] = ['label' => 'Automatic Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->render('../menu');

?>
<div class="automatic-order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
