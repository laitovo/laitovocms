<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\AutomaticOrder */

$this->title = 'Create Automatic Order';
$this->params['breadcrumbs'][] = ['label' => 'Automatic Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// $customScript = <<< SCRIPT
//     $(function(){
//     $('.modal-button').click(function(){
//         $('#modalView').modal('show').find('#modalContentView').load($(this).attr('value'));

//     });});
// SCRIPT;
// $this->registerJs($customScript, \yii\web\View::POS_READY);

// //вывод модального окна
// Modal::begin(['id'=>'modalView', 'size'=>'modal-md']);
// Pjax::begin(['enablePushState' => false]);
// echo "<div id='modalContentView'></div>";
// Pjax::end();
// Modal::end();
//вывод модального окна
//
?>
<div class="automatic-order-create">
    <div class="row">
        <div class="col-sm-4">
            <h3 class="pull-left">Артикулы</h3>
            <a href="<?=Url::to('add-article')?>" class="btn btn-default btn-sm pull-right modal-button">Добавить артикул</a>
            <div class="clearfix"></div>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $realization,
                'layout' => '{items}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'order_id',
                        'label' => '<span title="Заявка" class="glyphicon glyphicon-list-alt"></span> Заказ',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'article',
                        'label' => '<span title="Артикул" class="glyphicon glyphicon-asterisk"></span> Артикул',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'upn',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-file"></span> UPN',
                        'encodeLabel' => false,
                        'filter' => true,
                        'value' => function($data) {
                            return $data->upn ? $data->upn->upn_id : 'Не назначен';
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{send} {reserv}', //{time} {update} {cancel} {update}
                        'buttons' => [
                            'send' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>',
                                    Url::to(['add-upn','article' => $model->article]),
                                    [
                                        'title' => Yii::t('app', 'Добавить UPN'),
                                    ]);
                            },
                            'reserv' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-cloud-upload"></span>',
                                    Url::to(['add-reserv','article' => $model->article,'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'Зарезервировать'),
                                    ]);
                            },
                            // 'send'=>function ($url, $model ) use ($location) {

                            //     $t = $model->user_id ? Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user','user'=>$model->user_id]) : Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user', 'workplace'=>$model->id, 'location_type'=>$location->type]);
                            //     return Html::button('<span class="glyphicon glyphicon-share-alt"></span>', ['value'=>Url::to($t), 'class' => 'btn btn-info btn-xs custom_button']);
                            // },
                            // 'cancel'=>function ($url, $model){

                            //     $t = Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/cancel-user','workplace_id'=>$model->id, ]);
                            //     return Html::button('Снять', ['value'=>Url::to($t), 'class' => 'btn btn-warning btn-xs custom_button']);
                            // }
                        ],
                        // 'controller' => 'automatic-order',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-sm-4">
            <h3 class="pull-left">Остатки оборотный</h3>
            <!-- <button class="btn btn-default btn-sm pull-right modal-button">Зарезервировать</button> -->
            <div class="clearfix"></div>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $circulationBalance,
                'layout' => '{items}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'upn_id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-file"></span> UPN',
                        'encodeLabel' => false,
                    ],
                    [
                        'attribute' => 'literal',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-inbox"></span> Литера',
                        'encodeLabel' => false,
                    ],
                    [
                        'attribute' => 'storage.title',
                        'label' => '<span title="Идентификатор" class="glyphicon glyphicon-home"></span> Склад',
                        'encodeLabel' => false,
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-sm-4">
            <h3 class="pull-left">Остатки временный</h3>
            <!-- <button class="btn btn-default btn-sm pull-right modal-button">Переместить</button> -->
            <div class="clearfix"></div>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $temporaryBalance,
                'layout' => '{items}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'upn_id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-file"></span> UPN',
                        'encodeLabel' => false,
                    ],
                    [
                        'attribute' => 'literal',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-inbox"></span> Литера',
                        'encodeLabel' => false,
                    ],
                    [
                        'attribute' => 'storage.title',
                        'label' => '<span title="Идентификатор" class="glyphicon glyphicon-home"></span> Склад',
                        'encodeLabel' => false,
                    ],


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{send} {reserv}', //{time} {update} {cancel} {update}
                        'buttons' => [
                            'send' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-circle-arrow-left"></span>',
                                    Url::to(['send-remove','upn' => $model->upn_id]),
                                    [
                                        'title' => Yii::t('app', 'Переместить'),
                                    ]);
                            },
                            // 'send'=>function ($url, $model ) use ($location) {

                            //     $t = $model->user_id ? Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user','user'=>$model->user_id]) : Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user', 'workplace'=>$model->id, 'location_type'=>$location->type]);
                            //     return Html::button('<span class="glyphicon glyphicon-share-alt"></span>', ['value'=>Url::to($t), 'class' => 'btn btn-info btn-xs custom_button']);
                            // },
                            // 'cancel'=>function ($url, $model){

                            //     $t = Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/cancel-user','workplace_id'=>$model->id, ]);
                            //     return Html::button('Снять', ['value'=>Url::to($t), 'class' => 'btn btn-warning btn-xs custom_button']);
                            // }
                        ],
                        // 'controller' => 'automatic-order',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">

        <div class="jumbotron">
  <h1 class="text-center">Автоматическое пополнение складских остатков</h1> 
  <?= GridView::widget([
                // 'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $upns,
                'layout' => '{items}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-file"></span> UPN',
                        'encodeLabel' => false,
                    ],

                    [
                        'attribute' => 'id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-log-in"></span> Источник',
                        'encodeLabel' => false,
                    ],

                    [
                        'attribute' => 'id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-time"></span> Дата создания',
                        'encodeLabel' => false,
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]); ?>
</div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <h3 class="pull-left">Внутренние нужды</h3>
            <a href="<?=Url::to('add-in-upn')?>" class="btn btn-default btn-sm pull-right modal-button">Добавить вн. наряд</a>
            <div class="clearfix"></div>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $upns,
                'layout' => '{items}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'source.title',
                        'label' => '<span title="Заявка" class="glyphicon glyphicon-log-in"></span> Источник',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'article',
                        'label' => '<span title="Артикул" class="glyphicon glyphicon-asterisk"></span> Артикул',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-file"></span> UPN',
                        'encodeLabel' => false,
                        'filter' => true,
                        'value' => function($data) {
                            return $data->id ? $data->id : 'Не назначен';
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]); ?>
        </div>
        
        <div class="col-sm-4">
            <h3 class="pull-left">UPN's</h3>
            <!-- <button class="btn btn-default btn-sm pull-right modal-button">Добавить артикул</button> -->
            <div class="clearfix"></div>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $upns,
                'layout' => '{items}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-file"></span> UPN',
                        'encodeLabel' => false,
                    ],

                    [
                        'attribute' => 'id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-log-in"></span> Источник',
                        'encodeLabel' => false,
                    ],

                    [
                        'attribute' => 'id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-time"></span> Дата создания',
                        'encodeLabel' => false,
                    ],

[
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{send}', //{time} {update} {cancel} {update}
                        'buttons' => [
                            'send' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-play-circle"></span>',
                                    Url::to(['send-proizv','id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'Произвести'),
                                    ]);
                            },
                            // 'send'=>function ($url, $model ) use ($location) {

                            //     $t = $model->user_id ? Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user','user'=>$model->user_id]) : Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user', 'workplace'=>$model->id, 'location_type'=>$location->type]);
                            //     return Html::button('<span class="glyphicon glyphicon-share-alt"></span>', ['value'=>Url::to($t), 'class' => 'btn btn-info btn-xs custom_button']);
                            // },
                            // 'cancel'=>function ($url, $model){

                            //     $t = Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/cancel-user','workplace_id'=>$model->id, ]);
                            //     return Html::button('Снять', ['value'=>Url::to($t), 'class' => 'btn btn-warning btn-xs custom_button']);
                            // }
                        ],
                        // 'controller' => 'automatic-order',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-sm-4">
            <h3 class="pull-left">Диспетчер склад</h3>
            <!-- <button class="btn btn-default btn-sm pull-right modal-button">Распределить по складам</button> -->
            <div class="clearfix"></div>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $comingUpns,
                'layout' => '{items}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'logist_id',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-file"></span> UPN',
                        'encodeLabel' => false,
                    ],

                    [
                        'attribute' => 'literal',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-inbox"></span> Литера',
                        'encodeLabel' => false,
                        'value' => function($data) {
                            return $data->getValueLiteral();
                        }
                    ],

                    [
                        'attribute' => 'created_at',
                        'label' => '<span title="UPN" class="glyphicon glyphicon-time"></span> Произведено',
                        'format' => 'datetime',
                        'encodeLabel' => false,
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{sendvr} {sendob}', //{time} {update} {cancel} {update}
                        'buttons' => [
                            'sendvr' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-circle-arrow-left"></span>',
                                    Url::to(['send-oborot','upn' => $model->logist_id]),
                                    [
                                        'title' => Yii::t('app', 'Оборотный'),
                                    ]);
                            },
                            'sendob' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-circle-arrow-right"></span>',
                                    Url::to(['send-temporary','upn' => $model->logist_id]),
                                    [
                                        'title' => Yii::t('app', 'Временный'),
                                    ]);
                            },
                            // 'send'=>function ($url, $model ) use ($location) {

                            //     $t = $model->user_id ? Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user','user'=>$model->user_id]) : Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/change-user', 'workplace'=>$model->id, 'location_type'=>$location->type]);
                            //     return Html::button('<span class="glyphicon glyphicon-share-alt"></span>', ['value'=>Url::to($t), 'class' => 'btn btn-info btn-xs custom_button']);
                            // },
                            // 'cancel'=>function ($url, $model){

                            //     $t = Yii::$app->urlManager->createUrl(['laitovo/erp-location-workplace/cancel-user','workplace_id'=>$model->id, ]);
                            //     return Html::button('Снять', ['value'=>Url::to($t), 'class' => 'btn btn-warning btn-xs custom_button']);
                            // }
                        ],
                        // 'controller' => 'automatic-order',
                        'buttonOptions' => ['class' => 'deleteconfirm'],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
