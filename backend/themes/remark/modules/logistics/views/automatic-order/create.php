<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\AutomaticOrder */

$this->title = Yii::t('app','Настройки модуля автозаказа');
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>
<div class="automatic-order-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
