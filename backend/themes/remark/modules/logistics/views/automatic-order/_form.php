<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\AutomaticOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="automatic-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'team_id')->textInput() ?>

    <?= $form->field($model, 'unitOfPeriod')->textInput() ?>

    <?= $form->field($model, 'measuringInterval')->textInput() ?>

    <?= $form->field($model, 'frequency')->textInput() ?>
    
    <?= $form->field($model, 'fixRate')->textInput() ?>

    <?//= $form->field($model, 'created_at')->textInput() ?>

    <?//= $form->field($model, 'author_id')->textInput() ?>

    <?//= $form->field($model, 'updated_at')->textInput() ?>

    <?//= $form->field($model, 'updater_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Сохранить'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
