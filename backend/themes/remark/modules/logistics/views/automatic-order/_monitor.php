<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\widgets\GridView;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use backend\modules\laitovo\models\MainProductInfo;
use backend\modules\logistics\models\AutomaticOrderMonitor;

$this->render('../menu');

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\AutomaticOrder */

// var_dump($fromStorage->getModels());
// die();

$this->title = Yii::t('app','Монитор автозаказа');
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->view->registerJs('
    var searchArticle = function() { 
        $.get("' . Url::to(['/ajax/automatic-article-search']) . '",{"article":$("#check-article").val()},function(data){ 
            $(".auto-search").html(data.message.auto); 
            $(".group-search").html(data.message.group); 
            $(".article-search").html(data.message.article); 
            $(".rpp-search").html(data.message.rpp); 
            $(".rp-search").html(data.message.rp); 
            $(".pnp-search").html(data.message.pnp); 
            $(".pr-search").html(data.message.pr); 
            $(".ksp-search").html(data.message.ksp); 
            $(".kk-search").html(data.message.kk); 
            $(".chr-search").html(data.message.chr); 
            $(".op-search").html(data.message.op); 
            $(".of-search").html(data.message.of); 
        }); 
    } 
', \yii\web\View::POS_END); 
 
 
Yii::$app->view->registerJs('
$( document ).ready(function() {
    $.get("' . Url::to(['/ajax/product-types']) . '",{"type":$("#otkcreate-window").val(),"cloth":$("#otkcreate-tkan").val()},function(data){
                $("#otkcreate-goodid").html("");
                if (Object.keys(data).length!=1)
                    $("#otkcreate-goodid").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#otkcreate-goodid").append($("<option></option>").attr("value",key).text(value));
                });
            });


    var monitor = localStorage.getItem("monitorshow");
    if (monitor == "order") {
        $(\'.logic-info\').css(\'display\',\'none\');$(\'.automatic-info\').fadeIn(); 
    } else if (monitor == "storage") {
        $(\'.automatic-info\').css(\'display\',\'none\');$(\'.logic-info\').fadeIn(); 
    }

})
', \yii\web\View::POS_END);
?>
<div class="automatic-order-create">
   <div class="row">
    <div class="col-sm-4">
        <div class="input-group input-group-sm">
          <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
           <?= Html::input($type='text', $name = null, $value = null, ['class' => 'form-control','id' => 'check-article']); ?>
        </div>
    </div>
    <div class="col-sm-2">
       <?= Html::a($text='search', $url = null, [
            'class' => 'btn btn-default btn-sm',
            'onclick' => 'if ($(\'#check-article\').val()) {searchArticle(),$(\'.check-article-info\').slideDown()} else {$(\'.check-article-info\').slideUp()};
            ' 
        ]); ?>
        <?= Html::button($content='reset', [
            'class' => 'btn btn-default btn-sm',
            'onclick' => '($(\'#check-article\').val(\'\')); $(\'.check-article-info\').slideUp();
            ' 
        ]); ?>
    </div>
    <div class="col-sm-offset-4 col-sm-2">
        <div class="btn-group btn-group-sm pull-right" data-toggle="buttons">
          <label class="btn btn-info" onclick="$('.logic-info').css('display','none');$('.automatic-info').fadeIn();localStorage.setItem('monitorshow', 'order');">
            <?= Html::radio('name', $checked = true, [
                'autocomplete' => 'off',
                'checked' => 'checked',
                ]); ?> Автозаказ
          </label>
          <label class="btn btn-info" onclick="$('.automatic-info').css('display','none');$('.logic-info').fadeIn();localStorage.setItem('monitorshow', 'storage');">
            <?= Html::radio('name', $checked = false, [
                'autocomplete' => 'off',
                'checked' => 'checked',
                ]); ?> Остатки
          </label>
        </div>

    </div>
    <div class="cliearfix"></div>

   </div>
    <hr>
     <div class="row check-article-info" style="display: none;">
       <!-- Область вывода статиски -->
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>(Аналоги) Автомобиль</th> 
                    <th>Наименование группы</th> 
                    <th>Артикул</th> 
                    <th>РПП</th> 
                    <th>РП</th> 
                    <th>ПНП</th> 
                    <th>ПР</th> 
                    <th>КСП</th> 
                    <th>КК</th> 
                    <th>ЧР</th> 
                    <th>ОП</th> 
                    <th>ОФ</th> 
                </tr> 
            </thead> 
            <tbody> 
                <tr> 
                    <td class="auto-search"></td> 
                    <td class="group-search"></td> 
                    <td class="article-search"></td> 
                    <td class="rpp-search"></td> 
                    <td class="rp-search"></td> 
                    <td class="pnp-search"></td> 
                    <td class="pr-search"></td> 
                    <td class="ksp-search"></td> 
                    <td class="kk-search"></td> 
                    <td class="chr-search"></td> 
                    <td class="op-search"></td> 
                    <td class="of-search"></td> 
                </tr> 
            </tbody> 
        </table>
   </div>
   <div class="row logic-info" style="display: none;">
       <!-- Область вывода статиски -->
       <div class="col-sm-12">
           
        <div class="well well-lg text-center"><span style="font-weight:bold;font-size:1.5em;">Общая реализация по группам продуктов 2019 год</span></div>
       <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $groupsMain,
            'layout' => '{items}',
            'show' => ['title', 'id','realization','realization30','increase'],
            'salt' => 'dasdasdasda',
            'customHead' => '
               <table class="table-hover table">
                   <tr>
                       <th style="font-weight:bold;font-size:2em;line-height:1.7em" rowspan="2" width="15%">ИТОГО:</th>
                       <th class="text-center">Реализация<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last'). '</th>
                       <th class="text-center">+30 дней<br>' . $model->getMonthTitle('yearnext') . '</th>
                       <th class="text-center">Прирост к<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last') . '</th>
                   </tr>
                   <tr>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getRealizationSumByGroup('before',2) . ' / ' . $model->getRealizationSumByGroup('last',2) . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getRealizationSumByGroup('next',2) . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getIncreaseSumByGroup('before',2) .' / ' . $model->getIncreaseSumByGroup('last',2) . '</td>
                   </tr>
               </table> 
            ',
            'showCustomHead' => true,
            'footerRowOptions'=>['style'=>'font-weight:bold;text-decoration: underline;'],
            'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'alias',
                    'label' => ' Номер гр.',
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'title',
                    'label' => ' Наименование',
                    'encodeLabel' => false,
                    'filter' => 'true',
                ],
                [
                    'attribute' => 'realization',
                    'label' => 'Реализация<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last'),
                    'encodeLabel' => false,
                    'footer' => $model->getRealizationSumByGroup('before',2) .' / ' . $model->getRealizationSumByGroup('last',2),
                    'filter' => true,
                ],
                [
                    'attribute' => 'realization30',
                    'label' => ' +30 дней<br>' . $model->getMonthTitle('yearnext'),
                    'encodeLabel' => false,
                    'filter' => true,
                    'footer' => $model->getRealizationSumByGroup('next',2),
                ],
                [
                    'attribute' => 'increase',
                    'label' => 'Прирост<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last'),
                    'encodeLabel' => false,
                    'filter' => true,
                    'footer' => $model->getIncreaseSumByGroup('before',2) .' / ' . $model->getIncreaseSumByGroup('last',2),
                ],
            ],
        ]); ?>


       </div>
        <div class="col-sm-12">

   
        <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-hover'],
            'dataProvider' => $groupsOther,
            'layout' => '{items}',
            'show' => ['title', 'id','realization','realization30','increase'],
            'salt' => 'dasdasdasda',
            'customHead' => '
               <table class="table-hover table">
                   <tr>
                       <th style="font-weight:bold;font-size:2em;line-height:1.7em" rowspan="2" width="15%">ИТОГО:</th>
                       <th class="text-center">Реализация<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last'). '</th>
                       <th class="text-center">+30 дней<br>' . $model->getMonthTitle('yearnext') . '</th>
                       <th class="text-center">Прирост<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last') . '</th>
                   </tr>
                   <tr>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getRealizationSumByGroup('before',1) . ' / ' . $model->getRealizationSumByGroup('last',1) . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getRealizationSumByGroup('next',1) . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getIncreaseSumByGroup('before',1) .' / ' . $model->getIncreaseSumByGroup('last',1) . '</td>
                   </tr>
               </table> 
            ',
            'showCustomHead' => true,
            'footerRowOptions'=>['style'=>'font-weight:bold;text-decoration: underline;'],
            'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'alias',
                    'label' => ' Номер гр.',
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'title',
                    'label' => ' Наименование',
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'realization',
                    'label' => 'Реализация<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last'),
                    'encodeLabel' => false,
                    'filter' => true,
                    'footer' => $model->getRealizationSumByGroup('before',1) .' / ' . $model->getRealizationSumByGroup('last',1),
                ],
                [
                    'attribute' => 'realization30',
                    'label' => ' +30 дней<br>' . $model->getMonthTitle('next'),
                    'encodeLabel' => false,
                    'filter' => true,
                    'footer' => $model->getRealizationSumByGroup('next',1),
                ],
                [
                    'attribute' => 'increase',
                    'label' => 'Прирост<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last'),
                    'encodeLabel' => false,
                    'filter' => true,
                    'footer' => $model->getIncreaseSumByGroup('before',1) .' / ' . $model->getIncreaseSumByGroup('last',1),
                ],
            ],
        ]); ?>

       </div>

       <div class="clearfix"></div>
       <div class="panel panel-info">
          <div class="panel-heading text-center" style="font-size:1.4em">Остатки (План/Факт)</div>
          <div class="panel-body">
            <br>
       <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-striped'],
            'dataProvider' => $fromStorage,
            // 'layout' => '{items}{pager}',
            'customHead' => '
                <div class="well well-lg text-center"><span style="font-weight:bold;font-size:1.3em;">Коэффициенты и обозначения</span></div>
                <table class="table table-bordered">
                    <thead style="font-size:1.1em;font-weight:bolder">
                        <tr>
                            <th style="font-size:1.1em;font-weight:bolder">Сокращение</th>
                            <th style="font-size:1.1em;font-weight:bolder">Описание</th>
                            <th style="font-size:1.1em;font-weight:bolder">Вычисляется</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>РПП</td>
                            <td>Реализация предпоследнего месяца, арт. (' . $model->getMonthTitle('before') .')</td>
                            <td>Сумма реализованных артикулов за период</td>
                        </tr>
                        <tr>
                            <td>РП</td>
                            <td>Реализация послднего месяца, арт. (' . $model->getMonthTitle('last') .')</td>
                            <td>Сумма реализованных артикулов за период</td>
                        </tr>
                        <tr>
                            <td>ПНП</td>
                            <td>Последний непиковый прирост, отн.ед.</td>
                            <td>Реализация непиковый месяц&nbsp;/&nbsp;Реализация&nbsp;+30&nbsp;дней (по предыдущему году в разрезе групп)</td>
                        </tr>
                        <tr>
                            <td>ПР</td>
                            <td>План реализации +30дней</td>
                            <td>Непиковый месяц&nbsp;*&nbsp;ПНП</td>
                        </tr>
                        <tr>
                            <td>КСП</td>
                            <td>Коэффициент скорости производства</td>
                            <td>Средняя скорость производства UPN / 30 дней (текущие -30&nbsp;дней в разрезе групп)</td>
                        </tr>
                        <tr>
                            <td>КК</td>
                            <td>Коэффициент корректирующий</td>
                            <td>Ручная корректировка КСП (прямая зависимость КСП*КК)</td>
                        </tr>
                        <tr>
                            <td>ЧР</td>
                            <td>Частота реализации</td>
                            <td>Количество месяцев, в которых была реализация артикула (период -6&nbsp;месяцев)</td>
                        </tr>
                        <tr>
                            <td>ОФ</td>
                            <td>Остатки фактические</td>
                            <td>Сумма остатков на складах</td>
                        </tr>
                        <tr>
                            <td>ОП</td>
                            <td>Остатки плановые</td>
                            <td>ПР * КСП * КК</td>
                        </tr>
                        <tr>
                            <td>РДР</td>
                            <td>Расчетная доля реализации планируемых складских остатков</td>
                            <td>Отношение количества реализаций с плановых остатков / на общее количество реализаций (текущий месяц)</td>
                        </tr>
                    </tbody>
                </table>
                <p>*** РДР считается не учитывая группы автопополнения. Сравнивается реализация всех автошторок с выбранными на автопополнение.</p>
               <table class="table-hover table">
                   <tr>
                       <th style="font-weight:bold;font-size:2em;line-height:1.7em" width="15%">ИТОГО:</th>
                       <th class="text-center">Реализация<br>' . $model->getMonthTitle('before') . ' / ' . $model->getMonthTitle('last'). '</th>
                       <th class="text-center">Последний<br>непиковый прирост</th>
                       <th class="text-center">План<br>реализации</th>
                       <th class="text-center">Коэффициент<br>скорости производства (средний)</th>
                       <th class="text-center">Коэффициент<br>корректировки</th>
                       <th class="text-center">Остаток<br>фактический</th>
                       <th class="text-center">Остаток<br>плановый</th>
                   </tr>
                   <tr>
                       <td style="font-weight:bold;font-size:1.4em;color:blue">
                            РДР = <span  title ="Количество процентов позиций в которые мы попали" data-toggle="tooltip" data-placement="top">' . $model->getShareOfBalance() . '</span><br>
                            Со склада:' .$model->getShareOfBalance2('in') .'<br>
                            C производства:' .$model->getShareOfBalance2('out') .'<br>
                            Всего:' .$model->getShareOfBalance2('all') .'<br>
                            Отношение:<span  title ="Количество процентов позиций в которые мы попали" data-toggle="tooltip" data-placement="top">' .$model->getShareOfBalance2() .'</span>
                       </td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getRealizationSumByArticle('before') . ' / ' . $model->getRealizationSumByArticle('last') . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getIncreaseSumByArticle() . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getSumPlanByArticle() . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getMeanProizvRate() . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getFixRate() . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getSumFactBalance() . '</td>
                       <td class="text-center" style="font-weight:bold;font-size:1.4em">' . $model->getSumPlanStorageBalance() . '</td>
                   </tr>
               </table> 
            ',
            'showCustomHead' => true,
            // 'showFooter' => true,
            'footerRowOptions'=>['style'=>'font-weight:bold;text-decoration: underline;'],
            'show' => ['car','groupId','groupTitle','article','realizationLast','realizationBefore','increase','realizationPlan','balanceFact','balancePlan'],
            'salt' => 'hgfhgfhgf213',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'car',
                    'label' => '<span style="color:brown;font-weight:bold" title ="Аналоги продукта" data-toggle="tooltip" data-placement="top">(Аналоги)</span>  Автомобиль',
                    'encodeLabel' => false,
                    'format' => 'raw',
                    'filter' => true,
                    'value' => function($data) use ($model) {
                        return '<span style="color:brown;font-weight:bold" title ="' . $model->getCarAnalogsNames($data['article']) . '" data-toggle="tooltip" data-placement="top">( ' . $model->getCarAnalogsCount($data['article']) . ' )</span> - ' . $model->getCarName($data['article']);
                    }
                ],
                // [           
                //     'attribute' => 'groupAlias',
                //     'label' => 'Группа',
                //     'encodeLabel' => false,
                //     'filter' => true,
                //     'value' => function($data) use ($model) {
                //         return isset($data['groupAlias']) ? $data['groupAlias'] : '---';
                //     }
                // ],
                [
                    'attribute' => 'groupTitle',
                    'label' => ' Наим. Груп',
                    'encodeLabel' => false,
                    'filter' => true,
                    'format' => 'html',
                    'value' => function($data) use ($model) {
                        return isset($data['groupTitle']) ? '<span style="color:green;font-weight:bold">( ' . $data['groupAlias'] . ' )</span> - ' . $data['groupTitle'] : '---';
                    }
                ],
                [
                    'attribute' => 'article',
                    'label' => ' Артикул',
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                // [
                //     'attribute' => 'storage_id',
                //     'label' => ' Склад',
                //     'encodeLabel' => false,
                //     'filter' => true,
                // ],
                [
                    'attribute' => 'realizationBefore',
                    'label' => '<span title="Реализация предпоследнего месяца, арт. (' . $model->getMonthTitle('before') .')" data-toggle="tooltip" data-placement="top">РПП</span>',
                    // 'label' => ' Реализация<br>' . $model->getMonthTitle('before'),
                    'encodeLabel' => false,
                    'format' => 'html',
                    'filter' => true,
                    'value' => function($data) use ($model) {
                        return $model->getRealizationByArticle($data['article'],'before',true) ;
                    }
                ],
                [
                    'attribute' => 'realizationLast',
                    'label' => '<span title="Реализация послднего месяца, арт. (' . $model->getMonthTitle('last') .')" data-toggle="tooltip" data-placement="top">РП</span>',
                    // 'label' => ' Реализация<br>' . $model->getMonthTitle('last'),
                    'encodeLabel' => false,
                    'filter' => true,
                    'format' => 'html',
                    'value' => function($data) use ($model) {
                        return $model->getRealizationByArticle($data['article'],'last',true) ;
                    }
                ],
                [
                    'attribute' => 'increase',
                    'label' => '<span title="Последний непиковый прирост, отн.ед." data-toggle="tooltip" data-placement="top">ПНП</span>',
                    // 'label' => ' Последний<br> не пиковый <br>прирост, %',
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'realizationPlan',
                    'label' => '<span title="План реализации +30дней" data-toggle="tooltip" data-placement="top">ПР</span>',
                    // 'label' => ' План<br> реализаций',
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'rate',
                    'label' => '<span title="Коэффициент скорости производства" data-toggle="tooltip" data-placement="top">КСП</span>',
                    // 'label' => ' Коэффициент<br> скорости<br> производства',
                    // 'footer' => $model->getSumPlanByArticle(),
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'fixrate',
                    'label' => '<span title="Коэффициент корректирующий" data-toggle="tooltip" data-placement="top">КК</span>',
                    // 'label' => ' Коэффициент<br> корректирующий',
                    // 'footer' => $model->getSumPlanByArticle(),
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'frequence',
                    'label' => '<span title="Частота реализации" data-toggle="tooltip" data-placement="top">ЧР</span>',
                    // 'label' => ' Частота<br> реализации',
                    // 'footer' => $model->getSumPlanByArticle(),
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'balanceFact',
                    'label' => '<span title="Остатки фактические" data-toggle="tooltip" data-placement="top">ОФ</span>',
                    'encodeLabel' => false,
                    'filter' => true,
                ],
                [
                    'attribute' => 'balancePlan',
                    'label' => '<span title="Остатки плановые" data-toggle="tooltip" data-placement="top">ОП</span>',
                    'encodeLabel' => false,
                    'filter' => true,
                ],
            ],
        ]); ?>
   </div>
    </div>
    </div>


   <div class="automatic-info">
   <div class="row">
    <div class="col-sm-2">
        <div class="input-group input-group-sm">
          <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
           <?= Html::input($type='text', $name = 'OtkCreate[carArticle]', $value = null, [
            'id' => 'otkcreate-cararticle',
            'class' => 'form-control',
            'placeholder' => 'Артикул автомобиля',
            'onchange' => '$.get("' . Url::to(['/ajax/car-name']) . '",{"article":$(this).val()},function(data){
                    $("#car-name").html("");
                    if (data && data.status == "success") {
                        $("#car-name").html("<div class=\'alert alert-success\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>" + data.message + "</div>"
                        );
                        $("#submitbtn").removeAttr("disabled");
                    }else if (data && data.status == "error"){
                        $("#car-name").html("<div class=\'alert alert-danger\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>" + data.message + "</div>"
                        );
                        $("#submitbtn").attr("disabled","true");
                    }else{
                        $("#car-name").html("<div class=\'alert alert-danger\'><button type=\'button\' class=\'close\' data-dismiss=\'alert\' aria-hidden=\'true\'>&times;</button><strong>Внимание! </strong>Нет ответа по данному артикулу.</div>"
                        );
                        $("#submitbtn").attr("disabled","true");
                    };
                });
            ']); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="input-group input-group-sm">
          <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
          <?= Html::dropDownList('OtkCreate[window]', 
            $selection = null,
            $items = ArrayHelper::merge([''=>''],MainProductInfo::getGoodsPrefixList()), 
            ['id' => 'otkcreate-window',
             'class' => 'form-control',
             'onchange' => '$.get("' . Url::to(['/ajax/product-types']) . '",{"type":$(this).val(),"cloth":$("#otkcreate-tkan").val()},function( data){
                $("#otkcreate-goodid").html("");
                if (Object.keys(data).length!=1)
                    $("#otkcreate-goodid").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#otkcreate-goodid").append($("<option></option>").attr("value",key).text(value));
                });
            });']); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="input-group input-group-sm">
          <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
           <?= Html::dropDownList('OtkCreate[tkan]', 
            $selection = null,
            $items = ArrayHelper::merge([''=>''],MainProductInfo::getGoodsClothList()), 
            ['id' => 'otkcreate-tkan',
             'class' => 'form-control',
             'onchange' => '$.get("' . Url::to(['/ajax/product-types']) . '",{"type":$(this).val(),"cloth":$("#otkcreate-tkan").val()},function(data){
                $("#otkcreate-goodid").html("");
                if (Object.keys(data).length!=1)
                    $("#otkcreate-goodid").html("<option></option>");
                $.each(data, function(key, value) {
                    $("#otkcreate-goodid").append($("<option></option>").attr("value",key).text(value));
                });
            });']); ?>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="input-group input-group-sm">
          <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
             <?= Html::dropDownList('OtkCreate[goodid]', 
                $selection = null,
                $items = ['' => ''], 
                ['id' => 'otkcreate-goodid',
                 'class' => 'form-control',
                ]); ?>
        </div>
    </div>

        <?= Html::a(
            $text='<span class="glyphicon glyphicon-plus"></span> Добавить', 
            $url = null, 
            [
                'id' => 'submitbtn',
                'class' => 'btn btn-warning btn-sm',
                'style' => 'margin-bottom:3px;',
                'onclick' => '
                    $.post( "' . Url::to(['/ajax/product-types']) . '", { OtkCreate: "John", OtkCreate: "2pm" })
                  .done(function( data ) {
                    alert( "Data Loaded: " + data );
                  });

                '
            ]); ?>
            <div class="clearfix"></div>   

    <p id="car-name"></p>

       <!-- Монитор контроля заявками на производство -->
       <div class="panel panel-primary">
          <div class="panel-heading text-center" style="font-size:1.4em">Монитор контроля заявок на производство</div>
          <div class="panel-body">
            <br>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $fromOrder,
                'layout' => '{items}',
                'show' => ['article','id'],
                'salt' => 'eqwe21',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'article',
                        'label' => ' Артикул',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => ' UPN',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => ' Остаток/Резерв',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => ' Текущий статус',
                        'encodeLabel' => false,
                        'filter' => true,
                        'value' => function ($data) {
                            return '';
                        }
                    ],
                ],
            ]); ?>
          </div>
        </div>
   </div>
   <div class="row">
       <!-- Монитор контроля распределнием с диспетчер-склад -->
       <div class="panel panel-primary">
          <div class="panel-heading text-center" style="font-size:1.4em">Монитор контроля распределения с диспетчер-склад</div>
          <div class="panel-body">
            <br>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-hover'],
                'dataProvider' => $fromDispatcher,
                'layout' => '{items}',
                'show' => ['article','id','source.title'],
                'salt' => 'rewrewrewr',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'article',
                        'label' => ' Артикул',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => ' UPN',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'source.title',
                        'label' => ' Источник',
                        'encodeLabel' => false,
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'plan',
                        'label' => ' Плановое распределение',
                        'encodeLabel' => false,
                        'filter' => true,
                        'value' => function ($data) {
                            return $data->planLiteral;
                        }
                    ],
                    [
                        'attribute' => 'fact',
                        'label' => ' Фактическое распределение',
                        'encodeLabel' => false,
                        'filter' => true,
                        'value' => function ($data) {
                            return $data->factLiteral;
                        }
                    ],
                ],
            ]); ?>
          </div>
        </div>
   </div>
   </div>
</div>
