<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\AutomaticOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Automatic Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="automatic-order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Automatic Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'team_id',
            'measuringInterval',
            'unitOfPeriod',
            'created_at',
            // 'author_id',
            // 'updated_at',
            // 'updater_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
