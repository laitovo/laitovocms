<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\AutomaticOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Определение списка для автопополнения';
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

?>
<?Pjax::begin(['enablePushState' => false]);?>

<div class="automatic-order-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?= Html::beginForm ( $action = '', $method = 'post', $options = ['data-pjax'=>1] );?>
    <p>
        <?= Html::submitButton ( $content = 'Применить изменения', 
        						 $options = ['class' => 'btn btn-success'] ) ?>
    </p>
    <hr>
    <div class="col-md-6">
    	<h3><span style="color:red">НЕ</span> пополняются автоматически</h3>
	    <?= GridView::widget([
	        'dataProvider' => $dataProviderNo,
    		'tableOptions' => ['class' => 'table table-hover'],
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],

	            'title',
                [
                    'attribute' => 'Check',
                    'label' => 'Добавить',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::tag('div',
                            Html::checkbox("idYes[]",
                                $checked = false, 
                                ['value' => $data->id])
                           .Html::tag('label',('')),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    	);
                    },
                ],
	            // 'author_id',
	            // 'updated_at',
	            // 'updater_id',

	            // ['class' => 'yii\grid\ActionColumn'],
	        ],
	    ]); ?>
	</div>
    <div class="col-md-6">
    	<h3>Пополняются автоматически</h3>
	    <?= GridView::widget([
	        'dataProvider' => $dataProviderYes,
			'tableOptions' => ['class' => 'table table-hover'],
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],

	            'title',
                [
                    'attribute' => 'Check',
                    'label' => 'Убрать',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::tag('div',
                            Html::checkbox("idNo[]",
                                $checked = false, 
                                ['value' => $data->id])
                           .Html::tag('label',('')),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                    	);
                    },
                ],
	            // 'author_id',
	            // 'updated_at',
	            // 'updater_id',

	            // ['class' => 'yii\grid\ActionColumn'],
	        ],
	    ]); ?>
    	<?= Html::endform();?>
	</div>

</div>
<?Pjax::end();?>
