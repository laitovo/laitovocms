<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use backend\modules\laitovo\models\ErpLocation;
use backend\modules\laitovo\models\ErpNaryad;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Терминал оприходывания нарядов');

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    $("#erp-terminal-form").on("ajaxComplete", function (event, messages) {
        document.location.reload();
    });

    setInterval(function(){ $(".reloadterminal").click();}, 60*1000);

    var barcode=$(\'#erpterminal-barcode\');

    function searchterminal(e)
    {
        $.ajaxSetup({timeout:10000})
        $.get("' . Url::to(['search']) . '",{
            barcode: barcode.val()
        },function(data){

            $(".reloadterminal").click();
            if (data.status=="success"){
                notie.alert(1,data.message,15);
                if (data.vidacha && data.vidacha.length &&  data.reestr && data.reestr.length){
                    printJS({printable:"'.Url::to(['/laitovo/erp-naryad/print-both']).'?id="+Object.values(data.vidacha).join(",")+"&id2="+Object.values(data.reestr).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }else if (data.reestr && data.reestr.length){
                    printJS({printable:"'.Url::to(['/laitovo/erp-naryad/print-reestr']).'?id="+Object.values(data.reestr).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }else if (data.vidacha && data.vidacha.length){
                    printJS({printable:"'.Url::to(['/laitovo/erp-naryad/print']).'?id="+Object.values(data.vidacha).join(","), type:"pdf", showModal:true, modalMessage: "Печать..."});
                }
                if (data.etiketka){
                    printJS("' . Url::to(['print-label']) . '?id="+data.etiketka.model+"&literal="+data.etiketka.literal);
                }
            } else {
                notie.alert(3,data.message,15);
            }

        },"json");

        e.preventDefault();
    }
    
    var keypres;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="erpterminal-barcode" && e.which == 13){
                searchterminal(e);
            } else if (e.target.localName=="body") {
                if (keypres==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchterminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypres=e.which;
        }
    });


    
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }

    ', \yii\web\View::POS_END);

?>
<div class="col-xs-4">
    <input type="text" id="erpterminal-barcode" placeholder="Поиск..." class="form-control">
</div>
<div class="clearfix"></div>

<?if (in_array(Yii::$app->user->getId(),[21,48])):?>
<hr>
<div class="col-md-12">
    <h6>Штрихкод дял оприходывания по количеству:</h6>
    <div><?= \core\logic\CheckStoragePosition::getQuantitativeBarcode();?></div>
</div>
<?endif;?>

<?php Pjax::begin(['timeout'=>5000]); ?>

<div class="clearfix"></div>


<?= Html::a("Обновить", ['index'], ['class' => 'hidden reloadterminal']) ?>

<?= GridView::widget([
    'tableOptions'=>['class'=>'table table-hover'],
    'rowOptions' => function ($data) use ($upns) {
        if(in_array($data->upn_id, $upns)) {
            return ['class' => 'success'];
        } else {
            return ['class' => ''];
        }
    },
    'dataProvider' => $dataProvider,
    'show'=>['literal','storage.title','upn_id','upn.article','created_at'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'literal',
        'storage.title',
        'upn_id',
        'upn.article',
        'created_at:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{print}',
            'buttonOptions' => ['class' => 'print'],
            'buttons'=>[
                'print' => function ($url, $model) {
                    return Html::button('<span class="glyphicon glyphicon-file"></span>',[
                        'title' => Yii::t('app', 'Распечатать этикетку с литерой'),
                        'onclick' => 'printUrl(\'' . Url::to(['/logistics/naryad/print-single-label']) . '?id=' . $model->upn_id . '&literal=' . $model->literal. '\');',
                        'class' => 'btn btn-info btn-sm',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'left',
                    ]);
                }
            ],
        ],
    ],

]); ?>

<?php Pjax::end(); ?>
