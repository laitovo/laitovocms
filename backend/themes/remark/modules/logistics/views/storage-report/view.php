<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\widgets\GridView;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$customScript = <<< SCRIPT
      $(document).on("click","[data-remote]",function(e) {
            console.log('some');
            e.preventDefault();
            $("#p0").append("<div id=material-div></div>");
            $("#modalView #material-div").load($(this).data('remote'));
        });
        $('#modalView').on('hidden.bs.modal', function(){
            $('#reload_submit_pjax').click();
            $("#p0 > div").remove();
            $("#p0 > div").remove();
        }); 
SCRIPT;
$this->registerJs($customScript, \yii\web\View::POS_READY);

$this->render('../menu');

$this->title = Yii::t('app',"Наличие на складе для {$carName}");

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Остатки на складе'), 'url' => ['/logistics/storage-report/index']];
$this->params['breadcrumbs'][] = $this->title;

Modal::begin(['id'=>'modalView', 'size'=>'modal-lg']);
Pjax::begin(['enablePushState' => FALSE]);
Pjax::end();
Modal::end();

?>
<div class="order-index">

    <!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $provider,
        'show' => [
            'article',
            'statistics',
            'title',
            'count'
        ],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'article',
                'label' => 'Артикул',
            ],
            [
                'attribute' => 'title',
                'label' => 'Наименование',
                'value' => function($data) {
                    return \backend\helpers\ArticleHelper::getArticleForProduct($data['article']);
                }
            ],
            [
                'attribute' => 'statistics',
                'label' => 'Статистика(Последний месяц)',
            ],
            [
                'attribute' => 'count',
                'label' => 'Общее количество',
            ],
            ['class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'view'=>function($url, $data){
                        return Html::a('UPNs' ,
                            '#modalView', [
                                'title' => 'Список UPNов с литерами',
                                'class' => 'btn btn-xs btn-primary',
                                'style' => 'text-decoration:none',
                                'data-toggle'=>'modal',
                                'data-backdrop'=>false,
                                'data-remote'=>Url::to(['article','article'=>$data['article']])
                            ]);
                    },
                ],
                'template'=>'{view}'
            ],
        ],
    ]); ?>
</div>
