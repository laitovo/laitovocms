<?php

use yii\helpers\Url;
//use backend\widgets\GridView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->render('../menu');

$this->title = Yii::t('app',"Наличие на складе артикула: {$article}");

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Остатки на складе'), 'url' => ['/logistics/storage-report/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="order-index">

    <!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $provider,
//        'show' => [
//            'article',
//            'statistics',
//            'title',
//            'count'
//        ],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'upn.article',
                'label' => 'Артикул',
            ],
            [
                'attribute' => 'upn_id',
                'label' => 'UPN',
            ],
            [
                'attribute' => 'literal',
                'label' => 'Литера',
            ],
            [
                'attribute' => 'type',
                'label' => 'Происхождение',
                'format' => 'raw',
                'value' => function($data){
                    if ($data->upn && $data->upn->erpNaryad && ($myOrder = $data->upn->erpNaryad->order_id)) {
                        return '<a href="'.Url::to(['/laitovo/erp-order/view', 'id' => $myOrder]).'" target="_blank"><span style="color:green">Под заказ №' . $myOrder .'</span></a> <br>' . '<span style="color:black">'.(Yii::$app->formatter->asDatetime($data->upn->created_at)).'</span>';
                    }elseif($data->upn && $data->upn->erpNaryad && $data->upn->responsible_person == 'терминал выдачи') {
                        return '<span style="color:green">Создан при проверке на ОТК</span> <br>' .  '<span style="color:black">'.(Yii::$app->formatter->asDatetime($data->upn->created_at)).'</span>';
                    }elseif($data->upn && $data->upn->erpNaryad) {
                        return '<span style="color:green">Пополнение склада</span> <br>' .  '<span style="color:black">'.(Yii::$app->formatter->asDatetime($data->upn->created_at)).'</span>';
                    }else{
                        return '<span style="color:green">Перенос остатков или возврат на СКЛАДЕ</span> <br>' .  '<span style="color:black">'.(Yii::$app->formatter->asDatetime($data->upn->created_at)).'</span>';
                    }
                },
            ],
        ],
    ]); ?>
</div>
