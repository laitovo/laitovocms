<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->render('../menu');

$this->title = Yii::t('app','Остатки на складе - по машинам');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логистика'), 'url' => ['/logistics/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Остатки на складе'), 'url' => ['/logistics/storage-report/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $provider,
        'show' => [
//            'carId',
            'carArticle',
            'carName',
            'count',
            'FW',
            'FV',
            'FD',
            'RD',
            'RV',
            'BW'],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'carArticle',
                'label' => 'Артикул авто',
            ],
            [
                'attribute' => 'carName',
                'label' => 'Наименование авто',
            ],
            [
                'attribute' => 'count',
                'label' => 'Общее количество',
            ],
            [
                'attribute' => 'FW',
                'label' => 'FW',
            ],
            [
                'attribute' => 'FV',
                'label' => 'FV',
            ],
            [
                'attribute' => 'FD',
                'label' => 'FD',
            ],
            [
                'attribute' => 'RD',
                'label' => 'RD',
            ],
            [
                'attribute' => 'RV',
                'label' => 'RV',
            ],
            [
                'attribute' => 'BW',
                'label' => 'BW',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons'=>[
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',  Url::toRoute(['/logistics/storage-report/view', 'id' => $model['carId']]),[
                            'title' => Yii::t('app', 'Просмотреть остатки по данному автомобилю'),
                            'class' => 'btn btn-primary btn-xs',
                            'target' => '_blank',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'left',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
