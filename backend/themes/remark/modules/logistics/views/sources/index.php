<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\SourcesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->render('../menu');

$this->title = Yii::t('app','Источники нарядов');
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="logistics-sources-index">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary pull-left','title' => Yii::t('app','Добавить источник')]) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        'show' => ['title','type','created_at','author.name'],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'team_id',
            'title',
            [
                'attribute' => 'type',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->type ? $data->types[$data->type] : '';
                },
            ],
            'key',
            'created_at:datetime',
            'author.name',
            // 'updated_at',
            // 'updater_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttonOptions' => ['data-pjax' => '1'],
            ],
        ],
    ]); ?>
</div>
<?Pjax::end();?>

