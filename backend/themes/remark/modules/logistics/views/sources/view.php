<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\Sources */

$this->render('../menu');

$this->title = Yii::t('app','Источники нарядов');
$viewtitle = $model->title;
?>
<?Pjax::begin(['enablePushState' => true]);?>

<div class="logistics-sources-view">

    <h4><?= $viewtitle ?></h4>
    <hr>

    <p>
        <?= Html::a('<i class="icon wb-edit"></i> ' . Yii::t('app','Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="icon wb-trash"></i> ' . Yii::t('app','Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data-pjax' => '1',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'team.name',
            'title',
            [
                'attribute' => 'type',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->type ? $data->types[$data->type] : '';
                },
            ],
            'key',
            'created_at:datetime',
            'author.name',
            'updated_at:datetime',
            'updater.name',
        ],
    ]) ?>

</div>
<?Pjax::end();?>
