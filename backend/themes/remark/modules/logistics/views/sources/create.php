<?php

use yii\helpers\Html;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\Sources */

$this->render('../menu');

$this->title = Yii::t('app','Источники нарядов');
$viewtitle = Yii::t('app','Добавление источника нарядов')
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="logistics-sources-create">

    <h4><i><?= $viewtitle ?></i></h4>
    <hr>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?Pjax::end();?>
