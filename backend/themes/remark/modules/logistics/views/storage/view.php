<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\Storage */

$this->render('../menu');

$this->title = Yii::t('app','Места хранения');
$viewtitle = $model->title;


?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="storage-view">

    <h4><?= $viewtitle ?></h4>
    <hr>

    <p>
        <?= Html::a('<i class="icon wb-edit"></i> ' . Yii::t('app','Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="icon wb-trash"></i> ' . Yii::t('app','Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data-pjax' => '1',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'team_id',
            'title',
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return @$model->types[$model->type];
                }
            ],
            'literal',
            [
                'attribute' => 'logic',
                'value' => function($model) {
                    return @$model->logicList[$model->logic];
                }
            ],
            // 'created_at:datetime',
            // [
            //     'attribute' => 'author.name',
            //     'label' => 'Создал запись',
            // ],
            'updated_at:datetime',
            [
                'attribute' => 'updater.name',
                'label' => 'Последний редактор',
            ],
        ],
    ]) ?>


    <div class="col-md-12">
        <h3><span style="color:red">Литеры</span> склада</h3>
        <p>
            <?= Html::a('<i class="icon wb-plus"></i>', ['storage-literal/create','storage_id' => $model->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary','title' => Yii::t('app','Добавить место хранения')]) ?>

         </p>

        <?= GridView::widget([
            'dataProvider' => $dataProviderLiterals,
            'tableOptions' => ['class' => 'table table-hover'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                'capacity',
                'default_quantity',
                // [
                //     'attribute' => 'Check',
                //     'label' => 'Добавить',
                //     'format' => 'raw',
                //     'value' => function ($data) {
                //         return Html::tag('div',
                //             Html::checkbox("idYes[]",
                //                 $checked = false, 
                //                 ['value' => $data->id])
                //            .Html::tag('label',('')),
                //             ['class' => 'checkbox-custom checkbox-primary text-left']
                //         );
                //     },
                // ],
                // 'author_id',
                // 'updated_at',
                // 'updater_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'storage-literal',
                'buttonOptions' => ['data-pjax'=>1],
            ],
            ],
        ]); ?>
    </div>
    <hr>
    <?= Html::beginForm ( $action = '', $method = 'post', $options = ['data-pjax'=>1] );?>
    <p>
        <?= Html::submitButton ( $content = 'Применить изменения', 
                                 $options = ['class' => 'btn btn-success'] ) ?>
    </p>
    <hr>
    <div class="col-md-6">
        <h3><span style="color:red">НЕ</span> приходуются на склад</h3>
        <?= GridView::widget([
            'dataProvider' => $dataProviderNo,
            'tableOptions' => ['class' => 'table table-hover'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                [
                    'attribute' => 'Literal',
                    'label' => 'Литера',
                    'format' => 'raw',
                    'value' => function ($data) use ($literals) {
                        return Html::dropDownList('idYesLiteral['.$data->id.']', $selection = null, $items = $literals, ['class' => 'form-control']);
                    },
                ],
                [
                    'attribute' => 'Check',
                    'label' => 'Переместить ',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::tag('div',
                            Html::checkbox("idYes[]",
                                $checked = false, 
                                ['value' => $data->id])
                           .Html::tag('label',('<span class="glyphicon glyphicon-forward" aria-hidden="true"></span>')),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        );
                    },
                ],
                // 'author_id',
                // 'updated_at',
                // 'updater_id',

                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <div class="col-md-6">
        <h3>Приходуются на склад</h3>
        <?= GridView::widget([
            'dataProvider' => $dataProviderYes,
            'tableOptions' => ['class' => 'table table-hover'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                [
                    'attribute' => 'Literal',
                    'label' => 'Литера',
                    'format' => 'raw',
                    'value' => function ($data) use ($model) {
                        return $data->getLiteralByStorage($model->id);
                    },
                ],
                [
                    'attribute' => 'Check',
                    'label' => 'Переместить',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::tag('div',
                            Html::checkbox("idNo[]",
                                $checked = false, 
                                ['value' => $data->id])
                           .Html::tag('label',('<span class="glyphicon glyphicon-backward" aria-hidden="true"></span>')),
                            ['class' => 'checkbox-custom checkbox-primary text-left']
                        );
                    },
                ],
                // 'author_id',
                // 'updated_at',
                // 'updater_id',

                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?= Html::endform();?>
    </div>

</div>
<?Pjax::end();?>
