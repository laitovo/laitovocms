<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\Storage */

$this->render('../menu');

$this->title = Yii::t('app','Места хранения');
$viewtitle = Yii::t('app','Изменение места хранения');

?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="storage-update">

    <h4><i><?= $viewtitle ?></i></h4>
    <hr>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?Pjax::end();?>
