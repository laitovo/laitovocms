<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\StorageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->render('../menu');

$this->title = Yii::t('app','Места хранения');
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="storage-index">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary pull-left','title' => Yii::t('app','Добавить место хранения')]) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        'show' => ['title','type'],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            // 'team_id',
            'title',
            [
                'attribute' => 'type',
                'value' => function($data) {
                    return @$data->types[$data->type];
                }
            ],
            'literal',
            [
                'attribute' => 'logic',
                'value' => function($data) {
                    return @$data->logicList[$data->logic];
                }
            ],
            'created_at:datetime',
            // 'author_id',
            // 'updated_at',
            // 'updater_id',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttonOptions' => ['data-pjax' => '1'],
            ],
        ],
    ]); ?>
</div>
<?Pjax::end();?>
