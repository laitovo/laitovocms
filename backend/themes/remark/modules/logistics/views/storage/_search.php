<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\StorageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="storage-search">

    <?php $form = ActiveForm::begin([
        'id' => 'my-search-form',
        'action' => ['index'],
        'method' => 'post',
        'options' => ['data-pjax' => '1'],
    ]); ?>

    <?php // echo  $form->field($model, 'id') ?>

    <?php // echo  $form->field($model, 'team_id') ?>

    <?php  echo  $form->field($model, 'title') ?>

    <?php // echo  $form->field($model, 'type') ?>

    <?php // echo  $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'author_id') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updater_id') ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="icon wb-search"></i> ' . Yii::t('app','Искать'), ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('<i class="icon wb-refresh"></i> ' . Yii::t('app','Очистить'), [
            'class' => 'btn btn-default',
            'onclick' => 'console.log($(\'form\'));console.log($(\'[id ^= "storagesearch"]\').val(\'\'));$(\'#my-search-form\').submit();'
            ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
