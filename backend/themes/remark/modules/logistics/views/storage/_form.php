<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\Storage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="storage-form">
    
    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'options' => ['data-pjax' => ''],
    ]); ?>

    <?php // echo $form->field($model, 'team_id')->textInput() ?>

    <?php  echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList($model->types); ?>

    <?= $form->field($model, 'logic')->dropDownList($model->logicList); ?>

    <?php  echo $form->field($model, 'literal')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'created_at')->textInput() ?>

    <?php // echo $form->field($model, 'author_id')->textInput() ?>

    <?php // echo $form->field($model, 'updated_at')->textInput() ?>

    <?php // echo $form->field($model, 'updater_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="icon wb-plus"></i> ' . Yii::t('app','Добавить') : '<i class="icon wb-check"></i> ' . Yii::t('app','Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','data-pjax' => '1']) ?>
        <?php if ($model->isNewRecord) :?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
        <?php else: ?>
        <?= Html::a('<i class="icon wb-close"></i> ' . Yii::t('app','Отмена'), ['view','id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?php endif;?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
