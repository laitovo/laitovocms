<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\OrderEntry */

$this->render('../menu');

$this->title = Yii::t('app','Список заявок');
$viewtitle = $model->id;
?>
<div class="order-entry-view">

    <h4><?= $viewtitle ?></h4>
    <hr>

    <p>
        <?= Html::a('<i class="icon wb-edit"></i> ' . Yii::t('app','Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="icon wb-trash"></i> ' . Yii::t('app','Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data-pjax' => '1',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Вернуться к списку'), ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('<i class="icon wb-order"></i> ' . Yii::t('app','Редактировать содержимое'), ['order-entry/index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'title',
            'article',
            'quantity',
            'comment:ntext',
            'created_at',
            'author_id',
            'updated_at',
            'updater_id',
        ],
    ]) ?>

</div>
<?Pjax::end();?>
