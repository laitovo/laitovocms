<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\OrderEntrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->render('../menu');

$this->title = Yii::t('app','Список заявок');
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="order-entry-index">
    <?= Html::a('<i class="icon wb-close"></i> ' . Yii::t('app','Вернуться к заявке'), ['order/view','id' => $order->id], ['class' => 'btn btn-warning']) ?>
    <hr>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="icon wb-plus"></i>', ['create','id' => $order->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary pull-left','title' => Yii::t('app','Добавить заявку')]) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'order_id',
            'title',
            'article',
            'quantity',
            // 'comment:ntext',
            // 'created_at',
            // 'author_id',
            // 'updated_at',
            // 'updater_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttonOptions' => ['data-pjax' => '1'],
            ],
        ],
    ]); ?>
</div>
<?Pjax::end();?>
