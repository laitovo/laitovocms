<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\StorageLiteral */

$this->title = 'Места хранения';
$this->params['breadcrumbs'][] = ['label' => 'Storage Literals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="storage-literal-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?Pjax::end();?>
