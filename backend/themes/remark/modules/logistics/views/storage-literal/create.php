<?php

use yii\helpers\Html;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model backend\modules\logistics\models\StorageLiteral */

$this->title = 'Места хранения';
$this->params['breadcrumbs'][] = ['label' => 'Storage Literals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?Pjax::begin(['enablePushState' => true]);?>
<div class="storage-literal-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?Pjax::end();?>
