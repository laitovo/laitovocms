<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\StorageLiteralSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Storage Literals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-literal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Storage Literal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'storage_id',
            'capacity',
            'created_at',
            // 'author_id',
            // 'updated_at',
            // 'updater_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
