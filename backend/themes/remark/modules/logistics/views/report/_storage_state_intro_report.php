<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 04.01.18
 * Time: 10:07
 */

use backend\helpers\ArticleHelper;
use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\report\StorageStateReport */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по заполенным литерам на складе (Детально)';
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>
<div class="storage-literal-index">

    <?= Html::a('Верунтся к списку',['storage-state-report'],['class'=> 'btn btn-info'])?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class'=>'table table-hover'],
        'show' => ['literal','upn_id'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'storage.title',
            'literal',
            'upn_id',
            [
                'attribute' => 'upnArticle',
                'label' => 'Артикул',
                'value' => 'upn.article'
            ],
            [
                'attribute' => 'upnTitle',
                'label' => 'Наименование',
                'value' => function($data) {
                    return ArticleHelper::getInvoiceRusTitle($data->upn->article);
                }
            ],
            [
                'attribute' => 'upnReserved',
                'label' => 'Заразервировано',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::tag('i', '', ['class' => $data->upn->reserved_id ? 'wb-check text-success' : 'wb-close text-danger']);
                }
            ],
//            'created_at',
            // 'author_id',
            // 'updated_at',
            // 'updater_id',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{print}',
                'buttonOptions' => ['class' => 'print'],
                'buttons'=>[
                    'print' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-file"></span>',[
                            'title' => Yii::t('app', 'Распечатать этикетку с литерой'),
                            'onclick' => 'printUrl("' . Url::to(['/logistics/admission-terminal/print-label', 'id' => $model->upn_id, 'literal' => $model->literal]) . '");',
                            'class' => 'btn btn-info btn-sm',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'left',
                        ]);
                    }
                ],
            ],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php Yii::$app->view->registerJs('
    //Функция принимает url, получает по нему контент и выводит на печать
    function printUrl(url) {
        if (!$("#print-content").length) {
            $("body").append(\'<div id="print-content" class="hide"></div>\');
        }
        var html = \'<iframe id="iframe-print-content" src="\' + url + \'" style="display:none;"></iframe>\';
        $("#print-content").html(html);
        var iframe = document.getElementById("iframe-print-content");
        iframe.focus();
        iframe.contentWindow.print();
    }
', \yii\web\View::POS_END);
?>