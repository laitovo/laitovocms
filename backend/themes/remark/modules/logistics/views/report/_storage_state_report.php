<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 04.01.18
 * Time: 10:07
 */

use yii\helpers\Html;
use yii\helpers\Url;
use backend\widgets\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\logistics\models\report\StorageStateReport */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по заполенным литерам на складе';
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu')
?>
<div class="storage-literal-index">


    <div class="storage-literal-search">

        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'id' => 'storage-literal-search-form'
        ]); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($searchModel, 'literal') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($searchModel, 'upn_id')->label("UPN (вводить полностью)") ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            <?= Html::button('Сбросить', ['class' => 'btn btn-default','onclick' => '$(\'#storagestatereport-literal\').val(\'\');$(\'#storagestatereport-upn_id\').val(\'\');$(\'#storage-literal-search-form\').submit();']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'rowOptions' => function($model) {
            if ($model->literalObj && $model->count > $model->literalObj->capacity) { return ['class'=>'danger'];}
        },
        'tableOptions' => ['class'=>'table table-hover'],
        'show' => ['storage.title','literal','count'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'title',
            'storage.title',
            'literal',
            [
                    'attribute' => 'count',
                    'label' => 'Количество на литере',
            ],
//            'upn_id',
//            'capacity',
//            'created_at',
            // 'author_id',
            // 'updated_at',
            // 'updater_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{intro}', //{time} {update}
                'buttons' => [
                    'intro' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            Url::to(['storage-state-intro-report', 'literal' => $model->literal ]),
                            [
                                'title' => Yii::t('app', 'Изменить расписание рабочего места'),
                            ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>