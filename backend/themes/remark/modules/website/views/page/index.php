<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Страницы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->page->website->name, 'url' => ['default/view', 'id' => $model->page->website->id]];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu', ['website' => $model->page->website]);

?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create', 'id' => $model->page->website->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        // 'title',
        // 'description',
        // 'keywords',
        // 'metatag:ntext',
        // 'image',
        // 'anons:ntext',
        // 'content:ntext',
        // 'sort',
        // 'status',
        // 'created_at',
        // 'updated_at',
        // 'author_id',
        // 'updater_id',
        // 'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
