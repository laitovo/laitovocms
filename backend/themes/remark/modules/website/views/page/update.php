<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\website\Page */

$this->title = Yii::t('app', 'Редактировать страницу: ') . ' ' . $model->page->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->page->website->name, 'url' => ['default/view', 'id' => $model->page->website->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Страницы'), 'url' => ['index', 'id' => $model->page->website->id]];
$this->params['breadcrumbs'][] = $model->page->name;
$this->render('../menu', ['website' => $model->page->website]);
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
