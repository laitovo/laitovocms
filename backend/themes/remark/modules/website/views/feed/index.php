<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ленты');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->feed->website->name, 'url' => ['default/view', 'id' => $model->feed->website->id]];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu', ['website' => $model->feed->website]);
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create', 'id' => $model->feed->website->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        // 'title',
        // 'description',
        // 'keywords',
        // 'metatag:ntext',
        // 'image',
        // 'anons:ntext',
        // 'content:ntext',
        // 'sort',
        // 'status',
        // 'created_at',
        // 'updated_at',
        // 'author_id',
        // 'updater_id',
        // 'count_on_page',
        // 'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
