<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\website\Feed */

$this->title = Yii::t('app', 'Добавить ленту');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->feed->website->name, 'url' => ['default/view', 'id' => $model->feed->website->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ленты'), 'url' => ['index', 'id' => $model->feed->website->id]];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu', ['website' => $model->feed->website]);
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
