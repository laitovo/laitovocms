<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\website\Feed */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<div class="form-group">
    <?= Html::submitButton($model->feed->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->feed->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= $model->feed->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->feed->id], [
        'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
