<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\website\Website */
/* @var $form yii\widgets\ActiveForm */

Yii::$app->view->registerJs('

    function parseitemsjson () {
        var lang =[];
        $(\'.langitems .item\').each(function( index, value ) {
            itemjson=$(this).find("input").serializeArray();
            var obj ={};
            $.each(itemjson, function( index1, value1 ) {
                obj[value1.name]=value1.value;
            });
            lang.push(obj);
        });
        $(\'#websiteform-langs\').val(JSON.stringify(lang));
    }

    var edititem = null;

    $("body").on("click",".newlang",function(e){
        window.edititem = null;
        $(".langmodalform input").val("");
    });

    $("body").on("click",".editlang",function(e){
        window.edititem = $(this).parent();
        $(".langmodalform input").val("");
        $(".langmodalform .lang-name").val($(this).parent().find("input[name=\'name\']").val());
        $(".langmodalform .lang-lang").val($(this).parent().find("input[name=\'lang\']").val());
        $(".langmodalform").modal("show") 
        e.preventDefault();
    });

    $("body").on("submit",".savelang",function(e){
        $(".langitems .item.additem").removeClass("additem");
        if (window.edititem){
            $(".langitems .newitem").clone().insertAfter(window.edititem).removeClass("hidden newitem").addClass("item additem");
            window.edititem.remove();
        } else {
            $(".langitems .newitem").clone().appendTo(".langitems").removeClass("hidden newitem").addClass("item additem");
        }
        $(".langitems .item.additem input[name=\'lang\']").val($(".langmodalform .lang-lang").val());
        $(".langitems .item.additem input[name=\'name\']").val($(".langmodalform .lang-name").val());
        $(".langitems .item.additem input[name=\'name\']").prev().html("<span class=\'flag-icon flag-icon-"+$(".langmodalform .lang-lang").val()+"\'></span> "+$(".langmodalform .lang-name").val());

        parseitemsjson();
        $(".langmodalform").modal("hide") 
        e.preventDefault();
    });

    $("body").on("click",".langitems .deletethisitem",function(e){
        var item = $(this).parent();
        notie.confirm("' . Yii::t('app', 'Удалить язык?') . '", "' . Yii::t('yii', 'Yes') . '", "' . Yii::t('yii', 'No') . '", function() {
            item.remove();
            parseitemsjson();
        });
        e.preventDefault();
    });
    ', \yii\web\View::POS_END);

?>

<?php $form = ActiveForm::begin(['id' => 'website-form']); ?>

<?= $form->field($model, 'subdomain')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'langs')->hiddenInput() ?>

<?php ActiveForm::end(); ?>

<div class="langitems">
    <div class="newitem hidden">
        <a href="#" class="editlang"></a>
        <input type="hidden" name="name">
        <input type="hidden" name="lang">
        <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
    </div>
    <? foreach (Json::decode($model->langs ? $model->langs : '{}') as $lang): ?>
        <div class="item">
            <a href="#" class="editlang"><span
                        class="flag-icon flag-icon-<?= Html::encode($lang['lang']) ?>"></span> <?= Html::encode($lang['name']) ?>
            </a>
            <input type="hidden" name="name" value="<?= Html::encode($lang['name']); ?>">
            <input type="hidden" name="lang" value="<?= $lang['lang'] ?>">
            <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
        </div>
    <? endforeach ?>
</div>
<p></p>
<?php Modal::begin([
    // 'clientOptions'=>['show'=>$invite->hasErrors() ? true : false],
    'options' => ['class' => 'langmodalform fade modal'],
    'header' => Html::tag('h4', Yii::t('app', 'Язык'), ['class' => 'modal-title']),
    'toggleButton' => ['class' => 'newlang btn btn-icon btn-round btn-default btn-outline', 'label' => '<i class="icon wb-plus"></i>'],
]); ?>
<form class="savelang">

    <div class="form-group">
        <label class="control-label" for="lang-name"><?= Yii::t('app', 'Язык') ?></label>
        <input type="text" required="required" value="" class="required form-control lang-name" name="lang-name">
    </div>

    <div class="form-group">
        <label class="control-label" for="lang-lang"><?= Yii::t('app', 'Обозначение') ?></label>
        <input type="text" required="required" value="" class="required form-control lang-lang" name="lang-lang">
    </div>

    <?= Html::submitButton(Yii::t('app', 'Готово'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>

</form>

<?php Modal::end(); ?>
<p>&nbsp;</p>


<div class="form-group">
    <?= Html::submitButton($model->website->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['onclick' => '$("#website-form").submit();', 'class' => $model->website->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= $model->website->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->website->id], [
        'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</div>

