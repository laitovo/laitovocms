<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\website\Website */

Yii::$app->layout = '1columns';
$this->title = Yii::t('app', 'Добавить интернет-магазин');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
