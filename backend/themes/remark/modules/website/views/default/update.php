<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\website\Website */

$this->title = Yii::t('app', 'Редактировать интернет-магазин: ') . ' ' . $model->website->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->website->name, 'url' => ['view', 'id' => $model->website->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактировать');
$this->render('../menu', ['website' => $model->website]);
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
