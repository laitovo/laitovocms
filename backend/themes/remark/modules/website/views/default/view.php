<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\website\Website */

$this->title = $model->website->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu', ['website' => $model->website]);
?>

<?= DetailView::widget([
    'model' => $model->website,
    'attributes' => [
        'id',
        'subdomain',
        'name',
        'title',
        'description',
        'keywords',
        'metatag:ntext',
        'created_at',
        'updated_at',
        'expires_at',
        'author_id',
        'updater_id',
        'status',
        'template',
        'logo',
        'css:ntext',
        'js:ntext',
        'html:ntext',
        'robots:ntext',
        'json:ntext',
        'team_id',
    ],
]) ?>
