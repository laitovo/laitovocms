<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

Yii::$app->layout = '1columns';
$this->title = Yii::t('app', 'Интернет-магазины');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['subdomain', 'name'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'subdomain',
        'name',
        // 'title',
        // 'description',
        // 'keywords',
        // 'metatag:ntext',
        // 'created_at:datetime',
        // 'updated_at:datetime',
        // 'expires_at:datetime',
        // 'author_id',
        // 'updater_id',
        // [
        //     'attribute'=>'status',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return Html::tag('i','',['class'=>$data->status ? 'wb-check text-success' : 'wb-close text-danger']);
        //     },
        // ],
        // 'template',
        // 'logo',
        // 'css:ntext',
        // 'js:ntext',
        // 'html:ntext',
        // 'robots:ntext',
        // 'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
