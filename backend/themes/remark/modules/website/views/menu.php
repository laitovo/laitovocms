<?php

/* @var $this yii\web\View */

$this->params['menuItems'] = [
    [
        'items' => [
            ['label' => Yii::t('app', 'Страницы'), 'url' => ['page/index', 'id' => $website->id]],
            ['label' => Yii::t('app', 'Новости'), 'url' => ['feed/index', 'id' => $website->id]],
            ['label' => Yii::t('app', 'Товары'), 'url' => ['product/index', 'id' => $website->id]],
            ['label' => Yii::t('app', 'Заказы'), 'url' => ['order/index', 'id' => $website->id]],
            ['label' => Yii::t('app', 'Настройки'), 'url' => ['default/update', 'id' => $website->id]],
        ],
    ],
];