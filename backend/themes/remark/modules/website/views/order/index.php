<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Заказы');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->order->website->name, 'url' => ['default/view', 'id' => $model->order->website->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu', ['website' => $model->order->website]);
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create', 'id' => $model->order->website->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['number', 'created_at', 'sum'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'number',
            'format' => 'html',
            'value' => function ($data) {
                return $data->number ? Html::a(Yii::t('app', 'Заказ №{number}', ['number' => $data->number]), ['update', 'id' => $data->id]) : null;
            },
        ],
        'created_at:datetime:' . Yii::t('app', 'Дата'),
        [
            'attribute' => 'user_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->user ? Html::encode($data->user->name) : null;
            },
        ],
        'quantity:decimal:' . Yii::t('app', 'Кол-во'),
        'sum:currency:' . Yii::t('app', 'Сумма'),
        'status',
        'condition',

        // 'updated_at:datetime',
        // [
        //     'attribute'=>'author_id',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return $data->author ? Html::encode($data->author->name) : null;
        //     },
        // ],
        // [
        //     'attribute'=>'updater_id',
        //     'format'=>'html',
        //     'value'=>function ($data) {
        //         return $data->updater ? Html::encode($data->updater->name) : null;
        //     },
        // ],
        // 'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>
