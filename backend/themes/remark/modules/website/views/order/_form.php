<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use common\assets\twitter_cldr\TwitterCldrAsset;
use backend\themes\remark\assets\FormAsset;

/* @var $this yii\web\View */
/* @var $model backend\modules\order\models\OrderForm */
/* @var $form yii\widgets\ActiveForm */

$items = Json::decode($model->items);

TwitterCldrAsset::register($this);
FormAsset::register($this);

Yii::$app->view->registerJs('
    var fmt = new TwitterCldr.CurrencyFormatter();
    $(document).ready(function() {
        $(\'.editable-inline\').editable({
            mode: "inline",
            inputclass: "form-control",
            emptytext:"Не задано",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
            }
        });
        $(\'.editable-popup\').editable({
            mode: "popup",
            emptytext:"Не задано",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
            }
        });

        $(\'#tbody-items .name-editable\').editable({
            emptytext:"Укажите название товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .price-editable\').editable({
            emptytext:"Укажите цену",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .quantity-editable\').editable({
            emptytext:"Укажите кол-во",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
    });

    function parseitemsjson () {
        var items =[];
        var totalcount =0;
        var totalsum =0;
        $(\'#tbody-items .items-order\').each(function( index, value ) {
            $(this).find(\'.itemjson\').prop("disabled", false);
            itemjson=$(this).find(\'.itemjson\').serializeArray();
            $(this).find(\'.itemjson\').prop("disabled", true);
            var obj ={};
            $.each(itemjson, function( index1, value1 ) {
                obj[value1.name]=value1.value;
            });
            sum=parseFloat(parseFloat(obj.price)*parseFloat(obj.quantity));
            totalsum=parseFloat(totalsum+sum);
            totalcount=parseFloat(totalcount+parseFloat(obj.quantity));
            items.push(obj);
            $(this).find(\'.item-sum\').text(fmt.format(sum, {currency: "' . Yii::$app->formatter->currencyCode . '"}));
        });
        $(\'#orderform-items\').val(JSON.stringify(items));
        $(\'#orderform-totalcount\').text(totalcount);
        $(\'#orderform-totalsum\').text(fmt.format(totalsum, {currency: "' . Yii::$app->formatter->currencyCode . '"}));
    }
    $("body").on("click","#tbody-items .items-order .delete-orderitem",function(e){
        $(this).parent().parent().remove();
        parseitemsjson();
        e.preventDefault();
    });
    $("body").on("click","#add-orderitem",function(e){
        $(".add-item-tr").clone().appendTo("#tbody-items");

        $(\'#tbody-items .add-item-tr .name-editable\').editable({
            emptytext:"Укажите название товара",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .price-editable\').editable({
            emptytext:"Укажите цену",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });
        $(\'#tbody-items .add-item-tr .quantity-editable\').editable({
            emptytext:"Укажите кол-во",
            showbuttons: false,
            onblur: "submit",
            success: function(response, newValue) {
                $(this).next().next().val(newValue);
                parseitemsjson();
            }
        });

        $("#tbody-items .add-item-tr").removeClass("hidden add-item-tr");
        parseitemsjson();
        e.preventDefault();
    });
    ', \yii\web\View::POS_END);

?>

<?php $form = ActiveForm::begin(); ?>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Товар</th>
            <th>Цена</th>
            <th>Кол-во</th>
            <th>Сумма</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="tbody-items">
        <? if ($items) foreach ($items as $key => $row):
            $product_id = isset($row['product_id']) ? $row['product_id'] : null;
            $price = isset($row['price']) ? $row['price'] : null;
            $quantity = isset($row['quantity']) ? $row['quantity'] : null;
            $name = isset($row['name']) ? $row['name'] : '';
            $total = isset($total) ? $quantity + $total : $quantity;
            $sum = isset($sum) ? $quantity * $price + $sum : $quantity * $price;
            ?>
            <tr class="items-order">
                <td>
                    <?= $product_id ? Html::a('<i class="wb wb-eye"></i>', ['product/update', 'id' => $product_id]) : '' ?>
                    <a href="#" class="name-editable" data-type="text"><?= $name ?></a>
                    <input class="itemjson" type="hidden" disabled="disabled" name="name" value='<?= $name ?>'>
                    <input class="itemjson" type="hidden" disabled="disabled" name="product_id"
                           value='<?= $product_id ?>'>
                </td>
                <td>
                    <a href="#" class="price-editable" data-type="text"><?= $price ?></a>
                    <input class="itemjson" type="hidden" disabled="disabled" name="price" value='<?= $price ?>'>
                </td>
                <td>
                    <a href="#" class="quantity-editable" data-type="text"><?= $quantity ?></a>
                    <input class="itemjson" type="hidden" disabled="disabled" name="quantity" value='<?= $quantity ?>'>
                </td>
                <td class="item-sum"><?= Yii::$app->formatter->asCurrency($quantity * $price) ?></td>
                <td>
                    <a href="#" class="text-danger delete-orderitem"><i class="wb wb-close"></i></a>
                    <input class="itemjson" type="hidden" disabled="disabled" name="id"
                           value='<?= isset($row['id']) ? $row['id'] : null ?>'>
                    <input class="itemjson" type="hidden" disabled="disabled" name="retail"
                           value='<?= isset($row['retail']) ? $row['retail'] : null ?>'>
                    <input class="itemjson" type="hidden" disabled="disabled" name="properties"
                           value='<?= isset($row['properties']) ? $row['properties'] : '[]' ?>'>
                </td>
            </tr>
        <? endforeach ?>
        </tbody>
        <tr class="items-order hidden add-item-tr">
            <td>
                <a href="#" class="name-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" disabled="disabled" name="name" value=''>
                <input class="itemjson" type="hidden" disabled="disabled" name="product_id" value='<?= null ?>'>
            </td>
            <td>
                <a href="#" class="price-editable" data-type="text"></a>
                <input class="itemjson" type="hidden" disabled="disabled" name="price" value='0'>
            </td>
            <td>
                <a href="#" class="quantity-editable" data-type="text">1</a>
                <input class="itemjson" type="hidden" disabled="disabled" name="quantity" value='1'>
            </td>
            <td class="item-sum"><?= Yii::$app->formatter->asCurrency(0) ?></td>
            <td>
                <a href="#" class="text-danger delete-orderitem"><i class="wb wb-close"></i></a>
                <input class="itemjson" type="hidden" disabled="disabled" name="id" value='<?= null ?>'>
                <input class="itemjson" type="hidden" disabled="disabled" name="retail" value='<?= null ?>'>
                <input class="itemjson" type="hidden" disabled="disabled" name="properties" value='[]'>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="text-right lead"><?= Yii::t('app', 'Итого:') ?></td>
            <td class="lead" id="orderform-totalcount"><?= isset($total) ? $total : 0 ?></td>
            <td class="lead"
                id="orderform-totalsum"><?= Yii::$app->formatter->asCurrency(isset($sum) ? $sum : 0) ?></td>
            <td></td>
        </tr>
    </table>
</div>
<p>
    <?= Html::a('<span class="icon wb-plus"></span>', '#', ['class' => 'btn btn-icon btn-outline btn-round btn-default', 'id' => 'add-orderitem', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('yii', 'Добавить товар')]) ?>
</p>
<?= $form->field($model, 'items', ['template' => "{input}\n{error}"])->hiddenInput() ?>

<hr>

<?= $form->field($model, 'person')->textInput() ?>
<?= $form->field($model, 'email')->textInput() ?>
<?= $form->field($model, 'phone')->textInput() ?>

<div class="form-group">
    <?= Html::submitButton($model->order->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    <?= $model->order->isNewRecord ? '' : Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->order->id], [
        'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>

</div>

<?php ActiveForm::end(); ?>
