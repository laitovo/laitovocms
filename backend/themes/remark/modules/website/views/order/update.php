<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\order\models\OrderForm */

$this->title = Yii::t('app', 'Заказ №{number}', ['number' => $model->order->number]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->order->website->name, 'url' => ['default/view', 'id' => $model->order->website->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заказы'), 'url' => ['index', 'id' => $model->order->website->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu', ['website' => $model->order->website]);
?>

<p class="pull-right" style="margin-top:-40px;"><?= Yii::$app->formatter->asDatetime($model->order->created_at) ?></p>
<?= $this->render('_form', [
    'model' => $model,
]) ?>

