<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\website\News */

$this->title = Yii::t('app', 'Добавить новость');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->news->feed->website->name, 'url' => ['default/view', 'id' => $model->news->feed->website->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ленты'), 'url' => ['feed/index', 'id' => $model->news->feed->website->id]];
$this->params['breadcrumbs'][] = ['label' => $model->news->feed->name, 'url' => ['news/index', 'id' => $model->news->feed->id]];
$this->params['breadcrumbs'][] = $this->title;
$this->render('../menu', ['website' => $model->news->feed->website]);
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
