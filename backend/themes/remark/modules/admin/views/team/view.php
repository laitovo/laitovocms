<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Team */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Администрирование'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Команды'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<p>
    <?= Html::a('<i class="icon wb-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('yii', 'Update')]) ?>
    <?= Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>

<div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'image',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => Html::tag('i', '', ['class' => $model->status ? 'wb-check text-success' : 'wb-close text-danger']),
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'author_id',
                'format' => 'html',
                'value' => $model->author ? Html::a(Html::encode($model->author->name), ['user/view', 'id' => $model->author_id]) : null,
            ],
            [
                'attribute' => 'updater_id',
                'format' => 'html',
                'value' => $model->updater ? Html::a(Html::encode($model->updater->name), ['user/view', 'id' => $model->updater_id]) : null,
            ],
            'json:ntext',
            [
                'attribute' => 'logs',
                'format' => 'html',
                'value' => Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['log/index', 'LogSearch[field_id]' => $model->id, 'LogSearch[table]' => $model->tableName(), 'sort' => '-date']),
            ],
        ],
    ]) ?>
</div>

