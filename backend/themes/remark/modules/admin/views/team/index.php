<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Команды');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Администрирование'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>
<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        'image',
        [
            'attribute' => 'status',
            'format' => 'html',
            'value' => function ($data) {
                return Html::tag('i', '', ['class' => $data->status ? 'wb-check text-success' : 'wb-close text-danger']);
            },
        ],
        'created_at:datetime',
        'updated_at:datetime',
        [
            'attribute' => 'author_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->author ? Html::a(Html::encode($data->author->name), ['user/view', 'id' => $data->author_id]) : null;
            },
        ],
        [
            'attribute' => 'updater_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->updater ? Html::a(Html::encode($data->updater->name), ['user/view', 'id' => $data->updater_id]) : null;
            },
        ],
        'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>