<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\user\User */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'role')->dropDownList(ArrayHelper::map($model->roles, 'id', 'name')) ?>

<?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'website_id')->textInput() ?>

<?= $form->field($model, 'language')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'timezone')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'json')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'subscribe')->checkbox(['data-plugin' => "switchery"]) ?>

<?= $form->field($model, 'status')->checkbox(['data-plugin' => "switchery"]) ?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

