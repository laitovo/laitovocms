<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\user\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Администрирование'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= Html::a('<i class="icon wb-plus"></i>', ['create'], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('app', 'Добавить')]) ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['name', 'email'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        // 'auth_key',
        // 'password_hash',
        // 'password_reset_token',
        // 'access_token',
        'name',
        'username',
        'email:email',
        'phone',
        'role',
        'image',
        [
            'attribute' => 'subscribe',
            'format' => 'html',
            'value' => function ($data) {
                return Html::tag('i', '', ['class' => $data->subscribe ? 'wb-check text-success' : 'wb-close text-danger']);
            },
        ],
        [
            'attribute' => 'status',
            'format' => 'html',
            'value' => function ($data) {
                return Html::tag('i', '', ['class' => $data->status ? 'wb-check text-success' : 'wb-close text-danger']);
            },
        ],
        'created_at:datetime',
        'updated_at:datetime',
        [
            'attribute' => 'author_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->author ? Html::a(Html::encode($data->author->name), ['view', 'id' => $data->author_id]) : null;
            },
        ],
        [
            'attribute' => 'updater_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->updater ? Html::a(Html::encode($data->updater->name), ['view', 'id' => $data->updater_id]) : null;
            },
        ],
        'website_id',
        'language',
        'timezone',
        'json:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'buttonOptions' => ['class' => 'deleteconfirm'],
        ],
    ],
]); ?>

