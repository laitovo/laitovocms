<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\user\User */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Администрирование'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
$roles = ArrayHelper::map($model->roles, 'id', 'name');
?>

<p>
    <?= Html::a('<i class="icon wb-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-icon btn-outline btn-round btn-primary', 'data-toggle' => "tooltip", 'data-original-title' => Yii::t('yii', 'Update')]) ?>
    <?= Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>

<div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            // 'access_token',
            'name',
            'username',
            'email:email',
            'phone',
            [
                'attribute' => 'role',
                'format' => 'raw',
                'value' => isset($roles[$model->role]) ? $roles[$model->role] : null,
            ],
            'image',
            [
                'attribute' => 'subscribe',
                'format' => 'html',
                'value' => Html::tag('i', '', ['class' => $model->subscribe ? 'wb-check text-success' : 'wb-close text-danger']),
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => Html::tag('i', '', ['class' => $model->status ? 'wb-check text-success' : 'wb-close text-danger']),
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'author_id',
                'format' => 'html',
                'value' => $model->author ? Html::a(Html::encode($model->author->name), ['view', 'id' => $model->author_id]) : null,
            ],
            [
                'attribute' => 'updater_id',
                'format' => 'html',
                'value' => $model->updater ? Html::a(Html::encode($model->updater->name), ['view', 'id' => $model->updater_id]) : null,
            ],
            'website_id',
            'language',
            'timezone',
            'json:ntext',
            [
                'attribute' => 'logs',
                'format' => 'html',
                'value' => Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['log/index', 'LogSearch[field_id]' => $model->id, 'LogSearch[table]' => $model->tableName(), 'sort' => '-date']),
            ],
        ],
    ]) ?>
</div>

