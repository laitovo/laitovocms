<?php

/* @var $this View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\themes\remark\assets\FormAsset;
use common\assets\toastr\ToastrAsset;
use common\assets\notie\NotieAsset;
use yii\widgets\Pjax;
use yii\web\View;


$this->title = Yii::t('app', 'Информация по UPN');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Администрирование'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');

FormAsset::register($this);
ToastrAsset::register($this);
NotieAsset::register($this);

$this->registerJsFile('//printjs-4de6.kxcdn.com/print.min.js');
$this->registerCssFile('//printjs-4de6.kxcdn.com/print.min.css');

Yii::$app->view->registerJs('
    var barcode=$(\'#upn-terminal-barcode\');

    function searchTerminal(e)
    {
        $.get("' . Url::to(['search']) . '",{
            upn: barcode.val()
        },function(data){
            $(".reload-terminal").click();
            if (data.status=="success"){
                notie.alert(1,data.message,15);
                $("#upn-info").html(data.html);
            } else {
                notie.alert(3,data.message,15);
                $("#upn-info").html(data.html);
            }

        },"json");

        e.preventDefault();
    }
    
    var keypress;
    $("html").on("keyup","body",function(e){
        if (e.which !== 0 && ( (/[a-zA-Zа-яА-Я0-9-_ ]/.test(e.key) && e.key.length==1) || e.which == 13 || e.which == 8 || e.which == 27 ) ){
            if (e.target.id=="upn-terminal-barcode" && e.which == 13){
                searchTerminal(e);
            } else if (e.target.localName=="body") {
                if (keypress==13){
                    barcode.val("");
                }
                if (e.which == 27 || e.which == 8){
                    barcode.val("");
                } else if (e.which == 13){
                    searchTerminal(e);
                } else{
                    barcode.val(barcode.val()+e.key);
                }
            }
            keypress=e.which;
        }
    });

    ', \yii\web\View::POS_END);

?>
<div class="pull-left">
    <label for="upn-terminal-barcode">Поле для ввода upn</label>
    <input type="text" id="upn-terminal-barcode" placeholder="Введите UPN..." class="form-control">
</div>

<div class="clearfix"></div>
<hr>
<div id="upn-info" ></div>
