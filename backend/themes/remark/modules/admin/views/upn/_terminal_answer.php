<?php

use backend\modules\laitovo\models\ErpNaryad;
use \backend\modules\logistics\models\Naryad;
use common\models\laitovo\ErpNaryad as ErpNaryadAlias;
use \core\models\article\Article;
use \returnModule\core\models\ReturnJournal;
use \core\models\realizationLog\RealizationLog;

/**
* @var $data array
 */


?>

<style>
    .value{
        color: black;
        display: inline-block;
        padding: 2px;
    }

    .value-row{
        margin: 5px 1px;
    }
</style>

<?php foreach ($data as $item): ?>
    <?php
        /** @var  $upn Naryad */
        $upn = $item['upn'];

        /** @var  $returns ReturnJournal[] */
        $returns = $item['returns'];

        /** @var  $realizations RealizationLog[] */
        $realizations = $item['realizations'];

        /** @var  $workOrder ErpNaryad */
        $workOrder = $item['workOrder'];
    ?>
<div class="row">
    <div class="col-md-4">
        <h5>Информация о UPN</h5>
        <ul>
            <li class="value-row">Идентификатор: <span class="value"><?= $upn->id?></span></li>
            <li class="value-row">Артикул: <span class="value"><?= $upn->article?></span></li>

            <li class="value-row">Штрихкод: <span class="value"><?= $upn->barcode?></span></li>
            <li class="value-row">Комментарий: <span class="value"><?= $upn->comment?></span></li>
            <li class="value-row">Дата создания: <span class="value"><?= Yii::$app->formatter->asDatetime($upn->created_at)?></span></li>
            <li class="value-row">Дата последнего изменения: <span class="value"><?= $upn->updated_at ? Yii::$app->formatter->asDatetime($upn->updated_at) : 'null'?></span></li>
            <li class="value-row">Упакован: <span class="value"><?= $upn->packed ? 'Да' : 'Нет'?></span></li>
            <li class="value-row">Зарезервирован: <span class="value"><?= $upn->reserved_id ? 'Да' : 'Нет'?></span></li>
            <?if ($upn->reserved_id):?>
                <? $orderEntry = $upn->orderEntry; ?>
                <? $order = $orderEntry->order; ?>
                <ul>
                    <li> Заказ:
                        <ul>
                            <li class="value-row">Номер: <span class="value"><?= $order->source_innumber?></span></li>
                        </ul>
                    </li>
                    <li> Позиция:
                        <ul>
                            <li class="value-row">Идентификатор: <span class="value"><?= $orderEntry->id?></span></li>
                        </ul>
                    </li>
                </ul>
            <?endif;?>
        </ul>
    </div>

    <div class="col-md-4">
        <h5>Информация по Артикулу</h5>
        <?php $articleInfo = new Article($upn->article) ?>
        <ul>
            <li class="value-row">Бренд: <span class="value"><?= $articleInfo->brand?></span></li>
            <li class="value-row">Количество экранов в артикуле: <span class="value"><?= $articleInfo->countWindow?></span></li>
            <li class="value-row">Форма: <span class="value"><?= $articleInfo->type?></span></li>
            <li class="value-row">Клипсы: <span class="value"><?= !empty($articleInfo->getClipsWithTypesAndCount()) ? implode('|', $articleInfo->getClipsWithTypesAndCount()) : 'Без клипс'?></span></li>
            <li>Применимость
                <ul>
                    <? foreach ($articleInfo->carNameWithAnalogs as $nameWithAnalog): ?>
                        <li><?= $nameWithAnalog?></li>
                    <? endforeach; ?>
                </ul>
            </li>
            <li>
                Статистика:
                <ul>
                    <li class="value-row">Продажи последние 6 месяцев: <span class="value"><?= $articleInfo->saleSixMonth ?></span></li>
                    <li class="value-row">Нужна или нет: <span class="value"><?= $articleInfo->isNeeded() ? 'Да' : 'Нет' ?></span></li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="col-md-4">
        <h5>Местонахождение на складе</h5>
        <? $state = $upn->storageState ?>
        <ul>
            <?php if ($state): ?>
                <li class="value-row">Склад: <span class="value"><?= $state->storage->title?></span></li>
                <li class="value-row">Литера: <span class="value"><?= $state->literal?></span></li>
                <li class="value-row">Дата оприходования: <span class="value"><?= Yii::$app->formatter->asDatetime($state->created_at)?></span></li>
            <?php else: ?>
                <li>Не зарегестрирован, на складе отсутсвует...</li>
            <?php endif; ?>
        </ul>
        <h5>Местонахождение на производстве</h5>
        <ul>
            <?php if ($workOrder): ?>
                <?php if ($workOrder->status == ErpNaryadAlias::STATUS_READY): ?>
                    <li class="value-row">
                        Наряд был изготовлен на производстве...
                    </li>
                <?php else: ?>
                    <li class="value-row">
                        <?= @$workOrder->location && !$workOrder->locationstart ? @$workOrder->location->name : 'Диспетчер : '.@$workOrder->locationstart->name; ?>
                    </li>
                <?php endif; ?>
            <?php else: ?>
                <li>Не изготавливался на производстве...</li>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <h5>Информация по возвратам</h5>
        <?php if (!empty($returns)):?>
            <ul>
                <?php foreach ($returns as $return): ?>
                    <li class="value-row">Идентификатор: <span class="value"><?= $return->id?></span></li>
                    <li class="value-row">Заказ: <span class="value"><?= $return->order_number?></span></li>
                <?php endforeach;?>
            </ul>
        <?php else : ?>
            <p>Информация по возвратам отсутсвует...</p>
        <?php endif; ?>
    </div>

    <div class="col-md-4">
        <h5>Информация по реализациям</h5>
        <?php if (!empty($realizations)):?>
            <ul>
                <?php foreach ($realizations as $realization): ?>
                    <li class="value-row">Заказ: <span class="value"><?= $realization->orderEntry->order->source_innumber?></span></li>
                    <li class="value-row">Литера, с которой реализовано: <span class="value"><?= $realization->literal?></span></li>
                    <li class="value-row">Дата реализации: <span class="value"><?= Yii::$app->formatter->asDatetime($realization->createdAt)?></span></li>
                <?php endforeach;?>
            </ul>
        <?php else : ?>
            <p>Информация по реализациям отсутсвует...</p>
        <?php endif; ?>
    </div>
    <div class="col-md-4">
        &nbsp;
    </div>
</div>
<div class="clearfix"></div>
<hr>
<?php endforeach; ?>



