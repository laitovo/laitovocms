<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LogSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<a class="btn btn-icon btn-outline btn-round btn-default" data-toggle="collapse" href="#exampleCollapseExample"
   aria-expanded="false" aria-controls="exampleCollapseExample">
    <i class="fa fa-search"></i>
</a>
<div class="collapse" id="exampleCollapseExample">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <? //= $form->field($model, 'id') ?>

    <?= $form->field($model, 'table') ?>

    <?= $form->field($model, 'field_id') ?>

    <?= $form->field($model, 'model') ?>

    <?= $form->field($model, 'ip') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'url') ?>

    <?= $form->field($model, 'app') ?>

    <?= $form->field($model, 'module') ?>

    <?= $form->field($model, 'controller') ?>

    <?= $form->field($model, 'action') ?>

    <?= $form->field($model, 'operator') ?>

    <?= $form->field($model, 'log') ?>

    <?= $form->field($model, 'user_id') ?>

    <? //= $form->field($model, 'date') ?>

    <? //= $form->field($model, 'time') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'btn btn-outline btn-round btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
