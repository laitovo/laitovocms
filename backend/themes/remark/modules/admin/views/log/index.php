<?php

use yii\helpers\Html;
use backend\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Логи');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Администрирование'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>
<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-hover'],
    'dataProvider' => $dataProvider,
    'show' => ['table', 'field_id', 'user_id', 'date'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'table',
        'field_id',
        'model',
        'ip',
        'host',
        'url:ntext',
        'app',
        'module',
        'controller',
        'action',
        'operator',
        'log:ntext',
        [
            'attribute' => 'user_id',
            'format' => 'html',
            'value' => function ($data) {
                return $data->user ? Html::a(Html::encode($data->user->name), ['user/view', 'id' => $data->user_id]) : null;
            },
        ],
        'date:datetime',
        'time',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
        ],
    ],
]); ?>

