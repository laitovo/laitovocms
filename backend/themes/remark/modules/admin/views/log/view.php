<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Log */

$this->title = $model->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Администрирование'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Логи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('../menu');
?>

<p>
    <?= Html::a('<i class="icon wb-trash"></i>', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
        'data-toggle' => "tooltip",
        'data-original-title' => Yii::t('yii', 'Delete'),
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>

<div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'table',
            'field_id',
            'model',
            'ip',
            'host',
            'url:ntext',
            'app',
            'module',
            'controller',
            'action',
            'operator',
            'log:ntext',
            [
                'attribute' => 'user_id',
                'format' => 'html',
                'value' => $model->user ? Html::a(Html::encode($model->user->name), ['user/view', 'id' => $model->user_id]) : null,
            ],
            'date:datetime',
            'time',
        ],
    ]) ?>
</div>
