<?php

/* @var $this yii\web\View */

$this->params['menuItems'] = [
    [
        'items' => [
            ['label' => Yii::t('app', 'Пользователи'), 'url' => ['user/index']],
            ['label' => Yii::t('app', 'Команды'), 'url' => ['team/index']],
            ['label' => Yii::t('app', 'Логи'), 'url' => ['log/index']],
            ['label' => Yii::t('app', 'Информация по UPN'), 'url' => ['upn/terminal']],
            ['label' => Yii::t('app', 'Полуфабрикаты'), 'url' => ['semi-finished-articles/index']],
        ],
    ],
];