<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\SettingsForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

Yii::$app->layout='2columns';
$this->render('menu');

$this->title = Yii::t('app', 'Сменить пароль');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки'), 'url' => ['/settings']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'oldpassword')->passwordInput(['maxlength' => true]) ?>

<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

<?= $form->field($model, 'passwordr')->passwordInput(['maxlength' => true]) ?>

<?= Html::submitButton( Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>

<?php ActiveForm::end(); ?>
