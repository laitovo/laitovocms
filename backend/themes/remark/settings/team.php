<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\TeamForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\widgets\GridView;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

Yii::$app->layout='2columns';
$this->render('menu');

$this->title = Yii::t('app', 'Команда: {team}',['team'=>$model->team->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки'), 'url' => ['/settings']];
$this->params['breadcrumbs'][] = $this->title;


Yii::$app->view->registerJs('

    function parseitemsjson () {
        var organization =[];
        $(\'.organizationitems .item\').each(function( index, value ) {
            itemjson=$(this).find("input").serializeArray();
            var obj ={};
            $.each(itemjson, function( index1, value1 ) {
                obj[value1.name]=value1.value;
            });
            organization.push(obj);
        });
        $(\'#teamform-organization\').val(JSON.stringify(organization));
    }

    var edititem = null;

    $("body").on("click",".neworganization",function(e){
        window.edititem = null;
        $(".organizationmodalform input").val("");
    });

    $("body").on("click",".editorganization",function(e){
        window.edititem = $(this).parent();
        $(".organizationmodalform input").val("");
        $(".organizationmodalform .organization-id").val($(this).parent().find("input[name=\'id\']").val());
        $(".organizationmodalform .organization-name").val($(this).parent().find("input[name=\'name\']").val());
        $(".organizationmodalform").modal("show") 
        e.preventDefault();
    });

    $("body").on("submit",".saveorganization",function(e){
        $(".organizationitems .newitem input[name=\'id\']").val($(".organizationmodalform .organization-id").val());
        $(".organizationitems .newitem input[name=\'name\']").val($(".organizationmodalform .organization-name").val());
        $(".organizationitems .newitem input[name=\'name\']").prev().text($(".organizationmodalform .organization-name").val());

        if (window.edititem){
            $(".organizationitems .newitem").clone().insertAfter(window.edititem).removeClass("hidden newitem").addClass("item");
            window.edititem.remove();
        } else {
            $(".organizationitems .newitem").clone().appendTo(".organizationitems").removeClass("hidden newitem").addClass("item");
        }

        parseitemsjson();
        $(".organizationmodalform").modal("hide") 
        e.preventDefault();
    });

    $("body").on("click",".organizationitems .deletethisitem",function(e){
        var item = $(this).parent();
        notie.confirm("'.Yii::t('app', 'Удалить организацию?').'", "'.Yii::t('yii', 'Yes').'", "'.Yii::t('yii', 'No').'", function() {
            item.remove();
            parseitemsjson();
        });
        e.preventDefault();
    });
    ', \yii\web\View::POS_END);

?>

<?php $form = ActiveForm::begin(['id'=>'teamform-form']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <h4><?=Yii::t('app', 'Реквизиты')?></h4>
    <?= $form->field($model, 'organization')->hiddenInput()->label(false) ?>

<?php ActiveForm::end(); ?>

<div class="organizationitems">
    <div class="newitem hidden">
        <a href="#" class="editorganization"></a>
        <input type="hidden" name="name">
        <input type="hidden" name="id">
        <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
    </div>
    <?foreach ($model->team->organizations as $org):?>
        <div class="item">
            <a href="#" class="editorganization"><?=Html::encode($org->name)?></a>
            <input type="hidden" name="name" value="<?= Html::encode($org->name);?>">
            <input type="hidden" name="id" value="<?=$org->id?>">
            <a href="#" class="deletethisitem"><i class="wb-close text-danger"></i></a>
        </div>
    <?endforeach?>
</div>

<?php Modal::begin([
    // 'clientOptions'=>['show'=>$invite->hasErrors() ? true : false],
    'options'=>['class'=>'organizationmodalform fade modal'],
    'header' => Html::tag('h4',Yii::t('app', 'Реквизиты организации'),['class'=>'modal-title']),
    'toggleButton' => ['class'=>'neworganization btn btn-icon btn-round btn-default btn-outline','label' => '<i class="icon wb-plus"></i>'],
]);?>
<form class="saveorganization">

    <div class="form-group">
        <label class="control-label" for="organization-name"><?=Yii::t('app', 'Наименование')?></label>
        <input type="text" required="required" value="" class="required form-control organization-name" name="organization-name">
        <input type="hidden" class="organization-id" name="organization-id">
    </div>

    <?= Html::submitButton( Yii::t('app', 'Готово'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>

</form>

<?php Modal::end();?>


<p>&nbsp;</p>
<?= Html::submitButton( Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary btn-round btn-outline','onclick'=>'$("#teamform-form").submit();']) ?>


<hr>

<h4><?=Yii::t('app', 'Модули')?></h4>
<ul class="list-group list-group-full col-sm-4">
	<?foreach ($modules as $mod):?>

	<?=Yii::$app->team->can($mod->name) ? '<li class="col-xs-10 list-group-item active"><i class="wb-check text-success"></i> '.$mod->description.'</li>':'<li class="col-xs-10 list-group-item disabled"><i class="wb-close text-danger"></i> '.$mod->description.'</li>'?>
	<?if ($mod->modules):?>
		<ul class="">
			<?foreach ($mod->modules as $submod):?>
				<?=Yii::$app->team->can($submod->name) ? '<li class="list-group-item active"><i class="wb-check text-success"></i> '.$submod->description.'</li>':'<li class="list-group-item disabled"><i class="wb-close text-danger"></i> '.$submod->description.'</li>'?>
			<?endforeach?>
		</ul>
	<?endif?>

    <div class="col-xs-2 <?=Yii::$app->team->can($mod->name) ? '': 'hidden'?>">
        <?php Modal::begin([
            'header' => Html::tag('h4',Yii::t('app', 'Настройка полей'),['class'=>'modal-title']),
            'toggleButton' => ['class'=>'btn btn-round btn-default btn-icon btn-outline','label' => '<i class="icon wb-settings"></i>'],
        ]);?>

        <?if (isset($fields[$mod->name]))foreach ($fields[$mod->name] as $key => $value):?>
            <?= Html::tag('p',Html::a($value, ['fields','team'=>$model->team->id,'module'=>$mod->name,'table'=>$key])); ?>
        <?endforeach?>

        <?php Modal::end();?>
    </div>

	<?endforeach?>
</ul>
<div class="clearfix"></div>
<hr>

<h4><?=Yii::t('app', 'Сотрудники')?></h4>

<div class="row visible-xs">
    <div class="col-xs-12">
        <?php $form = ActiveForm::begin(['method'=>'get','action'=>['team','id'=>$model->team->id]]); ?>
            <div class="form-group">
                <div class="input-search input-search-dark"><i class="input-search-icon wb-search" aria-hidden="true"></i><input type="text" class="form-control" name="search" value="<?=Yii::$app->request->get('search')?>" placeholder="<?=Yii::t('app', 'Поиск')?>"></div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="pull-right hidden-xs" style="margin-left:10px;">
    <?php $form = ActiveForm::begin(['method'=>'get','action'=>['team','id'=>$model->team->id]]); ?>
        <div class="form-group">
            <div class="input-search input-search-dark"><i class="input-search-icon wb-search" aria-hidden="true"></i><input type="text" class="form-control" name="search" value="<?=Yii::$app->request->get('search')?>" placeholder="<?=Yii::t('app', 'Поиск')?>"></div>
        </div>
    <?php ActiveForm::end(); ?>
</div>


<?php Modal::begin([
	'clientOptions'=>['show'=>$invite->hasErrors() ? true : false],
    'header' => Html::tag('h4',Yii::t('app', 'Пригласить сотрудников'),['class'=>'modal-title']),
    'toggleButton' => ['class'=>'pull-left btn btn-round btn-default btn-outline','label' => '<i class="fa fa-user-plus"></i> '.Yii::t('app', 'Пригласить сотрудников')],
]);?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($invite, 'emails')->textArea() ?>

<?= Html::submitButton( Yii::t('app', 'Пригласить'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>

<?php ActiveForm::end(); ?>

<?php Modal::end();?>


<?= GridView::widget([
    'tableOptions'=>['class'=>'table table-hover'],
    'dataProvider' => $dataProvider,
    'show'=>['name','email'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'name',
        'email:email',
        'phone',
        'role',
        'created_at:datetime:'.Yii::t('app', 'Дата регистрации'),
        [
            'attribute'=>'status',
            'format'=>'html',
            'value'=>function ($data) {
                return $data->status ? Html::tag('i','',['class'=>'wb-check text-success']) : Html::tag('i','',['class'=>'wb-close text-danger']);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => $model->team->author_id==Yii::$app->user->id ? '{delete}' : '',
            'buttons' => [
                'delete' => function ($url, $user, $key) use ($model) {
                    if ($user->id!=Yii::$app->user->id) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['unlink-team','id'=>$model->team->id,'user'=>$user->id],[
                            'title' => Yii::t('app', 'Уволить'),
                            'class' => 'deleteconfirm',
                            'aria-label' => Yii::t('app', 'Уволить'),
                            'data-confirm' => Yii::t('app', 'Вы уверены, что хотите убрать сотрудника из команды?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
                },
            ]
        ],
    ],
]); ?>

<?= $model->team->author_id==Yii::$app->user->id ? '' : Html::a($model->team->author_id!=Yii::$app->user->id ? '<i class="icon wb-power"></i>' : '<i class="icon wb-trash"></i>', ['delete-team', 'id' => $model->team->id], [
    'class' => 'pull-right btn btn-icon btn-outline btn-round  btn-danger deleteconfirm',
    'data-toggle'=>"tooltip",
    'data-original-title'=>$model->team->author_id!=Yii::$app->user->id ? Yii::t('app', 'Выйти из команды') : Yii::t('app', 'Удалить команду'),
    'data' => [
        'confirm'=>$model->team->author_id!=Yii::$app->user->id ? Yii::t('app', 'Вы уверены, что хотите выйти из команды? Вернуться в команду можно будет только по приглашению.') : Yii::t('app', 'Вы уверены, что хотите удалить команду?'),
        'method' => 'post',
    ],
]) ?>
