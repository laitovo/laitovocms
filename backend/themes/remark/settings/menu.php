<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
$team=ArrayHelper::map(Yii::$app->user->identity->teams,'id','name');
$teams=[];
if ($team){
    foreach ($team as $id => $name) {
        $teams[]=['label' => Html::encode($name), 'url' => ['settings/team','id'=>$id]];
    }
}
$teams=ArrayHelper::merge($teams,[['label' => '<i class="icon fa fa-plus"></i>'.Yii::t('app', 'Создать команду'), 'url' => ['settings/create-team']]]);
$this->params['menuItems'] = [
    [
        'label' => Yii::t('app', 'Настройки'),
        'items' => [
            ['label' => '<i class="icon fa fa-user"></i>'.Yii::t('app', 'Профиль'), 'url' => ['settings/user']],
        ],
    ],
    [
        'label' => Yii::t('app', 'Безопасность'),
        'items' => [
            ['label' => '<i class="icon fa fa-unlock-alt"></i>'.Yii::t('app', 'Сменить пароль'), 'url' => ['settings/password']],
        ],
    ],
    [
        'label' => '<i class="icon fa fa-users"></i>'.Yii::t('app', 'Команды'),
        'items' => $teams,
    ],
];