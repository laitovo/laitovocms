<?php

namespace backend\helpers;

use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\ErpProductType;
use backend\modules\laitovo\models\PatternRegister;
use common\models\laitovo\Cars;
use common\models\laitovo\ClipsScheme;
use common\models\laitovo\CarClips;
use core\logic\CheckStoragePosition;
use yii\helpers\Json;

class ArticleHelper {

    public static function validate($article)
    {
        $value = explode('-', $article);
        $count = count($value);
        if ($count > 5 || $count < 4)
            return false;
        /**
         * @var $car Cars
         */
        if ($count == 5 && (!in_array(mb_strtoupper($value[0]),['FW','FV','FD','RD','RV','BW']) || !($car = Cars::find()->where(['article' => $value[2]])->one()) || mb_strtoupper($value[1]) != mb_strtoupper($car->getTranslit(mb_substr($car->mark,0,1)))))
            return false;

        if ($count == 4 && !in_array(mb_strtoupper($value[0]),['OT']) )
            return false;

        return true;
    }

    public static function validateBarcode($barcode)
    {
        $length = mb_strlen($barcode);
        if ($length != 9)
            return false;

        $window = mb_substr($barcode,0,2);
        if (!in_array($window, ['FW','FD','FV','RD','RV','BW','OT']))
            return false;

        $carArticle = ltrim(mb_substr($barcode,2,4),0);
        if ($window != 'OT' && !Cars::find()->where(['article' => $carArticle])->one())
            return false;

        if ($window == 'OT' && !CheckStoragePosition::isStorageBarcode($barcode))
            return false;

        return true;
    }

    public static function createArticleFromBarcode($barcode)
    {
        $article = null;

        if (!self::validateBarcode($barcode))
            return $article;

        $value = [];

        $window     = mb_substr($barcode,0,2);
        $carArticle = ltrim(mb_substr($barcode,2,4),0);
        $cloth      = ($res = mb_substr($barcode,6,2)) == '00' ? '0' : ltrim(mb_substr($barcode,6,2),0);
        $type       = mb_substr($barcode,8);

        $value[] = $window;
        if ($window != 'OT') {
            /**
             * @var $car Cars
             */
            $car = Cars::find()->where(['article' => $carArticle])->one();
            $value[] = $mark = $car->getTranslit(mb_substr($car->mark,0,1));
        }
        $value[] = $carArticle;
        $value[] = $cloth;
        $value[] = $type;
        $article = implode('-',$value);

        return $article;
    }

    public static function rebuildArticleForAnalog($article,$newCarArticle)
    {
        $newArticle = null;

        if (!self::validate($article))
            return $newArticle;

        $value = explode('-', $article);
        $count = count($value);
        if ($count != 5)
            return $newArticle;

        $car = Cars::find()->where(['article' => $newCarArticle])->one();
        if (!$car)
            return $newArticle;
        $value[1] = $car->getTranslit(mb_substr($car->mark,0,1));
        $value[2] = $newCarArticle;

        $article = implode('-',$value);

        return mb_strtoupper($article);
    }

    public static function windowBW($article)
    {
        $value = explode('-', $article);
        if ($value[0] == 'BW') return true;
        return false;
    }

    public static function isNoName($article)
    {
        $value = explode('-', $article);
        return  $value[3] == 55;
    }

    public static function getChikoInMagnet($article)
    {
        $value = explode('-', $article);
        if ((@$value[3] == 67 || @$value[3] == 72) && (@$value[4] == 5)){
            return true;
        }

        return false;
    }

    public static function laitovoInMagnet($article)
    {
        $value = explode('-', $article);
        if($value[3] == 67  && $value[4] == 2) return true;
        if($value[3] == 67  && $value[4] == 4) return true;
        if($value[3] == 70  && $value[4] == 2) return true;
        if($value[3] == 70  && $value[4] == 1) return true;
        if($value[3] == 70  && $value[4] == 8) return true;
        if($value[3] == 71  && $value[4] == 2) return true;
        if($value[3] == 72  && $value[4] == 2) return true;
        if($value[3] == 72  && $value[4] == 4) return true;
        if($value[3] == 72  && $value[4] == 8) return true;
        return false;
    }

    public static function getBrand($article)
    {
        $value = explode('-', $article);

        if (@$value[4] == 5)
            return 'Chiko';

        return 'Laitovo';
    }

    public static function chikoMagnet($article)
    {
        $value = explode('-', $article);
        if ($value[3] == 45 && $value[4] == 5) return true;
        if ($value[3] == 48 && $value[4] == 5) return true;
        return false;
    }

    public static function vizorMagnet($article)
    {
        $value = explode('-', $article);
        if ($value[3] == 68) return true;
        return false;
    }

    public static function vizorInMagnet($article)
    {
        $value = explode('-', $article);
        if ($value[3] ==69) return true;
        return false;
    }

    public static function laitovoMagnet($article)
    {
        $value = explode('-', $article);
        if (@$value[3] == 45 && @$value[4] == 4
            || @$value[3] == 45 && @$value[4] == 8
            || @$value[3] == 45 && @$value[4] == 2
            || @$value[3] == 48 && @$value[4] == 4
            || @$value[3] == 48 && @$value[4] == 8
            || @$value[3] == 48 && @$value[4] == 2) return true;
        return false;
    }

    public static function mosquitoNet($article)
    {
        $value = explode('-', $article);
        if($value[3]==49) return true;
        return false ;
    }

    /**
     * Возвращает тип оконного проема, для которого изготавливается продукция.
     * Если это дополнительная продукция, возвращаем null
     *
     * @param $article
     * @return mixed|null
     */
    public static function getWindow($article)
    {
        $value = mb_substr($article, 0, 2, 'utf-8');
        $value = str_replace('FW', 'ПШ', $value);
        $value = str_replace('FV', 'ПФ', $value);
        $value = str_replace('FD', 'ПБ', $value);
        $value = str_replace('RD', 'ЗБ', $value);
        $value = str_replace('RV', 'ЗФ', $value);
        $value = str_replace('BW', 'ЗШ', $value);

        return $value != 'OT' ? $value : null;
    }

    /**
     * Возвращает тип оконного проема, для которого изготавливается продукция.
     * Если это дополнительная продукция, возвращаем null
     *
     * @param $article
     * @return mixed|null
     */
    public static function getWindowEn($article)
    {
        $value = mb_substr($article, 0, 2, 'utf-8');

        return $value != 'OT' ? $value : null;
    }

    public static function getCountWindow($article)
    {
        switch (self::getWindow($article)) {
            case 'ПШ':
                return 1;
            case 'ПФ':
                return 2;
            case 'ПБ':
                return 2;
            case 'ЗБ':
                return 2;
            case 'ЗФ':
                return 2;
            case 'ЗШ':
                return 1;
            default:
                return 1;
        }
    }

    /**
     * @param $article
     * @return string|null
     */
    public static function getCarArticle($article)
    {
        $value = explode('-', $article);
        if ($value[0] != 'OT')
            return @$value[2];
        return null;
    }

    public static function getCar($article)
    {
        $carArticle = self::getCarArticle($article);
        return $carArticle ? Cars::find()->where(['article' => $carArticle]) : null;
    }

    public static function getCarModel($article)
    {
        $carArticle = self::getCarArticle($article);
        return $carArticle ? Cars::find()->where(['article' => $carArticle])->one() : null;
    }


    public static function getType($article)
    {
        $value = explode('-', $article);
        if(@$value[0]=='RV') {
            $car = self::getCar($article)->one();
            if($car && isset($car->json('_rv')[strtolower(self::getBrand($article))]['standart']['forma']) && $car->json('_rv')[strtolower(self::getBrand($article))]['standart']['forma'] =='Треугольная')
                return 'Треугольная';
        }

        if(@$value[0]=='BW') {
            $car = self::getCar($article)->one();
            if ($car && isset($car->json('_bw')['chastei']) &&  $car->json('_bw')['chastei'] == '2')
                return 'Стандарт 2 части';
        }

        if (@$value[3] == 2)
            return 'ВырезДляКурящих';

        if (@$value[3] == 3)
            return 'ВырезДляЗеркала';

        if (@$value[3] == 6)
            return 'Укороченный';

        return 'Стандарт';
    }

    public static function getType_clips($article)
    {
        $value = explode('-', $article);
        if( @$value[3] == 45 || @$value[3] == 48 ||
            @$value[3] == 67 || @$value[3] == 70 ||
            @$value[3] == 71 || @$value[3] == 68 || @$value[3] == 69)
        {
            return 'Магнитные';
        }else{
            return 'Простые';
        }
    }


    /**
     * Возвращает массив клипс и их количество.
     *
     * @param $article
     * @return array
     */
    public static function getClips($article)
    {
        $newClips = [];
        $newArray = [];
        $clipsShema = ClipsScheme::find()->where([
            'window' => self::getWindow($article),
            'car_id' => self::getCar($article)->one()->id,
            'brand'=>self::getBrand($article),
            'type' => self::getType($article),
            'type_clips'=>self::getType_clips($article),
        ])->one();
        if (isset($clipsShema)) {
            $clips = Json::decode($clipsShema->json);
            foreach ($clips['scheme'] as $item => $value) {
                if ($value) $newClips[] = $value;
            }

            if (isset($newClips) && count($newClips)) {
                foreach ($newClips as $key => $item) {
                    if ($clipItem = CarClips::findOne($item)) {
                        if (isset($newArray[$clipItem->name])) {
                            $newArray[$clipItem->name]++;
                        } else {
                            $newArray[$clipItem->name] = 1;
                        }
                    }
                }

                foreach ($newArray as $key => $value) {
                    switch (self::getWindow($article)) {
                        case 'ПШ':
                            $newArray[$key] = $value * 1;
                            break;
                        case 'ПФ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ПБ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗБ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗФ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗШ':
                            $newArray[$key] = $value * 1;
                            break;
                        default:
                            $newArray[$key] = $value * 1;
                    }
                }
            }
        }
        ksort($newArray);
        return $newArray;
    }


    /**
     * Возвращает массив клипс и их количество.
     *
     * @param $article
     * @return array
     */
    public static function getClipsOnly($article)
    {
        $typeClips = self::isTape($article) ? 'Скотч' : 'Простые';
        $newClips = [];
        $newArray = [];
        $clipsShema = ClipsScheme::find()->where([
            'window' => self::getWindow($article),
            'car_id' => self::getCar($article)->one()->id,
            'brand'=>self::getBrand($article),
            'type' => self::getType($article),
            'type_clips'=> $typeClips,
        ])->one();
        if (isset($clipsShema)) {
            $clips = Json::decode($clipsShema->json);
            foreach ($clips['scheme'] as $item => $value) {
                if ($value) $newClips[] = $value;
            }

            if (isset($newClips) && count($newClips)) {
                foreach ($newClips as $key => $item) {
                    if ($clipItem = CarClips::findOne($item)) {
                        if (isset($newArray[$clipItem->name])) {
                            $newArray[$clipItem->name]++;
                        } else {
                            $newArray[$clipItem->name] = 1;
                        }
                    }
                }

                foreach ($newArray as $key => $value) {
                    switch (self::getWindow($article)) {
                        case 'ПШ':
                            $newArray[$key] = $value * 1;
                            break;
                        case 'ПФ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ПБ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗБ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗФ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗШ':
                            $newArray[$key] = $value * 1;
                            break;
                        default:
                            $newArray[$key] = $value * 1;
                    }
                }
            }
        }
        ksort($newArray);
        return $newArray;
    }

    /**
     * Возвращает массив клипс и их количество.
     *
     * @param $article
     * @return array
     */
    public static function getMagnets($article)
    {
        $newClips = [];
        $newArray = [];
        $clipsShema = ClipsScheme::find()->where([
            'window' => self::getWindow($article),
            'car_id' => self::getCar($article)->one()->id,
            'brand'=>self::getBrand($article),
            'type' => self::getType($article),
            'type_clips'=> 'Магнитные',
        ])->one();
        if (isset($clipsShema)) {
            $clips = Json::decode($clipsShema->json);
            foreach ($clips['scheme'] as $item => $value) {
                if ($value) $newClips[] = $value;
            }

            if (isset($newClips) && count($newClips)) {
                foreach ($newClips as $key => $item) {
                    if ($clipItem = CarClips::findOne($item)) {
                        if (isset($newArray[$clipItem->name])) {
                            $newArray[$clipItem->name]++;
                        } else {
                            $newArray[$clipItem->name] = 1;
                        }
                    }
                }

                foreach ($newArray as $key => $value) {
                    switch (self::getWindow($article)) {
                        case 'ПШ':
                            $newArray[$key] = $value * 1;
                            break;
                        case 'ПФ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ПБ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗБ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗФ':
                            $newArray[$key] = $value * 2;
                            break;
                        case 'ЗШ':
                            $newArray[$key] = $value * 1;
                            break;
                        default:
                            $newArray[$key] = $value * 1;
                    }
                }
            }
        }
        ksort($newArray);
        return $newArray;
    }

    public static function getClipsWithTypes($article)
    {
        $typeClips = self::isTape($article) ? 'Скотч' : 'Простые';
        $newClips = [];
        $newArray = [];
        $clipsShema = ClipsScheme::find()->where([
            'window' => self::getWindow($article),
            'car_id' => self::getCar($article)->one()->id,
            'brand'=>self::getBrand($article),
            'type' => self::getType($article),
            'type_clips'=> $typeClips,
        ])->one();
        if (isset($clipsShema)) {
            $clips = Json::decode($clipsShema->json);
            foreach ($clips['scheme'] as $item => $value) {
                if ($value) $newClips[] = $value;
            }

            if (isset($newClips) && count($newClips)) {
                foreach ($newClips as $key => $item) {
                    /**
                     * @var $clipsOne CarClips
                     */
                    $clipsOne = CarClips::findOne($item);
                    if ($clipsOne) {
                        $newArray[$clipsOne->name] = $clipsOne->type;
                    }
                }
            }
        }
        ksort($newArray);
        return $newArray;
    }

    /**
     * Функция возвращает массив клипс вида ['side' => ['name' => 'type']]
     *
     * @param $article string
     * @return array
     */
    public static function getClipsWithSidesAndTypes($article)
    {
        $typeClips = self::isTape($article) ? 'Скотч' : 'Простые';
        $newClips = [];
        $newArray = [];
        $clipsShema = ClipsScheme::find()->where([
            'window' => self::getWindow($article),
            'car_id' => self::getCar($article)->one()->id,
            'brand'=>self::getBrand($article),
            'type' => self::getType($article),
            'type_clips'=> $typeClips,
        ])->one();
        if (isset($clipsShema)) {
            $clips = Json::decode($clipsShema->json);
            foreach ($clips['scheme'] as $clipIndex => $clipId) {
                if ($clipId) $newClips[$clipIndex] = $clipId;
            }

            foreach ($newClips as $clipIndex => $clipId) {
                /**
                 * @var $clipsOne CarClips
                 */
                $clipsOne = CarClips::findOne($clipId);
                if ($clipsOne) {
                    $window = self::getWindowEn($article);
                    $isRvTriangle = self::isRvTriangular($article);
                    $side = CarClips::getSide($window, $clipIndex, $isRvTriangle);
                    if ($side) {
                        $newArray[$side][$clipsOne->name] = $clipsOne->type;
                    }
                }
            }
        }
        ksort($newArray);

        return $newArray;
    }

    /**
     * Фнукция необходима для того, чтобы вывести клипсы для инструкций в нужном формате
     *
     * @param $article
     * @return array
     */
    public static function getClipsTypes($article)
    {
        $newClips = [];
        $newArray = [];
        $clipsShema = ClipsScheme::find()->where([
            'window' => self::getWindow($article),
            'car_id' => self::getCar($article)->one()->id,
            'brand'=>self::getBrand($article),
            'type' => self::getType($article),
            'type_clips'=>self::getType_clips($article),
        ])->one();
        if (isset($clipsShema)) {
            $clips = Json::decode($clipsShema->json);
            foreach ($clips['scheme'] as $item => $value) {
                if ($value) $newClips[] = $value;
            }

            if (isset($newClips) && count($newClips)) {
                foreach ($newClips as $key => $item) {
                    /**
                     * @var $clipsOne CarClips
                     */
                    $clipsOne = CarClips::findOne($item);
                    if ($clipsOne) {
                        $newArray[$clipsOne->type] = $clipsOne->type;
                    }
                }
            }
        }

        ksort($newArray);
        return $newArray;
    }

    public static function getInvoiceTitle($article)
    {
        $car = self::getCarModel($article);

        $type = [
            '1'=>'STANDART',
            '2'=>'SMOKER',
            '3'=>'MIRROR',
            '4'=>'ADD. MOVEABLE',
            '5'=>'FRAMELESS',
            '55' => 'STANDART',
            '6'=>'SHORTED',
            '7'=>'FOLDING',
            '67'=>'WITH MAGNETIC HOLDERS',
            '70'=>'WITH MAGNETIC HOLDERS',
            '71'=>'WITH MAGNETIC HOLDERS',
            '72'=>'SHORTED, WITH MAGNETIC HOLDERS',
        ];

        $type2= [
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'1.5',
            '5'=>'5',
            '6'=>'6',
        ];


        if ($car) {
            $value = explode('-', $article);
            $tkan = isset($value[4]) ? $value[4] : null ;
            $kind = isset($value[3]) ? $value[3] : null ;
            $name='Protective screens for car windows for ';
            $name.= mb_strtoupper($car->fullEnName);
            $name.=' (№'.@$type2[$tkan];
            $name.=', '.self::getWindowEn($article);
            $name.=', '.@$type[$kind].')';
        }else {
            switch ($article) {
                //Лайтбеги
                case 'OT-1189-47-3':
                    $name = 'LaitBag - trunk organiser (Black)';
                    break;
                case 'OT-1189-47-7':
                    $name = 'LaitBag - trunk organiser (Khaki)';
                    break;
                //Запасные комлпекты
                case 'OT-564-0-0':
                    $name = 'Set of clip holders for protective screens for car windows';
                    break;
                case 'OT-1394-0-0':
                    $name = 'Set of magnetic holders';
                    break;
                //Ароматизаторы
                case 'OT-1220-0-0':
                    $name = 'Air freshener Laitovo (BUBBLE GUM) (carton)';
                    break;
                case 'OT-1221-0-0':
                    $name = 'Air freshener Laitovo (NEW CAR) (carton)';
                    break;
                case 'OT-1222-0-0':
                    $name = 'Air Freshener Laitovo (CITRUS) (carton)';
                    break;
                case 'OT-1224-0-0':
                    $name = 'Air Freshener Laitovo (ANTI-TABAK) (carton)';
                    break;
                case 'OT-1223-0-0':
                    $name = 'Air Freshener Laitovo (VANILLA) (carton)';
                    break;
                default:
                    $name = '';
                    break;
            }
        }

        return $name;
    }

    public static function getInvoiceRusTitle($article)
    {
        $car = self::getCarModel($article);

        $type = [
            '1'=>'СТАНДАРТ',
            '2'=>'ВЫРЕЗ ДЛЯ КУРЕНИЯ',
            '3'=>'ВЫРЕЗ ДЛЯ ЗЕРКАЛА',
            '4'=>'СДВИЖНОЙ',
            '5'=>'БЕСКАРКАСНЫЙ',
            '55' => 'СТАНДАРТ',
            '6'=>'УКОРОЧЕННЫЙ',
            '7'=>'СКЛАДНОЙ',
            '67'=>'НА МАГНИТАХ',
            '70'=>'НА МАГНИТАХ',
            '71'=>'НА МАГНИТАХ',
            '72'=>'УКОРОЧЕННЫЙ, НА МАГНИТАХ',
        ];

        $type2= [
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'1.5',
            '5'=>'5',
            '6'=>'6',
        ];


        if ($car) {
            $value = explode('-', $article);
            $tkan = isset($value[4]) ? $value[4] : null ;
            $kind = isset($value[3]) ? $value[3] : null ;
            $name='Защитные экраны для автомобильных окон ';
            $name.= mb_strtoupper($car->fullName);
            $name.=' (№'.@$type2[$tkan];
            $name.=', '.self::getWindow($article);
            $name.=', '.@$type[$kind].')';
        }else {
            switch ($article) {
                //Лайтбеги
                case 'OT-1189-47-3':
                    $name = 'Органайзер для багажника (основной) (ЧЕРНЫЙ)';
                    break;
                case 'OT-1189-47-7':
                    $name = 'Органайзер для багажника (основной) (ХАКИ)';
                    break;
                //Запасные комлпекты
                case 'OT-564-0-0':
                    $name = 'Комплекты клипс к защитным экранам для автомобильных окон';
                    break;
                case 'OT-1394-0-0':
                    $name = 'Комплект магнитных держателей';
                    break;
                //Ароматизаторы
                case 'OT-1220-0-0':
                    $name = 'Ароматизатор автомобильный (БУБЛЬ ГУМ) (картон)';
                    break;
                case 'OT-1221-0-0':
                    $name = 'Ароматизатор автомобильный (НОВАЯ МАШИНА) (картон)';
                    break;
                case 'OT-1222-0-0':
                    $name = 'Ароматизатор автомобильный (ЦИТРУС) (картон)';
                    break;
                case 'OT-1224-0-0':
                    $name = 'Ароматизатор автомобильный (АНТИ-ТАБАК) (картон)';
                    break;
                case 'OT-1223-0-0':
                    $name = 'Ароматизатор автомобильный (ВАНИЛЬНЫЙ) (картон)';
                    break;
                case 'OT-1056-0-0':
                    $name = 'Наклейка Laitovo Family';
                    break;
                case 'OT-1000-1-0':
                    $name = 'Образец рекламный №1 - Шелфтокер с карманом';
                    break;
                case 'OT-1000-2-0':
                    $name = 'Рекламный образец №2 - шелфтокер с образцом';
                    break;
                case 'OT-1000-6-0':
                    $name = 'Рекламный образец №6 - ярлык';
                    break;
                case 'OT-678-0-0':
                    $name = 'Сумка для хранения защитных экранов';
                    break;
                case 'OT-1190-47-3':
                    $name = 'Органайзер для багажника LaitBag (Дополнительный) Чёрный (20х30х25)';
                    break;
                default:
                    $name = CheckStoragePosition::getStorageTitle($article);
                    break;
            }
        }

        return $name;
    }

    public static function getArticleForProduct($article)
    {
        $products = ErpProductType::find()->all();
        foreach ($products as $product) {
            if ($product->rule && preg_match($product->rule, $article)) {
                return $product->title;
            }
        }
        $name = CheckStoragePosition::getStorageTitle($article);
        return $name;
    }

    public static function getTypeTitleRu($article)
    {
        $type = [
            '1'=>'СТАНДАРТ',
            '2'=>'ВЫРЕЗ ДЛЯ КУРЕНИЯ',
            '3'=>'ВЫРЕЗ ДЛЯ ЗЕРКАЛА',
            '4'=>'СДВИЖНОЙ',
            '5'=>'БЕСКАРКАСНЫЙ',
            '55' => 'СТАНДАРТ',
            '6'=>'УКОРОЧЕННЫЙ',
            '7'=>'СКЛАДНОЙ',
            '67'=>'НА МАГНИТАХ',
            '70'=>'НА МАГНИТАХ',
            '71'=>'НА МАГНИТАХ',
            '72'=>'УКОРОЧЕННЫЙ, НА МАГНИТАХ',
            '45'=>'MAGNET, СТАНДАРТ',
            '48'=>'MAGNET, УКОРОЧЕННЫЙ',
        ];

        $car = self::getCarModel($article);

        if ($car) {
            $value = explode('-', $article);
            $kind = isset($value[3]) ? $value[3] : null ;
            return @$type[$kind];
        }

        return '';

    }

    public static function getTypeTitleEn($article) {
        $type = [
            '1'=>'STANDARD',
            '2'=>'SMOKER',
            '3'=>'MIRROR',
            '4'=>'ADD. MOVEABLE',
            '5'=>'FRAMELESS',
            '55' => 'STANDARD',
            '6'=>'SHORTED',
            '7'=>'FOLDING',
            '67'=>'WITH MAGNETIC HOLDERS',
            '70'=>'WITH MAGNETIC HOLDERS',
            '71'=>'WITH MAGNETIC HOLDERS',
            '72'=>'SHORTED, WITH MAGNETIC HOLDERS',
            '45'=>'MAGNET, STANDARD',
            '48'=>'MAGNET, SHORTED',
        ];

        $car = self::getCarModel($article);

        if ($car) {
            $value = explode('-', $article);
            $kind = isset($value[3]) ? $value[3] : null ;
            return @$type[$kind];
        }

        return '';
    }

    public static function getWindowTitleRu($article)
    {
        $type = [
            'FW'=> 'ЛОБОВОЕ СТЕКЛО',
            'FV'=> 'ПЕРЕДНИЕ ФОРТОЧКИ',
            'FD'=> 'ПЕРЕДНИЕ БОКОВЫЕ',
            'RD'=> 'ЗАДНИЕ БОКОВЫЕ',
            'RV'=> 'ЗАДНИЕ ФОРТОЧКИ',
            'BW'=> 'ЗАДНЯЯ ШТОРКА',
        ];

        $car = self::getCarModel($article);

        if ($car) {
            $value = explode('-', $article);
            $kind = isset($value[0]) ? $value[0] : null ;
            return @$type[$kind];
        }

        return '';

    }

    public static function getWindowTitleEn($article)
    {
        $type = [
            'FW'=> 'FRONT WINDOW',
            'FV'=> 'FRONT VENTS',
            'FD'=> 'FRONT SIDE WINDOWS',
            'RD'=> 'REAR SIDE WINDOWS',
            'RV'=> 'REAR VENTS',
            'BW'=> 'BACK WINDOW',
        ];

        $car = self::getCarModel($article);

        if ($car) {
            $value = explode('-', $article);
            $kind = isset($value[0]) ? $value[0] : null ;
            return @$type[$kind];
        }

        return '';
    }

    public static function getArticleBarcode($article)
    {
        $value = explode('-', $article);
        switch (count($value)) {
            case 5:
                return $value[0]
                    . str_pad($value[2], 4, "0", STR_PAD_LEFT)
                    . str_pad($value[3], 2, "0", STR_PAD_LEFT)
                    . $value[4];

            case 4:
                return $value[0]
                    . str_pad($value[1], 4, "0", STR_PAD_LEFT)
                    . str_pad($value[2], 2, "0", STR_PAD_LEFT)
                    . $value[3];

            default:
                return '';
        }
    }

    /**
     * Функция указывает - нужнен ли крбчек для установки данного артикула
     *
     * @param $article
     * @return bool|mixed
     */
    public static function needHook($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;
        $window = self::getWindowEn($article);
        if (in_array(mb_strtoupper($window), ['FD','RD']) && self::isOnMagnets($article))
            return false;

        $FD = ($car->json('kryuchokpb')  == '1');
        $RD = ($car->json('kryuchokzb')  == '1');
        $BW = ($car->json('kryuchokzps')  == '1');
        $RV = false;
        $FV = false;

        $res = [
            'FD' => $FD,
            'RD' => $RD,
            'BW' => $BW,
            'RV' => $RV,
            'FV' => $FV,
        ];

        return $res[mb_strtoupper($window)]??false;
    }

    public static function isRVSquare($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;

        $carsForm = new CarsForm($car->id);

        return $carsForm->rv_laitovo_standart_forma == 'Квадратная';
    }

    public static function isRvTriangular($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;

        $carsForm = new CarsForm($car->id);

        return $carsForm->rv_laitovo_standart_forma == 'Треугольная';
    }

    public static function isBwSklad($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;

        $value = explode('-', $article);
        return isset($value[0]) && isset($value[3]) &&  $value[0] == 'BW' &&  $value[3] == 7 ;
    }

    public static function isMagnet($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;

        $value = explode('-', $article);
        return isset($value[0]) && isset($value[3]) &&  ($value[3] == 45 ||  $value[3] == 48) ;
    }

    public static function isOnMagnets($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;

        $value = explode('-', $article);
        return isset($value[0]) && isset($value[3]) &&  (
            $value[3] == 67 || $value[3] == 70 ||
            $value[3] == 71 || $value[3] == 72 ||
            $value[3] == 68 || $value[3] == 69
        ) ;
    }

    public static function isTape($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;
        $value = explode('-', $article);

        $carsForm = new CarsForm($car->id);
        $window = self::getWindow($article);

        return ( (($window == 'ПБ' && $carsForm->fd_use_tape) || ($window == 'ЗБ' && $carsForm->rd_use_tape)) && isset($value[3]) && in_array($value[3],[1,3,4,6]));
    }

    /**
     * Функция по артикулу смотрит и говорит - нужно лекало или нет для изготовление продукта.
     * @param $article
     * @return bool
     */
    public static function isNeedTemplate($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;

        $value = explode('-', $article);
        return isset($value[0]) && isset($value[3]) && $value[0] != 'OT' && ($value[3] == 4 ||  $value[3] == 5) ;
    }

    /**
     * Функция по артикулу смотрит и говорит - существует ли лекало на данный вид продукта или нет.
     * @param $article
     * @return bool
     */
    public static function isTemplateExists($article)
    {
        $cars = [];

        $car = self::getCarModel($article);
        if (!$car) return false;

        $value = explode('-', $article);

        $window = self::getWindow($article);

        $analogs = $car->analogs;

        foreach ($analogs as $analog) {
            $elements = $analog->json('elements');
            if (in_array($window,$elements)) {
                $cars[] = $analog->analog_id;
            }
        }

        $cars[] = $car->id;

        if (!( isset($value[0]) && isset($value[3]) && $value[0] != 'OT' && ($value[3] == 4 ||  $value[3] == 5) ) ) return false;

        switch ($value[3]) {
            case 4 :
                return PatternRegister::find()->where(['and',
                    ['in','car_id',$cars],
                    ['product_type' => PatternRegister::SD],
                    ['window_type' => $window],
                ])->exists();
            case 5 :
                return PatternRegister::find()->where(['and',
                    ['in','car_id',$cars],
                    ['product_type' => PatternRegister::BK],
                    ['window_type' => $window],
                ])->exists();
            default:
                return false;

        }
    }


    /**
     * Функция по артикулу смотрит и говорит - явялется ли продукт донтулуком.
     * @param $article
     * @return bool
     */
    public static function isDontLook($article)
    {
        $car = self::getCarModel($article);
        if (!$car) return false;

        $value = explode('-', $article);
        return isset($value[0]) && isset($value[3]) && $value[0] != 'OT' && ($value[3] == 4 ||  $value[3] == 5) ;
    }

    /**
     * @param $article
     * @return bool
     */
    public static function isStandardScreen($article)
    {
        $value = explode('-', $article);
        if ( count($value) == 5 && (
               ($value[0] == 'FD' && in_array($value[2],[1,3,4,6]))
            || ($value[0] == 'RD' && in_array($value[2],[1]))
            || ($value[0] == 'FV' && in_array($value[2],[1]))
            || ($value[0] == 'RV' && in_array($value[2],[1]))
            || ($value[0] == 'BW' && in_array($value[2],[1,7]))
            )
        ) return true;
        return false;
    }


}