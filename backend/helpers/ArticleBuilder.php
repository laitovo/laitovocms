<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 10/18/18
 * Time: 8:13 AM
 */

namespace backend\helpers;


use common\models\laitovo\CarsAnalog;
use obmen\models\laitovo\Cars;
use yii\helpers\Json;

class ArticleBuilder
{
    public static function buildList($article)
    {
        /**
         * @var $list array Результирующий массив со всеми данными
         */
        $list = [];

        /**
         * Разбиваем артикул на составляющие и получаем из них массив
         */
        $value = explode('-', $article);

        /**
         * Нам не нужны артикулы ополнительной продукции, так как в них не зашито ничего об автомобилях
         */
        if ($value[0] == 'OT') return $list;

        /**
         * Нормализуем значение оконного проема
         */
        $valueSearch = $value[0];
        $valueSearch = str_replace('FW', 'ПШ', $valueSearch);
        $valueSearch = str_replace('FV', 'ПФ', $valueSearch);
        $valueSearch = str_replace('FD', 'ПБ', $valueSearch);
        $valueSearch = str_replace('RD', 'ЗБ', $valueSearch);
        $valueSearch = str_replace('RV', 'ЗФ', $valueSearch);
        $valueSearch = str_replace('BW', 'ЗШ', $valueSearch);


        /**
         * Ищем id автомобиля в системе
         */
        $carId =  (int)Cars::find()->select('id')->where(['article' => @$value[2]])->scalar();

        /**
         * Если автомобиля в базе данных не найдено, то возвращаем пустой массив информации
         */
        if (!$carId) return $list;

        /**
         * Ищем все автомобили аналоги
         */
        $analogsIds = self::searchAnalogs($carId,$valueSearch);
        $analogs = Cars::find()->where(['in','id',$analogsIds])->all();

        /**
         * Если ни одного автомобиля небыло найдено - возвращаем пустой массив
         */
        if (!$analogs) return  $list;

        /**
         * Генерируем список
         */
        foreach ($analogs as $analog) {
            $item = [];

            /**
             * Заименяем необходимые значения на значении авомобиля аналога
             * @var $analog Cars
             */
            $value[2] = $analog->article;
            $value[1] = $analog->getTranslit(mb_substr($analog->mark,0,1));

            /**
             * Получаем новый артикул
             */
            $newArticle = implode('-',$value);

            /**
             * Получаем штрихкод для нового артикула
             */
            $newBarcode = ArticleHelper::getArticleBarcode($newArticle);

            /**
             * Собираем всю информацию по позиции
             */
            $item['article'] = $newArticle;
            $item['barcode'] = $newBarcode;
            $item['carTitle'] = $analog->getFullEnName();

            $list[] = (object)$item;
        }

        return $list;
    }

    private static function searchAnalogs($car_id,$type,$ids = [])
    {
        $ids2 = [];
        $ids2[] = $car_id;
        $analogs = CarsAnalog::find()->where(['car_id' => $car_id])->all();
        foreach ($analogs as $analog) {
            $json=Json::decode($analog->json ? $analog->json : '{}');
            $types = isset($json['elements']) ? $json['elements'] : [];
            if (!in_array($analog->analog_id, $ids2) && in_array($type,$types)) {
                $ids2[] = $analog->analog_id;
            }
        }

        $diff = array_diff($ids2, $ids);

        if (count($diff)) {
            foreach ($diff as $key => $value) {
                $values = self::searchAnalogs($value,$type,array_merge($diff, $ids));
                if ($values) {
                    foreach ($values as $value) {
                        if (!in_array($value, $diff)) {
                            $diff[] = $value;
                        }
                    }
                }

            }
        }

        //возвращаем либо массив либо ложь
        return empty($diff) ? false : $diff;
    }
}