<?php

namespace backend\helpers;

use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\CarsScheme;
use common\models\laitovo\Cars;

class SchemeHelper {
    public static function getFile($article)
    {
        $carArticle = ArticleHelper::getCarArticle($article);
        $brand = ArticleHelper::getBrand($article);
        $type = ArticleHelper::getType($article);
        $type_clips = ArticleHelper::getType_clips($article);
        $window = ArticleHelper::getWindow($article);
        $scheme = new CarsScheme();
        if ($carArticle && ($car = Cars::find()->where(['article' => $carArticle])->one()) != null) {
            $scheme->model = new CarsForm($car->id);
        }
        $scheme->brand = $brand;
        $scheme->type = $type;
        $scheme->type_clips = $type_clips;
        $scheme->window = $window;

        return $scheme->file();
    }
}