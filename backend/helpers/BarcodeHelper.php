<?php
namespace backend\helpers;

class BarcodeHelper {

    public static function toLatin($str)
    {
        $tr = array(
            "Й"=>"q","Ц"=>"w","У"=>"e","К"=>"r","Е"=>"t","Н"=>"y","Г"=>"u","Ш"=>"i","Щ"=>"o","З"=>"p","Ф"=>"a","Ы"=>"s","В"=>"d","А"=>"f","П"=>"g","Р"=>"h","О"=>"j","Л"=>"k","Д"=>"l","Я"=>"z","Ч"=>"x","С"=>"c","М"=>"v","И"=>"b","Т"=>"n","Ь"=>"m",
            "й"=>"q","ц"=>"w","у"=>"e","к"=>"r","е"=>"t","н"=>"y","г"=>"u","ш"=>"i","щ"=>"o","з"=>"p","ф"=>"a","ы"=>"s","в"=>"d","а"=>"f","п"=>"g","р"=>"h","о"=>"j","л"=>"k","д"=>"l","я"=>"z","ч"=>"x","с"=>"c","м"=>"v","и"=>"b","т"=>"n","ь"=>"m"
        );
        return strtr($str,$tr);
    }

    public static function generateBarcode($article)
    {
        $value = explode('-', $article);
        switch (count($value)) {
            case 5:
                return $value[0]
                    . str_pad($value[2], 4, "0", STR_PAD_LEFT)
                    . str_pad($value[3], 2, "0", STR_PAD_LEFT)
                    . $value[4];

            case 4:
                return $value[0]
                    . str_pad($value[1], 4, "0", STR_PAD_LEFT)
                    . str_pad($value[2], 2, "0", STR_PAD_LEFT)
                    . $value[3];

            default:
                return '';
        }
    }

    /**
     * Генерирует штрихкод для наряда из штрихкода UPN а
     *
     * @param $upnBarcode
     * @return string
     */
    public static function upnToWorkOrderBarcode($upnBarcode)
    {
        $upnBarcode = self::toLatin($upnBarcode);
        $upnBarcode = mb_strtoupper($upnBarcode);
        if (($first = mb_substr($upnBarcode,0,1) !== 'L' && $first = mb_substr($upnBarcode,0,2) !== 'RL') || mb_strlen($upnBarcode) < 9)
            throw new \DomainException('Вы передали штрихкод не UPN а, а какой то другой!');

        return 'N' . $upnBarcode;
    }
}