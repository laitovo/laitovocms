fastcgi_read_timeout 300;

server {
    listen 80;
    server_name biz-zone.biz;

    location /.well-known/acme-challenge/ {
        root /var/www/certbot;
    }

    location /.well-known/pki-validation/ {
        root /var/www/ssl;
    }

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    charset utf-8;
    client_max_body_size 128M;

    listen 443 ssl;
    ssl_certificate     /etc/letsencrypt/live/biz-zone.biz/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/biz-zone.biz/privkey.pem;

    #include /etc/letsencrypt/options-ssl-nginx.conf;
    #ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

    server_name biz-zone.biz;
    root        /var/www/html/backend/web;
    index       index.php;

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    # uncomment to avoid processing of calls to non-existing static files by Yii
    #location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
    #    try_files $uri =404;
    #}
    #error_page 404 /404.html;

    # deny accessing php files for the /assets directory
    location ~ ^/assets/.*\.php$ {
        deny all;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    location ~* /\. {
        deny all;
    }
}