<?php

namespace core\entities\logisticsShipment\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\Shipment_OrderEntry;
use core\relations\Shipment_ShipmentDocument;
use core\relations\TransportCompany_Shipment;
use core\relations\User_Shipment;
use Yii;

/**
 * Сущность "Отгрузка".
 *
 * Описывает отгрузку готовой продукции со склада.
 * При отгрузке компонуются позиции из разных заказов
 * (может отгружаться сразу несколько заказов, отдельные заказы могут отгружаться частично).
 *
 * Class Shipment
 * @package core\entities\logisticsShipment\models
 */
class Shipment extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Наименование транспортной компании
     */
    public $transportCompanyName;
    /**
     * @var int Идентификатор транспортной компании
     */
    public $transportCompanyId;
    /**
     * @var int Дата отгрузки
     */
    public $shipmentDate;
    /**
     * @var string Трэк-код
     */
    public $trackCode;
    /**
     * @var double Вес
     */
    public $weight;
    /**
     * @var string Габариты
     */
    public $size;
    /**
     * @var string Комментарий
     */
    public $comment;
    /**
     * @var string Логистический статус (Обработан, Не обработан, Ожидается взвешивание, ...)
     */
    public $logisticsStatus;
    /**
     * @var bool Со взвешиванием / без взвешивания
     */
    public $hasWeighing;
    /**
     * @var bool С измерением габаритов / без измерения габаритов
     */
    public $hasSizing;
    /**
     * @var string Некий слепок отгрузки на момент её отмены
     */
    public $cancelLog;
    /**
     * @var int Дата упаковки
     */
    public $packedAt;
    /**
     * @var int Дата отгрузки (фактическая)
     */
    public $shippedAt;
    /**
     * @var int Дата обработки
     */
    public $handledAt;
    /**
     * @var bool Флаг, отвечающий за то, учтена ли отгрузка в бухучете (проведен ли в 1С документ-реализация)
     */
    public $accountedIn1C;
    /**
     * @var int Дата проведения в 1С
     */
    public $accountedAt;
    /**
     * @var bool Флаг, отвечающий за то, выслан ли трэк-код клиенту
     */
    public $isTrackCodeSent;
    /**
     * @var int Дата отправки трэк-кода клиенту
     */
    public $trackCodeSentAt;
    /**
     * @var double Сумма доставки
     */
    public $deliveryPrice;
    /**
     * @var double Явная пометка о том, что доставка бесплатна (чтобы точно знать, что сумму доставки не забыли проставить)
     */
    public $isDeliveryFree;
    /**
     * @var boolean Явная пометка о том, что оплата будет производится наложенным платежом
     */
    public $isCashOnDelivery;
        /**
     * @var string Официальный порядковый номер Товарно-транспортной накладной
     */
    public $officialNumber;
    /**
     * @var string Год для официального порядкового номера Товарно-транспортной накладной
     */
    public $officialNumberYear;
    /**
     * @var string Строка поиска для поиска по отгрузке
     */
    public $searchString;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    #Логистические статусы
    const LOGISTICS_STATUS_NOT_HANDLED          = 'not_handled';
    const LOGISTICS_STATUS_ON_STORAGE_REQUEST   = 'on_storage_request';
    const LOGISTICS_STATUS_ON_STORAGE_RESPONSE  = 'on_storage_response';
    const LOGISTICS_STATUS_HANDLED              = 'handled';
    const LOGISTICS_STATUS_PACKED               = 'packed';
    const LOGISTICS_STATUS_SHIPPED              = 'shipped';
    const LOGISTICS_STATUS_CANCELED             = 'canceled';

    public function titles()
    {
        return [
            'id'                   => Yii::t('app', 'ID'),
            'transportCompanyName' => Yii::t('app', 'Транспортная компания'),
            'transportCompanyId'   => Yii::t('app', 'Транспортная компания'),
            'shipmentDate'         => Yii::t('app', 'Дата отгрузки'),
            'trackCode'            => Yii::t('app', 'Трэк-код'),
            'weight'               => Yii::t('app', 'Вес'),
            'size'                 => Yii::t('app', 'Габариты'),
            'comment'              => Yii::t('app', 'Комментарий'),
            'logisticsStatus'      => Yii::t('app', 'Статус'),
            'hasWeighing'          => Yii::t('app', 'Взвешивание'),
            'hasSizing'            => Yii::t('app', 'Измерение габаритов'),
            'cancelLog'            => Yii::t('app', 'Лог при отмене'),
            'packedAt'             => Yii::t('app', 'Дата упаковки'),
            'shippedAt'            => Yii::t('app', 'Дата отгрузки (фактическая)'),
            'handledAt'            => Yii::t('app', 'Дата обработки'),
            'accountedIn1C'        => Yii::t('app', 'Трэк-код выслан'),
            'accountedAt'          => Yii::t('app', 'Дата проведения в 1С'),
            'isTrackCodeSent'      => Yii::t('app', 'Проведен в 1С'),
            'trackCodeSentAt'      => Yii::t('app', 'Дата отправки трэк-кода'),
            'deliveryPrice'        => Yii::t('app', 'Сумма доставки'),
            'isDeliveryFree'       => Yii::t('app', 'Доставка бесплатна'),
            'createdAt'            => Yii::t('app', 'Дата создания'),
            'updatedAt'            => Yii::t('app', 'Дата изменения'),
            'authorId'             => Yii::t('app', 'Автор'),
            'updaterId'            => Yii::t('app', 'Редактор'),
        ];
    }

    public function possibleValues()
    {
        return [
            'logisticsStatus' => [
                self::LOGISTICS_STATUS_NOT_HANDLED          => Yii::t('app', 'Не обработан'),
                self::LOGISTICS_STATUS_ON_STORAGE_REQUEST   => Yii::t('app', 'Отправлен запрос на склад'),
                self::LOGISTICS_STATUS_ON_STORAGE_RESPONSE  => Yii::t('app', 'Получен ответ со склада'),
                self::LOGISTICS_STATUS_HANDLED              => Yii::t('app', 'Обработан'),
                self::LOGISTICS_STATUS_PACKED               => Yii::t('app', 'Упакован'),
                self::LOGISTICS_STATUS_SHIPPED              => Yii::t('app', 'Отгружен'),
                self::LOGISTICS_STATUS_CANCELED             => Yii::t('app', 'Отменен'),
            ],
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId',  'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }

    public function relations()
    {
        return [
            'shipmentDocuments'             => ['attribute' => 'id',                 'class' => Shipment_ShipmentDocument::class, 'getterMethod' => 'shipmentDocuments'],
            'orderEntries'                  => ['attribute' => 'id',                 'class' => Shipment_OrderEntry::class,       'getterMethod' => 'orderEntries'],
            'orderEntriesNotDeleted'        => ['attribute' => 'id',                 'class' => Shipment_OrderEntry::class,       'getterMethod' => 'orderEntriesNotDeleted'],
            'orderEntriesFromLog'           => ['attribute' => 'cancelLog',          'class' => Shipment_OrderEntry::class,       'getterMethod' => 'orderEntriesFromLog'],
            'orderEntriesFromLogNotDeleted' => ['attribute' => 'cancelLog',          'class' => Shipment_OrderEntry::class,       'getterMethod' => 'orderEntriesFromLogNotDeleted'],
            'transportCompany'              => ['attribute' => 'transportCompanyId', 'class' => TransportCompany_Shipment::class, 'getterMethod' => 'transportCompany', 'existsMethod' => 'transportCompanyExists'],
            'author'                        => ['attribute' => 'authorId',           'class' => User_Shipment::class,             'getterMethod' => 'author',           'existsMethod' => 'authorExists'],
            'updater'                       => ['attribute' => 'updaterId',          'class' => User_Shipment::class,             'getterMethod' => 'updater',          'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['transportCompanyName', 'trackCode', 'logisticsStatus', 'size'], 'string', 'max' => 255],
            [['officialNumberYear'], 'integer'],
            [['officialNumber'], 'integer'],
            [['officialNumber'], 'validateUniqueIndex'],
            [['comment'], 'string', 'max' => 1000],
            [['cancelLog'], 'string'],
            [['searchString'], 'string'],
            [['transportCompanyId', 'shipmentDate', 'handledAt', 'packedAt', 'shippedAt', 'accountedAt', 'trackCodeSentAt', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['weight', 'deliveryPrice'], 'number', 'min' => 0],
            [['hasWeighing', 'hasSizing', 'accountedIn1C', 'isTrackCodeSent', 'isDeliveryFree'], 'boolean'],
            [['logisticsStatus'], 'validatePossibility'],
            [['transportCompanyId', 'authorId', 'updaterId'], 'validateExists'],
        ];
    }


    public function validateUniqueIndex($attribute, $params)
    {
        if (!$this->_query->unique($this->id, [
            'officialNumber' => $this->officialNumber,
            'officialNumberYear' => $this->officialNumberYear,
        ])) {
            $this->addError($attribute, 'Это поле должно быть уникальным в разрезе одного года!');
        }
    }

}