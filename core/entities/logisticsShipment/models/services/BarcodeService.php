<?php

namespace core\entities\logisticsShipment\models\services;

use core\entities\base\BaseEntity;

/**
 * Сервис отвечает за работу со штрихкодами отгрузок.
 *
 * Class BarcodeService
 * @package core\entities\logisticsShipment\models\services
 */
class BarcodeService
{
    private $_getSet;
    private $_repository;

    const BARCODE_PREFIX = 'SHIP';

    public function __construct($getSet, $repository)
    {
        $this->_getSet      = $getSet;
        $this->_repository  = $repository;
    }

    /**
     * Поиск отгрузки по штрихкоду
     *
     * @param string $barcode
     * @return BaseEntity|null
     */
    public function findByBarcode($barcode)
    {
        $pattern = '/^' . self::BARCODE_PREFIX . '/';
        if (!preg_match($pattern, $barcode)) {
            return null;
        }
        $shipmentId = preg_replace($pattern, '', $barcode);

        return $this->_repository->findById($shipmentId);
    }

    /**
     * Получение штрихкода отгрузки
     *
     * @param BaseEntity $entity
     * @return string
     */
    public function getBarcode($entity)
    {
        return self::BARCODE_PREFIX . $this->_getSet->getId($entity);
    }
}