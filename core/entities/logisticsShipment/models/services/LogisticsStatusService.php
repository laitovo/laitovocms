<?php

namespace core\entities\logisticsShipment\models\services;

use backend\modules\laitovo\models\ErpNaryad;
use core\entities\base\BaseEntity;
use core\entities\logisticsShipment\models\Shipment;
use core\services\SBox;
use core\services\SShipmentSearch;
use Yii;
use Exception;

/**
 * Сервис отвечает за логику выставления логистического статуса отгрузки.
 *
 * Class LogisticsStatusService
 * @package core\entities\logisticsShipment\models\services
 */
class LogisticsStatusService
{
    private $_entityClass;
    private $_getSet;
    private $_repository;
    private $_relation;
    private $_connection;

    public function __construct($entityClass, $getSet, $repository, $relation)
    {
        $this->_entityClass = $entityClass;
        $this->_getSet      = $getSet;
        $this->_repository  = $repository;
        $this->_relation    = $relation;
        $this->_connection  = Yii::$app->db;
    }

    /**
     * Перевод отгрузки в статус "Отправлен запрос на склад"
     *
     * @param BaseEntity $entity
     * @param bool $hasWeighing Необходимость взвешивания
     * @param bool $hasSizing Необходимость измерения габаритов
     * @return bool Результат операции
     */
    public function sendStorageRequest($entity, $hasWeighing, $hasSizing)
    {
        if (!$this->_canSendStorageRequest($entity)) {
            return false;
        }
        if (!$this->_areAllPositionsOnStorage($entity)) {
            return false;
        }
        $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_ON_STORAGE_REQUEST);
        $this->_getSet->setHasWeighing($entity, $hasWeighing);
        $this->_getSet->setHasSizing($entity, $hasSizing);
        $searchStr = SShipmentSearch::generateSearchString($this->_getSet->getId($entity));
        $this->_getSet->setSearchString($entity, $searchStr);

        SBox::checkAndBoxed($this->_getSet->getId($entity));

        return $this->_repository->save($entity);
    }

    /**
     * Отмена запроса на склад
     *
     * @param BaseEntity $entity
     * @return bool Результат операции
     */
    public function cancelStorageRequest($entity)
    {
        if (!$this->_canCancelStorageRequest($entity)) {
            return false;
        }
        $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_NOT_HANDLED);
        $this->_getSet->setHasWeighing($entity, false);
        $this->_getSet->setHasSizing($entity, false);
        $searchStr = SShipmentSearch::generateSearchString($this->_getSet->getId($entity));
        $this->_getSet->setSearchString($entity, $searchStr);

        SBox::checkAndBoxed($this->_getSet->getId($entity));

        return $this->_repository->save($entity);
    }

    /**
     * Перевод отгрузки в статус "Получен ответ со склада"
     *
     * @param BaseEntity $entity
     * @return bool Результат операции
     */
    public function sendStorageResponse($entity)
    {
        if (!$this->_canSendStorageResponse($entity)) {
            return false;
        }
        $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_ON_STORAGE_RESPONSE);
        $searchStr = SShipmentSearch::generateSearchString($this->_getSet->getId($entity));
        $this->_getSet->setSearchString($entity, $searchStr);

        SBox::checkAndBoxed($this->_getSet->getId($entity));

        return $this->_repository->save($entity);
    }

    /**
     * Перевод отгрузки в статус "Отгружена"
     *
     * @param BaseEntity $entity
     * @param bool $generateNumber default false
     * @return bool Результат операции
     */
    public function ship($entity,$generateNumber = false)
    {
        if (!$this->_canBeShipped($entity)) {
            return false;
        }
        /**
         * При отгрузке заказа , если отгрузке необходимо сформировать официальный номер, тогда мы его формируем
         */
//        if ($generateNumber && ($numObj = $this->_numObj())) {
//            $this->_getSet->setOfficialNumber($entity, $numObj->number);
//            $this->_getSet->setOfficialNumberYear($entity, $numObj->year);
//        }
        $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_SHIPPED);
        $this->_getSet->setShippedAt($entity, time());

        return $this->_repository->save($entity);
    }

    /**
     * Возвращаем объект с годом и следующим порядковым номером для накладной
     * @return object
     */
    private function _numObj()
    {
        $year = date('Y',time());
        $number = $this->_repository->maxWhere(['officialNumberYear' => $year],'officialNumber');
        $data = [
            'year' => $year,
            'number' => ($number ? ($number + 1) : 1),
        ];
        return (object)$data;
    }

    /**
     * Установка веса.
     * Может повлечь за собой смену логистического статуса.
     *
     * @param BaseEntity $entity
     * @param double|null $weight
     */
    public function setWeight($entity, $weight)
    {
        $this->_getSet->setWeight($entity, $weight);
        $this->_updateLogisticsStatus($entity);
    }

    /**
     * Установка габаритов.
     * Может повлечь за собой смену логистического статуса.
     *
     * @param BaseEntity $entity
     * @param string|null $size
     */
    public function setSize($entity, $size)
    {
        $this->_getSet->setSize($entity, $size);
        $this->_updateLogisticsStatus($entity);
    }

    /**
     * Установка свойства "hasWeighing"
     *
     * @param BaseEntity $entity
     * @param bool $hasWeighing
     * @throws Exception
     */
    public function setHasWeighing($entity, $hasWeighing)
    {
        throw new Exception('Установка свойства "hasWeighing" возможна только через функцию "sendStorageRequest"');
    }

    /**
     * Установка свойства "hasSizing"
     *
     * @param BaseEntity $entity
     * @param bool $hasSizing
     * @throws Exception
     */
    public function setHasSizing($entity, $hasSizing)
    {
        throw new Exception('Установка свойства "hasSizing" возможна только через функцию "sendStorageRequest"');
    }

    /**
     * Проверка, что отгрузка находится в статусе "Не обработана"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isNotHandled($entity)
    {
        return $this->_getSet->getLogisticsStatus($entity) == $entity::LOGISTICS_STATUS_NOT_HANDLED;
    }

    /**
     * Проверка, что отгрузка находится в статусе "Отправлен запрос на склад"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isOnStorageRequest($entity)
    {
        return $this->_getSet->getLogisticsStatus($entity) == $entity::LOGISTICS_STATUS_ON_STORAGE_REQUEST;
    }

    /**
     * Проверка, что отгрузка находится в статусе "Получен ответ со склада"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isOnStorageResponse($entity)
    {
        return $this->_getSet->getLogisticsStatus($entity) == $entity::LOGISTICS_STATUS_ON_STORAGE_RESPONSE;
    }

    /**
     * Проверка, что отгрузка находится в статусе "Обработана"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isHandled($entity)
    {
        return $this->_getSet->getLogisticsStatus($entity) == $entity::LOGISTICS_STATUS_HANDLED;
    }

    /**
     * Проверка, что отгрузка находится в статусе "Упакована"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isPacked($entity)
    {
        return $this->_getSet->getLogisticsStatus($entity) == $entity::LOGISTICS_STATUS_PACKED;
    }

    /**
     * Проверка, что отгрузка находится в статусе "Отгружена"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isShipped($entity)
    {
        return $this->_getSet->getLogisticsStatus($entity) == $entity::LOGISTICS_STATUS_SHIPPED;
    }

    /**
     * Проверка, что отгрузка находится в статусе "Отменена"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isCanceled($entity)
    {
        return $this->_getSet->getLogisticsStatus($entity) == $entity::LOGISTICS_STATUS_CANCELED;
    }

    /**
     * Проверка, что отгрузка актуальна (не отменена) и ещё не достигла статуса "Обработана"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isBeforeHandling($entity)
    {
        return $this->isNotHandled($entity) || $this->isOnStorageRequest($entity) || $this->isOnStorageResponse($entity);
    }

    /**
     * Функция возвращает список отгрузок в статусе "Не обработана"
     *
     * @return array
     */
    public function findShipmentsNotHandled()
    {
        return $this->_repository->findBy(['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_NOT_HANDLED]);
    }

    /**
     * Функция возвращает список актуальных (не отменённых) отгрузок, которые ещё не достигли статуса "Отгружена"
     *
     * @return array
     */
    public function findShipmentsBeforeShipping()
    {
        $allShipments = $this->_repository->findAll();
        $shipmentsForPacking = [];
        foreach ($allShipments as $shipment) {
            if (!in_array($shipment->logisticsStatus, [
                $this->_entityClass::LOGISTICS_STATUS_CANCELED,
                $this->_entityClass::LOGISTICS_STATUS_SHIPPED,
            ])) {
                $shipmentsForPacking[] = $shipment;
            }
        }

        return $shipmentsForPacking;
    }

    /**
     * Функция возвращает список всех отгрузок в статусе "Упакована"
     *
     * @return array
     */
    public function findShipmentsPacked()
    {
        return $this->_repository->findBy(['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_PACKED]);
    }

    /**
     * Функция возвращает списко все отгрузок, которые на конкретное число должны быть отгружены
     *
     * @param $reverse
     * @param $time
     * @return mixed
     */
    public function findShipmentsMustBeShipped($time,$reverse = false)
    {
        if ($reverse) {
            $data = mktime(23, 59, 59, date('m',$time), date('d',$time),date('y',$time));

            return $this->_repository->findWhere(['and',
                ['or',
                    ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_HANDLED],
                    ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_PACKED]
                ],
                ['>','shipmentDate',$data]
            ]);
        }

        $data = mktime(23, 59, 59, date('m',$time), date('d',$time),date('y',$time));

        return $this->_repository->findWhere(['and',
            ['or',
                ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_HANDLED],
                ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_PACKED]
            ],
            ['<','shipmentDate',$data]
        ]);
    }

    /**
     * Возвращаем перенесенные отгрузки с определенной даты
     *
     * @param $time integer
     * @return mixed
     */
    public function findShipmentsCarriedOver($time)
    {
        $data = mktime(0, 0, 0, date('m',$time), date('d',$time),date('y',$time));

        return $this->_repository->findWhere(['and',
            ['or',
                ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_HANDLED],
                ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_PACKED]
            ],
            ['<','shipmentDate',$data]
        ]);
    }

    /**
     * Функция возвращает упакованные для определенной даты отгрузки
     *
     * @param $time integer
     * @return mixed
     */
    public function findShipmentsPackedForShip($time)
    {
        $data = mktime(23, 59, 59, date('m',$time), date('d',$time),date('y',$time));

        return $this->_repository->findWhere(['and',
            ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_PACKED],
            ['<','shipmentDate',$data]
        ]);
    }


    /**
     * Функция возвращает список всех отгрузок, которые были обработаны за последние ... секунд
     *
     * @param int $seconds
     * @return array
     */
    public function findShipmentsWasHandledInLast($seconds)
    {
        return $this->_repository->findWhere(['>', 'handledAt', (time() - $seconds)]);
    }

    /**
     * Проверка, что отгрузка может быть переведена в статус "Отправлен запрос на склад".
     * Результат проверки зависит от текущего статуса отгрузки.
     *
     * @param BaseEntity $entity
     * @return bool
     */
    private function _canSendStorageRequest($entity)
    {
        if (!$this->isNotHandled($entity)) {
            return false;
        }

        return true;
    }

    /**
     * Проверка, что отгрузка может быть переведена в статус "Отправлен запрос на склад".
     * Результат проверки зависит от текущего статуса отгрузки.
     *
     * @param BaseEntity $entity
     * @return bool
     */
    private function _canCancelStorageRequest($entity)
    {
        if (!$this->isOnStorageRequest($entity) && !$this->isOnStorageResponse($entity)) {
            return false;
        }

        return true;
    }

    /**
     * Проверка, что отгрузка может быть переведена в статус "Получен ответ со склада".
     * Результат проверки зависит от текущего статуса отгрузки, а также от заполненности необходимых полей.
     *
     * @param BaseEntity $entity
     * @return bool
     */
    private function _canSendStorageResponse($entity)
    {
        if (!$this->isOnStorageRequest($entity)) {
            return false;
        }
        if (!$this->_isStorageRequestPerformed($entity)) {
            return false;
        }

        return true;
    }

    /**
     * Проверка, что запрошенная у склада информация заполнена
     *
     * @param $entity
     * @return bool
     */
    private function _isStorageRequestPerformed($entity)
    {
        $isWeightReady = $this->_getSet->getWeight($entity) || !$this->_getSet->getHasWeighing($entity);
        $isSizeReady   = $this->_getSet->getSize($entity) || !$this->_getSet->getHasSizing($entity);

        return $isWeightReady && $isSizeReady;
    }

    /**
     * Проверка, что отгрузка может быть переведена в статус "Отгружена".
     * Результат проверки зависит от текущего статуса отгрузки.
     *
     * @param BaseEntity $entity
     * @return bool
     */
    private function _canBeShipped($entity)
    {
        if (!$this->isPacked($entity)) {
            return false;
        }

        return true;
    }

    /**
     * Автоматическая установка логистического статуса
     *
     * @param BaseEntity $entity
     */
    private function _updateLogisticsStatus($entity)
    {
        if (!$this->isBeforeHandling($entity)) {
            return;
        }
        $hasWeighing = $this->_getSet->getHasWeighing($entity);
        $hasSizing = $this->_getSet->getHasSizing($entity);
        $weight = $this->_getSet->getWeight($entity);
        $size = $this->_getSet->getSize($entity);
        switch (true) {
            case !$hasWeighing && !$hasSizing:
                $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_NOT_HANDLED);
                break;
            case $hasWeighing && !$hasSizing && !$weight:
                $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_ON_STORAGE_REQUEST);
                break;
            case !$hasWeighing && $hasSizing && !$size:
                $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_ON_STORAGE_REQUEST);
                break;
            case $hasWeighing && $hasSizing && !($weight && $size):
                $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_ON_STORAGE_REQUEST);
                break;
            case $hasWeighing && !$hasSizing && $weight:
                $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_ON_STORAGE_RESPONSE);
                break;
            case !$hasWeighing && $hasSizing && $size:
                $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_ON_STORAGE_RESPONSE);
                break;
            case $hasWeighing && $hasSizing && $weight && $size:
                $this->_getSet->setLogisticsStatus($entity, $entity::LOGISTICS_STATUS_ON_STORAGE_RESPONSE);
                break;
            default:
                break;
        }
    }

    private function _areAllPositionsOnStorage($shipment)
    {
        $orderEntries = $this->_relation->getOrderEntriesNotDeleted($shipment);
        foreach ($orderEntries as $orderEntry) {
            $isOnStorage = !empty($orderEntry->upn->storageState) || ErpNaryad::find()->where(['logist_id' => $orderEntry->upn->id])->andWhere(['status' => ErpNaryad::STATUS_READY])->exists();
            if (!$isOnStorage) {
                return false;
            }
        }

        return true;
    }
}