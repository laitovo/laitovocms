<?php

namespace core\entities\logisticsShipment\models\services;

use core\entities\base\BaseEntity;
use Exception;

/**
 * Сервис отвечает за учёт отгрузок в бухучете.
 *
 * Class AccountingService
 * @package core\entities\logisticsShipment\models\services
 */
class AccountingService
{
    private $_getSet;
    private $_repository;
    private $_entityClass;

    public function __construct($entityClass, $getSet, $repository)
    {
        $this->_entityClass = $entityClass;
        $this->_getSet      = $getSet;
        $this->_repository  = $repository;
    }

    /**
     * Функция возвращает список всех отгрузок, которые могут быть учтены в бухучете
     *
     * @return array
     */
    public function findShipmentsCanBeAccounted()
    {
        return $this->_repository->findWhere(['and',
            ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_SHIPPED],
            ['accountedIn1C'   => false],
        ]);
    }

    /**
     * Функция проверяет, может ли отгрузка быть учтена в бухучете.
     * Учёт разрешён только для отгрузок в статусе "Отгружена".
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function canBeAccounted($entity)
    {
        $this->_getSet->instanceOf($entity);
        return $this->_getSet->getLogisticsStatus($entity) == $this->_entityClass::LOGISTICS_STATUS_SHIPPED &&
            $this->_getSet->getAccountedIn1C($entity) == false;
    }

    /**
     * Пометка о том, что отгрузка учтена в бухучете
     *
     * @param BaseEntity $entity
     * @return bool Результат операции
     */
    public function account($entity)
    {
        if (!$this->canBeAccounted($entity)) {
            return false;
        }
        $this->_getSet->setAccountedIn1C($entity, true);
        $this->_getSet->setAccountedAt($entity, time());

        return $this->_repository->save($entity);
    }

    /**
     * Установка свойства "accountedIn1C"
     *
     * @param BaseEntity $entity
     * @param bool $accountedIn1C
     * @throws \Exception
     */
    public function setAccountedIn1C($entity, $accountedIn1C)
    {
        throw new Exception('Установка свойства "accountedIn1C" возможна только через функцию "account"');
    }

    /**
     * Установка свойства "accountedAt"
     *
     * @param BaseEntity $entity
     * @param int $accountedAt
     * @throws \Exception
     */
    public function setAccountedAt($entity, $accountedAt)
    {
        throw new Exception('Установка свойства "accountedAt" возможна только через функцию "account"');
    }
}