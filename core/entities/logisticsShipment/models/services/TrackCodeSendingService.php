<?php

namespace core\entities\logisticsShipment\models\services;

use core\entities\base\BaseEntity;
use Exception;

/**
 * Сервис отвечает за рассылку трэк-кодов.
 *
 * Class TrackCodeSendingService
 * @package core\entities\logisticsShipment\models\services
 */
class TrackCodeSendingService
{
    private $_getSet;
    private $_repository;
    private $_entityClass;

    public function __construct($entityClass, $getSet, $repository)
    {
        $this->_entityClass = $entityClass;
        $this->_getSet      = $getSet;
        $this->_repository  = $repository;
    }

    public function findOneShipmentForSendTrackCode()
    {
        return $this->_repository->findWhere(['and',
            ['logisticsStatus' => $this->_entityClass::LOGISTICS_STATUS_SHIPPED],
            ['isTrackCodeSent' => false],
        ])[0] ?? null;
    }

    /**
     * Пометка о том, что трэк-код выслан клиенту.
     * Это означает, что клиент может отслеживать свою посылку.
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function sendTrackCode($entity)
    {
        $this->_getSet->instanceOf($entity);
        if (!$this->_canSendTrackCode($entity)) {
            return false;
        }
        $this->_getSet->setIsTrackCodeSent($entity, true);
        $this->_getSet->setTrackCodeSentAt($entity, time());

        return $this->_repository->save($entity);
    }

    /**
     * Установка свойства "isTrackCodeSent"
     *
     * @param BaseEntity $entity
     * @param bool $isTrackCodeSent
     * @throws \Exception
     */
    public function setIsTrackCodeSent($entity, $isTrackCodeSent)
    {
        throw new Exception('Установка свойства "isTrackCodeSent" возможна только через функцию "sendTrackCode"');
    }

    /**
     * Установка свойства "trackCodeSentAt"
     *
     * @param BaseEntity $entity
     * @param int $trackCodeSentAt
     * @throws \Exception
     */
    public function setTrackCodeSentAt($entity, $trackCodeSentAt)
    {
        throw new Exception('Установка свойства "trackCodeSentAt" возможна только через функцию "sendTrackCode"');
    }

    /**
     * Функция проверяет, может ли трэк-код быть выслан клиенту.
     * Операция разрешена только для отгрузок в статусе "Отгружена".
     *
     * @param BaseEntity $entity
     * @return bool
     */
    private function _canSendTrackCode($entity)
    {
        return $this->_getSet->getLogisticsStatus($entity) == $this->_entityClass::LOGISTICS_STATUS_SHIPPED &&
            $this->_getSet->getIsTrackCodeSent($entity) == false;
    }
}