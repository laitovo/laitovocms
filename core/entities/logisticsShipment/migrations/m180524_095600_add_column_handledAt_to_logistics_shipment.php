<?php

use yii\db\Migration;

class m180524_095600_add_column_handledAt_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'handledAt', $this->integer()->comment('Дата обработки'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','handledAt');
    }
}
