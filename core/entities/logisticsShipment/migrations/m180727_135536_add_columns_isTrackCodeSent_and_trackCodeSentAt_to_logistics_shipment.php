<?php

use yii\db\Migration;

class m180727_135536_add_columns_isTrackCodeSent_and_trackCodeSentAt_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'isTrackCodeSent', $this->boolean()->defaultValue(false)->comment('Пометка о том, что трэк-код выслан клиенту'));
        $this->addColumn('{{%logistics_shipment}}', 'trackCodeSentAt', $this->integer()->comment('Дата отправки трэк-кода клиенту'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','isTrackCodeSent');
        $this->dropColumn('{{%logistics_shipment}}','trackCodeSentAt');
    }
}
