<?php

use yii\db\Migration;

class m180806_131743_add_column_transportCompanyId_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'transportCompanyId', $this->integer()->comment('Идентификатор транспортной компании'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','transportCompanyId');
    }
}
