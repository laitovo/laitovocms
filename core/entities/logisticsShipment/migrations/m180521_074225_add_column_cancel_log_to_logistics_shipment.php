<?php

use yii\db\Migration;

class m180521_074225_add_column_cancel_log_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'cancelLog', $this->text()->comment('Некий слепок отгрузки на момент её отмены (какие были позиции и т.д.)'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','cancelLog');
    }
}
