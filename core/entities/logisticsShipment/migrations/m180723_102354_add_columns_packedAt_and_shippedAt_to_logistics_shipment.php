<?php

use yii\db\Migration;

class m180723_102354_add_columns_packedAt_and_shippedAt_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'packedAt', $this->integer()->comment('Дата упаковки'));
        $this->addColumn('{{%logistics_shipment}}', 'shippedAt', $this->integer()->comment('Дата отгрузки (фактическая)'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','packedAt');
        $this->dropColumn('{{%logistics_shipment}}','shippedAt');
    }
}
