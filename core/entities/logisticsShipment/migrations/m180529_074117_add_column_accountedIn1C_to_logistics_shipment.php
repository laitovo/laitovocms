<?php

use yii\db\Migration;

class m180529_074117_add_column_accountedIn1C_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'accountedIn1C', $this->boolean()->defaultValue(false)->comment('Флаг, отвечающий за то, учтена ли отгрузка в бухучете (проведен ли в 1С документ-реализация).'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','accountedIn1C');
    }
}
