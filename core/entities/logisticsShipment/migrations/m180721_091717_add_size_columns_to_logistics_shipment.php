<?php

use yii\db\Migration;

class m180721_091717_add_size_columns_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'size', $this->string()->comment('Габариты'));
        $this->addColumn('{{%logistics_shipment}}', 'hasSizing', $this->boolean()->defaultValue(false)
            ->comment('С измерением габаритов / без измерения габаритов'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','size');
        $this->dropColumn('{{%logistics_shipment}}','hasSizing');
    }
}
