<?php

use yii\db\Migration;

class m180508_131120_create_table_logistics_shipment extends Migration
{
    /**
     * Таблица "Отгрузки"
     */
    public function up()
    {
        $this->createTable('{{%logistics_shipment}}', [
            'id'                   => $this->primaryKey(),
            'transportCompanyName' => $this->string()->comment('Наименование транспортной компании'),
            'shipmentDate'         => $this->integer()->comment('Дата отгрузки'),
            'trackCode'            => $this->string()->comment('Трэк-код'),
            'weight'               => $this->double()->comment('Вес'),
            'comment'              => $this->string(1000)->comment('Комментарий'),
            'logisticsStatus'      => $this->string()->defaultValue('not_handled')->comment('Логистический статус (Обработан, Не обработан, Ожидается взвешивание, ...)'),
            'hasWeighing'          => $this->boolean()->defaultValue(false)->comment('Со взвешиванием / без взвешивания'),
            'createdAt'            => $this->integer()->comment('Дата создания'),
            'updatedAt'            => $this->integer()->comment('Дата последнего изменения'),
            'authorId'             => $this->integer()->comment('Автор'),
            'updaterId'            => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%logistics_shipment}}');
    }
}
