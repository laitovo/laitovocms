<?php

use yii\db\Migration;

class m180719_101815_add_delivery_price_columns_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'deliveryPrice', $this->double()->comment('Сумма доставки'));
        $this->addColumn('{{%logistics_shipment}}', 'isDeliveryFree', $this->boolean()->defaultValue(false)
            ->comment('Явная пометка о том, что доставка бесплатна (чтобы точно знать, что сумму доставки не забыли проставить)'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','deliveryPrice');
        $this->dropColumn('{{%logistics_shipment}}','isDeliveryFree');
    }
}
