<?php

use yii\db\Migration;

class m180719_084423_add_column_accountedAt_to_logistics_shipment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'accountedAt', $this->integer()->comment('Дата проведения в 1С'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}','accountedAt');
    }
}
