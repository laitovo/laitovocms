<?php

namespace core\entities\logisticsShipment;

use core\entities\base\BaseEntity;
use core\entities\base\BaseManager;
use core\entities\logisticsShipment\models\services\AccountingService;
use core\entities\logisticsShipment\models\services\BarcodeService;
use core\entities\logisticsShipment\models\services\LogisticsStatusService;
use core\entities\logisticsShipment\models\services\TrackCodeSendingService;
use core\entities\logisticsShipment\models\Shipment;

/**
 * Класс предоставляет функционал для работы с сущностью "Отгрузка"
 *
 * Class ShipmentManager
 * @package core\entities\logisticsShipment
 */
class ShipmentManager extends BaseManager
{
    private $_logisticsStatusService;
    private $_barcodeService;
    private $_accountingService;
    private $_trackCodeSendingService;

    protected function _init()
    {
        $this->_table       = '{{%logistics_shipment}}';
        $this->_entityClass = Shipment::class;
    }

    protected function _initServices()
    {
        $this->_logisticsStatusService  = new LogisticsStatusService($this->_entityClass, $this->_getSet, $this->_repository, $this->_relation);
        $this->_barcodeService          = new BarcodeService($this->_getSet, $this->_repository);
        $this->_accountingService       = new AccountingService($this->_entityClass, $this->_getSet, $this->_repository);
        $this->_trackCodeSendingService = new TrackCodeSendingService($this->_entityClass, $this->_getSet, $this->_repository);
    }

    /**
     * Перевод отгрузки в статус "Отправлен запрос на склад".
     * Это означает, что логист запросил у кладовщика информацию, без которой не может завершить обработку отгрузки.
     *
     * @param BaseEntity $entity
     * @param bool $hasWeighing Необходимость взвешивания
     * @param bool $hasSizing Необходимость измерения габаритов
     * @return bool Результат операции
     */
    public function sendStorageRequest($entity, $hasWeighing, $hasSizing)
    {
        return $this->_logisticsStatusService->sendStorageRequest($entity, $hasWeighing, $hasSizing);
    }

    /**
     * Отмена запроса на склад
     *
     * @param BaseEntity $entity
     * @return bool Результат операции
     */
    public function cancelStorageRequest($entity)
    {
        return $this->_logisticsStatusService->cancelStorageRequest($entity);
    }

    /**
     * Перевод отгрузки в статус "Получен ответ со склада".
     * Это означает, что кладовщик заполнил необходимую информацию, и логист может завершить обработку отгрузки.
     *
     * @param BaseEntity $entity
     * @return bool Результат операции
     */
    public function sendStorageResponse($entity)
    {
        return $this->_logisticsStatusService->sendStorageResponse($entity);
    }

    /**
     * Перевод отгрузки в статус "Обработана".
     *
     * public function handle($entity)
     *{
     *    Данная операция выходит за рамки одного менеджера, поэтому была вынесена в логику.
     *    См. core\logic\HandleShipment.
     *}
     */

    /**
     * Перевод отгрузки в статус "Упакована".
     *
     * public function pack($entity)
     *{
     *    Данная операция выходит за рамки одного менеджера, поэтому была вынесена в логику.
     *    См. core\logic\PackShipment.
     *}
    */

    /**
     * Перевод отгрузки в статус "Отгружена".
     * Это означает, что отгрузка была физически исполнена.
     *
     * @param BaseEntity $entity
     * @param bool $generateNumber default false
     * @return bool Результат операции
     */
    public function ship($entity,$generateNumber = false)
    {
        return $this->_logisticsStatusService->ship($entity,$generateNumber);
    }

    /**
     * Перевод отгрузки в статус "Отменена".
     *
     * public function cancel($entity)
     *{
     *    Данная операция выходит за рамки одного менеджера, поэтому была вынесена в логику.
     *    См. core\logic\CancelShipment.
     *}
     */

    /**
     * Установка свойства "weight".
     * Может повлечь за собой смену логистического статуса.
     *
     * @param BaseEntity $entity
     * @param double|null $weight
     */
    public function setWeight($entity, $weight)
    {
        $this->_logisticsStatusService->setWeight($entity, $weight);
    }

    /**
     * Установка свойства "size".
     * Может повлечь за собой смену логистического статуса.
     *
     * @param BaseEntity $entity
     * @param string|null $size
     */
    public function setSize($entity, $size)
    {
        $this->_logisticsStatusService->setSize($entity, $size);
    }

    /**
     * Установка свойства "hasWeighing"
     *
     * @param BaseEntity $entity
     * @param bool $hasWeighing
     */
    public function setHasWeighing($entity, $hasWeighing)
    {
        $this->_logisticsStatusService->setHasWeighing($entity, $hasWeighing);
    }

    /**
     * Установка свойства "hasSizing"
     *
     * @param BaseEntity $entity
     * @param bool $hasSizing
     */
    public function setHasSizing($entity, $hasSizing)
    {
        $this->_logisticsStatusService->setHasSizing($entity, $hasSizing);
    }

    /**
     * Проверка, что отгрузка находится в статусе "Не обработана"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isNotHandled($entity)
    {
        return $this->_logisticsStatusService->isNotHandled($entity);
    }

    /**
     * Проверка, что отгрузка находится в статусе "Отправлен запрос на склад"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isOnStorageRequest($entity)
    {
        return $this->_logisticsStatusService->isOnStorageRequest($entity);
    }

    /**
     * Проверка, что отгрузка находится в статусе "Получен ответ со склада"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isOnStorageResponse($entity)
    {
        return $this->_logisticsStatusService->isOnStorageResponse($entity);
    }

    /**
     * Проверка, что отгрузка находится в статусе "Обработана"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isHandled($entity)
    {
        return $this->_logisticsStatusService->isHandled($entity);
    }

    /**
     * Проверка, что отгрузка находится в статусе "Упакована"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isPacked($entity)
    {
        return $this->_logisticsStatusService->isPacked($entity);
    }

    /**
     * Проверка, что отгрузка находится в статусе "Отгружена"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isShipped($entity)
    {
        return $this->_logisticsStatusService->isShipped($entity);
    }

    /**
     * Проверка, что отгрузка находится в статусе "Отменена"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isCanceled($entity)
    {
        return $this->_logisticsStatusService->isCanceled($entity);
    }

    /**
     * Проверка, что отгрузка актуальна (не отменена) и ещё не достигла статуса "Обработана"
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function isBeforeHandling($entity)
    {
        return $this->_logisticsStatusService->isBeforeHandling($entity);
    }

    /**
     * Функция возвращает список отгрузок в статусе "Не обработана"
     *
     * @return array
     */
    public function findShipmentsNotHandled()
    {
        return $this->_logisticsStatusService->findShipmentsNotHandled();
    }

    /**
     * Функция возвращает список актуальных (не отменённых) отгрузок, которые ещё не достигли статуса "Отгружена"
     *
     * @return array
     */
    public function findShipmentsBeforeShipping()
    {
        return $this->_logisticsStatusService->findShipmentsBeforeShipping();
    }

    /**
     * Функция возвращает список всех отгрузок в статусе "Упакована"
     *
     * @return array
     */
    public function findShipmentsPacked()
    {
        return $this->_logisticsStatusService->findShipmentsPacked();
    }

    /**
     * Функция возвращает список всех отгрузок которые уже обработаны + уже упакованы
     *
     * @param $reverse
     * @param $time
     * @return mixed
     */
    public function findShipmentsMustBeShipped($time,$reverse = false)
    {
        return $this->_logisticsStatusService->findShipmentsMustBeShipped($time,$reverse);
    }

    /**
     * Функция возвращает перенесенные отгрузки с другой даты
     *
     * @param $time
     * @return mixed
     */
    public function findShipmentsCarriedOver($time)
    {
        return $this->_logisticsStatusService->findShipmentsCarriedOver($time);
    }

    /**
     * Функция возвращает упакованные для определенной даты отгрузки
     *
     * @param $time
     * @return mixed
     */
    public function findShipmentsPackedForShip($time)
    {
        return $this->_logisticsStatusService->findShipmentsPackedForShip($time);
    }

    /**
     * Функция возвращает список всех отгрузок, которые были обработаны за последние ... секунд
     *
     * @param int $seconds
     * @return array
     */
    public function findShipmentsWasHandledInLast($seconds)
    {
        return $this->_logisticsStatusService->findShipmentsWasHandledInLast($seconds);
    }

    /**
     * Функция возвращает список всех отгрузок, которые могут быть учтены в бухучете
     *
     * @return array
     */
    public function findShipmentsCanBeAccounted()
    {
        return $this->_accountingService->findShipmentsCanBeAccounted();
    }

    /**
     * Функция проверяет, может ли отгрузка быть учтена в бухучете
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function canBeAccounted($entity)
    {
        return $this->_accountingService->canBeAccounted($entity);
    }

    /**
     * Установка свойства "accountedIn1C"
     *
     * @param BaseEntity $entity
     * @param bool $accountedIn1C
     */
    public function setAccountedIn1C($entity, $accountedIn1C)
    {
        $this->_accountingService->setAccountedIn1C($entity, $accountedIn1C);
    }

    /**
     * Установка свойства "accountedAt"
     *
     * @param BaseEntity $entity
     * @param int $accountedAt
     */
    public function setAccountedAt($entity, $accountedAt)
    {
        $this->_accountingService->setAccountedAt($entity, $accountedAt);
    }

    /**
     * Пометка о том, что отгрузка учтена в бухучете (в 1С проведен документ-реализация).
     * Это означает, что данная отгрузка представляет собой факт, а не набросок или черновик.
     *
     * @param BaseEntity $entity
     * @return bool Результат операции
     */
    public function account($entity)
    {
        return $this->_accountingService->account($entity);
    }

    public function findOneShipmentForSendTrackCode()
    {
        return $this->_trackCodeSendingService->findOneShipmentForSendTrackCode();
    }

    /**
     * Пометка о том, что трэк-код выслан клиенту.
     * Это означает, что клиент может отслеживать свою посылку.
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function sendTrackCode($entity)
    {
        return $this->_trackCodeSendingService->sendTrackCode($entity);
    }

    /**
     * Установка свойства "isTrackCodeSent"
     *
     * @param BaseEntity $entity
     * @param bool $isTrackCodeSent
     * @throws \Exception
     */
    public function setIsTrackCodeSent($entity, $isTrackCodeSent)
    {
        return $this->_trackCodeSendingService->setIsTrackCodeSent($entity, $isTrackCodeSent);
    }

    /**
     * Установка свойства "trackCodeSentAt"
     *
     * @param BaseEntity $entity
     * @param int $trackCodeSentAt
     * @throws \Exception
     */
    public function setTrackCodeSentAt($entity, $trackCodeSentAt)
    {
        return $this->_trackCodeSendingService->setTrackCodeSentAt($entity, $trackCodeSentAt);
    }

    /**
     * Поиск отгрузки по штрихкоду
     *
     * @param string $barcode
     * @return BaseEntity|null
     */
    public function findByBarcode($barcode)
    {
        return $this->_barcodeService->findByBarcode($barcode);
    }

    /**
     * Поиск отгрузки по номеру
     *
     * @param $number
     * @param $year
     * @return array
     */
    public function findByOfficialNumber($number,$year)
    {
        return $this->_repository->findWhere(['and',
            ['officialNumber' => $number],
            ['officialNumberYear' => $year]
        ]);
    }

    /**
     * Получение штрихкода отгрузки
     *
     * @param BaseEntity $entity
     * @return string
     */
    public function getBarcode($entity)
    {
        return $this->_barcodeService->getBarcode($entity);
    }
}