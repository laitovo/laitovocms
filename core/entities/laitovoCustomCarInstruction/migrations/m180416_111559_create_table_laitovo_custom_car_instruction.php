<?php

use yii\db\Migration;

class m180416_111559_create_table_laitovo_custom_car_instruction extends Migration
{
    /**
     * Таблица "Индивидуальные инструкции".
     * Префикс laitovo_... означает, что данная таблица относится к модулю "Laitovo".
     */
    public function up()
    {
        $this->createTable('{{%laitovo_custom_car_instruction}}', [
            'id' => $this->primaryKey(),
            'carId'  => $this->integer()->notNull()->comment('Автомобиль'),
            'carArticle'  => $this->integer()->notNull()->comment('Артикул автомобиля'),
            'instructionTypeId'  => $this->integer()->notNull()->comment('Вид инструкции'),
            'fileKey'  => $this->string()->notNull()->comment('Ключ для доступа к файлу'),
            'isUsed'  => $this->boolean()->notNull()->defaultValue(false)->comment('Используется или нет'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%laitovo_custom_car_instruction}}');
    }
}
