<?php

use yii\db\Migration;

class m180619_113629_alter_column_fileKey_from_laitovo_custom_car_instruction extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%laitovo_custom_car_instruction}}', 'fileKey', $this->string()->comment('Ключ для доступа к файлу'));
    }

    public function down()
    {
        $this->alterColumn('{{%laitovo_custom_car_instruction}}', 'fileKey', $this->string()->notNull()->comment('Ключ для доступа к файлу'));
    }
}
