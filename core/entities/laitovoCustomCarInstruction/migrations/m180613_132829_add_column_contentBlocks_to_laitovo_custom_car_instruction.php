<?php

use yii\db\Migration;

class m180613_132829_add_column_contentBlocks_to_laitovo_custom_car_instruction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_custom_car_instruction}}', 'contentBlocks', $this->text()->comment('Отдельные блоки инструкции (json-поле)'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_custom_car_instruction}}','contentBlocks');
    }
}
