<?php

namespace core\entities\laitovoCustomCarInstruction;

use core\entities\base\BaseManager;
use core\entities\laitovoCustomCarInstruction\models\CustomCarInstruction;

/**
 * Класс предоставляет функционал для работы с сущностью "Индивидуальная инструкция"
 *
 * Class CustomCarInstructionManager
 * @package core\entities\laitovoCustomCarInstruction
 */
class CustomCarInstructionManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%laitovo_custom_car_instruction}}';
        $this->_entityClass = CustomCarInstruction::class;
    }
}