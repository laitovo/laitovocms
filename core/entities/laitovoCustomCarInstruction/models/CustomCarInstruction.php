<?php

namespace core\entities\laitovoCustomCarInstruction\models;

use core\entities\base\BaseEntity;
use core\relations\Car_CustomCarInstruction;
use core\relations\InstructionType_CustomCarInstruction;
use Yii;

/**
 * Сущность "Индивидуальная инструкция".
 * Описывает инструкции, которые печатаются вместо стандартных инструкций для особых автомобилей
 * (инструкция - это мануал по установке шторок).
 *
 * Class CustomCarInstruction
 * @package core\entities\laitovoCustomCarInstruction\models
 */
class CustomCarInstruction extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var int Автомобиль
     */
    public $carId;
    /**
     * @var string Артикул автомобиля
     */
    public $carArticle;
    /**
     * @var int Вид инструкции
     */
    public $instructionTypeId;
    /**
     * @var string Ключ для доступа к файлу
     */
    public $fileKey;
    /**
     * @var bool Используется или нет
     */
    public $isUsed;
    /**
     * @var string Отдельные блоки инструкции (json-поле)
     */
    public $contentBlocks;

    public function titles()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'carId' => Yii::t('app', 'Автомобиль'),
            'carArticle' => Yii::t('app', 'Артикул автомобиля'),
            'instructionTypeId' => Yii::t('app', 'Вид инструкции'),
            'fileKey' => Yii::t('app', 'Ключ к файлу'),
            'isUsed' => Yii::t('app', 'Активна'),
            'contentBlocks' => Yii::t('app', 'Блоки'),
        ];
    }

    public function relations()
    {
        return [
            'instructionType' => ['attribute' => 'instructionTypeId', 'class' => InstructionType_CustomCarInstruction::class, 'getterMethod' => 'instructionType', 'existsMethod' => 'instructionTypeExists'],
            'car'             => ['attribute' => 'carId', 'class' => Car_CustomCarInstruction::class, 'getterMethod' => 'car', 'existsMethod' => 'carExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['carId', 'carArticle', 'instructionTypeId'], 'integer'],
            [['fileKey'], 'string', 'max' => 255],
            [['contentBlocks'], 'string'],
            [['isUsed'], 'boolean'],
            [['carId', 'carArticle', 'instructionTypeId', 'isUsed'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['instructionTypeId', 'carId'], 'validateExists'],
        ];
    }

    public function beforeValidate()
    {
        $uniqueGroupOfProperties = [
            'carId' => $this->carId,
            'carArticle' => $this->carArticle,
            'instructionTypeId' => $this->instructionTypeId,
        ];
        if (!$this->_query->unique($this->id, $uniqueGroupOfProperties)) {
            $this->addError('carId', 'Не уникальный набор свойств');
            $this->addError('carArticle', 'Не уникальный набор свойств');
            $this->addError('instructionTypeId', 'Не уникальный набор свойств');
            return false;
        }

        return parent::beforeValidate();
    }
}