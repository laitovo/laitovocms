<?php

namespace core\entities\logisticsLostUpn;

use core\entities\base\BaseManager;
use core\entities\logisticsLostUpn\models\LostUpn;

/**
 * Класс предоставляет функционал для работы с сущностью "Потерянный UPN"
 *
 * Class LostUpnManager
 * @package core\entities\logisticsLostUpn
 */
class LostUpnManager extends BaseManager
{
    protected function _init()
    {
        $this->_table       = '{{%logistics_lost_upn}}';
        $this->_entityClass = LostUpn::class;
    }
}