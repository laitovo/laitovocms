<?php

namespace core\entities\logisticsLostUpn\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\OrderEntry_LostUpn;
use core\relations\Shipment_LostUpn;
use core\relations\Storage_LostUpn;
use core\relations\Upn_LostUpn;
use core\relations\User_LostUpn;
use Yii;

/**
 * Сущность "Потерянный UPN".
 * Потерянный UPN представляет собой единицу продукции, которая числилась на складе, но по факту оказалась не найдена.
 *
 * Class LostUpn
 * @package core\entities\logisticsLostUpn\models
 */
class LostUpn extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var int Идентификатор UPN-а (логистического наряда)
     */
    public $upnId;
    /**
     * @var int Идентификатор склада
     */
    public $storageId;
    /**
     * @var string Литера - уникальное циферно-буквенное обозначение места хранения продукции на складе
     */
    public $literal;
    /**
     * @var int Идентификатор отгрузки
     */
    public $shipmentId;
    /**
     * @var int Идентификатор позиции из логистического заказа
     */
    public $orderEntryId;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    public function titles()
    {
        return [
            'id'                => Yii::t('app', 'ID'),
            'upnId'             => Yii::t('app', 'UPN'),
            'storageId'         => Yii::t('app', 'Склад'),
            'literal'           => Yii::t('app', 'Литера'),
            'shipmentId'        => Yii::t('app', 'Отгрузка'),
            'orderEntryId'      => Yii::t('app', 'Позиция'),
            'createdAt'         => Yii::t('app', 'Дата создания'),
            'updatedAt'         => Yii::t('app', 'Дата изменения'),
            'authorId'          => Yii::t('app', 'Автор'),
            'updaterId'         => Yii::t('app', 'Редактор'),
        ];
    }

    public function relations()
    {
        return [
            'upn'        => ['attribute' => 'upnId',        'class' => Upn_LostUpn::class,        'getterMethod' => 'upn',        'existsMethod' => 'upnExists'],
            'storage'    => ['attribute' => 'storageId',    'class' => Storage_LostUpn::class,    'getterMethod' => 'storage',    'existsMethod' => 'storageExists'],
            'shipment'   => ['attribute' => 'shipmentId',   'class' => Shipment_LostUpn::class,   'getterMethod' => 'shipment',   'existsMethod' => 'shipmentExists'],
            'orderEntry' => ['attribute' => 'orderEntryId', 'class' => OrderEntry_LostUpn::class, 'getterMethod' => 'orderEntry', 'existsMethod' => 'orderEntryExists'],
            'author'     => ['attribute' => 'authorId',     'class' => User_LostUpn::class,       'getterMethod' => 'author',     'existsMethod' => 'authorExists'],
            'updater'    => ['attribute' => 'updaterId',    'class' => User_LostUpn::class,       'getterMethod' => 'updater',    'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id', 'upnId', 'storageId', 'shipmentId', 'orderEntryId', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['literal'], 'string', 'max' => 255],
            [['upnId', 'storageId', 'literal'], 'required'],
            [['upnId', 'storageId', 'shipmentId', 'orderEntryId', 'authorId', 'updaterId'], 'validateExists'],
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId',  'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }
}