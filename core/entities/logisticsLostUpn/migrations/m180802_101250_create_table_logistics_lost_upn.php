<?php

use yii\db\Migration;

class m180802_101250_create_table_logistics_lost_upn extends Migration
{
    /**
     * Таблица "Потерянные UPN-ы"
     */
    public function up()
    {
        $this->createTable('{{%logistics_lost_upn}}', [
            'id'              => $this->primaryKey(),
            'upnId'           => $this->integer()->notNull()->comment('Идентификатор UPN-а (логистического наряда)'),
            'storageId'       => $this->integer()->notNull()->comment('Идентификатор склада'),
            'literal'         => $this->string()->notNull()->comment('Литера - уникальное циферно-буквенное обозначение места хранения продукции на складе'),
            'shipmentId'      => $this->integer()->comment('Идентификатор отгрузки'),
            'orderEntryId'    => $this->integer()->comment('Идентификатор позиции из логистического заказа'),
            'createdAt'       => $this->integer()->comment('Дата создания'),
            'updatedAt'       => $this->integer()->comment('Дата последнего изменения'),
            'authorId'        => $this->integer()->comment('Автор'),
            'updaterId'       => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%logistics_lost_upn}}');
    }
}
