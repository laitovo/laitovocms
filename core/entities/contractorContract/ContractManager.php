<?php

namespace core\entities\contractorContract;

use core\entities\base\BaseManager;
use core\entities\contractorContract\models\Contract;

/**
 * Класс предоставляет функционал для работы с сущностью "Договор с контрагентом"
 *
 * Class ContractManager
 * @package core\entities\contractorContract
 */
class ContractManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%contractor_contract}}';
        $this->_entityClass = Contract::class;
    }
}