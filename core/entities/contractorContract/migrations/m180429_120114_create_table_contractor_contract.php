<?php

use yii\db\Migration;

class m180429_120114_create_table_contractor_contract extends Migration
{
    /**
     * Таблица "Договоры с контрагентами"
     */
    public function up()
    {
        $this->createTable('{{%contractor_contract}}', [
            'id'           => $this->primaryKey(),
            'name'         => $this->string()->notNull()->comment('Наименование'),
            'contractorId' => $this->integer()->notNull()->comment('Идентификатор контрагента'),
            'createdAt'    => $this->integer()->comment('Дата создания'),
            'updatedAt'    => $this->integer()->comment('Дата последнего изменения'),
            'authorId'     => $this->integer()->comment('Автор'),
            'updaterId'    => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%contractor_contract}}');
    }
}
