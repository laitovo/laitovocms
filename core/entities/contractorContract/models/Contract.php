<?php

namespace core\entities\contractorContract\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\Contractor_Contract;
use core\relations\User_ContractorContract;
use Yii;

/**
 * Сущность "Договор с контрагентом".
 *
 * Class Contract
 * @package core\entities\contractorContract\models
 */
class Contract extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Наименование
     */
    public $name;
    /**
     * @var int Идентификатор контрагента
     */
    public $contractorId;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    public function titles()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'name'         => Yii::t('app', 'Наименование'),
            'contractorId' => Yii::t('app', 'Контрагент'),
            'createdAt'    => Yii::t('app', 'Дата создания'),
            'updatedAt'    => Yii::t('app', 'Дата изменения'),
            'authorId'     => Yii::t('app', 'Автор'),
            'updaterId'    => Yii::t('app', 'Редактор'),
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }

    public function relations()
    {
        return [
            'contractor' => ['attribute' => 'contractorId', 'class' => Contractor_Contract::class, 'getterMethod' => 'contractor', 'existsMethod' => 'contractorExists'],
            'author'     => ['attribute' => 'authorId', 'class' => User_ContractorContract::class, 'getterMethod' => 'author', 'existsMethod' => 'authorExists'],
            'updater'    => ['attribute' => 'updaterId', 'class' => User_ContractorContract::class, 'getterMethod' => 'updater', 'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['contractorId', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['name', 'contractorId'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['contractorId', 'authorId', 'updaterId'], 'validateExists'],
        ];
    }
}