<?php

namespace core\entities\storageAnalytics\models;

use core\entities\base\BaseEntity;
use Yii;

class StorageAnalytics extends BaseEntity
{
    public $id;
    public $car;
    public $type;
    public $analogGroupId;
    public $article;
    public $realizationBefore;
    public $realizationLast;
    public $increasePlan;
    public $realizationPlan;
    public $productionSpeed;
    public $realizationFrequency;
    public $balancePlan;
    public $correctionCoefficient;

    public function titles()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'car' => Yii::t('app', 'Автомобиль'),
            'type' => Yii::t('app', 'Наим. Груп'),
            'analogGroupId' => Yii::t('app', 'Группа аналогов'),
            'article' => Yii::t('app', 'Артикул'),
            'realizationBefore' => Yii::t('app', 'РПП'),
            'realizationLast' => Yii::t('app', 'РП'),
            'increasePlan' => Yii::t('app', 'ПНП'),
            'realizationPlan' => Yii::t('app', 'ПР'),
            'productionSpeed' => Yii::t('app', 'КСП'),
            'realizationFrequency' => Yii::t('app', 'ЧР'),
            'balancePlan' => Yii::t('app', 'ОП'),
            'correctionCoefficient' => Yii::t('app', 'КК'),
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['analogGroupId', 'realizationBefore', 'realizationLast', 'realizationPlan', 'correctionCoefficient', 'realizationFrequency', 'balancePlan'], 'integer'],
            [['increasePlan', 'productionSpeed'], 'number'],
            [['car', 'type', 'article'], 'string', 'max' => 255],
        ];
    }
}