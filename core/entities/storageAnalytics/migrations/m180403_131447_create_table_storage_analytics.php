<?php

use yii\db\Migration;

class m180403_131447_create_table_storage_analytics extends Migration
{
    /**
     * Таблица "Аналитика склада"
     */
    public function up()
    {
        $this->createTable('{{%storage_analytics}}', [
            'id' => $this->primaryKey(),
            'car'  => $this->string()->comment('Наименование автомобиля, для которого предназначается данный продукт'),
            'type'  => $this->string()->comment('Тип продукта'),
            'analogGroupId'  => $this->integer()->comment('Группа аналогов'),
            'article'  => $this->string()->comment('Уникальный артикул продукта'),
            'realizationBefore'  => $this->integer()->comment('Количество единиц продукта, проданных за предпоследний месяц'),
            'realizationLast'  => $this->integer()->comment('Количество единиц продукта, проданных за последний месяц'),
            'increasePlan'  => $this->double()->comment('Количество единиц продукта, которое планируется продать в следующем месяце'),
            'realizationPlan'  => $this->integer()->comment('Ожидаемый относительный прирост реализации в следующем месяце'),
            'productionSpeed'  => $this->double()->comment('Коэффициент скорости производства'),
            'realizationFrequency'  => $this->integer()->comment('Частота реализации продукта (сколько единиц продано за последние полгода)'),
            'balancePlan'  => $this->integer()->comment('Плановые остатки на следующий месяц (сколько продукта надо держать на складе)'),
            'correctionCoefficient'  => $this->integer()->comment('Корректирующий коэффициент для плановых остатков'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%storage_analytics}}');
    }
}
