<?php

namespace core\entities\storageAnalytics;

use core\entities\base\BaseManager;
use core\entities\storageAnalytics\models\StorageAnalytics;

class StorageAnalyticsManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%storage_analytics}}';
        $this->_entityClass = StorageAnalytics::class;
    }
}