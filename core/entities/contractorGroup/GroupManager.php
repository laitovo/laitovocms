<?php

namespace core\entities\contractorGroup;

use core\entities\base\BaseManager;
use core\entities\contractorGroup\models\Group;

/**
 * Класс предоставляет функционал для работы с сущностью "Группа контрагентов"
 *
 * Class GroupManager
 * @package core\entities\contractorGroup
 */
class GroupManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%contractor_group}}';
        $this->_entityClass = Group::class;
    }
}