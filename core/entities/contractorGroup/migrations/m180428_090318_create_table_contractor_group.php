<?php

use yii\db\Migration;

class m180428_090318_create_table_contractor_group extends Migration
{
    /**
     * Таблица "Группы контрагентов"
     */
    public function up()
    {
        $this->createTable('{{%contractor_group}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull()->unique()->comment('Наименование группы'),
            'parentGroupId' => $this->integer()->comment('Идентификатор родительской группы контрагентов'),
            'createdAt'     => $this->integer()->comment('Дата создания'),
            'updatedAt'     => $this->integer()->comment('Дата последнего изменения'),
            'authorId'      => $this->integer()->comment('Автор'),
            'updaterId'     => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%contractor_group}}');
    }
}
