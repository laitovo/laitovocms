<?php

namespace core\entities\contractorGroup\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\ContractorGroup_Contractor;
use core\relations\ContractorGroup_ContractorGroup;
use core\relations\User_ContractorGroup;
use Yii;

/**
 * Сущность "Группа контрагентов".
 * Группы могут быть примерно такими: "Поставщики", "Покупатели", "Покупатели de", "Покупатели rus", ...
 * По идее, каждый контрагент может принадлежать только одной группе.
 * Группы могут быть вложенными.
 *
 * Class Group
 * @package core\entities\contractorGroup\models
 */
class Group extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Наименование группы
     */
    public $name;
    /**
     * @var int Идентификатор родительской группы контрагентов
     */
    public $parentGroupId;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    public function titles()
    {
        return [
            'id'            => Yii::t('app', 'ID'),
            'name'          => Yii::t('app', 'Наименование'),
            'parentGroupId' => Yii::t('app', 'Родительская группа'),
            'createdAt'     => Yii::t('app', 'Дата создания'),
            'updatedAt'     => Yii::t('app', 'Дата изменения'),
            'authorId'      => Yii::t('app', 'Автор'),
            'updaterId'     => Yii::t('app', 'Редактор'),
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }

    public function relations()
    {
        return [
            'contractors' => [
                'attribute'    => 'id',
                'class'        => ContractorGroup_Contractor::class,
                'getterMethod' => 'contractors',
            ],
            'childs'      => [
                'attribute'    => 'id',
                'class'        => ContractorGroup_ContractorGroup::class,
                'getterMethod' => 'childs',
            ],
            'allChilds'   => [
                'attribute'    => 'id',
                'class'        => ContractorGroup_ContractorGroup::class,
                'getterMethod' => 'allChilds',
            ],
            'parent'      => [
                'attribute'    => 'parentGroupId',
                'class'        => ContractorGroup_ContractorGroup::class,
                'getterMethod' => 'parent',
                'existsMethod' => 'parentExists',
            ],
            'topParent'   => [
                'attribute'    => 'parentGroupId',
                'class'        => ContractorGroup_ContractorGroup::class,
                'getterMethod' => 'topParent',
            ],
            'author'      => [
                'attribute'    => 'authorId',
                'class'        => User_ContractorGroup::class,
                'getterMethod' => 'author',
                'existsMethod' => 'authorExists',
            ],
            'updater'     => [
                'attribute'    => 'updaterId',
                'class'        => User_ContractorGroup::class,
                'getterMethod' => 'updater',
                'existsMethod' => 'updaterExists',
            ],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['parentGroupId', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['name'], 'validateUnique'],
            [['parentGroupId'], 'validateExists', 'params' => ['relation' => 'parent']],
            [['authorId', 'updaterId'], 'validateExists'],
        ];
    }

    public function beforeValidate()
    {
        if ($this->parentGroupId) {
            if ($this->parentGroupId == $this->id) {
                $this->addError('parentGroupId', 'Группа не может быть родительской для самой себя.');
                return false;
            }
            $manager = \Yii::$container->get('core\entities\contractorGroup\GroupManager');
            foreach ($manager->getRelatedAllChilds($this) as $childGroup) {
                if ($this->parentGroupId == $manager->getId($childGroup)) {
                    $this->addError('parentGroupId', 'Дочерняя группа не может быть родительской.');
                    return false;
                }
            }
        }

        return parent::beforeValidate();
    }
}