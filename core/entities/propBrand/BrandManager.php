<?php

namespace core\entities\propBrand;

use core\entities\base\BaseManager;
use core\entities\propBrand\models\Brand;
use core\entities\propBrand\models\services\QueryFormatter;

/**
 * Класс предоставляет функционал для работы с сущностью "Бренд"
 *
 * Class BrandManager
 * @package core\entities\propBrand
 */
class BrandManager extends BaseManager
{
    /**
     * @var QueryFormatter Надстройка над базовым query для форматирования результирующей выборки (выбор полей, индексация, сортировка и т.д.)
     */
    private $_queryFormatter;

    protected function _init()
    {
        $this->_table = '{{%prop_brand}}';
        $this->_entityClass = Brand::class;
    }

    protected function _initServices()
    {
        $this->_queryFormatter = new QueryFormatter($this->_query);
    }

    /**
     * Список наименований всех брендов.
     *
     * На выходе массив вида 'id' => 'name'.
     * Данная функция может быть полезна для заполнения выпадающих списков.
     *
     * @return array
     */
    public function getNameListIndexById()
    {
        return $this->_queryFormatter->getNameListIndexById();
    }
}