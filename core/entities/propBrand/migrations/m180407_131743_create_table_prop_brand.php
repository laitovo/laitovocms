<?php

use yii\db\Migration;

class m180407_131743_create_table_prop_brand extends Migration
{
    /**
     * Таблица "Бренды".
     * Префикс prop_... означает, что данная таблица является справочником (описанием возможных значений некого свойства).
     */
    public function up()
    {
        $this->createTable('{{%prop_brand}}', [
            'id' => $this->primaryKey(),
            'name'  => $this->string()->notNull()->unique()->comment('Наименование бренда'),
            'description'  => $this->string(1000)->comment('Описание бренда'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%prop_brand}}');
    }
}
