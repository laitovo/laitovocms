<?php

use yii\db\Migration;

class m180412_072340_add_column_instruction_alias_to_prop_brand extends Migration
{
    public function up()
    {
        $this->addColumn('{{%prop_brand}}', 'instructionAlias', $this->string()->comment('Наименование, используемое для распечатки инструкций'));
    }

    public function down()
    {
        $this->dropColumn('{{%prop_brand}}','instructionAlias');
    }
}
