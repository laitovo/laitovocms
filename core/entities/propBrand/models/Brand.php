<?php

namespace core\entities\propBrand\models;

use core\entities\base\BaseEntity;
use Yii;

/**
 * Сущность "Бренд".
 * Бренд представляет собой некую условную группу (категорию) продукции.
 *
 * Class Brand
 * @package core\entities\propBrand\models
 */
class Brand extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Наименование
     */
    public $name;
    /**
     * @var string Наименование, используемое для распечатки инструкций
     */
    public $instructionAlias;
    /**
     * @var string Описание бренда
     */
    public $description;

    public function titles()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Наименование'),
            'instructionAlias' => Yii::t('app', 'В инструкциях'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'instructionAlias'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['name'], 'validateUnique', 'message' => 'Это поле должно быть уникальным'],
            [['name', 'instructionAlias'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
        ];
    }
}