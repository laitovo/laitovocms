<?php

namespace core\entities\contractorContact\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\Contractor_Contact;
use core\relations\User_ContractorContact;
use Yii;

/**
 * Сущность "Контактное лицо контрагента".
 *
 * Class Contact
 * @package core\entities\contractorContact\models
 */
class Contact extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string ФИО
     */
    public $fio;
    /**
     * @var string Должность
     */
    public $post;
    /**
     * @var string Телефон
     */
    public $phone;
    /**
     * @var string Электронная почта
     */
    public $email;
    /**
     * @var string Другая контактная информация
     */
    public $otherContacts;
    /**
     * @var string Комментарий
     */
    public $comment;
    /**
     * @var int Идентификатор контрагента
     */
    public $contractorId;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    public function titles()
    {
        return [
            'id'            => Yii::t('app', 'ID'),
            'fio'           => Yii::t('app', 'ФИО'),
            'post'          => Yii::t('app', 'Должность'),
            'phone'         => Yii::t('app', 'Телефон'),
            'email'         => Yii::t('app', 'Электронная почта'),
            'otherContacts' => Yii::t('app', 'Другая контактная информация'),
            'comment'       => Yii::t('app', 'Комментарий'),
            'contractorId'  => Yii::t('app', 'Контрагент'),
            'createdAt'     => Yii::t('app', 'Дата создания'),
            'updatedAt'     => Yii::t('app', 'Дата изменения'),
            'authorId'      => Yii::t('app', 'Автор'),
            'updaterId'     => Yii::t('app', 'Редактор'),
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }

    public function relations()
    {
        return [
            'contractor' => ['attribute' => 'contractorId', 'class' => Contractor_Contact::class, 'getterMethod' => 'contractor', 'existsMethod' => 'contractorExists'],
            'author'     => ['attribute' => 'authorId', 'class' => User_ContractorContact::class, 'getterMethod' => 'author', 'existsMethod' => 'authorExists'],
            'updater'    => ['attribute' => 'updaterId', 'class' => User_ContractorContact::class, 'getterMethod' => 'updater', 'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['fio', 'post', 'phone', 'email'], 'string', 'max' => 255],
            [['otherContacts', 'comment'], 'string', 'max' => 1000],
            [['contractorId', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['fio', 'contractorId'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['email'], 'email'],
            [['contractorId', 'authorId', 'updaterId'], 'validateExists'],
        ];
    }

    public function beforeValidate()
    {
        if (!$this->phone && !$this->email) {
            $this->addError('phone', 'Необходимо указать телефон или электронную почту');
            $this->addError('email', 'Необходимо указать телефон или электронную почту');

            return false;
        }

        return parent::beforeValidate();
    }
}