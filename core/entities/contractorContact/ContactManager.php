<?php

namespace core\entities\contractorContact;

use core\entities\base\BaseManager;
use core\entities\contractorContact\models\Contact;

/**
 * Класс предоставляет функционал для работы с сущностью "Контактное лицо контрагента"
 *
 * Class ContactManager
 * @package core\entities\contractorContact
 */
class ContactManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%contractor_contact}}';
        $this->_entityClass = Contact::class;
    }
}