<?php

use yii\db\Migration;

class m180429_132131_create_table_contractor_contact extends Migration
{
    /**
     * Таблица "Контактные лица контрагентов"
     */
    public function up()
    {
        $this->createTable('{{%contractor_contact}}', [
            'id'            => $this->primaryKey(),
            'fio'           => $this->string()->notNull()->comment('ФИО'),
            'post'          => $this->string()->comment('Должность'),
            'phone'         => $this->string()->comment('Телефон'),
            'email'         => $this->string()->comment('Электронная почта'),
            'otherContacts' => $this->string(1000)->comment('Другая контактная информация'),
            'comment'       => $this->string(1000)->comment('Комментарий'),
            'contractorId'  => $this->integer()->notNull()->comment('Идентификатор контрагента'),
            'createdAt'     => $this->integer()->comment('Дата создания'),
            'updatedAt'     => $this->integer()->comment('Дата последнего изменения'),
            'authorId'      => $this->integer()->comment('Автор'),
            'updaterId'     => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%contractor_contact}}');
    }
}
