<?php

namespace core\entities\logisticsRealizationLog\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\Upn_RealizationLog;
use core\relations\OrderEntry_RealizationLog;
use core\relations\User_RealizationLog;
use Yii;

/**
 * Сущность "Лог реализации".
 * Описывает, какой UPN ушёл под определённый заказ. UPN может уходить на несколько заказов, в случае если клиенты возвращают товар.
 * При этом UPN заново приходуется, и информация о нём перезаписывается. Поэтому нужны логи, чтобы хранить историю.
 * Решение выделить под это отдельную таблицу (вместо какого-нибудь json-свойства) принято из соображений оптимизации
 * (чтобы по логам можно было быстро что-то найти без дополнительных парсингов).
 *
 * Class RealizationLog
 * @package core\entities\logisticsRealizationLog\models
 */
class RealizationLog extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var int Идентификатор UPN-а (логистического наряда)
     */
    public $upnId;
    /**
     * @var int Идентификатор позиции из логистического заказа
     */
    public $orderEntryId;
    /**
     * @var string Литера - уникальное циферно-буквенное обозначение места хранения продукции на складе
     */
    public $literal;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    public function titles()
    {
        return [
            'id'                   => Yii::t('app', 'ID'),
            'upnId'                => Yii::t('app', 'UPN'),
            'orderEntryId'         => Yii::t('app', 'Позиция из заказа'),
            'literal'              => Yii::t('app', 'Литера'),
            'createdAt'            => Yii::t('app', 'Дата создания'),
            'updatedAt'            => Yii::t('app', 'Дата изменения'),
            'authorId'             => Yii::t('app', 'Автор'),
            'updaterId'            => Yii::t('app', 'Редактор'),
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId',  'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }

    public function relations()
    {
        return [
            'upn'        => ['attribute' => 'upnId',        'class' => Upn_RealizationLog::class,        'getterMethod' => 'upn',        'existsMethod' => 'upnExists'],
            'orderEntry' => ['attribute' => 'orderEntryId', 'class' => OrderEntry_RealizationLog::class, 'getterMethod' => 'orderEntry', 'existsMethod' => 'orderEntryExists'],
            'author'     => ['attribute' => 'authorId',     'class' => User_RealizationLog::class,       'getterMethod' => 'author',     'existsMethod' => 'authorExists'],
            'updater'    => ['attribute' => 'updaterId',    'class' => User_RealizationLog::class,       'getterMethod' => 'updater',    'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['literal'], 'string', 'max' => 255],
            [['upnId', 'orderEntryId', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['upnId', 'orderEntryId', 'literal'], 'required'],
            [['upnId', 'orderEntryId', 'authorId', 'updaterId'], 'validateExists'],
            [['orderEntryId'], 'validateUnique'],
        ];
    }
}