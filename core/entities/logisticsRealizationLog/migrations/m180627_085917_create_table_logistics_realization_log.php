<?php

use yii\db\Migration;

class m180627_085917_create_table_logistics_realization_log extends Migration
{
    /**
     * Таблица "Логи реализации"
     */
    public function up()
    {
        $this->createTable('{{%logistics_realization_log}}', [
            'id'              => $this->primaryKey(),
            'upnId'           => $this->integer()->notNull()->comment('Идентификатор UPN-а (логистического наряда)'),
            'orderEntryId'    => $this->integer()->notNull()->unique()->comment('Идентификатор позиции из логистического заказа'),
            'literal'         => $this->string()->notNull()->comment('Литера - уникальное циферно-буквенное обозначение места хранения продукции на складе'),
            'createdAt'       => $this->integer()->comment('Дата создания'),
            'updatedAt'       => $this->integer()->comment('Дата последнего изменения'),
            'authorId'        => $this->integer()->comment('Автор'),
            'updaterId'       => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%logistics_realization_log}}');
    }
}
