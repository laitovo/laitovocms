<?php

namespace core\entities\logisticsRealizationLog;

use core\entities\base\BaseManager;
use core\entities\logisticsRealizationLog\models\RealizationLog;

/**
 * Класс предоставляет функционал для работы с сущностью "Лог реализации"
 *
 * Class RealizationLogManager
 * @package core\entities\logisticsRealizationLog
 */
class RealizationLogManager extends BaseManager
{
    protected function _init()
    {
        $this->_table       = '{{%logistics_realization_log}}';
        $this->_entityClass = RealizationLog::class;
    }
}