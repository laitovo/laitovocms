<?php

use yii\db\Migration;

class m180429_100750_create_table_contractor_bank_account extends Migration
{
    /**
     * Таблица "Банковские счета контрагентов"
     */
    public function up()
    {
        $this->createTable('{{%contractor_bank_account}}', [
            'id'           => $this->primaryKey(),
            'account'      => $this->string()->notNull()->unique()->comment('Номер счёта'),
            'contractorId' => $this->integer()->notNull()->comment('Идентификатор контрагента'),
            'createdAt'    => $this->integer()->comment('Дата создания'),
            'updatedAt'    => $this->integer()->comment('Дата последнего изменения'),
            'authorId'     => $this->integer()->comment('Автор'),
            'updaterId'    => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%contractor_bank_account}}');
    }
}
