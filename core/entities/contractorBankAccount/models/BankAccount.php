<?php

namespace core\entities\contractorBankAccount\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\Contractor_BankAccount;
use core\relations\User_ContractorBankAccount;
use Yii;

/**
 * Сущность "Банковский счёт контрагента".
 *
 * Class BankAccount
 * @package core\entities\contractorBankAccount\models
 */
class BankAccount extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Счёт
     */
    public $account;
    /**
     * @var int Идентификатор контрагента
     */
    public $contractorId;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    public function titles()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'account'      => Yii::t('app', 'Счет'),
            'contractorId' => Yii::t('app', 'Контрагент'),
            'createdAt'    => Yii::t('app', 'Дата создания'),
            'updatedAt'    => Yii::t('app', 'Дата изменения'),
            'authorId'     => Yii::t('app', 'Автор'),
            'updaterId'    => Yii::t('app', 'Редактор'),
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }

    public function relations()
    {
        return [
            'contractor' => ['attribute' => 'contractorId', 'class' => Contractor_BankAccount::class, 'getterMethod' => 'contractor', 'existsMethod' => 'contractorExists'],
            'author'     => ['attribute' => 'authorId', 'class' => User_ContractorBankAccount::class, 'getterMethod' => 'author', 'existsMethod' => 'authorExists'],
            'updater'    => ['attribute' => 'updaterId', 'class' => User_ContractorBankAccount::class, 'getterMethod' => 'updater', 'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['account'], 'string', 'max' => 255],
            [['contractorId', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['account', 'contractorId'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['account'], 'validateUnique'],
            [['contractorId', 'authorId', 'updaterId'], 'validateExists'],
        ];
    }
}