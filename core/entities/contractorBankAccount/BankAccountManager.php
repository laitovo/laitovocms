<?php

namespace core\entities\contractorBankAccount;

use core\entities\base\BaseManager;
use core\entities\contractorBankAccount\models\BankAccount;

/**
 * Класс предоставляет функционал для работы с сущностью "Банковский счёт контрагента"
 *
 * Class BankAccountManager
 * @package core\entities\contractorBankAccount
 */
class BankAccountManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%contractor_bank_account}}';
        $this->_entityClass = BankAccount::class;
    }
}