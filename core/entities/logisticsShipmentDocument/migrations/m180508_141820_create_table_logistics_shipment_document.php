<?php

use yii\db\Migration;

class m180508_141820_create_table_logistics_shipment_document extends Migration
{
    public function up()
    {
        $this->createTable('logistics_shipment_document', [
            'id'         => $this->primaryKey(),
            'shipmentId' => $this->integer()->notNull()->comment('Идентификатор отгрузки'),
            'fileKey'    => $this->string()->notNull()->comment('Ключ для доступа к файлу'),
            'createdAt'  => $this->integer()->comment('Дата создания'),
            'updatedAt'  => $this->integer()->comment('Дата последнего изменения'),
            'authorId'   => $this->integer()->comment('Автор'),
            'updaterId'  => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('logistics_shipment_document');
    }
}
