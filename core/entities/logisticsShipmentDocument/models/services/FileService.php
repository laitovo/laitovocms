<?php

namespace core\entities\logisticsShipmentDocument\models\services;

use core\entities\base\BaseEntity;
use Yii;

/**
 * Сервис отвечает за работу с физическими файлами документов на сервере
 *
 * Class FileService
 * @package core\entities\logisticsShipmentDocument\models\services
 */
class FileService
{
    private $_getSet;
    private $_repository;
    private $_fileManager;

    public function __construct($getSet, $repository)
    {
        $this->_getSet = $getSet;
        $this->_repository = $repository;
        $this->_fileManager = Yii::$app->fileManager;
    }

    /**
     * Сохранение документа + физическая загрузка файла на сервер
     *
     * @param BaseEntity $entity
     * @param string $formAttributeName Поле формы, из которого брать файл
     * @return bool
     */
    public function saveWithFile($entity, $formAttributeName)
    {
        $this->_getSet->instanceOf($entity);
        if (!($entity->fileKey = $this->_fileManager->uploadTeamFile($formAttributeName, 'shipment/documents'))) {
            return false;
        }

        return $this->_repository->save($entity);
    }

    /**
     * Удаление документа + физическое удаление файла с сервера
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function deleteWithFile($entity)
    {
        $this->_getSet->instanceOf($entity);
        if (!$this->_fileManager->deleteFile($entity->fileKey)) {
            return false;
        }

        return $this->_repository->delete($entity);
    }
}