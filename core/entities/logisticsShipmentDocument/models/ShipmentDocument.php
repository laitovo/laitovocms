<?php

namespace core\entities\logisticsShipmentDocument\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\Shipment_ShipmentDocument;
use core\relations\User_ShipmentDocument;
use Yii;

/**
 * Сущность "Документ отгрузки".
 * Описывает документы, без которых продукция не может быть отгружена со склада.
 * В основном это документы транспортной компании, которые нужны для отправки заказа.
 *
 * Class ShipmentDocument
 * @package core\entities\logisticsShipmentDocument\models
 */
class ShipmentDocument extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var int Идентификатор отгрузки
     */
    public $shipmentId;
    /**
     * @var string Ключ для доступа к файлу
     */
    public $fileKey;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    public function titles()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'shipmentId' => Yii::t('app', 'Отгрузка'),
            'fileKey'    => Yii::t('app', 'Ключ к файлу'),
            'createdAt'  => Yii::t('app', 'Дата создания'),
            'updatedAt'  => Yii::t('app', 'Дата изменения'),
            'authorId'   => Yii::t('app', 'Автор'),
            'updaterId'  => Yii::t('app', 'Редактор'),
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }

    public function relations()
    {
        return [
            'shipment' => ['attribute' => 'shipmentId', 'class' => Shipment_ShipmentDocument::class, 'getterMethod' => 'shipment', 'existsMethod' => 'shipmentExists'],
            'author'   => ['attribute' => 'authorId', 'class' => User_ShipmentDocument::class, 'getterMethod' => 'author', 'existsMethod' => 'authorExists'],
            'updater'  => ['attribute' => 'updaterId', 'class' => User_ShipmentDocument::class, 'getterMethod' => 'updater', 'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['fileKey'], 'string', 'max' => 255],
            [['shipmentId', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['shipmentId', 'fileKey'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['shipmentId', 'authorId', 'updaterId'], 'validateExists'],
        ];
    }

    public function beforeValidate()
    {
        if (!Yii::$app->fileManager->fileExists($this->fileKey)) {
            $this->addError('fileKey', 'Не удалось найти файл по ключу.');
            return false;
        }

        return parent::beforeValidate();
    }
}