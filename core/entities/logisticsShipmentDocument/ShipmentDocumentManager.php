<?php

namespace core\entities\logisticsShipmentDocument;

use core\entities\base\BaseEntity;
use core\entities\base\BaseManager;
use core\entities\logisticsShipmentDocument\models\services\FileService;
use core\entities\logisticsShipmentDocument\models\ShipmentDocument;

/**
 * Класс предоставляет функционал для работы с сущностью "Документ отгрузки"
 *
 * Class ShipmentDocumentManager
 * @package core\entities\logisticsShipmentDocument
 */
class ShipmentDocumentManager extends BaseManager
{
    private $_fileService;

    protected function _init()
    {
        $this->_table = '{{%logistics_shipment_document}}';
        $this->_entityClass = ShipmentDocument::class;
    }

    protected function _initServices()
    {
        $this->_fileService = new FileService($this->_getSet, $this->_repository);
    }

    /**
     * Сохранение документа + физическая загрузка файла на сервер
     *
     * @param BaseEntity $entity
     * @param string $formAttributeName Поле формы, из которого брать файл
     * @return bool
     */
    public function saveWithFile($entity, $formAttributeName)
    {
        return $this->_fileService->saveWithFile($entity, $formAttributeName);
    }

    /**
     * Удаление документа + физическое удаление файла с сервера
     *
     * @param BaseEntity $entity
     * @return bool
     */
    public function deleteWithFile($entity)
    {
        return $this->_fileService->deleteWithFile($entity);
    }
}