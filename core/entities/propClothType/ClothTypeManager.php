<?php

namespace core\entities\propClothType;

use core\entities\base\BaseManager;
use core\entities\propClothType\models\ClothType;

/**
 * Класс предоставляет функционал для работы с сущностью "Вид ткани"
 *
 * Class ClothTypeManager
 * @package core\entities\propClothType
 */
class ClothTypeManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%prop_cloth_type}}';
        $this->_entityClass = ClothType::class;
    }
}