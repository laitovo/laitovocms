<?php

use yii\db\Migration;

class m180410_061046_create_table_prop_cloth_type extends Migration
{
    /**
     * Таблица "Виды тканей".
     * Префикс prop_... означает, что данная таблица является справочником (описанием возможных значений некого свойства).
     */
    public function up()
    {
        $this->createTable('{{%prop_cloth_type}}', [
            'id' => $this->primaryKey(),
            'name'  => $this->string()->notNull()->unique()->comment('Наименование вида ткани'),
            'alias' => $this->double()->notNull()->unique()->comment('Номер вида ткани, используемый на производстве'),
            'articlePart' => $this->string()->notNull()->unique()->comment('Идентификатор вида ткани, используемый в артикулах'),
            'description'  => $this->string(1000)->comment('Описание вида ткани'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%prop_cloth_type}}');
    }
}
