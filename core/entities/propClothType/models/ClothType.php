<?php

namespace core\entities\propClothType\models;

use core\entities\base\BaseEntity;
use Yii;

/**
 * Сущность "Вид ткани".
 * Описывает наименование, состав и другие хар-ки ткани.
 *
 * Class ClothType
 * @package core\entities\propClothType\models
 */
class ClothType extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Наименование
     */
    public $name;
    /**
     * @var double Идентификатор, используемый на проиводстве,
     */
    public $alias;
    /**
     * @var string Идентификатор, используемый в артикулах продуктов
     */
    public $articlePart;
    /**
     * @var string Описание ткани
     */
    public $description;

    public function titles()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'name'        => Yii::t('app', 'Наименование'),
            'alias'       => Yii::t('app', 'Номер'),
            'articlePart' => Yii::t('app', 'В артикуле'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'alias', 'articlePart'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['name', 'alias', 'articlePart'], 'validateUnique', 'message' => 'Это поле должно быть уникальным'],
            [['name', 'articlePart'], 'string', 'max' => 255],
            [['alias'], 'double'],
            [['description'], 'string', 'max' => 1000],
        ];
    }
}
