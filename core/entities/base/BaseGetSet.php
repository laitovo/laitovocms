<?php

namespace core\entities\base;

use core\helpers\StringHelper;
use Yii;
use Exception;
use ReflectionClass;
use ReflectionProperty;

/**
 * Базовый класс для работы со свойствами сущности
 *
 * Class BaseGetSet
 * @package core\entities\base
 */
class BaseGetSet
{
    protected $_entityClass;

    public function __construct($entityClass)
    {
        $this->_entityClass = $entityClass;
    }

    public function instanceOf($entity) {
        if (!($entity instanceof $this->_entityClass)) throw new Exception(Yii::t('app','При обращении к свойству передан объект неверного типа'));
    }

    public function toArray($entity)
    {
        $this->instanceOf($entity);

        return $entity->toArray();
    }

    public function setAttributes($entity, $attributes, $validate = true)
    {
        $this->instanceOf($entity);

        return $entity->setAttributes($attributes, $validate);
    }

    public function getAttributes()
    {
        $class = new ReflectionClass($this->_entityClass);
        $names = [];
        foreach ($class->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }

        return $names;
    }

    public function getAttributesWithLabels()
    {
        $result = [];
        $labels = $this->_entityClass::titles();
        foreach ($labels as $key => $value) {
            $result[$key] = ['attribute' => $key, 'label' => $value];
        }

        return $result;
    }

    public function getPrimaryKey()
    {
        return 'id';
    }

    public function fillByForm($entity, $formData)
    {
        $this->instanceOf($entity);

        return $entity->load($formData);
    }

    public function validate($entity)
    {
        $this->instanceOf($entity);

        return $entity->validate();
    }

    /**
     * Автоматические геттеры и сеттеры
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call (string $name, array $arguments) {
        if (preg_match('/^get/',$name) === 1 && preg_match('/Variants$/',$name) === 1) {
            return $this->_handleGetVariants($name, $arguments);
        } elseif (preg_match('/^get/',$name) === 1 && preg_match('/VariantsWithLabels$/',$name) === 1) {
            return $this->_handleGetVariantsWithLabels($name, $arguments);
        } elseif (preg_match('/^get/',$name) === 1 && preg_match('/Label$/',$name) === 1) {
            return $this->_handleGetLabel($name, $arguments);
        } elseif (preg_match('/^get/',$name) === 1 && preg_match('/Priority$/',$name) === 1) {
            return $this->_handleGetPriority($name, $arguments);
        }  elseif (preg_match('/^get/',$name) === 1) {
            return $this->_handleGetter($name, $arguments);
        } elseif (preg_match('/^set/',$name) === 1) {
            $this->_handleSetter($name, $arguments);
        } else {
            throw new Exception('Не найдена функция ' . $name);
        }
    }

    /**
     * Обработчик для функций вида get[ИмяСвойства]().
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws Exception
     */
    private function _handleGetter($name, $arguments)
    {
        $entity = $arguments[0] ?? null;
        $this->instanceOf($entity);
        $attributeInCamelCase = StringHelper::toCamelCase(substr($name, 3));
        $attributeInSnakeCase = StringHelper::toSnakeCase($attributeInCamelCase);
        if (!property_exists($entity, $attribute = $attributeInCamelCase) && !property_exists($entity, $attribute = $attributeInSnakeCase)) {
            throw new Exception('Ошибка при обработке функции ' . $name . '. Не найдено свойство ' . $attributeInCamelCase);
        }

        return $entity->$attribute;
    }

    /**
     * Обработчик для функций вида set[ИмяСвойства]().
     *
     * @param $name
     * @param $arguments
     * @throws Exception
     */
    private function _handleSetter($name, $arguments)
    {
        $entity = $arguments[0] ?? null;
        $this->instanceOf($entity);
        $attributeInCamelCase = StringHelper::toCamelCase(substr($name, 3));
        $attributeInSnakeCase = StringHelper::toSnakeCase($attributeInCamelCase);
        if (!property_exists($entity, $attribute = $attributeInCamelCase) && !property_exists($entity, $attribute = $attributeInSnakeCase)) {
            throw new Exception('Ошибка при обработке функции ' . $name . '. Не найдено свойство ' . $attributeInCamelCase);
        }
        if (!isset($arguments[1])) {
            throw new Exception('Некорректный вызов функции ' . $name . '. Не передано устанавливаемое значение.');
        }
        $value = $arguments[1];
        $entity->$attribute = $value;
    }

    /**
     * Обработчик для функций вида get[ИмяСвойства]Variants().
     * Возвращает список возможных значений свойства.
     *
     * @param $name
     * @param $arguments
     * @return array
     * @throws Exception
     */
    private function _handleGetVariants($name, $arguments)
    {
        $attributeInCamelCase = StringHelper::toCamelCase(preg_replace(['/^get/', '/Variants$/'], '', $name));
        $attributeInSnakeCase = StringHelper::toSnakeCase($attributeInCamelCase);
        $possibleValues = $this->_entityClass::possibleValues();
        if (!array_key_exists($attribute = $attributeInCamelCase, $possibleValues) && !array_key_exists($attribute = $attributeInSnakeCase, $possibleValues)) {
            throw new Exception('Ошибка при обработке функции ' . $name . '. Не описаны возможные значения для свойства ' . $attributeInCamelCase . '.');
        }

        return array_keys($possibleValues[$attribute]);
    }

    /**
     * Обработчик для функций вида get[ИмяСвойства]VariantsWithLabels().
     * Возвращает список возможных значений свойства в формате "Значение свойства" => "Читабельный заголовок".
     * Такой формат подойдёт для выпадающих списков.
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws Exception
     */
    private function _handleGetVariantsWithLabels($name, $arguments)
    {
        $attributeInCamelCase = StringHelper::toCamelCase(preg_replace(['/^get/', '/VariantsWithLabels$/'], '', $name));
        $attributeInSnakeCase = StringHelper::toSnakeCase($attributeInCamelCase);
        $possibleValues = $this->_entityClass::possibleValues();
        if (!array_key_exists($attribute = $attributeInCamelCase, $possibleValues) && !array_key_exists($attribute = $attributeInSnakeCase, $possibleValues)) {
            throw new Exception('Ошибка при обработке функции ' . $name . '. Не описаны возможные значения для свойства ' . $attributeInCamelCase . '.');
        }

        return $possibleValues[$attribute];
    }

    /**
     * Обработчик для функций вида get[ИмяСвойства]Label($entity).
     * Возвращает читабельный заголовок для некого свойства сущности.
     *
     * @param $name
     * @param $arguments
     * @return string|null
     * @throws Exception
     */
    private function _handleGetLabel($name, $arguments)
    {
        $entity = $arguments[0] ?? null;
        $this->instanceOf($entity);
        $attributeInCamelCase = StringHelper::toCamelCase(preg_replace(['/^get/', '/Label$/'], '', $name));
        $attributeInSnakeCase = StringHelper::toSnakeCase($attributeInCamelCase);
        if (!property_exists($entity, $attribute = $attributeInCamelCase) && !property_exists($entity, $attribute = $attributeInSnakeCase)) {
            throw new Exception('Ошибка при обработке функции ' . $name . '. Не найдено свойство ' . $attributeInCamelCase);
        }
        if ($entity->$attribute === null) {
            return null;
        }
        $possibleValues = $this->_entityClass::possibleValues();

        return $possibleValues[$attribute][$entity->$attribute] ?? $entity->$attribute;
    }

    /**
     * Обработчик для функций вида get[ИмяСвойства]Priority($entity).
     * Возвращает приоритет (числовой эквивалент) для некого свойства сущности.
     * Приоритет будет полезен, если надо отсортировать список сущностей по какому-либо статусу.
     *
     * @param $name
     * @param $arguments
     * @return int|null
     * @throws Exception
     */
    private function _handleGetPriority($name, $arguments)
    {
        $entity = $arguments[0] ?? null;
        $this->instanceOf($entity);
        $attributeInCamelCase = StringHelper::toCamelCase(preg_replace(['/^get/', '/Priority$/'], '', $name));
        $attributeInSnakeCase = StringHelper::toSnakeCase($attributeInCamelCase);
        if (!property_exists($entity, $attribute = $attributeInCamelCase) && !property_exists($entity, $attribute = $attributeInSnakeCase)) {
            throw new Exception('Ошибка при обработке функции ' . $name . '. Не найдено свойство ' . $attributeInCamelCase);
        }
        if ($entity->$attribute === null) {
            return null;
        }
        $possibleValues = $this->_entityClass::possibleValues();

        return array_search($entity->$attribute, array_keys($possibleValues[$attribute]));
    }
}