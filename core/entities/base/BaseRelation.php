<?php

namespace core\entities\base;

use core\helpers\StringHelper;
use Exception;
use Yii;

/**
 * Базовый класс для доступа к связанным сущностям.
 *
 * Class BaseRelation
 * @package core\entities\base
 */
class BaseRelation
{
    protected $_getSet;

    public function __construct($getSet)
    {
        $this->_getSet = $getSet;
    }

    /**
     * Геттеры для связанных сущностей
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        if (preg_match('/^get/', $name) !== 1) {
            throw new Exception('Не найдена функция ' . $name);
        }
        $entity = $arguments[0] ?? null;
        $this->_getSet->instanceOf($entity);
        $relationAlias = lcfirst(preg_replace('/^get/', '', $name));
        $relationInfo = $entity->relations()[$relationAlias] ?? null;
        if (!$this->_checkRelationInfo($relationInfo)) {
            throw new Exception('Связь ' . $relationAlias . ' отсутствует или некорректно описана.');
        }

        return $this->_getRelatedEntities($entity, $relationInfo);
    }

    /**
     * Проверка наличия связанной сущности
     *
     * @param BaseEntity $entity
     * @param string $attribute
     * @param string $relationAlias
     * @return bool
     * @throws Exception
     */
    public function exists($entity, $attribute, $relationAlias = null)
    {
        $this->_getSet->instanceOf($entity);
        $relationInfo = $relationAlias ? ($entity->relations()[$relationAlias] ?? null) : $this->_getRelationInfoByAttribute($entity, $attribute);
        if (!$this->_checkRelationInfo($relationInfo)) {
            throw new Exception('Связь для ' . $attribute . ' отсутствует или некорректно описана.');
        }
        if (!isset($relationInfo['existsMethod'])) {
            throw new Exception('Связь для ' . $attribute . ' некорректно описана. Не задан existsMethod.');
        }

        return $this->_relatedEntityExists($entity, $relationInfo);
    }

    /**
     * @param BaseEntity $entity
     * @param array $relationInfo Элемент из массива relations()
     * @return bool
     * @throws Exception
     */
    private function _relatedEntityExists($entity, $relationInfo)
    {
        $relation = Yii::$container->get($relationInfo['class']);
        $method = $relationInfo['existsMethod'];
        $attribute = $relationInfo['attribute'];
        $id = $this->_getSet->{'get' . ucfirst(StringHelper::toCamelCase($attribute))}($entity);
        if (!method_exists($relation, $method)) {
            $shortClassName = StringHelper::getShortClassName(get_class($relation));
            throw new Exception(Yii::t('app',
                'Ошибка при проверке наличия связанной сущности. Не найден метод ' . $method . ' в классе ' . $shortClassName . '.'));
        }

        return $relation->$method($id);
    }

    /**
     * @param BaseEntity $entity
     * @param array $relationInfo Элемент из массива relations()
     * @return BaseEntity[]|BaseEntity|null
     * @throws Exception
     */
    private function _getRelatedEntities($entity, $relationInfo)
    {
        $relation = Yii::$container->get($relationInfo['class']);
        $method = $relationInfo['getterMethod'];
        $attribute = $relationInfo['attribute'];
        $id = $this->_getSet->{'get' . ucfirst(StringHelper::toCamelCase($attribute))}($entity);
        if (!method_exists($relation, $method)) {
            $shortClassName = StringHelper::getShortClassName(get_class($relation));
            throw new Exception(Yii::t('app','Ошибка при получении связанной сущности. Не найден метод ' . $method . ' в классе ' . $shortClassName . '.'));
        }

        return $relation->$method($id);
    }

    /**
     * @param BaseEntity $entity
     * @param string $attribute
     * @return array|null $relationInfo Элемент из массива relations() (или null, если ничего не нашлось)
     */
    private function _getRelationInfoByAttribute($entity, $attribute)
    {
        foreach ($entity->relations() as $relationInfo) {
            if (isset($relationInfo['attribute']) && $relationInfo['attribute'] == $attribute) {
                return $relationInfo;
            }
        }

        return null;
    }

    /**
     * @param mixed $relationInfo Элемент из массива relations()
     * @return bool
     */
    private function _checkRelationInfo($relationInfo)
    {
        if (empty($relationInfo)) {
            return false;
        }
        if (!isset($relationInfo['attribute']) || !isset($relationInfo['getterMethod']) || !isset($relationInfo['class'])) {
            return false;
        }

        return true;
    }
}

