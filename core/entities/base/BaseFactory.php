<?php

namespace core\entities\base;

/**
 * Базовый класс, предоставляющий набор функций для создания сущностей
 *
 * Class BaseFactory
 * @package core\entities\base
 */
class BaseFactory
{
    protected $_entityClass;
    protected $_query;
    protected $_relation;

    public function __construct($entityClass, $query, $relation)
    {
        $this->_entityClass = $entityClass;
        $this->_query = $query;
        $this->_relation = $relation;
    }

    public function create()
    {
        return new $this->_entityClass($this->_query, $this->_relation);
    }
}