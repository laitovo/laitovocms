<?php

namespace core\entities\base;

use yii\db\Query;

/**
 * Базовый класс, предоставляющий набор функций для чтения информации из хранилища данных
 *
 * Class BaseQuery
 * @package core\entities\base
 */
class BaseQuery
{
    protected $_table;

    public function __construct($table)
    {
        $this->_table = $table;
    }

    /**
     * Функция проверяет наличие записи с такими атрибутами в хранилище данных
     *
     * @param array $attributes
     * @return bool
     */
    public function exists($attributes)
    {
        $query = (new Query())
            ->select('*')
            ->from($this->_table);
        foreach ($attributes as $key => $value) {
            $query->andWhere($key . '= :'.$key, [':'.$key => $value]);
        }

        return $query->exists();
    }

    /**
     * Функция проверяет набор атрибутов на уникальность.
     * Помимо атрибутов явно передаём id, чтобы не сравнивать запись саму с собой.
     * Если id пуст, то просто проверяем отсутствие записей с такими атрибутами.
     * Если id не пуст, то проверяем отсутствие записей с такими атрибутами и другим id.
     *
     * @param int|null $id
     * @param array $attributes
     * @return bool
     */
    public function unique($id, $attributes)
    {
        $query = (new Query())
            ->select('*')
            ->from($this->_table);
        foreach ($attributes as $key => $value) {
            if ($key == 'id') {
                continue;
            }
            $query->andWhere($key . '= :'.$key, [':'.$key => $value]);
        }
        if ($id) {
            $query->andWhere('not id = :id',[':id' => $id]);
        }

        return !$query->exists();
    }

    public function findAll()
    {
        return (new Query())
            ->select('*')
            ->from($this->_table)
            ->all();
    }

    public function findBy($attributes)
    {
        $query = (new Query())
            ->select('*')
            ->from($this->_table);
        foreach ($attributes as $key => $value) {
            $query->andWhere($key . '= :'.$key, [':'.$key => $value]);
        }

        return $query->all();
    }

    public function findWhere($conditions)
    {
        return (new Query())
            ->select('*')
            ->from($this->_table)
            ->where($conditions)
            ->all();
    }

    public function findWhereColumn($column,$conditions)
    {
        return (new Query())
            ->select($column)
            ->from($this->_table)
            ->where($conditions)
            ->column();
    }

    public function findById($id)
    {
         return (new Query())
            ->select('*')
            ->from($this->_table)
            ->where('id = :id',[':id' => $id])
            ->one();
    }

    public function countAll()
    {
        return (new Query())
            ->select('*')
            ->from($this->_table)
            ->count();
    }

    public function countBy($attributes)
    {
        $query = (new Query())
            ->select('*')
            ->from($this->_table);
        foreach ($attributes as $key => $value) {
            $query->andWhere($key . '= :'.$key, [':'.$key => $value]);
        }

        return $query->count();
    }

    public function countWhere($conditions)
    {
        return (new Query())
            ->select('*')
            ->from($this->_table)
            ->where($conditions)
            ->count();
    }

    public function maxWhere($conditions,$column)
    {
        return (new Query())
            ->select('*')
            ->from($this->_table)
            ->where($conditions)
            ->max($column);
    }
}