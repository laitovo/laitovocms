<?php

namespace core\entities\base;

use Exception;

/**
 * Базовый класс-обертка (класс-интерфейс), предоставляющий набор функций для работы с единицей бизнес-логики (сущностью)
 *
 * Class BaseManager
 * @package core\entities\base
 */
abstract class BaseManager
{
    /**
     * @var string Наименование таблицы БД
     */
    protected $_table;
    /**
     * @var string Класс для чтения информации из хранилища данных
     */
    protected $_queryClass;
    /**
     * @var string Класс для внесения изменений в хранилище данных
     */
    protected $_commandClass;
    /**
     * @var string Класс для создания сущностей
     */
    protected $_factoryClass;
    /**
     * @var string Класс для сохранения, получения, изменения и удаления сущностей из хранилища данных
     */
    protected $_repositoryClass;
    /**
     * @var string Класс для работы со свойствами сущности
     */
    protected $_getSetClass;
    /**
     * @var string Класс для получения связанных сущностей
     */
    protected $_relationClass;
    /**
     * @var string Класс для генерации автоматических полей сущности
     */
    protected $_autoFillClass;
    /**
     * @var string Класс для самой сущности
     */
    protected $_entityClass;
    /**
     * @var BaseQuery
     */
    protected $_query;
    /**
     * @var BaseCommand
     */
    protected $_command;
    /**
     * @var BaseFactory
     */
    protected $_factory;
    /**
     * @var BaseRepository
     */
    protected $_repository;
    /**
     * @var BaseGetSet
     */
    protected $_getSet;
    /**
     * @var BaseRelation
     */
    protected $_relation;
    /**
     * @var BaseAutoFill
     */
    protected $_autoFill;

    /**
     * BaseManager constructor.
     */
    public function __construct()
    {
        $this->_init();
        $this->_afterInit();
        $this->_initBaseClasses();
        $this->_initServices();
    }

    /**
     * Задание базовой информации о сущности, необходимой для работы менеджера.
     *
     * Данная функция должна быть обязательно переопределена при наследовании.
     * Минимально необходимо задать _table и _entityClass
     * (предварительно надо создать таблицу в БД, а также требуемый класс, унаследованный от BaseEntity).
     * При необходимости можно переопределить и другие базовые классы.
     * Пример:
     *     $this->_table           = 'users';
     *     $this->_entityClass     = User::class;
     *     $this->_queryClass      = UserQuery::class;
     *     $this->_commandClass    = UserCommand::class;
     *     $this->_factoryClass    = UserFactory::class;
     *     $this->_repositoryClass = UserRepository::class;
     *     $this->_getSetClass     = UserGetSet::class;
     *     $this->_relationClass   = UserRelation::class;
     *     $this->_autoFillClass   = UserAutoFill::class;
     */
    abstract protected function _init();

    /**
     * Проверка заданной информации о сущности, подставление дефолтных значений.
     * Данная функция позволяет не наследовать все базовые классы, нужные менеджеру, а ограничиться только необходимыми.
     */
    private function _afterInit()
    {
        $this->_queryClass = $this->_queryClass ?: BaseQuery::class;
        $this->_commandClass = $this->_commandClass ?: BaseCommand::class;
        $this->_factoryClass = $this->_factoryClass ?: BaseFactory::class;
        $this->_repositoryClass = $this->_repositoryClass ?: BaseRepository::class;
        $this->_getSetClass = $this->_getSetClass ?: BaseGetSet::class;
        $this->_relationClass = $this->_relationClass ?: BaseRelation::class;
        $this->_autoFillClass = $this->_autoFillClass ?: BaseAutoFill::class;
    }

    /**
     * Инициализация базовых сервисов, необходимых для работы менеджера.
     */
    private function _initBaseClasses()
    {
        $this->_query = new $this->_queryClass($this->_table);
        $this->_command = new $this->_commandClass($this->_table);
        $this->_getSet = new $this->_getSetClass($this->_entityClass);
        $this->_relation = new $this->_relationClass($this->_getSet);
        $this->_autoFill = new $this->_autoFillClass($this->_getSet);
        $this->_factory = new $this->_factoryClass($this->_entityClass, $this->_query, $this->_relation);
        $this->_repository = new $this->_repositoryClass($this->_getSet, $this->_factory, $this->_query,
            $this->_command, $this->_autoFill);
    }

    /**
     * Инициализация дополнительных сервисов для работы с сущностью.
     *
     * По идее, если не удается ограничиться функционалом базовых классов, дополнительные сервисы должны быть заданы тут.
     * Дополнительные сервисы будут созданы после базовых, поэтому могут их использовать.
     * Пример:
     *     $this->_queryFormatter = new QueryFormatter($this->_query);
     *     $this->_deletingService = new DeletingService($this->_repository);
     */
    protected function _initServices() {}

    /**
     * Автоматические геттеры и сеттеры
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call ($name, $arguments) {
        if (preg_match('/^getRelated/',$name) === 1) {
            $name = preg_replace('/^getRelated/', 'get', $name);
            return $this->_relation->$name(...$arguments);
        } elseif (preg_match('/^get/',$name) === 1 || preg_match('/^set/',$name) === 1) {
            return $this->_getSet->$name(...$arguments);
        } else {
            throw new Exception('Не найдена функция ' . $name);
        }
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->_repository->findAll();
    }

    /**
     * @param array $attributes
     * @return array
     */
    public function findBy($attributes)
    {
        return $this->_repository->findBy($attributes);
    }

    /**
     * @param array $conditions
     * @return array
     */
    public function findWhere($conditions)
    {
        return $this->_repository->findWhere($conditions);
    }

    /**
     * @param string $column
     * @param array $conditions
     * @return array
     */
    public function findWhereColumn($column,$conditions)
    {
        return $this->_query->findWhereColumn($column,$conditions);
    }

    /**
     * @param int $id
     * @return BaseEntity|null
     */
    public function findById($id)
    {
        return $this->_repository->findById($id);
    }

    /**
     * @param $condition
     * @param $field
     * @return mixed
     */
    public function maxWhere($condition,$field)
    {
        return $this->_repository->maxWhere($condition,$field);
    }

    /**
     * @return int
     */
    public function countAll()
    {
        return $this->_repository->countAll();
    }

    /**
     * @param array $attributes
     * @return int
     */
    public function countBy($attributes)
    {
        return $this->_repository->countBy($attributes);
    }

    /**
     * @param array $conditions
     * @return int
     */
    public function countWhere($conditions)
    {
        return $this->_repository->countWhere($conditions);
    }

    /**
     * @param int $id
     * return bool
     */
    public function exists($id)
    {
        return $this->_repository->exists($id);
    }

    /**
     * @return BaseEntity
     */
    public function create()
    {
        return $this->_factory->create();
    }

    /**
     * @param BaseEntity $entity
     * @param bool $autoFill Заполнять ли автоматические поля перед сохранением
     * @return bool
     */
    public function save($entity, $autoFill = true)
    {
        return $this->_repository->save($entity, $autoFill);
    }

    /**
     * @param BaseEntity $entity
     * @return bool
     */
    public function delete($entity)
    {
        return $this->_repository->delete($entity);
    }

    /**
     * Функция возвращает массив с наименованиями свойств сущности
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->_getSet->getAttributes();
    }

    /**
     * Функция возвращает массив с наименованиями и заголовками свойств сущности.
     * Данная функция может быть полезна для виджетов.
     *
     * Пример:
     * [
     *     ['attribute' => 'id', 'label' => 'ID'],
     *     ['attribute' => 'name', 'label' => 'Наименование'],
     *     ['attribute' => 'status', 'label' => 'Статус'],
     * ]
     *
     * @return array
     */
    public function getAttributesWithLabels()
    {
        return $this->_getSet->getAttributesWithLabels();
    }

    /**
     * Массовое заполнение свойств сущности
     *
     * @param BaseEntity $entity
     * @param array $attributes
     */
    public function setAttributes($entity, $attributes)
    {
        $this->_getSet->setAttributes($entity, $attributes);
    }

    /**
     * Заполнение свойств сущности данными из формы.
     * Вернёт false, если форма пуста или не связана c сущностью.
     *
     * @param BaseEntity $entity
     * @param array $formData
     * @return bool
     */
    public function fillByForm($entity, $formData)
    {
        return $this->_getSet->fillByForm($entity, $formData);
    }

    /**
     * Наименование свойства, которое является первичным ключом
     *
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->_getSet->getPrimaryKey();
    }
}