<?php

namespace core\entities\base;

use core\helpers\StringHelper;
use Yii;
use Exception;

/**
 * Базовый класс, отвечающий за заполнение автоматических свойств сущности
 *
 * Class BaseAutoFill
 * @package core\entities\base
 */
class BaseAutoFill
{
    protected $_getSet;

    #conditions
    const CONDITION_ALWAYS = '_conditionAlways';
    const CONDITION_IF_NULL = '_conditionIfNull';
    #handlers
    const HANDLER_TIMESTAMP = '_handlerTimestamp';
    const HANDLER_USER = '_handlerUser';

    public function __construct($getSet)
    {
        $this->_getSet = $getSet;
    }

    /**
     * Заполнение автоматических свойств сущности
     *
     * @param BaseEntity $entity
     */
    public function setAutoFields($entity)
    {
        foreach ($entity->autoFill() as $fieldData) {
            $this->_setAutoField($entity, $fieldData);
        }
    }

    /**
     * Заполнение автоматического свойства сущности.
     *
     * Свойство заполняется на основе переданного массива $fieldData:
     * - $fieldData['attribute'] - наименование свойства;
     * - $fieldData['condition'] - функция данного класса, описывающая условие, при котором будет выполняться автозаполнение свойства;
     * - $fieldData['handler'] - функция данного класса, генерирующая значение свойства.
     *
     * @param BaseEntity $entity
     * @param array $fieldData Правила автозаполнения свойства
     * @throws Exception
     */
    protected function _setAutoField($entity, $fieldData)
    {
        if (empty($fieldData['attribute']) || empty($fieldData['condition']) || empty($fieldData['handler'])) {
            throw new Exception(Yii::t('app','Некорректный вызов автозаполнения. Необходимо передать свойство, условие и обработчик.'));
        }
        if (!method_exists(static::class, $fieldData['condition'])) {
            throw new Exception(Yii::t('app','Некорректный вызов автозаполнения. Не найдена функция ' . $fieldData['condition'] . '.'));
        }
        if (!method_exists(static::class, $fieldData['handler'])) {
            throw new Exception(Yii::t('app','Некорректный вызов автозаполнения. Не найдена функция ' . $fieldData['handler'] . '.'));
        }
        $fieldName = $fieldData['attribute'];
        $conditionFunctionName = $fieldData['condition'];
        $handlerFunctionName = $fieldData['handler'];
        if (!$this->$conditionFunctionName($entity, $fieldName)) {
            return;
        }
        $value = $this->$handlerFunctionName($entity);
        $this->_setProperty($entity, $fieldName, $value);
    }

    protected function _getProperty($entity, $fieldName)
    {
        $functionName = 'get' . ucfirst(StringHelper::toCamelCase($fieldName));

        return $this->_getSet->$functionName($entity);
    }

    protected function _setProperty($entity, $fieldName, $value)
    {
        $functionName = 'set' . ucfirst(StringHelper::toCamelCase($fieldName));
        $this->_getSet->$functionName($entity, $value);
    }

    #conditions
    /**
     * @param BaseEntity $entity
     * @param string $fieldName
     * @return bool
     */
    protected function _conditionAlways($entity, $fieldName)
    {
        return true;
    }

    /**
     * @param BaseEntity $entity
     * @param string $fieldName
     * @return bool
     */
    protected function _conditionIfNull($entity, $fieldName)
    {
        return $this->_getProperty($entity, $fieldName) === null;
    }

    #handlers
    /**
     * Текущая метка времени
     *
     * @param BaseEntity $entity
     * @return int
     */
    protected function _handlerTimestamp($entity)
    {
        return time();
    }

    /**
     * id текущего пользователя
     *
     * @param BaseEntity $entity
     * @return int
     */
    protected function _handlerUser($entity)
    {
        return Yii::$app->user->getId();
    }
}