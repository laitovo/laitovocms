<?php

namespace core\entities\base;

use Yii;
use Exception;
use Throwable;

/**
 * Базовый класс, предоставляющий набор функций для внесения изменений в хранилище данных
 *
 * Class BaseCommand
 * @package core\entities\base
 */
class BaseCommand
{
    protected $_connection;
    protected $_table;

    public function __construct($table)
    {
        $this->_connection = Yii::$app->db;
        $this->_table = $table;
    }

    public function insert($columns)
    {
        $transaction = $this->_connection->beginTransaction();
        try {
            $count = $this->_connection->createCommand()->insert($this->_table, $columns)->execute();
            $id = $this->_connection->getLastInsertID();
            if (!$count) {
                return false;
            }
            $transaction->commit();
            return $id;
        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        } catch (Throwable $e) {
            $transaction->rollBack();
            return false;
        }
    }

    public function update($columns)
    {
        $count = $this->_connection->createCommand()->update($this->_table, $columns, 'id = :id',
            [':id' => $columns['id']])->execute();

        return (bool)$count;
    }

    public function delete($columns)
    {
        $count = $this->_connection->createCommand()->delete($this->_table, 'id = :id',
            [':id' => $columns['id']])->execute();

        return (bool)$count;
    }
}