<?php

namespace core\entities\base;

use yii\base\Model;
use Exception;

/**
 * Базовый класс, описывающий единицу бизнес-логики (сущность).
 * Свойства entity задаются публичными переменными.
 * Также для свойств необходимо задать заголовки и правила валидации.
 *
 * Class BaseEntity
 * @package core\entities\base
 */
abstract class BaseEntity extends Model
{
    /**
     * @var BaseQuery Сервис для чтения информации из хранилища данных
     */
    protected $_query;
    /**
     * @var BaseRelation Сервис для получения связанных сущностей
     */
    protected $_relation;

    /**
     * Описание свойств:
     *
     * public $id;
     * public $name;
     * ...
     * ...
     * ...
     */

    /**
     * BaseEntity constructor.
     * @param BaseEntity $query
     * @param array $config
     */
    public function __construct($query, $relation, array $config = [])
    {
        $this->_query = $query;
        $this->_relation = $relation;

        parent::__construct($config);
    }

    public function attributeLabels()
    {
        return $this->titles();
    }

    public function rules()
    {
        return $this->validationRules();
    }

    /**
     * Заголовки свойств.
     * См. https://www.yiiframework.com/doc/api/2.0/yii-base-model#attributeLabels()-detail
     *
     * @return array
     */
    abstract function titles();

    /**
     * Правила валидации.
     * См. https://www.yiiframework.com/doc/api/2.0/yii-base-model#rules()-detail
     * Должны быть описаны правила для каждого свойства.
     *
     * @return array
     */
    abstract function validationRules();

    /**
     * Автоматически генерируемые поля.
     *
     * Для каждого поля необходимо задать условие и обработчик.
     * Пример:
     *   return [
     *       ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
     *       ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
     *       ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_USER],
     *       ['attribute' => 'authorId', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
     *  ];
     *
     * @return array
     */
    public function autoFill()
    {
        return [];
    }

    /**
     * Связи с другими сущностями.
     *
     * Каждая связь описывается в формате 'Наименование связи' => [Массив с информацией о связи].
     * Пример для некой сущности Order:
     *   return [
     *      'author'    => ['attribute' => 'authorId', 'class' => User_Order::class, 'getterMethod' => 'author', 'existsMethod' => 'authorExists'],
     *      'updater'   => ['attribute' => 'updaterId', 'class' => User_Order::class, 'getterMethod' => 'updater', 'existsMethod' => 'updaterExists'],
     *      'entries'   => ['attribute' => 'id', 'class' => Order_OrderEntry::class, 'getterMethod' => 'orderEntries'],
     *      'documents' => ['attribute' => 'id', 'class' => Order_OrderDocument::class, 'getterMethod' => 'orderDocuments'],
     *   ];
     *
     * На основе наименования будет сформирован геттер для доступа к связанной сущности
     * (например, author - getRelatedAuthor, documents - getRelatedDocuments).
     *
     * Информация о связи:
     *   attribute - это поле текущей сущности, через которое осуществляется связь (указывать обязательно);
     *   class - это класс, связывающий текущую сущность с какой-то другой (указывать обязательно);
     *   getterMethod - это метод связывающего класса для получения объектов связанной сущности (указывать обязательно);
     *   existsMethod - это метод связывающего класса для проверки наличия связанной сущности (указывать не обязательно, только для валидации).
     *
     * @return array
     */
    public function relations()
    {
        return [];
    }

    /**
     * Возможные значения свойств.
     *
     * Каждое свойство описывается в формате 'Атрибут' => [Массив возможных значений].
     * В массиве возможных значений ключом идёт само значение, а значением - его заголовок.
     *
     * Пример для неких свойств "status" и "color":
     *   return [
     *      'status' => [
     *          'not_handled' => 'Не обработан',
     *          'on_handling' => 'В обработке',
     *          'handled' => 'Обработан',
     *      ],
     *      'color' => [
     *          'red' => 'Красный',
     *          'yellow' => 'Желтый',
     *          'green' => 'Зеленый',
     *      ],
     *   ];
     *
     * @return array
     */
    public function possibleValues()
    {
        return [];
    }

    /**
     * Проверка свойства на уникальность
     *
     * @param $attribute
     * @param $params
     */
    public function validateUnique($attribute, $params)
    {
        if (!$this->_query->unique($this->id, [$attribute => $this->$attribute])) {
            $this->addError($attribute, 'Это поле должно быть уникальным');
        }
    }

    /**
     * Проверка наличия связанной сущности.
     * Если по атрибуту нельзя однозначно идентифицировать связь, можно дополнительно передать параметр 'relation' (ключ из массива relations()).
     *
     * @param $attribute
     * @param $params
     */
    public function validateExists($attribute, $params)
    {
        $relationAlias = $params['relation'] ?? null;
        if (!$this->_relation->exists($this, $attribute, $relationAlias)) {
            $this->addError($attribute, 'Не удалось найти выбранный объект');
        }
    }

    /**
     * Проверка что значение свойства входит в список возможных значений
     *
     * @param $attribute
     * @param $params
     * @throws Exception
     */
    public function validatePossibility($attribute, $params)
    {
        if (!array_key_exists($attribute, $this->possibleValues())) {
            throw new Exception('Ошибка валидации. Не описаны возможные значения для свойства ' . $attribute . '.');
        }
        if (!in_array($this->$attribute, array_keys($this->possibleValues()[$attribute]))) {
            $this->addError($attribute, 'Значение поля не входит в список возможных значений');
        }
    }
}