<?php

namespace core\entities\base;

use Yii;
use Exception;

/**
 * Базовый класс, предоставляющий набор функций для сохранения, получения, изменения и удаления сущностей из хранилища данных
 *
 * Class BaseRepository
 * @package core\entities\base
 */
class BaseRepository
{
    protected $_query;
    protected $_command;
    protected $_factory;
    protected $_getSet;
    protected $_autoFill;

    public function __construct($getSet, $factory, $query, $command, $autoFill)
    {
        $this->_query = $query;
        $this->_command = $command;
        $this->_factory = $factory;
        $this->_getSet = $getSet;
        $this->_autoFill = $autoFill;
    }

    public function findAll()
    {
        $entities = [];
        $rows = $this->_query->findAll();
        if (empty($rows)) {
            return $entities;
        }
        foreach ($rows as $row) {
            $entities[] = $this->_createEntityByArray($row);
        }

        return $entities;
    }

    public function findBy($attributes)
    {
        $entities = [];
        $rows = $this->_query->findBy($attributes);
        if (empty($rows)) {
            return $entities;
        }
        foreach ($rows as $row) {
            $entities[] = $this->_createEntityByArray($row);
        }

        return $entities;
    }

    public function findWhere($conditions)
    {
        $entities = [];
        $rows = $this->_query->findWhere($conditions);
        if (empty($rows)) {
            return $entities;
        }
        foreach ($rows as $row) {
            $entities[] = $this->_createEntityByArray($row);
        }

        return $entities;
    }

    public function findById($id)
    {
        $row = $this->_query->findById($id);
        if (!$row) {
            return null;
        }

        return $this->_createEntityByArray($row);
    }

    public function countAll()
    {
        return $this->_query->countAll();
    }

    public function countBy($attributes)
    {
        return $this->_query->countBy($attributes);
    }

    public function countWhere($conditions)
    {
        return $this->_query->countWhere($conditions);
    }

    public function maxWhere($conditions,$field)
    {
        return $this->_query->maxWhere($conditions,$field);
    }

    public function save($entity, $autoFill = true)
    {
        if ($autoFill) {
            $this->_autoFill->setAutoFields($entity);
        }
        if (!$this->_getSet->validate($entity)) {
            return false;
        }
        if ($this->_isNewRecord($entity)) {
            return $this->_insert($entity);
        } else {
            return $this->_update($entity);
        }
    }

    public function delete($entity)
    {
        $columns = $this->_getSet->toArray($entity);

        return $this->_command->delete($columns);
    }

    private function _createEntityByArray($attributes)
    {
        $entity = $this->_factory->create();
        $this->_getSet->setAttributes($entity, $attributes, $validate = false);

        return $entity;
    }

    public function exists($id)
    {
        return $this->_query->exists(['id' => $id]);
    }

    private function _insert($entity)
    {
        $columns = array_filter($this->_getSet->toArray($entity), function($column) {
            return $column !== null;
        });
        if (!($id = $this->_command->insert($columns))) {
            return false;
        }
        $this->_getSet->setId($entity, $id);

        return true;
    }

    private function _update($entity)
    {
        $columns = $this->_getSet->toArray($entity);

        return $this->_command->update($columns);
    }

    private function _isNewRecord($entity)
    {
        return !$this->_query->exists(['id' => $this->_getSet->getId($entity)]);
    }
}