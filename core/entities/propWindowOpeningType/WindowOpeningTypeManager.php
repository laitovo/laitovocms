<?php

namespace core\entities\propWindowOpeningType;

use core\entities\base\BaseManager;
use core\entities\propWindowOpeningType\models\services\QueryFormatter;
use core\entities\propWindowOpeningType\models\WindowOpeningType;

/**
 * Класс предоставляет функционал для работы с сущностью "Вид оконного проёма"
 *
 * Class WindowOpeningTypeManager
 * @package core\entities\propWindowOpeningType
 */
class WindowOpeningTypeManager extends BaseManager
{
    /**
     * @var QueryFormatter Надстройка над базовым query для форматирования результирующей выборки (выбор полей, индексация, сортировка и т.д.)
     */
    private $_queryFormatter;

    protected function _init()
    {
        $this->_table = '{{%prop_window_opening_type}}';
        $this->_entityClass = WindowOpeningType::class;
    }

    protected function _initServices()
    {
        $this->_queryFormatter = new QueryFormatter($this->_query);
    }

    /**
     * Список наименований всех видов оконных проёмов.
     *
     * На выходе массив вида 'id' => 'name'.
     * Данная функция может быть полезна для заполнения выпадающих списков.
     *
     * @return array
     */
    public function getNameListIndexById()
    {
        return $this->_queryFormatter->getNameListIndexById();
    }
}