<?php

namespace core\entities\propWindowOpeningType\models;

use core\entities\base\BaseEntity;
use Yii;

/**
 * Сущность "Вид оконного проёма".
 * Описывает оконный проём автомобиля (передний боковой, задний, ...).
 *
 * Class WindowOpeningType
 * @package core\entities\propWindowOpeningType\models
 */
class WindowOpeningType extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Наименование
     */
    public $name;
    /**
     * @var string Идентификатор, используемый в артикулах продуктов
     */
    public $articlePart;
    /**
     * @var string Наименование, используемое для распечатки инструкций
     */
    public $instructionAlias;
    /**
     * @var string Описание оконного проёма
     */
    public $description;

    public function titles()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Наименование'),
            'articlePart' => Yii::t('app', 'В артикуле'),
            'instructionAlias' => Yii::t('app', 'В инструкциях'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'articlePart'], 'validateUnique', 'message' => 'Это поле должно быть уникальным'],
            [['name', 'articlePart', 'instructionAlias'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['name', 'articlePart', 'instructionAlias'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
        ];
    }
}