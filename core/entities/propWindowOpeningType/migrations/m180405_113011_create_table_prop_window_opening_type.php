<?php

use yii\db\Migration;

class m180405_113011_create_table_prop_window_opening_type extends Migration
{
    /**
     * Таблица "Виды оконных проёмов".
     * Префикс prop_... означает, что данная таблица является справочником (описанием значений некого свойства).
     */
    public function up()
    {
        $this->createTable('{{%prop_window_opening_type}}', [
            'id' => $this->primaryKey(),
            'name'  => $this->string()->notNull()->unique()->comment('Наименование вида оконного проёма'),
            'articlePart' => $this->string()->notNull()->unique()->comment('Идентификатор вида оконного проёма, используемый в артикулах'),
            'description'  => $this->string(1000)->comment('Описание вида оконного проёма'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%prop_window_opening_type}}');
    }
}
