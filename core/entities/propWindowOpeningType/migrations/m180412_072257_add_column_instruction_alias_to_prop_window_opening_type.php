<?php

use yii\db\Migration;

class m180412_072257_add_column_instruction_alias_to_prop_window_opening_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%prop_window_opening_type}}', 'instructionAlias', $this->string()->comment('Наименование, используемое для распечатки инструкций'));
    }

    public function down()
    {
        $this->dropColumn('{{%prop_window_opening_type}}','instructionAlias');
    }
}
