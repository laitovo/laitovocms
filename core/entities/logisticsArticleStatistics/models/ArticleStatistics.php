<?php

namespace core\entities\logisticsArticleStatistics\models;

use core\entities\base\BaseEntity;
use Yii;

/**
 * Сущность "Реализованный артикул".
 * Представляет собой пометку о проданной единице продукции.
 * Используется для статистики.
 *
 * Class ArticleStatistics
 * @package core\entities\logisticsArticleStatistics\models
 */
class ArticleStatistics extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Артикул
     */
    public $article;
    /**
     * @var int Источник статистики
     */
    public $type;
    /**
     * @var int Дата создания
     */
    public $created_at;

    #Источники статистики
    const SOURCE_LAITOVO = 1;
    const SOURCE_BIZZONE = 2;

    public static $sourceTitles = [
        self::SOURCE_LAITOVO => 'laitovo.ru',
        self::SOURCE_BIZZONE => 'biz-zone.biz',
    ];

    public function titles()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'article' => Yii::t('app', 'Артикул'),
            'type' => Yii::t('app', 'Источник статистики'),
            'created_at' => Yii::t('app', 'Дата создания'),
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['article'], 'string', 'max' => 255],
            [['type', 'created_at'], 'integer'],
            [['article'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['type'], 'in', 'range' => [self::SOURCE_LAITOVO, self::SOURCE_BIZZONE], 'message' => 'Значение поля не входит в список возможных значений'],
        ];
    }
}