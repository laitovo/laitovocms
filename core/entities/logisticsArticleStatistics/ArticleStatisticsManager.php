<?php

namespace core\entities\logisticsArticleStatistics;

use core\entities\base\BaseManager;
use core\entities\logisticsArticleStatistics\models\ArticleStatistics;

/**
 * Класс предоставляет функционал для работы с сущностью "Реализованный артикул"
 *
 * Class ArticleStatisticsManager
 * @package core\entities\logisticsArticleStatistics
 */
class ArticleStatisticsManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%logistics_article_statistics}}';
        $this->_entityClass = ArticleStatistics::class;
    }
}