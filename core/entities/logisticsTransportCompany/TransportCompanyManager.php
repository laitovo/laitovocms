<?php

namespace core\entities\logisticsTransportCompany;

use core\entities\base\BaseManager;
use core\entities\logisticsTransportCompany\models\TransportCompany;

/**
 * Класс предоставляет функционал для работы с сущностью "Транспортная компания"
 *
 * Class TransportCompanyManager
 * @package core\entities\logisticsTransportCompany
 */
class TransportCompanyManager extends BaseManager
{
    protected function _init()
    {
        $this->_table       = '{{%logistics_transport_company}}';
        $this->_entityClass = TransportCompany::class;
    }
}