<?php

namespace core\entities\logisticsTransportCompany\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\User_TransportCompany;
use Yii;

/**
 * Сущность "Транспортная компания"
 *
 * Class TransportCompany
 * @package core\entities\logisticsTransportCompany\models
 */
class TransportCompany extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Наименование
     */
    public $name;
    /**
     * @var string Ссылка для отслеживания трэк-кода
     */
    public $trackingLink;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    public function titles()
    {
        return [
            'id'                => Yii::t('app', 'ID'),
            'name'              => Yii::t('app', 'Наименование'),
            'trackingLink'      => Yii::t('app', 'Ссылка для отслеживания'),
            'createdAt'         => Yii::t('app', 'Дата создания'),
            'authorId'          => Yii::t('app', 'Автор'),
            'updatedAt'         => Yii::t('app', 'Дата изменения'),
            'updaterId'         => Yii::t('app', 'Редактор'),
        ];
    }

    public function relations()
    {
        return [
            'author'  => ['attribute' => 'authorId',  'class' => User_TransportCompany::class, 'getterMethod' => 'author',  'existsMethod' => 'authorExists'],
            'updater' => ['attribute' => 'updaterId', 'class' => User_TransportCompany::class, 'getterMethod' => 'updater', 'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['name', 'trackingLink'], 'string', 'max' => 255],
            [['trackingLink'], 'url', 'defaultScheme' => 'http'],
            [['name'], 'required'],
            [['name'], 'validateUnique'],
            [['authorId', 'updaterId'], 'validateExists'],
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId',  'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }
}