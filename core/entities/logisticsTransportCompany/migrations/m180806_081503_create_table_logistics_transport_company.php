<?php

use yii\db\Migration;

class m180806_081503_create_table_logistics_transport_company extends Migration
{
    /**
     * Таблица "Транспортные компании"
     */
    public function up()
    {
        $this->createTable('{{%logistics_transport_company}}', [
            'id'              => $this->primaryKey(),
            'name'            => $this->string()->notNull()->unique()->comment('Наименование'),
            'trackingLink'    => $this->string()->comment('Ссылка для отслеживания трэк-кода'),
            'createdAt'       => $this->integer()->comment('Дата создания'),
            'updatedAt'       => $this->integer()->comment('Дата последнего изменения'),
            'authorId'        => $this->integer()->comment('Автор'),
            'updaterId'       => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%logistics_transport_company}}');
    }
}
