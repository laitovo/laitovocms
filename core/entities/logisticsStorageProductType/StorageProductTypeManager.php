<?php

namespace core\entities\logisticsStorageProductType;

use core\entities\base\BaseManager;
use core\entities\logisticsStorageProductType\models\StorageProductType;

/**
 * Класс предоставляет функционал для работы с сущностью "Вид складской продукции"
 *
 * Class StorageProductTypeManager
 * @package core\entities\logisticsStorageProductType
 */
class StorageProductTypeManager extends BaseManager
{
    protected function _init()
    {
        $this->_table       = '{{%logistics_storage_product_type}}';
        $this->_entityClass = StorageProductType::class;
    }
}