<?php

use yii\db\Migration;

class m180730_112150_create_table_logistics_storage_product_type extends Migration
{
    /**
     * Таблица "Виды складской продукции"
     */
    public function up()
    {
        $this->createTable('{{%logistics_storage_product_type}}', [
            'id'                => $this->primaryKey(),
            'title'             => $this->string()->comment('Наименование'),
            'article'           => $this->string()->comment('Артикул'),
            'barcode'           => $this->string()->comment('Штрихкод'),
            'group'             => $this->integer()->comment('Группа для печати (некая категория складской продукции, используемая для удобства печати)'),
            'hasBalanceControl' => $this->boolean()->defaultValue(false)->comment('Контроль складских остатков - пометка о том, что мы обязаны контролировать остатки по данному виду продукции'),
            'minBalance'        => $this->integer()->comment('Минимальный объем складских остатков'),
            'createdAt'         => $this->integer()->comment('Дата создания'),
            'updatedAt'         => $this->integer()->comment('Дата последнего изменения'),
            'authorId'          => $this->integer()->comment('Автор'),
            'updaterId'         => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%logistics_storage_product_type}}');
    }
}
