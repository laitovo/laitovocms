<?php

use yii\db\Migration;

class m181003_154258_add_column_canBeProduced_to_logistics_storage_product_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_storage_product_type}}', 'canBeProduced', $this->boolean()
            ->defaultValue(false)->comment('Может ли мы производить данный вид продукции'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_storage_product_type}}','canBeProduced');
    }
}
