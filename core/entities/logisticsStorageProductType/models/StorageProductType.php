<?php

namespace core\entities\logisticsStorageProductType\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\User_StorageProductType;
use Yii;

/**
 * Сущность "Вид складской продукции".
 * Под складской продукцией понимается товар, который не производится, а закупается (например, ароматизаторы, органайзеры, ...).
 * Данная сущность позволяет контролировать баланс складских остатков для складской продукции (сколько минимально должно быть, сколько осталось, ...).
 * По сути данная сущность является продолжением сущности "Вид продукции" (core\entities\laitovoErpProductType\models\ProductType).
 * В будущем возможно объединение сущностей "Вид продукции" и "Вид складской продукции", просто будет флаг со склада/не со склада.
 * Вид складской продукции можно рассматривать как группу артикулов.
 *
 * Class StorageProductType
 * @package core\entities\logisticsStorageProductType\models
 */
class StorageProductType extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Наименование
     */
    public $title;
    /**
     * @var string Артикул
     */
    public $article;
    /**
     * @var string Штрихкод
     */
    public $barcode;
    /**
     * @var int Группа для печати (некая категория складской продукции, используемая для удобства печати)
     */
    public $group;
    /**
     * @var bool Контроль складских остатков - пометка о том, что мы обязаны контролировать остатки по данному виду продукции
     */
    public $hasBalanceControl;
    /**
     * @var bool Может ли мы производить данный вид продукции
     */
    public $canBeProduced;
    /**
     * @var int Минимальный объем складских остатков
     */
    public $minBalance;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    #Группы для печати
    const GROUP_VELCRO = 1;
    const GROUP_COVER_WHEEL = 2;
    const GROUP_STICKER = 3;
    const GROUP_ORGANIZER = 4;
    const GROUP_COVER_SEAT = 5;
    const GROUP_FLAVORING = 6;
    const GROUP_CAPE_SHEEP = 7;
    const GROUP_CAPE = 8;
    const GROUP_LAITDOG = 9;
    const GROUP_MAGNET = 10;
    const GROUP_COVER_SNOWBOARD = 11;
    const GROUP_BAGS = 13;

    public function titles()
    {
        return [
            'id'                => Yii::t('app', 'ID'),
            'title'             => Yii::t('app', 'Наименование'),
            'article'           => Yii::t('app', 'Артикул'),
            'barcode'           => Yii::t('app', 'Штрихкод'),
            'group'             => Yii::t('app', 'Группа'),
            'hasBalanceControl' => Yii::t('app', 'Контроль складских остатков'),
            'canBeProduced'     => Yii::t('app', 'Через производство'),
            'minBalance'        => Yii::t('app', 'Минимальный объем'),
            'createdAt'         => Yii::t('app', 'Дата создания'),
            'updatedAt'         => Yii::t('app', 'Дата изменения'),
            'authorId'          => Yii::t('app', 'Автор'),
            'updaterId'         => Yii::t('app', 'Редактор'),
        ];
    }

    public function possibleValues()
    {
        return [
            'group' => [
                self::GROUP_VELCRO          => Yii::t('app', 'Запасной комплект застежек-липучек для экрана'),
                self::GROUP_COVER_WHEEL     => Yii::t('app', 'Чехлы для хранения автомобильных колёс'),
                self::GROUP_STICKER         => Yii::t('app', 'Наклейка'),
                self::GROUP_ORGANIZER       => Yii::t('app', 'Органайзер'),
                self::GROUP_COVER_SEAT      => Yii::t('app', 'Чехол для спинки сиденья'),
                self::GROUP_FLAVORING       => Yii::t('app', 'Ароматизатор'),
                self::GROUP_CAPE_SHEEP      => Yii::t('app', 'Комплект накидок из натуральной овчины'),
                self::GROUP_CAPE            => Yii::t('app', 'Комплект накидок'),
                self::GROUP_LAITDOG         => Yii::t('app', 'LaitDog'),
                self::GROUP_MAGNET          => Yii::t('app', 'Комплект магнитных держателей'),
                self::GROUP_COVER_SNOWBOARD => Yii::t('app', 'Чехол-рюкзак для сноуборда'),
                self::GROUP_BAGS            => Yii::t('app', 'Сумки для экранов'),
            ],
        ];
    }

    public function relations()
    {
        return [
            'author'  => ['attribute' => 'authorId',  'class' => User_StorageProductType::class, 'getterMethod' => 'author',  'existsMethod' => 'authorExists'],
            'updater' => ['attribute' => 'updaterId', 'class' => User_StorageProductType::class, 'getterMethod' => 'updater', 'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id', 'group', 'minBalance', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['title', 'article', 'barcode'], 'string', 'max' => 255],
            [['hasBalanceControl', 'canBeProduced'], 'boolean'],
            [['title', 'article', 'barcode'], 'required'],
            [['article', 'barcode'], 'validateUnique'],
            [['group'], 'validatePossibility'],
            [['authorId', 'updaterId'], 'validateExists'],
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId',  'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS,  'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }
}