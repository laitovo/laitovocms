<?php

namespace core\entities\laitovoInstructionType;

use core\entities\base\BaseEntity;
use core\entities\base\BaseManager;
use core\entities\laitovoInstructionType\models\InstructionType;
use core\entities\laitovoInstructionType\models\services\ArticleService;

/**
 * Класс предоставляет функционал для работы с сущностью "Вид инструкций"
 *
 * Class InstructionTypeManager
 * @package core\entities\laitovoInstructionType
 */
class InstructionTypeManager extends BaseManager
{
    private $_articleService;

    protected function _init()
    {
        $this->_table = '{{%laitovo_instruction_type}}';
        $this->_entityClass = InstructionType::class;
    }

    protected function _initServices()
    {
        $this->_articleService = new ArticleService($this->_getSet, $this->_repository);
    }

    /**
     * Поиск вида инструкций по артикулу
     *
     * @param string $article
     * @return BaseEntity|null
     */
    public function findByArticle($article)
    {
        return $this->_articleService->findByArticle($article);
    }
}