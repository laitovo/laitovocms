<?php

namespace core\entities\laitovoInstructionType\models;

use core\entities\base\BaseEntity;
use core\relations\Brand_InstructionType;
use core\relations\ExecutionType_InstructionType;
use core\relations\WindowOpeningType_InstructionType;
use Yii;

/**
 * Сущность "Вид инструкций".
 * Описывает набор свойств для распечатки инструкций (по установке шторок).
 * Данная сущность является вспомогательной, то есть используется для виджетов и т.д., сама же по себе большого смысла не несёт.
 *
 * Class InstructionType
 * @package core\entities\laitovoInstructionType\models
 */
class InstructionType extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var int Вид оконного проёма
     */
    public $windowOpeningTypeId;
    /**
     * @var int Бренд
     */
    public $brandId;
    /**
     * @var int Вид исполнения
     */
    public $executionTypeId;
    /**
     * @var string Тип крепления
     */
    public $fixationType;
    /**
     * @var string Маска артикула (часть артикула без информации о машине, например, "FD-%-%-1-2")
     */
    public $articleMask;
    /**
     * @var string Паттерн (регулярное выражение) для проверки соответствия данного вида инструкций конкретному артикулу
     */
    public $articlePattern;
    /**
     * @var string Поле для согласования инструкций с карточкой автомобиля
     */
    public $carsFormStatus;

    #Тип крепления
    const FIXATION_TYPE_SIMPLE = 'simple';
    const FIXATION_TYPE_MAGNETIC = 'magnetic';

    public function possibleValues()
    {
        return [
            'fixationType' => [
                self::FIXATION_TYPE_SIMPLE   => Yii::t('app', 'Простые'),
                self::FIXATION_TYPE_MAGNETIC => Yii::t('app', 'Магнитные'),
            ],
        ];
    }

    public function titles()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'windowOpeningTypeId' => Yii::t('app', 'Вид оконного проёма'),
            'brandId' => Yii::t('app', 'Бренд'),
            'executionTypeId' => Yii::t('app', 'Вид исполнения'),
            'fixationType' => Yii::t('app', 'Тип крепления'),
            'articleMask' => Yii::t('app', 'Маска артикула'),
            'articlePattern' => Yii::t('app', 'Паттерн артикула'),
            'carsFormStatus' => Yii::t('app', 'Зависимый статус'),
        ];
    }

    public function relations()
    {
        return [
            'windowOpeningType' => ['attribute' => 'windowOpeningTypeId', 'class' => WindowOpeningType_InstructionType::class, 'getterMethod' => 'windowOpeningType', 'existsMethod' => 'windowOpeningTypeExists'],
            'brand'             => ['attribute' => 'brandId', 'class' => Brand_InstructionType::class, 'getterMethod' => 'brand', 'existsMethod' => 'brandExists'],
            'executionType'     => ['attribute' => 'executionTypeId', 'class' => ExecutionType_InstructionType::class, 'getterMethod' => 'executionType', 'existsMethod' => 'executionTypeExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['windowOpeningTypeId', 'brandId', 'executionTypeId'], 'integer'],
            [['fixationType', 'articleMask', 'articlePattern', 'carsFormStatus'], 'string', 'max' => 255],
            [['windowOpeningTypeId', 'brandId', 'executionTypeId', 'fixationType', 'articleMask', 'articlePattern'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['articleMask'], 'validateUnique'],
            [['fixationType'], 'validatePossibility'],
            [['windowOpeningTypeId', 'brandId', 'executionTypeId'], 'validateExists'],
        ];
    }

    public function beforeValidate()
    {
        if (!$this->_query->unique($this->id, $this->toArray())) {
            $this->addError('brandId', 'Не уникальный набор свойств');
            $this->addError('windowOpeningTypeId', 'Не уникальный набор свойств');
            $this->addError('executionTypeId', 'Не уникальный набор свойств');
            $this->addError('fixationType', 'Не уникальный набор свойств');
            return false;
        }

        if (!preg_match('/^(FW|FV|FD|RD|RV|BW)-%-%-\d+-\d+$/', $this->articleMask)) {
            $this->addError('articleMask', 'Маска артикула должна быть вида "FD-%-%-1-1"');
            return false;
        }

        return parent::beforeValidate();
    }
}