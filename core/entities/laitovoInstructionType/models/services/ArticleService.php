<?php

namespace core\entities\laitovoInstructionType\models\services;

use core\entities\base\BaseEntity;

/**
 * Сервис отвечает за работу с артикулами.
 *
 * Class ArticleService
 * @package core\entities\laitovoInstructionType\models\services
 */
class ArticleService
{
    private $_getSet;
    private $_repository;

    public function __construct($getSet, $repository)
    {
        $this->_getSet      = $getSet;
        $this->_repository  = $repository;
    }

    /**
     * Поиск вида инструкций по артикулу
     *
     * @param string $article
     * @return BaseEntity|null
     */
    public function findByArticle($article)
    {
        $instructionTypes = array_filter($this->_repository->findAll(), function($instructionType) use ($article) {
            $articlePattern = $this->_getSet->getArticlePattern($instructionType);
            return preg_match($articlePattern, $article);
        });
        foreach ($instructionTypes as $instructionType) {
            return $instructionType;
        }

        return null;
    }
}