<?php

use yii\db\Migration;

class m180621_083903_add_column_articlePattern_to_laitovo_instruction_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_instruction_type}}', 'articlePattern', $this->string()->notNull()->comment('Паттерн (регулярное выражение) для проверки соответствия данного вида инструкций конкретному артикулу'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_instruction_type}}','articlePattern');
    }
}
