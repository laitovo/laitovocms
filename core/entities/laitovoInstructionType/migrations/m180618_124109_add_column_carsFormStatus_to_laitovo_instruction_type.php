<?php

use yii\db\Migration;

class m180618_124109_add_column_carsFormStatus_to_laitovo_instruction_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_instruction_type}}', 'carsFormStatus', $this->string()->comment('Поле для согласования инструкций с карточкой автомобиля'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_instruction_type}}','carsFormStatus');
    }
}
