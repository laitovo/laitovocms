<?php

use yii\db\Migration;

class m180618_080741_add_column_articleMask_to_laitovo_instruction_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_instruction_type}}', 'articleMask', $this->string()->notNull()->unique()->comment('Маска артикула (часть артикула без информации о машине, например, "FD-%-%-1-2")'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_instruction_type}}','articleMask');
    }
}
