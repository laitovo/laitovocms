<?php

use yii\db\Migration;

class m180412_082126_create_table_laitovo_instruction_type extends Migration
{
    /**
     * Таблица "Виды инструкций".
     * Префикс laitovo_... означает, что данная таблица относится к модулю "Laitovo".
     */
    public function up()
    {
        $this->createTable('{{%laitovo_instruction_type}}', [
            'id' => $this->primaryKey(),
            'windowOpeningTypeId'  => $this->integer()->notNull()->comment('Вид оконного проёма'),
            'brandId'  => $this->integer()->notNull()->comment('Бренд'),
            'executionTypeId'  => $this->integer()->notNull()->comment('Вид исполнения'),
            'fixationType'  => $this->string()->notNull()->comment('Тип крепления'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%laitovo_instruction_type}}');
    }
}
