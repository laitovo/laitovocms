<?php

namespace core\entities\laitovoErpProductType;

use core\entities\base\BaseManager;
use core\entities\laitovoErpProductType\models\ProductType;

/**
 * Класс предоставляет функционал для работы с сущностью "Вид продукции"
 *
 * Class ProductTypeManager
 * @package core\entities\laitovoErpProductType
 */
class ProductTypeManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%laitovo_erp_product_type}}';
        $this->_entityClass = ProductType::class;
    }
}