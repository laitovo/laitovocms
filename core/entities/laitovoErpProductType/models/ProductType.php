<?php

namespace core\entities\laitovoErpProductType\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\ProductionScheme_ProductType;
use core\relations\User_ProductType;
use Yii;

/**
 * Сущность "Вид продукции".
 * Продукция делится на виды на основании оконного проёма, ткани и исполнения.
 * Вид продукции можно рассматривать как группу артикулов.
 *
 * Class ProductType
 * @package core\entities\laitovoInstructionType\models
 */
class ProductType extends BaseEntity
{
    //todo: описать за что отвечают свойства (возможно нужны дополнительные правила валидации)
    public $id;
    public $title;
    public $title_en;
    public $category;
    public $rule;
    public $article;
    public $production_scheme_id;
    public $automatic;
    public $created_at;
    public $author_id;

    public function titles()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Наименование'),
            'title_en' => Yii::t('app', 'Наименование (EN)'),
            'category' => Yii::t('app', 'Категория'),
            'rule' => Yii::t('app', 'Правило'),
            'article' => Yii::t('app', 'Артикул'),
            'production_scheme_id' => Yii::t('app', 'ID'),
            'automatic' => Yii::t('app', 'ID'),
            'author_id' => Yii::t('app', 'Автор'),
            'created_at' => Yii::t('app', 'Дата создания'),
        ];
    }

    public function relations()
    {
        return [
            'author'           => ['attribute' => 'author_id', 'class' => User_ProductType::class, 'getterMethod' => 'author', 'existsMethod' => 'authorExists'],
            'productionScheme' => ['attribute' => 'production_scheme_id', 'class' => ProductionScheme_ProductType::class, 'getterMethod' => 'productionScheme', 'existsMethod' => 'productionSchemeExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['author_id', 'created_at', 'production_scheme_id'], 'integer'],
            [['title','title_en', 'rule', 'article','category'], 'string', 'max' => 255],
            [['automatic'], 'boolean'],
            [['author_id', 'production_scheme_id'], 'validateExists'],
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'author_id', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'created_at', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
        ];
    }
}