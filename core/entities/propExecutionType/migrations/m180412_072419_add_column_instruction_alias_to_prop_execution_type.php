<?php

use yii\db\Migration;

class m180412_072419_add_column_instruction_alias_to_prop_execution_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%prop_execution_type}}', 'instructionAlias', $this->string()->comment('Наименование, используемое для распечатки инструкций'));
    }

    public function down()
    {
        $this->dropColumn('{{%prop_execution_type}}','instructionAlias');
    }
}
