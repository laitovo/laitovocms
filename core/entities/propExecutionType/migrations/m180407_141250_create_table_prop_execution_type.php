<?php

use yii\db\Migration;

class m180407_141250_create_table_prop_execution_type extends Migration
{
    /**
     * Таблица "Виды исполнений".
     * Префикс prop_... означает, что данная таблица является справочником (описанием возможных значений некого свойства).
     */
    public function up()
    {
        $this->createTable('{{%prop_execution_type}}', [
            'id' => $this->primaryKey(),
            'name'  => $this->string()->notNull()->unique()->comment('Наименование вида исполнения'),
            'articlePart' => $this->string()->notNull()->unique()->comment('Идентификатор вида исполнения, используемый в артикулах'),
            'description'  => $this->string(1000)->comment('Описание вида исполнения'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%prop_execution_type}}');
    }
}
