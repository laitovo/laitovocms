<?php

namespace core\entities\propExecutionType;

use core\entities\base\BaseManager;
use core\entities\propExecutionType\models\ExecutionType;
use core\entities\propExecutionType\models\services\QueryFormatter;

/**
 * Класс предоставляет функционал для работы с сущностью "Вид исполнения"
 *
 * Class ExecutionTypeManager
 * @package core\entities\propExecutionType
 */
class ExecutionTypeManager extends BaseManager
{
    /**
     * @var QueryFormatter Надстройка над базовым query для форматирования результирующей выборки (выбор полей, индексация, сортировка и т.д.)
     */
    private $_queryFormatter;

    protected function _init()
    {
        $this->_table = '{{%prop_execution_type}}';
        $this->_entityClass = ExecutionType::class;
    }

    protected function _initServices()
    {
        $this->_queryFormatter = new QueryFormatter($this->_query);
    }

    /**
     * Список наименований всех видов исполнений.
     *
     * На выходе массив вида 'id' => 'name'.
     * Данная функция может быть полезна для заполнения выпадающих списков.
     *
     * @return array
     */
    public function getNameListIndexById()
    {
        return $this->_queryFormatter->getNameListIndexById();
    }
}