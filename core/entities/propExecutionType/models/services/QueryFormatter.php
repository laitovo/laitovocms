<?php

namespace core\entities\propExecutionType\models\services;

use core\entities\base\BaseQuery;
use yii\helpers\ArrayHelper;

class QueryFormatter
{
    /**
     * @var BaseQuery
     */
    protected $_query;

    public function __construct($query)
    {
        $this->_query = $query;
    }

    public function getNameListIndexById()
    {
        return $this->_indexById($this->_query->findAll());
    }

    private function _indexById($array)
    {
        return ArrayHelper::map($array, 'id', 'name');
    }
}