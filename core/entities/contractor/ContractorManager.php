<?php

namespace core\entities\contractor;

use core\entities\base\BaseManager;
use core\entities\contractor\models\Contractor;

/**
 * Класс предоставляет функционал для работы с сущностью "Контрагент"
 *
 * Class ContractorManager
 * @package core\entities\contractor
 */
class ContractorManager extends BaseManager
{
    protected function _init()
    {
        $this->_table = '{{%contractor}}';
        $this->_entityClass = Contractor::class;
    }
}