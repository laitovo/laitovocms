<?php

namespace core\entities\contractor\models;

use core\entities\base\BaseEntity;
use core\entities\base\BaseAutoFill;
use core\relations\Contractor_Contact;
use core\relations\Contractor_Contract;
use core\relations\Contractor_BankAccount;
use core\relations\ContractorGroup_Contractor;
use core\relations\User_Contractor;
use Yii;

/**
 * Сущность "Контрагент".
 * Описывает человека или организацию, с которым мы взаимодействуем в рамках какого-то договора.
 *
 * Class Contractor
 * @package core\entities\contractor\models
 */
class Contractor extends BaseEntity
{
    /**
     * @var int Идентификатор
     */
    public $id;
    /**
     * @var string Тип лица (физическое, юридическое)
     */
    public $personType;
    /**
     * @var string ФИО для физ. лица или наименование для юр. лица
     */
    public $name;
    /**
     * @var int Группа контрагентов (поставщики, покупатели, ...)
     */
    public $groupId;
    /**
     * @var string Документ
     */
    public $document;
    /**
     * @var string ИНН
     */
    public $inn;
    /**
     * @var string КПП
     */
    public $kpp;
    /**
     * @var string Код по ОКПО
     */
    public $crrbCode;
    /**
     * @var string Европейский ИНН
     */
    public $vatId;
    /**
     * @var string Юридический адрес
     */
    public $legalAddress;
    /**
     * @var string Фактический адрес
     */
    public $actualAddress;
    /**
     * @var string Телефон
     */
    public $phone;
    /**
     * @var string Дополнительная контактная информация
     */
    public $otherContacts;
    /**
     * @var string Комментарий
     */
    public $comment;
    /**
     * @var int Дата создания
     */
    public $createdAt;
    /**
     * @var int Дата последнего изменения
     */
    public $updatedAt;
    /**
     * @var int Автор
     */
    public $authorId;
    /**
     * @var int Последний редактор
     */
    public $updaterId;

    #Тип лица
    const PERSON_TYPE_PHYSICAL = 'physical';
    const PERSON_TYPE_LEGAL = 'legal';

    public function titles()
    {
        return [
            'id'            => Yii::t('app', 'ID'),
            'personType'    => Yii::t('app', 'Вид'),
            'name'          => Yii::t('app', 'Наименование'),
            'groupId'       => Yii::t('app', 'Группа контрагентов'),
            'document'      => Yii::t('app', 'Документ'),
            'inn'           => Yii::t('app', 'ИНН'),
            'kpp'           => Yii::t('app', 'КПП'),
            'crrbCode'      => Yii::t('app', 'Код по ОКПО'),
            'vatId'         => Yii::t('app', 'VAT ID'),
            'legalAddress'  => Yii::t('app', 'Юридический адрес'),
            'actualAddress' => Yii::t('app', 'Фактический адрес'),
            'phone'         => Yii::t('app', 'Телефон'),
            'otherContacts' => Yii::t('app', 'Другое (любая контактная информация)'),
            'comment'       => Yii::t('app', 'Комментарий'),
            'createdAt'     => Yii::t('app', 'Дата создания'),
            'updatedAt'     => Yii::t('app', 'Дата изменения'),
            'authorId'      => Yii::t('app', 'Автор'),
            'updaterId'     => Yii::t('app', 'Редактор'),
        ];
    }

    public function possibleValues()
    {
        return [
            'personType' => [
                self::PERSON_TYPE_PHYSICAL => Yii::t('app', 'Физ. лицо'),
                self::PERSON_TYPE_LEGAL    => Yii::t('app', 'Юр. лицо')
            ],
        ];
    }

    public function autoFill()
    {
        return [
            ['attribute' => 'createdAt', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'updatedAt', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_TIMESTAMP],
            ['attribute' => 'authorId', 'condition' => BaseAutoFill::CONDITION_IF_NULL, 'handler' => BaseAutoFill::HANDLER_USER],
            ['attribute' => 'updaterId', 'condition' => BaseAutoFill::CONDITION_ALWAYS, 'handler' => BaseAutoFill::HANDLER_USER],
        ];
    }

    public function relations()
    {
        return [
            'bankAccounts' => ['attribute' => 'id', 'class' => Contractor_BankAccount::class, 'getterMethod' => 'bankAccounts'],
            'contracts'    => ['attribute' => 'id', 'class' => Contractor_Contract::class, 'getterMethod' => 'contracts'],
            'contacts'     => ['attribute' => 'id', 'class' => Contractor_Contact::class, 'getterMethod' => 'contacts'],
            'group'        => ['attribute' => 'groupId', 'class' => ContractorGroup_Contractor::class, 'getterMethod' => 'group', 'existsMethod' => 'groupExists'],
            'author'       => ['attribute' => 'authorId', 'class' => User_Contractor::class, 'getterMethod' => 'author', 'existsMethod' => 'authorExists'],
            'updater'      => ['attribute' => 'updaterId', 'class' => User_Contractor::class, 'getterMethod' => 'updater', 'existsMethod' => 'updaterExists'],
        ];
    }

    public function validationRules()
    {
        return [
            [['id'], 'integer'],
            [['groupId', 'createdAt', 'authorId', 'updatedAt', 'updaterId'], 'integer'],
            [['personType','name', 'document', 'inn', 'kpp', 'crrbCode', 'vatId', 'legalAddress', 'actualAddress', 'phone', 'comment'], 'string', 'max' => 255],
            [['personType', 'name'], 'required', 'message' => 'Это поле обязательно для заполнения'],
            [['personType'], 'validatePossibility'],
            [['groupId', 'authorId', 'updaterId'], 'validateExists'],
        ];
    }
}