<?php

use yii\db\Migration;

class m180427_092440_create_table_contractors extends Migration
{
    /**
     * Таблица "Контрагенты"
     */
    public function up()
    {
        $this->createTable('{{%contractor}}', [
            'id'            => $this->primaryKey(),
            'personType'    => $this->string()->notNull()->comment('Тип лица (физическое, юридическое)'),
            'name'          => $this->string()->notNull()->comment('ФИО для физ. лица или наименование для юр. лица'),
            'groupId'       => $this->integer()->comment('Группа контрагентов (поставщики, покупатели, ...)'),
            'document'      => $this->string()->comment('Документ'),
            'inn'           => $this->string()->comment('ИНН'),
            'kpp'           => $this->string()->comment('КПП'),
            'crrbCode'      => $this->string()->comment('Код по ОКПО'),
            'vatId'         => $this->string()->comment('Европейский ИНН'),
            'legalAddress'  => $this->string(1000)->comment('Юридический адрес'),
            'actualAddress' => $this->string(1000)->comment('Фактический адрес'),
            'phone'         => $this->string()->comment('Телефон'),
            'otherContacts' => $this->string(1000)->comment('Дополнительная контактная информация'),
            'comment'       => $this->string(1000)->comment('Комментарий'),
            'createdAt'     => $this->integer()->comment('Дата создания'),
            'updatedAt'     => $this->integer()->comment('Дата последнего изменения'),
            'authorId'      => $this->integer()->comment('Автор'),
            'updaterId'     => $this->integer()->comment('Последний редактор'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%contractor}}');
    }
}
