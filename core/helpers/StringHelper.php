<?php

namespace core\helpers;

class StringHelper
{
    public static function toLatin($str)
    {
        $tr = array(
            "Й"=>"q","Ц"=>"w","У"=>"e","К"=>"r","Е"=>"t","Н"=>"y","Г"=>"u","Ш"=>"i","Щ"=>"o","З"=>"p","Ф"=>"a","Ы"=>"s","В"=>"d","А"=>"f","П"=>"g","Р"=>"h","О"=>"j","Л"=>"k","Д"=>"l","Я"=>"z","Ч"=>"x","С"=>"c","М"=>"v","И"=>"b","Т"=>"n","Ь"=>"m",
            "й"=>"q","ц"=>"w","у"=>"e","к"=>"r","е"=>"t","н"=>"y","г"=>"u","ш"=>"i","щ"=>"o","з"=>"p","ф"=>"a","ы"=>"s","в"=>"d","а"=>"f","п"=>"g","р"=>"h","о"=>"j","л"=>"k","д"=>"l","я"=>"z","ч"=>"x","с"=>"c","м"=>"v","и"=>"b","т"=>"n","ь"=>"m"
        );

        return strtr($str,$tr);
    }

    public static function toCamelCase($str)
    {
        return lcfirst(str_replace(' ', '', ucwords(strtr($str, '_-', ' '))));
    }

    public static function toSnakeCase($str)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $str));
    }

    public static function getShortClassName($className) {
        $namespaceParts = explode('\\', $className);

        return array_pop($namespaceParts);
    }

    public static function isUrl($str)
    {
        return (bool)filter_var($str, FILTER_VALIDATE_URL);
    }
}