<?php

namespace core\helpers;

/**
 * Трейт для логирования времени, вывода системных сообщений в консольных контроллерах
 *
 * Trait ConsoleLogTrait
 * @package core\helpers
 */
trait ConsoleLogTrait
{
    private $consoleErrors = [];

    private $consoleSuccess = [];

    /**
     * Вывести сообщение в консоль
     * @param $message
     */
    private function echo($message)
    {
        echo $message . PHP_EOL;
    }

    /**
     * Добавить сообщение об ошибке
     * @param $error
     */
    private function addError($error)
    {
        $this->consoleErrors[] = $error . PHP_EOL;
    }

    /**
     * Отобразить ошибки
     */
    private function displayErrors()
    {
        if (!empty($this->consoleErrors)) {
            foreach ($this->consoleErrors as $error) {
                $this->echo($error);
            }
            $this->echo('Всего ошибок :' . count($this->consoleErrors));
        } else {
            $this->echo('0 ошибок');
        }
    }

    /**
     * Добавить сообщение об успехе
     * @param $success
     */
    private function addSuccess($success)
    {
        $this->consoleSuccess[] = $success . PHP_EOL;
    }

    /**
     * Отобразить сообщения об успехе
     */
    private function displaySuccess()
    {
        if (!empty($this->consoleSuccess)) {
            foreach ($this->consoleSuccess as $success) {
                $this->echo($success);
            }
            $this->echo('Всего успешно выполнено :' . count($this->consoleSuccess));
        } else {
            $this->echo('0 успешных операций');
        }
    }
}