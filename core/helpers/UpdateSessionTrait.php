<?php

namespace core\helpers;

use Yii;

/**
 * Даный трейт предназначен для ведения в yii/web/controllers для записи и извлечения занчений в сессии, которые приходят от пользоателя
 *
 * Trait UpdateSessionTrait
 * @package core\helpers
 */
trait UpdateSessionTrait {

    /**
     * Функция устанавливает и затем возвращает значение переданного параметра, который нужно сохранить в сессиию, по его наименованию.
     *
     * @param $name string [ Ключ значения в массиве сессии ]
     * @param $pageFlag [ Флаг, который нам однозначно утверждает, что это значение пришло со страницы формы ]
     * @param bool $defaultValue [ Значение по умолчанию, если ничего не извлечено и ничего не установлено не было ]
     * @return array|mixed
     */
    private function _updateSession($name, $pageFlag, $defaultValue = false)
    {
        $session      = Yii::$app->session;
        $remember     = Yii::$app->request->get($pageFlag) ?: Yii::$app->request->post($pageFlag);
        $sessionValue = !$remember ? $session->get($name, $defaultValue) : (Yii::$app->request->get($name, false) ?: Yii::$app->request->post($name, false));
        $session->set($name, $sessionValue);

        return $sessionValue;
    }
}