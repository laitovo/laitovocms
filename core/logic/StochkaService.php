<?php

namespace core\logic;

use common\models\laitovo\ErpNaryad;
use common\models\laitovo\ErpUser;
use Yii;

/**
 * Class StochkaService
 * @package core\logic
 *
 * Класс отвечает за логику работы со сточкой резинки на участке швейка
 */
class StochkaService
{
    /**
     * @param ErpNaryad $workOrder
     * @param ErpUser $user
     * @return boolean
     */
    public static function checkAndSetValue(ErpNaryad $workOrder, ErpUser $user)
    {
        //Костыль для сточки резинки
        if ($workOrder->location_id == Yii::$app->params['erp_shveika']) {
            $workplaces = $user->recieveWorkplacesByLocation(Yii::$app->params['erp_shveika']);
            if (!empty($workplaces) && ($workplace = array_shift($workplaces)) && $workplace->property == 'Сточка резинки') {
                $workOrder->stochka = true;
                $workOrder->start = Yii::$app->params['erp_shveika'];
                return true;
            }
        }
        return false;
    }
}