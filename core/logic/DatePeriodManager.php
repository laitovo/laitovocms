<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 7/31/18
 * Time: 4:53 PM
 */

namespace core\logic;


use common\models\laitovo\Weekend;
use yii\helpers\ArrayHelper;

class DatePeriodManager
{
    ### ИНИЦИАЛИЗАЦИЯ КЛАССА #######################################################

    /**
     * @var DatePeriodManager Функция для хранения экземпляра этого класса.
     */
    private  static  $_instance = null;

    /**
     * @return DatePeriodManager Функция возвращает экземпляр класса
     */
    private static function _getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {}
    private function __wakeup() {}
    private function __construct() {}

    ### ПЕРЕМЕННЫЕ ####################################################################################################


    ### ИНТЕРФЕЙС #####################################################################################################

    //Самая главная функция которая мне нужна - это передать в timestamp две даты и получить мнежду ними разницу


    /**
     * Функция для получения рабочего времени между двумя отметками времени.
     *
     * @param $timestamp1
     * @param $timestamp2
     * @return false|float|int
     * @throws \Exception
     */
    public static function getDiffOfDates($timestamp1, $timestamp2)
    {
        $handler = self::_getInstance();
        return $handler->_getDiffOfDates($timestamp1, $timestamp2);
    }


    //Какие функции нам понадобятся ????
    //Нам понадобится функция которая объеденяет в группу несколько upn

    ### ЯДРО #####################################################################################################

    /**
     * Функция возвращает разнцицу между датами в формте Timestamp в секундах, с учетом проставленных выходных дней в системе.
     * В качестве аргумента принимает 2 отметки времени. С учетом выходных возвращаем рабочее время в секнудах
     *
     * @param $timestamp1
     * @param $timestamp2
     * @return false|float|int
     * @throws \Exception
     */
    private function _getDiffOfDates($timestamp1, $timestamp2)
    {
        //Определяем все выходные, которые у нас есть
        $weekend = Weekend::find()->all();
        $arWeekend = ArrayHelper::map($weekend,'id','weekend');

        //Процесс получения начала периода
        if ( in_array( ($date = date('Y-m-d',$timestamp1)), $arWeekend ) ) {
            $timestamp1 = strtotime('+1 day', strtotime(date('Y-m-d',$timestamp1))); //Получаем отметку времени завтра
        }
        $dateFrom = date('Y-m-d H:i:s',$timestamp1);
        $from = new \DateTime($dateFrom);

        //Процесс получения конца периода
        if ( in_array( ($date = date('Y-m-d',$timestamp2)), $arWeekend ) ) {
            $timestamp2 = strtotime(date('Y-m-d',$timestamp2)); //Получаем отметку времени завтра
        }
        $dateTo = date('Y-m-d H:i:s',$timestamp2);
        $to = new \DateTime($dateTo);

        $period = new \DatePeriod($from, new \DateInterval('P1D'), $to);

        //Получаем даты в периоде
        $arrayOfDates = array_map(
            function($item){return $item->format('Y-m-d');},
            iterator_to_array($period)
        );

        //Количество выходных дней в периоде
        $count = 0;
        foreach ($arrayOfDates as $oneDate) {
            if (in_array($oneDate,$arWeekend)) {
                $count++;
            }
        }
        //Вычитаемое время выходных дней
        $deductibleTime = (60*60*24) * $count;

        //Результирующее время
        $resultTime = $timestamp2 - $timestamp1 - $deductibleTime;

        return $resultTime;
    }


































}