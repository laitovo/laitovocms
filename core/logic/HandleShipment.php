<?php

namespace core\logic;

use backend\modules\laitovo\models\ErpNaryad;
use core\entities\logisticsShipment\models\Shipment;
use core\services\SBox;
use core\services\SShipmentSearch;
use Yii;
use Exception;

/**
 * Обработка отгрузок
 *
 * Class HandleShipment
 * @package core\logic
 */
class HandleShipment
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    /**
     * Перевод отгрузки в статус "Обработана".
     * Это означает, что логист завершил работу над отгрузкой.
     * Данная операция необратима. По идее, после этого логист не может редактировать отгрузку, она переходит к кладовщикам.
     *
     * @param Shipment $shipment
     * @param bool $generateNumber
     * @return bool
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function handle($shipment,$generateNumber = false)
    {
        $this->_instanceOf($shipment);
        if (!$this->_canBeHandled($shipment)) {
            return false;
        }
        /**
         * При отгрузке заказа , если отгрузке необходимо сформировать официальный номер, тогда мы его формируем
         */
        if ($generateNumber && ($numObj = $this->_numObj()) && (!$this->_shipmentManager->getOfficialNumber($shipment) || !$this->_shipmentManager->getOfficialNumberYear($shipment)) ) {
            $this->_shipmentManager->setOfficialNumber($shipment, $numObj->number);
            $this->_shipmentManager->setOfficialNumberYear($shipment, $numObj->year);
        }

        $this->_shipmentManager->setLogisticsStatus($shipment, $shipment::LOGISTICS_STATUS_HANDLED);
        $this->_shipmentManager->setHandledAt($shipment, time());
        $this->_checkOnEmptyBox($shipment);
        $this->_setBoxesReadyToPack($shipment);
        if ($this->_checkOnPackedShipment($shipment))
        {
            $this->_shipmentManager->setPackedAt($shipment, time());
            $this->_shipmentManager->setLogisticsStatus($shipment, $shipment::LOGISTICS_STATUS_PACKED);
        };

        /**
         * При отгрузке заказа по актам, заказ при обработке становиться сразу упакованным
         */
        $this->_moveActPositions($shipment);

        /**
         * Простановка строки поиска
         */
        $searchStr = SShipmentSearch::generateSearchString($this->_shipmentManager->getId($shipment));
        $this->_shipmentManager->setSearchString($shipment, $searchStr);

        return $this->_shipmentManager->save($shipment);
    }

    /**
     * Возвращаем объект с годом и следующим порядковым номером для накладной
     * @return object
     */
    private function _numObj()
    {
        $year = date('Y',time());
        $number = $this->_shipmentManager->maxWhere(['officialNumberYear' => $year],'officialNumber');
        $data = [
            'year' => $year,
            'number' => ($number ? ($number + 1) : 1),
        ];
        return (object)$data;
    }

    /**
     * Проверка, что отгрузка может быть переведена в статус "Обработана".
     * Результат проверки зависит от текущего статуса отгрузки, а также от заполненности необходимых полей.
     * Кроме того, все позиции отгрузки должны быть на складе.
     *
     * @param Shipment $shipment
     * @return bool
     */
    private function _canBeHandled($shipment)
    {
        if (!$this->_shipmentManager->isBeforeHandling($shipment)) {
            return false;
        }
        if (!$this->_shipmentManager->getRelatedTransportCompany($shipment)) {
            return false;
        }
        if (!$this->_shipmentManager->getShipmentDate($shipment)) {
            return false;
        }
        if (!$this->_shipmentManager->getTrackCode($shipment)) {
            return false;
        }
        if ($this->_shipmentManager->getHasWeighing($shipment) && !$this->_shipmentManager->getWeight($shipment)) {
            return false;
        }
        if ($this->_shipmentManager->getHasSizing($shipment) && !$this->_shipmentManager->getSize($shipment)) {
            return false;
        }
        if (!$this->_shipmentManager->getIsDeliveryFree($shipment) && !$this->_shipmentManager->getDeliveryPrice($shipment)) {
            return false;
        }
        if (!$this->_areAllPositionsOnStorage($shipment)) {
            return false;
        }

        return true;
    }

    private function _areAllPositionsOnStorage($shipment)
    {
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        foreach ($orderEntries as $orderEntry) {
            $isOnStorage = !empty($orderEntry->upn->storageState) || ErpNaryad::find()->where(['logist_id' => $orderEntry->upn->id])->andWhere(['status' => ErpNaryad::STATUS_READY])->exists();
            if (!$isOnStorage) {
                return false;
            }
        }

        return true;
    }

    /**
     * Для позиций заказа, которые двигались по акту приема передачи, мы ставим эти позиции сразу упакованными.
     *
     * @param $shipment
     */
    private function _moveActPositions($shipment)
    {
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        $count = 0;
        $boxes = [];
        foreach ($orderEntries as $orderEntry) {
            if (($upn = $orderEntry->upn) && ($workOrder = $upn->erpNaryad) && $workOrder->actNumber) {
                $upn->packed = true;
                $upn->save();
                $count++;
                if (!in_array($orderEntry->boxId, $boxes)) {
                    $boxes[] = $orderEntry->boxId;
                }
            }
        }

        if (count($orderEntries) == $count) {
            $this->_shipmentManager->setLogisticsStatus($shipment, $shipment::LOGISTICS_STATUS_PACKED);
            $this->_shipmentManager->setPackedAt($shipment, time());
            foreach ($boxes as $boxId) {
                SBox::pack($boxId);
            }
        }
    }

    /**
     * Проверяем есть ли элементы без коробки и в случае чего создаем коробку под позиции.
     *
     * @param $shipment
     * @throws Exception
     * @throws \yii\db\Exception
     */
    private function _checkOnEmptyBox($shipment)
    {
        $elements = [];
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        foreach ($orderEntries as $orderEntry) {
            if (!$orderEntry->boxId) {
                $elements[] = $orderEntry;
            }
        }
        if (!empty($elements) && !SBox::boxed($elements))
            throw new Exception('Не получилось создать коробку под все элементы');
    }

    /**
     * @param $shipment
     * @throws Exception
     */
    private function _setBoxesReadyToPack($shipment)
    {
        $boxes    = [];
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        foreach ($orderEntries as $orderEntry) {
            if (!($box = $orderEntry->boxId))
                throw new Exception('Не все позиции распределены по коробкам');
            if (!in_array($box,$boxes))
                $boxes[] = $box;
        }
        if (!empty($boxes) && !SBox::setReadyToPack($boxes))
            throw new Exception('Не получилось коробки поставить в статус - готовы к упаковке');
    }

    /**
     * @param $shipment
     * @return bool
     * @throws Exception
     */
    private function _checkOnPackedShipment($shipment)
    {
        $boxes    = [];
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        foreach ($orderEntries as $orderEntry) {
            if (!($box = $orderEntry->boxId))
                throw new Exception('Не все позиции распределены по коробкам');
            if (!in_array($box,$boxes))
                $boxes[] = $box;
        }

        return SBox::isAllPacked($boxes);
    }


    /**
     * @param $shipment
     * @throws Exception
     */
    private function _instanceOf($shipment)
    {
        if (!($shipment instanceof Shipment)) {
            throw new Exception('В сервис для обработки отгрузок передан неверный тип объекта');
        }
    }
}