<?php

namespace core\logic;

use core\entities\logisticsShipment\models\Shipment;
use Yii;
use Exception;

/**
 * Упаковка отгрузок
 *
 * Class PackShipment
 * @package core\logic
 */
class PackShipment
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    /**
     * Перевод отгрузки в статус "Упакована".
     *
     * @param Shipment $shipment
     * @return bool Результат операции
     */
    public function pack($shipment)
    {
        $this->_instanceOf($shipment);
        if (!$this->_canBePacked($shipment)) {
            return false;
        }
        $this->_shipmentManager->setLogisticsStatus($shipment, $shipment::LOGISTICS_STATUS_PACKED);
        $this->_shipmentManager->setPackedAt($shipment, time());

        return $this->_shipmentManager->save($shipment);
    }

    /**
     * Проверка, может ли быть отгрузка переведена в статус "Упакована".
     * Отгрузка может быть упакована только после обработки логистом.
     * Кроме того, должны быть упакованы все позиции (исключение - отгрузки, где все позиции со склада).
     *
     * @param Shipment $shipment
     * @return bool
     */
    private function _canBePacked($shipment)
    {
        if (!$this->_shipmentManager->isHandled($shipment)) {
            return false;
        }
        if ($this->_isHasUpns($shipment) && !$this->_allPositionsArePacked($shipment)) {
            return false;
        }

        return true;
    }

    private function _isHasUpns($shipment)
    {
        foreach ($this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment) as $orderEntry) {
            if ($orderEntry->upn) {
                return true;
            }
        }

        return false;
    }

    /**
     * Проверка, что все позиции отгрузки упакованы
     *
     * @param Shipment $shipment
     * @return bool
     */
    private function _allPositionsArePacked($shipment)
    {
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        foreach ($orderEntries as $orderEntry) {
            $isPacked = $orderEntry->upn->packed ?? false;
            if (!$isPacked) {
                return false;
            }
        }

        return true;
    }

    private function _instanceOf($shipment)
    {
        if (!($shipment instanceof Shipment)) {
            throw new Exception('В сервис для упаковки отгрузок передан неверный тип объекта');
        }
    }
}