<?php
namespace core\logic;

use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use core\models\article\Article;
use Yii;


/**
 * Service для работы с моделью StorageState
 *
 * Class SStorageState
 * @package core\logic
 */
class SStorageState
{
    public static function addStateForUpn($upnId,$sump = false)
    {
        $storage = null;
        $prod = null;

        $upn = Naryad::find()->where(['id' => $upnId])->one();
        if (!$upn) return false;

        /**
         * @var $upn Naryad
         */
        $article = $upn->article;

        //Статситику по артикулу
        $objArticle = new Article($article);


        //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
        if (!$sump) {
            $storageType = Storage::TYPE_REVERSE;
        }else{
            $storageType = Storage::TYPE_SUMP;
        }

        $productId = $objArticle->product ? $objArticle->product->id : null;
        if ($productId) {
            $storage = Storage::findStorageForProduct($productId,$storageType);
        }
        if (!$storage) {
            return null;
        }

        $literal = $storage->getActualLiteral($productId);

        $transaction = \Yii::$app->db->beginTransaction();

        //Содание UPN для наряда
        $logist = new Naryad();
        $logist->article = $article;
        $logist->team_id = Yii::$app->team->getId();
        $logist->source_id = 2;
        if (!$logist->save()) {
            $transaction->rollback();
            return null;
        }

        //Ставим, что данный наряд распределен с диспечер-склад
        $row = new StorageState();
        $row->upn_id = $logist->id;
        $row->storage_id = $storage->id;
        $row->literal = $literal;
        if (!$row->save()) {
            $transaction->rollback();
            return null;
        }

        $transaction->commit();

        return $row;
    }

    public static function addStateForArticle($article,$sump = false)
    {
        $storage = null;
        $prod = null;

        //Статситику по артикулу
        $objArticle = new Article($article);

        //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
        if (!$sump) {
            $storageType = Storage::TYPE_REVERSE;
        }else{
            $storageType = Storage::TYPE_SUMP;
        }

        $productId = $objArticle->product ? $objArticle->product->id : null;
        if ($productId) {
            $storage = Storage::findStorageForProduct($productId,$storageType);
        }
        if (!$storage) {
            return null;
        }

        $literal = $storage->getActualLiteral($productId);

        $transaction = \Yii::$app->db->beginTransaction();

        //Содание UPN для наряда
        $logist = new Naryad();
        $logist->article = $article;
        $logist->team_id = Yii::$app->team->getId();
        $logist->source_id = 2;
        if (!$logist->save()) {
            $transaction->rollback();
            return null;
        }

        //Ставим, что данный наряд распределен с диспечер-склад
        $row = new StorageState();
        $row->upn_id = $logist->id;
        $row->storage_id = $storage->id;
        $row->literal = $literal;
        if (!$row->save()) {
            $transaction->rollback();
            return null;
        }
        $transaction->commit();

        return $row;
    }
}