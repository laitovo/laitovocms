<?php

namespace core\logic;

use yii\helpers\ArrayHelper;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\models\Naryad;
use Yii;

/**
 * Получение информации об остатках на складе
 *
 * Class GetFactBalance
 * @package core\logic
 */
class GetStorageBalance
{
    private $_articlesData;
    private $_storageProductTypeManager;

    public function __construct()
    {
        $this->_storageProductTypeManager = Yii::$container->get('core\entities\logisticsStorageProductType\StorageProductTypeManager');
    }

    /**
     * Получение фактического остатка по артикулу
     * (только для позиций со склада)
     *
     * @param string $article
     * @return int
     */
    public function getFactBalanceByArticle($article)
    {
        if (!CheckStoragePosition::isStorageArticle($article)) {
            return 0;
        }
        $this->_initArticlesData();

        return (int)($this->_articlesData[$article]['factBalance'] ?? 0);
    }

    /**
     * Получение фактического остатка по артикулу
     * (только для позиций со склада)
     *
     * @param string $article
     * @return int
     */
    public function getFactBalanceReservedByArticle($article)
    {
        if (!CheckStoragePosition::isStorageArticle($article)) {
            return 0;
        }
        $this->_initArticlesData();

        return (int)($this->_articlesData[$article]['factBalanceReserved'] ?? 0);
    }

    /**
     * Получение минимального остатка по артикулу
     * (только для позиций со склада)
     *
     * @param string $article
     * @return int
     */
    public function getMinBalanceByArticle($article)
    {
        if (!CheckStoragePosition::isStorageArticle($article)) {
            return 0;
        }
        $this->_initArticlesData();

        return (int)($this->_articlesData[$article]['minBalance'] ?? 0);
    }

    /**
     * Проверка, включен ли для артикула контроль остатков
     * (только для позиций со склада)
     *
     * @param string $article
     * @return bool
     */
    public function isArticleOnBalanceControl($article)
    {
        if (!CheckStoragePosition::isStorageArticle($article)) {
            return false;
        }
        $this->_initArticlesData();

        return (int)($this->_articlesData[$article]['hasBalanceControl'] ?? false);
    }

    /**
     * Получение полного списка артикулов для контроля остатков
     *
     * @return array Массив элементов вида ["article" => "12345", "minBalance" => 100, "factBalance" => 99]
     */
    public function getBalanceList()
    {
        $this->_initArticlesData();

        return $this->_articlesData;
    }

    /**
     * Получение списка артикулов, по которым фактический остаток меньше минимального
     *
     * @return array Массив элементов вида ["article" => "12345", "minBalance" => 100, "factBalance" => 99]
     */
    public function getLowBalanceList()
    {
        $this->_initArticlesData();
        $data = [];
        foreach ($this->_articlesData as $articleData) {
            if ($articleData['hasBalanceControl'] && ($articleData['factBalance'] < $articleData['minBalance'])) {
                $data[] = $articleData;
            }
        }

        return $data;
    }

    private function _initArticlesData()
    {
        if ($this->_articlesData) {
            return;
        }
        $this->_articlesData = [];
        $storageProductTypes = $this->_storageProductTypeManager->findWhere(['and',['hasBalanceControl' => true],['is not','minBalance',null],['>','minBalance',0]]);
        $articles = ArrayHelper::map($storageProductTypes,'article','article');
        $storageProductTypes = ArrayHelper::index($storageProductTypes,'article');

        $storagesArray = Storage::find()->where(['in','type',[Storage::TYPE_REVERSE,Storage::TYPE_TIME]])->all();
        $idsSt = ArrayHelper::map($storagesArray,'id','id');
        $query = StorageState::find()->alias('t')->joinWith('upn')->where(['in','t.storage_id',$idsSt])->andwhere(['in','logistics_naryad.article',$articles]);
        $query->select(['upn_id']);
        $rows = $query->all();
        $ids = ArrayHelper::map($rows,'upn_id','upn_id');

        $query = Naryad::find();
        $query->where(['in','id',$ids]);
        $query->andWhere(['reserved_id' => null]);
        $query->groupBy(['article']);
        $query->select(['article','factBalance' => 'COUNT(article)']);
        $rows = $query->asArray()->all();
        $rows = ArrayHelper::index($rows,'article');

        $storagesArray = Storage::find()->where(['in','type',[Storage::TYPE_REVERSE,Storage::TYPE_TIME,Storage::TYPE_TRANSIT]])->all();
        $idsSt = ArrayHelper::map($storagesArray,'id','id');
        $query = StorageState::find()->alias('t')->joinWith('upn')->where(['in','t.storage_id',$idsSt])->andwhere(['in','logistics_naryad.article',$articles]);
        $query->select(['upn_id']);
        $rows2 = $query->all();
        $ids = ArrayHelper::map($rows2,'upn_id','upn_id');

        $query = Naryad::find();
        $query->where(['in','id',$ids]);
        $query->andWhere(['is not','reserved_id', null]);
        $query->groupBy(['article']);
        $query->select(['article','factBalance' => 'COUNT(article)']);
        $rows2 = $query->asArray()->all();
        $rows2 = ArrayHelper::index($rows2,'article');

        foreach ($articles as $key => $value) {
            $this->_articlesData[$key]['title']                = $storageProductTypes[$key]->title ?? null;
            $this->_articlesData[$key]['article']              = $key;
            $this->_articlesData[$key]['factBalance']          = $rows[$key]['factBalance']??0;
            $this->_articlesData[$key]['factBalanceReserved']  = $rows2[$key]['factBalance']??0;
            $this->_articlesData[$key]['hasBalanceControl']    = $storageProductTypes[$key]->hasBalanceControl ?? null;
            $this->_articlesData[$key]['minBalance']           = $storageProductTypes[$key]->minBalance ?? null;
        }
    }
}