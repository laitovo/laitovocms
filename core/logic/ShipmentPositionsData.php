<?php

namespace core\logic;

use backend\modules\logistics\models\OrderEntry;
use core\entities\logisticsShipment\models\Shipment;
use Yii;

/**
 * Класс предоставляет сводную информацию о позициях отгрузки
 *
 * Class ShipmentPositionsData
 * @package core\logic
 */
class ShipmentPositionsData
{
    private function __construct() {}

    /**
     * Сумма позиций
     *
     * @param Shipment $shipment
     * @return double
     */
    public static function getSumPrice(Shipment $shipment)
    {
        $sum = 0;
        foreach (self::_getPositions($shipment) as $orderEntry) {
            $sum += $orderEntry->price;
        }

        return $sum;
    }

    /**
     * Количество позиций
     *
     * @param Shipment $shipment
     * @return int
     */
    public static function getCount(Shipment $shipment)
    {
        return count(self::_getPositions($shipment));
    }

    /**
     * Оплаченная сумма
     *
     * @param Shipment $shipment
     * @return double
     */
    public static function getSumPaid(Shipment $shipment)
    {
        $orders = [];
        foreach (self::_getPositions($shipment) as $orderEntry) {
            if (!isset($orders[$orderEntry->order_id])) {
                $order = $orderEntry->order;
                $orders[$orderEntry->order_id] = $order->paid;
            }
        }
        $sum = array_sum($orders);

        return $sum;
    }

    /**
     * Сумма, оплаченная бонусами
     *
     * @param Shipment $shipment
     * @return double
     */
    public static function getSumBonus(Shipment $shipment)
    {
        $orders = [];
        foreach (self::_getPositions($shipment) as $orderEntry) {
            if (!isset($orders[$orderEntry->order_id])) {
                $order = $orderEntry->order;
                $orders[$orderEntry->order_id] = $order->bonus;
            }
        }
        $sum = array_sum($orders);

        return $sum;
    }

    /**
     * @param Shipment $shipment
     * @return OrderEntry[]
     */
    private static function _getPositions($shipment)
    {
        $shipmentManager = self::_getShipmentManager();
        if (!$shipmentManager->isCanceled($shipment)) {
            return $shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
        } else {
            return $shipmentManager->getRelatedOrderEntriesFromLogNotDeleted($shipment);
        }
    }

    private static function _getShipmentManager()
    {
        return Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }
}