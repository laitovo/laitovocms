<?php

namespace core\logic;

use core\entities\logisticsShipment\models\Shipment;
use Yii;
use Exception;

/**
 * Отмена отгрузок
 *
 * Class CancelShipment
 * @package core\logic
 */
class CancelShipment
{
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    /**
     * Отмена отгрузки (данная операция необратима):
     * 1). Перевод отгрузки в статус "Отменена";
     * 2). Запись лога, чтобы помнить какие позиции были в отгрузке до отмены;
     * 3). Удаление позиций из отгрузки.
     *
     * @param Shipment $shipment
     * @return bool Результат операции
     */
    public function cancel($shipment)
    {
        $this->_instanceOf($shipment);
        if (!$this->_canBeCanceled($shipment)) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        $this->_shipmentManager->setLogisticsStatus($shipment, $shipment::LOGISTICS_STATUS_CANCELED);
        $this->_shipmentManager->setCancelLog($shipment, $this->_generateCancelLog($shipment));
        if (!$this->_shipmentManager->save($shipment)) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->_deletePositions($shipment)) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();

        return true;
    }

    /**
     * Проверка, может ли отгрузка быть отменена.
     * Отгрузка может быть отменена до момента, когда товар окажется у кладовщиков, и пойдут необратимые процессы отправки (составление актов, ...).
     * Иными словами, нет смысла отменять отгрузку, если заказ уже уехал.
     *
     * @param Shipment $shipment
     * @return bool
     */
    private function _canBeCanceled($shipment)
    {
        if (!$this->_shipmentManager->isBeforeHandling($shipment)) {
            return false;
        }

        return true;
    }

    /**
     * Функция генерирует значение для поля "cancelLog"
     *
     * @param Shipment $shipment
     * @return string
     */
    private function _generateCancelLog($shipment)
    {
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntries($shipment);
        $ids = [];
        foreach ($orderEntries as $orderEntry) {
            $ids[] = $orderEntry->id;
        }

        return json_encode(['orderEntryIds' => $ids]);
    }

    /**
     * Отвязка позиций от отгрузки
     *
     * @param Shipment $shipment
     * @return bool Результат операции (false, если что-то не удалось отвязать)
     */
    private function _deletePositions($shipment)
    {
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntries($shipment);
        foreach ($orderEntries as $orderEntry) {
            $orderEntry->shipment_id = null;
            if (!$orderEntry->save()) {
                return false;
            }
        }

        return true;
    }

    private function _instanceOf($shipment)
    {
        if (!($shipment instanceof Shipment)) {
            throw new Exception('В сервис для отмены отгрузок передан неверный тип объекта');
        }
    }
}