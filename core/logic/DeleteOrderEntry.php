<?php

namespace core\logic;

use backend\modules\logistics\models\OrderEntry;
use Yii;
use Exception;

/**
 * Удаление логистических позиций
 *
 * Class DeleteOrderEntry
 * @package core\logic
 */
class DeleteOrderEntry
{
    private $_shipmentManager;
    private $_cancelShipment;
    private $_reserveUpn;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_cancelShipment  = Yii::$container->get('core\logic\CancelShipment');
        $this->_reserveUpn      = Yii::$container->get('core\logic\ReserveUpn');
    }

    /**
     * Удаление позиции:
     * 1). Непосредственное удаление позиции. При этом позиция не удаляется физически, а просто заполняются специальные поля.
     *     Такой подход позволяет восстановить её при необходимости;
     * 2). Снятие с резерва UPN;
     * 3). Проверка отгрузки. Отгрузка, в которой все позиции удалены, автоматически отменяется.
     *
     * @param OrderEntry $orderEntry
     * @return array
     */
    public function delete($orderEntry)
    {
        $this->_instanceOf($orderEntry);
        if (!$this->canBeDeleted($orderEntry)) {
            return [
                'status'  => false,
                'message' => Yii::t('app', 'Данная позиция не может быть удалена')
            ];
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->_deleteOrderEntry($orderEntry)) {
            $transaction->rollBack();
            return [
                'status'  => false,
                'message' => Yii::t('app', 'Не удалось удалить позицию')
            ];
        }
        if (!$this->_dereserveUpn($orderEntry)) {
            $transaction->rollBack();
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось снять UPN с резерва')
            ];
        }
        $shipment = $this->_getShipment($orderEntry->shipment_id);
        if ($shipment && $this->_areAllPositionsDeleted($shipment) && !$this->_cancelShipment($shipment)) {
            $transaction->rollBack();
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось отменить приказ на отгрузку')
            ];
        }
        $transaction->commit();

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    /**
     * Проверка, может ли позиция быть удалена.
     * Позиция может быть удалена до момента, когда она окажется у кладовщиков, и пойдут необратимые процессы отправки (составление актов, ...).
     * Иными словами, нет смысла удалять позицию, если заказ уже уехал.
     *
     * @param OrderEntry $orderEntry
     * @return bool
     */
    public function canBeDeleted($orderEntry)
    {
        $this->_instanceOf($orderEntry);
        $shipment = $this->_shipmentManager->findById($orderEntry->shipment_id);
        if ($shipment && !$this->_shipmentManager->isBeforeHandling($shipment)) {
            return false;
        }

        return true;
    }

    /**
     * Восстановление позиции:
     * 1). Непосредственное восстановление позиции (обнуление специальных полей);
     * 2). Резервирование UPN.
     *
     * @param OrderEntry $orderEntry
     * @return array
     */
    public function restore($orderEntry)
    {
        $this->_instanceOf($orderEntry);
        if (!$this->canBeRestored($orderEntry)) {
            return [
                'status'  => false,
                'message' => Yii::t('app', 'Данная позиция не может быть восстановлена')
            ];
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->_restoreOrderEntry($orderEntry)) {
            $transaction->rollBack();
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось восстановить позицию')
            ];
        }
        if (!$this->_reserveUpn($orderEntry)) {
            $transaction->rollBack();
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось зарезервировать UPN')
            ];
        }
        $transaction->commit();

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    /**
     * Проверка, может ли позиция быть восстановлена.
     * Позиция может быть восстановлена до момента, когда она окажется у кладовщиков, и пойдут необратимые процессы отправки (составление актов, ...).
     * Иными словами, нет смысла восстанавливать позицию, если заказ уже уехал.
     *
     * @param OrderEntry $orderEntry
     * @return bool
     */
    public function canBeRestored($orderEntry)
    {
        $this->_instanceOf($orderEntry);
        $shipment = $this->_shipmentManager->findById($orderEntry->shipment_id);
        if ($shipment && !$this->_shipmentManager->isBeforeHandling($shipment)) {
            return false;
        }

        return true;
    }

    private function _deleteOrderEntry($orderEntry)
    {
        $orderEntry->is_deleted = true;
        $orderEntry->deleted_at = time();
        $orderEntry->deleted_by = Yii::$app->user->id;

        return $orderEntry->save();
    }

    private function _restoreOrderEntry($orderEntry)
    {
        $orderEntry->is_deleted = null;
        $orderEntry->deleted_at = null;
        $orderEntry->deleted_by = null;

        return $orderEntry->save();
    }

    private function _reserveUpn($orderEntry)
    {
        return $this->_reserveUpn->reserve($orderEntry)['status'];
    }

    private function _dereserveUpn($orderEntry)
    {
        return $this->_reserveUpn->dereserve($orderEntry)['status'];
    }

    private function _getShipment($id)
    {
        return $this->_shipmentManager->findById($id);
    }

    private function _areAllPositionsDeleted($shipment)
    {
        $orderEntries = $this->_shipmentManager->getRelatedOrderEntries($shipment);
        if (!count($orderEntries)) {
            return false;
        }
        foreach ($orderEntries as $orderEntry) {
            if (!$orderEntry->is_deleted) {
                return false;
            }
        }

        return true;
    }

    private function _cancelShipment($shipment)
    {
        return $this->_cancelShipment->cancel($shipment);
    }

    private function _instanceOf($orderEntry)
    {
        if (!($orderEntry instanceof OrderEntry)) {
            throw new Exception('В сервис для удаления логистической позиции передан неверный тип объекта');
        }
    }
}