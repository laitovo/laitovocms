<?php

namespace core\logic;

use common\models\logistics\Naryad;
use common\models\logistics\Storage;
use common\models\logistics\StorageState;
use core\models\article\Article;
use core\models\shipment\Shipment;

class RelevanceReport
{
    /**
     * Функция генерации данных, которые будет дергать консольное приложение
     */
    public static function generateData($storageId)
    {
        $tableName = $storageId == 1 ? 'tmp_relevance_report_main_storage' : 'tmp_relevance_report';


        if (!Storage::find()->where(['id' => $storageId])->exists())
            throw new DomainExecption("Склад с идентификатором $storageId не найден в системе");

        $data = [];

        /**
         * Находим все позиции на складе
         */
        $positions = StorageState::find()
            ->joinWith('upn')
            ->where(['logistics_naryad.reserved_id' => null])
            ->andWhere(['logistics_storage_state.storage_id' => $storageId])
            ->andWhere(['<','logistics_naryad.created_at', time() - 3600*24*1.5*365])
            ->all();

        foreach ($positions as $position) {
            $item = [];
            $objArticle = new Article($position->upn->article);
            if (!$objArticle->carArticle || !$objArticle->product) continue;
            $item['storage_id'] = $storageId;
            $item['upn']        = $position->upn->id;
            $item['article']    = $objArticle->id;
            $item['title']      = $objArticle->product->title;
            $item['window']     = $objArticle->window;
            $item['cars']       = implode(' || ',$objArticle->carNameWithAnalogs);

            $item['soldArticle'] = Shipment::find()
                ->joinWith('notDeletedOrderEntries entries')
                ->where(['logisticsStatus' => Shipment::LOGISTICS_STATUS_SHIPPED])
                ->andWhere(['>=','shipmentDate',time()-3600*24*365*1.5])
                ->andWhere(['entries.article' => $objArticle->articleWithAnalogs])
                ->count();

            $soldWindowsQuery = Shipment::find()
                ->joinWith('notDeletedOrderEntries entries')
                ->where(['logisticsStatus' => Shipment::LOGISTICS_STATUS_SHIPPED])
                ->andWhere(['>=','shipmentDate',time()-3600*24*365*1.5]);

            $qArr = ['or'];
            foreach ($objArticle->getCarArticlesWithAnalogs() as $articleCar) {
                $qArr[] = ['like','entries.article',"{$objArticle->windowEn}-%-$articleCar-%",false];
            }
            $item['soldWindow'] = $soldWindowsQuery->andWhere($qArr)->count();

            $soldCarsQuery = Shipment::find()
                ->joinWith('notDeletedOrderEntries entries')
                ->where(['logisticsStatus' => Shipment::LOGISTICS_STATUS_SHIPPED])
                ->andWhere(['>=','shipmentDate',time()-3600*24*365*1.5]);

            $qArr = ['or'];
            foreach ($objArticle->getCarArticlesWithAnalogs() as $articleCar) {
                $qArr[] = ['like','entries.article',"%-%-$articleCar-%",false];
            }

            $soldCarsQuery->andWhere($qArr);

            $item['soldCars'] = $soldCarsQuery->count();

            $item['inStock'] = $objArticle->balanceFact;

            echo 'UPN:' .  $item['upn']. PHP_EOL;

            $data[] = $item;
        }

        \Yii::$app->db->createCommand()->delete($tableName)->execute();
        \Yii::$app->db->createCommand()->batchInsert($tableName,['storage_id', 'upn', 'article','title','window', 'cars', 'soldArticle','soldWindow','soldCars','inStock'] , $data)->execute();
    }

    /** Отчет для дениса на раз */
    public static function generateDataNew($storageId)
    {
        $tableName = $storageId == 1 ? 'tmp_relevance_report_main_storage' : 'tmp_relevance_report';


        if (!Storage::find()->where(['id' => $storageId])->exists())
            throw new DomainExecption("Склад с идентификатором $storageId не найден в системе");

        $data = [];

        $notPositions = Naryad::find()
            ->select('id')
            ->where(['NOT LIKE','article','OT-%',false])
            ->andWhere(['like','comment','%Пополнение склада%',false])
            ->andWhere(['>','created_at',1635827512])
            ->column();
        /**
         * Находим все позиции на складе
         */
        $positions = StorageState::find()
            ->joinWith('upn')
            ->where(['logistics_naryad.reserved_id' => null])
            ->andWhere(['logistics_storage_state.storage_id' => $storageId])
            ->andWhere(['not in', 'logistics_naryad.id', $notPositions])
            ->all();

        foreach ($positions as $position) {
            $item = [];
            $objArticle = new Article($position->upn->article);
            if (!$objArticle->carArticle || !$objArticle->product) continue;
            $item['storage_id'] = $storageId;
            $item['upn']        = $position->upn->id;
            $item['article']    = $objArticle->id;
            $item['title']      = $objArticle->product->title;
            $item['window']     = $objArticle->window;
            $item['cars']       = implode(' || ',$objArticle->carNameWithAnalogs);

            $item['soldArticle'] = Shipment::find()
                ->joinWith('notDeletedOrderEntries entries')
                ->where(['logisticsStatus' => Shipment::LOGISTICS_STATUS_SHIPPED])
                ->andWhere(['>=','shipmentDate',time()-3600*24*365*1.5])
                ->andWhere(['entries.article' => $objArticle->articleWithAnalogs])
                ->count();

            $soldWindowsQuery = Shipment::find()
                ->joinWith('notDeletedOrderEntries entries')
                ->where(['logisticsStatus' => Shipment::LOGISTICS_STATUS_SHIPPED])
                ->andWhere(['>=','shipmentDate',time()-3600*24*365*1.5]);

            $qArr = ['or'];
            foreach ($objArticle->getCarArticlesWithAnalogs() as $articleCar) {
                $qArr[] = ['like','entries.article',"{$objArticle->windowEn}-%-$articleCar-%",false];
            }
            $item['soldWindow'] = $soldWindowsQuery->andWhere($qArr)->count();

            $soldCarsQuery = Shipment::find()
                ->joinWith('notDeletedOrderEntries entries')
                ->where(['logisticsStatus' => Shipment::LOGISTICS_STATUS_SHIPPED])
                ->andWhere(['>=','shipmentDate',time()-3600*24*365*1.5]);

            $qArr = ['or'];
            foreach ($objArticle->getCarArticlesWithAnalogs() as $articleCar) {
                $qArr[] = ['like','entries.article',"%-%-$articleCar-%",false];
            }

            $soldCarsQuery->andWhere($qArr);

            $item['soldCars'] = $soldCarsQuery->count();

            $item['inStock'] = $objArticle->balanceFact;

            echo 'UPN:' .  $item['upn']. PHP_EOL;

            $data[] = $item;
        }

        \Yii::$app->db->createCommand()->delete($tableName)->execute();
        \Yii::$app->db->createCommand()->batchInsert($tableName,['storage_id', 'upn', 'article','title','window', 'cars', 'soldArticle','soldWindow','soldCars','inStock'] , $data)->execute();
    }
}