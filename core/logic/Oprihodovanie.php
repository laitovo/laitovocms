<?php

namespace core\logic;

use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use core\models\article\Article;
use Yii;

/**
 * Класс отвечает за оприходование продукции на склад
 *
 * Class Oprihodovanie
 * @package core\logic
 */
class Oprihodovanie
{
    /**
     * Оприходование артикула (создание UPN и StorageState)
     *
     * @param $article
     * @return array
     * @throws \yii\db\Exception
     */
    public function oprihodArticle($article)
    {
        //Статситику по артикулу
        $objArticle = new Article($article);

        //Получим планируемый баланс
        $plan = $objArticle->balancePlan;
        $fact = $objArticle->balanceFact;

        //Если он болше нуля тогда тип  - оборотный, если меньше, тогда тип накопительный
        $storageType = ($plan && $plan > 0 && $plan > $fact) || mb_substr($article, 0,
            2) == 'OT' ? Storage::TYPE_REVERSE : Storage::TYPE_TIME;

        $productId = $objArticle->product ? $objArticle->product->id : null; //id вида продукта

        $storage = $productId ? Storage::findStorageForProduct($productId, $storageType) : null;

        if (!$storage) {
            //Устанавливаем ответ по умолчанию
            $response['status'] = false;
            $response['message'] = Yii::t('app', 'Не определено место хранения для ' . $article);

            return $response;
        }

        //Внутри склада определяем литеру
        $literal = $storage->getActualLiteral($productId);

        $transaction = Yii::$app->db->beginTransaction();

        //Содание UPN для наряда
        $logist = new Naryad();
        $logist->article = $article;
        $logist->team_id = Yii::$app->team->getId();
        $logist->source_id = 2;

        if (!$logist->save()) {
            $transaction->rollBack();

            return [
                'status'  => false,
                'message' => Yii::t('app', 'Не удалось создать UPN для ' . $article),
            ];
        }

        //Ставим, что данный наряд распределен с диспечер-склад
        $row = new StorageState();
        $row->upn_id = $logist->id;
        $row->storage_id = $storage->id;
        $row->literal = $literal;

        if (!$row->save()) {
            $transaction->rollBack();

            return [
                'status'  => false,
                'message' => Yii::t('app', 'Не удалось создать складскую позицию для ' . $article),
            ];
        }

        $transaction->commit();

        return [
            'status'  => true,
            'message' => Yii::t('app', 'Успешно выполнено'),
        ];
    }
}