<?php

namespace core\logic;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use common\models\laitovo\Cars;
use core\models\article\Article;
use core\services\SProductionLiteral;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use Yii;

class DistributionProductionLiterals
{
    /**
     * Функция распределения наряда
     *
     * @param $barcode
     * @return array
     */
    public function distribute($barcode): array
    {
        $service = new SProductionLiteral();

        //1. Проверить, изготавливается ли этот наряд или нет.
        $barcode = BarcodeHelper::toLatin($barcode);

        if ($prodGroup = ErpNaryad::findProdGroup($barcode)) {
            //Находим наряды этой группы
            $workOrders = ErpNaryad::find()->where(['prodGroup' => $prodGroup])->all();
            if (empty($workOrders)) {
                return ['status' => 'error', 'message' => Yii::t('app','В группе нет ни одного наряда!')];
            }

            $locationIdsArray = ArrayHelper::map($workOrders,'start','start');
            if (count($locationIdsArray) != 1)
                return ['status' => 'error', 'message' => Yii::t('app','Наряды находятся на разных участках!!')];

            $location_id = array_shift($locationIdsArray);
            $ids = ArrayHelper::map($workOrders,'id','id');
            $workOrder = array_shift($workOrders);

            if ($workOrder->start == Yii::$app->params['erp_otk']) {
                foreach ($ids as $id) {
                    SProductionLiteral::unRegisterLiteral($id, Yii::$app->params['erp_clipsi']);
                }
            }

            if ($literalIndex = (new Query())->from('core_production_literal_naryad')->select('prodLiteralId')->where(['erpNaryadId' => $ids])->andWhere(['locationId' => $workOrder->start])->scalar())
                return ['status' => 'error', 'message' => Yii::t('app','Наряды уже распеределены по литере! ' . $literalIndex)];

            $freeLiteral = $service->findEmptyLiteral($location_id);
            try {
                foreach ($ids as $id) {
                    $service->registerLiteral($freeLiteral, $id, $location_id);
                }
                return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $freeLiteral  </span>")];
            } catch (Exception $e) {
                return ['status' => 'error', 'message' => $e->getMessage()];
            }
        }

        if (!($workOrder = ErpNaryad::find()->where(['barcode' => $barcode])->one()))
            return ['status' => 'error', 'message' => Yii::t('app','Штрих-код НЕ верен! Наряда с данным штрих-кодом не существует!')];

        if ($workOrder->actNumber)
            return ['status' => 'error', 'message' => Yii::t('app','Наряды по АКТУ не могут быть оприходованы в литеру!')];

        if (!in_array($workOrder->status,[ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE])
            || $workOrder->location_id != Yii::$app->params['erp_dispatcher']
            || $workOrder->distributed
            || !$workOrder->start )
            return ['status' => 'error', 'message' => Yii::t('app','Данному наряду не может быть присвоена литера! С ним необходимо разобраться!')];

        //Снимаем с резерва литеру под клипсы, если наряд уже перешел на отк
        if ($workOrder->start == Yii::$app->params['erp_otk']) {
            SProductionLiteral::unRegisterLiteral($workOrder->id, Yii::$app->params['erp_clipsi']);
        }

        //Если данному наряду УЖЕ присвоена литера, тогда ответить с номером литеры.
        if ($literalId = (new Query())->from('core_production_literal_naryad')->select('prodLiteralId')->where(['erpNaryadId' => $workOrder->id])->andWhere(['locationId' => $workOrder->start])->scalar())
            return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $literalId  </span>")];

        ########################################################################################
        #Здесь начинается алгоритм распределения наряда по литерам

        $articleObj = new Article($workOrder->article);
        //Определяем срочность наряда
        $sort = $workOrder->sort;

        //Определяем ткань
        $cloth = $articleObj->articleCloth;

        //Определяем оконный проем
        $window = $articleObj->windowEn;

        //Номенклатура на магнитах
        $onMagnets = $articleObj->isOnMagnets();

        #Вместимость литер
        $capacity = 6;

        #Свободная литера
        $freeLiteral = null;

        if(!in_array($window,['FV','FD','RD','RV','BW']))
            return ['status' => 'error', 'message' => Yii::t('app','Данный вид наряда не может быть распределен по новым литерам! Новые литеры предназначены только для автошторок!')];

        switch ($workOrder->start) {
            case Yii::$app->params['erp_okleika'] :
                //Если ткань чико и оконные проем не BW.
                $analogLike = ['like','article',"$window-%-%-%-$cloth",false];

                if ($cloth == 5 and $window != 'BW') {
                    $analogLike = ['or',
                        ['like','article',"FV-%-%-%-$cloth",false],
                        ['like','article',"FD-%-%-%-$cloth",false],
                        ['like','article',"RD-%-%-%-$cloth",false],
                        ['like','article',"RV-%-%-%-$cloth",false],
                    ];
                } elseif (in_array($cloth,[2,4,8]) && in_array($window,['FV','FD'])) {
                    $analogLike = ['or',
                        ['like','article',"FV-%-%-%-$cloth",false],
                        ['like','article',"FD-%-%-%-$cloth",false],
                    ];
                } elseif (in_array($cloth,[2,4,8]) && in_array($window,['RD','RV'])) {
                    $analogLike = ['or',
                        ['like','article',"RV-%-%-%-$cloth",false],
                        ['like','article',"RD-%-%-%-$cloth",false],
                    ];
                }

                //Для грузовиков чико на оклейке отдельные ячейки.
                if ($cloth == 5 && $window == 'FD' && ($carObj = $articleObj->car) && $carObj->category == 'D') {
                    $carArticles = Cars::find()->where(['category' => 'D'])->select('article')->column();
                    $regExp = 'FD-.-(' . implode('|',$carArticles) . ')-.+-5';
                    $analogLike = ['REGEXP', 'article', $regExp];
                }

                //Находим наряды, аналогичные текущему(проверяемому, пропикиваемому) по оконному проему ,ткани и срочности,
                // которые находятся на участке Диспетчер-Оклейка
                $analogWorkOrdersInProduction = ErpNaryad::find()
                    ->select('id')
                    ->where(['and',
                        ['prodGroup' => null],
                        ['start' => $workOrder->start],
                        ['sort' => $sort],
                        $analogLike,
                        ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                    ])->column();

                //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);

                if (!$freeLiteral || $service->isNotEffectiveLiteral($freeLiteral)) {
                    $analogWorkOrdersInProduction = ErpNaryad::find()
                        ->select('id')
                        ->where(['and',
                            ['prodGroup' => null],
                            ['start' => $workOrder->start],
                            $analogLike,
                            ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                        ])->column();

                    $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);
                }

                //Здесь зачисляем наряд на литеру под нужным номером
                try {
                    $service->registerLiteral($freeLiteral, $workOrder->id, $workOrder->start);
                    return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $freeLiteral  </span>")];
                } catch (Exception $e) {
                    return ['status' => 'error', 'message' => $e->getMessage()];
                }

            case Yii::$app->params['erp_shveika'] :
                //Определяем - является ли наряд - ДоунтЛуком
                $isDontLook = $articleObj->isDontLook();

                //Если это ДоунтЛук
                if ($isDontLook) {
                    //Находим другие ДоунтЛуки на производстве перед участком швейка
                    $analogWorkOrdersInProduction = ErpNaryad::find()
                        ->select('id')
                        ->where(['and',
                            ['prodGroup' => null],
                            ['start' => $workOrder->start],
                            ['or',
                                ['article','like','%-%-%-4-%',false],
                                ['article','like','%-%-%-5-%',false],
                            ],
                            ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                            ['stochka' => $workOrder->stochka],
                        ])->column();

                    //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                    $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);
                } else {
                    //Находим наряды, аналогичные текущему(проверяемому, пропикиваемому) по срочности,
                    $analogWorkOrdersInProductionQuery = ErpNaryad::find()
                        ->select('id')
                        ->where(['and',
                            ['prodGroup' => null],
                            ['start' => $workOrder->start],
                            ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                            ['stochka' => $workOrder->stochka],
                        ]);
                        if (!$workOrder->stochka) {
                            $analogWorkOrdersInProductionQuery->andWhere(['sort' => $sort]);
                        }
                    $analogWorkOrdersInProduction = $analogWorkOrdersInProductionQuery->column();

                    //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                    $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);

                    if (!$freeLiteral || $service->isNotEffectiveLiteral($freeLiteral)) {
                        $analogWorkOrdersInProductionQuery = ErpNaryad::find()
                            ->select('id')
                            ->where(['and',
                                ['prodGroup' => null],
                                ['start' => $workOrder->start],
                                ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                                ['stochka' => $workOrder->stochka],
                            ]);
                        $analogWorkOrdersInProduction = $analogWorkOrdersInProductionQuery->column();

                        //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                        $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);;
                    }
                }

                //Здесь зачисляем наряд на литеру под нужным номером
                try {
                    $service->registerLiteral($freeLiteral, $workOrder->id, $workOrder->start);
                    return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $freeLiteral  </span>")];
                } catch (Exception $e) {
                    return ['status' => 'error', 'message' => $e->getMessage()];
                }

            case Yii::$app->params['erp_otk'] :
                //Определяем, является ли данный наряд частью комплекта
                $upn = $workOrder->upn;
                if (!$upn || !$upn->group || empty(($group = UpnGroup::getAnotherFromGroup($upn)))) {
                    //Тогда находим свободную литеру для одного наряда
                    //Находим наряды, аналогичные текущему(проверяемому, пропикиваемому) по срочности,
                    $analogWorkOrdersInProduction = ErpNaryad::find()
                        ->select('id')
                        ->where(['and',
                            ['prodGroup' => null],
                            ['start' => $workOrder->start],
                            ['sort' => $sort],
                            ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                        ])->column();

                    //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                    $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);

                    if ($freeLiteral || $service->isNotEffectiveLiteral($freeLiteral)) {
                        $analogWorkOrdersInProduction = ErpNaryad::find()
                            ->select('id')
                            ->where(['and',
                                ['prodGroup' => null],
                                ['start' => $workOrder->start],
                                ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                            ])->column();

                        //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                        $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);
                    }
                    //Здесь зачисляем наряд на литеру под нужным номером
                    try {
                        $service->registerLiteral($freeLiteral, $workOrder->id, $workOrder->start);
                        return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $freeLiteral  </span>")];
                    } catch (Exception $e) {
                        return ['status' => 'error', 'message' => $e->getMessage()];
                    }
                } else {
                    //Тогда находим свободную литеру для комплекта нарядов
                    $ids = ArrayHelper::map($group,'id','id');

                    //Ищем элементы, которые находятся перед отк и они с литерой
                    $groupWorkOrdersOnLocation = ErpNaryad::find()
                        ->select('id')
                        ->where(['and',
                            ['order_id' => $workOrder->order_id],
                            ['in','logist_id',$ids],
                            ['start' => Yii::$app->params['erp_otk']],
                            ['status' => [ErpNaryad::STATUS_IN_WORK, ErpNaryad::STATUS_IN_PAUSE]],
                            ['location_id' => Yii::$app->params['erp_dispatcher']],
                        ])
                        ->column();

                    #Если один из элементов комплекта уже находился в литере перед отк, то тогда мы сразу же нашли литеру для текущего комплекта
                    if (!empty($groupWorkOrdersOnLocation))
                        $freeLiteral = $service->findAnalogLiteral($groupWorkOrdersOnLocation, $workOrder->start, $capacity);

                    #В противном случае:
                    if (!$freeLiteral) {
                        $needCapacity = $capacity - count($ids);

                        $groupWorkOrdersBeforeLocation = ErpNaryad::find()
                            ->select('id')
                            ->where(['and',
                                ['order_id' => $workOrder->order_id],
                                ['in','logist_id',$ids],
                                ['or',
                                    ['status' => [ ErpNaryad::STATUS_IN_WORK, ErpNaryad::STATUS_IN_PAUSE ] ],
                                    ['status' => null ]
                                ],
                            ])
                            ->column();

                        $analogWorkOrdersInProduction = ErpNaryad::find()
                            ->select('id')
                            ->where(['and',
                                ['prodGroup' => null],
                                ['start' => $workOrder->start],
                                ['sort' => $sort],
                                ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                            ])->column();

                        $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $needCapacity);

                        if (!$freeLiteral || $service->isNotEffectiveLiteral($freeLiteral)) {
                            $analogWorkOrdersInProduction = ErpNaryad::find()
                                ->select('id')
                                ->where(['and',
                                    ['prodGroup' => null],
                                    ['start' => $workOrder->start],
                                    ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                                ])->column();

                            $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $needCapacity);
                        }

                        //Создание связей литеры и наряда
                        try {
                            $service->registerLiteral($freeLiteral, $workOrder->id, $workOrder->start);
                            foreach ($groupWorkOrdersBeforeLocation as $id) {
                                if (!$service->existsLiteral($id, $workOrder->start)) {
                                    $service->registerLiteral($freeLiteral,$id, $workOrder->start);
                                }
                            }
                            return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $freeLiteral  </span>")];
                        } catch (Exception $e) {
                            return ['status' => 'error', 'message' => $e->getMessage()];
                        }
                    } else {
                        //Создание связей литеры и наряда
                        try {
                            $service->registerLiteral($freeLiteral, $workOrder->id, $workOrder->start);
                            return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $freeLiteral  </span>")];
                        } catch (Exception $e) {
                            return ['status' => 'error', 'message' => $e->getMessage()];
                        }
                    }
                }

            case Yii::$app->params['erp_clipsi'] :

                //Находим наряды, аналогичные текущему(проверяемому, пропикиваемому) по срочности,
                $analogWorkOrdersInProductionQuery = ErpNaryad::find()
                    ->select('id')
                    ->where(['and',
                        ['prodGroup' => null],
                        ['start' => $workOrder->start],
                        ['sort' => $sort],
                        ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                    ]);

                if ($onMagnets)
                    $analogWorkOrdersInProductionQuery->andWhere(['REGEXP','article','.+-.-.+-(67|71|72|70)-.+']);
                else
                    $analogWorkOrdersInProductionQuery->andWhere(['NOT REGEXP','article','.+-.-.+-(67|71|72|70)-.+']);

                $analogWorkOrdersInProduction = $analogWorkOrdersInProductionQuery->column();

                //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);

                if (!$freeLiteral || $service->isNotEffectiveLiteral($freeLiteral)) {
                    $analogWorkOrdersInProductionQuery = ErpNaryad::find()
                        ->select('id')
                        ->where(['and',
                            ['prodGroup' => null],
                            ['start' => $workOrder->start],
                            ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                        ]);

                    if ($onMagnets)
                        $analogWorkOrdersInProductionQuery->andWhere(['REGEXP','article','.+-.-.+-(67|71|72|70)-.+']);
                    else
                        $analogWorkOrdersInProductionQuery->andWhere(['NOT REGEXP','article','.+-.-.+-(67|71|72|70)-.+']);

                    $analogWorkOrdersInProduction = $analogWorkOrdersInProductionQuery->column();


                    //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                    $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);
                }

                //Здесь зачисляем наряд на литеру под нужным номером
                try {
                    $service->registerLiteral($freeLiteral, $workOrder->id, $workOrder->start);
                    return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $freeLiteral  </span>")];
                } catch (Exception $e) {
                    return ['status' => 'error', 'message' => $e->getMessage()];
                }

            default:
                //Находим наряды, аналогичные текущему(проверяемому, пропикиваемому) по срочности,
                $analogWorkOrdersInProduction = ErpNaryad::find()
                    ->select('id')
                    ->where(['and',
                        ['prodGroup' => null],
                        ['start' => $workOrder->start],
                        ['sort' => $sort],
                        ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                    ])->column();

                //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);

                if (!$freeLiteral || $service->isNotEffectiveLiteral($freeLiteral)) {
                    $analogWorkOrdersInProduction = ErpNaryad::find()
                        ->select('id')
                        ->where(['and',
                            ['prodGroup' => null],
                            ['start' => $workOrder->start],
                            ['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]],
                        ])->column();

                    //Находим литеру с учетом участка, аналогичных нарядов и вместимости
                    $freeLiteral = $service->findLiteral($analogWorkOrdersInProduction, $workOrder->start, $capacity);
                }

                //Здесь зачисляем наряд на литеру под нужным номером
                try {
                    $service->registerLiteral($freeLiteral, $workOrder->id, $workOrder->start);
                    return ['status' => 'success', 'message' => Yii::t('app',"ПОЛОЖИ В ЛИТЕРУ <span style='font-weight: bold; font-size:  15em; color: red'>  $freeLiteral  </span>")];
                } catch (Exception $e) {
                    return ['status' => 'error', 'message' => $e->getMessage()];
                }
        }
    }
}