<?php

namespace core\logic;

use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\OrderEntry;
use core\entities\logisticsShipment\ShipmentManager;
use core\models\article\Article;
use Yii;
use Exception;

/**
 * Резервирование позиций
 *
 * Class OrderEntryReservation
 * @package core\logic
 */
class OrderEntryReservation
{
    ### ИНИЦИАЛИЗАЦИЯ КЛАССА #######################################################
    /**
     * @var OrderEntryReservation Функция для хранения экземпляра этого класса.
     */
    private  static  $_instance = null;

    /**
     * @return OrderEntryReservation
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private static function _getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {}
    private function __wakeup() {}

    /**
     * OrderEntryReservation constructor.
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    ### ПЕРЕМЕННЫЕ ####################################################################################################

    /**
     * @var ShipmentManager $_shipmentManager Хранит в себе менеджер для работы с отгрузками.
     */
    private $_shipmentManager;

    ### ИНТЕРФЕЙС #####################################################################################################

    /**
     * Процедура производит резервирование некого UPN для OrderEntry(позиции заказа)
     *
     * @param $orderEntry
     * @return string
     * @throws Exception
     */
    public static function reserve($orderEntry)
    {
        $handler = self::_getInstance();
        return $handler->_reserve($orderEntry);
    }

    /**
     * Процедура производит снятие с резерва некого UPN для OrderEntry(позиции заказа)
     *
     * @param $orderEntry
     * @return string
     * @throws Exception
     */
    public static function unReserve($orderEntry)
    {
        $handler = self::_getInstance();
        return $handler->_unReserve($orderEntry);
    }

    ### ЯДРО

    /**
     * Процедура производит резервирование некого UPN для OrderEntry(позиции заказа)
     *
     * @param $orderEntry
     * @return string
     * @throws Exception
     */
    private function _reserve($orderEntry)
    {
        //Проверим, нужный ли объект нам передали
        $this->_instanceOf($orderEntry);
        //Проверим, может ли быть зарезервирован upn
        $this->_canBeReserved($orderEntry);
        //Попробуем зарезервировать UPN для нашей OrderEntry
        $message = $this->_reserveFreeUpn($orderEntry);

        return $message;
    }


    /**
     * Процедура производит снятие с резерва некого UPN для OrderEntry(позиции заказа)
     *
     * @param $orderEntry
     * @return string
     * @throws Exception
     */
    private function _unReserve($orderEntry)
    {
        //Проверим, нужный ли объект нам передали
        $this->_instanceOf($orderEntry);
        //Проверим, может ли быть зарезервирован upn
        $this->_canBeUnReserved($orderEntry);
        //Попробуем зарезервировать UPN для нашей OrderEntry
        $message = $this->_unReserveUpn($orderEntry);

        return $message;
    }

    /**
     * Процедура производит проверку, правильный ли объект к нам пришел. Если нет, тогда выкидывает исключение.
     *
     * @param OrderEntry $orderEntry
     * @throws Exception
     */
    private function _instanceOf($orderEntry)
    {
        if (!($orderEntry instanceof OrderEntry)) {
            throw new Exception('В сервис для удаления логистической позиции передан неверный тип объекта');
        }
    }

    /**
     * Процедура проверяет, можно ли провести процесс резервирования для данной позиции.
     * Если по какой либо причине невозможно зарезервировать, данная причина выкидывается в качестве исключения.
     *
     * @param $orderEntry
     * @throws \Exception
     */
    public function _canBeReserved($orderEntry)
    {
        $this->_instanceOf($orderEntry);
        //1. Резервирование OrderEntry невозможно, если она(позиция) отменена
        if ($orderEntry->is_deleted) {
            throw new \DomainException('Невозможно провести резервирование для данной позиции, так как она отменена!');
        }
        //2. Резервирование OrderEntry невозможно, если для нее(позиции) уже разервирован другой UPN.
        if ($orderEntry->upn) {
            throw new \DomainException('Невозможно провести резервирование для данной позиции, так как для нее уже зарезервирован UPN!');
        }
        //3. Резервирование OrderEntry невозможно, если она(позиция) индивидуальна. Проверка, что это не складская позиция, только для нашего спокойствия.
        if ($orderEntry->is_custom && !CheckStoragePosition::isStoragePosition($orderEntry)) {
            throw new \DomainException('Эта позиция индивидуальна, поэтому процесс резервирования для нее невозможен!');
        }
    }

    /**
     * Процедура резервирует свободный UPN под нашу позицию
     *
     * @param $orderEntry
     * @return string
     */
    private function _reserveFreeUpn($orderEntry)
    {
        $search = new Article($orderEntry->article);
        if (!$search->balanceFact) {
            throw new \DomainException('На складе не числится ни одной позиции для резерва!');
        }

        $freeUpn = $search->findFreeUpn();
        /**
         * @var $freeUpn Naryad
         */
        $freeUpn->reserved_id = $orderEntry->id;
        if (!$freeUpn->save()) {
            throw new \DomainException('Не удалось зарезервировать UPN при поптыке сохранения!');
        };

        return 'Для данной позиции успешно зарезервирован UPN № '. $freeUpn->id .', который находит на литере: '. @$freeUpn->storageState->literal;
    }

    /**
     * Процедура проверяет, можно ли провести процесс снятия с резерва для данной позиции.
     * Если по какой либо причине невозможно зарезервировать, данная причина выкидывается в качестве исключения.
     *
     * @param $orderEntry
     * @throws \Exception
     */
    public function _canBeUnReserved($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        //1. Снятие с резерва невозможно, если нет зарезервированного UPN для данной позиции
        $reservedUpn = $orderEntry->upn;
        if (!$reservedUpn) {
            throw new \DomainException('Под данную позицию незарезервированно ни одного UPN');
        }
        //2. Снятие с резерва невозможно, если зарезервированный UPN уже упакован
        if ($reservedUpn->packed) {
            throw new \DomainException('Под данную позицию незарезервированно ни одного UPN');
        }
        //3. Снятие с резерва невозможно, если зарезервированный позиция содержится в отгрузке, с которой уже занимаются (не в статусе not_handled)
        if ($orderEntry->shipment_id && ($shipment = $this->_shipmentManager->findById($orderEntry->shipment_id)) && !$this->_shipmentManager->isNotHandled($shipment)) {
            throw new \DomainException('Невозможно провести резервирование для данной позиции, ее уже процесс отгрузки запущен!');
        }
    }

    /**
     * Процедура снимает с резерва зарезервированный UPN под нашу позицию
     *
     * @param $orderEntry
     * @return string
     */
    private function _unReserveUpn($orderEntry)
    {
        $reservedUpn = $orderEntry->upn;

        $reservedUpn->reserved_id = null;
        if (!$reservedUpn->save()) {
            throw new \DomainException('Не удалось снять с резерва UPN при поптыке сохранения!');
        };

        return 'Данная позиция успешно снята с резерва !';
    }
}