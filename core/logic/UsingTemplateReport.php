<?php

namespace core\logic;

use backend\modules\laitovo\models\ErpNaryad;
use common\models\laitovo\Cars;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class UsingTemplateReport
{
    /**
     * @var string Таблица отчета
     */
    private static $_tableName = 'tmp_using_template';


    /**
     * @return ActiveDataProvider
     */
    public static function execute()
    {
        $data = \Yii::$app->cache->get('global_using_report_data_value');

        $query = (new Query())->from(self::$_tableName)->select('CAST(templateNumber AS UNSIGNED) as templateNumber, usingCount1, usingCount2, usingCount3, usingCount4, usingCount5, cars');

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $provider->setSort([
            'attributes' => [
                'templateNumber','usingCount1','usingCount2','usingCount3','usingCount4','usingCount5','cars'
            ],
            'defaultOrder' => ['usingCount1' => SORT_ASC]
        ]);

        return $provider;

    }

    /**
     * Функция генерации данных, которые будет дергать консольное приложение
     */
    public static function generateData()
    {
        /**
         * @var $car Cars
         */
        $data = [];
        foreach (Cars::find()->where('mastered is not null')->orderBy('article')->batch(1) as $carAr) {
           $car = $carAr[0];
           $templateNumber = (int)$car->json('nomerlekala');
           if (!$templateNumber) continue;
           $carName = $car->getFullName();

           $period = 60*60*24*365;
           $timeTo = time();
           $timeFrom = $timeTo - $period;

           //Статистика за 1 год
           $rows = ErpNaryad::find()
                ->select('id')
                ->andWhere(['status' => ErpNaryad::STATUS_READY])
                ->andWhere(['>=','created_at', $timeFrom])
                ->andWhere(['<','created_at', $timeTo])
                ->andWhere(['not like','article',"OT-%",false])
                ->andWhere(['not like','article',"FW-%",false])
                ->andWhere(['like','article',"%-%-{$car->article}-%-%",false])
                ->column() ;
            $usingCount1 = count($rows);

            $timeTo = $timeFrom;
            $timeFrom = $timeTo - $period;

            //Статистика за 2 года
            $rows = ErpNaryad::find()
                ->select('id')
                ->andWhere(['status' => ErpNaryad::STATUS_READY])
                ->andWhere(['>=','created_at', $timeFrom])
                ->andWhere(['<','created_at', $timeTo])
                ->andWhere(['not like','article',"OT-%",false])
                ->andWhere(['not like','article',"FW-%",false])
                ->andWhere(['like','article',"%-%-{$car->article}-%-%",false])
                ->column();
            $usingCount2 = count($rows) + $usingCount1;

            $timeTo = $timeFrom;
            $timeFrom = $timeTo - $period;

            //Статистика за 3 года
            $rows = ErpNaryad::find()
                ->select('id')
                ->andWhere(['status' => ErpNaryad::STATUS_READY])
                ->andWhere(['>=','created_at', $timeFrom])
                ->andWhere(['<','created_at', $timeTo])
                ->andWhere(['not like','article',"OT-%",false])
                ->andWhere(['not like','article',"FW-%",false])
                ->andWhere(['like','article',"%-%-{$car->article}-%-%",false])
                ->column();
            $usingCount3 = count($rows) + $usingCount2;

            $timeTo = $timeFrom;
            $timeFrom = $timeTo - $period;

            //Статистика за 4 года
            $rows = ErpNaryad::find()
                ->select('id')
                ->andWhere(['status' => ErpNaryad::STATUS_READY])
                ->andWhere(['>=','created_at', $timeFrom])
                ->andWhere(['<','created_at', $timeTo])
                ->andWhere(['not like','article',"OT-%",false])
                ->andWhere(['not like','article',"FW-%",false])
                ->andWhere(['like','article',"%-%-{$car->article}-%-%",false])
                ->column();
            $usingCount4 = count($rows) + $usingCount3;

            $timeTo = $timeFrom;
            $timeFrom = $timeTo - $period;

            //Статистика за 5 лет
            $rows = ErpNaryad::find()
                ->select('id')
                ->andWhere(['status' => ErpNaryad::STATUS_READY])
                ->andWhere(['>=','created_at', $timeFrom])
                ->andWhere(['<','created_at', $timeTo])
                ->andWhere(['not like','article',"OT-%",false])
                ->andWhere(['not like','article',"FW-%",false])
                ->andWhere(['like','article',"%-%-{$car->article}-%-%",false])
                ->column();
            $usingCount5 = count($rows) + $usingCount4;

           if (isset($data[$templateNumber])) {
               $data[$templateNumber]['usingCount1'] += $usingCount1;
               $data[$templateNumber]['usingCount2'] += $usingCount2;
               $data[$templateNumber]['usingCount3'] += $usingCount3;
               $data[$templateNumber]['usingCount4'] += $usingCount4;
               $data[$templateNumber]['usingCount5'] += $usingCount5;
               $data[$templateNumber]['cars'] .= ';' . $carName;
           } else {
               $data[$templateNumber]['templateNumber'] = $templateNumber;
               $data[$templateNumber]['usingCount1'] = $usingCount1;
               $data[$templateNumber]['usingCount2'] = $usingCount2;
               $data[$templateNumber]['usingCount3'] = $usingCount3;
               $data[$templateNumber]['usingCount4'] = $usingCount4;
               $data[$templateNumber]['usingCount5'] = $usingCount5;
               $data[$templateNumber]['cars'] = $carName;
           }
            echo 'Артикул:' .  $car->article . PHP_EOL;
        }

        \Yii::$app->db->createCommand()->delete(self::$_tableName)->execute();
        \Yii::$app->db->createCommand()->batchInsert(self::$_tableName,['templateNumber', 'usingCount1', 'usingCount2', 'usingCount3', 'usingCount4', 'usingCount5', 'cars'], $data)->execute();
    }
}