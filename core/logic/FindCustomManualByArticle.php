<?php

namespace core\logic;

use Yii;
use backend\helpers\ArticleHelper;

/**
 * Поиск индивидуальных инструкций по артикулу
 *
 * Class FindCustomManualByArticle
 * @package core\logic
 */
class FindCustomManualByArticle
{
    private $_instructionTypeManager;
    private $_customCarInstructionManager;
    private $_article;
    private $_car;
    private $_instructionType;
    private $_customManual;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_customCarInstructionManager = Yii::$container->get('core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager');
    }

    public function search($article)
    {
        $this->_article = $article;
        $this->_car = $this->_findCar($this->_article);
        $this->_instructionType = $this->_findInstructionType($this->_article);
        $this->_customManual = $this->_findCustomManual($this->_car, $this->_instructionType);
    }

    public function exists()
    {
        return $this->_customManual != null;
    }

    public function getCarId()
    {
        return !$this->_customManual ? null : $this->_customCarInstructionManager->getCarId($this->_customManual);
    }

    public function getInstructionTypeId()
    {
        return !$this->_customManual ? null : $this->_customCarInstructionManager->getInstructionTypeId($this->_customManual);
    }

    private function _findCar($article)
    {
        return ArticleHelper::getCar($article)->one();
    }

    private function _findInstructionType($article)
    {
        return $this->_instructionTypeManager->findByArticle($article);
    }

    private function _findCustomManual($car, $instructionType)
    {
        if (!$car) {
            return null;
        }
        if (!$instructionType) {
            return null;
        }
        $customManual = $this->_customCarInstructionManager->findWhere([
            'instructionTypeId' => $this->_instructionTypeManager->getId($instructionType),
            'carId' => $car->id
        ])[0] ?? null;

        return $customManual;
    }
}