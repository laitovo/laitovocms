<?php

namespace core\logic;

use common\models\logistics\OrderEntry;
use core\models\storageProductType\StorageProductType;
use Exception;

/**
 * Класс ответственен за работу с позициями, которые отгружаются только со склада и не изготавливаются на производстве по стандартной логике.
 *
 * Class CheckStoragePosition
 * @package core\logic
 */
class CheckStoragePosition
{
    ### ИНИЦИАЛИЗАЦИЯ КЛАССА #######################################################
    /**
     * @var CheckStoragePosition Функция для хранения экземпляра этого класса.
     */
    private  static  $_instance = null;

    private static function _getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {}

    private function __wakeup() {}

    private function __construct() {}

    ### ПЕРЕМЕННЫЕ #################################################################

    ### ИНТЕРФЕЙС ########################################################################

    /**
     * Функция возвращает ответ на вопрос, является ли позиция складской.
     *
     * @param $orderEntry
     * @return bool
     * @throws Exception
     */
    public static function isStoragePosition($orderEntry): bool
    {
        $handler = self::_getInstance();
        return $handler->_isStoragePosition($orderEntry);
    }

    /**
     * Функция возвращает ответ на вопрос, может ли позиция быть произведена на производстве.
     *
     * @param $article
     * @return bool
     * @throws Exception
     */
    public static function canBeProduced($article): bool
    {
        $handler = self::_getInstance();
        return $handler->_canBeProduced($article);
    }

    /**
     * @param $barcode
     * @return bool
     */
    public static function isStorageBarcode($barcode): bool
    {
        $handler = self::_getInstance();
        return $handler->_isStorageBarcode($barcode);
    }

    /**
     * @param $article
     * @return bool
     */
    public static function isStorageArticle($article): bool
    {
        $handler = self::_getInstance();
        return $handler->_isStorageArticle($article);
    }

    /**
     * @param $article
     * @return bool|string
     */
    public static function getStorageTitle($article)
    {
        $handler = self::_getInstance();
        return $handler->_getStorageTitle($article);
    }

    /**
     * Функция возвращает штрихкод для складской позиции
     *
     * @param $barcode
     * @return bool
     * @throws Exception
     */
    public static function getStorageArticleByBarcode($barcode)
    {
        $handler = self::_getInstance();
        return $handler->_getStorageArticleByBarcode($barcode);
    }

    /**
     * Возвращает значение штрих-кода, который позволяет оприходовать товар по количеству.
     *
     * @return string
     */
    public static function getQuantitativeBarcode(): string
    {
        return mb_strtoupper(md5('QuantitativeBarcode'));
    }

    ### ЯДРО ########################################################################

    /**
     * Функция возвращает ответ на вопрос, является ли позиция складской.
     *
     * @param OrderEntry $orderEntry
     * @return bool
     * @throws Exception
     */
    private function _isStoragePosition(OrderEntry $orderEntry): bool
    {
        $this->_instanceOf($orderEntry);
        return StorageProductType::find()->where(['article' => $orderEntry->article])->exists();
    }

    /**
     * @param $article
     * @return bool
     */
    private function _canBeProduced($article): bool
    {
        return StorageProductType::find()
            ->where(['article' => $article])
            ->andWhere(['canBeProduced' => 1])
            ->exists();
    }

    /**
     * Функция возвращает артикул по штрих-коду
     *
     * @param $barcode
     * @return bool
     */
    private function _getStorageArticleByBarcode($barcode)
    {
        return StorageProductType::find()->select('article')->where(['barcode' => $barcode])->scalar() ?: null;
    }

    /**
     * Функция отвечает, является ли переданный штрих-код - складской позицией
     *
     * @param $barcode
     * @return bool
     */
    private function _isStorageBarcode($barcode): bool
    {
        return StorageProductType::find()->where(['barcode' => $barcode])->exists();
    }

    /**
     * @param $article
     * @return bool
     */
    private function _isStorageArticle($article): bool
    {
        return StorageProductType::find()->where(['article' => $article])->exists();
    }

    /**
     * @param $article
     * @return bool
     */
    private function _getStorageTitle($article)
    {
        return StorageProductType::find()->select('title')->where(['article' => $article])->scalar() ?: '';
    }

    /**
     * Процедура, которая позволяет проверить, является ли переданный объект экземпляром класса позиции заказа.
     *
     * @param $orderEntry
     * @throws Exception
     */
    private function _instanceOf($orderEntry)
    {
        if (!($orderEntry instanceof OrderEntry)) {
            throw new Exception('В сервис для резервирования UPN передан неверный тип объекта');
        }
    }
}