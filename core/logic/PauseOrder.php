<?php

namespace core\logic;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpOrder;
use Yii;
use Exception;

/**
 * Постановка заказов на паузу
 *
 * Class PauseOrder
 * @package core\logic
 */
class PauseOrder
{
    /**
     * Проверка, стоит ли заказ на паузе
     * Определяется по наличию хотя бы одного наряда, поставленного на паузу через заказ
     *
     * @param ErpOrder $order
     * @return bool
     */
    public function isPaused($order) {
        $this->_instanceOf($order);
        foreach ($order->naryads as $naryad) {
            if ($naryad->isPausedByOrder()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Проверка, может ли заказ быть поставлен на паузу
     * Определяется по наличию хотя бы одного наряда, который может быть поставлен на паузу
     *
     * @param ErpOrder $order
     * @return bool
     */
    public function canBePaused($order) {
        $this->_instanceOf($order);
        foreach ($order->naryads as $naryad) {
            if ($naryad->canBePaused()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Постановка заказа на паузу
     *
     * @param ErpOrder $order
     * @return array
     */
    public function pause($order)
    {
        $this->_instanceOf($order);
        $transaction = Yii::$app->db->beginTransaction();
        foreach ($order->naryads as $naryad) {
            if (!$naryad->canBePaused()) {
                continue;
            }
            if (!$naryad->pause($byOrder = true)) {
                $transaction->rollBack();
                return [
                    'status'  => false,
                    'message' => Yii::t('app', 'Операция отменена, так как не удалось поставить на паузу наряд ' . $naryad->barcode)
                ];
            }
        }
        $transaction->commit();

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    /**
     * Снятие заказа с паузы
     *
     * @param ErpOrder $order
     * @return array
     */
    public function unpause($order)
    {
        $this->_instanceOf($order);
        $transaction = Yii::$app->db->beginTransaction();
        foreach ($order->naryads as $naryad) {
            if (!$naryad->isPausedByOrder()) {
                continue;
            }
            if (!$naryad->unpause()) {
                $transaction->rollBack();
                return [
                    'status'  => false,
                    'message' => Yii::t('app', 'Операция отменена, так как не удалось снять с паузы наряд ' . $naryad->barcode)
                ];
            }
        }
        $transaction->commit();

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    /**
     * Количество заказов на паузе
     *
     * @return int
     */
    public function getPausedOrdersCount()
    {
        return ErpNaryad::find()->where(['and',
            ['status' => ErpNaryad::STATUS_IN_PAUSE],
            ['is_paused_by_order' => true]
        ])->groupBy('order_id')->count();
    }

    /**
     * Функция возвращает массив с идентификаторами заказов, стоящих на паузе
     *
     * @return array
     */
    public function getPausedOrdersIds()
    {
        return ErpNaryad::find()->select(['order_id'])->where(['and',
            ['status' => ErpNaryad::STATUS_IN_PAUSE],
            ['is_paused_by_order' => true]
        ])->groupBy('order_id')->column();
    }

    private function _instanceOf($order)
    {
        if (!($order instanceof ErpOrder)) {
            throw new Exception('В сервис для постановки заказов на паузу передан неверный тип объекта');
        }
    }
}