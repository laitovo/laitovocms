<?php

namespace core\logic;

use backend\modules\laitovo\models\ErpOrder;
use backend\modules\laitovo\models\ErpNaryad;
use Yii;
use Exception;

/**
 * Изменение срочности заказов
 *
 * Class AccelerateOrder
 * @package core\logic
 */
class AccelerateOrder
{
    /**
     * Проверка, может ли заказ менять срочность.
     * Определяется по наличию хотя бы одного наряда, который может менять срочность.
     * Также можно поменять срочность необработанному заказу, у которого ещё нет нарядов.
     *
     * @param ErpOrder $order
     * @return bool
     */
    public function canUpdatePriority($order) {
        $this->_instanceOf($order);
        if ($order->status == null) {
            return true;
        }
        foreach ($order->naryads as $naryad) {
            if ($naryad->canUpdatePriority()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Изменение срочности заказа
     *
     * @param ErpOrder $order
     * @param int $sort
     * @return array
     */
    public function updatePriority($order, $sort)
    {
        $this->_instanceOf($order);
        if (!$this->canUpdatePriority($order)) {
            return [
                'status'  => false,
                'message' => Yii::t('app', 'Данный заказ не может менять срочность')
            ];
        }
        $transaction = Yii::$app->db->beginTransaction();
        $order->sort = $sort;
        if (!$order->save()) {
            $transaction->rollBack();
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось сохранить заказ')
            ];
        }
        if ($order->status == null) {
            $transaction->commit();
            return [
                'status' => true,
                'message' => Yii::t('app', 'Успешно выполнено')
            ];
        }

        if (!ErpNaryad::updateAll(['oldSort' => $sort],['order_id' => $order->id])) {
            $transaction->rollBack();
            return [
                'status'  => false,
                'message' => Yii::t('app', 'Операция отменена, так как не удалось изменить срочность наряда ' . $naryad->barcode)
            ];
        }

        if ($sort < 5) {
            if (!ErpNaryad::updateAll(['sort' => $sort, 'autoSpeed' => false],['order_id' => $order->id])) {
                $transaction->rollBack();
                return [
                    'status'  => false,
                    'message' => Yii::t('app', 'Операция отменена, так как не удалось изменить срочность наряда ' . $naryad->barcode)
                ];
            }
        } else {
            if (!ErpNaryad::updateAll(['sort' => $sort, 'autoSpeed' => false],['and',['order_id' => $order->id],['!=', 'sort', 5]])) {
                $transaction->rollBack();
                return [
                    'status'  => false,
                    'message' => Yii::t('app', 'Операция отменена, так как не удалось изменить срочность наряда ' . $naryad->barcode)
                ];
            }
        }


        $transaction->commit();

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    /**
     * Функция возвращает приоритет заказа в разрезе его нарядов.
     * Если в данном заказе у всех нарядов, находящихся на производстве, одинаковый приоритет, то функция вернёт его.
     * Если же заказ ещё не обработан, и у него пока нет нарядов, то функция вернёт приоритет самого заказа, поскольку новые наряды в дальнейшем его унаследуют.
     * В противном случае функция вернёт false.
     *
     * @param ErpOrder $order
     * @return int|false
     */
    public function getNaryadsSort($order)
    {
        $this->_instanceOf($order);
        if ($order->status == null) {
            return $order->sort;
        }
        $sorts = [];
        foreach ($order->naryads as $naryad) {
            if (!$naryad->isOnProduction()) {
                continue;
            }
            $sorts[] = $naryad->sort;
        }
        $sorts = array_unique($sorts);
        if (count($sorts) != 1) {
            return false;
        }

        return array_pop($sorts);
    }

    private function _instanceOf($order)
    {
        if (!($order instanceof ErpOrder)) {
            throw new Exception('В сервис для изменения срочности заказов передан неверный тип объекта');
        }
    }
}