<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 7/26/18
 * Time: 4:51 PM
 */

namespace core\logic;
use backend\helpers\ArticleHelper;
use backend\modules\logistics\models\OrderEntry;
use obmen\models\order\Order;
use yii\helpers\ArrayHelper;

/**
 * Класс несет ответсвенность за работу с комплектацией позиций в наряде.
 * Комплектация - это возможность указать, как должны быть скомплектованы позиции в заказе.
 *
 * Class Equipment
 * @package core\logic
 */
class Equipment
{
    ### ИНИЦИАЛИЗАЦИЯ КЛАССА #######################################################
    /**
     * @var Equipment Функция для хранения экземпляра этого класса.
     */
    private  static  $_instance = null;

    /**
     * @return Equipment Функция возвращает экземпляр класса
     */
    private static function _getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {}
    private function __wakeup() {}
    private function __construct() {}

    ### ИНТЕРФЕЙС #####################################################################################################

    /**
     * Функция возвращает сгруппированные по комлпектациям позиции заказа
     *
     * @param $order
     * @return array
     */
    public static function groupOrderByBundle($order)
    {
        $handler = self::_getInstance();
        return $handler->_groupOrderByBundle($order);
    }

    /**
     * Функция возвращает сгруппированные по комлпектациям позиции заказа
     *
     * @param $order
     * @return array
     */
    public static function groupOrderBySetIndex($order)
    {
        $handler = self::_getInstance();
        return $handler->_groupOrderBySetIndex($order);
    }

    /**
     * Функция возвращает сгруппированные по комлпектациям позиции заказа
     *
     * @param $orderEntries
     * @return array
     */
    public static function groupByBundle($orderEntries)
    {
        $handler = self::_getInstance();
        return $handler->_groupByBundle($orderEntries);
    }

    /**
     * Возвращает
     *
     * @param $order
     * @return OrderEntry[] $orderEntries
     */
    private function _groupOrderByBundle($order)
    {
        $elements = $order->orderEntries;
        if (empty($elements)) return [];

        if ($order->category == 'Экспорт' || $order->username == 'ПК Полипласт ООО' || $order->username == 's.c. ALFA ROM TRANS s.r.l.') {
            return $this->_groupByBundle($elements);
        }else{
            return [];
        }
    }

    /**
     * Возвращает
     *
     * @param $order
     * @return OrderEntry[] $orderEntries
     */
    private function _groupOrderBySetIndex($order)
    {
        $elements = $order->orderEntries;
        if (empty($elements)) return [];

        return $this->_groupBySetIndex($elements);

//        if ($order->category == 'Экспорт' || $order->username == 'ПК Полипласт ООО' || $order->username == 's.c. ALFA ROM TRANS s.r.l.') {
//        }else{
//            return [];
//        }
    }

    /**
     * Возвращает
     *
     * @param $orderEntries
     * @return OrderEntry[] $orderEntries
     */
    private function _groupByBundle($orderEntries)
    {
        $preparedPositions = $this->_preparePositionsToProcessing($orderEntries);

        $result = [];
        if (empty($preparedPositions) || !is_array($preparedPositions)) return $result;

        $ids = [];
        foreach ($preparedPositions as $key => $position) {
            if (in_array($key,$ids)) continue;
            $equipment= [];
            $equipment[] = OrderEntry::findOne($key);
            if (empty($position['set'])) {
                $ids[] = $key;
                $result[] = $equipment;
                continue;
            }
            $ids[] = $key;
            foreach ($position['set'] as $window) {
                foreach ($preparedPositions as $key2 => $position2) {
                    if (in_array($key2,$ids)) continue;
                    if ($position2['window'] == $window && $position2['carArticle'] == $position['carArticle']) {
                        $equipment[] = OrderEntry::findOne($key2);
                        $ids[] = $key2;
                        break;
                    }
                }
            }
            $result[] = $equipment;
        }

        return $result;
    }

    /**
     * Возвращает
     *
     * @param $orderEntries
     * @return OrderEntry[] $orderEntries
     */
    private function _groupBySetIndex($orderEntries)
    {
        $sets = [];
        $noSets = [];

        foreach ($orderEntries as $orderEntry) {
            if ($orderEntry->setIndex) {
                $sets[$orderEntry->setIndex][] = $orderEntry;
            }else{
                $noSets[] = [$orderEntry];
            }
        }

        return ArrayHelper::merge($sets,$noSets);
    }

    /**
     *
     *
     * @param $orderEntries
     * @return array
     */
    private  function _preparePositionsToProcessing($orderEntries)
    {
        $result = [];
        if (empty($orderEntries) || !is_array($orderEntries)) return $result;

        foreach ($orderEntries as $orderEntry) {
            if (!($orderEntry instanceof OrderEntry)) {
                throw new \DomainException('Должен быть передан обеъкт backend\modules\logistics\models\OrderEntry');
            }

            $id          = $orderEntry->id;
            $article     = $orderEntry->article;
            $carArticle  = ArticleHelper::getCarArticle($article);
            $window      = $this->_transformWindowName(ArticleHelper::getWindow($article));
            $set         = $orderEntry->settext;

            $result[$id]['article']     = $article;
            $result[$id]['carArticle']  = $carArticle;
            $result[$id]['window']      = $window;
            $result[$id]['set']         = $this->_transformSetToWindows($set,$window);
        }

        return $result;
    }

    /**
     * Функция преобразовывает текстовое поле SetText, которое содержит сведенья для комплектации, в список оконных проемов.
     *
     * @param $setText
     * @param $exceptionWindow
     * @return array
     */
    private function _transformSetToWindows($setText,$exceptionWindow)
    {
        $windows = [];
        if (empty($setText)) return $windows;

        $equipment = explode(';',$setText);
        foreach ($equipment as $window) {
            $window = $this->_transformWindowName($window);
            if (!empty($window) && $window != $exceptionWindow) {
                $windows[] = $window;
            }
        }

        return $windows;
    }

    /**
     * Функция приводит наименованеи окна в нужное нам
     *
     * @param $window
     * @return mixed
     */
    private function _transformWindowName($window)
    {
        $window = str_replace('ПШ', 'FW', $window);
        $window = str_replace('ПФ', 'FV', $window);
        $window = str_replace('ПБ', 'FD', $window);
        $window = str_replace('ЗБ', 'RD', $window);
        $window = str_replace('ЗФ', 'RV', $window);
        $window = str_replace('ЗШ', 'BW', $window);
        return $window;
    }
}