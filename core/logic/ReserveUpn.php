<?php

namespace core\logic;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpNaryadFactory;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use core\models\article\Article;
use core\models\semiFinishedArticles\SemiFinishedArticles;
use Yii;
use Exception;
use yii\helpers\ArrayHelper;

/**
 * Резервирование UPN-ов
 *
 * Class ReserveUpn
 * @package core\logic
 */
class ReserveUpn
{
    /**
     * @param $article
     * @return mixed
     */
    private static function createObj($article)
    {
        return new Article($article);
    }

    /**
     * Резервирование UPN-а под логистическую позицию.
     * Ищем на складе свободный UPN, если нашли, то резервируем его, а если не нашли, то создаём новый и резервируем его.
     * Для нового UPN создаём производственный наряд.
     *
     * @param OrderEntry $orderEntry
     * @return array
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function reserve($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        $transaction = Yii::$app->db->beginTransaction();

        $sump = null;
        $freeProdUpn = null;
        /**
         * $sump - подходящий UPN из отстойника
         */
        if ($sump = $this->_findUpnFromSump($orderEntry)) {
            $upn = $sump;
        } elseif ($freeUpn = $this->_findFreeUpn($orderEntry)) {
            $upn = $freeUpn;
        } elseif ($freeProdUpn = $this->_findFreeProdUpn($orderEntry)) {
            $upn = $freeProdUpn;
        } elseif ($newFilterUpn = $this->_addFreeFilterUpn($orderEntry)) {
            $upn = $newFilterUpn;
        }else {
            $upn = $this->_createUpn($orderEntry);
        }

        $isNew = !$upn->id;
        $isSump = (boolean)$sump;
        $isProd = (boolean)$freeProdUpn;

        $isStoragePosition = $this->_isOnStorage($orderEntry);
        $upn->reserved_id = $orderEntry->id;
        if ($isProd)
            $upn->comment =  $upn->comment ? $upn->comment . " - Reserved from prod" : "Reserved from prod";
        if (!$upn->save()) {
            $transaction->rollBack();
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось сохранить UPN')
            ];
        }
        if (($isNew || $isSump) && !$isStoragePosition && !$this->_createErpNaryad($orderEntry, $upn, $isSump)) {
            $transaction->rollBack();
            return [
                'status' => false,
                'message' => Yii::t('app', 'Не удалось создать производственный наряд для нового UPN')
            ];
        }

        if ($isProd) {
            $workOrder = $upn->erpNaryad;
            $workOrder->order_id = $orderEntry->order->source_innumber;
            $workOrder->sort = $workOrder->sort == 7 ? 6 : $workOrder->sort;
            $workOrder->status = $workOrder->status == ErpNaryad::STATUS_IN_PAUSE ? ErpNaryad::STATUS_IN_WORK : $workOrder->status;
            if (!$workOrder->save()) {
                $transaction->rollBack();
                return [
                    'status' => false,
                    'message' => Yii::t('app', 'Не удалось наряд на производстве UPN')
                ];
            }
        }

        $transaction->commit();

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    /**
     * Снятие UPN-а с резерва
     *
     * @param OrderEntry $orderEntry
     * @return array
     * @throws Exception
     */
    public function dereserve($orderEntry)
    {
        $this->_instanceOf($orderEntry);
        if (!($upn = $orderEntry->upn)) {
            return [
                'status' => true,
                'message' => Yii::t('app', 'UPN не зарезервирован')
            ];
        }
        /**
         * @var $upn Naryad
         */
        $group = UpnGroup::getAllFromGroup($upn);
        foreach ($group as $item) {
            $item->pid = null;
            $item->group = null;
            $item->save();
        }

        $upn->packed = 0;
        $upn->reserved_id = null;
        if (!$upn->save()) {
            return [
                'status' => true,
                'message' => Yii::t('app', 'Не удалось сохранить UPN')
            ];
        }

        if (($workOrder = $upn->erpNaryad)) {
            $workOrder->order_id = null;
            if (!$workOrder->save()) {
                return [
                    'status' => true,
                    'message' => Yii::t('app', 'Не удалось сохранить Наряд на производстве')
                ];
            }
        }

        return [
            'status' => true,
            'message' => Yii::t('app', 'Успешно выполнено')
        ];
    }

    /**
     * @param $orderEntry
     * @return bool
     * @throws Exception
     */
    private function _isOnStorage($orderEntry)
    {
        return CheckStoragePosition::isStoragePosition($orderEntry) && !CheckStoragePosition::canBeProduced($orderEntry->article);
    }

    /**
     * @param $orderEntry
     * @return null
     * @throws Exception
     */
    private function _findFreeUpn($orderEntry)
    {
        if ($orderEntry->is_custom && !CheckStoragePosition::isStoragePosition($orderEntry)) {
            return null;
        }
        if ($orderEntry->setIndex)
            return null;
        if ($orderEntry->withoutLabels || $orderEntry->halfStake)
            return null;

        //Если дополнительный набор клипс
        if (preg_match('/OT-(564)/', $orderEntry->article) === 1) {
            return null;
        }

        $search = self::createObj($orderEntry->article);

        //Вставил костыль, которые не позволяет оптовикам резервировать все органайзеры. Под физика всегда есть остаток 5 шт.
        $orgFlag = false;
        if (preg_match('/OT-(1189)/', $orderEntry->article) === 1) {
            if (($order = $orderEntry->order) && $order->category == 'Опт') {
                $orgFlag = true;
            }
        }
        if (!$search->balanceFact || ($orgFlag && $search->balanceFact <= 0)) {
            return null;
        }

        return $search->findFreeUpn();
    }

    /**
     * @param $orderEntry
     * @return null
     * @throws Exception
     */
    private function _findFreeProdUpn($orderEntry)
    {
        if ($orderEntry->is_custom && !CheckStoragePosition::isStoragePosition($orderEntry)) {
            return null;
        }
        if ($orderEntry->setIndex)
            return null;

        if ($orderEntry->withoutLabels || $orderEntry->halfStake)
            return null;

        //Если дополнительный набор клипс
        if (preg_match('/OT-(564)/', $orderEntry->article) === 1) {
            return null;
        }

        $search = self::createObj($orderEntry->article);

        $workOrder = ErpNaryad::find()
            ->where(['order_id' => null])
            ->andWhere(['in','article',$search->articleWithAnalogs])
            ->andWhere(['or',
                ['and',
                    ['distributed' => 0],
                    ['status' => ErpNaryad::STATUS_READY],
                    ['is not','is_new_scheme', null]
                ],
                ['status' => ErpNaryad::STATUS_IN_PAUSE],
                ['status' => ErpNaryad::STATUS_IN_WORK],
                ['and',
                    ['distributed' => 0],
                    ['ready_date' => null],
                    ['status' => null],
                    ['is not','start', null]
                ],
            ])->orderBy('id ASC')
            ->one();

        if ($workOrder) return $workOrder->upn;

        return null;
    }

    private function _addFreeFilterUpn($orderEntry)
    {
        if (preg_match('/^KF/', $orderEntry->article) !== 1) {
            return null;
        }

        $upn = new Naryad();
        $upn->article = $orderEntry->article;
        $upn->team_id = Yii::$app->team->id;
        $upn->source_id = 1;
        $upn->save();

        $row = new StorageState();
        $row->upn_id = $upn->id;
        $row->storage_id = 14;
        $row->literal = 'KF-A1000';
        $row->save();

        return $upn;
    }
    /**
     * Ищем UPN из Отстойника
     *
     * @param $orderEntry
     * @return array|null|\yii\db\ActiveRecord
     * @throws Exception
     */
    private function _findUpnFromSump($orderEntry)
    {
        if ($orderEntry->is_custom && !$this->_isOnStorage($orderEntry)) {
            return null;
        }

        if ($orderEntry->setIndex)
            return null;

        if ($orderEntry->withoutLabels || $orderEntry->halfStake)
            return null;

        //Если дополнительный набор клипс
        if (preg_match('/OT-(564)/', $orderEntry->article) === 1) {
            return null;
        }

        try {
            $obj = self::createObj($orderEntry->article);
        }catch (Exception $exception){
            return null;
        }

        $storagesArray = Storage::find()->where(['in','type',[Storage::TYPE_SUMP]])->all();
        $idsSt = ArrayHelper::map($storagesArray,'id','id');

        $query = StorageState::find()->alias('t')->where(['in','t.storage_id',$idsSt])->andWhere(['!=','t.literal','UA-A1000'])->joinWith('upn');

        $query->select(['upn_id']);
        $rows = $query->all();

        $ids = ArrayHelper::map($rows,'upn_id','upn_id');

        $query = Naryad::find();
        $query->where(['in','id',$ids]);
        $query->andWhere(['in','article',$obj->articleWithAnalogs]);
        $query->andWhere(['reserved_id'=> null]);
        $query->orderBy(['created_at' => SORT_ASC]);
        $upn = $query->one();

        return $upn;
    }

    /**
     * @param OrderEntry $orderEntry
     * @return Naryad
     */
    private function _createUpn($orderEntry)
    {
        $upn = new Naryad();
        $upn->article = $orderEntry->article;
        $upn->team_id = Yii::$app->team->id;
        $upn->source_id = 1;


        $order = $orderEntry->order;
        if ($order && $order->remoteStorage == 2) {
            /**
             * @var $obj Article./
             */
            $obj = self::createObj($orderEntry->article);
            if (in_array($obj->windowEn,['FD','RD']) && $obj->isChiko()) {
                $upn->laitovoSimple = true;
            }
        }

        return $upn;
    }

    /**
     * @param $orderEntry
     * @param $upn
     * @param $sump
     * @return bool
     */
    private function _createErpNaryad($orderEntry,$upn,$sump)
    {
        $factory = new ErpNaryadFactory();
        $workOrderBarcode = BarcodeHelper::upnToWorkOrderBarcode($upn->barcode);
        $erpNaryad = $factory->createFromOrderEntry($orderEntry, $upn->id, $workOrderBarcode, $upn->comment);
        $erpNaryad->is_new_scheme = true;
        $erpNaryad->order_id = (int)$orderEntry->order->source_innumber;

        /**
         * Если upn с отстойника, тогда делаем соответсвующую пометку наряду и сразу выдаем на ОТК участок.
         */
        if ($sump) {
            $erpNaryad->isFromSump = true;
            $erpNaryad->sumpLiteral = $upn->storageState? $upn->storageState->literal : null;
            $erpNaryad->start = Yii::$app->params['erp_otk'];
            $erpNaryad->sort = 2;
        }

        /**
         * Если это дополнительный комплект клипс, то сразу делаем его в топ спид
         */
        if (preg_match('/OT-(564)/', $orderEntry->article) === 1) {
            $erpNaryad->sort = 2;
        }

        $articleObj  = new Article($orderEntry->article);
        $carArticles = $articleObj->carArticlesWithAnalogs;
        $typeMask = $articleObj->typeSqlMask;

        /**
         * Функционал для поиска полуфабриката на складе полуфабрикатов
         */
        $semi = SemiFinishedArticles::find()
            ->where(['in','carArticle',$carArticles])
            ->andWhere(['mask' => $typeMask])
            ->andWhere(['upnId' => null])
            ->one();
        if ($semi) {
            $semi->upnId = $upn->id;
            $semi->reservedAt = time();
            $semi->save();
        }

        return $erpNaryad->save();
    }

    /**
     * @param $orderEntry
     * @throws Exception
     */
    private function _instanceOf($orderEntry)
    {
        if (!($orderEntry instanceof OrderEntry)) {
            throw new Exception('В сервис для резервирования UPN передан неверный тип объекта');
        }
    }
}