<?php
namespace core\logic;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\OrderEntry;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;


/**
 * Данный класс логики отвечает за работу с группами UPN(продуктов) - ов
 *
 * Class UpnGroup
 * @package core\logic
 */
class UpnGroup
{
    ### ИНИЦИАЛИЗАЦИЯ КЛАССА #######################################################

    /**
     * @var UpnGroup Функция для хранения экземпляра этого класса.
     */
    private  static  $_instance = null;

    /**
     * @return UpnGroup Функция возвращает экземпляр класса
     */
    private static function _getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {}
    private function __wakeup() {}
    private function __construct() {}

    ### ПЕРЕМЕННЫЕ ####################################################################################################


    ### ИНТЕРФЕЙС #####################################################################################################


    /**
     * Функция объединяет массив UPNов
     *
     * @param Naryad $upn
     * @return array
     */
    public static function getAnotherFromGroup($upn)
    {
        $handler = self::_getInstance();
        return $handler->_getAnotherFromGroup($upn);
    }

    /**
     * @param $upn
     * @return boolean
     */
    public static function boostSpeedForGroup($upn)
    {
        $handler = self::_getInstance();
        return $handler->_boostSpeedForGroup($upn);
    }

    /**
     * @param $upn
     * @param $startLocationId
     * @return bool
     */
    public static function boostSpeedForGroupNew($upn, $startLocationId)
    {
        $handler = self::_getInstance();
        return $handler->_boostSpeedForGroupNew($upn, $startLocationId);
    }

    /**
     * Функция объединяет массив UPNов
     *
     * @param $upn
     * @return array
     */
    public static function getAllFromGroup($upn)
    {
        $handler = self::_getInstance();
        return $handler->_getAllFromGroup($upn);
    }

    /**
     * Возвращает лиетру для группы
     *
     * @param $upn
     * @return null
     */
    public static function getStorageStateOfGroup($upn)
    {
        $handler = self::_getInstance();
        return $handler->_getStorageStateOfGroup($upn);
    }

    /**
     * Функция объединяет массив UPNов
     *
     * @param Naryad[] $upns
     * @return bool
     * @throws Exception
     */
    public static function groupUpns($upns)
    {
        $handler = self::_getInstance();
        return $handler->_groupUpns($upns);
    }

    /**
     * Функция объединяет массив OrderEntries
     *
     * @param OrderEntry[] $orderEntries
     * @return bool
     * @throws Exception
     */
    public static function groupOrderEntries($orderEntries)
    {
        $handler = self::_getInstance();
        return $handler->_groupOrderEntries($orderEntries);
    }

    //Какие функции нам понадобятся ????
    //Нам понадобится функция которая объеденяет в группу несколько upn

    ### ЯДРО #####################################################################################################

    /**
     * Создает новую группу UPN-ов из переданного массива UPN-ов
     *
     * @param  OrderEntry[] $orderEntries
     * @return bool
     * @throws Exception
     */
    public function _groupOrderEntries($orderEntries)
    {
        $upns = [];
        foreach ($orderEntries as $orderEntry) {
            $upn = $orderEntry->upn;
            if (!$upn) return false;
            $upns[] = $upn;
        }

        return $this->_groupUpns($upns);
    }

    /**
     * Возващает литеру для группы артикулов одну и ту же
     *
     * @param $upn
     * @return null
     */
    private function _getStorageStateOfGroup($upn)
    {
        $response = null;
        if (!($upn instanceof \common\models\logistics\Naryad) || !($group = $upn->group)) return $response;
        $another = $this->_getAnotherFromGroup($upn);
        if (empty($another)) return $response;
        foreach ($another as $item) {
            if (($state = $item->storageState))
                return $state;
        }

        return $response;
    }

    /**
     * Создает новую группу UPN-ов из переданного массива UPN-ов
     *
     * @param  Naryad[]| $upns
     * @return bool
     * @throws Exception
     */
    private function _groupUpns($upns)
    {
        if (empty($upns) || count($upns) == 1) return false;

        $query = Naryad::find();
        $query->select(['MAX(logistics_naryad.group)']);
        $num = $query->scalar();
        $num = $num ?(int)$num :0;

        $groupNum = ++$num;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($upns as $upn) {
                $upn->group = $groupNum;
                $upn->save();
            }
            $transaction->commit();
            return true;
        }catch (\Exception $exception) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param $upn
     * @return array
     */
    private function _getAnotherFromGroup($upn)
    {
        if (!($upn instanceof \common\models\logistics\Naryad) || !($group = $upn->group)) return [];
        return Naryad::find()->where(['group' => $group])->andWhere(['!=','id',$upn->id])->all() ?: [];
    }

    /**
     * @param $upn
     * @return boolean
     */
    private function _boostSpeedForGroup($upn)
    {
        if (!($upn instanceof \common\models\logistics\Naryad) || !($group = $upn->group)) return false;
        $another =  Naryad::find()->where(['group' => $group])->all();
        if ($another && count($another) > 1) {
            $ids = ArrayHelper::map($another,'id','id');
            $idsForCompletion = ErpNaryad::find()
                ->select(['id','sort'])
                ->where(['and',
                    ['in','logist_id',$ids],
                    ['and',
                        ['or',
                            ['and',['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                            ['status' => null],
                        ],
                    ],
                    ['or',['!=', 'location_id', Yii::$app->params['erp_otk']],['location_id' => null]],
                    ['or',['!=', 'start', Yii::$app->params['erp_otk']],['start' => null]],
                ])
                ->all();

            if (!empty($idsForCompletion)) {
                foreach ($idsForCompletion as $completionWorkorder) {
                    if ($completionWorkorder->sort != 5)
                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['oldSort' => $completionWorkorder->sort],['id' => $completionWorkorder->id])->execute();
                    if ($completionWorkorder->sort > 5)
                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['sort' => 5],['id' => $completionWorkorder->id])->execute();
                }
                ErpNaryad::updateAll(['isCompletion' => true], ['in','logist_id',$ids]);
                $currentNaryad = $upn->erpNaryad;
                if ($currentNaryad && $currentNaryad->sort == 5) {
                    $sort = $currentNaryad->oldSort ?: 6;
                    Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['sort' => $sort],['id' => $currentNaryad->id])->execute();
                }
            } else {
                $minSort = ErpNaryad::find()->where(['in','logist_id',$ids])->min('sort');
                $minOldSort = ErpNaryad::find()->where(['in','logist_id',$ids])->min('oldSort');
                $sort = $minSort < 5 && $minSort > 0 ? $minSort : ($minOldSort > 0 && $minOldSort != 5? $minOldSort : 6);
                ErpNaryad::updateAll(['sort' => $sort], ['in','logist_id',$ids]);
                ErpNaryad::updateAll(['isCompletion' => false], ['in','logist_id',$ids]);
            }
        }
        return true;
    }


    /**
     * @param $upn
     * @param $startLocationId
     * @return bool
     * @throws Exception
     */
    private function _boostSpeedForGroupNew($upn, $startLocationId)
    {
        if (!($upn instanceof \common\models\logistics\Naryad) || !($group = $upn->group)) return false;

        //Стандартный маршрут комплектов
        //Изгиб - Оклейка - Швейка - Клипсы - Отк
        $locationRoute = [2, 3, 4, 1, 6];
        if (!$startLocationId || !in_array($startLocationId,$locationRoute))
            return false;
        $locationBefore = [];
        $locationAfter = [];
        $flag = false;
        foreach ($locationRoute as $location) {
            if ($location == $startLocationId) {
                $flag = true; continue;
            }
            !$flag ? $locationBefore[] = $location : $locationAfter[] = $location;
        }

        $upnsFromGroup =  Naryad::find()->where(['group' => $group])->all();
        if ($upnsFromGroup && count($upnsFromGroup) > 1) {
            $ids = ArrayHelper::map($upnsFromGroup,'id','id');
            $idsForCompletionBefore = [];
            $existsForCompletionAfter = false;

            if (!empty($locationBefore))
                //Сначала ищем наряды перед
                $idsForCompletionBefore = ErpNaryad::find()
                    ->select(['id','sort'])
                    ->where(['and',
                        ['in','logist_id',$ids],
                        ['and',
                            ['or',
                                ['and',['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                                ['status' => null],
                            ],
                        ],
                        ['or',
                            ['and',['location_id' => $locationBefore],['start' => null]],
                            ['and',
                                ['or',['location_id' => null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                ['start' => $locationBefore]
                            ],
                        ],
                    ])
                    ->all();

            if (!empty($locationAfter))
                //Потом ищем наряды после
                $idsForCompletionAfter = ErpNaryad::find()
                    ->select(['id','sort'])
                    ->where(['and',
                        ['in','logist_id',$ids],
                        ['and',
                            ['or',
                                ['and',['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                                ['status' => null],
                            ],
                        ],
                        ['or',
                            ['and',['location_id' => $locationAfter],['start' => null]],
                            ['and',
                                ['or',['location_id' => null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                                ['start' => $locationAfter]
                            ],
                        ],
                    ])
                    ->all();

            //Далее ищем наряды рядом
            $idsForCompletionCurrent = ErpNaryad::find()
                ->select(['id','sort', 'oldSort'])
                ->where(['and',
                    ['in','logist_id',$ids],
                    ['and',
                        ['or',
                            ['and',['!=', 'status', ErpNaryad::STATUS_READY], ['!=', 'status', ErpNaryad::STATUS_CANCEL]],
                            ['status' => null],
                        ],
                    ],
                    ['or',
                        ['and',['location_id' => $startLocationId],['start' => null]],
                        ['and',
                            ['or',['location_id' => null],['location_id' => Yii::$app->params['erp_dispatcher']]],
                            ['start' => $startLocationId]
                        ],
                    ],
                ])
                ->all();

            //Если нет нарядов до, а есть наряды после, тогда
            if (empty($idsForCompletionBefore) && !empty($idsForCompletionAfter)) {
                foreach ($idsForCompletionAfter as $completionWorkorder) {
                    Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['isCompletion' => true],['id' => $completionWorkorder->id])->execute();
                }
                foreach ($idsForCompletionCurrent as $currentNaryad) {
                    if ($currentNaryad->sort > 5) {
                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['oldSort' => $currentNaryad->sort],['id' => $currentNaryad->id])->execute();
                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['sort' => 5],['id' => $currentNaryad->id])->execute();
                    }
                    Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['isCompletion' => true],['id' => $currentNaryad->id])->execute();
                }
                return true;
            }


            //Если есть наряды до, и нет нарядов после, тогда
            if (!empty($idsForCompletionBefore) && empty($idsForCompletionAfter)) {
                foreach ($idsForCompletionBefore as $completionWorkorder) {
                    if ($completionWorkorder->sort > 5) {
                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['oldSort' => $completionWorkorder->sort],['id' => $completionWorkorder->id])->execute();
                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['sort' => 5],['id' => $completionWorkorder->id])->execute();
                    }
                    Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['isCompletion' => true],['id' => $completionWorkorder->id])->execute();
                }
                foreach ($idsForCompletionCurrent as $currentNaryad) {
                    if ($currentNaryad->sort == 5) {
                        $sort = $currentNaryad->oldSort ?: 6;
                        Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['sort' => $sort],['id' => $currentNaryad->id])->execute();
                    }
                    Yii::$app->db->createCommand()->update(ErpNaryad::tableName(),['isCompletion' => true],['id' => $currentNaryad->id])->execute();
                }
                return true;
            }

            //Если нет нарядов до, и нет нарядов после
            if (empty($idsForCompletionBefore) && empty($idsForCompletionAfter)) {
                $minSort = ErpNaryad::find()->where(['in','logist_id',$ids])->min('sort');
                $minOldSort = ErpNaryad::find()->where(['in','logist_id',$ids])->min('oldSort');
                $sort = $minSort < 5 && $minSort > 0 ? $minSort : ($minOldSort > 0 && $minOldSort != 5? $minOldSort : 6);
                ErpNaryad::updateAll(['sort' => $sort], ['in','logist_id',$ids]);
                ErpNaryad::updateAll(['isCompletion' => false], ['in','logist_id',$ids]);
                return true;
            }
        }
        return true;
    }

    private function _getAllFromGroup($upn)
    {
        if (!($upn instanceof \common\models\logistics\Naryad) || !($group = $upn->group)) return [];
        return Naryad::find()->where(['group' => $group])->all() ?: [];
    }
}