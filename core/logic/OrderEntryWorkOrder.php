<?php

namespace core\logic;

use backend\helpers\BarcodeHelper;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpNaryadFactory;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\Naryad;
use core\entities\logisticsShipment\ShipmentManager;
use Yii;
use Exception;

/**
 * Создание заказа и upn для позиции
 *
 * Class OrderEntryWorkOrder
 * @package core\logic
 */
class OrderEntryWorkOrder
{
    ### ИНИЦИАЛИЗАЦИЯ КЛАССА #######################################################
    /**
     * @var OrderEntryWorkOrder Функция для хранения экземпляра этого класса.
     */
    private  static  $_instance = null;

    /**
     * @return OrderEntryWorkOrder
     */
    private static function _getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {}
    private function __wakeup() {}
    private function __construct() {}

    ### ИНТЕРФЕЙС ######################################################################################################

    /**
     * @param $orderEntry
     * @return null
     * @throws Exception
     */
    public static function createWorkOrderWithUpn($orderEntry)
    {
        $handler = self::_getInstance();
        $upn = $handler->_createUpnAndReserve($orderEntry);
        $handler->_createWorkOrder($orderEntry,$upn);
        return 'Вы успешно создали заказ на работы для производства!';
    }

    /**
     * @param $orderEntry
     * @return null
     * @throws Exception
     */
    public static function createUpn($orderEntry)
    {
        $handler = self::_getInstance();
        return $handler->_createUpn($orderEntry) ?: null;
    }

    /**
     * @param $orderEntry
     * @return null
     * @throws Exception
     */
    public static function createReservedUpn($orderEntry)
    {
        $handler = self::_getInstance();
        return $handler->_createUpnAndReserve($orderEntry) ?: null;
    }


    /**
     * @param $orderEntry
     * @param $upn
     * @return null
     * @throws Exception
     */
    public static function createWorkOrder($orderEntry,$upn)
    {
        $handler = self::_getInstance();
        return $handler->_createWorkOrder($orderEntry,$upn) ?: null;
    }

    /**
     * Отменяет наряд для OrderEntry
     *
     * @param $orderEntry
     * @return null
     * @throws Exception
     */
    public static function cancelWorkOrder($orderEntry)
    {
        $handler = self::_getInstance();
        $handler->_cancelWorkOrder($orderEntry);
        return 'Вы успешно отменили заказ на работы для данной позиции';
    }

    ### ЯДРО ###########################################################################################################

    /**
     * Функция создает и возвращает UPN
     *
     * @param $orderEntry
     * @return Naryad
     * @throws Exception
     */
    private function _createUpn($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        $upn = new Naryad();
        $upn->article = $orderEntry->article;
        $upn->team_id = Yii::$app->team->id;
//        $upn->comment = "Создано непосредственно реализацией";
        $upn->source_id = 1;

        return $upn;
    }

    /**
     * Функция создает и возвращает UPN
     *
     * @param $orderEntry
     * @return Naryad
     * @throws Exception
     */
    private function _createUpnAndReserve($orderEntry)
    {
        $upn = $this->_createUpn($orderEntry);
        if ($upn) {
            $upn->reserved_id = $orderEntry->id;
            $upn->save();
        }

        return $upn;
    }

    /**
     * Функция созадет и возвращает WorkOrder(ErpOrder)
     *
     * @param $orderEntry
     * @param $upn
     * @return bool
     * @throws Exception
     */
    private function _createWorkOrder($orderEntry, $upn)
    {
        $this->_instanceOf($orderEntry);

        $factory = new ErpNaryadFactory();
        $workOrderBarcode = BarcodeHelper::upnToWorkOrderBarcode($upn->barcode);
        $erpNaryad = $factory->create($orderEntry->article,$orderEntry->title,$upn->id,$workOrderBarcode,$upn->comment,$orderEntry->settext,$orderEntry->itemtext);
        $erpNaryad->is_new_scheme = true;
        $erpNaryad->order_id = (int)$orderEntry->order->source_innumber;

        return $erpNaryad->save();
    }


    /**
     * Функция отменяет рабочий наряд
     *
     * @param $orderEntry
     * @return bool
     * @throws Exception
     */
    private function _cancelWorkOrder($orderEntry)
    {
        $this->_instanceOf($orderEntry);

        $upn = $orderEntry->upn;
        if ($upn && ($workOrder = $upn->erpNaryad)) {
            if ($workOrder->status == null) {
                $workOrder->location_id = null;
                $workOrder->status = ErpNaryad::STATUS_CANCEL;
                $workOrder->logist_id = null;
                $upn->reserved_id = null;
                return $workOrder->save() && $upn->save();
            }else{
                $workOrder->logist_id = null;
                $upn->reserved_id = null;
                return $workOrder->save() && $upn->save();
            }
        }

        return false;
    }


    /**
     * Процедура производит проверку, правильный ли объект к нам пришел. Если нет, тогда выкидывает исключение.
     *
     * @param OrderEntry $orderEntry
     * @throws Exception
     */
    private function _instanceOf($orderEntry)
    {
        if (!($orderEntry instanceof OrderEntry)) {
            throw new Exception('В сервис для удаления логистической позиции передан неверный тип объекта');
        }
    }
}