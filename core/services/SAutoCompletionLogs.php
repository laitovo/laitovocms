<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 2/25/19
 * Time: 10:12 AM
 */

namespace core\services;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\StorageState;
use core\models\article\Article;
use core\models\autoCompletionBlocks\AutoCompletionBlocks;
use core\models\autoCompletionItems\AutoCompletionItems;
use core\models\autoCompletionLogs\AutoCompletionLogs;
use yii\widgets\Block;

/**
 * Сервис для работы с логами автокомплектации.
 *
 * Class SAutoCompletionLogs
 * @package core\services
 */
class SAutoCompletionLogs
{
    /**
     * Функция для регистрации автопополнения для заказов физиков
     *
     * @param Order $order
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function logIndividual(Order $order)
    {
        if ($order->category !== 'Физик') return false;

        $elements = $order->orderEntries;

        $articles = [];

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            foreach ($elements as $element) {
                if (isset($articles[$element->article])) {
                    $articles[$element->article]++;
                }else {
                    $articles[$element->article] = 1;
                }
            }
            foreach ($articles as $article => $count) {
                $result = $count <= 2 ? $count : 2;
                $objArticle = new Article($article);

                $log = new AutoCompletionLogs();
                $log->orderNumber = $order->source_innumber;
                $log->article     = $article;
                $log->requested   = $count;
                $log->inStorage   = $objArticle->balanceFact;
                $log->result      = $result;
                $log->createdAt   = time();
                $log->status      = false;
                $log->blocked     = SAutoCompletionBlocks::isBlocked($article);
//                $log->indexGroup  = $element->setIndex;

                if (!$log->save()) {
                    $transaction->rollBack();
                    return false;
                }
            }
            $transaction->commit();
            return true;
        }catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * [ Функция по записи в лог для оптовых заказов ]
     *
     * @param Order $order
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function logWholesaler(Order $order)
    {
        if ($order->category == 'Физик') return false;

        $elements = $order->orderEntries;

        $articles = [];

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            foreach ($elements as $element) {
                /**
                 * @var $state StorageState
                 */
                if (!($upn = $element->upn)) continue;
                if (!($state = $upn->storageState)) continue;
                if ($state->created_at > $element->created_at) continue;

                if (isset($articles[$element->article])) {
                    $articles[$element->article]++;
                }else {
                    $articles[$element->article] = 1;
                }

            }

            foreach ($articles as $article => $count) {
                $result = $count <= 2 ? $count : 2;
                $objArticle = new Article($article);

                $log = new AutoCompletionLogs();
                $log->orderNumber = $order->source_innumber;
                $log->article     = $article;
                $log->requested   = $count;
                $log->inStorage   = $objArticle->balanceFact;
                $log->result      = $result;
                $log->createdAt   = time();
                $log->status      = false;
                $log->blocked     = SAutoCompletionBlocks::isBlocked($article);
//                $log->indexGroup  = $element->setIndex;

                if (!$log->save()) {
                    $transaction->rollBack();
                    return false;
                }
            }
            $transaction->commit();
            return true;
        }catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * [ Фукнция обработки логов в конкретные позиции к автопополнению ]
     *
     * @param int $limit
     * @throws \yii\db\Exception
     */
    public static function processLogs($limit = 0)
    {
        /**
         * Получаем не обработнные логи
         */
        $query = AutoCompletionLogs::find()->notHandled();
        if ($limit && is_int($limit) && $limit > 0) {
            $query->limit($limit);
        }
        $logs = $query->all();

        /**
         * Если пустыне кидаем исключение
         */
        if (empty($logs))
            throw new \DomainException('Нет логов для обработки');

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            foreach ($logs as $log) {
                if ($log->blocked) {
                    $log->status = true;
                    if (!$log->save()) {
                        $transaction->rollBack();
                        throw new \DomainException('Нет удалось изменить статус лога и поставить его обработанным');
                    }
                    continue;
                };
                if (preg_match('/FD-\w+-\d+-1-[245]/',$log->article) !== 1) {
                    $log->status = true;
                    if (!$log->save()) {
                        $transaction->rollBack();
                        throw new \DomainException('Нет удалось изменить статус лога и поставить его обработанным');
                    }
                    continue;
                }

                $item = new AutoCompletionItems();
                $item->logId       = $log->id;
                $item->orderNumber = $log->orderNumber;
                $item->article     = $log->article;
                $item->requested   = $log->result;
                $item->createdAt   = time();
                $item->updatedAt   = time();
                $item->status      = true; //включил автопополнение - выключить - поставить статус true
                $item->indexGroup  = $log->indexGroup;

                $log->status = true;
                if (!$item->save() || !$log->save()) {
                    $transaction->rollBack();
                    throw new \DomainException('Нет удалось изменить статус лога и поставить его обработанным');
                }
            }
            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw new \DomainException($e->getMessage());
        }
    }

}