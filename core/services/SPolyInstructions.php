<?php


namespace core\services;

use Yii;
/**
 * Сервис для работы с инструкциями полипласта
 *
 * Class SPolyInstructions
 * @package core\services
 */
class SPolyInstructions
{
    /**
     * Путь до файлов с инструкциями полипласта
     */
    const BASE_FILE_PATH = '/img/inspoly/';

    /**
     * @param $carArticle
     * @return bool|string|null
     */
    public static function findInstruction($carArticle) {
        $filePathFs = Yii::getAlias('@backend/web'. self::BASE_FILE_PATH . $carArticle . '.pdf');
        $filePathWeb = Yii::getAlias(self::BASE_FILE_PATH . $carArticle . '.pdf');
        if (file_exists($filePathFs)) {
            return $filePathWeb;
        }
        return null;
    }

}