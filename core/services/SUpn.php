<?php

namespace core\services;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Naryad;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use common\models\laitovo\ErpNaryad as ErpNaryadAlias;
use common\models\logistics\Storage as StorageAlias;
use core\models\shipment\Shipment;
use Yii;

/**
 * Сервис для работы с UPN
 */
class SUpn
{
    /**
     * @param $upn
     * @param $erpNaryad
     * @param $state
     * @param $storage
     * @param $shipment
     * @return string|null
     */
    public static function getLocation($upn, $erpNaryad, $state, $storage, $shipment )
    {
        if ($upn === null) return null;

        /** @var $shipment Shipment */
        if ($shipment) {
            if ($shipment->logisticsStatus == Shipment::LOGISTICS_STATUS_HANDLED) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipment->id]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipment->id .' ]. Ожидается упаковка : ' . date('d.m.Y H:i:s', $shipment->handledAt) . '</a>';
            } elseif($shipment->logisticsStatus == Shipment::LOGISTICS_STATUS_PACKED) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipment->id]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipment->id .' ]. Упакован, ожидается отгрузка : ' . date('d.m.Y H:i:s', $shipment->packedAt) . '</a>';
            } elseif($shipment->logisticsStatus == Shipment::LOGISTICS_STATUS_SHIPPED) {
                return '<a href="'.  Yii::$app->urlManager->createUrl(['logistics/shipment-monitor/default/view','id' => $shipment->id]) .'" target="_blank" data-pjax="0" style="color:white">Отгрузка : [ '. $shipment->id .' ]. Отгружен : ' . date('d.m.Y H:i:s', $shipment->shippedAt) . '</a>';
            }
        }

        /** @var $erpNaryad ErpNaryad */
        /** @var $upn Naryad */
        /** @var $state StorageState */
        /** @var $storage Storage */
        //Если текущий upn находится на складе - тогда пишем склад
        if ($state) {
            if ($storage) {
                $distributed_date = $erpNaryad ? $erpNaryad->distributed_date: 0;
                $reserved_at_date = $upn->reserved_at;
                $reservedAt = max($distributed_date, $reserved_at_date);
                $reservedAt = $reservedAt ? date('d.m.Y H:i:s', $reservedAt) : null;
                $literal = $state->literal;
                $str =  ($literal && $reservedAt) ? ($literal . ' : ' . $reservedAt) : $literal;
                if ($storage->type == StorageAlias::TYPE_SUMP) {
                    return $str ." <br> Отстойник. Ожидает проверки ОТК";
                }
                return $str;
            } else {
                return 'Склад';
            }
        }
        //Если текущий upn находиться на производстве - пишем локацию
        if ($erpNaryad) {
            if ($erpNaryad->status == ErpNaryadAlias::STATUS_READY)
                return 'Диспетчер-Склад : ' . date('d.m.Y H:i:s', $erpNaryad->updated_at);

            return !($locationStart = $erpNaryad->locationstart) && ($location = $erpNaryad->location)
                ? $location->name
                : 'Диспетчер : ' . $locationStart->name;
        }

        return 'Ожидается обработка';
    }
}