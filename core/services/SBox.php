<?php
namespace core\services;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\Storage;
use core\models\box\Box;
use core\models\shipment\Shipment;
use yii\helpers\ArrayHelper;
use Exception;

/**
 * Description [
 *      Сервис.
 *      Даный сервис предоставляет функционал для работы с коробками в логистическом модуле.
 * ]
 *
 * Class SBox
 */
class SBox
{
    /**
     * [Функция возвращает массив отгрузок, которым принадежат коробки]
     *
     * @param $ids
     * @return array
     */
    public static function getShipmentIdsForBoxes($ids)
    {
        return OrderEntry::find()->where(['in','boxId',$ids])->select(['shipment_id'])->distinct()->column();
    }

    /**
     * [Функция отвечат за упаковку коробки - простановку коробки в статус упаковано]
     * @param $id
     * @return bool
     */
    public static function pack($id)
    {
        $box = Box::findOne($id);
        if (!$box)
            return false;

        $notFullyPacked = OrderEntry::find()->joinWith('upn')->where(['boxId' => $id])->andWhere(['logistics_naryad.packed' => false])->exists();
        if ($notFullyPacked)
            return false;

        $boxElements = $box->orderEntries;
        if(empty($boxElements))
            return false;

        $remote = null;
        $recipients = [];
        foreach ($boxElements as $element) {
            $order = $element->order;
            if ($remote === null)
                $remote = $order  && ($remoteStorage = $order->remoteStorage) ? $remoteStorage : 0;

            if (!in_array($order->export_username, $recipients))
                $recipients[] = $order->export_username;
        }




        if ($remote) {
            $transit = false;
            if ($remote == 2) {
                if (count($recipients) != 1 || ( count($recipients) == 1 && in_array('Склад', $recipients) )) {
                    $transit = false;
                } else {
                    $transit = true;
                }
            }

            $storage = new SRemoteStorage($remote);
            $literal = $storage->getLiteral($transit);

            if (empty($literal))
                return false;

            $box->remoteStorageLiteral = $literal['literal'];
            $box->remoteStorageBarcode = $literal['barcode'];
        }

        $box->status = Box::STATUS_PACKED;

        return $box->save();
    }

    /**
     * [Объединяет все переданные элементы в одну коробку.
     * Если элемент уже принадлежит другой коробке, тогда он не будет объединен.
     *
     * @param $elements OrderEntry[]
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function mergeInBox($elements)
    {
        if (!is_array($elements)) return  false;

        $transaction = \Yii::$app->db->beginTransaction();

        try{
            $box = new Box();
            $box->save();

            foreach ($elements as $element) {
                if (!($element instanceof OrderEntry)) {
                    $transaction->rollBack();
                    return false;
                }
                if ($element->boxId) {
                    $transaction->rollBack();
                    return false;
                }
                $element->boxId = $box->id;
                if (!$element->save()) {
                    $transaction->rollBack();
                    return false;
                };
            }
        }catch (\Exception $exception){
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    /**
     * [Устанавлиает статус коробкам - готовы к упаковке, если у них статуст - не обарбатаны.
     * Если они уже упакованы, ничего не делаем]
     * @todo Добавить проверку, что для каждого OrderEntry есть UPN, у коротго есть STORAGE STATE.
     *
     * @param $boxesIds
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function setReadyToPack($boxesIds)
    {
        $boxesIds = array_unique($boxesIds);
        $boxes = Box::find()->where(['in','id',$boxesIds])->all();
        if (count($boxes) !== count($boxesIds))
            return false;

        $transaction = \Yii::$app->db->beginTransaction();

        try{
            foreach ($boxes as $box) {
                if ($box->status == Box::STATUS_NOT_HANDLED) {
                    $box->status = Box::STATUS_READY_TO_PACK;
                    if (!$box->save()) {
                        $transaction->rollBack();
                        return false;
                    }
                    $shipment = $box->shipment;
                    if ($shipment && !$shipment->searchString) {
                        $shipment->searchString = SShipmentSearch::generateSearchString($shipment->id);
                        if (!$shipment->save()) {
                            $transaction->rollBack();
                            return false;
                        }
                    }

                }
            }
        }catch (\Exception $exception){
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    /**
     * Все коробки упакованы. Да иил нет ?
     *
     * @param $boxesIds
     * @return bool
     */
    public static function isAllPacked($boxesIds)
    {
        $boxesIds = array_unique($boxesIds);
        $boxes = Box::find()->where(['in','id',$boxesIds])->all();
        if (count($boxes) !== count($boxesIds))
            return false;

        foreach ($boxes as $box) {
            if ($box->status !== Box::STATUS_PACKED)
                return false;
        }
        return true;
    }

    /**
     * Алгоритм распределения по кробкам OrderEntries
     *
     * @param $elements OrderEntry[]
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function boxed($elements,$witBags = false)
    {
        if (!is_array($elements)) return  false;

        $transaction = \Yii::$app->db->beginTransaction();

        /**
         * @var $maxElementCount integer [ Количество основной продукции которое можно уложить в одну коробку при нормальной раскладке ]
         */
        $maxElementCount = $witBags ? 30 : 35;

        /**
         * @var $normalElementCount integer [ Количество основной продукции которое можно уложить в одну коробку при максимальной раскладке ]
         */
        $normalElementCount = $witBags ? 25 : 30;

        /**
         * @var array $exclusions [ Исключения соответствий. Индексы массива - кооэффициент ед.позиции ]
         */
        $exclusions = [
            '#^(OT)-(1190)-(47)-\d$#' => 0.5,    //Дополнительный органайзер (2)
            '#^(OT)-(1211)-(0)-\d$#'  => 0.2,    //Чехлы на спинки сиденья  (5)
            '#^(OT)-(1394)-(0)-\d$#'  => 0.02,   //Комплект магнитов (50)
            '#^(OT)-(564)-\d+-\d$#'   => 0.02,   //Комплект дополнительных зажимов клипс (50)
            '#^(OT)-(1220)-\d+-\d$#'  => 0.01,   //Ароматизатор Laitovo (Бубль Гум) (100)
            '#^(OT)-(1221)-\d+-\d$#'  => 0.01,   //Ароматизатор Laitovo (Новая машина) (100)
            '#^(OT)-(1222)-\d+-\d$#'  => 0.01,   //Ароматизатор Laitovo (Цитрус) (100)
            '#^(OT)-(1223)-\d+-\d$#'  => 0.01,   //Ароматизатор Laitovo (Ваниль) (100)
            '#^(OT)-(1224)-\d+-\d$#'  => 0.01,   //Ароматизатор Laitovo (Антитабак) (100)
        ];

        try{
            /**
             * [ 0-ая часть алгоритма распределяет элементы по заказам ]
             */

            /**
             * @var $orderedElements array [ Массив элементов, распределенных сгласно заказам ]
             */
            $orderedElements = [];

            /**
             * @var $orderedElementsICargo array [ Массив элементов, распределенных сгласно заказам для icargo]
             */
            $orderedElementsICargo = [];

            /**
             * @var $moveActElements array [ Массив элементов, которые производились по актам приема передачи]
             */
            $moveActElements = [];

            foreach ($elements as $element) {
                if ($element->upn && ($workOrder = $element->upn->erpNaryad) != null &&  ($actNumber = $workOrder->actNumber)) {
                    $moveActElements[$actNumber][] = $element;
                }
            }

            if (!empty($moveActElements)) {
                $box = new Box();
                $box->save();
                foreach ($moveActElements as $act => $items) {

                    /**
                     * @var $entry OrderEntry
                     */
                    foreach ($items as $entry) {
                        $entry->boxId = $box->id;
                        if (!$entry->save()) {
                            $transaction->rollBack();
                            return false;
                        };
                    }
                }

                $transaction->commit();
                return true;
            }



            foreach ($elements as $element) {
                //Распределяються по коробкам только готовые позиции которые есть на складе
                if ($element->upn && ( (($state = $element->upn->storageState) != null && ($storage =$state->storage) != null && ($storage->type != Storage::TYPE_SUMP)) ||
                    ErpNaryad::find()->where(['logist_id' => $element->upn->id])->andWhere(['status' => ErpNaryad::STATUS_READY])->exists())
                ) {
                    /**
                     * @var Order $order
                     */
                    $order = $element->order;

                    if ($order && $order->remoteStorage == 2) {
                        $orderedElementsICargo[$order->export_username][] = $element;
                    } else {
                        $orderedElements[$element->order_id][] = $element;
                    }
                }
            }

            if (!empty($orderedElements) && !empty($orderedElementsICargo)) {
                $transaction->rollBack();
                return false;
            }


            if ($orderedElements) {
                $shipmentBoxes= [];

                foreach ($orderedElements as $orderId => $elements) {
                    /**
                     * [ 1-ая часть алгоритма распределяет по коробкам с учетом разделения на основную и дополнительную продукцию
                     * и нормированной (оптимальной) укладки продукции в коробки ]
                     */

                    /**
                     * Установим началальные переменные
                     * @var $addCount     float   [ Суммарный объем для коробки дополнительной продукции ]
                     * @var $addBoxIndex  integer [ Начальный индекс для коробки дополнительной продукции ]
                     * @var $mainCount    float   [ Суммарный объем для коробки основной продукции ]
                     * @var $mainBoxIndex integer [ Начальный индекс для коробки основной продукции ]
                     * @var $addBoxes     array   [ Элементы дополнительной продукции, распределенные по коробкам с учетом оптимального заполнения коробки ]
                     * @var $mainBoxes    array   [ Элементы основной продукции, распределенные по коробкам с учетом оптимального заполнения коробки ]
                     */
                    $addCount     = 0;
                    $addBoxIndex  = 0;
                    $mainCount    = 0;
                    $mainBoxIndex = 0;
                    $addBoxes     = [];
                    $mainBoxes    = [];

                    $exists = [];

                    foreach ($elements as $element) {
                        /**
                         * @var $countIndex float [ Коэффициент занятого места в коробке для позиции ]
                         */
                        $countIndex = 1;

                        //Проверочки
                        if (!($element instanceof OrderEntry)) {
                            $transaction->rollBack();
                            return false;
                        }
                        if ($element->boxId) {
                            continue;
                        }

                        if ($element->setIndex) {

                            if (in_array($element->id,$exists)) {
                                continue;
                            }

                            $set = OrderEntry::find()->where(['setIndex' => $element->setIndex,'order_id' => $element->order_id])->all();
                            $exists = ArrayHelper::merge($exists,ArrayHelper::map($set,'id','id'));

                            if ($set) {
                                //Распределяем элементы по коробкам
                                $countIndex = (count($set) * $countIndex);
                                $mainCount += $countIndex;
                                if ($mainCount > $normalElementCount) {
                                    $mainBoxIndex++;
                                    $mainCount = $countIndex;
                                }
                                $mainBoxes[$mainBoxIndex]['elements'][] = ['element' => $set, 'index' => $countIndex];
                                $mainBoxes[$mainBoxIndex]['count'] = $mainCount;

                                continue;
                            }
                        }

                        //Отделяем основную продукцию от дополнительной
                        if (preg_match('#^(OT)-\S+$#', $element->article) === 1) {
                            //Ищем индекс вместимости
                            foreach ($exclusions as $pattern => $index) {
                                if (preg_match($pattern, $element->article) === 1) {
                                    $countIndex = $index;
                                    break;
                                }
                            }

                            //Распределяем элементы по коробкам
                            $addCount += $countIndex;
                            if ($addCount > $normalElementCount) {
                                $addBoxIndex++;
                                $addCount = $countIndex;
                            }
                            $addBoxes[$addBoxIndex]['elements'][] = ['element' => $element, 'index' => $countIndex];
                            $addBoxes[$addBoxIndex]['count'] = $addCount;
                            continue;
                        } else {
                            //Распределяем элементы по коробкам
                            $mainCount += $countIndex;
                            if ($mainCount > $normalElementCount) {
                                $mainBoxIndex++;
                                $mainCount = $countIndex;
                            }
                            $mainBoxes[$mainBoxIndex]['elements'][] = ['element' => $element, 'index' => $countIndex];
                            $mainBoxes[$mainBoxIndex]['count'] = $mainCount;
                            continue;
                        }
                    }

                    /**
                     * [ 2-ая часть алгоритма распределяет по коробкам с учетом разделения на основную и дополнительную продукцию
                     * и максимальной укладки продукции в коробки ]
                     */

                    /**
                     * @var $boxElementCount float [ Суммарный объем для коробки ]
                     * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                     * @var $mainBoxesMax    array [ Элементы основной продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                     */
                    $boxElementCount = 0;
                    $boxIndex = 0;
                    $mainBoxesMax = [];
                    foreach ($mainBoxes as $mainBox) {
                        foreach ($mainBox['elements'] as $element) {
                            $boxElementCount += $element['index'];
                            if ($boxElementCount > $maxElementCount) {
                                $boxIndex++;
                                $boxElementCount = $element['index'];
                            }
                            $mainBoxesMax[$boxIndex]['elements'][] = $element;
                            $mainBoxesMax[$boxIndex]['count'] = $boxElementCount;
                        }
                    }
                    if (count($mainBoxesMax) < count($mainBoxes))
                        $mainBoxes = $mainBoxesMax;


                    /**
                     * @var $boxElementCount float [ Суммарный объем для коробки ]
                     * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                     * @var $addBoxesMax     array [ Элементы дополнительной продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                     */
                    $boxElementCount = 0;
                    $boxIndex = 0;
                    $addBoxesMax = [];
                    foreach ($addBoxes as $addBox) {
                        foreach ($addBox['elements'] as $element) {
                            $boxElementCount += $element['index'];
                            if ($boxElementCount > $maxElementCount) {
                                $boxIndex++;
                                $boxElementCount = $element['index'];
                            }
                            $addBoxesMax[$boxIndex]['elements'][] = $element;
                            $addBoxesMax[$boxIndex]['count'] = $boxElementCount;

                        }
                    }
                    if (count($addBoxesMax) < count($addBoxes))
                        $addBoxes = $addBoxesMax;

                    /**
                     * [ 3-ая часть алгоритма распределяет по коробкам БЕЗ УЧЕТА разделения на основную и дополнительную продукцию
                     * и максимальной укладки продукции в коробки ]
                     */

                    $allBoxes = ArrayHelper::merge($mainBoxes,$addBoxes);

                    /**
                     * @var $boxElementCount float [ Суммарный объем для коробки ]
                     * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                     * @var $allBoxesMax     array [ Элементы любой продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                     */
                    $boxElementCount = 0;
                    $boxIndex = 0;
                    $allBoxesMax = [];
                    foreach ($allBoxes as $allBox) {
                        foreach ($allBox['elements'] as $element) {
                            $boxElementCount += $element['index'];
                            if ($boxElementCount > $maxElementCount) {
                                $boxIndex++;
                                $boxElementCount = $element['index'];
                            }
                            $allBoxesMax[$boxIndex]['elements'][] = $element;
                            $allBoxesMax[$boxIndex]['count'] = $boxElementCount;
                        }
                    }
                    if (count($allBoxesMax) < count($allBoxes))
                        $allBoxes = $allBoxesMax;


                    /**
                     * [ 4-ая часть алгоритма. Коробки заказа добавляем в коробки отгрузки ]
                     *
                     * @todo с учетом count в индексе кородки нам надо будет распределить коробки, сначала их всех пишем в один массив
                     * @todo затем перебирпем и ищем в оставщихся подходящее количество позиций
                     */

                    foreach ($allBoxes as $allBox) {
                        $shipmentBoxes[] = $allBox;
                    }

                }

                $allShipmentBoxes = [];

                $shipmentBoxes2 = $shipmentBoxes;

                $keys = [];
                foreach ($shipmentBoxes as $key => $shipmentBox) {
                    if (in_array($key,$keys)) continue;
                    $diff = $maxElementCount - $shipmentBox['count'];
                    $keys[] = $key;
                    if ($diff > 0) {
                        foreach ($shipmentBoxes2 as $key2 => $shipmentBox2) {
                            if (in_array($key2, $keys)) continue;
                            if ($diff > 0 && $shipmentBox2['count'] <= $diff) {
                                $shipmentBox['elements'] = ArrayHelper::merge($shipmentBox['elements'], $shipmentBox2['elements']);
                                $shipmentBox['count'] = $shipmentBox['count'] + $shipmentBox2['count'];
                                $keys[] = $key2;
                                $diff -= $shipmentBox2['count'];
                                if ($diff <= 0) break;
                            }
                        }
                    }

                    $allShipmentBoxes[] = $shipmentBox;
                }


                /**
                 * [ 4-ая часть алгоритма созадет уже коробки, такие какие нужны из проеобразованного массива ]
                 */

                foreach ($allShipmentBoxes as $kit) {
                    $box = new Box();
                    $box->save();
                    /**
                     * @var $entry OrderEntry
                     */
                    foreach ($kit['elements'] as $element) {
                        if (is_array($element['element'])) {
                            $items = $element['element'];
                        } else {
                            $items = [$element['element']];
                        }

                        foreach ($items as $entry) {
                            $entry->boxId = $box->id;
                            if (!$entry->save()) {
                                $transaction->rollBack();
                                return false;
                            };
                        }
                    }
                }
            }

            if ($orderedElementsICargo) {

                $shipmentBoxes= [];

                foreach ($orderedElementsICargo as $recipient => $elements) {
                    /**
                     * [ 1-ая часть алгоритма распределяет по коробкам с учетом разделения на основную и дополнительную продукцию
                     * и нормированной (оптимальной) укладки продукции в коробки ]
                     */

                    /**
                     * Установим началальные переменные
                     * @var $addCount     float   [ Суммарный объем для коробки дополнительной продукции ]
                     * @var $addBoxIndex  integer [ Начальный индекс для коробки дополнительной продукции ]
                     * @var $mainCount    float   [ Суммарный объем для коробки основной продукции ]
                     * @var $mainBoxIndex integer [ Начальный индекс для коробки основной продукции ]
                     * @var $addBoxes     array   [ Элементы дополнительной продукции, распределенные по коробкам с учетом оптимального заполнения коробки ]
                     * @var $mainBoxes    array   [ Элементы основной продукции, распределенные по коробкам с учетом оптимального заполнения коробки ]
                     */
                    $addCount     = 0;
                    $addBoxIndex  = 0;
                    $mainCount    = 0;
                    $mainBoxIndex = 0;
                    $addBoxes     = [];
                    $mainBoxes    = [];

                    $exists = [];

                    // Если заказ на пополнение склада
                    if ($recipient === 'Склад') {
                        foreach ($elements as $element) {
                            /**
                             * @var $countIndex float [ Коэффициент занятого места в коробке для позиции ]
                             */
                            $countIndex = 1;

                            //Проверочки
                            if (!($element instanceof OrderEntry)) {
                                $transaction->rollBack();
                                return false;
                            }
                            if ($element->boxId) {
                                continue;
                            }

                            if ($element->setIndex) {

                                if (in_array($element->id,$exists)) {
                                    continue;
                                }

                                $set = OrderEntry::find()->where(['setIndex' => $element->setIndex,'order_id' => $element->order_id])->all();
                                $exists = ArrayHelper::merge($exists,ArrayHelper::map($set,'id','id'));

                                if ($set) {
                                    //Распределяем элементы по коробкам
                                    $countIndex = 5;
                                    $mainCount += $countIndex;
                                    if ($mainCount > $normalElementCount) {
                                        $mainBoxIndex++;
                                        $mainCount = $countIndex;
                                    }
                                    $mainBoxes[$mainBoxIndex]['elements'][] = ['element' => $set, 'index' => $countIndex];
                                    $mainBoxes[$mainBoxIndex]['count'] = $mainCount;

                                    continue;
                                }
                            }

                            //Отделяем основную продукцию от дополнительной
                            if (preg_match('#^(OT)-\S+$#', $element->article) === 1) {
                                //Ищем индекс вместимости
                                foreach ($exclusions as $pattern => $index) {
                                    if (preg_match($pattern, $element->article) === 1) {
                                        $countIndex = $index;
                                        break;
                                    }
                                }

                                //Распределяем элементы по коробкам
                                $addCount += $countIndex;
                                if ($addCount > $normalElementCount) {
                                    $addBoxIndex++;
                                    $addCount = $countIndex;
                                }
                                $addBoxes[$addBoxIndex]['elements'][] = ['element' => $element, 'index' => $countIndex];
                                $addBoxes[$addBoxIndex]['count'] = $addCount;
                                continue;
                            } else {
                                //Распределяем элементы по коробкам
                                $mainCount += $countIndex;
                                if ($mainCount > $normalElementCount) {
                                    $mainBoxIndex++;
                                    $mainCount = $countIndex;
                                }
                                $mainBoxes[$mainBoxIndex]['elements'][] = ['element' => $element, 'index' => $countIndex];
                                $mainBoxes[$mainBoxIndex]['count'] = $mainCount;
                                continue;
                            }
                        }

                        /**
                         * [ 2-ая часть алгоритма распределяет по коробкам с учетом разделения на основную и дополнительную продукцию
                         * и максимальной укладки продукции в коробки ]
                         */

                        /**
                         * @var $boxElementCount float [ Суммарный объем для коробки ]
                         * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                         * @var $mainBoxesMax    array [ Элементы основной продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                         */
                        $boxElementCount = 0;
                        $boxIndex = 0;
                        $mainBoxesMax = [];
                        foreach ($mainBoxes as $mainBox) {
                            foreach ($mainBox['elements'] as $element) {
                                $boxElementCount += $element['index'];
                                if ($boxElementCount > $maxElementCount) {
                                    $boxIndex++;
                                    $boxElementCount = $element['index'];
                                }
                                $mainBoxesMax[$boxIndex]['elements'][] = $element;
                                $mainBoxesMax[$boxIndex]['count'] = $boxElementCount;
                            }
                        }
                        if (count($mainBoxesMax) < count($mainBoxes))
                            $mainBoxes = $mainBoxesMax;


                        /**
                         * @var $boxElementCount float [ Суммарный объем для коробки ]
                         * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                         * @var $addBoxesMax     array [ Элементы дополнительной продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                         */
                        $boxElementCount = 0;
                        $boxIndex = 0;
                        $addBoxesMax = [];
                        foreach ($addBoxes as $addBox) {
                            foreach ($addBox['elements'] as $element) {
                                $boxElementCount += $element['index'];
                                if ($boxElementCount > $maxElementCount) {
                                    $boxIndex++;
                                    $boxElementCount = $element['index'];
                                }
                                $addBoxesMax[$boxIndex]['elements'][] = $element;
                                $addBoxesMax[$boxIndex]['count'] = $boxElementCount;

                            }
                        }
                        if (count($addBoxesMax) < count($addBoxes))
                            $addBoxes = $addBoxesMax;

                        /**
                         * [ 3-ая часть алгоритма распределяет по коробкам БЕЗ УЧЕТА разделения на основную и дополнительную продукцию
                         * и максимальной укладки продукции в коробки ]
                         */

                        $allBoxes = ArrayHelper::merge($mainBoxes,$addBoxes);

                        /**
                         * @var $boxElementCount float [ Суммарный объем для коробки ]
                         * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                         * @var $allBoxesMax     array [ Элементы любой продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                         */
                        $boxElementCount = 0;
                        $boxIndex = 0;
                        $allBoxesMax = [];
                        foreach ($allBoxes as $allBox) {
                            foreach ($allBox['elements'] as $element) {
                                $boxElementCount += $element['index'];
                                if ($boxElementCount > $maxElementCount) {
                                    $boxIndex++;
                                    $boxElementCount = $element['index'];
                                }
                                $allBoxesMax[$boxIndex]['elements'][] = $element;
                                $allBoxesMax[$boxIndex]['count'] = $boxElementCount;
                            }
                        }
                        if (count($allBoxesMax) < count($allBoxes))
                            $allBoxes = $allBoxesMax;


                        /**
                         * [ 4-ая часть алгоритма. Коробки заказа добавляем в коробки отгрузки ]
                         *
                         * @todo с учетом count в индексе кородки нам надо будет распределить коробки, сначала их всех пишем в один массив
                         * @todo затем перебирпем и ищем в оставщихся подходящее количество позиций
                         */

                        foreach ($allBoxes as $allBox) {
                            $shipmentBoxes[] = $allBox;
                        }
                        //Если транзитный заказ
                    } else {
                        foreach ($elements as $element) {
                            /**
                             * @var $countIndex float [ Коэффициент занятого места в коробке для позиции ]
                             */
                            $countIndex = 1;

                            //Проверочки
                            if (!($element instanceof OrderEntry)) {
                                $transaction->rollBack();
                                return false;
                            }
                            if ($element->boxId) {
                                continue;
                            }

                            if ($element->setIndex) {

                                if (in_array($element->id,$exists)) {
                                    continue;
                                }

                                $set = OrderEntry::find()->where(['setIndex' => $element->setIndex,'order_id' => $element->order_id])->all();
                                $exists = ArrayHelper::merge($exists,ArrayHelper::map($set,'id','id'));

                                if ($set) {
                                    //Распределяем элементы по коробкам
                                    $countIndex = (count($set) * $countIndex);
                                    $mainCount += $countIndex;
                                    if ($mainCount > $normalElementCount) {
                                        $mainBoxIndex++;
                                        $mainCount = $countIndex;
                                    }
                                    $mainBoxes[$mainBoxIndex]['elements'][] = ['element' => $set, 'index' => $countIndex];
                                    $mainBoxes[$mainBoxIndex]['count'] = $mainCount;

                                    continue;
                                }
                            }

                            //Отделяем основную продукцию от дополнительной
                            if (preg_match('#^(OT)-\S+$#', $element->article) === 1) {
                                //Ищем индекс вместимости
                                foreach ($exclusions as $pattern => $index) {
                                    if (preg_match($pattern, $element->article) === 1) {
                                        $countIndex = $index;
                                        break;
                                    }
                                }

                                //Распределяем элементы по коробкам
                                $addCount += $countIndex;
                                if ($addCount > $normalElementCount) {
                                    $addBoxIndex++;
                                    $addCount = $countIndex;
                                }
                                $addBoxes[$addBoxIndex]['elements'][] = ['element' => $element, 'index' => $countIndex];
                                $addBoxes[$addBoxIndex]['count'] = $addCount;
                                continue;
                            } else {
                                //Распределяем элементы по коробкам
                                $mainCount += $countIndex;
                                if ($mainCount > $normalElementCount) {
                                    $mainBoxIndex++;
                                    $mainCount = $countIndex;
                                }
                                $mainBoxes[$mainBoxIndex]['elements'][] = ['element' => $element, 'index' => $countIndex];
                                $mainBoxes[$mainBoxIndex]['count'] = $mainCount;
                                continue;
                            }
                        }

                        /**
                         * [ 2-ая часть алгоритма распределяет по коробкам с учетом разделения на основную и дополнительную продукцию
                         * и максимальной укладки продукции в коробки ]
                         */

                        /**
                         * @var $boxElementCount float [ Суммарный объем для коробки ]
                         * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                         * @var $mainBoxesMax    array [ Элементы основной продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                         */
                        $boxElementCount = 0;
                        $boxIndex = 0;
                        $mainBoxesMax = [];
                        foreach ($mainBoxes as $mainBox) {
                            foreach ($mainBox['elements'] as $element) {
                                $boxElementCount += $element['index'];
                                if ($boxElementCount > $maxElementCount) {
                                    $boxIndex++;
                                    $boxElementCount = $element['index'];
                                }
                                $mainBoxesMax[$boxIndex]['elements'][] = $element;
                                $mainBoxesMax[$boxIndex]['count'] = $boxElementCount;
                            }
                        }
                        if (count($mainBoxesMax) < count($mainBoxes))
                            $mainBoxes = $mainBoxesMax;


                        /**
                         * @var $boxElementCount float [ Суммарный объем для коробки ]
                         * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                         * @var $addBoxesMax     array [ Элементы дополнительной продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                         */
                        $boxElementCount = 0;
                        $boxIndex = 0;
                        $addBoxesMax = [];
                        foreach ($addBoxes as $addBox) {
                            foreach ($addBox['elements'] as $element) {
                                $boxElementCount += $element['index'];
                                if ($boxElementCount > $maxElementCount) {
                                    $boxIndex++;
                                    $boxElementCount = $element['index'];
                                }
                                $addBoxesMax[$boxIndex]['elements'][] = $element;
                                $addBoxesMax[$boxIndex]['count'] = $boxElementCount;

                            }
                        }
                        if (count($addBoxesMax) < count($addBoxes))
                            $addBoxes = $addBoxesMax;

                        /**
                         * [ 3-ая часть алгоритма распределяет по коробкам БЕЗ УЧЕТА разделения на основную и дополнительную продукцию
                         * и максимальной укладки продукции в коробки ]
                         */

                        $allBoxes = ArrayHelper::merge($mainBoxes,$addBoxes);

                        /**
                         * @var $boxElementCount float [ Суммарный объем для коробки ]
                         * @var $boxIndex        int   [ Инкрементируемый индекс коробки ]
                         * @var $allBoxesMax     array [ Элементы любой продукции, распределенные по коробкам с учетом МАКСИМАЛЬНОГО заполнения коробки ]
                         */
                        $boxElementCount = 0;
                        $boxIndex = 0;
                        $allBoxesMax = [];
                        foreach ($allBoxes as $allBox) {
                            foreach ($allBox['elements'] as $element) {
                                $boxElementCount += $element['index'];
                                if ($boxElementCount > $maxElementCount) {
                                    $boxIndex++;
                                    $boxElementCount = $element['index'];
                                }
                                $allBoxesMax[$boxIndex]['elements'][] = $element;
                                $allBoxesMax[$boxIndex]['count'] = $boxElementCount;
                            }
                        }
                        if (count($allBoxesMax) < count($allBoxes))
                            $allBoxes = $allBoxesMax;


                        /**
                         * [ 4-ая часть алгоритма. Коробки заказа добавляем в коробки отгрузки ]
                         *
                         * @todo с учетом count в индексе кородки нам надо будет распределить коробки, сначала их всех пишем в один массив
                         * @todo затем перебирпем и ищем в оставщихся подходящее количество позиций
                         */

                        foreach ($allBoxes as $allBox) {
                            $shipmentBoxes[] = $allBox;
                        }
                    }
                }

                $allShipmentBoxes = $shipmentBoxes;

                /**
                 * [ 4-ая часть алгоритма созадет уже коробки, такие какие нужны из проеобразованного массива ]
                 */

                foreach ($allShipmentBoxes as $kit) {
                    $box = new Box();
                    $box->save();
                    /**
                     * @var $entry OrderEntry
                     */
                    foreach ($kit['elements'] as $element) {
                        if (is_array($element['element'])) {
                            $items = $element['element'];
                        } else {
                            $items = [$element['element']];
                        }

                        foreach ($items as $entry) {
                            $entry->boxId = $box->id;
                            if (!$entry->save()) {
                                $transaction->rollBack();
                                return false;
                            };
                        }
                    }
                }
            }

        }catch (\Exception $exception) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    public static function checkAndBoxed($shipmentId)
    {
        $elements = [];
        $shipment = Shipment::findOne($shipmentId);

        $orderEntries = $shipment->notDeletedOrderEntries;
        foreach ($orderEntries as $orderEntry) {
            if (!$orderEntry->boxId) {
                $elements[] = $orderEntry;
            }
        }
        if (!empty($elements) && !self::boxed($elements))
            throw new Exception('Не получилось создать коробку под все элементы');


        $boxes = [];
        $orderEntries = $shipment->notDeletedOrderEntries;
        foreach ($orderEntries as $orderEntry) {
            if (!($box = $orderEntry->boxId))
                throw new Exception('Не все позиции распределены по коробкам');
            if (!in_array($box,$boxes))
                $boxes[] = $box;
        }
        if (!empty($boxes) && !self::setReadyToPack($boxes))
            throw new Exception('Не получилось коробки поставить в статус - готовы к упаковке');
    }
}