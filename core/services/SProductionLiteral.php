<?php
namespace core\services;


use backend\modules\laitovo\models\ErpNaryad;
use Yii;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;

class SProductionLiteral
{
    /**
     * @param $locationId
     * @return mixed|null
     */
    public function findEmptyLiteral($locationId)
    {
        $letter = null;
        switch ($locationId) {
            case Yii::$app->params['erp_okleika']:
                $letter = 'A';
                break;
            case Yii::$app->params['erp_shveika']:
                $letter = 'B';
                break;
            case Yii::$app->params['erp_clipsi']:
                $letter = 'D';
                break;
            case Yii::$app->params['erp_otk']:
                $letter = 'C';
                break;
            case Yii::$app->params['erp_izgib']:
                $letter = 'E';
                break;
        }

        if ($letter) {
            $busyLiterals = (new Query())
                ->select('prodLiteralId')
                ->distinct()
                ->from('core_production_literal_naryad')
                ->where(['like','prodLiteralId',"$letter%",false])
                ->orderBy(new Expression('LENGTH(`prodLiteralId`), `prodLiteralId`'))
                ->column();

            //Если этот список не пуст, то есть есть занятые литеры
            if (!empty($busyLiterals)) {
                //Тогда ищем свободную литеру, начиная с 1ой, исключая все занятые литеры
                //Ищем свободные литеры до максимальной занятой
                $freeLiteralsList = array_diff($this->generateLiteralList($letter), $busyLiterals);
                //Если свободные литеры в диапазоне занятых существуют
                if (!empty($freeLiteralsList)) {
                    //Берем из них литеру, с минимальным числовым индексом
                    $flip = array_flip($freeLiteralsList);
                    $key = min($flip);
                    return $freeLiteralsList[$key];
                }
            }
        }

        //Получаем список занятых производственных литер
        $busyLiterals = (new Query())
            ->select('prodLiteralId')
            ->distinct()
            ->from('core_production_literal_naryad')
            ->orderBy(new Expression('LENGTH(`prodLiteralId`), `prodLiteralId`'))
            ->column();

        //Если этот список не пуст, то есть есть занятые литеры
        if (!empty($busyLiterals)) {
            //Тогда ищем свободную литеру, начиная с 1ой, исключая все занятые литеры
            //Ищем свободные литеры до максимальной занятой
            $freeLiteralsList = array_diff($this->generateLiteralList(), $busyLiterals);
            //Если свободные литеры в диапазоне занятых существуют
            if (!empty($freeLiteralsList)) {
                //Берем из них литеру, с минимальным числовым индексом
                $flip = array_flip($freeLiteralsList);
                $key= max($flip);
                $freeLiteral = $freeLiteralsList[$key];
            } else {
                //В противном случае, если нет свободных литер в диапазоне занятых, берем следующую литеру за всеми занятыми.
                $freeLiteral = null;
            }
        } else {
            $freeLiteral = $this->getStartLiteral();
        }

        return $freeLiteral;
    }

    public function test()
    {
        //Получаем список занятых производственных литер
        $busyLiteralsCount = (new Query())
            ->select(new Expression('COUNT(prodLiteralId) as count'))
            ->distinct()
            ->from('core_production_literal_naryad')
            ->orderBy(new Expression('LENGTH(`prodLiteralId`), `prodLiteralId`'))
            ->groupBy('prodLiteralId')
            ->having('COUNT(prodLiteralId) < 10')
            ->indexBy('prodLiteralId')
            ->column();
        echo '<pre>' . print_r($busyLiteralsCount).'</pre>';

        //Получаем список занятых производственных литер
        $busyLiterals = (new Query())
            ->select('prodLiteralId')
            ->distinct()
            ->from('core_production_literal_naryad')
            ->orderBy(new Expression('LENGTH(`prodLiteralId`), `prodLiteralId`'))
            ->column();

        echo '<pre>' . print_r($busyLiterals).'</pre>';

        $freeLiteralsList = array_diff($this->generateLiteralList(), $busyLiterals);

        echo '<pre>' . print_r($freeLiteralsList).'</pre>';

        $flip = array_flip($freeLiteralsList);

        echo '<pre>' . print_r($flip).'</pre>';

        $key= min($flip);

        echo '<pre>' . print_r($key).'</pre>';

        $freeLiteral = $freeLiteralsList[$key];

        echo '<pre>' . print_r($freeLiteral).'</pre>';

    }

    /**
     * @param $analogWorkOrders
     * @param $locationId
     * @param $capacity
     * @return false|string|null
     */
    public function findAnalogLiteral($analogWorkOrders, $locationId, $capacity)
    {
        //Находим литеры, на которых расположены аналогичные наряды
        $literalList = (new Query())
            ->select('prodLiteralId')
            ->distinct()
            ->from('core_production_literal_naryad')
            ->where(['erpNaryadId' => $analogWorkOrders])
            ->andWhere(['locationId' => $locationId])
            ->column();

        //Если такие литеры существуют
        if (!empty($literalList)) {
            //Из них выбираем литеру, которая заполнена не полностью и имеет минимальный числовой индекс
            $freeLiteral = (new Query())
                ->select(new Expression('prodLiteralId'))
                ->from('core_production_literal_naryad')
                ->where(['prodLiteralId' => $literalList])
                ->groupBy('prodLiteralId')
                ->having("COUNT(prodLiteralId) < $capacity")
                ->orderBy(new Expression('LENGTH(`prodLiteralId`), `prodLiteralId`'))
                ->scalar();
        } else {
            //В противном случае аналогичной литеры - нет.
            $freeLiteral = null;
        }
        return $freeLiteral;
    }


    public function findLiteral($analogWorkOrders, $locationId, $capacity)
    {
        $freeLiteral = null;
        //Если такие наряды были найдены,
        if (!empty($analogWorkOrders))
            $freeLiteral = $this->findAnalogLiteral($analogWorkOrders, $locationId, $capacity);
        if (!$freeLiteral)
            //Если же не найдено свободной литеры с аналогичными нарядами, получаем новую пустую литеру под наряд
            $freeLiteral = $this->findEmptyLiteral($locationId);

        return $freeLiteral;
    }

    /**
     * @param $literal
     * @param $workOrderId
     * @param $locationId
     * @throws Exception
     */
    public function registerLiteral($literal, $workOrderId, $locationId)
    {
        Yii::$app->db->createCommand()->insert('core_production_literal_naryad',
            [
                'prodLiteralId' => $literal,
                'erpNaryadId' => $workOrderId,
                'locationId' => $locationId,
                'createdAt' => time(),
            ])->execute();
    }

    /**
     * @param $workOrderId
     * @param $locationId
     * @return bool
     */
    public function existsLiteral($workOrderId, $locationId)
    {
        return (new Query())->from('core_production_literal_naryad')->where(['and',
            ['erpNaryadId' => $workOrderId],
            ['locationId' => $locationId],
        ])->exists();
    }

    /**
     * Убираем запись о том, что наряд лежал в такой то ячейке для этого участка
     *
     * @param $workOrderId
     * @param $locationId
     * @throws Exception
     */
    public static function unRegisterLiteral($workOrderId, $locationId)
    {
        $row =(new Query())->from('core_production_literal_naryad')->where(['and',
            ['erpNaryadId' => $workOrderId],
            ['locationId' => $locationId],
        ])->one();

        if ($row) {
            $logData = [
                'prodLiteralId' => $row['prodLiteralId'],
                'erpNaryadId' => $row['erpNaryadId'],
                'locationId' => $row['locationId'],
                'createdAt' => $row['createdAt'],
                'deletedAt' => time(),
            ];

            $result = Yii::$app->db->createCommand()->delete('core_production_literal_naryad',
                ['and',
                    ['erpNaryadId' => $workOrderId],
                    ['locationId' => $locationId],
                ])->execute();

            if ($result)
                Yii::$app->db->createCommand()->insert('core_production_literal_log', $logData)->execute();
        }
    }

    /**
     * Возвращает массив идентификаторов нарядов, которые зарегистрированы на той же литере, что и переданный идентификатор наряда, включая его самого.
     *
     * @param $workOrderId
     * @param $locationId
     * @return array
     */
    public static function searchWorkOrdersFromLiteral($workOrderId, $locationId): array
    {
        $literal =(new Query())
            ->select('prodLiteralId')
            ->from('core_production_literal_naryad')->where(['and',
                ['erpNaryadId' => $workOrderId],
                ['locationId' => $locationId],
            ])->scalar();

        if (!$literal) return [$workOrderId];

        return (new Query())
            ->select('erpNaryadId')
            ->from('core_production_literal_naryad')->where(['and',
                ['prodLiteralId' => $literal],
                ['locationId' => $locationId],
            ])->column();
    }

    /**
     * Возвращает массив идентификаторов нарядов, отсортированных по производственной литере.
     *
     * @param $workOrderIds
     * @param $locationId
     * @return array
     */
    public static function orderWorkOrdersByLiteralLog($workOrderIds): array
    {
        $count = 1;
        foreach ($workOrderIds as $id) {
            $locationId = ErpNaryad::find()->where(['id' => $id])->select('location_id')->scalar();
            $array[$id] = (new Query())
                    ->select('prodLiteralId')
                    ->from('core_production_literal_log')->where(['and',
                        ['erpNaryadId' => $id],
                        ['locationId' => $locationId],
                ])
            ->orderBy('createdAt DESC')
            ->scalar() ?: $count++;
        }

        natsort($array);
        return array_keys($array);
    }

    /***
     * @param $workOrderId
     * @param $locationId
     */
    public static function workOrderInfo($workOrderId, $locationId)
    {
        return (new Query())
            ->select('prodLiteralId')
            ->from('core_production_literal_naryad')->where(['and',
                ['erpNaryadId' => $workOrderId],
                ['locationId' => $locationId],
            ])->scalar();
    }

    /**
     * @param $workOrderId
     * @param $locationId
     * @return false|string|null
     */
    public static function workOrderLog($workOrderId, $locationId)
    {
        return (new Query())
            ->select('prodLiteralId')
            ->from('core_production_literal_log')->where(['and',
                ['erpNaryadId' => $workOrderId],
                ['locationId' => $locationId],
            ])->orderBy('createdAt desc')
            ->scalar();
    }

    /**
     * Функция проверяет, была ли выдана литера полностью
     *
     * @param $workOrderIds
     * @return mixed|null
     */
    public static function checkCommonLiteral($workOrderIds)
    {
        $locations = ErpNaryad::find()
            ->select('location_id')
            ->distinct()
            ->where(['id' => $workOrderIds])
            ->column();
        if (empty($locations) || count($locations) > 1)
            return null;

        $locationId = array_shift($locations);

        //Проверка для клипс
        $literals = (new Query())
            ->select('prodLiteralId')
            ->distinct()
            ->from('core_production_literal_naryad')->where(['and',
                ['erpNaryadId' => $workOrderIds],
                ['locationId' => $locationId],
            ])->column();

        if (!empty($literals) && count($literals) == 1) {
            $literal = array_shift($literals);

            $countInLiteral = (new Query())
                ->from('core_production_literal_naryad')
                ->where(['prodLiteralId' => $literal])
                ->count();

            if ($countInLiteral == count($workOrderIds))
                return $literal;
        }

        $literals = [];
        foreach ($workOrderIds as $id) {
            $literal = (new Query())
                ->select('prodLiteralId')
                ->from('core_production_literal_log')->where(['and',
                    ['erpNaryadId' => $id],
                    ['locationId' => $locationId],
                ])->orderBy('createdAt desc')
                ->scalar();
            if (!$literal) return null;
            if (!in_array($literal, $literals))
                $literals[] = $literal;
        }

        if (count($literals) > 1)
            return null;

        $literal = array_shift($literals);

        $countInLiteral = (new Query())
            ->from('core_production_literal_naryad')
            ->where(['prodLiteralId' => $literal])
            ->count();

        return $countInLiteral ? null : $literal;
    }

    /*
     * Генерирует список именованных литер
     */
    public function generateLiteralList($letter = null): array
    {
        $maxLetter = 'G';
        $list = [];
        $letterRange = range('A', $maxLetter);
        if ($letter && in_array($letter,$letterRange))
            $letterRange = [$letter];
        foreach ($letterRange as $letter) {
            $countInLetter = ($letter == 'G' ? 20 : 20);
            foreach (range(1,$countInLetter) as $number) {
                $list[] = $letter . $number;
            }
        }
        natsort($list);
        return $list;
    }

    /**
     * Возвращает первую литеру из списка литер
     *
     * @return mixed
     */
    private function getStartLiteral()
    {
        $list = $this->generateLiteralList();
        return array_shift($list);
    }

    /**
     * Функция возвращает массив со статистическими значениям о заполненности литер и другим значениями аналитики алгоритма
     */
    public function statistics() : array
    {
        //Наряды на производстве, под которые необходимы производственные литеры.
        $countWorkOrdersNeededInLiterals = ErpNaryad::find()
            ->select('id')
            ->where(['status' => [ErpNaryad::STATUS_IN_WORK,ErpNaryad::STATUS_IN_PAUSE]])
            ->andWhere(['or',['!=','start',Yii::$app->params['erp_izgib']], ['start' => null]] )
            ->andWhere(['or', ['!=','location_id',Yii::$app->params['erp_otk']], ['location_id' => null]] )
            ->andWhere(['REGEXP', 'article', '(FV|FD|RD|RV|BW)-.+'])
            ->andWhere(['actNumber' => null])
            ->count();

        //Всего литер
        $countLiterals = count($literalList = $this->generateLiteralList());

        //Занято литер
        $countBusyLiterals = (new Query())
            ->select('prodLiteralId')
            ->distinct()
            ->from('core_production_literal_naryad')
            ->count();

        //Занято нарядами
        $countBusyPlaces = (new Query())
            ->select('prodLiteralId')
            ->from('core_production_literal_naryad')
            ->count();

        //Пустых литер
        $countEmptyLiterals = $countLiterals - $countBusyLiterals;

        //Пустых мест для нарядов
        $countWorkOrdersInLiteral = 5;
        $countPlaceForWorkOrders = $countLiterals * $countWorkOrdersInLiteral;

        $freeWorkOrdersPlace = $countPlaceForWorkOrders - $countBusyPlaces;

        $occupancyRate =  round($countBusyPlaces / $countPlaceForWorkOrders * 100 , 2);

        return [
            'countLiterals' => $countLiterals, //Всего производственных литер
            'countEmptyLiterals' => $countEmptyLiterals, //Количество пустых литер
            'countBusyLiterals' => $countBusyLiterals, //Количество НЕ пустых литер ( литеры, которые содержат хотя бы 1 наряд)

            'countPlaceForWorkOrders' => $countPlaceForWorkOrders, //Количество мест под наряды
            'countWorkOrdersNeededInLiterals' => $countWorkOrdersNeededInLiterals, //Количество нарядов, нуждающихся в литере
            'freeWorkOrdersPlace' => $freeWorkOrdersPlace, //Количество свободных мест для нарядов, нуждающихся в литере
            'occupancyRate' => $occupancyRate, //Коэффициент заполненности в %
            'status' => ($occupancyRate > 85 ? 'danger':'success'),
        ];
    }


    /**
     * Функция сообщает нам, что стоит переложить комплекты более эффективно
     *
     * @param $literal
     * @return mixed
     */
    public function isNotEffectiveLiteral($literal)
    {
        //Всего литер
        $literalList = $this->generateLiteralList();

        //Занято литер
        $busyLiterals = (new Query())
            ->select('prodLiteralId')
            ->distinct()
            ->from('core_production_literal_naryad')
            ->column();

        $emptyLiterals = array_diff($literalList,$busyLiterals);

        return in_array($literal,$emptyLiterals) && count($emptyLiterals) < 15;
    }
}