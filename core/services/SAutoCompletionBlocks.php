<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 2/25/19
 * Time: 10:12 AM
 */

namespace core\services;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use backend\modules\logistics\models\StorageState;
use core\models\article\Article;
use core\models\autoCompletionBlocks\AutoCompletionBlocks;
use core\models\autoCompletionItems\AutoCompletionItems;
use core\models\autoCompletionLogs\AutoCompletionLogs;
use yii\widgets\Block;

/**
 * Сервис для работы с блоками автокомплектаци
 *
 * Class SAutoCompletionBlocks
 * @package core\services
 */
class SAutoCompletionBlocks
{
    /**
     * [ Функция проверки блокировки всех
     *
     * @param $article
     * @return bool
     */
    public static function isBlocked($article)
    {
        $masks = [];
        $articleObj = new Article($article);

        $masks[] = $article;
        $masks[] = $articleObj->carSqlMask;
        $masks[] = $articleObj->productSqlMask;

        foreach ($masks as $mask) {
            if (AutoCompletionBlocks::find()->oneByMask($mask))
                return true;
        }

        return false;
    }

    /**
     * [ Функция блокировки конкретного артикула ]
     *
     * @param $article
     * @return bool
     */
    public static function blockArticle($article)
    {
        $articleObj = new Article($article);

        if (AutoCompletionBlocks::find()->maskExists($articleObj->id))
            return true;

        $block = new AutoCompletionBlocks();
        $block->catalogMask  = $articleObj->id;
        $block->carTitle     = $articleObj->carName;
        $block->productTitle = $articleObj->title;

        return $block->save();
    }

    /**
     * [ Функция блокировки конкретного автомобиля ]
     *
     * @param $article
     * @return bool|AutoCompletionBlocks|null
     */
    public static function blockCar($article)
    {
        $articleObj = new Article($article);
        if (!$articleObj->car) {
            return false;
        }

        if ($block = AutoCompletionBlocks::find()->oneByMask($articleObj->carSqlMask))
            return $block;

        $block = new AutoCompletionBlocks();
        $block->catalogMask  = $articleObj->carSqlMask;
        $block->carTitle     = $articleObj->carName;

        return $block->save() ? $block : null;
    }
}