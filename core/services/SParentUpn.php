<?php
namespace core\services;

use backend\modules\logistics\models\Naryad;

/**
 * Description [
 *      Сервис.
 *      Работа с "ПАПОЙ" UPN ( PARENT UPN FOR GROUP OF UPN's)
 * ]
 *
 * Class SParentUpn
 */
class SParentUpn
{
    /**
     * Созадем папу UPN. Если создался, тогда возвращаем, нет - тогда пустоту.
     *
     * @param $ids array [ Массив id upn ов]
     * @return Naryad|null
     * @throws \yii\db\Exception
     */
    public static function create($ids)
    {
        $upns = Naryad::find()->where(['in','id',$ids])->all();
        if (count($upns) !== count($ids)) {
            return null;
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $parentUpn = new Naryad();
            $parentUpn->team_id   = 1;
            $parentUpn->source_id = 2;
            $parentUpn->isParent  = true;

            if (!$parentUpn->save()) {
                $transaction->rollBack();
                return null;
            }

            foreach ($upns as $upn) {
                /** @var $upn Naryad */
                $upn->pid = $parentUpn->id;
                if (!$upn->save()) {
                    $transaction->rollBack();
                    return null;
                };
            }

            $transaction->commit();
            return $parentUpn;

        } catch (\Exception $exception) {
            $transaction->rollBack();
            return null;
        }
    }

    /**
     * @param $ids
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getCommonParent($ids)
    {
        $upns = Naryad::find()->where(['in','id',$ids])->all();
        if (count($upns) !== count($ids)) {
            return null;
        }

        $firstParent = null;
        foreach ($upns as $upn) {
            /** @var $upn Naryad */
            $firstParent = $firstParent ?: $upn->pid;
            $currentParent = $upn->pid;
            if (!$currentParent || $currentParent != $firstParent)
                return null;
        }

        $parent = Naryad::find()->where(['id' => $firstParent])->one();

        return $parent ? $parent : null;
    }
}