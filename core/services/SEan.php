<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 2/25/19
 * Time: 10:12 AM
 */

namespace core\services;

use common\models\logistics\Naryad;
use core\models\article\Article;
use core\models\autoCompletionBlocks\AutoCompletionBlocks;
use core\models\ean\Ean;
use obmen\models\laitovo\Cars;


/**
 * Сервис для работы с номерами EAN
 *
 * Class SAutoCompletionBlocks
 * @package core\services
 */
class SEan
{
    /**
     * @param array $articles
     * @return string|null
     */
    public static function getEanForSet(array $articles)
    {
        /**
         * @var $carArticle integer Артикул автомобиля
         */
        $carArticle = null;

        /**
         * @var $car Cars Автомобиль
         */
        $car = null;

        /**
         * @var $brand null|string Автомобиль
         */
        $brand = null;

        /**
         * @var $elements array Элементы комлпекта артикулов
         */
        $elements = [];

        foreach ($articles as $article) {
            $articleObj = new Article($article);
            if ($carArticle === null) {
                $carArticle = $articleObj->carArticle;
                $car = $articleObj->car;
            } elseif ($carArticle !== $articleObj->carArticle || !$carArticle || !$car)
                return null;

            if (isset($elements[$articleObj->windowEn])) return null;

            $elements[$articleObj->windowEn] = $articleObj->windowEn;

            if ($articleObj->windowEn == 'RD' && $articleObj->brand == 'Chiko') {
                $brand = Ean::TYPE_SIMPLE;
            } elseif ($articleObj->windowEn == 'RD' && $articleObj->brand != 'Chiko') {
                $brand =  Ean::TYPE_LAITOVO;
            }
        }

        $carWindows = [
            'RD' => (int)$car->json('rd'),
            'RV' => (int)$car->json('rv'),
            'BW' => (int)$car->json('bw'),
        ];

        foreach ($carWindows as $carWindow => $status) {
            if ($status && !isset($elements[$carWindow])) {
                return null;
            } else {
                unset($elements[$carWindow]);
            }
        }

        if (count($elements)) return null;
        if ($brand === null) return null;

        $ean = Ean::find()->where(['carArticle' => $carArticle])->andWhere(['type' => $brand])->one();

        return $ean? $ean->ean : null;
    }
}