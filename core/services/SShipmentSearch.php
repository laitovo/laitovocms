<?php
namespace core\services;


use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use core\models\shipment\Shipment;

/**
 * Данный сервис используется для поиска по произвольному сочетанию опредленных объектов (Отгрузок)
 *
 * Class SShipmentSearch
 */
class SShipmentSearch
{
    /**
     * Функция для генерации строки поиска по отгрузкам
     *
     * @param  int $shipmentId
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function generateSearchString($shipmentId)
    {
        $shipment = Shipment::findOne($shipmentId);
        if (!$shipment)
            return '';
        /**
         * @var $parts array [ Термины, ключевые слова для поиска по отгрузкам ]
         */
        $parts = [];
        $parts[] = $shipment->id;
        $parts[] = $shipment->shipmentDate ? \Yii::$app->formatter->asDate($shipment->shipmentDate, 'dd.MM.yyyy') : $shipment->shipmentDate;

        $orders = $shipment->orders;

        if (!empty($orders))
            foreach ($orders as $order) {
                $parts[] = $order->manager;
                $parts[] = $order->delivery;
                $parts[] = $order->address;
                $parts[] = date('d.m.Y', $order->created_at);
                $parts[] = $order->username;
                $parts[] = $order->userphone;
                $parts[] = $order->useremail;
                $parts[] = $order->category;
                $parts[] = $order->source_innumber;
                if ($order->is_need_ttn) {
                    $parts[] = '(С ТТН)';
                }
                if ($order->isCOD) {
                    $parts[] = '(наложка)';
                }
            }

        return mb_strtolower(implode('|', $parts));
    }

    /**
     * Функция для фильтрации определнных отгрузок
     *
     * @param $search
     * @param $ids
     * @return array|Shipment[]
     */
    public static function search($search,$ids)
    {
        $searchPattern = mb_strtolower($search);
        return Shipment::find()->where(['like','searchString',$searchPattern])->andWhere(['in','id',$ids])->select('id')->column();
    }
}