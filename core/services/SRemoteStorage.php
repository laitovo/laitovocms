<?php
namespace core\services;

use Yii;
use yii\httpclient\Client;

/**
 * Сервис для работы с удаленным складом
 *
 * Class SRemoteStorage
 * @package core\services
 */
class SRemoteStorage
{
    /** @var \yii\httpclient\Request  */
    private $request;

    public function __construct($remoteStorage)
    {
        $client = new Client([
            'baseUrl' => Yii::$app->params['remoteStorage']['host'],
        ]);
        $this->request = $client
            ->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('POST')
            ->addHeaders(['Authorization' => 'Bearer ' . Yii::$app->params['remoteStorage']['token'][$remoteStorage]]);
    }

    public function getLiteral(bool $transit = false)
    {
        $url = 'get-literal?transit=' . ($transit ? 1 : 0);

        /** @var \yii\httpclient\Response $response */
        $response = $this->request->setUrl($url)->send();
        if ($response->isOk) {
            $data = $response->data;
            return $data;
        }

        return [];
    }

    public function postLiteral($arr)
    {
        $url = 'add-in-literal';

        /** @var \yii\httpclient\Response $response */
        $response = $this->request->setUrl($url)->setData($arr)->send();
        if ($response->isOk) {
            return true;
        }

        return false;
    }
}