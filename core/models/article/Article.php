<?php
namespace core\models\article;

use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpProductType;
use backend\modules\laitovo\models\PatternRegister;
use common\models\laitovo\CarClips;
use common\models\laitovo\Cars;
use common\models\laitovo\ClipsScheme;
use backend\modules\logistics\models\Storage;
use backend\modules\logistics\models\StorageState;
use backend\modules\logistics\models\Naryad;
use core\models\articleStatistics\ArticleStatistics as SaleStatistics;
use core\models\articleStatistics\ArticleStatisticsGenerator;
use core\logic\CheckStoragePosition;
use core\models\articleAnalog\ArticleAnalog;
use core\models\articleAnalog\ArticleAnalogGenerator;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class Article
 *
 * @property $id                     string                [ id Артикула ]
 * @property $car                    Cars|null             [ Автомобиль из артикула ]
 * @property $carForm                Cars|null             [ Карточка автомобиля из артикула ]
 * @property $carArticle             int|null              [ Артикул автомобиля в системе, которому принадлежит артикул ]
 * @property $carName                string|null           [ Наименование автомобиля, для которого создан данный артикул ]
 * @property $carEnName              string|null           [ Наименование автомобиля (EN), для которого создан данный артикул ]
 * @property $carNameWithAnalogs     array                 [ Наименование автомобиля, для которого создан данный артикул + аналоги]
 * @property $carArticlesWithAnalogs array                 [ Артикул автомобиля, для которого создан данный артикул + аналоги]
 * @property $window                 string|null           [ Оконный проем (RU) ]
 * @property $windowEn               string|null           [ Оконный проем (EN) ]
 * @property $brand                  string                [ Бренд, которому принадлежит артикул ]
 * @property $title                  string                [ Наименование вида продукта для артикула ]
 * @property $type                   string                [ Тип изготовления артикула ]
 * @property $type_clips             string                [ Тип клипс у артикула ]
 * @property $countWindow            integer               [ Количество оконных проемов ]
 * @property $isTape                 boolean               [ В текущий момент крепления для артикула изготавливаем на скотче (Да, Нет) ]
 * @property $isRvTriangular         boolean               [ Является ли данный артикул - треульной задней форточкой ]
 * @property $product                ErpProductType|null   [ Продукт в системе - к которому относится данный артикул ]
 * @property $groupName              string|null           [ Наименование группы - в которую входит артикул ]
 * @property $articleWithAnalogs     array                 [ Список артикулов с аналогами ]
 * @property $saleStatistics         SaleStatistics|null   [ Объект статистику по артикулу ]
 * @property $increase               integer               [ Коэффициент увеличения продаж в будущем месяце на основе предыдущего года ]
 * @property $saleRate               integer               [ Количесто продаж за последний (N период) ]
 * @property $saleLast               integer               [ Количесто продаж в предыдушем месяце ]
 * @property $saleSixMonth           integer               [ Количесто продаж в 6 предыдуших месяцах ]
 * @property $salePlan               integer               [ Планируемуе продажи в месяце на базе статисике ]
 * @property $makeRate               integer               [ Как быстро изготавливается такая продукция на производстве ]
 * @property $fixRate                integer               [ Корректировочный коэффициент скорости производства ]
 * @property $minFrequency           integer               [ Коэффициент для артикула, Мин. частота реализации, мес ]
 * @property $salePrevious           integer               [ Количесто продаж в предпредыдушем месяце ]
 * @property $balancePlan            integer               [ Планируемый остаток ]
 * @property $balanceFact            integer               [ Фактический остаток остаток ]
 * @property $balanceProdFree        integer               [ Текущее количество на производстве на пополнение склада ]
 *
 * @property $articleType            integer               [ Исполнение из артикула ]
 * @property $articleCloth           integer               [ Ткань из артикула ]
 *
 * @property $carSqlMask             integer               [ Маска для поиска в базе по машине ]
 * @property $productSqlMask         integer               [ Маска для поиска в базе по продукту ]
 * @property $typeSqlMask            integer               [ Маска для поиска в базе по исполнению независимо от ткани ]
 *
 * @property $needHook               boolean               [ Маска для поиска в базе по продукту ]
 */
class Article extends Model
{
    /**
     * @var string $id [ Уникальный идентификатор артикула. Например FD-R-123-1-2 ]
     */
    private $_id;


    private $_articleWithAnalogs;

    /**
     * @var Cars|null $car [ Автомобиль из артикула ]
     */
    private $_car;

    /**
     * @var CarsForm|null $carForm [ Карточка автомобиля для артикула ]
     */
    private $_carForm;

    /**
     * @var $_carArticle integer|null [ Артикул автомобиля ]
     */
    private $_carArticle;

    /**
     * @var array|null $_carNameWithAnalogs [ Список наименований автомобилей, на которые можно установить данный артикул ]
     */
    private $_carNameWithAnalogs;

    /**
     * @var array|null $_carArticlesWithAnalogs [ Список артикулов автомобилей, на которые можно установить данный артикул ]
     */
    private $_carArticlesWithAnalogs;

    /**
     * @var ErpProductType|null $_product  [ Продукт в системе - к которому относится данный артикул ]
     */
    private $_product;

    /**
     * @var $_value0 string|null [ Зачение с индексом 0 после парсинга артикула ]
     */
    private $_value0;

    /**
     * @var $_value1 string|null [ Зачение с индексом 0 после парсинга артикула ]
     */
    private $_value1;

    /**
     * @var $_value2 string|null [ Зачение с индексом 0 после парсинга артикула ]
     */
    private $_value2;

    /**
     * @var $_value3 string|null [ Зачение с индексом 0 после парсинга артикула ]
     */
    private $_value3;

    /**
     * @var $_value4 string|null [ Зачение с индексом 0 после парсинга артикула ]
     */
    private $_value4;

    /**
     * @var $_valueList array|null [ Массив значений после парсинга артикула ]
     */
    private $_valueList;

    /**
     * @var $_window string|null [ Оконный проем (RU) ]
     */
    private $_window;

    /**
     * @var $_windowEn string|null [ Оконный проем (EN) ]
     */
    private $_windowEn;

    /**
     * @var $_countWindow integer|null [ Количество оконный проемов в автомобиле ]
     */
    private $_countWindow;

    /**
     * @var $_saleStatistics SaleStatistics [ Объект статистики по продажам данного артикула ]
     */
    private $_saleStatistics;

    /**
     * @var $_balanceFact integer [ Фактическое количество артикула на остатках ]
     */
    private $_balanceFact;

    /**
     * @var $_balanceProdFree integer [ Фактическое количество артикула на производстве на пополнение]
     */
    private $_balanceProdFree;


    /**
     * Article constructor.
     * @param $id
     * @param array $config
     */
    public function __construct($id, array $config = [])
    {
        $this->_id = mb_strtoupper($id);
        $this->explode();
        parent::__construct($config);
    }

    /**
     * @return string [ Функция возвращает идентификатор артикула ]
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * [ Процедура, которая парсит артикул на составляющие и записывает значения по артикулу ]
     */
    private function explode()
    {
        $value = explode('-', $this->_id);
        $this->_valueList = $value;
        $this->_value0 = $value[0]??null;
        $this->_value1 = $value[1]??null;
        $this->_value2 = $value[2]??null;
        $this->_value3 = $value[3]??null;
        $this->_value4 = $value[4]??null;
    }

    /**
     * @return bool [ Функция отвечает - является ли артикул валидным ]
     */
    public function valid()
    {
        $count = count($this->_valueList);
        if ($count > 5 || $count < 4)
            return false;
        /**
         * @var $car Cars
         */
        if ($count == 5 && (!in_array($this->_value0,['FW','FV','FD','RD','RV','BW']) || !$this->car || $this->_value1 != $this->car->getTranslit(mb_substr($car->mark,0,1))))
            return false;

        if ($count == 4 && !in_array($this->_value0,['OT']) )
            return false;

        return true;
    }

    /**
     * [ Функция возвращает артикул автомобиля ]
     *
     * @return int|null|string
     */
    public function getCarArticle()
    {
        if ($this->_carArticle) return $this->_carArticle;

        $this->_carArticle = $this->_value0 && $this->_value0 != 'OT' ? $this->_value2 : null;
        return $this->_carArticle;
    }

    /**
     * [ Функция возврвщает модель автомобиля из системы, которому принадлежит артикул. ]
     *
     * @return array|Cars|null|\yii\db\ActiveRecord
     */
    public function getCar()
    {
        if ($this->_car) return $this->_car;
        return ($this->carArticle ? ($this->_car = Cars::find()->where(['article' => $this->carArticle])->one()) : ($this->_car = null));
    }


    /**
     * [ Функция возвращает карточку автомобиля, которому принадлежит артикул ]
     *
     * @return CarsForm|null
     * @throws \yii\web\NotFoundHttpException
     */
    public function getCarForm()
    {
        if ($this->_carForm) return $this->_carForm;
        if ($this->car)
            $this->_carForm = $form = new CarsForm($this->car->id);
        return $this->_carForm;
    }

    /**
     * [ Функция говорит что артикул для оконного проема BW ]
     *
     * @return bool
     */
    public function windowBW()
    {
        return ($this->_value0 == 'BW');
    }

    /**
     * [ Функция говорит что артикул продукта Chiko на магнитах ]
     *
     * @return bool
     */
    public function getChikoInMagnet()
    {
        return (($this->_value3 == 67 || $this->_value3 == 72) && ($this->_value4 == 5));
    }

    /**
     * [ Функция говорит что артикул продукта Laitovo на магнитах ]
     *
     * @return bool
     */
    public function laitovoInMagnet()
    {
        if($this->_value3 == 67  && $this->_value4 == 2) return true;
        if($this->_value3 == 67  && $this->_value4 == 4) return true;
        if($this->_value3 == 70  && $this->_value4 == 2) return true;
        if($this->_value3 == 70  && $this->_value4 == 1) return true;
        if($this->_value3 == 70  && $this->_value4 == 8) return true;
        if($this->_value3 == 71  && $this->_value4 == 2) return true;
        if($this->_value3 == 72  && $this->_value4 == 2) return true;
        if($this->_value3 == 72  && $this->_value4 == 4) return true;
        if($this->_value3 == 72  && $this->_value4 == 8) return true;
        return false;
    }

    public function isOnMagnets()
    {
        return in_array($this->_value3,[67,70,71,72]);
    }

    public function isNoName()
    {
        return $this->_value3 == 55;
    }

    /**
     * [ Функция возвращает бренд, которому принадлежит артикул ]
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->_value4 == 5 ? 'Chiko' : 'Laitovo';
    }

    /**
     * [ Функция возвращает ответ на вопрос, являеться ли артикул представителем бренда Chiko ]
     *
     * @return string
     */
    public function isChiko()
    {
        return $this->_value4 == 5;
    }

    /**
     * [ Функция говорит что артикул продукта Chiko Magnet ]
     *
     * @return bool
     */
    public function chikoMagnet()
    {
        return (($this->_value3 == 45 && $this->_value4 == 5) || ($this->_value3 == 48 && $this->_value4 == 5));
    }

    /**
     * [ Функция говорит что артикул продукта Козырек Магнет]
     *
     * @return bool
     */
    public  function vizorMagnet()
    {
        return $this->_value3 == 68;
    }

    /**
     * [ Функция говорит что артикул продукта Laitovo Магнет]
     *
     * @return bool
     */
    public function laitovoMagnet()
    {
        return ($this->_value3 == 45 && $this->_value4 == 4
             || $this->_value3 == 45 && $this->_value4 == 8
             || $this->_value3 == 45 && $this->_value4 == 2
             || $this->_value3 == 48 && $this->_value4 == 4
             || $this->_value3 == 48 && $this->_value4 == 8
             || $this->_value3 == 48 && $this->_value4 == 2);
    }

    /**
     * [ Функция говорит что артикул продукта является Маскиткой]
     *
     * @return bool
     */
    public function mosquitoNet()
    {
        return ($this->_value3 == 49);
    }

    /**
     * Возвращает тип оконного проема, для которого изготавливается продукция.
     * Если это дополнительная продукция, возвращаем null
     *
     * @return mixed|null
     */
    public  function getWindow()
    {
        if ($this->_window) return $this->_window;

        $value = $this->_value0;
        $value = str_replace('FW', 'ПШ', $value);
        $value = str_replace('FV', 'ПФ', $value);
        $value = str_replace('FD', 'ПБ', $value);
        $value = str_replace('RD', 'ЗБ', $value);
        $value = str_replace('RV', 'ЗФ', $value);
        $value = str_replace('BW', 'ЗШ', $value);

        $this->_window = $value != 'OT' ? $value : null;

        return $this->_window;
    }

    /**
     * Возвращает тип оконного проема, для которого изготавливается продукция.
     * Если это дополнительная продукция, возвращаем null
     *
     * @return mixed|null
     */
    public function getWindowEn()
    {
        if ($this->_windowEn) return $this->_windowEn;

        $this->_windowEn = $this->_value0 != 'OT' ? $this->_value0 : null;
        return $this->_windowEn;
    }

    /**
     * [ Функция возвращает количество оконных проемов автомобиля ]
     *
     * @return int
     */
    public function getCountWindow()
    {
        if ($this->_countWindow) return $this->_countWindow;

        switch ($this->window) {
            case 'ПШ':
                $count = 1;
                break;
            case 'ПФ':
                $count =  2;
                break;
            case 'ПБ':
                $count =  2;
                break;
            case 'ЗБ':
                $count =  2;
                break;
            case 'ЗФ':
                $count =  2;
                break;
            case 'ЗШ':
                $count =  1;
                break;
            default:
                $count =  0;
        }
        $this->_countWindow = $count;
        return $this->_countWindow;
    }

    /**
     * [ Функция говорит нам, установлено ли на текущий момент - использовать крепления на скотче, вместо клипс ]
     *
     * @return bool
     */
    public function isTape()
    {
        if (!$this->_carForm) return false;
        return (($this->window == 'ПБ' && $this->_carForm->fd_use_tape) || ($this->window == 'ЗБ' && $this->_carForm->rd_use_tape));
    }

    /**
     * [ Функция говорит нам, что данный артикул - является треульной форточкой ]
     *
     * @return bool
     */
    public function isRvTriangular()
    {
        if (!$this->_carForm) return false;
        return $this->_carForm->rv_laitovo_standart_forma == 'Треугольная';
    }

    /**
     * [ Функция говорит нам, что данный артикул - является квадратной форточкой ]
     *
     * @return bool
     */
    public function isRVSquare()
    {
        if (!$this->_carForm) return false;
        return $this->_carForm->rv_laitovo_standart_forma == 'Квадратная';
    }

    /**
     * [ Функция говорит нам, что данный артикул - является складной задней шторкой ]
     *
     * @return bool
     */
    public function isBwSklad()
    {
        $car = $this->car;
        if (!$car) return false;

        return $this->_value0 == 'BW' &&  $this->_value3 == 7 ;
    }

    /**
     * [ Функция говорит нам, что данный артикул - изготовлен по технологии "Magnet" ]
     *
     * @return bool
     */
    public  function isMagnet()
    {
        $car = $this->car;
        if (!$car) return false;

        return $this->_value0 &&  ($this->_value3 == 45 ||  $this->_value3 == 48) ;
    }

    /**
     * [ Функция по артикулу смотрит и говорит - нужно лекало или нет для изготовление продукта. ]
     *
     * @return bool
     */
    public function isNeedTemplate()
    {
        return $this->isDontLook() ;
    }

    /**
     * Функция по артикулу смотрит и говорит - явялется ли продукт донтулуком.
     *
     * @return bool
     */
    public function isDontLook()
    {
        $car = $this->car;
        if (!$car) return false;

        return $this->_value0 && $this->_value0 != 'OT' && ($this->_value3 == 4 ||  $this->_value3 == 5);
    }

    /**
     * [ Функция по артикулу смотрит и говорит - существует ли лекало на данный вид продукта или нет. ]
     *
     * @return bool
     */
    public function isTemplateExists()
    {
        $cars = [];

        $car = $this->car;
        if (!$car) return false;

        $analogs = $car->analogs;

        foreach ($analogs as $analog) {
            $elements = $analog->json('elements');
            if (in_array($this->window,$elements)) {
                $cars[] = $analog->analog_id;
            }
        }

        $cars[] = $car->id;

        if (!$this->isNeedTemplate()) return false;

        switch ($this->_value3) {
            case 4 :
                return PatternRegister::find()->where(['and',
                    ['in','car_id',$cars],
                    ['product_type' => PatternRegister::SD],
                    ['window_type' => $this->window],
                ])->exists();
            case 5 :
                return PatternRegister::find()->where(['and',
                    ['in','car_id',$cars],
                    ['product_type' => PatternRegister::BK],
                    ['window_type' => $this->window],
                ])->exists();
            default:
                return false;

        }
    }

    /**
     * [ Функция возвращает тип изготовления артикула ]
     *
     * @return string
     */
    public function getType()
    {
        if ($this->windowEn == 'RV') {
            if($this->car && isset($this->car->json('_rv')[strtolower($this->brand)]['standart']['forma']) && $this->car->json('_rv')[strtolower($this->brand)]['standart']['forma'] =='Треугольная')
                return 'Треугольная';
        }

        if($this->windowEn == 'BW') {
            if($this->car && isset($this->car->json('_bw')['chastei']) &&  $this->car->json('_bw')['chastei'] == '2')
                return 'Стандарт 2 части';
        }

        if ($this->_value3 == 2)
            return 'ВырезДляКурящих';

        if ($this->_value3 == 3)
            return 'ВырезДляЗеркала';

        if ($this->_value3 == 6)
            return 'Укороченный';

        return 'Стандарт';
    }

    /**
     * [ Тип клипс, используемый в артикуле ]
     *
     * @return string
     */
    public function getType_clips()
    {
        if ($this->_value3 == 45 || $this->_value3 == 48 ||
            $this->_value3 == 67 || $this->_value3 == 70 ||
            $this->_value3 == 71 || $this->_value3 == 68 || $this->_value3 == 69) {
            return 'Магнитные';
        }else{
            return 'Простые';
        }
    }


    ### ###
    ### [ Функции для извлечении информаци о клипсах, через артикул ]
    ###

    /**
     * [ Возвращает массив клипс, индексируемое наименованием и их количество. ]
     *
     * @todo Запись в память
     * @return array
     */
    public function getClips()
    {
        $newArray = [];

        if ($clipsSchema = $this->findScheme($this->type_clips)) {
            $newArray = $this->clipsCount($clipsSchema);
        }
        ksort($newArray);
        return $newArray;
    }

    /**
     * [ Возвращает массив клипс, индексируемое наименованием и их количество. (НЕ МАГНИТНЫЕ ЗАЖИМЫ ДЕРЖАТЕЛИ : Клипсы, На скотче) ]
     *
     * @todo Запись в память
     * @return array
     */
    public function getClipsOnly()
    {
        $typeClips = $this->isTape() ? 'Скотч' : 'Простые';
        $newArray = [];

        if ($clipsSchema = $this->findScheme($typeClips)) {
            $newArray = $this->clipsCount($clipsSchema);
        }
        ksort($newArray);
        return $newArray;
    }

    /**
     * [ Возвращает массив магнитов, индексируемое наименованием и их количество. (МАГНИТНЫЕ ЗАЖИМЫ ДЕРЖАТЕЛИ) ]
     *
     * @todo Запись в память
     * @return array
     */
    public function getMagnets()
    {
        $typeClips = 'Магнитные';
        $newArray = [];

        if ($clipsSchema = $this->findScheme($typeClips)) {
            $newArray = $this->clipsCount($clipsSchema);
        }
        ksort($newArray);
        return $newArray;
    }

    /**
     * [ Возвращает массив клипс с типами клипс ]
     *
     * @todo Запись в память
     * @return array
     */
    public function getClipsWithTypes()
    {
        $typeClips = $this->isTape() ? 'Скотч' : 'Простые';
        $newArray = [];

        if ($clipsSchema = $this->findScheme($typeClips)) {
            $newArray = $this->clipsTypes($clipsSchema);
        }
        ksort($newArray);
        return $newArray;
    }

    /**
     * [ Возвращает массив клипс с типами клипс ]
     *
     * @todo Запись в память
     * @return array
     */
    public function getClipsWithTypesAndCount()
    {
        $typeClips = $this->isTape() ? 'Скотч' : 'Простые';
        $newArray = [];

        if ($clipsSchema = $this->findScheme($typeClips)) {
            $newArray = $this->clipsCountByNameAndType($clipsSchema);
        }
        ksort($newArray);
        return $newArray;
    }


    /**
     * [ Функция возвращает массив клипс вида ['side' => ['name' => 'type']] ]
     *
     * @return array
     */
    public function getClipsWithSidesAndTypes()
    {
        $newClips = [];
        $newArray = [];

        if ($clipsSchema = $this->findScheme('Простые')) {
            $clips = Json::decode($clipsSchema->json);
            foreach ($clips['scheme'] as $clipIndex => $clipId) {
                if ($clipId) $newClips[$clipIndex] = $clipId;
            }

            foreach ($newClips as $clipIndex => $clipId) {
                /**
                 * @var $clipsOne CarClips
                 */
                $clipsOne = CarClips::findOne($clipId);
                if ($clipsOne) {
                    $side = CarClips::getSide($this->windowEn, $clipIndex, $this->isRvTriangular);
                    if ($side) {
                        $newArray[$side][$clipsOne->name] = $clipsOne->type;
                    }
                }
            }
        }
        ksort($newArray);

        return $newArray;
    }

    /**
     * [ Функция необходима для того, чтобы вывести клипсы для инструкций в нужном формате ]
     *
     * @return array
     */
    public function getClipsTypes()
    {
        $newClips = [];
        $newArray = [];

        if ($clipsSchema = $this->findScheme($this->type_clips)) {
            $clips = Json::decode($clipsSchema->json);
            foreach ($clips['scheme'] as $item => $value) {
                if ($value) $newClips[] = $value;
            }

            if (isset($newClips) && count($newClips)) {
                foreach ($newClips as $key => $item) {
                    /**
                     * @var $clipsOne CarClips
                     */
                    $clipsOne = CarClips::findOne($item);
                    if ($clipsOne) {
                        $newArray[$clipsOne->type] = $clipsOne->type;
                    }
                }
            }
        }

        ksort($newArray);
        return $newArray;
    }

    /**
     * [ Функция ищет схему расстановки креплений, в зависимости от типа креплений артикула ]
     *
     * @param $typeClips
     * @return array|null|\yii\db\ActiveRecord
     */
    private function findScheme($typeClips)
    {
        if (!$this->car) return null;
        return $clipsSchema = ClipsScheme::find()->where([
            'window' => $this->window,
            'car_id' => $this->car->id,
            'brand'  => $this->brand,
            'type'   => $this->type,
            'type_clips'=> $typeClips,
        ])->one();
    }

    /**
     * [Преобразует прееданную схему креплений в массив вида - наименование : тип]
     *
     * @todo Запись в память
     * @param $scheme
     * @return array
     */
    private function clipsTypes($scheme)
    {
        $newArray = [];
        $newClips = [];

        if (!$scheme) return $newArray;

        $clips = Json::decode($scheme->json);
        foreach ($clips['scheme'] as $item => $value) {
            if ($value) $newClips[] = $value;
        }

        if (isset($newClips) && count($newClips)) {
            foreach ($newClips as $key => $item) {
                /**
                 * @var $clipsOne CarClips
                 */
                $clipsOne = CarClips::findOne($item);
                if ($clipsOne) {
                    $newArray[$clipsOne->name] = $clipsOne->type;
                }
            }
        }

        return $newArray;
    }

    /**
     * [Преобразует прееданную схему креплений в массив вида - наименование : количество]
     *
     * @todo Запись в память
     * @param $scheme
     * @return array
     */
    private function clipsCount($scheme)
    {
        $newArray = [];
        $newClips = [];

        if (!$scheme) return $newArray;

        $clips = Json::decode($scheme->json);
        foreach ($clips['scheme'] as $item => $value) {
            if ($value) $newClips[] = $value;
        }

        if (isset($newClips) && count($newClips)) {
            foreach ($newClips as $key => $item) {
                if ($clipItem = CarClips::findOne($item)) {
                    if (isset($newArray[$clipItem->name])) {
                        $newArray[$clipItem->name]++;
                    } else {
                        $newArray[$clipItem->name] = 1;
                    }
                }
            }

            foreach ($newArray as $key => $value) {
                $newArray[$key] = $value * $this->countWindow;
            }
        }

        return $newArray;
    }

    /**
     * [Преобразует прееданную схему креплений в массив вида - наименование : количество]
     *
     * @todo Запись в память
     * @param $scheme
     * @return array
     */
    private function clipsCountByNameAndType($scheme)
    {
        $newArray = [];
        $newClips = [];

        if (!$scheme) return $newArray;

        $clips = Json::decode($scheme->json);
        foreach ($clips['scheme'] as $item => $value) {
            if ($value) $newClips[] = $value;
        }

        if (isset($newClips) && count($newClips)) {
            foreach ($newClips as $key => $item) {
                if ($clipItem = CarClips::findOne($item)) {
                    if (isset($newArray[$clipItem->name.$clipItem->type])) {
                        $newArray[$clipItem->name.$clipItem->type]++;
                    } else {
                        $newArray[$clipItem->name.$clipItem->type] = 1;
                    }
                }
            }

            foreach ($newArray as $key => $value) {
                $newArray[$key] = $value * $this->countWindow;
            }
        }

        return $newArray;
    }


    ###
    ### [ Функции для извлечении информаци о клипсах, через артикул ]
    ######

    //////////////////////////////////////////////////////////////////////

    ######
    ### [ Функции для генерации информации для документов, через артикул ]
    ###

    /**
     * [ Возвращаем наименование артикула в Инвойсе (EN) ]
     *
     * @return string
     */
    public function getInvoiceTitle()
    {
        $car = $this->car;

        $type = [
            '1'=>'STANDART',
            '2'=>'SMOKER',
            '3'=>'MIRROR',
            '4'=>'ADD. MOVEABLE',
            '5'=>'FRAMELESS',
            '55' => 'STANDART',
            '6'=>'SHORTED',
            '7'=>'FOLDING',
            '67'=>'WITH MAGNETIC HOLDERS',
            '70'=>'WITH MAGNETIC HOLDERS',
            '71'=>'WITH MAGNETIC HOLDERS',
            '72'=>'SHORTED, WITH MAGNETIC HOLDERS',
        ];

        $type2= [
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'1.5',
            '5'=>'5',
            '6'=>'6',
        ];


        if ($car) {
            $name='Protective screens for car windows for ';
            $name.= mb_strtoupper($car->fullEnName);
            $name.=' (№' . @$type2[$this->_value4];
            $name.=', '  . $this->windowEn;
            $name.=', '  . @$type[$this->_value3].')';
        }else {
            switch ($this->_id) {
                //Лайтбеги
                case 'OT-1189-47-3':
                    $name = 'LaitBag - trunk organiser (Black)';
                    break;
                case 'OT-1189-47-7':
                    $name = 'LaitBag - Kofferraum Organizer (Khaki)';
                    break;
                //Запасные комлпекты
                case 'OT-564-0-0':
                    $name = 'Set of clip holders for protective screens for car windows';
                    break;
                case 'OT-1394-0-0':
                    $name = 'Set of magnetic holders';
                    break;
                //Ароматизаторы
                case 'OT-1220-0-0':
                    $name = 'Air freshener Laitovo (BUBBLE GUM) (carton)';
                    break;
                case 'OT-1221-0-0':
                    $name = 'Air freshener Laitovo (NEW CAR) (carton)';
                    break;
                case 'OT-1222-0-0':
                    $name = 'Air Freshener Laitovo (CITRUS) (carton)';
                    break;
                case 'OT-1224-0-0':
                    $name = 'Air Freshener Laitovo (ANTI-TABAK) (carton)';
                    break;
                case 'OT-1223-0-0':
                    $name = 'Air Freshener Laitovo (VANILLA) (carton)';
                    break;
                default:
                    $name = '';
                    break;
            }
        }

        return $name;
    }

    /**
     * [ Возвращаем наименование артикула в Инвойсе (RUS) ]
     *
     * @return string
     */
    public function getInvoiceRusTitle()
    {
        $car = $this->car;

        $type = [
            '1'=>'СТАНДАРТ',
            '2'=>'ВЫРЕЗ ДЛЯ КУРЕНИЯ',
            '3'=>'ВЫРЕЗ ДЛЯ ЗЕРКАЛА',
            '4'=>'СДВИЖНОЙ',
            '5'=>'БЕСКАРКАСНЫЙ',
            '55' => 'СТАНДАРТ',
            '6'=>'УКОРОЧЕННЫЙ',
            '7'=>'СКЛАДНОЙ',
            '67'=>'НА МАГНИТАХ',
            '70'=>'НА МАГНИТАХ',
            '71'=>'НА МАГНИТАХ',
            '72'=>'УКОРОЧЕННЫЙ, НА МАГНИТАХ',
        ];

        $type2= [
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'1.5',
            '5'=>'5',
            '6'=>'6',
        ];

        if ($car) {
            $name='Защитные экраны для автомобильных окон ';
            $name.= mb_strtoupper($car->fullName);
            $name.=' (№' . @$type2[$this->_value4];
            $name.=', '  . $this->windowEn;
            $name.=', '  . @$type[$this->_value3].')';
        }else {
            switch ($this->_id) {
                //Лайтбеги
                case 'OT-1189-47-3':
                    $name = 'Органайзер для багажника (основной) (ЧЕРНЫЙ)';
                    break;
                case 'OT-1189-47-7':
                    $name = 'Органайзер для багажника (основной) (ХАКИ)';
                    break;
                //Запасные комлпекты
                case 'OT-564-0-0':
                    $name = 'Комплекты клипс к защитным экранам для автомобильных окон';
                    break;
                case 'OT-1394-0-0':
                    $name = 'Комплект магнитных держателей';
                    break;
                //Ароматизаторы
                case 'OT-1220-0-0':
                    $name = 'Ароматизатор автомобильный (БУБЛЬ ГУМ) (картон)';
                    break;
                case 'OT-1221-0-0':
                    $name = 'Ароматизатор автомобильный (НОВАЯ МАШИНА) (картон)';
                    break;
                case 'OT-1222-0-0':
                    $name = 'Ароматизатор автомобильный (ЦИТРУС) (картон)';
                    break;
                case 'OT-1224-0-0':
                    $name = 'Ароматизатор автомобильный (АНТИ-ТАБАК) (картон)';
                    break;
                case 'OT-1223-0-0':
                    $name = 'Ароматизатор автомобильный (ВАНИЛЬНЫЙ) (картон)';
                    break;
                case 'OT-1056-0-0':
                    $name = 'Наклейка Laitovo Family';
                    break;
                case 'OT-1000-1-0':
                    $name = 'Образец рекламный №1 - Шелфтокер с карманом';
                    break;
                case 'OT-1000-2-0':
                    $name = 'Рекламный образец №2 - шелфтокер с образцом';
                    break;
                case 'OT-1000-6-0':
                    $name = 'Рекламный образец №6 - ярлык';
                    break;
                case 'OT-678-0-0':
                    $name = 'Сумка для хранения защитных экранов';
                    break;
                case 'OT-1190-47-3':
                    $name = 'Органайзер для багажника LaitBag (Дополнительный) Чёрный (20х30х25)';
                    break;
                default:
                    $name = CheckStoragePosition::getStorageTitle($this->_id);
                    break;
            }
        }

        return $name;
    }

    ###
    ### [ Функции для генерации информации для документов, через артикул ]
    ######

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ######
    ### [ Функции для генерации новых индивидуальны инструкций ]
    ###

    /**
     * [ Получить наименование исполнения артикула RU ]
     *
     * @return mixed|string
     */
    public function getTypeTitleRu()
    {
        $type = [
            '1'=>'СТАНДАРТ',
            '2'=>'ВЫРЕЗ ДЛЯ КУРЕНИЯ',
            '3'=>'ВЫРЕЗ ДЛЯ ЗЕРКАЛА',
            '4'=>'СДВИЖНОЙ',
            '5'=>'БЕСКАРКАСНЫЙ',
            '55' => 'СТАНДАРТ',
            '6'=>'УКОРОЧЕННЫЙ',
            '7'=>'СКЛАДНОЙ',
            '67'=>'НА МАГНИТАХ',
            '70'=>'НА МАГНИТАХ',
            '71'=>'НА МАГНИТАХ',
            '72'=>'УКОРОЧЕННЫЙ, НА МАГНИТАХ',
            '45'=>'MAGNET, СТАНДАРТ',
            '48'=>'MAGNET, УКОРОЧЕННЫЙ',
        ];

        $car = $this->car;

        if ($car) {
            return @$type[$this->_value3];
        }
        return '';

    }

    /**
     * [ Получить наименование исполнения артикула EN ]
     *
     * @return mixed|string
     */
    public function getTypeTitleEn()
    {
        $type = [
            '1'=>'STANDARD',
            '2'=>'SMOKER',
            '3'=>'MIRROR',
            '4'=>'ADD. MOVEABLE',
            '5'=>'FRAMELESS',
            '55' => 'STANDARD',
            '6'=>'SHORTED',
            '7'=>'FOLDING',
            '67'=>'WITH MAGNETIC HOLDERS',
            '70'=>'WITH MAGNETIC HOLDERS',
            '71'=>'WITH MAGNETIC HOLDERS',
            '72'=>'SHORTED, WITH MAGNETIC HOLDERS',
            '45'=>'MAGNET, STANDARD',
            '48'=>'MAGNET, SHORTED',
        ];

        $car = $this->car;
        if ($car) {
            return @$type[$this->_value3];
        }
        return '';
    }

    /**
     * [ Получить наименование оконного проема артикула RU ]
     *
     * @return mixed|string
     */
    public function getWindowTitleRu()
    {
        $type = [
            'FW'=> 'ЛОБОВОЕ СТЕКЛО',
            'FV'=> 'ПЕРЕДНИЕ ФОРТОЧКИ',
            'FD'=> 'ПЕРЕДНИЕ БОКОВЫЕ',
            'RD'=> 'ЗАДНИЕ БОКОВЫЕ',
            'RV'=> 'ЗАДНИЕ ФОРТОЧКИ',
            'BW'=> 'ЗАДНЯЯ ШТОРКА',
        ];

        $car = $this->car;
        if ($car) {
            return @$type[$this->_value0];
        }
        return '';
    }

    /**
     * [ Получить наименование оконного проема артикула EN ]
     *
     * @return mixed|string
     */
    public function getWindowTitleEn()
    {
        $type = [
            'FW'=> 'FRONT WINDOW',
            'FV'=> 'FRONT VENTS',
            'FD'=> 'FRONT SIDE WINDOWS',
            'RD'=> 'REAR SIDE WINDOWS',
            'RV'=> 'REAR VENTS',
            'BW'=> 'BACK WINDOW',
        ];

        $car = $this->car;
        if ($car) {
            return @$type[$this->_value0];
        }
        return '';
    }

    /**
     * [ Функция возвращает штрихкод артикула ]
     *
     * @return string
     */
    public function getArticleBarcode()
    {
        switch (count($this->_valueList)) {
            case 5:
                return $this->_value0
                    . str_pad($this->_value2, 4, "0", STR_PAD_LEFT)
                    . str_pad($this->_value3, 2, "0", STR_PAD_LEFT)
                    . $this->_value4;

            case 4:
                return $this->_value0
                    . str_pad($this->_value1, 4, "0", STR_PAD_LEFT)
                    . str_pad($this->_value2, 2, "0", STR_PAD_LEFT)
                    . $this->_value3;

            default:
                return '';
        }
    }

    /**
     * Функция указывает - нужнен ли крбчек для установки данного артикула
     *
     * @return bool|mixed
     */
    public function needHook()
    {
        $car = $this->car;
        if (!$car) return false;

        $FD = ($car->json('kryuchokpb')  == '1');
        $RD = ($car->json('kryuchokzb')  == '1');
        $BW = ($car->json('kryuchokzps')  == '1');
        $RV = false;
        $FV = false;

        $res = [
            'FD' => $FD,
            'RD' => $RD,
            'BW' => $BW,
            'RV' => $RV,
            'FV' => $FV,
        ];

        return $res[$this->windowEn] ?? false;
    }


    ####################################################################
    ### Статистика по артикулу


    /**
     * [ Функция возвращает список наименований автомобилей, на которые подходит данный артикул ]
     *
     * @return array|null
     */
    public function getCarNameWithAnalogs()
    {
        if ($this->_carNameWithAnalogs) return $this->_carNameWithAnalogs;

        $analogNames = (new Query())
            ->select('analogName')
            ->from('tmp_analogs')
            ->where('carArticle = :carArticle')
            ->andWhere('window = :window')
            ->andWhere('analogArticle is not NULL')
            ->addParams([':carArticle' => $this->carArticle])
            ->addParams([':window' => $this->window])
            ->column();

        $this->_carNameWithAnalogs = ArrayHelper::merge([$this->carName],$analogNames);
        return $this->_carNameWithAnalogs;
    }

    /**
     * [ Функция возвращает список артикулов автомобилей, на которые подходит данный артикул ]
     *
     * @return array|null
     */
    public function getCarArticlesWithAnalogs()
    {
        if ($this->_carArticlesWithAnalogs) return $this->_carArticlesWithAnalogs;

        $analogArticles = (new Query())
            ->select('analogArticle')
            ->from('tmp_analogs')
            ->where('carArticle = :carArticle')
            ->andWhere('window = :window')
            ->andWhere('analogArticle is not NULL')
            ->addParams([':carArticle' => $this->carArticle])
            ->addParams([':window' => $this->window])
            ->column();

        $this->_carArticlesWithAnalogs = ArrayHelper::merge([$this->carArticle],$analogArticles);
        return $this->_carArticlesWithAnalogs;
    }

    /**
     * @return array|ArticleAnalog[]
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function getArticleWithAnalogs()
    {
        if ($this->_articleWithAnalogs) return $this->_articleWithAnalogs;

        $exist = ArticleAnalog::find()->articleExists($this->_id);
        if (!$exist) {
            $type = $this->car ? ArticleAnalog::TYPE_CAR : ArticleAnalog::TYPE_COMMON;
                $generator = new ArticleAnalogGenerator();
                $generator->execute($this->_id,$this->carArticle,$this->window,$type);
        }
        $this->_articleWithAnalogs = ArticleAnalog::find()->analogsWithArticle($this->_id)->select('analogArticle')->column();

        return $this->_articleWithAnalogs;
    }

    /**
     * @return int
     */
    public function getSaleLast()
    {
        return $this->saleStatistics ? $this->saleStatistics->saleLast : 0;
    }

    /**
     * @return int
     */
    public function getSaleSixMonth()
    {
        return $this->saleStatistics ? $this->saleStatistics->saleSixMonth : 0;
    }

    /**
     * @return int
     */
    public function getFixRate()
    {
        return $this->saleStatistics ? $this->saleStatistics->fixRate : 0;
    }

    /**
     * @return int
     */
    public function getMinFrequency()
    {
        return $this->saleStatistics ? $this->saleStatistics->minFrequency : 0;
    }

    /**
     * @return int
     */
    public function getSalePrevious()
    {
        return $this->saleStatistics ? $this->saleStatistics->salePrevious : 0;
    }

    /**
     * @return int
     */
    public function getSaleRate()
    {
        return $this->saleStatistics ? $this->saleStatistics->saleRate : 0;
    }

    /**
     * @return bool
     */
    public function isNeeded()
    {
        if ($this->saleRate >= 1) {
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getSalePlan()
    {
        return $this->saleStatistics ? $this->saleStatistics->salePlan : 0;
    }

    /**
     * @return float
     */
    public function getIncrease()
    {
        return $this->saleStatistics ? $this->saleStatistics->increase : 0;
    }

    /**
     * @return float
     */
    public function getMakeRate()
    {
        return $this->saleStatistics ? $this->saleStatistics->makeRate : 0;
    }

    /**
     * @return int
     */
    public function getBalancePlan()
    {
        return $this->saleStatistics ? $this->saleStatistics->balancePlan : 0;
    }

    /**
     * @return int
     */
    public function getBalanceFact()
    {
        if ($this->_balanceFact !== null) return $this->_balanceFact;

        $idsSt = Storage::find()->where(['in','type',[Storage::TYPE_REVERSE,Storage::TYPE_TIME]])->select(['id'])->column();

        $query = StorageState::find()->alias('t')->where(['in','t.storage_id',$idsSt])->joinWith('upn');
        $query->select(['upn_id']);
        $query->andWhere(['in','logistics_naryad.article', $this->articleWithAnalogs]);
        $ids = $query->column();

        $query = Naryad::find();
        $query->andWhere(['in','article', $this->articleWithAnalogs]);
        $query->andWhere(['reserved_id' => null]);
        $query->select(['id']);
        $rows = $query->column();

        $res = array_intersect($ids,$rows);
        return ($this->_balanceFact = (count($res)));
    }
    /**
     * @return int
     */
    public function getBalanceProdFree()
    {
        if ($this->_balanceProdFree !== null) return $this->_balanceProdFree;

        $query = new Query();
        $query->select('article')
            ->from('laitovo_erp_naryad')
            ->where('logist_id is not null and order_id is null')
            ->andWhere(['not in','status',[ErpNaryad::STATUS_CANCEL,ErpNaryad::STATUS_FINISH,ErpNaryad::STATUS_CHECKED,ErpNaryad::STATUS_FROM_SKLAD,ErpNaryad::STATUS_READY_TO_WORK]])
            ->andWhere(['distributed' => 0])
            ->andWhere(['IN', 'article', $this->articleWithAnalogs]);

        $prod = $query->count() ?: 0;

        return ($this->_balanceProdFree = $prod);
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function findFreeUpn()
    {
        $idsSt = Storage::find()->select(['id'])->where(['in','type',[Storage::TYPE_REVERSE,Storage::TYPE_TIME]])->column();

        $query = StorageState::find()->alias('t')->where(['in','t.storage_id',$idsSt])->joinWith('upn');
        $query->select(['upn_id']);
        $ids = $query->column();

        $query = Naryad::find();
        $query->where(['in','id',$ids]);
        $query->andWhere(['in','article', $this->articleWithAnalogs]);
        $query->andWhere(['reserved_id'=> null]);
        $query->orderBy(['created_at' => SORT_ASC]);
        $upn = $query->one();

        return $upn;
    }

    /**
     * @return array|SaleStatistics|null
     */
    public function getSaleStatistics()
    {
        if ($this->_saleStatistics) return $this->_saleStatistics;

        $sale = SaleStatistics::find()->oneByArticle($this->_id);
        if (!$sale) {
            $generator = new ArticleStatisticsGenerator();
            $generator->execute($this->_id);
        }
        return ($this->_saleStatistics = ($sale ?: SaleStatistics::find()->oneByArticle($this->_id)));
    }

    /**
     * [ Функция для определения продукта в системе с которым ассоциируется артикул ]
     *
     * @return ErpProductType|null|\yii\db\ActiveRecord
     */
    public function getProduct()
    {
        if ($this->_product) return $this->_product;

        $products = ErpProductType::find()->all();
        foreach ($products as $product) {
            /**
             * @var $product ErpProductType
             */
            if ($product->rule && preg_match($product->rule, $this->_id)) {
                $this->_product = $product;
                break;
            }
        }
        return $this->_product;
    }

    /**
     * [ Функция возвращает номер группы ]
     *
     * @return string
     */
    public function getGroupName()
    {
        return $this->product ? $this->product->title : '';
    }

    /**
     * [ Функция возвращает наименование артикула]
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->product ? $this->product->title : '';
    }

    /**
     * [ Функция возвращает наименование автомобиля, для которого был создан артикул ]
     *
     * @return string
     */
    public function getCarName()
    {
        return $this->car ? $this->car->name : '';
    }

    /**
     * [ Функция возвращает наименование автомобиля, для которого был создан артикул ]
     *
     * @return string
     */
    public function getCarEnName()
    {
        return $this->car ? $this->car->fullEnName : '';
    }

    /**
     * Возвращает маску для поиска по SQL базе по автомобилю артикулов
     */
    public function getCarSqlMask()
    {
        return '%-%-' . $this->carArticle . '-%-%';
    }

    /**
     * Возвращает маску для поиска по SQL базе по автомобилю артикулов
     */
    public function getProductSqlMask()
    {
        return $this->_value0 . '-%-%-' . $this->_value3 . '-' . $this->_value4;
    }

    /**
     * Возвращает маску для поиска по SQL базе по автомобилю артикулов
     */
    public function getTypeSqlMask()
    {
        return $this->_value0 . '-%-%-' . $this->_value3 . '-%';
    }

    public function getArticleType()
    {
        return $this->carArticle ? $this->_value3 : null;
    }

    public function getArticleCloth()
    {
        return $this->carArticle ? $this->_value4 : null;
    }

    public function getNeedHook()
    {
        $car = $this->car;
        if (!$car) return false;

        $FD = ($car->json('kryuchokpb')  == '1');
        $RD = ($car->json('kryuchokzb')  == '1');
        $BW = true;
        $RV = false;
        $FV = false;

        $res = [
            'FD' => $FD,
            'RD' => $RD,
            'BW' => $BW,
            'RV' => $RV,
            'FV' => $FV,
        ];

        $window =   $this->windowEn;
        return $res[mb_strtoupper($window)]??false;
    }

}