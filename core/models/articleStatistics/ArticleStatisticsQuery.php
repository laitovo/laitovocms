<?php

namespace core\models\articleStatistics;

/**
 * This is the ActiveQuery class for [[ArticleAnalog]].
 *
 * @see ArticleAnalog
 */
class ArticleStatisticsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @param $article
     * @param null $db
     * @return bool
     */
    public function actualExists($article,$db = null)
    {
        return $this->andWhere(['and',['article' => $article],['>','date',(time() - 60*60*3)]])->exists($db);
    }

    /**
     * @param $article
     * @param null $db
     * @return array|ArticleStatistics|null
     */
    public function oneByArticle($article,$db = null)
    {
        return $this->andWhere(['article' => $article])->one($db);
    }

    /**
     * @inheritdoc
     * @return ArticleStatistics[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ArticleStatistics|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
