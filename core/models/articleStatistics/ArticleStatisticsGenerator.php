<?php
namespace core\models\articleStatistics;

use backend\modules\laitovo\models\ErpNaryad;
use backend\modules\laitovo\models\ErpProductType;
use backend\modules\logistics\models\AutomaticOrder;
use backend\modules\logistics\models\StorageState;
use core\models\article\Article;
use yii\helpers\ArrayHelper;
use common\models\logistics\ArticleStatistics as ArticleStatistics;
use core\models\articleStatistics\ArticleStatistics as SaleStatistics;

/**
 * Класс который отвечает за генерацию статистики
 *
 * Class ArticleStatisticsGenerator
 * @package core\models\articleStatistics
 */
class ArticleStatisticsGenerator
{
    /**
     * [ Основная, которая запускает генерацию статистики. ]
     *
     * @param string|null $article
     */
    public function execute($article = null)
    {
        $articles = $article ? [$article] : $this->extractArticles();
        $conf   = AutomaticOrder::receive(1);
        //инициализируем праматеры из настроек
        $fixRate      = $conf->fixRate   ? $conf->fixRate  : 2;
        $minFrequency = $conf->frequency ? $conf->frequency: 3;

        $summary = [];

        $articlesCount = count($articles);
        $rowArticleCount = 0;
        foreach ($articles as $article) {
            $rowArticleCount++;
            echo "Всего артикулов: {$articlesCount}"  . PHP_EOL;
            echo "Текущая позиция: {$rowArticleCount}"  . PHP_EOL;
            echo "Осталось для обработки: " . ($articlesCount - $rowArticleCount)  . PHP_EOL;
            echo "Артикул: {$article}" . PHP_EOL;
            if (!SaleStatistics::find()->actualExists($article)) {
                $object = new Article($article);
                $data = $this->extractData($object,$fixRate,$minFrequency);
                foreach ($object->articleWithAnalogs as $articleRow) {
                    $row = SaleStatistics::find()->oneByArticle($articleRow);
                    if ($row) {
                        $row->attributes = ArrayHelper::merge(['fixRate' => $fixRate, 'minFrequency' => $minFrequency],$data);
                    }else{
                        $row =  new SaleStatistics(ArrayHelper::merge(['article' => $articleRow,'fixRate' => $fixRate, 'minFrequency' => $minFrequency],$data));
                    }
                    if (!$row->save()) {
                        $content = '';
                        foreach ($row->errors as $one) {
                            $content .= implode(',',$one);
                        }
                        echo "Errors : " .  $content . PHP_EOL;
                        $summary[] = 'Артикул : ' . $articleRow . " Errors : " .  $content;
                    }
                }
                echo "__________________________________" . PHP_EOL;
            }else{
                echo "Запись актуальна" . PHP_EOL;
            }

        }
        echo "SUM ERRORS : " . PHP_EOL . implode(',' . PHP_EOL, $summary);
    }

    /**
     * [ Функция возвращает весь список артикулов, по которым должна быть записана или обновлена статистика ]
     *
     * @return array
     */
    private function extractArticles()
    {
        $query = ArticleStatistics::find();
        $query->select(['article'])->distinct();
        $array1 =$query->column();

        $query = StorageState::find()->alias('t')->joinWith('upn');
        $query->select(['article'])->distinct();
        $array2 = $query->column();

        $array = ArrayHelper::merge($array1,$array2);
        $array = array_unique($array);
        return $array;
    }

    /**
     * @param $articleObj Article
     * @param $fixRate
     * @param $minFrequency
     * @return array
     */
    private function extractData($articleObj,$fixRate,$minFrequency)
    {
        $productId    = null;
        $title        = null;
        $groupName    = null;
        $increase     = 0;
        $makeRate     = 0;
        $salePlan     = 0;
        $balancePlan  = 0;

        echo "Артикул + аналоги: ".  (implode(', ',$articleObj->articleWithAnalogs)) .  PHP_EOL;

        $month1 = $this->extractDataByMonth("-1 month", $articleObj->articleWithAnalogs);
        $month2 = $this->extractDataByMonth("-2 month", $articleObj->articleWithAnalogs);
        $month3 = $this->extractDataByMonth("-3 month", $articleObj->articleWithAnalogs);
        $month4 = $this->extractDataByMonth("-4 month", $articleObj->articleWithAnalogs);
        $month5 = $this->extractDataByMonth("-5 month", $articleObj->articleWithAnalogs);
        $month6 = $this->extractDataByMonth("-6 month", $articleObj->articleWithAnalogs);

        $statistics = ArrayHelper::merge($month1, $month2, $month3, $month4, $month5, $month6);

        $sumMonth1 = 0;
        $sumMonth2 = 0;
        $sumMonth3 = 0;
        $sumMonth4 = 0;
        $sumMonth5 = 0;
        $sumMonth6 = 0;

        foreach ($statistics as $key => $value) {
            $month1 = isset($value['-1 month']) ? $value['-1 month'] : 0;
            $month2 = isset($value['-2 month']) ? $value['-2 month'] : 0;
            $month3 = isset($value['-3 month']) ? $value['-3 month'] : 0;
            $month4 = isset($value['-4 month']) ? $value['-4 month'] : 0;
            $month5 = isset($value['-5 month']) ? $value['-5 month'] : 0;
            $month6 = isset($value['-6 month']) ? $value['-6 month'] : 0;

            $sumMonth1 += $month1;
            $sumMonth2 += $month2;
            $sumMonth3 += $month3;
            $sumMonth4 += $month4;
            $sumMonth5 += $month5;
            $sumMonth6 += $month6;
        }

        $saleLast = $sumMonth1;
        $salePrevious = $sumMonth2;
        $saleSixMonth = $sumMonth1 + $sumMonth2 + $sumMonth3 + $sumMonth4 + $sumMonth5 + $sumMonth6;

        $saleRate = 0;
        if ($sumMonth1 > 0) $saleRate++;
        if ($sumMonth2 > 0) $saleRate++;
        if ($sumMonth3 > 0) $saleRate++;
        if ($sumMonth4 > 0) $saleRate++;
        if ($sumMonth5 > 0) $saleRate++;
        if ($sumMonth6 > 0) $saleRate++;

        /**
         * @var $product ErpProductType
         */
        if ($product = $articleObj->product) {
            $productId   = $product->id;
            $title       = $articleObj->title;
            $groupName   = $articleObj->groupName;
            $increase    = round($saleLast > $salePrevious ? $this->calcIncrease($product, 2) : $this->calcIncrease($product, 1), 2);
            $makeRate    = $this->calcMakeRate($product);
            $salePlan    = round($saleLast > $salePrevious ? $this->calcSalePlan($salePrevious, $increase) : $this->calcSalePlan($saleLast, $increase), 0);
            $balancePlan = $this->calcPlanBalance($fixRate, $salePlan, $makeRate, $saleRate, $minFrequency);
        }

        echo 'saleLast'      . ' : ' .  $saleLast     .   PHP_EOL;
        echo 'salePrevious'  . ' : ' .  $salePrevious .   PHP_EOL;
        echo 'saleRate'      . ' : ' .  $saleRate     .   PHP_EOL;
        echo 'productId'     . ' : ' .  $productId    .   PHP_EOL;
        echo 'title'         . ' : ' .  $title        .   PHP_EOL;
        echo 'groupName'     . ' : ' .  $groupName    .   PHP_EOL;
        echo 'increase'      . ' : ' .  $increase     .   PHP_EOL;
        echo 'makeRate'      . ' : ' .  $makeRate     .   PHP_EOL;
        echo 'salePlan'      . ' : ' .  $salePlan     .   PHP_EOL;
        echo 'balancePlan'   . ' : ' .  $balancePlan  .   PHP_EOL;


        return [
            'saleLast'     =>  $saleLast,
            'salePrevious' =>  $salePrevious,
            'saleSixMonth' =>  $saleSixMonth,
            'saleRate'     =>  $saleRate,
            'productId'    =>  $productId,
            'title'        =>  $title,
            'groupName'    =>  $groupName,
            'increase'     =>  $increase,
            'makeRate'     =>  $makeRate ,
            'salePlan'     =>  $salePlan,
            'balancePlan'  =>  $balancePlan,
            'date'         =>  time(),
        ];

    }

    /**
     * @param $monthValue
     * @param array $articles
     * @return array
     */
    private function extractDataByMonth($monthValue,$articles=[])
    {
        $result = [];
        $month  = strtotime($monthValue);
        $date_f  = mktime (  0, 0, 0, date("n",$month),                 1, date("Y",$month));
        $date_to = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
            ['in','article', $articles],
        ]);
        $query->groupBy(['article']);
        $query->select(['article','count' => 'COUNT(id)']);
        $rows = $query->asArray()->all();
        foreach ($rows as $key => $value) {
            $result[$value['article']][$monthValue] = $value['count'];
        }
        return $result;
    }

    /**
     * @param ErpProductType $product
     * @param $type
     * @return float|int
     */
    private function calcIncrease(ErpProductType $product, $type)
    {

        //Получим правило для поиска в базе
        $rule = $product->rule;
        $rule = trim($rule,'#');
        $rule = str_replace('\w', '[[:alnum:]]', $rule);
        $rule = str_replace('\d', '[[:digit:]]', $rule);

        //Определим количество реализаций за -1 year - 2 month
        $month  = strtotime('-14 month');
        $date_f  = mktime (  0, 0, 0, date("n",$month),                 1, date("Y",$month));
        $date_to = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
            ['REGEXP','article', $rule],
        ]);
        $month14 = $query->count();

        //Определим количество реализаций за -1 year - 1 month
        $month  = strtotime('-13 month');
        $date_f  = mktime (  0, 0, 0, date("n",$month),                 1, date("Y",$month));
        $date_to = mktime ( 23,59,59, date("n",$month),date('t', $month), date("Y",$month));
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
            ['REGEXP','article', $rule],
        ]);
        $month13 = $query->count();

        //Определим количество реализаций за -1 year + 30 month
        $month  = strtotime('-12 month');
        $date_f  = $month;
        $date_to = $month + 60*60*24*30;
        $query = ArticleStatistics::find();
        $query->where(['and',
            ['>=','created_at', $date_f],
            ['<=','created_at', $date_to],
            ['REGEXP','article', $rule],
        ]);
        $days30 = $query->count();

        //в зависимости от типа непикового прироста отдаем разные отношения
        switch ($type) {
            case 1:
                return $month13 ? ($days30 / $month13)-1 : 0;

            case 2:
                return $month14 ? ($days30 / $month14)-1 : 0;
        }
    }

    /**
     * @param ErpProductType $product
     * @return float|int
     */
    private function calcMakeRate(ErpProductType $product)
    {
        //Получим правило для поиска в базе
        $rule = $product->rule;
        $rule = trim($rule,'#');
        $rule = str_replace('\w', '[[:alnum:]]', $rule);
        $rule = str_replace('\d', '[[:digit:]]', $rule);

        $month1  = strtotime('-1 month');
        $month2  = strtotime('-6 month');
        $date_f  = mktime (  0, 0, 0, date("n",$month1),                 1, date("Y",$month1));
        $date_to = mktime ( 23,59,59, date("n",$month2),date('t', $month2), date("Y",$month2));
        $query = ErpNaryad::find();
        $query->where(['and',
            ['status' => ErpNaryad::STATUS_READY],
            ['<=','created_at', $date_f],
            ['>=','created_at', $date_to],
            ['REGEXP','article', $rule],

        ]);
        $query->groupBy(['article']);
        $query->select(['article','rate' => 'AVG((ready_date - created_at))']);
        $rows = $query->asArray()->all();

        $rate = 0;
        $count = 0;
        foreach ($rows as $key => $value) {
            $rate += $value['rate'];
            $count++;
        }

        return $count ? $rate / $count / (60*60*24*30) : 0;
    }

    /**
     * @param $sale
     * @param $increase
     * @return float
     */
    private function calcSalePlan($sale,$increase)
    {
        return round($sale + ($sale * $increase));
    }

    /**
     * @param $fixRate
     * @param $salePlan
     * @param $makeRate
     * @param $saleRate
     * @param $minFrequency
     * @return float|int
     */
    private function calcPlanBalance($fixRate,$salePlan,$makeRate,$saleRate,$minFrequency)
    {
        $planB = $saleRate > $minFrequency ? 1 : 0;
        return ceil($salePlan * $makeRate * $fixRate) > 0 ?  ceil($salePlan * $makeRate * $fixRate) : $planB;
    }


}