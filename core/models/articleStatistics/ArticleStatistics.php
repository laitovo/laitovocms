<?php

namespace core\models\articleStatistics;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%laitovo_article_sale_statistics}}".
 *
 * @property string  $article
 * @property integer $saleLast
 * @property integer $salePrevious
 * @property integer $saleSixMonth
 * @property integer $saleRate
 * @property integer $salePlan
 * @property float   $increase
 * @property float   $makeRate
 * @property integer $fixRate
 * @property integer $minFrequency
 * @property integer $date
 */
class ArticleStatistics extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_article_sale_statistics}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article','date','saleLast','salePrevious','saleSixMonth','saleRate','salePlan','increase','makeRate','balancePlan','fixRate','minFrequency'], 'required'],
            [['date','saleLast','salePrevious','saleSixMonth','saleRate','salePlan','balancePlan','fixRate','minFrequency'], 'integer'],
            [['article'], 'unique'],
            [['increase','makeRate'], 'double'],
            [['article'], 'string', 'max' => 255],
            [['productId','title','groupName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article'      => Yii::t('app', 'Артикул'),
            'productId'    => Yii::t('app', 'Вид продукта'),
            'title'        => Yii::t('app', 'Наименование продукта'),
            'groupName'    => Yii::t('app', 'Наименование группы'),
            'saleLast'     => Yii::t('app', 'Продано в последний месяц'),
            'salePrevious' => Yii::t('app', 'Продано в предыдущий месяц'),
            'saleSixMonth' => Yii::t('app', 'Продано за 6 месяцев'),
            'saleRate'     => Yii::t('app', 'Частота продаж'),
            'salePlan'     => Yii::t('app', 'Планируемые продажи'),
            'increase'     => Yii::t('app', 'Последний непиковый прирост'),
            'makeRate'     => Yii::t('app', 'Скорость производства'),
            'balancePlan'  => Yii::t('app', 'Планируемый баланс'),
            'fixRate'      => Yii::t('app', 'Корректировочный коэффициент скорости производства'),
            'minFrequency' => Yii::t('app', 'Мин. частота реализации, мес'),
            'date'         => Yii::t('app', 'Дата'),
        ];
    }

    /**
     * @inheritdoc
     * @return ArticleStatisticsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleStatisticsQuery(get_called_class());
    }
}
