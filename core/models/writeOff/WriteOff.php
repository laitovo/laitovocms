<?php

namespace core\models\writeOff;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%core_write_off}}".
 *
 * @property integer $id
 * @property integer $clientId
 * @property string $locale
 * @property integer $documentId
 * @property integer $date
 * @property string $type
 * @property string $total
 * @property string  $currency
 */
class WriteOff extends ActiveRecord
{
    const MONEY_RETURN_TYPE = 'Возврат покупателю';
    const COMMISSION_TYPE = 'Комиссия';
    const OTHER_TYPE = 'Прочие расходы';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_write_off}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientId', 'locale', 'documentId', 'date', 'total'], 'required'],
            [['clientId', 'documentId', 'date'], 'integer'],
            [['locale'], 'string'],
            [['currency'], 'string'],
            [['total'], 'number'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clientId' => Yii::t('app', 'Уникальный идентификатор клиента в определнной локали'),
            'locale' => Yii::t('app', 'Locale'),
            'documentId' => Yii::t('app', 'Номер документа в соответсвуйющей локали'),
            'date' => Yii::t('app', 'Учетная дата поступления'),
            'type' => Yii::t('app', 'Направление расхода'),
            'total' => Yii::t('app', 'Сумма списания'),
            'currency' => Yii::t('app', 'Валюта'),
        ];
    }

    /**
     * @inheritdoc
     * @return WriteOffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WriteOffQuery(get_called_class());
    }
}
