<?php

namespace core\models\returnNote;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[ReturnNote]].
 *
 * @see ReturnNote
 */
class ReturnNoteQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ReturnNote[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ReturnNote|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
