<?php

namespace core\models\returnNote;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%core_return_note}}".
 *
 * @property integer $id
 * @property integer $clientId
 * @property string $locale
 * @property integer $documentId
 * @property integer $date
 * @property string $sumWithoutNds
 * @property string $amountNds
 * @property string $sumNds
 * @property string $sumWithNds
 */
class ReturnNote extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_return_note}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientId', 'locale', 'documentId', 'date', 'sumWithoutNds', 'amountNds', 'sumNds', 'sumWithNds'], 'required'],
            [['clientId', 'documentId', 'date'], 'integer'],
            [['locale'], 'string'],
            [['sumWithoutNds', 'amountNds', 'sumNds', 'sumWithNds'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clientId' => Yii::t('app', 'Уникальный идентификатор клиента в определнной локали'),
            'locale' => Yii::t('app', 'Locale'),
            'documentId' => Yii::t('app', 'Уникальный номер документа возврата в системе'),
            'date' => Yii::t('app', 'Учетная дата возврата'),
            'sumWithoutNds' => Yii::t('app', 'Сумма возврата без НДС'),
            'amountNds' => Yii::t('app', 'Значение налога НДС в %'),
            'sumNds' => Yii::t('app', 'Сумма налога НДС с возврате'),
            'sumWithNds' => Yii::t('app', 'Сумма реализации с учетом НДС'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReturnNoteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReturnNoteQuery(get_called_class());
    }
}
