<?php

namespace core\models\autoCompletionLogs;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%logistics_autocompletion_logs}}".
 *
 * @property integer $id
 * @property integer $orderNumber
 * @property string $article
 * @property integer $requested
 * @property integer $inStorage
 * @property integer $result
 * @property integer $createdAt
 * @property integer $status
 * @property integer $blocked
 * @property integer $indexGroup
 */
class AutoCompletionLogs extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%logistics_autocompletion_logs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderNumber', 'article', 'requested', 'inStorage', 'createdAt'], 'required'],
            [['orderNumber', 'requested', 'inStorage', 'result', 'createdAt', 'indexGroup'], 'integer'],
            [['status','blocked'], 'boolean'],
            [['article'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'orderNumber' => Yii::t('app', 'Номер заказа'),
            'article'     => Yii::t('app', 'Артикул'),
            'requested'   => Yii::t('app', 'Запрошено по заказу'),
            'inStorage'   => Yii::t('app', 'Находится на складе'),
            'result'      => Yii::t('app', 'Результат в автопополнение'),
            'createdAt'   => Yii::t('app', 'Дата создания записи'),
            'status'      => Yii::t('app', 'Статус'),
            'blocked'     => Yii::t('app', 'Блокирован'),
            'indexGroup'  => Yii::t('app', 'Индекс комплекта'),
        ];
    }

    /**
     * @inheritdoc
     * @return AutoCompletionLogsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutoCompletionLogsQuery(get_called_class());
    }
}
