<?php

namespace core\models\autoCompletionLogs;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[AutoCompletionLogs]].
 *
 * @see AutocompletionLogs
 */
class AutoCompletionLogsQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @return $this Получить записи, которые еще не подвергались обработке
     */
    public function notHandled()
    {
        return $this->andWhere(['status' => false]);
    }

    /**
     * @inheritdoc
     * @return AutoCompletionLogs[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AutoCompletionLogs|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
