<?php

namespace core\models\semiFinishedArticles;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tmp_semi_finished_articles}}".
 *
 * @property integer $id
 * @property integer $carArticle
 * @property string $mask
 * @property integer $literal
 * @property integer $upnId
 * @property integer $reservedAt
 */
class SemiFinishedArticles extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tmp_semi_finished_articles}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['carArticle', 'mask', 'literal'], 'required'],
            [['carArticle', 'upnId', 'reservedAt','literal'], 'integer'],
            [['mask'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'carArticle' => Yii::t('app', 'Артикул автомобиля, для которого изготовлен полуфабрикат'),
            'mask' => Yii::t('app', 'Маска артикула, для изготовления которого подойдет данная рамка'),
            'literal' => Yii::t('app', 'Литера'),
            'upnId' => Yii::t('app', 'Наряд, на который был использован данный полуфабрикат'),
            'reservedAt' => Yii::t('app', 'Дата, когда был заерезервированный данный полуфабрикат под наряд'),
        ];
    }

    /**
     * @inheritdoc
     * @return SemiFinishedArticlesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SemiFinishedArticlesQuery(get_called_class());
    }
}
