<?php

namespace core\models\semiFinishedArticles;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[SemiFinishedArticles]].
 *
 * @see SemiFinishedArticles
 */
class SemiFinishedArticlesQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SemiFinishedArticles[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SemiFinishedArticles|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
