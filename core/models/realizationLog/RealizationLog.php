<?php

namespace core\models\realizationLog;

use backend\modules\logistics\models\OrderEntry;
use Yii;

/**
 * This is the model class for table "{{%logistics_realization_log}}".
 *
 * @property integer $id
 * @property integer $upnId
 * @property integer $orderEntryId
 * @property string $literal
 * @property integer $createdAt
 * @property integer $updatedAt
 * @property integer $authorId
 * @property integer $updaterId
 */
class RealizationLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%logistics_realization_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upnId', 'orderEntryId', 'literal'], 'required'],
            [['upnId', 'orderEntryId', 'createdAt', 'updatedAt', 'authorId', 'updaterId'], 'integer'],
            [['literal'], 'string', 'max' => 255],
            [['orderEntryId'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'upnId' => Yii::t('app', 'Идентификатор UPN-а (логистического наряда)'),
            'orderEntryId' => Yii::t('app', 'Идентификатор позиции из логистического заказа'),
            'literal' => Yii::t('app', 'Литера - уникальное циферно-буквенное обозначение места хранения продукции на складе'),
            'createdAt' => Yii::t('app', 'Дата создания'),
            'updatedAt' => Yii::t('app', 'Дата последнего изменения'),
            'authorId' => Yii::t('app', 'Автор'),
            'updaterId' => Yii::t('app', 'Последний редактор'),
        ];
    }

    /**
     * Все не удаленные элементы, которые должны поехать в этой отгрузке
     * @return \yii\db\ActiveQuery
     */
    public function getOrderEntry()
    {
        return $this->hasOne(OrderEntry::className(), ['id' => 'orderEntryId']);
    }
    /**
     * @inheritdoc
     * @return RealizationLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RealizationLogQuery(get_called_class());
    }
}
