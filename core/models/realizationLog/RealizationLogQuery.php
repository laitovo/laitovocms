<?php

namespace core\models\realizationLog;

/**
 * This is the ActiveQuery class for [[RealizationLog]].
 *
 * @see RealizationLog
 */
class RealizationLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RealizationLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RealizationLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
