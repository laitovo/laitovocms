<?php

namespace core\models\shipment;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Shipment]].
 *
 * @see Shipment
 */
class ShipmentQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * Поиск списка отгрузок по переданым идетификатрам.
     *
     * @param array $ids
     * @return ShipmentQuery
     */
    public function byIds(array $ids)
    {
        return $this->andWhere(['in', 'id', $ids]);
    }

    /**
     * Поиск списка отгрузок по переданым идетификатрам.
     *
     * @param array $ids
     * @return ShipmentQuery
     */
    public function orIds(array $ids)
    {
        return $this->orWhere(['in', 'id', $ids]);
    }

    /**
     * Находит все отгрузки, которые должны быть отгружены до, или после указанный даты
     *
     * @param $time [ Дата, по котор
     * @param bool $reverse
     * @return ShipmentQuery
     */
    public function mustBeShipped($time,$reverse = false)
    {
        $arrow = $reverse ? '>' : '<';
        $data = mktime(23, 59, 59, date('m',$time), date('d',$time),date('y',$time));

        return $this->andWhere(['and',
            ['or',
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_HANDLED],
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_PACKED],
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_ON_STORAGE_REQUEST],

            ],
            [$arrow,'shipmentDate',$data]
        ]);
    }

    public function mustBeShippedNotReady($time,$reverse = false)
    {
        $arrow = $reverse ? '>' : '<';
        $data = mktime(23, 59, 59, date('m',$time), date('d',$time),date('y',$time));

        return $this->andWhere(['and',
            ['logisticsStatus' => Shipment::LOGISTICS_STATUS_PACKED],
            [$arrow,'shipmentDate',$data]
        ]);
    }

    /**
     * Возвращаем перенесенные отгрузки с определенной даты
     *
     * @param $time integer
     * @return mixed
     */
    public function carriedOver($time)
    {
        $data = mktime(0, 0, 0, date('m',$time), date('d',$time),date('y',$time));

        return $this->andWhere(['and',
            ['or',
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_HANDLED],
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_PACKED],
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_ON_STORAGE_REQUEST],
            ],
            ['<','shipmentDate',$data]
        ]);
    }

    /**
     * Функция возвращает упакованные для определенной даты отгрузки
     *
     * @param $time integer
     * @return mixed
     */
    public function packedForShip($time)
    {
        $data = mktime(23, 59, 59, date('m',$time), date('d',$time),date('y',$time));

        return $this->andWhere(['and',
            ['logisticsStatus' => Shipment::LOGISTICS_STATUS_PACKED],
            ['<','shipmentDate',$data]
        ]);
    }

    /**
     * Функция возвращает список актуальных (не отменённых) отгрузок, которые ещё не достигли статуса "Отгружена"
     *
     * @return ShipmentQuery
     */
    public function beforeShipping()
    {
        return $this->andWhere(['or',
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_HANDLED],
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_PACKED],
                ['logisticsStatus' => Shipment::LOGISTICS_STATUS_ON_STORAGE_REQUEST]
        ]);
    }

    /**
     * @inheritdoc
     * @return Shipment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Shipment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Сортировка отгрузок для складского монитора
     *
     * @return ShipmentQuery
     */
    public function storageSort()
    {
        return $this->orderBy("(CASE logisticsStatus WHEN 'on_storage_request' THEN 0 WHEN 'handled' THEN 1 WHEN 'packed' THEN 2 END) asc, shipmentDate");
    }
}
