<?php

namespace core\models\shipment;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use core\models\box\Box;
use core\models\transportCompany\TransportCompany;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%logistics_shipment}}".
 *
 * @property integer $id
 * @property @depricated string $transportCompanyName [ Наименование транспортной компании ]
 * @property integer $shipmentDate [ Дата, когда должна быть отгружена отгрузка ]
 * @property string $trackCode [ Трек - код отгрузки, для отслеживания грузка при движении в транспортной компании ]
 * @property double $weight [ Вес отгрузки ]
 * @property string $comment [ Комментарий ко всей отгрузке ]
 * @property string $logisticsStatus [ Статус отгрузки на текущий момент ]
 * @property integer $hasWeighing [ 'Со взвешиванием / без взвешивания' ]
 * @property integer $createdAt [ Дата создания ]
 * @property integer $updatedAt [ Дата последнего изменения ]
 * @property integer $authorId [ Кто создал ]
 * @property integer $updaterId [ Последний редактор ]
 * @property string $cancelLog  [ Некий слепок отгрузки на момент её отмены (какие были позиции и т.д.) ]
 * @property integer $handledAt [ Дата обработки ]
 * @property integer $accountedIn1C [ Флаг, отвечающий за то, учтена ли отгрузка в бухучете (проведен ли в 1С документ-реализация). ]
 * @property integer $accountedAt [ Дата проведения в 1С ]
 * @property double $deliveryPrice [ Сумма доставки ]
 * @property integer $isDeliveryFree [ Явная пометка о том, что доставка бесплатна (чтобы точно знать, что сумму доставки не забыли проставить) ]
 * @property string $size [ Габариты ]
 * @property integer $hasSizing [ С измерением габаритов / без измерения габаритов ]
 * @property integer $packedAt [ Дата упаковки ]
 * @property integer $shippedAt [ Дата отгрузки (фактическая) ]
 * @property integer $isTrackCodeSent [ Пометка о том, что трэк-код выслан клиенту ]
 * @property integer $trackCodeSentAt [ Дата отправки трэк-кода клиенту ]
 * @property integer $transportCompanyId [ Идентификатор транспортной компании ]
 * @property integer $officialNumber [ Официальный номер ТТН ]
 * @property integer $officialNumberYear [ Год для официального номера ТТН ]
 * @property string  $searchString [ Строка поиска ]
 * @property boolean  $documentsArePrinted [ Документы распечатаны или нет]
 *
 *
 * @property Order[] $orders [ Заказы, входящие в отгрузку ]
 * @property Box[]   $boxes  [ Коробки, входящие в отгрузку ]
 * @property array   $logisticsStatusList  [ Перечень наименований статусов ]
 * @property array   $logisticsStatusName  [ Наименование статуса отгрузки ]
 * @property TransportCompany|null  $transportCompany [ Транспортная компания, которой поедет данная отгрузка ]
 * @property OrderEntry[]  $notDeletedOrderEntries [ Позиции из отгрузки, которые должны уехать ]
 * @property integer $entriesForPack [ Количество позиций в отгрузке, готовое к упаковке ]
 */
class Shipment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%logistics_shipment}}';
    }

    const SCENARIO_SAMPLE = 'sample';

    

    /**
     * Логистические статусы, которые могут быть применены к отгрузке.
     */
    const LOGISTICS_STATUS_NOT_HANDLED          = 'not_handled';
    const LOGISTICS_STATUS_ON_STORAGE_REQUEST   = 'on_storage_request';
    const LOGISTICS_STATUS_ON_STORAGE_RESPONSE  = 'on_storage_response';
    const LOGISTICS_STATUS_HANDLED              = 'handled';
    const LOGISTICS_STATUS_PACKED               = 'packed';
    const LOGISTICS_STATUS_SHIPPED              = 'shipped';
    const LOGISTICS_STATUS_CANCELED             = 'canceled';

    /**
     * Перечень наименований статусов
     *
     * @return array
     */
    public function getLogisticsStatusList()
    {
        return[
            self::LOGISTICS_STATUS_NOT_HANDLED          => Yii::t('app', 'Не обработана'),
            self::LOGISTICS_STATUS_ON_STORAGE_REQUEST   => Yii::t('app', 'Отправлен запрос на склад'),
            self::LOGISTICS_STATUS_ON_STORAGE_RESPONSE  => Yii::t('app', 'Получен ответ со склада'),
            self::LOGISTICS_STATUS_HANDLED              => Yii::t('app', 'Обработана'),
            self::LOGISTICS_STATUS_PACKED               => Yii::t('app', 'Упакована'),
            self::LOGISTICS_STATUS_SHIPPED              => Yii::t('app', 'Отгружена'),
            self::LOGISTICS_STATUS_CANCELED             => Yii::t('app', 'Отменена')
        ];
    }

    /**
     * Получить название статуса по ключу
     *
     * @return mixed|string
     */
    public function getLogisticsStatusName()
    {
        $names = $this->logisticsStatusList;
        return $names[$this->logisticsStatus] ?? 'статус отстуствует';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipmentDate', 'hasWeighing', 'createdAt', 'updatedAt', 'authorId', 'updaterId', 'handledAt', 'accountedIn1C', 'accountedAt', 'isDeliveryFree', 'hasSizing', 'packedAt', 'shippedAt', 'isTrackCodeSent', 'trackCodeSentAt', 'transportCompanyId', 'officialNumber', 'officialNumberYear'], 'integer'],
            [['weight', 'deliveryPrice'], 'number'],
            [['documentsArePrinted'], 'boolean'],
            [['cancelLog'], 'string'],
            [['transportCompanyName', 'trackCode', 'logisticsStatus', 'size'], 'string', 'max' => 255],
            [['comment'], 'string', 'max' => 1000],
            [['searchString'], 'string'],
            [['officialNumber', 'officialNumberYear'], 'unique', 'targetAttribute' => ['officialNumber', 'officialNumberYear'], 'message' => 'The combination of Официальный номер ТТН and Год для официального номера ТТН has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'transportCompanyName' => Yii::t('app', 'Наименование транспортной компании'),
            'shipmentDate' => Yii::t('app', 'Дата отгрузки'),
            'trackCode' => Yii::t('app', 'Трэк-код'),
            'weight' => Yii::t('app', 'Вес'),
            'comment' => Yii::t('app', 'Комментарий'),
            'logisticsStatus' => Yii::t('app', 'Логистический статус (Обработан, Не обработан, Ожидается взвешивание, ...)'),
            'hasWeighing' => Yii::t('app', 'Со взвешиванием / без взвешивания'),
            'createdAt' => Yii::t('app', 'Дата создания'),
            'updatedAt' => Yii::t('app', 'Дата последнего изменения'),
            'authorId' => Yii::t('app', 'Автор'),
            'updaterId' => Yii::t('app', 'Последний редактор'),
            'cancelLog' => Yii::t('app', 'Некий слепок отгрузки на момент её отмены (какие были позиции и т.д.)'),
            'handledAt' => Yii::t('app', 'Дата обработки'),
            'accountedIn1C' => Yii::t('app', 'Флаг, отвечающий за то, учтена ли отгрузка в бухучете (проведен ли в 1С документ-реализация).'),
            'accountedAt' => Yii::t('app', 'Дата проведения в 1С'),
            'deliveryPrice' => Yii::t('app', 'Сумма доставки'),
            'isDeliveryFree' => Yii::t('app', 'Явная пометка о том, что доставка бесплатна (чтобы точно знать, что сумму доставки не забыли проставить)'),
            'size' => Yii::t('app', 'Габариты'),
            'hasSizing' => Yii::t('app', 'С измерением габаритов / без измерения габаритов'),
            'packedAt' => Yii::t('app', 'Дата упаковки'),
            'shippedAt' => Yii::t('app', 'Дата отгрузки (фактическая)'),
            'isTrackCodeSent' => Yii::t('app', 'Пометка о том, что трэк-код выслан клиенту'),
            'trackCodeSentAt' => Yii::t('app', 'Дата отправки трэк-кода клиенту'),
            'transportCompanyId' => Yii::t('app', 'Идентификатор транспортной компании'),
            'officialNumber' => Yii::t('app', 'Официальный номер ТТН'),
            'officialNumberYear' => Yii::t('app', 'Год для официального номера ТТН'),
            'searchString' => Yii::t('app', 'Строка поиска'),
            'documentsArePrinted' => Yii::t('app', 'Документы распечатаны'),
        ];
    }

    /**
     * @inheritdoc
     * @return ShipmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShipmentQuery(get_called_class());
    }

    /**
     * Связь для получения заказов, входящих в отгрузку
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])
            ->viaTable(OrderEntry::tableName(), ['shipment_id' => 'id']);
    }

    /**
     * Проверяем, необходимо ли вкладывать ТТН в отгрузку
     *
     * @return bool
     */
    public function isNeedTTN()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id'])
            ->viaTable(OrderEntry::tableName(), ['shipment_id' => 'id'])
            ->where(['is_need_ttn' => true])->exists();
    }

    /**
     * Связь для получения коробок, которые входят в отгрузку
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBoxes()
    {
        return $this->hasMany(Box::className(), ['id' => 'boxId'])
            ->viaTable(OrderEntry::tableName(), ['shipment_id' => 'id']);
    }

    /**
     * Связь с транспортной компанией, которой поедет данная отгрузка
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTransportCompany()
    {
        return $this->hasOne(TransportCompany::className(),['id' => 'transportCompanyId']);
    }

    /**
     * Все не удаленные элементы, которые должны поехать в этой отгрузке
     * @return \yii\db\ActiveQuery
     */
    public function getNotDeletedOrderEntries()
    {
        return $this->hasMany(OrderEntry::className(), ['shipment_id' => 'id'])->where('is_deleted is NULL');
    }

    /**
     * Количество позиций, которые можно упаковать для данной отгрузки
     *
     * @return bool|int|string|null
     */
    public function getEntriesForPack()
    {
        return $this->getBoxes()
            ->where(['status' => Box::STATUS_READY_TO_PACK])
            ->joinWith('orderEntries')
            ->count();
    }

    /**
     * @return bool
     */
    public function isHandled()
    {
        return $this->logisticsStatus === self::LOGISTICS_STATUS_HANDLED;
    }

    /**
     * @return bool
     */
    public function isPacked()
    {
        return $this->logisticsStatus === self::LOGISTICS_STATUS_PACKED;
    }

    /**
     * @return bool
     */
    public function isOnStorageRequest()
    {
        return $this->logisticsStatus === self::LOGISTICS_STATUS_ON_STORAGE_REQUEST;
    }

    /**
     * @return bool
     */
    public function isShipped()
    {
        return $this->logisticsStatus === self::LOGISTICS_STATUS_SHIPPED;
    }

    /**
     * Означает, что отгрузка еще не обработана
     *
     * @return bool
     */
    public function isBeforeHandling(): bool
    {
        return !$this->logisticsStatus || in_array($this->logisticsStatus, [
                self::LOGISTICS_STATUS_NOT_HANDLED,
                self::LOGISTICS_STATUS_ON_STORAGE_REQUEST,
                self::LOGISTICS_STATUS_ON_STORAGE_RESPONSE
            ]);
    }

}
