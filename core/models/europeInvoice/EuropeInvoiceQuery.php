<?php

namespace core\models\europeInvoice;

/**
 * This is the ActiveQuery class for [[EuropeInvoice]].
 *
 * @see EuropeInvoice
 */
class EuropeInvoiceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return EuropeInvoice[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EuropeInvoice|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return EuropeInvoiceQuery
     */
    public function notLoad()
    {
        return $this->andWhere(['isLoad' => false]);
    }

    /**
     * @return EuropeInvoiceQuery
     */
    public function onlyForGermany()
    {
        return $this->andWhere(['isLaitovoManufactory' => true]);
    }
}
