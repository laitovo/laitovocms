<?php

namespace core\models\europeInvoice;

use common\models\logistics\Naryad;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%europe_invoice_entry}}".
 *
 * @property integer $id
 * @property integer $invoiceId
 * @property integer $upnId
 * @property string $article
 * @property string $title
 * @property double $price
 * @property double $quantity
 *
 * @property EuropeInvoice $invoice
 */
class EuropeInvoiceEntry extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%europe_invoice_entry}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoiceId','upnId'], 'integer'],
            [['article', 'title', 'price', 'quantity','upnId'], 'required'],
            [['price', 'quantity'], 'number'],
            [['article', 'title'], 'string', 'max' => 255],
            [['invoiceId'], 'exist', 'skipOnError' => true, 'targetClass' => EuropeInvoice::className(), 'targetAttribute' => ['invoiceId' => 'id']],
            [['upnId'], 'exist', 'skipOnError' => true, 'targetClass' => Naryad::className(), 'targetAttribute' => ['upnId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'invoiceId' => Yii::t('app', 'Инвойс, к которому относятся строки табличной части'),
            'upnId' => Yii::t('app', 'Уникальный номер продукта'),
            'article' => Yii::t('app', 'Артикул продукта'),
            'title' => Yii::t('app', 'Наименование продукта'),
            'price' => Yii::t('app', 'Цена продукта'),
            'quantity' => Yii::t('app', 'Количество продукта'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(EuropeInvoice::className(), ['id' => 'invoiceId']);
    }

    /**
     * @inheritdoc
     * @return EuropeInvoiceEntryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EuropeInvoiceEntryQuery(get_called_class());
    }
}
