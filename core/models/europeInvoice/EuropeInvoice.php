<?php

namespace core\models\europeInvoice;

use backend\modules\logistics\modules\europeInvoice\models\SEntryFill;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%europe_invoice}}".
 *
 * @property integer $id
 * @property string $basedOn
 * @property double $rate
 * @property bool $isLoad
 * @property integer $createdAt
 * @property integer $authorId
 * @property bool $isLaitovoManufactory
 *
 * @property EuropeInvoiceEntry[] $europeInvoiceEntries
 */
class EuropeInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%europe_invoice}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['authorId'],
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['basedOn'], 'string'],
            [['rate'], 'double'],
            [['rate'], 'required'],
            [['createdAt', 'authorId'], 'integer'],
            [['isLoad'], 'boolean'],
            [['isLaitovoManufactory'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'basedOn' => Yii::t('app', 'Список отгрузок'),
            'rate' => Yii::t('app', 'Курс валюты'),
            'isLoad' => Yii::t('app', 'Выгружен в учет'),
            'createdAt' => Yii::t('app', 'Дата создания'),
            'authorId' => Yii::t('app', 'Автор'),
            'totalPrice' => Yii::t('app', 'Сумма по инвойсу, евро'),
            'totalCount' => Yii::t('app', 'Количество артикулов'),
            'isLaitovoManufactory' => Yii::t('app', 'Отгрузка в Laitovo Manufactory'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEuropeInvoiceEntries()
    {
        return $this->hasMany(EuropeInvoiceEntry::className(), ['invoiceId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTotalPrice()
    {
        return $this->hasMany(EuropeInvoiceEntry::className(), ['invoiceId' => 'id'])->sum('price');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTotalCount()
    {
        return $this->hasMany(EuropeInvoiceEntry::className(), ['invoiceId' => 'id'])->sum('quantity');
    }

    /**
     * @inheritdoc
     * @return EuropeInvoiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EuropeInvoiceQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert || isset($changedAttributes['basedOn']) || isset($changedAttributes['rate'])){
            SEntryFill::fill($this);
        }
        return parent::afterSave($insert, $changedAttributes);
    }
}
