<?php

namespace core\models\europeInvoice;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use core\models\europeInvoice\EuropeInvoice;

/**
 * EuropeInvoiceSearch represents the model behind the search form about `core\models\europeInvoice\EuropeInvoice`.
 */
class EuropeInvoiceSearch extends EuropeInvoice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'createdAt', 'authorId'], 'integer'],
            [['basedOn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EuropeInvoice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdAt' => $this->createdAt,
            'authorId' => $this->authorId,
        ]);

        $query->andFilterWhere(['like', 'basedOn', $this->basedOn]);

        return $dataProvider;
    }
}
