<?php

namespace core\models\europeInvoice;

/**
 * This is the ActiveQuery class for [[EuropeInvoiceEntry]].
 *
 * @see EuropeInvoiceEntry
 */
class EuropeInvoiceEntryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return EuropeInvoiceEntry[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EuropeInvoiceEntry|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
