<?php

namespace core\models\transportCompany;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[TransportCompany]].
 *
 * @see TransportCompany
 */
class TransportCompanyQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TransportCompany[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TransportCompany|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
