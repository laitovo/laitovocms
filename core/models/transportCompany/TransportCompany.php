<?php

namespace core\models\transportCompany;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "logistics_transport_company".
 *
 * @property integer $id
 * @property string $name
 * @property string $trackingLink
 * @property integer $createdAt
 * @property integer $updatedAt
 * @property integer $authorId
 * @property integer $updaterId
 */
class TransportCompany extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_transport_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['createdAt', 'updatedAt', 'authorId', 'updaterId'], 'integer'],
            [['name', 'trackingLink'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Наименование'),
            'trackingLink' => Yii::t('app', 'Ссылка для отслеживания трэк-кода'),
            'createdAt' => Yii::t('app', 'Дата создания'),
            'updatedAt' => Yii::t('app', 'Дата последнего изменения'),
            'authorId' => Yii::t('app', 'Автор'),
            'updaterId' => Yii::t('app', 'Последний редактор'),
        ];
    }

    /**
     * @inheritdoc
     * @return TransportCompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransportCompanyQuery(get_called_class());
    }
}
