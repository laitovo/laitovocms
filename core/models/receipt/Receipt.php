<?php

namespace core\models\receipt;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%core_receipt}}".
 *
 * @property integer $id
 * @property integer $clientId
 * @property string  $locale
 * @property integer $documentId
 * @property integer $date
 * @property string  $fromType
 * @property string  $toType
 * @property integer $orderNumber
 * @property string  $total
 * @property string  $currency
 * @property string  $mode
 */
class Receipt extends ActiveRecord
{

    const MODE_REALIZATION = 'Реализация';
    const MODE_OTHER = 'Прочее';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_receipt}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientId', 'locale', 'date', 'total','documentId','currency','mode'], 'required'],
            [['clientId', 'date', 'orderNumber','documentId'], 'integer'],
            [['locale'], 'string'],
            [['currency'], 'string'],
            [['mode'], 'string'],
            [['total'], 'number'],
            [['fromType', 'toType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clientId' => Yii::t('app', 'Уникальный идентификатор клиента в определнной локали'),
            'locale' => Yii::t('app', 'Locale'),
            'documentId' => Yii::t('app', 'Уникальный номер документа в соответвующей ЛОКАЛИ (Системе)'),
            'date' => Yii::t('app', 'Учетная дата поступления'),
            'fromType' => Yii::t('app', 'Источник поступления, прихода'),
            'toType' => Yii::t('app', 'Куда поступили средства'),
            'orderNumber' => Yii::t('app', 'Номер заказа, на основании которого была произведена реализация'),
            'total' => Yii::t('app', 'Сумма поступления'),
            'currency' => Yii::t('app', 'Валюта'),
            'mode' => Yii::t('app', 'Вид поступления'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReceiptQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReceiptQuery(get_called_class());
    }
}
