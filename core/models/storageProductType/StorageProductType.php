<?php

namespace core\models\storageProductType;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%logistics_storage_product_type}}".
 *
 * @property int $id
 * @property string|null $title Наименование
 * @property int $hide Не показывать в списке
 * @property string|null $article Артикул
 * @property string|null $barcode Штрихкод
 * @property int|null $group Группа для печати (некая категория складской продукции, используемая для удобства печати)
 * @property int|null $hasBalanceControl Контроль складских остатков - пометка о том, что мы обязаны контролировать остатки по данному виду продукции
 * @property int|null $minBalance Минимальный объем складских остатков
 * @property int|null $createdAt Дата создания
 * @property int|null $updatedAt Дата последнего изменения
 * @property int|null $authorId Автор
 * @property int|null $updaterId Последний редактор
 * @property int|null $canBeProduced Может ли мы производить данный вид продукции
 */
class StorageProductType extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%logistics_storage_product_type}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['hide', 'group', 'hasBalanceControl', 'minBalance', 'createdAt', 'updatedAt', 'authorId', 'updaterId', 'canBeProduced'], 'integer'],
            [['title', 'article', 'barcode'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Наименование'),
            'hide' => Yii::t('app', 'Не показывать в списке'),
            'article' => Yii::t('app', 'Артикул'),
            'barcode' => Yii::t('app', 'Штрихкод'),
            'group' => Yii::t('app', 'Группа для печати (некая категория складской продукции, используемая для удобства печати)'),
            'hasBalanceControl' => Yii::t('app', 'Контроль складских остатков - пометка о том, что мы обязаны контролировать остатки по данному виду продукции'),
            'minBalance' => Yii::t('app', 'Минимальный объем складских остатков'),
            'createdAt' => Yii::t('app', 'Дата создания'),
            'updatedAt' => Yii::t('app', 'Дата последнего изменения'),
            'authorId' => Yii::t('app', 'Автор'),
            'updaterId' => Yii::t('app', 'Последний редактор'),
            'canBeProduced' => Yii::t('app', 'Может ли мы производить данный вид продукции'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return StorageProductTypeQuery the active query used by this AR class.
     */
    public static function find(): StorageProductTypeQuery
    {
        return new StorageProductTypeQuery(get_called_class());
    }
}
