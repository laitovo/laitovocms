<?php

namespace core\models\storageProductType;

use yii\db\ActiveRecord;

/**
 * This is the ActiveQuery class for [[StorageProductType]].
 *
 * @see StorageProductType
 */
class StorageProductTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return StorageProductType[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return array|ActiveRecord|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
