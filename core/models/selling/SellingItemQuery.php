<?php

namespace core\models\selling;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[SellingItem]].
 *
 * @see SellingItem
 */
class SellingItemQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SellingItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SellingItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
