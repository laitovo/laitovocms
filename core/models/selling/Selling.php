<?php

namespace core\models\selling;

use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "core_selling".
 *
 * @property integer $id
 * @property integer $clientId
 * @property string $locale
 * @property integer $date
 * @property integer $orderNumber
 * @property string $sumGoodsWithoutNds
 * @property string $sumDeliveryWithoutNds
 * @property string $discountWithoutNds
 * @property string $sumWithoutNds
 * @property string $amountNds
 * @property string $sumNds
 * @property string $discountWithNds
 * @property string $sumWithNds
 *
 * @property SellingItem[] $sellingItems
 */
class Selling extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_selling';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientId', 'locale', 'date', 'sumGoodsWithoutNds', 'sumDeliveryWithoutNds', 'discountWithoutNds', 'sumWithoutNds', 'amountNds', 'sumNds', 'discountWithNds', 'sumWithNds'], 'required'],
            [['clientId', 'date', 'orderNumber'], 'integer'],
            [['locale'], 'string'],
            [['sumGoodsWithoutNds', 'sumDeliveryWithoutNds', 'discountWithoutNds', 'sumWithoutNds', 'amountNds', 'sumNds', 'discountWithNds', 'sumWithNds'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clientId' => Yii::t('app', 'Уникальный идентификатор клиента в определнной локали'),
            'locale' => Yii::t('app', 'Locale'),
            'date' => Yii::t('app', 'Учетная дата реализации'),
            'orderNumber' => Yii::t('app', 'Номер заказа, на основании которого была произведена реализация'),
            'sumGoodsWithoutNds' => Yii::t('app', 'Сумма стоимости позиций реализации без НДС'),
            'sumDeliveryWithoutNds' => Yii::t('app', 'Стоимость доставки без НДС'),
            'discountWithoutNds' => Yii::t('app', 'Скидка без НДС (оплата бонусами)'),
            'sumWithoutNds' => Yii::t('app', 'Сумма реализации без НДС ( Сумма стоимости позиций реализации без НДС + Стоимость доставки без НДС - Скидка без НДС (оплата бонусами))'),
            'amountNds' => Yii::t('app', 'Значение налога НДС в %'),
            'sumNds' => Yii::t('app', 'Сумма налога НДС с реализации'),
            'discountWithNds' => Yii::t('app', 'Скидка с НДС (оплата бонусами)'),
            'sumWithNds' => Yii::t('app', 'Сумма реализации с учетом НДС'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSellingItems()
    {
        return $this->hasMany(SellingItem::className(), ['sellingId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return SellingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SellingQuery(get_called_class());
    }
}
