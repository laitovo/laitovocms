<?php

namespace core\models\selling;

use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "core_selling_item".
 *
 * @property integer $id
 * @property integer $sellingId
 * @property string $title
 * @property string $article
 * @property string $count
 * @property string $priceWithoutNds
 * @property string $costWithoutNds
 * @property string $discountWithoutNds
 * @property string $sumWithoutNds
 * @property string $amountNds
 * @property string $sumNds
 * @property string $discountWithNds
 * @property string $sumWithNds
 *
 * @property Selling $selling
 */
class SellingItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_selling_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sellingId', 'title', 'article', 'count', 'priceWithoutNds', 'costWithoutNds', 'discountWithoutNds', 'sumWithoutNds', 'amountNds', 'sumNds', 'discountWithNds', 'sumWithNds'], 'required'],
            [['sellingId'], 'integer'],
            [['count', 'priceWithoutNds', 'costWithoutNds', 'discountWithoutNds', 'sumWithoutNds', 'amountNds', 'sumNds', 'discountWithNds', 'sumWithNds'], 'number'],
            [['title', 'article'], 'string', 'max' => 255],
            [['sellingId'], 'exist', 'skipOnError' => true, 'targetClass' => Selling::className(), 'targetAttribute' => ['sellingId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sellingId' => Yii::t('app', 'Уникальный идентификатор реализации'),
            'title' => Yii::t('app', 'Наименовании позиции товара'),
            'article' => Yii::t('app', 'Артикул товара'),
            'count' => Yii::t('app', 'Количество условных единиц товара (по умолчанию шт.)'),
            'priceWithoutNds' => Yii::t('app', 'Цена за единицу без НДС'),
            'costWithoutNds' => Yii::t('app', 'Стоимость без НДС = ( Количество условных единиц товара (по умолчанию шт.) * Цена за единицу без НДС )'),
            'discountWithoutNds' => Yii::t('app', 'Скидка без НДС (оплата бонусами)'),
            'sumWithoutNds' => Yii::t('app', 'Сумма позиции без НДС ( Стоимость без НДС - Скидка без НДС (оплата бонусами))'),
            'amountNds' => Yii::t('app', 'Значение налога НДС в %'),
            'sumNds' => Yii::t('app', 'Сумма налога НДС с реализации'),
            'discountWithNds' => Yii::t('app', 'Скидка с НДС(оплата бонусами)'),
            'sumWithNds' => Yii::t('app', 'Сумма позиции с учетом НДС'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSelling()
    {
        return $this->hasOne(Selling::className(), ['id' => 'sellingId']);
    }

    /**
     * @inheritdoc
     * @return SellingItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SellingItemQuery(get_called_class());
    }
}
