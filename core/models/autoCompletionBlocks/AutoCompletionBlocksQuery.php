<?php

namespace core\models\autoCompletionBlocks;

use  yii\db\ActiveQuery;
/**
 * This is the ActiveQuery class for [[AutoCompletionBlocks]].
 *
 * @see AutoCompletionBlocks
 */
class AutoCompletionBlocksQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * [ Проверяет - если данная маска ]
     *
     * @param $mask
     * @return bool
     */
    public function maskExists($mask)
    {
        return $this->andWhere(['catalogMask' => $mask])->exists();
    }

    /**
     * [ Возвращаеи блокировку по маске ]
     *
     * @param $mask
     * @return array|AutoCompletionBlocks|null
     */
    public function oneByMask($mask)
    {
        return $this->andWhere(['catalogMask' => $mask])->one();
    }

    /**
     * @inheritdoc
     * @return AutoCompletionBlocks[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AutoCompletionBlocks|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
