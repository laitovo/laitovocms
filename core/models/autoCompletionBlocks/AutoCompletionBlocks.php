<?php

namespace core\models\autoCompletionBlocks;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%logistics_autocompletion_blocks}}".
 *
 * @property integer $id
 * @property string $catalogMask
 * @property string $carTitle
 * @property string $productTitle
 */
class AutoCompletionBlocks extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%logistics_autocompletion_blocks}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalogMask'], 'string', 'max' => 20],
            [['carTitle'], 'string', 'max' => 255],
            [['productTitle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'catalogMask'  => Yii::t('app', 'Маска атикула для блокировки'),
            'carTitle'      => Yii::t('app', 'Наименование автомобиля'),
            'productTitle' => Yii::t('app', 'Наименование продукта'),
        ];
    }

    /**
     * @inheritdoc
     * @return AutoCompletionBlocksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutoCompletionBlocksQuery(get_called_class());
    }
}
