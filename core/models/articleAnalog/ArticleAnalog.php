<?php

namespace core\models\articleAnalog;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%laitovo_article_analog}}".
 *
 * @property string $article
 * @property string $analogArticle
 * @property integer $date
 */
class ArticleAnalog extends ActiveRecord
{
    const TYPE_CAR = 1;
    const TYPE_COMMON = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_article_analog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article', 'analogArticle', 'date'], 'required'],
            [['date'], 'integer'],
            [['article', 'analogArticle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article' => Yii::t('app', 'Артикул'),
            'analogArticle' => Yii::t('app', 'Артикул-аналог'),
            'date' => Yii::t('app', 'Дата'),
        ];
    }

    /**
     * @inheritdoc
     * @return ArticleAnalogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleAnalogQuery(get_called_class());
    }
}
