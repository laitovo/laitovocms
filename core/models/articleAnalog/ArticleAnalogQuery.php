<?php

namespace core\models\articleAnalog;

/**
 * This is the ActiveQuery class for [[ArticleAnalog]].
 *
 * @see ArticleAnalog
 */
class ArticleAnalogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function articleExists($article,$db = null)
    {
        return $this->andWhere(['article' => $article])->exists($db);
    }

    public function analogsWithArticle($article)
    {
        return $this->andWhere(['article' => $article]);
    }

    /**
     * @inheritdoc
     * @return ArticleAnalog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ArticleAnalog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}