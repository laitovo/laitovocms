<?php
namespace core\models\articleAnalog;

use backend\helpers\ArticleHelper;
use yii\db\Query;

/**
 * Класс который отвечает за генерацию статистики
 *
 * Class ArticleStatisticsGenerator
 * @package core\models\articleStatistics
 */
class ArticleAnalogGenerator
{
    /**
     * @param $article
     * @param $carArticle
     * @param $window
     * @param $type
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function execute($article,$carArticle,$window,$type)
    {
        switch ($type) {
            case ArticleAnalog::TYPE_CAR:
                $this->saveCar($article,$carArticle,$window);
                break;
            case ArticleAnalog::TYPE_COMMON:
                $this->saveCommon($article);
                break;
        }
    }

    /**
     * @param $article
     * @param $carArticle
     * @param $window
     * @return array
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    private function saveCar($article,$carArticle,$window)
    {
        $articlesArray[] = $article;

        $analogArticles = (new Query())
            ->select('analogArticle')
            ->from('tmp_analogs')
            ->where('carArticle = :carArticle')
            ->andWhere('window = :window')
            ->andWhere('analogArticle is not NULL')
            ->addParams([':carArticle' => $carArticle])
            ->addParams([':window' => $window])
            ->column();

        if (!empty($analogArticles)) {
            foreach ($analogArticles as $analogArticle) {
                $newArticle = ArticleHelper::rebuildArticleForAnalog($article,$analogArticle);
                if ($newArticle)
                    $articlesArray[] = $newArticle;
            }
        }

        $rows = ArticleAnalog::find()->analogsWithArticle($article)->all();
        if ($rows) {
            foreach ($rows as $row) {
                $row->delete();
            }
        }

        foreach ($articlesArray as $articleRow) {
            $row = new ArticleAnalog(['article' => $article, 'analogArticle' => $articleRow,'date' => time()]);
            $row->save();
        }
    }

    /**
     * @param $article
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    private function saveCommon($article)
    {
        $rows = ArticleAnalog::find()->analogsWithArticle($article)->all();
        if ($rows) {
            foreach ($rows as $row) {
                $row->delete();
            }
        }

        $row = new ArticleAnalog(['article' => $article, 'analogArticle' => $article, 'date' => time()]);
        $row->save();
    }

}