<?php

namespace core\models\autoCompletionItems;

use core\models\article\Article;
use Yii;
use yii\db\ActiveRecord;
use core\models\autoCompletionLogs\AutoCompletionLogs;

/**
 * This is the model class for table "{{%logistics_autocompletion_items}}".
 *
 * @property integer $id
 * @property integer $orderNumber
 * @property integer $logId
 * @property string  $article
 * @property integer $requested
 * @property integer $createdAt
 * @property integer $updatedAt
 * @property integer $status
 * @property integer $indexGroup
 *
 * @property AutoCompletionLogs $log
 */
class AutoCompletionItems extends ActiveRecord
{
    /**
     * @var Article|null Приватная переменная для хранения объекта артикула
     */
    private $_articleObj;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%logistics_autocompletion_items}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderNumber', 'logId', 'requested', 'createdAt', 'updatedAt', 'indexGroup'], 'integer'],
            [['logId', 'article', 'requested'], 'required'],
            [['article'], 'string', 'max' => 20],
            [['status'], 'boolean'],
            [['logId'], 'exist', 'skipOnError' => true, 'targetClass' => AutoCompletionLogs::className(), 'targetAttribute' => ['logId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'orderNumber' => Yii::t('app', 'Номер заказа'),
            'logId'       => Yii::t('app', 'Лог'),
            'article'     => Yii::t('app', 'Артикул'),
            'requested'   => Yii::t('app', 'Количество'),
            'createdAt'   => Yii::t('app', 'Дата создания записи'),
            'updatedAt'   => Yii::t('app', 'Дата обновления записи'),
            'status'      => Yii::t('app', 'Статус'),
            'indexGroup'  => Yii::t('app', 'Индекс комплекта'),

            'carName'     => Yii::t('app', 'Наименование автомобиля'),
            'title'       => Yii::t('app', 'Наименование продука'),
            'statistics'  => Yii::t('app', 'Статистика'),
            'balance'     => Yii::t('app', 'Остатки'),
            'balanceProdFree' => Yii::t('app', 'На производстве'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLog()
    {
        return $this->hasOne(AutoCompletionLogs::className(), ['id' => 'logId']);
    }

    /**
     * @inheritdoc
     * @return AutoCompletionItemsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutoCompletionItemsQuery(get_called_class());
    }

    /**
     * Функция для чтения наименования автомобиля, для которого идет автопополнение
     */
    public function getCarName()
    {
        $article = $this->loadArticle();
        return $article->carName;
    }

    /**
     * Функция для чтения наименования артикула, для которого идет автопополнение
     */
    public function getTitle()
    {
        $article = $this->loadArticle();
        return $article->title;
    }

    /**
     * Функция для чтения статистики продаж за последние 2 месяца
     */
    public function getStatistics()
    {
        $article = $this->loadArticle();
        return $article->saleSixMonth;
    }

    /**
     * Функция для чтения фактических остатков
     */
    public function getBalance()
    {
        $article = $this->loadArticle();
        return $article->balanceFact;
    }

    /**
     * Функция для чтения позиций на производстве для пополнения
     */
    public function getBalanceProdFree()
    {
        $article = $this->loadArticle();
        return $article->balanceProdFree;
    }

    /**
     * @return Article|null Возвращает объект артикула
     */
    private function loadArticle()
    {
        if ($this->_articleObj) return $this->_articleObj;
        $this->_articleObj = new Article($this->article);
        return $this->_articleObj;
    }
}
