<?php

namespace core\models\autoCompletionItems;

use yii\db\ActiveQuery;
/**
 * This is the ActiveQuery class for [[AutoCompletionItems]].
 *
 * @see AutoCompletionItems
 */
class AutoCompletionItemsQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @return $this Получить записи, которые еще не подвергались обработке
     */
    public function notHandled()
    {
        return $this->andWhere(['status' => false]);
    }

    /**
     * [ Получить записи не обработанные по артикулу
     *
     * @param $article
     * @return $this
     */
    public function notHandledByArticle($article)
    {
        return $this->andWhere(['article' => $article, 'status' => false]);
    }

    /**
     * [ Получить записи не обработанные по маске
     *
     * @param $mask
     * @return $this
     */
    public function notHandledLikeMask($mask)
    {
        return $this->andWhere(['and',
            ['like','article',$mask,false],
            ['status' => false]
        ]);
    }

    /**
     * @inheritdoc
     * @return AutoCompletionItems[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AutoCompletionItems|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
