<?php
/**
 * Created by PhpStorm.
 * User: heksweb
 * Date: 2/28/19
 * Time: 6:44 AM
 */

namespace core\models\autoCompletionItems;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

class AutoCompletionItemsSearch extends AutoCompletionItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'createdAt'], 'integer'],
//            [['title', 'rule'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoCompletionItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdAt' => $this->createdAt,
        ]);

//        $query->andFilterWhere(['like', 'title', $this->title])
//            ->andFilterWhere(['like', 'rule', $this->rule]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function searchNotHandled($params)
    {
        $query = AutoCompletionItems::find()->select('article,SUM(requested) as requested, MAX(createdAt) as createdAt')->notHandled()->groupBy('article');

        // add conditions that should always apply here

        $dataProvider = new ArrayDataProvider([
            'allModels'  => [],
            'sort'       => [
                'attributes' => ['carName','title','article','requested','statistics','balance','balanceProdFree','createdAt'],
                'defaultOrder' => ['carName' => SORT_ASC]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdAt' => $this->createdAt,
        ]);

//        $query->andFilterWhere(['like', 'title', $this->title])
//            ->andFilterWhere(['like', 'rule', $this->rule]);

        $all = $query->all();
        $allElem = [];
        foreach ($all as $one) {
            $elem = [
                'carName'            => $one->carName,
                'title'              => $one->title,
                'article'            => $one->article,
                'requested'          => $one->requested,
                'statistics'         => $one->statistics,
                'balance'            => $one->balance,
                'balanceProdFree'    => $one->balanceProdFree,
                'createdAt'          => $one->createdAt,
            ];
            $allElem[] = $elem;
        }

        $dataProvider->allModels = $allElem;

        return $dataProvider;

    }
}
