<?php

namespace core\models\debt;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%core_debt}}".
 *
 * @property integer $id
 * @property integer $clientId
 * @property string $locale
 * @property integer $date
 * @property string $total
 * @property string $type
 * @property integer $documentId
 * @property integer $updatedAt
 */
class Debt extends ActiveRecord
{
    const RETURN      = 'Возврат';
    const ADJUSTMENT  = 'Корректировка';
    const REALIZATION = 'Реализация';
    const BALANCE     = 'Движение д/с';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_debt}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientId', 'locale', 'date', 'total','type','documentId'], 'required'],
            [['clientId', 'date','documentId','updatedAt'], 'integer'],
            [['locale'], 'string'],
            [['total'], 'number'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clientId' => Yii::t('app', 'Уникальный идентификатор клиента в определнной локали'),
            'locale' => Yii::t('app', 'Locale'),
            'date' => Yii::t('app', 'Учетная дата поступления'),
            'total' => Yii::t('app', 'Сумма поступления'),
            'type' => Yii::t('app', 'Тип документа'),
            'documentId' => Yii::t('app', 'Номер документа в соответсвующей системе'),
            'updatedAt' => Yii::t('app', 'Дата последнего апдейта в родительской таблице'),
        ];
    }

    /**
     * @inheritdoc
     * @return DebtQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DebtQuery(get_called_class());
    }
}
