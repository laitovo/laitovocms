<?php

namespace core\models\box;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\OrderEntry;
use core\models\shipment\Shipment;
use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%logistics_box}}".
 *
 * @property integer $id
 * @property integer $status
 * @property boolean $remoteStorageLiteral
 * @property boolean $remoteStorageBarcode
 *
 * @property boolean $isPacked
 *
 * @property OrderEntry[] $orderEntries
 * @property Order[] $orders
 * @property Shipment $shipment
 */
class Box extends ActiveRecord
{

    /**
     * @const[integer] STATUS_NOT_HANDLED [Статус - в обработке]
     */
    const STATUS_NOT_HANDLED = 0;

    /**
     * @const[integer] STATUS_READY_TO_PACK [Статус - коробка готова к упаковке]
     */
    const STATUS_READY_TO_PACK = 10;

    /**
     * @const[integer] STATUS_READY_TO_PACK [Статус - коробка упакована]
     */
    const STATUS_PACKED = 20;
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return '{{%logistics_box}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOrderEntries()
    {
        return $this->hasMany(OrderEntry::className(), ['boxId' => 'id']);
    }

    /**
     * Связь для получения заказов, входящих в коробку
     *
     * @return ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])
            ->viaTable(OrderEntry::tableName(), ['boxId' => 'id']);
    }

    /**
     * Связь для получения отгрузки, в которую входит коробка
     * @return ActiveQuery
     */
    public function getShipment()
    {
        return $this->hasOne(Shipment::className(),['id' => 'shipment_id'])
            ->viaTable(OrderEntry::tableName(),['boxId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return BoxQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BoxQuery(get_called_class());
    }

    /**
     * [Свойство - указывает упакована ли коробка]
     *
     * @return bool
     */
    public function getIsPacked()
    {
        return $this->status === self::STATUS_PACKED;
    }
}
