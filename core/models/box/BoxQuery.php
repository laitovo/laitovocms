<?php

namespace core\models\box;

use  yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Box]].
 *
 * @see Box
 */
class BoxQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Box[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Box|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * [Возвращаем все коробки, которые готовы к упаковке.]
     *
     * @inheritdoc
     * @return Box|array|null
     */
    public function readyToPack($db = null)
    {
        return $this->andWhere(['status' => Box::STATUS_READY_TO_PACK])->all($db);
    }
}
