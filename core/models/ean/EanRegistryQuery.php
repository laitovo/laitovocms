<?php

namespace core\models\ean;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Ean]].
 *
 * @see Ean
 */
class EanRegistryQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Ean[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Ean|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
