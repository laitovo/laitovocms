<?php

namespace core\models\ean;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EanSearch represents the model behind the search form about `core\models\ean\Ean`.
 */
class EanSearch extends Ean
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ean', 'carTitle'], 'safe'],
            [['carArticle'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ean::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'carArticle' => $this->carArticle,
        ]);

        $query->andFilterWhere(['like', 'ean', $this->ean])
            ->andFilterWhere(['like', 'carTitle', $this->carTitle]);

        return $dataProvider;
    }
}
