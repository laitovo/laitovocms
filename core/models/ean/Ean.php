<?php

namespace core\models\ean;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%ean_registry}}".
 *
 * @property string $ean
 * @property integer $carArticle
 * @property string $carTitle
 */
class Ean extends ActiveRecord
{
    /**
     * Константы, которые опрделяют тип зпс, для которой заведен EAN
     */
    const TYPE_LAITOVO = 'laitovo';
    const TYPE_SIMPLE  = 'simple';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ean_registry}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ean', 'carArticle', 'carTitle', 'type'], 'required'],
            [['carArticle'], 'integer'],
            [['ean', 'carTitle'], 'string', 'max' => 255],
            ['type','in','range' => [self::TYPE_LAITOVO, self::TYPE_SIMPLE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ean' => Yii::t('app', 'Номер EAN'),
            'carArticle' => Yii::t('app', 'Артикул автомобиля'),
            'carTitle' => Yii::t('app', 'Наименование автомобиля'),
            'type' => Yii::t('app', 'Бренд ЗПС'),
        ];
    }

    /**
     * @inheritdoc
     * @return EanRegistryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EanRegistryQuery(get_called_class());
    }

    /**
     * Возвращает список возможных типов еан
     * @return array
     */
    public function typesList()
    {
        return [
            self::TYPE_LAITOVO => self::TYPE_LAITOVO,
            self::TYPE_SIMPLE => self::TYPE_SIMPLE
        ];
    }
}
