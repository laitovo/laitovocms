<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_RealizationLog
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_realizationLogManager;

    public function __construct()
    {
        $this->_realizationLogManager = Yii::$container->get('core\entities\logisticsRealizationLog\RealizationLogManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function realizationLogsByAuthor($authorId)
    {
        return $this->_realizationLogManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function realizationLogsByUpdater($updaterId)
    {
        return $this->_realizationLogManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}