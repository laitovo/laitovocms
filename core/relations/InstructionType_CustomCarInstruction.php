<?php

namespace core\relations;

use Yii;

class InstructionType_CustomCarInstruction
{
    private $_instructionTypeManager;
    private $_customCarInstructionManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_customCarInstructionManager = Yii::$container->get('core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager');
    }

    public function instructionType($instructionTypeId)
    {
        return $this->_instructionTypeManager->findById($instructionTypeId);
    }

    public function instructionTypeExists($instructionTypeId)
    {
        return $this->_instructionTypeManager->exists($instructionTypeId);
    }

    public function customCarInstructions($instructionTypeId)
    {
        return $this->_customCarInstructionManager->findBy([
            'instructionTypeId' => $instructionTypeId,
        ]);
    }
}