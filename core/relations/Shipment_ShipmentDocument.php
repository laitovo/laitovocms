<?php

namespace core\relations;

use Yii;

class Shipment_ShipmentDocument
{
    private $_shipmentManager;
    private $_shipmentDocumentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_shipmentDocumentManager = Yii::$container->get('core\entities\logisticsShipmentDocument\ShipmentDocumentManager');
    }

    public function shipment($shipmentId)
    {
        return $this->_shipmentManager->findById($shipmentId);
    }

    public function shipmentExists($shipmentId)
    {
        return $this->_shipmentManager->exists($shipmentId);
    }

    public function shipmentDocuments($shipmentId)
    {
        return $this->_shipmentDocumentManager->findBy([
            'shipmentId' => $shipmentId,
        ]);
    }
}