<?php

namespace core\relations;

use Yii;

class ContractorGroup_Contractor
{
    private $_contractorManager;
    private $_groupManager;

    public function __construct()
    {
        $this->_groupManager = Yii::$container->get('core\entities\contractorGroup\GroupManager');
        $this->_contractorManager = Yii::$container->get('core\entities\contractor\ContractorManager');
    }

    public function group($groupId)
    {
        return $this->_groupManager->findById($groupId);
    }

    public function groupExists($groupId)
    {
        return $this->_groupManager->exists($groupId);
    }

    public function contractors($groupId)
    {
        return $this->_contractorManager->findBy([
            'groupId' => $groupId,
        ]);
    }
}