<?php

namespace core\relations;

use Yii;

class Contractor_BankAccount
{
    private $_contractorManager;
    private $_bankAccountManager;

    public function __construct()
    {
        $this->_contractorManager = Yii::$container->get('core\entities\contractor\ContractorManager');
        $this->_bankAccountManager = Yii::$container->get('core\entities\contractorBankAccount\BankAccountManager');
    }

    public function contractor($contractorId)
    {
        return $this->_contractorManager->findById($contractorId);
    }

    public function contractorExists($contractorId)
    {
        return $this->_contractorManager->exists($contractorId);
    }

    public function bankAccounts($contractorId)
    {
        return $this->_bankAccountManager->findBy([
            'contractorId' => $contractorId,
        ]);
    }
}