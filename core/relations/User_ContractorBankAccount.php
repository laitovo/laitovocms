<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_ContractorBankAccount
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_bankAccountManager;

    public function __construct()
    {
        $this->_bankAccountManager = Yii::$container->get('core\entities\contractorBankAccount\BankAccountManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function contractorBankAccountsByAuthor($authorId)
    {
        return $this->_bankAccountManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function contractorBankAccountsByUpdater($updaterId)
    {
        return $this->_bankAccountManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}