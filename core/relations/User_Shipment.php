<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_Shipment
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_shipmentManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function shipmentsByAuthor($authorId)
    {
        return $this->_shipmentManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function shipmentsByUpdater($updaterId)
    {
        return $this->_shipmentManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}