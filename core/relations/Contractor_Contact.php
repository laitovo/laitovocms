<?php

namespace core\relations;

use Yii;

class Contractor_Contact
{
    private $_contractorManager;
    private $_contactManager;

    public function __construct()
    {
        $this->_contractorManager = Yii::$container->get('core\entities\contractor\ContractorManager');
        $this->_contactManager = Yii::$container->get('core\entities\contractorContact\ContactManager');
    }

    public function contractor($contractorId)
    {
        return $this->_contractorManager->findById($contractorId);
    }

    public function contractorExists($contractorId)
    {
        return $this->_contractorManager->exists($contractorId);
    }

    public function contacts($contractorId)
    {
        return $this->_contactManager->findBy([
            'contractorId' => $contractorId,
        ]);
    }
}