<?php

namespace core\relations;

use Yii;
use common\models\laitovo\Cars;

class Car_CustomCarInstruction
{
    private $_carManager; //todo: заюзать carManager, когда будет
    private $_customCarInstructionManager;

    public function __construct()
    {
        $this->_customCarInstructionManager = Yii::$container->get('core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager');
    }

    public function car($carId)
    {
        return Cars::findOne($carId);
    }

    public function carExists($carId)
    {
        return Cars::findOne($carId) != null;
    }

    public function customCarInstructions($carId)
    {
        return $this->_customCarInstructionManager->findBy([
            'carId' => $carId,
        ]);
    }
}