<?php

namespace core\relations;

use backend\modules\logistics\models\OrderEntry;
use Yii;

class OrderEntry_LostUpn
{
    private $_orderEntryManager; //todo: заюзать orderEntryManager, когда будет
    private $_lostUpnManager;

    public function __construct()
    {
        $this->_lostUpnManager = Yii::$container->get('core\entities\logisticsLostUpn\LostUpnManager');
    }

    public function orderEntry($orderEntryId)
    {
        return OrderEntry::findOne($orderEntryId);
    }

    public function orderEntryExists($orderEntryId)
    {
        return OrderEntry::findOne($orderEntryId) != null;
    }

    public function lostUpns($orderEntryId)
    {
        return $this->_lostUpnManager->findBy(['orderEntryId' => $orderEntryId]);
    }
}