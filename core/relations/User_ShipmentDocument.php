<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_ShipmentDocument
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_shipmentDocumentManager;

    public function __construct()
    {
        $this->_shipmentDocumentManager = Yii::$container->get('core\entities\logisticsShipmentDocument\ShipmentDocumentManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function shipmentDocumentsByAuthor($authorId)
    {
        return $this->_shipmentDocumentManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function shipmentDocumentsByUpdater($updaterId)
    {
        return $this->_shipmentDocumentManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}