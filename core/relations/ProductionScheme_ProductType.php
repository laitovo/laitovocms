<?php

namespace core\relations;

use Yii;
use common\models\laitovo\ProductionScheme;

class ProductionScheme_ProductType
{
    private $_productionSchemeManager; //todo: заюзать productionSchemeManager, когда будет
    private $_productTypeManager;

    public function __construct()
    {
        $this->_productTypeManager = Yii::$container->get('core\entities\laitovoErpProductType\ProductTypeManager');
    }

    public function productionScheme($productionSchemeId)
    {
        return ProductionScheme::findOne($productionSchemeId);
    }

    public function productionSchemeExists($productionSchemeId)
    {
        return ProductionScheme::findOne($productionSchemeId) != null;
    }

    public function productTypes($productionSchemeId)
    {
        return $this->_productTypeManager->findBy([
            'productionSchemeId' => $productionSchemeId,
        ]);
    }
}