<?php

namespace core\relations;

use Yii;

class Brand_InstructionType
{
    private $_instructionTypeManager;
    private $_brandManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_brandManager = Yii::$container->get('core\entities\propBrand\BrandManager');
    }

    public function brand($brandId)
    {
        return $this->_brandManager->findById($brandId);
    }

    public function brandExists($brandId)
    {
        return $this->_brandManager->exists($brandId);
    }

    public function instructionTypes($brandId)
    {
        return $this->_instructionTypeManager->findBy([
            'brandId' => $brandId,
        ]);
    }
}