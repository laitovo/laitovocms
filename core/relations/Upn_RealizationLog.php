<?php

namespace core\relations;

use backend\modules\logistics\models\Naryad;
use Yii;

class Upn_RealizationLog
{
    private $_upnManager; //todo: заюзать upnManager, когда будет
    private $_realizationLogManager;

    public function __construct()
    {
        $this->_realizationLogManager = Yii::$container->get('core\entities\logisticsRealizationLog\RealizationLogManager');
    }

    public function upn($upnId)
    {
        return Naryad::findOne($upnId);
    }

    public function upnExists($upnId)
    {
        return Naryad::findOne($upnId) != null;
    }

    public function realizationLogs($upnId)
    {
        return $this->_realizationLogManager->findBy(['upnId' => $upnId]);
    }
}