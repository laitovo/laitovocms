<?php

namespace core\relations;

use Yii;
use Exception;

class ContractorGroup_ContractorGroup
{
    private $_groupManager;

    public function __construct()
    {
        $this->_groupManager = Yii::$container->get('core\entities\contractorGroup\GroupManager');
    }

    /**
     * Родительская группа нижнего (ближайшего) уровня
     *
     * @param int|null $parentId
     * @return object|null
     */
    public function parent($parentId)
    {
        return $this->_groupManager->findById($parentId);
    }

    /**
     * Проверка существования родительской группы нижнего (ближайшего) уровня
     *
     * @param int|null $parentId
     * @return bool
     */
    public function parentExists($parentId)
    {
        return $this->_groupManager->exists($parentId);
    }

    /**
     * Дочерние группы верхнего (ближайшего) уровня
     *
     * @param int $id
     * @return array
     */
    public function childs($id)
    {
        return $this->_groupManager->findBy([
            'parentGroupId' => $id,
        ]);
    }

    /**
     * Родительская группа верхнего уровня
     *
     * @param int $parentId
     * @param array $idsOfGroupsInChain Идентификаторы всех групп в цепочке.
     * @return object|null
     * @throws Exception Если какая-то группа дважды попала в $idsOfGroupsInChain, значит цепочка замкнута.
     */
    public function topParent($parentId, $idsOfGroupsInChain = [])
    {
        if (in_array($parentId, $idsOfGroupsInChain)) {
            $groupName = $this->_groupManager->getName($this->_groupManager->findById($parentId));
            throw new Exception(Yii::t('yii', 'Обнаружена замкнутая цепочка групп. Группа ' . $groupName . ' оказалась дочерней для самой себя.'));
        }
        $idsOfGroupsInChain[] = $parentId;
        $parent = $this->_groupManager->findById($parentId);
        if (!$parent) {
            return null;
        }

        return $this->topParent($this->_groupManager->getParentGroupId($parent), $idsOfGroupsInChain) ?: $parent;
    }

    /**
     * Дочерние группы всех уровней
     *
     * @param int $id
     * @param array $idsOfGroupsInChain Идентификаторы всех групп в цепочке.
     * @return array
     * @throws Exception Если какая-то группа дважды попала в $idsOfGroupsInChain, значит цепочка замкнута.
     */
    public function allChilds($id, $idsOfGroupsInChain = [])
    {
        if (in_array($id, $idsOfGroupsInChain)) {
            $groupName = $this->_groupManager->getName($this->_groupManager->findById($id));
            throw new Exception(Yii::t('yii', 'Обнаружена замкнутая цепочка групп. Группа ' . $groupName . ' оказалась родительской для самой себя.'));
        }
        $idsOfGroupsInChain[] = $id;
        $allChilds = [];
        foreach ($this->childs($id) as $child) {
            $allChilds = array_merge($allChilds, [$child], $this->allChilds($this->_groupManager->getId($child), $idsOfGroupsInChain));
        }

        return $allChilds;
    }
}