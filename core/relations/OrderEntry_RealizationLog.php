<?php

namespace core\relations;

use backend\modules\logistics\models\OrderEntry;
use Yii;

class OrderEntry_RealizationLog
{
    private $_orderEntryManager; //todo: заюзать orderEntryManager, когда будет
    private $_realizationLogManager;

    public function __construct()
    {
        $this->_realizationLogManager = Yii::$container->get('core\entities\logisticsRealizationLog\RealizationLogManager');
    }

    public function orderEntry($orderEntryId)
    {
        return OrderEntry::findOne($orderEntryId);
    }

    public function orderEntryExists($orderEntryId)
    {
        return OrderEntry::findOne($orderEntryId) != null;
    }

    public function realizationLogs($orderEntryId)
    {
        return $this->_realizationLogManager->findBy(['orderEntryId' => $orderEntryId]);
    }
}