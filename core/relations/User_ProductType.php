<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_ProductType
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_productTypeManager;

    public function __construct()
    {
        $this->_productTypeManager = Yii::$container->get('core\entities\laitovoErpProductType\ProductTypeManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function productTypesByAuthor($authorId)
    {
        return $this->_productTypeManager->findBy([
            'author_id' => $authorId,
        ]);
    }
}