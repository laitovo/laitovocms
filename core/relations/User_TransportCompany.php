<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_TransportCompany
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_transportCompanyManager;

    public function __construct()
    {
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function transportCompaniesByAuthor($authorId)
    {
        return $this->_transportCompanyManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function transportCompaniesByUpdater($updaterId)
    {
        return $this->_transportCompanyManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}