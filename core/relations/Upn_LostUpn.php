<?php

namespace core\relations;

use backend\modules\logistics\models\Naryad;
use Yii;

class Upn_LostUpn
{
    private $_upnManager; //todo: заюзать upnManager, когда будет
    private $_lostUpnManager;

    public function __construct()
    {
        $this->_lostUpnManager = Yii::$container->get('core\entities\logisticsLostUpn\LostUpnManager');
    }

    public function upn($upnId)
    {
        return Naryad::findOne($upnId);
    }

    public function upnExists($upnId)
    {
        return Naryad::findOne($upnId) != null;
    }

    public function lostUpns($upnId)
    {
        return $this->_lostUpnManager->findBy(['upnId' => $upnId]);
    }
}