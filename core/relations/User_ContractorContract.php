<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_ContractorContract
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_contractManager;

    public function __construct()
    {
        $this->_contractManager = Yii::$container->get('core\entities\contractorContract\ContractManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function contractorContractsByAuthor($authorId)
    {
        return $this->_contractManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function contractorContractsByUpdater($updaterId)
    {
        return $this->_contractManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}