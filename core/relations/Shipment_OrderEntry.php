<?php

namespace core\relations;

use backend\modules\logistics\models\OrderEntry;
use Yii;

class Shipment_OrderEntry
{
    private $_shipmentManager;
    private $_orderEntryManager; //todo: заюзать orderEntryManager, когда будет

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function shipment($shipmentId)
    {
        return $this->_shipmentManager->findById($shipmentId);
    }

    public function shipmentExists($shipmentId)
    {
        return $this->_shipmentManager->exists($shipmentId);
    }

    public function orderEntries($shipmentId)
    {
        return OrderEntry::find()->where(['shipment_id' => $shipmentId])->all();
    }

    public function orderEntriesNotDeleted($shipmentId)
    {
        return OrderEntry::find()->where(['and',
            ['shipment_id' => $shipmentId],
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ]
        ])->all();
    }

    public function orderEntriesFromLog($cancelLog)
    {
        $orderEntryIds = json_decode($cancelLog, true)['orderEntryIds'] ?? [];

        return OrderEntry::find()->where(['in', 'id', $orderEntryIds])->all();
    }

    public function orderEntriesFromLogNotDeleted($cancelLog)
    {
        $orderEntryIds = json_decode($cancelLog, true)['orderEntryIds'] ?? [];

        return OrderEntry::find()->where(['and',
            ['in', 'id', $orderEntryIds],
            ['or',
                ['is_deleted' => false],
                ['is_deleted' => null]
            ]
        ])->all();
    }
}