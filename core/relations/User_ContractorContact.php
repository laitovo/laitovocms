<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_ContractorContact
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_contactManager;

    public function __construct()
    {
        $this->_contactManager = Yii::$container->get('core\entities\contractorContact\ContactManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function contractorContactsByAuthor($authorId)
    {
        return $this->_contactManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function contractorContactsByUpdater($updaterId)
    {
        return $this->_contactManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}