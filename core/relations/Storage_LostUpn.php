<?php

namespace core\relations;

use backend\modules\logistics\models\Storage;
use Yii;

class Storage_LostUpn
{
    private $_storageManager; //todo: заюзать storageManager, когда будет
    private $_lostUpnManager;

    public function __construct()
    {
        $this->_lostUpnManager = Yii::$container->get('core\entities\logisticsLostUpn\LostUpnManager');
    }

    public function storage($storageId)
    {
        return Storage::findOne($storageId);
    }

    public function storageExists($storageId)
    {
        return Storage::findOne($storageId) != null;
    }

    public function lostUpns($storageId)
    {
        return $this->_lostUpnManager->findBy(['storageId' => $storageId]);
    }
}