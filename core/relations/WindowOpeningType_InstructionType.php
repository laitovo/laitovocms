<?php

namespace core\relations;

use Yii;

class WindowOpeningType_InstructionType
{
    private $_instructionTypeManager;
    private $_windowOpeningTypeManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_windowOpeningTypeManager = Yii::$container->get('core\entities\propWindowOpeningType\WindowOpeningTypeManager');
    }

    public function windowOpeningType($windowOpeningTypeId)
    {
        return $this->_windowOpeningTypeManager->findById($windowOpeningTypeId);
    }

    public function windowOpeningTypeExists($windowOpeningTypeId)
    {
        return $this->_windowOpeningTypeManager->exists($windowOpeningTypeId);
    }

    public function instructionTypes($windowOpeningTypeId)
    {
        return $this->_instructionTypeManager->findBy([
            'windowOpeningTypeId' => $windowOpeningTypeId,
        ]);
    }
}