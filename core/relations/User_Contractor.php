<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_Contractor
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_contractorManager;

    public function __construct()
    {
        $this->_contractorManager = Yii::$container->get('core\entities\contractor\ContractorManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function contractorsByAuthor($authorId)
    {
        return $this->_contractorManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function contractorsByUpdater($updaterId)
    {
        return $this->_contractorManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}