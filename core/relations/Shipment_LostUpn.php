<?php

namespace core\relations;

use Yii;

class Shipment_LostUpn
{
    private $_shipmentManager;
    private $_lostUpnManager;

    public function __construct()
    {
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_lostUpnManager = Yii::$container->get('core\entities\logisticsLostUpn\LostUpnManager');
    }

    public function shipment($shipmentId)
    {
        return $this->_shipmentManager->findById($shipmentId);
    }

    public function shipmentExists($shipmentId)
    {
        return $this->_shipmentManager->exists($shipmentId);
    }

    public function lostUpns($shipmentId)
    {
        return $this->_lostUpnManager->findBy(['shipmentId' => $shipmentId]);
    }
}