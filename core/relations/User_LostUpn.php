<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_LostUpn
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_lostUpnManager;

    public function __construct()
    {
        $this->_lostUpnManager = Yii::$container->get('core\entities\logisticsLostUpn\LostUpnManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function lostUpnsByAuthor($authorId)
    {
        return $this->_lostUpnManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function lostUpnsByUpdater($updaterId)
    {
        return $this->_lostUpnManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}