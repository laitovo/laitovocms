<?php

namespace core\relations;

use Yii;

class Contractor_Contract
{
    private $_contractorManager;
    private $_contractManager;

    public function __construct()
    {
        $this->_contractorManager = Yii::$container->get('core\entities\contractor\ContractorManager');
        $this->_contractManager = Yii::$container->get('core\entities\contractorContract\ContractManager');
    }

    public function contractor($contractorId)
    {
        return $this->_contractorManager->findById($contractorId);
    }

    public function contractorExists($contractorId)
    {
        return $this->_contractorManager->exists($contractorId);
    }

    public function contracts($contractorId)
    {
        return $this->_contractManager->findBy([
            'contractorId' => $contractorId,
        ]);
    }
}