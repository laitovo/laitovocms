<?php

namespace core\relations;

use Yii;

class ExecutionType_InstructionType
{
    private $_instructionTypeManager;
    private $_executionTypeManager;

    public function __construct()
    {
        $this->_instructionTypeManager = Yii::$container->get('core\entities\laitovoInstructionType\InstructionTypeManager');
        $this->_executionTypeManager = Yii::$container->get('core\entities\propExecutionType\ExecutionTypeManager');
    }

    public function executionType($executionTypeId)
    {
        return $this->_executionTypeManager->findById($executionTypeId);
    }

    public function executionTypeExists($executionTypeId)
    {
        return $this->_executionTypeManager->exists($executionTypeId);
    }

    public function instructionTypes($executionTypeId)
    {
        return $this->_instructionTypeManager->findBy([
            'executionTypeId' => $executionTypeId,
        ]);
    }
}