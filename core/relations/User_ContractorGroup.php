<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_ContractorGroup
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_groupManager;

    public function __construct()
    {
        $this->_groupManager = Yii::$container->get('core\entities\contractorGroup\GroupManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function contractorGroupsByAuthor($authorId)
    {
        return $this->_groupManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function contractorGroupsByUpdater($updaterId)
    {
        return $this->_groupManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}