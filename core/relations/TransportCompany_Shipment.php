<?php

namespace core\relations;

use Yii;

class TransportCompany_Shipment
{
    private $_transportCompanyManager;
    private $_shipmentManager;

    public function __construct()
    {
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');
        $this->_shipmentManager         = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
    }

    public function transportCompany($transportCompanyId)
    {
        return $this->_transportCompanyManager->findById($transportCompanyId);
    }

    public function transportCompanyExists($transportCompanyId)
    {
        return $this->_transportCompanyManager->exists($transportCompanyId);
    }

    public function shipments($transportCompanyId)
    {
        return $this->_shipmentManager->findBy([
            'transportCompanyId' => $transportCompanyId,
        ]);
    }
}