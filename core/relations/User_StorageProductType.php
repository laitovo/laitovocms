<?php

namespace core\relations;

use Yii;
use common\models\User;

class User_StorageProductType
{
    private $_userManager; //todo: заюзать userManager, когда будет
    private $_storageProductTypeManager;

    public function __construct()
    {
        $this->_storageProductTypeManager = Yii::$container->get('core\entities\logisticsStorageProductType\StorageProductTypeManager');
    }

    public function author($authorId)
    {
        return User::findOne($authorId);
    }

    public function authorExists($authorId)
    {
        return User::findOne($authorId) != null;
    }

    public function updater($updaterId)
    {
        return User::findOne($updaterId);
    }

    public function updaterExists($updaterId)
    {
        return User::findOne($updaterId) != null;
    }

    public function storageProductTypesByAuthor($authorId)
    {
        return $this->_storageProductTypeManager->findBy([
            'authorId' => $authorId,
        ]);
    }

    public function storageProductTypesByUpdater($updaterId)
    {
        return $this->_storageProductTypeManager->findBy([
            'updaterId' => $updaterId,
        ]);
    }
}