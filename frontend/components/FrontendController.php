<?php
namespace frontend\components;

use Yii;
use frontend\components\GeoTool;

class FrontendController extends \yii\web\Controller {

    public $region = null;

    public function beforeAction($action) {
        $setLang = \Yii::$app->request->post('site-lang', false);
        if ($setLang !== false) {
            Languages::setCurrent($setLang);

            Yii::$app->getResponse()->redirect(['site/pages', 'type' => 'main']);
            return false;
        }

        Yii::$app->language = Languages::getCurrentCode();
        $urlManager = Yii::$app->getUrlManager();

        $setCity = \Yii::$app->request->post('city_selector', false);
        if ($setCity !== false) {
            $city = \common\models\Region::findOne($setCity['city_id']);
            if (!is_null($city)) {
                $main = GeoTool::_searchCountrieByCity($city);
                $currentCity = [
                    'id' => $city->id,
                    'name' => $city->name,
                    'pid' => $city->parent_id,
                    'main_id' => $main->id,
                    'main_name' => $main->name,
                ];
                GeoTool::setUserCityId($currentCity);
            }
        }
        
        $this->region = GeoTool::getUserCity();

        if (Yii::$app->getRequest()->isAjax === false && $urlManager->alias != false) {
            Yii::$app->name = $urlManager->webSite->name;
            $title = [$urlManager->webSite->title?:\Yii::$app->name];
            $this->view->registerCss($urlManager->webSite->css);
            $this->view->registerJs($urlManager->webSite->js);
            if (!is_null($urlManager->alias->title)) {
                $title[] = $urlManager->alias->title;
            }

            $titleReverse = array_reverse($title);

            $this->view->title = implode(' | ', $titleReverse);
        }
        return parent::beforeAction($action);
    }
    
}
