<?php
namespace frontend\components\url_rules;

class AjaxBasket implements \yii\web\UrlRuleInterface {

    public function createUrl($manager, $route, $params) {
        return false;
    }

    public function parseRequest($manager, $request) {
        $pathInfo = $request->getPathInfo();
        if ($request->isAjax === false || empty($pathInfo) || strpos($pathInfo, 'ajax/basket/') !== 0) {
            return false;
        }
        $parsePath = str_replace('ajax/basket/', '', $pathInfo);
        return ['basket/' . $parsePath, []];
    }
}