<?php
namespace frontend\components\url_rules;

class News implements \yii\web\UrlRuleInterface {
    
    private $_route = 'site/news';

    public function createUrl($manager, $route, $params) {
        if ($this->_route !== $route || !isset($params['id'])) {
            return false;
        }
        $alias = \common\models\website\Alias::find()->where(['news_id' => $params['id']])->one();
        unset($params['id']);
        if (is_null($alias)) {
            return false;
        }
        $url = preg_replace('/(^\/)/', '', $alias->link);
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }
        
        return $url;
    }

    public function parseRequest($manager, $request) {

        if (is_null($manager->news)) {
            return false;
        }

        //Тут проверить тип страницы

        return [$this->_route, []];
    }
}