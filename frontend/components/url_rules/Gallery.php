<?php
namespace frontend\components\url_rules;

class Gallery implements \yii\web\UrlRuleInterface {
    
    private $_route = 'site/gallery';

    public function createUrl($manager, $route, $params) {
        if ($this->_route !== $route || !isset($params['id'])) {
            return false;
        }
        $alias = \common\models\website\Alias::find()->where(['gallery_id' => $params['id']])->one();
        unset($params['id']);
        if (is_null($alias)) {
            return false;
        }
        $url = preg_replace('/(^\/)/', '', $alias->link);
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }
        
        return $url;
    }

    public function parseRequest($manager, $request) {

        if (is_null($manager->gallery)) {
            return false;
        }

        //Тут проверить тип страницы

        return [$this->_route, []];
    }
}