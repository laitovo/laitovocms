<?php
namespace frontend\components\url_rules;

class Robots implements \yii\web\UrlRuleInterface {
    
    private $_route = 'site/robots';

    public function createUrl($manager, $route, $params) {
        if ($route !== $this->_route) {
            return false;
        }
        throw new \yii\web\HttpException(500, 'Not necessary');
    }

    public function parseRequest($manager, $request) {
        if ($request->pathInfo !== 'robots.txt') {
            return false;
        }

        return [$this->_route, []];
    }
}