<?php

namespace frontend\components;

use yii\helpers\Json;

class Languages {

    const LANG_SESSION_NAME = 'user-lang-selected';

    /**
     * @var array массив языков
     */
    private static $list = [];

    /**
     * Преобразует массив язык в ассоциативный
     * @param array $jsonSite - json сайта
     * @return array ассоциативный массив языков, ключами являются коды языка
     */
    private static function parse($jsonSite) {
        $langs = isset($jsonSite['langs'])?$jsonSite['langs']:false;

        $languages = [];
        if ($langs !== false) {
            $langArray = Json::decode($jsonSite['langs']);
            foreach ($langArray as $lang) {
                $languages[$lang['lang']] = $lang;
            }
        }

        return $languages;
    }

    /**
     * Получает ассоциативный массив языков
     * @return array ассоциативный массив языков
     */
    static function getList() {
        if (empty(self::$list)) {
            $site = \Yii::$app->urlManager->webSite;
            $jsonSite = Json::decode($site->json);

            self::$list = self::parse($jsonSite);
        }
        return self::$list;
    }

    /**
     * Получает код текущего языка
     * @return string код текущего языка
     */
    static function getCurrentCode() {
        $lang = self::getCurrent();
        return $lang['lang'];
    }

    /**
     * Получает наименование текущего языка
     * @return string наименование текущего языка
     */
    static function getCurrentName() {
        $lang = self::getCurrent();
        return $lang['name'];
    }

    /**
     * Получает информацию о текущем языке
     * @return array ассициативный массив информации о текущем языке ['lang' => %code%, 'name' => %name%]
     */
    static function getCurrent() {
        $list = self::getList();
        $userLang = \Yii::$app->session->get(self::LANG_SESSION_NAME, false);
        if (($userLang === false || isset($list[$userLang]) === false) && is_null(\Yii::$app->urlManager->alias) === false) {
            $alias = \Yii::$app->urlManager->alias;
            $userLang = $alias->lang;
       }

        if (empty($userLang)) {
            $currentLang = self::getDefault();
            $userLang = $currentLang['lang'];
            self::setCurrent($userLang);
        }
        $currentLang = $list[$userLang];
        return $currentLang;
    }

    /**
     * Получает язык по умолчанию для сайта
     * @return array ассициативный массив информации о текущем языке ['lang' => %code%, 'name' => %name%]
     */
    static function getDefault() {
        $list = self::getList();
        $first = array_shift($list);
        return $first;
    }

    /**
     * Устанавливает язык выбранный пользователем
     * @return boolean успешно ли поменян язык
     */
    static function setCurrent($lang) {
        $list = self::getList();

        if (isset($list[$lang])) {
            \Yii::$app->session->set(self::LANG_SESSION_NAME, $lang);
            return true;
        }

        return false;
    }

}
