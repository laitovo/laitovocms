<?php
namespace frontend\controllers;

use common\models\Region;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class AjaxController extends \frontend\components\FrontendController {

    public function beforeAction($action) {
        if (\Yii::$app->getRequest()->isAjax === false) {
            throw new \yii\web\HttpException('Ajax response only', 403);
        }
        return parent::beforeAction($action);
    }


    public function actionGet_city_list() {
        $countrieId = \Yii::$app->request->post('country_id');
        $country = Region::findOne($countrieId);
        if (is_null($country)) {
            throw new \yii\web\HttpException('Country not found', 500);
        }

        $citysList = Region::find()
            ->where([
                'parent_id' => ArrayHelper::map(Region::findAll([
                    'status' => 1,
                    'parent_id' => $country->id,
                ]), 'id', 'id'),
                'status' => 1,
            ])->all();
        $result = ['success' => true];

        $optionsHtml = '';
        foreach ($citysList as $city) {
            $optionsHtml .= Html::tag('option', $city->name);
        }

        $result['options'] = $optionsHtml;

        return json_encode($result);
    }

    public function actionAuto_selector_mark() {
        $catalogId = \Yii::$app->request->post('catalogId', false);
        $markId = \Yii::$app->request->post('markId', false);
        if ($markId === false || $catalogId === false) {
            throw new \yii\web\HttpException(500, 'Invalid request');
        }
        $mark = \common\models\order\Category::findOne(['id' => $markId, 'parent_id' => 1, 'status' => 1]);
        $catalog = \common\models\website\Catalog::findOne(['id' => $catalogId, 'status' => 1]);

        if (is_null($mark) || is_null($catalog)) {
            throw new \yii\web\HttpException(500, 'Not found mark or catalog');
        }

        return \frontend\widgets\AutoSelector::widget([
            'catalog' => $catalog,
            'mark' => $mark,
        ]);
    }
    
    public function actionAuto_selector_model() {
        $catalogIds = \Yii::$app->request->post('catalogIds', false);
        $markId = \Yii::$app->request->post('markId', false);
        $modelId = \Yii::$app->request->post('modelId', false);
        if ($markId === false || $modelId === false) {
            throw new \yii\web\HttpException(500, 'Invalid request');
        }
        $catalogs = \common\models\website\Catalog::findAll([
            'status' => 1,
            'id' => $catalogIds,
        ]);
        $mark = \common\models\order\Category::findOne(['id' => $markId, 'parent_id' => 1, 'status' => 1]);
        $model = \common\models\order\Category::findOne(['id' => $modelId, 'parent_id' => $mark->id, 'status' => 1]);

        if (is_null($mark) || is_null($model)) {
            throw new \yii\web\HttpException(500, 'Not found mark or catalog');
        }

        $result = [];

        foreach ($catalogs as $catalog) {
            $info = ['id' => $catalog->id];
            $info['garage'] = \frontend\widgets\AutoSelector::widget([
                'catalog' => $catalog,
                'mark' => $mark,
                'model' => $model,
            ]);

            $catalogProductsQuery = \common\models\website\CatalogProduct::find()
                ->from(\common\models\website\CatalogProduct::tableName() . ' cp')
                ->where(['cp.status' => 1, 'catalog_id' => $catalog->id])
                ->orderBy(['sort' => SORT_ASC])
                ->limit(6);

            $products = \common\models\order\Product::find()
                    ->from(\common\models\order\Product::tableName() . ' p')
                    ->join('LEFT JOIN', '{{%product_category}} cat', 'cat.product_id = p.id')
                    ->andWhere(['status' => 1, 'cat.category_id' => $model->id])
                    ->all();

            $catalogProductsQuery
                    ->joinWith('product p')
                    ->andWhere(['p.id' => ArrayHelper::map($products, 'id', 'id')]);

            $info['populars'] = '';
            foreach ($catalogProductsQuery->all() as $product) {
                $info['populars'] .= Html::tag(
                        'div',
                        \frontend\widgets\Product::widget(['catalogProduct' => $product]),
                        ['class' => 'nav-sub-catalog-item']
                );
            }


            $result[] = $info;
        }

        return json_encode($result);
    }

}
