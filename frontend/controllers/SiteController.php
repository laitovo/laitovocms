<?php
namespace frontend\controllers;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class SiteController extends \frontend\components\FrontendController {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionPages() {
        $page = \Yii::$app->urlManager->page;
        $alias = \Yii::$app->urlManager->alias;
        $pageInfo = \common\models\website\PageLang::find()->where(['lang' => $alias->lang, 'page_id' => $page->id])->one();
        if ($page->type === 'main') {
            return $this->render('index', [
                'page' => $page,
                'pageInfo' => $pageInfo,
            ]);
        }
        return $this->render('textpage', [
            'page' => $page,
            'pageInfo' => $pageInfo,
        ]);
    }
    
    public function actionGallery() {
        var_dump(\Yii::$app->urlManager->gallery);
    }

    public function actionNews() {
        var_dump(\Yii::$app->urlManager->news);
    }
    
    public function actionFeed() {
        $alias = \Yii::$app->urlManager->alias;
        $feed = \Yii::$app->urlManager->feed;
        $feedInfo = \common\models\website\FeedLang::find()->where(['lang' => $alias->lang,'feed_id' => $feed->id])->one();
        return $this->render('textpage', [
            'page' => $feed,
            'pageInfo' => $feedInfo,
        ]);
    }
    
    public function actionCatalog() {
        $catalog = \Yii::$app->urlManager->catalog;
        $mark = $model = false;
        $filter = \Yii::$app->request->get('filter', false);
        $currentCategorieId = false;
        if ($filter !== false) {
            if (isset($filter['mark']) && is_numeric($filter['mark'])) {
                if ($mark = \common\models\order\Category::findOne($filter['mark'])) {
                    $currentCategorieId = $mark->id;
                }
            }
            if (isset($filter['model']) && is_numeric($filter['model'])) {
                if ($model = \common\models\order\Category::findOne($filter['model'])) {
                    $currentCategorieId = $model->id;
                }
            }
        }

        $catalogProductsQuery = \common\models\website\CatalogProduct::find()
                ->from(\common\models\website\CatalogProduct::tableName() . ' cp')
                ->where(['cp.status' => 1, 'catalog_id' => $catalog->id])
                ->orderBy(['sort' => SORT_ASC])
                ->limit(6);
        
        if ($currentCategorieId !== false && $model != false) {
            $products = \common\models\order\Product::find()
                    ->from(\common\models\order\Product::tableName() . ' p')
                    ->join('LEFT JOIN', '{{%product_category}} cat', 'cat.product_id = p.id')
                    ->andWhere(['status' => 1, 'cat.category_id' => $currentCategorieId])
                    ->all();

            $catalogProductsQuery
                    ->joinWith('product p')
                    ->andWhere(['p.id' => ArrayHelper::map($products, 'id', 'id')]);
        }

        return $this->render('catalog_categorie', [
            'products' => $catalogProductsQuery->all(),
            'mark' => $mark,
            'model' => $model,
        ]);
    }
    
    public function actionProduct() {

        $catalogProduct = \Yii::$app->urlManager->product;
        if (is_null($catalogProduct)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $catalogProductLang = \common\models\website\CatalogProductLang::findOne([
            'catalog_product_id' => $catalogProduct->id,
            'lang' => \Yii::$app->language,
        ]);
        if (is_null($catalogProductLang)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $product = \common\models\order\Product::findOne([
                'id' => $catalogProduct->product_id,
                'status' => 1,
            ]);
        if (is_null($product)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $categories = $product->getCategories()->indexBy('id')->all();

        $city = \frontend\components\GeoTool::getUserCity();
        $price = \common\models\order\Price::findOne([
            'region_id' => $city['regions'],
            'product_id' => $product->id,
        ]);
        if (is_null($price)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $catalog = $catalogProduct->getCatalog(['catalog.status' => 1])->one();
        if (is_null($catalog)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $catalogLang = $catalog->getLangs(['catalog_lang.lang' => \Yii::$app->language])->one();
        if (is_null($catalogLang)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $images = [];
        if (!empty($catalogLang->image)) {
            $images[] = $catalogLang->image;
        }
        $json =Json::decode($catalogProduct->json);
        if (isset($json['photos'])) {
            $images = array_merge($images, $json['photos']);
        }
        
        $complects = \common\models\order\Product::find()
                ->joinWith(['catalogProducts cp', 'categories cat'])
                ->where([
                    'cp.status' => 1,
                    'cat.id' => array_keys($categories),
                ])
                ->andWhere(['!=', 'product.id', $product->id])
                ->orderBy(['cp.sort' => SORT_ASC])
                ->limit(3)
                ->all();
        
        $complectIds = ArrayHelper::map($complects, 'id', 'id');
        $complectPrices = \common\models\order\Price::find()
                ->indexBy('product_id')
                ->where([
                    'product_id' => $complectIds,
                    'region_id' => $city['regions']
                ])
                ->all();
        $complectCatalogProducts = \common\models\website\CatalogProduct::find()
                ->indexBy('product_id')
                ->where(['product_id' => $complectIds])
                ->all();
        $complectCatalogProductLangs = \common\models\website\CatalogProductLang::find()
                ->indexBy('catalog_product_id')
                ->where([
                    'catalog_product_id' => ArrayHelper::map($complectCatalogProducts, 'id', 'id'),
                    'lang' => \Yii::$app->language
                ])
                ->all();
        $complectImages = [];
        foreach ($complectCatalogProducts as $complectCatalogProduct) {
            $complectImages[$complectCatalogProduct->product_id] = '';
            if (!empty($complectCatalogProductLangs[$complectCatalogProduct->id]->image)) {
                $complectImages[$complectCatalogProduct->product_id] = $complectCatalogProductLangs[$complectCatalogProduct->id]->image;
            } else {
                $jsonInfo = Json::decode($complectCatalogProduct->json);
                if (isset($jsonInfo['photos'])) {
                    $complectImages[$complectCatalogProduct->product_id] = array_shift($jsonInfo['photos']);
                }
            }
        }
        
        return $this->render('catalog_element', [
            'product' => $product,
            'price' => $price,
            'catalogProduct' => $catalogProduct,
            'catalogProductLang' => $catalogProductLang,
            'images' => $images,

            'catalog' => $catalog,
            'catalogLang' => $catalogLang,

            'city' => $city,

            'complects' => $complects,
            'complectPrices' => $complectPrices,
            'complectCatalogProducts' => $complectCatalogProducts,
            'complectCatalogProductLangs' => $complectCatalogProductLangs,
            'complectImages' => $complectImages,
        ]);
    }

    public function actionSearch() {
        $text = \Yii::$app->request->get('text', false);
        if ($text != false) {
            $text = trim(strip_tags($text));
        }

        $this->view->title = \Yii::$app->urlManager->webSite->title?:\Yii::$app->name;
        $this->view->title .= ' | ' . \Yii::t('app', 'Поиск');

        $catalogProductLang = $catalogProduct = [];
        if ($text != false) {
            $catalogProductLang = \common\models\website\CatalogProductLang::find()
                    ->joinWith(['catalogProduct cp'])
                    ->indexBy('catalog_product_id')
                    ->where([
                        'cp.status' => 1,
                        'lang' => \Yii::$app->language,
                    ])
                    ->andFilterWhere(['LIKE', 'name', $text])
                    ->limit(20)
                    ->all();
            $catalogProduct = \common\models\website\CatalogProduct::find()
                    ->indexBy('id')
                    ->joinWith(['langs l'])
                    ->where([
                        'id' => array_keys($catalogProductLang)
                    ])
                    ->orderBy(['l.name' => SORT_ASC])
                    ->all();
        }
        return $this->render('search', [
            'text' => $text,
            'catalogProductLang' => $catalogProductLang,
            'catalogProduct' => $catalogProduct,
        ]);
    }

    public function actionRobots() {
        $site = \Yii::$app->urlManager->webSite;
        $robotsText = $site->robots;
        if (empty($robotsText)) {
            throw new \yii\web\NotFoundHttpException;
        }
        $response = \Yii::$app->getResponse();
        $response->format = \yii\web\Response::FORMAT_RAW;
        $lastModifiedTime = is_null($site->updated_at) === false?$site->updated_at:time();

        $response->getHeaders()->set('Content-Type', 'text/plain');
        $response->getHeaders()->set('Last-Modified', gmdate('D, d M Y H:i:s', $lastModifiedTime) . ' GMT');
        $response->getHeaders()->set('Etag', $lastModifiedTime);
        return $robotsText;
    }
    
}
