<?php

namespace frontend\controllers;
use frontend\widgets\BasketMini;

class BasketController extends \frontend\components\FrontendController {

    public function actionAdd_item() {
        $result = ['success' => true];

        $productIds = \Yii::$app->request->post('productId', false);

        $basket = \frontend\components\Basket::getLast();
        $productIdsArray = (array) $productIds;
        foreach ($productIdsArray as $productId) {
            $basket->addItem($productId);
        }
        $result['success'] = $basket->save();

        $result['basketMini'] = BasketMini::widget(['basket' => $basket]);

        \Yii::$app->getResponse()->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    public function actionRemove_item() {
        $result = ['success' => true];

        $productId = \Yii::$app->request->post('productId', false);

        $basket = \frontend\components\Basket::getLast();
        $basket->deleteItem($productId);
        $result['success'] = $basket->save();

        $result['basketMini'] = BasketMini::widget(['basket' => $basket]);

        \Yii::$app->getResponse()->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

}
