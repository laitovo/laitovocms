<?php
return [
    'Ваш город {city}?' => 'Your city {city}?',
    'Да' => 'Yes',
    'Нет, выбрать другой' => 'No, choose another',
    'Выбор города' => 'Choose city',
    'Выбрать' => 'Select',
];

