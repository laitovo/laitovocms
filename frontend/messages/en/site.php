<?php
return [
    'Подробнее' => 'Read more',
    'Подписка на рассылку' => 'Subscribe to Newsletter',
    'Связаться с нами' => 'Connect with us',
    'Мы в социальных сетях' => 'We are in social networks',
    'Сделано в' => 'Developed by',
    'Подписаться' => 'Subscribe',
    'Помощь автоюриста' => 'Lawyer help',
    'Поиск по сайту' => 'Site search',
    'Обратный звонок' => 'Back call',
    'Популярное' => 'Popular',
    'Выбрать автомобиль' => 'Select car',
    'Поиск' => 'Search',
    'Поиск по "{text}"' => 'Search for "{text}"'
];
