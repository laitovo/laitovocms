<?php

namespace frontend\widgets;

use yii\base\Widget;

class MenuTop extends Widget{

    public function run() {
        $menu = \common\models\website\Menu::find()->where([
            'website_id' => \Yii::$app->urlManager->webSite->id,
            'type' => 'top',
            'status' => 1,
        ])->one();
        if (is_null($menu)) {
            return;
        }
        $menuItems = \common\models\website\Menuitem::find()->where([
            'menu_id' => $menu->id,
            'status' => 1,
        ])->all();
        $menuItemsInfo = \common\models\website\MenuitemLang::find()->where([
            'lang' => \Yii::$app->urlManager->alias->lang,
            'menuitem_id' => \yii\helpers\ArrayHelper::map($menuItems, 'id', 'id'),
        ])->all();
        return $this->render('menu_top', [
            'menus' => $menuItemsInfo,
        ]);
    }

}
