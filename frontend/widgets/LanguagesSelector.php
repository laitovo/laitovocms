<?php

namespace frontend\widgets;

use yii\base\Widget;
use frontend\components\Languages;

class LanguagesSelector extends Widget {

    public function run() {

        $list = Languages::getList();
        $current = Languages::getCurrent();

        return $this->render('languages_selector', [
            'list' => $list,
            'current' => $current,
        ]);
    }

}
