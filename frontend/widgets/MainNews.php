<?php

namespace frontend\widgets;

use yii\base\Widget;
use common\models\website\Feed;
use common\models\website\FeedLang;
use common\models\website\News;
use common\models\website\NewsLang;

use yii\helpers\ArrayHelper;

class MainNews extends Widget {

    public function run() {

        $feeds = Feed::find()
                ->where([
                    'status' => 1,
                    'website_id' => \Yii::$app->urlManager->webSite->id,
                ])
                ->orderBy(['sort' => SORT_ASC])
                ->all();
        if (empty($feeds)) {
            return '';
        }
        $feedIds = ArrayHelper::map($feeds, 'id', 'id');
        $feedsInfo = FeedLang::find()
                ->where([
                    'feed_id' => $feedIds,
                    'lang' => \Yii::$app->language,
                ])
                ->indexBy('feed_id')
                ->all();

        $news = \common\models\website\News::find()
                ->where([
                    'status' => 1,
                    'feed_id' => $feedIds,
                ])
                ->orderBy(['date' => SORT_DESC])
                ->limit(5)
                ->all();
        $newsList = [];
        foreach ($news as $new) {
            $newsList[$new->feed_id][] = $new;
        }
        if (empty($news)) {
            return '';
        }
        $newsInfo = \common\models\website\NewsLang::find()
                ->where([
                    'lang' => \Yii::$app->language,
                    'news_id' => ArrayHelper::map($news, 'id', 'id'),
                ])
                ->indexBy('news_id')
                ->all();
        
        return $this->render('main_news', [
            'feeds' => $feeds,
            'feedsInfo' => $feedsInfo,
            'newsList' => $newsList,
            'newsInfo' => $newsInfo,
        ]);
    }

}
