<?php
namespace frontend\widgets;

use yii\base\Widget;
use common\models\website\Catalog;
use common\models\website\CatalogProduct;
use common\models\website\CatalogLang;
use frontend\components\Languages;

class CatalogMenu extends Widget {

    public function registerSctipts() {
        $js = <<<JS
function loadStep2(catalogId, markId) {
    $.post('/ajax/auto_selector_mark', {markId: markId, catalogId: catalogId}, function(html) {
        $('.js-menu-auto-selector').html(html);
        $('.js-styled-select').not($('.jqselect select, .jqselect')).styler({
            selectPlaceholder: '',
            selectSearch:true
        })
    }, 'html');
}
$('.js-menu-auto-selector').on('change', '.js-styled-select.js-step-1', function() {
    loadStep2($(this).closest('.js-menu-auto-selector').data('catalog-id'), $(this).val());
});
$('.js-menu-auto-selector').on('click', 'a.js-step-1', function() {
    loadStep2($(this).closest('.js-menu-auto-selector').data('catalog-id'), $(this).data('mark-id'));
    return false;
});
function loadStep3(markId, modelId) {
    var catalogIds = [];
    $('.js-menu-auto-selector').each(function() {
        elem = $(this);
        catalogIds.push(elem.data('catalog-id'));
    });
    $.post('/ajax/auto_selector_model', {catalogIds: catalogIds, markId: markId, modelId: modelId}, function(data) {
        $.each(data, function() {
            $('#menu-catalog-' + this.id + ' .js-menu-auto-selector').html(this.garage);
            console.log($('.js-menu-popular-' + this.id), this.populars);
            $('.js-menu-popular-' + this.id).html(this.populars);
        });
    }, 'json');
}
$('.js-menu-auto-selector').on('change', '.js-styled-select.js-step-2', function() {
    elem = $(this);
    loadStep3(elem.data('mark-id'), elem.val());
});
$('.js-menu-auto-selector').on('click', 'a.js-step-2', function() {
    elem = $(this);
    loadStep3(elem.data('mark-id'), elem.data('model-id'));
    return false;
});
JS;
        $this->view->registerJs($js);
    }


    public function run() {
        $this->registerSctipts();
        $catalogs = Catalog::find()
                ->indexBy('id')
                ->where(['website_id' => \Yii::$app->urlManager->webSite->id, 'status' => 1])
                ->orderBy(['sort' => SORT_ASC])
                ->all();
        $catalogsLang = CatalogLang::find()
                ->indexBy('catalog_id')
                ->where(['catalog_id' => array_keys($catalogs), 'lang' => Languages::getCurrentCode()])
                ->all();

        /// TODO получение по параметру
        $catalogProductByCatalog = [];
        foreach ($catalogs as $catalog) {
            $catalogProductByCatalog[$catalog->id] = CatalogProduct::find()
                    ->where(['status' => 1, 'catalog_id' => $catalog->id])
                    ->orderBy(['sort' => SORT_ASC])
                    ->limit(6)
                    ->all();
        }
//        $catalogProducts = CatalogProduct::find()
//                    ->where(['status' => 1])
//                    ->orderBy(['sort' => SORT_ASC])
//                    ->all();
//        $catalogProductByCatalog = [];
//
//        foreach ($catalogProducts as $catalogProduct) {
//            $catalogProductByCatalog[$catalogProduct->catalog_id][] = $catalogProduct;
//        }
        ///

        return $this->render('catalog_menu', [
            'catalogs' => $catalogs,
            'catalogsLang' => $catalogsLang,
            'popularProducts' => $catalogProductByCatalog,
        ]);
    }

}
