<?php

namespace frontend\widgets;

use yii\base\Widget;

class UserMenu extends Widget{

    public function run() {

        return $this->render('user_menu', [
            'isGuest' => \Yii::$app->user->isGuest,
        ]);
    }

}
