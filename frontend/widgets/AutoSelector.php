<?php
namespace frontend\widgets;

use yii\base\Widget;
use common\models\order\Category;
use yii\helpers\Json;

class AutoSelector extends Widget {

    public $catalog = false;
    public $mark = false;
    public $model = false;

    private $step = 1;

    public function init() {
        if ($this->catalog === false) {
            $this->catalog = \Yii::$app->urlManager->catalog;
        }

        if ($this->mark != false) {
            $this->step = 2;
        }

        if ($this->model != false) {
            $this->step = 3;

            if ($this->mark == false) {
                $this->mark = Category::findOne($this->model->parent_id);
            }
        }
    }


    public function run() {
        
        $categories = Category::find()->where(['status' => 1])->orderBy(['sort' => SORT_ASC]);

        $autoInfo = [];
        if ($this->step == 1) {
            $categories->andWhere(['parent_id' => 1]);
        } elseif($this->step == 2) {
            $categories->andWhere(['parent_id' => $this->mark->id]);
        } elseif($this->step == 3) {
            $autoInfo = Json::decode($this->model->json);
        }
        $autoCategories = $categories->all();
        $categorieList = \yii\helpers\ArrayHelper::map($autoCategories, 'id', 'name');
        $categorieJson = \yii\helpers\ArrayHelper::map($autoCategories, 'id', 'json');
        
        return $this->render('auto_selector', [
            'catalog' => $this->catalog,
            'step' => $this->step,
            'mark' => $this->mark,
            'model' => $this->model,
            'categorieList' => $categorieList,
            'categorieJson' => $categorieJson,
            'autoInfo' => $autoInfo,
        ]);
    }

}
