<?php
namespace frontend\widgets;

use yii\base\Widget;
use common\models\Region;
use yii\helpers\ArrayHelper;

class CitySelector extends Widget {

    public function run() {
        $userCity = \frontend\components\GeoTool::getUserCity();

        $showSelector = true;


        if (\Yii::$app->session->get('show-city-selector', null) !== null) {
            $showSelector = false;
        } else {
            \Yii::$app->session->set('show-city-selector', true);
        }

        $citysList = Region::find()
                ->where([
                    'parent_id' => ArrayHelper::map(Region::findAll([
                        'status' => 1,
                        'parent_id' => $userCity['main_id'],
                    ]), 'id', 'id'),
                    'status' => 1,
                ])
                ->all();
        $countries = Region::find()
                ->where(['status' => 1])
                ->andWhere(['IS', 'parent_id', null])
                ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                ->all();
        return $this->render('city_selector', [
            'userCity' => $userCity,
            'showSelector' => $showSelector,
            'countries' => ArrayHelper::map($countries, 'id', 'name'),
            'citysList' => ArrayHelper::map($citysList, 'id', 'name'),
        ]);
    }

}
