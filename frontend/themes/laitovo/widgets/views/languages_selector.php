<?php
use frontend\assets\LaitovoAsset;
?>
<div class="lang drop-hover">
    <div class="lang-pic">
        <img src="<?= LaitovoAsset::imgSrc('ru.jpg') ?>" alt="<?= $current['lang'] ?>"/>
    </div>
    <span class="link-dotted"><?= $current['name'] ?></span>
    <div class="drop drop--w hidden">
        <div class="lang-menu">
            <form action="<?= yii\helpers\Url::to(['site/pages', 'type' => 'main']) ?>" method="POST" id="languages-menu-selector">
                <input type="hidden" name="site-lang" value="<?= $current['lang'] ?>" id="site-lang">
                <?php foreach ($list as $code => $lang) { ?>
                    <?php if ($code == $current['lang']) { ?>
                        <div class="lang-menu-item">
                            <div>
                                <div class="lang-pic">
                                    <img src="<?= LaitovoAsset::imgSrc('ru.jpg') ?>" alt="<?= $code ?>"/>
                                </div>
                                <?= $lang['name'] ?>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="lang-menu-item">
                            <a href="#" data-lang="<?= $code ?>">
                                <div class="lang-pic">
                                    <img src="<?= LaitovoAsset::imgSrc('ru.jpg') ?>" alt="<?= $code ?>"/>
                                </div>
                                <?= $lang['name'] ?>
                            </a>
                        </div>
                    <?php } ?>
                <?php } ?>
            </form>
        </div>
    </div>
</div>