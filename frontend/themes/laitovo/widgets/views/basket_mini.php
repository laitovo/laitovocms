<?php
use yii\helpers\Url;
?>

<a href="<?= Url::to(['site/basket']) ?>" class="basket-num"><?= $count ?></a>
<a href="<?= Url::to(['site/basket']) ?>"><i class="ico-0020-bas"></i></a>
<div class="user-bar-drop user-bar-drop--mod hidden">
    <?php if ($count > 0) { ?>
        <table class="table-basket">
            <tbody>
                <?php foreach ($items as $item) {
                    $url = Url::to(['site/product', 'id' => $catalogProducts[$item->product_id]->id]);
                    ?>
                    <tr>
                        <td class="table-basket-w text-right">
                            <div class="table-basket-pic">
                                <?php if (isset($images[$item->product_id])) { ?>
                                    <a href="<?= $url ?>"><img src="<?= $images[$item->product_id] ?>"/></a>
                                <?php } ?>
                            </div>
                        </td>
                        <td class="table-basket-w1">
                            <div>
                                <a class="table-basket-link" href="<?= $url ?>"><?= $item->name ?></a>
                            </div>
                        </td>
                        <td class="table-basket-w2">
                            <span class="table-basket-price">
                                <?= number_format($item->price * $item->quantity, 0, '', ' ') ?> <?= $basket->getValute() ?>
                            </span>
                        </td>
                        <td><i class="ico-del ico-0005-x js-mini-basket-delete" data-id="<?= $item->product_id ?>"></i></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="basket-total clearfix">
            <div class="basket-total-left"><?= Yii::t('site/basket', 'Итого') ?>: <div class="basket-total-price"><?= number_format($price, 0, '', ' ') ?> <?= $basket->getValute() ?></div></div>
            <div class="basket-total-right">
                <a class="btn btn--blue" href="<?= Url::to(['site/basket']) ?>"><?= Yii::t('site/basket', 'Оформить заказ') ?></a>
            </div>
        </div>
    <?php } else { ?>
        <div class="text-center margin-top-10 margin-bottom-10"><?= Yii::t('site/basket', 'Корзина пуста') ?></div>
    <?php } ?>
</div>