<?php
use yii\helpers\Url;
?>
<div class="user-bar-item">
    <i class="ico-0021-user"></i>
    <div class="user-bar-drop hidden">
        <div class="private">
            <?php if ($isGuest) { ?>
                <div class="private-item">
                    <a class="private-link" href="<?= Url::to(['user/login']) ?>"><?= Yii::t('site/user', 'Войти') ?></a>
                </div>
                <div class="private-item">
                    <a class="private-link" href="<?= Url::to(['user/registration']) ?>"><?= Yii::t('site/user', 'Регистрация') ?></a>
                </div>
            <?php } else { ?>
                <div class="private-item">
                    <a class="private-link" href="<?= Url::to(['user/logout']) ?>"><?= Yii::t('site/user', 'Выйти') ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>