<?php
use yii\helpers\Url;
?>
<div class="info-tabs">
    <div class="info-tabs-menu clearfix">
        <div class="info-tabs-menu-wrapper">
            <?php foreach ($gallerys as $key => $gallery) {
                if (empty($photos[$gallery->id])) {
                    continue;
                }
                ?>
                <span class="info-tabs-item <?= $key == 0?'active':'' ?>">
                    <?= $galleryLang[$gallery->id]->name ?>
                </span>
            <?php } ?>
        </div>
    </div>
    <div class="info-tabs-content clearfix">
        <?php foreach ($gallerys as $key => $gallery) {
            if (empty($photos[$gallery->id])) {
                continue;
            }
            ?>
            <div class="<?= $key == 0?'active':'' ?>">
                <div class="carusel">
                    <div class="carusel-wrapper">
                        <?php foreach ($photos[$gallery->id] as $photo) { ?>
                            <div class="carusel-slide">
                                <a href="<?= Url::to(['site/gallery', 'id' => $gallery->id]) ?>">
                                    <img src="/upload/gallerys/<?= $photoLang[$photo->id]->photo ?>" alt="<?= $photoLang[$photo->id]->name ?>"/>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>