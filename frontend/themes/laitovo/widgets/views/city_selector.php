<?php
use yii\helpers\Html;
?>
<div class="header-city drop-hover">
    <i class="header-city-ico ico-0028-mark"></i>
    <span class="header-city-btn"><?= $userCity['name'] ?></span>
    <div class="drop drop--padding text-center <?= $showSelector === false ? 'hidden' : '' ?>">
        <div class="header-city-head"><?= Yii::t('site/geo', 'Ваш город {city}?', ['city' => $userCity['name']]) ?></div>
        <div class="header-city-btns">
            <span class="btn city-selector-close"><?= Yii::t('site/geo', 'Да') ?></span>
            <span class="btn btn--grey js-open-popup-city"><?= Yii::t('site/geo', 'Нет, выбрать другой') ?></span>
        </div>
    </div>
</div>

<div id="city" class="popup hidden">
    <div class="popup-head"><?= Yii::t('site/geo', 'Выбор города') ?></div>
    <div class="city-menu">
        <div class="city-menu-body">
            <div class="f-row">
                <div class="f-row-item f-left w-33">
                    <ul class="city-list reset">
                        <li class="city-list-item"><a class="city-list-link" href="#"><b>Москва</b></a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#"><b>Санкт-Петербург</b></a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Астрахань</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Барнаул</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Волгоград</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Воронеж</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Екатеринбург</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Ижевск</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Иркутск</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Казань</a></li>
                    </ul>
                </div>
                <div class="f-row-item f-left w-33">
                    <ul class="city-list reset">
                        <li class="city-list-item"><a class="city-list-link" href="#">Калининград</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Краснодар</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Красноярск</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Набережные Челны</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Нижний Новгород</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Новосибирск</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Омск</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Оренбург</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Пермь</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Ростов-на-Дону</a></li>
                    </ul>
                </div>
                <div class="f-row-item f-left w-33">
                    <ul class="city-list reset">
                        <li class="city-list-item"><a class="city-list-link" href="#">Самара</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Саратов</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Ставрополь</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Тольятти</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Тула</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Тюмень</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Ульяновск</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Уфа</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Челябинск</a></li>
                        <li class="city-list-item"><a class="city-list-link" href="#">Ярославль</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="city-menu-form clearfix">
            <form action="" method="POST">
                <div class="city-menu-form-item">
                    <?= Html::dropDownList('city_selector[countrie_id]', $userCity['main_id'], $countries, ['class' => 'js-styled-select', 'id' => 'city-selector-contries']) ?>
                </div>
                <div class="city-menu-form-item">
                    <?= Html::dropDownList('city_selector[city_id]', $userCity['id'], $citysList, ['class' => 'js-styled-select', 'id' => 'city-selector-citys']) ?>
                </div>
                <div class="city-menu-form-item">
                    <button class="btn btn--mod" type="submit"><?= Yii::t('site/geo', 'Выбрать') ?></button>
                </div>
            </form>
        </div>
    </div>
    <span class="popup-close js-popup-close"><i class="ico-0007-x"></i></span>
</div>
