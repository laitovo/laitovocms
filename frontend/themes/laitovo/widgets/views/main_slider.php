<div class="slideshow">
    <div class="slideshow-wrapper">
        <?php foreach ($slides as $slide) { ?>
            <div class="slideshow-slide">
                <img src="/upload/banners/<?= $slidesLang[$slide->id]->image ?>" alt=""/>
                <div class="slideshow-slide-text">
                    <div class="slideshow-slide-table">
                        <div class="slideshow-slide-td">
                            <div class="slideshow-slide-title">
                                <?= $slidesLang[$slide->id]->html ?>
                            </div>
                            <?php if (!empty($slidesLang[$slide->id]->link)) { ?>
                                <a class="btn btn--blue" href="<?= $slidesLang[$slide->id]->link ?>">
                                    <?= Yii::t('site', 'Подробнее') ?>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

