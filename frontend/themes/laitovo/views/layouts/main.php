<?php
use frontend\assets\LaitovoAsset;
use yii\helpers\Html;

LaitovoAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"/><![endif]-->
    <meta name="viewport" content="width=1190"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="wrapper">
    <header id="header">
        <div class="header-center clearfix">
            <div class="header-left">
                <div class="header-logo">
                    <a href="<?= yii\helpers\Url::to(['site/pages', 'type' => 'main']) ?>">
                        <img src="<?= LaitovoAsset::imgSrc('logo.png') ?>" alt=""/>
                    </a>
                </div>
                <?= frontend\widgets\CitySelector::widget() ?>
            </div>
            <div class="header-nav-search">
                <?= frontend\widgets\MenuTop::widget() ?>
                <div class="search">
                    <form action="/search">
                        <input class="search-input" name="text" type="text" placeholder="<?= Yii::t('site', 'Поиск по сайту') ?>" value=""/>
                        <button class="search-btn" type="submit"><i class="ico-0025-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="header-phone">
                <div class="header-phone-item"><span>+7 (499)</span> 703 06-85</div>
                <span class="header-phone-btn js-open-call"><i class="header-phone-btn-ico ico-0024-phone"></i><?= Yii::t('site', 'Обратный звонок') ?></span>
            </div>
            <div class="header-user-bar">
                <?= \frontend\widgets\LanguagesSelector::widget() ?>
                
                <div class="user-bar clearfix">
                    <?= frontend\widgets\UserMenu::widget() ?>

                <div class="user-bar-item" id="user-mini-basket">
                    <?= \frontend\widgets\BasketMini::widget() ?>
                </div>
                </div>
            </div>
            <div class="header-conf">
                <div class="header-conf-btn">
                    <div class="header-conf-btn-pic"><img src="<?= LaitovoAsset::imgSrc('conf-auto.png') ?>" alt=""/></div>
                    <span class="link-dotted js-open-select-auto"><?= Yii::t('site', 'Выбрать автомобиль') ?></span>
                </div>
                <div class="header-conf-body hidden">
                    <div class="header-conf-table">
                        <div class="header-conf-td">
                            <a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-14.png') ?>" alt=""/></a>
                        </div>
                        <div class="header-conf-td">
                             <div class="header-conf-head">Honda Accord VIII</div>
                            <i class="header-conf-ico ico-0023-refresh"></i>
                            <i class="header-conf-ico ico-0022-plus"></i>
                            <a class="header-conf-link" href="#">В гараж</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="nav">
            <div class="nav-center">
                <?= \frontend\widgets\CatalogMenu::widget() ?>
            </div>

        </nav>
    </header><!--header-->
    <div id="content">
        <?= $content ?>
    </div><!--content-->
</div><!--wrapper-->
<footer id="footer">
    <div class="footer-rss">
        <div class="footer-rss-center clearfix">
            <div class="help"><i class="help-ico ico-0013-molot"></i><a class="link-dotted" href="#"><?= Yii::t('site', 'Помощь автоюриста') ?></a></div>
            <form action="#">
                <span class="footer-rss-head"><?= Yii::t('site', 'Подписка на рассылку') ?></span>
                <input class="footer-rss-input input-text" type="text" value=""/>
                <button class="btn btn--big" id="rss-button" type="button"><?= Yii::t('site', 'Подписаться') ?></button>
            </form>
        </div>
    </div>
    <div class="footer-center clearfix">
        <nav class="footer-menu">
            <div class="footer-menu-head">О компании</div>
            <ul class="footer-menu-list reset">
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">О нас</a></li>
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Новости</a></li>
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Отзывы</a></li>
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Контакты</a></li>
            </ul>
        </nav>
        <nav class="footer-menu">
            <div class="footer-menu-head">Каталог</div>
            <ul class="footer-menu-list reset">
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Автошторки</a></li>
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Москитные сетки</a></li>
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Аксессуары</a></li>
            </ul>
        </nav>
        <nav class="footer-menu">
            <div class="footer-menu-head">Информация для покупателей</div>
            <ul class="footer-menu-list reset">
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Как сделать заказ?</a></li>
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Доставка и оплата</a></li>
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Полезная информация</a></li>
                <li class="footer-menu-item"><a class="footer-menu-link" href="#">Советы для покупки</a></li>
            </ul>
        </nav>
        <div class="footer-right">
            <div class="footer-right-head"><?= Yii::t('site', 'Связаться с нами') ?></div>
            <div class="header-phone header-phone--mb">
                <div class="header-phone-item"><span>+7 (499)</span> 703 06-85</div>
                <span class="header-phone-btn js-open-call"><i class="header-phone-btn-ico ico-0024-phone"></i><?= Yii::t('site', 'Обратный звонок') ?></span>
            </div>
            <div class="footer-right-head"><?= Yii::t('site', 'Мы в социальных сетях') ?></div>
            <div class="footer-social">
                <a class="footer-social-item" href="#"><i class="ico-0009-f"></i></a>
                <a class="footer-social-item" href="#"><i class="ico-0011-ok"></i></a>
                <a class="footer-social-item" href="#"><i class="ico-0010-yt"></i></a>
                <a class="footer-social-item" href="#"><i class="ico-0008-inst"></i></a>
            </div>
            <div><img src="<?= LaitovoAsset::imgSrc('likes.jpg') ?>" alt=""/></div>
            <div><img src="<?= LaitovoAsset::imgSrc('likes1.jpg') ?>" alt=""/></div>
        </div>
        <div class="footer-vidget"><img src="<?= LaitovoAsset::imgSrc('vidget.jpg') ?>" alt=""/></div>
        <a class="goggle-play" href="#"><img src="<?= LaitovoAsset::imgSrc('gp.jpg') ?>" alt=""/></a>
    </div>
    <div class="footer-line">
        <div class="footer-line-center clearfix">
            <div class="footer-line-copy"><?= common\models\website\Htmlblock::getBlockHtml('copy') ?></div>
             <a class="aisol" href="http://aisol.ru/"><?= Yii::t('site', 'Сделано в') ?> <img src="<?= LaitovoAsset::imgSrc('aisol.png') ?>"/></a>
            <div class="pay-methods">
                <span class="pay-methods-item"><i class="ico-0003-qiwi"></i></span>
                <span class="pay-methods-item"><i class="ico-0002-yd"></i></span>
                <span class="pay-methods-item"><i class="ico-0001-visa"></i></span>
                <span class="pay-methods-item"><i class="ico-0000-post"></i></span>
            </div>
        </div>
    </div>
</footer><!--footer-->
<div id="call" class="popup hidden">
    <div class="popup-head"><?= Yii::t('site/callback', 'Заказать обратный звонок') ?></div>
    <div class="call-back">
        <div>
            <div class="call-back-item">
                <input class="input-text" type="text" placeholder="<?= Yii::t('site/callback', 'Ваше имя') ?>" value=""/>
            </div>
            <div class="call-back-item">
                <input class="input-text" type="text" placeholder="<?= Yii::t('site/callback', 'Ваш телефон') ?>" value=""/>
            </div>
            <div class="call-back-btns">
                <button class="btn btn--big" id="call-back-button" type="button"><?= Yii::t('site/callback', 'Отправить') ?></button>
            </div>
        </div>
    </div>
    <span class="popup-close js-popup-close"><i class="ico-0007-x"></i></span>
</div>
<div id="select-auto" class="popup hidden">
    <div class="popup-head">Выбор автомобиля</div>
    <div class="select-auto">
        <form action="#">
            <div class="select-auto-item">
                <select class="js-styled-select select-mod w-100">
                    <option value="1">2009</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                </select>
            </div>
            <div class="select-auto-item">
                <select class="js-styled-select select-mod w-100">
                    <option value="1">Honda</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                </select>
            </div>
            <div class="select-auto-item">
                <select class="js-styled-select select-mod w-100">
                    <option value="1">Accord седан</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                </select>
            </div>
             <div class="select-auto-item">
                <select class="js-styled-select select-mod w-100">
                    <option value="1">2.0 (156 л.с.)</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                    <option value="1">Костромская область</option>
                </select>
            </div>
            <div class="select-auto-item">
                <div class="select-auto-table">
                    <div class="select-auto-td">
                        <div class="select-auto-pic"><img src="<?= LaitovoAsset::imgSrc('i.jpg') ?>" alt=""/></div>
                    </div>
                    <div class="select-auto-td">
                        <select class="js-styled-select select-mod w-100">
                            <option value="1">Гараж 1</option>
                            <option value="1">Костромская область</option>
                            <option value="1">Костромская область</option>
                            <option value="1">Костромская область</option>
                            <option value="1">Костромская область</option>
                            <option value="1">Костромская область</option>
                            <option value="1">Костромская область</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="select-auto-item-btns">
                <button class="btn btn--big" type="button">Выбрать</button>
            </div>
        </form>
    </div>
    <span class="popup-close js-popup-close"><i class="ico-0007-x"></i></span>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>