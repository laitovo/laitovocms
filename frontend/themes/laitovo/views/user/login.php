<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model frontend\forms\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="center clearfix">
    <h1><?= Yii::t('site/user', 'Вход') ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'email')->textInput(['class' => 'input-text']) ?>
        <?= $form->field($model, 'password')->passwordInput(['class' => 'input-text']) ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('site/user', 'Войти'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</section>
