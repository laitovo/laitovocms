<?php
use frontend\widgets\Product;
?>
<section class="center clearfix">
    <?php if ($text != false) { ?>
        <h1><?= Yii::t('app', 'Поиск по "{text}"', ['text' => $text]) ?></h1>
        <div class="search-product-wrapper">
            <?php foreach ($catalogProduct as $catalogProductId => $catalogProduct) { ?>
                <?= Product::widget([
                    'catalogProduct' => $catalogProduct,
                    'catalogProductId' => $catalogProductId,
                    'catalogProductLang' => $catalogProductLang[$catalogProductId]
                ]) ?>
            <?php } ?>
        </div>
    <?php } else { ?>
        <h1><?= Yii::t('app', 'Поиск') ?></h1>
        Пустой запрос
    <?php } ?>
</section>
