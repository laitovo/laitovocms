<section class="center clearfix">
    <div class="clearfix">
        <div class="nav-sub-left">
            <?= \frontend\widgets\AutoSelector::widget(['mark' => $mark, 'model' => $model]) ?>
        </div>
        <div class="nav-sub-line" style="height:600px;"></div>
        <div class="nav-sub-right">
            <div class="nav-sub-right-head">
                Самые популярные
                <?php if ($model != false) { ?>
                    для <?= $model->name ?>
                <?php } ?>
            </div>
            <div class="nav-sub-catalog clearfix">
                <?php foreach ($products as $product) { ?>
                    <div class="nav-sub-catalog-item">
                        <?= \frontend\widgets\Product::widget(['catalogProduct' => $product]) ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <?= \frontend\widgets\MainNews::widget() ?>

    <?= \frontend\widgets\MainGallery::widget() ?>
</section>