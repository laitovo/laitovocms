<?php

use frontend\assets\LaitovoAsset;
use yii\helpers\Url;
?>
<section class="center clearfix">
    <div class="breadcrumbs">
        <a href="/">Главная</a>
        <i>/</i>
        <a href="<?= Url::to(['site/catalog', 'id' => $catalog->id]) ?>">
            <?= $catalogLang->name ?>
        </a>
        <i>/</i>
        <span><?= $catalogProductLang->name ?></span>
    </div>
    <div class="clearfix">
        <div class="baner-left">
            <a href="#">
            </a>
        </div>
        <div class="baner-right">
            <div class="title"><h1><?= $catalogProductLang->name ?></h1></div>
            <div class="pro clearfix">
                <div class="pro-left">
                    <div class="pro-pic">
                        <?php foreach ($images as $key => $image) { ?>
                            <div class="<?= $key == 0?'active':'' ?>">
                                <a href="#">
                                    <img src="<?= $image ?>"/>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="pro-pic-min">
                        <div class="pro-pic-min-wrapper">
                            <ul class="reset">
                                <?php foreach ($images as $key => $image) { ?>
                                    <li class="pro-pic-min-item <?= $key == 0?'active':'' ?>">
                                        <img src="<?= $image ?>" alt=""/>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="pro-right">
                    <div class="clearfix">
                        <div class="cart-rating cart-rating--mod">
                            <?php for($i = 0; $i < $catalogProduct->rating; $i++) { ?>
                                <i class="ico-0016-star-orange"></i>
                            <?php } ?>
                            <?php for(; $i < 5; $i++) { ?>
                                <i class="ico-0017-star"></i>
                            <?php } ?>
                        </div>
                        <a class="pro-right-reviews-link" href="#product-feedbacks">Отзывы (0)</a>
                    </div>
                    <div class="pro-text">
                        <?= $catalogProductLang->anons ?>
                    </div>
                    <div class="pro-line"></div>
                    <div class="clearfix">
                        <div class="pro-line-price">
                            <?= number_format($price->price, 0, '.', ' ') ?> <?= $price->currency ?>
                        </div>
                        <div class="pro-btn-wrapper">
                            <button class="pro-btn pro-btn--orange js-basket-add" data-id="<?= $product->id ?>"><i class="pro-btn-ico ico-0014-bas"></i>Купить</button>
                        </div>
                    </div>
                    <div class="pro-line"></div>
                    <div class="pro-social">
                        <span class="pro-social-head">Поделиться в соц. сетях</span>
                        <img src="<?= LaitovoAsset::imgSrc('s.jpg') ?>" alt=""/>
                    </div>
                    <div class="pro-line"></div>
                    <div class="pro-delivery clearfix">
                        <div class="pro-delivery-left">
                            <div class="pro-delivery-title">Стоимость доставки</div>
                            <a><?= $city['name'] ?></a>
                        </div>
                        <div class="pro-delivery-right">
                            <div class="pro-delivery-item">СДЭК, <span>5-9 дней</span> - <b class="orange">300.00 руб</b></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--center-->
<?php if (!empty($complects)) { ?>
    <section class="set">
        <div class="set-center">
            <div class="set-title">Купить комплект</div>
            <div class="set-body clearfix">
                <?php
                $complectPrice = $price->price;
                foreach ($complects as $key => $complect) {
                    $complectPrice += $complectPrices[$complect->id]->price;
                    ?>
                    <div class="set-body-item">
                        <div class="cart">
                            <div class="cart-body cart-body--mod">
                                <div class="cart-pic">
                                    <?php if (!empty($complectImages[$complect->id])) { ?>
                                        <a href="<?= Url::to(['site/product', 'id' => $complectCatalogProducts[$complect->id]->id]) ?>">
                                            <img src="<?= $complectImages[$complect->id] ?>" alt="">
                                        </a>
                                    <?php } ?>
                                </div>
                                <div class="cart-name cart-name--mb">
                                    <a href="<?= Url::to(['site/product', 'id' => $complectCatalogProducts[$complect->id]->id]) ?>">
                                        <?= $complectCatalogProductLangs[$complectCatalogProducts[$complect->id]->id]->name ?>
                                    </a>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">
                                        <?= number_format($complectPrices[$complect->id]->price, 0, '', ' ') ?>
                                        <?= $complectPrices[$complect->id]->currency ?>
                                    </div>
                                </div>
                                <div class="cart-body-text hidden">
                                    <?= $complectCatalogProductLangs[$complectCatalogProducts[$complect->id]->id]->anons ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (count($complects) - 1 != $key) { ?>
                        <div class="set-body-sep">+</div>
                    <?php } ?>
                <?php } ?>
                <div class="set-body-sep">=</div>
                <div class="set-body-item">
                    <div class="cart">
                        <div class="cart-body cart-body--mod cart-body--bg">
                            <div class="cart-price cart-price--big">
                                <?= number_format($complectPrice, 0, '', ' ') ?> <?= $price->currency ?>
                            </div>
                            <button class="pro-btn pro-btn--orange js-basket-add" data-id="<?= json_encode(array_merge(array_keys($complectPrices), [$product->id])) ?>">
                                <i class="pro-btn-ico ico-0014-bas"></i>Купить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<section class="center clearfix">
    <div class="info-tabs" id="product-feedbacks">
        <div class="info-tabs-menu clearfix">
            <div class="info-tabs-menu-wrapper">
                <span class="info-tabs-item active">Описание</span>
                <span class="info-tabs-item">Отзывы</span>
            </div>
        </div>
        <div class="info-tabs-content clearfix">
            <div class="active">
                <div class="deck">
                    <div class="deck-link">
                        <a class="deck-link-item" href="#"><span class="ico-play"></span>Видео установки</a>
                        <a class="deck-link-item" href="#"><span class="ico-eye"></span>Сравнить Laitovo и Chiko</a>
                    </div>
                    <?= $catalogProductLang->content ?>
                </div>
            </div>
            <div>
                <div class="news-anons">
                    
                </div>
            </div>
        </div>
    </div>
    
    <?= frontend\widgets\MainGallery::widget() ?>
</section><!--center-->