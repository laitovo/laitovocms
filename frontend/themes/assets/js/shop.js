$(function() {
    $('.js-basket-add').on('click', function() {
        productId = $(this).data('id');
        
        $.post('/ajax/basket/add_item', {productId: productId}, function(data) {
            if (data.success) {
                $('#user-mini-basket').html(data.basketMini);
                $.growl.notice({title: 'Корзина', message: 'Товар добавлен'});
            } else {
                $.growl.error({title: 'Корзина', message: 'Товар не добавлен'});
            }
        }, 'json').error(function() {
            $.growl.error({title: 'Ошибка', message: 'Ошибка сервера'});
        }); 
    });
    
    $(document).on('click', '.js-mini-basket-delete', function() {
        productId = $(this).data('id');
        $.post('/ajax/basket/remove_item', {productId: productId}, function(data) {
            if (data.success) {
                $('#user-mini-basket').html(data.basketMini);
                $.growl.notice({title: 'Корзина', message: 'Товар удален'});
            } else {
                $.growl.error({title: 'Корзина', message: 'Товар не удален'});
            }
        }, 'json').error(function() {
            $.growl.error({title: 'Ошибка', message: 'Ошибка сервера'});
        }); 
    });
});