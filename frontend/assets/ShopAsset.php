<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class ShopAsset extends AssetBundle {
    
    public $baseUrl = '@frontend/themes/assets';
    public $sourcePath = '@frontend/themes/assets';
    
    public $css = [
        'css/jquery.growl.css'
    ];
    public $js = [
        'js/shop.js',
        'js/jquery.growl.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init() {
        parent::init();

        if (YII_DEBUG && !\Yii::$app->request->isPjax) {
            $this->publishOptions['forceCopy'] = true;
        }
    }
}
