<?php

namespace api\controllers;

use common\models\User;
use core\logic\ShipmentPositionsData;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\base\Module;
use yii\web\NotFoundHttpException;

class RealizationController extends BaseApiController
{
    private $_shipmentManager;

    public function __construct($id, Module $module, array $config = [])
    {
        Yii::$app->user->identity = User::findOne(21);

        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');

        parent::__construct($id, $module, $config);
    }

    /**
     * Функция возвращает список отгрузок для проведения реализаций
     *
     * @return array
     */
    public function actionFindShipments()
    {
        $this->_checkAccessToken(Yii::$app->request->get('service_token'));

        $rows = [];
        foreach ($this->_shipmentManager->findShipmentsCanBeAccounted() as $shipment) {
            $id = $this->_shipmentManager->getId($shipment);
            $date = $this->_shipmentManager->getShippedAt($shipment) ?: $this->_shipmentManager->getShipmentDate($shipment);
            $deliveryPrice = $this->_shipmentManager->getDeliveryPrice($shipment);
            $ofiicialNumber = $this->_shipmentManager->getOfficialNumber($shipment);
            $ofiicialNumberYear = $this->_shipmentManager->getOfficialNumberYear($shipment);
            $isCashOnDelivery = $this->_shipmentManager->getIsCashOnDelivery($shipment);

            if (!$date) {
                continue;
            }

            $row = [
                'id'                 => $id,
                'date'               => $date,
                'deliveryPrice'      => $deliveryPrice,
                'bonus'              => ShipmentPositionsData::getSumBonus($shipment),
                'isNeedTTN'          => false,
                'ofNum'              => $ofiicialNumber,
                'ofNumYear'          => $ofiicialNumberYear,
                'senderId'           => null,
                'receiverId'         => null,
                'payerId'            => null,
                'entries'            => [],
            ];

            $order = null;
            $entries = [];
            $orderEntries = $this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment);
            foreach ($orderEntries as $orderEntry) {
                $title      = $orderEntry->name;
                $article    = $orderEntry->article;
                $price      = $orderEntry->price ?: 0;
                $orderId    = ArrayHelper::getValue($orderEntry, 'order.source_innumber');

                if (!$order) {
                    $order = $orderEntry->order;
                }

                $key = $article . '_' . $price;
                if (isset($entries[$key])) {
                    $entries[$key]['count'] ++;
                } else {
                    $entries[$key] = [
                        'title'      => $title,
                        'article'    => $article,
                        'price'      => $price,
                        'nds'        => 0,
                        'orderId'    => $orderId,
                        'count'      => 1,
                    ];
                }
            }

            $row['entries'] = array_values($entries);

            if ($order) {
                $row['senderId']   = $order->sender_id;
                $row['receiverId'] = $order->receiver_id;
                $row['payerId']    = $order->payer_id;
                $row['isNeedTTN']  = $order->is_need_ttn;
                $row['isCOD']      = $isCashOnDelivery ?: $order->isCOD;
            }

            $rows[] = $row;
            if (count($rows) > 500){
                break;
            }
        }

        return $rows;
    }

    /**
     * Функция принимает уведомление о проведённой реализации
     *
     * @return int
     */
    public function actionAccountShipment()
    {
        $this->_checkAccessToken(Yii::$app->request->get('service_token'));

        $shipment = $this->_findShipment(Yii::$app->request->get('id'));
        if (!$this->_shipmentManager->account($shipment)) {
            return 0;
        }

        return 1;
    }

    private function _checkAccessToken($token)
    {
        if (!in_array($token, Yii::$app->params['serviceTokens'])) {
            throw new ForbiddenHttpException("incorrect auth token of service");
        }
    }

    private function _findShipment($id)
    {
        if (!($shipment = $this->_shipmentManager->findById($id))) {
            throw new NotFoundHttpException("shipment not found");
        }

        return $shipment;
    }
}
