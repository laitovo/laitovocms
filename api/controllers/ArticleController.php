<?php

namespace api\controllers;

use core\models\article\Article;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Контроллер отдает всю информацию по артикулам
 *
 * Class ArticleController
 * @package api\controllers
 */
class ArticleController extends BaseApiController
{

    /**
     * Функция отдает статистику по продажам для артикулов
     *
     * @return array
     * @throws ForbiddenHttpException
     */
    public function actionGetStatisticsForArticles()
    {
        $this->_checkAccessToken(Yii::$app->request->get('token'));

        $articles = Yii::$app->request->getBodyParam('articles');

        if (empty($articles)) {
            throw new \DomainException('Передан пустой массив артикулов!!!');
        }
        $response = [];

        foreach ($articles as $article) {
            $articleObj = new Article($article);
            $response[$article] = $articleObj->saleSixMonth;
        }

        return $response;
    }


    /**
     * @param $token
     * @throws ForbiddenHttpException
     */
    private function _checkAccessToken($token)
    {
        if (!in_array($token, Yii::$app->params['serviceTokens'])) {
            throw new ForbiddenHttpException("incorrect auth token of service");
        }
    }
}
