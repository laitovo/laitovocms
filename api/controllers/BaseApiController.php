<?php

namespace api\controllers;

use common\models\User;
use Yii;
use yii\rest\Controller;
use yii\base\Module;

/**
 * Базовый контроллер API
 */
class BaseApiController extends Controller
{
    public function __construct($id, Module $module, array $config = [])
    {
        Yii::$app->user->identity = User::findOne(21);

        parent::__construct($id, $module, $config);
    }
}
