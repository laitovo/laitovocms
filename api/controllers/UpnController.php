<?php

namespace api\controllers;

use backend\helpers\ArticleHelper;
use backend\helpers\BarcodeHelper;
use backend\modules\logistics\models\Naryad;
use core\models\article\Article;
use core\models\europeInvoice\EuropeInvoiceEntry;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Контроллер отдает всю информацию по upn
 *
 * Class UpnController
 * @package api\controllers
 */
class UpnController extends BaseApiController
{
    /**
     * Действие возвращает информацию по UPN по переданному баркоду
     *
     * @param $barcode
     * @return array|null|\yii\db\ActiveRecord
     * @throws ForbiddenHttpException
     */
    public function actionViewByBarcode($barcode)
    {
        $this->_checkAccessToken(Yii::$app->request->get('token'));

        $upn = Naryad::find()->where(['barcode' => $barcode])->one();
        if (!$upn) return [];

        $res = $upn->toArray();
        $res['order'] = $upn->orderEntry->order->source_innumber ?? null;

        return $res;
    }

    /**
     * Добалвение папы UPN
     *
     * @return array|null
     * @throws \yii\db\Exception
     */
    public function actionAddParentUpn()
    {
        $elements = Yii::$app->request->getBodyParam('elements');

        if (count($elements) < 2)
            throw new \DomainException('Нельзя скомплектовать 1 upn');

        $upns = Naryad::find()->where(['in','id',$elements])->all();

        if (!$upns || count($upns) !== count($elements))
            throw new \DomainException('UPNы не найдены в базе!!!');


        //Проверочки.
        //1. Проверочка - upnы не могут быть скомплектованы в один комплект так как разные машины.
        /**
         * @var $upn Naryad
         */
        $cars = [];
        foreach ($upns as $upn) {
            if ($upn->isParent) {
                throw new \DomainException('Невозможно создать папу UPN для папы UPN');
            }

            $article = new Article($upn->article);
            if (empty($cars))
                $cars = $article->carArticlesWithAnalogs;

            if (!in_array($article->carArticle,$cars)) {
                throw new \DomainException('Элементы принадлежат разным автомобилям. Их нельзя скомплектовать в комплект!!!');
            }
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $parentUpn = new Naryad();
            $parentUpn->team_id   = 1;
            $parentUpn->source_id = 2;
            $parentUpn->isParent  = true;

            if (!$parentUpn->save()) {
                $transaction->rollBack();
                return null;
            }

            foreach ($upns as $upn) {
                /** @var $upn Naryad */
                $upn->pid = $parentUpn->id;
                if (!$upn->save()) {
                    $transaction->rollBack();
                    return null;
                };
            }

            $transaction->commit();
            return ['upn' => $parentUpn->id, 'barcode' => $parentUpn->barcode];

        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw new \DomainException($exception->getMessage());
        }
    }

    /**
     * Действие возвращает информацию по UPN по переданному баркоду или артикулу.
     * Если был передан артикул, пытаемся создать upn
     *
     * @param null $barcode
     * @param null $article
     * @return array
     * @throws ForbiddenHttpException
     */
    public function actionGetUpnData($barcode = null, $article = null)
    {
        $this->_checkAccessToken(Yii::$app->request->get('token'));

        if (!$barcode && !$article)
            throw new \DomainException('Передайте хотя бы один из параметров (Штрихкод или артикул)');

        $upn = null;

        if ($barcode) {
            $barcode = BarcodeHelper::toLatin($barcode);
            /**
             * @var $upn Naryad
             */
            $upn = Naryad::find()->where(['barcode' => $barcode])->one();
            if (!$upn)
                throw new \DomainException('Upn по переданному штрихоку не найден. Штрихкод, который был передан : ' . $barcode);

        }elseif ($article) {
            $article = mb_strtoupper($article);
            if (!ArticleHelper::validate($article) && !ArticleHelper::validate( ($article = ArticleHelper::createArticleFromBarcode($article)) ))
                throw new \DomainException('Артикул, который был передан, не прошел верификацию, он не верен. : ' . $article);
            $upn = new Naryad();
            $upn->article = $article;
            $upn->team_id = 1;
            $upn->comment = "При заведении удаленного склада";
            $upn->source_id = 4;
            if (!$upn->save())
                throw new \DomainException('Не удалось сохранить новый UPN для артикула : ' . $article);
        }

        if (!$upn || !($upn instanceof Naryad))
            throw new \DomainException('Не удалось найти или сгенерировать новый UPN');

        $res = [
            'upn'     => $upn->id,
            'article' => $upn->article,
            'barcode' => $upn->barcode
        ];

        return $res;
    }

    /**
     * @return array|EuropeInvoiceEntry[]
     */
    public function actionUpnPrice()
    {
        $elements = Yii::$app->request->getBodyParam('upns');
        return EuropeInvoiceEntry::find()->where(['upnId' => $elements])->select(['upnId','price'])->asArray()->all();
    }

    /**
     * @param $token
     * @throws ForbiddenHttpException
     */
    private function _checkAccessToken($token)
    {
        if (!in_array($token, Yii::$app->params['serviceTokens'])) {
            throw new ForbiddenHttpException("incorrect auth token of service");
        }
    }
}
