<?php

namespace api\controllers;

use backend\helpers\ArticleHelper;
use Yii;
use yii\db\Query;
use yii\web\ForbiddenHttpException;

/**
 * Контроллер отдает информацию по аналогам
 *
 * Class AnalogController
 * @package api\controllers
 */
class AnalogController extends BaseApiController
{
    /**
     * Действие возвращает информацию по аналогам артикула
     *
     * @param $article
     * @return array
     * @throws ForbiddenHttpException
     */
    public function actionGetArticleAnalogs($article)
    {
        $this->_checkAccessToken(Yii::$app->request->get('token'));

        $article = mb_strtoupper($article);

        $articlesArray = [];
        $articlesArray[] = $article;

        if (!ArticleHelper::validate($article))
//            throw new \DomainException('Передан некоректный артикул. Он непрошел валидацию.' . $article);
            return [];
        
        //Если доп продукция
        if (!($window=ArticleHelper::getWindow($article)))
            return [];

        if (!($carArticle=ArticleHelper::getCarArticle($article)))
            throw new \DomainException('Не удалось извлечь артикул автомобиля из переданного артикула');

        $analogArticles = (new Query())
            ->select('analogArticle')
            ->from('tmp_analogs')
            ->where('carArticle = :carArticle')
            ->andWhere('window = :window')
            ->andWhere('analogArticle is not NULL')
            ->addParams([':carArticle' => $carArticle])
            ->addParams([':window' => $window])
            ->column();

        if (empty($analogArticles))
            return [];

        foreach ($analogArticles as $analogArticle) {
            $newArticle = ArticleHelper::rebuildArticleForAnalog($article,$analogArticle);
            if ($newArticle)
                $articlesArray[] = $newArticle;
        }

        return count($articlesArray)>1 ? $articlesArray : [];
    }

    /**
     * @param $token
     * @throws ForbiddenHttpException
     */
    private function _checkAccessToken($token)
    {
        if (!in_array($token, Yii::$app->params['serviceTokens'])) {
            throw new ForbiddenHttpException("incorrect auth token of service");
        }
    }
}
