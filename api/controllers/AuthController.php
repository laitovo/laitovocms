<?php

namespace api\controllers;

use common\models\User;
use Yii;
use yii\web\ForbiddenHttpException;

class AuthController extends BaseApiController
{
    public function actionFindIdentity()
    {
        $this->_checkAccessToken(Yii::$app->request->get('service_token'));
        $user = User::findIdentity(Yii::$app->request->get('id'));

        return ['user' => $user];
    }

    private function _checkAccessToken($token)
    {
        if (!in_array($token, Yii::$app->params['serviceTokens'])) {
            throw new ForbiddenHttpException("Incorrect auth token of service");
        }
    }
}
