<?php

namespace api\controllers;

use core\models\europeInvoice\EuropeInvoice;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Контроллер отдает информацию по инвойсам
 *
 * Class InvoiceController
 * @package api\controllers
 */
class InvoiceController extends BaseApiController
{
    /**
     * Действие возвращает информацию по аналогам артикула
     *
     * @param $article
     * @return array
     * @throws ForbiddenHttpException
     */
    public function actionList()
    {
        $this->_checkAccessToken(Yii::$app->request->get('token'));

        $invoices = EuropeInvoice::find()->notLoad()->onlyForGermany()->all();

        $result = [];

        foreach ($invoices as $invoice) {
            $item = [];
            $item['id'] = $invoice->id;
            $item['date'] = $invoice->createdAt;
            $item['isLoad'] = $invoice->isLoad;
            $item['elements'] = $invoice->europeInvoiceEntries;
            $result[] = $item;
        }

        return $result;
    }

    /**
     * Действие возвращает информацию по аналогам артикула
     *
     * @param $article
     * @return array
     * @throws ForbiddenHttpException
     */
    public function actionMark($id)
    {
        $this->_checkAccessToken(Yii::$app->request->get('token'));

        $invoice = EuropeInvoice::findOne($id);
        if (!$invoice) {
            throw new \DomainException('Инвойс с номером' . $id . ' не найден!' );
        }

        $invoice->isLoad = true;
        if (!$invoice->save()) {
            throw new \DomainException('Инвойс с номером' . $id . ' не удалось сохранить!' );
        }

        return [$id => true];
    }

    /**
     * @param $token
     * @throws ForbiddenHttpException
     */
    private function _checkAccessToken($token)
    {
        if (!in_array($token, Yii::$app->params['serviceTokens'])) {
            throw new ForbiddenHttpException("incorrect auth token of service");
        }
    }
}
