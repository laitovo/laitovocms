<?php
return [
    'apiurl'       => $_ENV['HOST_OBMEN_BIZZONE'],
    'access_token' => $_ENV['OBMEN_BIZZONE_TOKEN'],
    'obmen_ftp' => [
        'host'     => $_ENV['AF_FTP_HOST'],
        'port'     => $_ENV['AF_FTP_PORT'],
        'login'    => $_ENV['AF_FTP_LOGIN'],
        'password' => $_ENV['AF_FTP_PASSWORD'],
    ],
    'obmen_clients' => [
        'host'     => $_ENV['DB_CLIENTS_HOST'],
        'port'     => $_ENV['DB_CLIENTS_PORT'],
        'dbname'   => $_ENV['DB_CLIENTS_NAME'],
        'username' => $_ENV['DB_CLIENTS_USER'],
        'password' => $_ENV['DB_CLIENTS_PASSWD'],
        'charset'  => $_ENV['DB_CLIENTS_CHARSET'],
    ],
    'obmen_laitovo_site' => [
        'host'     => $_ENV['DB_LAITOVO_HOST'],
        'port'     => $_ENV['DB_LAITOVO_PORT'],
        'dbname'   => $_ENV['DB_LAITOVO_NAME'],
        'username' => $_ENV['DB_LAITOVO_USER'],
        'password' => $_ENV['DB_LAITOVO_PASSWD'],
        'charset'  => $_ENV['DB_LAITOVO_CHARSET'],
    ],
];
