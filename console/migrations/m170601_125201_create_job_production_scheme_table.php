<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_production_scheme`.
 */
class m170601_125201_create_job_production_scheme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%job_production_scheme}}', [
            'id' => $this->primaryKey(),
            'production_scheme_id' => $this->integer()->comment('Производственная схема'),
            'erp_job_scheme_id' => $this->integer()->comment('Маршрут'),
            
        ]);
        
        $this->addForeignKey('fk_job_production_scheme_type_production_scheme_id', '{{%job_production_scheme}}', 'production_scheme_id', '{{%production_scheme}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_job_production_scheme_type_erp_job_scheme_id', '{{%job_production_scheme}}', 'erp_job_scheme_id', '{{%laitovo_erp_job_scheme}}', 'id', 'SET NULL', 'CASCADE');
       }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_job_production_scheme_type_production_scheme_id', '{{%job_production_scheme}}');
        $this->dropForeignKey('fk_job_production_scheme_type_erp_job_scheme_id', '{{%job_production_scheme}}');
        $this->dropTable('job_production_scheme');
    }
}
