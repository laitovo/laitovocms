<?php

use yii\db\Migration;

class m190816_065849_add_contractorId_return_journal extends Migration
{
    public function up()
    {
        $this->addColumn('return_journal', 'contractorId', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('return_journal', 'contractorId');
    }
}
