<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_manning`.
 */
class m170821_112857_create_laitovo_erp_manning_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_erp_manning', [
            'id' => $this->primaryKey(),
            'location_id'=>$this->integer()->comment('Участок к которому закреплен сотрудник'),
            'user_id'=>$this->integer()->comment('Сотрудник'),
            'position_id'=>$this->integer()->comment('Должность'),
            'salary_one'=>$this->double()->comment('Оклад 1'),
            'salary_two'=>$this->double()->comment('Оклад 2'),
            'car_compensation'=>$this->double()->comment('Компенсация автомобиля'),
            'surcharge'=>$this->double()->comment('Сезонная доплата'),
            'qualification'=>$this->double()->comment('Квалификация'),
            'premium'=>$this->double()->comment('Премия'),
            'kpi_one'=>$this->double()->comment('KPI-1'),
            'kpi_two'=>$this->double()->comment('KPI-2'),
            'kpi_three'=>$this->double()->comment('KPI-3'),
            ]);

        $this->addForeignKey(
            'fk-laitovo_erp_manning-user_id',
            '{{%laitovo_erp_manning}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_manning-location_id',
            '{{%laitovo_erp_manning}}',
            'location_id',
            '{{%laitovo_erp_location}}',
            'id',
            'CASCADE'
        );


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_manning-user_id', '{{%laitovo_erp_manning}}');

        $this->dropForeignKey('fk-laitovo_erp_manning-location_id', '{{%laitovo_erp_manning}}');

        $this->dropTable('laitovo_erp_manning');
    }
}
