<?php

use yii\db\Migration;

class m170619_064619_create_laitovo_erp_additional_work_table extends Migration
{
    public function up()
    {
            $this->createTable('laitovo_erp_additional_work', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Работник'),
            'location_id' => $this->integer()->comment('Участок'),
            'job_type' => $this->integer()->comment('Вид работ'),
            'job_count' => $this->integer()->defaultValue(1)->comment('Количество работ'),
            'job_price' => $this->double()->comment('Стоимость работ'),
            'status' => $this->boolean()->comment('Флаг выполнения'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'finish_at' => $this->integer()->comment('Дата выполнения работ'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
        ]);
            
        $this->addForeignKey(
            'fk-laitovo_erp_additional_work-user_id',
            '{{%laitovo_erp_additional_work}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-laitovo_erp_additional_work-updater_id',
            '{{%laitovo_erp_additional_work}}',
            'updater_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
       
        $this->addForeignKey(
            'fk-laitovo_erp_additional_work-location_id',
            '{{%laitovo_erp_additional_work}}',
            'location_id',
            '{{%laitovo_erp_location}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_additional_work-author_id',
            '{{%laitovo_erp_additional_work}}',
            'author_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-laitovo_erp_additional_work-job_type',
            '{{%laitovo_erp_additional_work}}',
            'job_type',
            '{{%laitovo_erp_job_type}}',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_additional_work-user_id', '{{%laitovo_erp_additional_work}}');
       
        $this->dropForeignKey('fk-laitovo_erp_additional_work-updater_id', '{{%laitovo_erp_additional_work}}');
        
        $this->dropForeignKey('fk-laitovo_erp_additional_work-author_id', '{{%laitovo_erp_additional_work}}');
        
        $this->dropForeignKey('fk-laitovo_erp_additional_work-location_id', '{{%laitovo_erp_additional_work}}');
       
        $this->dropForeignKey('fk-laitovo_erp_additional_work-job_type', '{{%laitovo_erp_additional_work}}');
       
        $this->dropTable('laitovo_erp_additional_work');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
