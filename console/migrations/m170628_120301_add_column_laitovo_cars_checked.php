<?php

use yii\db\Migration;

class m170628_120301_add_column_laitovo_cars_checked extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_cars}}', 'checked', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_cars}}', 'checked');
    }
}
