<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_product_group`.
 */
class m170530_124011_create_laitovo_erp_product_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_product_group}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'location_id' => $this->integer(),
            //columns
            'created_at' => $this->integer(),
            'author_id' => $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_product_group_location_id', '{{%laitovo_erp_product_group}}', 'location_id', '{{%laitovo_erp_location}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_laitovo_erp_product_group_author_id', '{{%laitovo_erp_product_group}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_laitovo_erp_product_group_updater_id', '{{%laitovo_erp_product_group}}', 'updater_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_product_group_updater_id', '{{%laitovo_erp_product_group}}');
        $this->dropForeignKey('fk_laitovo_erp_product_group_author_id', '{{%laitovo_erp_product_group}}');
        $this->dropForeignKey('fk_laitovo_erp_product_group_location_id', '{{%laitovo_erp_product_group}}');

        $this->dropTable('{{%laitovo_erp_product_group}}');
    }
}
