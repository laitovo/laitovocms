<?php

use yii\db\Migration;

class m190225_065659_logistics_autocomplectation_logs_table extends Migration
{
    protected $table = '{{%logistics_autocompletion_logs}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table,
            [
                'id'         => $this->primaryKey(),
                'orderNumber'=> $this->integer()->notNull(),
                'article'    => $this->char(20)->notNull(),
                'requested'  => $this->integer()->notNull(),
                'inStorage'  => $this->integer()->notNull(),
                'result'     => $this->integer(),
                'createdAt'  => $this->integer()->notNull(),
                'status'     => $this->boolean()->defaultValue(0),
                'blocked'    => $this->boolean()->defaultValue(0),
                'indexGroup' => $this->smallInteger(),
            ],
            $this->tableOptions);
    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
