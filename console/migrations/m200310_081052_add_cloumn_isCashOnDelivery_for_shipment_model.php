<?php

use yii\db\Migration;

class m200310_081052_add_cloumn_isCashOnDelivery_for_shipment_model extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_shipment}}', 'isCashOnDelivery', $this->boolean()->defaultValue(false)
            ->comment('Явная пометка о том, что оплата за заказ производится наложенным платежом'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_shipment}}', 'isCashOnDelivery');
    }
}
