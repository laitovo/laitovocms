<?php

use yii\db\Migration;

class m170608_083803_add_column_workplace_type_location_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_location_workplace_type}}', 'location_id', $this->integer());

        $this->addForeignKey('fk_laitovo_erp_location_workplace_type_location_id',
            '{{%laitovo_erp_location_workplace_type}}', 'location_id',
            '{{%laitovo_erp_location}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_type_location_id', '{{%laitovo_erp_location_workplace_type}}');
        $this->dropColumn('{{%laitovo_erp_location_workplace_type}}', 'location_id');
    }
}
