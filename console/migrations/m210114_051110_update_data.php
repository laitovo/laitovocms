<?php

use yii\db\Migration;

class m210114_051110_update_data extends Migration
{

    public function safeUp()
    {
        $this->alterColumn('laitovo_erp_naryad','sort',$this->integer()->defaultValue(6));
        echo 'Значения срочности по умолчанию для таблицы нарядов изменена на 6' . PHP_EOL;

        $this->alterColumn('laitovo_erp_order','sort',$this->integer()->defaultValue(6));
        echo 'Значения срочности по умолчанию для таблицы заказов изменена на 6' . PHP_EOL;

        $connection = \Yii::$app->db;
        $connection->createCommand()->update('laitovo_erp_naryad',['sort' => 7],['sort' => 5])->execute();
        $connection->createCommand()->update('laitovo_erp_naryad',['sort' => 6],['sort' => 4])->execute();
        $connection->createCommand()->update('laitovo_erp_naryad',['sort' => 4],['sort' => 3])->execute();

        $connection->createCommand()->update('laitovo_erp_order',['sort' => 7],['sort' => 5])->execute();
        $connection->createCommand()->update('laitovo_erp_order',['sort' => 6],['sort' => 4])->execute();
        $connection->createCommand()->update('laitovo_erp_order',['sort' => 4],['sort' => 3])->execute();

        echo 'Приоритеты успешно обновлены' . PHP_EOL;
    }

    public function safeDown()
    {
        $connection = \Yii::$app->db;
        $connection->createCommand()->update('laitovo_erp_naryad',['sort' => 5],['sort' => 7])->execute();
        $connection->createCommand()->update('laitovo_erp_naryad',['sort' => 3],['sort' => 4])->execute();
        $connection->createCommand()->update('laitovo_erp_naryad',['sort' => 4],['sort' => 6])->execute();

        $connection->createCommand()->update('laitovo_erp_order',['sort' => 5],['sort' => 7])->execute();
        $connection->createCommand()->update('laitovo_erp_order',['sort' => 3],['sort' => 4])->execute();
        $connection->createCommand()->update('laitovo_erp_order',['sort' => 4],['sort' => 6])->execute();

        echo 'Приоритеты восттановлены в прежнее положение' . PHP_EOL;

        $this->alterColumn('laitovo_erp_naryad','sort',$this->integer()->defaultValue(4));
        echo 'Значения срочности по умолчанию для таблицы нарядов вернул на 4' . PHP_EOL;

        $this->alterColumn('laitovo_erp_order','sort',$this->integer()->defaultValue(4));
        echo 'Значения срочности по умолчанию для таблицы заказов вернул на 4' . PHP_EOL;
    }
}
