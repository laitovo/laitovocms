<?php

use yii\db\Migration;

class m170828_064309_add_columns_laitovo_erp_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_user}}','date_work', 'string');
        $this->addColumn('{{%laitovo_erp_user}}','date_dismissed', 'string');
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_user}}','date_work');
        $this->dropColumn('{{%laitovo_erp_user}}','date_dismissed');
    }

}
