<?php

use yii\db\Migration;

class m201125_050507_add_tmp_template_using_report_table extends Migration
{
    protected $table = '{{%tmp_using_template}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table,
        [
            'templateNumber' => $this->string()->notNull()->comment('Номер лекала'),
            'cars' => $this->text()->notNull()->comment('Список автомобилей на лекале'),
            'usingCount' => $this->integer()->notNull()->comment('Количество артикулов, изготовленных на лекале'),
        ],
        $this->tableOptions);

        $this->addPrimaryKey('template_number_pk',$this->table,'templateNumber');
    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
