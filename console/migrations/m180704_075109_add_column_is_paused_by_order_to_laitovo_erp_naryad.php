<?php

use yii\db\Migration;

class m180704_075109_add_column_is_paused_by_order_to_laitovo_erp_naryad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','is_paused_by_order',$this->boolean());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','is_paused_by_order');
    }
}
