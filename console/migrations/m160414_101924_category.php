<?php

use yii\db\Schema;
use yii\db\Migration;

class m160414_101924_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull()->comment('Команда'),
            'parent_id' => $this->integer()->comment('Родитель'),

            'name' => $this->string()->comment('Название'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('i_category_team_id','{{%category}}','team_id, parent_id, status, sort');

        $this->addForeignKey('fk_category_author_id','{{%category}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_category_updater_id','{{%category}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_category_team_id','{{%category}}','team_id','{{%team}}','id','CASCADE','CASCADE');

        $this->addForeignKey('fk_category_parent_id','{{%category}}','parent_id','{{%category}}','id','CASCADE','CASCADE');


        $this->createTable('{{%product_category}}', [
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'PRIMARY KEY(product_id, category_id)'
        ], $tableOptions);

        $this->createIndex('idx_product_category_product_id', '{{%product_category}}', 'product_id');
        $this->createIndex('idx_product_category_category_id', '{{%product_category}}', 'category_id');

        $this->addForeignKey('fk_product_category_product_id', '{{%product_category}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_category_category_id', '{{%product_category}}', 'category_id', '{{%category}}', 'id', 'CASCADE', 'CASCADE');



        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createCategory = $auth->createPermission('createCategory');
        $createCategory->description = 'Разрешение добавлять категории';
        $auth->add($createCategory);
        $readCategory = $auth->createPermission('readCategory');
        $readCategory->description = 'Разрешение просматривать категории';
        $auth->add($readCategory);
        $updateCategory = $auth->createPermission('updateCategory');
        $updateCategory->description = 'Разрешение редактировать категории';
        $auth->add($updateCategory);
        $deleteCategory = $auth->createPermission('deleteCategory');
        $deleteCategory->description = 'Разрешение удалять категории';
        $auth->add($deleteCategory);

        $rule = $auth->getRule('isAuthor');
        $updateOwnCategory = $auth->createPermission('updateOwnCategory');
        $updateOwnCategory->description = 'Разрешение редактировать созданные категории';
        $updateOwnCategory->ruleName = $rule->name;
        $auth->add($updateOwnCategory);
        $auth->addChild($updateOwnCategory, $updateCategory);

        $categoryReader = $auth->createRole('categoryReader');
        $auth->add($categoryReader);
        $auth->addChild($categoryReader, $readCategory);

        $categoryAuthor = $auth->createRole('categoryAuthor');
        $auth->add($categoryAuthor);
        $auth->addChild($categoryAuthor, $categoryReader);
        $auth->addChild($categoryAuthor, $createCategory);
        $auth->addChild($categoryAuthor, $updateOwnCategory);

        $categoryManager = $auth->createRole('categoryManager');
        $auth->add($categoryManager);
        $auth->addChild($categoryManager, $categoryAuthor);
        $auth->addChild($categoryManager, $updateCategory);

        $categoryAdmin = $auth->createRole('categoryAdmin');
        $auth->add($categoryAdmin);
        $auth->addChild($categoryAdmin, $categoryManager);
        $auth->addChild($categoryAdmin, $deleteCategory);

        $auth->addChild($admin, $categoryAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createCategory = $auth->getPermission('createCategory');
        $readCategory = $auth->getPermission('readCategory');
        $updateCategory = $auth->getPermission('updateCategory');
        $deleteCategory = $auth->getPermission('deleteCategory');
        $updateOwnCategory = $auth->getPermission('updateOwnCategory');

        $categoryReader = $auth->getRole('categoryReader');
        $categoryAuthor = $auth->getRole('categoryAuthor');
        $categoryManager = $auth->getRole('categoryManager');
        $categoryAdmin = $auth->getRole('categoryAdmin');

        $auth->removeChild($admin, $categoryAdmin);
        $auth->removeChildren($categoryAdmin);
        $auth->removeChildren($categoryManager);
        $auth->removeChildren($categoryAuthor);
        $auth->removeChildren($categoryReader);
        $auth->removeChildren($updateOwnCategory);

        $auth->remove($categoryAdmin);
        $auth->remove($categoryManager);
        $auth->remove($categoryAuthor);
        $auth->remove($categoryReader);
        $auth->remove($updateOwnCategory);
        $auth->remove($deleteCategory);
        $auth->remove($updateCategory);
        $auth->remove($readCategory);
        $auth->remove($createCategory);


        $this->delete('{{%product_category}}');
        $this->dropForeignKey('fk_product_category_product_id','{{%product_category}}');
        $this->dropForeignKey('fk_product_category_category_id','{{%product_category}}');
        $this->dropTable('{{%product_category}}');


        $this->delete('{{%category}}');

        $this->dropForeignKey('fk_category_parent_id','{{%category}}');
        $this->dropForeignKey('fk_category_team_id','{{%category}}');
        $this->dropForeignKey('fk_category_updater_id','{{%category}}');
        $this->dropForeignKey('fk_category_author_id','{{%category}}');

        $this->dropTable('{{%category}}');
    }
}
