<?php

use yii\db\Migration;

class m201215_070544_add_column_upn_id_to_invoice extends Migration
{
    public function up()
    {
        $this->addColumn('{{%europe_invoice_entry}}', 'upnId', $this->integer()
            ->comment('UPN'));
    }

    public function down()
    {
        $this->dropColumn('{{%europe_invoice_entry}}', 'upnId');
    }
}
