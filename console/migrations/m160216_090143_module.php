<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_090143_module extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%module}}', [
            'name' => $this->string()->comment('Название модуля'),
            'parent' => $this->string()->comment('Родительский модуль'),
            'description' => $this->string()->comment('Описание'),
            'type' => $this->integer()->comment('Тип модуля'),
            'last_version' => $this->float()->comment('Версия'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'json' => $this->text(),
            'PRIMARY KEY(name)'
        ], $tableOptions);

        $this->addForeignKey('module_parent','{{%module}}','parent','{{%module}}','name','CASCADE','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $readModule = $auth->createPermission('readModule');
        $readModule->description = 'Разрешение просматривать модули';
        $auth->add($readModule);
        $updateModule = $auth->createPermission('updateModule');
        $updateModule->description = 'Разрешение редактировать модули';
        $auth->add($updateModule);

        $moduleReader = $auth->createRole('moduleReader');
        $auth->add($moduleReader);
        $auth->addChild($moduleReader, $readModule);

        $moduleManager = $auth->createRole('moduleManager');
        $auth->add($moduleManager);
        $auth->addChild($moduleManager, $moduleReader);
        $auth->addChild($moduleManager, $updateModule);

        $moduleAdmin = $auth->createRole('moduleAdmin');
        $auth->add($moduleAdmin);
        $auth->addChild($moduleAdmin, $moduleManager);

        $auth->addChild($admin, $moduleAdmin);

        $this->createTable('{{%team_module}}', [
            'team_id' => $this->integer(),
            'module_name' => $this->string(),
            'version' => $this->float(),
            'PRIMARY KEY(team_id, module_name)'
        ], $tableOptions);

        $this->createIndex('idx_team_module_team_id', '{{%team_module}}', 'team_id');
        $this->createIndex('idx_team_module_module_name', '{{%team_module}}', 'module_name');

        $this->addForeignKey('fk_team_module_team_id', '{{%team_module}}', 'team_id', '{{%team}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_team_module_module_name', '{{%team_module}}', 'module_name', '{{%module}}', 'name', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->delete('{{%team_module}}');
        $this->dropForeignKey('fk_team_module_team_id','{{%team_module}}');
        $this->dropForeignKey('fk_team_module_module_name','{{%team_module}}');
        $this->dropTable('{{%team_module}}');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $readModule = $auth->getPermission('readModule');
        $updateModule = $auth->getPermission('updateModule');

        $moduleReader = $auth->getRole('moduleReader');
        $moduleManager = $auth->getRole('moduleManager');
        $moduleAdmin = $auth->getRole('moduleAdmin');

        $auth->removeChild($admin, $moduleAdmin);
        $auth->removeChildren($moduleAdmin);
        $auth->removeChildren($moduleManager);
        $auth->removeChildren($moduleReader);

        $auth->remove($moduleAdmin);
        $auth->remove($moduleManager);
        $auth->remove($moduleReader);
        $auth->remove($updateModule);
        $auth->remove($readModule);

        $this->delete('{{%module}}');

        $this->dropForeignKey('module_parent','{{%module}}');

        $this->dropTable('{{%module}}');
    }
}
