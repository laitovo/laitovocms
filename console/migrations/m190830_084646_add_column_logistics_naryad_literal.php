<?php

use yii\db\Migration;

class m190830_084646_add_column_logistics_naryad_literal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%logistics_naryad}}','transferLiteral', $this->string()->comment('Литера для перемещения склада'));
        $this->addColumn('{{%logistics_naryad}}','transferUserIn', $this->integer()->comment('Человек, который собирал в коробку'));
        $this->addColumn('{{%logistics_naryad}}','transferUserOut', $this->integer()->comment('Человек, который доставал из коробки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%logistics_naryad}}','transferLiteral');
        $this->dropColumn('{{%logistics_naryad}}','transferUserIn');
        $this->dropColumn('{{%logistics_naryad}}','transferUserOut');
    }
}
