<?php

use yii\db\Migration;

/**
 * Class m180528_060037_create_narayd_log_table Создает таблицу
 */
class m180528_060037_create_narayd_log_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%laitovo_erp_naryad_log}}', [
            'id'               => $this->primaryKey(),
            'workOrderId'      => $this->integer(),
            'date'             => $this->integer(),
            'locationFromName' => $this->string(),
            'locationFromId'   => $this->integer(),
            'locationToName'   => $this->string(),
            'locationToId'     => $this->integer(),
            'workerName'       => $this->string(),
            'workerId'         => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%laitovo_erp_naryad_log}}');
    }
}
