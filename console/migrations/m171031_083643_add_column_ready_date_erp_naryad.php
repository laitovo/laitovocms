<?php

use yii\db\Migration;

class m171031_083643_add_column_ready_date_erp_naryad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}', 'ready_date', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}', 'ready_date');
    }
}
