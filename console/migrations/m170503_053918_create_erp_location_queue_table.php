<?php

use yii\db\Migration;

/**
 * Handles the creation of table `erp_location_queue`.
 */
class m170503_053918_create_erp_location_queue_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%laitovo_erp_location_queue}}', [
            'id' => $this->primaryKey(),
            'location_id' => $this->integer(),
            'json' => $this->text(),
        ]);

        $this->addForeignKey('fk_location_id', '{{%laitovo_erp_location_queue}}', 'location_id', '{{%laitovo_erp_location}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_location_id', '{{%laitovo_erp_location_queue}}');
        $this->dropTable('laitovo_erp_location_queue');
    }
}
