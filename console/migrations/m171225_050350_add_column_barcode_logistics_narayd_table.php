<?php

use yii\db\Migration;

class m171225_050350_add_column_barcode_logistics_narayd_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_naryad}}', 'barcode', $this->string()->comment('штрихкод'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_naryad}}', 'barcode');
    }
}
