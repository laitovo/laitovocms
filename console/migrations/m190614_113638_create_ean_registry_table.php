<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ean_registry`.
 */
class m190614_113638_create_ean_registry_table extends Migration
{
    /**
     * @var $table
     */
    protected $table = '{{%ean_registry}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'ean'     => $this->string()->notNull()->comment('Номер EAN'),
            'carArticle' => $this->integer()->notNull()->unique()->comment('Артикул автомобиля'),
            'carTitle' => $this->string()->notNull()->unique()->comment('Наименование автомобиля'),
            'PRIMARY KEY(ean)'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
