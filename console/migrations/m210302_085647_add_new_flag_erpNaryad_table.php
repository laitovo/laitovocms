<?php

use yii\db\Migration;

class m210302_085647_add_new_flag_erpNaryad_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','isAnotherPlaced',$this->boolean()->defaultValue(false)->comment('расположен на других вешалках'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','isAnotherPlaced');
    }
}
