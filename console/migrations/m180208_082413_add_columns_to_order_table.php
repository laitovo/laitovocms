<?php

use yii\db\Migration;

class m180208_082413_add_columns_to_order_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_order}}','address',$this->string());
        $this->addColumn('{{%logistics_order}}','category',$this->string());
        $this->addColumn('{{%logistics_order}}','country',$this->string());
        $this->addColumn('{{%logistics_order}}','delivery',$this->string());
        $this->addColumn('{{%logistics_order}}','innercomment',$this->string());
        $this->addColumn('{{%logistics_order}}','manager',$this->string());
        $this->addColumn('{{%logistics_order}}','useremail',$this->string());
        $this->addColumn('{{%logistics_order}}','userinn',$this->string());
        $this->addColumn('{{%logistics_order}}','username',$this->string());
        $this->addColumn('{{%logistics_order}}','userphone',$this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_order}}','address');
        $this->dropColumn('{{%logistics_order}}','category');
        $this->dropColumn('{{%logistics_order}}','country');
        $this->dropColumn('{{%logistics_order}}','delivery');
        $this->dropColumn('{{%logistics_order}}','innercomment');
        $this->dropColumn('{{%logistics_order}}','manager');
        $this->dropColumn('{{%logistics_order}}','useremail');
        $this->dropColumn('{{%logistics_order}}','userinn');
        $this->dropColumn('{{%logistics_order}}','username');
        $this->dropColumn('{{%logistics_order}}','userphone');
    }
}
