<?php

use yii\db\Migration;

class m210114_085902_indx extends Migration
{
    public function safeUp()
    {
        $this->createIndex('idx_laitovo_erp_naryad_distributed_date_ready_date','laitovo_erp_naryad',['distributed_date','ready_date']);
        $this->createIndex('idx_laitovo_laitovo_erp_naryad_log_locationFromId_date','laitovo_erp_naryad_log',['locationFromId','date']);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_laitovo_laitovo_erp_naryad_log_locationFromId_date','laitovo_erp_naryad_log');
        $this->dropIndex('idx_laitovo_erp_naryad_distributed_date_ready_date','laitovo_erp_naryad');
    }
}
