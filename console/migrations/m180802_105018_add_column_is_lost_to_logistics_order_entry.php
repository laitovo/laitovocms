<?php

use yii\db\Migration;

class m180802_105018_add_column_is_lost_to_logistics_order_entry extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order_entry}}','is_lost', $this->boolean()->defaultValue(false)
            ->comment('Пометка о том, что для данной позиции потерян UPN'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order_entry}}','is_lost');
    }
}
