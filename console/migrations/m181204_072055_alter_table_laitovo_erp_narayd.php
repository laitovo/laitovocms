<?php

use yii\db\Migration;

class m181204_072055_alter_table_laitovo_erp_narayd extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','isNeedTemplate',$this->boolean()->defaultValue(false)->comment('Необходимо изготовить лекало'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','isNeedTemplate');
    }
}
