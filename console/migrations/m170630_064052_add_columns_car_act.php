<?php

use yii\db\Migration;

class m170630_064052_add_columns_car_act extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_cars_act}}', 'element', $this->string());
        $this->addColumn('{{%laitovo_cars_act}}', 'type', $this->string());
        $this->addColumn('{{%laitovo_cars_act}}', 'checked', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_cars_act}}', 'element');
        $this->dropColumn('{{%laitovo_cars_act}}', 'type');
        $this->dropColumn('{{%laitovo_cars_act}}', 'checked');
    }
}
