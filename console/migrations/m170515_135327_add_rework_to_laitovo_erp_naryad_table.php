<?php

use yii\db\Migration;

class m170515_135327_add_rework_to_laitovo_erp_naryad_table extends Migration
{
    public function up()
    {
         $this->addColumn('laitovo_erp_naryad', 'rework', $this->integer());

    }

    public function down()
    {
         $this->dropColumn('laitovo_erp_naryad', 'rework');
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
