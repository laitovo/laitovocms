<?php

use yii\db\Migration;

class m180709_093027_add_deleting_columns_to_logistics_order_entry extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order_entry}}','is_deleted', $this->boolean()
            ->comment('Пометка, что позиция удалена из заказа. От физического удаления решено отказаться,
             потому что здесь важно иметь возможность безболезненно восстановить удалённую сущность.'));
        $this->addColumn('{{%logistics_order_entry}}','deleted_at', $this->integer());
        $this->addColumn('{{%logistics_order_entry}}','deleted_by', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order_entry}}','is_deleted');
        $this->dropColumn('{{%logistics_order_entry}}','deleted_at');
        $this->dropColumn('{{%logistics_order_entry}}','deleted_by');
    }
}
