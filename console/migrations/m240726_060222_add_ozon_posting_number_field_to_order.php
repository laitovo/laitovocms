<?php

use yii\db\Migration;

class m240726_060222_add_ozon_posting_number_field_to_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','ozon_posting_number', $this->string()->comment('Идентификатор отправления в OZON'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','ozon_posting_number');
    }
}
