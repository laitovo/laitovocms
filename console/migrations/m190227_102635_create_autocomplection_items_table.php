<?php

use yii\db\Migration;

/**
 * Handles the creation of table `autocomplection_items`.
 */
class m190227_102635_create_autocomplection_items_table extends Migration
{
    protected $table = '{{%logistics_autocompletion_items}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table,
            [
                'id'         => $this->primaryKey(),
                'orderNumber'=> $this->integer(),
                'logId'      => $this->integer()->notNull(),
                'article'    => $this->char(20)->notNull(),
                'requested'  => $this->smallInteger()->notNull(),
                'createdAt'  => $this->integer(),
                'updatedAt'  => $this->integer(),
                'status'     => $this->boolean()->defaultValue(0)->notNull(),
                'indexGroup' => $this->smallInteger(),
            ],
            $this->tableOptions);

        $this->addForeignKey('fk-autocompletion_items_log-id', $this->table, 'logId', '{{%logistics_autocompletion_logs}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
