<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_location_workplace`.
 */
class m170524_050916_create_laitovo_erp_location_workplace_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_location_workplace}}', [
            'id' => $this->primaryKey(),
            'location_id' => $this->integer(),
            'location_number' => $this->string()->comment('Номер внутри локации'),
            'type' => $this->string()->comment('Тип рабочего места'),
            'user_id' => $this->integer()->comment('Текущий пользователь'),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_location_workplace_location_id', '{{%laitovo_erp_location_workplace}}', 'location_id', '{{%laitovo_erp_location}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_location_workplace_user_id', '{{%laitovo_erp_location_workplace}}', 'user_id', '{{%laitovo_erp_user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_user_id', '{{%laitovo_erp_location_workplace}}');
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_location_id', '{{%laitovo_erp_location_workplace}}');
        
        $this->dropTable('laitovo_erp_location_workplace');
    }
}
