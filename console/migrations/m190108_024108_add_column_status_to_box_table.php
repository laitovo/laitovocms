<?php

use yii\db\Migration;

class m190108_024108_add_column_status_to_box_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_box}}', 'status', 'TINYINT UNSIGNED DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_box}}', 'status');
    }
}
