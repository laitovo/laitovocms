<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_storage_log`.
 */
class m171102_043931_create_logistics_storage_log_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%logistics_storage_log}}', [
            'id' => $this->primaryKey(),
            'storage_id' => $this->integer()->comment('Место хранения'),
            'literal' => $this->string()->comment('Литера'),
            'upn_id' => $this->integer()->comment('Уникальный номер продукта'),
            'reserved' => $this->boolean()->comment('Зарезервирован'),
            'order_id' => $this->integer()->comment('Для кого зарезервирован'),
            'action' => $this->string()->comment('Действие'),
            'comment' => $this->string()->comment('Комментарий'),

            //columns
            'created_at' => $this->integer(),
            'author_id' =>  $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_storage_log_storage_id', '{{%logistics_storage_log}}', 
            'storage_id', '{{%logistics_storage}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_log_upn_id', '{{%logistics_storage_log}}', 
            'upn_id', '{{%logistics_naryad}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_log_order_id', '{{%logistics_storage_log}}', 
            'order_id', '{{%logistics_order}}', 'id', 
            'SET NULL', 'CASCADE');
        
        $this->addForeignKey('fk_logistics_storage_log_author_id', '{{%logistics_storage_log}}', 
            'author_id', '{{%user}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_log_updater_id', '{{%logistics_storage_log}}', 
            'updater_id', '{{%user}}', 'id', 
            'SET NULL', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_storage_log_updater_id', '{{%logistics_storage_log}}');
        $this->dropForeignKey('fk_logistics_storage_log_author_id', '{{%logistics_storage_log}}');
        $this->dropForeignKey('fk_logistics_storage_log_order_id', '{{%logistics_storage_log}}');
        $this->dropForeignKey('fk_logistics_storage_log_upn_id', '{{%logistics_storage_log}}');
        $this->dropForeignKey('fk_logistics_storage_log_storage_id', '{{%logistics_storage_log}}');

        $this->dropTable('{{%logistics_storage_log}}');
    }
}
