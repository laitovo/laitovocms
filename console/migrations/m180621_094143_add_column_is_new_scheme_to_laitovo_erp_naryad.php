<?php

use yii\db\Migration;

class m180621_094143_add_column_is_new_scheme_to_laitovo_erp_naryad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','is_new_scheme',$this->boolean());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','is_new_scheme');
    }
}
