<?php

use yii\db\Migration;

/**
 * Handles the creation of table `unit_accounting`.
 */
class m170529_122700_create_unit_accounting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('unit_accounting', [
            
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->comment('Дата создания'),
            'material_parameter_id' => $this->integer()->comment('Материал'),
            'autor_id' => $this->integer()->comment('Автор записи'),
            'accounting_unit' => $this->integer()->comment('Учетная единица'),
            'accounting_unit_price' => $this->double()->comment('Цена учетной единицы'),
        ]);
        
        $this->createIndex(
            'idx-unit_accounting-material_parameter_id',
            'unit_accounting',
            'material_parameter_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-unit_accounting-material_parameter_id',
            'unit_accounting',
            'material_parameter_id',
            'material_parameters',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-unit_accounting-autor_id',
            'unit_accounting',
            'autor_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-unit_accounting-autor_id',
            'unit_accounting',
            'autor_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-unit_accounting-accounting_unit',
            'unit_accounting',
            'accounting_unit'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-unit_accounting-accounting_unit',
            'unit_accounting',
            'accounting_unit',
            'unit_materials',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
         $this->dropForeignKey(
            'fk-unit_accounting-material_parameter_id',
            'unit_accounting'
        );

        
        $this->dropIndex(
            'idx-unit_accounting-material_parameter_id',
            'unit_accounting'
        );
         $this->dropForeignKey(
            'fk-unit_accounting-autor_id',
            'unit_accounting'
        );


        $this->dropIndex(
            'idx-unit_accounting-autor_id',
            'unit_accounting'
        );
         $this->dropForeignKey(
            'fk-unit_accounting-accounting_unit',
            'unit_accounting'
        );

       
        $this->dropIndex(
            'idx-unit_accounting-accounting_unit',
            'unit_accounting'
        );
        $this->dropTable('unit_accounting');
    }
}
