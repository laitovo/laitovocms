<?php

use yii\db\Migration;

class m180718_133029_add_column_is_custom_to_logistics_order_entry extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order_entry}}','is_custom',$this->boolean()->defaultValue(false)->comment('Является ли позиция индивидуальной в изготовлении (индивидуальная позиция не ищется на складе, а всегда запускается в производство)'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order_entry}}','is_custom');
    }
}
