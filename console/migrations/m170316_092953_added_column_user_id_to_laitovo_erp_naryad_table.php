<?php

use yii\db\Migration;

class m170316_092953_added_column_user_id_to_laitovo_erp_naryad_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}', 'user_id', $this->Integer(10));

        $this->createIndex('idx_laitovo_erp_naryad_user_id', '{{%laitovo_erp_naryad}}', 'user_id');

        $this->addForeignKey('fk_laitovo_erp_naryad_user_id',
            '{{%laitovo_erp_naryad}}', 'user_id',
            '{{%laitovo_erp_user}}', 'id', 
            'SET NULL',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_naryad_user_id', '{{%laitovo_erp_naryad}}');

        $this->dropIndex('idx_laitovo_erp_naryad_user_id', '{{%laitovo_erp_naryad}}');

        $this->dropColumn('{{%laitovo_erp_naryad}}', 'user_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
