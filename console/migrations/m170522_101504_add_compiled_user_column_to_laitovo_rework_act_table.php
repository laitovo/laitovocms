<?php

use yii\db\Migration;

/**
 * Handles adding compiled_user to table `laitovo_rework_act`.
 */
class m170522_101504_add_compiled_user_column_to_laitovo_rework_act_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('laitovo_rework_act', 'compiled_user', $this->integer());
        $this->createIndex(
            'idx-laitovo_rework_act-compiled_user',
            'laitovo_rework_act',
            'compiled_user'
        );
        $this->addForeignKey(
            'fk-laitovo_rework_act-compiled_user',
            'laitovo_rework_act',
            'compiled_user',
            'laitovo_erp_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-laitovo_rework_act-compiled_user',
            'laitovo_rework_act'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-laitovo_rework_act-compiled_user',
            'laitovo_rework_act'
        );
        $this->dropColumn('laitovo_rework_act', 'compiled_user');
    }
}
