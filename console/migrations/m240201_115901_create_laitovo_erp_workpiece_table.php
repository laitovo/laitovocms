<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%laitovo_erp_workpiece}}`.
 */
class m240201_115901_create_laitovo_erp_workpiece_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_workpiece_nomenclature}}', [
            'id' => $this->primaryKey()->comment('ID'),
            'title' => $this->string()->notNull()->comment('Наименование'),
        ], $tableOptions);

        $this->createTable('{{%laitovo_erp_workpiece_document}}', [
            'id'              => $this->primaryKey()->comment('ID'),
            'nomenclature_id' => $this->integer()->comment('ID Номенклатуры'),
            'user_id'         => $this->integer()->comment('ID Сотрудника'),
            'document_type'   => $this->tinyInteger()->comment('Тип документа'),
            'count'           => $this->integer()->comment('Количество')->defaultValue('0')->notNull(),
            'date'            => $this->integer()->comment('Дата')->notNull()
        ], $tableOptions);

        $this->addForeignKey('laitovo_erp_workpiece_document_user_id', '{{%laitovo_erp_workpiece_document}}', 'user_id', '{{%laitovo_erp_user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('laitovo_erp_workpiece_document_nomenclature_id', '{{%laitovo_erp_workpiece_document}}', 'nomenclature_id', '{{%laitovo_erp_workpiece_nomenclature}}', 'id', 'SET NULL', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('laitovo_erp_workpiece_document_nomenclature_id', '{{%laitovo_erp_workpiece_document}}');
        $this->dropForeignKey('laitovo_erp_workpiece_document_user_id', '{{%laitovo_erp_workpiece_document}}');

        $this->dropTable('{{%laitovo_erp_workpiece_document}}');
        $this->dropTable('{{%laitovo_erp_workpiece_nomenclature}}');
    }
}
