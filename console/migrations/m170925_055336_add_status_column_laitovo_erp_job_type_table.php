<?php

use yii\db\Migration;

class m170925_055336_add_status_column_laitovo_erp_job_type_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_erp_job_type', 'status', 'integer');
    }

    public function down()
    {
        $this->dropColumn('laitovo_erp_job_type', 'status');
    }
}
