<?php

use yii\db\Migration;

class m170607_061906_create_laitovo_erp_location_workplace_register extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_location_workplace_register}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'workplace_id' => $this->integer(),
            'register_at' => $this->integer(11),
            'action_type' => $this->string(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_location_workplace_register_user_id',
            '{{%laitovo_erp_location_workplace_register}}', 'user_id',
            '{{%laitovo_erp_user}}', 'id', 'SET NULL', 'CASCADE');
        
        $this->addForeignKey('fk_laitovo_erp_location_workplace_register_workplace_id',
            '{{%laitovo_erp_location_workplace_register}}', 'workplace_id',
            '{{%laitovo_erp_location_workplace}}', 'id', 'SET NULL', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_register_workplace_id', '{{%laitovo_erp_location_workplace_register}}');

        $this->dropForeignKey('fk_laitovo_erp_location_workplace_register_user_id', '{{%laitovo_erp_location_workplace_register}}');

        $this->dropTable('{{%laitovo_erp_location_workplace_register}}');
    }

}
