<?php

use yii\db\Migration;

class m170317_132739_fix_fk extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_laitovo_erp_naryad_start','{{%laitovo_erp_naryad}}');
        $this->addForeignKey('fk_laitovo_erp_naryad_start','{{%laitovo_erp_naryad}}','start','{{%laitovo_erp_location}}','id','SET NULL','CASCADE');

    }

    public function down()
    {
    }

}
