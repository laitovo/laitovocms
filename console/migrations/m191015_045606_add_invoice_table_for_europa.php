<?php

use yii\db\Migration;

class m191015_045606_add_invoice_table_for_europa extends Migration
{
    protected $table1 = '{{%europe_invoice}}';
    protected $table2 = '{{%europe_invoice_entry}}';

    public function safeUp()
    {
        $this->createTable($this->table1, [
            'id'=> $this->primaryKey(),
            'basedOn' => $this->text()->comment('Перечисление отгрузок в формате Json, на освании которых был создан инвойс'),
            'createdAt' => $this->integer()->notNull()->comment('Артикул продукта'),
            'authorId' => $this->integer()->comment('Наименование продукта'),
        ]);

        $this->createTable($this->table2, [
            'id'=> $this->primaryKey(),
            'invoiceId'=> $this->integer()->comment('Инвойс, к которому относятся строки табличной части'),
            'article' => $this->string()->notNull()->comment('Артикул продукта'),
            'title' => $this->string()->notNull()->comment('Наименование продукта'),
            'price' => $this->double()->notNull()->comment('Цена продукта'),
            'quantity' => $this->double()->notNull()->comment('Количество продукта'),
        ]);

        $this->addForeignKey('invoiceId_europe_invoice_entry',$this->table2,'invoiceId',$this->table1,'id','SET NULL','CASCADE');
    }

    public function safeDown()
    {

        $this->dropForeignKey('invoiceId_europe_invoice_entry',$this->table2);
        $this->dropTable($this->table1);
        $this->dropTable($this->table2);
    }
}
