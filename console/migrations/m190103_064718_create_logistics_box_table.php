<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_box`.
 */
class m190103_064718_create_logistics_box_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%logistics_box}}', [
            'id' => $this->primaryKey(),
        ]);

        $this->addColumn('{{%logistics_order_entry}}','boxId',$this->integer()->comment('Коробка'));
        $this->addForeignKey('fk_logistics_order_entry_boxId_logistics_box_id',
            '{{%logistics_order_entry}}','boxId',
            '{{%logistics_box}}','id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_logistics_order_entry_boxId_logistics_box_id','{{%logistics_order_entry}}');
        $this->dropColumn('{{%logistics_order_entry}}','boxId');
        $this->dropTable('{{%logistics_box}}');
    }
}
