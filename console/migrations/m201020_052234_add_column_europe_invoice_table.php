<?php

use yii\db\Migration;

class m201020_052234_add_column_europe_invoice_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%europe_invoice}}', 'isLaitovoManufactory', $this->boolean()->defaultValue(false)
            ->comment('Отгрузка в Laitovo Manufactory'));
    }

    public function down()
    {
        $this->dropColumn('{{%europe_invoice}}', 'isLaitovoManufactory');
    }
}
