<?php

use yii\db\Migration;

class m181127_033137_add_column_confirmed_laitovo_erp_additional_work extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_additional_work}}','confirmed',$this->boolean()->comment('Подтверждено начисление зарплаты руководителем'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_additional_work}}','confirmed');
    }
}
