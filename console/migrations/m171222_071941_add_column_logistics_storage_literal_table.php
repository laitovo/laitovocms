<?php

use yii\db\Migration;

class m171222_071941_add_column_logistics_storage_literal_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_storage_literal}}', 'default_quantity', $this->integer()->comment('Количество литер по умолчанию'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_storage_literal}}', 'default_quantity');
    }
}
