<?php

use yii\db\Migration;

class m171117_054633_add_columns_automatic_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_automatic_order}}', 'frequency', $this->integer()->comment('Частота реализации'));
        $this->addColumn('{{%logistics_automatic_order}}', 'fixRate', $this->integer()->comment('Корректирующий коэффициент'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_automatic_order}}', 'fixRate');
        $this->dropColumn('{{%logistics_automatic_order}}', 'frequency');
    }
}
