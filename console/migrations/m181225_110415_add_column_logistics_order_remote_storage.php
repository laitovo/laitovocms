<?php

use yii\db\Migration;

class m181225_110415_add_column_logistics_order_remote_storage extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_order}}', 'remoteStorage', 'TINYINT UNSIGNED DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_order}}', 'remoteStorage');
    }
}
