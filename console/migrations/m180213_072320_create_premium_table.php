<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_premium`.
 */
class m180213_072320_create_premium_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_erp_premium', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Сотрудник'),
            'amount' => $this->double()->comment('Размер премии'),
            'is_paid' => $this->boolean()->defaultValue(0)->comment('Начислена или нет'),
            'paid_at' => $this->integer()->comment('Дата начисления'),
            'rework_act_id' => $this->integer()->comment('Документ основания'),
            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('laitovo_erp_premium');
    }
}
