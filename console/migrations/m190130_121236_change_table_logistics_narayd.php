<?php

use yii\db\Migration;

class m190130_121236_change_table_logistics_narayd extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_naryad}}','isParent',$this->boolean()->defaultValue(false)->comment('Является ли данный upn - родителем для других upn'));
        $this->addColumn('{{%logistics_naryad}}','pid',$this->integer()->comment('ПАПА-UPN'));
        $this->addForeignKey('fk_logistics_naryad_pid', '{{%logistics_naryad}}', 'pid', '{{%logistics_naryad}}', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('fk_logistics_naryad_pid','{{%logistics_naryad}}');
        $this->dropColumn('{{%logistics_naryad}}','pid');
        $this->dropColumn('{{%logistics_naryad}}','isParent');
    }
}
