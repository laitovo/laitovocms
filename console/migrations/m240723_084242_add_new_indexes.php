<?php

use yii\db\Migration;

class m240723_084242_add_new_indexes extends Migration
{
    protected $table = '{{%name_table}}';
    protected $tableOptions;

    public function safeUp()
    {
        $this->createIndex('idx_laitovo_erp_naryad_status_logist_id','{{%laitovo_erp_naryad}}',['status','distributed','ready_date','logist_id']);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_laitovo_erp_naryad_status_logist_id','{{%laitovo_erp_naryad}}',['status','created_at']);
    }
}
