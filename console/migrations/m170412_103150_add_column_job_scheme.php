<?php

use yii\db\Migration;

class m170412_103150_add_column_job_scheme extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_job_scheme}}', 'rule', $this->text()->comment('Правило применения'));
    }

    public function down()
    {
        echo "m170412_103150_add_column_job_scheme cannot be reverted.\n";

        $this->dropColumn('{{%laitovo_erp_job_scheme}}', 'rule');
    }
}
