<?php

use yii\db\Migration;

class m210304_074555_add_column_old_sort_erp_naryad_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','oldSort',$this->integer()->defaultValue(0));
        $this->addColumn('{{%laitovo_erp_naryad}}','isCompletion',$this->boolean()->defaultValue(false)->comment('Комплектация'));

    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','isCompletion');
        $this->dropColumn('{{%laitovo_erp_naryad}}','oldSort');
    }
}
