<?php

use yii\db\Migration;

class m180208_083903_add_columns_to_order_entry extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_order_entry}}','barcode',$this->string());
        $this->addColumn('{{%logistics_order_entry}}','car',$this->string());
        $this->addColumn('{{%logistics_order_entry}}','hlyastik',$this->string());
        $this->addColumn('{{%logistics_order_entry}}','lekalo',$this->string());
        $this->addColumn('{{%logistics_order_entry}}','magnit',$this->string());
        $this->addColumn('{{%logistics_order_entry}}','name',$this->string());
        $this->addColumn('{{%logistics_order_entry}}','natyag',$this->string());
        $this->addColumn('{{%logistics_order_entry}}','settext',$this->string());
        $this->addColumn('{{%logistics_order_entry}}','visota',$this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_order_entry}}','barcode');
        $this->dropColumn('{{%logistics_order_entry}}','car');
        $this->dropColumn('{{%logistics_order_entry}}','hlyastik');
        $this->dropColumn('{{%logistics_order_entry}}','lekalo');
        $this->dropColumn('{{%logistics_order_entry}}','magnit');
        $this->dropColumn('{{%logistics_order_entry}}','name');
        $this->dropColumn('{{%logistics_order_entry}}','natyag');
        $this->dropColumn('{{%logistics_order_entry}}','settext');
        $this->dropColumn('{{%logistics_order_entry}}','visota');
    }
}
