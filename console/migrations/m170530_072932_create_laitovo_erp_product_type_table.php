<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_product_type`.
 */
class m170530_072932_create_laitovo_erp_product_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_product_type}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->comment('Название'),
            'rule' => $this->string()->comment('Правило определения по артикулу'),
            //дополнительная информация
            'author_id' => $this->integer(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_product_type_author_id', '{{%laitovo_erp_product_type}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_product_type_author_id', '{{%laitovo_erp_product_type}}');
        $this->dropTable('{{%laitovo_erp_product_type}}');
    }
}
