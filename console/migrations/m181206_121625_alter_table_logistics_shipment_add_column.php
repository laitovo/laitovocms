<?php

use yii\db\Migration;

class m181206_121625_alter_table_logistics_shipment_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_shipment}}','officialNumber',$this->integer()->comment('Официальный номер ТТН'));
        $this->addColumn('{{%logistics_shipment}}','officialNumberYear',$this->integer(4)->comment('Год для официального номера ТТН'));

        $this->createIndex('logistics_shipment_officialNumber_index','{{%logistics_shipment}}',['officialNumber','officialNumberYear'],true);
    }

    public function safeDown()
    {
        $this->dropIndex('logistics_shipment_officialNumber_index','{{%logistics_shipment}}');
        $this->dropColumn('{{%logistics_shipment}}','officialNumberYear');
        $this->dropColumn('{{%logistics_shipment}}','officialNumber');
    }
}
