<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_200813_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey(),
            'table' => $this->string()->comment('Таблица'),
            'field_id' => $this->integer()->comment('ID Модели'),
            'model' => $this->string()->comment('Модель'),
            'ip' => $this->string()->comment('IP'),
            'host' => $this->string()->comment('HOST'),
            'url' => $this->text()->comment('URL'),
            'app' => $this->string()->comment('Приложение'),
            'module' => $this->string()->comment('Модуль'),
            'controller' => $this->string()->comment('Контроллер'),
            'action' => $this->string()->comment('Действие'),
            'operator' => $this->string()->comment('Оператор'),
            'log' => $this->text()->comment('Изменение'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'date' => $this->integer()->comment('Дата'),
            'time' => $this->float()->comment('Затрачено времени'),
        ], $tableOptions);

        $this->createIndex('log_table','{{%log}}','table, field_id, date');
        $this->createIndex('log_model','{{%log}}','model');
        $this->createIndex('log_date','{{%log}}','date');
        $this->createIndex('log_ip','{{%log}}','ip');
        $this->createIndex('log_host','{{%log}}','host');
        $this->createIndex('log_app','{{%log}}','app');
        $this->createIndex('log_module','{{%log}}','module');
        $this->createIndex('log_controller','{{%log}}','controller');
        $this->createIndex('log_action','{{%log}}','action');
        $this->createIndex('log_operator','{{%log}}','operator');
        $this->createIndex('log_time','{{%log}}','time');
        
        $this->addForeignKey('log_user_id','{{%log}}','user_id','{{%user}}','id','SET NULL','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $readLog = $auth->createPermission('readLog');
        $readLog->description = 'Разрешение просматривать логи';
        $auth->add($readLog);
        $deleteLog = $auth->createPermission('deleteLog');
        $deleteLog->description = 'Разрешение удалять логи';
        $auth->add($deleteLog);

        $logReader = $auth->createRole('logReader');
        $auth->add($logReader);
        $auth->addChild($logReader, $readLog);

        $logAdmin = $auth->createRole('logAdmin');
        $auth->add($logAdmin);
        $auth->addChild($logAdmin, $logReader);
        $auth->addChild($logAdmin, $deleteLog);

        $auth->addChild($admin, $logAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $readLog = $auth->getPermission('readLog');
        $deleteLog = $auth->getPermission('deleteLog');

        $logReader = $auth->getRole('logReader');
        $logAdmin = $auth->getRole('logAdmin');

        $auth->removeChild($admin, $logAdmin);
        $auth->removeChildren($logAdmin);
        $auth->removeChildren($logReader);

        $auth->remove($logAdmin);
        $auth->remove($logReader);
        $auth->remove($deleteLog);
        $auth->remove($readLog);
        
        $this->dropForeignKey('log_user_id','{{%log}}');
        $this->dropTable('{{%log}}');
    }
}
