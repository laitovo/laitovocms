<?php

use yii\db\Migration;

class m200408_103707_add_indexes_for_autocompletio extends Migration
{
    public function safeUp()
    {
        $this->createIndex('index_team_id_article','{{%logistics_naryad}}',['team_id','article']);
        $this->createIndex('index_upn_id_storage_id','{{%logistics_storage_state}}',['storage_id','upn_id']);
        $this->createIndex('index_upn_id','{{%logistics_storage_state}}',['upn_id']);
        $this->createIndex('index_storage_id','{{%logistics_storage_state}}',['storage_id']);
        $this->createIndex('index_cars_article','{{%laitovo_cars}}',['article']);
    }

    public function safeDown()
    {
        $this->dropIndex('index_cars_article','{{%laitovo_cars}}');
        $this->dropIndex('index_storage_id','{{%logistics_storage_state}}');
        $this->dropIndex('index_upn_id','{{%logistics_storage_state}}');
        $this->dropIndex('index_upn_id_storage_id','{{%logistics_storage_state}}');
        $this->dropIndex('index_team_id_article','{{%logistics_naryad}}');
    }
}
