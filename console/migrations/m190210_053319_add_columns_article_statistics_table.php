<?php

use yii\db\Migration;

class m190210_053319_add_columns_article_statistics_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_article_sale_statistics}}','fixRate',$this->integer()->comment('Корректировочный коэффициент скорости производства'));
        $this->addColumn('{{%laitovo_article_sale_statistics}}','minFrequency',$this->integer()->comment('Мин. частота реализации, мес'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_article_sale_statistics}}','fixRate');
        $this->dropColumn('{{%laitovo_article_sale_statistics}}','minFrequency');
    }
}
