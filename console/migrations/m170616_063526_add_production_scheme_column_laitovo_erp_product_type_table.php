<?php

use yii\db\Migration;

class m170616_063526_add_production_scheme_column_laitovo_erp_product_type_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_erp_product_type', 'production_scheme_id','integer');
        $this->addForeignKey(
            'fk-laitovo_erp_product_type-production_scheme_id',
            '{{%laitovo_erp_product_type}}',
            'production_scheme_id',
            '{{%production_scheme}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_product_type-production_scheme_id', '{{%laitovo_erp_product_type}}');
        $this->dropColumn('laitovo_erp_product_type', 'production_scheme_id');

       
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
