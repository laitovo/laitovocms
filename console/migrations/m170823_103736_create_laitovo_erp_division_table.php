<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_division`.
 */
class m170823_103736_create_laitovo_erp_division_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_erp_division', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(),
            'description'=>$this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('laitovo_erp_division');
    }
}
