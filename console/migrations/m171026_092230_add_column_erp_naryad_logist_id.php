<?php

use yii\db\Migration;

class m171026_092230_add_column_erp_naryad_logist_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}', 'logist_id', $this->integer()->comment('Номер наряда из логистики'));
        $this->addForeignKey('fk_laitovo_erp_naryad_logist_id', '{{%laitovo_erp_naryad}}', 'logist_id', '{{%logistics_naryad}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_naryad_logist_id', '{{%laitovo_erp_naryad}}');
        $this->dropColumn('{{%laitovo_erp_naryad}}', 'logist_id');
    }
}
