<?php

use yii\db\Migration;

class m171221_065148_add_column_erp_naryad_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}', 'distributed', $this->boolean()->defaultValue(0)->comment('Распределен с диспетчер-склад'));
        $this->addColumn('{{%laitovo_erp_naryad}}', 'distributed_date', $this->integer()->comment('Дата распределения'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}', 'distributed_date');
        $this->dropColumn('{{%laitovo_erp_naryad}}', 'distributed');
    }
}
