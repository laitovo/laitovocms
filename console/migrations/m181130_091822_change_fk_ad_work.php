<?php

use yii\db\Migration;

class m181130_091822_change_fk_ad_work extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropForeignKey('fk-laitovo_erp_additional_work-updater_id', '{{%laitovo_erp_additional_work}}');

        $this->dropForeignKey('fk-laitovo_erp_additional_work-author_id', '{{%laitovo_erp_additional_work}}');
    }

    public function safeDown()
    {
        $this->addForeignKey(
            'fk-laitovo_erp_additional_work-updater_id',
            '{{%laitovo_erp_additional_work}}',
            'updater_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_additional_work-author_id',
            '{{%laitovo_erp_additional_work}}',
            'author_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
    }

}
