<?php

use yii\db\Schema;
use yii\db\Migration;

class m160220_081208_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull()->comment('Команда'),
            'organization_id' => $this->integer()->comment('Организация'),
            'manager_id' => $this->integer()->comment('Менеджер'),
            'website_id' => $this->integer()->comment('Сайт'),
            'number' => $this->integer()->comment('Номер заказа'),

            'user_id' => $this->integer()->comment('Покупатель'),
            
            'client_team_id' => $this->integer()->comment('Команда покупателя'),
            'client_organization_id' => $this->integer()->comment('Организация покупателя'),

            'region_id' => $this->integer()->comment('Регион'),

            'status' => $this->string()->comment('Статус'),
            'condition' => $this->string()->comment('Состояние'),
            'discount' => $this->money(19,2)->comment('Скидка'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('order_author_id','{{%order}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('order_updater_id','{{%order}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('order_user_id','{{%order}}','user_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('order_team_id','{{%order}}','team_id','{{%team}}','id','CASCADE','CASCADE');
        $this->addForeignKey('order_client_team_id','{{%order}}','client_team_id','{{%team}}','id','SET NULL','CASCADE');

        $this->addForeignKey('order_manager_id','{{%order}}','manager_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('order_website_id','{{%order}}','website_id','{{%website}}','id','SET NULL','CASCADE');


        $this->createTable('{{%item}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull()->comment('Заказ'),
            'product_id' => $this->integer()->comment('Товар'),

            'name' => $this->string()->comment('Название'),
            
            'price' => $this->money(19,2)->comment('Цена'),
            'retail' => $this->money(19,2)->comment('Розничная(базовая) цена'),
            'currency' => $this->string()->comment('Валюта'),
            'nds' => $this->money()->comment('НДС %'),
            'nds_in' => $this->boolean()->defaultValue(true)->comment('НДС включен в цену'),
            'quantity' => $this->decimal(19,4)->comment('Количество'),
            'properties' => $this->text()->comment('Свойства'),
            'lock' => $this->boolean()->defaultValue(false)->comment('Зафиксировать'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),

        ], $tableOptions);

        $this->addForeignKey('item_author_id','{{%item}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('item_updater_id','{{%item}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('item_order_id','{{%item}}','order_id','{{%order}}','id','CASCADE','CASCADE');

        $this->addForeignKey('item_product_id','{{%item}}','product_id','{{%product}}','id','SET NULL','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createOrder = $auth->createPermission('createOrder');
        $createOrder->description = 'Разрешение добавлять заказы';
        $auth->add($createOrder);
        $readOrder = $auth->createPermission('readOrder');
        $readOrder->description = 'Разрешение просматривать заказы';
        $auth->add($readOrder);
        $updateOrder = $auth->createPermission('updateOrder');
        $updateOrder->description = 'Разрешение редактировать заказы';
        $auth->add($updateOrder);
        $deleteOrder = $auth->createPermission('deleteOrder');
        $deleteOrder->description = 'Разрешение удалять заказы';
        $auth->add($deleteOrder);

        $rule = $auth->getRule('isAuthor');
        $updateOwnOrder = $auth->createPermission('updateOwnOrder');
        $updateOwnOrder->description = 'Разрешение редактировать созданные заказы';
        $updateOwnOrder->ruleName = $rule->name;
        $auth->add($updateOwnOrder);
        $auth->addChild($updateOwnOrder, $updateOrder);

        $orderReader = $auth->createRole('orderReader');
        $auth->add($orderReader);
        $auth->addChild($orderReader, $readOrder);

        $orderAuthor = $auth->createRole('orderAuthor');
        $auth->add($orderAuthor);
        $auth->addChild($orderAuthor, $orderReader);
        $auth->addChild($orderAuthor, $createOrder);
        $auth->addChild($orderAuthor, $updateOwnOrder);

        $orderManager = $auth->createRole('orderManager');
        $auth->add($orderManager);
        $auth->addChild($orderManager, $orderAuthor);
        $auth->addChild($orderManager, $updateOrder);

        $orderAdmin = $auth->createRole('orderAdmin');
        $auth->add($orderAdmin);
        $auth->addChild($orderAdmin, $orderManager);
        $auth->addChild($orderAdmin, $deleteOrder);

        $auth->addChild($admin, $orderAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createOrder = $auth->getPermission('createOrder');
        $readOrder = $auth->getPermission('readOrder');
        $updateOrder = $auth->getPermission('updateOrder');
        $deleteOrder = $auth->getPermission('deleteOrder');
        $updateOwnOrder = $auth->getPermission('updateOwnOrder');

        $orderReader = $auth->getRole('orderReader');
        $orderAuthor = $auth->getRole('orderAuthor');
        $orderManager = $auth->getRole('orderManager');
        $orderAdmin = $auth->getRole('orderAdmin');

        $auth->removeChild($admin, $orderAdmin);
        $auth->removeChildren($orderAdmin);
        $auth->removeChildren($orderManager);
        $auth->removeChildren($orderAuthor);
        $auth->removeChildren($orderReader);
        $auth->removeChildren($updateOwnOrder);

        $auth->remove($orderAdmin);
        $auth->remove($orderManager);
        $auth->remove($orderAuthor);
        $auth->remove($orderReader);
        $auth->remove($updateOwnOrder);
        $auth->remove($deleteOrder);
        $auth->remove($updateOrder);
        $auth->remove($readOrder);
        $auth->remove($createOrder);

        $this->delete('{{%item}}');

        $this->dropForeignKey('item_product_id','{{%item}}');
        $this->dropForeignKey('item_order_id','{{%item}}');
        $this->dropForeignKey('item_updater_id','{{%item}}');
        $this->dropForeignKey('item_author_id','{{%item}}');

        $this->dropTable('{{%item}}');

        $this->delete('{{%order}}');

        $this->dropForeignKey('order_website_id','{{%order}}');
        $this->dropForeignKey('order_manager_id','{{%order}}');
        $this->dropForeignKey('order_client_team_id','{{%order}}');
        $this->dropForeignKey('order_team_id','{{%order}}');
        $this->dropForeignKey('order_user_id','{{%order}}');
        $this->dropForeignKey('order_updater_id','{{%order}}');
        $this->dropForeignKey('order_author_id','{{%order}}');

        $this->dropTable('{{%order}}');
    }
}
