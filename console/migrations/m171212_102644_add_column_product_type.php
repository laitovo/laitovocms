<?php

use yii\db\Migration;

class m171212_102644_add_column_product_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_product_type}}', 'automatic', $this->boolean()->comment('Автопополение вкл/выкл'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_product_type}}', 'automatic');
    }
}
