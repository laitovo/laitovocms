<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_job_type`.
 */
class m170324_110628_create_laitovo_erp_job_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_job_type}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->comment('Наименование'),
            'location_id' => $this->integer()->comment('Участок'),
            'rate' => $this->float()->comment('Коэффициент'),
            
            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
        ], $tableOptions);
        
        $this->createIndex(
            'idx-job_type-location_id',
            '{{%laitovo_erp_job_type}}',
            'location_id'
        );

        $this->addForeignKey(
            'fk-job_type-location_id',
            '{{%laitovo_erp_job_type}}',
            'location_id',
            '{{%laitovo_erp_location}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-job_type-location_id', '{{%laitovo_erp_job_type}}');
        $this->dropIndex('idx-job_type-location_id', '{{%laitovo_erp_job_type}}');

        $this->dropTable('{{%laitovo_erp_job_type}}');
    }
}
