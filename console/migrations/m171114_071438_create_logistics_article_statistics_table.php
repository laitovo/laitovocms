<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_article_statistics`.
 */
class m171114_071438_create_logistics_article_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%logistics_article_statistics}}', [
            'id' => $this->primaryKey(),
            'article' => $this->string()->comment('Артикул'),
            //columns
            'created_at' => $this->integer()->comment('Дата учета'),
            //marker
            'type' => $this->integer()->comment('Вид статистики'),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%logistics_article_statistics}}');
    }
}
