<?php

use yii\db\Migration;

class m180731_034025_add_two_columns_logistics_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_order}}','export_username',$this->string()->comment('Контрагент(экспорт)'));
        $this->addColumn('{{%logistics_order}}','export_type',$this->boolean()->comment('Категория контрагента(экспорт)'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_order}}','export_type');
        $this->dropColumn('{{%logistics_order}}','export_username');
    }
}
