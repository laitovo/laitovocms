<?php

use yii\db\Schema;
use yii\db\Migration;

class m160325_135258_price extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%price}}', [
            'region_id' => $this->integer()->notNull()->comment('Регион'),
            'product_id' => $this->integer()->notNull()->comment('Товар'),

            'oldprice' => $this->money(19,2)->comment('Старая цена'),
            'price' => $this->money(19,2)->comment('Цена'),
            'currency' => $this->string()->comment('Валюта'),
            'nds' => $this->money()->comment('НДС %'),
            'nds_in' => $this->boolean()->defaultValue(true)->comment('НДС включен в цену'),
            'maxdiscount' => $this->money(19,2)->comment('Максимальная скидка'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),
            'PRIMARY KEY(region_id, product_id)'
        ], $tableOptions);

        $this->createIndex('idx_price_price','{{%price}}','price');

        $this->addForeignKey('fk_price_author_id','{{%price}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_price_updater_id','{{%price}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_price_region_id', '{{%price}}', 'region_id', '{{%region}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_price_product_id', '{{%price}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%course}}', [
            'currency_from' => $this->string()->comment('Валюта 1'),
            'currency_to' => $this->string()->comment('Валюта 2'),
            'k' => $this->money()->comment('Коэффициент'),

            'PRIMARY KEY(currency_from, currency_to)'
        ], $tableOptions);


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createPrice = $auth->createPermission('createPrice');
        $createPrice->description = 'Разрешение добавлять цены';
        $auth->add($createPrice);
        $readPrice = $auth->createPermission('readPrice');
        $readPrice->description = 'Разрешение просматривать цены';
        $auth->add($readPrice);
        $updatePrice = $auth->createPermission('updatePrice');
        $updatePrice->description = 'Разрешение редактировать цены';
        $auth->add($updatePrice);
        $deletePrice = $auth->createPermission('deletePrice');
        $deletePrice->description = 'Разрешение удалять цены';
        $auth->add($deletePrice);

        $rule = $auth->getRule('isAuthor');
        $updateOwnPrice = $auth->createPermission('updateOwnPrice');
        $updateOwnPrice->description = 'Разрешение редактировать созданные цены';
        $updateOwnPrice->ruleName = $rule->name;
        $auth->add($updateOwnPrice);
        $auth->addChild($updateOwnPrice, $updatePrice);

        $priceReader = $auth->createRole('priceReader');
        $auth->add($priceReader);
        $auth->addChild($priceReader, $readPrice);

        $priceAuthor = $auth->createRole('priceAuthor');
        $auth->add($priceAuthor);
        $auth->addChild($priceAuthor, $priceReader);
        $auth->addChild($priceAuthor, $createPrice);
        $auth->addChild($priceAuthor, $updateOwnPrice);

        $priceManager = $auth->createRole('priceManager');
        $auth->add($priceManager);
        $auth->addChild($priceManager, $priceAuthor);
        $auth->addChild($priceManager, $updatePrice);

        $priceAdmin = $auth->createRole('priceAdmin');
        $auth->add($priceAdmin);
        $auth->addChild($priceAdmin, $priceManager);
        $auth->addChild($priceAdmin, $deletePrice);

        $auth->addChild($admin, $priceAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createPrice = $auth->getPermission('createPrice');
        $readPrice = $auth->getPermission('readPrice');
        $updatePrice = $auth->getPermission('updatePrice');
        $deletePrice = $auth->getPermission('deletePrice');
        $updateOwnPrice = $auth->getPermission('updateOwnPrice');

        $priceReader = $auth->getRole('priceReader');
        $priceAuthor = $auth->getRole('priceAuthor');
        $priceManager = $auth->getRole('priceManager');
        $priceAdmin = $auth->getRole('priceAdmin');

        $auth->removeChild($admin, $priceAdmin);
        $auth->removeChildren($priceAdmin);
        $auth->removeChildren($priceManager);
        $auth->removeChildren($priceAuthor);
        $auth->removeChildren($priceReader);
        $auth->removeChildren($updateOwnPrice);

        $auth->remove($priceAdmin);
        $auth->remove($priceManager);
        $auth->remove($priceAuthor);
        $auth->remove($priceReader);
        $auth->remove($updateOwnPrice);
        $auth->remove($deletePrice);
        $auth->remove($updatePrice);
        $auth->remove($readPrice);
        $auth->remove($createPrice);

        $this->delete('{{%course}}');
        $this->dropTable('{{%course}}');


        $this->delete('{{%price}}');

        $this->dropForeignKey('fk_price_product_id','{{%price}}');
        $this->dropForeignKey('fk_price_region_id','{{%price}}');
        $this->dropForeignKey('fk_price_updater_id','{{%price}}');
        $this->dropForeignKey('fk_price_author_id','{{%price}}');

        $this->dropTable('{{%price}}');


    }

}
