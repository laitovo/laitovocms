<?php

use yii\db\Migration;

class m240327_061424_change_tmp_using_template_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%tmp_using_template}}','usingCount');

        $this->addColumn('{{%tmp_using_template}}','usingCount1', $this->integer()->notNull()->comment('Количество артикулов, изготовленных на лекале за 1 год'));
        $this->addColumn('{{%tmp_using_template}}','usingCount2', $this->integer()->notNull()->comment('Количество артикулов, изготовленных на лекале за 1 год'));
        $this->addColumn('{{%tmp_using_template}}','usingCount3', $this->integer()->notNull()->comment('Количество артикулов, изготовленных на лекале за 1 год'));
        $this->addColumn('{{%tmp_using_template}}','usingCount4', $this->integer()->notNull()->comment('Количество артикулов, изготовленных на лекале за 1 год'));
        $this->addColumn('{{%tmp_using_template}}','usingCount5', $this->integer()->notNull()->comment('Количество артикулов, изготовленных на лекале за 1 год'));

        $this->createIndex('idx_laitovo_erp_naryad_status_created_at','{{%laitovo_erp_naryad}}',['status','created_at']);

    }

    public function safeDown()
    {
        $this->dropIndex('idx_laitovo_erp_naryad_status_created_at','{{%laitovo_erp_naryad}}');

        $this->dropColumn('{{%tmp_using_template}}','usingCount1');
        $this->dropColumn('{{%tmp_using_template}}','usingCount2');
        $this->dropColumn('{{%tmp_using_template}}','usingCount3');
        $this->dropColumn('{{%tmp_using_template}}','usingCount4');
        $this->dropColumn('{{%tmp_using_template}}','usingCount5');

        $this->addColumn('{{%tmp_using_template}}','usingCount', $this->integer()->notNull()->comment('Количество артикулов, изготовленных на лекале'));
    }
}
