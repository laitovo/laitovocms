<?php

use yii\db\Migration;

class m180214_081910_add_column_to_order_entry extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','ready',$this->boolean()->defaultValue(0)->comment('Готово к упаковке'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','ready');
    }
}
