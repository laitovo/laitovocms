<?php

use yii\db\Migration;

class m201208_075827_add_relevance_report_table extends Migration
{
    protected $table = '{{%tmp_relevance_report}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table,
        [
            'id'          => $this->primaryKey(),
            'storage_id'  => $this->integer()->notNull()->comment('Склад'),
            'upn'         => $this->integer()->notNull()->comment('Позиция'),
            'article'     => $this->string()->notNull()->comment('Артикул'),
            'title'       => $this->string()->notNull()->comment('Наименование продукта'),
            'window'      => $this->string()->notNull()->comment('Оконный проём'),
            'cars'        => $this->text()->notNull()->comment('Автомобили, на которые подходит артикул'),
            'soldArticle' => $this->integer()->notNull()->comment('Продано артикулов за последние 2 года'),
            'soldWindow'    => $this->integer()->notNull()->comment('Продано оконных проемов за последние 2 года'),
            'soldCars'    => $this->integer()->notNull()->comment('Продано автомобилей за последние 2 года'),
            'inStock'     => $this->integer()->notNull()->comment('Сейчас на складе'),
        ],
        $this->tableOptions);

        //$this->addForeignKey('fk-name-id', $this->table, 'this_child_id', '{{%parent_table}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
