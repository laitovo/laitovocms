<?php

use yii\db\Migration;

class m210617_114701_add_column_prodGroup_to_erpNaryad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','prodGroup',$this->integer()->comment('Номер производственной группы нарядов (изготавливаются вместе)'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','prodGroup');
    }
}
