<?php

use yii\db\Migration;

class m180222_035449_add_packed_and_distributed_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','packed',$this->boolean()->comment('Упакована'));
        $this->addColumn('{{%logistics_order}}','shipped',$this->boolean()->comment('Отгружена'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','shipped');
        $this->dropColumn('{{%logistics_order}}','packed');

    }
}
