<?php

use yii\db\Migration;

class m180307_121440_create_table_uploaded_file extends Migration
{
    public function up()
    {
        $this->createTable('uploaded_file', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Сотрудник'),
            'team_id' => $this->integer()->comment('Команда'),
            'relative_path' => $this->string()->comment('Путь к файлу относительно папки uploads без начального слэша (например, test/test.jpg)'),
            'uploaded_at' => $this->integer()->comment('Дата загрузки файла'),
            'access' => $this->string()->comment('Права доступа на файл'),
        ]);
    }

    public function down()
    {
        $this->dropTable('uploaded_file');
    }
}
