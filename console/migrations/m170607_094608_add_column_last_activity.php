<?php

use yii\db\Migration;

class m170607_094608_add_column_last_activity extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_location_workplace}}', 'last_activity', $this->integer(11));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_location_workplace}}', 'last_activity');
    }
}
