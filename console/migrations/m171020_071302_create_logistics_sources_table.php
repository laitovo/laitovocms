<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_sources`.
 */
class m171020_071302_create_logistics_sources_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%logistics_sources}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(),
            'title' => $this->string(),
            'type' => $this->smallInteger(),
            'key' => $this->string(),
            //columns
            'created_at' => $this->integer(),
            'author_id' =>  $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_sources_team_id', '{{%logistics_sources}}', 'team_id', '{{%team}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_sources_author_id', '{{%logistics_sources}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_sources_updater_id', '{{%logistics_sources}}', 'updater_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_sources_updater_id', '{{%logistics_sources}}');
        $this->dropForeignKey('fk_logistics_sources_author_id', '{{%logistics_sources}}');
        $this->dropForeignKey('fk_logistics_sources_team_id', '{{%logistics_sources}}');

        $this->dropTable('{{%logistics_sources}}');
    }
}
