<?php

use yii\db\Migration;

/**
 * Handles the creation of table `production_literal_naryad`.
 */
class m210407_071345_create_production_literal_naryad_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%core_production_literal_naryad}}', [
            'prodLiteralId' => $this->string(5),
            'erpNaryadId' => $this->integer(),
            'locationId' => $this->integer(),
            'createdAt' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk_production_literal_naryad','{{%core_production_literal_naryad}}',['prodLiteralId','erpNaryadId']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%core_production_literal_naryad}}');
    }
}
