<?php

use yii\db\Migration;

class m200917_061018_add_tmp_semi_finished_articles_table extends Migration
{
    protected $table = '{{%tmp_semi_finished_articles}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table,
        [
            'id'         => $this->primaryKey(),
            'carArticle' => $this->integer()->notNull()->comment('Артикул автомобиля, для которого изготовлен полуфабрикат'),
            'mask'       => $this->string()->notNull()->comment('Маска артикула, для изготовления которого подойдет данная рамка'),
            'literal'    => $this->integer()->notNull()->comment('Литера'),
            'upnId'      => $this->integer()->comment('Наряд, на который был использован данный полуфабрикат'),
            'reservedAt' => $this->integer()->comment('Дата, когда был заерезервированный данный полуфабрикат под наряд'),
        ],
        $this->tableOptions);
    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
