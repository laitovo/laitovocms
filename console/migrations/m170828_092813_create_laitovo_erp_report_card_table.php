<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_report_card`.
 */
class m170828_092813_create_laitovo_erp_report_card_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%laitovo_erp_report_card}}', [
            'id' => $this->primaryKey(),
            'user_id'=> $this->integer(),
            'month_year'=>$this->date(),
            'json'=> $this->string()
        ]);


        $this->createIndex(
            'idx-laitovo_erp_report_card-user_id',
            'laitovo_erp_report_card',
            'user_id'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_report_card-user_id',
            '{{%laitovo_erp_report_card}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_report_card-user_id','laitovo_erp_report_card');

        $this->dropIndex('idx-laitovo_erp_report_card-user_id','laitovo_erp_report_card');

        $this->dropTable('laitovo_erp_report_card');
    }
}
