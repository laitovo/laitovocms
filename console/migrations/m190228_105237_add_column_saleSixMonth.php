<?php

use yii\db\Migration;

class m190228_105237_add_column_saleSixMonth extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_article_sale_statistics}}','saleSixMonth',$this->integer()->notNull()->after('salePrevious')->comment('Продано за последние 6 месяцев'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_article_sale_statistics}}','saleSixMonth');
    }
}
