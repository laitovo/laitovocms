<?php

use yii\db\Migration;

class m171222_060344_add_column_logistics_storage_literal extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_storage}}', 'literal', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_storage}}', 'literal');
    }
}
