<?php

use yii\db\Migration;

class m170619_112732_add_job_price_column_laitovo_erp_users_report_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_erp_users_report', 'job_price','integer');
        $this->addColumn('laitovo_erp_users_report', 'additional_naryad_id','integer');
        $this->addForeignKey(
            'fk-laitovo_erp_users_report-additional_naryad_id',
            '{{%laitovo_erp_users_report}}',
            'additional_naryad_id',
            '{{%laitovo_erp_additional_work}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_users_report-additional_naryad_id', '{{%laitovo_erp_users_report}}');
        $this->dropColumn('laitovo_erp_users_report', 'job_price');
        $this->dropColumn('laitovo_erp_users_report', 'additional_naryad_id');
    }

}
