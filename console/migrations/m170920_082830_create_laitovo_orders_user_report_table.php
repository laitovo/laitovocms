<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_orders_user_report`.
 */
class m170920_082830_create_laitovo_orders_user_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_orders_user_report', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'client_name' => $this->string(),
            'client_inn' => $this->string(),
            'order_id' => $this->integer(),
            'sum_order' => $this->double(),
            'created_at' => $this->integer(),
            'site' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('laitovo_orders_user_report');
    }
}
