<?php

use yii\db\Migration;

class m180626_131806_add_column_is_new_scheme_to_laitovo_erp_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_order}}','is_new_scheme',$this->boolean());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_order}}','is_new_scheme');
    }
}
