<?php

use yii\db\Migration;

class m180305_070301_add_barcode_column_for_order_entry extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order_entry}}','order_barcode',$this->string()->comment('Штрихкод для заказа'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order_entry}}','order_barcode');
    }
}
