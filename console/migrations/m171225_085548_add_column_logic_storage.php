<?php

use yii\db\Migration;

class m171225_085548_add_column_logic_storage extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_storage}}', 'logic', $this->string()->comment('Вид логики пополнения'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_storage}}', 'logic');
    }
}
