<?php

use yii\db\Migration;

class m190206_125035_create_table_laitovo_article_analog extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        }

        $this->createTable('{{%laitovo_article_analog}}', [
            'article' => $this->string()->notNull(),
            'analogArticle' =>$this->string()->notNull(),
            'date' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_laitovo_article_analog_aricle','{{%laitovo_article_analog}}',['article','analogArticle']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%laitovo_article_analog}}');
    }
}
