<?php

use yii\db\Migration;

class m210416_053834_add_column_stochka_to_naryad_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','stochka',$this->boolean()->defaultValue(false)->comment('Наряд сточен'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','stochka');
    }
}
