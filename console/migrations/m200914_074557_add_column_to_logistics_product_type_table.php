<?php

use yii\db\Migration;

class m200914_074557_add_column_to_logistics_product_type_table extends Migration
{
    protected $table = '{{%logistics_storage_product_type}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        $this->addColumn($this->table,'hide',$this->boolean()->notNull()->defaultValue(0)->comment('Не показывать в списке')->after('title'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table,'hide');
    }
}
