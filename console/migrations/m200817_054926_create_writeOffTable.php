<?php

use yii\db\Migration;

class m200817_054926_create_writeOffTable extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('core_write_off', [
            'id'                    => $this->primaryKey(),
            'clientId'              => $this->integer()->notNull()->comment('Уникальный идентификатор клиента в определнной локали'),
            'locale'                => 'ENUM("undefined","ru", "eu") NOT NULL',
            'documentId'            => $this->integer()->notNull()->comment('Номер документа в соответсвуйющей локали'),
            'date'                  => $this->integer()->notNull()->comment('Учетная дата поступления'),
            'type'                  => $this->string()->comment('Направление расхода'),
            'total'                 => $this->decimal(10,2)->notNull()->comment('Сумма списания'),
            'currency'              => 'ENUM("undefined","EUR", "RUB") NOT NULL'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('core_write_off');
    }
}
