<?php

use yii\db\Migration;

class m170316_092322_added_column_coefficient_to_laitovo_erp_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_user}}', 'factor', $this->smallInteger(6));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_user}}', 'factor');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
