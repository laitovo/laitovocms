<?php

use yii\db\Migration;

class m200813_070306_create_table_core_return_note extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('core_return_note', [
            'id'                    => $this->primaryKey(),
            'clientId'              => $this->integer()->notNull()->comment('Уникальный идентификатор клиента в определнной локали'),
            'locale'                => 'ENUM("undefined","ru", "eu") NOT NULL',
            'documentId'            => $this->integer()->notNull()->comment('Уникальный номер документа возврата в системе'),
            'date'                  => $this->integer()->notNull()->comment('Учетная дата возврата'),
            'sumWithoutNds'         => $this->decimal(10,2)->notNull()->comment('Сумма возврата без НДС'),
            'amountNds'             => $this->decimal(10,2)->notNull()->comment('Значение налога НДС в %'),
            'sumNds'                => $this->decimal(10,2)->notNull()->comment('Сумма налога НДС с возврате'),
            'sumWithNds'            => $this->decimal(10,2)->notNull()->comment('Сумма реализации с учетом НДС'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('core_return_note');
    }
}
