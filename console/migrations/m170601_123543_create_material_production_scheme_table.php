<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_production_scheme`.
 */
class m170601_123543_create_material_production_scheme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%material_production_scheme}}', [
            'id' => $this->primaryKey(),
            'production_scheme_id' => $this->integer()->comment('Производственная схема'),
            'location_id' => $this->integer()->comment('Участок'),
            'material_parameter_id' => $this->integer()->comment('Материалы'),
            'material_count' => $this->double()->comment('Количество материала'),
            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор создания'),
            'updater_id' => $this->integer()->comment('Автор изменения'),
        ]);
        
        $this->addForeignKey('fk_material_production_scheme_type_location_id', '{{%material_production_scheme}}', 'location_id', '{{%laitovo_erp_location}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_material_production_scheme_type_material_parameter_id', '{{%material_production_scheme}}', 'material_parameter_id', '{{%material_parameters}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_material_production_scheme_type_production_scheme_id', '{{%material_production_scheme}}', 'production_scheme_id', '{{%production_scheme}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_material_production_scheme_type_author_id', '{{%material_production_scheme}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_material_production_scheme_type_updater_id', '{{%material_production_scheme}}', 'updater_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
         
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_material_production_scheme_type_location_id', '{{%material_production_scheme}}');
        $this->dropForeignKey('fk_material_production_scheme_type_material_parameter_id', '{{%material_production_scheme}}');
        $this->dropForeignKey('fk_material_production_scheme_type_production_scheme_id', '{{%material_production_scheme}}');
        $this->dropForeignKey('fk_material_production_scheme_type_author_id', '{{%material_production_scheme}}');
        $this->dropForeignKey('fk_material_production_scheme_type_updater_id', '{{%material_production_scheme}}');
        $this->dropTable('material_production_scheme');
    }
}
