<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_users_report`.
 */
class m170324_133541_create_laitovo_erp_users_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_users_report}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Работник'),
            'location_id' => $this->integer()->comment('Участок'),
            'naryad_id' => $this->integer()->comment('Наряд'),
            'job_type_id' => $this->integer()->comment('Вид работ'),
            'job_count' => $this->integer()->defaultValue(0)->comment('Количество'),
            'job_rate' => $this->float()->comment('Коэффициент'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
        ], $tableOptions);

        $this->createIndex(
            'idx-users_report-user_id',
            '{{%laitovo_erp_users_report}}',
            'user_id'
        );

        $this->addForeignKey(
            'fk-users_report-user_id',
            '{{%laitovo_erp_users_report}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );


        $this->createIndex(
            'idx-users_report-location_id',
            '{{%laitovo_erp_users_report}}',
            'location_id'
        );
        
        $this->addForeignKey(
            'fk-users_report-location_id',
            '{{%laitovo_erp_users_report}}',
            'location_id',
            '{{%laitovo_erp_location}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-users_report-naryad_id',
            '{{%laitovo_erp_users_report}}',
            'naryad_id'
        );

        $this->addForeignKey(
            'fk-users_report-naryad_id',
            '{{%laitovo_erp_users_report}}',
            'naryad_id',
            '{{%laitovo_erp_naryad}}',
            'id',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-users_report-job_type_id',
            '{{%laitovo_erp_users_report}}',
            'job_type_id'
        );

        $this->addForeignKey(
            'fk-users_report-job_type_id',
            '{{%laitovo_erp_users_report}}',
            'job_type_id',
            '{{%laitovo_erp_job_type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-users_report-user_id', '{{%laitovo_erp_users_report}}');
        $this->dropIndex('idx-users_report-user_id', '{{%laitovo_erp_users_report}}');

        $this->dropForeignKey('fk-users_report-naryad_id', '{{%laitovo_erp_users_report}}');
        $this->dropIndex('idx-users_report-naryad_id', '{{%laitovo_erp_users_report}}');

        $this->dropForeignKey('fk-users_report-location_id', '{{%laitovo_erp_users_report}}');
        $this->dropIndex('idx-users_report-location_id', '{{%laitovo_erp_users_report}}');

        $this->dropForeignKey('fk-users_report-job_type_id', '{{%laitovo_erp_users_report}}');
        $this->dropIndex('idx-users_report-job_type_id', '{{%laitovo_erp_users_report}}');

        $this->dropTable('{{%laitovo_erp_users_report}}');
    }
}
