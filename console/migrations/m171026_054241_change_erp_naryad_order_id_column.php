<?php

use yii\db\Migration;

class m171026_054241_change_erp_naryad_order_id_column extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_laitovo_erp_naryad_order_id', '{{%laitovo_erp_naryad}}');
        $this->alterColumn('{{%laitovo_erp_naryad}}', 'order_id', $this->integer()->comment('Заказ'));
        $this->addForeignKey('fk_laitovo_erp_naryad_order_id', '{{%laitovo_erp_naryad}}', 'order_id', '{{%laitovo_erp_order}}','id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_naryad_order_id', '{{%laitovo_erp_naryad}}');
        $this->alterColumn('{{%laitovo_erp_naryad}}', 'order_id', $this->integer()->notNull()->comment('Заказ'));
        $this->addForeignKey('fk_laitovo_erp_naryad_order_id', '{{%laitovo_erp_naryad}}', 'order_id', '{{%laitovo_erp_order}}','id', 'CASCADE', 'CASCADE');
    }
}
