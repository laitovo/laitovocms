<?php

use yii\db\Migration;

class m200701_075040_add_column_to_shipment extends Migration
{
    protected $table = '{{%logistics_shipment}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        parent::safeUp();

        $this->addColumn($this->table,'documentsArePrinted',$this->boolean()->notNull()->defaultValue(false)->comment('Документы распечатаны'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table,'documentsArePrinted');
    }
}
