<?php

use yii\db\Schema;
use yii\db\Migration;

class m160510_085039_organization extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%organization}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull()->comment('Команда'),

            'name' => $this->string()->comment('Наименование'),

            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('organization_author_id','{{%organization}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('organization_updater_id','{{%organization}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('organization_team_id','{{%organization}}','team_id','{{%team}}','id','CASCADE','CASCADE');

        $this->addForeignKey('order_organization_id','{{%order}}','organization_id','{{%organization}}','id','SET NULL','CASCADE');
        $this->addForeignKey('order_client_organization_id','{{%order}}','client_organization_id','{{%organization}}','id','SET NULL','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createOrganization = $auth->createPermission('createOrganization');
        $createOrganization->description = 'Разрешение добавлять организации';
        $auth->add($createOrganization);
        $readOrganization = $auth->createPermission('readOrganization');
        $readOrganization->description = 'Разрешение просматривать организации';
        $auth->add($readOrganization);
        $updateOrganization = $auth->createPermission('updateOrganization');
        $updateOrganization->description = 'Разрешение редактировать организации';
        $auth->add($updateOrganization);
        $deleteOrganization = $auth->createPermission('deleteOrganization');
        $deleteOrganization->description = 'Разрешение удалять организации';
        $auth->add($deleteOrganization);

        $rule = $auth->getRule('isAuthor');
        $updateOwnOrganization = $auth->createPermission('updateOwnOrganization');
        $updateOwnOrganization->description = 'Разрешение редактировать созданные организации';
        $updateOwnOrganization->ruleName = $rule->name;
        $auth->add($updateOwnOrganization);
        $auth->addChild($updateOwnOrganization, $updateOrganization);

        $organizationReader = $auth->createRole('organizationReader');
        $auth->add($organizationReader);
        $auth->addChild($organizationReader, $readOrganization);

        $organizationAuthor = $auth->createRole('organizationAuthor');
        $auth->add($organizationAuthor);
        $auth->addChild($organizationAuthor, $organizationReader);
        $auth->addChild($organizationAuthor, $createOrganization);
        $auth->addChild($organizationAuthor, $updateOwnOrganization);

        $organizationManager = $auth->createRole('organizationManager');
        $auth->add($organizationManager);
        $auth->addChild($organizationManager, $organizationAuthor);
        $auth->addChild($organizationManager, $updateOrganization);

        $organizationAdmin = $auth->createRole('organizationAdmin');
        $auth->add($organizationAdmin);
        $auth->addChild($organizationAdmin, $organizationManager);
        $auth->addChild($organizationAdmin, $deleteOrganization);

        $auth->addChild($admin, $organizationAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createOrganization = $auth->getPermission('createOrganization');
        $readOrganization = $auth->getPermission('readOrganization');
        $updateOrganization = $auth->getPermission('updateOrganization');
        $deleteOrganization = $auth->getPermission('deleteOrganization');
        $updateOwnOrganization = $auth->getPermission('updateOwnOrganization');

        $organizationReader = $auth->getRole('organizationReader');
        $organizationAuthor = $auth->getRole('organizationAuthor');
        $organizationManager = $auth->getRole('organizationManager');
        $organizationAdmin = $auth->getRole('organizationAdmin');

        $auth->removeChild($admin, $organizationAdmin);
        $auth->removeChildren($organizationAdmin);
        $auth->removeChildren($organizationManager);
        $auth->removeChildren($organizationAuthor);
        $auth->removeChildren($organizationReader);
        $auth->removeChildren($updateOwnOrganization);

        $auth->remove($organizationAdmin);
        $auth->remove($organizationManager);
        $auth->remove($organizationAuthor);
        $auth->remove($organizationReader);
        $auth->remove($updateOwnOrganization);
        $auth->remove($deleteOrganization);
        $auth->remove($updateOrganization);
        $auth->remove($readOrganization);
        $auth->remove($createOrganization);

        $this->delete('{{%organization}}');

        $this->dropForeignKey('order_organization_id','{{%order}}');
        $this->dropForeignKey('order_client_organization_id','{{%order}}');

        $this->dropForeignKey('organization_team_id','{{%organization}}');
        $this->dropForeignKey('organization_updater_id','{{%organization}}');
        $this->dropForeignKey('organization_author_id','{{%organization}}');

        $this->dropTable('{{%organization}}');
    }
}
