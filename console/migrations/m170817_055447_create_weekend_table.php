<?php

use yii\db\Migration;

/**
 * Handles the creation of table `weekend`.
 */
class m170817_055447_create_weekend_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('weekend', [
            'id' => $this->primaryKey(),
            'weekend'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('weekend');
    }
}
