<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_171028_domain extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%domain}}', [
            'id' => $this->primaryKey(),
            'website_id' => $this->integer()->notNull()->comment('Сайт'),
            'domain' => $this->string()->notNull()->comment('Домен'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),
            'main' => $this->boolean()->defaultValue(false)->comment('Основной'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'expires_at' => $this->integer()->comment('Срок действия'),
            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('domain_domain','{{%domain}}','domain',true);
        $this->createIndex('domain_status','{{%domain}}','domain, status');
        $this->createIndex('domain_main','{{%domain}}','website_id, main, status');

        $this->createIndex('domain_expires_at','{{%domain}}','expires_at');

        $this->addForeignKey('domain_author_id','{{%domain}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('domain_updater_id','{{%domain}}','updater_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('domain_website_id','{{%domain}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createDomain = $auth->createPermission('createDomain');
        $createDomain->description = 'Разрешение добавлять домены';
        $auth->add($createDomain);
        $readDomain = $auth->createPermission('readDomain');
        $readDomain->description = 'Разрешение просматривать домены';
        $auth->add($readDomain);
        $updateDomain = $auth->createPermission('updateDomain');
        $updateDomain->description = 'Разрешение редактировать домены';
        $auth->add($updateDomain);
        $deleteDomain = $auth->createPermission('deleteDomain');
        $deleteDomain->description = 'Разрешение удалять домены';
        $auth->add($deleteDomain);

        $rule = $auth->getRule('isAuthor');
        $updateOwnDomain = $auth->createPermission('updateOwnDomain');
        $updateOwnDomain->description = 'Разрешение редактировать созданные домены';
        $updateOwnDomain->ruleName = $rule->name;
        $auth->add($updateOwnDomain);
        $auth->addChild($updateOwnDomain, $updateDomain);

        $domainReader = $auth->createRole('domainReader');
        $auth->add($domainReader);
        $auth->addChild($domainReader, $readDomain);

        $domainAuthor = $auth->createRole('domainAuthor');
        $auth->add($domainAuthor);
        $auth->addChild($domainAuthor, $domainReader);
        $auth->addChild($domainAuthor, $createDomain);
        $auth->addChild($domainAuthor, $updateOwnDomain);

        $domainManager = $auth->createRole('domainManager');
        $auth->add($domainManager);
        $auth->addChild($domainManager, $domainAuthor);
        $auth->addChild($domainManager, $updateDomain);

        $domainAdmin = $auth->createRole('domainAdmin');
        $auth->add($domainAdmin);
        $auth->addChild($domainAdmin, $domainManager);
        $auth->addChild($domainAdmin, $deleteDomain);

        $auth->addChild($admin, $domainAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createDomain = $auth->getPermission('createDomain');
        $readDomain = $auth->getPermission('readDomain');
        $updateDomain = $auth->getPermission('updateDomain');
        $deleteDomain = $auth->getPermission('deleteDomain');
        $updateOwnDomain = $auth->getPermission('updateOwnDomain');

        $domainReader = $auth->getRole('domainReader');
        $domainAuthor = $auth->getRole('domainAuthor');
        $domainManager = $auth->getRole('domainManager');
        $domainAdmin = $auth->getRole('domainAdmin');

        $auth->removeChild($admin, $domainAdmin);
        $auth->removeChildren($domainAdmin);
        $auth->removeChildren($domainManager);
        $auth->removeChildren($domainAuthor);
        $auth->removeChildren($domainReader);
        $auth->removeChildren($updateOwnDomain);

        $auth->remove($domainAdmin);
        $auth->remove($domainManager);
        $auth->remove($domainAuthor);
        $auth->remove($domainReader);
        $auth->remove($updateOwnDomain);
        $auth->remove($deleteDomain);
        $auth->remove($updateDomain);
        $auth->remove($readDomain);
        $auth->remove($createDomain);

        $this->delete('{{%domain}}');

        $this->dropForeignKey('domain_website_id','{{%domain}}');
        $this->dropForeignKey('domain_updater_id','{{%domain}}');
        $this->dropForeignKey('domain_author_id','{{%domain}}');

        $this->dropTable('{{%domain}}');
    }
}
