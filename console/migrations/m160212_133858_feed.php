<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_133858_feed extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%feed}}', [
            'id' => $this->primaryKey(),
            'website_id' => $this->integer()->notNull()->comment('Сайт'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'count_on_page' => $this->integer()->defaultValue(24)->comment('Количество новостей на странице'),
            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('feed_website_id','{{%feed}}','website_id, status');
        $this->createIndex('feed_sort','{{%feed}}','sort');

        $this->addForeignKey('feed_author_id','{{%feed}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('feed_updater_id','{{%feed}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('feed_website_id','{{%feed}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $this->addColumn('{{%alias}}', 'feed_id', $this->integer()->comment('Лента новостей'));
        $this->addForeignKey('alias_feed_id','{{%alias}}','feed_id','{{%feed}}','id','CASCADE','CASCADE');

        $this->createTable('{{%feed_lang}}', [
            'feed_id' => $this->integer()->notNull()->comment('Лента'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),

            'image' => $this->string()->comment('Изображение'),
            'anons' => $this->text()->comment('Анонс'),
            'content' => $this->text()->comment('Контент'),
            'PRIMARY KEY(feed_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('feed_lang_feed_id','{{%feed_lang}}','feed_id','{{%feed}}','id','CASCADE','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createFeed = $auth->createPermission('createFeed');
        $createFeed->description = 'Разрешение добавлять ленту';
        $auth->add($createFeed);
        $readFeed = $auth->createPermission('readFeed');
        $readFeed->description = 'Разрешение просматривать ленту';
        $auth->add($readFeed);
        $updateFeed = $auth->createPermission('updateFeed');
        $updateFeed->description = 'Разрешение редактировать ленту';
        $auth->add($updateFeed);
        $deleteFeed = $auth->createPermission('deleteFeed');
        $deleteFeed->description = 'Разрешение удалять ленту';
        $auth->add($deleteFeed);

        $rule = $auth->getRule('isAuthor');
        $updateOwnFeed = $auth->createPermission('updateOwnFeed');
        $updateOwnFeed->description = 'Разрешение редактировать созданную ленту';
        $updateOwnFeed->ruleName = $rule->name;
        $auth->add($updateOwnFeed);
        $auth->addChild($updateOwnFeed, $updateFeed);

        $feedReader = $auth->createRole('feedReader');
        $auth->add($feedReader);
        $auth->addChild($feedReader, $readFeed);

        $feedAuthor = $auth->createRole('feedAuthor');
        $auth->add($feedAuthor);
        $auth->addChild($feedAuthor, $feedReader);
        $auth->addChild($feedAuthor, $createFeed);
        $auth->addChild($feedAuthor, $updateOwnFeed);

        $feedManager = $auth->createRole('feedManager');
        $auth->add($feedManager);
        $auth->addChild($feedManager, $feedAuthor);
        $auth->addChild($feedManager, $updateFeed);

        $feedAdmin = $auth->createRole('feedAdmin');
        $auth->add($feedAdmin);
        $auth->addChild($feedAdmin, $feedManager);
        $auth->addChild($feedAdmin, $deleteFeed);

        $auth->addChild($admin, $feedAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createFeed = $auth->getPermission('createFeed');
        $readFeed = $auth->getPermission('readFeed');
        $updateFeed = $auth->getPermission('updateFeed');
        $deleteFeed = $auth->getPermission('deleteFeed');
        $updateOwnFeed = $auth->getPermission('updateOwnFeed');

        $feedReader = $auth->getRole('feedReader');
        $feedAuthor = $auth->getRole('feedAuthor');
        $feedManager = $auth->getRole('feedManager');
        $feedAdmin = $auth->getRole('feedAdmin');

        $auth->removeChild($admin, $feedAdmin);
        $auth->removeChildren($feedAdmin);
        $auth->removeChildren($feedManager);
        $auth->removeChildren($feedAuthor);
        $auth->removeChildren($feedReader);
        $auth->removeChildren($updateOwnFeed);

        $auth->remove($feedAdmin);
        $auth->remove($feedManager);
        $auth->remove($feedAuthor);
        $auth->remove($feedReader);
        $auth->remove($updateOwnFeed);
        $auth->remove($deleteFeed);
        $auth->remove($updateFeed);
        $auth->remove($readFeed);
        $auth->remove($createFeed);

        $this->delete('{{%feed}}');

        $this->dropForeignKey('feed_lang_feed_id','{{%feed_lang}}');
        $this->dropTable('{{%feed_lang}}');

        $this->dropForeignKey('alias_feed_id','{{%alias}}');
        $this->dropColumn('{{%alias}}', 'feed_id');

        $this->dropForeignKey('feed_website_id','{{%feed}}');
        $this->dropForeignKey('feed_updater_id','{{%feed}}');
        $this->dropForeignKey('feed_author_id','{{%feed}}');

        $this->dropTable('{{%feed}}');
    }
}
