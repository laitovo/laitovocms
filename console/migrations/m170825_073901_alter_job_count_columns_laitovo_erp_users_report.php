<?php

use yii\db\Migration;

class m170825_073901_alter_job_count_columns_laitovo_erp_users_report extends Migration
{
    public function up()
    {
        $this->alterColumn('laitovo_erp_users_report', 'job_count', 'double');
    }

    public function down()
    {
        $this->alterColumn('laitovo_erp_users_report', 'job_count', 'integer');
    }
}
