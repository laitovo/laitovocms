<?php

use yii\db\Migration;

class m191022_064714_add_column_rate_to_invoice_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%europe_invoice}}','rate',$this->double()->after('basedOn')->comment('Курс валют'));
    }

    public function down()
    {
        $this->dropColumn('{{%europe_invoice}}','rate');

    }
}
