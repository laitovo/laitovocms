<?php

use yii\db\Migration;

class m170620_112207_add_position_scheme_id_from_laitovo_erp_job_scheme_table extends Migration
{
    public function up()
    {
       $this->addColumn('laitovo_erp_job_scheme', 'scheme_id','integer');
       $this->addForeignKey(
            'fk-job_sheme-scheme_id',
            '{{%laitovo_erp_job_scheme}}',
            'scheme_id',
            '{{%laitovo_erp_scheme}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-job_sheme-scheme_id', '{{%laitovo_erp_job_scheme}}');
        $this->dropColumn('laitovo_erp_job_scheme', 'scheme_id');
    }

}
