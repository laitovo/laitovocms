<?php

use yii\db\Migration;

class m171108_071407_alter_column_order_id_storage_table extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_logistics_storage_log_order_id', '{{%logistics_storage_log}}');
        $this->dropForeignKey('fk_logistics_storage_state_order_id', '{{%logistics_storage_state}}');

        $this->renameColumn('{{%logistics_storage_state}}', 'order_id', 'reserved_id');
        $this->renameColumn('{{%logistics_storage_log}}', 'order_id', 'reserved_id');

        $this->addForeignKey('fk_logistics_storage_state_reserved_id', '{{%logistics_storage_state}}', 
            'reserved_id', '{{%logistics_order_entry}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_log_reserved_id', '{{%logistics_storage_log}}', 
            'reserved_id', '{{%logistics_order_entry}}', 'id', 
            'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_logistics_storage_log_reserved_id', '{{%logistics_storage_log}}');
        $this->dropForeignKey('fk_logistics_storage_state_reserved_id', '{{%logistics_storage_state}}');

        $this->renameColumn('{{%logistics_storage_state}}', 'reserved_id', 'order_id');
        $this->renameColumn('{{%logistics_storage_log}}', 'reserved_id', 'order_id');

        $this->addForeignKey('fk_logistics_storage_state_order_id', '{{%logistics_storage_state}}', 
            'order_id', '{{%logistics_order_entry}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_log_order_id', '{{%logistics_storage_log}}', 
            'order_id', '{{%logistics_order_entry}}', 'id', 
            'SET NULL', 'CASCADE');
    }
}
