<?php

use yii\db\Migration;

class m170929_062754_recreate_relation_table extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_laitovo_erp_product_group_relation_group_id', '{{%laitovo_erp_product_group_relation}}');
        $this->dropForeignKey('fk_laitovo_erp_product_group_relation_product_id', '{{%laitovo_erp_product_group_relation}}');
        
        $this->dropTable('{{%laitovo_erp_product_group_relation}}');
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_product_group_relation}}', [
            'group_id' => $this->integer(),
            'product_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_product_group_relation_group_id', 
            '{{%laitovo_erp_product_group_relation}}', 'group_id',
            '{{%laitovo_erp_product_group}}', 'id',
            'SET NULL',
            'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_product_group_relation_product_id',
            '{{%laitovo_erp_product_group_relation}}', 'product_id',
            '{{%laitovo_erp_product_type}}', 'id',
            'SET NULL',
            'CASCADE');
    }

    public function down()
    {
        echo "m170929_062754_recreate_relation_table cannot be reverted.\n";

        return false;
    }
}
