<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_location`.
 */
class m170524_061642_create_material_location_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('material_location', [
            
            'material_parameters_id' => $this->integer(),
            'laitovo_erp_location_id' => $this->integer(),
            
        ]);
         $this->createIndex(
            'idx-material_location-material_parameters_id',
            'material_location',
            'material_parameters_id'
        );

        // add foreign key for table `material_parameters`
        $this->addForeignKey(
            'fk-material_location-material_parameters_id',
            'material_location',
            'material_parameters_id',
            'material_parameters',
            'id',
            'CASCADE'
        );

        // creates index for column `laitovo_erp_location_id`
        $this->createIndex(
            'idx-material_location-laitovo_erp_location_id',
            'material_location',
            'laitovo_erp_location_id'
        );

        // add foreign key for table `laitovo_erp_location`
        $this->addForeignKey(
            'fk-material_location-laitovo_erp_location_id',
            'material_location',
            'laitovo_erp_location_id',
            'laitovo_erp_location',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
         // drops foreign key for table `material_parameters`
        $this->dropForeignKey(
            'fk-material_location-material_parameters_id',
            'material_location'
        );

        // drops index for column `material_parameters_id`
        $this->dropIndex(
            'idx-material_location-material_parameters_id',
            'material_location'
        );

        // drops foreign key for table `laitovo_erp_location`
        $this->dropForeignKey(
            'fk-material_location-laitovo_erp_location_id',
            'material_location'
        );

        // drops index for column `laitovo_erp_location_id`
        $this->dropIndex(
            'idx-material_location-laitovo_erp_location_id',
            'material_location'
        );
        $this->dropTable('material_location');
    }
}
