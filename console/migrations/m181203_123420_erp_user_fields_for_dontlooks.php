<?php

use yii\db\Migration;

class m181203_123420_erp_user_fields_for_dontlooks extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_erp_user}}','canCreateTemplate',$this->boolean()->defaultValue(false)->comment('Может строить лекало на швейке для продуктов'));
        $this->addColumn('{{%laitovo_erp_user}}','canCreateDontlook',$this->boolean()->defaultValue(false)->comment('Может изготавливать донтлуки'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_erp_user}}','canCreateTemplate');
        $this->dropColumn('{{%laitovo_erp_user}}','canCreateDontlook');
    }
}
