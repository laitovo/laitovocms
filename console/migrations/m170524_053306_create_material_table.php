<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material`.
 */
class m170524_053306_create_material_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('material', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('material');
    }
}
