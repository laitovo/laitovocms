<?php

use yii\db\Migration;

class m170925_103828_add_role_columns_laitovo_orders_user_report_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_orders_user_report', 'role', 'string');
        $this->addColumn('laitovo_orders_user_report', 'region_name', 'string');
    }

    public function down()
    {
        $this->dropColumn('laitovo_orders_user_report', 'role');
        $this->dropColumn('laitovo_orders_user_report', 'region_name');
    }
}
