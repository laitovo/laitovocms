<?php

use yii\db\Migration;

class m170809_081235_alter_job_price_column_laitovo_erp_users_report_table extends Migration
{
    public function up()
    {
        $this->alterColumn('laitovo_erp_users_report', 'job_price', 'double');
    }

    public function down()
    {
        $this->alterColumn('laitovo_erp_users_report', 'job_price', 'integer');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
