<?php

use yii\db\Migration;

class m200806_100435_add_column_currenncy_to_receipt_table extends Migration
{
    protected $table = '{{%core_receipt}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        $this->addColumn($this->table,'currency','ENUM("undefined","EUR", "RUB") NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table,'currency');
    }
}
