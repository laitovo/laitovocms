<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_location_workplace_property`.
 */
class m170608_110522_create_laitovo_erp_location_workplace_property_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_location_workplace_property}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'title' => $this->string(),
            //columns
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),

            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('laitovo_erp_location_workplace_property_type_id',
            '{{%laitovo_erp_location_workplace_property}}', 'type_id',
            '{{%laitovo_erp_location_workplace_type}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_location_workplace_property_author_id',
            '{{%laitovo_erp_location_workplace_property}}', 'author_id',
            '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_location_workplace_property_updater_id',
            '{{%laitovo_erp_location_workplace_property}}', 'updater_id',
            '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_property_updater_id', '{{%laitovo_erp_location_workplace_property}}');

        $this->dropForeignKey('fk_laitovo_erp_location_workplace_property_author_id', '{{%laitovo_erp_location_workplace_property}}');

        $this->dropForeignKey('laitovo_erp_location_workplace_property_type_id', '{{%laitovo_erp_location_workplace_property}}');

        $this->dropTable('{{%laitovo_erp_location_workplace_property}}');
    }
}
