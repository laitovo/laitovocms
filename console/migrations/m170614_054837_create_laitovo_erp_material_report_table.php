<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_material_report`.
 */
class m170614_054837_create_laitovo_erp_material_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_erp_material_report', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Работник'),
            'location_id' => $this->integer()->comment('Участок'),
            'naryad_id' => $this->integer()->comment('Наряд'),
            'material_parameter_id' => $this->integer()->comment('Вид материала'),
            'material_count' => $this->double()->defaultValue(0)->comment('Количество материала'),
            

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
        ]);
        
         $this->addForeignKey(
            'fk-laitovo_erp_material_report-user_id',
            '{{%laitovo_erp_material_report}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_material_report-location_id',
            '{{%laitovo_erp_material_report}}',
            'location_id',
            '{{%laitovo_erp_location}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_material_report-naryad_id',
            '{{%laitovo_erp_material_report}}',
            'naryad_id',
            '{{%laitovo_erp_naryad}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_material_report-material_parameter_id',
            '{{%laitovo_erp_material_report}}',
            'material_parameter_id',
            '{{%material_parameters}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_material_report-user_id', '{{%laitovo_erp_material_report}}');
        
        $this->dropForeignKey('fk-laitovo_erp_material_report-naryad_id', '{{%laitovo_erp_material_report}}');
        
        $this->dropForeignKey('fk-laitovo_erp_material_report-location_id', '{{%laitovo_erp_material_report}}');
       
        $this->dropForeignKey('fk-laitovo_erp_material_report-material_parameter_id', '{{%laitovo_erp_material_report}}');
       
        $this->dropTable('laitovo_erp_material_report');
    }
}
