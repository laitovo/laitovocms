<?php

use yii\db\Migration;

class m221117_102341_add_column_to_erp_naryad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','addGroup',$this->integer()->comment('Номер производственной группы нарядов на дополнительную продукцию (изготавливаются вместе)'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','addGroup');
    }
}
