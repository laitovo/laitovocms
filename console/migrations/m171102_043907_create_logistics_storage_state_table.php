<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_storage_state`.
 */
class m171102_043907_create_logistics_storage_state_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%logistics_storage_state}}', [
            'id'         => $this->primaryKey(),
            'storage_id' => $this->integer()->comment('Место хранения'),
            'literal'    => $this->string()->comment('Литера'),
            'upn_id'     => $this->integer()->comment('Уникальный номер продукта'),
            'reserved'   => $this->boolean()->comment('Зарезервирован'),
            'order_id'   => $this->integer()->comment('Для кого зарезервирован'),

            //columns
            'created_at' => $this->integer(),
            'author_id'  => $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_storage_state_storage_id', 
            '{{%logistics_storage_state}}', 'storage_id', 
            '{{%logistics_storage}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_state_upn_id',
            '{{%logistics_storage_state}}', 'upn_id', 
            '{{%logistics_naryad}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_state_order_id',   
            '{{%logistics_storage_state}}', 'order_id',   
            '{{%logistics_order}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_state_author_id',  
            '{{%logistics_storage_state}}', 'author_id',  
            '{{%user}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_state_updater_id', 
            '{{%logistics_storage_state}}', 'updater_id', 
            '{{%user}}', 'id', 
            'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_storage_state_updater_id', '{{%logistics_storage_state}}');
        $this->dropForeignKey('fk_logistics_storage_state_author_id', '{{%logistics_storage_state}}');
        $this->dropForeignKey('fk_logistics_storage_state_order_id', '{{%logistics_storage_state}}');
        $this->dropForeignKey('fk_logistics_storage_state_upn_id', '{{%logistics_storage_state}}');
        $this->dropForeignKey('fk_logistics_storage_state_storage_id', '{{%logistics_storage_state}}');

        $this->dropTable('{{%logistics_storage_state}}');
    }
}
