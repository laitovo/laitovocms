<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_job_scheme`.
 */
class m170324_112133_create_laitovo_erp_job_scheme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_job_scheme}}', [
            'id' => $this->primaryKey(),
            'location_id' => $this->integer()->comment('Участок'),
            'type_id' => $this->integer()->comment('Вид работ'),
            'scheme_id' => $this->integer()->comment('Схема'),
            'job_count' => $this->integer()->defaultValue(0)->comment('Количество работ'),
            
            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
        ], $tableOptions);

        $this->createIndex(
            'idx-job_sheme-location_id',
            '{{%laitovo_erp_job_scheme}}',
            'location_id'
        );

        $this->addForeignKey(
            'fk-job_sheme-location_id',
            '{{%laitovo_erp_job_scheme}}',
            'location_id',
            '{{%laitovo_erp_location}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-job_sheme-type_id',
            '{{%laitovo_erp_job_scheme}}',
            'type_id'
        );

        $this->addForeignKey(
            'fk-job_sheme-type_id',
            '{{%laitovo_erp_job_scheme}}',
            'type_id',
            '{{%laitovo_erp_job_type}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-job_location_rate-location_id',
            '{{%laitovo_erp_job_scheme}}',
            'location_id'
        );

        $this->addForeignKey(
            'fk-job_sheme-scheme_id',
            '{{%laitovo_erp_job_scheme}}',
            'scheme_id',
            '{{%laitovo_erp_scheme}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-job_sheme-location_id', '{{%laitovo_erp_job_scheme}}');
        $this->dropIndex('idx-job_sheme-location_id', '{{%laitovo_erp_job_scheme}}');
        
        $this->dropForeignKey('fk-job_sheme-type_id', '{{%laitovo_erp_job_scheme}}');
        $this->dropIndex('idx-job_sheme-type_id', '{{%laitovo_erp_job_scheme}}');
        
        $this->dropForeignKey('fk-job_sheme-scheme_id', '{{%laitovo_erp_job_scheme}}');
        $this->dropIndex('idx-job_sheme-scheme_id', '{{%laitovo_erp_job_scheme}}');

        $this->dropTable('{{%laitovo_erp_job_scheme}}');
    }
}
