<?php

use yii\db\Migration;

class m200115_090842_change_ean_table_migration extends Migration
{
    public function up()
    {
        $this->addColumn('{{%ean_registry}}', 'type', $this->string()->notNull()->comment('Тип ЗПС Лайтово или Симпл'));
    }

    public function down()
    {
        $this->dropColumn('{{%ean_registry}}', 'type');
    }
}
