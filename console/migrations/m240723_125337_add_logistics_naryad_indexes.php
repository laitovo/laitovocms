<?php

use yii\db\Migration;

class m240723_125337_add_logistics_naryad_indexes extends Migration
{
    public function safeUp()
    {
        $this->createIndex('idx_logistics_naryad_barcode','{{%logistics_naryad}}','barcode');


        //$this->addForeignKey('fk-name-id', $this->table, 'this_child_id', '{{%parent_table}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_logistics_naryad_barcode','{{%logistics_naryad}}');

    }
}
