<?php

use yii\db\Migration;

class m190904_080206_change_column_type_literal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%logistics_naryad}}','transferLiteral', $this->integer()->comment('Литера для перемещения склада'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%logistics_naryad}}','transferLiteral', $this->string()->comment('Литера для перемещения склада'));
    }
}
