<?php

use yii\db\Migration;

class m180510_092829_add_column_shipment_id_to_logistics_order_entry extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order_entry}}', 'shipment_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order_entry}}', 'shipment_id');
    }
}
