<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_job_type_rate`.
 */
class m170324_122735_create_laitovo_erp_job_type_rate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_job_type_rate}}', [
            'id' => $this->primaryKey(),

            'type_id' => $this->integer()->comment('Вид работ'),
            'user_id' => $this->integer()->comment('Работник'),
            'rate' => $this->float()->comment('Коэффициент'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
        ], $tableOptions);

        $this->createIndex(
            'idx-job_type_rate-type_id',
            '{{%laitovo_erp_job_type_rate}}',
            'type_id'
        );

        $this->addForeignKey(
            'fk-job_type_rate-type_id',
            '{{%laitovo_erp_job_type_rate}}',
            'type_id',
            '{{%laitovo_erp_job_type}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-job_type_rate-user_id',
            '{{%laitovo_erp_job_type_rate}}',
            'user_id'
        );

        $this->addForeignKey(
            'fk-job_type_rate-user_id',
            '{{%laitovo_erp_job_type_rate}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-job_type_rate-type_id', '{{%laitovo_erp_job_type_rate}}');
        $this->dropIndex('idx-job_type_rate-type_id', '{{%laitovo_erp_job_type_rate}}');

        $this->dropForeignKey('fk-job_type_rate-user_id', '{{%laitovo_erp_job_type_rate}}');
        $this->dropIndex('idx-job_type_rate-user_id', '{{%laitovo_erp_job_type_rate}}');

        $this->dropTable('{{%laitovo_erp_job_type_rate}}');
    }
}
