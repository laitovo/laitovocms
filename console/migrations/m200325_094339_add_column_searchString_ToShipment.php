<?php

use yii\db\Migration;

class m200325_094339_add_column_searchString_ToShipment extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_shipment}}','searchString',$this->text()->comment('Строка, по которой будет осуществлятся поиск'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_shipment}}','searchString');
    }
}
