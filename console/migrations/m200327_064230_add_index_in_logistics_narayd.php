<?php

use yii\db\Migration;

class m200327_064230_add_index_in_logistics_narayd extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey('fk_logistics_naryad_reserved_id_order_entry_id',
            '{{%logistics_naryad}}','reserved_id',
            '{{%logistics_order_entry}}','id','SET NULL','CASCADE');

        $this->createIndex('index_team_id_reserved_id','{{%logistics_naryad}}',['team_id','reserved_id']);
        $this->createIndex('index_team_id_id','{{%logistics_naryad}}',['team_id','id']);

        $this->addForeignKey('fk_order_entry_shipment_id_logistics_shipment_id',
            '{{%logistics_order_entry}}','shipment_id',
            '{{%logistics_shipment}}','id','SET NULL','CASCADE');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_entry_shipment_id_logistics_shipment_id','{{%logistics_order_entry}}');

        $this->dropIndex('index_team_id_id','{{%logistics_naryad}}');
        $this->dropIndex('index_team_id_reserved_id','{{%logistics_naryad}}');
        $this->dropForeignKey('fk_logistics_naryad_reserved_id_order_entry_id','{{%logistics_naryad}}');
    }
}
