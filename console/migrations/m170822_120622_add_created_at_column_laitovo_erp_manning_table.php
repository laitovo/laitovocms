<?php

use yii\db\Migration;

class m170822_120622_add_created_at_column_laitovo_erp_manning_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_erp_manning', 'created_at', 'integer');
    }

    public function down()
    {
        $this->dropColumn('laitovo_erp_manning', 'created_at');
    }
}
