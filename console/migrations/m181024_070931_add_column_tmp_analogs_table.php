<?php

use yii\db\Migration;

class m181024_070931_add_column_tmp_analogs_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tmp_analogs}}','windowStatus',$this->boolean()->defaultValue(false)->after('window')->comment('Освоено или нет окно'));
        $this->addColumn('{{%tmp_analogs}}','windowExists',$this->boolean()->defaultValue(false)->after('window')->comment('Существует окно или нет'));
    }

    public function down()
    {
        $this->dropColumn('{{%tmp_analogs}}','windowStatus');
        $this->dropColumn('{{%tmp_analogs}}','windowExists');
    }


}
