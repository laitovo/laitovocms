<?php

use yii\db\Migration;

class m180321_131306_add_columns_to_uploaded_file extends Migration
{
    public function up()
    {
        $this->addColumn('{{%uploaded_file}}', 'original_name', $this->string()->comment('Оригинальное имя, с которым загружался файл'));
        $this->addColumn('{{%uploaded_file}}', 'size', $this->integer()->comment('Размер файла в байтах'));
    }

    public function down()
    {
        $this->dropColumn('{{%uploaded_file}}','original_name');
        $this->dropColumn('{{%uploaded_file}}','size');
    }
}
