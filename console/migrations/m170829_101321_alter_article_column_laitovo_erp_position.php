<?php

use yii\db\Migration;

class m170829_101321_alter_article_column_laitovo_erp_position extends Migration
{
    public function up()
    {
        $this->alterColumn('laitovo_erp_position','article','string');
    }

    public function down()
    {
        $this->alterColumn('laitovo_erp_position','article','integer');

    }
}
