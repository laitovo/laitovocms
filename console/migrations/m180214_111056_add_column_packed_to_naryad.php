<?php

use yii\db\Migration;

class m180214_111056_add_column_packed_to_naryad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_naryad}}','packed',$this->boolean()->defaultValue(0)->comment('Упакован'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_naryad}}','packed');
    }
}
