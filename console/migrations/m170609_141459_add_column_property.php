<?php

use yii\db\Migration;

class m170609_141459_add_column_property extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_location_workplace}}', 'property', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_location_workplace}}', 'property');
    }
}
