<?php

use yii\db\Migration;

class m170507_071004_add_literal_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}', 'literal', $this->integer());
        $this->addColumn('{{%laitovo_erp_naryad}}', 'literal_date', $this->integer(11));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}', 'literal_date');
        $this->dropColumn('{{%laitovo_erp_naryad}}', 'literal');
    }
}
