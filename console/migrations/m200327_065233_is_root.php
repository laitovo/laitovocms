<?php

use yii\db\Migration;

class m200327_065233_is_root extends Migration
{
    protected $table = '{{%user}}';
    protected $tableOptions;

    public function safeUp()
    {
        $this->addColumn($this->table, 'is_root', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        return $this->dropColumn($this->table, 'is_root');
    }
}
