<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_group_product_relation`.
 */
class m170531_055906_create_laitovo_erp_group_product_relation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_product_group_relation}}', [
            'group_id' => $this->integer(),
            'product_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_product_group_relation_group_id', 
            '{{%laitovo_erp_product_group_relation}}', 'group_id',
            '{{%laitovo_erp_product_group}}', 'id',
            'SET NULL',
            'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_product_group_relation_product_id',
            '{{%laitovo_erp_product_group_relation}}', 'product_id',
            '{{%laitovo_erp_product_type}}', 'id',
            'SET NULL',
            'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_product_group_relation_group_id', '{{%laitovo_erp_product_group_relation}}');
        $this->dropForeignKey('fk_laitovo_erp_product_group_relation_product_id', '{{%laitovo_erp_product_group_relation}}');
        
        $this->dropTable('{{%laitovo_erp_product_group_relation}}');
    }
}
