<?php

use yii\db\Migration;

class m170829_055630_add_article_columns_laitovo_erp_division_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_erp_division','article','string');
    }

    public function down()
    {
        $this->dropColumn('laitovo_erp_division','article');
    }
}
