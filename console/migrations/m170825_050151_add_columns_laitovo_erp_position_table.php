<?php

use yii\db\Migration;

class m170825_050151_add_columns_laitovo_erp_position_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_erp_position', 'salary_one','double');
        $this->addColumn('laitovo_erp_position', 'salary_two','double');
        $this->addColumn('laitovo_erp_position', 'car_compensation','double');
        $this->addColumn('laitovo_erp_position', 'surcharge','double');
        $this->addColumn('laitovo_erp_position', 'qualification','double');
        $this->addColumn('laitovo_erp_position', 'premium','double');
        $this->addColumn('laitovo_erp_position', 'kpi_one','double');
        $this->addColumn('laitovo_erp_position', 'kpi_two','double');
        $this->addColumn('laitovo_erp_position', 'kpi_three','double');
        $this->addColumn('laitovo_erp_position', 'article','integer');
        $this->addColumn('laitovo_erp_position','division_id','integer');
        $this->addForeignKey(
            'fk-laitovo_erp_position-division_id',
            '{{%laitovo_erp_position}}',
            'division_id',
            '{{%laitovo_erp_division}}',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropColumn('laitovo_erp_position', 'salary_one');
        $this->dropColumn('laitovo_erp_position', 'salary_two');
        $this->dropColumn('laitovo_erp_position', 'car_compensation');
        $this->dropColumn('laitovo_erp_position', 'surcharge');
        $this->dropColumn('laitovo_erp_position', 'qualification');
        $this->dropColumn('laitovo_erp_position', 'premium');
        $this->dropColumn('laitovo_erp_position', 'kpi_one');
        $this->dropColumn('laitovo_erp_position', 'kpi_two');
        $this->dropColumn('laitovo_erp_position', 'kpi_three');
        $this->dropColumn('laitovo_erp_position', 'article');
        $this->dropForeignKey('fk-laitovo_erp_position-division_id', '{{%laitovo_erp_position}}');
        $this->dropColumn('{{%laitovo_erp_position}}', 'division_id');
    }

}
