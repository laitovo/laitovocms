<?php

use yii\db\Migration;

class m200824_081008_add_column_to_return_journal_table extends Migration
{
    protected $table = '{{%return_journal}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        $this->addColumn($this->table,'isLoadedReturn',$this->boolean()->notNull()->defaultValue(0)->comment('Возврат загружен в монитор руководителя'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table,'isLoadedReturn');
    }
}
