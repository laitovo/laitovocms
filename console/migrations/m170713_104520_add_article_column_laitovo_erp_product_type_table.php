<?php

use yii\db\Migration;

class m170713_104520_add_article_column_laitovo_erp_product_type_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_erp_product_type', 'article','string');
    }

    public function down()
    {
        $this->dropColumn('laitovo_erp_product_type', 'article');
        
    }
}
