<?php

use yii\db\Migration;

class m240724_111905_add_indexes_to_tables extends Migration
{
    public function safeUp()
    {
        $this->createIndex('idx_laitovo_erp_naryad_stochka','{{%laitovo_erp_naryad}}','stochka');
        $this->createIndex('idx_logistics_order_entry_created_at','{{%logistics_order_entry}}','created_at');
        $this->createIndex('idx_logistics_order_entry_article','{{%logistics_order_entry}}','article');
    }

    public function safeDown()
    {
//        $this->dropIndex('idx_logistics_order_entry_article','{{%logistics_order_entry}}');
        $this->dropIndex('idx_logistics_order_entry_created_at','{{%logistics_order_entry}}');
        $this->dropIndex('idx_laitovo_erp_naryad_stochka','{{%laitovo_erp_naryad}}');
    }
}
