<?php

use yii\db\Migration;

class m181127_072836_add_column_comment_laitovo_erp_additional_work extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_additional_work}}','comment',$this->string()->comment('Комментарий'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_additional_work}}','comment');
    }
}
