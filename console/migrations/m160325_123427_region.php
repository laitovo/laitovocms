<?php

use yii\db\Schema;
use yii\db\Migration;

class m160325_123427_region extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%region}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->comment('Родитель'),

            'name' => $this->string()->comment('Название'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),
            'main' => $this->boolean()->defaultValue(false)->comment('Столица/Обл.центр/Основной...'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),
        ], $tableOptions);

        $this->createIndex('idx_region_parent_id','{{%region}}','parent_id, status, sort');
        $this->createIndex('idx_region_main','{{%region}}','main');

        $this->addForeignKey('fk_region_author_id','{{%region}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_region_updater_id','{{%region}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_region_parent_id','{{%region}}','parent_id','{{%region}}','id','CASCADE','CASCADE');

        $this->addForeignKey('order_region_id','{{%order}}','region_id','{{%region}}','id','SET NULL','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createRegion = $auth->createPermission('createRegion');
        $createRegion->description = 'Разрешение добавлять регионы';
        $auth->add($createRegion);
        $readRegion = $auth->createPermission('readRegion');
        $readRegion->description = 'Разрешение просматривать регионы';
        $auth->add($readRegion);
        $updateRegion = $auth->createPermission('updateRegion');
        $updateRegion->description = 'Разрешение редактировать регионы';
        $auth->add($updateRegion);
        $deleteRegion = $auth->createPermission('deleteRegion');
        $deleteRegion->description = 'Разрешение удалять регионы';
        $auth->add($deleteRegion);

        $rule = $auth->getRule('isAuthor');
        $updateOwnRegion = $auth->createPermission('updateOwnRegion');
        $updateOwnRegion->description = 'Разрешение редактировать созданные регионы';
        $updateOwnRegion->ruleName = $rule->name;
        $auth->add($updateOwnRegion);
        $auth->addChild($updateOwnRegion, $updateRegion);

        $regionReader = $auth->createRole('regionReader');
        $auth->add($regionReader);
        $auth->addChild($regionReader, $readRegion);

        $regionAuthor = $auth->createRole('regionAuthor');
        $auth->add($regionAuthor);
        $auth->addChild($regionAuthor, $regionReader);
        $auth->addChild($regionAuthor, $createRegion);
        $auth->addChild($regionAuthor, $updateOwnRegion);

        $regionManager = $auth->createRole('regionManager');
        $auth->add($regionManager);
        $auth->addChild($regionManager, $regionAuthor);
        $auth->addChild($regionManager, $updateRegion);

        $regionAdmin = $auth->createRole('regionAdmin');
        $auth->add($regionAdmin);
        $auth->addChild($regionAdmin, $regionManager);
        $auth->addChild($regionAdmin, $deleteRegion);

        $auth->addChild($admin, $regionAdmin);

    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createRegion = $auth->getPermission('createRegion');
        $readRegion = $auth->getPermission('readRegion');
        $updateRegion = $auth->getPermission('updateRegion');
        $deleteRegion = $auth->getPermission('deleteRegion');
        $updateOwnRegion = $auth->getPermission('updateOwnRegion');

        $regionReader = $auth->getRole('regionReader');
        $regionAuthor = $auth->getRole('regionAuthor');
        $regionManager = $auth->getRole('regionManager');
        $regionAdmin = $auth->getRole('regionAdmin');

        $auth->removeChild($admin, $regionAdmin);
        $auth->removeChildren($regionAdmin);
        $auth->removeChildren($regionManager);
        $auth->removeChildren($regionAuthor);
        $auth->removeChildren($regionReader);
        $auth->removeChildren($updateOwnRegion);

        $auth->remove($regionAdmin);
        $auth->remove($regionManager);
        $auth->remove($regionAuthor);
        $auth->remove($regionReader);
        $auth->remove($updateOwnRegion);
        $auth->remove($deleteRegion);
        $auth->remove($updateRegion);
        $auth->remove($readRegion);
        $auth->remove($createRegion);


        $this->delete('{{%region}}');

        $this->dropForeignKey('order_region_id','{{%order}}');
        $this->dropForeignKey('fk_region_parent_id','{{%region}}');
        $this->dropForeignKey('fk_region_updater_id','{{%region}}');
        $this->dropForeignKey('fk_region_author_id','{{%region}}');

        $this->dropTable('{{%region}}');
    }
}
