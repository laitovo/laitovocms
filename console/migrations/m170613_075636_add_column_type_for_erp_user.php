<?php

use yii\db\Migration;

class m170613_075636_add_column_type_for_erp_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_user}}', 'type', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_user}}', 'type');
    }
}
