<?php

use yii\db\Migration;

class m201009_103904_add_column_updatedAt_core_debt_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%core_debt}}', 'updatedAt', $this->integer()
            ->comment('Дата последнего апдейта в родительской таблице'));
    }

    public function down()
    {
        $this->dropColumn('{{%core_debt}}', 'updatedAt');
    }
}
