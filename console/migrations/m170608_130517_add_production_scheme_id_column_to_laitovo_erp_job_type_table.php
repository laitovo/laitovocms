<?php

use yii\db\Migration;

/**
 * Handles adding production_scheme_id to table `laitovo_erp_job_type`.
 */
class m170608_130517_add_production_scheme_id_column_to_laitovo_erp_job_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    
        $this->addColumn('laitovo_erp_job_type', 'production_scheme_id','integer');
        $this->addForeignKey(
            'fk-laitovo_erp_job_type-production_scheme_id',
            '{{%laitovo_erp_job_type}}',
            'production_scheme_id',
            '{{%production_scheme}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {    
       $this->dropForeignKey('fk-laitovo_erp_job_type-production_scheme_id', '{{%laitovo_erp_job_type}}');
       $this->dropColumn('laitovo_erp_job_type', 'production_scheme_id');
    }
}
