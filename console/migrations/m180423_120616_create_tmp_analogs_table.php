<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tmp_analogs`.
 */
class m180423_120616_create_tmp_analogs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%tmp_analogs}}', [
            'id' => $this->primaryKey(),
            'carArticle' => $this->integer(),
            'carName' => $this->string(),
            'window' => $this->string(),
            'analogArticle' => $this->string(),
            'analogName' => $this->string(),
            'last' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%tmp_analogs}}');
    }
}
