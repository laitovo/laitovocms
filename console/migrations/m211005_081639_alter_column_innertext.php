<?php

use yii\db\Migration;

class m211005_081639_alter_column_innertext extends Migration
{
    public function up()
    {
        $this->alterColumn('logistics_order','innercomment',$this->string(3000));
    }
}
