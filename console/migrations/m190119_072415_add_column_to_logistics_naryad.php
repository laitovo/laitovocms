<?php

use yii\db\Migration;

class m190119_072415_add_column_to_logistics_naryad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_naryad}}','withTape',$this->boolean()->defaultValue(false)->comment('Является ли данный upn - продуктом с креплениями на скотче'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_naryad}}','withTape');
    }
}
