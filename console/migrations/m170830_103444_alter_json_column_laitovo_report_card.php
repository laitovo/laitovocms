<?php

use yii\db\Migration;

class m170830_103444_alter_json_column_laitovo_report_card extends Migration
{
    public function up()
    {
        $this->alterColumn('laitovo_erp_report_card','json','text');
    }

    public function down()
    {

    }

}
