<?php

use yii\db\Migration;

class m170818_060605_alter_job_count_column_laitovo_erp_additional_table extends Migration
{
    public function up()
    {
        $this->alterColumn('laitovo_erp_additional_work', 'job_count', 'double');
    }

    public function down()
    {
        $this->alterColumn('laitovo_erp_additional_work', 'job_count', 'integer');
    }

}
