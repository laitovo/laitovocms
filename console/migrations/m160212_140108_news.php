<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_140108_news extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'feed_id' => $this->integer()->notNull()->comment('Лента'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'date' => $this->integer()->comment('Дата'),
            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('news_feed_id','{{%news}}','feed_id, status');
        $this->createIndex('news_sort','{{%news}}','sort');
        $this->createIndex('news_date','{{%news}}','date');

        $this->addForeignKey('news_author_id','{{%news}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('news_updater_id','{{%news}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('news_feed_id','{{%news}}','feed_id','{{%feed}}','id','CASCADE','CASCADE');

        $this->addColumn('{{%alias}}', 'news_id', $this->integer()->comment('Новость'));
        $this->addForeignKey('alias_news_id','{{%alias}}','news_id','{{%news}}','id','CASCADE','CASCADE');

        $this->createTable('{{%news_lang}}', [
            'news_id' => $this->integer()->notNull()->comment('Новость'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),

            'image' => $this->string()->comment('Изображение'),
            'anons' => $this->text()->comment('Анонс'),
            'content' => $this->text()->comment('Контент'),
            'PRIMARY KEY(news_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('news_lang_news_id','{{%news_lang}}','news_id','{{%news}}','id','CASCADE','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createNews = $auth->createPermission('createNews');
        $createNews->description = 'Разрешение добавлять новости';
        $auth->add($createNews);
        $readNews = $auth->createPermission('readNews');
        $readNews->description = 'Разрешение просматривать новости';
        $auth->add($readNews);
        $updateNews = $auth->createPermission('updateNews');
        $updateNews->description = 'Разрешение редактировать новости';
        $auth->add($updateNews);
        $deleteNews = $auth->createPermission('deleteNews');
        $deleteNews->description = 'Разрешение удалять новости';
        $auth->add($deleteNews);

        $rule = $auth->getRule('isAuthor');
        $updateOwnNews = $auth->createPermission('updateOwnNews');
        $updateOwnNews->description = 'Разрешение редактировать созданные новости';
        $updateOwnNews->ruleName = $rule->name;
        $auth->add($updateOwnNews);
        $auth->addChild($updateOwnNews, $updateNews);

        $newsReader = $auth->createRole('newsReader');
        $auth->add($newsReader);
        $auth->addChild($newsReader, $readNews);

        $newsAuthor = $auth->createRole('newsAuthor');
        $auth->add($newsAuthor);
        $auth->addChild($newsAuthor, $newsReader);
        $auth->addChild($newsAuthor, $createNews);
        $auth->addChild($newsAuthor, $updateOwnNews);

        $newsManager = $auth->createRole('newsManager');
        $auth->add($newsManager);
        $auth->addChild($newsManager, $newsAuthor);
        $auth->addChild($newsManager, $updateNews);

        $newsAdmin = $auth->createRole('newsAdmin');
        $auth->add($newsAdmin);
        $auth->addChild($newsAdmin, $newsManager);
        $auth->addChild($newsAdmin, $deleteNews);

        $auth->addChild($admin, $newsAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createNews = $auth->getPermission('createNews');
        $readNews = $auth->getPermission('readNews');
        $updateNews = $auth->getPermission('updateNews');
        $deleteNews = $auth->getPermission('deleteNews');
        $updateOwnNews = $auth->getPermission('updateOwnNews');

        $newsReader = $auth->getRole('newsReader');
        $newsAuthor = $auth->getRole('newsAuthor');
        $newsManager = $auth->getRole('newsManager');
        $newsAdmin = $auth->getRole('newsAdmin');

        $auth->removeChild($admin, $newsAdmin);
        $auth->removeChildren($newsAdmin);
        $auth->removeChildren($newsManager);
        $auth->removeChildren($newsAuthor);
        $auth->removeChildren($newsReader);
        $auth->removeChildren($updateOwnNews);

        $auth->remove($newsAdmin);
        $auth->remove($newsManager);
        $auth->remove($newsAuthor);
        $auth->remove($newsReader);
        $auth->remove($updateOwnNews);
        $auth->remove($deleteNews);
        $auth->remove($updateNews);
        $auth->remove($readNews);
        $auth->remove($createNews);

        $this->delete('{{%news}}');

        $this->dropForeignKey('news_lang_news_id','{{%news_lang}}');
        $this->dropTable('{{%news_lang}}');

        $this->dropForeignKey('alias_news_id','{{%alias}}');
        $this->dropColumn('{{%alias}}', 'news_id');

        $this->dropForeignKey('news_feed_id','{{%news}}');
        $this->dropForeignKey('news_updater_id','{{%news}}');
        $this->dropForeignKey('news_author_id','{{%news}}');

        $this->dropTable('{{%news}}');
    }
}
