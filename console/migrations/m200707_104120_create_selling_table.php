<?php

use yii\db\Migration;

/**
 * Handles the creation of table `selling`.
 */
class m200707_104120_create_selling_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('core_selling', [
            'id'                    => $this->primaryKey(),
            'clientId'              => $this->integer()->notNull()->comment('Уникальный идентификатор клиента в определнной локали'),
            'locale'                => 'ENUM("undefined","ru", "eu") NOT NULL',
            'date'                  => $this->integer()->notNull()->comment('Учетная дата реализации'),
            'orderNumber'           => $this->integer()->comment('Номер заказа, на основании которого была произведена реализация'),
            'sumGoodsWithoutNds'    => $this->decimal(10,2)->notNull()->comment('Сумма стоимости позиций реализации без НДС'),
            'sumDeliveryWithoutNds' => $this->decimal(10,2)->notNull()->comment('Стоимость доставки без НДС'),
            'discountWithoutNds'    => $this->decimal(10,2)->notNull()->comment('Скидка без НДС (оплата бонусами)'),
            'sumWithoutNds'         => $this->decimal(10,2)->notNull()->comment('Сумма реализации без НДС ( Сумма стоимости позиций реализации без НДС + Стоимость доставки без НДС - Скидка без НДС (оплата бонусами))'),
            'amountNds'             => $this->decimal(10,2)->notNull()->comment('Значение налога НДС в %'),
            'sumNds'                => $this->decimal(10,2)->notNull()->comment('Сумма налога НДС с реализации'),
            'discountWithNds'       => $this->decimal(10,2)->notNull()->comment('Скидка с НДС (оплата бонусами)'),
            'sumWithNds'            => $this->decimal(10,2)->notNull()->comment('Сумма реализации с учетом НДС'),
        ]);

        $this->createTable('core_selling_item', [
            'id'                    => $this->primaryKey(),
            'sellingId'             => $this->integer()->notNull()->comment('Уникальный идентификатор реализации'),
            'title'                 => $this->string()->notNull()->comment('Наименовании позиции товара'),
            'article'               => $this->string()->notNull()->comment('Артикул товара'),
            'count'                 => $this->decimal(10,3)->notNull()->comment('Количество условных единиц товара (по умолчанию шт.)'),
            'priceWithoutNds'       => $this->decimal(10,2)->notNull()->comment('Цена за единицу без НДС'),
            'costWithoutNds'        => $this->decimal(10,2)->notNull()->comment('Стоимость без НДС = ( Количество условных единиц товара (по умолчанию шт.) * Цена за единицу без НДС )'),
            'discountWithoutNds'    => $this->decimal(10,2)->notNull()->comment('Скидка без НДС (оплата бонусами)'),
            'sumWithoutNds'         => $this->decimal(10,2)->notNull()->comment('Сумма позиции без НДС ( Стоимость без НДС - Скидка без НДС (оплата бонусами))'),
            'amountNds'             => $this->decimal(10,2)->notNull()->comment('Значение налога НДС в %'),
            'sumNds'                => $this->decimal(10,2)->notNull()->comment('Сумма налога НДС с реализации'),
            'discountWithNds'       => $this->decimal(10,2)->notNull()->comment('Скидка с НДС(оплата бонусами)'),
            'sumWithNds'            => $this->decimal(10,2)->notNull()->comment('Сумма позиции с учетом НДС'),
        ]);

        $this->addForeignKey('selling_item_sellingId',
            'core_selling_item','sellingId',
            'core_selling','id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('selling_item_sellingId','core_selling_item');
        $this->dropTable('core_selling_item');
        $this->dropTable('core_selling');
    }
}
