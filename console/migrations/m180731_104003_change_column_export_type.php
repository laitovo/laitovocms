<?php

use yii\db\Migration;

class m180731_104003_change_column_export_type extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%logistics_order}}','export_type',$this->string());
    }

    public function down()
    {
        $this->alterColumn('{{%logistics_order}}','export_type',$this->boolean());
    }

}
