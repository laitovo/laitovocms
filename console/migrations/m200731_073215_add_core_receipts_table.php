<?php

use yii\db\Migration;

class m200731_073215_add_core_receipts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('core_receipt', [
            'id'                    => $this->primaryKey(),
            'clientId'              => $this->integer()->notNull()->comment('Уникальный идентификатор клиента в определнной локали'),
            'locale'                => 'ENUM("undefined","ru", "eu") NOT NULL',
            'documentId'            => $this->integer()->notNull()->comment('Номер документа в соответсвуйющей локали'),
            'date'                  => $this->integer()->notNull()->comment('Учетная дата поступления'),
            'fromType'              => $this->string()->comment('Источник поступления, прихода'),
            'toType'                => $this->string()->comment('Куда поступили средства'),
            'orderNumber'           => $this->integer()->comment('Номер заказа, на основании которого была произведена реализация'),
            'total'                 => $this->decimal(10,2)->notNull()->comment('Сумма поступления'),
       ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('core_receipt');
    }
}
