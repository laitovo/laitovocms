<?php

use yii\db\Migration;

class m191023_092422_add_column_isload_to_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%europe_invoice}}','isLoad',$this->boolean()->defaultValue(false)->after('rate')->comment('Выгружен в учет'));
    }

    public function down()
    {
        $this->dropColumn('{{%europe_invoice}}','isLoad');

    }
}
