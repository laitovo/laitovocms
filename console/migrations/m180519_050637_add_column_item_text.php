<?php

use yii\db\Migration;

class m180519_050637_add_column_item_text extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order_entry}}','itemtext',$this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order_entry}}','itemtext');
    }

}
