<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_product_performance`.
 */
class m170531_090220_create_laitovo_erp_product_performance_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_product_performance}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Сотрудник'),
            'group_id' => $this->integer()->comment('Группа продуктов'),
            'average_time' => $this->integer()->comment('Среднее время выполнения'),
            'count_naryads' => $this->integer()->comment('Количество нарядов в статистике'),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_product_performance_group_id',
            '{{%laitovo_erp_product_performance}}', 'group_id',
            '{{%laitovo_erp_product_group}}', 'id',
            'SET NULL',
            'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_product_performance_user_id',
            '{{%laitovo_erp_product_performance}}', 'user_id',
            '{{%laitovo_erp_user}}', 'id',
            'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_product_performance_group_id', '{{%laitovo_erp_product_performance}}');
        $this->dropForeignKey('fk_laitovo_erp_product_performance_user_id', '{{%laitovo_erp_product_performance}}');

        $this->dropTable('{{%laitovo_erp_product_performance}}');
    }
}
