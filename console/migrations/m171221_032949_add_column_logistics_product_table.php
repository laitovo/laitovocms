<?php

use yii\db\Migration;

class m171221_032949_add_column_logistics_product_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_storage_product}}', 'literal_id', $this->integer()->notNull());
        $this->addForeignKey('fk_logistics_storage_product_literal_id', '{{%logistics_storage_product}}', 'literal_id', '{{%logistics_storage_literal}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_logistics_storage_product_literal_id', '{{%logistics_storage_product}}');
        $this->dropColumn('{{%logistics_storage_product}}', 'literal_id');
    }
}
