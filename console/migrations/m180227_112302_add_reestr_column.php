<?php

use yii\db\Migration;

class m180227_112302_add_reestr_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','reestrId',$this->integer()->comment('уникальный идентификатор реестра'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','reestrId');
    }

}
