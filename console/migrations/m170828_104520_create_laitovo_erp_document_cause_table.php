<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_document_cause`.
 */
class m170828_104520_create_laitovo_erp_document_cause_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%laitovo_erp_document_cause}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(),
            'title'=>$this->string(),
            'date_start'=>$this->date(),
            'date_finish'=>$this->date(),
            'type'=>$this->string(),
        ]);

        $this->createIndex(
            'idx-laitovo_erp_document_cause-user_id',
            'laitovo_erp_document_cause',
            'user_id'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_document_cause-user_id',
            '{{%laitovo_erp_document_cause}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_document_cause-user_id','laitovo_erp_document_cause');

        $this->dropIndex('idx-laitovo_erp_document_cause-user_id','laitovo_erp_document_cause');

        $this->dropTable('{{%laitovo_erp_document_cause}}');
    }
}
