<?php

use yii\db\Migration;

class m170912_124133_alter_type_month_year_column_laitovo_erp_report_card_table extends Migration
{
    public function up()
    {
        $this->alterColumn('laitovo_erp_report_card','month_year','date');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
