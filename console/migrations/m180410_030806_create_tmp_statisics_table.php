<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tmp_statisics`.
 */
class m180410_030806_create_tmp_statisics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tmp_statistics', [
            'id'           => $this->primaryKey(),
            'article'      => $this->string()->comment('Артикул'),
            'prefix'       => $this->string()->comment('Первые две буквы артикула'),
            'car'          => $this->string()->comment('Наименование машины'),
            'needAbsolute' => $this->float()->comment('Абсолютная нехватка, в штуках'),
            'needRelative' => $this->float()->comment('Относительная нехватка, в процентах'),
            'plan'         => $this->float()->comment('Планируемый остаток'),
            'fact'         => $this->float()->comment('Фактический остаток'),
            'prod'         => $this->float()->comment('На производстве'),
            'updated_at'   => $this->integer()->comment('Время последнего пересчета записи'),
            'last_pull'    => $this->integer()->comment('Время последнего поплнения'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tmp_statistics');
    }
}
