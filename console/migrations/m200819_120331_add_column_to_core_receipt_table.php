<?php

use yii\db\Migration;

class m200819_120331_add_column_to_core_receipt_table extends Migration
{
    protected $table = '{{%core_receipt}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        $this->addColumn($this->table,'mode',$this->string()->notNull()->comment('Вид поступления ( от клиента или прочее )'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table,'mode');
    }
}
