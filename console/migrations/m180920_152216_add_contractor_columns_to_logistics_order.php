<?php

use yii\db\Migration;

class m180920_152216_add_contractor_columns_to_logistics_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_order}}','sender_id', $this->integer()->comment('Контрагент-отправитель'));
        $this->addColumn('{{%logistics_order}}','receiver_id', $this->integer()->comment('Контрагент-получатель'));
        $this->addColumn('{{%logistics_order}}','payer_id', $this->integer()->comment('Контрагент-плательщик'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_order}}','sender_id');
        $this->dropColumn('{{%logistics_order}}','receiver_id');
        $this->dropColumn('{{%logistics_order}}','payer_id');
    }
}
