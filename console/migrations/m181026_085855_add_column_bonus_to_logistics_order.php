<?php

use yii\db\Migration;

class m181026_085855_add_column_bonus_to_logistics_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%logistics_order}}','bonus', $this->double()->defaultValue(0)->comment('Бонусы') );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%logistics_order}}','bonus');
    }
}
