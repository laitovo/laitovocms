<?php

use yii\db\Migration;

class m170823_105955_alter_columns_laitovo_erp_manning_table extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk-laitovo_erp_manning-location_id', '{{%laitovo_erp_manning}}');
        $this->dropColumn('{{%laitovo_erp_manning}}', 'location_id');

        $this->dropForeignKey('fk-laitovo_erp_manning-user_id', '{{%laitovo_erp_manning}}');
        $this->dropColumn('{{%laitovo_erp_manning}}', 'user_id');

        $this->addColumn('{{%laitovo_erp_manning}}','division_id','integer');
        $this->addForeignKey(
            'fk-laitovo_erp_manning-division_id',
            '{{%laitovo_erp_manning}}',
            'division_id',
            '{{%laitovo_erp_division}}',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_manning-division_id', '{{%laitovo_erp_manning}}');
        $this->dropColumn('{{%laitovo_erp_manning}}', 'division_id');

        $this->addColumn('{{%laitovo_erp_manning}}','location_id','integer');
        $this->addForeignKey(
            'fk-laitovo_erp_manning-location_id',
            '{{%laitovo_erp_manning}}',
            'location_id',
            '{{%laitovo_erp_location}}',
            'id',
            'CASCADE'
        );

        $this->addColumn('{{%laitovo_erp_manning}}','user_id','integer');
        $this->addForeignKey(
            'fk-laitovo_erp_manning-user_id',
            '{{%laitovo_erp_manning}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
    }
}
