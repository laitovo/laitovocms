<?php

use yii\db\Migration;

class m201007_041133_add_column_change_price extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_user}}', 'otherPricing', $this->boolean()->defaultValue(false)
            ->comment('Применять новые расценки'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_user}}', 'otherPricing');
    }
}
