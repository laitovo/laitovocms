<?php

use yii\db\Migration;

class m181215_061252_alter_table_erp_naryad_add_column_isFromSump extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','isFromSump',$this->boolean()->defaultValue(false)->comment('Является ли данный наряд - нарядом из склада отстойника'));
        $this->addColumn('{{%laitovo_erp_naryad}}','sumpLiteral',$this->string()->comment('Литера наряда на складе отстойнике'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','sumpLiteral');
        $this->dropColumn('{{%laitovo_erp_naryad}}','isFromSump');
    }
}
