<?php

use yii\db\Schema;
use yii\db\Migration;

class m160323_134306_banner extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bannertype}}', [
            'id' => $this->primaryKey(),
            'website_id' => $this->integer()->notNull()->comment('Сайт'),

            'type' => $this->string()->comment('Тип баннера'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),
        ], $tableOptions);

        $this->createIndex('idx_bannertype_type','{{%bannertype}}','website_id, type',true);
        $this->createIndex('idx_bannertype_website_id','{{%bannertype}}','website_id, type, status');

        $this->addForeignKey('fk_bannertype_author_id','{{%bannertype}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_bannertype_updater_id','{{%bannertype}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_bannertype_website_id','{{%bannertype}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $this->createTable('{{%bannertype_lang}}', [
            'bannertype_id' => $this->integer()->notNull()->comment('Тип баннера'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),

            'PRIMARY KEY(bannertype_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('bannertype_lang_bannertype_id','{{%bannertype_lang}}','bannertype_id','{{%bannertype}}','id','CASCADE','CASCADE');

        $this->createTable('{{%banner}}', [
            'id' => $this->primaryKey(),
            'bannertype_id' => $this->integer()->notNull()->comment('Тип баннера'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),
        ], $tableOptions);

        $this->createIndex('idx_banner_bannertype_id','{{%banner}}','bannertype_id, status, sort');

        $this->addForeignKey('fk_banner_author_id','{{%banner}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_banner_updater_id','{{%banner}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_banner_bannertype_id','{{%banner}}','bannertype_id','{{%bannertype}}','id','CASCADE','CASCADE');

        $this->createTable('{{%banner_lang}}', [
            'banner_id' => $this->integer()->notNull()->comment('Баннер'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),
            'link' => $this->string()->comment('Ссылка'),
            'image' => $this->string()->comment('Изображение'),
            'html' => $this->text()->comment('HTML'),

            'PRIMARY KEY(banner_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('banner_lang_banner_id','{{%banner_lang}}','banner_id','{{%banner}}','id','CASCADE','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createBanner = $auth->createPermission('createBanner');
        $createBanner->description = 'Разрешение добавлять баннеры';
        $auth->add($createBanner);
        $readBanner = $auth->createPermission('readBanner');
        $readBanner->description = 'Разрешение просматривать баннеры';
        $auth->add($readBanner);
        $updateBanner = $auth->createPermission('updateBanner');
        $updateBanner->description = 'Разрешение редактировать баннеры';
        $auth->add($updateBanner);
        $deleteBanner = $auth->createPermission('deleteBanner');
        $deleteBanner->description = 'Разрешение удалять баннеры';
        $auth->add($deleteBanner);

        $rule = $auth->getRule('isAuthor');
        $updateOwnBanner = $auth->createPermission('updateOwnBanner');
        $updateOwnBanner->description = 'Разрешение редактировать созданные баннеры';
        $updateOwnBanner->ruleName = $rule->name;
        $auth->add($updateOwnBanner);
        $auth->addChild($updateOwnBanner, $updateBanner);

        $bannerReader = $auth->createRole('bannerReader');
        $auth->add($bannerReader);
        $auth->addChild($bannerReader, $readBanner);

        $bannerAuthor = $auth->createRole('bannerAuthor');
        $auth->add($bannerAuthor);
        $auth->addChild($bannerAuthor, $bannerReader);
        $auth->addChild($bannerAuthor, $createBanner);
        $auth->addChild($bannerAuthor, $updateOwnBanner);

        $bannerManager = $auth->createRole('bannerManager');
        $auth->add($bannerManager);
        $auth->addChild($bannerManager, $bannerAuthor);
        $auth->addChild($bannerManager, $updateBanner);

        $bannerAdmin = $auth->createRole('bannerAdmin');
        $auth->add($bannerAdmin);
        $auth->addChild($bannerAdmin, $bannerManager);
        $auth->addChild($bannerAdmin, $deleteBanner);

        $auth->addChild($admin, $bannerAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createBanner = $auth->getPermission('createBanner');
        $readBanner = $auth->getPermission('readBanner');
        $updateBanner = $auth->getPermission('updateBanner');
        $deleteBanner = $auth->getPermission('deleteBanner');
        $updateOwnBanner = $auth->getPermission('updateOwnBanner');

        $bannerReader = $auth->getRole('bannerReader');
        $bannerAuthor = $auth->getRole('bannerAuthor');
        $bannerManager = $auth->getRole('bannerManager');
        $bannerAdmin = $auth->getRole('bannerAdmin');

        $auth->removeChild($admin, $bannerAdmin);
        $auth->removeChildren($bannerAdmin);
        $auth->removeChildren($bannerManager);
        $auth->removeChildren($bannerAuthor);
        $auth->removeChildren($bannerReader);
        $auth->removeChildren($updateOwnBanner);

        $auth->remove($bannerAdmin);
        $auth->remove($bannerManager);
        $auth->remove($bannerAuthor);
        $auth->remove($bannerReader);
        $auth->remove($updateOwnBanner);
        $auth->remove($deleteBanner);
        $auth->remove($updateBanner);
        $auth->remove($readBanner);
        $auth->remove($createBanner);

        $this->delete('{{%banner}}');

        $this->dropForeignKey('banner_lang_banner_id','{{%banner_lang}}');
        $this->dropTable('{{%banner_lang}}');

        $this->dropForeignKey('fk_banner_bannertype_id','{{%banner}}');
        $this->dropForeignKey('fk_banner_updater_id','{{%banner}}');
        $this->dropForeignKey('fk_banner_author_id','{{%banner}}');

        $this->dropTable('{{%banner}}');


        $this->delete('{{%bannertype}}');

        $this->dropForeignKey('bannertype_lang_bannertype_id','{{%bannertype_lang}}');
        $this->dropTable('{{%bannertype_lang}}');

        $this->dropForeignKey('fk_bannertype_website_id','{{%bannertype}}');
        $this->dropForeignKey('fk_bannertype_updater_id','{{%bannertype}}');
        $this->dropForeignKey('fk_bannertype_author_id','{{%bannertype}}');

        $this->dropTable('{{%bannertype}}');
    }

}
