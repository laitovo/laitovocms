<?php

use yii\db\Migration;

class m200203_062210_add_columns_laitovo_cars extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_cars}}', 'blanket_default', $this->string());
        $this->addColumn('{{%laitovo_cars}}', 'blanket_custom', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_cars}}', 'blanket_custom');
        $this->dropColumn('{{%laitovo_cars}}', 'blanket_default');
    }
}

