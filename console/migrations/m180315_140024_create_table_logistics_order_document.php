<?php

use yii\db\Migration;

class m180315_140024_create_table_logistics_order_document extends Migration
{
    public function up()
    {
        $this->createTable('logistics_order_document', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'file_key' => $this->string()->comment('Ключ для доступа к файлу'),
            'author_id' => $this->integer()->comment('Кто загрузил'),
            'uploaded_at' => $this->integer()->comment('Дата загрузки документа'),
        ]);
    }

    public function down()
    {
        $this->dropTable('logistics_order_document');
    }
}
