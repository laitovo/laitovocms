<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_parameters`.
 */
class m170524_053354_create_material_parameters_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('material_parameters', [
            'id' => $this->primaryKey(),
            'created_at' =>$this->integer()->comment('Дата создания'),
            'material_id' => $this->integer(),
            'parameter' => $this->string()->comment('Параметр материала'),
           
        ]);
         $this->createIndex(
            'idx-material_parameters-material_id',
            'material_parameters',
            'material_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-material_parameters-material_id',
            'material_parameters',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
         $this->dropForeignKey(
            'fk-material_parameters-material_id',
            'material_parameters'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-material_parameters-material_id',
            'material_parameters'
        );
        $this->dropTable('material_parameters');
    }
}
