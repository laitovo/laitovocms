<?php

use yii\db\Migration;

class m180522_100717_add_erp_naryad_auto_speed extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','autoSpeed',$this->boolean()->defaultValue(0)->comment('Автоматически ускоренный алгоритмом'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','autoSpeed');
    }
}
