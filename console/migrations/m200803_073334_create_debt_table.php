<?php

use yii\db\Migration;

/**
 * Handles the creation of table `debt`.
 */
class m200803_073334_create_debt_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('core_debt', [
            'id'                    => $this->primaryKey(),
            'clientId'              => $this->integer()->notNull()->comment('Уникальный идентификатор клиента в определнной локали'),
            'locale'                => 'ENUM("undefined","ru", "eu") NOT NULL',
            'date'                  => $this->integer()->notNull()->comment('Учетная дата поступления'),
            'total'                 => $this->decimal(10,2)->notNull()->comment('Сумма поступления'),
            'type'                  => $this->string()->notNull()->comment('Тип документа'),
            'documentId'            => $this->integer()->notNull()->comment('Номер документа в соответсвующей системе'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('core_debt');
    }
}
