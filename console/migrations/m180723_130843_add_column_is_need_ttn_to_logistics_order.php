<?php

use yii\db\Migration;

class m180723_130843_add_column_is_need_ttn_to_logistics_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','is_need_ttn',$this->boolean()->defaultValue(false)->comment('Необходимость товарно-транспортной накладной'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','is_need_ttn');
    }
}
