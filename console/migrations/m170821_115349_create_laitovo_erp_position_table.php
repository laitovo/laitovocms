<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_position`.
 */
class m170821_115349_create_laitovo_erp_position_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_erp_position', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('laitovo_erp_position');
    }
}
