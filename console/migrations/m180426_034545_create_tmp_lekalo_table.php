<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tmp_lekalo`.
 */
class m180426_034545_create_tmp_lekalo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%tmp_lekalo}}', [
            'id' => $this->primaryKey(),
            'lekalo' => $this->integer(),
            'carArticle' => $this->integer(),
            'carName'    => $this->string(),
            'FW' => $this->boolean(),
            'FV' => $this->boolean(),
            'FD' => $this->boolean(),
            'RD' => $this->boolean(),
            'RV' => $this->boolean(),
            'BW' => $this->boolean(),
            'last' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%tmp_lekalo}}');
    }
}
