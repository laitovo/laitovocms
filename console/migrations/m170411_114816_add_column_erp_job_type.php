<?php

use yii\db\Migration;

class m170411_114816_add_column_erp_job_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_job_type}}', 'unit', $this->string()->comment('Единица измерения'));
    }

    public function down()
    {
        echo "m170411_114816_add_column_erp_job_type cannot be reverted.\n";
            $this->dropColumn('{{%laitovo_erp_job_type}}', 'unit');
    }
}
