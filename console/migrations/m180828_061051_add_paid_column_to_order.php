<?php

use yii\db\Migration;

class m180828_061051_add_paid_column_to_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','paid',$this->double()->comment('Оплаченная сумма по заказу'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','paid');
    }
}
