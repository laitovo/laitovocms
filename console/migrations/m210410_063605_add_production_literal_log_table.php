<?php

use yii\db\Migration;

class m210410_063605_add_production_literal_log_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%core_production_literal_log}}', [
            'id' => $this->primaryKey(),
            'prodLiteralId' => $this->string(5),
            'erpNaryadId' => $this->integer(),
            'locationId' => $this->integer(),
            'createdAt' => $this->integer(),
            'deletedAt' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%core_production_literal_log}}');
    }
}
