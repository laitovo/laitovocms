<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_storage_literal`.
 */
class m171220_075558_create_logistics_storage_literal_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }

        $this->createTable('{{%logistics_storage_literal}}', [
            'id' => $this->primaryKey(),
            'title' =>$this->string()->notNull(),
            'storage_id'=>$this->integer(),
            'capacity' => $this->integer()->comment('Емкость литеры'),

            //columns
            'created_at' => $this->integer(),
            'author_id'  => $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_storage_literal_storage_id', '{{%logistics_storage_literal}}', 'storage_id', '{{%logistics_storage}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_literal_author_id', '{{%logistics_storage_literal}}', 
            'author_id', '{{%user}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_literal_updater_id', '{{%logistics_storage_literal}}', 
            'updater_id', '{{%user}}', 'id', 
            'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_storage_literal_updater_id', '{{%logistics_storage_literal}}');
        $this->dropForeignKey('fk_logistics_storage_literal_author_id', '{{%logistics_storage_literal}}');
        $this->dropForeignKey('fk_logistics_storage_literal_storage_id', '{{%logistics_storage_literal}}');

        $this->dropTable('{{%logistics_storage_literal}}');
    }
}
