<?php

use yii\db\Migration;

class m240725_055736_add_column_laitovo_user_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','laitovo_user_id', $this->integer()->after('username')->notNull()->defaultValue(0)->comment('Идентификатор в LaitovoCMS'));
        $this->createIndex('idx_logistics_order_laitovo_user_id','{{%logistics_order}}','laitovo_user_id');
    }

    public function down()
    {
        $this->dropIndex('idx_logistics_order_laitovo_user_id','{{%logistics_order}}');
        $this->dropColumn('{{%logistics_order}}','laitovo_user_id');
    }
}
