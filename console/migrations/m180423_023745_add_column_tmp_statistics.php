<?php

use yii\db\Migration;

class m180423_023745_add_column_tmp_statistics extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tmp_statistics}}','title',$this->string());
        $this->addColumn('{{%tmp_statistics}}','analogs',$this->text());
    }

    public function down()
    {
        $this->dropColumn('{{%tmp_statistics}}','title');
        $this->dropColumn('{{%tmp_statistics}}','analogs');
    }
}
