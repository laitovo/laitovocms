<?php

use yii\db\Migration;

class m170524_045104_add_column_laitovo_erp_location extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_location}}', 'type', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_location}}', 'type');
    }
}
