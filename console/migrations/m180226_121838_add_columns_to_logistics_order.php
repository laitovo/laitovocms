<?php

use yii\db\Migration;

class m180226_121838_add_columns_to_logistics_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_order}}', 'logistics_status',$this->string()->defaultValue('not_handled')->comment('Логистический статус: "not_handled" (не обработана), "wait_for_weighing" (ожидает взвешивания), "handled" (обработана)'));
        $this->addColumn('{{%logistics_order}}', 'weight_status',$this->boolean()->defaultValue(false)->comment('Весовой статус: со взвешиванием / без взвешивания'));
        $this->addColumn('{{%logistics_order}}', 'payment_status',$this->string()->defaultValue('not_paid')->comment('Статус оплаты: "not_paid" (не оплачено), "paid" (оплачено), "C.O.D." (наложка)'));
        $this->addColumn('{{%logistics_order}}', 'delivery_type',$this->string()->defaultValue('free')->comment('Тип доставки: "free" (бесплатная), "paid" (платная)'));
        $this->addColumn('{{%logistics_order}}', 'delivery_price',$this->double()->comment('Сумма доставки'));
        $this->addColumn('{{%logistics_order}}', 'delivery_status',$this->string()->defaultValue('not_paid')->comment('Статус доставки: "not_paid" (не оплачено), "paid" (оплачено), "C.O.D." (наложка)'));
        $this->addColumn('{{%logistics_order}}', 'weight',$this->double()->comment('Вес заказа для транспортной компании'));
        $this->addColumn('{{%logistics_order}}', 'shipment_date',$this->integer()->comment('Дата отгрузки'));
        $this->addColumn('{{%logistics_order}}', 'track_code',$this->string()->comment('Трэк-код'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_order}}','logistics_status');
        $this->dropColumn('{{%logistics_order}}','weight_status');
        $this->dropColumn('{{%logistics_order}}','payment_status');
        $this->dropColumn('{{%logistics_order}}','delivery_type');
        $this->dropColumn('{{%logistics_order}}','delivery_price');
        $this->dropColumn('{{%logistics_order}}','delivery_status');
        $this->dropColumn('{{%logistics_order}}','weight');
        $this->dropColumn('{{%logistics_order}}','shipment_date');
        $this->dropColumn('{{%logistics_order}}','track_code');
    }
}
