<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_135756_alias extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%alias}}', [
            'website_id' => $this->integer()->notNull()->comment('Сайт'),
            'link' => $this->string()->notNull()->comment('Ссылка'),

            'name' => $this->string()->comment('Название'),
            'title' => $this->string()->comment('Заголовок'),
            'description' => $this->string()->comment('Описание'),
            'keywords' => $this->string()->comment('Ключевые слова'),
            'metatag' => $this->text()->comment('Мета теги'),

            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
            'json' => $this->text(),
            'lang' => $this->string()->comment('Язык'),
            'PRIMARY KEY(website_id, link)'
        ], $tableOptions);

        $this->createIndex('alias_website_id','{{%alias}}','website_id, link, status');

        $this->addForeignKey('alias_author_id','{{%alias}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('alias_updater_id','{{%alias}}','updater_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('alias_website_id','{{%alias}}','website_id','{{%website}}','id','CASCADE','CASCADE');

    }

    public function down()
    {
        $this->delete('{{%alias}}');

        $this->dropForeignKey('alias_website_id','{{%alias}}');
        $this->dropForeignKey('alias_updater_id','{{%alias}}');
        $this->dropForeignKey('alias_author_id','{{%alias}}');
        
        $this->dropTable('{{%alias}}');
    }
}
