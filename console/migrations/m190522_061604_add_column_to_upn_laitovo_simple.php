<?php

use yii\db\Migration;

class m190522_061604_add_column_to_upn_laitovo_simple extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_naryad}}','laitovoSimple',$this->boolean()->notNull()->defaultValue(false)->comment('Клеить лейбл "Лайтово Simple"'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_naryad}}','laitovoSimple');
    }
}
