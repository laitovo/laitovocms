<?php

use yii\db\Migration;

class m190228_071802_creaet_not_autocomplation_table extends Migration
{
    protected $table = '{{%logistics_autocompletion_blocks}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%name_table}}') {
            throw new DomainException('Name table not defined!');
        }

        parent::safeUp();

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table,
            [
                'id'          => $this->primaryKey(),
                'catalogMask' => $this->char(20),
            ],
            $this->tableOptions);

    }

    public function safeDown()
    {
        return $this->dropTable($this->table);
    }
}
