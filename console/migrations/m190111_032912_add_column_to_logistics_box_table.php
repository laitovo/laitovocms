<?php

use yii\db\Migration;

class m190111_032912_add_column_to_logistics_box_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_box}}', 'remoteStorageLiteral', $this->string()->comment('Литера удаленного склада'));
        $this->addColumn('{{%logistics_box}}', 'remoteStorageBarcode', $this->string()->comment('Штрихкод литеры удаленного склада'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_box}}', 'remoteStorageBarcode');
        $this->dropColumn('{{%logistics_box}}', 'remoteStorageLiteral');
    }
}
