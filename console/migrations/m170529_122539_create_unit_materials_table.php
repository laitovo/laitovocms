<?php

use yii\db\Migration;

/**
 * Handles the creation of table `unit_materials`.
 */
class m170529_122539_create_unit_materials_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('unit_materials', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('unit_materials');
    }
}
