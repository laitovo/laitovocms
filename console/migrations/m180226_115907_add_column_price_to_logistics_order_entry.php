<?php

use yii\db\Migration;

class m180226_115907_add_column_price_to_logistics_order_entry extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order_entry}}', 'price',$this->double()->comment('Цена'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order_entry}}','price');
    }
}
