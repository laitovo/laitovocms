<?php

use yii\db\Migration;

/**
 * Handles the creation of table `custom_article`.
 */
class m180329_100801_create_custom_article_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('custom_article', [
            'id' => $this->primaryKey(),
            'article' => $this->string()->notNull()->comment('Артикул'),
            'title' => $this->string()->notNull()->comment('Наименование'),
            'barcode' => $this->string()->notNull()->comment('Штрихкод')
        ]);

        $this->batchInsert('custom_article',
            ['article','title','barcode'],
            [
                ['LA-A-559'   , 'Audi A3 (8P Facelift) (5 Door) 2008 - 2012 (Premium Shade Set)', '5052644748669'],
                ['LA-A-1228'   , 'Audi A6 (C7) Avant 2011 - 2018 (Premium Shade Set)', '5052644748355'],
                ['LA-A-2'   , 'Audi A4 (B8) Saloon 2008 - 2015 (Premium Shade Set)', '5052644748478'],
                ['LA-A-3'   , 'Audi A4 (B8) Avant 2008 - 2015 (Premium Shade Set)', '5052644748690'],
                ['LA-A-582'   , 'Audi A3 (8V) Sportback 2012 - 2018 (Premium Shade Set)', '5052644748713'],
                ['LA-A-6'   , 'Audi A6 (C7) Saloon 2011 - 2018 (Premium Shade Set)', '5052644748560'],
                ['LA-A-8'   , 'Audi Q3 2012 - 2014 (Premium Shade Set)', '5052644748614'],
                ['LA-B-1005'   , 'BMW 1 Series (F21) (3 Door) 2012 - 2018 (Premium Shade Set)', '5052644748843'],
                ['LA-B-1113'   , 'BMW 3 Series (F31) Touring 2012 - 2018 (Premium Shade Set)', '5052644748256'],
                ['LA-B-23'   , 'BMW X1 (E84) 2010 - 2015 (Premium Shade Set)', '5052644748744'],
                ['LA-B-26'   , 'BMW X5 (E70) 2007 - 2013 (Premium Shade Set)', '5052644748768'],
                ['LA-B-43'   , 'BMW 3 Series (F30) Saloon 2012 - 2018 (Premium Shade Set)', '5052644748249'],
                ['LA-B-934'   , 'BMW X5 (F15/F85) 2014 - 2018 (Premium Shade Set)', '5052644748546'],
                ['LA-F-100'   , 'Ford Kuga Mk.2 2013 - 2018 (Premium Shade Set)', '5052644748386'],
                ['LA-F-119'   , 'Ford S-Max 2006 - 2015 (Premium Shade Set)', '5052644748591'],
                ['LA-F-1280'   , 'Ford Fiesta Mk.6 (5 Door) (Facelift) 2014 - 2017 (Premium Shade Set)', '5052644748683'],
                ['LA-F-1310'   , 'Ford Focus Mk.3 Hatchback (Facelift) 2015 - 2018 (Premium Shade Set)', '5052644748850'],
                ['LA-J-598'   , 'Jaguar XF Saloon 2007 - 2015 (Premium Shade Set)', '5052644748508'],
                ['LA-K-1453'   , 'Kia Sportage Mk.4 2016 - 2018 (Premium Shade Set)', '5052644748423'],
                ['LA-L-1243'   , 'Land Rover Discovery Sport 2015 - 2018 (Premium Shade Set)', '5052644748584'],
                ['LA-L-253'   , 'Land Rover Range Rover Evoque (5 Door) 2011 - 2018 (Premium Shade Set)', '5052644748607'],
                ['LA-L-314'   , 'Land Rover Range Rover Sport 2014 - 2018 (Premium Shade Set)', '5052644748393'],
                ['LA-M-1106'   , 'Mercedes A Class (W176) (5 Door) 2012 - 2018 (Premium Shade Set)', '5052644748317'],
                ['LA-M-293'   , 'Mercedes C Class (W204) Saloon 2007 - 2013 (Premium Shade Set)', '5052644748928'],
                ['LA-M-297'   , 'Mercedes E Class (W212) Saloon 2009 - 2016 (Premium Shade Set)', '5052644748959'],
                ['LA-M-312'   , 'Mini Countryman (R60) 2010 - 2017 (Premium Shade Set)', '5052644748911'],
                ['LA-M-669'   , 'Mercedes C Class (S204) Estate 2007 - 2014 (Premium Shade Set)', '5052644748997'],
                ['LA-M-683'   , 'Mercedes C Class (W205) Saloon 2014 - 2018 (Premium Shade Set)', '5052644748348'],
                ['LA-N-1040'   , 'Nissan Qashqai 2014 - 2018 (Premium Shade Set)', '5052644748430'],
                ['LA-N-1256'   , 'Nissan X-Trail (T32) 2014 - 2018 (Premium Shade Set)', '5052644748362'],
                ['LA-N-358'   , 'Nissan Qashqai 2007 - 2013 (Premium Shade Set)', '5052644748577'],
                ['LA-O-384'   , 'Vauxhall Astra (J) Mk.6 Hatchback (5 Door) 2010 - 2015 (Premium Shade Set)', '5052644748379'],
                ['LA-O-597'   , 'Vauxhall Zafira Tourer (C) Mk.3 2012 - 2018 (Premium Shade Set)', '5052644748751'],
                ['LA-S-441'   , 'Skoda Yeti 2009 - 2018 (Premium Shade Set)', '5052644748706'],
                ['LA-S-620'   , 'Skoda Octavia Mk.3 Estate 2013 - 2018 (Premium Shade Set)', '5052644748874'],
                ['LA-V-1050'   , 'Volkswagen Tiguan Mk.1 (Facelift) 2011 - 2016 (Premium Shade Set)', '5052644748485'],
                ['LA-V-1417'   , 'Volvo XC90 Mk.2 2015 - 2018 (Premium Shade Set)', '5052644748461'],
                ['LA-V-477'   , 'Volkswagen Golf Mk.7 Hatchback (5 Door) 2013 - 2017 (Premium Shade Set)', '5052644748263'],
                ['LA-V-519'   , 'Volkswagen Golf Mk.6 (5 Door) 2008 - 2012 (Premium Shade Set)', '5052644748331'],
                ['LA-V-533'   , 'Volkswagen Touareg Mk.2 2010 - 2018 (Premium Shade Set)', '5052644748409'],
                ['LA-V-552'   , 'Volvo XC60 2008 - 2017 (Premium Shade Set)', '5052644748287'],
                ['LA-V-555'   , 'Volvo XC90 Mk.1 2002 - 2014 (Premium Shade Set)', '5052644748492'],
                ['NP-A-1228'   , 'Audi A6 (C7) Avant 2011 - 2018 (Classic Shade Set)', '5052644744418'],
                ['NP-A-2'   , 'Audi A4 (B8) Saloon 2008 - 2015 (Classic Shade Set)', '5052644744470'],
                ['NP-A-3'   , 'Audi A4 (B8) Avant 2008 - 2015 (Classic Shade Set)', '5052644744487'],
                ['NP-A-559'   , 'Audi A3 (8P Facelift) (5 Door) 2008 - 2012 (Classic Shade Set)', '5052644744500'],
                ['NP-A-582'   , 'Audi A3 (8V) Sportback 2012 - 2018 (Classic Shade Set)', '5052644744524'],
                ['NP-A-6'   , 'Audi A6 (C7) Saloon 2011 - 2018 (Classic Shade Set)', '5052644744531'],
                ['NP-A-8'   , 'Audi Q3 2012 - 2014 (Classic Shade Set)', '5052644744555'],
                ['NP-A-9'   , 'Audi Q5 2008 - 2017 (Classic Shade Set)', '5052644744562'],
                ['NP-B-1004'   , 'BMW 1 Series (F20) (5 Door) 2012 - 2018 (Classic Shade Set)', '5052644744579'],
                ['NP-B-1005'   , 'BMW 1 Series (F21) (3 Door) 2012 - 2018 (Classic Shade Set)', '5052644744586'],
                ['NP-B-1113'   , 'BMW 3 Series (F31) Touring 2012 - 2018 (Classic Shade Set)', '5052644744593'],
                ['NP-B-23'   , 'BMW X1 (E84) 2010 - 2015 (Classic Shade Set)', '5052644744685'],
                ['NP-B-26'   , 'BMW X5 (E70) 2007 - 2013 (Classic Shade Set)', '5052644744708'],
                ['NP-B-32'   , 'BMW X3 (F25) 2010 - 2017 (Classic Shade Set)', '5052644744739'],
                ['NP-B-43'   , 'BMW 3 Series (F30) Saloon 2012 - 2018 (Classic Shade Set)', '5052644744746'],
                ['NP-B-934'   , 'BMW X5 (F15/F85) 2014 - 2018 (Classic Shade Set)', '5052644744760'],
                ['NP-F-100'   , 'Ford Kuga Mk.2 2013 - 2018 (Classic Shade Set)', '5052644744883'],
                ['NP-F-1181'   , 'Ford EcoSport 2012 - 2017 (Classic Shade Set)', '5052644745040'],
                ['NP-F-119'   , 'Ford S-Max 2006 - 2015 (Classic Shade Set)', '5052644745057'],
                ['NP-F-1280'   , 'Ford Fiesta Mk.6 (5 Door) (Facelift) 2014 - 2017 (Classic Shade Set)', '5052644745095'],
                ['NP-F-1310'   , 'Ford Focus Mk.3 Hatchback (Facelift) 2015 - 2018 (Classic Shade Set)', '5052644745118'],
                ['NP-H-1402'   , 'Hyundai Tucson Mk.3 2015 - 2018 (Classic Shade Set)', '5052644745224'],
                ['NP-J-598'   , 'Jaguar XF Saloon 2007 - 2015 (Classic Shade Set)', '5052644745606'],
                ['NP-K-1453'   , 'Kia Sportage Mk.4 2016 - 2018 (Classic Shade Set)', '5052644745637'],
                ['NP-L-1243'   , 'Land Rover Discovery Sport 2015 - 2018 (Classic Shade Set)', '5052644745873'],
                ['NP-L-253'   , 'Land Rover Range Rover Evoque (5 Door) 2011 - 2018 (Classic Shade Set)', '5052644745965'],
                ['NP-L-314'   , 'Land Rover Range Rover Sport 2014 - 2018 (Classic Shade Set)', '5052644746092'],
                ['NP-L-963'   , 'Land Rover Discovery 4 2009 - 2016 (Classic Shade Set)', '5052644746115'],
                ['NP-M-1106'   , 'Mercedes A Class (W176) (5 Door) 2012 - 2018 (Classic Shade Set)', '5052644746146'],
                ['NP-M-1286'   , 'Mercedes C Class (S205) Estate 2015 - 2018 (Classic Shade Set)', '5052644746245'],
                ['NP-M-293'   , 'Mercedes C Class (W204) Saloon 2007 - 2013 (Classic Shade Set)', '5052644746405'],
                ['NP-M-297'   , 'Mercedes E Class (W212) Saloon 2009 - 2016 (Classic Shade Set)', '5052644746429'],
                ['NP-M-312'   , 'Mini Countryman (R60) 2010 - 2017 (Classic Shade Set)', '5052644746528'],
                ['NP-M-669'   , 'Mercedes C Class (S204) Estate 2007 - 2014 (Classic Shade Set)', '5052644746610'],
                ['NP-M-683'   , 'Mercedes C Class (W205) Saloon 2014 - 2018 (Classic Shade Set)', '5052644746634'],
                ['NP-N-1040'   , 'Nissan Qashqai 2014 - 2018 (Classic Shade Set)', '5052644746665'],
                ['NP-N-1256'   , 'Nissan X-Trail (T32) 2014 - 2018 (Classic Shade Set)', '5052644746696'],
                ['NP-N-358'   , 'Nissan Qashqai 2007 - 2013 (Classic Shade Set)', '5052644746757'],
                ['NP-O-384'   , 'Vauxhall Astra (J) Mk.6 Hatchback (5 Door) 2010 - 2015 (Classic Shade Set)', '5052644746894'],
                ['NP-O-597'   , 'Vauxhall Zafira Tourer (C) Mk.3 2012 - 2018 (Classic Shade Set)', '5052644746993'],
                ['NP-S-441'   , 'Skoda Yeti 2009 - 2018 (Classic Shade Set)', '5052644747471'],
                ['NP-S-620'   , 'Skoda Octavia Mk.3 Estate 2013 - 2018 (Classic Shade Set)', '5052644747518'],
                ['NP-V-1050'   , 'Volkswagen Tiguan Mk.1 (Facelift) 2011 - 2016 (Classic Shade Set)', '5052644747808'],
                ['NP-V-1417'   , 'Volvo XC90 Mk.2 2015 - 2018 (Classic Shade Set)', '5052644747921'],
                ['NP-V-477'   , 'Volkswagen Golf Mk.7 Hatchback (5 Door) 2013 - 2017 (Classic Shade Set)', '5052644747945'],
                ['NP-V-519'   , 'Volkswagen Golf Mk.6 (5 Door) 2008 - 2012 (Classic Shade Set)', '5052644747983'],
                ['NP-V-533'   , 'Volkswagen Touareg Mk.2 2010 - 2018 (Classic Shade Set)', '5052644748072'],
                ['NP-V-552'   , 'Volvo XC60 2008 - 2017 (Classic Shade Set)', '5052644748188'],
                ['NP-V-555'   , 'Volvo XC90 Mk.1 2002 - 2014 (Classic Shade Set)', '5052644748218'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('custom_article');
    }
}
