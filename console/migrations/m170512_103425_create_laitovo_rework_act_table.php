<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_rework_act`.
 */
class m170512_103425_create_laitovo_rework_act_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_rework_act', [
            'id' => $this->primaryKey(),
            'naryad_id' => $this->integer()->comment('id наряда'),
            'created_at' => $this->integer()->comment('Дата создания'),
            'rework_number' => $this->string()->comment('Артикул акта'),
            'found_user_id' => $this->integer()->comment('Работник обнаруживший брак'),
            'made_user_id' => $this->integer()->comment('Работник допустивший брак'),           
            'deacription_problem' => $this->string()->comment('Описание проблемы'),
            'cause_problem' => $this->string()->comment('Причина брака'),
            'location_defect' => $this->integer()->comment('Участок где сделали брак'),
            'location_start' => $this->integer()->comment('Участок куда отправили на переделку'),
            'real_quilty' => $this->string()->comment('Реальный виновный'),
            'amount_damage' => $this->integer()->comment('Размер ущерба'),
            'deduction' => $this->integer()->comment('Размер удержания'),
            
        ]);
        
        $this->createIndex(
            'idx-laitovo_rework_act-found_user_id',
            'laitovo_rework_act',
            'found_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-laitovo_rework_act-found_user_id',
            'laitovo_rework_act',
            'found_user_id',
            'laitovo_erp_user',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-laitovo_rework_act-made_user_id',
            'laitovo_rework_act',
            'made_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-laitovo_rework_act-made_user_id',
            'laitovo_rework_act',
            'made_user_id',
            'laitovo_erp_user',
            'id',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-laitovo_rework_act-location_defect',
            'laitovo_rework_act',
            'location_defect'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-laitovo_rework_act-location_defect',
            'laitovo_rework_act',
            'location_defect',
            'laitovo_erp_location',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-laitovo_rework_act-location_start',
            'laitovo_rework_act',
            'location_start'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-laitovo_rework_act-location_start',
            'laitovo_rework_act',
            'location_start',
            'laitovo_erp_location',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-laitovo_rework_act-naryad_id',
            'laitovo_rework_act',
            'naryad_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-laitovo_rework_act-naryad_id',
            'laitovo_rework_act',
            'naryad_id',
            'laitovo_erp_naryad',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
         $this->dropForeignKey(
            'fk-laitovo_rework_act-found_user_id',
            'laitovo_rework_act'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-laitovo_rework_act-found_user_id',
            'laitovo_rework_act'
        );
         $this->dropForeignKey(
            'fk-laitovo_rework_act-made_user_id',
            'laitovo_rework_act'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-laitovo_rework_act-made_user_id',
            'laitovo_rework_act'
        );
         $this->dropForeignKey(
            'fk-laitovo_rework_act-location_defect',
            'laitovo_rework_act'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-laitovo_rework_act-location_defect',
            'laitovo_rework_act'
        );
         $this->dropForeignKey(
            'fk-laitovo_rework_act-location_start',
            'laitovo_rework_act'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-laitovo_rework_act-location_start',
            'laitovo_rework_act'
        );
         $this->dropForeignKey(
            'fk-laitovo_rework_act-naryad_id',
            'laitovo_rework_act'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-laitovo_rework_act-naryad_id',
            'laitovo_rework_act'
        );

        $this->dropTable('laitovo_rework_act');
    }
}
