<?php

use yii\db\Migration;

class m170629_061912_add_columns_liatovo_cars_for_checked extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_cars}}', 'checked_date', $this->integer());
        $this->addColumn('{{%laitovo_cars}}', 'checked_author', $this->integer());

        $this->addForeignKey('fk_laitovo_cars_checked_author', '{{%laitovo_cars}}', 'checked_author', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_cars_checked_author', '{{%laitovo_cars}}');
        $this->dropColumn('{{%laitovo_cars}}', 'checked_date');
        $this->dropColumn('{{%laitovo_cars}}', 'checked_author');
    }
}
