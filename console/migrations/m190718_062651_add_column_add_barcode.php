<?php

use yii\db\Migration;

class m190718_062651_add_column_add_barcode extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','actNumber',$this->integer()->comment('Номер акта примема передачи нарядов'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','actNumber');
    }
}
