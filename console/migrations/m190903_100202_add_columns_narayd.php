<?php

use yii\db\Migration;

class m190903_100202_add_columns_narayd extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%logistics_naryad}}','lastStorage', $this->integer()->comment('На каком складе было'));
        $this->addColumn('{{%logistics_naryad}}','lastLiteral', $this->string()->comment('В какой литере лежало'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%logistics_naryad}}','lastStorage');
        $this->dropColumn('{{%logistics_naryad}}','lastLiteral');
    }
}
