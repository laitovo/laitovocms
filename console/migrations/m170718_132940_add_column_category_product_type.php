<?php

use yii\db\Migration;

class m170718_132940_add_column_category_product_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_product_type}}', 'category',$this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_product_type}}','category');
    }
}
