<?php

use yii\db\Migration;

class m181026_154645_add_column_reserved_at_to_logistics_naryad extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%logistics_naryad}}','reserved_at', $this->integer()->comment('Дата резервирования'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%logistics_naryad}}','reserved_at');
    }
}
