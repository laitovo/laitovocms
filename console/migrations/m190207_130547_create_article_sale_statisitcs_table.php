<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article_sale_statisitcs`.
 */
class m190207_130547_create_article_sale_statisitcs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        }

        $this->createTable('{{%laitovo_article_sale_statistics}}', [
            'article'       => $this->string()->notNull(),
            'productId'     => $this->integer(),
            'title'         => $this->string(),
            'groupName'     => $this->string(),
            'saleLast'      => $this->integer()->notNull(),
            'salePrevious'  => $this->integer()->notNull(),
            'saleRate'      => $this->integer()->notNull(),
            'salePlan'      => $this->integer()->notNull(),
            'increase'      => $this->float()->notNull(),
            'makeRate'      => $this->float()->notNull(),
            'date'          => $this->integer()->notNull(),
            'balancePlan'   => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_laitovo_article_sale_statistics_article','{{%laitovo_article_sale_statistics}}','article');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%laitovo_article_sale_statistics}}');
    }
}
