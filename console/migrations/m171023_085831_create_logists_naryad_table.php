<?php

use yii\db\Migration;

class m171023_085831_create_logists_naryad_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%logistics_naryad}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(),
            'article' => $this->string()->comment('Артикул'),
            'source_id' => $this->integer()->comment('Источник поступления'),
            'responsible_person' => $this->string()->comment('Ответственное лицо'),
            'comment' => $this->text()->comment('Комментарий к наряду'),
            //columns
            'created_at' => $this->integer(),
            'author_id' =>  $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_naryad_source_id', 
            '{{%logistics_naryad}}', 'source_id', //from
            '{{%logistics_sources}}', 'id',       //to
            'SET NULL', // ON DELETE 
            'CASCADE'   // ON UPDATE
        );
        $this->addForeignKey('fk_logistics_naryad_team_id', '{{%logistics_naryad}}', 'team_id', '{{%team}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_naryad_author_id', '{{%logistics_naryad}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_naryad_updater_id', '{{%logistics_naryad}}', 'updater_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_naryad_updater_id', '{{%logistics_naryad}}');
        $this->dropForeignKey('fk_logistics_naryad_author_id', '{{%logistics_naryad}}');
        $this->dropForeignKey('fk_logistics_naryad_team_id', '{{%logistics_naryad}}');
        $this->dropForeignKey('fk_logistics_naryad_source_id', '{{%logistics_naryad}}');

        $this->dropTable('{{%logistics_naryad}}');
    }
}
