<?php

use yii\db\Migration;

class m180221_073814_add_column_reserved_id_to_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_naryad}}','reserved_id',$this->integer()->comment('Поизиция, для которой резервируется upn'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_naryad}}','reserved_id');
    }
}
