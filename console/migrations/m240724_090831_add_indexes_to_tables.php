<?php

use yii\db\Migration;

class m240724_090831_add_indexes_to_tables extends Migration
{
    public function safeUp()
    {
        $this->createIndex('idx_laitovo_erp_naryad_log_locdate','{{%laitovo_erp_naryad_log}}',['locationFromId', 'date']);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_laitovo_erp_naryad_log_locdate','{{%laitovo_erp_naryad_log}}');

    }
}
