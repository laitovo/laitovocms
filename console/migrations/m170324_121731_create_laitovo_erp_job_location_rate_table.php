<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_job_location_rate`.
 */
class m170324_121731_create_laitovo_erp_job_location_rate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_job_location_rate}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Работник'),
            'location_id' => $this->integer()->comment('Участок'),
            'rate' => $this->float()->comment('Коэффициент'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
        ], $tableOptions);

        $this->createIndex(
            'idx-job_location_rate-user_id',
            '{{%laitovo_erp_job_location_rate}}',
            'user_id'
        );

        $this->addForeignKey(
           'fk-job_location_rate-user_id',
            '{{%laitovo_erp_job_location_rate}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-job_location_rate-location_id',
            '{{%laitovo_erp_job_location_rate}}',
            'location_id'
        );

        $this->addForeignKey(
            'fk-job_location_rate-location_id',
            '{{%laitovo_erp_job_location_rate}}',
            'location_id',
            '{{%laitovo_erp_location}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-job_location_rate-user_id', '{{%laitovo_erp_job_location_rate}}');
        $this->dropIndex('idx-job_location_rate-user_id', '{{%laitovo_erp_job_location_rate}}');

        $this->dropForeignKey('fk-job_location_rate-location_id', '{{%laitovo_erp_job_location_rate}}');
        $this->dropIndex('idx-job_location_rate-location_id', '{{%laitovo_erp_job_location_rate}}');

        $this->dropTable('{{%laitovo_erp_job_location_rate}}');
    }
}
