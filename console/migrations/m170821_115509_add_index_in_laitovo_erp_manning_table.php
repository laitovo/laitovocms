<?php

use yii\db\Migration;

class m170821_115509_add_index_in_laitovo_erp_manning_table extends Migration
{
    public function up()
    {
        $this->addForeignKey(
            'fk-laitovo_erp_manning-position_id',
            '{{%laitovo_erp_manning}}',
            'position_id',
            '{{%laitovo_erp_position}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_manning-position_id', '{{%laitovo_erp_manning}}');
    }
}
