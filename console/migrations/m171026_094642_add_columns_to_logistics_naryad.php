<?php

use yii\db\Migration;

class m171026_094642_add_columns_to_logistics_naryad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_naryad}}', 'status', $this->smallInteger()->comment('Сатус'));
        $this->addColumn('{{%logistics_naryad}}', 'storage_id', $this->integer()->comment('Место хранения'));
        $this->addForeignKey('fk_logistics_naryad_storage_id', '{{%logistics_naryad}}', 'storage_id', '{{%logistics_storage}}', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('fk_logistics_naryad_storage_id', '{{%logistics_naryad}}');
        $this->dropColumn('{{%logistics_naryad}}', 'storage_id');
        $this->dropColumn('{{%logistics_naryad}}', 'status');
    }
}
