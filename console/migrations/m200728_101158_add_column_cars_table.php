<?php

use yii\db\Migration;

class m200728_101158_add_column_cars_table extends Migration
{
    protected $table = '{{%laitovo_cars}}';
    protected $tableOptions;

    public function safeUp()
    {
        if ($this->table == '{{%table_name}}') {
            throw new DomainException('Name table not defined!');
        }

        parent::safeUp();

        $this->addColumn($this->table,'templateNumber',$this->string()->comment('Номер лекала'));

        $this->fillTemplates();
    }

    public function safeDown()
    {
        $this->dropColumn($this->table,'templateNumber');
    }

    private function fillTemplates()
    {
        $cars = \common\models\laitovo\Cars::find()->all();
        foreach ($cars as $car) {
            $car->templateNumber = $car->lekalo;
            $car->save();
        }
    }
}
