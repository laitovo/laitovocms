<?php

use yii\db\Migration;

class m190621_083343_add_column_erp_naryad extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','isAdd',$this->boolean()->defaultValue(false)->comment('Наряд взять на дозакладку'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','isAdd');
    }
}
