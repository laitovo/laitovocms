<?php

use yii\db\Migration;

class m180115_061805_add_column_source_number_order_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','source_innumber',$this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','source_innumber');
    }
}
