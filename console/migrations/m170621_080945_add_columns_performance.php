<?php

use yii\db\Migration;

class m170621_080945_add_columns_performance extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_product_performance}}', 'day_count', $this->integer());
        $this->addColumn('{{%laitovo_erp_product_performance}}', 'full_time', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_product_performance}}', 'day_count');
        $this->dropColumn('{{%laitovo_erp_product_performance}}', 'full_time');
    }
}
