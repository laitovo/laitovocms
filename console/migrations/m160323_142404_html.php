<?php

use yii\db\Schema;
use yii\db\Migration;

class m160323_142404_html extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%htmlblock}}', [
            'id' => $this->primaryKey(),
            'website_id' => $this->integer()->notNull()->comment('Сайт'),

            'type' => $this->string()->comment('Тип блока'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),
        ], $tableOptions);

        $this->createIndex('idx_htmlblock_type','{{%htmlblock}}','website_id, type',true);
        $this->createIndex('idx_htmlblock_website_id','{{%htmlblock}}','website_id, type, status');

        $this->addForeignKey('fk_htmlblock_author_id','{{%htmlblock}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_htmlblock_updater_id','{{%htmlblock}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_htmlblock_website_id','{{%htmlblock}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $this->createTable('{{%htmlblock_lang}}', [
            'htmlblock_id' => $this->integer()->notNull()->comment('Html Блок'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),
            'html' => $this->text()->comment('HTML'),

            'PRIMARY KEY(htmlblock_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('htmlblock_lang_htmlblock_id','{{%htmlblock_lang}}','htmlblock_id','{{%htmlblock}}','id','CASCADE','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createHtmlblock = $auth->createPermission('createHtmlblock');
        $createHtmlblock->description = 'Разрешение добавлять html блоки';
        $auth->add($createHtmlblock);
        $readHtmlblock = $auth->createPermission('readHtmlblock');
        $readHtmlblock->description = 'Разрешение просматривать html блоки';
        $auth->add($readHtmlblock);
        $updateHtmlblock = $auth->createPermission('updateHtmlblock');
        $updateHtmlblock->description = 'Разрешение редактировать html блоки';
        $auth->add($updateHtmlblock);
        $deleteHtmlblock = $auth->createPermission('deleteHtmlblock');
        $deleteHtmlblock->description = 'Разрешение удалять html блоки';
        $auth->add($deleteHtmlblock);

        $rule = $auth->getRule('isAuthor');
        $updateOwnHtmlblock = $auth->createPermission('updateOwnHtmlblock');
        $updateOwnHtmlblock->description = 'Разрешение редактировать созданные html блоки';
        $updateOwnHtmlblock->ruleName = $rule->name;
        $auth->add($updateOwnHtmlblock);
        $auth->addChild($updateOwnHtmlblock, $updateHtmlblock);

        $htmlblockReader = $auth->createRole('htmlblockReader');
        $auth->add($htmlblockReader);
        $auth->addChild($htmlblockReader, $readHtmlblock);

        $htmlblockAuthor = $auth->createRole('htmlblockAuthor');
        $auth->add($htmlblockAuthor);
        $auth->addChild($htmlblockAuthor, $htmlblockReader);
        $auth->addChild($htmlblockAuthor, $createHtmlblock);
        $auth->addChild($htmlblockAuthor, $updateOwnHtmlblock);

        $htmlblockManager = $auth->createRole('htmlblockManager');
        $auth->add($htmlblockManager);
        $auth->addChild($htmlblockManager, $htmlblockAuthor);
        $auth->addChild($htmlblockManager, $updateHtmlblock);

        $htmlblockAdmin = $auth->createRole('htmlblockAdmin');
        $auth->add($htmlblockAdmin);
        $auth->addChild($htmlblockAdmin, $htmlblockManager);
        $auth->addChild($htmlblockAdmin, $deleteHtmlblock);

        $auth->addChild($admin, $htmlblockAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createHtmlblock = $auth->getPermission('createHtmlblock');
        $readHtmlblock = $auth->getPermission('readHtmlblock');
        $updateHtmlblock = $auth->getPermission('updateHtmlblock');
        $deleteHtmlblock = $auth->getPermission('deleteHtmlblock');
        $updateOwnHtmlblock = $auth->getPermission('updateOwnHtmlblock');

        $htmlblockReader = $auth->getRole('htmlblockReader');
        $htmlblockAuthor = $auth->getRole('htmlblockAuthor');
        $htmlblockManager = $auth->getRole('htmlblockManager');
        $htmlblockAdmin = $auth->getRole('htmlblockAdmin');

        $auth->removeChild($admin, $htmlblockAdmin);
        $auth->removeChildren($htmlblockAdmin);
        $auth->removeChildren($htmlblockManager);
        $auth->removeChildren($htmlblockAuthor);
        $auth->removeChildren($htmlblockReader);
        $auth->removeChildren($updateOwnHtmlblock);

        $auth->remove($htmlblockAdmin);
        $auth->remove($htmlblockManager);
        $auth->remove($htmlblockAuthor);
        $auth->remove($htmlblockReader);
        $auth->remove($updateOwnHtmlblock);
        $auth->remove($deleteHtmlblock);
        $auth->remove($updateHtmlblock);
        $auth->remove($readHtmlblock);
        $auth->remove($createHtmlblock);

        $this->delete('{{%htmlblock}}');

        $this->dropForeignKey('htmlblock_lang_htmlblock_id','{{%htmlblock_lang}}');
        $this->dropTable('{{%htmlblock_lang}}');

        $this->dropForeignKey('fk_htmlblock_website_id','{{%htmlblock}}');
        $this->dropForeignKey('fk_htmlblock_updater_id','{{%htmlblock}}');
        $this->dropForeignKey('fk_htmlblock_author_id','{{%htmlblock}}');

        $this->dropTable('{{%htmlblock}}');
    }

}
