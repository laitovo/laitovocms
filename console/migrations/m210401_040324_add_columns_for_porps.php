<?php

use yii\db\Migration;

class m210401_040324_add_columns_for_porps extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%laitovo_erp_naryad}}','withoutLabels',$this->boolean()->defaultValue(false)->comment('Без лейбы'));
        $this->addColumn('{{%laitovo_erp_naryad}}','halfStake',$this->boolean()->defaultValue(false)->comment('Пол ставки'));
        $this->addColumn('{{%logistics_order_entry}}','withoutLabels',$this->boolean()->defaultValue(false)->comment('Без лейбы'));
        $this->addColumn('{{%logistics_order_entry}}','halfStake',$this->boolean()->defaultValue(false)->comment('Пол ставки'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%laitovo_erp_naryad}}','withoutLabels');
        $this->dropColumn('{{%laitovo_erp_naryad}}','halfStake');
        $this->dropColumn('{{%logistics_order_entry}}','withoutLabels');
        $this->dropColumn('{{%logistics_order_entry}}','halfStake');
    }
}
