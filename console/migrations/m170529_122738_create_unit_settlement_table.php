<?php

use yii\db\Migration;

/**
 * Handles the creation of table `unit_settlement`.
 */
class m170529_122738_create_unit_settlement_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('unit_settlement', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->comment('Дата создания'),
            'material_parameter_id' => $this->integer()->comment('Материалы'),
            'autor_id' => $this->integer()->comment('Автор записи'),
            'unit_settlement' => $this->integer()->comment('Расчетная единица'),
            'coefficient_settlement' => $this->double()->comment('Коэффициент для расчета(взвешивание)'),
        ]);
        $this->createIndex(
            'idx-unit_settlement-material_parameter_id',
            'unit_settlement',
            'material_parameter_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-unit_settlement-material_parameter_id',
            'unit_settlement',
            'material_parameter_id',
            'material_parameters',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-unit_settlement-autor_id',
            'unit_settlement',
            'autor_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-unit_settlement-autor_id',
            'unit_settlement',
            'autor_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-unit_settlement-unit_settlement',
            'unit_settlement',
            'unit_settlement'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-unit_settlement-unit_settlement',
            'unit_settlement',
            'unit_settlement',
            'unit_materials',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
         $this->dropForeignKey(
            'fk-unit_settlement-material_parameter_id',
            'unit_settlement'
        );

        
        $this->dropIndex(
            'idx-unit_settlement-material_parameter_id',
            'unit_settlement'
        );
         $this->dropForeignKey(
            'fk-unit_settlement-autor_id',
            'unit_settlement'
        );


        $this->dropIndex(
            'idx-unit_settlement-autor_id',
            'unit_settlement'
        );
         $this->dropForeignKey(
            'fk-unit_settlement-unit_settlement',
            'unit_settlement'
        );

       
        $this->dropIndex(
            'idx-unit_settlement-unit_settlement',
            'unit_settlement'
        );
        $this->dropTable('unit_settlement');
    }
}
