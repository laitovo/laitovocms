<?php

use yii\db\Migration;

class m180212_100648_add_column_processed_to_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','processed',$this->boolean()->defaultValue(0)->comment('Состояние обработки'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','processed');
    }
}
