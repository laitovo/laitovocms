<?php

use yii\db\Migration;

class m161020_105010_laitovo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_config}}', [
            'id' => $this->primaryKey(),

            'name' => $this->string()->comment('Название'),

            'json' => $this->text(),
        ], $tableOptions);


        $this->createTable('{{%laitovo_cars}}', [
            'id' => $this->primaryKey(),
            'article' => $this->integer()->comment('Артикул'),

            'name' => $this->string()->comment('Название'),
            'mark' => $this->string()->comment('Марка'),
            'model' => $this->string()->comment('Модель'),
            'generation' => $this->integer()->comment('Поколение'),
            'carcass' => $this->string()->comment('Кузов'),
            'doors' => $this->integer()->comment('Дверей'),
            'firstyear' => $this->integer()->comment('Год начала производства'),
            'lastyear' => $this->integer()->comment('Год окончания производства'),
            'modification' => $this->string()->comment('Модификация'),
            'country' => $this->string()->comment('Страна производства'),
            'category' => $this->string()->comment('Категория цены'),

            'mounting' => $this->text()->comment('Крепления'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_cars_author_id','{{%laitovo_cars}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_cars_updater_id','{{%laitovo_cars}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->createTable('{{%laitovo_cars_clips}}', [
            'id' => $this->primaryKey(),
            'car_id' => $this->integer()->comment('Автомобиль'),

            'name' => $this->string()->comment('Наименование'),
            'type' => $this->string()->comment('Тип крепления'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_cars_clips_car_id','{{%laitovo_cars_clips}}','car_id','{{%laitovo_cars}}','id','CASCADE','CASCADE');
        $this->addForeignKey('fk_laitovo_cars_clips_author_id','{{%laitovo_cars_clips}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_cars_clips_updater_id','{{%laitovo_cars_clips}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->createTable('{{%laitovo_cars_clips_scheme}}', [
            'id' => $this->primaryKey(),
            'car_id' => $this->integer()->comment('Автомобиль'),
            'brand' => $this->string()->comment('Марка продукции'),
            'type' => $this->string()->comment('Вид продукции'),
            'type_clips' => $this->string()->comment('Тип зажима'),
            'window' => $this->string()->comment('Оконный проем'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_cars_clips_scheme_car_id','{{%laitovo_cars_clips_scheme}}','car_id','{{%laitovo_cars}}','id','CASCADE','CASCADE');
        $this->addForeignKey('fk_laitovo_cars_clips_scheme_author_id','{{%laitovo_cars_clips_scheme}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_cars_clips_scheme_updater_id','{{%laitovo_cars_clips_scheme}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->createTable('{{%laitovo_cars_act}}', [
            'id' => $this->primaryKey(),
            'car_id' => $this->integer()->comment('Автомобиль'),
            'parent_id' => $this->integer()->comment('Родитель'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_cars_act_car_id','{{%laitovo_cars_act}}','car_id','{{%laitovo_cars}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_cars_act_parent_id','{{%laitovo_cars_act}}','parent_id','{{%laitovo_cars_act}}','id','CASCADE','CASCADE');
        $this->addForeignKey('fk_laitovo_cars_act_author_id','{{%laitovo_cars_act}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_cars_act_updater_id','{{%laitovo_cars_act}}','updater_id','{{%user}}','id','SET NULL','CASCADE');


        $this->createTable('{{%laitovo_cars_analog}}', [
            'car_id' => $this->integer()->notNull(),
            'analog_id' => $this->integer()->notNull(),
            'json' => $this->text(),
            'PRIMARY KEY(car_id, analog_id)'
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_cars_analog_car_id', '{{%laitovo_cars_analog}}', 'car_id', '{{%laitovo_cars}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_laitovo_cars_analog_analog_id', '{{%laitovo_cars_analog}}', 'analog_id', '{{%laitovo_cars}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->delete('{{%laitovo_cars_analog}}');
        $this->dropForeignKey('fk_laitovo_cars_analog_analog_id','{{%laitovo_cars_analog}}');
        $this->dropForeignKey('fk_laitovo_cars_analog_car_id','{{%laitovo_cars_analog}}');
        $this->dropTable('{{%laitovo_cars_analog}}');

        $this->delete('{{%laitovo_cars_act}}');
        $this->dropForeignKey('fk_laitovo_cars_act_updater_id','{{%laitovo_cars_act}}');
        $this->dropForeignKey('fk_laitovo_cars_act_author_id','{{%laitovo_cars_act}}');
        $this->dropForeignKey('fk_laitovo_cars_act_parent_id','{{%laitovo_cars_act}}');
        $this->dropForeignKey('fk_laitovo_cars_act_car_id','{{%laitovo_cars_act}}');
        $this->dropTable('{{%laitovo_cars_act}}');

        $this->delete('{{%laitovo_cars_clips_scheme}}');
        $this->dropForeignKey('fk_laitovo_cars_clips_scheme_updater_id','{{%laitovo_cars_clips_scheme}}');
        $this->dropForeignKey('fk_laitovo_cars_clips_scheme_author_id','{{%laitovo_cars_clips_scheme}}');
        $this->dropForeignKey('fk_laitovo_cars_clips_scheme_car_id','{{%laitovo_cars_clips_scheme}}');
        $this->dropTable('{{%laitovo_cars_clips_scheme}}');

        $this->delete('{{%laitovo_cars_clips}}');
        $this->dropForeignKey('fk_laitovo_cars_clips_updater_id','{{%laitovo_cars_clips}}');
        $this->dropForeignKey('fk_laitovo_cars_clips_author_id','{{%laitovo_cars_clips}}');
        $this->dropForeignKey('fk_laitovo_cars_clips_car_id','{{%laitovo_cars_clips}}');
        $this->dropTable('{{%laitovo_cars_clips}}');

        $this->delete('{{%laitovo_cars}}');

        $this->dropForeignKey('fk_laitovo_cars_updater_id','{{%laitovo_cars}}');
        $this->dropForeignKey('fk_laitovo_cars_author_id','{{%laitovo_cars}}');

        $this->dropTable('{{%laitovo_cars}}');

        $this->delete('{{%laitovo_config}}');
        $this->dropTable('{{%laitovo_config}}');
    }

}
