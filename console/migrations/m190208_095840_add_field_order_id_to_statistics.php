<?php

use yii\db\Migration;

class m190208_095840_add_field_order_id_to_statistics extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_article_statistics}}','orderId',$this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_article_statistics}}','orderId');
    }

}
