<?php

use yii\db\Migration;

/**
 * Handles the creation of table `production_scheme`.
 */
class m170601_114415_create_production_scheme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%production_scheme}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->comment('Название схемы'),
            'scheme_id' => $this->integer()->comment('Название схемы'),
            'product_id' => $this->integer()->comment('Вид продукта'),
            'author_id' => $this->integer()->comment('Автор записи'),
            'created_at' => $this->integer()->comment('Дата создания'),
        ]);
        
         $this->addForeignKey('fk_production_scheme_type_scheme_id', '{{%production_scheme}}', 'scheme_id', '{{%laitovo_erp_scheme}}', 'id', 'SET NULL', 'CASCADE');
         $this->addForeignKey('fk_production_scheme_type_product_id', '{{%production_scheme}}', 'product_id', '{{%laitovo_erp_product_type}}', 'id', 'SET NULL', 'CASCADE');
         $this->addForeignKey('fk_production_scheme_type_author_id', '{{%production_scheme}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_production_scheme_type_scheme_id', '{{%production_scheme}}');
        $this->dropForeignKey('fk_production_scheme_type_product_id', '{{%production_scheme}}');
        $this->dropForeignKey('fk_production_scheme_type_author_id', '{{%production_scheme}}');
        $this->dropTable('production_scheme');
    }
}
