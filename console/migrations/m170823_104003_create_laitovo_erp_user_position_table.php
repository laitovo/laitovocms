<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_user_position`.
 */
class m170823_104003_create_laitovo_erp_user_position_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_erp_user_position', [
            'id' => $this->primaryKey(),
            'position_id'=>$this->integer()->comment('Должность'),
            'user_id'=>$this->integer()->comment('Сотрудник'),
            'date_work'=>$this->integer()->comment('Дата принятия'),
            'date_dismissed'=>$this->integer()->comment('Дата увольнения'),
            'article'=>$this->string()->comment('Личный артикул'),
        ]);

        $this->addForeignKey(
            'fk-laitovo_erp_user_position-user_id',
            '{{%laitovo_erp_user_position}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_user_position-position_id',
            '{{%laitovo_erp_user_position}}',
            'position_id',
            '{{%laitovo_erp_position}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_user_position-user_id', '{{%laitovo_erp_user_position}}');

        $this->dropForeignKey('fk-laitovo_erp_user_position-position_id', '{{%laitovo_erp_user_position}}');

        $this->dropTable('laitovo_erp_user_position');
    }
}
