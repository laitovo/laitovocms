<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_storage_product`.
 */
class m171220_052414_create_logistics_storage_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%logistics_storage_product}}', [
            //columns
            'storage_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'PRIMARY KEY(storage_id, product_id)'
        ], $tableOptions);

        $this->createIndex('idx_logistics_storage_product_storage_id', '{{%logistics_storage_product}}', 'storage_id');
        $this->createIndex('idx_logistics_storage_product_product_id', '{{%logistics_storage_product}}', 'product_id');

        $this->addForeignKey('fk_logistics_storage_product_storage_id', 
            '{{%logistics_storage_product}}', 'storage_id',
            '{{%logistics_storage}}', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_logistics_storage_product_product_id',
            '{{%logistics_storage_product}}', 'product_id',
            '{{%laitovo_erp_product_type}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_storage_product_product_id', '{{%logistics_storage_product}}');
        $this->dropForeignKey('fk_logistics_storage_product_storage_id', '{{%logistics_storage_product}}');

        $this->dropIndex('idx_logistics_storage_product_product_id', '{{%logistics_storage_product}}');
        $this->dropIndex('idx_logistics_storage_product_storage_id', '{{%logistics_storage_product}}');

        $this->dropTable('{{%logistics_storage_product}}');
    }
}
