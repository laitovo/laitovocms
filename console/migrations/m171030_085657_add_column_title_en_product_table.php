<?php

use yii\db\Migration;

class m171030_085657_add_column_title_en_product_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_product_type}}', 'title_en', $this->string()->comment('Английское наименование'));
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_product_type}}', 'title_en');
    }
}
