<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_131411_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),

            'auth_key' => $this->string(32),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string(),
            'access_token' => $this->string(),

            'name' => $this->string()->comment('ФИО'),
            'username' => $this->string()->comment('Логин'),
            'email' => $this->string()->comment('E-mail'),
            'phone' => $this->string()->comment('Телефон'),
            'role' => $this->string()->comment('Роль'),
            'image' => $this->string()->comment('Изображение'),
            'subscribe' => $this->boolean()->defaultValue(false)->comment('Подписка на рассылку'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),
            'discount' => $this->money(19,2)->comment('Скидка'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'website_id' => $this->integer()->comment('Сайт'),
            'language' => $this->string()->comment('Язык'),
            'timezone' => $this->string()->comment('Временная зона'),
            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('user_username','{{%user}}','website_id, username, status');
        $this->createIndex('user_email','{{%user}}','website_id, email, status');
        $this->createIndex('user_phone','{{%user}}','website_id, phone, status');
        $this->createIndex('user_access_token','{{%user}}','website_id, access_token, status');
        $this->createIndex('user_password_reset_token','{{%user}}','website_id, password_reset_token, status');
        $this->createIndex('user_auth_key','{{%user}}','auth_key');

        $this->addForeignKey('user_author_id','{{%user}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('user_updater_id','{{%user}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('auth_assignment_user_id','{{%auth_assignment}}','user_id','{{%user}}','id','CASCADE','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Разрешение добавлять пользователей';
        $auth->add($createUser);
        $readUser = $auth->createPermission('readUser');
        $readUser->description = 'Разрешение просматривать пользователей';
        $auth->add($readUser);
        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Разрешение редактировать пользователей';
        $auth->add($updateUser);
        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Разрешение удалять пользователей';
        $auth->add($deleteUser);

        $rule = new \common\rbac\AuthorRule;
        $auth->add($rule);
        $updateOwnUser = $auth->createPermission('updateOwnUser');
        $updateOwnUser->description = 'Разрешение редактировать созданных пользователей';
        $updateOwnUser->ruleName = $rule->name;
        $auth->add($updateOwnUser);
        $auth->addChild($updateOwnUser, $updateUser);

        $userReader = $auth->createRole('userReader');
        $auth->add($userReader);
        $auth->addChild($userReader, $readUser);

        $userAuthor = $auth->createRole('userAuthor');
        $auth->add($userAuthor);
        $auth->addChild($userAuthor, $userReader);
        $auth->addChild($userAuthor, $createUser);
        $auth->addChild($userAuthor, $updateOwnUser);

        $userManager = $auth->createRole('userManager');
        $auth->add($userManager);
        $auth->addChild($userManager, $userAuthor);
        $auth->addChild($userManager, $updateUser);

        $userAdmin = $auth->createRole('userAdmin');
        $auth->add($userAdmin);
        $auth->addChild($userAdmin, $userManager);
        $auth->addChild($userAdmin, $deleteUser);

        $auth->addChild($admin, $userAdmin);

    }

    public function down()
    {

        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $this->delete('{{%user}}');

        $this->dropForeignKey('auth_assignment_user_id','{{%auth_assignment}}');
        $this->dropForeignKey('user_updater_id','{{%user}}');
        $this->dropForeignKey('user_author_id','{{%user}}');

        $this->dropTable('{{%user}}');
    }
}
