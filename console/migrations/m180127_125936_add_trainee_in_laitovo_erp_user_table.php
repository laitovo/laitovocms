<?php

use yii\db\Migration;

/**
 * Class m180127_125936_add_trainee_in_laitovo_erp_user_table
 */
class m180127_125936_add_trainee_in_laitovo_erp_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('laitovo_erp_user', 'trainee', 'boolean');
    }

    public function down()
    {
        $this->dropColumn('laitovo_erp_user', 'trainee');
    }
}
