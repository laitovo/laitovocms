<?php

use yii\db\Migration;

class m170314_075926_laitovo_erp_index extends Migration
{
    public function up()
    {
        $this->createIndex('idx_laitovo_erp_order_status','{{%laitovo_erp_order}}','status');
        $this->createIndex('idx_laitovo_erp_order_sort','{{%laitovo_erp_order}}','sort');
        $this->createIndex('idx_laitovo_erp_order_created_at','{{%laitovo_erp_order}}','created_at');
        $this->createIndex('idx_laitovo_erp_order_updated_at','{{%laitovo_erp_order}}','updated_at');
        
        $this->createIndex('idx_laitovo_erp_location_barcode','{{%laitovo_erp_location}}','barcode');
        
        $this->createIndex('idx_laitovo_erp_naryad_barcode','{{%laitovo_erp_naryad}}','barcode');
        $this->createIndex('idx_laitovo_erp_naryad_reestr','{{%laitovo_erp_naryad}}','reestr');
        $this->createIndex('idx_laitovo_erp_naryad_article','{{%laitovo_erp_naryad}}','article');
        $this->createIndex('idx_laitovo_erp_naryad_status','{{%laitovo_erp_naryad}}','status');
        $this->createIndex('idx_laitovo_erp_naryad_sort','{{%laitovo_erp_naryad}}','sort');
        $this->createIndex('idx_laitovo_erp_naryad_created_at','{{%laitovo_erp_naryad}}','created_at');
        $this->createIndex('idx_laitovo_erp_naryad_updated_at','{{%laitovo_erp_naryad}}','updated_at');

        $this->createIndex('idx_laitovo_erp_user_barcode','{{%laitovo_erp_user}}','barcode');

        $this->addForeignKey('fk_laitovo_erp_naryad_start','{{%laitovo_erp_naryad}}','start','{{%laitovo_erp_scheme}}','id','SET NULL','CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_naryad_start','{{%laitovo_erp_naryad}}');
        
        $this->dropIndex('idx_laitovo_erp_order_status','{{%laitovo_erp_order}}');
        $this->dropIndex('idx_laitovo_erp_order_sort','{{%laitovo_erp_order}}');
        $this->dropIndex('idx_laitovo_erp_order_created_at','{{%laitovo_erp_order}}');
        $this->dropIndex('idx_laitovo_erp_order_updated_at','{{%laitovo_erp_order}}');
        
        $this->dropIndex('idx_laitovo_erp_location_barcode','{{%laitovo_erp_location}}');
        
        $this->dropIndex('idx_laitovo_erp_naryad_barcode','{{%laitovo_erp_naryad}}');
        $this->dropIndex('idx_laitovo_erp_naryad_reestr','{{%laitovo_erp_naryad}}');
        $this->dropIndex('idx_laitovo_erp_naryad_article','{{%laitovo_erp_naryad}}');
        $this->dropIndex('idx_laitovo_erp_naryad_status','{{%laitovo_erp_naryad}}');
        $this->dropIndex('idx_laitovo_erp_naryad_sort','{{%laitovo_erp_naryad}}');
        $this->dropIndex('idx_laitovo_erp_naryad_created_at','{{%laitovo_erp_naryad}}');
        $this->dropIndex('idx_laitovo_erp_naryad_updated_at','{{%laitovo_erp_naryad}}');

        $this->dropIndex('idx_laitovo_erp_user_barcode','{{%laitovo_erp_user}}');
    }
}
