<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_location_property_value`.
 */
class m170608_132142_create_laitovo_erp_location_property_value_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_location_workplace_property_value}}', [
            'id' => $this->primaryKey(),
            'workplace_id' => $this->integer(),
            'property_id' => $this->string(),
            'value' => $this->string(),
            //columns
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),

            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);


        $this->addForeignKey('fk_laitovo_erp_location_workplace_property_value_workplace_id',
            '{{%laitovo_erp_location_workplace_property_value}}', 'workplace_id',
            '{{%laitovo_erp_location_workplace}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_location_workplace_property_value_author_id',
            '{{%laitovo_erp_location_workplace_property_value}}', 'author_id',
            '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_location_workplace_property_value_updater_id',
            '{{%laitovo_erp_location_workplace_property_value}}', 'updater_id',
            '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_property_value_workplace_id', '{{%laitovo_erp_location_workplace_property_value}}');

        $this->dropForeignKey('fk_laitovo_erp_location_workplace_property_value_author_id', '{{%laitovo_erp_location_workplace_property_value}}');

        $this->dropForeignKey('fk_laitovo_erp_location_workplace_property_value_updater_id', '{{%laitovo_erp_location_workplace_property_value}}');

        $this->dropTable('{{%laitovo_erp_location_workplace_property_value}}');
    }
}
