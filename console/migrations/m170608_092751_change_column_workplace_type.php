<?php

use yii\db\Migration;

class m170608_092751_change_column_workplace_type extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%laitovo_erp_location_workplace}}', 'type');
        $this->addColumn('{{%laitovo_erp_location_workplace}}', 'type_id', $this->integer());

        $this->addForeignKey('fk_laitovo_erp_location_workplace_type_id',
            '{{%laitovo_erp_location_workplace}}', 'type_id',
            '{{%laitovo_erp_location_workplace_type}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_type_id', '{{%laitovo_erp_location_workplace}}');

        $this->dropColumn('{{%laitovo_erp_location_workplace}}', 'type_id');
        $this->addColumn('{{%laitovo_erp_location_workplace}}', 'type', $this->string());
    }
}
