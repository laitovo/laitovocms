<?php

use yii\db\Migration;

class m170608_072239_create_laitovo_erp_location_workplace_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_location_workplace_type}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            //columns
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_location_workplace_type_author_id',
            '{{%laitovo_erp_location_workplace_type}}', 'author_id',
            '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_laitovo_erp_location_workplace_type_updater_id',
            '{{%laitovo_erp_location_workplace_type}}', 'updater_id',
            '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_type_author_id', '{{%laitovo_erp_location_workplace_type}}');
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_type_updater_id', '{{%laitovo_erp_location_workplace_type}}');
        
        $this->dropTable('{{%laitovo_erp_location_workplace_type}}');
    }
}
