<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_fine`.
 */
class m170919_074240_create_laitovo_erp_fine_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_erp_fine', [
            'id' => $this->primaryKey(),
            'user_id'=> $this->integer(),
            'description'=> $this->text(),
            'amount'=>$this->double(),
            'created_at'=> $this->integer(),
        ]);
        $this->createIndex(
            'idx-laitovo_erp_fine-user_id',
            'laitovo_erp_fine',
            'user_id'
        );

        $this->addForeignKey(
            'fk-laitovo_erp_fine-user_id',
            '{{%laitovo_erp_fine}}',
            'user_id',
            '{{%laitovo_erp_user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_fine-user_id','laitovo_erp_fine');

        $this->dropIndex('idx-laitovo_erp_fine-user_id','laitovo_erp_fine');

        $this->dropTable('laitovo_erp_fine');
    }
}
