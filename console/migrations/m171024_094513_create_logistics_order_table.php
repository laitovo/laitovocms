<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_order`.
 */
class m171024_094513_create_logistics_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%logistics_order}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(),
            'source_id' => $this->integer()->comment('Источник поступления'),
            'responsible_person' => $this->string()->comment('Ответственное лицо'),
            'comment' => $this->text()->comment('Комментарий к заявке'),
            //columns
            'created_at' => $this->integer(),
            'author_id' =>  $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_order_source_id', 
            '{{%logistics_order}}', 'source_id', //from
            '{{%logistics_sources}}', 'id',       //to
            'SET NULL', // ON DELETE 
            'CASCADE'   // ON UPDATE
        );
        $this->addForeignKey('fk_logistics_order_team_id', '{{%logistics_order}}', 'team_id', '{{%team}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_order_author_id', '{{%logistics_order}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_order_updater_id', '{{%logistics_order}}', 'updater_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_order_updater_id', '{{%logistics_order}}');
        $this->dropForeignKey('fk_logistics_order_author_id', '{{%logistics_order}}');
        $this->dropForeignKey('fk_logistics_order_team_id', '{{%logistics_order}}');
        $this->dropForeignKey('fk_logistics_order_source_id', '{{%logistics_order}}');

        $this->dropTable('{{%logistics_order}}');
    }
}
