<?php

use yii\db\Migration;

class m171024_101758_create_logistics_order_entry_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%logistics_order_entry}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'title' => $this->string()->comment('Наименование'),
            'article' => $this->string()->comment('Артикул'),
            'quantity' => $this->integer()->comment('Количество'),
            'comment' => $this->text()->comment('Комментарий к позиции'),
            //columns
            'created_at' => $this->integer(),
            'author_id' =>  $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_order_entry_order_id', 
            '{{%logistics_order_entry}}', 'order_id', //from
            '{{%logistics_order}}', 'id',       //to
            'SET NULL', // ON DELETE 
            'CASCADE'   // ON UPDATE
        );
        $this->addForeignKey('fk_logistics_order_entry_author_id', '{{%logistics_order_entry}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_order_entry_updater_id', '{{%logistics_order_entry}}', 'updater_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_logistics_order_entry_updater_id', '{{%logistics_order_entry}}');
        $this->dropForeignKey('fk_logistics_order_entry_author_id', '{{%logistics_order_entry}}');
        $this->dropForeignKey('fk_logistics_order_entry_order_id', '{{%logistics_order_entry}}');

        $this->dropTable('{{%logistics_order_entry}}');
    }
}
