<?php

use yii\db\Migration;

class m180726_062033_add_upn_group_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_naryad}}','group',$this->integer()->comment('Группа объединения UPN - ов'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_naryad}}','group');
    }
}
