<?php

use yii\db\Migration;

class m170607_054701_add_column_activity extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_erp_location_workplace}}', 'activity', $this->integer());
    }   

    public function down()
    {
        $this->dropColumn('{{%laitovo_erp_location_workplace}}', 'activity');
    }
}
