<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_pattern_register`.
 */
class m171009_071654_create_laitovo_erp_pattern_register_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_erp_pattern_register', [
            'id' => $this->primaryKey(),
            'car_id' => $this->integer(),
            'pattern_article' => $this->string(),
            'product_type' => $this->string(),
            'window_type' => $this->string(),
        ]);

        $this->addForeignKey('fk-laitovo_erp_pattern_register-car_id', 'laitovo_erp_pattern_register', 'car_id', 'laitovo_cars', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-laitovo_erp_pattern_register-car_id', 'laitovo_erp_pattern_register');
        $this->dropTable('laitovo_erp_pattern_register');
    }
}
