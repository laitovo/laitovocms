<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_automatic_order`.
 */
class m171103_071504_create_logistics_automatic_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%logistics_automatic_order}}', [
            'id'                => $this->primaryKey(),
            'team_id'           => $this->integer()->comment('Команда'),
            'measuringInterval' => $this->integer()->comment('Интервал измерения'),
            'unitOfPeriod'      => $this->integer()->comment('Единица измерения периода'),
            //columns
            'created_at' => $this->integer(),
            'author_id'  => $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_automatic_order_author_id', '{{%logistics_automatic_order}}', 
            'author_id', '{{%user}}', 'id', 
            'SET NULL', 'CASCADE');

        $this->addForeignKey('fk_logistics_automatic_order_updater_id', '{{%logistics_automatic_order}}', 
            'updater_id', '{{%user}}', 'id', 
            'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_automatic_order_updater_id', '{{%logistics_automatic_order}}');
        $this->dropForeignKey('fk_logistics_automatic_order_author_id', '{{%logistics_automatic_order}}');
        
        $this->dropTable('{{%logistics_automatic_order}}');
    }
}
