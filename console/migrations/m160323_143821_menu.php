<?php

use yii\db\Schema;
use yii\db\Migration;

class m160323_143821_menu extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey(),
            'website_id' => $this->integer()->notNull()->comment('Сайт'),

            'type' => $this->string()->comment('Тип меню'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),
        ], $tableOptions);

        $this->createIndex('idx_menu_type','{{%menu}}','website_id, type',true);
        $this->createIndex('idx_menu_website_id','{{%menu}}','website_id, type, status');

        $this->addForeignKey('fk_menu_author_id','{{%menu}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_menu_updater_id','{{%menu}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_menu_website_id','{{%menu}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $this->createTable('{{%menu_lang}}', [
            'menu_id' => $this->integer()->notNull()->comment('Меню'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),

            'PRIMARY KEY(menu_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('menu_lang_menu_id','{{%menu_lang}}','menu_id','{{%menu}}','id','CASCADE','CASCADE');


        $this->createTable('{{%menuitem}}', [
            'id' => $this->primaryKey(),
            'menu_id' => $this->integer()->notNull()->comment('Меню'),
            'parent_id' => $this->integer()->comment('Родитель'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),
        ], $tableOptions);

        $this->createIndex('idx_menuitem_menu_id','{{%menuitem}}','menu_id, parent_id, status, sort');
        $this->createIndex('idx_menuitem_parent_id','{{%menuitem}}','parent_id, status, sort');

        $this->addForeignKey('fk_menuitem_author_id','{{%menuitem}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_menuitem_updater_id','{{%menuitem}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_menuitem_menu_id','{{%menuitem}}','menu_id','{{%menu}}','id','CASCADE','CASCADE');

        $this->addForeignKey('fk_menuitem_parent_id','{{%menuitem}}','parent_id','{{%menuitem}}','id','CASCADE','CASCADE');

        $this->createTable('{{%menuitem_lang}}', [
            'menuitem_id' => $this->integer()->notNull()->comment('Пункт меню'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),
            'link' => $this->string()->comment('Ссылка'),
            'image' => $this->string()->comment('Изображение'),

            'PRIMARY KEY(menuitem_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('menuitem_lang_menuitem_id','{{%menuitem_lang}}','menuitem_id','{{%menuitem}}','id','CASCADE','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createMenu = $auth->createPermission('createMenu');
        $createMenu->description = 'Разрешение добавлять меню';
        $auth->add($createMenu);
        $readMenu = $auth->createPermission('readMenu');
        $readMenu->description = 'Разрешение просматривать меню';
        $auth->add($readMenu);
        $updateMenu = $auth->createPermission('updateMenu');
        $updateMenu->description = 'Разрешение редактировать меню';
        $auth->add($updateMenu);
        $deleteMenu = $auth->createPermission('deleteMenu');
        $deleteMenu->description = 'Разрешение удалять меню';
        $auth->add($deleteMenu);

        $rule = $auth->getRule('isAuthor');
        $updateOwnMenu = $auth->createPermission('updateOwnMenu');
        $updateOwnMenu->description = 'Разрешение редактировать созданные меню';
        $updateOwnMenu->ruleName = $rule->name;
        $auth->add($updateOwnMenu);
        $auth->addChild($updateOwnMenu, $updateMenu);

        $menuReader = $auth->createRole('menuReader');
        $auth->add($menuReader);
        $auth->addChild($menuReader, $readMenu);

        $menuAuthor = $auth->createRole('menuAuthor');
        $auth->add($menuAuthor);
        $auth->addChild($menuAuthor, $menuReader);
        $auth->addChild($menuAuthor, $createMenu);
        $auth->addChild($menuAuthor, $updateOwnMenu);

        $menuManager = $auth->createRole('menuManager');
        $auth->add($menuManager);
        $auth->addChild($menuManager, $menuAuthor);
        $auth->addChild($menuManager, $updateMenu);

        $menuAdmin = $auth->createRole('menuAdmin');
        $auth->add($menuAdmin);
        $auth->addChild($menuAdmin, $menuManager);
        $auth->addChild($menuAdmin, $deleteMenu);

        $auth->addChild($admin, $menuAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createMenu = $auth->getPermission('createMenu');
        $readMenu = $auth->getPermission('readMenu');
        $updateMenu = $auth->getPermission('updateMenu');
        $deleteMenu = $auth->getPermission('deleteMenu');
        $updateOwnMenu = $auth->getPermission('updateOwnMenu');

        $menuReader = $auth->getRole('menuReader');
        $menuAuthor = $auth->getRole('menuAuthor');
        $menuManager = $auth->getRole('menuManager');
        $menuAdmin = $auth->getRole('menuAdmin');

        $auth->removeChild($admin, $menuAdmin);
        $auth->removeChildren($menuAdmin);
        $auth->removeChildren($menuManager);
        $auth->removeChildren($menuAuthor);
        $auth->removeChildren($menuReader);
        $auth->removeChildren($updateOwnMenu);

        $auth->remove($menuAdmin);
        $auth->remove($menuManager);
        $auth->remove($menuAuthor);
        $auth->remove($menuReader);
        $auth->remove($updateOwnMenu);
        $auth->remove($deleteMenu);
        $auth->remove($updateMenu);
        $auth->remove($readMenu);
        $auth->remove($createMenu);


        $this->delete('{{%menuitem}}');

        $this->dropForeignKey('menuitem_lang_menuitem_id','{{%menuitem_lang}}');
        $this->dropTable('{{%menuitem_lang}}');

        $this->dropForeignKey('fk_menuitem_parent_id','{{%menuitem}}');
        $this->dropForeignKey('fk_menuitem_menu_id','{{%menuitem}}');
        $this->dropForeignKey('fk_menuitem_updater_id','{{%menuitem}}');
        $this->dropForeignKey('fk_menuitem_author_id','{{%menuitem}}');

        $this->dropTable('{{%menuitem}}');


        $this->delete('{{%menu}}');

        $this->dropForeignKey('menu_lang_menu_id','{{%menu_lang}}');
        $this->dropTable('{{%menu_lang}}');

        $this->dropForeignKey('fk_menu_website_id','{{%menu}}');
        $this->dropForeignKey('fk_menu_updater_id','{{%menu}}');
        $this->dropForeignKey('fk_menu_author_id','{{%menu}}');

        $this->dropTable('{{%menu}}');
    }

}
