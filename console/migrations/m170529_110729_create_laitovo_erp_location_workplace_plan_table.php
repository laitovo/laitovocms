<?php

use yii\db\Migration;

/**
 * Handles the creation of table `laitovo_erp_location_workplace_plan`.
 */
class m170529_110729_create_laitovo_erp_location_workplace_plan_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_location_workplace_plan}}', [
            'id' => $this->primaryKey(),
            'workplace_id' => $this->integer(),
            'user_id' => $this->integer()->comment('сотрудник на участке'),
            'time_from' => $this->integer(11)->comment('время начала смены'),
            'time_to' => $this->integer(11)->comment('время окончания смены'),
            //columns
            'created_at' => $this->integer(),
            'author_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_location_workplace_plan_workplace_id', '{{%laitovo_erp_location_workplace_plan}}', 'workplace_id', '{{%laitovo_erp_location_workplace}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_laitovo_erp_location_workplace_plan_user_id', '{{%laitovo_erp_location_workplace_plan}}', 'user_id', '{{%laitovo_erp_user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_laitovo_erp_location_workplace_plan_author_id', '{{%laitovo_erp_location_workplace_plan}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_plan_author_id', '{{%laitovo_erp_location_workplace_plan}}');
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_plan_user_id', '{{%laitovo_erp_location_workplace_plan}}');
        $this->dropForeignKey('fk_laitovo_erp_location_workplace_plan_workplace_id', '{{%laitovo_erp_location_workplace_plan}}');
        
        $this->dropTable('{{%laitovo_erp_location_workplace_plan}}');
    }
}
