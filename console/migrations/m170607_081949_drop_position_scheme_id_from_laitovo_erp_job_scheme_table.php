<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `position_scheme_id_from_laitovo_erp_job_scheme`.
 */
class m170607_081949_drop_position_scheme_id_from_laitovo_erp_job_scheme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey('fk-job_sheme-scheme_id', '{{%laitovo_erp_job_scheme}}');
        $this->dropColumn('laitovo_erp_job_scheme', 'scheme_id');
        $this->addColumn('laitovo_erp_job_scheme', 'production_scheme_id', 'integer');
        $this->addForeignKey(
            'fk-job_sheme-production_scheme_id',
            '{{%laitovo_erp_job_scheme}}',
            'production_scheme_id',
            '{{%production_scheme}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {  
       $this->dropForeignKey('fk-job_sheme-production_scheme_id', '{{%laitovo_erp_job_scheme}}');
       $this->dropColumn('laitovo_erp_job_scheme', 'production_scheme_id');
       $this->addColumn('laitovo_erp_job_scheme', 'scheme_id','integer');
       $this->addForeignKey(
            'fk-job_sheme-scheme_id',
            '{{%laitovo_erp_job_scheme}}',
            'scheme_id',
            '{{%laitovo_erp_scheme}}',
            'id',
            'CASCADE'
        );
    }
}
