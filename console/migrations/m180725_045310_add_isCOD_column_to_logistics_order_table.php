<?php

use yii\db\Migration;

/**
 * Handles adding isCOD to table `logistics_order`.
 */
class m180725_045310_add_isCOD_column_to_logistics_order_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%logistics_order}}','isCOD',$this->boolean()->defaultValue(false)->comment('Заказ оплачивается наложенным платежем'));
    }

    public function down()
    {
        $this->dropColumn('{{%logistics_order}}','isCOD');
    }
}
