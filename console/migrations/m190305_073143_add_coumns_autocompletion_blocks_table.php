<?php

use yii\db\Migration;

class m190305_073143_add_coumns_autocompletion_blocks_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_autocompletion_blocks}}','carTitle',$this->string()->comment('Наименование автомобиля'));
        $this->addColumn('{{%logistics_autocompletion_blocks}}','productTitle',$this->string()->comment('Наименование продукта'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_autocompletion_blocks}}','productTitle');
        $this->dropColumn('{{%logistics_autocompletion_blocks}}','carTitle');
    }
}
