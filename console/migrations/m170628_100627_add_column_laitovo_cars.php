<?php

use yii\db\Migration;

class m170628_100627_add_column_laitovo_cars extends Migration
{
    public function up()
    {
        $this->addColumn('{{%laitovo_cars}}', 'mastered', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%laitovo_cars}}', 'mastered');
    }
}
