<?php

use yii\db\Migration;

class m190211_110756_add_column_set_index_to_order_entry_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%logistics_order_entry}}','setIndex',$this->integer()->after('settext')->comment('индекс комплекта'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%logistics_order_entry}}','setIndex');
    }
}
