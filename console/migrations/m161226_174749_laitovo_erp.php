<?php

use yii\db\Migration;

class m161226_174749_laitovo_erp extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%laitovo_erp_order}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),

            'status' => $this->string()->comment('Статус'),
            'sort' => $this->integer()->defaultValue(4)->comment('Приоритет'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_order_author_id','{{%laitovo_erp_order}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_order_updater_id','{{%laitovo_erp_order}}','updater_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_order_order_id','{{%laitovo_erp_order}}','order_id','{{%order}}','id','SET NULL','CASCADE');


        $this->createTable('{{%laitovo_erp_location}}', [
            'id' => $this->primaryKey(),

            'barcode' => $this->string()->comment('Штрихкод'),
            'name' => $this->string()->comment('Название'),
            'status' => $this->string()->comment('Статус'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_location_author_id','{{%laitovo_erp_location}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_location_updater_id','{{%laitovo_erp_location}}','updater_id','{{%user}}','id','SET NULL','CASCADE');


        $this->createTable('{{%laitovo_erp_scheme}}', [
            'id' => $this->primaryKey(),

            'status' => $this->string()->comment('Статус'),
            'name' => $this->string()->comment('Название'),
            'rule' => $this->text(),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_scheme_author_id','{{%laitovo_erp_scheme}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_scheme_updater_id','{{%laitovo_erp_scheme}}','updater_id','{{%user}}','id','SET NULL','CASCADE');


        $this->createTable('{{%laitovo_erp_naryad}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->comment('Родитель'),
            'barcode' => $this->string()->comment('Штрихкод'),
            'reestr' => $this->string()->comment('Реестр'),
            'article' => $this->string()->comment('Артикул'),

            'order_id' => $this->integer()->notNull()->comment('Заказ'),
            'scheme_id' => $this->integer()->comment('Схема'),
            'location_id' => $this->integer()->comment('Участок'),
            'status' => $this->string()->comment('Статус'),
            'sort' => $this->integer()->defaultValue(4)->comment('Приоритет'),
            'start' => $this->integer()->comment('Первый участок'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_naryad_parent_id','{{%laitovo_erp_naryad}}','parent_id','{{%laitovo_erp_naryad}}','id','CASCADE','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_naryad_author_id','{{%laitovo_erp_naryad}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_naryad_updater_id','{{%laitovo_erp_naryad}}','updater_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_naryad_order_id','{{%laitovo_erp_naryad}}','order_id','{{%laitovo_erp_order}}','id','CASCADE','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_naryad_scheme_id','{{%laitovo_erp_naryad}}','scheme_id','{{%laitovo_erp_scheme}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_naryad_location_id','{{%laitovo_erp_naryad}}','location_id','{{%laitovo_erp_location}}','id','SET NULL','CASCADE');

        $this->createTable('{{%laitovo_erp_user}}', [
            'id' => $this->primaryKey(),
            'barcode' => $this->string()->comment('Штрихкод'),
            'name' => $this->string()->comment('ФИО'),

            'location_id' => $this->integer()->comment('Участок'),
            'status' => $this->string()->comment('Статус'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_laitovo_erp_user_author_id','{{%laitovo_erp_user}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_user_updater_id','{{%laitovo_erp_user}}','updater_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_laitovo_erp_user_location_id','{{%laitovo_erp_user}}','location_id','{{%laitovo_erp_location}}','id','SET NULL','CASCADE');

    }

    public function down()
    {
        $this->delete('{{%laitovo_erp_user}}');
        $this->dropForeignKey('fk_laitovo_erp_user_location_id','{{%laitovo_erp_user}}');
        $this->dropForeignKey('fk_laitovo_erp_user_updater_id','{{%laitovo_erp_user}}');
        $this->dropForeignKey('fk_laitovo_erp_user_author_id','{{%laitovo_erp_user}}');
        $this->dropTable('{{%laitovo_erp_user}}');

        $this->delete('{{%laitovo_erp_naryad}}');
        $this->dropForeignKey('fk_laitovo_erp_naryad_location_id','{{%laitovo_erp_naryad}}');
        $this->dropForeignKey('fk_laitovo_erp_naryad_scheme_id','{{%laitovo_erp_naryad}}');
        $this->dropForeignKey('fk_laitovo_erp_naryad_order_id','{{%laitovo_erp_naryad}}');
        $this->dropForeignKey('fk_laitovo_erp_naryad_updater_id','{{%laitovo_erp_naryad}}');
        $this->dropForeignKey('fk_laitovo_erp_naryad_author_id','{{%laitovo_erp_naryad}}');
        $this->dropForeignKey('fk_laitovo_erp_naryad_parent_id','{{%laitovo_erp_naryad}}');
        $this->dropTable('{{%laitovo_erp_naryad}}');

        $this->delete('{{%laitovo_erp_scheme}}');
        $this->dropForeignKey('fk_laitovo_erp_scheme_updater_id','{{%laitovo_erp_scheme}}');
        $this->dropForeignKey('fk_laitovo_erp_scheme_author_id','{{%laitovo_erp_scheme}}');
        $this->dropTable('{{%laitovo_erp_scheme}}');

        $this->delete('{{%laitovo_erp_location}}');
        $this->dropForeignKey('fk_laitovo_erp_location_updater_id','{{%laitovo_erp_location}}');
        $this->dropForeignKey('fk_laitovo_erp_location_author_id','{{%laitovo_erp_location}}');
        $this->dropTable('{{%laitovo_erp_location}}');

        $this->delete('{{%laitovo_erp_order}}');
        $this->dropForeignKey('fk_laitovo_erp_order_order_id','{{%laitovo_erp_order}}');
        $this->dropForeignKey('fk_laitovo_erp_order_updater_id','{{%laitovo_erp_order}}');
        $this->dropForeignKey('fk_laitovo_erp_order_author_id','{{%laitovo_erp_order}}');
        $this->dropTable('{{%laitovo_erp_order}}');
    }

}
