<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics_storage`.
 */
class m171024_041124_create_logistics_storage_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%logistics_storage}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(),
            'title' => $this->string()->comment('Наименование'),
            'type' => $this->smallInteger()->comment('Вид места хранения'),
            //columns
            'created_at' => $this->integer(),
            'author_id' =>  $this->integer(),
            'updated_at' => $this->integer(),
            'updater_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_logistics_storage_team_id', '{{%logistics_storage}}', 'team_id', '{{%team}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_storage_author_id', '{{%logistics_storage}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_logistics_storage_updater_id', '{{%logistics_storage}}', 'updater_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_logistics_storage_updater_id', '{{%logistics_storage}}');
        $this->dropForeignKey('fk_logistics_storage_author_id', '{{%logistics_storage}}');
        $this->dropForeignKey('fk_logistics_storage_team_id', '{{%logistics_storage}}');

        $this->dropTable('{{%logistics_storage}}');
    }
}
