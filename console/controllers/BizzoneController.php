<?php
namespace console\controllers;

use backend\modules\laitovo\models\CarsForm;
use backend\modules\laitovo\models\ErpOrder;
use backend\modules\laitovo\models\ErpUser;
use backend\modules\laitovo\models\ErpUsersReport;
use backend\modules\logistics\models\Order;
use backend\modules\logistics\modules\europeInvoice\models\SEntryFill;
use common\models\laitovo\Config;
use common\models\laitovo\ErpNaryad;
use common\models\Log;
use common\models\logistics\Naryad;
use core\logic\DatePeriodManager;
use core\logic\Equipment;
use core\logic\RelevanceReport;
use core\logic\UpnGroup;
use core\logic\UsingTemplateReport;
use core\models\article\Article;
use core\models\articleAnalog\ArticleAnalog;
use core\models\articleAnalog\ArticleAnalogGenerator;
use core\models\articleStatistics\ArticleStatisticsGenerator;
use core\models\europeInvoice\EuropeInvoice;
use obmen\models\laitovo\Cars;
use obmen\models\laitovo\CarsAnalog;
use Yii;
use common\models\logistics\ArticleStatistics;
use yii\base\InvalidConfigException;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use core\models\shipment\Shipment;
use core\services\SShipmentSearch;
use backend\modules\logistics\models\StorageState;

class BizzoneController extends \yii\console\Controller
{
    /**
     * Удаление логов за посление года
     */
    public function actionDeleteLog()
    {
        $logs = Log::find()->where(['<','date',strtotime('- 2 years')])->limit(5000)->all();
        foreach ($logs as $log) {
            echo "Запись № {$log->id} готовится к удалению" . PHP_EOL;
            $log->delete();
            echo "Запись удалена" . PHP_EOL;
        }
    }

    /**
     * Вытаскиваем статистику с бизона
     */
    public function actionLoadLogistStatist()
    {
        $command = Yii::$app->db->createCommand("
            SELECT 
                laitovo_erp_naryad.created_at as created_at,
                laitovo_erp_naryad.article as article 
            FROM laitovo_erp_naryad

            WHERE
                status = 'Готов' OR status = 'Со склада'
            ORDER BY created_at ASC
        ");

        $rows = $command->queryAll();

        foreach ($rows as $row) {
            $statistics             = new ArticleStatistics(); 
            $statistics->article    = isset($row['article']) ? $row['article'] : null; 
            $statistics->created_at = isset($row['created_at']) ? $row['created_at'] : null; 
            $statistics->type       = ArticleStatistics::TYPE_BIZZONE; 
            $statistics->save(); 
        }
    }

    public function actionLoadStatistics()
    {
        $scriptSt = microtime(true);
        $deleteDate = time();
        $globalCount = 0;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
          SELECT DISTINCT article FROM logistics_article_statistics
          WHERE created_at > ". ((time()) - 60*60*24*31*7) ."
          AND article not like 'OT-%'
        ");

        $rows1 = $command->queryColumn();

        $command = $connection->createCommand("
          SELECT DISTINCT article FROM tmp_statistics
        ");

        $rows2 = $command->queryColumn();

        $rows = ArrayHelper::merge($rows1,$rows2);
        $rows = array_unique($rows);

        $readyArticles = [];

        $allCount = count($rows);

        foreach ($rows as $row) {
            $globalCount++;
            $countt = count($readyArticles);
            $startd = microtime(true);
            $articleObj = new Article($row);
            if (!in_array($articleObj->id,$readyArticles)) {
                $readyArticles = array_merge($readyArticles,$articleObj->articleWithAnalogs);

                if (in_array($articleObj->carArticle,['1635'])) {
                    $end = microtime(true);
                    $rtime = $end - $startd;
                    echo '#########################'. PHP_EOL .'all : ' . $allCount . PHP_EOL . 'left : ' . ($allCount-$globalCount) . PHP_EOL . 'position : ' . $globalCount . PHP_EOL . 'articles : ' . $countt . PHP_EOL . 'time : ' . $rtime . PHP_EOL;
                    continue;

                } //Не пишем статистику по inside auto
                if (!$articleObj->balancePlan && !$articleObj->saleSixMonth) {
                    $end = microtime(true);
                    $rtime = $end - $startd;
                    echo '#########################'. PHP_EOL .'all : ' . $allCount . PHP_EOL . 'left : ' . ($allCount-$globalCount) . PHP_EOL . 'position : ' . $globalCount . PHP_EOL . 'articles : ' . $countt . PHP_EOL . 'time : ' . $rtime . PHP_EOL;
                    continue;
                } //Не пишем статистику по inside auto

                //Получим фактические остатки.

                $query = new Query();
                $query->select('article')
                    ->from('laitovo_erp_naryad')
                    ->where('logist_id is not null')
                    ->andWhere(['distributed' => 0])
                    ->andWhere(['IN', 'article', $articleObj->articleWithAnalogs]);

                $prod = $query->count() ?: 0;

                $fact = $prod + $articleObj->balanceFact;
                $relInMonth = floor($articleObj->saleSixMonth / 6);
                $plan = $relInMonth > $articleObj->balancePlan ? $relInMonth : $articleObj->balancePlan;
                $abs = ($plan - $fact) > 0 ? $plan - $fact : 0;
                if (!($abs > 0)) {
                    continue;
                }
                $rel = !$plan ? 0 : (($plan - $fact) < 0 ? 0 : (($plan - $fact) / $plan) * 100);

                $bool = (new Query)
                    ->select('article')
                    ->from('tmp_statistics')
                    ->where('article = :article')
                    ->addParams([':article' => $articleObj->id])
                    ->exists();

                if ($bool) {
                    $connection->createCommand()->update('tmp_statistics', [
                        'prefix' => $articleObj->windowEn,
                        'car' => $articleObj->carName,
                        'title' => $articleObj->title,
                        'analogs' => implode('<br>',$articleObj->carNameWithAnalogs),
                        'needAbsolute' => $abs,
                        'needRelative' => $rel,
                        'plan' => $articleObj->balancePlan,
                        'fact' => $articleObj->balanceFact,
                        'prod' => $prod,
                        'updated_at' => time(),
                    ], 'article = :article', [':article' => $articleObj->id])->execute();
                } else {
                    $connection->createCommand()->insert('tmp_statistics', [
                        'article' => $articleObj->id,
                        'title' => $articleObj->title,
                        'analogs' => implode('<br>',$articleObj->carNameWithAnalogs),
                        'prefix' => $articleObj->windowEn,
                        'car' => $articleObj->carName,
                        'needAbsolute' => $abs,
                        'needRelative' => $rel,
                        'plan' => $articleObj->balancePlan,
                        'fact' => $articleObj->balanceFact,
                        'prod' => $prod,
                        'updated_at' => time(),
                    ])->execute();
                }
            }
            $end = microtime(true);
            $rtime = $end - $startd;
            echo '#########################'. PHP_EOL .'all : ' . $allCount . PHP_EOL . 'left : ' . ($allCount-$globalCount) . PHP_EOL . 'position : ' . $globalCount . PHP_EOL . 'articles : ' . $countt . PHP_EOL . 'time : ' . $rtime . PHP_EOL;
        }

        $crows = $connection->createCommand()->delete('tmp_statistics', 'updated_at < :date',
        [':date' => $deleteDate])->execute();

        $scriptEnd = microtime(true);
        $sctime = ($scriptEnd - $scriptSt) / (60*60) ;
        echo 'deleted : ' . $crows . PHP_EOL . 'duration : ' . $sctime . PHP_EOL;
    }

    /**
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionGenerateArticleAnalogs()
    {
        $articles = ArticleAnalog::find()->select('article')->distinct()->column();
        $generator = new ArticleAnalogGenerator();
        $articlesCount = count($articles);
        $rowArticleCount = 0;
        foreach ($articles as $article) {
            $rowArticleCount++;
            echo "Всего артикулов: {$articlesCount}"  . PHP_EOL;
            echo "Текущая позиция: {$rowArticleCount}"  . PHP_EOL;
            echo "Осталось для обработки: " . ($articlesCount - $rowArticleCount)  . PHP_EOL;
            echo "Артикул: {$article}" . PHP_EOL;

            $articleObj = new Article($article);
            $type = $articleObj->car ? ArticleAnalog::TYPE_CAR : ArticleAnalog::TYPE_COMMON;
            $generator->execute($articleObj->id,$articleObj->carArticle,$articleObj->window,$type);
            echo "___________" . PHP_EOL;

        }
        echo  "Все аналоги перебраны!" . PHP_EOL;
    }



    public function actionGenerateAnalogs()
    {
        $connection = Yii::$app->db;
        $scriptSt = microtime(true);
        $deleteDate = time();

        $cars = Cars::find()->all();
        $allCount = count($cars);
        $globalCount = 0;
        foreach ($cars as $car){
            $startd = microtime(true);
            $globalCount++;
            $winArray = ['ПШ' => 'fw','ПФ' => 'fv','ПБ' => 'fd','ЗБ' => 'rd','ЗФ' => 'rv','ЗШ' => 'bw'];
            foreach (['ПШ','ПФ','ПБ','ЗБ','ЗФ','ЗШ'] as $value){
                $analogsIds = $this->_searchAnalogs($car->id,$value);
                if ( ($deleteKey = array_search($car->id,$analogsIds)) !== false ) {
                    unset($analogsIds[$deleteKey]);
                }
                $carsForm = new CarsForm($car->id);
                $windowExists = $carsForm->{$winArray[$value] . '1'}?: 0 ;
                $windowStatus =  $carsForm->{$winArray[$value]} ?:0;

                $analogs = Cars::find()->where(['in','id',$analogsIds])->all();
                if (count($analogs)) {
                    foreach ($analogs as $analog) {


                        $bool = (new Query)
                            ->select('carArticle')
                            ->from('tmp_analogs')
                            ->where('carArticle = :carArticle')
                            ->andWhere('window = :window')
                            ->andWhere('analogArticle = :analogArticle')
                            ->addParams([':carArticle' => $car->article])
                            ->addParams([':window' => $value])
                            ->addParams([':analogArticle' => $analog->article])
                            ->exists();

                        if ($bool) {
                            $connection->createCommand()->update('tmp_analogs', [
                                'carArticle' => $car->article,
                                'carName' => $car->name,
                                'window' => $value,
                                'windowStatus' => $windowStatus,
                                'windowExists' => $windowExists,
                                'analogArticle' => $analog->article,
                                'analogName' => $analog->name,
                                'last' => time(),
                            ], 'carArticle = :carArticle AND window = :window AND analogArticle = :analogArticle',
                                [':carArticle' => $car->article,':window' => $value,':analogArticle' => $analog->article])->execute();
                        } else {
                            $connection->createCommand()->insert('tmp_analogs', [
                                'carArticle' => $car->article,
                                'carName' => $car->name,
                                'window' => $value,
                                'windowStatus' => $windowStatus,
                                'windowExists' => $windowExists,
                                'analogArticle' => $analog->article,
                                'analogName' => $analog->name,
                                'last' => time(),
                            ])->execute();
                        }
                    }
                }else{
                    $bool = (new Query)
                        ->select('carArticle')
                        ->from('tmp_analogs')
                        ->where('carArticle = :carArticle')
                        ->andWhere('window = :window')
                        ->andWhere('analogArticle = :analogArticle')
                        ->addParams([':carArticle' => $car->article])
                        ->addParams([':window' => $value])
                        ->addParams([':analogArticle' => null])
                        ->exists();

                    if ($bool) {
                        $connection->createCommand()->update('tmp_analogs', [
                            'carArticle' => $car->article,
                            'carName' => $car->name,
                            'window' => $value,
                            'windowStatus' => $windowStatus,
                            'windowExists' => $windowExists,
                            'analogArticle' => null,
                            'analogName' => 'По данному оконному проему для данной машины аналогов НЕТ!!!',
                            'last' => time(),
                        ], 'carArticle = :carArticle AND window = :window AND analogArticle = :analogArticle',
                            [':carArticle' => $car->article,':window' => $value,':analogArticle' => null])->execute();
                    } else {
                        $connection->createCommand()->insert('tmp_analogs', [
                            'carArticle' => $car->article,
                            'carName' => $car->name,
                            'window' => $value,
                            'windowStatus' => $windowStatus,
                            'windowExists' => $windowExists,
                            'analogArticle' => null,
                            'analogName' => 'По данному оконному проему для данной машины аналогов НЕТ!!!',
                            'last' => time(),
                        ])->execute();
                    }
                }


            }

            $end = microtime(true);
            $rtime = $end - $startd;
            echo '#########################'. PHP_EOL .'all : ' . $allCount . PHP_EOL . 'left : ' . ($allCount-$globalCount) . PHP_EOL . 'position : ' . $globalCount . PHP_EOL . 'time : ' . $rtime . PHP_EOL;
        }

        $crows = $connection->createCommand()->delete('tmp_analogs', 'last < :date',
            [':date' => $deleteDate])->execute();

        $scriptEnd = microtime(true);
        $sctime = ($scriptEnd - $scriptSt) / (60*60) ;
        echo 'deleted : ' . $crows . PHP_EOL . 'duration : ' . $sctime . PHP_EOL;
    }


    private function _searchAnalogs ($car_id,$type,$ids = []) {
        $ids2 = [];
        $ids2[] = $car_id;
        $analogs = CarsAnalog::find()->where(['car_id' => $car_id])->all();
        foreach ($analogs as $analog) {
            $json=Json::decode($analog->json ? $analog->json : '{}');
            $types = isset($json['elements']) ? $json['elements'] : [];
            if (!in_array($analog->analog_id, $ids2) && in_array($type,$types)) {
                $ids2[] = $analog->analog_id;
            }
        }

        //Находим расхождения в массивах
        $diff = array_diff($ids2, $ids);

        // if ($car_id == 638) {
        //     var_dump('IDS-2');
        //     var_dump($ids2);
        //     var_dump('IDS');
        //     var_dump($ids);
        //     var_dump('DIFF');
        //     var_dump($diff);
        //     var_dump(empty($diff));
        //     die();
        // }

        if (count($diff)) {
            foreach ($diff as $key => $value) {
                $values = $this->_searchAnalogs($value,$type,array_merge($diff, $ids));
                if ($values) {
                    foreach ($values as $value) {
                        if (!in_array($value, $diff)) {
                            $diff[] = $value;
                        }
                    }
                }

            }
        }


        //возвращаем либо массив либо ложь
        return empty($diff) ? false : $diff;
    }



    public function actionGenerateLekalo()
    {
        $connection = Yii::$app->db;
        $scriptSt = microtime(true);
        $deleteDate = time();

        $cars = Cars::find()->all();
        $allCount = count($cars);
        $globalCount = 0;
        foreach ($cars as $car) {
            $startd = microtime(true);
            $globalCount++;
            $bool = (new Query)
                ->select('carArticle')
                ->from('tmp_lekalo')
                ->where('carArticle = :carArticle')
                ->andWhere('lekalo = :lekalo')
                ->addParams([':carArticle' => $car->article])
                ->addParams([':lekalo' => $car->lekalo])
                ->exists();

            if ($bool) {
                $connection->createCommand()->update('tmp_lekalo', [
                    'lekalo' => $car->lekalo,
                    'carArticle' => $car->article,
                    'carName'    => $car->name,
                    'FW' => $car->FW,
                    'FV' => $car->FV,
                    'FD' => $car->FD,
                    'RD' => $car->RD,
                    'RV' => $car->RV,
                    'BW' => $car->BW,
                    'last' => time(),
                ], 'article = :article AND window = :lekalo',
                    [':carArticle' => $car->article,':lekalo' => $car->lekalo])->execute();
            } else {
                $connection->createCommand()->insert('tmp_lekalo', [
                    'lekalo' => $car->lekalo,
                    'carArticle' => $car->article,
                    'carName'    => $car->name,
                    'FW' => $car->FW,
                    'FV' => $car->FV,
                    'FD' => $car->FD,
                    'RD' => $car->RD,
                    'RV' => $car->RV,
                    'BW' => $car->BW,
                    'last' => time(),
                ])->execute();
            }

            $end = microtime(true);
            $rtime = $end - $startd;
            echo '#########################'. PHP_EOL .'all : ' . $allCount . PHP_EOL . 'left : ' . ($allCount-$globalCount) . PHP_EOL . 'position : ' . $globalCount . PHP_EOL . 'time : ' . $rtime . PHP_EOL;
        }

        $crows = $connection->createCommand()->delete('tmp_analogs', 'last < :date',
            [':date' => $deleteDate])->execute();

        $scriptEnd = microtime(true);
        $sctime = ($scriptEnd - $scriptSt) / (60*60) ;
        echo 'deleted : ' . $crows . PHP_EOL . 'duration : ' . $sctime . PHP_EOL;
    }

    /**
     * Функция должна автоматически устанавливать 3 наряда в топ спид
     *
     * @throws \Exception
     */
    public function actionAutoSpeed()
    {
        $config = Config::findOne(2);
        $count = $config->json('autoSpeed');
        $count = $count ?: 3;

        $countAS = ErpNaryad::find()
            ->where(['and',
                ['or',
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['and',
                        ['status' => NULL],
                        ['is NOT','start',NULL],
                    ]
                ],
                ['is not','order_id', null],
                ['autoSpeed' => true],
            ])->count();

        $ordersObjects = ErpNaryad::find()
            ->select('order_id, created_at')
            ->where(['and',
                ['or',
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['and',
                        ['status' => NULL],
                        ['is NOT','start',NULL],
                    ]
                ],
                ['is not','order_id', null],
                ['actNumber' => null],
                ['sort' => [5,6]]
            ])
            ->groupBy('order_id')
            ->orderBy('created_at')
            ->all();

        $ordersArray = [];

        foreach ($ordersObjects as $ordersObject) {
            echo $ordersObject->order_id . ' --- ' . date('d-m-Y',$ordersObject->created_at) . PHP_EOL;
            $logOrder = Order::find()->where(['source_innumber' => $ordersObject->order_id])->one();
            if (!$logOrder || ($logOrder && $logOrder->remoteStorage == 2 && $logOrder->export_username == 'Склад')) continue;

            //Только физиков ускоряем (костыль 08.08.2022)
            if ($logOrder->category != 'Физик' && $logOrder->export_type != 'Физик') continue;

            $workOrders = ErpNaryad::find()
                ->where(['and',
                    ['or',
                        ['status' => ErpNaryad::STATUS_IN_WORK],
                        ['status' => ErpNaryad::STATUS_IN_PAUSE],
                        ['and',
                            ['status' => NULL],
                            ['is NOT','start',NULL],
                        ]
                    ],
                    ['order_id' => $ordersObject->order_id],
                    ['actNumber' => null],
                    ['sort' => [5,6]]
                ])->all();

            if (!empty($workOrders))
                $ordersArray[] = $workOrders;
        }

        foreach ($ordersArray as $workOrdersArray) {
            foreach ($workOrdersArray as $workOrder) {
                echo $workOrder->order_id . ' --- ' . date('d-m-Y',$workOrder->created_at) . PHP_EOL;
                $sort = 4;
                //Органайзеры автоматически не ускоряются
                if (preg_match('/OT-1189/', $workOrder->article) === 1) continue;

                $time = DatePeriodManager::getDiffOfDates($workOrder->created_at,time());
                $q = ($time + 60*60*11)/($count*60*60*24);
                echo $q . PHP_EOL;
                if ($q >= 1 && ( $workOrder->sort == 6 || ( $workOrder->sort == 5 && $workOrder->oldSort != 7 ) ) ){
                    $workOrder->sort = $sort;
                    $workOrder->autoSpeed = true;
                    $workOrder->save();
                    $countAS++;
                }
            }
            if ($countAS >= 200) break;
        }

        Yii::$app->mailer->setTransport(Yii::$app->params['mailer.transport']['supportEmail']);

        Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo('web@laitovo.ru')
            ->setSubject( Yii::t('app', 'Регламентные задания Biz-zone') )
            ->setTextBody('Успешно выполнена команда Автоматического ускорения нарядов биззона в консоли ' . date("Y-m-d H:i:s"))
            ->send();
    }

    /**
     * @throws InvalidConfigException
     */
    public function actionSendTestMail()
    {
        Yii::$app->mailer->setTransport(Yii::$app->params['mailer.transport']['supportEmail']);

        Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo('web@laitovo.ru')
            ->setSubject( Yii::t('app', 'Регламентные задания Biz-zone') )
            ->setTextBody('Успешно выполнена команда отправки тестового письма через консоль через консоль')
            ->send();
    }

    //Времменная штука чтобы все upn сгруппировать по штукам
    public function actionGroupAllUpnsBySetIndex()
    {
        $orders = Order::find()->where(['>','created_at',(time()-60*60*24*30)])->all();
        foreach ($orders as $order){
            echo 'Группирую заказ' . $order->source_innumber . PHP_EOL;
            $equipment = Equipment::groupOrderBySetIndex($order);
            foreach ($equipment as $eq) {
                UpnGroup::groupOrderEntries($eq);
            }
        }

        echo 'Все Upn Сгруппированы.' . PHP_EOL;
    }

    /**
     * @param $order
     * @throws \yii\db\Exception
     */
    public function actionGroupAllUpnsBySettext($order)
    {
        $order = Order::find()->where(['source_innumber' => $order])->one();
        if (!$order) {
            echo 'Заказ не найден.' . PHP_EOL;
        }
        echo 'Группирую заказ' . $order->source_innumber . PHP_EOL;
        $equipment = Equipment::groupOrderByBundle($order);
        foreach ($equipment as $eq) {
            UpnGroup::groupOrderEntries($eq);
        }

        echo 'Все Upn Сгруппированы.' . PHP_EOL;
    }


    /**
     * @param $order
     * @throws \yii\db\Exception
     */
    public function actionGroupAllUpnsBySetIndexOne($order)
    {
        $order = Order::find()->where(['source_innumber' => $order])->one();
        if (!$order) {
            echo 'Заказ не найден.' . PHP_EOL;
        }
        echo 'Группирую заказ' . $order->source_innumber . PHP_EOL;
        $equipment = Equipment::groupOrderBySetIndex($order);
        foreach ($equipment as $eq) {
            UpnGroup::groupOrderEntries($eq);
        }

        echo 'Все Upn Сгруппированы.' . PHP_EOL;
    }

    /**
     * @param $order
     * @return bool
     * @throws \yii\db\Exception
     */
    public function actionSetIndexByGroup($order)
    {
        /**
         * Находим заказ по его номеру из Laitovo.ru
         */
        $order = Order::find()->where(['source_innumber' => $order])->one();
        if (!$order) {
            echo 'Заказ не найден.' . PHP_EOL;
        }
        echo 'Группирую заказ' . $order->source_innumber . PHP_EOL;


        $elements = $order->notDeletedOrderEntries;
        $setIndex = 1;
        $elementIds = [];

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($elements as $element) {
                if (in_array($element->id,$elementIds)) continue;
                if (!$element->upn) {
                    $transaction->rollBack();
                    throw new \DomainException("Отсутвует UPN у позиции {$element->id}");
                }
                if (($groupIndex = $element->upn->group)) {
                    $upns = Naryad::find()->where(['group' => $groupIndex])->all();
                    if (!$upns) {
                        $transaction->rollBack();
                        throw new \DomainException("Не могу найти UPN`s для группы №  {$groupIndex}" . PHP_EOL);
                    }
                    foreach ($upns as $upn) {
                        if (!($entry = $upn->orderEntry)) {
                            $transaction->rollBack();
                            throw new \DomainException("Не могу найти для какой позиции был зарезервирован UPN № {$upn->id}" . PHP_EOL);
                        }
                        $entry->setIndex = $setIndex;
                        $elementIds[] = $entry->id;
                        echo "В список отработанных элементов добавлен элемент № {$entry->id}" . PHP_EOL;

                        if (!($entry->save())) {
                            $transaction->rollBack();
                            throw new \DomainException("Не удалось сохранить позицию № {$entry->id}" . PHP_EOL);
                        }
                        echo "Элементу №  {$entry->id} был присвоен индекс {$setIndex}" . PHP_EOL;
                    }
                    $setIndex++;
                }else{
                    $elementIds[] = $element->id;
                    $element->setIndex = NULL;
                    echo "В список отработанных элементов добавлен элемент № {$element->id}" . PHP_EOL;
                    if (!($element->save())) {
                        $transaction->rollBack();
                        throw new \DomainException("Не удалось сохранить позицию № {$element->id}" . PHP_EOL);
                    }
                    echo "Элементу №  {$element->id} был присвоен индекс NULL" . PHP_EOL;
                }
            }
            $transaction->commit();
        }catch (\Exception $exception) {
            $transaction->rollBack();
            echo $exception->getMessage();
        }

        if (count($elements) == count($elementIds))
            echo 'Всем элементам заказа были проиндексированы' . PHP_EOL;
        else
            echo 'Ошибка!!! Не все элементы заказа подверглись индексации' . PHP_EOL;

        echo PHP_EOL;
    }


    /**
     * @param $name
     * @throws \yii\db\Exception
     */
    public function actionSetIndexByGroupOnName($name)
    {
        /**
         * Находим заказ по его номеру из Laitovo.ru
         */
        $orders = Order::find()->where(['export_username' => $name])->all();
        echo count($orders);
        if (!$orders) {
            echo 'Заказы не найдены.' . PHP_EOL;
        }

        foreach ($orders as $order) {
            echo 'Группирую заказ' . $order->source_innumber . PHP_EOL;


            $elements = $order->notDeletedOrderEntries;
            $setIndex = 1;
            $elementIds = [];

            $transaction = \Yii::$app->db->beginTransaction();
            try {
                foreach ($elements as $element) {
                    if (in_array($element->id,$elementIds)) continue;
                    if (!$element->upn) {
                        $transaction->rollBack();
                        throw new \DomainException("Отсутвует UPN у позиции {$element->id}");
                    }
                    if (($groupIndex = $element->upn->group)) {
                        $upns = Naryad::find()->where(['group' => $groupIndex])->all();
                        if (!$upns) {
                            $transaction->rollBack();
                            throw new \DomainException("Не могу найти UPN`s для группы №  {$groupIndex}" . PHP_EOL);
                        }
                        foreach ($upns as $upn) {
                            if (!($entry = $upn->orderEntry)) {
                                $transaction->rollBack();
                                throw new \DomainException("Не могу найти для какой позиции был зарезервирован UPN № {$upn->id}" . PHP_EOL);
                            }
                            $entry->setIndex = $setIndex;
                            $elementIds[] = $entry->id;
                            echo "В список отработанных элементов добавлен элемент № {$entry->id}" . PHP_EOL;

                            if (!($entry->save())) {
                                $transaction->rollBack();
                                throw new \DomainException("Не удалось сохранить позицию № {$entry->id}" . PHP_EOL);
                            }
                            echo "Элементу №  {$entry->id} был присвоен индекс {$setIndex}" . PHP_EOL;
                        }
                        $setIndex++;
                    }else{
                        $elementIds[] = $element->id;
                        $element->setIndex = NULL;
                        echo "В список отработанных элементов добавлен элемент № {$element->id}" . PHP_EOL;
                        if (!($element->save())) {
                            $transaction->rollBack();
                            throw new \DomainException("Не удалось сохранить позицию № {$element->id}" . PHP_EOL);
                        }
                        echo "Элементу №  {$element->id} был присвоен индекс NULL" . PHP_EOL;
                    }
                }
                $transaction->commit();
            }catch (\Exception $exception) {
                $transaction->rollBack();
                echo $exception->getMessage();
            }

            if (count($elements) == count($elementIds))
                echo 'Всем элементам заказа были проиндексированы' . PHP_EOL;
            else
                echo 'Ошибка!!! Не все элементы заказа подверглись индексации' . PHP_EOL;

//            var_dump($elementIds);
            echo PHP_EOL;
        }

    }

    /**
     * @param $order
     * @throws \yii\db\Exception
     */
    public function actionGroupAllChex()
    {
        $orders = Order::find()->where(['export_username' => 'Zdenek Kneisl'])->all();
        if (!$orders) {
            echo 'Заказ не найдены.' . PHP_EOL;
        }
        foreach ($orders as $order) {
            echo 'Группирую заказ' . $order->source_innumber . PHP_EOL;
            $equipment = Equipment::groupOrderByBundle($order);
            foreach ($equipment as $eq) {
                UpnGroup::groupOrderEntries($eq);
            }
        }

        echo 'Все Upn Сгруппированы.' . PHP_EOL;
    }

    /**
     * Экшн для заполения все контрагентных номеров
     * @return array|void
     */
    public function actionFillOrderIpn()
    {
        $orderExArr = Order::find()->where('payer_id is null OR sender_id is null OR receiver_id is NULL')->limit(150)->all();

        if (empty($orderExArr)) {
            echo 'Не найдено не одной заявки для поиска контрагентов' . PHP_EOL;
            return;
        }

        foreach ($orderExArr as $orderEx) {
            if (!($num = $orderEx->source_innumber)) {
                echo 'Не получается найти номер заявки, с которой работать' . PHP_EOL;
                return;
            }


            $client = new Client();
            $response = $client->createRequest()
                ->addHeaders(['Accept' => 'text/html,*/*'])
                ->setMethod('get')
                ->setUrl('http://laitovo.ru/api/paymentId/senderIpn')
                ->setData(['order' => $num,'token' => '$2a$13$XdSk4SVv5Y5lswtJEgYhueMkROPZIPN9B1iFc.MpU6mrHnA8q68PG'])
                ->send();

            if (!empty($response->content)) {
                $data = Json::decode($response->content);
                if (!empty($data['ipn'])) {
                    $orderEx->sender_id = $data['ipn'];
                }
            }

            $client = new Client();
            $response = $client->createRequest()
                ->addHeaders(['Accept' => 'text/html,*/*'])
                ->setMethod('get')
                ->setUrl('http://laitovo.ru/api/paymentId/payerIpn')
                ->setData(['order' => $num,'token' => '$2a$13$XdSk4SVv5Y5lswtJEgYhueMkROPZIPN9B1iFc.MpU6mrHnA8q68PG'])
                ->send();

            if (!empty($response->content)) {
                $data = Json::decode($response->content);
                if (!empty($data['ipn'])) {
                    $orderEx->payer_id = $data['ipn'];
                    $orderEx->receiver_id = $data['ipn'];
                }
            }

            if ($orderEx->save() && $orderEx->payer_id && $orderEx->receiver_id && $orderEx->sender_id) {
                echo 'Заказ' . $num . 'успешно получил необходимые данные : ' . PHP_EOL;
                echo 'Плательщик: '  . $orderEx->payer_id . PHP_EOL;
                echo 'Получатель: '  . $orderEx->receiver_id . PHP_EOL;
                echo 'Отправитель: ' . $orderEx->sender_id . PHP_EOL;
            }else{
                echo 'Заказ' . $num . 'не получил необходимых данных' . PHP_EOL;
            };
        }
    }

    public function actionGenerateArticleStatistics()
    {
        $scriptSt = microtime(true);

        $generator = new ArticleStatisticsGenerator();
        $generator->execute();

        $scriptEnd = microtime(true);
        $sctime = ($scriptEnd - $scriptSt) / (60*60) ;
        echo 'duration : ' . $sctime . PHP_EOL;
    }

    public function actionFixSalary()
    {
        /**
         * Идентификаторы строк зарплаты, которые являются правдой
         */
        $trueRows = [];

        /**
         * Идентификаторы строк зарплаты, которые являются ложью
         */
        $falseRows = [];

        /**
         * Номера заказов, для которых нужно пофиксить зарплату
         */
        $orders = [15016989,15016990];

        /**
         * Получили все идентификаторы нарядов
         */
        $workOrdersIds = ErpNaryad::find()->select('id')->where(['in','order_id',$orders])->column();

        $salaryRows = ErpUsersReport::find()->where(['in','naryad_id',$workOrdersIds])
            ->orderBy('naryad_id, location_id, job_type_id, job_count, job_rate, created_at DESC')->all();

        foreach ($salaryRows as $row) {
            $key = $row->naryad_id . '|'
                . $row->location_id . '|'
                . $row->job_type_id . '|'
                . $row->job_count . '|'
                . $row->job_rate;


            if (!isset($trueRows[$key])) {
                $trueRows[$key] = $row;
            }else {
                $falseRows[] = $row;
            }


            echo $row->naryad_id . ' | '
            . $row->location_id . ' | '
            . $row->job_type_id . ' | '
            . $row->job_count . ' | '
            . $row->job_rate . ' | '
            . $row->created_at . ' | '
            . PHP_EOL;
        }

        echo count($salaryRows) . PHP_EOL;
        echo count($trueRows) . PHP_EOL;
        echo count($falseRows) . PHP_EOL;
        echo count($falseRows) + count($trueRows) . PHP_EOL;


        $userSalary = [];
        $sumFalse = 0;
        foreach ($falseRows as $row) {
            $sumFalse += $row->job_price;
            if (isset($userSalary[$row->user_id])) {
                $userSalary[$row->user_id] += $row->job_price;
            } else {
                $userSalary[$row->user_id] = $row->job_price;
            }
        }

        echo "Ложно :: На сумму $sumFalse рублей"  . PHP_EOL;

        $sumTrue = 0;
        foreach ($trueRows as $row) {
            $sumTrue += $row->job_price;
        }

        echo "Истино :: На сумму $sumTrue рублей"  . PHP_EOL;

        $sumAll = 0;
        foreach ($salaryRows as $row) {
            $sumAll += $row->job_price;
        }

        echo "Всего :: На сумму $sumAll рублей"  . PHP_EOL;

        echo "Сумма :: На сумму " .  ($sumFalse + $sumTrue) . " рублей"  . PHP_EOL;

        foreach ($userSalary as $user_id => $row) {
            echo ErpUser::findOne($user_id)->name . " : ". $row . " рублей" . PHP_EOL ;
        }

        foreach ($falseRows as $row) {
            $row->delete();
        }
    }

    /**
     * Количество крючков за месяц
     */
    public function actionHookCount()
    {
        $count = 0;
        $workOrders = ErpNaryad::find()
            ->where(['>','created_at', time()-60*60*24*43])
            ->andWhere(['<','created_at', time()-60*60*24*12])
            ->andWhere(['status' => 'Готов'])->all();
        foreach ($workOrders as $workOrder) {
            $article = new Article($workOrder->article);
            if ($article->needHook) {
                $count++;
            }
        }
        echo "Количество крючков: $count" . PHP_EOL;
    }

    /**
     * Расчет себестоимости склада
     */
    public function actionStorageSum()
    {
        $subQuery = \backend\modules\logistics\models\Naryad::find()->where(['reserved_id' => null])->column();

        $states = StorageState::find()->with('upn')->where(['in','upn_id',$subQuery])->all();

        $sum = 0;
        $price = 0;
        $count = 0;
        $count2 = 0;
        foreach ($states as $state) {
            $article = new Article($state->upn->article);
            if (in_array($article->windowEn,['FD','RD']) && $article->brand == 'Chiko') {
                $price = 350;
            } elseif ((in_array($article->windowEn,['FD','RD']) && $article->brand != 'Chiko')) {
                $price = 500;
            } elseif (in_array($article->windowEn,['RV','FV']) ) {
                $price = 100;
            } elseif (in_array($article->windowEn,['BW'])) {
                $price = 230;
            }
            $sum += $price;
            if (in_array($article->windowEn,['FD','RD','RV','FV','BW','FW']))
                $count++;
            if ($article->windowEn == null)
                $count2++;


            echo $price . PHP_EOL;
        }
        echo $sum . PHP_EOL;
        echo $count . PHP_EOL;
        echo $count2 . PHP_EOL;
        echo "Количество UPN" . count($subQuery) . PHP_EOL;
        echo "Количество Остатков на складе" . count($states) . PHP_EOL;
    }

    public function actionGenerateSearchString()
    {
        foreach (Shipment::find()->select('id')->orderBy('id desc')->limit(500)->column() as $shipmentId) :
            $shipment = Shipment::findOne($shipmentId);
            $searchString = SShipmentSearch::generateSearchString($shipmentId);
            $shipment->searchString = $searchString;
            if ($shipment->save())
                echo "Отгрузка $shipmentId сохранена" . PHP_EOL;
            else
                echo "Отгрузку $shipmentId не удалось сохранить" . PHP_EOL;
        endforeach;
    }

    public function actionCountCarsGr()
    {
        $count = 0;
        $carArticles = [];
        $carTitles = [];
        $groupArticles = [];
        $cars = (new Query())
            ->from('tmp_analogs')
            ->where(['like','carName','%Lada%',false])
            ->andWhere(['window' => 'ПБ'])
            ->all();

        foreach ($cars as $carRow) {
            if (!$carRow['analogArticle']) {
                $count++;
                $carArticles[] = $carRow['carArticle'];
                $carTitles[$count] = $carRow['carName'];
                $groupArticles[$carRow['carArticle']][] = $carRow['carName'];
                continue;
            } else {
                if (!in_array($carRow['carArticle'], $carArticles)) {
                    $count++;
                    $carArticles[] = $carRow['carArticle'];
                    $carTitles[$count] = $carRow['carName'];
                    $groupArticles[$carRow['carArticle']][] = $carRow['carName'];
                }
                if (!in_array($carRow['analogArticle'], $carArticles)) {
                    $carArticles[] = $carRow['analogArticle'];
                    $groupArticles[$carRow['carArticle']][] = $carRow['analogName'];
                }
            }
        }

        echo "Количество групп аналогов : $count" . PHP_EOL;
        print_r($carTitles);

        $countGroups = 1;
        foreach ($groupArticles as $article => $list) {
            echo "Группы аналогов #$countGroups :" . PHP_EOL;
            echo implode(' ' . PHP_EOL, $list);
            echo ' ' . PHP_EOL;
            $countGroups++;
        }

    }

    /**
     * Генерация отчета по неиспользованным лекалам
     */
    public function actionTemplateUsing()
    {
        UsingTemplateReport::generateData();
        echo 'Данные для отчета сгенерированы';
    }

    /**
     * Генерация отчета по неиспользованным лекалам
     */
    public function actionReportRelevanceSump()
    {
        RelevanceReport::generateData(3);
        echo 'Данные для отчета сгенерированы';
    }

    /**
     * Генерация разового отчета для дениса
     */
    public function actionReportRelevanceOneTime()
    {
        RelevanceReport::generateDataNew(1);
        echo 'Данные для отчета сгенерированы';
    }

    /**
     * Генерация отчета по неиспользованным лекалам
     */
    public function actionReportRelevanceMain()
    {
        RelevanceReport::generateData(1);
        echo 'Данные для отчета сгенерированы';
    }

    public function actionRefillInvoices()
    {
        $invoices =  EuropeInvoice::find()->all();
        foreach ($invoices as $invoice) {
            SEntryFill::fill($invoice);
            echo "Инвойс {$invoice->id} успешно обновлен" . PHP_EOL;
        }
    }

    /**
     * Проверка ошибок в системе
     *
     * @return void
     * @throws InvalidConfigException
     */
    public function actionCheckErrors()
    {
        if (($inHandleOrders = ErpOrder::find()->where(['status' => ErpOrder::STATUS_IN_HANDLE])->andWhere(['<','updated_at', time()-(60*10)])->all())) {
            $numbers = ArrayHelper::map($inHandleOrders,'id','id');
            Yii::$app->mailer->setTransport(Yii::$app->params['mailer.transport']['supportEmail']);

            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo(['heksweb@gmail.com', 'web@laitovo.su'])
                ->setSubject( Yii::t('app', 'Регламентные задания Biz-zone') )
                ->setTextBody('Ошибка в системе! Есть наряды, которые долго висят в обработке. Их номера: ' . implode(', ',$numbers) . ' Дата: ' . date("Y-m-d H:i:s"))
                ->send();
        }
    }

    /**
     * Проверка отправки писем.
     *
     * @return void
     * @throws InvalidConfigException
     */
    public function actionTestMail()
    {
        Yii::$app->mailer->setTransport(Yii::$app->params['mailer.transport']['supportEmail']);

        try {
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo(['heksweb@gmail.com', 'web@laitovo.su'])
                ->setSubject( Yii::t('app', 'Регламентные задания Biz-zone') )
                ->setTextBody('Тестовое письмо для проверки почты. Дата: ' . date("Y-m-d H:i:s"))
                ->send();

            echo 'Success' . PHP_EOL;
        }catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

}
