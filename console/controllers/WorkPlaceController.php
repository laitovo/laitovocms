<?php

namespace console\controllers;

use yii\console\Controller;
use backend\modules\laitovo\models\ErpLocationWorkplaceRegister;
use Yii;

class WorkPlaceController extends Controller
{
    public function actionIndex()
    {
        //$ids = [
        //    'id_evdokimov' => 86,
        //    'id_kulagin' => 116
        //];

        $id = $this->prompt('Enter user id (86 - Evdokimov, 116 - Kulagin) ' . PHP_EOL, ['required' => true]);

        if ($id != 86 && $id != 116) {
            exit('Unknown id' . PHP_EOL);
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $year_value = date('Y', time());
            $text = 'Year ' . $year_value . '?';

            $year = null;
            $year = $this->checkYesNo($year, $text, $year_value);

            $month_value = date('m', time());
            $text = 'Month ' . $month_value . '?';

            $month = null;
            $month = $this->checkYesNo($month, $text, $month_value);

            if (!$year || !$month) {
                exit(2);
            }

            $numbers_str = $this->prompt('Enter days separated by space, example: 1 2 3 ...' . PHP_EOL, ['required' => true]);

            $numbers = explode(' ', $numbers_str);
            foreach ($numbers as $key => $item) {
                $numbers[$key] = trim($item);
            }

            $result = [];

            $i = 0;
            foreach ($numbers as $day) {
                $time = $year . '-' . $month . '-' . $day . ' 08:' . rand(15, 40);
                $result[$i]['in'] = [
                    'action' => 'Приход',
                    'time' => strtotime($time),
                    'time_human' => $time
                ];
                $time = $year . '-' . $month . '-' . $day . ' 17:' . rand(15, 40);
                $result[$i]['out'] = [
                    'action' => 'Уход',
                    'time' => strtotime($time),
                    'time_human' => $time
                ];
                $i++;
            }

            echo 'Result:' . PHP_EOL;
            print_r($result);
            echo PHP_EOL;

            $a = $this->prompt('Continue? (y/n) ', ['required' => true]);
            if ($a != 'y') {
                exit(3);
            }
            foreach ($result as $item) {
                $this->createRecord($id, $item['in']);
                $this->createRecord($id, $item['out']);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \DomainException($e->getMessage());
        }
        echo 'Data saved successfully' . PHP_EOL;
    }

    private function createRecord($user_id, array $item)
    {
        $model = new ErpLocationWorkplaceRegister();
        $model->user_id = $user_id;
        $model->register_at = $item['time'];
        $model->action_type = $item['action'];
        $model->save();
    }

    private function getData()
    {
        $var = $this->prompt('Enter your value', ['required' => true]);
        if (!$var) {
            exit(1);
        }
        return $var;
    }

    private function checkYesNo($var, $text, $value)
    {
        $answer = $this->prompt($text . '(y/n)', ['required' => true]);
        if ($answer != 'y' && $answer != 'n') {
            exit('Answer must be y or n');
        }
        if ($answer == 'y') {
            $var = $value;
        } else {
            $var = $this->getData();
        }
        return $var;
    }
}