<?php
namespace console\controllers;

use Yii;
use yii\httpclient\Client;
use obmen\models\ObmenLaitovo;
use obmen\models\ObmenLaitovoSite;

class ObmenLaitovoController extends \yii\console\Controller
{
    /**
     * Загрузка автомобилей из 1С Лайтово
     * @return type
     */
    public function actionLoadCars($download=true)
    {
        ini_set("max_execution_time", "0");
        ini_set("memory_limit","-1");

        if ($download){
            $client = new \SoapClient("http://46.42.17.89/laitovo/ws/WebSync1?wsdl");
            $soap =  $client->GetCar(array(
                "Hash"=>Yii::$app->params['laitovo_hash'],
            ));

            $return=mb_substr($soap->return,strpos($soap->return,'<?xml'),strlen($soap->return)-strpos($soap->return,'<?xml')-2);
            $return=str_replace('""', '"', $return);
            file_put_contents(dirname(Yii::$app->basePath).'/obmen/files/avto-laitovo-from1c.xml', $return);
        }

        $client = new Client();
        $response = $client->createRequest()
            ->addHeaders(['Accept' => 'application/xml'])
            ->setMethod('get')
            ->setUrl(Yii::$app->params['apiurl'].'/load')
            ->setData([
                'access-token' => Yii::$app->params['access_token'],
                'file' => 'avto-laitovo-from1c.xml',
            ])
            ->send();
        if ($response->isOk) {
            $response->content=str_replace('V8Exch:', 'V8Exch', $response->content);
            $obmen=new ObmenLaitovo($response->data);

            try {
                $obmen->importcars();
            } catch (\Exception $e) {
                Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo('web@laitovo.ru')
                ->setSubject( Yii::t('app', 'Проблема синхронизации | Лайтово') )
                ->setTextBody((string)$e)
                ->send();
            }
        }
    }

    /**
     * Загрузка заказов с сайта
     * @return type
     */
    public function actionLoadOrders()
    {
        $obmen=new ObmenLaitovoSite;
        $obmen->loadOrders();
    }


    /**
     * Отправка одного автомобиля на сайт
     * @param $article
     */
    public function actionPostOneCar($article)
    {
        $obmen=new ObmenLaitovoSite;
        $obmen->postOneCar($article);
    }

    /**
     * Загрузка заказов с сайта в модуль логистики
     * @return type
     */
    public function actionLoadOrdersForLogist()
    {
        $obmen=new ObmenLaitovoSite;
        $obmen->loadOrdersForLogist();
    }

    /**
     * Отправка автомобилей на сайт
     * @return type
     */
    public function actionPostCars()
    {
        $obmen=new ObmenLaitovoSite;
        $obmen->postCars();
    }

    /**
     * Вытаскиваем с сайта отчет по русским клиентам
     */
    public function actionLoadOrdersClientReportRu()
    {
        $obmen = new ObmenLaitovoSite();
        $obmen->loadOrdersClientRu();
    }

    /**
     * Вытаскиваем с сайта отчет по европейским клиентам
     */
    public function actionLoadOrdersClientReportDe()
    {
        $obmen = new ObmenLaitovoSite();
        $obmen->loadOrdersClientDe();
    }

    /**
     * Вытаскиваем с сайта статистику
     */
    public function actionLoadLogistStatist()
    {
        $obmen = new ObmenLaitovoSite();
        $obmen->loadLogistStatist();
    }

    public function actionUpdateLeftovers()
    {
        $obmen = new ObmenLaitovoSite();
        $obmen->updateLeftovers();
    }

    /**
     * Обновление бонусной составляющей
     *
     * @throws \yii\db\Exception
     */
    public function actionUpdateBonusOrders()
    {
        $obmen = new ObmenLaitovoSite();
        $obmen->updateBonusOrders();
    }
}
