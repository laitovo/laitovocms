<?php


namespace console\controllers;

use common\models\laitovo\ErpNaryad;
use common\models\laitovo\ErpOrder;
use Yii;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class TmpController extends Controller
{
    public function actionT12($number)
    {
        $check = (new Query())->from('logistics_shipment')->where(['id' => $number])->one();
        if (!$check) {
            exit('Shipment with id ' . $number . ' not found!' . PHP_EOL);
        }
        if ($check['officialNumber']) {
            exit('Number already exist - ' . $check['officialNumber'] . PHP_EOL);
        }
        $year = date('Y', time());
        $max = (new Query())
            ->from('logistics_shipment')
            ->where(['officialNumberYear' => $year])->max('officialNumber');
        $max++;
        \Yii::$app->db->createCommand()->update('logistics_shipment',
            ['officialNumber' => $max, 'officialNumberYear' => $year],
            ['id' => $number])->execute();
        echo $max . PHP_EOL;
    }

    public function actionIndex()
    {
        $table = 'laitovo_erp_naryad';
        $orders = $this->getOrders();

        $arr = (new Query())->select(['id', 'article'])->from($table)->where([
            'in',
            'order_id',
            $orders
        ])->all();
        foreach ($arr as $item) {
            $article = explode('-', $item['article']);
            if ($article[4] == 2) {
                $article[4] = 4;
                $article = implode('-', $article);
                //echo $article;
                echo Yii::$app->db->createCommand()->update($table, ['article' => $article],
                    ['id' => $item['id']])->execute();
                echo PHP_EOL;
            }
        }
    }

    public function actionAdd()
    {
        $table_naryad = 'logistics_naryad';
        $table_order = 'logistics_order';
        $table_order_entry = 'logistics_order_entry';
        $table_box = 'logistics_box';

        $ship_numbers = $this->getShip();

        $boxes = (new Query())
            ->select('lo.source_innumber, lb.remoteStorageBarcode')
            ->from($table_box . ' lb')
            ->leftJoin($table_order_entry . ' loe', 'lb.id = loe.boxId')
            ->leftJoin($table_order . ' lo', 'loe.order_id = lo.id')
            ->where(['in', 'loe.shipment_id', $ship_numbers])
            ->indexBy('source_innumber')->all();

        $orders = ArrayHelper::getColumn($boxes, 'source_innumber');

        $arr = (new Query())
            ->select("`ln`.`article`, `ln`.`barcode`, `ln`.`id` AS upn, `ln`.`pid`, `lo`.`source_innumber` AS order_number")
            ->from($table_naryad . ' ln')
            ->leftJoin($table_order_entry . ' loe', '`ln`.`reserved_id` = `loe`.`id`')
            ->leftJoin($table_order . ' lo', '`loe`.`order_id` = `lo`.`id`')
            ->where(['in', 'lo.source_innumber', $orders])->all();

        if (!$arr) {
            exit('Empty $arr');
        }

        $pids = [];

        foreach ($arr as $item) {
            if ($item['pid'] && !isset($pids[$item['pid']])) {
                $pids[$item['pid']] = $item['pid'];
                $pid_barcode = (new Query)->select('barcode')->from($table_naryad)->where(['id' => $item['pid']])->scalar();
                $data[] = [
                    'article' => null,
                    'order_number' => $item['order_number'],
                    'upn' => $item['pid'],
                    'upn_barcode' => $pid_barcode,
                    'parent_upn' => null,
                    'literal_barcode' => $boxes[$item['order_number']]['remoteStorageBarcode'],
                ];
            }
            $data[] = [
                'article' => $item['article'],
                'order_number' => $item['order_number'],
                'upn' => $item['upn'],
                'upn_barcode' => $item['barcode'],
                'parent_upn' => $item['pid'],
                'literal_barcode' => $boxes[$item['order_number']]['remoteStorageBarcode'],
            ];
        }

        print_r(json_encode($data));
        echo PHP_EOL;
    }

    private function getOrders()
    {
        $orders = $this->prompt('Номера заказов через запятую', ['required' => true]);
        $orders = explode(',', $orders);
        foreach ($orders as $key => $item) {
            $orders[$key] = trim($item);
        }
        return $orders;
    }

    private function getShip()
    {
        $orders = $this->prompt('Номера отгрузок через запятую', ['required' => true]);
        $orders = explode(',', $orders);
        foreach ($orders as $key => $item) {
            $orders[$key] = trim($item);
        }
        return $orders;
    }

    public function actionGetIpn()
    {
        $data = (new Query())->select('id, client_id')->from('return_journal')->all();
        \Yii::$app->setComponents(
            [
                'db' => [
                    'class' => 'yii\db\Connection',
                    'dsn' => 'mysql:host=95.213.191.91;dbname=laitovoweb',
                    'username' => 'laitovoweb',
                    'password' => '72nuWt7UUNfNqMEC',
                    'charset' => 'utf8',
                ]
            ]
        );

        foreach ($data as $key => $item) {
            $data[$key]['contractorId'] = (new Query())
                ->select('ipn')
                ->from('ru_user')
                ->where(['id' => $item['client_id']])->scalar();
        }

        $file = __DIR__ . '/ipns';

        $str = var_export($data, true);

        file_put_contents($file, $str);

    }

    public function actionSetIpn()
    {
        $file = __DIR__ . '/ipns.php';
        $data = require $file;
        foreach ($data as $item) {
            echo Yii::$app->db->createCommand()
                ->update('return_journal',
                    ['contractorId' => $item['contractorId']], ['id' => $item['id']])
                ->execute();
            echo PHP_EOL;
        }
        unlink($file);
    }

    public function actionTmp()
    {
        $data = (new Query())
            ->from('tmp_analogs')
            ->where(['windowExists' => 1, 'windowStatus' => 1])
            ->andWhere(['is not', 'analogArticle', null])
            ->all();

        $csv_arr[] = [
            'Артикул а/м', 'Имя а/м', 'Окно', 'Артикул а/м (аналог)', 'Имя а/м (аналог)'
        ];

        $data = ArrayHelper::index($data, null, 'carArticle');

        foreach ($data as $key => $items) {
            foreach ($items as $item) {
                $csv_arr[] = [
                    $item['carArticle'], $item['carName'], $item['window'], $item['analogArticle'], $item['analogName']
                ];
            }
        }

        $fp = fopen(dirname(__DIR__) . '/analogs.csv', 'w');
        foreach ($csv_arr as $fields) {
            fputcsv($fp, $fields, ';');
        }
        fclose($fp);
    }

    /**
     * Функция для обработки заказов, которые отсутсвуют на производстве
     *
     * @return void
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCloseOldOrders()
    {
        $ordersForCheck = ErpOrder::find()->select(['id','created_at'])->where(['status' => ErpOrder::STATUS_IN_WORK])->orderBy(['created_at' => SORT_ASC])->asArray()->all();
        foreach ($ordersForCheck as $order) {
            if (!(ErpNaryad::find()->where(['order_id' => $order['id']])->exists())) {
                if (Yii::$app->db->createCommand()->update(ErpOrder::tableName(), ['status' => ErpOrder::STATUS_READY], ['id' => $order['id']])->execute())
                    echo "Заказ № {$order['id']} от " . Yii::$app->formatter->asDateTime($order['created_at']) . " перевели в статус готов" . PHP_EOL;
            } elseif (!(ErpNaryad::find()->where(['order_id' => $order['id']])->andWhere(['or',['status' => null],['!=','status',ErpNaryad::STATUS_FINISH]])->exists())) {
                if (Yii::$app->db->createCommand()->update(ErpOrder::tableName(), ['status' => ErpOrder::STATUS_READY], ['id' => $order['id']])->execute())
                    echo "Заказ № {$order['id']} от " . Yii::$app->formatter->asDateTime($order['created_at']) . " перевели в статус готов" . PHP_EOL;
            } elseif (!(ErpNaryad::find()->where(['order_id' => $order['id']])->andWhere(['or',['status' => null],['!=','status',ErpNaryad::STATUS_READY]])->exists())) {
                if (Yii::$app->db->createCommand()->update(ErpOrder::tableName(), ['status' => ErpOrder::STATUS_READY], ['id' => $order['id']])->execute())
                    echo "Заказ № {$order['id']} от " . Yii::$app->formatter->asDateTime($order['created_at']) . " перевели в статус готов" . PHP_EOL;
            } else {
                echo "Заказ № {$order['id']} от " . Yii::$app->formatter->asDateTime($order['created_at']) . " остается в статусе на производстве" . PHP_EOL;
            }
        }
    }
}
