<?php

namespace console\controllers;

use backend\modules\leaderModule\blocks\ProductionPeriod\models\ProductionReport;
use core\models\selling\Selling;
use core\models\selling\SellingItem;
use Yii;
use yii\console\Controller;
use yii\db\Connection;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

class SellingController extends Controller
{

    private $locales = ['de', 'ru'];
    /**
     * TODO Необходимо определиться с перечнем заказов, которые мы можем считать РЕАЛИЗАЦИЕЙ.
     *
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {

        $laitovo = Yii::$app->params['obmen_laitovo_site'];
        $db_laitovo = new Connection([
            'dsn' => 'mysql:host=' . $laitovo['host'] . ';port=' . $laitovo['port'] . ';dbname=' . $laitovo['dbname'],
            'username' => $laitovo['username'],
            'password' => $laitovo['password'],
            'charset' => 'utf8',
        ]);

        $this->clearData();

        foreach ($this->locales as $locale) {

            $query = (new Query())
                ->select('id')
                ->from($locale . '_orders')
                ->where(['>=', 'enddate', '2018-01-01 00:00'])
                ->andWhere(['condition' => 'zakryit'])
                ->andWhere(['sellingIsLoaded' => 0]);
            if ($locale == 'de') {
                $query->andWhere(['<>', 'client', 112615]);
            }
            if ($locale == 'ru') {
                //100575,100111,135152,135398 - пользователи тестовые
                //108379 - Удаленный склад МСК
                $query->andWhere(['not in', 'client', [100575,100111,135152,135398,108379]]);
            }

            $laitovo_ids = $query->column($db_laitovo);

            $url = $locale == 'de' ? Yii::$app->params['laitovo-eu'] : Yii::$app->params['laitovo-ru'];
            foreach ($laitovo_ids as $order) {
                $client = new Client();
                /** @var \yii\httpclient\Response $response */
                $response = $client->createRequest()
                    ->setUrl($url . '/api/selling/read')
                    ->setData(['id' => $order])
                    ->send();

                if ($response->isOk && $response->data) {
                    $data = $response->data;

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        if ($selling = Selling::find()->where(['locale' => $data['selling']['locale']])->andWhere(['orderNumber' => $data['selling']['orderNumber']])->one()) {
                            $selling->load($data['selling']);
                            SellingItem::deleteAll(['sellingId' => $selling->id]);
                        } else {
                            $selling = new Selling($data['selling']);
                        }

                        if (!$selling->save())
                            throw new \DomainException('Не удалось сохранить реализацию!');

                        foreach ($data['items'] as $item) {
                            $item = new SellingItem($item);
                            $item->sellingId = $selling->id;
                            if (!$item->save())
                                throw new \DomainException('Не удалось сохранить запись к реализации!');
                        }

                        $response = $client->createRequest()
                            ->setUrl($url . '/api/selling/checked')
                            ->setData(['id' => $order])
                            ->send();
                        if (!$response->isOk) {
                            throw new \DomainException('Не удалось пометить релазиацию как выгруженная!');
                        }

                        $transaction->commit();
                        echo "Реализация № {$selling->id} по заказу $order выгружена" . PHP_EOL;
                    }catch (\Exception $exception) {
                        $transaction->rollBack();
                        echo $exception->getMessage();
                        exit(1);
                    }

                } else {
                    print_r($response->headers);
                    exit(1);
                }
            }
        }
    }

    private function clearData()
    {
        foreach ($this->locales as $locale) {
            $ordersForCheck = Selling::find()
                ->where(['>','date', strtotime('-4 months')])
                ->andWhere(['locale' => $locale])
                ->select('orderNumber as id, sumWithoutNds')
                ->asArray()
                ->all();

            $url = $locale == 'de' ? Yii::$app->params['laitovo-eu'] : Yii::$app->params['laitovo-ru'];
            $client = new Client();
            /** @var \yii\httpclient\Response $response */

            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl($url . '/api/selling/checkOrders')
                ->setData($ordersForCheck)
                ->setFormat('json')
                ->send();

            if ($response->isOk) {
                if ($response->data) {
                    $data = $response->data;
                    $ids = $data['ids'];

                    print_r($data);
                    echo PHP_EOL;

                    if (!empty($ids)) {
                        Selling::deleteAll(['orderNumber' => $ids , 'locale' => $locale]);
                        echo "($locale) Позиции, подвергшиеся изменению, были удалены" . PHP_EOL;
                    } else {
                        echo "($locale) Позиции к удалению остуствуют" . PHP_EOL;
                    }

                }
            }
        }
    }

}
