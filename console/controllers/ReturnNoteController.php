<?php

namespace console\controllers;

use core\models\receipt\Receipt;
use core\models\returnNote\ReturnNote;
use Yii;
use yii\console\Controller;
use yii\db\Connection;
use yii\db\Expression;
use yii\db\Query;
use yii\httpclient\Client;

class ReturnNoteController extends Controller
{

    public function actionIndex()
    {
        echo 'Загружаю документы из локали EU' . PHP_EOL;

        /**
         * Здесь выгружаем поступления из европы.
         */

        $laitovo = Yii::$app->params['obmen_laitovo_site'];
        $db_laitovo = new Connection([
            'dsn' => 'mysql:host=' . $laitovo['host'] . ';port=' . $laitovo['port'] . ';dbname=' . $laitovo['dbname'],
            'username' => $laitovo['username'],
            'password' => $laitovo['password'],
            'charset' => 'utf8',
        ]);

        $returnDeIds = (new Query())->select('id')
            ->from('de_returns')
            ->where(['status' => true])
            ->andWhere(['isLoaded' => false]) //Расчеты с клиентами - приход
            ->column($db_laitovo);

        /**
         * Добавить флаг, что загружено. в eu money
         *
         */
        $url = Yii::$app->params['laitovo-eu'];

        foreach ($returnDeIds as $returnDeId) {

            $client = new Client();
            /** @var \yii\httpclient\Response $response */
            $response = $client->createRequest()
                ->setUrl($url . '/api/returnNote/read')
                ->setData(['id' => $returnDeId])
                ->send();

            if ($response->isOk && $response->data) {
                $data = $response->data;

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($return = ReturnNote::find()->where(['locale' => $data['locale']])->andWhere(['documentId' => $data['documentId']])->one()) {
                        $return->load($data);
                    } else {
                        $return = new ReturnNote($data);
                    }

                    if (!$return->save())
                        throw new \DomainException('(DE) Не удалось сохранить реализацию!');

                    $response = $client->createRequest()
                        ->setUrl($url . '/api/returnNote/checked')
                        ->setData(['id' => $returnDeId])
                        ->send();
                    if (!$response->isOk) {
                        throw new \DomainException('(DE) Не удалось пометить возврат как выгруженный!');
                    }

                    $transaction->commit();
                    echo "Возврат № $returnDeId выгружен" . PHP_EOL;
                }catch (\Exception $exception) {
                    $transaction->rollBack();
                    echo $exception->getMessage();
                    exit(1);
                }

            } else {
                print_r($response->headers);
                exit(1);
            }
        }


        /**
         * Запрос сделан, поля подобраны как надо.
         * Теперь делаем выгрузку
         */
        echo 'Загружаю документы из локали RU' . PHP_EOL;

        $returnRuIds = (new Query())->select(new Expression('
            return.contractorId as clientId, 
            "ru" as locale, 
            return.id as documentId, 
            return.updated_at as date, 
            logist.return_sum as sumWithoutNds,
            logist.return_sum as sumWithNds,
            0 as amountNds,
            0 as sumNds
        '))
            ->from('return_journal return')
            ->leftJoin('return_logist logist','logist.return_journal_id = return.id')
            ->where(['isLoadedReturn' => false])
            ->andWhere(['!=','return.contractorId',0])
            ->andWhere(['return.status' => 60])
            ->andWhere(['is not','logist.id',null])
            ->all();

        foreach ($returnRuIds as $row) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($return = ReturnNote::find()->where(['and',[
                    'locale'      => $row['locale'],
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $return->load($row);
                } else {
                    $return = new ReturnNote($row);
                }

                if (!$return->save())
                    throw new \DomainException('Не удалось сохранить возврат!');

                $command = Yii::$app->db->createCommand()
                    ->update('return_journal',['isLoadedReturn' => true],['id' => $row['documentId']])
                    ->execute();

                if (!$command)
                    throw new \DomainException("Не удалось пометить возврат {$row['documentId']} как загруженный!");

                $transaction->commit();
                echo "(RU) Возврат № {$row['documentId']} выгружено" . PHP_EOL;
            }catch (\Exception $exception) {
                $transaction->rollBack();
                echo $exception->getMessage();
                exit(1);
            }
        }
    }
}
