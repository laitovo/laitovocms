<?php

namespace console\controllers;

use core\helpers\ConsoleLogTrait;
use core\models\debt\Debt;
use Yii;
use yii\console\Controller;
use yii\db\Connection;
use yii\db\Exception as DbException;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class DebtController
 * @package console\controllers
 *
 * Данный контроллер собирает данные по дебеторской задолженности клиентов для монитора руководителя
 */
class DebtController extends Controller
{
    use ConsoleLogTrait;
    /**
     * @throws DbException
     */
    public function actionIndex()
    {
        /**
         * Интервал ожидания между шагами
         */
        $sleepInterval = 1;

        // 1 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(RU) Подключаемся к базе данных 'клиенты'");

        $clients = Yii::$app->params['obmen_clients'];
        $db_clients = new Connection([
            'dsn' => 'mysql:host=' . $clients['host'] . ';port=' . $clients['port'] . ';dbname=' . $clients['dbname'],
            'username' => $clients['username'],
            'password' => $clients['password'],
            'charset' => 'utf8',
        ]);

        $this->echo("(RU) Подключение к базе данных 'клиенты' выполнено");

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        // 2 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(RU) Получаем идентификаторы записей, которые содержатся в родительской таблице 'debt_total'");

        //Данный подзапрос достает клиентов, которые являются покупателями
        $subQuery = (new Query())->select('contractorId')->from('contractors_contractor_groups')
            ->where(['contractorGroupId' => 1]);

        $idsParent = (new Query())->from('debt_total')
            ->select('id')
            ->where(['contractorId' => $subQuery]) //Только контрагенты - покупатели
            ->andWhere(['currencyId' => 643]) //Только взаиморасчеты в рублях
            ->column($db_clients);

        $this->echo("(RU) Получили идентифкаторы записей. Их количество: " . count($idsParent));

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        // 3 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(RU) Получаем идентификаторы записей, которые содержатся в дочерней таблице 'core_debt'");

        $idsChild = Debt::find()->select('documentId')->where(['locale' => 'ru'])->column();

        $this->echo("(RU) Получили идентифкаторы записей. Их количество: " . count($idsChild));

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        // 4 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(RU) Удаляем все записи, которых нет в дочерней таблице");

        $rowsForDelete = array_diff($idsChild, $idsParent);

        $deletedRowCount = Debt::deleteAll(['documentId' => $rowsForDelete,'locale' => 'ru']);

        $this->echo("(RU) Удалено записей :$deletedRowCount");

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        // 5 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(RU) Загружаем записи из родительской таблицы, которых нет в дочерней таблице, или были обновлены с момента последнего обмена");

        //Максимальное время последнего обновленя записей (насколько записи актуальны)
        $maxUpdatedAtChild = Debt::find()->where(['locale' => 'ru'])->max('updatedAt');

        $rowsForUpdate = [];
        if ($maxUpdatedAtChild)
            $rowsForUpdate = (new Query())->select('id')->from('debt_total')
                ->where(['id' => $idsChild])
                ->andWhere(['>','updatedAt', $maxUpdatedAtChild])
                ->column($db_clients);

        $this->echo("(RU) Записей к обновлению :" . count($rowsForUpdate));
        sleep($sleepInterval);


        $rowsForAdd = array_diff($idsParent, $idsChild);
        $this->echo("(RU) Записей к добавлению :" . count($rowsForAdd));
        sleep($sleepInterval);


        $rowsForAddAndUpdate = array_merge($rowsForAdd,$rowsForUpdate);
        $this->echo("(RU) Всего записей к выгрузке:" . count($rowsForAddAndUpdate));
        sleep($sleepInterval);

        $subQuery = (new Query())->select('contractorId')->from('contractors_contractor_groups')
            ->where(['contractorGroupId' => 1]);

        $maxUpdatedAtParent = (new Query())->from('debt_total')
            ->andWhere(['currencyId' => 643]) //Только взаиморасчеты в рублях
            ->where(['contractorId' => $subQuery]) //Только контрагенты - покупатели
            ->max('updatedAt',$db_clients);

        $debtRowsRu = (new Query())->select('id')->from('debt_total')
            ->where(['id' => $rowsForAddAndUpdate])
            ->column($db_clients);

        $this->echo("(RU) Записей к добавлению в дочернюю таблицу: " . count($debtRowsRu));

        foreach ($debtRowsRu as $rowId) {

            $row = (new Query())->select(new Expression('
                id as documentId,
                contractorId as clientId,
                total,
                date,
                "ru" as locale,
                case tableName
                    when "balance_documents" then "' . Debt::BALANCE . '"
                    when "adjustment" then "' . Debt::ADJUSTMENT . '"
                    when "realizations" then "' . Debt::REALIZATION . '"
                    when "realization_entries" then "' . Debt::REALIZATION . '"
                    when "returns" then "' . Debt::RETURN . '"
                end as type
                '
                ))  ->from('debt_total')
                    ->where(['id' => $rowId])
                    ->one($db_clients);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($debt = Debt::find()->where(['and',[
                    'locale'      => 'ru',
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $debt->load($row);
                    $debt->documentId  = $row['documentId'];
                    $debt->clientId    = $row['clientId'];
                    $debt->total       = $row['total'];
                    $debt->date        = $row['date'];
                    $debt->locale      = $row['locale'];
                    $debt->type        = $row['type'];
                } else {
                    $debt = new Debt($row);
                }

                if ($maxUpdatedAtParent)
                    $debt->updatedAt = $maxUpdatedAtParent;

                if (!$debt->save())
                    throw new \DomainException('Не удалось сохранить проводку!');

                $transaction->commit();
                $this->echo("(RU) Запись № {$row['documentId']} выгружена");
            }catch (\Exception $exception) {
                $transaction->rollBack();
                $this->echo($exception->getMessage());
                exit(1);
            }
        }

        $this->echo("(RU) Все записи была добавлены в дочернюю таблицу");

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        /**
         * Выгрузка европейской дебиторки.
         */

        // 6 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(EU) Подключаемся к базе данных 'laitovo'");

        $laitovo = Yii::$app->params['obmen_laitovo_site'];
        $db_laitovo = new Connection([
            'dsn' => 'mysql:host=' . $laitovo['host'] . ';port=' . $laitovo['port'] . ';dbname=' . $laitovo['dbname'],
            'username' => $laitovo['username'],
            'password' => $laitovo['password'],
            'charset' => 'utf8',
        ]);

        $this->echo("(EU) Подключение к базе данных 'laitovo' выполнено");

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        // 7 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(EU) Получаем идентификаторы записей, которые содержатся в родительской таблице 'de_user_money_acc'");

        //Данный подзапрос достает клиентов, которые являются покупателями
        $subQuery = (new Query())->select('id')->from('de_user')
            ->where(['groupValue' => 0]);

        $idsParent = (new Query())->from('de_user_money_acc')
            ->select('id')
            ->where(['userID' => $subQuery]) //Только контрагенты - покупатели
            ->andWhere(['accountingType' => [2,12]])
            ->column($db_laitovo);

        $this->echo("(EU) Получили идентифкаторы записей. Их количество: " . count($idsParent));

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        // 8 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(EU) Получаем идентификаторы записей, которые содержатся в дочерней таблице 'core_debt'");

        $idsChild = Debt::find()->select('documentId')->where(['locale' => 'eu'])->column();

        $this->echo("(EU) Получили идентифкаторы записей. Их количество: " . count($idsChild));

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        // 9 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(EU) Удаляем все записи, которых нет в родительской таблице");

        $rowsForDelete = array_diff($idsChild, $idsParent);

        $deletedRowCount = Debt::deleteAll(['documentId' => $rowsForDelete,'locale' => 'eu']);

        $this->echo("(EU) Удалено записей :$deletedRowCount");

        sleep($sleepInterval);

        ////////////////////////////////////////////////////////////////////////////////////////

        // 10 STEP ////////////////////////////////////////////////////////////////////////////////

        $this->echo("(EU) Загружаем записи из родительской таблицы, которых нет в дочерней таблице, или были обновлены с момента последнего обмена");

        //Максимальное время последнего обновленя записей (насколько записи актуальны)
        $maxUpdatedAtChild = Debt::find()->where(['locale' => 'eu'])->max('updatedAt');

        $rowsForUpdate = [];
        if ($maxUpdatedAtChild)
            $rowsForUpdate = (new Query())->select('id')->from('de_user_money_acc')
                ->where(['id' => $idsChild])
                ->andWhere(['>','unix_timestamp(updatedate)', $maxUpdatedAtChild])
                ->andWhere(['accountingType' => [2,12]])
                ->column($db_laitovo);

        $this->echo("(EU) Записей к обновлению :" . count($rowsForUpdate));
        sleep($sleepInterval);


        $rowsForAdd = array_diff($idsParent, $idsChild);
        $this->echo("(EU) Записей к добавлению :" . count($rowsForAdd));
        sleep($sleepInterval);


        $rowsForAddAndUpdate = array_merge($rowsForAdd,$rowsForUpdate);
        $this->echo("(EU) Всего записей к выгрузке:" . count($rowsForAddAndUpdate));
        sleep($sleepInterval);

        $subQuery = (new Query())->select('id')->from('de_user')
            ->where(['groupValue' => 0]);

        $maxUpdatedAtParent = (new Query())->from('de_user_money_acc')
            ->where(['userID' => $subQuery]) //Только контрагенты - покупатели
            ->andWhere(['accountingType' => [2,12]])
            ->max('unix_timestamp(updatedate)',$db_laitovo);

        $debtRowsDe = (new Query())->select('id')->from('de_user_money_acc')
            ->where(['id' => $rowsForAddAndUpdate])
            ->column($db_laitovo);

        $this->echo("(EU) Записей к добавлению в дочернюю таблицу: " . count($debtRowsDe));

        foreach ($debtRowsDe as $rowId) {

            $row = (new Query())
                ->select(new Expression('
                    id as documentId, 
                    userID as clientId, 
                    unix_timestamp(date) as date,
                    "eu" as locale,
                    (total * typeMove * -1) as total,
                case documentType
                    when 3 then "' . Debt::BALANCE . '"
                    when 4 then "' . Debt::REALIZATION . '"
                    when 5 then "' . Debt::RETURN . '"
                    when 8 then "' . Debt::BALANCE . '"
                    when 100 then "' . Debt::ADJUSTMENT . '"
                    when 200 then "' . Debt::ADJUSTMENT . '"
                    when 300 then "' . Debt::ADJUSTMENT . '"
                end as type'))
                ->from('de_user_money_acc')
                ->where(['id' => $rowId])
                ->one($db_laitovo);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($debt = Debt::find()->where(['and',[
                    'locale'      => 'eu',
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $debt->load($row);
                    $debt->documentId  = $row['documentId'];
                    $debt->clientId    = $row['clientId'];
                    $debt->total       = $row['total'];
                    $debt->date        = $row['date'];
                    $debt->locale      = $row['locale'];
                    $debt->type        = $row['type'];
                } else {
                    $debt = new Debt($row);
                }

                if ($maxUpdatedAtParent)
                    $debt->updatedAt = $maxUpdatedAtParent;

                if (!$debt->save())
                    throw new \DomainException('Не удалось сохранить проводку!');

                $transaction->commit();
                $this->echo("(EU) Запись № {$row['documentId']} выгружена");
            }catch (\Exception $exception) {
                $transaction->rollBack();
                $this->echo($exception->getMessage());
                exit(1);
            }
        }

        $this->echo("(EU) Все записи была добавлены в дочернюю таблицу");

        sleep($sleepInterval);
    }
}
