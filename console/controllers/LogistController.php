<?php
namespace console\controllers;

use backend\modules\logistics\models\Order;
use backend\modules\logistics\models\order\OrderService;
use common\models\laitovo\ErpOrder;
use console\models\CurlExample;
use core\services\SAutoCompletionLogs;
use yii\base\Module;
use backend\modules\logistics\modules\implementationMonitor\interfaces\IProcessService;
use Yii;

class LogistController extends \yii\console\Controller
{
    private $_processService;
    private $_shipmentManager;
    private $_transportCompanyManager;

    public function __construct(
        string $id,
        Module $module,
        IProcessService $processService,
        array $config = [])
    {
        $this->_processService  = $processService;
        $this->_shipmentManager = Yii::$container->get('core\entities\logisticsShipment\ShipmentManager');
        $this->_transportCompanyManager = Yii::$container->get('core\entities\logisticsTransportCompany\TransportCompanyManager');

        parent::__construct($id, $module, $config);
    }

    /**
     * Функция ищет первый необработанный заказ и обрабатывает его.
     * Обработка заказа - это поиск и резервирование upn-ов на складе или, если свободные upn-ы не нашлись, создание новых нарядов под позиции данного заказа.
     */
    public function actionAutoHandle()
    {
        $erpOrder = ErpOrder::find()->where(['status' => null])->orderBy('created_at')->one();

        if (!$erpOrder || !Order::find()->where(['source_innumber' => $erpOrder->id])->exists()) {
            echo 'no orders' . PHP_EOL;
            return;
        }

        echo 'order №' . $erpOrder->id . PHP_EOL;
        echo 'handling ...' . PHP_EOL;
        $erpOrder->status = ErpOrder::STATUS_IN_HANDLE;
        $erpOrder->save();
        if ($this->_processService->handleOrderBySourceInnumber($erpOrder->id)) {
            echo 'success' . PHP_EOL;
        } else {
            $erpOrder->status = null;
            $erpOrder->save();
            echo 'error' . PHP_EOL;
        }
    }

    /**
     * Функция высылает клиенту трэк-код.
     */
    public function actionSendTrackCode()
    {
        $shipment = $this->_shipmentManager->findOneShipmentForSendTrackCode();
        if (!$shipment) {
            echo 'no shipments' . PHP_EOL;
            return;
        }
        echo 'shipment №' . $this->_shipmentManager->getId($shipment) . PHP_EOL;
        echo 'track code sending ...' . PHP_EOL;
        $transportCompany     = $this->_shipmentManager->getRelatedTransportCompany($shipment);
        $trackingLink         = !$transportCompany ? null : $this->_transportCompanyManager->getTrackingLink($transportCompany);
        $transportCompanyName = !$transportCompany ? null : $this->_transportCompanyManager->getName($transportCompany);
        $orders = [];
        foreach ($this->_shipmentManager->getRelatedOrderEntriesNotDeleted($shipment) as $orderEntry) {
            if (!in_array($orderEntry->order->delivery,['Самовывоз Москва','Курьер с установкой Москва','Курьером Москва'])) {
                $orders[(int)$orderEntry->order->source_innumber] = [
                    'id'           => $orderEntry->order->source_innumber,
                    'track_code'   => $this->_shipmentManager->getTrackCode($shipment),
                    'track_link'   => $trackingLink,
                    'company_name' => $transportCompanyName,
//                    'deliveryPrice' => $this->_shipmentManager->getDeliveryPrice($shipment),
                ];
            }
        }
        if (!empty($orders)) {
            $curl = new CurlExample();
            try {
                $curl->addTrack($orders);
            } catch (\Exception $e){
                exit($e->getMessage() . PHP_EOL);
            }
        }

        if (!$this->_shipmentManager->sendTrackCode($shipment)) {
            echo 'saving error' . PHP_EOL;
            return;
        }

        echo 'success' . PHP_EOL;
    }

    /**
     * Данная функция использовалась для тестирования и была оставлена на всякий случай.
     *
     * @param $sourceInnumber
     */
    public function actionSendTrackCodeForOrder($sourceInnumber)
    {
        $order = Order::find()->where(['source_innumber' => $sourceInnumber])->one();
        if (!$order) {
            echo 'order not found' . PHP_EOL;
            return;
        }
        echo 'order №' . $order->source_innumber . PHP_EOL;
        echo 'track code sending ...' . PHP_EOL;
        $shipment = $this->_shipmentManager->findById($order->orderEntries[0]->shipment_id);
        if (!$shipment) {
            echo 'no shipments' . PHP_EOL;
            return;
        }
        $transportCompany     = $this->_shipmentManager->getRelatedTransportCompany($shipment);
        $trackingLink         = !$transportCompany ? null : $this->_transportCompanyManager->getTrackingLink($transportCompany);
        $transportCompanyName = !$transportCompany ? null : $this->_transportCompanyManager->getName($transportCompany);
        $orders = [
            [
                'id'           => $order->source_innumber,
                'track_code'   => $this->_shipmentManager->getTrackCode($shipment),
                'track_link'   => $trackingLink,
                'company_name' => $transportCompanyName,
            ]
        ];
        $curl = new CurlExample();
        try {
            $curl->addTrack($orders);
        } catch (\Exception $e){
            echo 'api error' . PHP_EOL;
            return;
        }

        echo 'success' . PHP_EOL;
    }


    /**
     * Данная функция используется только для тестирования рассылки трек кодов.
     *
     * @param $sourceInnumber
     * @param $trackCode
     */
    public function actionSendTrackCodeForTestOrder($sourceInnumber, $trackCode)
    {
        echo 'order №' . $sourceInnumber . PHP_EOL;
        echo 'track code sending ...' . PHP_EOL;
        $orders = [
            [
                'id'           => $sourceInnumber,
                'track_code'   => $trackCode,
                'track_link'   => 'http://test.tst',
                'company_name' => 'test',
            ]
        ];
        $curl = new CurlExample();
        try {
            $curl->addTrack($orders);
        } catch (\Exception $e){
            echo 'api error' . PHP_EOL;
            return;
        }

        echo 'success' . PHP_EOL;
    }

    /**
     * [ Задача крону для обрабтки логов ]
     *
     * @throws \yii\db\Exception
     */
    public function actionProcessAutoCompletion()
    {
        SAutoCompletionLogs::processLogs($limit = 0);
    }
}
