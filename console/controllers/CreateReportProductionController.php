<?php

namespace console\controllers;

use backend\modules\leaderModule\blocks\ProductionPeriod\models\ProductionReport;
use yii\console\Controller;
use Yii;
use yii\db\Connection;
use yii\db\Query;
use yii\httpclient\Client;

class CreateReportProductionController extends Controller
{

    private $locales = ['de', 'ru'];

    public function actionIndex()
    {
        $laitovo = Yii::$app->params['obmen_laitovo_site'];
        $db_laitovo = new Connection([
            'dsn' => 'mysql:host=' . $laitovo['host'] . ';port=' . $laitovo['port'] . ';dbname=' . $laitovo['dbname'],
            'username' => $laitovo['username'],
            'password' => $laitovo['password'],
            'charset' => 'utf8',
        ]);

        $this->clearData($db_laitovo);

        $storage_ids = (new Query())->select('user_id')->from('remote_storage')->column($db_laitovo);

        foreach ($this->locales as $locale) {

            $exclude = $locale == 'de' ? [2292010, 2292011, 2292012, 2292013, 2292014, 2292140, 2292138] :
                [15018600, 15018599, 15018598, 15019118, 15019119, 15019129, 15019134, 15019161, 15019159];

            $query = (new Query())
                ->select('do.id')
                ->from($locale . '_orders do')
                ->leftJoin($locale . '_user u', 'do.client = u.id')
                ->where(['>=', 'do.createdate', '2018-01-01 00:00'])
                ->andWhere(['or',
                    ['<>', 'do.condition', 'canceled'],
                    ['do.condition' => null]
                ])
                ->andWhere(['not in', 'do.id', $exclude])
                ->andWhere(['not in', 'do.client', $storage_ids])
                ->andWhere(['<>', 'u.role', 'store']);
            if ($locale == 'de') {
                $query->andWhere(['<>', 'do.client', 112615]);
            }

            $laitovo_ids = $query->column($db_laitovo);
            $current_ids = (new Query())
                ->select('order_id')
                ->from(ProductionReport::tableName())
                ->where(['locale' => $locale])
                ->column();

            $arr_diff = array_diff($laitovo_ids, $current_ids);

            $url = $locale == 'de' ? Yii::$app->params['laitovo-eu'] : Yii::$app->params['laitovo-ru'];
            foreach ($arr_diff as $order) {
                $client = new Client();
                /** @var \yii\httpclient\Response $response */
                $response = $client->createRequest()
                    ->setMethod('post')
                    ->setUrl($url . '/api/stat')
                    ->setData(['id' => $order])
                    ->setFormat('json')
                    ->send();

                if ($response->isOk && $response->data) {
                    $data = $response->data;
                    $data['order_id'] = $order;
                    print_r($data);
                    echo PHP_EOL;
                    $result = [
                        'date' => strtotime($data['date']),
                        'locale' => $locale,
                        'sum' => $data['sum'],
                        'type' => $data['type'],
                        'order_id' => $order
                    ];
                    Yii::$app->db->createCommand()->insert(ProductionReport::tableName(), $result)->execute();
                } else {
                    print_r($response->headers);
                    exit(1);
                }
            }
        }
    }

    private function clearData(Connection $connection)
    {
        foreach ($this->locales as $locale) {
            $ids = (new Query())
                ->select('id')
                ->from($locale . '_orders')
                ->where(['condition' => 'canceled'])
                ->andWhere(['>=', 'createdate', '2018-01-01 00:00'])
                ->column($connection);
            ProductionReport::deleteAll(['order_id' => $ids, 'locale' => $locale]);

            $ordersForCheck = ProductionReport::find()
                ->where(['>','date', strtotime('-4 month')])
                ->andWhere(['locale' => $locale])
                ->select('order_id as id, sum, type')
                ->asArray()
                ->all();


            $url = $locale == 'de' ? Yii::$app->params['laitovo-eu'] : Yii::$app->params['laitovo-ru'];
            $client = new Client();
            /** @var \yii\httpclient\Response $response */

            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl($url . '/api/stat/checkOrders')
                ->setData($ordersForCheck)
                ->setFormat('json')
                ->send();

            if ($response->isOk) {
                if ($response->data) {
                    $data = $response->data;
                    $ids = $data['ids'];

                    print_r($data);
                    echo PHP_EOL;

                    ProductionReport::deleteAll(['order_id' => $ids , 'locale' => $locale]);

                    echo "($locale) Позиции, подвергшиеся изменению были удалены" . PHP_EOL;
                }
            }
        }
    }


}