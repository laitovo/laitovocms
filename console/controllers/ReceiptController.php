<?php

namespace console\controllers;

use core\models\receipt\Receipt;
use Yii;
use yii\console\Controller;
use yii\db\Connection;
use yii\db\Exception as DbException;
use yii\db\Expression;
use yii\db\Query;
use yii\httpclient\Client;

class ReceiptController extends Controller
{

    /**
     * @throws DbException
     */
    public function actionIndex()
    {
        $clients = Yii::$app->params['obmen_clients'];
        $db_clients = new Connection([
            'dsn' => 'mysql:host=' . $clients['host'] . ';port=' . $clients['port'] . ';dbname=' . $clients['dbname'],
            'username' => $clients['username'],
            'password' => $clients['password'],
            'charset' => 'utf8',
        ]);

        /**
         * Запрос сделан, поля подобраны как надо.
         * Теперь делаем выгрузку
         */
        echo 'Загружаю документы из локали RU' . PHP_EOL;

        echo 'Сначала ищю, изменялись ли загруженные документы за последние сутки' . PHP_EOL;

        $moneyRUResultReLoaded = (new Query())->select(new Expression('
            docs.id as documentId, 
            contractor.id as clientId, 
            date, 
            sources.title as fromType, 
            sections.title as toType, 
            docs.order_id as orderNumber, 
            docs.total as total, 
            currency.abbreviation as currency,
            CASE 
               WHEN docs.article_type_id = 1 THEN "' . Receipt::MODE_REALIZATION. '"
               ELSE "' . Receipt::MODE_OTHER. '"  
            END as mode
        '))
            ->from('balance_documents docs')
            ->leftJoin('contractors contractor','contractor.id = docs.contractor_id')
            ->leftJoin('income_sources sources','sources.id = docs.source_id')
            ->leftJoin('accounting_sections sections','sections.id = docs.article_type_id')
            ->leftJoin('currencies currency','currency.code = docs.currencyId')
            ->where(['docs.type' => 'admission'])
//            ->andWhere(['docs.article_type_id' => 1])
            ->andWhere(['is not','docs.updatedAt', null])
            ->andWhere(['>','docs.updatedAt', time() - 60*60*24])
            ->andWhere(['docs.isLoadedReceipt' => true])

            ->all($db_clients);

        foreach ($moneyRUResultReLoaded as $row) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($receipt = Receipt::find()->where(['and',[
                    'locale'      => 'ru',
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $receipt->load($row);
                    $receipt->clientId = $row['clientId'];
                    $receipt->date = $row['date'];
                    $receipt->total = $row['total'];
                    $receipt->mode = $row['mode'];
                    $receipt->currency = $row['currency'];
                } else {
                    $receipt = new Receipt($row);
                    $receipt->locale = 'ru';
                }

                if (!$receipt->save())
                    throw new \DomainException('Не удалось сохранить реализацию!');

                $client = new Client();
                $url = Yii::$app->params['clients']['host'];
                $response = $client->createRequest()
                    ->setUrl($url . '/api/v1/receipt/checked')
                    ->setHeaders(['Authorization' => 'Bearer ' . Yii::$app->params['clients']['token']])
                    ->setData(['id' => $row['documentId']])
                    ->send();
                if (!$response->isOk) {
                    throw new \DomainException("Не удалось пометить документ {$row['documentId']} поступления как загруженный!");
                }

                $transaction->commit();
                echo "(RU) Поступление № {$row['documentId']} по заказу {$row['orderNumber']} выгружено" . PHP_EOL;
            }catch (\Exception $exception) {
                $transaction->rollBack();
                echo $exception->getMessage();
                exit(1);
            }
        }

        echo '(RU) Загружаю новые документы' . PHP_EOL;

        $moneyRUResult = (new Query())->select(new Expression('
            docs.id as documentId, 
            contractor.id as clientId, 
            date, 
            sources.title as fromType, 
            sections.title as toType, 
            docs.order_id as orderNumber, 
            docs.total as total, 
            currency.abbreviation as currency,
            CASE 
               WHEN docs.article_type_id = 1 THEN "' . Receipt::MODE_REALIZATION. '"
               ELSE "' . Receipt::MODE_OTHER. '"  
            END as mode
        '))
            ->from('balance_documents docs')
            ->leftJoin('contractors contractor','contractor.id = docs.contractor_id')
            ->leftJoin('income_sources sources','sources.id = docs.source_id')
            ->leftJoin('accounting_sections sections','sections.id = docs.article_type_id')
            ->leftJoin('currencies currency','currency.code = docs.currencyId')
            ->where(['docs.type' => 'admission'])
//            ->andWhere(['docs.article_type_id' => 1])
            ->andWhere(['docs.isLoadedReceipt' => false])
            ->all($db_clients);

        foreach ($moneyRUResult as $row) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($receipt = Receipt::find()->where(['and',[
                    'locale'      => 'ru',
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $receipt->load($row);
                    $receipt->clientId = $row['clientId'];
                    $receipt->date = $row['date'];
                    $receipt->total = $row['total'];
                    $receipt->mode = $row['mode'];
                    $receipt->currency = $row['currency'];
                } else {
                    $receipt = new Receipt($row);
                    $receipt->locale = 'ru';
                }

                if (!$receipt->save()) {
                    print_r($receipt->errors);
                    echo PHP_EOL;
                    print_r($receipt);
                    throw new \DomainException('Не удалось сохранить реализацию!');
                }
                $client = new Client();
                $url = Yii::$app->params['clients']['host'];
                $response = $client->createRequest()
                    ->setUrl($url . '/api/v1/receipt/checked')
                    ->setHeaders(['Authorization' => 'Bearer ' . Yii::$app->params['clients']['token']])
                    ->setData(['id' => $row['documentId']])
                    ->send();
                if (!$response->isOk) {
                    throw new \DomainException("Не удалось пометить документ {$row['documentId']} поступления как загруженный!");
                }

                $transaction->commit();
                echo "(RU) Поступление № {$row['documentId']} по заказу {$row['orderNumber']} выгружено" . PHP_EOL;
            }catch (\Exception $exception) {
                $transaction->rollBack();
                echo $exception->getMessage();
                exit(1);
            }
        }


        echo 'Загружаю документы из локали EU' . PHP_EOL;

        echo 'Сначала ищю, изменялись ли загруженные документы за последние сутки' . PHP_EOL;

        /**
         * Здесь выгружаем поступления из европы.
         */

        $laitovo = Yii::$app->params['obmen_laitovo_site'];
        $db_laitovo = new Connection([
            'dsn' => 'mysql:host=' . $laitovo['host'] . ';port=' . $laitovo['port'] . ';dbname=' . $laitovo['dbname'],
            'username' => $laitovo['username'],
            'password' => $laitovo['password'],
            'charset' => 'utf8',
        ]);


        $moneyDEResultReLoaded = (new Query())->select(new Expression('
            id as documentId, 
            user as clientId, 
            unix_timestamp(date) as date, 
            direction as fromType, 
            direction as toType, 
            orderid as orderNumber, 
            money as total,
            "EUR" as currency,
            CASE 
               WHEN article = 23 THEN "' . Receipt::MODE_REALIZATION. '"
               ELSE "' . Receipt::MODE_OTHER. '"  
            END as mode
        '))
            ->from('de_user_money')
            ->where(['>','money',0])
            ->andWhere(['isReceiptLoaded' => 1]) //Расчеты с клиентами - приход
            ->andWhere(['is not','user', null]) //Расчеты с клиентами - приход
            ->andWhere(['>','unix_timestamp(updatedate)', time() - 60*60*24])
            ->andWhere(['is not','updatedate', null])
            ->all($db_laitovo);

        /**
         * Добавить флаг, что загружено. в eu money
         *
         */
        foreach ($moneyDEResultReLoaded as $row) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($receipt = Receipt::find()->where(['and',[
                    'locale'      => 'eu',
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $receipt->load($row);
                    $receipt->clientId = $row['clientId'];
                    $receipt->date = $row['date'];
                    $receipt->total = $row['total'];
                    $receipt->mode = $row['mode'];
                    $receipt->currency = $row['currency'];
                } else {
                    $receipt = new Receipt($row);
                    $receipt->locale = 'eu';
                }

                if (!$receipt->save())
                    throw new \DomainException('Не удалось сохранить поступление!');

                $client = new Client();
                $url = Yii::$app->params['laitovo-eu'];
                $response = $client->createRequest()
                    ->setUrl($url . '/api/receipt/checked')
                    ->setData(['id' => $row['documentId']])
                    ->send();
                if (!$response->isOk) {
                    throw new \DomainException("Не удалось пометить документ {$row['documentId']} поступления как загруженный!");
                }

                $transaction->commit();
                echo "(EU) Поступление № {$row['documentId']} по заказу {$row['orderNumber']} выгружено" . PHP_EOL;
            }catch (\Exception $exception) {
                $transaction->rollBack();
                echo $exception->getMessage();
                exit(1);
            }
        }

        echo '(EU) Загружаю новые документы' . PHP_EOL;

        $moneyDEResult = (new Query())->select(new Expression('
            id as documentId, 
            user as clientId, 
            unix_timestamp(date) as date, 
            direction as fromType, 
            direction as toType, 
            orderid as orderNumber, 
            money as total,
            "EUR" as currency,
            CASE 
               WHEN article = 23 THEN "' . Receipt::MODE_REALIZATION. '"
               ELSE "' . Receipt::MODE_OTHER. '"  
            END as mode
        '))
            ->from('de_user_money')
            ->where(['>','money',0])
            ->andWhere(['isReceiptLoaded' => 0]) //Расчеты с клиентами - приход
            ->andWhere(['is not','user', null]) //Расчеты с клиентами - приход
            ->all($db_laitovo);

        /**
         * Добавить флаг, что загружено. в eu money
         *
         */
        foreach ($moneyDEResult as $row) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($receipt = Receipt::find()->where(['and',[
                    'locale'      => 'eu',
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $receipt->load($row);
                    $receipt->clientId = $row['clientId'];
                    $receipt->date = $row['date'];
                    $receipt->total = $row['total'];
                    $receipt->mode = $row['mode'];
                    $receipt->currency = $row['currency'];
                } else {
                    $receipt = new Receipt($row);
                    $receipt->locale = 'eu';
                }

                if (!$receipt->save())
                    throw new \DomainException('Не удалось сохранить поступление!');

                $client = new Client();
                $url = Yii::$app->params['laitovo-eu'];
                $response = $client->createRequest()
                    ->setUrl($url . '/api/receipt/checked')
                    ->setData(['id' => $row['documentId']])
                    ->send();
                if (!$response->isOk) {
                    throw new \DomainException("Не удалось пометить документ {$row['documentId']} поступления как загруженный!");
                }

                $transaction->commit();
                echo "(EU) Поступление № {$row['documentId']} по заказу {$row['orderNumber']} выгружено" . PHP_EOL;
            }catch (\Exception $exception) {
                $transaction->rollBack();
                echo $exception->getMessage();
                exit(1);
            }
        }
    }
}
