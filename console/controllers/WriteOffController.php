<?php

namespace console\controllers;

use core\models\writeOff\WriteOff;
use Yii;
use yii\console\Controller;
use yii\db\Connection;
use yii\db\Exception as DbException;
use yii\db\Expression;
use yii\db\Query;
use yii\httpclient\Client;

class WriteOffController extends Controller
{

    /**
     * @throws DbException
     */
    public function actionIndex()
    {
        $clients = Yii::$app->params['obmen_clients'];
        $db_clients = new Connection([
            'dsn' => 'mysql:host=' . $clients['host'] . ';port=' . $clients['port'] . ';dbname=' . $clients['dbname'],
            'username' => $clients['username'],
            'password' => $clients['password'],
            'charset' => 'utf8',
        ]);

        /**
         * Запрос сделан, поля подобраны как надо.
         * Теперь делаем выгрузку
         */
        echo 'Загружаю документы из локали RU' . PHP_EOL;

        $allWriteOffRu = (new Query())->select(new Expression('
            docs.id as documentId, 
            contractor.id as clientId, 
            "ru" as locale, 
            date, 
            CASE
                WHEN docs.article_type_id = 3 THEN "' . WriteOff::MONEY_RETURN_TYPE . '"
                WHEN (contractor.id = 50461 AND docs.article_type_id = 2) THEN "' . WriteOff::COMMISSION_TYPE . '"
                WHEN (contractor.id = 50473 AND docs.article_type_id = 4 AND docs.source_id = 5) THEN "' . WriteOff::COMMISSION_TYPE . '"
                ELSE "' . WriteOff::OTHER_TYPE . '"
            END as type,
            docs.total as total, 
            currency.abbreviation as currency
        '))
            ->from('balance_documents docs')
            ->leftJoin('contractors contractor','contractor.id = docs.contractor_id')
            ->leftJoin('income_sources sources','sources.id = docs.source_id')
            ->leftJoin('accounting_sections sections','sections.id = docs.article_type_id')
            ->leftJoin('currencies currency','currency.code = docs.currencyId')
            ->where(['docs.type' => 'write-off'])
            ->andWhere(['docs.isLoadedWriteOff' => false])
            ->andWhere(['!=','docs.article_type_id', 5])
            ->all($db_clients);

        foreach ($allWriteOffRu as $row) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($writeOff = WriteOff::find()->where(['and',[
                    'locale'      => 'ru',
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $writeOff->load($row);
                    $writeOff->clientId = $row['clientId'];
                    $writeOff->date = $row['date'];
                    $writeOff->type = $row['type'];
                    $writeOff->total = $row['total'];
                    $writeOff->currency = $row['currency'];
                } else {
                    $writeOff = new WriteOff($row);
                    $writeOff->locale = 'ru';
                }

                if (!$writeOff->save())
                    throw new \DomainException('Не удалось сохранить расход!');

                $client = new Client();
                $url = Yii::$app->params['clients']['host'];
                $response = $client->createRequest()
                    ->setUrl($url . '/api/v1/write-off/money-return-checked')
                    ->setHeaders(['Authorization' => 'Bearer ' . Yii::$app->params['clients']['token']])
                    ->setData(['id' => $row['documentId']])
                    ->send();
                if (!$response->isOk) {
                    throw new \DomainException("(RU) Не удалось пометить документ {$row['documentId']} расхода как загруженный!");
                }

                $transaction->commit();
                echo "(RU) Расход № {$row['documentId']} выгружен" . PHP_EOL;
            }catch (\Exception $exception) {
                $transaction->rollBack();
                echo $exception->getMessage();
                exit(1);
            }
        }


        echo 'Загружаю документы из локали EU' . PHP_EOL;

        /**
         * Здесь выгружаем поступления из европы.
         */
        $laitovo = Yii::$app->params['obmen_laitovo_site'];
        $db_laitovo = new Connection([
            'dsn' => 'mysql:host=' . $laitovo['host'] . ';port=' . $laitovo['port'] . ';dbname=' . $laitovo['dbname'],
            'username' => $laitovo['username'],
            'password' => $laitovo['password'],
            'charset' => 'utf8',
        ]);

        echo '(EU) Загружаю возвраты денежных средств клиентам' . PHP_EOL;


        $allWriteOffEu = (new Query())->select(new Expression('
            id as documentId,
            user as clientId,
            unix_timestamp(date) as date,
            CASE
                WHEN article = 24 THEN "' . WriteOff::MONEY_RETURN_TYPE . '"
                WHEN (user = 100194 AND article = 9) THEN "' . WriteOff::COMMISSION_TYPE . '"
                ELSE "' . WriteOff::OTHER_TYPE . '"
            END as type,
            (money * -1) as total,
            "EUR" as currency
        '))
            ->from('de_user_money')
            ->where(['<','money',0])
            ->andWhere(['isWriteOffLoaded' => 0]) //Расчеты с клиентами - приход
            ->all($db_laitovo);

        /**
         * Добавить флаг, что загружено. в eu money
         *
         */
        foreach ($allWriteOffEu as $row) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($writeOff = WriteOff::find()->where(['and',[
                    'locale'      => 'eu',
                    'documentId'  => $row['documentId']
                ]])->one()) {
                    $writeOff->load($row);
                    $writeOff->clientId = $row['clientId'];
                    $writeOff->date = $row['date'];
                    $writeOff->type = $row['type'];
                    $writeOff->total = $row['total'];
                    $writeOff->currency = $row['currency'];
                } else {
                    $writeOff = new WriteOff($row);
                    $writeOff->locale = 'eu';
                }

                if (!$writeOff->save())
                    throw new \DomainException('(EU) Не удалось сохранить расход!');

                $client = new Client();
                $url = Yii::$app->params['laitovo-eu'];
                $response = $client->createRequest()
                    ->setUrl($url . '/api/writeOff/checked')
                    ->setData(['id' => $row['documentId']])
                    ->send();
                if (!$response->isOk) {
                    throw new \DomainException("(EU) Не удалось пометить документ {$row['documentId']} расхода как загруженный!");
                }

                $transaction->commit();
                echo "(EU) Расход № {$row['documentId']} выгружен" . PHP_EOL;
            }catch (\Exception $exception) {
                $transaction->rollBack();
                echo $exception->getMessage();
                exit(1);
            }
        }
    }
}
