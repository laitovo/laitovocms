<?php
namespace console\models;

/** This is DTO object*/

class DTOCurl
{
    /** @var string */
    public $url;
    /** @var string */
    public $method = 'GET';
    /** @var array */
    public $data = [];
    /** @var int */
    public $codeResponse = 200;
    /** @var bool  */
    public $showResponseHeaders = false;
}