<?php
namespace console\models;

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.07.18
 * Time: 10:16
 */

class CurlExample
{
    const AUTH = 'Authorization: Bearer ';
//    const BASE_URL = 'http://188.43.51.186/';
    const BASE_URL = 'https://laitovo.ru/';

    /** @var string  */
    private $token = '$2a$13$XdSk4SVv5Y5lswtJEgYhueMkROPZIPN9B1iFc.MpU6mrHnA8q68PG';
    /** @var string */
    private $urlAddToken = 'api/track/add';

    public function addTrack($orders)
    {
        // ключи у всех массивом должны быть именно такие, как в примере
//        $orders = [
//            [
//                'id' => 15001490,
//                'track_code' => '23wers'
//            ],
//            [
//                'id' => 15001488,
//                'track_code' => '223werrs'
//            ],
//        ];
        $data['token'] = $this->token;
        $data['orders'] = $orders;
        $dto = new DTOCurl();
        $dto->url = $this->urlAddToken;
        $dto->data = $data;
        $dto->method = 'POST';
        $this->baseCurl($dto);
    }

    private function baseCurl(DTOcurl $dto)
    {
        $ch = curl_init(self::BASE_URL . $dto->url);
        // в нашем конкретном случае без авторизации, иначе раскоментировать
        //curl_setopt($ch, CURLOPT_HTTPHEADER, [
        //    'Content-Type: application/json',
        //    self::AUTH . $this->token
        //]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $dto->method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dto->data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        if ($code != $dto->codeResponse) {
            throw new \DomainException('Response code ' . $code . '. Code must be ' . $dto->codeResponse . '. Message: ' . $result);
        }
        curl_close($ch);
        // в нашем конкретном случае не нужно, иначе раскоментировать
        //return json_decode($result);
    }
}