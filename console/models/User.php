<?php

namespace console\models;

use yii\base\Component;

class User extends Component
{
    public $identity;
    public $isGuest;
    public $cover;

    const CONSOLE_USER_ID = 21;

    public function __construct(array $config = [])
    {
        $this->identity = \common\models\User::findOne(self::CONSOLE_USER_ID);
        $this->isGuest = false;
        $this->cover = false;

        parent::__construct($config);
    }

    public function __get($name)
    {
        return $this->identity->$name;
    }

    public function __call($name, $arguments)
    {
        return $this->identity->$name();
    }

    public function getIdentity()
    {
        return $this->identity;
    }

    public function getIsGuest()
    {
        return $this->isGuest;
    }

    public function getCover()
    {
        return $this->cover;
    }

    public function can()
    {
        return true;
    }

    public function checkAccess()
    {
        return true;
    }
}
