<?php

namespace console\models;

use yii\base\Component;

class Team extends Component
{
    public $team;

    const CONSOLE_TEAM_ID = 1;

    public function __construct(array $config = [])
    {
        $this->team = \common\models\team\Team::findOne(self::CONSOLE_TEAM_ID);

        parent::__construct($config);
    }

    public function __get($name)
    {
        return $this->team->$name;
    }

    public function __call($name, $arguments)
    {
        return $this->team->$name();
    }

    public function getId()
    {
        return $this->team->id;
    }

    public function getTeam()
    {
        return $this->team;
    }

    public function setTeam()
    {
        return true;
    }

    public function can()
    {
        return true;
    }
}
