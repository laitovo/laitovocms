<?php
return [
    'name' => 'Bizzone',
    'timeZone' => YII_ENV ? 'Europe/Moscow' : 'GMT+03:00',
    'sourceLanguage' => 'ru-RU',
    'language' => 'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            //'linkAssets' => true,
        ],    
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'formatter' => [
            'currencyCode' => 'RUB',
            'timeZone' => 'GMT+03:00',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'files/view/<fileKey:.*>' => 'file-access/view',
                'files/download/<fileKey:.*>' => 'file-access/download',
            ],
        ],
    ],
    'controllerMap' => [
        'file-access' => 'backend\components\fileManager\core\FileAccessController',
        // Миграции returnModule
        'return-migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'migration_return_module',
            'migrationPath' => '@returnModule/migrations',
            'templateFile' =>  '@returnModule/migrations/template/template.php'
        ],
        // миграции для отчетов
        'report-migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'migration_report_module',
            'migrationPath' => '@backend/modules/leaderModule/migrations',
            'templateFile' => '@console/migrations/template/template.php',
        ],
    ],

];
