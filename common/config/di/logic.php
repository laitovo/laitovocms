<?php

#Поиск индивидуальных инструкций по артикулу
Yii::$container->setSingleton(
    'core\logic\FindCustomManualByArticle',
    'core\logic\FindCustomManualByArticle'
);

#Удаление логистических позиций
Yii::$container->setSingleton(
    'core\logic\DeleteOrderEntry',
    'core\logic\DeleteOrderEntry'
);

#Постановка заказов на паузу
Yii::$container->setSingleton(
    'core\logic\PauseOrder',
    'core\logic\PauseOrder'
);

#Изменение срочности заказов
Yii::$container->setSingleton(
    'core\logic\AccelerateOrder',
    'core\logic\AccelerateOrder'
);

#Обработка отгрузок
Yii::$container->setSingleton(
    'core\logic\HandleShipment',
    'core\logic\HandleShipment'
);

#Упаковка отгрузок
Yii::$container->setSingleton(
    'core\logic\PackShipment',
    'core\logic\PackShipment'
);

#Отмена отгрузок
Yii::$container->setSingleton(
    'core\logic\CancelShipment',
    'core\logic\CancelShipment'
);

#Резервирование UPN-ов
Yii::$container->setSingleton(
    'core\logic\ReserveUpn',
    'core\logic\ReserveUpn'
);

#Получение информации об остатках на складе
Yii::$container->setSingleton(
    'core\logic\GetStorageBalance',
    'core\logic\GetStorageBalance'
);

#Оприходование продукции на склад
Yii::$container->setSingleton(
    'core\logic\Oprihodovanie',
    'core\logic\Oprihodovanie'
);