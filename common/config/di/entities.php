<?php

#Виды оконных проемов
Yii::$container->setSingleton(
    'core\entities\propWindowOpeningType\WindowOpeningTypeManager',
    'core\entities\propWindowOpeningType\WindowOpeningTypeManager'
);

#Бренды
Yii::$container->setSingleton(
    'core\entities\propBrand\BrandManager',
    'core\entities\propBrand\BrandManager'
);

#Виды исполнений
Yii::$container->setSingleton(
    'core\entities\propExecutionType\ExecutionTypeManager',
    'core\entities\propExecutionType\ExecutionTypeManager'
);

#Виды тканей
Yii::$container->setSingleton(
    'core\entities\propClothType\ClothTypeManager',
    'core\entities\propClothType\ClothTypeManager'
);

#Виды инструкций
Yii::$container->setSingleton(
    'core\entities\laitovoInstructionType\InstructionTypeManager',
    'core\entities\laitovoInstructionType\InstructionTypeManager'
);

#Индивидуальные инструкции для автомобилей
Yii::$container->setSingleton(
    'core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager',
    'core\entities\laitovoCustomCarInstruction\CustomCarInstructionManager'
);

#Аналитика склада
Yii::$container->setSingleton(
    'core\entities\storageAnalytics\StorageAnalyticsManager',
    'core\entities\storageAnalytics\StorageAnalyticsManager'
);

#Реализованные артикулы
Yii::$container->setSingleton(
    'core\entities\logisticsArticleStatistics\ArticleStatisticsManager',
    'core\entities\logisticsArticleStatistics\ArticleStatisticsManager'
);

#Виды продукции
Yii::$container->setSingleton(
    'core\entities\laitovoErpProductType\ProductTypeManager',
    'core\entities\laitovoErpProductType\ProductTypeManager'
);

#Контрагенты
Yii::$container->setSingleton(
    'core\entities\contractor\ContractorManager',
    'core\entities\contractor\ContractorManager'
);

#Группы контрагентов
Yii::$container->setSingleton(
    'core\entities\contractorGroup\GroupManager',
    'core\entities\contractorGroup\GroupManager'
);

#Банковские счета контрагентов
Yii::$container->setSingleton(
    'core\entities\contractorBankAccount\BankAccountManager',
    'core\entities\contractorBankAccount\BankAccountManager'
);

#Договоры контрагентов
Yii::$container->setSingleton(
    'core\entities\contractorContract\ContractManager',
    'core\entities\contractorContract\ContractManager'
);

#Контакты контрагентов
Yii::$container->setSingleton(
    'core\entities\contractorContact\ContactManager',
    'core\entities\contractorContact\ContactManager'
);

#Отгрузки
Yii::$container->setSingleton(
    'core\entities\logisticsShipment\ShipmentManager',
    'core\entities\logisticsShipment\ShipmentManager'
);

#Документы отгрузок
Yii::$container->setSingleton(
    'core\entities\logisticsShipmentDocument\ShipmentDocumentManager',
    'core\entities\logisticsShipmentDocument\ShipmentDocumentManager'
);

#Логи реализации
Yii::$container->setSingleton(
    'core\entities\logisticsRealizationLog\RealizationLogManager',
    'core\entities\logisticsRealizationLog\RealizationLogManager'
);

#Виды складской продукции
Yii::$container->setSingleton(
    'core\entities\logisticsStorageProductType\StorageProductTypeManager',
    'core\entities\logisticsStorageProductType\StorageProductTypeManager'
);

#Потерянные UPN-ы
Yii::$container->setSingleton(
    'core\entities\logisticsLostUpn\LostUpnManager',
    'core\entities\logisticsLostUpn\LostUpnManager'
);

#Транспортные компании
Yii::$container->setSingleton(
    'core\entities\logisticsTransportCompany\TransportCompanyManager',
    'core\entities\logisticsTransportCompany\TransportCompanyManager'
);