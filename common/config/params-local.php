<?php
return [
    'supportEmail' => $_ENV['SUPPORT_EMAIL'],
    'mailer.transport' =>[
        'supportEmail'=>[
            'class' => 'Swift_SmtpTransport',
            'host' => $_ENV['SUPPORT_EMAIL_HOST'],
            'username' => $_ENV['SUPPORT_EMAIL_USERNAME'],
            'password' => $_ENV['SUPPORT_EMAIL_PASSWORD'],
            'port' => '465',
            'encryption' => 'ssl',
        ],
    ],
    'remoteStorage' => [
        'host'  => $_ENV['HOST_REMOTE_STORAGE'],
        'token' => [
            1 => $_ENV['REMOTE_STORAGE_MOSCOW_TOKEN'],
            2 => $_ENV['REMOTE_STORAGE_HAMBURG_TOKEN']
        ] //Удаленный склад москва
    ],
    'clients' => [
        'host'  => $_ENV['HOST_CLIENTS'],
        'token' => $_ENV['CLIENTS_TOKEN']
    ],
    'laitovo-ru' => $_ENV['HOST_LAITOVO_RU'],
    'laitovo-eu' => $_ENV['HOST_LAITOVO_EU'],
];
