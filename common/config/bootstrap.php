<?php
require(__DIR__ . '/di/di.php');
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('obmen', dirname(dirname(__DIR__)) . '/obmen');
Yii::setAlias('api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('backendViews', dirname(dirname(__DIR__)) . '/backend/themes/remark/modules');
Yii::setAlias('backendUploads', dirname(dirname(__DIR__)) . '/backend/uploads');
Yii::setAlias('core', dirname(dirname(__DIR__)) . '/core');
Yii::setAlias('returnModule', dirname(dirname(__DIR__)) . '/backend/modules/returnmodule');
if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
  $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}