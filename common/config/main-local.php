<?php
$config = [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=' . $_ENV['DB_HOST'] .';dbname=' . $_ENV['DB_NAME'],
            'username' => $_ENV['DB_USER'],
            'password' => $_ENV['DB_PASSWD'],
            'charset' => $_ENV['DB_CHARSET'],
            'attributes' => [
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SESSION sql_mode = (SELECT REPLACE(@@sql_mode,"ONLY_FULL_GROUP_BY",""));'
            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
    ],
];

if (YII_ENV_DEV) {
    $config['components']['mailer']['useFileTransport'] = true;
}

return $config;

