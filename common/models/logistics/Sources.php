<?php

namespace common\models\logistics;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "logistics_sources".
 *
 * @property integer $id
 * @property integer $team_id
 * @property string $title
 * @property integer $type
 * @property string $key
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property Team $team
 * @property User $updater
 */
class Sources extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_sources';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'Идентификатор'),
            'team_id'    => Yii::t('app', 'Команда'),
            'title'      => Yii::t('app', 'Наименование'),
            'type'       => Yii::t('app', 'Тип'),
            'key'        => Yii::t('app', 'Ключ'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'author_id'  => Yii::t('app', 'Автор'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'updater_id' => Yii::t('app', 'Редактор'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updater_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'team_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'team_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->team->id;
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(\common\models\team\Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updater_id']);
    }
}
