<?php

namespace common\models\logistics;

use Yii;

/**
 * This is the model class for table "logistics_article_statistics".
 *
 * @property integer $id
 * @property string $article
 * @property integer $created_at
 * @property integer $type
 * @property integer $orderId
 */
class ArticleStatistics extends \common\models\LogActiveRecord
{
    /**
     * Виды возможных типов статистики
     */
    const TYPE_LAITOVO     = 1; //laitovo
    const TYPE_BIZZONE     = 2; //bizzone
    const TYPE_REALIZATION = 3; //realization

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_article_statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'type','orderId'], 'integer'],
            [['created_at', 'type','article'], 'required'],
            [['article'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Article',
            'created_at' => 'Created At',
            'type' => 'Type',
        ];
    }
}
