<?php

namespace common\models\logistics;

use Yii;
use \common\models\User;

/**
 * This is the model class for table "logistics_storage_literal".
 *
 * @property integer $id
 * @property string $title
 * @property integer $storage_id
 * @property integer $capacity
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property LogisticsStorage $storage
 * @property User $updater
 */
class StorageLiteral extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_storage_literal';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app','Идентификатор'),
            'title'      => Yii::t('app','Наименование'),
            'storage_id' => Yii::t('app','Место хрнения'),
            'capacity'   => Yii::t('app','Емкость'),
            'default_quantity'   => Yii::t('app','Количество по умолчанию'),
            'created_at' => Yii::t('app','Дата создания'),
            'author_id'  => Yii::t('app','Автор'),
            'updated_at' => Yii::t('app','Дата изменения'),
            'updater_id' => Yii::t('app','Редактор'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }
}
