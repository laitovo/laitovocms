<?php

namespace common\models\logistics;

use Yii;
use \common\models\logistics\Sources;
use \common\models\logistics\Storage;
use \common\models\team\Team;
use \common\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use \common\models\laitovo\ErpNaryad;


/**
 * This is the model class for table "logistics_naryad".
 *
 * @property integer $id
 * @property integer $team_id
 * @property string $article
 * @property boolean $withTape
 * @property integer $source_id
 * @property string $responsible_person
 * @property string $comment
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 * @property bool $laitovoSimple
 *
 * @property User $author
 * @property LogisticsSources $source
 * @property Team $team
 * @property User $updater
 * @property StorageState $storageState
 * @property ErpNaryad $erpNaryad
 */
class Naryad extends \common\models\LogActiveRecord
{
    public $title;
    public $count;
    //Статусы
    const STATUS_IN_PRODUCTION = 1; //на производстве
    const STATUS_IN_CLIENT     = 2; //у клиента
    const STATUS_WAITS_FOR     = 3; //ожидает прихода после производства
    const STATUS_IN_STORAGE    = 4; //на складе

    public $statuses = [
        '' => '',
        self::STATUS_IN_PRODUCTION => 'На производстве',
        self::STATUS_IN_CLIENT     => 'У клиента',
        self::STATUS_WAITS_FOR     => 'Ожидает прихода',
        self::STATUS_IN_STORAGE    => 'На складе',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_naryad';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>                  Yii::t('app','Идентификатор'),
            'team_id' =>             Yii::t('app','Команда'),
            'article' =>             Yii::t('app','Артикул'),
            'source_id' =>           Yii::t('app','Источник'),
            'responsible_person' =>  Yii::t('app','Ответственный'),
            'comment' =>             Yii::t('app','Комментарий'),
            'created_at' =>          Yii::t('app','Дата создания'),
            'author_id' =>           Yii::t('app','Автор'),
            'updated_at' =>          Yii::t('app','Дата изменения'),
            'updater_id' =>          Yii::t('app','Редактор'),
            'status' =>              Yii::t('app','Статус'),
            'storage_id' =>          Yii::t('app','Место хранения'),
            'barcode' =>             Yii::t('app','Штрихкод'),
            'laitovoSimple' =>       Yii::t('app','Приклеивать лейбл лайтово симпл'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updater_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
//            [
//                'class' => AttributeBehavior::className(),
//                'attributes' => [
//                    ActiveRecord::EVENT_BEFORE_INSERT => 'team_id',
//                    ActiveRecord::EVENT_BEFORE_UPDATE => 'team_id',
//                ],
//                'value' => function ($event) {
//                    return Yii::$app->team->id;
//                },
//            ],
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
//        return parent::find()->where([self::tableName() . '.team_id' => Yii::$app->team->id]);
        return parent::find()->where([self::tableName() . '.team_id' => 1]); //Временный костыль
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Sources::className(), ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageState()
    {
        return $this->hasOne(StorageState::className(), ['upn_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpNaryad()
    {
        return $this->hasOne(ErpNaryad::className(), ['logist_id' => 'id']);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderEntry()
    {
        return $this->hasOne(OrderEntry::className(), ['id' => 'reserved_id']);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(),['id' => 'pid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(self::className(),['pid' => 'id'])->orderBy('article');
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (in_array('reserved_id', array_keys($this->getDirtyAttributes()))) {
            $this->reserved_at = $this->reserved_id ? strtotime('now') : null;
        }

        return true;
    }
}
