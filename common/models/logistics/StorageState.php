<?php

namespace common\models\logistics;

use Yii;
use \common\models\User;
use \common\models\logistics\Storage;
use \common\models\logistics\Order;
use \common\models\logistics\OrderEntry;
use \common\models\logistics\Naryad;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "logistics_storage_state".
 *
 * @property integer $id
 * @property integer $storage_id
 * @property string $literal
 * @property integer $upn_id
 * @property integer $reserved
 * @property integer $order_id
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property LogisticsOrder $order
 * @property Storage $storage
 * @property User $updater
 * @property LogisticsNaryad $upn
 */
class StorageState extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_storage_state';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app','Идентификатор'),
            'storage_id' => Yii::t('app','Место хранения'),
            'literal'    => Yii::t('app','Литера'),
            'upn_id'     => Yii::t('app','UPN'),
            'reserved'   => Yii::t('app','Зарезервирован'),
            'reserved_id'=> Yii::t('app','Артикул'),
            'created_at' => Yii::t('app','Дата создания'),
            'author_id'  => Yii::t('app','Автор'),
            'updated_at' => Yii::t('app','Дата изменения'),
            'updater_id' => Yii::t('app','Редактор'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updater_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderEntry()
    {
        return $this->hasOne(OrderEntry::className(), ['id' => 'reserved_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpn()
    {
        return $this->hasOne(Naryad::className(), ['id' => 'upn_id']);
    }
}
