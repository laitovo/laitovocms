<?php

namespace common\models\logistics;

use Yii;
use \common\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
/**
 * This is the model class for table "logistics_automatic_order".
 *
 * @property integer $id
 * @property integer $team_id
 * @property integer $measuringInterval
 * @property integer $unitOfPeriod
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property User $updater
 */
class AutomaticOrder extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_automatic_order';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('app','Идентификатор'),
            'team_id'           => Yii::t('app','Команда'),
            'measuringInterval' => Yii::t('app','Интервал измерения, ед. периода'),
            'unitOfPeriod'      => Yii::t('app','Единица измерения периода, сек'),
            'frequency'         => Yii::t('app','Мин. частота реализации, мес'),
            'fixRate'           => Yii::t('app','Корректирующий коэффициент скорости производства'),
            'created_at'        => Yii::t('app','Дата создания'),
            'author_id'         => Yii::t('app','Автор'),
            'updated_at'        => Yii::t('app','Дата изменения'),
            'updater_id'        => Yii::t('app','Редактор'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updater_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'team_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'team_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->team->id;
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }
}
