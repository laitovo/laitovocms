<?php

namespace common\models\logistics;

use Yii;
use yii\console\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use \common\models\User;

/**
 * This is the model class for table "logistics_order".
 *
 * @property integer $id
 * @property integer $team_id
 * @property integer $source_id
 * @property string $responsible_person
 * @property string $comment
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 * @property integer $shipment_date
 * @property integer $remoteStorage
 * @property integer $laitovo_user_id
 * @property string $ozon_posting_number [Идентификатор отправления в OZON]
 * @property integer $export_username
 *
 * @property User $author
 * @property LogisticsSources $source
 * @property Team $team
 * @property User $updater
 * @property LogisticsOrderEntry[] $logisticsOrderEntries
 */
class Order extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_order';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('app','Номер'),
            'team_id'            => Yii::t('app','Команда'),
            'source_id'          => Yii::t('app','Источник'),
            'source_innumber'    => Yii::t('app','Внутренний номер заказа в источнике'),
            'responsible_person' => Yii::t('app','Ответственный'),
            'comment'            => Yii::t('app','Комментарий'),
            'created_at'         => Yii::t('app','Дата создания'),
            'author_id'          => Yii::t('app','Автор'),
            'updated_at'         => Yii::t('app','Дата изменения'),
            'updater_id'         => Yii::t('app','Редактор'),
            'export_username'    => Yii::t('app','Контрагент (экспорт)'),
            'export_type'        => Yii::t('app','Категория контрагента (экспорт)'),
            'remoteStorage'      => Yii::t('app','Удаленный склад'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updater_id',
                ],
                'value' => function ($event) {
                    if (Yii::$app->id=='app-console')
                        return null;
                    return Yii::$app->user->getId();
                },
            ],
        ];
    }

    //custom bad function - создана только для консольного поиска и в одной функции
    public static function findConsole($team)
    {
        if (!is_int($team))
            throw new Exception('Не верно передан идентификаор команды');

            return self::find()->where(['team_id' => $team]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Sources::className(), ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderEntries()
    {
        return $this->hasMany(OrderEntry::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotDeletedOrderEntries()
    {
        return $this->hasMany(OrderEntry::className(), ['order_id' => 'id'])->where('is_deleted is NULL');
    }
}
