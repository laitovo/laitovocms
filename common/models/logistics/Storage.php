<?php

namespace common\models\logistics;

use Yii;
use yii\helpers\ArrayHelper;
use \common\models\team\Team;
use \common\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use common\models\laitovo\ErpProductType;

/**
 * This is the model class for table "logistics_storage".
 *
 * @property integer $id
 * @property integer $team_id
 * @property string $title
 * @property integer $type
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 * @property string $literal
 *
 * @property User $author
 * @property Team $team
 * @property User $updater
 * @property string $typeLiteral
 */
class Storage extends \common\models\LogActiveRecord
{
    //Типы
    const TYPE_REVERSE = 1;
    const TYPE_TIME    = 2;
    const TYPE_SUMP    = 3;
    const TYPE_TRANSIT = 4;

    //Логики
    const LOGIC_IN_SERIES = 1;
    const LOGIC_DISTRIBUTED = 2;
    const LOGIC_ORDERED = 3;

    public $logicList = [
        '' => '',
        self::LOGIC_IN_SERIES => 'Заполнять литеру последовательно',
        self::LOGIC_DISTRIBUTED => 'Искать наименее полную литеру',
        self::LOGIC_ORDERED => 'Распределять по заказам',
    ];

    public $types = [
        '' => '',
        self::TYPE_REVERSE => 'Оборотный',
        self::TYPE_TIME => 'Накопительный',
        self::TYPE_SUMP => 'До востребования',
        self::TYPE_TRANSIT => 'Транзитный',
    ];

    public $typeLiterals = [
        '' => '',
        self::TYPE_REVERSE => 'T',
        self::TYPE_TIME => 'A',
        self::TYPE_SUMP => 'U',
        self::TYPE_TRANSIT => 'P',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_storage';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         =>  Yii::t('app','Идентификатор'),
            'team_id'    =>  Yii::t('app','Команда'),
            'title'      =>  Yii::t('app','Наименование'),
            'type'       =>  Yii::t('app','Тип'),
            'literal'    =>  Yii::t('app','Литера склада'),
            'logic'      =>  Yii::t('app','Логика пополнения'),

            'created_at' =>  Yii::t('app','Дата создания'),
            'author_id'  =>  Yii::t('app','Автор'),
            'updated_at' =>  Yii::t('app','Дата изменения'),
            'updater_id' =>  Yii::t('app','Редактор'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updater_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'team_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'team_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->team->id;
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.team_id' => Yii::$app->team->id]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * Продукты для хранения на складе
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(ErpProductType::className(), ['id' => 'product_id'])->viaTable('logistics_storage_product', ['storage_id' => 'id']);
    }


    /**
     * Продукты, которые НЕ хранятся на складе
     * @return \yii\db\ActiveQuery
     */
    public function getNoProducts()
    {
        $productsOnStorage = ArrayHelper::map($this->getProducts()->all(), 'id', 'id');
        return ErpProductType::find()->where(['not in','id',$productsOnStorage]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiterals()
    {
        return $this->hasMany(StorageLiteral::className(), ['storage_id' => 'id']);
    }

    /**
     * Получить тип литеры для склаа
     *
     * @return mixed
     */
    public function getTypeLiteral()
    {
        return $this->typeLiterals[$this->type];
    }

}
