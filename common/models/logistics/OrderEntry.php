<?php

namespace common\models\logistics;

use core\models\box\Box;
use core\models\shipment\Shipment;
use common\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use Yii;

/**
 * This is the model class for table "logistics_order_entry".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $title
 * @property string $article
 * @property string $car [Наименование автомобиля]
 * @property integer $quantity
 * @property string $comment
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property boolean $withoutLabels
 * @property boolean $halfStake
 * @property boolean $is_deleted [Позиция удалена из заказа]
 * @property boolean is_custom [Нестандартная позиция]
 *
 * @property User $author
 * @property Order $order
 * @property User $updater
 * @property Shipment $shipment
 * @property Naryad $upn
 */
class OrderEntry extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_order_entry';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app','Идентификатор'),
            'order_id'   => Yii::t('app','Заявка'),
            'title'      => Yii::t('app','Наименование'),
            'article'    => Yii::t('app','Артикул'),
            'quantity'   => Yii::t('app','Количество'),
            'comment'    => Yii::t('app','Комментарий'),
            'created_at' => Yii::t('app','Дата создания'),
            'author_id'  => Yii::t('app','Автор'),
            'updated_at' => Yii::t('app','Дата изменения'),
            'updater_id' => Yii::t('app','Редактор'),
            'boxId' => Yii::t('app','Коробка'),

            'withoutLabels' => Yii::t('app', 'Без лейбы'),
            'halfStake' => Yii::t('app', '0.5 ставки'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updater_id',
                ],
                'value' => function ($event) {
                    if (Yii::$app->id=='app-console')
                        return null;
                    return Yii::$app->user->getId();
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpn()
    {
        return $this->hasOne(Naryad::className(), ['reserved_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBox()
    {
        return $this->hasOne(Box::className(), ['id' => 'boxId']);
    }

    public function getShipment()
    {
        return $this->hasOne(Shipment::class, ['id' => 'shipment_id']);
    }
}
