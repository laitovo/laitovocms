<?php

namespace common\models\logistics;

use Yii;
use \common\models\User;
use \common\models\logistics\Storage;
use \common\models\logistics\Order;
use \common\models\logistics\OrderEntry;
use \common\models\logistics\Naryad;

/**
 * This is the model class for table "logistics_storage_log".
 *
 * @property integer $id
 * @property integer $storage_id
 * @property string $literal
 * @property integer $upn_id
 * @property integer $reserved
 * @property integer $order_id
 * @property string $action
 * @property string $comment
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property LogisticsOrder $order
 * @property LogisticsStorage $storage
 * @property User $updater
 * @property LogisticsNaryad $upn
 */
class StorageLog extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_storage_log';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app','Идентификатор'),
            'storage_id' => Yii::t('app','Место хранения'),
            'literal'    => Yii::t('app','Литера'),
            'upn_id'     => Yii::t('app','UPN'),
            'reserved'   => Yii::t('app','Зарезервирован'),
            'reserved_id'   => Yii::t('app','Артикул'),
            'action'     => Yii::t('app','Действие'),
            'comment'    => Yii::t('app','Комментарий'),

            'created_at' => Yii::t('app','Дата создания'),
            'author_id'  => Yii::t('app','Автор'),
            'updated_at' => Yii::t('app','Дата изменения'),
            'updater_id' => Yii::t('app','Редактор'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderEntry()
    {
        return $this->hasOne(OrderEntry::className(), ['id' => 'reserved_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpn()
    {
        return $this->hasOne(Naryad::className(), ['id' => 'upn_id']);
    }
}
