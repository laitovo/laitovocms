<?php

namespace common\models\website;

use Yii;

/**
 * This is the model class for table "{{%feed_lang}}".
 */
class FeedLang extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feed_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'feed_id' => Yii::t('app', 'Лента'),
            'lang' => Yii::t('app', 'Язык'),
            'name' => Yii::t('app', 'Название'),
            'image' => Yii::t('app', 'Изображение'),
            'anons' => Yii::t('app', 'Анонс'),
            'content' => Yii::t('app', 'Контент'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeed()
    {
        return $this->hasOne(Feed::className(), ['id' => 'feed_id']);
    }
}
