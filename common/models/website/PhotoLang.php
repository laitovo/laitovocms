<?php

namespace common\models\website;

use Yii;

/**
 * This is the model class for table "{{%photo_lang}}".
 */
class PhotoLang extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'photo_id' => Yii::t('app', 'Фото/видео'),
            'lang' => Yii::t('app', 'Язык'),
            'name' => Yii::t('app', 'Название'),
            'photo' => Yii::t('app', 'Фото'),
            'video' => Yii::t('app', 'Видео'),
            'html' => Yii::t('app', 'HTML'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto0()
    {
        return $this->hasOne(Photo::className(), ['id' => 'photo_id']);
    }
}
