<?php

namespace common\models\website;

use Yii;

/**
 * This is the model class for table "{{%htmlblock_lang}}".
 */
class HtmlblockLang extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%htmlblock_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'htmlblock_id' => Yii::t('app', 'Html Блок'),
            'lang' => Yii::t('app', 'Язык'),
            'name' => Yii::t('app', 'Название'),
            'html' => Yii::t('app', 'HTML'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHtmlblock()
    {
        return $this->hasOne(Htmlblock::className(), ['id' => 'htmlblock_id']);
    }
}
