<?php

namespace common\models\website;

use Yii;

/**
 * This is the model class for table "{{%banner_lang}}".
 */
class BannerLang extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'banner_id' => Yii::t('app', 'Баннер'),
            'lang' => Yii::t('app', 'Язык'),
            'name' => Yii::t('app', 'Название'),
            'link' => Yii::t('app', 'Ссылка'),
            'image' => Yii::t('app', 'Изображение'),
            'html' => Yii::t('app', 'HTML'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }
}
