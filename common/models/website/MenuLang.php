<?php

namespace common\models\website;

use Yii;

/**
 * This is the model class for table "{{%menu_lang}}".
 */
class MenuLang extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menu_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => Yii::t('app', 'Меню'),
            'lang' => Yii::t('app', 'Язык'),
            'name' => Yii::t('app', 'Название'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
}
