<?php

namespace common\models\website;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use common\models\user\User;

/**
 * This is the model class for table "{{%htmlblock}}".
 */
class Htmlblock extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%htmlblock}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'website_id' => Yii::t('app', 'Сайт'),
            'type' => Yii::t('app', 'Тип блока'),
            'status' => Yii::t('app', 'Активен'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'JSON'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite()
    {
        return $this->hasOne(Website::className(), ['id' => 'website_id']);
    }

    public function getLangs()
    {
        return $this->hasMany(HtmlblockLang::className(), ['htmlblock_id' => 'id']);
    }
    
    /**
     * Получение HTML блока по его типу
     * @param string $type - тип блока
     * @param string $lang - код языка. Если не задан, то используется текущий
     * @param integer $siteId - идентификатор сайта. Если не задан, то используется текущий
     */
    public static function getBlockHtml($type, $lang = null, $siteId = null) {
        if (is_null($lang)) {
            $lang = \Yii::$app->language;
        }
        if (is_null($siteId)) {
            $siteId = \Yii::$app->urlManager->webSite->id;
        }
        $block = self::find()->where([
            'status' => 1,
            'type' => $type,
            'website_id' => $siteId,
        ])->one();
        if (is_null($block)) {
            \Yii::error("Блок с типом \"{$type}\" не найден");
            return '';
        }
        $langBlock = $block
                ->getLangs()
                ->andWhere(['lang' => $lang])
                ->one();
        return $langBlock->html;
    }
}
