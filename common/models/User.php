<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * Общая модель пользователя
 *
 * @property string $password Пароль пользователя. This property is write-only.
 * @property bool $is_root
 * @property string $name
 *
 * @author basyan <basyan@yandex.ru>
 */
class User extends \common\models\user\User implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;


    /**
     * @ignore
     */
    public function rules()
    {
        return [
            ['website_id', 'default', 'value' => Yii::$app->params['website_id']],
            ['is_root', 'default', 'value' => 0],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    public function fields()
    {
        return ['id', 'auth_key', 'name'];
    }
    
    /**
     * @ignore
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE, 'website_id'=>Yii::$app->params['website_id']]);
    }

    /**
     * Найти пользователя по токену
     *
     * @todo будет позже
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token, 'status' => self::STATUS_ACTIVE, 'website_id'=>Yii::$app->params['website_id']]);
    }

    /**
     * Найти пользователя по логину
     *
     * @param string $username Логин пользователя
     * @return static|null Пользователь
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE, 'website_id'=>Yii::$app->params['website_id']]);
    }

    /**
     * Найти пользователя по email
     *
     * @param string $email Email пользователя
     * @return static|null Пользователь
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE, 'website_id'=>Yii::$app->params['website_id']]);
    }

    /**
     * Найти пользователя по телефону
     *
     * @param string $phone Телефон пользователя
     * @return static|null Пользователь
     */
    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => $phone, 'status' => self::STATUS_ACTIVE, 'website_id'=>Yii::$app->params['website_id']]);
    }

    /**
     * Найти пользователя по токену восстановления пароля
     *
     * @param string $token Токен восстановления пароля
     * @return static|null Пользователь
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
            'website_id'=>Yii::$app->params['website_id'],
        ]);
    }

    /**
     * Валидация токена восстановления пароля
     *
     * @param string $token Токен восстановления пароля
     * @return boolean true|false
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @ignore
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @ignore
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @ignore
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Валидация пароля
     *
     * @param string $password Пароль пользователя
     * @return boolean true|false
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Генерация хеша пароля
     *
     * @param string $password Пароль пользователя
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Генерация нового password_reset_token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Удаления токена password_reset_token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function isRoot()
    {
        return $this->is_root;
    }

    public function getName()
    {
        return $this->name;
    }
}
