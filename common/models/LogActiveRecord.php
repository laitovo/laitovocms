<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\models;

use Yii;
use yii\helpers\Json;

/**
 * Чтобы писать логи в `{{%log}}` необходимо унаследовать класс объекта ActiveRecord от [[LogActiveRecord]]
 *
 * @author basyan <basyan@yandex.ru>
 */
class LogActiveRecord extends \yii\db\ActiveRecord
{
    private $_startTime;


    /**
     * @ignore
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->_startTime = microtime(true);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @ignore
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->_startTime = microtime(true);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @ignore
     */
	public function afterSave($insert, $changedAttributes)
	{
        if (Yii::$app->id=='app-console')
            return parent::afterSave($insert, $changedAttributes);

//        if ($insert)
//            $this->log('INSERT');
//        else
//            $this->log('UPDATE');
        return parent::afterSave($insert, $changedAttributes);
	}

    /**
     * @ignore
     */
	public function afterDelete()
	{
        if (Yii::$app->id=='app-console')
            return parent::afterDelete();

        $this->log('DELETE');
        return parent::afterDelete();
	}

    private function log($operator='')
    {
        $textlog=[];
        foreach ($this as $key => $value) {
            $textlog[]=[$key=>$value];
        };
        $log = new Log();
        $log->table=$this->tableName();
        $log->model=get_class($this);
        $log->field_id=isset($this->id) ? $this->id : null;
        $log->log=Json::encode($textlog);
        $log->time = microtime(true) - $this->_startTime;
        $log->operator = $operator;
        $log->save();
        return;
    }

    public function json($field='')
    {
        $json=Json::decode($this->json ? : '{}');
        return (isset($json[$field])) ? $json[$field] : null;
    }

    public function setjson($field,$value='')
    {
        $json=Json::decode($this->json ? : '{}');
        $json[$field]=$value;
        $this->json=Json::encode($json);
        return $this->save();
    }

    public function getFields($salt='')
    {
        $json=Json::decode(Yii::$app->team->team->json ? Yii::$app->team->team->json : '{}');
        return (isset($json['formbuilder']) && isset($json['formbuilder'][Yii::$app->controller->module->id]) && isset($json['formbuilder'][Yii::$app->controller->module->id][$salt.$this->tableName()])) ? $json['formbuilder'][Yii::$app->controller->module->id][$salt.$this->tableName()]['target'] : [];
    }

    public function getFieldsrender($salt='')
    {
        $json=Json::decode(Yii::$app->team->team->json ? Yii::$app->team->team->json : '{}');
        return (isset($json['formbuilder']) && isset($json['formbuilder'][Yii::$app->controller->module->id]) && isset($json['formbuilder'][Yii::$app->controller->module->id][$salt.$this->tableName()])) ? $json['formbuilder'][Yii::$app->controller->module->id][$salt.$this->tableName()]['render'] : '';
    }
}
