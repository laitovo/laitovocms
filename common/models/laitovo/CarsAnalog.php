<?php

namespace common\models\laitovo;

use Yii;

/**
 * This is the model class for table "{{%laitovo_cars_analog}}".
 */
class CarsAnalog extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_cars_analog}}';
    }

    public function getAnalog()
    {
        return $this->hasOne(Cars::className(), ['id' => 'analog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }

}
