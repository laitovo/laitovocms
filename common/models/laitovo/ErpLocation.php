<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use common\models\user\User;
use yii\db\ActiveRecord;
use common\models\laitovo\MaterialLocation;
use common\models\laitovo\MaterialParameters;
use common\models\laitovo\JobProductionScheme;
use yii\helpers\ArrayHelper;

/**
 * Class ErpLocation
 * @package common\models\laitovo
 *
 * @property string $name Наименование участка
 */
class ErpLocation extends \common\models\LogActiveRecord
{
    
    public $materials;
    public $accounting_unit;
    public $accounting_unit_price;
    public $unit_settlement;
    public $coefficient_settlement;
    public $price_settlement;
    //Типы участков организации
    const TYPE_PRODUCTION='Производственный';
    const TYPE_SUPPROT='Вспомогательный';
    
    public $types=[
        ''=>'',
        self::TYPE_PRODUCTION=>self::TYPE_PRODUCTION,
        self::TYPE_SUPPROT=>self::TYPE_SUPPROT,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_location}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'barcode',
                ],
                'value' => function ($event) {
                    return uniqid();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'barcode' => Yii::t('app', 'Штрихкод'),
            'name' => Yii::t('app', 'Название'),
            'status' => Yii::t('app', 'Статус'),
            'sort' => Yii::t('app', 'Сортировка'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'type' => Yii::t('app', 'Тип участка'),
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNaryads()
    {
        return $this->hasMany(ErpNaryad::className(), ['location_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNaryadstostart()
    {
        return $this->hasMany(ErpNaryad::className(), ['start' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(ErpUser::className(), ['location_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersRate()
    {
        return $this->hasMany(\backend\modules\laitovo\models\ErpJobLocationRate::className(), ['location_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkplaces()
    {
        return $this->hasMany(ErpLocationWorkplace::className(), ['location_id' => 'id']);
    }

    public function getMaterialLocation()
    {
        return $this->hasMany(MaterialLocation::className(), ['laitovo_erp_location_id' => 'id']);
    }
    public function getMaterials()
    {
        return $this->hasMany(MaterialParameters::className(), ['id' => 'material_parameters_id'])->viaTable('material_location', ['laitovo_erp_location_id' => 'id']);
    }
    public function getJobProductionScheme()
    {
        return $this->hasMany(JobProductionScheme::className(), ['location_id' => 'id']);
    }

    //My intarface
    public function isProduction()
    {
        if ($this->type == self::TYPE_PRODUCTION)
            return true;
        return false;
    }

    public function isSupport()
    {
        if ($this->type == self::TYPE_SUPPROT)
            return true;
        return false;
    }

    //My static interface
    public static function getAllSupport()
    {
        return self::find()->where(['type' => self::TYPE_SUPPROT])->all();
    }

    public static function getAllSupportIdsAsArray()
    {
        $supportLocationsIds = [];
        $supportLocations = self::getAllSupport();
        if ($supportLocations)
            return ArrayHelper::map($supportLocations,'id','id');
        return [];
    }

    public static function getAllProduction()
    {
        return self::find()->where(['type' => self::TYPE_PRODUCTION])->all();
    }

    public static function getAllProductionIdsAsArray()
    {
        $supportLocations = self::getAllProduction();
        if ($supportLocations)
            return ArrayHelper::map($supportLocations,'id','id');
        return [];
    }

    public function recieveActiveWorkplacesProperties($userId)
    {
        if (!in_array($this->id,[18, Yii::$app->params['erp_shveika'], Yii::$app->params['erp_otk'], Yii::$app->params['erp_clipsi'] ]) || $userId == null) return '';

        $activeWorkplaces = ErpLocationWorkplace::find()
            ->select('property')
            ->where(['and',
                ['location_id' => $this->id],
                ['user_id' => $userId],
                ['activity' => 1],
                ['is not', 'user_id', NULL],
                ['is not', 'property', NULL],
            ])
            ->distinct()
            ->column();

        return empty($activeWorkplaces) ? '' : implode(', ',$activeWorkplaces);
    }
}
