<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\user\User;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%laitovo_cars_act}}".
 */
class CarsAct extends \common\models\LogActiveRecord
{
    const TYPE_DRAFT = 'Черновик';
    const TYPE_CHANGES = 'Изменения';
    const TYPE_MASTERING = 'Освоение';

    public $types = [
        self::TYPE_DRAFT => self::TYPE_DRAFT,
        self::TYPE_CHANGES => self::TYPE_CHANGES,
        self::TYPE_MASTERING => self::TYPE_MASTERING,
    ];

    const ELEMENT_FW = 'fw';
    const ELEMENT_FV = 'fv';
    const ELEMENT_FD = 'fd';
    const ELEMENT_RD = 'rd';
    const ELEMENT_RV = 'rv';
    const ELEMENT_BW = 'bw';

    public $elements = [
        self::ELEMENT_FW => self::ELEMENT_FW,
        self::ELEMENT_FV => self::ELEMENT_FV,
        self::ELEMENT_FD => self::ELEMENT_FD,
        self::ELEMENT_RD => self::ELEMENT_RD,
        self::ELEMENT_RV => self::ELEMENT_RV,
        self::ELEMENT_BW => self::ELEMENT_BW,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_cars_act}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'car_id' => Yii::t('app', 'Автомобиль'),
            'created_at' => Yii::t('app', 'Дата освоения'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Ответственный'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'Json'),
            'element' => Yii::t('app', 'Элемент'),
            'type' => Yii::t('app', 'Тип'),
            'checked' => Yii::t('app', 'Одобрен'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    //Функция вычисления различий
    //Возвращаемое значение массив различающихся значений с учетом последних
    public function getDifference()
    {
        //Вычисялем расходения ключей
        $result = [];
        $json = Json::decode($this->json);
        $array1 = isset($json['before']) ? $json['before'] : [];
        $array2 = isset($json['after']) ? $json['after'] : [];

        foreach ($array2 as $key => $value) {
            if (!is_array($value))
                if ((array_key_exists($key, $array1) && $array1[$key] != $value) || !array_key_exists($key, $array1))
                    $result[$key] = $value;
        }

        foreach ($array2['auto'] as $key => $value) {
            if (!in_array($key, ['json','author_id','checked','checked_author','checked_date','mastered','mounting','status','sync_at','updated_at','updater_id','sort']))
                if ((array_key_exists($key, $array1['auto']) && $array1['auto'][$key] != $value) || !array_key_exists($key, $array1['auto']))
                    $result['auto'][$key] = $value;
        }

        foreach ($array2['fields'] as $key => $value) {
            if ((array_key_exists($key, $array1['fields']) && $array1['fields'][$key] != $value) || !array_key_exists($key, $array1['fields']))
                $result['fields'][$key] = $value;
        }

        //обратная дополнительная проверка
        foreach ($array1 as $key => $value) {
            if (!is_array($value))
                if (!array_key_exists($key, $array2))
                    $result[$key] = $value;
        }

        foreach ($array1['auto'] as $key => $value) {
            if (!in_array($key, ['json','author_id','checked','checked_author','checked_date','mastered','mounting','status','sync_at','updated_at','updater_id','sort']))
                if (!array_key_exists($key, $array2['auto']))
                    $result['auto'][$key] = $value;
        }

        foreach ($array1['fields'] as $key => $value) {
            if (!array_key_exists($key, $array2['fields']))
                $result['fields'][$key] = $value;
        }

        return $result;
    }

    /*################################################################*/
    //Статическая функция для получаения количества всех актов освоения проверенных
    public static function receiveMasteringActCheckedQuery()
    {
        return self::find()->where(['is not','checked',null])->andWhere(['type'=>self::TYPE_MASTERING]);
    }

    //Статическая функция для получаения количества всех актов внесения изменений проверенных
    public static function receiveChangesActCheckedQuery()
    {
        return self::find()->where(['is not','checked',null])->andWhere(['type'=>self::TYPE_CHANGES]);
    }

    //Статическая функция для получаения количества всех актов освоения на проверку
    public static function receiveMasteringActForCheckCount()
    {
        return self::find()->where(['checked' => null])->andWhere(['type'=>self::TYPE_MASTERING])->count();
    }

    //Статическая функция для получаения всех актов освоения на проверку
    public static function receiveMasteringActForCheckQuery()
    {
        return self::find()->where(['checked' => null])->andWhere(['type'=>self::TYPE_MASTERING]);
    }

    //Статическая функция для получаения всех актов освоения на проверку
    public static function receiveChangesActForCheckCount()
    {
        return self::find()->where(['checked' => null])->andWhere(['type'=>self::TYPE_CHANGES])->count();
    }

    //Статическая функция для получаения всех актов освоения на проверку
    public static function receiveChangesActForReworkCount()
    {
        return self::find()->where(['checked' => 2])->andWhere(['type'=>self::TYPE_CHANGES])->count();
    }

    //Статическая функция для получаения количества всех актов освоения на проверку
    public static function receiveMasteringActForReworkCount()
    {
        return self::find()->where(['checked' => 2])->andWhere(['type'=>self::TYPE_MASTERING])->count();
    }

    //Статическая функция для получаения всех актов освоения на проверку
    public static function receiveChangesActForReworkQuery()
    {
        return self::find()->where(['checked' => 2])->andWhere(['type'=>self::TYPE_CHANGES]);
    }

    //Статическая функция для получаения количества всех актов освоения на проверку
    public static function receiveMasteringActForReworkQuery()
    {
        return self::find()->where(['checked' => 2])->andWhere(['type'=>self::TYPE_MASTERING]);
    }
    
    
    //Статическая функция для получаения всех актов освоения на проверку
    public static function receiveChangesActForCheckQuery()
    {
        return self::find()->where(['checked' => null])->andWhere(['type'=>self::TYPE_CHANGES]);
    }
}
