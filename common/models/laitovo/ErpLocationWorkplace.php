<?php

namespace common\models\laitovo;

use Yii;

/**
 * This is the model class for table "laitovo_erp_location_workplace".
 *
 * @property integer $id
 * @property integer $location_id
 * @property integer $location_number
 * @property string $type
 * @property integer $user_id
 *
 * @property LaitovoErpLocation $location
 * @property LaitovoErpUser $user
 */
class ErpLocationWorkplace extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_location_workplace';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'location_id' => 'Участок',
            'location_number' => 'Идентификатор внутри участка',
            'type_id' => 'Тип рабочего места',
            'property' => 'Свойство',
            'activity' => 'Активность',
            'last_activity' => 'Последняя активность',
            'user_id' => 'Сотрудник',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ErpLocationWorkplaceType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(ErpLocationWorkplacePlan::className(), ['workplace_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastPlan()
    {
        $plan = ErpLocationWorkplacePlan::find()
            ->where(['workplace_id' => $this->id])
            ->orderBy('time_to desc')
            ->one();
        return $plan ? $plan : null;
    }

    public function getProperties()
    {
        switch ($this->location_id) {
            case Yii::$app->params['erp_okleika']:
                return [
                    'Все ткани' => 'Все ткани',
                    '1' => '1 - Москитка',
                    '2' => '2 - Лайтово - темная',
                    '3' => '3 - Бифлекс',
                    '4' => '4 - 1,5 - Лайтово - medium',
                    '5C' => '5C - 5 - Chiko - светлая (Задние шторки)',
                    '5T' => '5T - 5 - Chiko - тёмная (Все, кроме задних шторок)',
                    '8' => '8 - 1.25 - Лайтово - светлая',
                ] ;
            case Yii::$app->params['erp_shveika']:
                return [
                    'Сточка резинки' => 'Сточка резинки',
                    'Пошив без сточки' => 'Пошив без сточки',
                    'Пошив' => 'Пошив',
                ] ;
            case Yii::$app->params['erp_clipsi']:
                return [
                    'Клипсы' => 'Клипсы',
                    'Магниты' => 'Магниты',
                    'К+М' => 'К+М',
                ] ;
            case Yii::$app->params['erp_otk']:
                return [
                    'Автошторки' => 'Автошторки',
                    'Органайзеры' => 'Органайзеры',
                    'Автоодеяло' => 'Автоодеяло',
                ] ;
            case 18:
                return [
                    'ЧехолНаСпинку' => 'Чехол на спинку',
                    'Органайзеры' => 'Органайзеры',
                    'Фильтры' => 'Фильтры',
                    'Автоодеяло' => 'Автоодеяло',
                ] ;
            default:
                return [];
        }
    }


}
