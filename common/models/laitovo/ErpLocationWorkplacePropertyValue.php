<?php

namespace common\models\laitovo;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "laitovo_erp_location_workplace_property_value".
 *
 * @property integer $id
 * @property integer $workplace_id
 * @property integer $property_id
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property User $author
 * @property LaitovoErpLocationWorkplaceProperty $property
 * @property User $updater
 * @property LaitovoErpLocationWorkplace $workplace
 */
class ErpLocationWorkplacePropertyValue extends \common\models\LogActiveRecord
{
    const PROP_TKAN_NUMBER = 'Номер ткани';


    public $properties = [
        self::PROP_TKAN_NUMBER => self:PROP_TKAN_NUMBER;
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_location_workplace_property_value';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'workplace_id' => 'Рабочее место',
            'property_id' => 'Свойство',
            'value' => 'Значение',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'author_id' => 'Автор',
            'updater_id' => 'Изменил',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updater_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkplace()
    {
        return $this->hasOne(ErpLocationWorkplace::className(), ['id' => 'workplace_id']);
    }
}
