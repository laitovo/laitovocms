<?php

namespace common\models\laitovo;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "laitovo_erp_naryad_log".
 *
 * @property integer $id
 * @property integer $workOrderId
 * @property integer $date
 * @property string $locationFromName
 * @property integer $locationFromId
 * @property string $locationToName
 * @property integer $locationToId
 * @property string $workerName
 * @property integer $workerId
 */
class ErpNaryadLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_naryad_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workOrderId', 'date', 'locationFromId', 'locationToId', 'workerId'], 'integer'],
            [['locationFromName', 'locationToName', 'workerName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('app', 'ID'),
            'workOrderId'      => Yii::t('app', 'ID наряда'),
            'date'             => Yii::t('app', 'Дата'),
            'locationFromName' => Yii::t('app', 'Наименование локации откуда'),
            'locationFromId'   => Yii::t('app', 'ID локации откуда'),
            'locationToName'   => Yii::t('app', 'Наименование локации куда'),
            'locationToId'     => Yii::t('app', 'ID локации куда'),
            'workerName'       => Yii::t('app', 'Наименование сотрудника'),
            'workerId'         => Yii::t('app', 'ID сотрудника'),
        ];
    }

    /**
     * Функция для логирования наряда по переданным атрибутам
     * @param $attributes
     * @return bool
     */
    public static function log($attributes) {
        if (!empty($attributes)) {
            $log = new self();
            $log->setAttributes($attributes);
            $status = $log->save();
            return $status;
        }
        return false;
    }

    /**
     * Функция для получения логов по идентификаторов пользователей, которые принимали участие в производстве наряда
     * @param $workOrderId integer
     * @return array
     */
    public static function getUsersIds($workOrderId) {
        $result = [];
        //Если нечего или не почему показать то пустой массив отдаем
        if (empty($workOrderId)) return $result;

        //Идентификатор локации диспетчера
        $dispatcher = Yii::$app->params['erp_dispatcher'];

        $logs = self::find()
            ->where(['workOrderId' => $workOrderId])
            ->andWhere(['is not','workOrderId',null])
            ->andWhere(['is not','locationFromId',null])
            ->andWhere(['!=','locationFromId',$dispatcher])
            ->andWhere(['or',['locationToId' => $dispatcher],['locationToId' => null]])
            ->all();

        if (!$logs) return $result;

        $result = ArrayHelper::map($logs,'workerId','workerId');

        return $result;
    }
}
