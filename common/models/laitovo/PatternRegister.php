<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 09.10.17
 * Time: 10:26
 */

namespace common\models\laitovo;

use common\models\LogActiveRecord;

class PatternRegister extends LogActiveRecord
{
    const FD = 'ПБ';
    const FV = 'ПФ';
    const RV = 'ЗФ';
    const RD = 'ЗБ';
    const FW = 'ПШ';
    const BW = 'ЗШ';

    const BK = 'Бескаркасный';
    const SD = 'Сдвижной';

    public static function tableName()
    {
        return 'laitovo_erp_pattern_register';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id'], 'required'],
        ];
    }

    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' =>'car_id']);
    }

    public static function getFdBk($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::BK],
                ['window_type' => self::FD]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getFdSd($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::SD],
                ['window_type' => self::FD]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }


    public static function getFvBk($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::BK],
                ['window_type' => self::FV]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getFvSd($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::SD],
                ['window_type' => self::FV]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getRvBk($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::BK],
                ['window_type' => self::RV]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getRvSd($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::SD],
                ['window_type' => self::RV]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getRdBk($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::BK],
                ['window_type' => self::RD]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getRdSd($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::SD],
                ['window_type' => self::RD]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getFwBk($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::BK],
                ['window_type' => self::FW]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getFwSd($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::SD],
                ['window_type' => self::FW]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getBwBk($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::BK],
                ['window_type' => self::BW]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }

    public static function getBwSd($car_id)
    {
        $result = self::find()->where(['and',
                ['car_id' => $car_id],
                ['product_type' => self::SD],
                ['window_type' => self::BW]
            ])->one();
        if($result){
            return $result->pattern_article;
        }
    }
}