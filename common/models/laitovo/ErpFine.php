<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 19.09.17
 * Time: 10:54
 */

namespace common\models\laitovo;

use common\models\LogActiveRecord;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use Yii;

class ErpFine extends LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_fine';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'integer'],
            ['amount', 'double'],
            ['description', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Сотрудник',
            'description' => 'Причина штрафа',
            'amount' => 'Размер штрафа',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],

                ],
            ],
        ];
    }



    public function getUser()
    {
        return $this->hasOne(ErpUser::className(),['id'=>'user_id']);
    }

    public function beforeDelete()
    {
        if (!in_array(Yii::$app->user->getId(),[2,21]))
            return false;
        return parent::beforeDelete();
    }

}