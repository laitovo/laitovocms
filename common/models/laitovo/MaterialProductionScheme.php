<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use common\models\user\User;

/**
 * This is the model class for table "material_production_scheme".
 *
 * @property integer $id
 * @property integer $production_scheme_id
 * @property integer $location_id
 * @property integer $material_parameter_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property User $author
 * @property LaitovoErpLocation $location
 * @property MaterialParameters $materialParameter
 * @property ProductionScheme $productionScheme
 * @property User $updater
 */
class MaterialProductionScheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_production_scheme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['production_scheme_id', 'location_id', 'material_parameter_id', 'created_at', 'updated_at', 'author_id', 'updater_id', ], 'integer'],
            [['material_count'], 'double'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['material_parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialParameters::className(), 'targetAttribute' => ['material_parameter_id' => 'id']],
            [['production_scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionScheme::className(), 'targetAttribute' => ['production_scheme_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'production_scheme_id' => 'Production Scheme ID',
            'location_id' => 'Location ID',
            'material_parameter_id' => 'Material Parameter ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
            'material_count' => 'Количество метериала',
        ];
    }
    
     public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialParameter()
    {
        return $this->hasOne(MaterialParameters::className(), ['id' => 'material_parameter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionScheme()
    {
        return $this->hasOne(ProductionScheme::className(), ['id' => 'production_scheme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }
}
