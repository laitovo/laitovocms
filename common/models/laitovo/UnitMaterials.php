<?php

namespace common\models\laitovo;

use Yii;


/**
 * This is the model class for table "unit_materials".
 *
 * @property integer $id
 * @property string $name
 *
 * @property UnitAccounting[] $unitAccountings
 * @property UnitSettlement[] $unitSettlements
 */
class UnitMaterials extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_materials';
    }
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'accounting_unit' => 'Учетная единица',
            'unit_settlement' => 'Расчетная единица',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitAccountings()
    {
        return $this->hasMany(UnitAccounting::className(), ['accounting_unit' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitSettlements()
    {
        return $this->hasMany(UnitSettlement::className(), ['unit_settlement' => 'id']);
    }
}
