<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\user\User;
use common\models\order\Order;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%laitovo_erp_order}}".
 *
 * @property string $status [ Статус наряда ]
 */
class ErpOrder extends \common\models\LogActiveRecord
{
    const STATUS_READY_TO_WORK = 'Готов к производству';
    const STATUS_IN_WORK       = 'На производстве';
    const STATUS_READY         = 'Готов к отгрузке';
    const STATUS_CHECKED       = 'Проверен';
    const STATUS_CANCEL        = 'Отменен';
    const STATUS_FINISH        = 'Завершен';
    const STATUS_IN_HANDLE     = 'В обработке';

    private $category;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_order}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    public $statuses=[
        ''=>'',
        self::STATUS_READY_TO_WORK=>self::STATUS_READY_TO_WORK,
        self::STATUS_IN_WORK=>self::STATUS_IN_WORK,
        self::STATUS_READY=>self::STATUS_READY,
        self::STATUS_CHECKED=>self::STATUS_CHECKED,
        self::STATUS_CANCEL=>self::STATUS_CANCEL,
        self::STATUS_FINISH=>self::STATUS_FINISH,
    ];

    public $prioritets = [
        7=>'Низкий',
        6=>'Обычный',
        //5=>'Комплектация',
        4=>'Срочный',
        3=>'Большой заказ',
        2=>'TopSpeed',
        //1=>'Срочный-Переделка',
    ];

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Заказ'),
            'status' => Yii::t('app', 'Статус'),
            'sort' => Yii::t('app', 'Приоритет'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNaryads()
    {
        return $this->hasMany(ErpNaryad::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    public function getName()
    {
        if ($this->order)
            return Yii::t('app', 'Заказ №{n}',['n'=>$this->order->number ?: $this->order->id]);
        return Yii::t('app', 'Заказ №{n}',['n'=>$this->id]);
    }

    public function getCategory()
    {
        $category = $this->json('category');
        return isset($category) ? $category : ''; 
    }
}
