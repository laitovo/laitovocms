<?php

namespace common\models\laitovo;

use Yii;
use backend\modules\laitovo\models\ErpJobScheme;

/**
 * This is the model class for table "job_production_scheme".
 *
 * @property integer $id
 * @property integer $production_scheme_id
 * @property integer $erp_job_scheme_id
 *
 * @property LaitovoErpJobScheme $erpJobScheme
 * @property ProductionScheme $productionScheme
 */
class JobProductionScheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_production_scheme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['production_scheme_id', 'erp_job_scheme_id'], 'integer'],
            [['erp_job_scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpJobScheme::className(), 'targetAttribute' => ['erp_job_scheme_id' => 'id']],
            [['production_scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionScheme::className(), 'targetAttribute' => ['production_scheme_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'production_scheme_id' => 'Production Scheme ID',
            'erp_job_scheme_id' => 'Erp Job Scheme ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpJobScheme()
    {
        return $this->hasOne(ErpJobScheme::className(), ['id' => 'erp_job_scheme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionScheme()
    {
        return $this->hasOne(ProductionScheme::className(), ['id' => 'production_scheme_id']);
    }
}
