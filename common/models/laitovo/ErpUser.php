<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use common\models\user\User;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%laitovo_erp_user}}".
 *
 * Class ErpUser
 * @package common\models\laitovo
 *
 * @property integer|null $location_id [ рабочий участок, на котором находится сейчас работник ]
 * @property boolean $canCreateTemplate [ Может создавать лекало (Швейка) ]
 * @property boolean $canCreateDontlook [ Может шить Don't look ]
 * @property boolean $otherPricing [ Применять другие расценки ]
 */
class ErpUser extends \common\models\LogActiveRecord
{
    //статусы работников
    const STATUS_WORK = 'Работает';
    const STATUS_DISMISSED = 'Уволен';
    const STATUS_HOLIDAY = 'Отпуск';

    //типы работников
    const TYPE_PRODUCTION = 'Производственная';
    const TYPE_SUPPORT = 'Вспомогательная';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_user}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'barcode',
                ],
                'value' => function ($event) {
                    return uniqid();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'barcode' => Yii::t('app', 'Штрихкод'),
            'name' => Yii::t('app', 'ФИО'),
            'location_id' => Yii::t('app', 'Участок'),
            'status' => Yii::t('app', 'Статус'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'factor' => Yii::t('app', 'Коэффициент'),
            'json' => Yii::t('app', 'Json'),
            'type' => Yii::t('app', 'Группа'),
            'dateTo' => Yii::t('app', 'Конец периода * (23:59:59)'),
            'dateFrom' => Yii::t('app', 'Начало периода *(00:00:00)'),
            'date_work' => Yii::t('app', 'Дата приема на работу'),
            'date_dismissed' => Yii::t('app', 'Дата увольнения'),
            'trainee' => Yii::t('app', 'Стажер'),
            'canCreateTemplate' => Yii::t('app', 'Может создавать лекало (Швейка)'),
            'canCreateDontlook' => Yii::t('app', 'Может шить Don\'t look'),
            'otherPricing' => Yii::t('app', 'Применять другие расценки'),
        ];
    }

    public $statuses=[
        self::STATUS_WORK=> self::STATUS_WORK,
        self::STATUS_DISMISSED=> self::STATUS_DISMISSED,
        self::STATUS_HOLIDAY=> self::STATUS_HOLIDAY,
    ];

    public $types=[
        self::TYPE_PRODUCTION=> self::TYPE_PRODUCTION,
        self::TYPE_SUPPORT=> self::TYPE_SUPPORT,
    ];

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    public function getNaryads()
    {
        return $this->hasMany(ErpNaryad::className(), ['user_id' => 'id']);
    }

    //рабочие наряды на пользователе
    public function recieveCountActiveNaryads()
    {
        return 
        ErpNaryad::find()
        ->where(['and',
            ['or',
                ['laitovo_erp_naryad.status' => ErpNaryad::STATUS_IN_PAUSE],
                ['laitovo_erp_naryad.status' => ErpNaryad::STATUS_IN_WORK],
            ],
            ['user_id' => $this->id],
            ['and', 
                ['!=','location_id',Yii::$app->params['erp_dispatcher']],
                ['IS NOT','location_id', NULL],
            ],
        ])->count();
    }

        //рабочие наряды на пользователе
    public function recieveCountDangerNaryads()
    {
        $ids = [];
        //ищем наряды, которые в статусе В РАБОТЕ ИЛИ НА ПАУЗЕ
        $naryads = ErpNaryad::find()
            ->where(['and',
                ['or',
                    ['status' => ErpNaryad::STATUS_IN_WORK],
                    ['status' => ErpNaryad::STATUS_IN_PAUSE],
                ],
                ['IS NOT','location_id',NULL],
                ['user_id' => $this->id],
            ])
            ->all();

        //получив весь список нарядов нам надо заглянуть в каждый в лог и посмотреть дату последнего движения
        if ($naryads)
            foreach ($naryads as $naryad) {
                if ($naryad->json('log') && count($naryad->json('log')))
                {
                    $key = count($naryad->json('log')) - 1;
                    $date = $naryad->json('log')[$key]['date']; //формат timestamp
                    if ($date && ($date < (time()-60*60*24)))
                    {
                        $ids[] = $naryad->id;
                    }
                }
            }
            
        //надо найти наряды, которые находяться в статусе на паузе или в работе и находятся на пользователе
        return ErpNaryad::find()
            ->where(['and',
                ['in','laitovo_erp_naryad.id',$ids],
            ])->count();
    }

    public function getWorkplaces()
    {
        return $this->hasMany(ErpLocationWorkplace::className(), ['user_id' => 'id']);
    }

    public function recieveWorkplacesByLocation($location_id)
    {
        $workplaces = ErpLocationWorkplace::find()
            ->where(['user_id' => $this->id])
            ->andWhere(['location_id' => $location_id])
            ->all();

         return $workplaces ? $workplaces : null;   
    }

    public function getFactor()
    {
        return $this->factor ? $this->factor : 1;
    }


    public function getCongestion()
    {
        $factor = $this->getFactor();
        return (self::getNaryads()->count() / $factor);
    }
    
    public function getActiveWorkplaces()
    {
        $workplaces = $this->getWorkplaces()->andWhere(['location_id'=>Yii::$app->params['erp_okleika'], 'activity'=>true])->all();
        $location_number = ArrayHelper::map($workplaces, 'id', 'location_number');
        if($workplaces)
        {
            return implode(', ', $location_number);
        }
        return "";
    }

    public function getWorkplacePlans()
    {
        return $this->hasMany(ErpLocationWorkplacePlan::className(), ['user_id' => 'id']);
    }

    public function getWorkplacePlansIdsAsArr()
    {
        $plans = $this->workplacePlans;
        if ($plans)
            return ArrayHelper::map($plans,'workplace_id','workplace_id');
        return [];
    }

    public static function getAllWorkers(){
        return
        self::find()
            ->alias('t')
            ->joinWith('location location')
            ->where(['t.status' => self::STATUS_WORK])
            ->andWhere(['location.type' => \backend\modules\laitovo\models\ErpLocation::TYPE_PRODUCTION])
            ->all();
    }

    public function getNameWithLocation()
    {
        return $this->name . ' - ' . (@$this->location ? @$this->location->name : '');
    }

    /**
     * @return mixed|string
     * добавляем приставку стажер к имени пользователя
     */
    public function getName()
    {
        return $this->trainee ? '"СТАЖЕР" ' . $this->name : $this->name;
    }

    /**
     * @param $name
     * @return string
     * добавляем приставку стажер там где нет ytkmpz dpznm id пользователя
     */
    public static function  getNameByName($name)
    {
        $name = trim($name);
        $user = ErpUser::findOne(['name' => $name]);
        if ($user && $user->trainee) {
            return '"СТАЖЕР" ' . $user->name;
        }
        return $name;
    }

}