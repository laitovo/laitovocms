<?php

namespace common\models\laitovo;

use Yii;

/**
 * This is the model class for table "material".
 *
 * @property integer $id
 * @property string $name
 *
 * @property MaterialParameters[] $materialParameters
 */
class Material extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Вид материала',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialParameters()
    {
        return $this->hasMany(MaterialParameters::className(), ['material_id' => 'id']);
    }
}
