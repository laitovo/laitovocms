<?php

namespace common\models\laitovo;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "laitovo_erp_position".
 *
 * @property integer $id
 * @property string $title
 *
 * @property ErpManning[] $laitovoErpMannings
 */
class ErpPosition extends \common\models\LogActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_position';
    }

    public function rules()
    {
        return [
            [['title'], 'unique'],
            [['salary_one', 'salary_two', 'car_compensation', 'surcharge', 'qualification', 'premium', 'kpi_one', 'kpi_two', 'kpi_three'], 'number'],
            [['division_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpDivision::className(), 'targetAttribute' => ['division_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название должности'),
            'division_id' => Yii::t('app', 'Отдел'),
            'salary_one' => Yii::t('app', 'Оклад-1'),
            'salary_two' => Yii::t('app', 'Оклад-2'),
            'car_compensation' => Yii::t('app', 'Компенсация а/м'),
            'surcharge' => Yii::t('app', 'Сезонная доплата'),
            'qualification' => Yii::t('app', 'Квалификация'),
            'premium' => Yii::t('app', 'Премия'),
            'kpi_one' => Yii::t('app', 'KPI-1'),
            'kpi_two' => Yii::t('app', 'KPI-2'),
            'kpi_three' => Yii::t('app', 'KPI-3'),
            'article' => Yii::t('app', 'Артикул'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpMannings()
    {
        return $this->hasMany(ErpManning::className(), ['position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivision()
    {
        return $this->hasOne(ErpDivision::className(), ['id' => 'division_id']);
    }


    public function getUserPosition()
    {
        return $this->hasOne(ErpUserPosition::className(), ['position_id' => 'id']);
    }


    public function getUser()
    {
        return $this->hasOne(ErpUser::className(),['id'=>'user_id'])->viaTable('laitovo_erp_user_position',['position_id'=>'id']);
    }

    static function position_list()
    {
        return ArrayHelper::map(self::find()->all(),'id','title');
    }

    static function position_list_not_user()
    {
        $user_position = ArrayHelper::map(ErpUserPosition::find()->all(),'position_id','position_id');
        if($user_position)
            return ArrayHelper::map(self::find()->where(['not in', 'id',$user_position])->all(),'id','title');
        return ArrayHelper::map(self::find()->all(),'id','title');
    }
}
