<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use common\models\User;

/**
 * This is the model class for table "laitovo_erp_material_report".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $location_id
 * @property integer $naryad_id
 * @property integer $material_parameter_id
 * @property integer $material_count
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property LaitovoErpLocation $location
 * @property MaterialParameters $materialParameter
 * @property LaitovoErpNaryad $naryad
 * @property LaitovoErpUser $user
 */
class ErpMaterialReport extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_material_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'location_id', 'naryad_id', 'material_parameter_id', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['material_count'],'double'],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['material_parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialParameters::className(), 'targetAttribute' => ['material_parameter_id' => 'id']],
            [['naryad_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpNaryad::className(), 'targetAttribute' => ['naryad_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }  

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Работник'),
            'location_id' => Yii::t('app', 'Участок'),
            'naryad_id' => Yii::t('app', 'Наряд'),
            'material_parameter_id' => Yii::t('app', 'Материал'),
            'material_count' => Yii::t('app', 'Количество материала'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialParameter()
    {
        return $this->hasOne(MaterialParameters::className(), ['id' => 'material_parameter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNaryad()
    {
        return $this->hasOne(ErpNaryad::className(), ['id' => 'naryad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }
  
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }
}
