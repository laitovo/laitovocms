<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/28/17
 * Time: 2:18 PM
 */

namespace common\models\laitovo;




use backend\modules\laitovo\models\DateAction;
use backend\modules\laitovo\models\ErpUsersReport;
use Yii;



class ErpReportCard extends \common\models\LogActiveRecord
{
    public $date;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_report_card}}';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'ФИО'),
            'month_year' => Yii::t('app', 'Месяц-Год'),
            'json' => Yii::t('app', 'Json'),
        ];
    }



    public function getArrayOfJson($json)
    {
        $resultArr = [
            'final_difference'=>[
                'label'=>'раб. час',
            ],
            'time_in'=>[
                'label'=>'приход',
            ],
            'time_out'=>[
                'label'=>'уход',
            ],
            'in_production'=>[
                'label'=>'сделка',
            ],
        ];

        foreach($json as $key=>$value)
        {
            if($key!='fact_sum' && $key!='month_rate')
            {
                foreach ($value as $title=>$data)
                {
                    $resultArr[$title][$key]=$data;
                }
            }else{
                $resultArr['final_difference'][$key] = $value;
            }
        }
        return $resultArr;
    }



    public function getArchiveReportCard($date, $division_id){
        $resultArr = [];
        $resultArr1 = [];
        $resultArr2 = [];

        $archive_list = self::find()->joinWith('position p')->where(['month_year'=>date('Y-m-d',strtotime($date)),'p.division_id'=>$division_id])->all();

        if($archive_list) {
            foreach ($archive_list as $archive) {
                $resultArr1[$archive->user_id]['user_id'] = $archive->user_id ? $archive->user_id : '';
                $resultArr1[$archive->user_id]['user_name'] = $archive->user_id ? $archive->user->name : '';
                $resultArr2[$archive->user_id] = $this->getArrayOfJson(json_decode($archive->json))['final_difference'];
                $resultArr[$archive->user_id] = array_merge($resultArr1[$archive->user_id],$resultArr2[$archive->user_id]);
            }
        }
        return $resultArr;
    }



    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }



    public function getUserPosition()
    {
        return $this->hasOne(ErpUserPosition::className(),['user_id'=>'user_id']);
    }



    public function getPosition()
    {
        return $this->hasOne(ErpPosition::className(),['id'=>'position_id'])->viaTable('laitovo_erp_user_position',['user_id'=>'user_id']);
    }

    public function getUsersReport()
    {
        return $this->hasMany(ErpUsersReport::className(),['user_id' => 'user_id']);
    }

    public function getFine()
    {
        return $this->hasMany(ErpFine::className(),['user_id' => 'user_id']);
    }

}