<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/23/17
 * Time: 2:30 PM
 */

namespace common\models\laitovo;


use common\models\LogActiveRecord;
use Yii;
class ErpUserPosition extends  LogActiveRecord
{

    public $division_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_user_position';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id', 'user_id'], 'integer'],
            [['article'],'string'],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpPosition::className(), 'targetAttribute' => ['position_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['division_id'], 'safe'],
            [['date_work','date_dismissed'], function($value){
                    return strtotime($value);
            }]
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'position_id' => Yii::t('app', 'Должность'),
            'user_id' => Yii::t('app', 'ФИО'),
            'date_work' => Yii::t('app', 'Дата принятия'),
            'date_dismissed' => Yii::t('app', 'Дата увольнения'),
            'article' => Yii::t('app', 'Артикул'),
        ];
    }



    public function getDivision()
    {
        return $this->hasOne(ErpDivision::className(),['id'=>'division_id'])->viaTable('laitovo_erp_position',['position_id'=>'id']);
    }
}