<?php

namespace common\models\laitovo;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%laitovo_erp_location}}".
 */
class ErpLocationQueue extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_location_queue}}';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'location_id' => Yii::t('app', 'Локация'),             
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }
}
