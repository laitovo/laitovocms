<?php

namespace common\models\laitovo;

use Yii;
use common\models\laitovo\ErpLocation;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "material_location".
 *
 * @property integer $material_parameters_id
 * @property integer $laitovo_erp_location_id
 *
 * @property LaitovoErpLocation $laitovoErpLocation
 * @property MaterialParameters $materialParameters
 */
class MaterialLocation extends \common\models\LogActiveRecord
{   
   
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_location';
    }
//     private $_locationId = null;
     
//      public function init()
//        {
//            $this->on(static::EVENT_AFTER_INSERT, [$this, 'saveLocationId']);
//            $this->on(static::EVENT_AFTER_UPDATE, [$this, 'saveLocationId']);
//
//            parent::init();
//        }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material_parameters_id', 'laitovo_erp_location_id'], 'required'],
            [['material_parameters_id', 'laitovo_erp_location_id'], 'integer'],
            [['laitovo_erp_location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocation::className(), 'targetAttribute' => ['laitovo_erp_location_id' => 'id']],
            [['material_parameters_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialParameters::className(), 'targetAttribute' => ['material_parameters_id' => 'id']],
            [['locationId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'material_parameters_id' => 'Material Parameters ID',
            'laitovo_erp_location_id' => 'Участок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'laitovo_erp_location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialParameters()
    {
        return $this->hasOne(MaterialParameters::className(), ['id' => 'material_parameters_id']);
    }
    
}


