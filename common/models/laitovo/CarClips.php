<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\user\User;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%laitovo_cars_clips}}".
 *
 * @property $name
 * @property $type
 */
class CarClips extends \common\models\LogActiveRecord
{
    const SIDE_TOP    = 'top';
    const SIDE_RIGHT  = 'right';
    const SIDE_BOTTOM = 'bottom';
    const SIDE_LEFT   = 'left';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_cars_clips}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'car_id' => Yii::t('app', 'Автомобиль'),
            'name' => Yii::t('app', 'Наименование'),
            'type' => Yii::t('app', 'Тип крепления'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }

    public function getFullname()
    {
        return $this->name.'/'.$this->type;
    }

    /**
     * Функция возвращает стророну, на которой находится клипса
     *
     * @param string $window Наименование вида оконного проёма
     * @param int $clipIndex Индекс клипсы на схеме (см. ClipsScheme::$json)
     * @param bool $isRvTriangle Флаг для треугольных форточек (для них своя логика определения стороны)
     * @return string|null
     */
    public static function getSide($window, $clipIndex, $isRvTriangle)
    {
        if ($window == 'FV') {
            $sides = [
                0 => self::SIDE_LEFT,
                1 => self::SIDE_LEFT,
                2 => self::SIDE_LEFT,
                3 => self::SIDE_RIGHT,
                4 => self::SIDE_RIGHT,
                5 => self::SIDE_RIGHT,
                6 => self::SIDE_BOTTOM,
            ];
        } elseif ($window == 'FD') {
            $sides = [
                0 => self::SIDE_LEFT,
                1 => self::SIDE_LEFT,
                2 => self::SIDE_LEFT,
                3 => self::SIDE_TOP,
                4 => self::SIDE_TOP,
                5 => self::SIDE_TOP,
                6 => self::SIDE_RIGHT,
                7 => self::SIDE_BOTTOM,
                8 => self::SIDE_BOTTOM,
                9 => self::SIDE_BOTTOM,
            ];
        } elseif ($window == 'RV' && $isRvTriangle) {
            $sides = [
                0 => self::SIDE_LEFT,
                1 => self::SIDE_LEFT,
                2 => self::SIDE_LEFT,
                3 => self::SIDE_RIGHT,
                4 => self::SIDE_RIGHT,
                5 => self::SIDE_RIGHT,
                6 => self::SIDE_BOTTOM,
            ];
        } elseif (in_array($window, ['RD', 'RV'])) {
            $sides = [
                0  => self::SIDE_LEFT,
                1  => self::SIDE_LEFT,
                2  => self::SIDE_LEFT,
                3  => self::SIDE_TOP,
                4  => self::SIDE_TOP,
                5  => self::SIDE_TOP,
                6  => self::SIDE_RIGHT,
                7  => self::SIDE_RIGHT,
                8  => self::SIDE_RIGHT,
                9  => self::SIDE_BOTTOM,
                10 => self::SIDE_BOTTOM,
                11 => self::SIDE_BOTTOM,
            ];
        } elseif ($window == 'BW') {
            $sides = [
                0  => self::SIDE_LEFT,
                1  => self::SIDE_LEFT,
                2  => self::SIDE_TOP,
                3  => self::SIDE_TOP,
                4  => self::SIDE_TOP,
                5  => self::SIDE_TOP,
                6  => self::SIDE_TOP,
                7  => self::SIDE_RIGHT,
                8  => self::SIDE_RIGHT,
                9  => self::SIDE_BOTTOM,
                10 => self::SIDE_BOTTOM,
                11 => self::SIDE_BOTTOM,
                12 => self::SIDE_BOTTOM,
                13 => self::SIDE_BOTTOM,
            ];
        } else {
            $sides = [];
        }

        return ArrayHelper::getValue($sides, $clipIndex);
    }
}
