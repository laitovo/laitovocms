<?php

namespace common\models\laitovo;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "laitovo_erp_location_workplace_register".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $workplace_id
 * @property integer $register_at
 * @property string $action_type
 *
 * @property LaitovoErpUser $user
 * @property LaitovoErpLocationWorkplace $workplace
 */
class ErpLocationWorkplaceRegister extends \common\models\LogActiveRecord
{
    const TYPE_IN='Приход';
    const TYPE_OUT='Уход';
    const TYPE_ACTIVE='Активный';
    const TYPE_INACTIVE='Неактивный';
    const TYPE_SEND_FROM='Команда на снятие';
    const TYPE_SEND_TO='Команда на постановку';
    const TYPE_MOVE_FROM='Снятие ';
    const TYPE_MOVE_TO='Постановка';


    public $actiontypes = [
        self::TYPE_IN=>self::TYPE_IN,
        self::TYPE_OUT=>self::TYPE_OUT,
        self::TYPE_ACTIVE=>self::TYPE_ACTIVE,
        self::TYPE_INACTIVE=>self::TYPE_INACTIVE,
        self::TYPE_SEND_FROM=>self::TYPE_SEND_FROM,
        self::TYPE_SEND_TO=>self::TYPE_SEND_TO,
        self::TYPE_MOVE_FROM=>self::TYPE_MOVE_FROM,
        self::TYPE_MOVE_TO=>self::TYPE_MOVE_TO,
    ];

//     public function behaviors()
//     {
//         return [
//             [
//                 'class' => TimestampBehavior::className(),
//                 'attributes' => [
//                     ActiveRecord::EVENT_BEFORE_INSERT => ['register_at'],
//                 ],
//                 // если вместо метки времени UNIX используется datetime:
//                 // 'value' => new Expression('NOW()'),
//             ],
//         ];
//     }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_location_workplace_register';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'workplace_id', 'register_at'], 'integer'],
            [['action_type'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['workplace_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpLocationWorkplace::className(), 'targetAttribute' => ['workplace_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'workplace_id' => 'Рабочее место',
            'register_at' => 'Дата регистрации',
            'action_type' => 'Вид действия',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkplace()
    {
        return $this->hasOne(ErpLocationWorkplace::className(), ['id' => 'workplace_id']);
    }
}
