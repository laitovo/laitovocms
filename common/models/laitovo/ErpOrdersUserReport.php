<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 20.09.17
 * Time: 11:37
 */

namespace common\models\laitovo;

use common\models\LogActiveRecord;

class ErpOrdersUserReport extends LogActiveRecord
{
    const LAITOVO_RU = 'laitovo.ru';
    const LAITOVO_DE = 'laitovo.de';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_orders_user_report}}';
    }

//    public function rules()
//    {
//        return [
//            [['client_id', 'order_id', 'client_inn', 'sum_order', 'created_at'], 'integer'],
//            [['client_name', 'site'], 'string'],
//        ];
//    }
}