<?php

namespace common\models\laitovo;

use Yii;
use common\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
/**
 * This is the model class for table "unit_accounting".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $material_parameter_id
 * @property integer $autor_id
 * @property integer $accounting_unit
 * @property integer $accounting_unit_price
 *
 * @property UnitMaterials $accountingUnit
 * @property LaitovoErpUser $autor
 * @property MaterialParameters $materialParameter
 */
class UnitAccounting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_accounting';
    }
    public $accounting_unit2;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'material_parameter_id', 'autor_id', 'accounting_unit'], 'integer'],
            [[  'accounting_unit_price' ], 'required'],
            [['accounting_unit2'], 'safe'],       
            [['accounting_unit_price'], 'double'],         
            [['accounting_unit'], 'exist', 'skipOnError' => true, 'targetClass' => UnitMaterials::className(), 'targetAttribute' => ['accounting_unit' => 'id']],
            [['autor_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['autor_id' => 'id']],
            [['material_parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialParameters::className(), 'targetAttribute' => ['material_parameter_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'material_parameter_id' => 'Material Parameter ID',
            'autor_id' => 'Autor ID',
            'accounting_unit' => 'Учетная единица',
            'accounting_unit2' => 'Учетная единица',
            'accounting_unit_price' => 'Цена учетной единицы',
        ];
    }
     public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                   
                ],
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'autor_id',
                    
                ],
                'value' => Yii::$app->user->identity->getId()
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountingUnit()
    {
        return $this->hasOne(UnitMaterials::className(), ['id' => 'accounting_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(User::className(), ['id' => 'autor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialParameter()
    {
        return $this->hasOne(MaterialParameters::className(), ['id' => 'material_parameter_id']);
    }
}
