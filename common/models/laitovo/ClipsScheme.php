<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\user\User;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%laitovo_cars_clips_scheme}}".
 */
class ClipsScheme extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_cars_clips_scheme}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'car_id' => Yii::t('app', 'Автомобиль'),
            'brand' => Yii::t('app', 'Марка продукции'),
            'type' => Yii::t('app', 'Вид продукции'),
            'type_clips' => Yii::t('app', 'Тип зажима'),
            'window' => Yii::t('app', 'Оконный проем'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }
}
