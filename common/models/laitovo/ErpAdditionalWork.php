<?php

namespace common\models\laitovo;

use Yii;
use backend\modules\laitovo\models\ErpJobType;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use common\models\user\User;

/**
 * This is the model class for table "laitovo_erp_additional_work".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $location_id
 * @property integer $job_type
 * @property double $job_count
 * @property double $job_price
 * @property double $rate
 * @property integer $status
 * @property integer $created_at
 * @property integer $finish_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 * @property integer $confirmed
 *
 * @property ErpUser $author
 * @property ErpJobType $jobType
 * @property ErpLocation $location
 * @property ErpUser $user
 */
class ErpAdditionalWork extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_additional_work';
    }

    /**
     * @inheritdoc
     */
   

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'ФИО получившего наряд'),
            'location_id' => Yii::t('app', 'Участок для наряда'),
            'job_type' => Yii::t('app', 'Вид работ'),
            'job_count' => Yii::t('app', 'Количество работ'),
            'job_price' => Yii::t('app', 'Стоимость работ'),
            'rate' => Yii::t('app', 'Коэффициент'),
            'status' => Yii::t('app', 'Статус наряда'),
            'confirmed' => Yii::t('app', 'Подтвержден'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'finish_at' => Yii::t('app', 'Дата окончания'),
            'updated_at' => Yii::t('app', 'Дата обновления'),
            'author_id' => Yii::t('app', 'Автор записи'),
            'updater_id' => Yii::t('app', 'Автор исправления'),
            'comment' => Yii::t('app', 'Комментарий'),
        ];
    }
     public function behaviors()
    {
        return [
            //Устанавливаем дату создания
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            //Устанавливаем автора создания
           [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobType()
    {
        return $this->hasOne(ErpJobType::className(), ['id' => 'job_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }
    public function getUpdater()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'updater_id']);
    }
}
