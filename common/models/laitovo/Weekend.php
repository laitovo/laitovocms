<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/17/17
 * Time: 9:11 AM
 */

namespace common\models\laitovo;


use yii\base\Model;
use DatePeriod;
use DateInterval;
use DateTime;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use Yii;
use common\models\LogActiveRecord;


class Weekend  extends LogActiveRecord
{
    public static function tableName()
    {
        return 'weekend';
    }

    public $dateFrom;

    public $week_list=[
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
    ];

    public function rules()
    {
        return [
            [['weekend'], 'required'],
        ];
    }

    //первый день месяца
    public function getDateFrom($first_day_month)
    {
        return date('Y-m-d', $first_day_month);
    }

  //последний день месяца
    public function getDateTo($first_day_month)
    {
        return date('Y-m-t', $first_day_month);
    }

    //массив дней за месяц
    public function getDateArray($first_day_month)
    {
        $begin = new DateTime($this->getDateFrom($first_day_month));
        $end = new DateTime($this->getDateTo($first_day_month));
        $end = $end->modify( '+1 day' );

        $period = new DatePeriod($begin, new DateInterval('P1D'), $end);
        $arrayOfDates = array_map(
            function($item){
                return $item->format('Y-m-d-l-W');
            },
            iterator_to_array($period)
        );
        return $arrayOfDates;
    }


    //массив для провайдера данных
    public function resultArray($first_day_month)
    {
        $result =[];

        $dateArray = $this->getDateArray($first_day_month);
        if($dateArray)
        {
            foreach ($dateArray as $date)
            {
                $value = explode('-', $date);
                $const[$value[4]] =[
                    'Monday'=>'',
                    'Tuesday'=>'',
                    'Wednesday'=>'',
                    'Thursday'=>'',
                    'Friday'=>'',
                    'Saturday'=>'',
                    'Sunday'=>''
                ];

                $result[$value[4]][$value[3]]=$value[0].'-'. $value[1].'-'.$value[2];
                $result[$value[4]] = array_replace($const[$value[4]], $result[$value[4]]);
            }
        }
        return $result;
    }

    public function getArrayDataProvider($first_day_month)
    {
        $arrayDataProvider = new ArrayDataProvider([
            'allModels' => $this->resultArray($first_day_month),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $arrayDataProvider;
    }

    public function label($day)
    {
        if($day=='Monday') return 'ПН';
        if($day=='Tuesday') return 'ВТ';
        if($day=='Wednesday') return 'СР';
        if($day=='Thursday') return 'ЧТ';
        if($day=='Friday') return 'ПТ';
        if($day=='Saturday') return 'СБ';
        if($day=='Sunday') return 'ВС';
    }


    public function getFirstDayMonth()
    {
        $arr = [];
        for($i=1; $i<=12; $i++)
        {
            $arr[] = mktime(0, 0, 0, $i, 1);
        }
        return $arr;
    }

    public function getMonthYear($day)
    {
        $m='';
        $y = date('Y',$day);
        $month = date('m', $day);
        if($month){
            switch ($month) {
                case 1:
                    $m = 'Январь';
                    break;
                case 2:
                    $m = 'Февраль';
                    break;
                case 3:
                    $m = 'Март';
                    break;
                case 4:
                    $m = 'Апрель';
                    break;
                case 5:
                    $m = 'Май';
                    break;
                case 6:
                    $m = 'Июнь';
                    break;
                case 7:
                    $m = 'Июль';
                    break;
                case 8:
                    $m = 'Август';
                    break;
                case 9:
                    $m = 'Сентябрь';
                    break;
                case 10:
                    $m = 'Октябрь';
                    break;
                case 11:
                    $m = 'Ноябрь';
                    break;
                case 12:
                    $m = 'Декабрь';
                    break;
            }
        }
        return $m.' '.$y;
    }

    public function weekendIn($day)
    {
        $weekend = Weekend::find()->where(['weekend'=>$day])->one();
        if($weekend && $weekend->id) return true;
        return false;
    }


    static function getWeekendList(){
        return ArrayHelper::map(self::find()->all(),'weekend','weekend');
    }
}