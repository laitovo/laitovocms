<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\User;
use common\models\laitovo\MaterialProductionScheme;
use backend\modules\laitovo\models\ErpJobScheme;
use common\models\laitovo\Config;
use backend\modules\laitovo\models\ErpProductType;

class ProductionScheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'production_scheme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scheme_id', 'product_id', 'author_id', 'created_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title', 'scheme_id', 'product_id'], 'required'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpProductType::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpScheme::className(), 'targetAttribute' => ['scheme_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название карточки продукта',
            'scheme_id' => 'Маршрут',
            'product_id' => 'Название продукта',
            'author_id' => 'Автор записи',
            'created_at' => 'Дата создания',
            'scheme.name' => 'Название маршрута',
            'product.title' => 'Название продукта',
            'author.name' => 'Автор записи',
        ];
    }
    
     public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                   
                ],
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                    
                ],
                'value' => Yii::$app->user->identity->getId()
            ],
        ];
    }
    
    
    /*
     * Считаем себестоимость материалов в цикле
     */
    public function materialCount()
    {
        $material_list = MaterialProductionScheme::find()->where(['production_scheme_id'=>$this->id])->all();
        $totalMaterial = 0;
        if ($material_list) {
            foreach ($material_list as $material) {
                $unitPrice = $material->materialParameter->lastAccounting->accounting_unit_price*$material->materialParameter->lastSettlement->coefficient_settlement;
                if($unitPrice!=0 && $material->material_count !=0) 
                    {
                        $totalMaterial += $material->material_count*$unitPrice;
                    }
            }
            return $totalMaterial;
        }
    }
    
    
    /*
     * Считаем себестоимость работ в цикле
     */
    public function totalJobCount()
    {
        $totalJob = 0;
//        $totalClips = 0;
//        $count=0;
        
        $job_types = ErpJobScheme::find()->where(['production_scheme_id'=>$this->id])->all();
        $config = Config::findOne(2);//отсюда достаем цену нормированного коэффициента и среднюю дневную норму
        if($config->json('averageRate') !=0 && $config->json('hourrate') !=0 )
        {
            
            if ($job_types) {
                
                foreach ($job_types as $job_type) {
                    if($job_type->type->rate !=0 && $job_type->job_count!=0)
                    {
                        $totalJob += $config->json('averageRate')/ $job_type->type->rate * $job_type->job_count * $config->json('hourrate');
                    }
                }
            }
        }
       
//        $job_clips = ErpJobScheme::find()->where(['production_scheme_id'=>$this->id, 'location_id'=>Yii::$app->params['erp_clipsi']])->all();
//        
//        if($config->json('averageRate') !=0 && $config->json('hourrate') !=0 )
//        {
//            if ($job_clips)
//            {
//                foreach ($job_clips as $clips)
//                {
//                        if($clips->type_id == Yii::$app->params['jobClips1'] || $clips->type_id == Yii::$app->params['jobClips2']
//                                || $clips->type_id == Yii::$app->params['jobClips3'] )
//                        {
//                             if($clips->type->rate !=0 && $clips->job_count!=0)
//                            {
//                                $totalClips += $config->json('averageRate')/ $clips->type->rate * $clips->job_count * $config->json('hourrate');
//                               // $count++;
//                            }
//                        }else{
//                            if($clips->type->rate !=0 && $clips->job_count!=0)
//                            {
//                                $totalJob += $config->json('averageRate')/ $clips->type->rate * $clips->job_count * $config->json('hourrate');
//                            }
//                        }
//                }
//            }
//            
//        }
//        if($totalClips!=0 && $count!=0 && $totalJob!=0)
//           {
//                return   $totalJobClips = $totalClips+$totalJob;
//           }else{
//                return $totalJobClips = $totalJob;
//           }   
        return $totalJob;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobProductionSchemes()
    {
        return $this->hasMany(JobProductionScheme::className(), ['production_scheme_id' => 'id']);
    }


    public function getMaterialProductionSchemes()
    {
        return $this->hasMany(MaterialProductionScheme::className(), ['production_scheme_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(ErpProductType::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheme()
    {
        return $this->hasOne(ErpScheme::className(), ['id' => 'scheme_id']);
    }
}
