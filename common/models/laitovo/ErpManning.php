<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "laitovo_erp_manning".
 *
 * @property integer $id
 * @property integer $position_id
 * @property double $salary_one
 * @property double $salary_two
 * @property double $car_compensation
 * @property double $surcharge
 * @property double $qualification
 * @property double $premium
 * @property double $kpi_one
 * @property double $kpi_two
 * @property double $kpi_three
 */
class ErpManning extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_manning';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],

                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'position_id' => Yii::t('app', 'Должность'),
            'division_id' => Yii::t('app', 'Отдел'),
            'salary_one' => Yii::t('app', 'Оклад-1'),
            'salary_two' => Yii::t('app', 'Оклад-2'),
            'car_compensation' => Yii::t('app', 'Компенсация а/м'),
            'surcharge' => Yii::t('app', 'Сезонная доплата'),
            'qualification' => Yii::t('app', 'Квалификация'),
            'premium' => Yii::t('app', 'Премия'),
            'kpi_one' => Yii::t('app', 'KPI-1'),
            'kpi_two' => Yii::t('app', 'KPI-2'),
            'kpi_three' => Yii::t('app', 'KPI-3'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(ErpPosition::className(), ['id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivision()
    {
        return $this->hasOne(ErpDivision::className(), ['id' => 'division_id']);
    }

    public function getUserPosition()
    {
        return $this->hasOne(ErpUserPosition::className(), ['id' => 'position_id']);
    }

}
