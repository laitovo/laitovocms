<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/28/17
 * Time: 1:55 PM
 */

namespace common\models\laitovo;



use Yii;
class ErpDocumentCause extends \common\models\LogActiveRecord
{
    const SICK_LEAVE = 'Б';
    const ADDITIONAL_LEAVE = 'ОД'; // отпуск за свой счет
    const ANNUAL_VACATION = 'ОТ'; // ежегодный отпуск
    const BISINESS_TRIP = 'К'; // командировка

    static $types = [
            self::SICK_LEAVE => 'Больничка',
            self::ADDITIONAL_LEAVE => 'Отпуск за свой счет',
            self::ANNUAL_VACATION => 'Оплачиваемый отпуск',
            self::BISINESS_TRIP => 'Командировка'
        ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_document_cause}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'ФИО'),
            'title' => Yii::t('app', 'Номер документа'),
            'date_start' => Yii::t('app', 'Дата начала'),
            'date_finish' => Yii::t('app', 'Дата окончания'),
            'type' => Yii::t('app', 'Тип документа'),
        ];
    }


    public function getUser()
    {
        return $this->hasOne(ErpUser::className(),['id'=>'user_id']);
    }

}