<?php

namespace common\models\laitovo;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\user\User;
use yii\db\ActiveRecord;
use common\models\laitovo\CarsAct;

/**
 * @property string $name
 * @property string $mark
 * @property string $fullEnName
 * @property string $shortEnName
 * @property string $enCarcass
 * @property string $doors
 * @property string $carcass
 * @property string $article
 *
 * This is the model class for table "{{%laitovo_cars}}".
 */
class Cars extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_cars}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'article' => Yii::t('app', 'Артикул'),
            'mark' => Yii::t('app', 'Марка'),
            'model' => Yii::t('app', 'Модель'),
            'generation' => Yii::t('app', 'Поколение'),
            'carcass' => Yii::t('app', 'Кузов'),
            'doors' => Yii::t('app', 'Дверей'),
            'firstyear' => Yii::t('app', 'Год начала производства'),
            'lastyear' => Yii::t('app', 'Год окончания производства'),
            'modification' => Yii::t('app', 'Модификация'),
            'country' => Yii::t('app', 'Страна производства'),
            'category' => Yii::t('app', 'Категория цены'),
            'mounting' => Yii::t('app', 'Крепления'),
            'sort' => Yii::t('app', 'Сортировка'),
            'status' => Yii::t('app', 'Активен'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'Json'),
            'mastered' => Yii::t('app', 'Освоена'),
            'checked' => Yii::t('app', 'Проверена'),
            'checked_date' => Yii::t('app', 'Дата проверки'),
            'checked_author' => Yii::t('app', 'Кто проверил'),
            'blanket_default' => Yii::t('app', 'Одеяло, стандартный номер'),
            'blanket_custom' => Yii::t('app', 'Одеяло, индивидуальный размер'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckedAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'checked_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClips()
    {
        return $this->hasMany(CarClips::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchemes()
    {
        return $this->hasMany(ClipsScheme::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActs()
    {
        return $this->hasMany(CarsAct::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnalogs()
    {
        return $this->hasMany(CarsAnalog::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnalogCars()
    {
        return $this->hasMany(self::className(), ['id' => 'analog_id'])->viaTable('{{%laitovo_cars_analog}}', ['car_id' => 'id']);
    }

    /**
     * [getArticleListWithAnalogs Функция , которая возврщает список артикулов через запятую в виде строки, вместе с текущим артикулом автомобилей]
     * This is a cool function
     * @author Алексей Евдокимов <heksweb@gmail.com>
     * @version     [version]
     * @date        2017-12-18
     * @anotherdate 2017-12-18T15:34:30+0300
     * @return      [type]                   [description]
     */
    public function getArticleListWithAnalogs()
    {
        //получим все аналоги
        $result = '';
        $articles = [];
        $articles[] = $this->article;
        $analogCars = $this->getAnalogCars()->all();
        foreach ($analogCars as $key => $value) {
            $articles[] = $value->article;
        }
        if (count($articles)) {
            $result = implode(' | ', $articles);
        }
        return $result;
    }

    public function getFullName()
    {
        return $this->mark
        .' '.$this->model
        .' '.($this->generation?$this->generation.'G':'')
        .' '.$this->carcass
        .' '.$this->doors.'D'
        .' ('.$this->firstyear
        .' - '.($this->lastyear?:'н.в.')
        .') '.$this->modification
        ;
    }

    public function getFullEnName()
    {
        $carcass=[
            "Седан"=>"Sedan",
            "Универсал"=>"Station wagon",
            "Внедорожник"=>"SUV",
            "Купе"=>"Coupe",
            "Кроссовер"=>"Crossover",
            "Хетчбэк"=>"Hatchback",
            "Минивэн"=>"Minivan",
            "Пикап"=>"Pickup",
            "Компактвэн"=>"Compact MPV",
            "Фургон"=>"Van",
            "Микроавтобус"=>"Minivan",
            "Кабриолет"=>"Cabrio",
            "Грузовик"=>"Truck",
            "Лифтбэк"=>"Liftback",
            "Фастбэк"=>"Fastback",
        ];
        return $this->json('mark_en')
        .' '.$this->json('model_en')
        .' '.($this->generation?$this->generation.'G':'')
        .' '.$this->json('modification_en')
        .' '.$this->doors.'D'
        .' '.$carcass[$this->carcass]
        .' ('.$this->firstyear
        .' - '.($this->lastyear?:date('Y'))
        .')'
        ;
    }

    public function getEnCarcass()
    {
        $carcass = [
            "Седан"=>"Sedan",
            "Универсал"=>"Station wagon",
            "Внедорожник"=>"SUV",
            "Купе"=>"Coupe",
            "Кроссовер"=>"Crossover",
            "Хетчбэк"=>"Hatchback",
            "Минивэн"=>"Minivan",
            "Пикап"=>"Pickup",
            "Компактвэн"=>"Compact MPV",
            "Фургон"=>"Van",
            "Микроавтобус"=>"Minivan",
            "Кабриолет"=>"Cabrio",
            "Грузовик"=>"Truck",
            "Лифтбэк"=>"Liftback",
            "Фастбэк"=>"Fastback",
        ];

        return $carcass[$this->carcass];
    }

    public function getShortEnName()
    {
        $carcass=[
            "Седан"=>"Sedan",
            "Универсал"=>"Station wagon",
            "Внедорожник"=>"SUV",
            "Купе"=>"Coupe",
            "Кроссовер"=>"Crossover",
            "Хетчбэк"=>"Hatchback",
            "Минивэн"=>"Minivan",
            "Пикап"=>"Pickup",
            "Компактвэн"=>"Compact MPV",
            "Фургон"=>"Van",
            "Микроавтобус"=>"Minivan",
            "Кабриолет"=>"Cabrio",
            "Грузовик"=>"Truck",
            "Лифтбэк"=>"Liftback",
            "Фастбэк"=>"Fastback",
        ];
        return $this->json('mark_en')
        .' '.$this->json('model_en')
        .' '.($this->generation?$this->generation.'G':'')
        .' '.$this->json('modification_en')
//        .' '.$this->doors.'D'
//        .' '.$carcass[$this->carcass]
        .' ('.$this->firstyear
        .' - '.($this->lastyear?:date('Y'))
        .')'
        ;
    }

    public function getNaryads()
    {
        //получим наряды с данным артикулом автомобиля
        $count = ErpNaryad::find()->where(['and',
            ['like', 'article', '%-%-'.$this->article.'-%-%',false], 
            ['>', 'created_at', time() - 2*60*60*24*30], 
        ])->count();

        $result = round($count/2);
        return $result;

    }

    public function getNaryadsByMonth($month = 0)
    {
        $date = $month ? time() - $month*60*60*24*30 : 0;
        //получим наряды с данным артикулом автомобиля
        return ErpNaryad::find()->where(['and',
            ['like', 'article', '%-%-'.$this->article.'-%-%',false], 
            ['>', 'created_at', $date], 
            ['<', 'created_at', time()], 
        ])->count();
    }


    /*#######################################################################*/
    // Функция для поиска количества непроверенных автомобилей
    public static function receiveNotCheckedCarsCount()
    {
        return self::find()->where(['or',
            ['is','checked',null],
        ])->count();
    }

    // Функция для поиска количества непроверенных автомобилей
    public static function receiveNotCheckedReworkCarsCount()
    {
        return self::find()->where(['or',
            ['checked'=> 2],
        ])->count();
    }

    // Функция для поиска количества непроверенных автомобилей
    public static function receiveNotCheckedReworkCarsQuery()
    {
        return self::find()->where(['or',
            ['checked'=> 2],
        ]);
    }

    // Функция для поиска непроверенных автомобилей
    public static function receiveNotCheckedCarsQuery()
    {
        return self::find()->where(['or',
            ['is','checked',null],
        ]);
    }
    /*######################################################################*/

    /*#######################################################################*/
    // Функция для поиска акта освоения для машины по элементу
    public function receiveMasteringActByElement($elem)
    {
        $acts = CarsAct::find()->where(['and',
            ['car_id' => $this->id],
        ])->all();
        if ($acts)
            foreach ($acts as $act) {
                if ($act->element){
                    $elements = explode(',', $act->element);
                    if (in_array($elem, $elements))
                        return $act;
                }
            }
        return false;   
    }

    /*#######################################################################*/
    // Освоена ли машина полностью ?
    public function isFullMastered()
    {
        $result = true;
        if ($this->json('fw1') && !$this->json('fw') && !$this->receiveMasteringActByElement(CarsAct::ELEMENT_FW)) $result = false;
        if ($this->json('fv1') && !$this->json('fv') && !$this->receiveMasteringActByElement(CarsAct::ELEMENT_FV)) $result = false;
        if ($this->json('fd1') && !$this->json('fd') && !$this->receiveMasteringActByElement(CarsAct::ELEMENT_FD)) $result = false;
        if ($this->json('rd1') && !$this->json('rd') && !$this->receiveMasteringActByElement(CarsAct::ELEMENT_RD)) $result = false;
        if ($this->json('rv1') && !$this->json('rv') && !$this->receiveMasteringActByElement(CarsAct::ELEMENT_RV)) $result = false;
        if ($this->json('bw1') && !$this->json('bw') && !$this->receiveMasteringActByElement(CarsAct::ELEMENT_BW)) $result = false;
        return $result;
    }


    public function carsAllArray($query,$hide = false,$lekalo = false)
    {
        $newArr = [];
        $cars = $query->asArray()->all();
        foreach ($cars as $car) {
           // return $car;
            $json = json_decode($car['json']);

            foreach ($json as $key =>$value)
            {
                if ($key == 'nomerlekala') { 
                    $car[$key] = (int)$value;       
                }else{ 
                    $car[$key] = $value; 
                } 
            }
            $newArr[$car['id']] = $car;
            if ($hide && ( !isset($car['nomerlekala']) || $car['nomerlekala'] == 0)) {
               unset($newArr[$car['id']]);
            }
            if ($lekalo && isset($car['nomerlekala']) && $car['nomerlekala'] == (int)$lekalo) {
                return [$car];
            }
        }

        //Если был поиск по лекалу но ничего не найдено, тогда пустой массив
        if ($lekalo) return  [];

        return $newArr;
    }

    public function getPatternRegister()
    {
        return $this->hasMany(PatternRegister::className(), ['car_id' =>'id']);
    }

    public function getTranslit($str)
    {
        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g",
            "Д" => "d", "Е" => "e", "Ж" => "j", "З" => "z", "И" => "i",
            "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            " " => "_", "." => "", ":" => "", '"' => "", "'" => "", "ё" => "e",
            "(" => "", ")" => "", "," => "", "/" => "", "`" => ""
        );
        return strtr($str, $tr);
    }

    public function getLekalo() {
        return $this->json('nomerlekala') ? (int)$this->json('nomerlekala') : null;
    }

}
