<?php

namespace common\models\laitovo;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "laitovo_erp_location_workplace_plan".
 *
 * @property integer $id
 * @property integer $workplace_id
 * @property integer $user_id
 * @property integer $time_from
 * @property integer $time_to
 * @property integer $created_at
 * @property integer $author_id
 *
 * @property User $author
 * @property LaitovoErpUser $user
 * @property LaitovoErpLocationWorkplace $workplace
 */
class ErpLocationWorkplacePlan extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_location_workplace_plan';
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'workplace_id' => 'Workplace ID',
            'user_id' => 'Сотрудник',
            'time_from' => 'Time From',
            'time_to' => 'Time To',
            'created_at' => 'Created At',
            'author_id' => 'Author ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkplace()
    {
        return $this->hasOne(ErpLocationWorkplace::className(), ['id' => 'workplace_id']);
    }
}
