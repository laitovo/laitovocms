<?php

namespace common\models\laitovo;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "laitovo_erp_product_group".
 *
 * @property integer $id
 * @property string $title
 * @property integer $location_id
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $updated_at
 * @property integer $updater_id
 *
 * @property User $author
 * @property LaitovoErpLocation $location
 * @property User $updater
 */
class ErpProductGroup extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_product_group';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'location_id' => 'Участок',
            'created_at' => 'Дата создания',
            'author_id' => 'Автор',
            'updated_at' => 'Дата изменения',
            'updater_id' => 'Изменил',
        ];
    }

    public function behaviors()
    {
        return [
            //Устанавливаем дату создания
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            //Устанавливаем автора создания
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id', 'updater_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    //получаем все продукты для группы, используя связующую таблицу
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(ErpProductType::className(), ['id' => 'product_id'])
            ->viaTable('laitovo_erp_product_group_relation', ['group_id' => 'id']);
    }

    //получаем все продукты для группы, используя связующую таблицу
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOddProducts()
    {
        $ids = [];
        //Ищем все группы с такой же локацией
        $locationGroup = self::find()->where(['location_id' => $this->location_id])->all();
        //Для каждой группы находим продукты и записываем их id в массив
        if ($locationGroup)
            foreach ($locationGroup as $group) {
                $products = $group->products;
                if ($products)
                    foreach ($products as $product) {
                        if (!in_array($product->id, $ids)){
                            $ids[$product->id]=$product->id;
                        }
                    }
            }

        $activeQuery = ErpProductType::find()->where(['not in','id',$ids]);

        return $activeQuery ? $activeQuery : null;
    }
}
