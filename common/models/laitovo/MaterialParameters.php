<?php

namespace common\models\laitovo;

use Yii;
use common\models\laitovo\Material;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class MaterialParameters extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_parameters';
    }
     protected $_locationId;
     protected $accounting_unit;
     protected $accounting_unit_price;
     protected $unit_settlement;
     protected $coefficient_settlement;

    /**
     * @inheritdoc
     */
   
     public function behaviors()
        {
            return [
                [
                    'class' => TimestampBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],

                    ],
                ]
            ];
        }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'material_id' => 'Material ID',
            'parameter' => 'Параметр материала',
            'accounting_unit' => 'Учетная единица',
            'accounting_unit_price' => 'Цена учетной единицы',
            'unit_settlement' => 'Расчетная единица',
            'coefficient_settlement' => 'Коэффициент для расчета(взвешивание)',
            'price_settlement' => 'Стоимость расчетной единицы',
            'autor_id' => 'Автор последних изменений',
            'created_at' => 'Дата последних изменений',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialLocations()
    {
        return $this->hasMany(MaterialLocation::className(), ['material_parameters_id' => 'id']);
    }
     public function getLocations()
    {
        return $this->hasMany(ErpLocation::className(), ['id' => 'laitovo_erp_location_id'])->viaTable('material_location', ['material_parameters_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['id' => 'material_id']);
          
    }
    
    public function getMaterialName()
    {
        $material = $this->material;
        if($material && $this->parameter)
            return $material->name.' '. $this->parameter;
        if($material && !$this->parameter)
            return $material->name;
        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitAccountings()
    {
        return $this->hasMany(UnitAccounting::className(), ['material_parameter_id' => 'id']);
    }
    
    public function getlastAccounting()
    {
        return  UnitAccounting::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getlastSettlement()
    {
        return UnitSettlement::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one(); 
    }

    /**Получаем учетную единицу
     * @return mixed
     */
    public function getAccountingUnitName()
    {
        $unit = UnitAccounting::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
        return $unit->accountingUnit->name;
    }

    /**
     * Получаем коэффициент расчета
     * @return mixed
     *
     */
    public function getCoefficient_settlement()
    {
        $unit = UnitSettlement::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
        return $unit->coefficient_settlement;
    }
    /**
     * Получаем стоимость учетной единицу
     */

    public function getAccounting_unit_price()
    {
        if($this->getIsNewRecord()) return $this->accounting_unit_price;
        $unit = UnitAccounting::find()->where(['material_parameter_id'=>$this->id])->orderBy('created_at DESC')->one();
        return $unit->accounting_unit_price;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitSettlements()
    {
        return $this->hasMany(UnitSettlement::className(), ['material_parameter_id' => 'id']);
    }
    
      static function locationName($id)
   {
        $location = ArrayHelper::map(ErpLocation::find()->joinWith('materials')->andWhere(['material_parameters_id'=>$id])->all(), 'id','name');
      // die(print_r($location, true));
        return implode(', ', $location);
    }
    
     public function setLocationId($ids)
        {
            $this->_locationId = (array) $ids;
        }
     protected function saveLocationsId()
    {
        
        //сброс всех текущих связей
        $this->unlinkAll('locations', true);
        $locations = ErpLocation::find()->where(['id' => $this->_locationId])->all();
        foreach ($locations as $location) {
            //линкуем модель
            $this->locations = $location;
        }
    }
    
    public function getLocationId()
    {
        return $this->getRelation('locations')->indexBy('id')->select('id')->column();
    }
    
   
}
