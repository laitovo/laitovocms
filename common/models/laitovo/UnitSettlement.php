<?php

namespace common\models\laitovo;

use Yii;
use common\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "unit_settlement".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $material_parameter_id
 * @property integer $autor_id
 * @property integer $unit_settlement
 * @property integer $coefficient_settlement
 *
 * @property LaitovoErpUser $autor
 * @property MaterialParameters $materialParameter
 * @property UnitMaterials $unitSettlement
 */
class UnitSettlement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_settlement';
    }
    public $unit_settlement2;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'material_parameter_id', 'autor_id', 'unit_settlement'], 'integer'],
            [[  'coefficient_settlement' ], 'required'],
            [['unit_settlement2'], 'safe'],
            [['coefficient_settlement'], 'double'],
            [['autor_id'], 'exist', 'skipOnError' => true, 'targetClass' => ErpUser::className(), 'targetAttribute' => ['autor_id' => 'id']],
            [['material_parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialParameters::className(), 'targetAttribute' => ['material_parameter_id' => 'id']],
            [['unit_settlement'], 'exist', 'skipOnError' => true, 'targetClass' => UnitMaterials::className(), 'targetAttribute' => ['unit_settlement' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'material_parameter_id' => 'Material Parameter ID',
            'autor_id' => 'Autor ID',
            'unit_settlement' => 'Расчетная единица',
            'unit_settlement2' => 'Расчетная единица',
            'coefficient_settlement' => 'Коэффициент для расчета(взвешивание)',
        ];
    }
     public function behaviors()
        {
            return [
                [
                    'class' => TimestampBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],

                    ],
                ],
                 [
                    'class' => AttributeBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => 'autor_id',
                    ],
                    'value' => Yii::$app->user->identity->getId()
            ],
            ];
        }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(User::className(), ['id' => 'autor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialParameter()
    {
        return $this->hasOne(MaterialParameters::className(), ['id' => 'material_parameter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitSettlement()
    {
        return $this->hasOne(UnitMaterials::className(), ['id' => 'unit_settlement']);
    }
}
