<?php

namespace common\models\laitovo;

use Yii;
use common\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use common\models\laitovo\ProductionScheme;
use backend\modules\logistics\models\StorageLiteral;

/**
 * This is the model class for table "laitovo_erp_product_type".
 *
 * @property integer $id
 * @property string $title
 * @property string $rule
 * @property integer $author_id
 * @property integer $created_at
 * @property integer $production_scheme_id [ идентификатор карточки продукта ]
 *
 * @property User $author
 */
class ErpProductType extends \common\models\LogActiveRecord
{
    CONST CATEGORY_LAITOVO_WITH_CLAMPS = 'Laitovo на зажимах';
    CONST CATEGORY_LAITOVO_WITH_MAGNETS = 'Laitovo на магнитах';
    CONST CATEGORY_CHIKO_WITH_CLAMPS = 'Chiko на зажимах';
    CONST CATEGORY_CHIKO_WITH_MAGNETS = 'Chiko на магнитах';
    CONST CATEGORY_DONTLOOK_SLIDING = 'Don\'t look сдвижной';
    CONST CATEGORY_DONTLOOK_FRAMELESS = 'Don\'t look бескаркасный';
    CONST CATEGORY_PEAKS = 'Козырьки';
    CONST CATEGORY_ANOTHER = 'Все прочее';

    //категории продукта
    public $categories = [
        '' => '',
    self::CATEGORY_LAITOVO_WITH_CLAMPS  => self::CATEGORY_LAITOVO_WITH_CLAMPS  ,
    self::CATEGORY_LAITOVO_WITH_MAGNETS => self::CATEGORY_LAITOVO_WITH_MAGNETS ,
    self::CATEGORY_CHIKO_WITH_CLAMPS    => self::CATEGORY_CHIKO_WITH_CLAMPS    ,
    self::CATEGORY_CHIKO_WITH_MAGNETS   => self::CATEGORY_CHIKO_WITH_MAGNETS   ,
    self::CATEGORY_DONTLOOK_SLIDING     => self::CATEGORY_DONTLOOK_SLIDING     ,
    self::CATEGORY_DONTLOOK_FRAMELESS   => self::CATEGORY_DONTLOOK_FRAMELESS   ,
    self::CATEGORY_PEAKS                => self::CATEGORY_PEAKS                ,
    self::CATEGORY_ANOTHER              => self::CATEGORY_ANOTHER              ,
        //другие элементы
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_product_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'created_at', 'production_scheme_id'], 'integer'],
            [['title','title_en', 'rule', 'article','category'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['production_scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionScheme::className(), 'targetAttribute' => ['production_scheme_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID') ,
            'title' => Yii::t('app', 'Наименование') ,
            'title_en' => Yii::t('app', 'Наименование (EN)') ,
            'rule' => Yii::t('app', 'Правило') ,
            'author_id' => Yii::t('app', 'Автор'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'production_scheme_id'=>Yii::t('app', 'Производственный цикл'),
            'category'=>Yii::t('app', 'Категория'),
            'article'=>Yii::t('app', 'Артикул')
        ];
    }

    public function behaviors()
    {
        return [
            //Устанавливаем дату создания
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                ],
            ],
            //Устанавливаем автора создания
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
    
    public function getProductionScheme()
    {
        return $this->hasOne(ProductionScheme::className(), ['id' => 'production_scheme_id']);
    }

    //получаем все продукты для группы, используя связующую таблицу
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(ErpProductGroup::className(), ['id' => 'group_id'])
            ->viaTable('laitovo_erp_product_group_relation', ['product_id' => 'id']);
    }


    public function getLiteralByStorage($storage_id)
    {
        $row = (new \yii\db\Query())
            ->from('logistics_storage_product')
            ->where(['product_id' => $this->id])
            ->andWhere(['storage_id' => $storage_id])
            ->one();
        if (isset($row['literal_id'])) {
            $literal = StorageLiteral::findOne($row['literal_id']);
            return $literal->title;
        }
        return '';

    }
}
