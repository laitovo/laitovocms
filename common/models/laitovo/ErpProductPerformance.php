<?php

namespace common\models\laitovo;

use Yii;

/**
 * This is the model class for table "laitovo_erp_product_performance".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $group_id
 * @property integer $average_time
 * @property integer $count_naryads
 *
 * @property LaitovoErpProductGroup $group
 * @property LaitovoErpUser $user
 */
class ErpProductPerformance extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_product_performance';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Сотрудник',
            'group_id' => 'Группа продуктов',
            'average_time' => 'Среднее время',
            'count_naryads' => 'Количество нарядов',
            'full_time' => 'Общее время',
            'day_count' => 'нарядов за день',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(ErpProductGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }
}
