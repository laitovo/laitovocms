<?php

namespace common\models\laitovo;

use backend\modules\laitovo\models\LaitovoReworkAct;
use common\models\logistics\Naryad as LogisticsNaryad;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use common\models\user\User;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%laitovo_erp_naryad}}".
 */

/**
 * Class ErpNaryad
 * @package common\models\laitovo
 *
 * @property integer|null $location_id       [ идентификатор рабочего участка ]
 * @property string|null  $literal           [ литера для для маркировки нарядов ]
 * @property integer|null $literal_date      [ дата присвоения литеры наряду ]
 * @property integer|null $sort              [ приоритет выдачи нарядов ]
 * @property integer|null $oldSort           [ приоритет выдачи нарядов ]
 * @property integer      $id                [ идентификатор наряда ]
 * @property integer      $ready_date        [ дата готовности наряда ]
 * @property integer      $updated_at        [ дата последнего обновления наряда ]
 * @property integer      $start             [ участок производства, на который должен быть выдан наряд ]
 * @property string       $status            [ статус наряда ]
 * @property integer      $distributed       [ статус - убарн с производства на склад или нет (true - да , false - нет) ]
 * @property integer      $distributed_date  [ дата, когда наряд убран с производства на склад ]
 * @property integer      $scheme_id         [ схема движения наряда по произвосдвту, идентификатор ]
 * @property ErpScheme    $scheme            [ схема движения наряда по произвосдвту, объект ]
 * @property integer      $article           [ арткиул продукта ]
 * @property integer      $reestr            [ реестр, которому принадлежит наряд. DEPRECATED. NOT USED. ]
 * @property integer      $isNeedTemplate    [ необходимо пежде изготовить лекало для изготовление данного наряда ]
 * @property integer      $isFromSump        [ наряд на проверку Продукта из склада отстойник ]
 * @property integer      $sumpLiteral       [ литера склада отстойника ]
 * @property integer      $rework            [ переделка ]
 * @property boolean      $isAdd             [ переделка ]
 * @property boolean      $actNumber         [ Номер акта приема - передачи нарядов ]
 * @property integer      $prodGroup         [ Номер производственной группы ]
 * @property integer      $addGroup          [ Номер производственной группы дополнительной продукции (Органайзеры, Автоодеяла ..)]
 * @property boolean      $withoutLabels     [ Без лейбы ]
 * @property boolean      $alfStake         [ 0.5 ставки ]
 * @property string       $number
 * @property boolean      $stochka
 * @property boolean      $autoSpeed
 * @property ErpLocation  $location
 * @property ErpLocation  $locationstart
 */
class ErpNaryad extends \common\models\LogActiveRecord
{
    const STATUS_READY_TO_WORK='Выдан';
    const STATUS_FROM_SKLAD='Со склада';
    const STATUS_IN_WORK='В работе';
    const STATUS_READY='Готов';
    const STATUS_CHECKED='Проверен';
    const STATUS_CANCEL='Отменен';
    const STATUS_FINISH='Завершен';
    const STATUS_IN_PAUSE='На паузе';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_erp_naryad}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'barcode',
                ],
                'value' => function ($event) {
                    return $this->barcode ?: uniqid();
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'sort',
                ],
                'value' => function ($event) {
                    return $this->sort ?:($this->order ? $this->order->sort : 6);
                },
            ],
        ];
    }

    public $statuses=[
        ''=>'',
        self::STATUS_READY_TO_WORK=>self::STATUS_READY_TO_WORK,
        self::STATUS_FROM_SKLAD=>self::STATUS_FROM_SKLAD,
        self::STATUS_IN_WORK=>self::STATUS_IN_WORK,
        self::STATUS_READY=>self::STATUS_READY,
        self::STATUS_CHECKED=>self::STATUS_CHECKED,
        self::STATUS_CANCEL=>self::STATUS_CANCEL,
        self::STATUS_FINISH=>self::STATUS_FINISH,
        self::STATUS_IN_PAUSE=>self::STATUS_IN_PAUSE,
    ];

    public $prioritets = [
        7=>'Низкий',
        6=>'Обычный',
        5=>'Комплектация',
        4=>'Срочный',
        3=>'Большой заказ',
        2=>'TopSpeed',
        1=>'Срочный-Переделка',
    ];
    
          

    public $prioritetsForView = [
        7=>'Низкий',
        6=>'Обычный',
        5=>'Комплектация',
        4=>'Срочный',
        3=>'Большой заказ'
    ];
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Родитель'),
            'barcode' => Yii::t('app', 'Штрихкод'),
            'reestr' => Yii::t('app', 'Реестр'),
            'article' => Yii::t('app', 'Номенклатура'),
            'order_id' => Yii::t('app', 'Заказ'),
            'scheme_id' => Yii::t('app', 'Схема'),
            'location_id' => Yii::t('app', 'Участок'),
            'status' => Yii::t('app', 'Статус'),
            'sort' => Yii::t('app', 'Приоритет'),
            'oldSort' => Yii::t('app', 'Приоритет(Для комплектации)'),
            'start' => Yii::t('app', 'Участок, с которого выдать'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'user_id' => Yii::t('app', 'Работник'),
            'literal' => Yii::t('app', 'Литерал'),
            'literal_date' => Yii::t('app', 'Дата изменения литерала'),
            'json' => Yii::t('app', 'Json'),
            'logist_id' => Yii::t('app', 'Номер няряда в логистике'),
            'ready_date' => Yii::t('app', 'Дата готовности наряда'),
            'distributed' => Yii::t('app', 'Распределен с диспетчер-склад'),
            'distributed_date' => Yii::t('app', 'Дата распределния'),
            'isNeedTemplate' => Yii::t('app', 'Необходимо иготовить лекало'),
            'isFromSump' => Yii::t('app', 'Из склада-отстойника'),
            'sumpLiteral' => Yii::t('app', 'Литера склада отстойника'),
            'actNumber' => Yii::t('app', 'Номер акта приема - передачи нарядов'),
            'withoutLabels' => Yii::t('app', 'Без лейбы'),
            'halfStake' => Yii::t('app', '0.5 ставки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationstart()
    {
        return $this->hasOne(ErpLocation::className(), ['id' => 'start']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheme()
    {
        return $this->hasOne(ErpScheme::className(), ['id' => 'scheme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(ErpOrder::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    public function getNumber()
    {
        return Yii::t('app', '{n}',['n'=>str_replace('N','',str_replace('L', '', str_replace('B', '', str_replace('D', '/', $this->barcode))))]);
    }

    public function getName()
    {
        return Yii::t('app', 'Наряд №{n}',['n'=>$this->number]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReworkActs()
    {
        return $this->hasMany(LaitovoReworkAct::className(), ['naryad_id' => 'id']);
    }
            
    public function getUser()
    {
        return $this->hasOne(ErpUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpn()
    {
        return $this->hasOne(LogisticsNaryad::className(), ['id' => 'logist_id']);
    }
    
    public function getUserLocationDate()
    {
        $jsons = Json::decode($this->json);
        foreach($jsons['log'] as $json)
        {
            if($json['user']== $this->user->name)  
            return $json['date'];
        }
        return false ;
    }

    public function getProductType(){
        $product_types = ErpProductType::find()->all();

        if($product_types)
        {
            foreach ($product_types as $product_type)
            {
                if ($product_type->rule && preg_match($product_type->rule, $this->article))
                {
                    $product = $product_type;
                    break;
                }
            }
        }return $product ? $product : false;
    }
}
