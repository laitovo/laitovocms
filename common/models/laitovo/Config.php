<?php

namespace common\models\laitovo;

use Yii;

/**
 * This is the model class for table "{{%laitovo_config}}".
 */
class Config extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%laitovo_config}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'json' => Yii::t('app', 'Json'),
        ];
    }
}
