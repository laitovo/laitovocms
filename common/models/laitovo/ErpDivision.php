<?php
/**
 * Created by PhpStorm.
 * User: krapiva
 * Date: 8/23/17
 * Time: 2:15 PM
 */

namespace common\models\laitovo;


use common\models\LogActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

class ErpDivision extends  LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laitovo_erp_division';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'string'],
            [['title'], 'required'],
            [['article'], 'unique'],
            [['article'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Отдел'),
            'article' => Yii::t('app', 'Артикул отдела'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

    static function delete_list()
    {
        return ArrayHelper::map(self::find()->all(),'id','title');
    }


    static function division_list()
    {
        return ArrayHelper::merge([''=>''],ArrayHelper::map(self::find()->all(),'id','title'));
    }
}