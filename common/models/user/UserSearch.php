<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\models\user;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\user\User;

/**
 * UserSearch represents the model behind the search form about `\common\models\user\User`.
 *
 * @author basyan <basyan@yandex.ru>
 */
class UserSearch extends User
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['id', 'subscribe', 'status', 'created_at', 'updated_at', 'author_id', 'updater_id', 'website_id'], 'integer'],
            [['auth_key', 'password_hash', 'password_reset_token', 'access_token', 'name', 'username', 'email', 'phone', 'role', 'image', 'language', 'timezone', 'json'], 'safe'],
        ];
    }

    /**
     * @ignore
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'subscribe' => $this->subscribe,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author_id' => $this->author_id,
            'updater_id' => $this->updater_id,
            'website_id' => $this->website_id,
        ]);

        $query->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'access_token', $this->access_token])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'timezone', $this->timezone])
            ->andFilterWhere(['like', 'json', $this->json]);

        return $dataProvider;
    }
}
