<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\models\user;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see    UserModule
 * @author basyan <basyan@yandex.ru>
 */
class UserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @ignore
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @ignore
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}