<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\models\user;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use common\models\team\Team;

/**
 * Модель для таблицы `{{%user}}`.
 *
 * @property \yii\db\ActiveQuery $author Пользователь. This property is read-only.
 * @property \yii\db\ActiveQuery $updater Пользователь. This property is read-only.
 * @property \yii\db\ActiveQuery $users Пользователи. This property is read-only.
 * @property $email string
 *
 * @author basyan <basyan@yandex.ru>
 */
class User extends \common\models\LogActiveRecord
{
    /**
     * @ignore
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'language',
                ],
                'value' => function ($event) {
                    return $this->language ? $this->language : Yii::$app->request->preferredLanguage;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert || isset($changedAttributes['role'])){
            $auth = Yii::$app->authManager;
            $auth->revokeall($this->id);
            if ($this->role){
                $role = $auth->getRole($this->role);
                $auth->assign($role, $this->id);
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @ignore
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'access_token' => Yii::t('app', 'Access Token'),
            'name' => Yii::t('app', 'ФИО'),
            'username' => Yii::t('app', 'Логин'),
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Телефон'),
            'role' => Yii::t('app', 'Роль'),
            'image' => Yii::t('app', 'Изображение'),
            'subscribe' => Yii::t('app', 'Подписка на рассылку'),
            'status' => Yii::t('app', 'Активен'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'website_id' => Yii::t('app', 'Сайт'),
            'language' => Yii::t('app', 'Язык'),
            'timezone' => Yii::t('app', 'Временная зона'),
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * Роли пользователей
     * @return array Роли
     */
    public function getRoles()
    {
        return $roles=[
            ['id'=>'admin','name'=>Yii::t('app', 'Администратор')],
        ];
    }

    /**
     * Последний кто изменял данные пользователя
     * @return \yii\db\ActiveQuery Пользователь
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * Автор пользователя
     * @return \yii\db\ActiveQuery Пользователь
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * Пользователи которых создал указанный пользователь
     * @return \yii\db\ActiveQuery Пользователи
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['author_id' => 'id']);
    }

    /**
     * Команды членом которых является данный пользователь
     * @return \yii\db\ActiveQuery Команды
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), ['id' => 'team_id'])->viaTable('{{%user_team}}', ['user_id' => 'id']);
    }

    /**
     * @ignore
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @return string ДЛЯ RETURN MODULE
     */
    public function getRole()
    {
        return ($this->id == 21 ? 'admin' : 'no_admin');
    }

    /**
     * @return string ДЛЯ RETURN MODULE
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string ДЛЯ RETURN MODULE
     */

    /**
     * ДЛЯ RETURN MODULE
     * @param $email
     * @return bool
     */
    public function checkByEmail($email)
    {
        return self::find()->where(['email' => $email])->exists();
    }
}
