<?php

namespace common\models\team;

/**
 * This is the ActiveQuery class for [[Team]].
 *
 * @see Team
 * @author basyan <basyan@yandex.ru>
 */
class TeamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @ignore
     * @return Team[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @ignore
     * @return Team|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}