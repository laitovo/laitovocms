<?php

namespace common\models\team;

use Yii;

/**
 * Модель для таблицы `{{%module}}`.
 *
 * @author basyan <basyan@yandex.ru>
 */
class Module extends \common\models\LogActiveRecord
{
    /**
     * @ignore
     */
    public static function tableName()
    {
        return '{{%module}}';
    }

    /**
     * @ignore
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название модуля'),
            'parent' => Yii::t('app', 'Родительский модуль'),
            'description' => Yii::t('app', 'Описание'),
            'type' => Yii::t('app', 'Тип модуля'),
            'last_version' => Yii::t('app', 'Версия'),
            'status' => Yii::t('app', 'Активен'),
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentMod()
    {
        return $this->hasOne(Module::className(), ['name' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModules()
    {
        return $this->hasMany(Module::className(), ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), ['id' => 'team_id'])->viaTable('{{%team_module}}', ['module_name' => 'name']);
    }

    /**
     * @ignore
     * @return ModuleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ModuleQuery(get_called_class());
    }
}
