<?php

namespace common\models\team;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\team\Team;

/**
 * TeamSearch represents the model behind the search form about `\common\models\team\Team`.
 *
 * @author basyan <basyan@yandex.ru>
 */
class TeamSearch extends Team
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['name', 'image', 'json'], 'safe'],
        ];
    }

    /**
     * @ignore
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Team::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author_id' => $this->author_id,
            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'json', $this->json]);

        return $dataProvider;
    }
}
