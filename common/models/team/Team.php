<?php

namespace common\models\team;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use common\models\user\User;
use common\models\order\Category;
use common\models\order\Order;
use common\models\order\Product;
use common\models\website\Website;
use common\models\order\Promo;

/**
 * Модель для таблицы `{{%team}}`.
 *
 * @author basyan <basyan@yandex.ru>
 */
class Team extends \common\models\LogActiveRecord
{
    /**
     * @ignore
     */
    public static function tableName()
    {
        return '{{%team}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',
            ],
        ];
    }

    /**
     * @ignore
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название команды'),
            'image' => Yii::t('app', 'Изображение'),
            'status' => Yii::t('app', 'Активен'),
            'discount' => Yii::t('app', 'Скидка'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyOrders()
    {
        return $this->hasMany(Order::className(), ['client_team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizations()
    {
        return $this->hasMany(Organization::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['team_id' => 'id']);
    }

    /**
     * Последний кто редактировал
     * @return \yii\db\ActiveQuery Редактор
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * Автор команды
     * @return \yii\db\ActiveQuery Автор
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * Версии модулей доступных команде
     * @return \yii\db\ActiveQuery
     */
    public function getTeamModules()
    {
        return $this->hasMany(TeamModule::className(), ['team_id' => 'id']);
    }

    /**
     * Модули доступные команде
     * @return \yii\db\ActiveQuery Модули
     */
    public function getModules()
    {
        return $this->hasMany(Module::className(), ['name' => 'module_name'])->viaTable('{{%team_module}}', ['team_id' => 'id']);
    }

    /**
     * Члены команды
     * @return \yii\db\ActiveQuery Пользователи
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%user_team}}', ['team_id' => 'id']);
    }

    /**
     * Клиенты команды
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Team::className(), ['id' => 'client_id'])->viaTable('team_client', ['team_id' => 'id']);
    }

    /**
     * Сайты команды
     * @return \yii\db\ActiveQuery Сайты
     */
    public function getWebsites()
    {
        return $this->hasMany(Website::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromos()
    {
        return $this->hasMany(Promo::className(), ['team_id' => 'id']);
    }

    /**
     * @ignore
     * @return TeamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TeamQuery(get_called_class());
    }
}
