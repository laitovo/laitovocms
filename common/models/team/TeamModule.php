<?php

namespace common\models\team;

use Yii;

/**
 * This is the model class for table "{{%team_module}}".
 */
class TeamModule extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team_module}}';
    }
}
