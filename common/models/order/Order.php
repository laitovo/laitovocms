<?php

namespace common\models\order;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use common\models\Region;
use common\models\user\User;
use common\models\team\Team;
use common\models\team\Organization;
use common\models\website\Website;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%order}}".
 */
class Order extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'number',
                ],
                'value' => function ($event) {
                    if ($this->organization){
                        $json=Json::decode($this->organization->json ? $this->organization->json : '{}');
                        if (isset($json['nextidorder'])){
                            $json['nextidorder']+=1;
                        } else {
                            $json['nextidorder']=2;
                        }
                        $this->organization->json=Json::encode($json);
                        $this->organization->save();
                    } else {
                        $json=Json::decode($this->team->json ? $this->team->json : '{}');
                        if (isset($json['nextidorder'])){
                            $json['nextidorder']+=1;
                        } else {
                            $json['nextidorder']=2;
                        }
                        $this->team->json=Json::encode($json);
                        $this->team->save();
                    }
                    return $json['nextidorder']-1;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'team_id' => Yii::t('app', 'Команда'),
            'website_id' => Yii::t('app', 'Сайт'),
            'manager_id' => Yii::t('app', 'Менеджер'),
            'number' => Yii::t('app', 'Номер заказа'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'status' => Yii::t('app', 'Статус'),
            'condition' => Yii::t('app', 'Состояние'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite()
    {
        return $this->hasOne(Website::className(), ['id' => 'website_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'client_team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'client_organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuantity()
    {
        return $this->hasMany(Item::className(), ['order_id' => 'id'])->sum('quantity');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSum($nds=false)
    {
        if ($nds){
            return $this->hasMany(Item::className(), ['order_id' => 'id'])->sum('if (nds_in=0,price*quantity*(nds/100+1),price*quantity)');
        } else {
            return $this->hasMany(Item::className(), ['order_id' => 'id'])->sum('if (nds_in=1,price*quantity/(nds/100+1),price*quantity)');
        }
    }    

}
