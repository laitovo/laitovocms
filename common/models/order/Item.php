<?php

namespace common\models\order;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use common\models\user\User;

/**
 * This is the model class for table "{{%item}}".
 */
class Item extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_id'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Заказ'),
            'product_id' => Yii::t('app', 'Товар'),
            'name' => Yii::t('app', 'Название'),
            'price' => Yii::t('app', 'Цена'),
            'retail' => Yii::t('app', 'Розничная(базовая) цена'),
            'nds' => Yii::t('app', 'Ставка НДС (%)'),
            'nds_in' => Yii::t('app', 'НДС включен в цену'),
            'currency' => Yii::t('app', 'Валюта'),
            'quantity' => Yii::t('app', 'Количество'),
            'properties' => Yii::t('app', 'Свойства'),
            'lock' => Yii::t('app', 'Зафиксировать'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'author_id' => Yii::t('app', 'Автор'),
            'updater_id' => Yii::t('app', 'Редактор'),
            'json' => Yii::t('app', 'JSON'),
        ];
    }

    public function price($nds=false, $currency=null, $price=null)
    {
        if ($currency===null)
            $currency=Yii::$app->formatter->currencyCode;

        if ($price===null)
            $price=$this->price;

        if ($nds && !$this->nds_in)
            $price*=$this->nds/100+1;
        elseif (!$nds && $this->nds_in)
            $price/=$this->nds/100+1;
        
        if ($currency && $this->currency!=$currency && ($course=Course::find()->where(['currency_from'=>$this->currency,'currency_to'=>$currency])->one())!==null)
            return $price*$course->k;
        elseif ($currency && $this->currency!=$currency && ($course=Course::find()->where(['currency_to'=>$this->currency,'currency_from'=>$currency])->one())!==null)
            return $price/$course->k;
        return $price;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }
}
