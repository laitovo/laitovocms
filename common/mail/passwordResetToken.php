<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $continue string|null */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token, 'continue' => $continue]);
?>
<table data-module="header" data-bgcolor="Main BG" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eceff3">
	<tr>
		<td align="center">
			<table width="350" border="0" align="center" cellpadding="0" cellspacing="0" class="table-inner">
				<tr>
					<td height="50">
					</td>
				</tr>
				<!--Header Logo-->

				<tr>
					<td align="center" style="line-height: 0px;">
						<img data-crop="false" style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/image_uploads/2016/01/30/KDXJBtRoj8mxPfwGHs0iTnFALQWcu3ed.ico" alt="img" />
					</td>
				</tr>
				<!--end Header Logo-->

				<tr>
					<td height="10">
					</td>
				</tr>
				<!--slogan-->

				<tr>
					<td data-link-style="text-decoration:none; color:#95a5a6;" data-link-color="Slogan Link" data-color="Slogan" data-size="Slogan" align="center" style="font-family: 'Open Sans', Arial, sans-serif; color:#95a5a6; font-size:13px; letter-spacing: 1px;line-height: 28px; font-style:italic;" class="editable">
						<singleline>
							<?=Yii::$app->name?>
						</singleline>
					</td>
				</tr>
				<!--end slogan-->

				<tr>
					<td height="10">
					</td>
				</tr>
			</table>
			<table class="table-inner" width="350" border="0" cellspacing="0" cellpadding="0">
				<!--headline-->

				<tr>
					<td data-bgcolor="Header" data-link-style="text-decoration:none; color:#FFFFFF;" data-link-color="Headline Link" data-color="Headline" data-size="Headline" height="50" align="center" bgcolor="#6ec8c7" style=" border-top-left-radius:6px; border-top-right-radius: 6px;font-family: 'Open sans', Arial, sans-serif; color:#FFFFFF; font-size:20px;font-weight: bold;" class="editable">
						<singleline>
							Восстановление пароля
						</singleline>
					</td>
				</tr>
				<!--end headline-->

			</table>
		</td>
	</tr>
</table>


<table data-module="image" data-bgcolor="Main BG" align="center" bgcolor="#eceff3" width="100%" border="0" cellspacing="0" cellpadding="0" style="position: relative; opacity: 1; z-index: 0;">
	<tr>
		<td align="center">
			<table class="table-inner" width="350" align="center" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="5">
					</td>
				</tr>
				<tr>
					<td align="center">
						<table align="center" class="table-inner" width="300" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td data-link-style="text-decoration:none; color:#6ec8c7;" data-link-color="Content Link" data-color="Content" data-size="Content" align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#7f8c8d; font-size:14px; line-height: 28px;" class="editable">

									<p>Здравствуйте <?= Html::encode($user->name) ?>,</p>

									<p>Вы запросили восстановление Вашего пароля на сайте <?= Html::a(Yii::$app->request->hostinfo, Yii::$app->request->hostinfo)?>. Если Вы этого не делали, проигнорируйте это письмо.</p>

									<p>Чтобы поменять пароль на другой, пройдите по этой ссылке:</p>


								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<table data-module="footer" data-bgcolor="Main BG" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eceff3" class="currentTable">
	<tr>
		<td align="center" class="editable">
			<table class="table-inner" width="350" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td data-bgcolor="List BG" height="25" bgcolor="#f8f8f8">
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
							<tr>
								<td width="30" align="right" valign="top">
									<table width="30" border="0" align="right" cellpadding="0" cellspacing="0">
										<tr>
											<td data-bgcolor="List BG" height="25" bgcolor="#f8f8f8" style="border-bottom-left-radius:6px;">
											</td>
										</tr>
										<tr>
											<td height="25">
											</td>
										</tr>
									</table>
								</td>
								<td rowspan="2" align="center" background="http://www.stampready.net/dashboard/zip_uploads/2015/10/15/7nAaz1sdlkbVjfyQceNmqKFG/Invite/images/cta-bg.png" style="background-size: auto; background-position: 50% 0%; background-repeat: repeat-x;">
									<table data-bgcolor="Button" style="border-radius:6px;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#6ec8c7">
										<!--Button-->

										<tr>
											<td data-link-style="text-decoration:none; color:#FFFFFF;" data-link-color="Button Link" data-color="Button" data-size="Button" width="540" height="50" align="center" style="padding-left: 15px;padding-right: 15px; font-family: 'Open Sans', Arial, sans-serif; font-size: 18px;color:#FFFFFF;font-weight: bold;" class="editable">
												
												<a href="<?=$resetLink?>" style="text-decoration: none; color:#FFFFFF" data-color="Button Link" class="editable">
												<singleline>
													Изменить пароль
												</singleline>
												</a>
											</td>
										</tr>
										<!--end Button-->

									</table>
								</td>
								<td width="30" align="left" valign="top">
									<table width="30" border="0" align="left" cellpadding="0" cellspacing="0">
										<tr>
											<td data-bgcolor="List BG" height="25" bgcolor="#f8f8f8" style="border-bottom-right-radius:6px;">
											</td>
										</tr>
										<tr>
											<td height="25">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table align="center" class="table-inner" width="350" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="10">
					</td>
				</tr>
				<!--social-->

				<tr>
					<td data-link-style="text-decoration:none; color:#FFFFFF;" data-link-color="Preference Link" data-color="Preference" data-size="Preference" align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#7f8c8d; font-size:12px; line-height: 28px;">

						<p>С уважением,<br>
						Администрация <?=Yii::$app->name?></p>

					</td>
				</tr>
				<!--end preference-->

			</table>
		</td>
	</tr>
	<tr>
		<td height="55">
		</td>
	</tr>
</table>

