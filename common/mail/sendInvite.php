<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$link = Yii::$app->urlManager->createAbsoluteUrl(['/site/invite', 'team' => $team->id, 'email'=>$email, 'invite'=>$invite]);
?>
<div class="password-reset">
	<p>Здравствуйте!</p>
    <p><?= Html::encode($user->name) ?> приглашает Вас присоединиться к команде "<?=$team->name?>" на <?=Yii::$app->name?>.</p>
    <p>Чтобы принять приглашение перейдите по ссылке:<br><?= Html::a(Html::encode($link), $link) ?></p>
    <p>--<br>С уважением,<br>команда сервиса <?=Yii::$app->name?></p>
</div>
