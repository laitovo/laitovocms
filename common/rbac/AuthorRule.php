<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\rbac;

use yii\rbac\Rule;

/**
 * Проверка author_id на соответствие с пользователем
 *
 * @author basyan <basyan@yandex.ru>
 */
class AuthorRule extends Rule
{
    /**
     * @var string Название
     */
    public $name = 'isAuthor';


    /**
     * Условия при выполнении которых данное правило сработает
     * @param string|integer $user the user ID.
     * @param \yii\rbac\Item $item the role or permission that this rule is associated width
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['model']) ? $params['model']->author_id == $user : false;
    }
}