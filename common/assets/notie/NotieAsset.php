<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\assets\notie;

use yii\web\AssetBundle;

/**
 * JavaScript плагин уведомлений [notie.js](https://jaredreich.com/projects/notie.js/)
 *
 * @author basyan <basyan@yandex.ru>
 */
class NotieAsset extends AssetBundle
{
	/**
	 * @var string Путь к папке с файлами.
	 */
    public $sourcePath = '@common/assets/notie/assets';
    /**
     * @var array JavaScript файлы.
     */
    public $js = [
        'js/notie.js',
    ];
}