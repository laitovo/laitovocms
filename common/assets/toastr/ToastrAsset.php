<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\assets\toastr;

use yii\web\AssetBundle;

/**
 * JavaScript плагин уведомлений [toastr](http://codeseven.github.io/toastr/)
 *
 * @author basyan <basyan@yandex.ru>
 */
class ToastrAsset extends AssetBundle
{
    /**
     * @var string Путь к папке с файлами.
     */
    public $sourcePath = '@common/assets/toastr/assets';
    /**
     * @var array CSS файлы.
     */
    public $css = [
        'build/toastr.min.css',
    ];
    /**
     * @var array JavaScript файлы.
     */
    public $js = [
        'build/toastr.min.js',
    ];
    /**
     * @var array Требуется JQuery.
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
