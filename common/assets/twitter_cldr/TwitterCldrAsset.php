<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\assets\twitter_cldr;

use yii\web\AssetBundle;

/**
 * JavaScript интернационализация даты, времени, валюты и тд. от twitter.
 * Yii::t() на клиенте будет реализовано в 2.1.x
 *
 * @see [https://github.com/twitter/twitter-cldr-js](https://github.com/twitter/twitter-cldr-js)
 * @see [https://github.com/yiisoft/yii2/issues/274](https://github.com/yiisoft/yii2/issues/274)
 * @author basyan <basyan@yandex.ru>
 */
class TwitterCldrAsset extends AssetBundle
{
	/**
	 * @var string Локаль
	 */
    public $locale='ru';
    /**
     * @var string Путь к папке с файлами.
     */
    public $sourcePath = '@common/assets/twitter_cldr/assets';


    /**
     * @ignore
     */
    public function registerAssetFiles($view)
    {
        if ($this->locale) $this->js[] = 'twitter_cldr/' . $this->locale . '.js';
        parent::registerAssetFiles($view);
    }

}
