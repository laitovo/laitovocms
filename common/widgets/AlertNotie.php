<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\widgets;

use yii;
use common\assets\notie\NotieAsset;

/**
 * Виджет вывода сообщения из сессии. Используется плагин [[\common\assets\notie\NotieAsset]].
 * Отображается только одно сообщение.
 * Пример использования:
 *
 * ```php
 * \Yii::$app->getSession()->setFlash('error', 'This is the message');
 * \Yii::$app->getSession()->setFlash('success', 'This is the message');
 * \Yii::$app->getSession()->setFlash('info', 'This is the message');
 * \Yii::$app->getSession()->setFlash('warning', 'This is the message');
 * ```
 *
 * @author basyan <basyan@yandex.ru>
 */
class AlertNotie extends \yii\bootstrap\Widget
{
    /**
     * @var integer Продолжительность отображения сообщения (сек)
     */
    public $time=4;
    /**
     * @var array Типы сообщений.
     */
    public $alertTypes = [
        'success' => 1,
        'warning' => 2,
        'error'   => 3,
        'info'    => 4
    ];


    /**
     * @ignore
     */
    public function init()
    {
        parent::init();

        $view = $this->getView();
        NotieAsset::register($view);

        $session = \Yii::$app->getSession();
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $message = (string) $data;

                $view->registerJs("

                    notie.alert(".$this->alertTypes[$type].", '".$message."', ".$this->time.");

                    ", \yii\web\View::POS_END);

                $session->removeFlash($type);

                break;
            }
        }
    }
}
